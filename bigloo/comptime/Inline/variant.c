/*===========================================================================*/
/*   (Inline/variant.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Inline/variant.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INLINE_VARIANT_TYPE_DEFINITIONS
#define BGL_INLINE_VARIANT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_localzf2variantzf2_bgl
	{
		bool_t BgL_variantz00;
	}                         *BgL_localzf2variantzf2_bglt;


#endif													// BGL_INLINE_VARIANT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2occurrencezd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt, long);
	static obj_t BGl_z62localzf2variantzd2occurrencewz42zzinline_variantz00(obj_t,
		obj_t);
	static obj_t BGl_z62localzf2variantzd2idz42zzinline_variantz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2occurrencewzd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt, long);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62shrinkzd2argsz12za2zzinline_variantz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzinline_variantz00 = BUNSPEC;
	static obj_t
		BGl_z62localzf2variantzd2removablezd2setz12z82zzinline_variantz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_atomz00zzast_nodez00;
	BGL_EXPORTED_DECL bool_t
		BGl_localzf2variantzd2variantz20zzinline_variantz00(BgL_localz00_bglt);
	static obj_t BGl_z62localzf2variantzf3z63zzinline_variantz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_localzf2variantzd2occurrencewz20zzinline_variantz00(BgL_localz00_bglt);
	static obj_t
		BGl_z62localzf2variantzd2namezd2setz12z82zzinline_variantz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62localzf2variantzd2namez42zzinline_variantz00(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzinline_variantz00(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzinline_variantz00(void);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_localzf2variantzd2valuez20zzinline_variantz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2removablez20zzinline_variantz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_localzf2variantzd2userzf3zd3zzinline_variantz00(BgL_localz00_bglt);
	static BgL_localz00_bglt
		BGl_z62localzf2variantzd2nilz42zzinline_variantz00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62localzf2variantzd2variantz42zzinline_variantz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2variantzd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt, bool_t);
	BGL_EXPORTED_DECL bool_t BGl_localzf2variantzf3z01zzinline_variantz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62localzf2variantzd2valuezd2setz12z82zzinline_variantz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzinline_variantz00(void);
	static BgL_typez00_bglt
		BGl_z62localzf2variantzd2typez42zzinline_variantz00(obj_t, obj_t);
	static obj_t
		BGl_z62localzf2variantzd2variantzd2setz12z82zzinline_variantz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62substitutionsz62zzinline_variantz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62localzf2variantzd2occurrencez42zzinline_variantz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62localzf2variantzd2accesszd2setz12z82zzinline_variantz00(obj_t, obj_t,
		obj_t);
	static BgL_localz00_bglt
		BGl_z62makezd2localzf2variantz42zzinline_variantz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t
		BGl_z62localzf2variantzd2userzf3zd2setz12z71zzinline_variantz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62localzf2variantzd2typezd2setz12z82zzinline_variantz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_appz00_bglt
		BGl_removezd2invariantzd2argsz12z12zzinline_variantz00(BgL_appz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2accesszd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt, obj_t);
	BGL_EXPORTED_DECL long
		BGl_localzf2variantzd2keyz20zzinline_variantz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_variantzd2argszd2zzinline_variantz00(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzinline_variantz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2namez20zzinline_variantz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_shrinkzd2argsz12zc0zzinline_variantz00(BgL_variablez00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_localzf2variantzf2zzinline_variantz00 = BUNSPEC;
	static obj_t
		BGl_z62localzf2variantzd2fastzd2alphazd2setz12z50zzinline_variantz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62localzf2variantzd2fastzd2alphaz90zzinline_variantz00(obj_t, obj_t);
	static obj_t
		BGl_z62localzf2variantzd2occurrencezd2setz12z82zzinline_variantz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2valuezd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt, BgL_valuez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2removablezd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2namezd2setz12ze0zzinline_variantz00(BgL_localz00_bglt,
		obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzinline_variantz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinline_variantz00(void);
	BGL_EXPORTED_DECL long
		BGl_localzf2variantzd2occurrencez20zzinline_variantz00(BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzinline_variantz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinline_variantz00(void);
	static obj_t BGl_z62localzf2variantzd2removablez42zzinline_variantz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzinline_variantz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_substitutionsz00zzinline_variantz00(BgL_variablez00_bglt, obj_t, obj_t);
	static obj_t BGl_z62localzf2variantzd2accessz42zzinline_variantz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_localzf2variantzd2typez20zzinline_variantz00(BgL_localz00_bglt);
	static obj_t BGl_z62invariantzd2argszb0zzinline_variantz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2accessz20zzinline_variantz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2userzf3zd2setz12z13zzinline_variantz00
		(BgL_localz00_bglt, bool_t);
	extern obj_t BGl_valuez00zzast_varz00;
	static BgL_appz00_bglt
		BGl_z62removezd2invariantzd2argsz12z70zzinline_variantz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2idz20zzinline_variantz00(BgL_localz00_bglt);
	static BgL_localz00_bglt BGl_z62lambda1630z62zzinline_variantz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62localzf2variantzd2occurrencewzd2setz12z82zzinline_variantz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_localzf2variantzd2nilz20zzinline_variantz00(void);
	static BgL_localz00_bglt BGl_z62lambda1647z62zzinline_variantz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2localzf2variantz20zzinline_variantz00(obj_t, obj_t,
		BgL_typez00_bglt, BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long,
		bool_t, long, bool_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static BgL_localz00_bglt BGl_z62lambda1652z62zzinline_variantz00(obj_t,
		obj_t);
	static obj_t BGl_z62localzf2variantzd2keyz42zzinline_variantz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2fastzd2alphazf2zzinline_variantz00(BgL_localz00_bglt);
	static BgL_valuez00_bglt
		BGl_z62localzf2variantzd2valuez42zzinline_variantz00(obj_t, obj_t);
	static obj_t BGl_z62localzf2variantzd2userzf3zb1zzinline_variantz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_invariantzd2argszd2zzinline_variantz00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	static obj_t BGl_z62variantzd2argszb0zzinline_variantz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2fastzd2alphazd2setz12z32zzinline_variantz00
		(BgL_localz00_bglt, obj_t);
	static obj_t BGl_z62lambda1682z62zzinline_variantz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2variantzd2typezd2setz12ze0zzinline_variantz00(BgL_localz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62lambda1683z62zzinline_variantz00(obj_t, obj_t, obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzf3zd2envzd3zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1840z00,
		BGl_z62localzf2variantzf3z63zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2localzf2variantzd2envzf2zzinline_variantz00,
		BgL_bgl_za762makeza7d2localza71841za7,
		BGl_z62makezd2localzf2variantz42zzinline_variantz00, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2keyzd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1842z00,
		BGl_z62localzf2variantzd2keyz42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2namezd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1843z00,
		BGl_z62localzf2variantzd2namez42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2variantzd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1844z00,
		BGl_z62localzf2variantzd2variantz42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2userzf3zd2setz12zd2envzc1zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1845z00,
		BGl_z62localzf2variantzd2userzf3zd2setz12z71zzinline_variantz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2occurrencezd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1846z00,
		BGl_z62localzf2variantzd2occurrencez42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2valuezd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1847z00,
		BGl_z62localzf2variantzd2valuez42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2removablezd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1848z00,
		BGl_z62localzf2variantzd2removablez42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_variantzd2argszd2envz00zzinline_variantz00,
		BgL_bgl_za762variantza7d2arg1849z00,
		BGl_z62variantzd2argszb0zzinline_variantz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1837z00zzinline_variantz00,
		BgL_bgl_string1837za700za7za7i1850za7, "inline_variant", 14);
	      DEFINE_STRING(BGl_string1838z00zzinline_variantz00,
		BgL_bgl_string1838za700za7za7i1851za7,
		"_ inline_variant local/variant bool variant done read ", 54);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1831z00zzinline_variantz00,
		BgL_bgl_za762lambda1683za7621852z00,
		BGl_z62lambda1683z62zzinline_variantz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1832z00zzinline_variantz00,
		BgL_bgl_za762lambda1682za7621853z00,
		BGl_z62lambda1682z62zzinline_variantz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1833z00zzinline_variantz00,
		BgL_bgl_za762lambda1652za7621854z00,
		BGl_z62lambda1652z62zzinline_variantz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1834z00zzinline_variantz00,
		BgL_bgl_za762za7c3za704anonymo1855za7,
		BGl_z62zc3z04anonymousza31651ze3ze5zzinline_variantz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1835z00zzinline_variantz00,
		BgL_bgl_za762lambda1647za7621856z00,
		BGl_z62lambda1647z62zzinline_variantz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1836z00zzinline_variantz00,
		BgL_bgl_za762lambda1630za7621857z00,
		BGl_z62lambda1630z62zzinline_variantz00, 0L, BUNSPEC, 14);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2userzf3zd2envz01zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1858z00,
		BGl_z62localzf2variantzd2userzf3zb1zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2accesszd2setz12zd2envz32zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1859z00,
		BGl_z62localzf2variantzd2accesszd2setz12z82zzinline_variantz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2namezd2setz12zd2envz32zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1860z00,
		BGl_z62localzf2variantzd2namezd2setz12z82zzinline_variantz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2typezd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1861z00,
		BGl_z62localzf2variantzd2typez42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2accesszd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1862z00,
		BGl_z62localzf2variantzd2accessz42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2fastzd2alphazd2envz20zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1863z00,
		BGl_z62localzf2variantzd2fastzd2alphaz90zzinline_variantz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2occurrencewzd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1864z00,
		BGl_z62localzf2variantzd2occurrencewz42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2variantzd2setz12zd2envz32zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1865z00,
		BGl_z62localzf2variantzd2variantzd2setz12z82zzinline_variantz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2nilzd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1866z00,
		BGl_z62localzf2variantzd2nilz42zzinline_variantz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_invariantzd2argszd2envz00zzinline_variantz00,
		BgL_bgl_za762invariantza7d2a1867z00,
		BGl_z62invariantzd2argszb0zzinline_variantz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_substitutionszd2envzd2zzinline_variantz00,
		BgL_bgl_za762substitutions1868za7,
		BGl_z62substitutionsz62zzinline_variantz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_removezd2invariantzd2argsz12zd2envzc0zzinline_variantz00,
		BgL_bgl_za762removeza7d2inva1869z00,
		BGl_z62removezd2invariantzd2argsz12z70zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2occurrencewzd2setz12zd2envz32zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1870z00,
		BGl_z62localzf2variantzd2occurrencewzd2setz12z82zzinline_variantz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_shrinkzd2argsz12zd2envz12zzinline_variantz00,
		BgL_bgl_za762shrinkza7d2args1871z00,
		BGl_z62shrinkzd2argsz12za2zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2fastzd2alphazd2setz12zd2envze0zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1872z00,
		BGl_z62localzf2variantzd2fastzd2alphazd2setz12z50zzinline_variantz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2occurrencezd2setz12zd2envz32zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1873z00,
		BGl_z62localzf2variantzd2occurrencezd2setz12z82zzinline_variantz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2idzd2envzf2zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1874z00,
		BGl_z62localzf2variantzd2idz42zzinline_variantz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2typezd2setz12zd2envz32zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1875z00,
		BGl_z62localzf2variantzd2typezd2setz12z82zzinline_variantz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2valuezd2setz12zd2envz32zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1876z00,
		BGl_z62localzf2variantzd2valuezd2setz12z82zzinline_variantz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2variantzd2removablezd2setz12zd2envz32zzinline_variantz00,
		BgL_bgl_za762localza7f2varia1877z00,
		BGl_z62localzf2variantzd2removablezd2setz12z82zzinline_variantz00, 0L,
		BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzinline_variantz00));
		     ADD_ROOT((void *) (&BGl_localzf2variantzf2zzinline_variantz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzinline_variantz00(long
		BgL_checksumz00_2482, char *BgL_fromz00_2483)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinline_variantz00))
				{
					BGl_requirezd2initializa7ationz75zzinline_variantz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinline_variantz00();
					BGl_libraryzd2moduleszd2initz00zzinline_variantz00();
					BGl_cnstzd2initzd2zzinline_variantz00();
					BGl_importedzd2moduleszd2initz00zzinline_variantz00();
					BGl_objectzd2initzd2zzinline_variantz00();
					return BGl_methodzd2initzd2zzinline_variantz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinline_variantz00(void)
	{
		{	/* Inline/variant.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "inline_variant");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"inline_variant");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "inline_variant");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"inline_variant");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"inline_variant");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"inline_variant");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"inline_variant");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"inline_variant");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinline_variantz00(void)
	{
		{	/* Inline/variant.scm 15 */
			{	/* Inline/variant.scm 15 */
				obj_t BgL_cportz00_2417;

				{	/* Inline/variant.scm 15 */
					obj_t BgL_stringz00_2424;

					BgL_stringz00_2424 = BGl_string1838z00zzinline_variantz00;
					{	/* Inline/variant.scm 15 */
						obj_t BgL_startz00_2425;

						BgL_startz00_2425 = BINT(0L);
						{	/* Inline/variant.scm 15 */
							obj_t BgL_endz00_2426;

							BgL_endz00_2426 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2424)));
							{	/* Inline/variant.scm 15 */

								BgL_cportz00_2417 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2424, BgL_startz00_2425, BgL_endz00_2426);
				}}}}
				{
					long BgL_iz00_2418;

					BgL_iz00_2418 = 6L;
				BgL_loopz00_2419:
					if ((BgL_iz00_2418 == -1L))
						{	/* Inline/variant.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Inline/variant.scm 15 */
							{	/* Inline/variant.scm 15 */
								obj_t BgL_arg1839z00_2420;

								{	/* Inline/variant.scm 15 */

									{	/* Inline/variant.scm 15 */
										obj_t BgL_locationz00_2422;

										BgL_locationz00_2422 = BBOOL(((bool_t) 0));
										{	/* Inline/variant.scm 15 */

											BgL_arg1839z00_2420 =
												BGl_readz00zz__readerz00(BgL_cportz00_2417,
												BgL_locationz00_2422);
										}
									}
								}
								{	/* Inline/variant.scm 15 */
									int BgL_tmpz00_2510;

									BgL_tmpz00_2510 = (int) (BgL_iz00_2418);
									CNST_TABLE_SET(BgL_tmpz00_2510, BgL_arg1839z00_2420);
							}}
							{	/* Inline/variant.scm 15 */
								int BgL_auxz00_2423;

								BgL_auxz00_2423 = (int) ((BgL_iz00_2418 - 1L));
								{
									long BgL_iz00_2515;

									BgL_iz00_2515 = (long) (BgL_auxz00_2423);
									BgL_iz00_2418 = BgL_iz00_2515;
									goto BgL_loopz00_2419;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinline_variantz00(void)
	{
		{	/* Inline/variant.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* make-local/variant */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2localzf2variantz20zzinline_variantz00(obj_t BgL_id1114z00_3,
		obj_t BgL_name1115z00_4, BgL_typez00_bglt BgL_type1116z00_5,
		BgL_valuez00_bglt BgL_value1117z00_6, obj_t BgL_access1118z00_7,
		obj_t BgL_fastzd2alpha1119zd2_8, obj_t BgL_removable1120z00_9,
		long BgL_occurrence1121z00_10, long BgL_occurrencew1122z00_11,
		bool_t BgL_userzf31123zf3_12, long BgL_key1124z00_13,
		bool_t BgL_variant1125z00_14)
	{
		{	/* Inline/variant.sch 43 */
			{	/* Inline/variant.sch 43 */
				BgL_localz00_bglt BgL_new1130z00_2428;

				{	/* Inline/variant.sch 43 */
					BgL_localz00_bglt BgL_tmp1128z00_2429;
					BgL_localzf2variantzf2_bglt BgL_wide1129z00_2430;

					{
						BgL_localz00_bglt BgL_auxz00_2518;

						{	/* Inline/variant.sch 43 */
							BgL_localz00_bglt BgL_new1127z00_2431;

							BgL_new1127z00_2431 =
								((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_localz00_bgl))));
							{	/* Inline/variant.sch 43 */
								long BgL_arg1306z00_2432;

								BgL_arg1306z00_2432 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1127z00_2431),
									BgL_arg1306z00_2432);
							}
							{	/* Inline/variant.sch 43 */
								BgL_objectz00_bglt BgL_tmpz00_2523;

								BgL_tmpz00_2523 = ((BgL_objectz00_bglt) BgL_new1127z00_2431);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2523, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1127z00_2431);
							BgL_auxz00_2518 = BgL_new1127z00_2431;
						}
						BgL_tmp1128z00_2429 = ((BgL_localz00_bglt) BgL_auxz00_2518);
					}
					BgL_wide1129z00_2430 =
						((BgL_localzf2variantzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_localzf2variantzf2_bgl))));
					{	/* Inline/variant.sch 43 */
						obj_t BgL_auxz00_2531;
						BgL_objectz00_bglt BgL_tmpz00_2529;

						BgL_auxz00_2531 = ((obj_t) BgL_wide1129z00_2430);
						BgL_tmpz00_2529 = ((BgL_objectz00_bglt) BgL_tmp1128z00_2429);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2529, BgL_auxz00_2531);
					}
					((BgL_objectz00_bglt) BgL_tmp1128z00_2429);
					{	/* Inline/variant.sch 43 */
						long BgL_arg1305z00_2433;

						BgL_arg1305z00_2433 =
							BGL_CLASS_NUM(BGl_localzf2variantzf2zzinline_variantz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1128z00_2429), BgL_arg1305z00_2433);
					}
					BgL_new1130z00_2428 = ((BgL_localz00_bglt) BgL_tmp1128z00_2429);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1130z00_2428)))->BgL_idz00) =
					((obj_t) BgL_id1114z00_3), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_namez00) =
					((obj_t) BgL_name1115z00_4), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1116z00_5), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1117z00_6), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_accessz00) =
					((obj_t) BgL_access1118z00_7), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1119zd2_8), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_removablez00) =
					((obj_t) BgL_removable1120z00_9), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_occurrencez00) =
					((long) BgL_occurrence1121z00_10), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1122z00_11), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1130z00_2428)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31123zf3_12), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1130z00_2428)))->BgL_keyz00) =
					((long) BgL_key1124z00_13), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1130z00_2428)))->BgL_valzd2noescapezd2) =
					((obj_t) BTRUE), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1130z00_2428)))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2variantzf2_bglt BgL_auxz00_2565;

					{
						obj_t BgL_auxz00_2566;

						{	/* Inline/variant.sch 43 */
							BgL_objectz00_bglt BgL_tmpz00_2567;

							BgL_tmpz00_2567 = ((BgL_objectz00_bglt) BgL_new1130z00_2428);
							BgL_auxz00_2566 = BGL_OBJECT_WIDENING(BgL_tmpz00_2567);
						}
						BgL_auxz00_2565 = ((BgL_localzf2variantzf2_bglt) BgL_auxz00_2566);
					}
					((((BgL_localzf2variantzf2_bglt) COBJECT(BgL_auxz00_2565))->
							BgL_variantz00) = ((bool_t) BgL_variant1125z00_14), BUNSPEC);
				}
				return BgL_new1130z00_2428;
			}
		}

	}



/* &make-local/variant */
	BgL_localz00_bglt BGl_z62makezd2localzf2variantz42zzinline_variantz00(obj_t
		BgL_envz00_2301, obj_t BgL_id1114z00_2302, obj_t BgL_name1115z00_2303,
		obj_t BgL_type1116z00_2304, obj_t BgL_value1117z00_2305,
		obj_t BgL_access1118z00_2306, obj_t BgL_fastzd2alpha1119zd2_2307,
		obj_t BgL_removable1120z00_2308, obj_t BgL_occurrence1121z00_2309,
		obj_t BgL_occurrencew1122z00_2310, obj_t BgL_userzf31123zf3_2311,
		obj_t BgL_key1124z00_2312, obj_t BgL_variant1125z00_2313)
	{
		{	/* Inline/variant.sch 43 */
			return
				BGl_makezd2localzf2variantz20zzinline_variantz00(BgL_id1114z00_2302,
				BgL_name1115z00_2303, ((BgL_typez00_bglt) BgL_type1116z00_2304),
				((BgL_valuez00_bglt) BgL_value1117z00_2305), BgL_access1118z00_2306,
				BgL_fastzd2alpha1119zd2_2307, BgL_removable1120z00_2308,
				(long) CINT(BgL_occurrence1121z00_2309),
				(long) CINT(BgL_occurrencew1122z00_2310),
				CBOOL(BgL_userzf31123zf3_2311), (long) CINT(BgL_key1124z00_2312),
				CBOOL(BgL_variant1125z00_2313));
		}

	}



/* local/variant? */
	BGL_EXPORTED_DEF bool_t BGl_localzf2variantzf3z01zzinline_variantz00(obj_t
		BgL_objz00_15)
	{
		{	/* Inline/variant.sch 44 */
			{	/* Inline/variant.sch 44 */
				obj_t BgL_classz00_2434;

				BgL_classz00_2434 = BGl_localzf2variantzf2zzinline_variantz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* Inline/variant.sch 44 */
						BgL_objectz00_bglt BgL_arg1807z00_2435;

						BgL_arg1807z00_2435 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Inline/variant.sch 44 */
								long BgL_idxz00_2436;

								BgL_idxz00_2436 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2435);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2436 + 3L)) == BgL_classz00_2434);
							}
						else
							{	/* Inline/variant.sch 44 */
								bool_t BgL_res1821z00_2439;

								{	/* Inline/variant.sch 44 */
									obj_t BgL_oclassz00_2440;

									{	/* Inline/variant.sch 44 */
										obj_t BgL_arg1815z00_2441;
										long BgL_arg1816z00_2442;

										BgL_arg1815z00_2441 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Inline/variant.sch 44 */
											long BgL_arg1817z00_2443;

											BgL_arg1817z00_2443 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2435);
											BgL_arg1816z00_2442 = (BgL_arg1817z00_2443 - OBJECT_TYPE);
										}
										BgL_oclassz00_2440 =
											VECTOR_REF(BgL_arg1815z00_2441, BgL_arg1816z00_2442);
									}
									{	/* Inline/variant.sch 44 */
										bool_t BgL__ortest_1115z00_2444;

										BgL__ortest_1115z00_2444 =
											(BgL_classz00_2434 == BgL_oclassz00_2440);
										if (BgL__ortest_1115z00_2444)
											{	/* Inline/variant.sch 44 */
												BgL_res1821z00_2439 = BgL__ortest_1115z00_2444;
											}
										else
											{	/* Inline/variant.sch 44 */
												long BgL_odepthz00_2445;

												{	/* Inline/variant.sch 44 */
													obj_t BgL_arg1804z00_2446;

													BgL_arg1804z00_2446 = (BgL_oclassz00_2440);
													BgL_odepthz00_2445 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2446);
												}
												if ((3L < BgL_odepthz00_2445))
													{	/* Inline/variant.sch 44 */
														obj_t BgL_arg1802z00_2447;

														{	/* Inline/variant.sch 44 */
															obj_t BgL_arg1803z00_2448;

															BgL_arg1803z00_2448 = (BgL_oclassz00_2440);
															BgL_arg1802z00_2447 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2448,
																3L);
														}
														BgL_res1821z00_2439 =
															(BgL_arg1802z00_2447 == BgL_classz00_2434);
													}
												else
													{	/* Inline/variant.sch 44 */
														BgL_res1821z00_2439 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1821z00_2439;
							}
					}
				else
					{	/* Inline/variant.sch 44 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &local/variant? */
	obj_t BGl_z62localzf2variantzf3z63zzinline_variantz00(obj_t BgL_envz00_2314,
		obj_t BgL_objz00_2315)
	{
		{	/* Inline/variant.sch 44 */
			return
				BBOOL(BGl_localzf2variantzf3z01zzinline_variantz00(BgL_objz00_2315));
		}

	}



/* local/variant-nil */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_localzf2variantzd2nilz20zzinline_variantz00(void)
	{
		{	/* Inline/variant.sch 45 */
			{	/* Inline/variant.sch 45 */
				obj_t BgL_classz00_1858;

				BgL_classz00_1858 = BGl_localzf2variantzf2zzinline_variantz00;
				{	/* Inline/variant.sch 45 */
					obj_t BgL__ortest_1117z00_1859;

					BgL__ortest_1117z00_1859 = BGL_CLASS_NIL(BgL_classz00_1858);
					if (CBOOL(BgL__ortest_1117z00_1859))
						{	/* Inline/variant.sch 45 */
							return ((BgL_localz00_bglt) BgL__ortest_1117z00_1859);
						}
					else
						{	/* Inline/variant.sch 45 */
							return
								((BgL_localz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1858));
						}
				}
			}
		}

	}



/* &local/variant-nil */
	BgL_localz00_bglt BGl_z62localzf2variantzd2nilz42zzinline_variantz00(obj_t
		BgL_envz00_2316)
	{
		{	/* Inline/variant.sch 45 */
			return BGl_localzf2variantzd2nilz20zzinline_variantz00();
		}

	}



/* local/variant-variant */
	BGL_EXPORTED_DEF bool_t
		BGl_localzf2variantzd2variantz20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_16)
	{
		{	/* Inline/variant.sch 46 */
			{
				BgL_localzf2variantzf2_bglt BgL_auxz00_2611;

				{
					obj_t BgL_auxz00_2612;

					{	/* Inline/variant.sch 46 */
						BgL_objectz00_bglt BgL_tmpz00_2613;

						BgL_tmpz00_2613 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_2612 = BGL_OBJECT_WIDENING(BgL_tmpz00_2613);
					}
					BgL_auxz00_2611 = ((BgL_localzf2variantzf2_bglt) BgL_auxz00_2612);
				}
				return
					(((BgL_localzf2variantzf2_bglt) COBJECT(BgL_auxz00_2611))->
					BgL_variantz00);
			}
		}

	}



/* &local/variant-variant */
	obj_t BGl_z62localzf2variantzd2variantz42zzinline_variantz00(obj_t
		BgL_envz00_2317, obj_t BgL_oz00_2318)
	{
		{	/* Inline/variant.sch 46 */
			return
				BBOOL(BGl_localzf2variantzd2variantz20zzinline_variantz00(
					((BgL_localz00_bglt) BgL_oz00_2318)));
		}

	}



/* local/variant-variant-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2variantzd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt BgL_oz00_17, bool_t BgL_vz00_18)
	{
		{	/* Inline/variant.sch 47 */
			{
				BgL_localzf2variantzf2_bglt BgL_auxz00_2621;

				{
					obj_t BgL_auxz00_2622;

					{	/* Inline/variant.sch 47 */
						BgL_objectz00_bglt BgL_tmpz00_2623;

						BgL_tmpz00_2623 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_2622 = BGL_OBJECT_WIDENING(BgL_tmpz00_2623);
					}
					BgL_auxz00_2621 = ((BgL_localzf2variantzf2_bglt) BgL_auxz00_2622);
				}
				return
					((((BgL_localzf2variantzf2_bglt) COBJECT(BgL_auxz00_2621))->
						BgL_variantz00) = ((bool_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &local/variant-variant-set! */
	obj_t BGl_z62localzf2variantzd2variantzd2setz12z82zzinline_variantz00(obj_t
		BgL_envz00_2319, obj_t BgL_oz00_2320, obj_t BgL_vz00_2321)
	{
		{	/* Inline/variant.sch 47 */
			return
				BGl_localzf2variantzd2variantzd2setz12ze0zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2320), CBOOL(BgL_vz00_2321));
		}

	}



/* local/variant-key */
	BGL_EXPORTED_DEF long
		BGl_localzf2variantzd2keyz20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_19)
	{
		{	/* Inline/variant.sch 48 */
			return
				(((BgL_localz00_bglt) COBJECT(
						((BgL_localz00_bglt) BgL_oz00_19)))->BgL_keyz00);
		}

	}



/* &local/variant-key */
	obj_t BGl_z62localzf2variantzd2keyz42zzinline_variantz00(obj_t
		BgL_envz00_2322, obj_t BgL_oz00_2323)
	{
		{	/* Inline/variant.sch 48 */
			return
				BINT(BGl_localzf2variantzd2keyz20zzinline_variantz00(
					((BgL_localz00_bglt) BgL_oz00_2323)));
		}

	}



/* local/variant-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_localzf2variantzd2userzf3zd3zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_22)
	{
		{	/* Inline/variant.sch 50 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_22)))->BgL_userzf3zf3);
		}

	}



/* &local/variant-user? */
	obj_t BGl_z62localzf2variantzd2userzf3zb1zzinline_variantz00(obj_t
		BgL_envz00_2324, obj_t BgL_oz00_2325)
	{
		{	/* Inline/variant.sch 50 */
			return
				BBOOL(BGl_localzf2variantzd2userzf3zd3zzinline_variantz00(
					((BgL_localz00_bglt) BgL_oz00_2325)));
		}

	}



/* local/variant-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2userzf3zd2setz12z13zzinline_variantz00
		(BgL_localz00_bglt BgL_oz00_23, bool_t BgL_vz00_24)
	{
		{	/* Inline/variant.sch 51 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_23)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_24), BUNSPEC);
		}

	}



/* &local/variant-user?-set! */
	obj_t BGl_z62localzf2variantzd2userzf3zd2setz12z71zzinline_variantz00(obj_t
		BgL_envz00_2326, obj_t BgL_oz00_2327, obj_t BgL_vz00_2328)
	{
		{	/* Inline/variant.sch 51 */
			return
				BGl_localzf2variantzd2userzf3zd2setz12z13zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2327), CBOOL(BgL_vz00_2328));
		}

	}



/* local/variant-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_localzf2variantzd2occurrencewz20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_25)
	{
		{	/* Inline/variant.sch 52 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_25)))->BgL_occurrencewz00);
		}

	}



/* &local/variant-occurrencew */
	obj_t BGl_z62localzf2variantzd2occurrencewz42zzinline_variantz00(obj_t
		BgL_envz00_2329, obj_t BgL_oz00_2330)
	{
		{	/* Inline/variant.sch 52 */
			return
				BINT(BGl_localzf2variantzd2occurrencewz20zzinline_variantz00(
					((BgL_localz00_bglt) BgL_oz00_2330)));
		}

	}



/* local/variant-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2occurrencewzd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt BgL_oz00_26, long BgL_vz00_27)
	{
		{	/* Inline/variant.sch 53 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_26)))->BgL_occurrencewz00) =
				((long) BgL_vz00_27), BUNSPEC);
		}

	}



/* &local/variant-occurrencew-set! */
	obj_t
		BGl_z62localzf2variantzd2occurrencewzd2setz12z82zzinline_variantz00(obj_t
		BgL_envz00_2331, obj_t BgL_oz00_2332, obj_t BgL_vz00_2333)
	{
		{	/* Inline/variant.sch 53 */
			return
				BGl_localzf2variantzd2occurrencewzd2setz12ze0zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2332), (long) CINT(BgL_vz00_2333));
		}

	}



/* local/variant-occurrence */
	BGL_EXPORTED_DEF long
		BGl_localzf2variantzd2occurrencez20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_28)
	{
		{	/* Inline/variant.sch 54 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_28)))->BgL_occurrencez00);
		}

	}



/* &local/variant-occurrence */
	obj_t BGl_z62localzf2variantzd2occurrencez42zzinline_variantz00(obj_t
		BgL_envz00_2334, obj_t BgL_oz00_2335)
	{
		{	/* Inline/variant.sch 54 */
			return
				BINT(BGl_localzf2variantzd2occurrencez20zzinline_variantz00(
					((BgL_localz00_bglt) BgL_oz00_2335)));
		}

	}



/* local/variant-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2occurrencezd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt BgL_oz00_29, long BgL_vz00_30)
	{
		{	/* Inline/variant.sch 55 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_29)))->BgL_occurrencez00) =
				((long) BgL_vz00_30), BUNSPEC);
		}

	}



/* &local/variant-occurrence-set! */
	obj_t BGl_z62localzf2variantzd2occurrencezd2setz12z82zzinline_variantz00(obj_t
		BgL_envz00_2336, obj_t BgL_oz00_2337, obj_t BgL_vz00_2338)
	{
		{	/* Inline/variant.sch 55 */
			return
				BGl_localzf2variantzd2occurrencezd2setz12ze0zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2337), (long) CINT(BgL_vz00_2338));
		}

	}



/* local/variant-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2removablez20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_31)
	{
		{	/* Inline/variant.sch 56 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_31)))->BgL_removablez00);
		}

	}



/* &local/variant-removable */
	obj_t BGl_z62localzf2variantzd2removablez42zzinline_variantz00(obj_t
		BgL_envz00_2339, obj_t BgL_oz00_2340)
	{
		{	/* Inline/variant.sch 56 */
			return
				BGl_localzf2variantzd2removablez20zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2340));
		}

	}



/* local/variant-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2removablezd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt BgL_oz00_32, obj_t BgL_vz00_33)
	{
		{	/* Inline/variant.sch 57 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_32)))->BgL_removablez00) =
				((obj_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &local/variant-removable-set! */
	obj_t BGl_z62localzf2variantzd2removablezd2setz12z82zzinline_variantz00(obj_t
		BgL_envz00_2341, obj_t BgL_oz00_2342, obj_t BgL_vz00_2343)
	{
		{	/* Inline/variant.sch 57 */
			return
				BGl_localzf2variantzd2removablezd2setz12ze0zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2342), BgL_vz00_2343);
		}

	}



/* local/variant-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2fastzd2alphazf2zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_34)
	{
		{	/* Inline/variant.sch 58 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_34)))->BgL_fastzd2alphazd2);
		}

	}



/* &local/variant-fast-alpha */
	obj_t BGl_z62localzf2variantzd2fastzd2alphaz90zzinline_variantz00(obj_t
		BgL_envz00_2344, obj_t BgL_oz00_2345)
	{
		{	/* Inline/variant.sch 58 */
			return
				BGl_localzf2variantzd2fastzd2alphazf2zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2345));
		}

	}



/* local/variant-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2fastzd2alphazd2setz12z32zzinline_variantz00
		(BgL_localz00_bglt BgL_oz00_35, obj_t BgL_vz00_36)
	{
		{	/* Inline/variant.sch 59 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_35)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &local/variant-fast-alpha-set! */
	obj_t
		BGl_z62localzf2variantzd2fastzd2alphazd2setz12z50zzinline_variantz00(obj_t
		BgL_envz00_2346, obj_t BgL_oz00_2347, obj_t BgL_vz00_2348)
	{
		{	/* Inline/variant.sch 59 */
			return
				BGl_localzf2variantzd2fastzd2alphazd2setz12z32zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2347), BgL_vz00_2348);
		}

	}



/* local/variant-access */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2accessz20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_37)
	{
		{	/* Inline/variant.sch 60 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_37)))->BgL_accessz00);
		}

	}



/* &local/variant-access */
	obj_t BGl_z62localzf2variantzd2accessz42zzinline_variantz00(obj_t
		BgL_envz00_2349, obj_t BgL_oz00_2350)
	{
		{	/* Inline/variant.sch 60 */
			return
				BGl_localzf2variantzd2accessz20zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2350));
		}

	}



/* local/variant-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2accesszd2setz12ze0zzinline_variantz00
		(BgL_localz00_bglt BgL_oz00_38, obj_t BgL_vz00_39)
	{
		{	/* Inline/variant.sch 61 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_38)))->BgL_accessz00) =
				((obj_t) BgL_vz00_39), BUNSPEC);
		}

	}



/* &local/variant-access-set! */
	obj_t BGl_z62localzf2variantzd2accesszd2setz12z82zzinline_variantz00(obj_t
		BgL_envz00_2351, obj_t BgL_oz00_2352, obj_t BgL_vz00_2353)
	{
		{	/* Inline/variant.sch 61 */
			return
				BGl_localzf2variantzd2accesszd2setz12ze0zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2352), BgL_vz00_2353);
		}

	}



/* local/variant-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_localzf2variantzd2valuez20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_40)
	{
		{	/* Inline/variant.sch 62 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_40)))->BgL_valuez00);
		}

	}



/* &local/variant-value */
	BgL_valuez00_bglt BGl_z62localzf2variantzd2valuez42zzinline_variantz00(obj_t
		BgL_envz00_2354, obj_t BgL_oz00_2355)
	{
		{	/* Inline/variant.sch 62 */
			return
				BGl_localzf2variantzd2valuez20zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2355));
		}

	}



/* local/variant-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2valuezd2setz12ze0zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_41, BgL_valuez00_bglt BgL_vz00_42)
	{
		{	/* Inline/variant.sch 63 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_41)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_42), BUNSPEC);
		}

	}



/* &local/variant-value-set! */
	obj_t BGl_z62localzf2variantzd2valuezd2setz12z82zzinline_variantz00(obj_t
		BgL_envz00_2356, obj_t BgL_oz00_2357, obj_t BgL_vz00_2358)
	{
		{	/* Inline/variant.sch 63 */
			return
				BGl_localzf2variantzd2valuezd2setz12ze0zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2357),
				((BgL_valuez00_bglt) BgL_vz00_2358));
		}

	}



/* local/variant-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_localzf2variantzd2typez20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_43)
	{
		{	/* Inline/variant.sch 64 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_43)))->BgL_typez00);
		}

	}



/* &local/variant-type */
	BgL_typez00_bglt BGl_z62localzf2variantzd2typez42zzinline_variantz00(obj_t
		BgL_envz00_2359, obj_t BgL_oz00_2360)
	{
		{	/* Inline/variant.sch 64 */
			return
				BGl_localzf2variantzd2typez20zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2360));
		}

	}



/* local/variant-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2typezd2setz12ze0zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_44, BgL_typez00_bglt BgL_vz00_45)
	{
		{	/* Inline/variant.sch 65 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_44)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_45), BUNSPEC);
		}

	}



/* &local/variant-type-set! */
	obj_t BGl_z62localzf2variantzd2typezd2setz12z82zzinline_variantz00(obj_t
		BgL_envz00_2361, obj_t BgL_oz00_2362, obj_t BgL_vz00_2363)
	{
		{	/* Inline/variant.sch 65 */
			return
				BGl_localzf2variantzd2typezd2setz12ze0zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2362),
				((BgL_typez00_bglt) BgL_vz00_2363));
		}

	}



/* local/variant-name */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2namez20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_46)
	{
		{	/* Inline/variant.sch 66 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_46)))->BgL_namez00);
		}

	}



/* &local/variant-name */
	obj_t BGl_z62localzf2variantzd2namez42zzinline_variantz00(obj_t
		BgL_envz00_2364, obj_t BgL_oz00_2365)
	{
		{	/* Inline/variant.sch 66 */
			return
				BGl_localzf2variantzd2namez20zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2365));
		}

	}



/* local/variant-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2namezd2setz12ze0zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* Inline/variant.sch 67 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_47)))->BgL_namez00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &local/variant-name-set! */
	obj_t BGl_z62localzf2variantzd2namezd2setz12z82zzinline_variantz00(obj_t
		BgL_envz00_2366, obj_t BgL_oz00_2367, obj_t BgL_vz00_2368)
	{
		{	/* Inline/variant.sch 67 */
			return
				BGl_localzf2variantzd2namezd2setz12ze0zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2367), BgL_vz00_2368);
		}

	}



/* local/variant-id */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2variantzd2idz20zzinline_variantz00(BgL_localz00_bglt
		BgL_oz00_49)
	{
		{	/* Inline/variant.sch 68 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_49)))->BgL_idz00);
		}

	}



/* &local/variant-id */
	obj_t BGl_z62localzf2variantzd2idz42zzinline_variantz00(obj_t BgL_envz00_2369,
		obj_t BgL_oz00_2370)
	{
		{	/* Inline/variant.sch 68 */
			return
				BGl_localzf2variantzd2idz20zzinline_variantz00(
				((BgL_localz00_bglt) BgL_oz00_2370));
		}

	}



/* invariant-args */
	BGL_EXPORTED_DEF obj_t
		BGl_invariantzd2argszd2zzinline_variantz00(BgL_nodez00_bglt BgL_nodez00_52,
		BgL_variablez00_bglt BgL_varz00_53, obj_t BgL_callsz00_54)
	{
		{	/* Inline/variant.scm 36 */
			{	/* Inline/variant.scm 37 */
				BgL_valuez00_bglt BgL_funz00_1416;

				BgL_funz00_1416 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_53))->BgL_valuez00);
				{	/* Inline/variant.scm 37 */
					obj_t BgL_argsz00_1417;

					BgL_argsz00_1417 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1416)))->BgL_argsz00);
					{	/* Inline/variant.scm 38 */
						obj_t BgL_valsz00_1418;

						BgL_valsz00_1418 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_52)))->BgL_argsz00);
						{	/* Inline/variant.scm 39 */

							{
								obj_t BgL_ll1270z00_1420;
								obj_t BgL_ll1271z00_1421;

								BgL_ll1270z00_1420 = BgL_argsz00_1417;
								BgL_ll1271z00_1421 = BgL_valsz00_1418;
							BgL_zc3z04anonymousza31307ze3z87_1422:
								if (NULLP(BgL_ll1270z00_1420))
									{	/* Inline/variant.scm 42 */
										((bool_t) 1);
									}
								else
									{	/* Inline/variant.scm 42 */
										{	/* Inline/variant.scm 43 */
											obj_t BgL_localz00_1424;
											obj_t BgL_valz00_1425;

											BgL_localz00_1424 = CAR(((obj_t) BgL_ll1270z00_1420));
											BgL_valz00_1425 = CAR(((obj_t) BgL_ll1271z00_1421));
											{	/* Inline/variant.scm 43 */
												bool_t BgL_test1886z00_2731;

												if (
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_localz00_1424))))->
															BgL_accessz00) == CNST_TABLE_REF(0)))
													{	/* Inline/variant.scm 44 */
														bool_t BgL__ortest_1131z00_1446;

														{	/* Inline/variant.scm 44 */
															obj_t BgL_classz00_1868;

															BgL_classz00_1868 = BGl_atomz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_valz00_1425))
																{	/* Inline/variant.scm 44 */
																	BgL_objectz00_bglt BgL_arg1807z00_1870;

																	BgL_arg1807z00_1870 =
																		(BgL_objectz00_bglt) (BgL_valz00_1425);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Inline/variant.scm 44 */
																			long BgL_idxz00_1876;

																			BgL_idxz00_1876 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_1870);
																			BgL__ortest_1131z00_1446 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_1876 + 2L)) ==
																				BgL_classz00_1868);
																		}
																	else
																		{	/* Inline/variant.scm 44 */
																			bool_t BgL_res1822z00_1901;

																			{	/* Inline/variant.scm 44 */
																				obj_t BgL_oclassz00_1884;

																				{	/* Inline/variant.scm 44 */
																					obj_t BgL_arg1815z00_1892;
																					long BgL_arg1816z00_1893;

																					BgL_arg1815z00_1892 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Inline/variant.scm 44 */
																						long BgL_arg1817z00_1894;

																						BgL_arg1817z00_1894 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_1870);
																						BgL_arg1816z00_1893 =
																							(BgL_arg1817z00_1894 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_1884 =
																						VECTOR_REF(BgL_arg1815z00_1892,
																						BgL_arg1816z00_1893);
																				}
																				{	/* Inline/variant.scm 44 */
																					bool_t BgL__ortest_1115z00_1885;

																					BgL__ortest_1115z00_1885 =
																						(BgL_classz00_1868 ==
																						BgL_oclassz00_1884);
																					if (BgL__ortest_1115z00_1885)
																						{	/* Inline/variant.scm 44 */
																							BgL_res1822z00_1901 =
																								BgL__ortest_1115z00_1885;
																						}
																					else
																						{	/* Inline/variant.scm 44 */
																							long BgL_odepthz00_1886;

																							{	/* Inline/variant.scm 44 */
																								obj_t BgL_arg1804z00_1887;

																								BgL_arg1804z00_1887 =
																									(BgL_oclassz00_1884);
																								BgL_odepthz00_1886 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_1887);
																							}
																							if ((2L < BgL_odepthz00_1886))
																								{	/* Inline/variant.scm 44 */
																									obj_t BgL_arg1802z00_1889;

																									{	/* Inline/variant.scm 44 */
																										obj_t BgL_arg1803z00_1890;

																										BgL_arg1803z00_1890 =
																											(BgL_oclassz00_1884);
																										BgL_arg1802z00_1889 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_1890, 2L);
																									}
																									BgL_res1822z00_1901 =
																										(BgL_arg1802z00_1889 ==
																										BgL_classz00_1868);
																								}
																							else
																								{	/* Inline/variant.scm 44 */
																									BgL_res1822z00_1901 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL__ortest_1131z00_1446 =
																				BgL_res1822z00_1901;
																		}
																}
															else
																{	/* Inline/variant.scm 44 */
																	BgL__ortest_1131z00_1446 = ((bool_t) 0);
																}
														}
														if (BgL__ortest_1131z00_1446)
															{	/* Inline/variant.scm 44 */
																BgL_test1886z00_2731 = BgL__ortest_1131z00_1446;
															}
														else
															{	/* Inline/variant.scm 45 */
																bool_t BgL__ortest_1132z00_1447;

																{	/* Inline/variant.scm 45 */
																	obj_t BgL_classz00_1902;

																	BgL_classz00_1902 = BGl_kwotez00zzast_nodez00;
																	if (BGL_OBJECTP(BgL_valz00_1425))
																		{	/* Inline/variant.scm 45 */
																			BgL_objectz00_bglt BgL_arg1807z00_1904;

																			BgL_arg1807z00_1904 =
																				(BgL_objectz00_bglt) (BgL_valz00_1425);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Inline/variant.scm 45 */
																					long BgL_idxz00_1910;

																					BgL_idxz00_1910 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_1904);
																					BgL__ortest_1132z00_1447 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_1910 + 2L)) ==
																						BgL_classz00_1902);
																				}
																			else
																				{	/* Inline/variant.scm 45 */
																					bool_t BgL_res1823z00_1935;

																					{	/* Inline/variant.scm 45 */
																						obj_t BgL_oclassz00_1918;

																						{	/* Inline/variant.scm 45 */
																							obj_t BgL_arg1815z00_1926;
																							long BgL_arg1816z00_1927;

																							BgL_arg1815z00_1926 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Inline/variant.scm 45 */
																								long BgL_arg1817z00_1928;

																								BgL_arg1817z00_1928 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_1904);
																								BgL_arg1816z00_1927 =
																									(BgL_arg1817z00_1928 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_1918 =
																								VECTOR_REF(BgL_arg1815z00_1926,
																								BgL_arg1816z00_1927);
																						}
																						{	/* Inline/variant.scm 45 */
																							bool_t BgL__ortest_1115z00_1919;

																							BgL__ortest_1115z00_1919 =
																								(BgL_classz00_1902 ==
																								BgL_oclassz00_1918);
																							if (BgL__ortest_1115z00_1919)
																								{	/* Inline/variant.scm 45 */
																									BgL_res1823z00_1935 =
																										BgL__ortest_1115z00_1919;
																								}
																							else
																								{	/* Inline/variant.scm 45 */
																									long BgL_odepthz00_1920;

																									{	/* Inline/variant.scm 45 */
																										obj_t BgL_arg1804z00_1921;

																										BgL_arg1804z00_1921 =
																											(BgL_oclassz00_1918);
																										BgL_odepthz00_1920 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_1921);
																									}
																									if ((2L < BgL_odepthz00_1920))
																										{	/* Inline/variant.scm 45 */
																											obj_t BgL_arg1802z00_1923;

																											{	/* Inline/variant.scm 45 */
																												obj_t
																													BgL_arg1803z00_1924;
																												BgL_arg1803z00_1924 =
																													(BgL_oclassz00_1918);
																												BgL_arg1802z00_1923 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_1924,
																													2L);
																											}
																											BgL_res1823z00_1935 =
																												(BgL_arg1802z00_1923 ==
																												BgL_classz00_1902);
																										}
																									else
																										{	/* Inline/variant.scm 45 */
																											BgL_res1823z00_1935 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL__ortest_1132z00_1447 =
																						BgL_res1823z00_1935;
																				}
																		}
																	else
																		{	/* Inline/variant.scm 45 */
																			BgL__ortest_1132z00_1447 = ((bool_t) 0);
																		}
																}
																if (BgL__ortest_1132z00_1447)
																	{	/* Inline/variant.scm 45 */
																		BgL_test1886z00_2731 =
																			BgL__ortest_1132z00_1447;
																	}
																else
																	{	/* Inline/variant.scm 46 */
																		bool_t BgL_test1898z00_2784;

																		{	/* Inline/variant.scm 46 */
																			obj_t BgL_classz00_1936;

																			BgL_classz00_1936 =
																				BGl_varz00zzast_nodez00;
																			if (BGL_OBJECTP(BgL_valz00_1425))
																				{	/* Inline/variant.scm 46 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_1938;
																					BgL_arg1807z00_1938 =
																						(BgL_objectz00_bglt)
																						(BgL_valz00_1425);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Inline/variant.scm 46 */
																							long BgL_idxz00_1944;

																							BgL_idxz00_1944 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_1938);
																							BgL_test1898z00_2784 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_1944 + 2L)) ==
																								BgL_classz00_1936);
																						}
																					else
																						{	/* Inline/variant.scm 46 */
																							bool_t BgL_res1824z00_1969;

																							{	/* Inline/variant.scm 46 */
																								obj_t BgL_oclassz00_1952;

																								{	/* Inline/variant.scm 46 */
																									obj_t BgL_arg1815z00_1960;
																									long BgL_arg1816z00_1961;

																									BgL_arg1815z00_1960 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Inline/variant.scm 46 */
																										long BgL_arg1817z00_1962;

																										BgL_arg1817z00_1962 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_1938);
																										BgL_arg1816z00_1961 =
																											(BgL_arg1817z00_1962 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_1952 =
																										VECTOR_REF
																										(BgL_arg1815z00_1960,
																										BgL_arg1816z00_1961);
																								}
																								{	/* Inline/variant.scm 46 */
																									bool_t
																										BgL__ortest_1115z00_1953;
																									BgL__ortest_1115z00_1953 =
																										(BgL_classz00_1936 ==
																										BgL_oclassz00_1952);
																									if (BgL__ortest_1115z00_1953)
																										{	/* Inline/variant.scm 46 */
																											BgL_res1824z00_1969 =
																												BgL__ortest_1115z00_1953;
																										}
																									else
																										{	/* Inline/variant.scm 46 */
																											long BgL_odepthz00_1954;

																											{	/* Inline/variant.scm 46 */
																												obj_t
																													BgL_arg1804z00_1955;
																												BgL_arg1804z00_1955 =
																													(BgL_oclassz00_1952);
																												BgL_odepthz00_1954 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_1955);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_1954))
																												{	/* Inline/variant.scm 46 */
																													obj_t
																														BgL_arg1802z00_1957;
																													{	/* Inline/variant.scm 46 */
																														obj_t
																															BgL_arg1803z00_1958;
																														BgL_arg1803z00_1958
																															=
																															(BgL_oclassz00_1952);
																														BgL_arg1802z00_1957
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_1958,
																															2L);
																													}
																													BgL_res1824z00_1969 =
																														(BgL_arg1802z00_1957
																														==
																														BgL_classz00_1936);
																												}
																											else
																												{	/* Inline/variant.scm 46 */
																													BgL_res1824z00_1969 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test1898z00_2784 =
																								BgL_res1824z00_1969;
																						}
																				}
																			else
																				{	/* Inline/variant.scm 46 */
																					BgL_test1898z00_2784 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test1898z00_2784)
																			{	/* Inline/variant.scm 46 */
																				BgL_test1886z00_2731 =
																					(
																					(((BgL_variablez00_bglt) COBJECT(
																								(((BgL_varz00_bglt) COBJECT(
																											((BgL_varz00_bglt)
																												BgL_valz00_1425)))->
																									BgL_variablez00)))->
																						BgL_accessz00) ==
																					CNST_TABLE_REF(0));
																			}
																		else
																			{	/* Inline/variant.scm 46 */
																				BgL_test1886z00_2731 = ((bool_t) 0);
																			}
																	}
															}
													}
												else
													{	/* Inline/variant.scm 43 */
														BgL_test1886z00_2731 = ((bool_t) 0);
													}
												if (BgL_test1886z00_2731)
													{	/* Inline/variant.scm 43 */
														{	/* Inline/variant.scm 49 */
															BgL_localzf2variantzf2_bglt BgL_wide1136z00_1437;

															BgL_wide1136z00_1437 =
																((BgL_localzf2variantzf2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_localzf2variantzf2_bgl))));
															{	/* Inline/variant.scm 49 */
																obj_t BgL_auxz00_2817;
																BgL_objectz00_bglt BgL_tmpz00_2813;

																BgL_auxz00_2817 =
																	((obj_t) BgL_wide1136z00_1437);
																BgL_tmpz00_2813 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt)
																		((BgL_localz00_bglt) BgL_localz00_1424)));
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2813,
																	BgL_auxz00_2817);
															}
															((BgL_objectz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_1424)));
															{	/* Inline/variant.scm 49 */
																long BgL_arg1317z00_1438;

																BgL_arg1317z00_1438 =
																	BGL_CLASS_NUM
																	(BGl_localzf2variantzf2zzinline_variantz00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																			(BgL_localz00_bglt) ((BgL_localz00_bglt)
																				BgL_localz00_1424))),
																	BgL_arg1317z00_1438);
															}
															((BgL_localz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_1424)));
														}
														{
															BgL_localzf2variantzf2_bglt BgL_auxz00_2831;

															{
																obj_t BgL_auxz00_2832;

																{	/* Inline/variant.scm 49 */
																	BgL_objectz00_bglt BgL_tmpz00_2833;

																	BgL_tmpz00_2833 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt)
																			((BgL_localz00_bglt) BgL_localz00_1424)));
																	BgL_auxz00_2832 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2833);
																}
																BgL_auxz00_2831 =
																	((BgL_localzf2variantzf2_bglt)
																	BgL_auxz00_2832);
															}
															((((BgL_localzf2variantzf2_bglt)
																		COBJECT(BgL_auxz00_2831))->BgL_variantz00) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
														}
														((BgL_localz00_bglt)
															((BgL_localz00_bglt) BgL_localz00_1424));
													}
												else
													{	/* Inline/variant.scm 43 */
														{	/* Inline/variant.scm 50 */
															BgL_localzf2variantzf2_bglt BgL_wide1140z00_1442;

															BgL_wide1140z00_1442 =
																((BgL_localzf2variantzf2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_localzf2variantzf2_bgl))));
															{	/* Inline/variant.scm 50 */
																obj_t BgL_auxz00_2847;
																BgL_objectz00_bglt BgL_tmpz00_2843;

																BgL_auxz00_2847 =
																	((obj_t) BgL_wide1140z00_1442);
																BgL_tmpz00_2843 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt)
																		((BgL_localz00_bglt) BgL_localz00_1424)));
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2843,
																	BgL_auxz00_2847);
															}
															((BgL_objectz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_1424)));
															{	/* Inline/variant.scm 50 */
																long BgL_arg1318z00_1443;

																BgL_arg1318z00_1443 =
																	BGL_CLASS_NUM
																	(BGl_localzf2variantzf2zzinline_variantz00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																			(BgL_localz00_bglt) ((BgL_localz00_bglt)
																				BgL_localz00_1424))),
																	BgL_arg1318z00_1443);
															}
															((BgL_localz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_1424)));
														}
														{
															BgL_localzf2variantzf2_bglt BgL_auxz00_2861;

															{
																obj_t BgL_auxz00_2862;

																{	/* Inline/variant.scm 50 */
																	BgL_objectz00_bglt BgL_tmpz00_2863;

																	BgL_tmpz00_2863 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt)
																			((BgL_localz00_bglt) BgL_localz00_1424)));
																	BgL_auxz00_2862 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2863);
																}
																BgL_auxz00_2861 =
																	((BgL_localzf2variantzf2_bglt)
																	BgL_auxz00_2862);
															}
															((((BgL_localzf2variantzf2_bglt)
																		COBJECT(BgL_auxz00_2861))->BgL_variantz00) =
																((bool_t) ((bool_t) 1)), BUNSPEC);
														}
														((BgL_localz00_bglt)
															((BgL_localz00_bglt) BgL_localz00_1424));
										}}}
										{	/* Inline/variant.scm 42 */
											obj_t BgL_arg1322z00_1452;
											obj_t BgL_arg1323z00_1453;

											BgL_arg1322z00_1452 = CDR(((obj_t) BgL_ll1270z00_1420));
											BgL_arg1323z00_1453 = CDR(((obj_t) BgL_ll1271z00_1421));
											{
												obj_t BgL_ll1271z00_2877;
												obj_t BgL_ll1270z00_2876;

												BgL_ll1270z00_2876 = BgL_arg1322z00_1452;
												BgL_ll1271z00_2877 = BgL_arg1323z00_1453;
												BgL_ll1271z00_1421 = BgL_ll1271z00_2877;
												BgL_ll1270z00_1420 = BgL_ll1270z00_2876;
												goto BgL_zc3z04anonymousza31307ze3z87_1422;
											}
										}
									}
							}
							{
								obj_t BgL_l1275z00_1456;

								BgL_l1275z00_1456 = BgL_callsz00_54;
							BgL_zc3z04anonymousza31324ze3z87_1457:
								if (PAIRP(BgL_l1275z00_1456))
									{	/* Inline/variant.scm 53 */
										{	/* Inline/variant.scm 54 */
											BgL_appz00_bglt BgL_appz00_1459;

											BgL_appz00_1459 =
												((BgL_appz00_bglt) CAR(BgL_l1275z00_1456));
											{	/* Inline/variant.scm 54 */
												obj_t BgL_g1142z00_1460;

												BgL_g1142z00_1460 =
													(((BgL_appz00_bglt) COBJECT(BgL_appz00_1459))->
													BgL_argsz00);
												{
													obj_t BgL_actualsz00_1462;
													obj_t BgL_argsz00_1463;

													BgL_actualsz00_1462 = BgL_g1142z00_1460;
													BgL_argsz00_1463 = BgL_argsz00_1417;
												BgL_zc3z04anonymousza31326ze3z87_1464:
													if (NULLP(BgL_argsz00_1463))
														{	/* Inline/variant.scm 57 */
															CNST_TABLE_REF(1);
														}
													else
														{	/* Inline/variant.scm 57 */
															if (NULLP(BgL_actualsz00_1462))
																{
																	obj_t BgL_l1273z00_1468;

																	{	/* Inline/variant.scm 60 */
																		bool_t BgL_tmpz00_2888;

																		BgL_l1273z00_1468 = BgL_argsz00_1463;
																	BgL_zc3z04anonymousza31329ze3z87_1469:
																		if (PAIRP(BgL_l1273z00_1468))
																			{	/* Inline/variant.scm 60 */
																				{	/* Inline/variant.scm 61 */
																					obj_t BgL_localz00_1471;

																					BgL_localz00_1471 =
																						CAR(BgL_l1273z00_1468);
																					{
																						BgL_localzf2variantzf2_bglt
																							BgL_auxz00_2892;
																						{
																							obj_t BgL_auxz00_2893;

																							{	/* Inline/variant.scm 61 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_2894;
																								BgL_tmpz00_2894 =
																									((BgL_objectz00_bglt) (
																										(BgL_localz00_bglt)
																										BgL_localz00_1471));
																								BgL_auxz00_2893 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_2894);
																							}
																							BgL_auxz00_2892 =
																								((BgL_localzf2variantzf2_bglt)
																								BgL_auxz00_2893);
																						}
																						((((BgL_localzf2variantzf2_bglt)
																									COBJECT(BgL_auxz00_2892))->
																								BgL_variantz00) =
																							((bool_t) ((bool_t) 1)), BUNSPEC);
																					}
																				}
																				{
																					obj_t BgL_l1273z00_2900;

																					BgL_l1273z00_2900 =
																						CDR(BgL_l1273z00_1468);
																					BgL_l1273z00_1468 = BgL_l1273z00_2900;
																					goto
																						BgL_zc3z04anonymousza31329ze3z87_1469;
																				}
																			}
																		else
																			{	/* Inline/variant.scm 60 */
																				BgL_tmpz00_2888 = ((bool_t) 1);
																			}
																		BBOOL(BgL_tmpz00_2888);
																	}
																}
															else
																{	/* Inline/variant.scm 63 */
																	bool_t BgL_test1907z00_2903;

																	{	/* Inline/variant.scm 63 */
																		bool_t BgL_test1908z00_2904;

																		{	/* Inline/variant.scm 63 */
																			obj_t BgL_arg1370z00_1490;

																			BgL_arg1370z00_1490 =
																				CAR(((obj_t) BgL_actualsz00_1462));
																			{	/* Inline/variant.scm 63 */
																				obj_t BgL_classz00_1992;

																				BgL_classz00_1992 =
																					BGl_varz00zzast_nodez00;
																				if (BGL_OBJECTP(BgL_arg1370z00_1490))
																					{	/* Inline/variant.scm 63 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_1994;
																						BgL_arg1807z00_1994 =
																							(BgL_objectz00_bglt)
																							(BgL_arg1370z00_1490);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Inline/variant.scm 63 */
																								long BgL_idxz00_2000;

																								BgL_idxz00_2000 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_1994);
																								BgL_test1908z00_2904 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2000 + 2L)) ==
																									BgL_classz00_1992);
																							}
																						else
																							{	/* Inline/variant.scm 63 */
																								bool_t BgL_res1825z00_2025;

																								{	/* Inline/variant.scm 63 */
																									obj_t BgL_oclassz00_2008;

																									{	/* Inline/variant.scm 63 */
																										obj_t BgL_arg1815z00_2016;
																										long BgL_arg1816z00_2017;

																										BgL_arg1815z00_2016 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Inline/variant.scm 63 */
																											long BgL_arg1817z00_2018;

																											BgL_arg1817z00_2018 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_1994);
																											BgL_arg1816z00_2017 =
																												(BgL_arg1817z00_2018 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2008 =
																											VECTOR_REF
																											(BgL_arg1815z00_2016,
																											BgL_arg1816z00_2017);
																									}
																									{	/* Inline/variant.scm 63 */
																										bool_t
																											BgL__ortest_1115z00_2009;
																										BgL__ortest_1115z00_2009 =
																											(BgL_classz00_1992 ==
																											BgL_oclassz00_2008);
																										if (BgL__ortest_1115z00_2009)
																											{	/* Inline/variant.scm 63 */
																												BgL_res1825z00_2025 =
																													BgL__ortest_1115z00_2009;
																											}
																										else
																											{	/* Inline/variant.scm 63 */
																												long BgL_odepthz00_2010;

																												{	/* Inline/variant.scm 63 */
																													obj_t
																														BgL_arg1804z00_2011;
																													BgL_arg1804z00_2011 =
																														(BgL_oclassz00_2008);
																													BgL_odepthz00_2010 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2011);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_2010))
																													{	/* Inline/variant.scm 63 */
																														obj_t
																															BgL_arg1802z00_2013;
																														{	/* Inline/variant.scm 63 */
																															obj_t
																																BgL_arg1803z00_2014;
																															BgL_arg1803z00_2014
																																=
																																(BgL_oclassz00_2008);
																															BgL_arg1802z00_2013
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2014,
																																2L);
																														}
																														BgL_res1825z00_2025
																															=
																															(BgL_arg1802z00_2013
																															==
																															BgL_classz00_1992);
																													}
																												else
																													{	/* Inline/variant.scm 63 */
																														BgL_res1825z00_2025
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test1908z00_2904 =
																									BgL_res1825z00_2025;
																							}
																					}
																				else
																					{	/* Inline/variant.scm 63 */
																						BgL_test1908z00_2904 = ((bool_t) 0);
																					}
																			}
																		}
																		if (BgL_test1908z00_2904)
																			{	/* Inline/variant.scm 63 */
																				BgL_test1907z00_2903 =
																					(
																					((obj_t)
																						(((BgL_varz00_bglt) COBJECT(
																									((BgL_varz00_bglt)
																										CAR(
																											((obj_t)
																												BgL_actualsz00_1462)))))->
																							BgL_variablez00)) ==
																					CAR(((obj_t) BgL_argsz00_1463)));
																			}
																		else
																			{	/* Inline/variant.scm 63 */
																				BgL_test1907z00_2903 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test1907z00_2903)
																		{	/* Inline/variant.scm 65 */
																			obj_t BgL_arg1346z00_1481;
																			obj_t BgL_arg1348z00_1482;

																			BgL_arg1346z00_1481 =
																				CDR(((obj_t) BgL_actualsz00_1462));
																			BgL_arg1348z00_1482 =
																				CDR(((obj_t) BgL_argsz00_1463));
																			{
																				obj_t BgL_argsz00_2942;
																				obj_t BgL_actualsz00_2941;

																				BgL_actualsz00_2941 =
																					BgL_arg1346z00_1481;
																				BgL_argsz00_2942 = BgL_arg1348z00_1482;
																				BgL_argsz00_1463 = BgL_argsz00_2942;
																				BgL_actualsz00_1462 =
																					BgL_actualsz00_2941;
																				goto
																					BgL_zc3z04anonymousza31326ze3z87_1464;
																			}
																		}
																	else
																		{	/* Inline/variant.scm 63 */
																			{	/* Inline/variant.scm 67 */
																				obj_t BgL_arg1349z00_1483;

																				BgL_arg1349z00_1483 =
																					CAR(((obj_t) BgL_argsz00_1463));
																				{
																					BgL_localzf2variantzf2_bglt
																						BgL_auxz00_2945;
																					{
																						obj_t BgL_auxz00_2946;

																						{	/* Inline/variant.scm 67 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_2947;
																							BgL_tmpz00_2947 =
																								((BgL_objectz00_bglt) (
																									(BgL_localz00_bglt)
																									BgL_arg1349z00_1483));
																							BgL_auxz00_2946 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_2947);
																						}
																						BgL_auxz00_2945 =
																							((BgL_localzf2variantzf2_bglt)
																							BgL_auxz00_2946);
																					}
																					((((BgL_localzf2variantzf2_bglt)
																								COBJECT(BgL_auxz00_2945))->
																							BgL_variantz00) =
																						((bool_t) ((bool_t) 1)), BUNSPEC);
																				}
																			}
																			{	/* Inline/variant.scm 68 */
																				obj_t BgL_arg1351z00_1484;
																				obj_t BgL_arg1352z00_1485;

																				BgL_arg1351z00_1484 =
																					CDR(((obj_t) BgL_actualsz00_1462));
																				BgL_arg1352z00_1485 =
																					CDR(((obj_t) BgL_argsz00_1463));
																				{
																					obj_t BgL_argsz00_2958;
																					obj_t BgL_actualsz00_2957;

																					BgL_actualsz00_2957 =
																						BgL_arg1351z00_1484;
																					BgL_argsz00_2958 =
																						BgL_arg1352z00_1485;
																					BgL_argsz00_1463 = BgL_argsz00_2958;
																					BgL_actualsz00_1462 =
																						BgL_actualsz00_2957;
																					goto
																						BgL_zc3z04anonymousza31326ze3z87_1464;
																				}
																			}
																		}
																}
														}
												}
											}
										}
										{
											obj_t BgL_l1275z00_2959;

											BgL_l1275z00_2959 = CDR(BgL_l1275z00_1456);
											BgL_l1275z00_1456 = BgL_l1275z00_2959;
											goto BgL_zc3z04anonymousza31324ze3z87_1457;
										}
									}
								else
									{	/* Inline/variant.scm 53 */
										((bool_t) 1);
									}
							}
							{
								obj_t BgL_argsz00_1496;
								obj_t BgL_invariantz00_1497;

								BgL_argsz00_1496 = BgL_argsz00_1417;
								BgL_invariantz00_1497 = BNIL;
							BgL_zc3z04anonymousza31372ze3z87_1498:
								if (NULLP(BgL_argsz00_1496))
									{	/* Inline/variant.scm 73 */
										return bgl_reverse_bang(BgL_invariantz00_1497);
									}
								else
									{	/* Inline/variant.scm 75 */
										obj_t BgL_arg1375z00_1500;
										obj_t BgL_arg1376z00_1501;

										BgL_arg1375z00_1500 = CDR(((obj_t) BgL_argsz00_1496));
										{	/* Inline/variant.scm 76 */
											bool_t BgL_test1914z00_2966;

											{	/* Inline/variant.scm 76 */
												BgL_localz00_bglt BgL_oz00_2040;

												BgL_oz00_2040 =
													((BgL_localz00_bglt) CAR(((obj_t) BgL_argsz00_1496)));
												{
													BgL_localzf2variantzf2_bglt BgL_auxz00_2970;

													{
														obj_t BgL_auxz00_2971;

														{	/* Inline/variant.scm 76 */
															BgL_objectz00_bglt BgL_tmpz00_2972;

															BgL_tmpz00_2972 =
																((BgL_objectz00_bglt) BgL_oz00_2040);
															BgL_auxz00_2971 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2972);
														}
														BgL_auxz00_2970 =
															((BgL_localzf2variantzf2_bglt) BgL_auxz00_2971);
													}
													BgL_test1914z00_2966 =
														(((BgL_localzf2variantzf2_bglt)
															COBJECT(BgL_auxz00_2970))->BgL_variantz00);
												}
											}
											if (BgL_test1914z00_2966)
												{	/* Inline/variant.scm 76 */
													BgL_arg1376z00_1501 = BgL_invariantz00_1497;
												}
											else
												{	/* Inline/variant.scm 78 */
													obj_t BgL_arg1408z00_1504;

													BgL_arg1408z00_1504 = CAR(((obj_t) BgL_argsz00_1496));
													BgL_arg1376z00_1501 =
														MAKE_YOUNG_PAIR(BgL_arg1408z00_1504,
														BgL_invariantz00_1497);
												}
										}
										{
											obj_t BgL_invariantz00_2981;
											obj_t BgL_argsz00_2980;

											BgL_argsz00_2980 = BgL_arg1375z00_1500;
											BgL_invariantz00_2981 = BgL_arg1376z00_1501;
											BgL_invariantz00_1497 = BgL_invariantz00_2981;
											BgL_argsz00_1496 = BgL_argsz00_2980;
											goto BgL_zc3z04anonymousza31372ze3z87_1498;
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* &invariant-args */
	obj_t BGl_z62invariantzd2argszb0zzinline_variantz00(obj_t BgL_envz00_2371,
		obj_t BgL_nodez00_2372, obj_t BgL_varz00_2373, obj_t BgL_callsz00_2374)
	{
		{	/* Inline/variant.scm 36 */
			return
				BGl_invariantzd2argszd2zzinline_variantz00(
				((BgL_nodez00_bglt) BgL_nodez00_2372),
				((BgL_variablez00_bglt) BgL_varz00_2373), BgL_callsz00_2374);
		}

	}



/* variant-args */
	BGL_EXPORTED_DEF obj_t
		BGl_variantzd2argszd2zzinline_variantz00(BgL_variablez00_bglt BgL_varz00_55)
	{
		{	/* Inline/variant.scm 85 */
			{	/* Inline/variant.scm 86 */
				BgL_valuez00_bglt BgL_funz00_1507;

				BgL_funz00_1507 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_55))->BgL_valuez00);
				{	/* Inline/variant.scm 86 */
					obj_t BgL_argsz00_1508;

					BgL_argsz00_1508 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1507)))->BgL_argsz00);
					{	/* Inline/variant.scm 87 */

						{
							obj_t BgL_argsz00_1511;
							obj_t BgL_variantz00_1512;

							BgL_argsz00_1511 = BgL_argsz00_1508;
							BgL_variantz00_1512 = BNIL;
						BgL_zc3z04anonymousza31411ze3z87_1513:
							if (NULLP(BgL_argsz00_1511))
								{	/* Inline/variant.scm 90 */
									return bgl_reverse_bang(BgL_variantz00_1512);
								}
							else
								{	/* Inline/variant.scm 92 */
									obj_t BgL_arg1421z00_1515;
									obj_t BgL_arg1422z00_1516;

									BgL_arg1421z00_1515 = CDR(((obj_t) BgL_argsz00_1511));
									{	/* Inline/variant.scm 93 */
										bool_t BgL_test1916z00_2993;

										{	/* Inline/variant.scm 93 */
											BgL_localz00_bglt BgL_oz00_2047;

											BgL_oz00_2047 =
												((BgL_localz00_bglt) CAR(((obj_t) BgL_argsz00_1511)));
											{
												BgL_localzf2variantzf2_bglt BgL_auxz00_2997;

												{
													obj_t BgL_auxz00_2998;

													{	/* Inline/variant.scm 93 */
														BgL_objectz00_bglt BgL_tmpz00_2999;

														BgL_tmpz00_2999 =
															((BgL_objectz00_bglt) BgL_oz00_2047);
														BgL_auxz00_2998 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2999);
													}
													BgL_auxz00_2997 =
														((BgL_localzf2variantzf2_bglt) BgL_auxz00_2998);
												}
												BgL_test1916z00_2993 =
													(((BgL_localzf2variantzf2_bglt)
														COBJECT(BgL_auxz00_2997))->BgL_variantz00);
											}
										}
										if (BgL_test1916z00_2993)
											{	/* Inline/variant.scm 94 */
												obj_t BgL_arg1437z00_1519;

												BgL_arg1437z00_1519 = CAR(((obj_t) BgL_argsz00_1511));
												BgL_arg1422z00_1516 =
													MAKE_YOUNG_PAIR(BgL_arg1437z00_1519,
													BgL_variantz00_1512);
											}
										else
											{	/* Inline/variant.scm 93 */
												BgL_arg1422z00_1516 = BgL_variantz00_1512;
											}
									}
									{
										obj_t BgL_variantz00_3008;
										obj_t BgL_argsz00_3007;

										BgL_argsz00_3007 = BgL_arg1421z00_1515;
										BgL_variantz00_3008 = BgL_arg1422z00_1516;
										BgL_variantz00_1512 = BgL_variantz00_3008;
										BgL_argsz00_1511 = BgL_argsz00_3007;
										goto BgL_zc3z04anonymousza31411ze3z87_1513;
									}
								}
						}
					}
				}
			}
		}

	}



/* &variant-args */
	obj_t BGl_z62variantzd2argszb0zzinline_variantz00(obj_t BgL_envz00_2375,
		obj_t BgL_varz00_2376)
	{
		{	/* Inline/variant.scm 85 */
			return
				BGl_variantzd2argszd2zzinline_variantz00(
				((BgL_variablez00_bglt) BgL_varz00_2376));
		}

	}



/* substitutions */
	BGL_EXPORTED_DEF obj_t
		BGl_substitutionsz00zzinline_variantz00(BgL_variablez00_bglt BgL_varz00_56,
		obj_t BgL_actualsz00_57, obj_t BgL_varzd2argszd2_58)
	{
		{	/* Inline/variant.scm 100 */
			{	/* Inline/variant.scm 101 */
				BgL_valuez00_bglt BgL_funz00_1522;

				BgL_funz00_1522 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_56))->BgL_valuez00);
				{	/* Inline/variant.scm 101 */
					obj_t BgL_allzd2argszd2_1523;

					BgL_allzd2argszd2_1523 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1522)))->BgL_argsz00);
					{	/* Inline/variant.scm 102 */

						{
							obj_t BgL_actualsz00_1526;
							obj_t BgL_allzd2argszd2_1527;
							obj_t BgL_varzd2argszd2_1528;
							obj_t BgL_substitutionsz00_1529;

							BgL_actualsz00_1526 = BgL_actualsz00_57;
							BgL_allzd2argszd2_1527 = BgL_allzd2argszd2_1523;
							BgL_varzd2argszd2_1528 = BgL_varzd2argszd2_58;
							BgL_substitutionsz00_1529 = BNIL;
						BgL_zc3z04anonymousza31449ze3z87_1530:
							if (NULLP(BgL_allzd2argszd2_1527))
								{	/* Inline/variant.scm 108 */
									return bgl_reverse_bang(BgL_substitutionsz00_1529);
								}
							else
								{	/* Inline/variant.scm 110 */
									bool_t BgL_test1918z00_3017;

									{	/* Inline/variant.scm 110 */
										BgL_localz00_bglt BgL_oz00_2053;

										BgL_oz00_2053 =
											((BgL_localz00_bglt)
											CAR(((obj_t) BgL_allzd2argszd2_1527)));
										{
											BgL_localzf2variantzf2_bglt BgL_auxz00_3021;

											{
												obj_t BgL_auxz00_3022;

												{	/* Inline/variant.scm 110 */
													BgL_objectz00_bglt BgL_tmpz00_3023;

													BgL_tmpz00_3023 =
														((BgL_objectz00_bglt) BgL_oz00_2053);
													BgL_auxz00_3022 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3023);
												}
												BgL_auxz00_3021 =
													((BgL_localzf2variantzf2_bglt) BgL_auxz00_3022);
											}
											BgL_test1918z00_3017 =
												(((BgL_localzf2variantzf2_bglt)
													COBJECT(BgL_auxz00_3021))->BgL_variantz00);
										}
									}
									if (BgL_test1918z00_3017)
										{	/* Inline/variant.scm 111 */
											obj_t BgL_arg1472z00_1534;
											obj_t BgL_arg1473z00_1535;
											obj_t BgL_arg1485z00_1536;
											obj_t BgL_arg1489z00_1537;

											BgL_arg1472z00_1534 = CDR(((obj_t) BgL_actualsz00_1526));
											BgL_arg1473z00_1535 =
												CDR(((obj_t) BgL_allzd2argszd2_1527));
											BgL_arg1485z00_1536 =
												CDR(((obj_t) BgL_varzd2argszd2_1528));
											{	/* Inline/variant.scm 114 */
												obj_t BgL_arg1502z00_1538;

												BgL_arg1502z00_1538 =
													CAR(((obj_t) BgL_varzd2argszd2_1528));
												BgL_arg1489z00_1537 =
													MAKE_YOUNG_PAIR(BgL_arg1502z00_1538,
													BgL_substitutionsz00_1529);
											}
											{
												obj_t BgL_substitutionsz00_3040;
												obj_t BgL_varzd2argszd2_3039;
												obj_t BgL_allzd2argszd2_3038;
												obj_t BgL_actualsz00_3037;

												BgL_actualsz00_3037 = BgL_arg1472z00_1534;
												BgL_allzd2argszd2_3038 = BgL_arg1473z00_1535;
												BgL_varzd2argszd2_3039 = BgL_arg1485z00_1536;
												BgL_substitutionsz00_3040 = BgL_arg1489z00_1537;
												BgL_substitutionsz00_1529 = BgL_substitutionsz00_3040;
												BgL_varzd2argszd2_1528 = BgL_varzd2argszd2_3039;
												BgL_allzd2argszd2_1527 = BgL_allzd2argszd2_3038;
												BgL_actualsz00_1526 = BgL_actualsz00_3037;
												goto BgL_zc3z04anonymousza31449ze3z87_1530;
											}
										}
									else
										{	/* Inline/variant.scm 116 */
											obj_t BgL_arg1509z00_1539;
											obj_t BgL_arg1513z00_1540;
											obj_t BgL_arg1514z00_1541;

											BgL_arg1509z00_1539 = CDR(((obj_t) BgL_actualsz00_1526));
											BgL_arg1513z00_1540 =
												CDR(((obj_t) BgL_allzd2argszd2_1527));
											{	/* Inline/variant.scm 119 */
												obj_t BgL_arg1516z00_1542;

												{	/* Inline/variant.scm 119 */
													bool_t BgL_test1919z00_3045;

													{	/* Inline/variant.scm 119 */
														obj_t BgL_arg1544z00_1546;

														BgL_arg1544z00_1546 =
															CAR(((obj_t) BgL_actualsz00_1526));
														{	/* Inline/variant.scm 119 */
															obj_t BgL_classz00_2062;

															BgL_classz00_2062 = BGl_varz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_arg1544z00_1546))
																{	/* Inline/variant.scm 119 */
																	BgL_objectz00_bglt BgL_arg1807z00_2064;

																	BgL_arg1807z00_2064 =
																		(BgL_objectz00_bglt) (BgL_arg1544z00_1546);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Inline/variant.scm 119 */
																			long BgL_idxz00_2070;

																			BgL_idxz00_2070 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2064);
																			BgL_test1919z00_3045 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2070 + 2L)) ==
																				BgL_classz00_2062);
																		}
																	else
																		{	/* Inline/variant.scm 119 */
																			bool_t BgL_res1826z00_2095;

																			{	/* Inline/variant.scm 119 */
																				obj_t BgL_oclassz00_2078;

																				{	/* Inline/variant.scm 119 */
																					obj_t BgL_arg1815z00_2086;
																					long BgL_arg1816z00_2087;

																					BgL_arg1815z00_2086 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Inline/variant.scm 119 */
																						long BgL_arg1817z00_2088;

																						BgL_arg1817z00_2088 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2064);
																						BgL_arg1816z00_2087 =
																							(BgL_arg1817z00_2088 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2078 =
																						VECTOR_REF(BgL_arg1815z00_2086,
																						BgL_arg1816z00_2087);
																				}
																				{	/* Inline/variant.scm 119 */
																					bool_t BgL__ortest_1115z00_2079;

																					BgL__ortest_1115z00_2079 =
																						(BgL_classz00_2062 ==
																						BgL_oclassz00_2078);
																					if (BgL__ortest_1115z00_2079)
																						{	/* Inline/variant.scm 119 */
																							BgL_res1826z00_2095 =
																								BgL__ortest_1115z00_2079;
																						}
																					else
																						{	/* Inline/variant.scm 119 */
																							long BgL_odepthz00_2080;

																							{	/* Inline/variant.scm 119 */
																								obj_t BgL_arg1804z00_2081;

																								BgL_arg1804z00_2081 =
																									(BgL_oclassz00_2078);
																								BgL_odepthz00_2080 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2081);
																							}
																							if ((2L < BgL_odepthz00_2080))
																								{	/* Inline/variant.scm 119 */
																									obj_t BgL_arg1802z00_2083;

																									{	/* Inline/variant.scm 119 */
																										obj_t BgL_arg1803z00_2084;

																										BgL_arg1803z00_2084 =
																											(BgL_oclassz00_2078);
																										BgL_arg1802z00_2083 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2084, 2L);
																									}
																									BgL_res1826z00_2095 =
																										(BgL_arg1802z00_2083 ==
																										BgL_classz00_2062);
																								}
																							else
																								{	/* Inline/variant.scm 119 */
																									BgL_res1826z00_2095 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1919z00_3045 =
																				BgL_res1826z00_2095;
																		}
																}
															else
																{	/* Inline/variant.scm 119 */
																	BgL_test1919z00_3045 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test1919z00_3045)
														{	/* Inline/variant.scm 119 */
															BgL_arg1516z00_1542 =
																((obj_t)
																(((BgL_varz00_bglt) COBJECT(
																			((BgL_varz00_bglt)
																				CAR(
																					((obj_t) BgL_actualsz00_1526)))))->
																	BgL_variablez00));
														}
													else
														{	/* Inline/variant.scm 119 */
															BgL_arg1516z00_1542 =
																CAR(((obj_t) BgL_actualsz00_1526));
														}
												}
												BgL_arg1514z00_1541 =
													MAKE_YOUNG_PAIR(BgL_arg1516z00_1542,
													BgL_substitutionsz00_1529);
											}
											{
												obj_t BgL_substitutionsz00_3080;
												obj_t BgL_allzd2argszd2_3079;
												obj_t BgL_actualsz00_3078;

												BgL_actualsz00_3078 = BgL_arg1509z00_1539;
												BgL_allzd2argszd2_3079 = BgL_arg1513z00_1540;
												BgL_substitutionsz00_3080 = BgL_arg1514z00_1541;
												BgL_substitutionsz00_1529 = BgL_substitutionsz00_3080;
												BgL_allzd2argszd2_1527 = BgL_allzd2argszd2_3079;
												BgL_actualsz00_1526 = BgL_actualsz00_3078;
												goto BgL_zc3z04anonymousza31449ze3z87_1530;
											}
										}
								}
						}
					}
				}
			}
		}

	}



/* &substitutions */
	obj_t BGl_z62substitutionsz62zzinline_variantz00(obj_t BgL_envz00_2377,
		obj_t BgL_varz00_2378, obj_t BgL_actualsz00_2379,
		obj_t BgL_varzd2argszd2_2380)
	{
		{	/* Inline/variant.scm 100 */
			return
				BGl_substitutionsz00zzinline_variantz00(
				((BgL_variablez00_bglt) BgL_varz00_2378), BgL_actualsz00_2379,
				BgL_varzd2argszd2_2380);
		}

	}



/* shrink-args! */
	BGL_EXPORTED_DEF obj_t
		BGl_shrinkzd2argsz12zc0zzinline_variantz00(BgL_variablez00_bglt
		BgL_varz00_59)
	{
		{	/* Inline/variant.scm 127 */
			{	/* Inline/variant.scm 128 */
				BgL_valuez00_bglt BgL_funz00_1549;

				BgL_funz00_1549 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_59))->BgL_valuez00);
				{	/* Inline/variant.scm 128 */
					obj_t BgL_argsz00_1550;

					BgL_argsz00_1550 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1549)))->BgL_argsz00);
					{	/* Inline/variant.scm 129 */

						{
							obj_t BgL_l1277z00_1552;

							{	/* Inline/variant.scm 130 */
								bool_t BgL_tmpz00_3086;

								BgL_l1277z00_1552 = BgL_argsz00_1550;
							BgL_zc3z04anonymousza31547ze3z87_1553:
								if (PAIRP(BgL_l1277z00_1552))
									{	/* Inline/variant.scm 130 */
										{	/* Inline/variant.scm 130 */
											obj_t BgL_az00_1555;

											BgL_az00_1555 = CAR(BgL_l1277z00_1552);
											{	/* Inline/variant.scm 130 */
												bool_t BgL_test1925z00_3090;

												{	/* Inline/variant.scm 130 */
													obj_t BgL_tmpz00_3091;

													{	/* Inline/variant.scm 130 */
														BgL_objectz00_bglt BgL_tmpz00_3092;

														BgL_tmpz00_3092 =
															((BgL_objectz00_bglt) BgL_az00_1555);
														BgL_tmpz00_3091 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3092);
													}
													BgL_test1925z00_3090 = CBOOL(BgL_tmpz00_3091);
												}
												if (BgL_test1925z00_3090)
													{	/* Inline/variant.scm 130 */
														{	/* Inline/variant.scm 130 */
															long BgL_arg1552z00_1558;

															{	/* Inline/variant.scm 130 */
																obj_t BgL_arg1553z00_1559;

																{	/* Inline/variant.scm 130 */
																	obj_t BgL_arg1559z00_1560;

																	{	/* Inline/variant.scm 130 */
																		obj_t BgL_arg1815z00_2106;
																		long BgL_arg1816z00_2107;

																		BgL_arg1815z00_2106 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Inline/variant.scm 130 */
																			long BgL_arg1817z00_2108;

																			BgL_arg1817z00_2108 =
																				BGL_OBJECT_CLASS_NUM(
																				((BgL_objectz00_bglt) BgL_az00_1555));
																			BgL_arg1816z00_2107 =
																				(BgL_arg1817z00_2108 - OBJECT_TYPE);
																		}
																		BgL_arg1559z00_1560 =
																			VECTOR_REF(BgL_arg1815z00_2106,
																			BgL_arg1816z00_2107);
																	}
																	BgL_arg1553z00_1559 =
																		BGl_classzd2superzd2zz__objectz00
																		(BgL_arg1559z00_1560);
																}
																{	/* Inline/variant.scm 130 */
																	obj_t BgL_tmpz00_3102;

																	BgL_tmpz00_3102 =
																		((obj_t) BgL_arg1553z00_1559);
																	BgL_arg1552z00_1558 =
																		BGL_CLASS_NUM(BgL_tmpz00_3102);
															}}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_az00_1555),
																BgL_arg1552z00_1558);
														}
														{	/* Inline/variant.scm 130 */
															BgL_objectz00_bglt BgL_tmpz00_3107;

															BgL_tmpz00_3107 =
																((BgL_objectz00_bglt) BgL_az00_1555);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3107, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_az00_1555);
														BgL_az00_1555;
													}
												else
													{	/* Inline/variant.scm 130 */
														BFALSE;
													}
											}
										}
										{
											obj_t BgL_l1277z00_3111;

											BgL_l1277z00_3111 = CDR(BgL_l1277z00_1552);
											BgL_l1277z00_1552 = BgL_l1277z00_3111;
											goto BgL_zc3z04anonymousza31547ze3z87_1553;
										}
									}
								else
									{	/* Inline/variant.scm 130 */
										BgL_tmpz00_3086 = ((bool_t) 1);
									}
								return BBOOL(BgL_tmpz00_3086);
							}
						}
					}
				}
			}
		}

	}



/* &shrink-args! */
	obj_t BGl_z62shrinkzd2argsz12za2zzinline_variantz00(obj_t BgL_envz00_2381,
		obj_t BgL_varz00_2382)
	{
		{	/* Inline/variant.scm 127 */
			return
				BGl_shrinkzd2argsz12zc0zzinline_variantz00(
				((BgL_variablez00_bglt) BgL_varz00_2382));
		}

	}



/* remove-invariant-args! */
	BGL_EXPORTED_DEF BgL_appz00_bglt
		BGl_removezd2invariantzd2argsz12z12zzinline_variantz00(BgL_appz00_bglt
		BgL_appz00_60)
	{
		{	/* Inline/variant.scm 141 */
			{	/* Inline/variant.scm 143 */
				obj_t BgL_g1148z00_1564;

				BgL_g1148z00_1564 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											(((BgL_varz00_bglt) COBJECT(
														(((BgL_appz00_bglt) COBJECT(BgL_appz00_60))->
															BgL_funz00)))->BgL_variablez00)))->
									BgL_valuez00))))->BgL_argsz00);
				{
					obj_t BgL_formalsz00_1568;
					obj_t BgL_oldzd2argszd2_1569;
					obj_t BgL_newzd2argszd2_1570;

					BgL_formalsz00_1568 = BgL_g1148z00_1564;
					BgL_oldzd2argszd2_1569 =
						(((BgL_appz00_bglt) COBJECT(BgL_appz00_60))->BgL_argsz00);
					BgL_newzd2argszd2_1570 = BNIL;
				BgL_zc3z04anonymousza31565ze3z87_1571:
					if (NULLP(BgL_oldzd2argszd2_1569))
						{	/* Inline/variant.scm 147 */
							((((BgL_appz00_bglt) COBJECT(BgL_appz00_60))->BgL_argsz00) =
								((obj_t) bgl_reverse_bang(BgL_newzd2argszd2_1570)), BUNSPEC);
							return BgL_appz00_60;
						}
					else
						{	/* Inline/variant.scm 150 */
							bool_t BgL_test1927z00_3125;

							{	/* Inline/variant.scm 150 */
								bool_t BgL_test1928z00_3126;

								{	/* Inline/variant.scm 150 */
									obj_t BgL_arg1606z00_1589;

									BgL_arg1606z00_1589 = CAR(((obj_t) BgL_formalsz00_1568));
									{	/* Inline/variant.scm 150 */
										obj_t BgL_classz00_2123;

										BgL_classz00_2123 =
											BGl_localzf2variantzf2zzinline_variantz00;
										if (BGL_OBJECTP(BgL_arg1606z00_1589))
											{	/* Inline/variant.scm 150 */
												BgL_objectz00_bglt BgL_arg1807z00_2125;

												BgL_arg1807z00_2125 =
													(BgL_objectz00_bglt) (BgL_arg1606z00_1589);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Inline/variant.scm 150 */
														long BgL_idxz00_2131;

														BgL_idxz00_2131 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2125);
														BgL_test1928z00_3126 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2131 + 3L)) == BgL_classz00_2123);
													}
												else
													{	/* Inline/variant.scm 150 */
														bool_t BgL_res1827z00_2156;

														{	/* Inline/variant.scm 150 */
															obj_t BgL_oclassz00_2139;

															{	/* Inline/variant.scm 150 */
																obj_t BgL_arg1815z00_2147;
																long BgL_arg1816z00_2148;

																BgL_arg1815z00_2147 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Inline/variant.scm 150 */
																	long BgL_arg1817z00_2149;

																	BgL_arg1817z00_2149 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2125);
																	BgL_arg1816z00_2148 =
																		(BgL_arg1817z00_2149 - OBJECT_TYPE);
																}
																BgL_oclassz00_2139 =
																	VECTOR_REF(BgL_arg1815z00_2147,
																	BgL_arg1816z00_2148);
															}
															{	/* Inline/variant.scm 150 */
																bool_t BgL__ortest_1115z00_2140;

																BgL__ortest_1115z00_2140 =
																	(BgL_classz00_2123 == BgL_oclassz00_2139);
																if (BgL__ortest_1115z00_2140)
																	{	/* Inline/variant.scm 150 */
																		BgL_res1827z00_2156 =
																			BgL__ortest_1115z00_2140;
																	}
																else
																	{	/* Inline/variant.scm 150 */
																		long BgL_odepthz00_2141;

																		{	/* Inline/variant.scm 150 */
																			obj_t BgL_arg1804z00_2142;

																			BgL_arg1804z00_2142 =
																				(BgL_oclassz00_2139);
																			BgL_odepthz00_2141 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2142);
																		}
																		if ((3L < BgL_odepthz00_2141))
																			{	/* Inline/variant.scm 150 */
																				obj_t BgL_arg1802z00_2144;

																				{	/* Inline/variant.scm 150 */
																					obj_t BgL_arg1803z00_2145;

																					BgL_arg1803z00_2145 =
																						(BgL_oclassz00_2139);
																					BgL_arg1802z00_2144 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2145, 3L);
																				}
																				BgL_res1827z00_2156 =
																					(BgL_arg1802z00_2144 ==
																					BgL_classz00_2123);
																			}
																		else
																			{	/* Inline/variant.scm 150 */
																				BgL_res1827z00_2156 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1928z00_3126 = BgL_res1827z00_2156;
													}
											}
										else
											{	/* Inline/variant.scm 150 */
												BgL_test1928z00_3126 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test1928z00_3126)
									{	/* Inline/variant.scm 151 */
										bool_t BgL_test1933z00_3151;

										{	/* Inline/variant.scm 151 */
											BgL_localz00_bglt BgL_oz00_2158;

											BgL_oz00_2158 =
												((BgL_localz00_bglt)
												CAR(((obj_t) BgL_formalsz00_1568)));
											{
												BgL_localzf2variantzf2_bglt BgL_auxz00_3155;

												{
													obj_t BgL_auxz00_3156;

													{	/* Inline/variant.scm 151 */
														BgL_objectz00_bglt BgL_tmpz00_3157;

														BgL_tmpz00_3157 =
															((BgL_objectz00_bglt) BgL_oz00_2158);
														BgL_auxz00_3156 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3157);
													}
													BgL_auxz00_3155 =
														((BgL_localzf2variantzf2_bglt) BgL_auxz00_3156);
												}
												BgL_test1933z00_3151 =
													(((BgL_localzf2variantzf2_bglt)
														COBJECT(BgL_auxz00_3155))->BgL_variantz00);
											}
										}
										if (BgL_test1933z00_3151)
											{	/* Inline/variant.scm 151 */
												BgL_test1927z00_3125 = ((bool_t) 0);
											}
										else
											{	/* Inline/variant.scm 151 */
												BgL_test1927z00_3125 = ((bool_t) 1);
											}
									}
								else
									{	/* Inline/variant.scm 150 */
										BgL_test1927z00_3125 = ((bool_t) 0);
									}
							}
							if (BgL_test1927z00_3125)
								{	/* Inline/variant.scm 152 */
									obj_t BgL_arg1589z00_1580;
									obj_t BgL_arg1591z00_1581;

									BgL_arg1589z00_1580 = CDR(((obj_t) BgL_formalsz00_1568));
									BgL_arg1591z00_1581 = CDR(((obj_t) BgL_oldzd2argszd2_1569));
									{
										obj_t BgL_oldzd2argszd2_3167;
										obj_t BgL_formalsz00_3166;

										BgL_formalsz00_3166 = BgL_arg1589z00_1580;
										BgL_oldzd2argszd2_3167 = BgL_arg1591z00_1581;
										BgL_oldzd2argszd2_1569 = BgL_oldzd2argszd2_3167;
										BgL_formalsz00_1568 = BgL_formalsz00_3166;
										goto BgL_zc3z04anonymousza31565ze3z87_1571;
									}
								}
							else
								{	/* Inline/variant.scm 156 */
									obj_t BgL_arg1593z00_1582;
									obj_t BgL_arg1594z00_1583;
									obj_t BgL_arg1595z00_1584;

									BgL_arg1593z00_1582 = CDR(((obj_t) BgL_formalsz00_1568));
									BgL_arg1594z00_1583 = CDR(((obj_t) BgL_oldzd2argszd2_1569));
									{	/* Inline/variant.scm 158 */
										obj_t BgL_arg1602z00_1585;

										BgL_arg1602z00_1585 = CAR(((obj_t) BgL_oldzd2argszd2_1569));
										BgL_arg1595z00_1584 =
											MAKE_YOUNG_PAIR(BgL_arg1602z00_1585,
											BgL_newzd2argszd2_1570);
									}
									{
										obj_t BgL_newzd2argszd2_3177;
										obj_t BgL_oldzd2argszd2_3176;
										obj_t BgL_formalsz00_3175;

										BgL_formalsz00_3175 = BgL_arg1593z00_1582;
										BgL_oldzd2argszd2_3176 = BgL_arg1594z00_1583;
										BgL_newzd2argszd2_3177 = BgL_arg1595z00_1584;
										BgL_newzd2argszd2_1570 = BgL_newzd2argszd2_3177;
										BgL_oldzd2argszd2_1569 = BgL_oldzd2argszd2_3176;
										BgL_formalsz00_1568 = BgL_formalsz00_3175;
										goto BgL_zc3z04anonymousza31565ze3z87_1571;
									}
								}
						}
				}
			}
		}

	}



/* &remove-invariant-args! */
	BgL_appz00_bglt
		BGl_z62removezd2invariantzd2argsz12z70zzinline_variantz00(obj_t
		BgL_envz00_2383, obj_t BgL_appz00_2384)
	{
		{	/* Inline/variant.scm 141 */
			return
				BGl_removezd2invariantzd2argsz12z12zzinline_variantz00(
				((BgL_appz00_bglt) BgL_appz00_2384));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinline_variantz00(void)
	{
		{	/* Inline/variant.scm 15 */
			{	/* Inline/variant.scm 22 */
				obj_t BgL_arg1627z00_1598;
				obj_t BgL_arg1629z00_1599;

				{	/* Inline/variant.scm 22 */
					obj_t BgL_v1279z00_1634;

					BgL_v1279z00_1634 = create_vector(1L);
					{	/* Inline/variant.scm 22 */
						obj_t BgL_arg1675z00_1635;

						BgL_arg1675z00_1635 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc1832z00zzinline_variantz00,
							BGl_proc1831z00zzinline_variantz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1279z00_1634, 0L, BgL_arg1675z00_1635);
					}
					BgL_arg1627z00_1598 = BgL_v1279z00_1634;
				}
				{	/* Inline/variant.scm 22 */
					obj_t BgL_v1280z00_1645;

					BgL_v1280z00_1645 = create_vector(0L);
					BgL_arg1629z00_1599 = BgL_v1280z00_1645;
				}
				return (BGl_localzf2variantzf2zzinline_variantz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(4),
						CNST_TABLE_REF(5), BGl_localz00zzast_varz00, 37050L,
						BGl_proc1836z00zzinline_variantz00,
						BGl_proc1835z00zzinline_variantz00, BFALSE,
						BGl_proc1834z00zzinline_variantz00,
						BGl_proc1833z00zzinline_variantz00, BgL_arg1627z00_1598,
						BgL_arg1629z00_1599), BUNSPEC);
			}
		}

	}



/* &lambda1652 */
	BgL_localz00_bglt BGl_z62lambda1652z62zzinline_variantz00(obj_t
		BgL_envz00_2391, obj_t BgL_o1121z00_2392)
	{
		{	/* Inline/variant.scm 22 */
			{	/* Inline/variant.scm 22 */
				long BgL_arg1654z00_2450;

				{	/* Inline/variant.scm 22 */
					obj_t BgL_arg1661z00_2451;

					{	/* Inline/variant.scm 22 */
						obj_t BgL_arg1663z00_2452;

						{	/* Inline/variant.scm 22 */
							obj_t BgL_arg1815z00_2453;
							long BgL_arg1816z00_2454;

							BgL_arg1815z00_2453 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Inline/variant.scm 22 */
								long BgL_arg1817z00_2455;

								BgL_arg1817z00_2455 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1121z00_2392)));
								BgL_arg1816z00_2454 = (BgL_arg1817z00_2455 - OBJECT_TYPE);
							}
							BgL_arg1663z00_2452 =
								VECTOR_REF(BgL_arg1815z00_2453, BgL_arg1816z00_2454);
						}
						BgL_arg1661z00_2451 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1663z00_2452);
					}
					{	/* Inline/variant.scm 22 */
						obj_t BgL_tmpz00_3197;

						BgL_tmpz00_3197 = ((obj_t) BgL_arg1661z00_2451);
						BgL_arg1654z00_2450 = BGL_CLASS_NUM(BgL_tmpz00_3197);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1121z00_2392)), BgL_arg1654z00_2450);
			}
			{	/* Inline/variant.scm 22 */
				BgL_objectz00_bglt BgL_tmpz00_3203;

				BgL_tmpz00_3203 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2392));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3203, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2392));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1121z00_2392));
		}

	}



/* &<@anonymous:1651> */
	obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzinline_variantz00(obj_t
		BgL_envz00_2393, obj_t BgL_new1120z00_2394)
	{
		{	/* Inline/variant.scm 22 */
			{
				BgL_localz00_bglt BgL_auxz00_3211;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1120z00_2394))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(6)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3219;

					{	/* Inline/variant.scm 22 */
						obj_t BgL_classz00_2457;

						BgL_classz00_2457 = BGl_typez00zztype_typez00;
						{	/* Inline/variant.scm 22 */
							obj_t BgL__ortest_1117z00_2458;

							BgL__ortest_1117z00_2458 = BGL_CLASS_NIL(BgL_classz00_2457);
							if (CBOOL(BgL__ortest_1117z00_2458))
								{	/* Inline/variant.scm 22 */
									BgL_auxz00_3219 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_2458);
								}
							else
								{	/* Inline/variant.scm 22 */
									BgL_auxz00_3219 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2457));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1120z00_2394))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_3219), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_3229;

					{	/* Inline/variant.scm 22 */
						obj_t BgL_classz00_2459;

						BgL_classz00_2459 = BGl_valuez00zzast_varz00;
						{	/* Inline/variant.scm 22 */
							obj_t BgL__ortest_1117z00_2460;

							BgL__ortest_1117z00_2460 = BGL_CLASS_NIL(BgL_classz00_2459);
							if (CBOOL(BgL__ortest_1117z00_2460))
								{	/* Inline/variant.scm 22 */
									BgL_auxz00_3229 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_2460);
								}
							else
								{	/* Inline/variant.scm 22 */
									BgL_auxz00_3229 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2459));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1120z00_2394))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_3229), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1120z00_2394))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1120z00_2394))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2variantzf2_bglt BgL_auxz00_3266;

					{
						obj_t BgL_auxz00_3267;

						{	/* Inline/variant.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_3268;

							BgL_tmpz00_3268 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1120z00_2394));
							BgL_auxz00_3267 = BGL_OBJECT_WIDENING(BgL_tmpz00_3268);
						}
						BgL_auxz00_3266 = ((BgL_localzf2variantzf2_bglt) BgL_auxz00_3267);
					}
					((((BgL_localzf2variantzf2_bglt) COBJECT(BgL_auxz00_3266))->
							BgL_variantz00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_3211 = ((BgL_localz00_bglt) BgL_new1120z00_2394);
				return ((obj_t) BgL_auxz00_3211);
			}
		}

	}



/* &lambda1647 */
	BgL_localz00_bglt BGl_z62lambda1647z62zzinline_variantz00(obj_t
		BgL_envz00_2395, obj_t BgL_o1117z00_2396)
	{
		{	/* Inline/variant.scm 22 */
			{	/* Inline/variant.scm 22 */
				BgL_localzf2variantzf2_bglt BgL_wide1119z00_2462;

				BgL_wide1119z00_2462 =
					((BgL_localzf2variantzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2variantzf2_bgl))));
				{	/* Inline/variant.scm 22 */
					obj_t BgL_auxz00_3281;
					BgL_objectz00_bglt BgL_tmpz00_3277;

					BgL_auxz00_3281 = ((obj_t) BgL_wide1119z00_2462);
					BgL_tmpz00_3277 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2396)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3277, BgL_auxz00_3281);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2396)));
				{	/* Inline/variant.scm 22 */
					long BgL_arg1650z00_2463;

					BgL_arg1650z00_2463 =
						BGL_CLASS_NUM(BGl_localzf2variantzf2zzinline_variantz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1117z00_2396))), BgL_arg1650z00_2463);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1117z00_2396)));
			}
		}

	}



/* &lambda1630 */
	BgL_localz00_bglt BGl_z62lambda1630z62zzinline_variantz00(obj_t
		BgL_envz00_2397, obj_t BgL_id1103z00_2398, obj_t BgL_name1104z00_2399,
		obj_t BgL_type1105z00_2400, obj_t BgL_value1106z00_2401,
		obj_t BgL_access1107z00_2402, obj_t BgL_fastzd2alpha1108zd2_2403,
		obj_t BgL_removable1109z00_2404, obj_t BgL_occurrence1110z00_2405,
		obj_t BgL_occurrencew1111z00_2406, obj_t BgL_userzf31112zf3_2407,
		obj_t BgL_key1113z00_2408, obj_t BgL_valzd2noescape1114zd2_2409,
		obj_t BgL_volatile1115z00_2410, obj_t BgL_variant1116z00_2411)
	{
		{	/* Inline/variant.scm 22 */
			{	/* Inline/variant.scm 22 */
				long BgL_occurrence1110z00_2467;
				long BgL_occurrencew1111z00_2468;
				bool_t BgL_userzf31112zf3_2469;
				long BgL_key1113z00_2470;
				bool_t BgL_volatile1115z00_2471;
				bool_t BgL_variant1116z00_2472;

				BgL_occurrence1110z00_2467 = (long) CINT(BgL_occurrence1110z00_2405);
				BgL_occurrencew1111z00_2468 = (long) CINT(BgL_occurrencew1111z00_2406);
				BgL_userzf31112zf3_2469 = CBOOL(BgL_userzf31112zf3_2407);
				BgL_key1113z00_2470 = (long) CINT(BgL_key1113z00_2408);
				BgL_volatile1115z00_2471 = CBOOL(BgL_volatile1115z00_2410);
				BgL_variant1116z00_2472 = CBOOL(BgL_variant1116z00_2411);
				{	/* Inline/variant.scm 22 */
					BgL_localz00_bglt BgL_new1153z00_2473;

					{	/* Inline/variant.scm 22 */
						BgL_localz00_bglt BgL_tmp1151z00_2474;
						BgL_localzf2variantzf2_bglt BgL_wide1152z00_2475;

						{
							BgL_localz00_bglt BgL_auxz00_3301;

							{	/* Inline/variant.scm 22 */
								BgL_localz00_bglt BgL_new1150z00_2476;

								BgL_new1150z00_2476 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Inline/variant.scm 22 */
									long BgL_arg1646z00_2477;

									BgL_arg1646z00_2477 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1150z00_2476),
										BgL_arg1646z00_2477);
								}
								{	/* Inline/variant.scm 22 */
									BgL_objectz00_bglt BgL_tmpz00_3306;

									BgL_tmpz00_3306 = ((BgL_objectz00_bglt) BgL_new1150z00_2476);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3306, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1150z00_2476);
								BgL_auxz00_3301 = BgL_new1150z00_2476;
							}
							BgL_tmp1151z00_2474 = ((BgL_localz00_bglt) BgL_auxz00_3301);
						}
						BgL_wide1152z00_2475 =
							((BgL_localzf2variantzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2variantzf2_bgl))));
						{	/* Inline/variant.scm 22 */
							obj_t BgL_auxz00_3314;
							BgL_objectz00_bglt BgL_tmpz00_3312;

							BgL_auxz00_3314 = ((obj_t) BgL_wide1152z00_2475);
							BgL_tmpz00_3312 = ((BgL_objectz00_bglt) BgL_tmp1151z00_2474);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3312, BgL_auxz00_3314);
						}
						((BgL_objectz00_bglt) BgL_tmp1151z00_2474);
						{	/* Inline/variant.scm 22 */
							long BgL_arg1642z00_2478;

							BgL_arg1642z00_2478 =
								BGL_CLASS_NUM(BGl_localzf2variantzf2zzinline_variantz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1151z00_2474),
								BgL_arg1642z00_2478);
						}
						BgL_new1153z00_2473 = ((BgL_localz00_bglt) BgL_tmp1151z00_2474);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1153z00_2473)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1103z00_2398)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_namez00) =
						((obj_t) BgL_name1104z00_2399), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1105z00_2400)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1106z00_2401)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_accessz00) =
						((obj_t) BgL_access1107z00_2402), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1108zd2_2403), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_removablez00) =
						((obj_t) BgL_removable1109z00_2404), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_occurrencez00) =
						((long) BgL_occurrence1110z00_2467), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1111z00_2468), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1153z00_2473)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31112zf3_2469), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1153z00_2473)))->BgL_keyz00) =
						((long) BgL_key1113z00_2470), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1153z00_2473)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1114zd2_2409), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1153z00_2473)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1115z00_2471), BUNSPEC);
					{
						BgL_localzf2variantzf2_bglt BgL_auxz00_3351;

						{
							obj_t BgL_auxz00_3352;

							{	/* Inline/variant.scm 22 */
								BgL_objectz00_bglt BgL_tmpz00_3353;

								BgL_tmpz00_3353 = ((BgL_objectz00_bglt) BgL_new1153z00_2473);
								BgL_auxz00_3352 = BGL_OBJECT_WIDENING(BgL_tmpz00_3353);
							}
							BgL_auxz00_3351 = ((BgL_localzf2variantzf2_bglt) BgL_auxz00_3352);
						}
						((((BgL_localzf2variantzf2_bglt) COBJECT(BgL_auxz00_3351))->
								BgL_variantz00) = ((bool_t) BgL_variant1116z00_2472), BUNSPEC);
					}
					return BgL_new1153z00_2473;
				}
			}
		}

	}



/* &lambda1683 */
	obj_t BGl_z62lambda1683z62zzinline_variantz00(obj_t BgL_envz00_2412,
		obj_t BgL_oz00_2413, obj_t BgL_vz00_2414)
	{
		{	/* Inline/variant.scm 22 */
			{	/* Inline/variant.scm 22 */
				bool_t BgL_vz00_2480;

				BgL_vz00_2480 = CBOOL(BgL_vz00_2414);
				{
					BgL_localzf2variantzf2_bglt BgL_auxz00_3359;

					{
						obj_t BgL_auxz00_3360;

						{	/* Inline/variant.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_3361;

							BgL_tmpz00_3361 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_2413));
							BgL_auxz00_3360 = BGL_OBJECT_WIDENING(BgL_tmpz00_3361);
						}
						BgL_auxz00_3359 = ((BgL_localzf2variantzf2_bglt) BgL_auxz00_3360);
					}
					return
						((((BgL_localzf2variantzf2_bglt) COBJECT(BgL_auxz00_3359))->
							BgL_variantz00) = ((bool_t) BgL_vz00_2480), BUNSPEC);
				}
			}
		}

	}



/* &lambda1682 */
	obj_t BGl_z62lambda1682z62zzinline_variantz00(obj_t BgL_envz00_2415,
		obj_t BgL_oz00_2416)
	{
		{	/* Inline/variant.scm 22 */
			{	/* Inline/variant.scm 22 */
				bool_t BgL_tmpz00_3367;

				{
					BgL_localzf2variantzf2_bglt BgL_auxz00_3368;

					{
						obj_t BgL_auxz00_3369;

						{	/* Inline/variant.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_3370;

							BgL_tmpz00_3370 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_2416));
							BgL_auxz00_3369 = BGL_OBJECT_WIDENING(BgL_tmpz00_3370);
						}
						BgL_auxz00_3368 = ((BgL_localzf2variantzf2_bglt) BgL_auxz00_3369);
					}
					BgL_tmpz00_3367 =
						(((BgL_localzf2variantzf2_bglt) COBJECT(BgL_auxz00_3368))->
						BgL_variantz00);
				}
				return BBOOL(BgL_tmpz00_3367);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinline_variantz00(void)
	{
		{	/* Inline/variant.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinline_variantz00(void)
	{
		{	/* Inline/variant.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinline_variantz00(void)
	{
		{	/* Inline/variant.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1837z00zzinline_variantz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1837z00zzinline_variantz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1837z00zzinline_variantz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1837z00zzinline_variantz00));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1837z00zzinline_variantz00));
		}

	}

#ifdef __cplusplus
}
#endif
