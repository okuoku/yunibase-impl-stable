/*===========================================================================*/
/*   (Inline/size.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Inline/size.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INLINE_SIZE_TYPE_DEFINITIONS
#define BGL_INLINE_SIZE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_siza7edzd2sequencez75_bgl
	{
		long BgL_siza7eza7;
	}                            *BgL_siza7edzd2sequencez75_bglt;

	typedef struct BgL_siza7edzd2syncz75_bgl
	{
		long BgL_siza7eza7;
	}                        *BgL_siza7edzd2syncz75_bglt;

	typedef struct BgL_siza7edzd2switchz75_bgl
	{
		long BgL_siza7eza7;
	}                          *BgL_siza7edzd2switchz75_bglt;

	typedef struct BgL_siza7edzd2letzd2funza7_bgl
	{
		long BgL_siza7eza7;
	}                             *BgL_siza7edzd2letzd2funza7_bglt;

	typedef struct BgL_siza7edzd2letzd2varza7_bgl
	{
		long BgL_siza7eza7;
	}                             *BgL_siza7edzd2letzd2varza7_bglt;


#endif													// BGL_INLINE_SIZE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62nodezd2siza7ezd2siza7edzd2swit1440zb0zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31692ze3ze5zzinline_siza7eza7(obj_t,
		obj_t);
	static BgL_switchz00_bglt BGl_z62lambda1690z62zzinline_siza7eza7(obj_t,
		obj_t);
	static BgL_switchz00_bglt BGl_z62lambda1693z62zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1776z62zzinline_siza7eza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1777z62zzinline_siza7eza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzinline_siza7eza7 = BUNSPEC;
	static obj_t BGl_z62nodezd2siza7ezd2var1406zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_siza7edzd2syncz75zzinline_siza7eza7 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzinline_siza7eza7(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2conditiona1434zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2cast1428zc5zzinline_siza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzinline_siza7eza7(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzinline_siza7eza7(void);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2funcall1426zc5zzinline_siza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62nodezd2siza7ezd2setq1432zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2app1420zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2sync1416zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2retblock1454zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_siza7edzd2letzd2varza7zzinline_siza7eza7 = BUNSPEC;
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2fail1436zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2letzd2var1446z17zzinline_siza7eza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzinline_siza7eza7(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62nodezd2siza7ezd2boxzd2ref1460z17zzinline_siza7eza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t make_string(long, unsigned char);
	static obj_t BGl_z62nodezd2siza7e1399z17zzinline_siza7eza7(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2siza7ezd2kwote1408zc5zzinline_siza7eza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static long BGl_mz00zzinline_siza7eza7 = 0L;
	static obj_t BGl_z62nodezd2siza7ez17zzinline_siza7eza7(obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2siza7ezd2jumpzd2exzd2it1452zc5zzinline_siza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzinline_siza7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_siza7edzd2switchz75zzinline_siza7eza7 = BUNSPEC;
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2siza7ezd2appzd2ly1423z17zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_siza7edzd2sequencez75zzinline_siza7eza7 = BUNSPEC;
	BGL_EXPORTED_DECL long
		BGl_nodezd2siza7ez75zzinline_siza7eza7(BgL_nodez00_bglt);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzinline_siza7eza7(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinline_siza7eza7(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzinline_siza7eza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzinline_siza7eza7(void);
	static obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzinline_siza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	static obj_t BGl_z62nodezd2siza7ezd2atom1403zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2makezd2box1458z17zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2return1456zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62nodezd2siza7ezd2boxzd2setz121462z05zzinline_siza7eza7(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31725ze3ze5zzinline_siza7eza7(obj_t,
		obj_t);
	static BgL_syncz00_bglt BGl_z62lambda1614z62zzinline_siza7eza7(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_syncz00_bglt BGl_z62lambda1617z62zzinline_siza7eza7(obj_t, obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2extern1430zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2switch1438zc5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1706z62zzinline_siza7eza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1707z62zzinline_siza7eza7(obj_t, obj_t, obj_t);
	static BgL_syncz00_bglt BGl_z62lambda1627z62zzinline_siza7eza7(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static BgL_letzd2funzd2_bglt BGl_z62lambda1718z62zzinline_siza7eza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2siza7ezd2siza7edzd2letzd21444z62zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62nodezd2siza7ezd2siza7edzd2letzd21448z62zzinline_siza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_letzd2funzd2_bglt BGl_z62lambda1723z62zzinline_siza7eza7(obj_t,
		obj_t);
	static BgL_letzd2funzd2_bglt BGl_z62lambda1726z62zzinline_siza7eza7(obj_t,
		obj_t);
	static BgL_sequencez00_bglt BGl_z62lambda1565z62zzinline_siza7eza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t
		BGl_z62nodezd2siza7ezd2siza7edzd2sequ1414zb0zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1652z62zzinline_siza7eza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1653z62zzinline_siza7eza7(obj_t, obj_t, obj_t);
	static BgL_sequencez00_bglt BGl_z62lambda1574z62zzinline_siza7eza7(obj_t,
		obj_t);
	static BgL_sequencez00_bglt BGl_z62lambda1577z62zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1739z62zzinline_siza7eza7(obj_t, obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2letzd2fun1442z17zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_siza7edzd2letzd2funza7zzinline_siza7eza7 = BUNSPEC;
	static obj_t
		BGl_z62nodezd2siza7ezd2setzd2exzd2it1450zc5zzinline_siza7eza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1740z62zzinline_siza7eza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31576ze3ze5zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62nodezd2siza7ezd2siza7edzd2sync1418zb0zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2siza7ezd2sequence1412zc5zzinline_siza7eza7(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_letzd2varzd2_bglt BGl_z62lambda1752z62zzinline_siza7eza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_letzd2varzd2_bglt BGl_z62lambda1755z62zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1595z62zzinline_siza7eza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1596z62zzinline_siza7eza7(obj_t, obj_t, obj_t);
	static BgL_switchz00_bglt BGl_z62lambda1682z62zzinline_siza7eza7(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_letzd2varzd2_bglt BGl_z62lambda1763z62zzinline_siza7eza7(obj_t,
		obj_t);
	static obj_t __cnst[9];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2031z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1707za7622090z00, BGl_z62lambda1707z62zzinline_siza7eza7,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2032z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1706za7622091z00, BGl_z62lambda1706z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2033z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1693za7622092z00, BGl_z62lambda1693z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2034z00zzinline_siza7eza7,
		BgL_bgl_za762za7c3za704anonymo2093za7,
		BGl_z62zc3z04anonymousza31692ze3ze5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2035z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1690za7622094z00, BGl_z62lambda1690z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2036z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1682za7622095z00, BGl_z62lambda1682z62zzinline_siza7eza7,
		0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2037z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1740za7622096z00, BGl_z62lambda1740z62zzinline_siza7eza7,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2038z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1739za7622097z00, BGl_z62lambda1739z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2039z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1726za7622098z00, BGl_z62lambda1726z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2040z00zzinline_siza7eza7,
		BgL_bgl_za762za7c3za704anonymo2099za7,
		BGl_z62zc3z04anonymousza31725ze3ze5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2041z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1723za7622100z00, BGl_z62lambda1723z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2042z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1718za7622101z00, BGl_z62lambda1718z62zzinline_siza7eza7,
		0L, BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2043z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1777za7622102z00, BGl_z62lambda1777z62zzinline_siza7eza7,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2050z00zzinline_siza7eza7,
		BgL_bgl_string2050za700za7za7i2103za7, "node-size1399", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2044z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1776za7622104z00, BGl_z62lambda1776z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2051z00zzinline_siza7eza7,
		BgL_bgl_string2051za700za7za7i2105za7, "No method for this object", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2045z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1763za7622106z00, BGl_z62lambda1763z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2046z00zzinline_siza7eza7,
		BgL_bgl_za762za7c3za704anonymo2107za7,
		BGl_z62zc3z04anonymousza31762ze3ze5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2053z00zzinline_siza7eza7,
		BgL_bgl_string2053za700za7za7i2108za7, "node-size", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2047z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1755za7622109z00, BGl_z62lambda1755z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2048z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1752za7622110z00, BGl_z62lambda1752z62zzinline_siza7eza7,
		0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2049z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2111za7,
		BGl_z62nodezd2siza7e1399z17zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2052z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2112za7,
		BGl_z62nodezd2siza7ezd2atom1403zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2054z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2113za7,
		BGl_z62nodezd2siza7ezd2var1406zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2055z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2114za7,
		BGl_z62nodezd2siza7ezd2kwote1408zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2056z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2115za7,
		BGl_z62nodezd2siza7ezd2sequence1412zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2057z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2116za7,
		BGl_z62nodezd2siza7ezd2siza7edzd2sequ1414zb0zzinline_siza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2117za7,
		BGl_z62nodezd2siza7ezd2sync1416zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2059z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2118za7,
		BGl_z62nodezd2siza7ezd2siza7edzd2sync1418zb0zzinline_siza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2060z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2119za7,
		BGl_z62nodezd2siza7ezd2app1420zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2061z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2120za7,
		BGl_z62nodezd2siza7ezd2appzd2ly1423z17zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2062z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2121za7,
		BGl_z62nodezd2siza7ezd2funcall1426zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2063z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2122za7,
		BGl_z62nodezd2siza7ezd2cast1428zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2064z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2123za7,
		BGl_z62nodezd2siza7ezd2extern1430zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2065z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2124za7,
		BGl_z62nodezd2siza7ezd2setq1432zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2066z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2125za7,
		BGl_z62nodezd2siza7ezd2conditiona1434zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2067z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2126za7,
		BGl_z62nodezd2siza7ezd2fail1436zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2068z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2127za7,
		BGl_z62nodezd2siza7ezd2switch1438zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2069z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2128za7,
		BGl_z62nodezd2siza7ezd2siza7edzd2swit1440zb0zzinline_siza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2070z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2129za7,
		BGl_z62nodezd2siza7ezd2letzd2fun1442z17zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2071z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2130za7,
		BGl_z62nodezd2siza7ezd2siza7edzd2letzd21444z62zzinline_siza7eza7, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2072z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2131za7,
		BGl_z62nodezd2siza7ezd2letzd2var1446z17zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2073z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2132za7,
		BGl_z62nodezd2siza7ezd2siza7edzd2letzd21448z62zzinline_siza7eza7, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2074z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2133za7,
		BGl_z62nodezd2siza7ezd2setzd2exzd2it1450zc5zzinline_siza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2081z00zzinline_siza7eza7,
		BgL_bgl_string2081za700za7za7i2134za7, "INLINESZ", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2075z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2135za7,
		BGl_z62nodezd2siza7ezd2jumpzd2exzd2it1452zc5zzinline_siza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2082z00zzinline_siza7eza7,
		BgL_bgl_string2082za700za7za7i2136za7, ">>> inline-size node=", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2076z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2137za7,
		BGl_z62nodezd2siza7ezd2retblock1454zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2083z00zzinline_siza7eza7,
		BgL_bgl_string2083za700za7za7i2138za7, "--- inline-size node=", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2077z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2139za7,
		BGl_z62nodezd2siza7ezd2return1456zc5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2084z00zzinline_siza7eza7,
		BgL_bgl_string2084za700za7za7i2140za7, " size=", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2078z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2141za7,
		BGl_z62nodezd2siza7ezd2makezd2box1458z17zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2085z00zzinline_siza7eza7,
		BgL_bgl_string2085za700za7za7i2142za7, " null=", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2079z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2143za7,
		BGl_z62nodezd2siza7ezd2boxzd2ref1460z17zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2086z00zzinline_siza7eza7,
		BgL_bgl_string2086za700za7za7i2144za7, "<<< inline-size node=", 21);
	      DEFINE_STRING(BGl_string2087z00zzinline_siza7eza7,
		BgL_bgl_string2087za700za7za7i2145za7, "inline_size", 11);
	      DEFINE_STRING(BGl_string2088z00zzinline_siza7eza7,
		BgL_bgl_string2088za700za7za7i2146za7,
		"node-size1399 sized-let-var sized-let-fun sized-switch sized-sync inline_size sized-sequence long size ",
		103);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2080z00zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2147za7,
		BGl_z62nodezd2siza7ezd2boxzd2setz121462z05zzinline_siza7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
		BgL_bgl_za762nodeza7d2siza7a7e2148za7,
		BGl_z62nodezd2siza7ez17zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2019z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1596za7622149z00, BGl_z62lambda1596z62zzinline_siza7eza7,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2020z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1595za7622150z00, BGl_z62lambda1595z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2021z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1577za7622151z00, BGl_z62lambda1577z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2022z00zzinline_siza7eza7,
		BgL_bgl_za762za7c3za704anonymo2152za7,
		BGl_z62zc3z04anonymousza31576ze3ze5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2023z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1574za7622153z00, BGl_z62lambda1574z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2024z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1565za7622154z00, BGl_z62lambda1565z62zzinline_siza7eza7,
		0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2025z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1653za7622155z00, BGl_z62lambda1653z62zzinline_siza7eza7,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2026z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1652za7622156z00, BGl_z62lambda1652z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2027z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1627za7622157z00, BGl_z62lambda1627z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2028z00zzinline_siza7eza7,
		BgL_bgl_za762za7c3za704anonymo2158za7,
		BGl_z62zc3z04anonymousza31626ze3ze5zzinline_siza7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2029z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1617za7622159z00, BGl_z62lambda1617z62zzinline_siza7eza7,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2030z00zzinline_siza7eza7,
		BgL_bgl_za762lambda1614za7622160z00, BGl_z62lambda1614z62zzinline_siza7eza7,
		0L, BUNSPEC, 6);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzinline_siza7eza7));
		     ADD_ROOT((void *) (&BGl_siza7edzd2syncz75zzinline_siza7eza7));
		     ADD_ROOT((void *) (&BGl_siza7edzd2letzd2varza7zzinline_siza7eza7));
		     ADD_ROOT((void *) (&BGl_siza7edzd2switchz75zzinline_siza7eza7));
		     ADD_ROOT((void *) (&BGl_siza7edzd2sequencez75zzinline_siza7eza7));
		     ADD_ROOT((void *) (&BGl_siza7edzd2letzd2funza7zzinline_siza7eza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinline_siza7eza7(long
		BgL_checksumz00_3222, char *BgL_fromz00_3223)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinline_siza7eza7))
				{
					BGl_requirezd2initializa7ationz75zzinline_siza7eza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinline_siza7eza7();
					BGl_libraryzd2moduleszd2initz00zzinline_siza7eza7();
					BGl_cnstzd2initzd2zzinline_siza7eza7();
					BGl_importedzd2moduleszd2initz00zzinline_siza7eza7();
					BGl_objectzd2initzd2zzinline_siza7eza7();
					BGl_genericzd2initzd2zzinline_siza7eza7();
					BGl_methodzd2initzd2zzinline_siza7eza7();
					return BGl_toplevelzd2initzd2zzinline_siza7eza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinline_siza7eza7(void)
	{
		{	/* Inline/size.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "inline_size");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"inline_size");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "inline_size");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "inline_size");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "inline_size");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "inline_size");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "inline_size");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"inline_size");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "inline_size");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "inline_size");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"inline_size");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "inline_size");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinline_siza7eza7(void)
	{
		{	/* Inline/size.scm 15 */
			{	/* Inline/size.scm 15 */
				obj_t BgL_cportz00_2922;

				{	/* Inline/size.scm 15 */
					obj_t BgL_stringz00_2929;

					BgL_stringz00_2929 = BGl_string2088z00zzinline_siza7eza7;
					{	/* Inline/size.scm 15 */
						obj_t BgL_startz00_2930;

						BgL_startz00_2930 = BINT(0L);
						{	/* Inline/size.scm 15 */
							obj_t BgL_endz00_2931;

							BgL_endz00_2931 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2929)));
							{	/* Inline/size.scm 15 */

								BgL_cportz00_2922 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2929, BgL_startz00_2930, BgL_endz00_2931);
				}}}}
				{
					long BgL_iz00_2923;

					BgL_iz00_2923 = 8L;
				BgL_loopz00_2924:
					if ((BgL_iz00_2923 == -1L))
						{	/* Inline/size.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Inline/size.scm 15 */
							{	/* Inline/size.scm 15 */
								obj_t BgL_arg2089z00_2925;

								{	/* Inline/size.scm 15 */

									{	/* Inline/size.scm 15 */
										obj_t BgL_locationz00_2927;

										BgL_locationz00_2927 = BBOOL(((bool_t) 0));
										{	/* Inline/size.scm 15 */

											BgL_arg2089z00_2925 =
												BGl_readz00zz__readerz00(BgL_cportz00_2922,
												BgL_locationz00_2927);
										}
									}
								}
								{	/* Inline/size.scm 15 */
									int BgL_tmpz00_3256;

									BgL_tmpz00_3256 = (int) (BgL_iz00_2923);
									CNST_TABLE_SET(BgL_tmpz00_3256, BgL_arg2089z00_2925);
							}}
							{	/* Inline/size.scm 15 */
								int BgL_auxz00_2928;

								BgL_auxz00_2928 = (int) ((BgL_iz00_2923 - 1L));
								{
									long BgL_iz00_3261;

									BgL_iz00_3261 = (long) (BgL_auxz00_2928);
									BgL_iz00_2923 = BgL_iz00_3261;
									goto BgL_loopz00_2924;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinline_siza7eza7(void)
	{
		{	/* Inline/size.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzinline_siza7eza7(void)
	{
		{	/* Inline/size.scm 15 */
			BGl_mz00zzinline_siza7eza7 = 0L;
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinline_siza7eza7(void)
	{
		{	/* Inline/size.scm 15 */
			{	/* Inline/size.scm 22 */
				obj_t BgL_arg1561z00_1564;
				obj_t BgL_arg1564z00_1565;

				{	/* Inline/size.scm 22 */
					obj_t BgL_v1389z00_1594;

					BgL_v1389z00_1594 = create_vector(1L);
					{	/* Inline/size.scm 22 */
						obj_t BgL_arg1591z00_1595;

						BgL_arg1591z00_1595 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2020z00zzinline_siza7eza7,
							BGl_proc2019z00zzinline_siza7eza7, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1389z00_1594, 0L, BgL_arg1591z00_1595);
					}
					BgL_arg1561z00_1564 = BgL_v1389z00_1594;
				}
				{	/* Inline/size.scm 22 */
					obj_t BgL_v1390z00_1605;

					BgL_v1390z00_1605 = create_vector(0L);
					BgL_arg1564z00_1565 = BgL_v1390z00_1605;
				}
				BGl_siza7edzd2sequencez75zzinline_siza7eza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
					CNST_TABLE_REF(3), BGl_sequencez00zzast_nodez00, 10745L,
					BGl_proc2024z00zzinline_siza7eza7, BGl_proc2023z00zzinline_siza7eza7,
					BFALSE, BGl_proc2022z00zzinline_siza7eza7,
					BGl_proc2021z00zzinline_siza7eza7, BgL_arg1561z00_1564,
					BgL_arg1564z00_1565);
			}
			{	/* Inline/size.scm 23 */
				obj_t BgL_arg1611z00_1614;
				obj_t BgL_arg1613z00_1615;

				{	/* Inline/size.scm 23 */
					obj_t BgL_v1391z00_1642;

					BgL_v1391z00_1642 = create_vector(1L);
					{	/* Inline/size.scm 23 */
						obj_t BgL_arg1646z00_1643;

						BgL_arg1646z00_1643 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2026z00zzinline_siza7eza7,
							BGl_proc2025z00zzinline_siza7eza7, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1391z00_1642, 0L, BgL_arg1646z00_1643);
					}
					BgL_arg1611z00_1614 = BgL_v1391z00_1642;
				}
				{	/* Inline/size.scm 23 */
					obj_t BgL_v1392z00_1653;

					BgL_v1392z00_1653 = create_vector(0L);
					BgL_arg1613z00_1615 = BgL_v1392z00_1653;
				}
				BGl_siza7edzd2syncz75zzinline_siza7eza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(4),
					CNST_TABLE_REF(3), BGl_syncz00zzast_nodez00, 41409L,
					BGl_proc2030z00zzinline_siza7eza7, BGl_proc2029z00zzinline_siza7eza7,
					BFALSE, BGl_proc2028z00zzinline_siza7eza7,
					BGl_proc2027z00zzinline_siza7eza7, BgL_arg1611z00_1614,
					BgL_arg1613z00_1615);
			}
			{	/* Inline/size.scm 24 */
				obj_t BgL_arg1678z00_1662;
				obj_t BgL_arg1681z00_1663;

				{	/* Inline/size.scm 24 */
					obj_t BgL_v1393z00_1692;

					BgL_v1393z00_1692 = create_vector(1L);
					{	/* Inline/size.scm 24 */
						obj_t BgL_arg1702z00_1693;

						BgL_arg1702z00_1693 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2032z00zzinline_siza7eza7,
							BGl_proc2031z00zzinline_siza7eza7, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1393z00_1692, 0L, BgL_arg1702z00_1693);
					}
					BgL_arg1678z00_1662 = BgL_v1393z00_1692;
				}
				{	/* Inline/size.scm 24 */
					obj_t BgL_v1394z00_1703;

					BgL_v1394z00_1703 = create_vector(0L);
					BgL_arg1681z00_1663 = BgL_v1394z00_1703;
				}
				BGl_siza7edzd2switchz75zzinline_siza7eza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(5),
					CNST_TABLE_REF(3), BGl_switchz00zzast_nodez00, 37483L,
					BGl_proc2036z00zzinline_siza7eza7, BGl_proc2035z00zzinline_siza7eza7,
					BFALSE, BGl_proc2034z00zzinline_siza7eza7,
					BGl_proc2033z00zzinline_siza7eza7, BgL_arg1678z00_1662,
					BgL_arg1681z00_1663);
			}
			{	/* Inline/size.scm 25 */
				obj_t BgL_arg1714z00_1712;
				obj_t BgL_arg1717z00_1713;

				{	/* Inline/size.scm 25 */
					obj_t BgL_v1395z00_1741;

					BgL_v1395z00_1741 = create_vector(1L);
					{	/* Inline/size.scm 25 */
						obj_t BgL_arg1736z00_1742;

						BgL_arg1736z00_1742 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2038z00zzinline_siza7eza7,
							BGl_proc2037z00zzinline_siza7eza7, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1395z00_1741, 0L, BgL_arg1736z00_1742);
					}
					BgL_arg1714z00_1712 = BgL_v1395z00_1741;
				}
				{	/* Inline/size.scm 25 */
					obj_t BgL_v1396z00_1752;

					BgL_v1396z00_1752 = create_vector(0L);
					BgL_arg1717z00_1713 = BgL_v1396z00_1752;
				}
				BGl_siza7edzd2letzd2funza7zzinline_siza7eza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(6),
					CNST_TABLE_REF(3), BGl_letzd2funzd2zzast_nodez00, 7325L,
					BGl_proc2042z00zzinline_siza7eza7, BGl_proc2041z00zzinline_siza7eza7,
					BFALSE, BGl_proc2040z00zzinline_siza7eza7,
					BGl_proc2039z00zzinline_siza7eza7, BgL_arg1714z00_1712,
					BgL_arg1717z00_1713);
			}
			{	/* Inline/size.scm 26 */
				obj_t BgL_arg1750z00_1761;
				obj_t BgL_arg1751z00_1762;

				{	/* Inline/size.scm 26 */
					obj_t BgL_v1397z00_1791;

					BgL_v1397z00_1791 = create_vector(1L);
					{	/* Inline/size.scm 26 */
						obj_t BgL_arg1771z00_1792;

						BgL_arg1771z00_1792 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2044z00zzinline_siza7eza7,
							BGl_proc2043z00zzinline_siza7eza7, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1397z00_1791, 0L, BgL_arg1771z00_1792);
					}
					BgL_arg1750z00_1761 = BgL_v1397z00_1791;
				}
				{	/* Inline/size.scm 26 */
					obj_t BgL_v1398z00_1802;

					BgL_v1398z00_1802 = create_vector(0L);
					BgL_arg1751z00_1762 = BgL_v1398z00_1802;
				}
				return (BGl_siza7edzd2letzd2varza7zzinline_siza7eza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(7),
						CNST_TABLE_REF(3), BGl_letzd2varzd2zzast_nodez00, 20381L,
						BGl_proc2048z00zzinline_siza7eza7,
						BGl_proc2047z00zzinline_siza7eza7, BFALSE,
						BGl_proc2046z00zzinline_siza7eza7,
						BGl_proc2045z00zzinline_siza7eza7, BgL_arg1750z00_1761,
						BgL_arg1751z00_1762), BUNSPEC);
			}
		}

	}



/* &lambda1763 */
	BgL_letzd2varzd2_bglt BGl_z62lambda1763z62zzinline_siza7eza7(obj_t
		BgL_envz00_2736, obj_t BgL_o1169z00_2737)
	{
		{	/* Inline/size.scm 26 */
			{	/* Inline/size.scm 26 */
				long BgL_arg1765z00_2934;

				{	/* Inline/size.scm 26 */
					obj_t BgL_arg1767z00_2935;

					{	/* Inline/size.scm 26 */
						obj_t BgL_arg1770z00_2936;

						{	/* Inline/size.scm 26 */
							obj_t BgL_arg1815z00_2937;
							long BgL_arg1816z00_2938;

							BgL_arg1815z00_2937 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Inline/size.scm 26 */
								long BgL_arg1817z00_2939;

								BgL_arg1817z00_2939 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_o1169z00_2737)));
								BgL_arg1816z00_2938 = (BgL_arg1817z00_2939 - OBJECT_TYPE);
							}
							BgL_arg1770z00_2936 =
								VECTOR_REF(BgL_arg1815z00_2937, BgL_arg1816z00_2938);
						}
						BgL_arg1767z00_2935 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1770z00_2936);
					}
					{	/* Inline/size.scm 26 */
						obj_t BgL_tmpz00_3316;

						BgL_tmpz00_3316 = ((obj_t) BgL_arg1767z00_2935);
						BgL_arg1765z00_2934 = BGL_CLASS_NUM(BgL_tmpz00_3316);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_letzd2varzd2_bglt) BgL_o1169z00_2737)), BgL_arg1765z00_2934);
			}
			{	/* Inline/size.scm 26 */
				BgL_objectz00_bglt BgL_tmpz00_3322;

				BgL_tmpz00_3322 =
					((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_o1169z00_2737));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3322, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_o1169z00_2737));
			return
				((BgL_letzd2varzd2_bglt) ((BgL_letzd2varzd2_bglt) BgL_o1169z00_2737));
		}

	}



/* &<@anonymous:1762> */
	obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzinline_siza7eza7(obj_t
		BgL_envz00_2738, obj_t BgL_new1168z00_2739)
	{
		{	/* Inline/size.scm 26 */
			{
				BgL_letzd2varzd2_bglt BgL_auxz00_3330;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_new1168z00_2739))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3334;

					{	/* Inline/size.scm 26 */
						obj_t BgL_classz00_2941;

						BgL_classz00_2941 = BGl_typez00zztype_typez00;
						{	/* Inline/size.scm 26 */
							obj_t BgL__ortest_1117z00_2942;

							BgL__ortest_1117z00_2942 = BGL_CLASS_NIL(BgL_classz00_2941);
							if (CBOOL(BgL__ortest_1117z00_2942))
								{	/* Inline/size.scm 26 */
									BgL_auxz00_3334 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_2942);
								}
							else
								{	/* Inline/size.scm 26 */
									BgL_auxz00_3334 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2941));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_new1168z00_2739))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_3334), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_letzd2varzd2_bglt) BgL_new1168z00_2739))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_letzd2varzd2_bglt)
										BgL_new1168z00_2739))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_letzd2varzd2_bglt)
							COBJECT(((BgL_letzd2varzd2_bglt) ((BgL_letzd2varzd2_bglt)
										BgL_new1168z00_2739))))->BgL_bindingsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_3353;

					{	/* Inline/size.scm 26 */
						obj_t BgL_classz00_2943;

						BgL_classz00_2943 = BGl_nodez00zzast_nodez00;
						{	/* Inline/size.scm 26 */
							obj_t BgL__ortest_1117z00_2944;

							BgL__ortest_1117z00_2944 = BGL_CLASS_NIL(BgL_classz00_2943);
							if (CBOOL(BgL__ortest_1117z00_2944))
								{	/* Inline/size.scm 26 */
									BgL_auxz00_3353 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_2944);
								}
							else
								{	/* Inline/size.scm 26 */
									BgL_auxz00_3353 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2943));
								}
						}
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt)
										((BgL_letzd2varzd2_bglt) BgL_new1168z00_2739))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_3353), BUNSPEC);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt)
									((BgL_letzd2varzd2_bglt) BgL_new1168z00_2739))))->
						BgL_removablezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_siza7edzd2letzd2varza7_bglt BgL_auxz00_3366;

					{
						obj_t BgL_auxz00_3367;

						{	/* Inline/size.scm 26 */
							BgL_objectz00_bglt BgL_tmpz00_3368;

							BgL_tmpz00_3368 =
								((BgL_objectz00_bglt)
								((BgL_letzd2varzd2_bglt) BgL_new1168z00_2739));
							BgL_auxz00_3367 = BGL_OBJECT_WIDENING(BgL_tmpz00_3368);
						}
						BgL_auxz00_3366 =
							((BgL_siza7edzd2letzd2varza7_bglt) BgL_auxz00_3367);
					}
					((((BgL_siza7edzd2letzd2varza7_bglt) COBJECT(BgL_auxz00_3366))->
							BgL_siza7eza7) = ((long) 0L), BUNSPEC);
				}
				BgL_auxz00_3330 = ((BgL_letzd2varzd2_bglt) BgL_new1168z00_2739);
				return ((obj_t) BgL_auxz00_3330);
			}
		}

	}



/* &lambda1755 */
	BgL_letzd2varzd2_bglt BGl_z62lambda1755z62zzinline_siza7eza7(obj_t
		BgL_envz00_2740, obj_t BgL_o1165z00_2741)
	{
		{	/* Inline/size.scm 26 */
			{	/* Inline/size.scm 26 */
				BgL_siza7edzd2letzd2varza7_bglt BgL_wide1167z00_2946;

				BgL_wide1167z00_2946 =
					((BgL_siza7edzd2letzd2varza7_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_siza7edzd2letzd2varza7_bgl))));
				{	/* Inline/size.scm 26 */
					obj_t BgL_auxz00_3381;
					BgL_objectz00_bglt BgL_tmpz00_3377;

					BgL_auxz00_3381 = ((obj_t) BgL_wide1167z00_2946);
					BgL_tmpz00_3377 =
						((BgL_objectz00_bglt)
						((BgL_letzd2varzd2_bglt)
							((BgL_letzd2varzd2_bglt) BgL_o1165z00_2741)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3377, BgL_auxz00_3381);
				}
				((BgL_objectz00_bglt)
					((BgL_letzd2varzd2_bglt)
						((BgL_letzd2varzd2_bglt) BgL_o1165z00_2741)));
				{	/* Inline/size.scm 26 */
					long BgL_arg1761z00_2947;

					BgL_arg1761z00_2947 =
						BGL_CLASS_NUM(BGl_siza7edzd2letzd2varza7zzinline_siza7eza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_letzd2varzd2_bglt)
								((BgL_letzd2varzd2_bglt) BgL_o1165z00_2741))),
						BgL_arg1761z00_2947);
				}
				return
					((BgL_letzd2varzd2_bglt)
					((BgL_letzd2varzd2_bglt)
						((BgL_letzd2varzd2_bglt) BgL_o1165z00_2741)));
			}
		}

	}



/* &lambda1752 */
	BgL_letzd2varzd2_bglt BGl_z62lambda1752z62zzinline_siza7eza7(obj_t
		BgL_envz00_2742, obj_t BgL_loc1157z00_2743, obj_t BgL_type1158z00_2744,
		obj_t BgL_sidezd2effect1159zd2_2745, obj_t BgL_key1160z00_2746,
		obj_t BgL_bindings1161z00_2747, obj_t BgL_body1162z00_2748,
		obj_t BgL_removablezf31163zf3_2749, obj_t BgL_siza7e1164za7_2750)
	{
		{	/* Inline/size.scm 26 */
			{	/* Inline/size.scm 26 */
				bool_t BgL_removablezf31163zf3_2950;
				long BgL_siza7e1164za7_2951;

				BgL_removablezf31163zf3_2950 = CBOOL(BgL_removablezf31163zf3_2749);
				BgL_siza7e1164za7_2951 = (long) CINT(BgL_siza7e1164za7_2750);
				{	/* Inline/size.scm 26 */
					BgL_letzd2varzd2_bglt BgL_new1260z00_2952;

					{	/* Inline/size.scm 26 */
						BgL_letzd2varzd2_bglt BgL_tmp1257z00_2953;
						BgL_siza7edzd2letzd2varza7_bglt BgL_wide1258z00_2954;

						{
							BgL_letzd2varzd2_bglt BgL_auxz00_3397;

							{	/* Inline/size.scm 26 */
								BgL_letzd2varzd2_bglt BgL_new1256z00_2955;

								BgL_new1256z00_2955 =
									((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_letzd2varzd2_bgl))));
								{	/* Inline/size.scm 26 */
									long BgL_arg1754z00_2956;

									BgL_arg1754z00_2956 =
										BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1256z00_2955),
										BgL_arg1754z00_2956);
								}
								{	/* Inline/size.scm 26 */
									BgL_objectz00_bglt BgL_tmpz00_3402;

									BgL_tmpz00_3402 = ((BgL_objectz00_bglt) BgL_new1256z00_2955);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3402, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1256z00_2955);
								BgL_auxz00_3397 = BgL_new1256z00_2955;
							}
							BgL_tmp1257z00_2953 = ((BgL_letzd2varzd2_bglt) BgL_auxz00_3397);
						}
						BgL_wide1258z00_2954 =
							((BgL_siza7edzd2letzd2varza7_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_siza7edzd2letzd2varza7_bgl))));
						{	/* Inline/size.scm 26 */
							obj_t BgL_auxz00_3410;
							BgL_objectz00_bglt BgL_tmpz00_3408;

							BgL_auxz00_3410 = ((obj_t) BgL_wide1258z00_2954);
							BgL_tmpz00_3408 = ((BgL_objectz00_bglt) BgL_tmp1257z00_2953);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3408, BgL_auxz00_3410);
						}
						((BgL_objectz00_bglt) BgL_tmp1257z00_2953);
						{	/* Inline/size.scm 26 */
							long BgL_arg1753z00_2957;

							BgL_arg1753z00_2957 =
								BGL_CLASS_NUM(BGl_siza7edzd2letzd2varza7zzinline_siza7eza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1257z00_2953),
								BgL_arg1753z00_2957);
						}
						BgL_new1260z00_2952 = ((BgL_letzd2varzd2_bglt) BgL_tmp1257z00_2953);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1260z00_2952)))->BgL_locz00) =
						((obj_t) BgL_loc1157z00_2743), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1260z00_2952)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1158z00_2744)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1260z00_2952)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1159zd2_2745), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1260z00_2952)))->BgL_keyz00) =
						((obj_t) BgL_key1160z00_2746), BUNSPEC);
					((((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_new1260z00_2952)))->BgL_bindingsz00) =
						((obj_t) BgL_bindings1161z00_2747), BUNSPEC);
					((((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_new1260z00_2952)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1162z00_2748)),
						BUNSPEC);
					((((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
										BgL_new1260z00_2952)))->BgL_removablezf3zf3) =
						((bool_t) BgL_removablezf31163zf3_2950), BUNSPEC);
					{
						BgL_siza7edzd2letzd2varza7_bglt BgL_auxz00_3434;

						{
							obj_t BgL_auxz00_3435;

							{	/* Inline/size.scm 26 */
								BgL_objectz00_bglt BgL_tmpz00_3436;

								BgL_tmpz00_3436 = ((BgL_objectz00_bglt) BgL_new1260z00_2952);
								BgL_auxz00_3435 = BGL_OBJECT_WIDENING(BgL_tmpz00_3436);
							}
							BgL_auxz00_3434 =
								((BgL_siza7edzd2letzd2varza7_bglt) BgL_auxz00_3435);
						}
						((((BgL_siza7edzd2letzd2varza7_bglt) COBJECT(BgL_auxz00_3434))->
								BgL_siza7eza7) = ((long) BgL_siza7e1164za7_2951), BUNSPEC);
					}
					return BgL_new1260z00_2952;
				}
			}
		}

	}



/* &lambda1777 */
	obj_t BGl_z62lambda1777z62zzinline_siza7eza7(obj_t BgL_envz00_2751,
		obj_t BgL_oz00_2752, obj_t BgL_vz00_2753)
	{
		{	/* Inline/size.scm 26 */
			{	/* Inline/size.scm 26 */
				long BgL_vz00_2959;

				BgL_vz00_2959 = (long) CINT(BgL_vz00_2753);
				{
					BgL_siza7edzd2letzd2varza7_bglt BgL_auxz00_3442;

					{
						obj_t BgL_auxz00_3443;

						{	/* Inline/size.scm 26 */
							BgL_objectz00_bglt BgL_tmpz00_3444;

							BgL_tmpz00_3444 =
								((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_2752));
							BgL_auxz00_3443 = BGL_OBJECT_WIDENING(BgL_tmpz00_3444);
						}
						BgL_auxz00_3442 =
							((BgL_siza7edzd2letzd2varza7_bglt) BgL_auxz00_3443);
					}
					return
						((((BgL_siza7edzd2letzd2varza7_bglt) COBJECT(BgL_auxz00_3442))->
							BgL_siza7eza7) = ((long) BgL_vz00_2959), BUNSPEC);
		}}}

	}



/* &lambda1776 */
	obj_t BGl_z62lambda1776z62zzinline_siza7eza7(obj_t BgL_envz00_2754,
		obj_t BgL_oz00_2755)
	{
		{	/* Inline/size.scm 26 */
			{	/* Inline/size.scm 26 */
				long BgL_tmpz00_3450;

				{
					BgL_siza7edzd2letzd2varza7_bglt BgL_auxz00_3451;

					{
						obj_t BgL_auxz00_3452;

						{	/* Inline/size.scm 26 */
							BgL_objectz00_bglt BgL_tmpz00_3453;

							BgL_tmpz00_3453 =
								((BgL_objectz00_bglt) ((BgL_letzd2varzd2_bglt) BgL_oz00_2755));
							BgL_auxz00_3452 = BGL_OBJECT_WIDENING(BgL_tmpz00_3453);
						}
						BgL_auxz00_3451 =
							((BgL_siza7edzd2letzd2varza7_bglt) BgL_auxz00_3452);
					}
					BgL_tmpz00_3450 =
						(((BgL_siza7edzd2letzd2varza7_bglt) COBJECT(BgL_auxz00_3451))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_3450);
			}
		}

	}



/* &lambda1726 */
	BgL_letzd2funzd2_bglt BGl_z62lambda1726z62zzinline_siza7eza7(obj_t
		BgL_envz00_2756, obj_t BgL_o1155z00_2757)
	{
		{	/* Inline/size.scm 25 */
			{	/* Inline/size.scm 25 */
				long BgL_arg1733z00_2962;

				{	/* Inline/size.scm 25 */
					obj_t BgL_arg1734z00_2963;

					{	/* Inline/size.scm 25 */
						obj_t BgL_arg1735z00_2964;

						{	/* Inline/size.scm 25 */
							obj_t BgL_arg1815z00_2965;
							long BgL_arg1816z00_2966;

							BgL_arg1815z00_2965 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Inline/size.scm 25 */
								long BgL_arg1817z00_2967;

								BgL_arg1817z00_2967 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_o1155z00_2757)));
								BgL_arg1816z00_2966 = (BgL_arg1817z00_2967 - OBJECT_TYPE);
							}
							BgL_arg1735z00_2964 =
								VECTOR_REF(BgL_arg1815z00_2965, BgL_arg1816z00_2966);
						}
						BgL_arg1734z00_2963 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1735z00_2964);
					}
					{	/* Inline/size.scm 25 */
						obj_t BgL_tmpz00_3467;

						BgL_tmpz00_3467 = ((obj_t) BgL_arg1734z00_2963);
						BgL_arg1733z00_2962 = BGL_CLASS_NUM(BgL_tmpz00_3467);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_letzd2funzd2_bglt) BgL_o1155z00_2757)), BgL_arg1733z00_2962);
			}
			{	/* Inline/size.scm 25 */
				BgL_objectz00_bglt BgL_tmpz00_3473;

				BgL_tmpz00_3473 =
					((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_o1155z00_2757));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3473, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_o1155z00_2757));
			return
				((BgL_letzd2funzd2_bglt) ((BgL_letzd2funzd2_bglt) BgL_o1155z00_2757));
		}

	}



/* &<@anonymous:1725> */
	obj_t BGl_z62zc3z04anonymousza31725ze3ze5zzinline_siza7eza7(obj_t
		BgL_envz00_2758, obj_t BgL_new1154z00_2759)
	{
		{	/* Inline/size.scm 25 */
			{
				BgL_letzd2funzd2_bglt BgL_auxz00_3481;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_new1154z00_2759))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3485;

					{	/* Inline/size.scm 25 */
						obj_t BgL_classz00_2969;

						BgL_classz00_2969 = BGl_typez00zztype_typez00;
						{	/* Inline/size.scm 25 */
							obj_t BgL__ortest_1117z00_2970;

							BgL__ortest_1117z00_2970 = BGL_CLASS_NIL(BgL_classz00_2969);
							if (CBOOL(BgL__ortest_1117z00_2970))
								{	/* Inline/size.scm 25 */
									BgL_auxz00_3485 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_2970);
								}
							else
								{	/* Inline/size.scm 25 */
									BgL_auxz00_3485 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2969));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_new1154z00_2759))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_3485), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_letzd2funzd2_bglt) BgL_new1154z00_2759))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_letzd2funzd2_bglt)
										BgL_new1154z00_2759))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_letzd2funzd2_bglt)
							COBJECT(((BgL_letzd2funzd2_bglt) ((BgL_letzd2funzd2_bglt)
										BgL_new1154z00_2759))))->BgL_localsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_3504;

					{	/* Inline/size.scm 25 */
						obj_t BgL_classz00_2971;

						BgL_classz00_2971 = BGl_nodez00zzast_nodez00;
						{	/* Inline/size.scm 25 */
							obj_t BgL__ortest_1117z00_2972;

							BgL__ortest_1117z00_2972 = BGL_CLASS_NIL(BgL_classz00_2971);
							if (CBOOL(BgL__ortest_1117z00_2972))
								{	/* Inline/size.scm 25 */
									BgL_auxz00_3504 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_2972);
								}
							else
								{	/* Inline/size.scm 25 */
									BgL_auxz00_3504 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2971));
								}
						}
					}
					((((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt)
										((BgL_letzd2funzd2_bglt) BgL_new1154z00_2759))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_3504), BUNSPEC);
				}
				{
					BgL_siza7edzd2letzd2funza7_bglt BgL_auxz00_3514;

					{
						obj_t BgL_auxz00_3515;

						{	/* Inline/size.scm 25 */
							BgL_objectz00_bglt BgL_tmpz00_3516;

							BgL_tmpz00_3516 =
								((BgL_objectz00_bglt)
								((BgL_letzd2funzd2_bglt) BgL_new1154z00_2759));
							BgL_auxz00_3515 = BGL_OBJECT_WIDENING(BgL_tmpz00_3516);
						}
						BgL_auxz00_3514 =
							((BgL_siza7edzd2letzd2funza7_bglt) BgL_auxz00_3515);
					}
					((((BgL_siza7edzd2letzd2funza7_bglt) COBJECT(BgL_auxz00_3514))->
							BgL_siza7eza7) = ((long) 0L), BUNSPEC);
				}
				BgL_auxz00_3481 = ((BgL_letzd2funzd2_bglt) BgL_new1154z00_2759);
				return ((obj_t) BgL_auxz00_3481);
			}
		}

	}



/* &lambda1723 */
	BgL_letzd2funzd2_bglt BGl_z62lambda1723z62zzinline_siza7eza7(obj_t
		BgL_envz00_2760, obj_t BgL_o1151z00_2761)
	{
		{	/* Inline/size.scm 25 */
			{	/* Inline/size.scm 25 */
				BgL_siza7edzd2letzd2funza7_bglt BgL_wide1153z00_2974;

				BgL_wide1153z00_2974 =
					((BgL_siza7edzd2letzd2funza7_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_siza7edzd2letzd2funza7_bgl))));
				{	/* Inline/size.scm 25 */
					obj_t BgL_auxz00_3529;
					BgL_objectz00_bglt BgL_tmpz00_3525;

					BgL_auxz00_3529 = ((obj_t) BgL_wide1153z00_2974);
					BgL_tmpz00_3525 =
						((BgL_objectz00_bglt)
						((BgL_letzd2funzd2_bglt)
							((BgL_letzd2funzd2_bglt) BgL_o1151z00_2761)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3525, BgL_auxz00_3529);
				}
				((BgL_objectz00_bglt)
					((BgL_letzd2funzd2_bglt)
						((BgL_letzd2funzd2_bglt) BgL_o1151z00_2761)));
				{	/* Inline/size.scm 25 */
					long BgL_arg1724z00_2975;

					BgL_arg1724z00_2975 =
						BGL_CLASS_NUM(BGl_siza7edzd2letzd2funza7zzinline_siza7eza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_letzd2funzd2_bglt)
								((BgL_letzd2funzd2_bglt) BgL_o1151z00_2761))),
						BgL_arg1724z00_2975);
				}
				return
					((BgL_letzd2funzd2_bglt)
					((BgL_letzd2funzd2_bglt)
						((BgL_letzd2funzd2_bglt) BgL_o1151z00_2761)));
			}
		}

	}



/* &lambda1718 */
	BgL_letzd2funzd2_bglt BGl_z62lambda1718z62zzinline_siza7eza7(obj_t
		BgL_envz00_2762, obj_t BgL_loc1144z00_2763, obj_t BgL_type1145z00_2764,
		obj_t BgL_sidezd2effect1146zd2_2765, obj_t BgL_key1147z00_2766,
		obj_t BgL_locals1148z00_2767, obj_t BgL_body1149z00_2768,
		obj_t BgL_siza7e1150za7_2769)
	{
		{	/* Inline/size.scm 25 */
			{	/* Inline/size.scm 25 */
				long BgL_siza7e1150za7_2978;

				BgL_siza7e1150za7_2978 = (long) CINT(BgL_siza7e1150za7_2769);
				{	/* Inline/size.scm 25 */
					BgL_letzd2funzd2_bglt BgL_new1254z00_2979;

					{	/* Inline/size.scm 25 */
						BgL_letzd2funzd2_bglt BgL_tmp1252z00_2980;
						BgL_siza7edzd2letzd2funza7_bglt BgL_wide1253z00_2981;

						{
							BgL_letzd2funzd2_bglt BgL_auxz00_3544;

							{	/* Inline/size.scm 25 */
								BgL_letzd2funzd2_bglt BgL_new1251z00_2982;

								BgL_new1251z00_2982 =
									((BgL_letzd2funzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_letzd2funzd2_bgl))));
								{	/* Inline/size.scm 25 */
									long BgL_arg1722z00_2983;

									BgL_arg1722z00_2983 =
										BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1251z00_2982),
										BgL_arg1722z00_2983);
								}
								{	/* Inline/size.scm 25 */
									BgL_objectz00_bglt BgL_tmpz00_3549;

									BgL_tmpz00_3549 = ((BgL_objectz00_bglt) BgL_new1251z00_2982);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3549, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1251z00_2982);
								BgL_auxz00_3544 = BgL_new1251z00_2982;
							}
							BgL_tmp1252z00_2980 = ((BgL_letzd2funzd2_bglt) BgL_auxz00_3544);
						}
						BgL_wide1253z00_2981 =
							((BgL_siza7edzd2letzd2funza7_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_siza7edzd2letzd2funza7_bgl))));
						{	/* Inline/size.scm 25 */
							obj_t BgL_auxz00_3557;
							BgL_objectz00_bglt BgL_tmpz00_3555;

							BgL_auxz00_3557 = ((obj_t) BgL_wide1253z00_2981);
							BgL_tmpz00_3555 = ((BgL_objectz00_bglt) BgL_tmp1252z00_2980);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3555, BgL_auxz00_3557);
						}
						((BgL_objectz00_bglt) BgL_tmp1252z00_2980);
						{	/* Inline/size.scm 25 */
							long BgL_arg1720z00_2984;

							BgL_arg1720z00_2984 =
								BGL_CLASS_NUM(BGl_siza7edzd2letzd2funza7zzinline_siza7eza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1252z00_2980),
								BgL_arg1720z00_2984);
						}
						BgL_new1254z00_2979 = ((BgL_letzd2funzd2_bglt) BgL_tmp1252z00_2980);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1254z00_2979)))->BgL_locz00) =
						((obj_t) BgL_loc1144z00_2763), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1254z00_2979)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1145z00_2764)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1254z00_2979)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1146zd2_2765), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1254z00_2979)))->BgL_keyz00) =
						((obj_t) BgL_key1147z00_2766), BUNSPEC);
					((((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
										BgL_new1254z00_2979)))->BgL_localsz00) =
						((obj_t) BgL_locals1148z00_2767), BUNSPEC);
					((((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
										BgL_new1254z00_2979)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1149z00_2768)),
						BUNSPEC);
					{
						BgL_siza7edzd2letzd2funza7_bglt BgL_auxz00_3579;

						{
							obj_t BgL_auxz00_3580;

							{	/* Inline/size.scm 25 */
								BgL_objectz00_bglt BgL_tmpz00_3581;

								BgL_tmpz00_3581 = ((BgL_objectz00_bglt) BgL_new1254z00_2979);
								BgL_auxz00_3580 = BGL_OBJECT_WIDENING(BgL_tmpz00_3581);
							}
							BgL_auxz00_3579 =
								((BgL_siza7edzd2letzd2funza7_bglt) BgL_auxz00_3580);
						}
						((((BgL_siza7edzd2letzd2funza7_bglt) COBJECT(BgL_auxz00_3579))->
								BgL_siza7eza7) = ((long) BgL_siza7e1150za7_2978), BUNSPEC);
					}
					return BgL_new1254z00_2979;
				}
			}
		}

	}



/* &lambda1740 */
	obj_t BGl_z62lambda1740z62zzinline_siza7eza7(obj_t BgL_envz00_2770,
		obj_t BgL_oz00_2771, obj_t BgL_vz00_2772)
	{
		{	/* Inline/size.scm 25 */
			{	/* Inline/size.scm 25 */
				long BgL_vz00_2986;

				BgL_vz00_2986 = (long) CINT(BgL_vz00_2772);
				{
					BgL_siza7edzd2letzd2funza7_bglt BgL_auxz00_3587;

					{
						obj_t BgL_auxz00_3588;

						{	/* Inline/size.scm 25 */
							BgL_objectz00_bglt BgL_tmpz00_3589;

							BgL_tmpz00_3589 =
								((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_2771));
							BgL_auxz00_3588 = BGL_OBJECT_WIDENING(BgL_tmpz00_3589);
						}
						BgL_auxz00_3587 =
							((BgL_siza7edzd2letzd2funza7_bglt) BgL_auxz00_3588);
					}
					return
						((((BgL_siza7edzd2letzd2funza7_bglt) COBJECT(BgL_auxz00_3587))->
							BgL_siza7eza7) = ((long) BgL_vz00_2986), BUNSPEC);
		}}}

	}



/* &lambda1739 */
	obj_t BGl_z62lambda1739z62zzinline_siza7eza7(obj_t BgL_envz00_2773,
		obj_t BgL_oz00_2774)
	{
		{	/* Inline/size.scm 25 */
			{	/* Inline/size.scm 25 */
				long BgL_tmpz00_3595;

				{
					BgL_siza7edzd2letzd2funza7_bglt BgL_auxz00_3596;

					{
						obj_t BgL_auxz00_3597;

						{	/* Inline/size.scm 25 */
							BgL_objectz00_bglt BgL_tmpz00_3598;

							BgL_tmpz00_3598 =
								((BgL_objectz00_bglt) ((BgL_letzd2funzd2_bglt) BgL_oz00_2774));
							BgL_auxz00_3597 = BGL_OBJECT_WIDENING(BgL_tmpz00_3598);
						}
						BgL_auxz00_3596 =
							((BgL_siza7edzd2letzd2funza7_bglt) BgL_auxz00_3597);
					}
					BgL_tmpz00_3595 =
						(((BgL_siza7edzd2letzd2funza7_bglt) COBJECT(BgL_auxz00_3596))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_3595);
			}
		}

	}



/* &lambda1693 */
	BgL_switchz00_bglt BGl_z62lambda1693z62zzinline_siza7eza7(obj_t
		BgL_envz00_2775, obj_t BgL_o1142z00_2776)
	{
		{	/* Inline/size.scm 24 */
			{	/* Inline/size.scm 24 */
				long BgL_arg1699z00_2989;

				{	/* Inline/size.scm 24 */
					obj_t BgL_arg1700z00_2990;

					{	/* Inline/size.scm 24 */
						obj_t BgL_arg1701z00_2991;

						{	/* Inline/size.scm 24 */
							obj_t BgL_arg1815z00_2992;
							long BgL_arg1816z00_2993;

							BgL_arg1815z00_2992 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Inline/size.scm 24 */
								long BgL_arg1817z00_2994;

								BgL_arg1817z00_2994 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_switchz00_bglt) BgL_o1142z00_2776)));
								BgL_arg1816z00_2993 = (BgL_arg1817z00_2994 - OBJECT_TYPE);
							}
							BgL_arg1701z00_2991 =
								VECTOR_REF(BgL_arg1815z00_2992, BgL_arg1816z00_2993);
						}
						BgL_arg1700z00_2990 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1701z00_2991);
					}
					{	/* Inline/size.scm 24 */
						obj_t BgL_tmpz00_3612;

						BgL_tmpz00_3612 = ((obj_t) BgL_arg1700z00_2990);
						BgL_arg1699z00_2989 = BGL_CLASS_NUM(BgL_tmpz00_3612);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_switchz00_bglt) BgL_o1142z00_2776)), BgL_arg1699z00_2989);
			}
			{	/* Inline/size.scm 24 */
				BgL_objectz00_bglt BgL_tmpz00_3618;

				BgL_tmpz00_3618 =
					((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_o1142z00_2776));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3618, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_o1142z00_2776));
			return ((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_o1142z00_2776));
		}

	}



/* &<@anonymous:1692> */
	obj_t BGl_z62zc3z04anonymousza31692ze3ze5zzinline_siza7eza7(obj_t
		BgL_envz00_2777, obj_t BgL_new1141z00_2778)
	{
		{	/* Inline/size.scm 24 */
			{
				BgL_switchz00_bglt BgL_auxz00_3626;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_switchz00_bglt) BgL_new1141z00_2778))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3630;

					{	/* Inline/size.scm 24 */
						obj_t BgL_classz00_2996;

						BgL_classz00_2996 = BGl_typez00zztype_typez00;
						{	/* Inline/size.scm 24 */
							obj_t BgL__ortest_1117z00_2997;

							BgL__ortest_1117z00_2997 = BGL_CLASS_NIL(BgL_classz00_2996);
							if (CBOOL(BgL__ortest_1117z00_2997))
								{	/* Inline/size.scm 24 */
									BgL_auxz00_3630 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_2997);
								}
							else
								{	/* Inline/size.scm 24 */
									BgL_auxz00_3630 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2996));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_switchz00_bglt) BgL_new1141z00_2778))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_3630), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_switchz00_bglt) BgL_new1141z00_2778))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_switchz00_bglt)
										BgL_new1141z00_2778))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_nodez00_bglt BgL_auxz00_3646;

					{	/* Inline/size.scm 24 */
						obj_t BgL_classz00_2998;

						BgL_classz00_2998 = BGl_nodez00zzast_nodez00;
						{	/* Inline/size.scm 24 */
							obj_t BgL__ortest_1117z00_2999;

							BgL__ortest_1117z00_2999 = BGL_CLASS_NIL(BgL_classz00_2998);
							if (CBOOL(BgL__ortest_1117z00_2999))
								{	/* Inline/size.scm 24 */
									BgL_auxz00_3646 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_2999);
								}
							else
								{	/* Inline/size.scm 24 */
									BgL_auxz00_3646 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_2998));
								}
						}
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt)
										((BgL_switchz00_bglt) BgL_new1141z00_2778))))->
							BgL_testz00) = ((BgL_nodez00_bglt) BgL_auxz00_3646), BUNSPEC);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt)
									((BgL_switchz00_bglt) BgL_new1141z00_2778))))->
						BgL_clausesz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3659;

					{	/* Inline/size.scm 24 */
						obj_t BgL_classz00_3000;

						BgL_classz00_3000 = BGl_typez00zztype_typez00;
						{	/* Inline/size.scm 24 */
							obj_t BgL__ortest_1117z00_3001;

							BgL__ortest_1117z00_3001 = BGL_CLASS_NIL(BgL_classz00_3000);
							if (CBOOL(BgL__ortest_1117z00_3001))
								{	/* Inline/size.scm 24 */
									BgL_auxz00_3659 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3001);
								}
							else
								{	/* Inline/size.scm 24 */
									BgL_auxz00_3659 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3000));
								}
						}
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt)
										((BgL_switchz00_bglt) BgL_new1141z00_2778))))->
							BgL_itemzd2typezd2) =
						((BgL_typez00_bglt) BgL_auxz00_3659), BUNSPEC);
				}
				{
					BgL_siza7edzd2switchz75_bglt BgL_auxz00_3669;

					{
						obj_t BgL_auxz00_3670;

						{	/* Inline/size.scm 24 */
							BgL_objectz00_bglt BgL_tmpz00_3671;

							BgL_tmpz00_3671 =
								((BgL_objectz00_bglt)
								((BgL_switchz00_bglt) BgL_new1141z00_2778));
							BgL_auxz00_3670 = BGL_OBJECT_WIDENING(BgL_tmpz00_3671);
						}
						BgL_auxz00_3669 = ((BgL_siza7edzd2switchz75_bglt) BgL_auxz00_3670);
					}
					((((BgL_siza7edzd2switchz75_bglt) COBJECT(BgL_auxz00_3669))->
							BgL_siza7eza7) = ((long) 0L), BUNSPEC);
				}
				BgL_auxz00_3626 = ((BgL_switchz00_bglt) BgL_new1141z00_2778);
				return ((obj_t) BgL_auxz00_3626);
			}
		}

	}



/* &lambda1690 */
	BgL_switchz00_bglt BGl_z62lambda1690z62zzinline_siza7eza7(obj_t
		BgL_envz00_2779, obj_t BgL_o1138z00_2780)
	{
		{	/* Inline/size.scm 24 */
			{	/* Inline/size.scm 24 */
				BgL_siza7edzd2switchz75_bglt BgL_wide1140z00_3003;

				BgL_wide1140z00_3003 =
					((BgL_siza7edzd2switchz75_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_siza7edzd2switchz75_bgl))));
				{	/* Inline/size.scm 24 */
					obj_t BgL_auxz00_3684;
					BgL_objectz00_bglt BgL_tmpz00_3680;

					BgL_auxz00_3684 = ((obj_t) BgL_wide1140z00_3003);
					BgL_tmpz00_3680 =
						((BgL_objectz00_bglt)
						((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_o1138z00_2780)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3680, BgL_auxz00_3684);
				}
				((BgL_objectz00_bglt)
					((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_o1138z00_2780)));
				{	/* Inline/size.scm 24 */
					long BgL_arg1691z00_3004;

					BgL_arg1691z00_3004 =
						BGL_CLASS_NUM(BGl_siza7edzd2switchz75zzinline_siza7eza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_switchz00_bglt)
								((BgL_switchz00_bglt) BgL_o1138z00_2780))),
						BgL_arg1691z00_3004);
				}
				return
					((BgL_switchz00_bglt)
					((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_o1138z00_2780)));
			}
		}

	}



/* &lambda1682 */
	BgL_switchz00_bglt BGl_z62lambda1682z62zzinline_siza7eza7(obj_t
		BgL_envz00_2781, obj_t BgL_loc1130z00_2782, obj_t BgL_type1131z00_2783,
		obj_t BgL_sidezd2effect1132zd2_2784, obj_t BgL_key1133z00_2785,
		obj_t BgL_test1134z00_2786, obj_t BgL_clauses1135z00_2787,
		obj_t BgL_itemzd2type1136zd2_2788, obj_t BgL_siza7e1137za7_2789)
	{
		{	/* Inline/size.scm 24 */
			{	/* Inline/size.scm 24 */
				long BgL_siza7e1137za7_3008;

				BgL_siza7e1137za7_3008 = (long) CINT(BgL_siza7e1137za7_2789);
				{	/* Inline/size.scm 24 */
					BgL_switchz00_bglt BgL_new1248z00_3009;

					{	/* Inline/size.scm 24 */
						BgL_switchz00_bglt BgL_tmp1246z00_3010;
						BgL_siza7edzd2switchz75_bglt BgL_wide1247z00_3011;

						{
							BgL_switchz00_bglt BgL_auxz00_3699;

							{	/* Inline/size.scm 24 */
								BgL_switchz00_bglt BgL_new1245z00_3012;

								BgL_new1245z00_3012 =
									((BgL_switchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_switchz00_bgl))));
								{	/* Inline/size.scm 24 */
									long BgL_arg1689z00_3013;

									BgL_arg1689z00_3013 =
										BGL_CLASS_NUM(BGl_switchz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1245z00_3012),
										BgL_arg1689z00_3013);
								}
								{	/* Inline/size.scm 24 */
									BgL_objectz00_bglt BgL_tmpz00_3704;

									BgL_tmpz00_3704 = ((BgL_objectz00_bglt) BgL_new1245z00_3012);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3704, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1245z00_3012);
								BgL_auxz00_3699 = BgL_new1245z00_3012;
							}
							BgL_tmp1246z00_3010 = ((BgL_switchz00_bglt) BgL_auxz00_3699);
						}
						BgL_wide1247z00_3011 =
							((BgL_siza7edzd2switchz75_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_siza7edzd2switchz75_bgl))));
						{	/* Inline/size.scm 24 */
							obj_t BgL_auxz00_3712;
							BgL_objectz00_bglt BgL_tmpz00_3710;

							BgL_auxz00_3712 = ((obj_t) BgL_wide1247z00_3011);
							BgL_tmpz00_3710 = ((BgL_objectz00_bglt) BgL_tmp1246z00_3010);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3710, BgL_auxz00_3712);
						}
						((BgL_objectz00_bglt) BgL_tmp1246z00_3010);
						{	/* Inline/size.scm 24 */
							long BgL_arg1688z00_3014;

							BgL_arg1688z00_3014 =
								BGL_CLASS_NUM(BGl_siza7edzd2switchz75zzinline_siza7eza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1246z00_3010),
								BgL_arg1688z00_3014);
						}
						BgL_new1248z00_3009 = ((BgL_switchz00_bglt) BgL_tmp1246z00_3010);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1248z00_3009)))->BgL_locz00) =
						((obj_t) BgL_loc1130z00_2782), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1248z00_3009)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1131z00_2783)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1248z00_3009)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1132zd2_2784), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1248z00_3009)))->BgL_keyz00) =
						((obj_t) BgL_key1133z00_2785), BUNSPEC);
					((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
										BgL_new1248z00_3009)))->BgL_testz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_test1134z00_2786)),
						BUNSPEC);
					((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
										BgL_new1248z00_3009)))->BgL_clausesz00) =
						((obj_t) BgL_clauses1135z00_2787), BUNSPEC);
					((((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
										BgL_new1248z00_3009)))->BgL_itemzd2typezd2) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BgL_itemzd2type1136zd2_2788)), BUNSPEC);
					{
						BgL_siza7edzd2switchz75_bglt BgL_auxz00_3737;

						{
							obj_t BgL_auxz00_3738;

							{	/* Inline/size.scm 24 */
								BgL_objectz00_bglt BgL_tmpz00_3739;

								BgL_tmpz00_3739 = ((BgL_objectz00_bglt) BgL_new1248z00_3009);
								BgL_auxz00_3738 = BGL_OBJECT_WIDENING(BgL_tmpz00_3739);
							}
							BgL_auxz00_3737 =
								((BgL_siza7edzd2switchz75_bglt) BgL_auxz00_3738);
						}
						((((BgL_siza7edzd2switchz75_bglt) COBJECT(BgL_auxz00_3737))->
								BgL_siza7eza7) = ((long) BgL_siza7e1137za7_3008), BUNSPEC);
					}
					return BgL_new1248z00_3009;
				}
			}
		}

	}



/* &lambda1707 */
	obj_t BGl_z62lambda1707z62zzinline_siza7eza7(obj_t BgL_envz00_2790,
		obj_t BgL_oz00_2791, obj_t BgL_vz00_2792)
	{
		{	/* Inline/size.scm 24 */
			{	/* Inline/size.scm 24 */
				long BgL_vz00_3016;

				BgL_vz00_3016 = (long) CINT(BgL_vz00_2792);
				{
					BgL_siza7edzd2switchz75_bglt BgL_auxz00_3745;

					{
						obj_t BgL_auxz00_3746;

						{	/* Inline/size.scm 24 */
							BgL_objectz00_bglt BgL_tmpz00_3747;

							BgL_tmpz00_3747 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_2791));
							BgL_auxz00_3746 = BGL_OBJECT_WIDENING(BgL_tmpz00_3747);
						}
						BgL_auxz00_3745 = ((BgL_siza7edzd2switchz75_bglt) BgL_auxz00_3746);
					}
					return
						((((BgL_siza7edzd2switchz75_bglt) COBJECT(BgL_auxz00_3745))->
							BgL_siza7eza7) = ((long) BgL_vz00_3016), BUNSPEC);
		}}}

	}



/* &lambda1706 */
	obj_t BGl_z62lambda1706z62zzinline_siza7eza7(obj_t BgL_envz00_2793,
		obj_t BgL_oz00_2794)
	{
		{	/* Inline/size.scm 24 */
			{	/* Inline/size.scm 24 */
				long BgL_tmpz00_3753;

				{
					BgL_siza7edzd2switchz75_bglt BgL_auxz00_3754;

					{
						obj_t BgL_auxz00_3755;

						{	/* Inline/size.scm 24 */
							BgL_objectz00_bglt BgL_tmpz00_3756;

							BgL_tmpz00_3756 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_oz00_2794));
							BgL_auxz00_3755 = BGL_OBJECT_WIDENING(BgL_tmpz00_3756);
						}
						BgL_auxz00_3754 = ((BgL_siza7edzd2switchz75_bglt) BgL_auxz00_3755);
					}
					BgL_tmpz00_3753 =
						(((BgL_siza7edzd2switchz75_bglt) COBJECT(BgL_auxz00_3754))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_3753);
			}
		}

	}



/* &lambda1627 */
	BgL_syncz00_bglt BGl_z62lambda1627z62zzinline_siza7eza7(obj_t BgL_envz00_2795,
		obj_t BgL_o1128z00_2796)
	{
		{	/* Inline/size.scm 23 */
			{	/* Inline/size.scm 23 */
				long BgL_arg1629z00_3019;

				{	/* Inline/size.scm 23 */
					obj_t BgL_arg1630z00_3020;

					{	/* Inline/size.scm 23 */
						obj_t BgL_arg1642z00_3021;

						{	/* Inline/size.scm 23 */
							obj_t BgL_arg1815z00_3022;
							long BgL_arg1816z00_3023;

							BgL_arg1815z00_3022 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Inline/size.scm 23 */
								long BgL_arg1817z00_3024;

								BgL_arg1817z00_3024 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_syncz00_bglt) BgL_o1128z00_2796)));
								BgL_arg1816z00_3023 = (BgL_arg1817z00_3024 - OBJECT_TYPE);
							}
							BgL_arg1642z00_3021 =
								VECTOR_REF(BgL_arg1815z00_3022, BgL_arg1816z00_3023);
						}
						BgL_arg1630z00_3020 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1642z00_3021);
					}
					{	/* Inline/size.scm 23 */
						obj_t BgL_tmpz00_3770;

						BgL_tmpz00_3770 = ((obj_t) BgL_arg1630z00_3020);
						BgL_arg1629z00_3019 = BGL_CLASS_NUM(BgL_tmpz00_3770);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_syncz00_bglt) BgL_o1128z00_2796)), BgL_arg1629z00_3019);
			}
			{	/* Inline/size.scm 23 */
				BgL_objectz00_bglt BgL_tmpz00_3776;

				BgL_tmpz00_3776 =
					((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_o1128z00_2796));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3776, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_o1128z00_2796));
			return ((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_o1128z00_2796));
		}

	}



/* &<@anonymous:1626> */
	obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzinline_siza7eza7(obj_t
		BgL_envz00_2797, obj_t BgL_new1127z00_2798)
	{
		{	/* Inline/size.scm 23 */
			{
				BgL_syncz00_bglt BgL_auxz00_3784;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_new1127z00_2798))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3788;

					{	/* Inline/size.scm 23 */
						obj_t BgL_classz00_3026;

						BgL_classz00_3026 = BGl_typez00zztype_typez00;
						{	/* Inline/size.scm 23 */
							obj_t BgL__ortest_1117z00_3027;

							BgL__ortest_1117z00_3027 = BGL_CLASS_NIL(BgL_classz00_3026);
							if (CBOOL(BgL__ortest_1117z00_3027))
								{	/* Inline/size.scm 23 */
									BgL_auxz00_3788 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3027);
								}
							else
								{	/* Inline/size.scm 23 */
									BgL_auxz00_3788 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3026));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_syncz00_bglt) BgL_new1127z00_2798))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_3788), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_3798;

					{	/* Inline/size.scm 23 */
						obj_t BgL_classz00_3028;

						BgL_classz00_3028 = BGl_nodez00zzast_nodez00;
						{	/* Inline/size.scm 23 */
							obj_t BgL__ortest_1117z00_3029;

							BgL__ortest_1117z00_3029 = BGL_CLASS_NIL(BgL_classz00_3028);
							if (CBOOL(BgL__ortest_1117z00_3029))
								{	/* Inline/size.scm 23 */
									BgL_auxz00_3798 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_3029);
								}
							else
								{	/* Inline/size.scm 23 */
									BgL_auxz00_3798 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3028));
								}
						}
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt)
										((BgL_syncz00_bglt) BgL_new1127z00_2798))))->BgL_mutexz00) =
						((BgL_nodez00_bglt) BgL_auxz00_3798), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_3808;

					{	/* Inline/size.scm 23 */
						obj_t BgL_classz00_3030;

						BgL_classz00_3030 = BGl_nodez00zzast_nodez00;
						{	/* Inline/size.scm 23 */
							obj_t BgL__ortest_1117z00_3031;

							BgL__ortest_1117z00_3031 = BGL_CLASS_NIL(BgL_classz00_3030);
							if (CBOOL(BgL__ortest_1117z00_3031))
								{	/* Inline/size.scm 23 */
									BgL_auxz00_3808 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_3031);
								}
							else
								{	/* Inline/size.scm 23 */
									BgL_auxz00_3808 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3030));
								}
						}
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt)
										((BgL_syncz00_bglt) BgL_new1127z00_2798))))->
							BgL_prelockz00) = ((BgL_nodez00_bglt) BgL_auxz00_3808), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_3818;

					{	/* Inline/size.scm 23 */
						obj_t BgL_classz00_3032;

						BgL_classz00_3032 = BGl_nodez00zzast_nodez00;
						{	/* Inline/size.scm 23 */
							obj_t BgL__ortest_1117z00_3033;

							BgL__ortest_1117z00_3033 = BGL_CLASS_NIL(BgL_classz00_3032);
							if (CBOOL(BgL__ortest_1117z00_3033))
								{	/* Inline/size.scm 23 */
									BgL_auxz00_3818 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_3033);
								}
							else
								{	/* Inline/size.scm 23 */
									BgL_auxz00_3818 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3032));
								}
						}
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt)
										((BgL_syncz00_bglt) BgL_new1127z00_2798))))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_3818), BUNSPEC);
				}
				{
					BgL_siza7edzd2syncz75_bglt BgL_auxz00_3828;

					{
						obj_t BgL_auxz00_3829;

						{	/* Inline/size.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_3830;

							BgL_tmpz00_3830 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_new1127z00_2798));
							BgL_auxz00_3829 = BGL_OBJECT_WIDENING(BgL_tmpz00_3830);
						}
						BgL_auxz00_3828 = ((BgL_siza7edzd2syncz75_bglt) BgL_auxz00_3829);
					}
					((((BgL_siza7edzd2syncz75_bglt) COBJECT(BgL_auxz00_3828))->
							BgL_siza7eza7) = ((long) 0L), BUNSPEC);
				}
				BgL_auxz00_3784 = ((BgL_syncz00_bglt) BgL_new1127z00_2798);
				return ((obj_t) BgL_auxz00_3784);
			}
		}

	}



/* &lambda1617 */
	BgL_syncz00_bglt BGl_z62lambda1617z62zzinline_siza7eza7(obj_t BgL_envz00_2799,
		obj_t BgL_o1123z00_2800)
	{
		{	/* Inline/size.scm 23 */
			{	/* Inline/size.scm 23 */
				BgL_siza7edzd2syncz75_bglt BgL_wide1125z00_3035;

				BgL_wide1125z00_3035 =
					((BgL_siza7edzd2syncz75_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_siza7edzd2syncz75_bgl))));
				{	/* Inline/size.scm 23 */
					obj_t BgL_auxz00_3843;
					BgL_objectz00_bglt BgL_tmpz00_3839;

					BgL_auxz00_3843 = ((obj_t) BgL_wide1125z00_3035);
					BgL_tmpz00_3839 =
						((BgL_objectz00_bglt)
						((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_o1123z00_2800)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3839, BgL_auxz00_3843);
				}
				((BgL_objectz00_bglt)
					((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_o1123z00_2800)));
				{	/* Inline/size.scm 23 */
					long BgL_arg1625z00_3036;

					BgL_arg1625z00_3036 =
						BGL_CLASS_NUM(BGl_siza7edzd2syncz75zzinline_siza7eza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_syncz00_bglt)
								((BgL_syncz00_bglt) BgL_o1123z00_2800))), BgL_arg1625z00_3036);
				}
				return
					((BgL_syncz00_bglt)
					((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_o1123z00_2800)));
			}
		}

	}



/* &lambda1614 */
	BgL_syncz00_bglt BGl_z62lambda1614z62zzinline_siza7eza7(obj_t BgL_envz00_2801,
		obj_t BgL_loc1117z00_2802, obj_t BgL_type1118z00_2803,
		obj_t BgL_mutex1119z00_2804, obj_t BgL_prelock1120z00_2805,
		obj_t BgL_body1121z00_2806, obj_t BgL_siza7e1122za7_2807)
	{
		{	/* Inline/size.scm 23 */
			{	/* Inline/size.scm 23 */
				long BgL_siza7e1122za7_3041;

				BgL_siza7e1122za7_3041 = (long) CINT(BgL_siza7e1122za7_2807);
				{	/* Inline/size.scm 23 */
					BgL_syncz00_bglt BgL_new1243z00_3042;

					{	/* Inline/size.scm 23 */
						BgL_syncz00_bglt BgL_tmp1241z00_3043;
						BgL_siza7edzd2syncz75_bglt BgL_wide1242z00_3044;

						{
							BgL_syncz00_bglt BgL_auxz00_3858;

							{	/* Inline/size.scm 23 */
								BgL_syncz00_bglt BgL_new1240z00_3045;

								BgL_new1240z00_3045 =
									((BgL_syncz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_syncz00_bgl))));
								{	/* Inline/size.scm 23 */
									long BgL_arg1616z00_3046;

									BgL_arg1616z00_3046 = BGL_CLASS_NUM(BGl_syncz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1240z00_3045),
										BgL_arg1616z00_3046);
								}
								{	/* Inline/size.scm 23 */
									BgL_objectz00_bglt BgL_tmpz00_3863;

									BgL_tmpz00_3863 = ((BgL_objectz00_bglt) BgL_new1240z00_3045);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3863, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1240z00_3045);
								BgL_auxz00_3858 = BgL_new1240z00_3045;
							}
							BgL_tmp1241z00_3043 = ((BgL_syncz00_bglt) BgL_auxz00_3858);
						}
						BgL_wide1242z00_3044 =
							((BgL_siza7edzd2syncz75_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_siza7edzd2syncz75_bgl))));
						{	/* Inline/size.scm 23 */
							obj_t BgL_auxz00_3871;
							BgL_objectz00_bglt BgL_tmpz00_3869;

							BgL_auxz00_3871 = ((obj_t) BgL_wide1242z00_3044);
							BgL_tmpz00_3869 = ((BgL_objectz00_bglt) BgL_tmp1241z00_3043);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3869, BgL_auxz00_3871);
						}
						((BgL_objectz00_bglt) BgL_tmp1241z00_3043);
						{	/* Inline/size.scm 23 */
							long BgL_arg1615z00_3047;

							BgL_arg1615z00_3047 =
								BGL_CLASS_NUM(BGl_siza7edzd2syncz75zzinline_siza7eza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1241z00_3043),
								BgL_arg1615z00_3047);
						}
						BgL_new1243z00_3042 = ((BgL_syncz00_bglt) BgL_tmp1241z00_3043);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1243z00_3042)))->BgL_locz00) =
						((obj_t) BgL_loc1117z00_2802), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1243z00_3042)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1118z00_2803)),
						BUNSPEC);
					((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt)
										BgL_new1243z00_3042)))->BgL_mutexz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_mutex1119z00_2804)),
						BUNSPEC);
					((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt)
										BgL_new1243z00_3042)))->BgL_prelockz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_prelock1120z00_2805)),
						BUNSPEC);
					((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt)
										BgL_new1243z00_3042)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1121z00_2806)),
						BUNSPEC);
					{
						BgL_siza7edzd2syncz75_bglt BgL_auxz00_3893;

						{
							obj_t BgL_auxz00_3894;

							{	/* Inline/size.scm 23 */
								BgL_objectz00_bglt BgL_tmpz00_3895;

								BgL_tmpz00_3895 = ((BgL_objectz00_bglt) BgL_new1243z00_3042);
								BgL_auxz00_3894 = BGL_OBJECT_WIDENING(BgL_tmpz00_3895);
							}
							BgL_auxz00_3893 = ((BgL_siza7edzd2syncz75_bglt) BgL_auxz00_3894);
						}
						((((BgL_siza7edzd2syncz75_bglt) COBJECT(BgL_auxz00_3893))->
								BgL_siza7eza7) = ((long) BgL_siza7e1122za7_3041), BUNSPEC);
					}
					return BgL_new1243z00_3042;
				}
			}
		}

	}



/* &lambda1653 */
	obj_t BGl_z62lambda1653z62zzinline_siza7eza7(obj_t BgL_envz00_2808,
		obj_t BgL_oz00_2809, obj_t BgL_vz00_2810)
	{
		{	/* Inline/size.scm 23 */
			{	/* Inline/size.scm 23 */
				long BgL_vz00_3049;

				BgL_vz00_3049 = (long) CINT(BgL_vz00_2810);
				{
					BgL_siza7edzd2syncz75_bglt BgL_auxz00_3901;

					{
						obj_t BgL_auxz00_3902;

						{	/* Inline/size.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_3903;

							BgL_tmpz00_3903 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_2809));
							BgL_auxz00_3902 = BGL_OBJECT_WIDENING(BgL_tmpz00_3903);
						}
						BgL_auxz00_3901 = ((BgL_siza7edzd2syncz75_bglt) BgL_auxz00_3902);
					}
					return
						((((BgL_siza7edzd2syncz75_bglt) COBJECT(BgL_auxz00_3901))->
							BgL_siza7eza7) = ((long) BgL_vz00_3049), BUNSPEC);
		}}}

	}



/* &lambda1652 */
	obj_t BGl_z62lambda1652z62zzinline_siza7eza7(obj_t BgL_envz00_2811,
		obj_t BgL_oz00_2812)
	{
		{	/* Inline/size.scm 23 */
			{	/* Inline/size.scm 23 */
				long BgL_tmpz00_3909;

				{
					BgL_siza7edzd2syncz75_bglt BgL_auxz00_3910;

					{
						obj_t BgL_auxz00_3911;

						{	/* Inline/size.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_3912;

							BgL_tmpz00_3912 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_oz00_2812));
							BgL_auxz00_3911 = BGL_OBJECT_WIDENING(BgL_tmpz00_3912);
						}
						BgL_auxz00_3910 = ((BgL_siza7edzd2syncz75_bglt) BgL_auxz00_3911);
					}
					BgL_tmpz00_3909 =
						(((BgL_siza7edzd2syncz75_bglt) COBJECT(BgL_auxz00_3910))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_3909);
			}
		}

	}



/* &lambda1577 */
	BgL_sequencez00_bglt BGl_z62lambda1577z62zzinline_siza7eza7(obj_t
		BgL_envz00_2813, obj_t BgL_o1115z00_2814)
	{
		{	/* Inline/size.scm 22 */
			{	/* Inline/size.scm 22 */
				long BgL_arg1584z00_3052;

				{	/* Inline/size.scm 22 */
					obj_t BgL_arg1585z00_3053;

					{	/* Inline/size.scm 22 */
						obj_t BgL_arg1589z00_3054;

						{	/* Inline/size.scm 22 */
							obj_t BgL_arg1815z00_3055;
							long BgL_arg1816z00_3056;

							BgL_arg1815z00_3055 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Inline/size.scm 22 */
								long BgL_arg1817z00_3057;

								BgL_arg1817z00_3057 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt) BgL_o1115z00_2814)));
								BgL_arg1816z00_3056 = (BgL_arg1817z00_3057 - OBJECT_TYPE);
							}
							BgL_arg1589z00_3054 =
								VECTOR_REF(BgL_arg1815z00_3055, BgL_arg1816z00_3056);
						}
						BgL_arg1585z00_3053 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1589z00_3054);
					}
					{	/* Inline/size.scm 22 */
						obj_t BgL_tmpz00_3926;

						BgL_tmpz00_3926 = ((obj_t) BgL_arg1585z00_3053);
						BgL_arg1584z00_3052 = BGL_CLASS_NUM(BgL_tmpz00_3926);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sequencez00_bglt) BgL_o1115z00_2814)), BgL_arg1584z00_3052);
			}
			{	/* Inline/size.scm 22 */
				BgL_objectz00_bglt BgL_tmpz00_3932;

				BgL_tmpz00_3932 =
					((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_o1115z00_2814));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3932, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_o1115z00_2814));
			return
				((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt) BgL_o1115z00_2814));
		}

	}



/* &<@anonymous:1576> */
	obj_t BGl_z62zc3z04anonymousza31576ze3ze5zzinline_siza7eza7(obj_t
		BgL_envz00_2815, obj_t BgL_new1114z00_2816)
	{
		{	/* Inline/size.scm 22 */
			{
				BgL_sequencez00_bglt BgL_auxz00_3940;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_sequencez00_bglt) BgL_new1114z00_2816))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3944;

					{	/* Inline/size.scm 22 */
						obj_t BgL_classz00_3059;

						BgL_classz00_3059 = BGl_typez00zztype_typez00;
						{	/* Inline/size.scm 22 */
							obj_t BgL__ortest_1117z00_3060;

							BgL__ortest_1117z00_3060 = BGL_CLASS_NIL(BgL_classz00_3059);
							if (CBOOL(BgL__ortest_1117z00_3060))
								{	/* Inline/size.scm 22 */
									BgL_auxz00_3944 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3060);
								}
							else
								{	/* Inline/size.scm 22 */
									BgL_auxz00_3944 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3059));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_sequencez00_bglt) BgL_new1114z00_2816))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_3944), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt)
									((BgL_sequencez00_bglt) BgL_new1114z00_2816))))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt)
							COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_sequencez00_bglt)
										BgL_new1114z00_2816))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sequencez00_bglt)
							COBJECT(((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt)
										BgL_new1114z00_2816))))->BgL_nodesz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_sequencez00_bglt)
							COBJECT(((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt)
										BgL_new1114z00_2816))))->BgL_unsafez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_sequencez00_bglt)
							COBJECT(((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt)
										BgL_new1114z00_2816))))->BgL_metaz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_siza7edzd2sequencez75_bglt BgL_auxz00_3969;

					{
						obj_t BgL_auxz00_3970;

						{	/* Inline/size.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_3971;

							BgL_tmpz00_3971 =
								((BgL_objectz00_bglt)
								((BgL_sequencez00_bglt) BgL_new1114z00_2816));
							BgL_auxz00_3970 = BGL_OBJECT_WIDENING(BgL_tmpz00_3971);
						}
						BgL_auxz00_3969 =
							((BgL_siza7edzd2sequencez75_bglt) BgL_auxz00_3970);
					}
					((((BgL_siza7edzd2sequencez75_bglt) COBJECT(BgL_auxz00_3969))->
							BgL_siza7eza7) = ((long) 0L), BUNSPEC);
				}
				BgL_auxz00_3940 = ((BgL_sequencez00_bglt) BgL_new1114z00_2816);
				return ((obj_t) BgL_auxz00_3940);
			}
		}

	}



/* &lambda1574 */
	BgL_sequencez00_bglt BGl_z62lambda1574z62zzinline_siza7eza7(obj_t
		BgL_envz00_2817, obj_t BgL_o1111z00_2818)
	{
		{	/* Inline/size.scm 22 */
			{	/* Inline/size.scm 22 */
				BgL_siza7edzd2sequencez75_bglt BgL_wide1113z00_3062;

				BgL_wide1113z00_3062 =
					((BgL_siza7edzd2sequencez75_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_siza7edzd2sequencez75_bgl))));
				{	/* Inline/size.scm 22 */
					obj_t BgL_auxz00_3984;
					BgL_objectz00_bglt BgL_tmpz00_3980;

					BgL_auxz00_3984 = ((obj_t) BgL_wide1113z00_3062);
					BgL_tmpz00_3980 =
						((BgL_objectz00_bglt)
						((BgL_sequencez00_bglt)
							((BgL_sequencez00_bglt) BgL_o1111z00_2818)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3980, BgL_auxz00_3984);
				}
				((BgL_objectz00_bglt)
					((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt) BgL_o1111z00_2818)));
				{	/* Inline/size.scm 22 */
					long BgL_arg1575z00_3063;

					BgL_arg1575z00_3063 =
						BGL_CLASS_NUM(BGl_siza7edzd2sequencez75zzinline_siza7eza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sequencez00_bglt)
								((BgL_sequencez00_bglt) BgL_o1111z00_2818))),
						BgL_arg1575z00_3063);
				}
				return
					((BgL_sequencez00_bglt)
					((BgL_sequencez00_bglt) ((BgL_sequencez00_bglt) BgL_o1111z00_2818)));
			}
		}

	}



/* &lambda1565 */
	BgL_sequencez00_bglt BGl_z62lambda1565z62zzinline_siza7eza7(obj_t
		BgL_envz00_2819, obj_t BgL_loc1103z00_2820, obj_t BgL_type1104z00_2821,
		obj_t BgL_sidezd2effect1105zd2_2822, obj_t BgL_key1106z00_2823,
		obj_t BgL_nodes1107z00_2824, obj_t BgL_unsafe1108z00_2825,
		obj_t BgL_meta1109z00_2826, obj_t BgL_siza7e1110za7_2827)
	{
		{	/* Inline/size.scm 22 */
			{	/* Inline/size.scm 22 */
				bool_t BgL_unsafe1108z00_3066;
				long BgL_siza7e1110za7_3068;

				BgL_unsafe1108z00_3066 = CBOOL(BgL_unsafe1108z00_2825);
				BgL_siza7e1110za7_3068 = (long) CINT(BgL_siza7e1110za7_2827);
				{	/* Inline/size.scm 22 */
					BgL_sequencez00_bglt BgL_new1238z00_3069;

					{	/* Inline/size.scm 22 */
						BgL_sequencez00_bglt BgL_tmp1236z00_3070;
						BgL_siza7edzd2sequencez75_bglt BgL_wide1237z00_3071;

						{
							BgL_sequencez00_bglt BgL_auxz00_4000;

							{	/* Inline/size.scm 22 */
								BgL_sequencez00_bglt BgL_new1235z00_3072;

								BgL_new1235z00_3072 =
									((BgL_sequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sequencez00_bgl))));
								{	/* Inline/size.scm 22 */
									long BgL_arg1573z00_3073;

									BgL_arg1573z00_3073 =
										BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1235z00_3072),
										BgL_arg1573z00_3073);
								}
								{	/* Inline/size.scm 22 */
									BgL_objectz00_bglt BgL_tmpz00_4005;

									BgL_tmpz00_4005 = ((BgL_objectz00_bglt) BgL_new1235z00_3072);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4005, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1235z00_3072);
								BgL_auxz00_4000 = BgL_new1235z00_3072;
							}
							BgL_tmp1236z00_3070 = ((BgL_sequencez00_bglt) BgL_auxz00_4000);
						}
						BgL_wide1237z00_3071 =
							((BgL_siza7edzd2sequencez75_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_siza7edzd2sequencez75_bgl))));
						{	/* Inline/size.scm 22 */
							obj_t BgL_auxz00_4013;
							BgL_objectz00_bglt BgL_tmpz00_4011;

							BgL_auxz00_4013 = ((obj_t) BgL_wide1237z00_3071);
							BgL_tmpz00_4011 = ((BgL_objectz00_bglt) BgL_tmp1236z00_3070);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4011, BgL_auxz00_4013);
						}
						((BgL_objectz00_bglt) BgL_tmp1236z00_3070);
						{	/* Inline/size.scm 22 */
							long BgL_arg1571z00_3074;

							BgL_arg1571z00_3074 =
								BGL_CLASS_NUM(BGl_siza7edzd2sequencez75zzinline_siza7eza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1236z00_3070),
								BgL_arg1571z00_3074);
						}
						BgL_new1238z00_3069 = ((BgL_sequencez00_bglt) BgL_tmp1236z00_3070);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1238z00_3069)))->BgL_locz00) =
						((obj_t) BgL_loc1103z00_2820), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1238z00_3069)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1104z00_2821)),
						BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1238z00_3069)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1105zd2_2822), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1238z00_3069)))->BgL_keyz00) =
						((obj_t) BgL_key1106z00_2823), BUNSPEC);
					((((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_new1238z00_3069)))->BgL_nodesz00) =
						((obj_t) ((obj_t) BgL_nodes1107z00_2824)), BUNSPEC);
					((((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_new1238z00_3069)))->BgL_unsafez00) =
						((bool_t) BgL_unsafe1108z00_3066), BUNSPEC);
					((((BgL_sequencez00_bglt) COBJECT(((BgL_sequencez00_bglt)
										BgL_new1238z00_3069)))->BgL_metaz00) =
						((obj_t) ((obj_t) BgL_meta1109z00_2826)), BUNSPEC);
					{
						BgL_siza7edzd2sequencez75_bglt BgL_auxz00_4038;

						{
							obj_t BgL_auxz00_4039;

							{	/* Inline/size.scm 22 */
								BgL_objectz00_bglt BgL_tmpz00_4040;

								BgL_tmpz00_4040 = ((BgL_objectz00_bglt) BgL_new1238z00_3069);
								BgL_auxz00_4039 = BGL_OBJECT_WIDENING(BgL_tmpz00_4040);
							}
							BgL_auxz00_4038 =
								((BgL_siza7edzd2sequencez75_bglt) BgL_auxz00_4039);
						}
						((((BgL_siza7edzd2sequencez75_bglt) COBJECT(BgL_auxz00_4038))->
								BgL_siza7eza7) = ((long) BgL_siza7e1110za7_3068), BUNSPEC);
					}
					return BgL_new1238z00_3069;
				}
			}
		}

	}



/* &lambda1596 */
	obj_t BGl_z62lambda1596z62zzinline_siza7eza7(obj_t BgL_envz00_2828,
		obj_t BgL_oz00_2829, obj_t BgL_vz00_2830)
	{
		{	/* Inline/size.scm 22 */
			{	/* Inline/size.scm 22 */
				long BgL_vz00_3076;

				BgL_vz00_3076 = (long) CINT(BgL_vz00_2830);
				{
					BgL_siza7edzd2sequencez75_bglt BgL_auxz00_4046;

					{
						obj_t BgL_auxz00_4047;

						{	/* Inline/size.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_4048;

							BgL_tmpz00_4048 =
								((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_2829));
							BgL_auxz00_4047 = BGL_OBJECT_WIDENING(BgL_tmpz00_4048);
						}
						BgL_auxz00_4046 =
							((BgL_siza7edzd2sequencez75_bglt) BgL_auxz00_4047);
					}
					return
						((((BgL_siza7edzd2sequencez75_bglt) COBJECT(BgL_auxz00_4046))->
							BgL_siza7eza7) = ((long) BgL_vz00_3076), BUNSPEC);
		}}}

	}



/* &lambda1595 */
	obj_t BGl_z62lambda1595z62zzinline_siza7eza7(obj_t BgL_envz00_2831,
		obj_t BgL_oz00_2832)
	{
		{	/* Inline/size.scm 22 */
			{	/* Inline/size.scm 22 */
				long BgL_tmpz00_4054;

				{
					BgL_siza7edzd2sequencez75_bglt BgL_auxz00_4055;

					{
						obj_t BgL_auxz00_4056;

						{	/* Inline/size.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_4057;

							BgL_tmpz00_4057 =
								((BgL_objectz00_bglt) ((BgL_sequencez00_bglt) BgL_oz00_2832));
							BgL_auxz00_4056 = BGL_OBJECT_WIDENING(BgL_tmpz00_4057);
						}
						BgL_auxz00_4055 =
							((BgL_siza7edzd2sequencez75_bglt) BgL_auxz00_4056);
					}
					BgL_tmpz00_4054 =
						(((BgL_siza7edzd2sequencez75_bglt) COBJECT(BgL_auxz00_4055))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_4054);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinline_siza7eza7(void)
	{
		{	/* Inline/size.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_proc2049z00zzinline_siza7eza7, BGl_nodez00zzast_nodez00,
				BGl_string2050z00zzinline_siza7eza7);
		}

	}



/* &node-size1399 */
	obj_t BGl_z62nodezd2siza7e1399z17zzinline_siza7eza7(obj_t BgL_envz00_2834,
		obj_t BgL_nodez00_2835)
	{
		{	/* Inline/size.scm 33 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(8),
				BGl_string2051z00zzinline_siza7eza7,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2835)));
		}

	}



/* node-size */
	BGL_EXPORTED_DEF long BGl_nodezd2siza7ez75zzinline_siza7eza7(BgL_nodez00_bglt
		BgL_nodez00_152)
	{
		{	/* Inline/size.scm 33 */
			{	/* Inline/size.scm 33 */
				obj_t BgL_method1401z00_1811;

				{	/* Inline/size.scm 33 */
					obj_t BgL_res2018z00_2506;

					{	/* Inline/size.scm 33 */
						long BgL_objzd2classzd2numz00_2477;

						BgL_objzd2classzd2numz00_2477 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_152));
						{	/* Inline/size.scm 33 */
							obj_t BgL_arg1811z00_2478;

							BgL_arg1811z00_2478 =
								PROCEDURE_REF(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
								(int) (1L));
							{	/* Inline/size.scm 33 */
								int BgL_offsetz00_2481;

								BgL_offsetz00_2481 = (int) (BgL_objzd2classzd2numz00_2477);
								{	/* Inline/size.scm 33 */
									long BgL_offsetz00_2482;

									BgL_offsetz00_2482 =
										((long) (BgL_offsetz00_2481) - OBJECT_TYPE);
									{	/* Inline/size.scm 33 */
										long BgL_modz00_2483;

										BgL_modz00_2483 =
											(BgL_offsetz00_2482 >> (int) ((long) ((int) (4L))));
										{	/* Inline/size.scm 33 */
											long BgL_restz00_2485;

											BgL_restz00_2485 =
												(BgL_offsetz00_2482 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Inline/size.scm 33 */

												{	/* Inline/size.scm 33 */
													obj_t BgL_bucketz00_2487;

													BgL_bucketz00_2487 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2478), BgL_modz00_2483);
													BgL_res2018z00_2506 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2487), BgL_restz00_2485);
					}}}}}}}}
					BgL_method1401z00_1811 = BgL_res2018z00_2506;
				}
				return
					(long) CINT(BGL_PROCEDURE_CALL1(BgL_method1401z00_1811,
						((obj_t) BgL_nodez00_152)));
		}}

	}



/* &node-size */
	obj_t BGl_z62nodezd2siza7ez17zzinline_siza7eza7(obj_t BgL_envz00_2836,
		obj_t BgL_nodez00_2837)
	{
		{	/* Inline/size.scm 33 */
			return
				BINT(BGl_nodezd2siza7ez75zzinline_siza7eza7(
					((BgL_nodez00_bglt) BgL_nodez00_2837)));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinline_siza7eza7(void)
	{
		{	/* Inline/size.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7, BGl_atomz00zzast_nodez00,
				BGl_proc2052z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7, BGl_varz00zzast_nodez00,
				BGl_proc2054z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_kwotez00zzast_nodez00, BGl_proc2055z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_sequencez00zzast_nodez00, BGl_proc2056z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_siza7edzd2sequencez75zzinline_siza7eza7,
				BGl_proc2057z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7, BGl_syncz00zzast_nodez00,
				BGl_proc2058z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_siza7edzd2syncz75zzinline_siza7eza7,
				BGl_proc2059z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7, BGl_appz00zzast_nodez00,
				BGl_proc2060z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2061z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_funcallz00zzast_nodez00, BGl_proc2062z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7, BGl_castz00zzast_nodez00,
				BGl_proc2063z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_externz00zzast_nodez00, BGl_proc2064z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7, BGl_setqz00zzast_nodez00,
				BGl_proc2065z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_conditionalz00zzast_nodez00, BGl_proc2066z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7, BGl_failz00zzast_nodez00,
				BGl_proc2067z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_switchz00zzast_nodez00, BGl_proc2068z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_siza7edzd2switchz75zzinline_siza7eza7,
				BGl_proc2069z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2070z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_siza7edzd2letzd2funza7zzinline_siza7eza7,
				BGl_proc2071z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2072z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_siza7edzd2letzd2varza7zzinline_siza7eza7,
				BGl_proc2073z00zzinline_siza7eza7, BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2074z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2075z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_retblockz00zzast_nodez00, BGl_proc2076z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_returnz00zzast_nodez00, BGl_proc2077z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2078z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2079z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2siza7ezd2envza7zzinline_siza7eza7,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2080z00zzinline_siza7eza7,
				BGl_string2053z00zzinline_siza7eza7);
		}

	}



/* &node-size-box-set!1462 */
	obj_t BGl_z62nodezd2siza7ezd2boxzd2setz121462z05zzinline_siza7eza7(obj_t
		BgL_envz00_2866, obj_t BgL_nodez00_2867)
	{
		{	/* Inline/size.scm 272 */
			return
				BINT(
				(2L +
					BGl_nodezd2siza7ez75zzinline_siza7eza7(
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2867)))->
							BgL_valuez00))));
		}

	}



/* &node-size-box-ref1460 */
	obj_t BGl_z62nodezd2siza7ezd2boxzd2ref1460z17zzinline_siza7eza7(obj_t
		BgL_envz00_2868, obj_t BgL_nodez00_2869)
	{
		{	/* Inline/size.scm 266 */
			return BINT(2L);
		}

	}



/* &node-size-make-box1458 */
	obj_t BGl_z62nodezd2siza7ezd2makezd2box1458z17zzinline_siza7eza7(obj_t
		BgL_envz00_2870, obj_t BgL_nodez00_2871)
	{
		{	/* Inline/size.scm 260 */
			return
				BINT(
				(1L +
					BGl_nodezd2siza7ez75zzinline_siza7eza7(
						(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_2871)))->
							BgL_valuez00))));
		}

	}



/* &node-size-return1456 */
	obj_t BGl_z62nodezd2siza7ezd2return1456zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2872, obj_t BgL_nodez00_2873)
	{
		{	/* Inline/size.scm 253 */
			return
				BINT(BGl_nodezd2siza7ez75zzinline_siza7eza7(
					(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_2873)))->BgL_valuez00)));
		}

	}



/* &node-size-retblock1454 */
	obj_t BGl_z62nodezd2siza7ezd2retblock1454zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2874, obj_t BgL_nodez00_2875)
	{
		{	/* Inline/size.scm 246 */
			return
				BINT(BGl_nodezd2siza7ez75zzinline_siza7eza7(
					(((BgL_retblockz00_bglt) COBJECT(
								((BgL_retblockz00_bglt) BgL_nodez00_2875)))->BgL_bodyz00)));
		}

	}



/* &node-size-jump-ex-it1452 */
	obj_t BGl_z62nodezd2siza7ezd2jumpzd2exzd2it1452zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2876, obj_t BgL_nodez00_2877)
	{
		{	/* Inline/size.scm 238 */
			return
				BINT(
				(1L +
					(BGl_nodezd2siza7ez75zzinline_siza7eza7(
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
										((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2877)))->
								BgL_exitz00)) +
						BGl_nodezd2siza7ez75zzinline_siza7eza7(((
									(BgL_jumpzd2exzd2itz00_bglt)
									COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2877)))->
								BgL_valuez00)))));
		}

	}



/* &node-size-set-ex-it1450 */
	obj_t BGl_z62nodezd2siza7ezd2setzd2exzd2it1450zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2878, obj_t BgL_nodez00_2879)
	{
		{	/* Inline/size.scm 230 */
			return
				BINT(
				(1L +
					(BGl_nodezd2siza7ez75zzinline_siza7eza7(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2879)))->
								BgL_bodyz00)) +
						BGl_nodezd2siza7ez75zzinline_siza7eza7((((BgL_setzd2exzd2itz00_bglt)
									COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2879)))->
								BgL_onexitz00)))));
		}

	}



/* &node-size-sized-let-1448 */
	obj_t BGl_z62nodezd2siza7ezd2siza7edzd2letzd21448z62zzinline_siza7eza7(obj_t
		BgL_envz00_2880, obj_t BgL_nodez00_2881)
	{
		{	/* Inline/size.scm 224 */
			{	/* Inline/size.scm 225 */
				long BgL_tmpz00_4168;

				{
					BgL_siza7edzd2letzd2varza7_bglt BgL_auxz00_4169;

					{
						obj_t BgL_auxz00_4170;

						{	/* Inline/size.scm 225 */
							BgL_objectz00_bglt BgL_tmpz00_4171;

							BgL_tmpz00_4171 =
								((BgL_objectz00_bglt)
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2881));
							BgL_auxz00_4170 = BGL_OBJECT_WIDENING(BgL_tmpz00_4171);
						}
						BgL_auxz00_4169 =
							((BgL_siza7edzd2letzd2varza7_bglt) BgL_auxz00_4170);
					}
					BgL_tmpz00_4168 =
						(((BgL_siza7edzd2letzd2varza7_bglt) COBJECT(BgL_auxz00_4169))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_4168);
			}
		}

	}



/* &node-size-let-var1446 */
	obj_t BGl_z62nodezd2siza7ezd2letzd2var1446z17zzinline_siza7eza7(obj_t
		BgL_envz00_2882, obj_t BgL_nodez00_2883)
	{
		{	/* Inline/size.scm 211 */
			{	/* Inline/size.scm 212 */
				long BgL_tmpz00_4178;

				{	/* Inline/size.scm 212 */
					obj_t BgL_g1227z00_3088;
					long BgL_g1228z00_3089;

					BgL_g1227z00_3088 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2883)))->BgL_bindingsz00);
					BgL_g1228z00_3089 =
						BGl_nodezd2siza7ez75zzinline_siza7eza7(
						(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2883)))->BgL_bodyz00));
					{
						obj_t BgL_bindingsz00_3091;
						long BgL_siza7eza7_3092;

						BgL_bindingsz00_3091 = BgL_g1227z00_3088;
						BgL_siza7eza7_3092 = BgL_g1228z00_3089;
					BgL_loopz00_3090:
						if (NULLP(BgL_bindingsz00_3091))
							{	/* Inline/size.scm 214 */
								{	/* Inline/size.scm 216 */
									BgL_siza7edzd2letzd2varza7_bglt BgL_wide1231z00_3093;

									BgL_wide1231z00_3093 =
										((BgL_siza7edzd2letzd2varza7_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_siza7edzd2letzd2varza7_bgl))));
									{	/* Inline/size.scm 216 */
										obj_t BgL_auxz00_4191;
										BgL_objectz00_bglt BgL_tmpz00_4187;

										BgL_auxz00_4191 = ((obj_t) BgL_wide1231z00_3093);
										BgL_tmpz00_4187 =
											((BgL_objectz00_bglt)
											((BgL_letzd2varzd2_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nodez00_2883)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4187, BgL_auxz00_4191);
									}
									((BgL_objectz00_bglt)
										((BgL_letzd2varzd2_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2883)));
									{	/* Inline/size.scm 216 */
										long BgL_arg1937z00_3094;

										BgL_arg1937z00_3094 =
											BGL_CLASS_NUM
											(BGl_siza7edzd2letzd2varza7zzinline_siza7eza7);
										BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
													(BgL_letzd2varzd2_bglt) ((BgL_letzd2varzd2_bglt)
														BgL_nodez00_2883))), BgL_arg1937z00_3094);
									}
									((BgL_letzd2varzd2_bglt)
										((BgL_letzd2varzd2_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2883)));
								}
								{
									BgL_siza7edzd2letzd2varza7_bglt BgL_auxz00_4205;

									{
										obj_t BgL_auxz00_4206;

										{	/* Inline/size.scm 216 */
											BgL_objectz00_bglt BgL_tmpz00_4207;

											BgL_tmpz00_4207 =
												((BgL_objectz00_bglt)
												((BgL_letzd2varzd2_bglt)
													((BgL_letzd2varzd2_bglt) BgL_nodez00_2883)));
											BgL_auxz00_4206 = BGL_OBJECT_WIDENING(BgL_tmpz00_4207);
										}
										BgL_auxz00_4205 =
											((BgL_siza7edzd2letzd2varza7_bglt) BgL_auxz00_4206);
									}
									((((BgL_siza7edzd2letzd2varza7_bglt)
												COBJECT(BgL_auxz00_4205))->BgL_siza7eza7) =
										((long) BgL_siza7eza7_3092), BUNSPEC);
								}
								((BgL_letzd2varzd2_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2883));
								BgL_tmpz00_4178 = BgL_siza7eza7_3092;
							}
						else
							{	/* Inline/size.scm 218 */
								obj_t BgL_arg1938z00_3095;
								long BgL_arg1939z00_3096;

								BgL_arg1938z00_3095 = CDR(((obj_t) BgL_bindingsz00_3091));
								{	/* Inline/size.scm 219 */
									long BgL_arg1940z00_3097;

									{	/* Inline/size.scm 219 */
										obj_t BgL_arg1941z00_3098;

										{	/* Inline/size.scm 219 */
											obj_t BgL_pairz00_3099;

											BgL_pairz00_3099 = CAR(((obj_t) BgL_bindingsz00_3091));
											BgL_arg1941z00_3098 = CDR(BgL_pairz00_3099);
										}
										BgL_arg1940z00_3097 =
											BGl_nodezd2siza7ez75zzinline_siza7eza7(
											((BgL_nodez00_bglt) BgL_arg1941z00_3098));
									}
									BgL_arg1939z00_3096 =
										(BgL_siza7eza7_3092 + BgL_arg1940z00_3097);
								}
								{
									long BgL_siza7eza7_4225;
									obj_t BgL_bindingsz00_4224;

									BgL_bindingsz00_4224 = BgL_arg1938z00_3095;
									BgL_siza7eza7_4225 = BgL_arg1939z00_3096;
									BgL_siza7eza7_3092 = BgL_siza7eza7_4225;
									BgL_bindingsz00_3091 = BgL_bindingsz00_4224;
									goto BgL_loopz00_3090;
								}
							}
					}
				}
				return BINT(BgL_tmpz00_4178);
			}
		}

	}



/* &node-size-sized-let-1444 */
	obj_t BGl_z62nodezd2siza7ezd2siza7edzd2letzd21444z62zzinline_siza7eza7(obj_t
		BgL_envz00_2884, obj_t BgL_nodez00_2885)
	{
		{	/* Inline/size.scm 205 */
			{	/* Inline/size.scm 206 */
				long BgL_tmpz00_4227;

				{
					BgL_siza7edzd2letzd2funza7_bglt BgL_auxz00_4228;

					{
						obj_t BgL_auxz00_4229;

						{	/* Inline/size.scm 206 */
							BgL_objectz00_bglt BgL_tmpz00_4230;

							BgL_tmpz00_4230 =
								((BgL_objectz00_bglt)
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2885));
							BgL_auxz00_4229 = BGL_OBJECT_WIDENING(BgL_tmpz00_4230);
						}
						BgL_auxz00_4228 =
							((BgL_siza7edzd2letzd2funza7_bglt) BgL_auxz00_4229);
					}
					BgL_tmpz00_4227 =
						(((BgL_siza7edzd2letzd2funza7_bglt) COBJECT(BgL_auxz00_4228))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_4227);
			}
		}

	}



/* &node-size-let-fun1442 */
	obj_t BGl_z62nodezd2siza7ezd2letzd2fun1442z17zzinline_siza7eza7(obj_t
		BgL_envz00_2886, obj_t BgL_nodez00_2887)
	{
		{	/* Inline/size.scm 190 */
			{	/* Inline/size.scm 191 */
				long BgL_tmpz00_4237;

				{	/* Inline/size.scm 191 */
					obj_t BgL_g1220z00_3102;
					long BgL_g1221z00_3103;

					BgL_g1220z00_3102 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2887)))->BgL_localsz00);
					BgL_g1221z00_3103 =
						(bgl_list_length(
							(((BgL_letzd2funzd2_bglt) COBJECT(
										((BgL_letzd2funzd2_bglt) BgL_nodez00_2887)))->
								BgL_localsz00)) +
						BGl_nodezd2siza7ez75zzinline_siza7eza7((((BgL_letzd2funzd2_bglt)
									COBJECT(((BgL_letzd2funzd2_bglt) BgL_nodez00_2887)))->
								BgL_bodyz00)));
					{
						obj_t BgL_localsz00_3105;
						long BgL_siza7eza7_3106;

						BgL_localsz00_3105 = BgL_g1220z00_3102;
						BgL_siza7eza7_3106 = BgL_g1221z00_3103;
					BgL_loopz00_3104:
						if (NULLP(BgL_localsz00_3105))
							{	/* Inline/size.scm 194 */
								{	/* Inline/size.scm 196 */
									BgL_siza7edzd2letzd2funza7_bglt BgL_wide1224z00_3107;

									BgL_wide1224z00_3107 =
										((BgL_siza7edzd2letzd2funza7_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_siza7edzd2letzd2funza7_bgl))));
									{	/* Inline/size.scm 196 */
										obj_t BgL_auxz00_4254;
										BgL_objectz00_bglt BgL_tmpz00_4250;

										BgL_auxz00_4254 = ((obj_t) BgL_wide1224z00_3107);
										BgL_tmpz00_4250 =
											((BgL_objectz00_bglt)
											((BgL_letzd2funzd2_bglt)
												((BgL_letzd2funzd2_bglt) BgL_nodez00_2887)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4250, BgL_auxz00_4254);
									}
									((BgL_objectz00_bglt)
										((BgL_letzd2funzd2_bglt)
											((BgL_letzd2funzd2_bglt) BgL_nodez00_2887)));
									{	/* Inline/size.scm 196 */
										long BgL_arg1923z00_3108;

										BgL_arg1923z00_3108 =
											BGL_CLASS_NUM
											(BGl_siza7edzd2letzd2funza7zzinline_siza7eza7);
										BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
													(BgL_letzd2funzd2_bglt) ((BgL_letzd2funzd2_bglt)
														BgL_nodez00_2887))), BgL_arg1923z00_3108);
									}
									((BgL_letzd2funzd2_bglt)
										((BgL_letzd2funzd2_bglt)
											((BgL_letzd2funzd2_bglt) BgL_nodez00_2887)));
								}
								{
									BgL_siza7edzd2letzd2funza7_bglt BgL_auxz00_4268;

									{
										obj_t BgL_auxz00_4269;

										{	/* Inline/size.scm 196 */
											BgL_objectz00_bglt BgL_tmpz00_4270;

											BgL_tmpz00_4270 =
												((BgL_objectz00_bglt)
												((BgL_letzd2funzd2_bglt)
													((BgL_letzd2funzd2_bglt) BgL_nodez00_2887)));
											BgL_auxz00_4269 = BGL_OBJECT_WIDENING(BgL_tmpz00_4270);
										}
										BgL_auxz00_4268 =
											((BgL_siza7edzd2letzd2funza7_bglt) BgL_auxz00_4269);
									}
									((((BgL_siza7edzd2letzd2funza7_bglt)
												COBJECT(BgL_auxz00_4268))->BgL_siza7eza7) =
										((long) BgL_siza7eza7_3106), BUNSPEC);
								}
								((BgL_letzd2funzd2_bglt)
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2887));
								BgL_tmpz00_4237 = (1L + BgL_siza7eza7_3106);
							}
						else
							{	/* Inline/size.scm 198 */
								obj_t BgL_arg1924z00_3109;
								long BgL_arg1925z00_3110;

								BgL_arg1924z00_3109 = CDR(((obj_t) BgL_localsz00_3105));
								{	/* Inline/size.scm 200 */
									long BgL_arg1926z00_3111;

									{	/* Inline/size.scm 200 */
										long BgL_arg1927z00_3112;

										{	/* Inline/size.scm 200 */
											obj_t BgL_arg1928z00_3113;

											BgL_arg1928z00_3113 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				CAR(
																					((obj_t) BgL_localsz00_3105))))))->
																BgL_valuez00))))->BgL_bodyz00);
											BgL_arg1927z00_3112 =
												BGl_nodezd2siza7ez75zzinline_siza7eza7((
													(BgL_nodez00_bglt) BgL_arg1928z00_3113));
										}
										BgL_arg1926z00_3111 = (1L + BgL_arg1927z00_3112);
									}
									BgL_arg1925z00_3110 =
										(BgL_siza7eza7_3106 + BgL_arg1926z00_3111);
								}
								{
									long BgL_siza7eza7_4294;
									obj_t BgL_localsz00_4293;

									BgL_localsz00_4293 = BgL_arg1924z00_3109;
									BgL_siza7eza7_4294 = BgL_arg1925z00_3110;
									BgL_siza7eza7_3106 = BgL_siza7eza7_4294;
									BgL_localsz00_3105 = BgL_localsz00_4293;
									goto BgL_loopz00_3104;
								}
							}
					}
				}
				return BINT(BgL_tmpz00_4237);
			}
		}

	}



/* &node-size-sized-swit1440 */
	obj_t BGl_z62nodezd2siza7ezd2siza7edzd2swit1440zb0zzinline_siza7eza7(obj_t
		BgL_envz00_2888, obj_t BgL_nodez00_2889)
	{
		{	/* Inline/size.scm 184 */
			{	/* Inline/size.scm 185 */
				long BgL_tmpz00_4296;

				{
					BgL_siza7edzd2switchz75_bglt BgL_auxz00_4297;

					{
						obj_t BgL_auxz00_4298;

						{	/* Inline/size.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_4299;

							BgL_tmpz00_4299 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2889));
							BgL_auxz00_4298 = BGL_OBJECT_WIDENING(BgL_tmpz00_4299);
						}
						BgL_auxz00_4297 = ((BgL_siza7edzd2switchz75_bglt) BgL_auxz00_4298);
					}
					BgL_tmpz00_4296 =
						(((BgL_siza7edzd2switchz75_bglt) COBJECT(BgL_auxz00_4297))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_4296);
			}
		}

	}



/* &node-size-switch1438 */
	obj_t BGl_z62nodezd2siza7ezd2switch1438zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2890, obj_t BgL_nodez00_2891)
	{
		{	/* Inline/size.scm 168 */
			{	/* Inline/size.scm 169 */
				long BgL_tmpz00_4306;

				{	/* Inline/size.scm 169 */
					obj_t BgL_g1214z00_3116;
					long BgL_g1215z00_3117;

					BgL_g1214z00_3116 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2891)))->BgL_clausesz00);
					BgL_g1215z00_3117 =
						(1L +
						BGl_nodezd2siza7ez75zzinline_siza7eza7(
							(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_2891)))->BgL_testz00)));
					{
						obj_t BgL_clausesz00_3119;
						long BgL_siza7eza7_3120;

						BgL_clausesz00_3119 = BgL_g1214z00_3116;
						BgL_siza7eza7_3120 = BgL_g1215z00_3117;
					BgL_loopz00_3118:
						if (NULLP(BgL_clausesz00_3119))
							{	/* Inline/size.scm 171 */
								{	/* Inline/size.scm 173 */
									BgL_siza7edzd2switchz75_bglt BgL_wide1218z00_3121;

									BgL_wide1218z00_3121 =
										((BgL_siza7edzd2switchz75_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_siza7edzd2switchz75_bgl))));
									{	/* Inline/size.scm 173 */
										obj_t BgL_auxz00_4320;
										BgL_objectz00_bglt BgL_tmpz00_4316;

										BgL_auxz00_4320 = ((obj_t) BgL_wide1218z00_3121);
										BgL_tmpz00_4316 =
											((BgL_objectz00_bglt)
											((BgL_switchz00_bglt)
												((BgL_switchz00_bglt) BgL_nodez00_2891)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4316, BgL_auxz00_4320);
									}
									((BgL_objectz00_bglt)
										((BgL_switchz00_bglt)
											((BgL_switchz00_bglt) BgL_nodez00_2891)));
									{	/* Inline/size.scm 173 */
										long BgL_arg1897z00_3122;

										BgL_arg1897z00_3122 =
											BGL_CLASS_NUM(BGl_siza7edzd2switchz75zzinline_siza7eza7);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_switchz00_bglt)
													((BgL_switchz00_bglt) BgL_nodez00_2891))),
											BgL_arg1897z00_3122);
									}
									((BgL_switchz00_bglt)
										((BgL_switchz00_bglt)
											((BgL_switchz00_bglt) BgL_nodez00_2891)));
								}
								{
									BgL_siza7edzd2switchz75_bglt BgL_auxz00_4334;

									{
										obj_t BgL_auxz00_4335;

										{	/* Inline/size.scm 173 */
											BgL_objectz00_bglt BgL_tmpz00_4336;

											BgL_tmpz00_4336 =
												((BgL_objectz00_bglt)
												((BgL_switchz00_bglt)
													((BgL_switchz00_bglt) BgL_nodez00_2891)));
											BgL_auxz00_4335 = BGL_OBJECT_WIDENING(BgL_tmpz00_4336);
										}
										BgL_auxz00_4334 =
											((BgL_siza7edzd2switchz75_bglt) BgL_auxz00_4335);
									}
									((((BgL_siza7edzd2switchz75_bglt) COBJECT(BgL_auxz00_4334))->
											BgL_siza7eza7) = ((long) BgL_siza7eza7_3120), BUNSPEC);
								}
								((BgL_switchz00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2891));
								BgL_tmpz00_4306 = BgL_siza7eza7_3120;
							}
						else
							{	/* Inline/size.scm 175 */
								obj_t BgL_arg1898z00_3123;
								long BgL_arg1899z00_3124;

								BgL_arg1898z00_3123 = CDR(((obj_t) BgL_clausesz00_3119));
								{	/* Inline/size.scm 176 */
									long BgL_arg1901z00_3125;

									{	/* Inline/size.scm 176 */
										long BgL_arg1902z00_3126;
										long BgL_arg1903z00_3127;

										{	/* Inline/size.scm 176 */
											bool_t BgL_test2178z00_4347;

											{	/* Inline/size.scm 176 */
												obj_t BgL_tmpz00_4348;

												{	/* Inline/size.scm 176 */
													obj_t BgL_pairz00_3128;

													BgL_pairz00_3128 = CAR(((obj_t) BgL_clausesz00_3119));
													BgL_tmpz00_4348 = CAR(BgL_pairz00_3128);
												}
												BgL_test2178z00_4347 = PAIRP(BgL_tmpz00_4348);
											}
											if (BgL_test2178z00_4347)
												{	/* Inline/size.scm 177 */
													obj_t BgL_auxz00_4353;

													{	/* Inline/size.scm 177 */
														obj_t BgL_pairz00_3129;

														BgL_pairz00_3129 =
															CAR(((obj_t) BgL_clausesz00_3119));
														BgL_auxz00_4353 = CAR(BgL_pairz00_3129);
													}
													BgL_arg1902z00_3126 =
														bgl_list_length(BgL_auxz00_4353);
												}
											else
												{	/* Inline/size.scm 176 */
													BgL_arg1902z00_3126 = 1L;
												}
										}
										{	/* Inline/size.scm 179 */
											obj_t BgL_arg1914z00_3130;

											{	/* Inline/size.scm 179 */
												obj_t BgL_pairz00_3131;

												BgL_pairz00_3131 = CAR(((obj_t) BgL_clausesz00_3119));
												BgL_arg1914z00_3130 = CDR(BgL_pairz00_3131);
											}
											BgL_arg1903z00_3127 =
												BGl_nodezd2siza7ez75zzinline_siza7eza7(
												((BgL_nodez00_bglt) BgL_arg1914z00_3130));
										}
										BgL_arg1901z00_3125 =
											(BgL_arg1902z00_3126 + BgL_arg1903z00_3127);
									}
									BgL_arg1899z00_3124 =
										(BgL_siza7eza7_3120 + BgL_arg1901z00_3125);
								}
								{
									long BgL_siza7eza7_4366;
									obj_t BgL_clausesz00_4365;

									BgL_clausesz00_4365 = BgL_arg1898z00_3123;
									BgL_siza7eza7_4366 = BgL_arg1899z00_3124;
									BgL_siza7eza7_3120 = BgL_siza7eza7_4366;
									BgL_clausesz00_3119 = BgL_clausesz00_4365;
									goto BgL_loopz00_3118;
								}
							}
					}
				}
				return BINT(BgL_tmpz00_4306);
			}
		}

	}



/* &node-size-fail1436 */
	obj_t BGl_z62nodezd2siza7ezd2fail1436zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2892, obj_t BgL_nodez00_2893)
	{
		{	/* Inline/size.scm 159 */
			{	/* Inline/size.scm 160 */
				long BgL_tmpz00_4368;

				{	/* Inline/size.scm 160 */
					long BgL_proczd2siza7ez75_3133;

					BgL_proczd2siza7ez75_3133 =
						BGl_nodezd2siza7ez75zzinline_siza7eza7(
						(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_2893)))->BgL_procz00));
					{	/* Inline/size.scm 160 */
						long BgL_msgzd2siza7ez75_3134;

						BgL_msgzd2siza7ez75_3134 =
							BGl_nodezd2siza7ez75zzinline_siza7eza7(
							(((BgL_failz00_bglt) COBJECT(
										((BgL_failz00_bglt) BgL_nodez00_2893)))->BgL_msgz00));
						{	/* Inline/size.scm 161 */
							long BgL_objzd2siza7ez75_3135;

							BgL_objzd2siza7ez75_3135 =
								BGl_nodezd2siza7ez75zzinline_siza7eza7(
								(((BgL_failz00_bglt) COBJECT(
											((BgL_failz00_bglt) BgL_nodez00_2893)))->BgL_objz00));
							{	/* Inline/size.scm 162 */

								BgL_tmpz00_4368 =
									(2L +
									(BgL_proczd2siza7ez75_3133 +
										(BgL_msgzd2siza7ez75_3134 + BgL_objzd2siza7ez75_3135)));
				}}}}
				return BINT(BgL_tmpz00_4368);
			}
		}

	}



/* &node-size-conditiona1434 */
	obj_t BGl_z62nodezd2siza7ezd2conditiona1434zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2894, obj_t BgL_nodez00_2895)
	{
		{	/* Inline/size.scm 150 */
			{	/* Inline/size.scm 151 */
				long BgL_tmpz00_4382;

				{	/* Inline/size.scm 151 */
					long BgL_testzd2siza7ez75_3137;

					BgL_testzd2siza7ez75_3137 =
						BGl_nodezd2siza7ez75zzinline_siza7eza7(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2895)))->BgL_testz00));
					{	/* Inline/size.scm 151 */
						long BgL_truezd2siza7ez75_3138;

						BgL_truezd2siza7ez75_3138 =
							BGl_nodezd2siza7ez75zzinline_siza7eza7(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_2895)))->
								BgL_truez00));
						{	/* Inline/size.scm 152 */
							long BgL_falsezd2siza7ez75_3139;

							BgL_falsezd2siza7ez75_3139 =
								BGl_nodezd2siza7ez75zzinline_siza7eza7(
								(((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_nodez00_2895)))->
									BgL_falsez00));
							{	/* Inline/size.scm 153 */

								BgL_tmpz00_4382 =
									(1L +
									(BgL_testzd2siza7ez75_3137 +
										(BgL_truezd2siza7ez75_3138 + BgL_falsezd2siza7ez75_3139)));
				}}}}
				return BINT(BgL_tmpz00_4382);
			}
		}

	}



/* &node-size-setq1432 */
	obj_t BGl_z62nodezd2siza7ezd2setq1432zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2896, obj_t BgL_nodez00_2897)
	{
		{	/* Inline/size.scm 144 */
			return
				BINT(
				(2L +
					BGl_nodezd2siza7ez75zzinline_siza7eza7(
						(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_2897)))->BgL_valuez00))));
		}

	}



/* &node-size-extern1430 */
	obj_t BGl_z62nodezd2siza7ezd2extern1430zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2898, obj_t BgL_nodez00_2899)
	{
		{	/* Inline/size.scm 134 */
			{	/* Inline/size.scm 135 */
				long BgL_tmpz00_4401;

				{	/* Inline/size.scm 135 */
					obj_t BgL_g1211z00_3142;

					BgL_g1211z00_3142 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2899)))->BgL_exprza2za2);
					{
						obj_t BgL_argsz00_3144;
						long BgL_siza7eza7_3145;

						BgL_argsz00_3144 = BgL_g1211z00_3142;
						BgL_siza7eza7_3145 = 1L;
					BgL_loopz00_3143:
						if (NULLP(BgL_argsz00_3144))
							{	/* Inline/size.scm 137 */
								BgL_tmpz00_4401 = BgL_siza7eza7_3145;
							}
						else
							{	/* Inline/size.scm 139 */
								obj_t BgL_arg1877z00_3146;
								long BgL_arg1878z00_3147;

								BgL_arg1877z00_3146 = CDR(((obj_t) BgL_argsz00_3144));
								{	/* Inline/size.scm 139 */
									long BgL_arg1879z00_3148;

									{	/* Inline/size.scm 139 */
										obj_t BgL_arg1880z00_3149;

										BgL_arg1880z00_3149 = CAR(((obj_t) BgL_argsz00_3144));
										BgL_arg1879z00_3148 =
											BGl_nodezd2siza7ez75zzinline_siza7eza7(
											((BgL_nodez00_bglt) BgL_arg1880z00_3149));
									}
									BgL_arg1878z00_3147 =
										(BgL_siza7eza7_3145 + BgL_arg1879z00_3148);
								}
								{
									long BgL_siza7eza7_4414;
									obj_t BgL_argsz00_4413;

									BgL_argsz00_4413 = BgL_arg1877z00_3146;
									BgL_siza7eza7_4414 = BgL_arg1878z00_3147;
									BgL_siza7eza7_3145 = BgL_siza7eza7_4414;
									BgL_argsz00_3144 = BgL_argsz00_4413;
									goto BgL_loopz00_3143;
								}
							}
					}
				}
				return BINT(BgL_tmpz00_4401);
			}
		}

	}



/* &node-size-cast1428 */
	obj_t BGl_z62nodezd2siza7ezd2cast1428zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2900, obj_t BgL_nodez00_2901)
	{
		{	/* Inline/size.scm 127 */
			return
				BINT(BGl_nodezd2siza7ez75zzinline_siza7eza7(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_2901)))->BgL_argz00)));
		}

	}



/* &node-size-funcall1426 */
	obj_t BGl_z62nodezd2siza7ezd2funcall1426zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2902, obj_t BgL_nodez00_2903)
	{
		{	/* Inline/size.scm 117 */
			{	/* Inline/size.scm 118 */
				long BgL_tmpz00_4420;

				{	/* Inline/size.scm 118 */
					obj_t BgL_g1208z00_3152;
					long BgL_g1209z00_3153;

					BgL_g1208z00_3152 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2903)))->BgL_argsz00);
					BgL_g1209z00_3153 =
						BGl_nodezd2siza7ez75zzinline_siza7eza7(
						(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_2903)))->BgL_funz00));
					{
						obj_t BgL_argsz00_3155;
						long BgL_siza7eza7_3156;

						BgL_argsz00_3155 = BgL_g1208z00_3152;
						BgL_siza7eza7_3156 = BgL_g1209z00_3153;
					BgL_loopz00_3154:
						if (NULLP(BgL_argsz00_3155))
							{	/* Inline/size.scm 120 */
								BgL_tmpz00_4420 = BgL_siza7eza7_3156;
							}
						else
							{	/* Inline/size.scm 122 */
								obj_t BgL_arg1868z00_3157;
								long BgL_arg1869z00_3158;

								BgL_arg1868z00_3157 = CDR(((obj_t) BgL_argsz00_3155));
								{	/* Inline/size.scm 122 */
									long BgL_arg1870z00_3159;

									{	/* Inline/size.scm 122 */
										obj_t BgL_arg1872z00_3160;

										BgL_arg1872z00_3160 = CAR(((obj_t) BgL_argsz00_3155));
										BgL_arg1870z00_3159 =
											BGl_nodezd2siza7ez75zzinline_siza7eza7(
											((BgL_nodez00_bglt) BgL_arg1872z00_3160));
									}
									BgL_arg1869z00_3158 =
										(BgL_siza7eza7_3156 + BgL_arg1870z00_3159);
								}
								{
									long BgL_siza7eza7_4436;
									obj_t BgL_argsz00_4435;

									BgL_argsz00_4435 = BgL_arg1868z00_3157;
									BgL_siza7eza7_4436 = BgL_arg1869z00_3158;
									BgL_siza7eza7_3156 = BgL_siza7eza7_4436;
									BgL_argsz00_3155 = BgL_argsz00_4435;
									goto BgL_loopz00_3154;
								}
							}
					}
				}
				return BINT(BgL_tmpz00_4420);
			}
		}

	}



/* &node-size-app-ly1423 */
	obj_t BGl_z62nodezd2siza7ezd2appzd2ly1423z17zzinline_siza7eza7(obj_t
		BgL_envz00_2904, obj_t BgL_nodez00_2905)
	{
		{	/* Inline/size.scm 111 */
			return
				BINT(
				(1L +
					(BGl_nodezd2siza7ez75zzinline_siza7eza7(
							(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_2905)))->BgL_funz00)) +
						BGl_nodezd2siza7ez75zzinline_siza7eza7(
							(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_2905)))->
								BgL_argz00)))));
		}

	}



/* &node-size-app1420 */
	obj_t BGl_z62nodezd2siza7ezd2app1420zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2906, obj_t BgL_nodez00_2907)
	{
		{	/* Inline/size.scm 91 */
			{	/* Inline/size.scm 92 */
				long BgL_tmpz00_4447;

				if (CBOOL(BGl_getenvz00zz__osz00(BGl_string2081z00zzinline_siza7eza7)))
					{	/* Inline/size.scm 92 */
						BGl_mz00zzinline_siza7eza7 = (BGl_mz00zzinline_siza7eza7 + 1L);
						{	/* Inline/size.scm 94 */
							obj_t BgL_port1380z00_3163;

							{	/* Inline/size.scm 94 */
								obj_t BgL_tmpz00_4452;

								BgL_tmpz00_4452 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1380z00_3163 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4452);
							}
							bgl_display_obj(make_string(BGl_mz00zzinline_siza7eza7,
									((unsigned char) ' ')), BgL_port1380z00_3163);
							bgl_display_string(BGl_string2082z00zzinline_siza7eza7,
								BgL_port1380z00_3163);
							{	/* Inline/size.scm 94 */
								obj_t BgL_arg1836z00_3164;

								BgL_arg1836z00_3164 =
									BGl_shapez00zztools_shapez00(
									((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2907)));
								bgl_display_obj(BgL_arg1836z00_3164, BgL_port1380z00_3163);
							}
							bgl_display_char(((unsigned char) 10), BgL_port1380z00_3163);
					}}
				else
					{	/* Inline/size.scm 92 */
						BFALSE;
					}
				{	/* Inline/size.scm 95 */
					obj_t BgL_g1201z00_3165;
					long BgL_g1202z00_3166;

					BgL_g1201z00_3165 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2907)))->BgL_argsz00);
					{	/* Inline/size.scm 96 */
						BgL_varz00_bglt BgL_arg1858z00_3167;

						BgL_arg1858z00_3167 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_2907)))->BgL_funz00);
						BgL_g1202z00_3166 =
							BGl_nodezd2siza7ez75zzinline_siza7eza7(
							((BgL_nodez00_bglt) BgL_arg1858z00_3167));
					}
					{
						obj_t BgL_argsz00_3169;
						long BgL_siza7eza7_3170;

						BgL_argsz00_3169 = BgL_g1201z00_3165;
						BgL_siza7eza7_3170 = BgL_g1202z00_3166;
					BgL_loopz00_3168:
						if (CBOOL(BGl_getenvz00zz__osz00
								(BGl_string2081z00zzinline_siza7eza7)))
							{	/* Inline/size.scm 98 */
								obj_t BgL_port1387z00_3171;

								{	/* Inline/size.scm 98 */
									obj_t BgL_tmpz00_4472;

									BgL_tmpz00_4472 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_port1387z00_3171 =
										BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4472);
								}
								bgl_display_obj(make_string(BGl_mz00zzinline_siza7eza7,
										((unsigned char) ' ')), BgL_port1387z00_3171);
								bgl_display_string(BGl_string2083z00zzinline_siza7eza7,
									BgL_port1387z00_3171);
								{	/* Inline/size.scm 98 */
									obj_t BgL_arg1840z00_3172;

									if (NULLP(BgL_argsz00_3169))
										{	/* Inline/size.scm 98 */
											BgL_arg1840z00_3172 = BNIL;
										}
									else
										{	/* Inline/size.scm 98 */
											obj_t BgL_head1383z00_3173;

											{	/* Inline/size.scm 98 */
												obj_t BgL_arg1847z00_3174;

												{	/* Inline/size.scm 98 */
													obj_t BgL_arg1848z00_3175;

													BgL_arg1848z00_3175 = CAR(((obj_t) BgL_argsz00_3169));
													BgL_arg1847z00_3174 =
														BGl_shapez00zztools_shapez00(BgL_arg1848z00_3175);
												}
												BgL_head1383z00_3173 =
													MAKE_YOUNG_PAIR(BgL_arg1847z00_3174, BNIL);
											}
											{	/* Inline/size.scm 98 */
												obj_t BgL_g1386z00_3176;

												BgL_g1386z00_3176 = CDR(((obj_t) BgL_argsz00_3169));
												{
													obj_t BgL_l1381z00_3178;
													obj_t BgL_tail1384z00_3179;

													BgL_l1381z00_3178 = BgL_g1386z00_3176;
													BgL_tail1384z00_3179 = BgL_head1383z00_3173;
												BgL_zc3z04anonymousza31842ze3z87_3177:
													if (NULLP(BgL_l1381z00_3178))
														{	/* Inline/size.scm 98 */
															BgL_arg1840z00_3172 = BgL_head1383z00_3173;
														}
													else
														{	/* Inline/size.scm 98 */
															obj_t BgL_newtail1385z00_3180;

															{	/* Inline/size.scm 98 */
																obj_t BgL_arg1845z00_3181;

																{	/* Inline/size.scm 98 */
																	obj_t BgL_arg1846z00_3182;

																	BgL_arg1846z00_3182 =
																		CAR(((obj_t) BgL_l1381z00_3178));
																	BgL_arg1845z00_3181 =
																		BGl_shapez00zztools_shapez00
																		(BgL_arg1846z00_3182);
																}
																BgL_newtail1385z00_3180 =
																	MAKE_YOUNG_PAIR(BgL_arg1845z00_3181, BNIL);
															}
															SET_CDR(BgL_tail1384z00_3179,
																BgL_newtail1385z00_3180);
															{	/* Inline/size.scm 98 */
																obj_t BgL_arg1844z00_3183;

																BgL_arg1844z00_3183 =
																	CDR(((obj_t) BgL_l1381z00_3178));
																{
																	obj_t BgL_tail1384z00_4496;
																	obj_t BgL_l1381z00_4495;

																	BgL_l1381z00_4495 = BgL_arg1844z00_3183;
																	BgL_tail1384z00_4496 =
																		BgL_newtail1385z00_3180;
																	BgL_tail1384z00_3179 = BgL_tail1384z00_4496;
																	BgL_l1381z00_3178 = BgL_l1381z00_4495;
																	goto BgL_zc3z04anonymousza31842ze3z87_3177;
																}
															}
														}
												}
											}
										}
									bgl_display_obj(BgL_arg1840z00_3172, BgL_port1387z00_3171);
								}
								bgl_display_string(BGl_string2084z00zzinline_siza7eza7,
									BgL_port1387z00_3171);
								bgl_display_obj(BINT(BgL_siza7eza7_3170), BgL_port1387z00_3171);
								bgl_display_string(BGl_string2085z00zzinline_siza7eza7,
									BgL_port1387z00_3171);
								{	/* Inline/size.scm 98 */
									bool_t BgL_arg1849z00_3184;

									BgL_arg1849z00_3184 = NULLP(BgL_argsz00_3169);
									bgl_display_obj(BBOOL(BgL_arg1849z00_3184),
										BgL_port1387z00_3171);
								}
								bgl_display_char(((unsigned char) 10), BgL_port1387z00_3171);
							}
						else
							{	/* Inline/size.scm 97 */
								BFALSE;
							}
						if (NULLP(BgL_argsz00_3169))
							{	/* Inline/size.scm 99 */
								if (CBOOL(BGl_getenvz00zz__osz00
										(BGl_string2081z00zzinline_siza7eza7)))
									{	/* Inline/size.scm 101 */
										{	/* Inline/size.scm 102 */
											obj_t BgL_port1388z00_3185;

											{	/* Inline/size.scm 102 */
												obj_t BgL_tmpz00_4511;

												BgL_tmpz00_4511 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1388z00_3185 =
													BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4511);
											}
											bgl_display_obj(make_string(BGl_mz00zzinline_siza7eza7,
													((unsigned char) ' ')), BgL_port1388z00_3185);
											bgl_display_string(BGl_string2086z00zzinline_siza7eza7,
												BgL_port1388z00_3185);
											bgl_display_obj(BINT(BgL_siza7eza7_3170),
												BgL_port1388z00_3185);
											bgl_display_char(((unsigned char) 10),
												BgL_port1388z00_3185);
										}
										BGl_mz00zzinline_siza7eza7 =
											(BGl_mz00zzinline_siza7eza7 - 1L);
									}
								else
									{	/* Inline/size.scm 101 */
										BFALSE;
									}
								BgL_tmpz00_4447 = BgL_siza7eza7_3170;
							}
						else
							{	/* Inline/size.scm 106 */
								obj_t BgL_arg1853z00_3186;
								long BgL_arg1854z00_3187;

								BgL_arg1853z00_3186 = CDR(((obj_t) BgL_argsz00_3169));
								{	/* Inline/size.scm 106 */
									long BgL_arg1856z00_3188;

									{	/* Inline/size.scm 106 */
										obj_t BgL_arg1857z00_3189;

										BgL_arg1857z00_3189 = CAR(((obj_t) BgL_argsz00_3169));
										BgL_arg1856z00_3188 =
											BGl_nodezd2siza7ez75zzinline_siza7eza7(
											((BgL_nodez00_bglt) BgL_arg1857z00_3189));
									}
									BgL_arg1854z00_3187 =
										(BgL_siza7eza7_3170 + BgL_arg1856z00_3188);
								}
								{
									long BgL_siza7eza7_4529;
									obj_t BgL_argsz00_4528;

									BgL_argsz00_4528 = BgL_arg1853z00_3186;
									BgL_siza7eza7_4529 = BgL_arg1854z00_3187;
									BgL_siza7eza7_3170 = BgL_siza7eza7_4529;
									BgL_argsz00_3169 = BgL_argsz00_4528;
									goto BgL_loopz00_3168;
								}
							}
					}
				}
				return BINT(BgL_tmpz00_4447);
			}
		}

	}



/* &node-size-sized-sync1418 */
	obj_t BGl_z62nodezd2siza7ezd2siza7edzd2sync1418zb0zzinline_siza7eza7(obj_t
		BgL_envz00_2908, obj_t BgL_nodez00_2909)
	{
		{	/* Inline/size.scm 85 */
			{	/* Inline/size.scm 86 */
				long BgL_tmpz00_4531;

				{
					BgL_siza7edzd2syncz75_bglt BgL_auxz00_4532;

					{
						obj_t BgL_auxz00_4533;

						{	/* Inline/size.scm 86 */
							BgL_objectz00_bglt BgL_tmpz00_4534;

							BgL_tmpz00_4534 =
								((BgL_objectz00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2909));
							BgL_auxz00_4533 = BGL_OBJECT_WIDENING(BgL_tmpz00_4534);
						}
						BgL_auxz00_4532 = ((BgL_siza7edzd2syncz75_bglt) BgL_auxz00_4533);
					}
					BgL_tmpz00_4531 =
						(((BgL_siza7edzd2syncz75_bglt) COBJECT(BgL_auxz00_4532))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_4531);
			}
		}

	}



/* &node-size-sync1416 */
	obj_t BGl_z62nodezd2siza7ezd2sync1416zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2910, obj_t BgL_nodez00_2911)
	{
		{	/* Inline/size.scm 74 */
			{	/* Inline/size.scm 75 */
				obj_t BgL_siza7eza7_3192;

				{	/* Inline/size.scm 76 */
					obj_t BgL_b1379z00_3193;

					{	/* Inline/size.scm 76 */
						long BgL_a1376z00_3194;

						BgL_a1376z00_3194 =
							BGl_nodezd2siza7ez75zzinline_siza7eza7(
							(((BgL_syncz00_bglt) COBJECT(
										((BgL_syncz00_bglt) BgL_nodez00_2911)))->BgL_mutexz00));
						{	/* Inline/size.scm 77 */
							obj_t BgL_b1377z00_3195;

							{	/* Inline/size.scm 77 */
								long BgL_a1374z00_3196;

								BgL_a1374z00_3196 =
									BGl_nodezd2siza7ez75zzinline_siza7eza7(
									(((BgL_syncz00_bglt) COBJECT(
												((BgL_syncz00_bglt) BgL_nodez00_2911)))->
										BgL_prelockz00));
								{	/* Inline/size.scm 78 */
									long BgL_b1375z00_3197;

									BgL_b1375z00_3197 =
										BGl_nodezd2siza7ez75zzinline_siza7eza7(
										(((BgL_syncz00_bglt) COBJECT(
													((BgL_syncz00_bglt) BgL_nodez00_2911)))->
											BgL_bodyz00));
									{	/* Inline/size.scm 75 */

										{	/* Inline/size.scm 75 */
											obj_t BgL_za71za7_3198;
											obj_t BgL_za72za7_3199;

											BgL_za71za7_3198 = BINT(BgL_a1374z00_3196);
											BgL_za72za7_3199 = BINT(BgL_b1375z00_3197);
											{	/* Inline/size.scm 75 */
												obj_t BgL_tmpz00_3200;

												BgL_tmpz00_3200 = BINT(0L);
												if (BGL_ADDFX_OV(BgL_za71za7_3198, BgL_za72za7_3199,
														BgL_tmpz00_3200))
													{	/* Inline/size.scm 75 */
														BgL_b1377z00_3195 =
															bgl_bignum_add(bgl_long_to_bignum(
																(long) CINT(BgL_za71za7_3198)),
															bgl_long_to_bignum(
																(long) CINT(BgL_za72za7_3199)));
													}
												else
													{	/* Inline/size.scm 75 */
														BgL_b1377z00_3195 = BgL_tmpz00_3200;
													}
											}
										}
									}
								}
							}
							{	/* Inline/size.scm 75 */

								if (INTEGERP(BgL_b1377z00_3195))
									{	/* Inline/size.scm 75 */
										obj_t BgL_za71za7_3201;

										BgL_za71za7_3201 = BINT(BgL_a1376z00_3194);
										{	/* Inline/size.scm 75 */
											obj_t BgL_tmpz00_3202;

											BgL_tmpz00_3202 = BINT(0L);
											if (BGL_ADDFX_OV(BgL_za71za7_3201, BgL_b1377z00_3195,
													BgL_tmpz00_3202))
												{	/* Inline/size.scm 75 */
													BgL_b1379z00_3193 =
														bgl_bignum_add(bgl_long_to_bignum(
															(long) CINT(BgL_za71za7_3201)),
														bgl_long_to_bignum((long) CINT(BgL_b1377z00_3195)));
												}
											else
												{	/* Inline/size.scm 75 */
													BgL_b1379z00_3193 = BgL_tmpz00_3202;
												}
										}
									}
								else
									{	/* Inline/size.scm 75 */
										BgL_b1379z00_3193 =
											BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(BgL_a1376z00_3194),
											BgL_b1377z00_3195);
									}
							}
						}
					}
					{	/* Inline/size.scm 75 */

						if (INTEGERP(BgL_b1379z00_3193))
							{	/* Inline/size.scm 75 */
								obj_t BgL_za71za7_3203;

								BgL_za71za7_3203 = BINT(1L);
								{	/* Inline/size.scm 75 */
									obj_t BgL_tmpz00_3204;

									BgL_tmpz00_3204 = BINT(0L);
									if (BGL_ADDFX_OV(BgL_za71za7_3203, BgL_b1379z00_3193,
											BgL_tmpz00_3204))
										{	/* Inline/size.scm 75 */
											BgL_siza7eza7_3192 =
												bgl_bignum_add(bgl_long_to_bignum(
													(long) CINT(BgL_za71za7_3203)),
												bgl_long_to_bignum((long) CINT(BgL_b1379z00_3193)));
										}
									else
										{	/* Inline/size.scm 75 */
											BgL_siza7eza7_3192 = BgL_tmpz00_3204;
										}
								}
							}
						else
							{	/* Inline/size.scm 75 */
								BgL_siza7eza7_3192 =
									BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(1L), BgL_b1379z00_3193);
							}
					}
				}
				{	/* Inline/size.scm 79 */
					BgL_siza7edzd2syncz75_bglt BgL_wide1198z00_3205;

					BgL_wide1198z00_3205 =
						((BgL_siza7edzd2syncz75_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_siza7edzd2syncz75_bgl))));
					{	/* Inline/size.scm 79 */
						obj_t BgL_auxz00_4591;
						BgL_objectz00_bglt BgL_tmpz00_4587;

						BgL_auxz00_4591 = ((obj_t) BgL_wide1198z00_3205);
						BgL_tmpz00_4587 =
							((BgL_objectz00_bglt)
							((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2911)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4587, BgL_auxz00_4591);
					}
					((BgL_objectz00_bglt)
						((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2911)));
					{	/* Inline/size.scm 79 */
						long BgL_arg1822z00_3206;

						BgL_arg1822z00_3206 =
							BGL_CLASS_NUM(BGl_siza7edzd2syncz75zzinline_siza7eza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_syncz00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_2911))), BgL_arg1822z00_3206);
					}
					((BgL_syncz00_bglt)
						((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2911)));
				}
				{
					BgL_siza7edzd2syncz75_bglt BgL_auxz00_4605;

					{
						obj_t BgL_auxz00_4606;

						{	/* Inline/size.scm 79 */
							BgL_objectz00_bglt BgL_tmpz00_4607;

							BgL_tmpz00_4607 =
								((BgL_objectz00_bglt)
								((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2911)));
							BgL_auxz00_4606 = BGL_OBJECT_WIDENING(BgL_tmpz00_4607);
						}
						BgL_auxz00_4605 = ((BgL_siza7edzd2syncz75_bglt) BgL_auxz00_4606);
					}
					((((BgL_siza7edzd2syncz75_bglt) COBJECT(BgL_auxz00_4605))->
							BgL_siza7eza7) =
						((long) (long) CINT(BgL_siza7eza7_3192)), BUNSPEC);
				}
				((BgL_syncz00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2911));
				return BgL_siza7eza7_3192;
			}
		}

	}



/* &node-size-sized-sequ1414 */
	obj_t BGl_z62nodezd2siza7ezd2siza7edzd2sequ1414zb0zzinline_siza7eza7(obj_t
		BgL_envz00_2912, obj_t BgL_nodez00_2913)
	{
		{	/* Inline/size.scm 68 */
			{	/* Inline/size.scm 69 */
				long BgL_tmpz00_4617;

				{
					BgL_siza7edzd2sequencez75_bglt BgL_auxz00_4618;

					{
						obj_t BgL_auxz00_4619;

						{	/* Inline/size.scm 69 */
							BgL_objectz00_bglt BgL_tmpz00_4620;

							BgL_tmpz00_4620 =
								((BgL_objectz00_bglt)
								((BgL_sequencez00_bglt) BgL_nodez00_2913));
							BgL_auxz00_4619 = BGL_OBJECT_WIDENING(BgL_tmpz00_4620);
						}
						BgL_auxz00_4618 =
							((BgL_siza7edzd2sequencez75_bglt) BgL_auxz00_4619);
					}
					BgL_tmpz00_4617 =
						(((BgL_siza7edzd2sequencez75_bglt) COBJECT(BgL_auxz00_4618))->
						BgL_siza7eza7);
				}
				return BINT(BgL_tmpz00_4617);
			}
		}

	}



/* &node-size-sequence1412 */
	obj_t BGl_z62nodezd2siza7ezd2sequence1412zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2914, obj_t BgL_nodez00_2915)
	{
		{	/* Inline/size.scm 56 */
			{	/* Inline/size.scm 57 */
				long BgL_tmpz00_4627;

				{	/* Inline/size.scm 57 */
					obj_t BgL_g1191z00_3209;

					BgL_g1191z00_3209 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2915)))->BgL_nodesz00);
					{
						obj_t BgL_nodesz00_3211;
						long BgL_siza7eza7_3212;

						BgL_nodesz00_3211 = BgL_g1191z00_3209;
						BgL_siza7eza7_3212 = 0L;
					BgL_loopz00_3210:
						if (NULLP(BgL_nodesz00_3211))
							{	/* Inline/size.scm 59 */
								{	/* Inline/size.scm 61 */
									BgL_siza7edzd2sequencez75_bglt BgL_wide1194z00_3213;

									BgL_wide1194z00_3213 =
										((BgL_siza7edzd2sequencez75_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_siza7edzd2sequencez75_bgl))));
									{	/* Inline/size.scm 61 */
										obj_t BgL_auxz00_4637;
										BgL_objectz00_bglt BgL_tmpz00_4633;

										BgL_auxz00_4637 = ((obj_t) BgL_wide1194z00_3213);
										BgL_tmpz00_4633 =
											((BgL_objectz00_bglt)
											((BgL_sequencez00_bglt)
												((BgL_sequencez00_bglt) BgL_nodez00_2915)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4633, BgL_auxz00_4637);
									}
									((BgL_objectz00_bglt)
										((BgL_sequencez00_bglt)
											((BgL_sequencez00_bglt) BgL_nodez00_2915)));
									{	/* Inline/size.scm 61 */
										long BgL_arg1805z00_3214;

										BgL_arg1805z00_3214 =
											BGL_CLASS_NUM
											(BGl_siza7edzd2sequencez75zzinline_siza7eza7);
										BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
													(BgL_sequencez00_bglt) ((BgL_sequencez00_bglt)
														BgL_nodez00_2915))), BgL_arg1805z00_3214);
									}
									((BgL_sequencez00_bglt)
										((BgL_sequencez00_bglt)
											((BgL_sequencez00_bglt) BgL_nodez00_2915)));
								}
								{
									BgL_siza7edzd2sequencez75_bglt BgL_auxz00_4651;

									{
										obj_t BgL_auxz00_4652;

										{	/* Inline/size.scm 61 */
											BgL_objectz00_bglt BgL_tmpz00_4653;

											BgL_tmpz00_4653 =
												((BgL_objectz00_bglt)
												((BgL_sequencez00_bglt)
													((BgL_sequencez00_bglt) BgL_nodez00_2915)));
											BgL_auxz00_4652 = BGL_OBJECT_WIDENING(BgL_tmpz00_4653);
										}
										BgL_auxz00_4651 =
											((BgL_siza7edzd2sequencez75_bglt) BgL_auxz00_4652);
									}
									((((BgL_siza7edzd2sequencez75_bglt)
												COBJECT(BgL_auxz00_4651))->BgL_siza7eza7) =
										((long) BgL_siza7eza7_3212), BUNSPEC);
								}
								((BgL_sequencez00_bglt)
									((BgL_sequencez00_bglt) BgL_nodez00_2915));
								BgL_tmpz00_4627 = BgL_siza7eza7_3212;
							}
						else
							{	/* Inline/size.scm 63 */
								obj_t BgL_arg1806z00_3215;
								long BgL_arg1808z00_3216;

								BgL_arg1806z00_3215 = CDR(((obj_t) BgL_nodesz00_3211));
								{	/* Inline/size.scm 63 */
									long BgL_arg1812z00_3217;

									{	/* Inline/size.scm 63 */
										obj_t BgL_arg1820z00_3218;

										BgL_arg1820z00_3218 = CAR(((obj_t) BgL_nodesz00_3211));
										BgL_arg1812z00_3217 =
											BGl_nodezd2siza7ez75zzinline_siza7eza7(
											((BgL_nodez00_bglt) BgL_arg1820z00_3218));
									}
									BgL_arg1808z00_3216 =
										(BgL_siza7eza7_3212 + BgL_arg1812z00_3217);
								}
								{
									long BgL_siza7eza7_4670;
									obj_t BgL_nodesz00_4669;

									BgL_nodesz00_4669 = BgL_arg1806z00_3215;
									BgL_siza7eza7_4670 = BgL_arg1808z00_3216;
									BgL_siza7eza7_3212 = BgL_siza7eza7_4670;
									BgL_nodesz00_3211 = BgL_nodesz00_4669;
									goto BgL_loopz00_3210;
								}
							}
					}
				}
				return BINT(BgL_tmpz00_4627);
			}
		}

	}



/* &node-size-kwote1408 */
	obj_t BGl_z62nodezd2siza7ezd2kwote1408zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2916, obj_t BgL_nodez00_2917)
	{
		{	/* Inline/size.scm 50 */
			return BINT(1L);
		}

	}



/* &node-size-var1406 */
	obj_t BGl_z62nodezd2siza7ezd2var1406zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2918, obj_t BgL_nodez00_2919)
	{
		{	/* Inline/size.scm 44 */
			return BINT(1L);
		}

	}



/* &node-size-atom1403 */
	obj_t BGl_z62nodezd2siza7ezd2atom1403zc5zzinline_siza7eza7(obj_t
		BgL_envz00_2920, obj_t BgL_nodez00_2921)
	{
		{	/* Inline/size.scm 38 */
			return BINT(1L);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinline_siza7eza7(void)
	{
		{	/* Inline/size.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2087z00zzinline_siza7eza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2087z00zzinline_siza7eza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2087z00zzinline_siza7eza7));
			return
				BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2087z00zzinline_siza7eza7));
		}

	}

#ifdef __cplusplus
}
#endif
