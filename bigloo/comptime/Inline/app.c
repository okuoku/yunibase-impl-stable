/*===========================================================================*/
/*   (Inline/app.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Inline/app.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INLINE_APP_TYPE_DEFINITIONS
#define BGL_INLINE_APP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_isfunz00_bgl
	{
		struct BgL_nodez00_bgl *BgL_originalzd2bodyzd2;
		obj_t BgL_recursivezd2callszd2;
		bool_t BgL_tailrecz00;
	}               *BgL_isfunz00_bglt;


#endif													// BGL_INLINE_APP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern bool_t
		BGl_iszd2recursivezf3z21zzinline_recursionz00(BgL_variablez00_bglt);
	extern obj_t BGl_za2optimzd2unrollzd2loopzf3za2zf3zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzinline_appz00 = BUNSPEC;
	extern obj_t BGl_nodezd2walkzd2zzast_nodez00(BgL_nodez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_atomz00zzast_nodez00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_zc3z04exitza31330ze3ze70z60zzinline_appz00(obj_t);
	BGL_IMPORT obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzinline_appz00(void);
	static bool_t BGl_inlinezd2closurezf3z21zzinline_appz00(BgL_variablez00_bglt,
		obj_t);
	extern obj_t BGl_withzd2libraryzd2modulez12z12zzmodule_libraryz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzinline_appz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2userzd2inliningzf3za2z21zzengine_paramz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzinline_appz00(void);
	static obj_t BGl_z62zc3z04anonymousza31332ze3ze5zzinline_appz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_inlinezd2appzf3z21zzinline_appz00(BgL_variablez00_bglt, long, long,
		obj_t);
	static long BGl_callzd2siza7ez75zzinline_appz00(BgL_appz00_bglt);
	extern obj_t BGl_za2inliningzf3za2zf3zzengine_paramz00;
	extern BgL_nodez00_bglt
		BGl_inlinezd2appzd2simplez00zzinline_simplez00(BgL_nodez00_bglt, long,
		obj_t, obj_t);
	extern obj_t BGl_isfunz00zzinline_inlinez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzinline_appz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_recursionz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_simplez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_siza7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	static obj_t BGl_z62inlinezd2appzf3z43zzinline_appz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern long BGl_nodezd2siza7ez75zzinline_siza7eza7(BgL_nodez00_bglt);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzinline_appz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinline_appz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern obj_t BGl_za2optimzd2loopzd2inliningzf3za2zf3zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzinline_appz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinline_appz00(void);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_inlinezd2appzd2zzinline_appz00(BgL_appz00_bglt, long, obj_t);
	extern BgL_nodez00_bglt
		BGl_inlinezd2appzd2recursivez00zzinline_recursionz00(BgL_nodez00_bglt, long,
		obj_t);
	extern obj_t BGl_za2inlinedzd2callsza2zd2zzinline_walkz00;
	extern obj_t BGl_retblockz00zzast_nodez00;
	extern obj_t BGl_za2inlinezd2modeza2zd2zzinline_walkz00;
	static BgL_nodez00_bglt BGl_z62inlinezd2appzb0zzinline_appz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2optimzd2atomzd2inliningzf3za2zf3zzengine_paramz00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[8];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_inlinezd2appzd2envz00zzinline_appz00,
		BgL_bgl_za762inlineza7d2appza71740za7,
		BGl_z62inlinezd2appzb0zzinline_appz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1736z00zzinline_appz00,
		BgL_bgl_string1736za700za7za7i1741za7, "simple", 6);
	      DEFINE_STRING(BGl_string1737z00zzinline_appz00,
		BgL_bgl_string1737za700za7za7i1742za7, "inline_app", 10);
	      DEFINE_STRING(BGl_string1738z00zzinline_appz00,
		BgL_bgl_string1738za700za7za7i1743za7,
		"import predicate reducer sgfun all snifun static sifun ", 55);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_inlinezd2appzf3zd2envzf3zzinline_appz00,
		BgL_bgl_za762inlineza7d2appza71744za7,
		BGl_z62inlinezd2appzf3z43zzinline_appz00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzinline_appz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinline_appz00(long
		BgL_checksumz00_2417, char *BgL_fromz00_2418)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinline_appz00))
				{
					BGl_requirezd2initializa7ationz75zzinline_appz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinline_appz00();
					BGl_libraryzd2moduleszd2initz00zzinline_appz00();
					BGl_cnstzd2initzd2zzinline_appz00();
					BGl_importedzd2moduleszd2initz00zzinline_appz00();
					return BGl_methodzd2initzd2zzinline_appz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinline_appz00(void)
	{
		{	/* Inline/app.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "inline_app");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "inline_app");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"inline_app");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "inline_app");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "inline_app");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "inline_app");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"inline_app");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "inline_app");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"inline_app");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinline_appz00(void)
	{
		{	/* Inline/app.scm 15 */
			{	/* Inline/app.scm 15 */
				obj_t BgL_cportz00_2391;

				{	/* Inline/app.scm 15 */
					obj_t BgL_stringz00_2398;

					BgL_stringz00_2398 = BGl_string1738z00zzinline_appz00;
					{	/* Inline/app.scm 15 */
						obj_t BgL_startz00_2399;

						BgL_startz00_2399 = BINT(0L);
						{	/* Inline/app.scm 15 */
							obj_t BgL_endz00_2400;

							BgL_endz00_2400 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2398)));
							{	/* Inline/app.scm 15 */

								BgL_cportz00_2391 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2398, BgL_startz00_2399, BgL_endz00_2400);
				}}}}
				{
					long BgL_iz00_2392;

					BgL_iz00_2392 = 7L;
				BgL_loopz00_2393:
					if ((BgL_iz00_2392 == -1L))
						{	/* Inline/app.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Inline/app.scm 15 */
							{	/* Inline/app.scm 15 */
								obj_t BgL_arg1739z00_2394;

								{	/* Inline/app.scm 15 */

									{	/* Inline/app.scm 15 */
										obj_t BgL_locationz00_2396;

										BgL_locationz00_2396 = BBOOL(((bool_t) 0));
										{	/* Inline/app.scm 15 */

											BgL_arg1739z00_2394 =
												BGl_readz00zz__readerz00(BgL_cportz00_2391,
												BgL_locationz00_2396);
										}
									}
								}
								{	/* Inline/app.scm 15 */
									int BgL_tmpz00_2445;

									BgL_tmpz00_2445 = (int) (BgL_iz00_2392);
									CNST_TABLE_SET(BgL_tmpz00_2445, BgL_arg1739z00_2394);
							}}
							{	/* Inline/app.scm 15 */
								int BgL_auxz00_2397;

								BgL_auxz00_2397 = (int) ((BgL_iz00_2392 - 1L));
								{
									long BgL_iz00_2450;

									BgL_iz00_2450 = (long) (BgL_auxz00_2397);
									BgL_iz00_2392 = BgL_iz00_2450;
									goto BgL_loopz00_2393;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinline_appz00(void)
	{
		{	/* Inline/app.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* inline-app */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_inlinezd2appzd2zzinline_appz00(BgL_appz00_bglt BgL_nodez00_3,
		long BgL_kfactorz00_4, obj_t BgL_stackz00_5)
	{
		{	/* Inline/app.scm 35 */
			{	/* Inline/app.scm 36 */
				BgL_varz00_bglt BgL_calleez00_1442;

				BgL_calleez00_1442 =
					(((BgL_appz00_bglt) COBJECT(BgL_nodez00_3))->BgL_funz00);
				{	/* Inline/app.scm 36 */
					BgL_variablez00_bglt BgL_varz00_1443;

					BgL_varz00_1443 =
						(((BgL_varz00_bglt) COBJECT(BgL_calleez00_1442))->BgL_variablez00);
					{	/* Inline/app.scm 37 */
						BgL_valuez00_bglt BgL_sfunz00_1444;

						BgL_sfunz00_1444 =
							(((BgL_variablez00_bglt) COBJECT(BgL_varz00_1443))->BgL_valuez00);
						{	/* Inline/app.scm 40 */

							{	/* Inline/app.scm 44 */
								bool_t BgL_test1748z00_2456;

								{	/* Inline/app.scm 44 */
									obj_t BgL_classz00_1755;

									BgL_classz00_1755 = BGl_sfunz00zzast_varz00;
									{	/* Inline/app.scm 44 */
										BgL_objectz00_bglt BgL_arg1807z00_1757;

										{	/* Inline/app.scm 44 */
											obj_t BgL_tmpz00_2457;

											BgL_tmpz00_2457 =
												((obj_t) ((BgL_objectz00_bglt) BgL_sfunz00_1444));
											BgL_arg1807z00_1757 =
												(BgL_objectz00_bglt) (BgL_tmpz00_2457);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Inline/app.scm 44 */
												long BgL_idxz00_1763;

												BgL_idxz00_1763 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1757);
												BgL_test1748z00_2456 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_1763 + 3L)) == BgL_classz00_1755);
											}
										else
											{	/* Inline/app.scm 44 */
												bool_t BgL_res1719z00_1788;

												{	/* Inline/app.scm 44 */
													obj_t BgL_oclassz00_1771;

													{	/* Inline/app.scm 44 */
														obj_t BgL_arg1815z00_1779;
														long BgL_arg1816z00_1780;

														BgL_arg1815z00_1779 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Inline/app.scm 44 */
															long BgL_arg1817z00_1781;

															BgL_arg1817z00_1781 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1757);
															BgL_arg1816z00_1780 =
																(BgL_arg1817z00_1781 - OBJECT_TYPE);
														}
														BgL_oclassz00_1771 =
															VECTOR_REF(BgL_arg1815z00_1779,
															BgL_arg1816z00_1780);
													}
													{	/* Inline/app.scm 44 */
														bool_t BgL__ortest_1115z00_1772;

														BgL__ortest_1115z00_1772 =
															(BgL_classz00_1755 == BgL_oclassz00_1771);
														if (BgL__ortest_1115z00_1772)
															{	/* Inline/app.scm 44 */
																BgL_res1719z00_1788 = BgL__ortest_1115z00_1772;
															}
														else
															{	/* Inline/app.scm 44 */
																long BgL_odepthz00_1773;

																{	/* Inline/app.scm 44 */
																	obj_t BgL_arg1804z00_1774;

																	BgL_arg1804z00_1774 = (BgL_oclassz00_1771);
																	BgL_odepthz00_1773 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_1774);
																}
																if ((3L < BgL_odepthz00_1773))
																	{	/* Inline/app.scm 44 */
																		obj_t BgL_arg1802z00_1776;

																		{	/* Inline/app.scm 44 */
																			obj_t BgL_arg1803z00_1777;

																			BgL_arg1803z00_1777 =
																				(BgL_oclassz00_1771);
																			BgL_arg1802z00_1776 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_1777, 3L);
																		}
																		BgL_res1719z00_1788 =
																			(BgL_arg1802z00_1776 ==
																			BgL_classz00_1755);
																	}
																else
																	{	/* Inline/app.scm 44 */
																		BgL_res1719z00_1788 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1748z00_2456 = BgL_res1719z00_1788;
											}
									}
								}
								if (BgL_test1748z00_2456)
									{	/* Inline/app.scm 46 */
										bool_t BgL_test1753z00_2480;

										if (BGl_inlinezd2appzf3z21zzinline_appz00(BgL_varz00_1443,
												BgL_kfactorz00_4,
												BGl_callzd2siza7ez75zzinline_appz00(BgL_nodez00_3),
												BgL_stackz00_5))
											{	/* Inline/app.scm 46 */
												BgL_test1753z00_2480 = ((bool_t) 1);
											}
										else
											{	/* Inline/app.scm 46 */
												BgL_test1753z00_2480 =
													BGl_inlinezd2closurezf3z21zzinline_appz00
													(BgL_varz00_1443, BgL_stackz00_5);
											}
										if (BgL_test1753z00_2480)
											{	/* Inline/app.scm 46 */
												if (
													((((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_sfunz00_1444)))->
															BgL_classz00) == CNST_TABLE_REF(0)))
													{	/* Inline/app.scm 51 */
														BFALSE;
													}
												else
													{	/* Inline/app.scm 51 */
														BGl_za2inlinedzd2callsza2zd2zzinline_walkz00 =
															ADDFX
															(BGl_za2inlinedzd2callsza2zd2zzinline_walkz00,
															BINT(1L));
													}
												{	/* Inline/app.scm 53 */
													bool_t BgL_test1757z00_2492;

													{	/* Inline/app.scm 53 */
														bool_t BgL_test1758z00_2493;

														{	/* Inline/app.scm 53 */
															obj_t BgL_classz00_1791;

															BgL_classz00_1791 = BGl_globalz00zzast_varz00;
															{	/* Inline/app.scm 53 */
																BgL_objectz00_bglt BgL_arg1807z00_1793;

																{	/* Inline/app.scm 53 */
																	obj_t BgL_tmpz00_2494;

																	BgL_tmpz00_2494 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_varz00_1443));
																	BgL_arg1807z00_1793 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_2494);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Inline/app.scm 53 */
																		long BgL_idxz00_1799;

																		BgL_idxz00_1799 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_1793);
																		BgL_test1758z00_2493 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_1799 + 2L)) ==
																			BgL_classz00_1791);
																	}
																else
																	{	/* Inline/app.scm 53 */
																		bool_t BgL_res1720z00_1824;

																		{	/* Inline/app.scm 53 */
																			obj_t BgL_oclassz00_1807;

																			{	/* Inline/app.scm 53 */
																				obj_t BgL_arg1815z00_1815;
																				long BgL_arg1816z00_1816;

																				BgL_arg1815z00_1815 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Inline/app.scm 53 */
																					long BgL_arg1817z00_1817;

																					BgL_arg1817z00_1817 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_1793);
																					BgL_arg1816z00_1816 =
																						(BgL_arg1817z00_1817 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_1807 =
																					VECTOR_REF(BgL_arg1815z00_1815,
																					BgL_arg1816z00_1816);
																			}
																			{	/* Inline/app.scm 53 */
																				bool_t BgL__ortest_1115z00_1808;

																				BgL__ortest_1115z00_1808 =
																					(BgL_classz00_1791 ==
																					BgL_oclassz00_1807);
																				if (BgL__ortest_1115z00_1808)
																					{	/* Inline/app.scm 53 */
																						BgL_res1720z00_1824 =
																							BgL__ortest_1115z00_1808;
																					}
																				else
																					{	/* Inline/app.scm 53 */
																						long BgL_odepthz00_1809;

																						{	/* Inline/app.scm 53 */
																							obj_t BgL_arg1804z00_1810;

																							BgL_arg1804z00_1810 =
																								(BgL_oclassz00_1807);
																							BgL_odepthz00_1809 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_1810);
																						}
																						if ((2L < BgL_odepthz00_1809))
																							{	/* Inline/app.scm 53 */
																								obj_t BgL_arg1802z00_1812;

																								{	/* Inline/app.scm 53 */
																									obj_t BgL_arg1803z00_1813;

																									BgL_arg1803z00_1813 =
																										(BgL_oclassz00_1807);
																									BgL_arg1802z00_1812 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_1813, 2L);
																								}
																								BgL_res1720z00_1824 =
																									(BgL_arg1802z00_1812 ==
																									BgL_classz00_1791);
																							}
																						else
																							{	/* Inline/app.scm 53 */
																								BgL_res1720z00_1824 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1758z00_2493 = BgL_res1720z00_1824;
																	}
															}
														}
														if (BgL_test1758z00_2493)
															{	/* Inline/app.scm 53 */
																BgL_test1757z00_2492 =
																	CBOOL(
																	(((BgL_globalz00_bglt) COBJECT(
																				((BgL_globalz00_bglt)
																					BgL_varz00_1443)))->BgL_libraryz00));
															}
														else
															{	/* Inline/app.scm 53 */
																BgL_test1757z00_2492 = ((bool_t) 0);
															}
													}
													if (BgL_test1757z00_2492)
														{	/* Inline/app.scm 56 */
															obj_t BgL_arg1304z00_1457;

															BgL_arg1304z00_1457 =
																(((BgL_globalz00_bglt) COBJECT(
																		((BgL_globalz00_bglt) BgL_varz00_1443)))->
																BgL_modulez00);
															BGl_withzd2libraryzd2modulez12z12zzmodule_libraryz00
																(BgL_arg1304z00_1457);
														}
													else
														{	/* Inline/app.scm 53 */
															BFALSE;
														}
												}
												{	/* Inline/app.scm 57 */
													bool_t BgL_test1763z00_2523;

													if (CBOOL
														(BGl_za2optimzd2loopzd2inliningzf3za2zf3zzengine_paramz00))
														{	/* Inline/app.scm 57 */
															BgL_test1763z00_2523 =
																BGl_iszd2recursivezf3z21zzinline_recursionz00
																(BgL_varz00_1443);
														}
													else
														{	/* Inline/app.scm 57 */
															BgL_test1763z00_2523 = ((bool_t) 0);
														}
													if (BgL_test1763z00_2523)
														{	/* Inline/app.scm 57 */
															return
																BGl_inlinezd2appzd2recursivez00zzinline_recursionz00
																(((BgL_nodez00_bglt) BgL_nodez00_3),
																BgL_kfactorz00_4, BgL_stackz00_5);
														}
													else
														{	/* Inline/app.scm 57 */
															return
																BGl_inlinezd2appzd2simplez00zzinline_simplez00(
																((BgL_nodez00_bglt) BgL_nodez00_3),
																BgL_kfactorz00_4, BgL_stackz00_5,
																BGl_string1736z00zzinline_appz00);
														}
												}
											}
										else
											{	/* Inline/app.scm 46 */
												return ((BgL_nodez00_bglt) BgL_nodez00_3);
											}
									}
								else
									{	/* Inline/app.scm 44 */
										return ((BgL_nodez00_bglt) BgL_nodez00_3);
									}
							}
						}
					}
				}
			}
		}

	}



/* &inline-app */
	BgL_nodez00_bglt BGl_z62inlinezd2appzb0zzinline_appz00(obj_t BgL_envz00_2377,
		obj_t BgL_nodez00_2378, obj_t BgL_kfactorz00_2379, obj_t BgL_stackz00_2380)
	{
		{	/* Inline/app.scm 35 */
			return
				BGl_inlinezd2appzd2zzinline_appz00(
				((BgL_appz00_bglt) BgL_nodez00_2378),
				(long) CINT(BgL_kfactorz00_2379), BgL_stackz00_2380);
		}

	}



/* inline-closure? */
	bool_t BGl_inlinezd2closurezf3z21zzinline_appz00(BgL_variablez00_bglt
		BgL_varz00_6, obj_t BgL_stackz00_7)
	{
		{	/* Inline/app.scm 66 */
			{	/* Inline/app.scm 67 */
				bool_t BgL_test1765z00_2536;

				if (PAIRP(BgL_stackz00_7))
					{	/* Inline/app.scm 67 */
						obj_t BgL_arg1319z00_1477;

						BgL_arg1319z00_1477 = CAR(BgL_stackz00_7);
						{	/* Inline/app.scm 67 */
							obj_t BgL_classz00_1828;

							BgL_classz00_1828 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg1319z00_1477))
								{	/* Inline/app.scm 67 */
									BgL_objectz00_bglt BgL_arg1807z00_1830;

									BgL_arg1807z00_1830 =
										(BgL_objectz00_bglt) (BgL_arg1319z00_1477);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Inline/app.scm 67 */
											long BgL_idxz00_1836;

											BgL_idxz00_1836 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1830);
											BgL_test1765z00_2536 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_1836 + 2L)) == BgL_classz00_1828);
										}
									else
										{	/* Inline/app.scm 67 */
											bool_t BgL_res1721z00_1861;

											{	/* Inline/app.scm 67 */
												obj_t BgL_oclassz00_1844;

												{	/* Inline/app.scm 67 */
													obj_t BgL_arg1815z00_1852;
													long BgL_arg1816z00_1853;

													BgL_arg1815z00_1852 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Inline/app.scm 67 */
														long BgL_arg1817z00_1854;

														BgL_arg1817z00_1854 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1830);
														BgL_arg1816z00_1853 =
															(BgL_arg1817z00_1854 - OBJECT_TYPE);
													}
													BgL_oclassz00_1844 =
														VECTOR_REF(BgL_arg1815z00_1852,
														BgL_arg1816z00_1853);
												}
												{	/* Inline/app.scm 67 */
													bool_t BgL__ortest_1115z00_1845;

													BgL__ortest_1115z00_1845 =
														(BgL_classz00_1828 == BgL_oclassz00_1844);
													if (BgL__ortest_1115z00_1845)
														{	/* Inline/app.scm 67 */
															BgL_res1721z00_1861 = BgL__ortest_1115z00_1845;
														}
													else
														{	/* Inline/app.scm 67 */
															long BgL_odepthz00_1846;

															{	/* Inline/app.scm 67 */
																obj_t BgL_arg1804z00_1847;

																BgL_arg1804z00_1847 = (BgL_oclassz00_1844);
																BgL_odepthz00_1846 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_1847);
															}
															if ((2L < BgL_odepthz00_1846))
																{	/* Inline/app.scm 67 */
																	obj_t BgL_arg1802z00_1849;

																	{	/* Inline/app.scm 67 */
																		obj_t BgL_arg1803z00_1850;

																		BgL_arg1803z00_1850 = (BgL_oclassz00_1844);
																		BgL_arg1802z00_1849 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_1850, 2L);
																	}
																	BgL_res1721z00_1861 =
																		(BgL_arg1802z00_1849 == BgL_classz00_1828);
																}
															else
																{	/* Inline/app.scm 67 */
																	BgL_res1721z00_1861 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1765z00_2536 = BgL_res1721z00_1861;
										}
								}
							else
								{	/* Inline/app.scm 67 */
									BgL_test1765z00_2536 = ((bool_t) 0);
								}
						}
					}
				else
					{	/* Inline/app.scm 67 */
						BgL_test1765z00_2536 = ((bool_t) 0);
					}
				if (BgL_test1765z00_2536)
					{	/* Inline/app.scm 67 */
						if (
							(((obj_t) BgL_varz00_6) ==
								(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																CAR(BgL_stackz00_7))))->BgL_valuez00))))->
									BgL_thezd2closurezd2globalz00)))
							{	/* Inline/app.scm 69 */
								bool_t BgL_test1772z00_2570;

								{	/* Inline/app.scm 69 */
									bool_t BgL__ortest_1108z00_1471;

									{	/* Inline/app.scm 69 */
										obj_t BgL_classz00_1865;

										BgL_classz00_1865 = BGl_localz00zzast_varz00;
										{	/* Inline/app.scm 69 */
											BgL_objectz00_bglt BgL_arg1807z00_1867;

											{	/* Inline/app.scm 69 */
												obj_t BgL_tmpz00_2571;

												BgL_tmpz00_2571 =
													((obj_t) ((BgL_objectz00_bglt) BgL_varz00_6));
												BgL_arg1807z00_1867 =
													(BgL_objectz00_bglt) (BgL_tmpz00_2571);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Inline/app.scm 69 */
													long BgL_idxz00_1873;

													BgL_idxz00_1873 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1867);
													BgL__ortest_1108z00_1471 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1873 + 2L)) == BgL_classz00_1865);
												}
											else
												{	/* Inline/app.scm 69 */
													bool_t BgL_res1722z00_1898;

													{	/* Inline/app.scm 69 */
														obj_t BgL_oclassz00_1881;

														{	/* Inline/app.scm 69 */
															obj_t BgL_arg1815z00_1889;
															long BgL_arg1816z00_1890;

															BgL_arg1815z00_1889 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Inline/app.scm 69 */
																long BgL_arg1817z00_1891;

																BgL_arg1817z00_1891 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1867);
																BgL_arg1816z00_1890 =
																	(BgL_arg1817z00_1891 - OBJECT_TYPE);
															}
															BgL_oclassz00_1881 =
																VECTOR_REF(BgL_arg1815z00_1889,
																BgL_arg1816z00_1890);
														}
														{	/* Inline/app.scm 69 */
															bool_t BgL__ortest_1115z00_1882;

															BgL__ortest_1115z00_1882 =
																(BgL_classz00_1865 == BgL_oclassz00_1881);
															if (BgL__ortest_1115z00_1882)
																{	/* Inline/app.scm 69 */
																	BgL_res1722z00_1898 =
																		BgL__ortest_1115z00_1882;
																}
															else
																{	/* Inline/app.scm 69 */
																	long BgL_odepthz00_1883;

																	{	/* Inline/app.scm 69 */
																		obj_t BgL_arg1804z00_1884;

																		BgL_arg1804z00_1884 = (BgL_oclassz00_1881);
																		BgL_odepthz00_1883 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1884);
																	}
																	if ((2L < BgL_odepthz00_1883))
																		{	/* Inline/app.scm 69 */
																			obj_t BgL_arg1802z00_1886;

																			{	/* Inline/app.scm 69 */
																				obj_t BgL_arg1803z00_1887;

																				BgL_arg1803z00_1887 =
																					(BgL_oclassz00_1881);
																				BgL_arg1802z00_1886 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1887, 2L);
																			}
																			BgL_res1722z00_1898 =
																				(BgL_arg1802z00_1886 ==
																				BgL_classz00_1865);
																		}
																	else
																		{	/* Inline/app.scm 69 */
																			BgL_res1722z00_1898 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL__ortest_1108z00_1471 = BgL_res1722z00_1898;
												}
										}
									}
									if (BgL__ortest_1108z00_1471)
										{	/* Inline/app.scm 69 */
											BgL_test1772z00_2570 = BgL__ortest_1108z00_1471;
										}
									else
										{	/* Inline/app.scm 69 */
											BgL_test1772z00_2570 =
												(
												(((BgL_globalz00_bglt) COBJECT(
															((BgL_globalz00_bglt) BgL_varz00_6)))->
													BgL_importz00) == CNST_TABLE_REF(1));
										}
								}
								if (BgL_test1772z00_2570)
									{	/* Inline/app.scm 69 */
										return
											(
											(((BgL_variablez00_bglt) COBJECT(BgL_varz00_6))->
												BgL_occurrencez00) == 1L);
									}
								else
									{	/* Inline/app.scm 69 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Inline/app.scm 68 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Inline/app.scm 67 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* call-size */
	long BGl_callzd2siza7ez75zzinline_appz00(BgL_appz00_bglt BgL_nodez00_8)
	{
		{	/* Inline/app.scm 75 */
			{	/* Inline/app.scm 77 */
				long BgL_csiza7eza7_1479;

				BgL_csiza7eza7_1479 =
					(1L +
					bgl_list_length(
						(((BgL_appz00_bglt) COBJECT(BgL_nodez00_8))->BgL_argsz00)));
				if (CBOOL(BGl_za2optimzd2atomzd2inliningzf3za2zf3zzengine_paramz00))
					{	/* Inline/app.scm 79 */
						obj_t BgL_atomsz00_1480;

						{	/* Inline/app.scm 79 */
							obj_t BgL_hook1242z00_1482;

							BgL_hook1242z00_1482 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
							{	/* Inline/app.scm 79 */
								obj_t BgL_g1243z00_1483;

								BgL_g1243z00_1483 =
									(((BgL_appz00_bglt) COBJECT(BgL_nodez00_8))->BgL_argsz00);
								{
									obj_t BgL_l1239z00_1485;
									obj_t BgL_h1240z00_1486;

									BgL_l1239z00_1485 = BgL_g1243z00_1483;
									BgL_h1240z00_1486 = BgL_hook1242z00_1482;
								BgL_zc3z04anonymousza31321ze3z87_1487:
									if (NULLP(BgL_l1239z00_1485))
										{	/* Inline/app.scm 79 */
											BgL_atomsz00_1480 = CDR(BgL_hook1242z00_1482);
										}
									else
										{	/* Inline/app.scm 79 */
											bool_t BgL_test1780z00_2611;

											{	/* Inline/app.scm 80 */
												obj_t BgL_xz00_1496;

												BgL_xz00_1496 = CAR(((obj_t) BgL_l1239z00_1485));
												{	/* Inline/app.scm 80 */
													bool_t BgL__ortest_1111z00_1497;

													{	/* Inline/app.scm 80 */
														obj_t BgL_classz00_1905;

														BgL_classz00_1905 = BGl_atomz00zzast_nodez00;
														if (BGL_OBJECTP(BgL_xz00_1496))
															{	/* Inline/app.scm 80 */
																BgL_objectz00_bglt BgL_arg1807z00_1907;

																BgL_arg1807z00_1907 =
																	(BgL_objectz00_bglt) (BgL_xz00_1496);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Inline/app.scm 80 */
																		long BgL_idxz00_1913;

																		BgL_idxz00_1913 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_1907);
																		BgL__ortest_1111z00_1497 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_1913 + 2L)) ==
																			BgL_classz00_1905);
																	}
																else
																	{	/* Inline/app.scm 80 */
																		bool_t BgL_res1723z00_1938;

																		{	/* Inline/app.scm 80 */
																			obj_t BgL_oclassz00_1921;

																			{	/* Inline/app.scm 80 */
																				obj_t BgL_arg1815z00_1929;
																				long BgL_arg1816z00_1930;

																				BgL_arg1815z00_1929 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Inline/app.scm 80 */
																					long BgL_arg1817z00_1931;

																					BgL_arg1817z00_1931 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_1907);
																					BgL_arg1816z00_1930 =
																						(BgL_arg1817z00_1931 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_1921 =
																					VECTOR_REF(BgL_arg1815z00_1929,
																					BgL_arg1816z00_1930);
																			}
																			{	/* Inline/app.scm 80 */
																				bool_t BgL__ortest_1115z00_1922;

																				BgL__ortest_1115z00_1922 =
																					(BgL_classz00_1905 ==
																					BgL_oclassz00_1921);
																				if (BgL__ortest_1115z00_1922)
																					{	/* Inline/app.scm 80 */
																						BgL_res1723z00_1938 =
																							BgL__ortest_1115z00_1922;
																					}
																				else
																					{	/* Inline/app.scm 80 */
																						long BgL_odepthz00_1923;

																						{	/* Inline/app.scm 80 */
																							obj_t BgL_arg1804z00_1924;

																							BgL_arg1804z00_1924 =
																								(BgL_oclassz00_1921);
																							BgL_odepthz00_1923 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_1924);
																						}
																						if ((2L < BgL_odepthz00_1923))
																							{	/* Inline/app.scm 80 */
																								obj_t BgL_arg1802z00_1926;

																								{	/* Inline/app.scm 80 */
																									obj_t BgL_arg1803z00_1927;

																									BgL_arg1803z00_1927 =
																										(BgL_oclassz00_1921);
																									BgL_arg1802z00_1926 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_1927, 2L);
																								}
																								BgL_res1723z00_1938 =
																									(BgL_arg1802z00_1926 ==
																									BgL_classz00_1905);
																							}
																						else
																							{	/* Inline/app.scm 80 */
																								BgL_res1723z00_1938 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL__ortest_1111z00_1497 =
																			BgL_res1723z00_1938;
																	}
															}
														else
															{	/* Inline/app.scm 80 */
																BgL__ortest_1111z00_1497 = ((bool_t) 0);
															}
													}
													if (BgL__ortest_1111z00_1497)
														{	/* Inline/app.scm 80 */
															BgL_test1780z00_2611 = BgL__ortest_1111z00_1497;
														}
													else
														{	/* Inline/app.scm 80 */
															obj_t BgL_classz00_1939;

															BgL_classz00_1939 = BGl_kwotez00zzast_nodez00;
															if (BGL_OBJECTP(BgL_xz00_1496))
																{	/* Inline/app.scm 80 */
																	BgL_objectz00_bglt BgL_arg1807z00_1941;

																	BgL_arg1807z00_1941 =
																		(BgL_objectz00_bglt) (BgL_xz00_1496);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Inline/app.scm 80 */
																			long BgL_idxz00_1947;

																			BgL_idxz00_1947 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_1941);
																			BgL_test1780z00_2611 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_1947 + 2L)) ==
																				BgL_classz00_1939);
																		}
																	else
																		{	/* Inline/app.scm 80 */
																			bool_t BgL_res1724z00_1972;

																			{	/* Inline/app.scm 80 */
																				obj_t BgL_oclassz00_1955;

																				{	/* Inline/app.scm 80 */
																					obj_t BgL_arg1815z00_1963;
																					long BgL_arg1816z00_1964;

																					BgL_arg1815z00_1963 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Inline/app.scm 80 */
																						long BgL_arg1817z00_1965;

																						BgL_arg1817z00_1965 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_1941);
																						BgL_arg1816z00_1964 =
																							(BgL_arg1817z00_1965 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_1955 =
																						VECTOR_REF(BgL_arg1815z00_1963,
																						BgL_arg1816z00_1964);
																				}
																				{	/* Inline/app.scm 80 */
																					bool_t BgL__ortest_1115z00_1956;

																					BgL__ortest_1115z00_1956 =
																						(BgL_classz00_1939 ==
																						BgL_oclassz00_1955);
																					if (BgL__ortest_1115z00_1956)
																						{	/* Inline/app.scm 80 */
																							BgL_res1724z00_1972 =
																								BgL__ortest_1115z00_1956;
																						}
																					else
																						{	/* Inline/app.scm 80 */
																							long BgL_odepthz00_1957;

																							{	/* Inline/app.scm 80 */
																								obj_t BgL_arg1804z00_1958;

																								BgL_arg1804z00_1958 =
																									(BgL_oclassz00_1955);
																								BgL_odepthz00_1957 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_1958);
																							}
																							if ((2L < BgL_odepthz00_1957))
																								{	/* Inline/app.scm 80 */
																									obj_t BgL_arg1802z00_1960;

																									{	/* Inline/app.scm 80 */
																										obj_t BgL_arg1803z00_1961;

																										BgL_arg1803z00_1961 =
																											(BgL_oclassz00_1955);
																										BgL_arg1802z00_1960 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_1961, 2L);
																									}
																									BgL_res1724z00_1972 =
																										(BgL_arg1802z00_1960 ==
																										BgL_classz00_1939);
																								}
																							else
																								{	/* Inline/app.scm 80 */
																									BgL_res1724z00_1972 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1780z00_2611 =
																				BgL_res1724z00_1972;
																		}
																}
															else
																{	/* Inline/app.scm 80 */
																	BgL_test1780z00_2611 = ((bool_t) 0);
																}
														}
												}
											}
											if (BgL_test1780z00_2611)
												{	/* Inline/app.scm 79 */
													obj_t BgL_nh1241z00_1492;

													{	/* Inline/app.scm 79 */
														obj_t BgL_arg1326z00_1494;

														BgL_arg1326z00_1494 =
															CAR(((obj_t) BgL_l1239z00_1485));
														BgL_nh1241z00_1492 =
															MAKE_YOUNG_PAIR(BgL_arg1326z00_1494, BNIL);
													}
													SET_CDR(BgL_h1240z00_1486, BgL_nh1241z00_1492);
													{	/* Inline/app.scm 79 */
														obj_t BgL_arg1325z00_1493;

														BgL_arg1325z00_1493 =
															CDR(((obj_t) BgL_l1239z00_1485));
														{
															obj_t BgL_h1240z00_2666;
															obj_t BgL_l1239z00_2665;

															BgL_l1239z00_2665 = BgL_arg1325z00_1493;
															BgL_h1240z00_2666 = BgL_nh1241z00_1492;
															BgL_h1240z00_1486 = BgL_h1240z00_2666;
															BgL_l1239z00_1485 = BgL_l1239z00_2665;
															goto BgL_zc3z04anonymousza31321ze3z87_1487;
														}
													}
												}
											else
												{	/* Inline/app.scm 79 */
													obj_t BgL_arg1327z00_1495;

													BgL_arg1327z00_1495 =
														CDR(((obj_t) BgL_l1239z00_1485));
													{
														obj_t BgL_l1239z00_2669;

														BgL_l1239z00_2669 = BgL_arg1327z00_1495;
														BgL_l1239z00_1485 = BgL_l1239z00_2669;
														goto BgL_zc3z04anonymousza31321ze3z87_1487;
													}
												}
										}
								}
							}
						}
						return (BgL_csiza7eza7_1479 + bgl_list_length(BgL_atomsz00_1480));
					}
				else
					{	/* Inline/app.scm 78 */
						return BgL_csiza7eza7_1479;
					}
			}
		}

	}



/* <@exit:1330>~0 */
	obj_t BGl_zc3z04exitza31330ze3ze70z60zzinline_appz00(obj_t BgL_nodez00_2390)
	{
		{	/* Inline/app.scm 89 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1112z00_1502;

			if (SET_EXIT(BgL_an_exit1112z00_1502))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1112z00_1502 = (void *) jmpbuf;
					{	/* Inline/app.scm 89 */
						obj_t BgL_env1116z00_1503;

						BgL_env1116z00_1503 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1116z00_1503, BgL_an_exit1112z00_1502, 1L);
						{	/* Inline/app.scm 89 */
							obj_t BgL_an_exitd1113z00_1504;

							BgL_an_exitd1113z00_1504 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1116z00_1503);
							{	/* Inline/app.scm 89 */
								bool_t BgL_res1115z00_1507;

								{	/* Inline/app.scm 90 */
									obj_t BgL_zc3z04anonymousza31332ze3z87_2381;

									BgL_zc3z04anonymousza31332ze3z87_2381 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31332ze3ze5zzinline_appz00,
										(int) (1L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31332ze3z87_2381,
										(int) (0L), BgL_an_exitd1113z00_1504);
									BGl_nodezd2walkzd2zzast_nodez00(((BgL_nodez00_bglt)
											BgL_nodez00_2390), BgL_zc3z04anonymousza31332ze3z87_2381);
								}
								BgL_res1115z00_1507 = ((bool_t) 0);
								POP_ENV_EXIT(BgL_env1116z00_1503);
								return BBOOL(BgL_res1115z00_1507);
							}
						}
					}
				}
		}

	}



/* &<@anonymous:1332> */
	obj_t BGl_z62zc3z04anonymousza31332ze3ze5zzinline_appz00(obj_t
		BgL_envz00_2382, obj_t BgL_nz00_2384)
	{
		{	/* Inline/app.scm 90 */
			{	/* Inline/app.scm 90 */
				obj_t BgL_an_exitd1113z00_2383;

				BgL_an_exitd1113z00_2383 = PROCEDURE_REF(BgL_envz00_2382, (int) (0L));
				{	/* Inline/app.scm 90 */
					bool_t BgL_test1790z00_2688;

					{	/* Inline/app.scm 90 */
						obj_t BgL_classz00_2402;

						BgL_classz00_2402 = BGl_kwotez00zzast_nodez00;
						if (BGL_OBJECTP(BgL_nz00_2384))
							{	/* Inline/app.scm 90 */
								BgL_objectz00_bglt BgL_arg1807z00_2403;

								BgL_arg1807z00_2403 = (BgL_objectz00_bglt) (BgL_nz00_2384);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Inline/app.scm 90 */
										long BgL_idxz00_2404;

										BgL_idxz00_2404 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2403);
										BgL_test1790z00_2688 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2404 + 2L)) == BgL_classz00_2402);
									}
								else
									{	/* Inline/app.scm 90 */
										bool_t BgL_res1725z00_2407;

										{	/* Inline/app.scm 90 */
											obj_t BgL_oclassz00_2408;

											{	/* Inline/app.scm 90 */
												obj_t BgL_arg1815z00_2409;
												long BgL_arg1816z00_2410;

												BgL_arg1815z00_2409 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Inline/app.scm 90 */
													long BgL_arg1817z00_2411;

													BgL_arg1817z00_2411 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2403);
													BgL_arg1816z00_2410 =
														(BgL_arg1817z00_2411 - OBJECT_TYPE);
												}
												BgL_oclassz00_2408 =
													VECTOR_REF(BgL_arg1815z00_2409, BgL_arg1816z00_2410);
											}
											{	/* Inline/app.scm 90 */
												bool_t BgL__ortest_1115z00_2412;

												BgL__ortest_1115z00_2412 =
													(BgL_classz00_2402 == BgL_oclassz00_2408);
												if (BgL__ortest_1115z00_2412)
													{	/* Inline/app.scm 90 */
														BgL_res1725z00_2407 = BgL__ortest_1115z00_2412;
													}
												else
													{	/* Inline/app.scm 90 */
														long BgL_odepthz00_2413;

														{	/* Inline/app.scm 90 */
															obj_t BgL_arg1804z00_2414;

															BgL_arg1804z00_2414 = (BgL_oclassz00_2408);
															BgL_odepthz00_2413 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2414);
														}
														if ((2L < BgL_odepthz00_2413))
															{	/* Inline/app.scm 90 */
																obj_t BgL_arg1802z00_2415;

																{	/* Inline/app.scm 90 */
																	obj_t BgL_arg1803z00_2416;

																	BgL_arg1803z00_2416 = (BgL_oclassz00_2408);
																	BgL_arg1802z00_2415 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2416,
																		2L);
																}
																BgL_res1725z00_2407 =
																	(BgL_arg1802z00_2415 == BgL_classz00_2402);
															}
														else
															{	/* Inline/app.scm 90 */
																BgL_res1725z00_2407 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1790z00_2688 = BgL_res1725z00_2407;
									}
							}
						else
							{	/* Inline/app.scm 90 */
								BgL_test1790z00_2688 = ((bool_t) 0);
							}
					}
					if (BgL_test1790z00_2688)
						{	/* Inline/app.scm 90 */
							return
								unwind_stack_until(BgL_an_exitd1113z00_2383, BFALSE, BTRUE,
								BFALSE, BFALSE);
						}
					else
						{	/* Inline/app.scm 90 */
							return BFALSE;
						}
				}
			}
		}

	}



/* inline-app? */
	BGL_EXPORTED_DEF bool_t
		BGl_inlinezd2appzf3z21zzinline_appz00(BgL_variablez00_bglt BgL_varz00_10,
		long BgL_kfactorz00_11, long BgL_callzd2siza7ez75_12, obj_t BgL_stackz00_13)
	{
		{	/* Inline/app.scm 96 */
			{	/* Inline/app.scm 101 */
				BgL_valuez00_bglt BgL_sfunz00_1513;

				BgL_sfunz00_1513 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_10))->BgL_valuez00);
				{	/* Inline/app.scm 101 */
					obj_t BgL_bodyz00_1514;

					{	/* Inline/app.scm 102 */
						bool_t BgL_test1795z00_2713;

						{	/* Inline/app.scm 102 */
							obj_t BgL_classz00_2015;

							BgL_classz00_2015 = BGl_isfunz00zzinline_inlinez00;
							{	/* Inline/app.scm 102 */
								BgL_objectz00_bglt BgL_arg1807z00_2017;

								{	/* Inline/app.scm 102 */
									obj_t BgL_tmpz00_2714;

									BgL_tmpz00_2714 =
										((obj_t) ((BgL_objectz00_bglt) BgL_sfunz00_1513));
									BgL_arg1807z00_2017 = (BgL_objectz00_bglt) (BgL_tmpz00_2714);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Inline/app.scm 102 */
										long BgL_idxz00_2023;

										BgL_idxz00_2023 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2017);
										BgL_test1795z00_2713 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2023 + 4L)) == BgL_classz00_2015);
									}
								else
									{	/* Inline/app.scm 102 */
										bool_t BgL_res1726z00_2048;

										{	/* Inline/app.scm 102 */
											obj_t BgL_oclassz00_2031;

											{	/* Inline/app.scm 102 */
												obj_t BgL_arg1815z00_2039;
												long BgL_arg1816z00_2040;

												BgL_arg1815z00_2039 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Inline/app.scm 102 */
													long BgL_arg1817z00_2041;

													BgL_arg1817z00_2041 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2017);
													BgL_arg1816z00_2040 =
														(BgL_arg1817z00_2041 - OBJECT_TYPE);
												}
												BgL_oclassz00_2031 =
													VECTOR_REF(BgL_arg1815z00_2039, BgL_arg1816z00_2040);
											}
											{	/* Inline/app.scm 102 */
												bool_t BgL__ortest_1115z00_2032;

												BgL__ortest_1115z00_2032 =
													(BgL_classz00_2015 == BgL_oclassz00_2031);
												if (BgL__ortest_1115z00_2032)
													{	/* Inline/app.scm 102 */
														BgL_res1726z00_2048 = BgL__ortest_1115z00_2032;
													}
												else
													{	/* Inline/app.scm 102 */
														long BgL_odepthz00_2033;

														{	/* Inline/app.scm 102 */
															obj_t BgL_arg1804z00_2034;

															BgL_arg1804z00_2034 = (BgL_oclassz00_2031);
															BgL_odepthz00_2033 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2034);
														}
														if ((4L < BgL_odepthz00_2033))
															{	/* Inline/app.scm 102 */
																obj_t BgL_arg1802z00_2036;

																{	/* Inline/app.scm 102 */
																	obj_t BgL_arg1803z00_2037;

																	BgL_arg1803z00_2037 = (BgL_oclassz00_2031);
																	BgL_arg1802z00_2036 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2037,
																		4L);
																}
																BgL_res1726z00_2048 =
																	(BgL_arg1802z00_2036 == BgL_classz00_2015);
															}
														else
															{	/* Inline/app.scm 102 */
																BgL_res1726z00_2048 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1795z00_2713 = BgL_res1726z00_2048;
									}
							}
						}
						if (BgL_test1795z00_2713)
							{
								BgL_nodez00_bglt BgL_auxz00_2737;

								{
									BgL_isfunz00_bglt BgL_auxz00_2738;

									{
										obj_t BgL_auxz00_2739;

										{	/* Inline/app.scm 103 */
											BgL_objectz00_bglt BgL_tmpz00_2740;

											BgL_tmpz00_2740 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_sfunz00_1513));
											BgL_auxz00_2739 = BGL_OBJECT_WIDENING(BgL_tmpz00_2740);
										}
										BgL_auxz00_2738 = ((BgL_isfunz00_bglt) BgL_auxz00_2739);
									}
									BgL_auxz00_2737 =
										(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_2738))->
										BgL_originalzd2bodyzd2);
								}
								BgL_bodyz00_1514 = ((obj_t) BgL_auxz00_2737);
							}
						else
							{	/* Inline/app.scm 102 */
								BgL_bodyz00_1514 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_sfunz00_1513)))->BgL_bodyz00);
							}
					}
					{	/* Inline/app.scm 102 */

						{	/* Inline/app.scm 106 */
							bool_t BgL_test1801z00_2749;

							{	/* Inline/app.scm 106 */
								obj_t BgL_classz00_2052;

								BgL_classz00_2052 = BGl_nodez00zzast_nodez00;
								if (BGL_OBJECTP(BgL_bodyz00_1514))
									{	/* Inline/app.scm 106 */
										BgL_objectz00_bglt BgL_arg1807z00_2054;

										BgL_arg1807z00_2054 =
											(BgL_objectz00_bglt) (BgL_bodyz00_1514);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Inline/app.scm 106 */
												long BgL_idxz00_2060;

												BgL_idxz00_2060 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2054);
												BgL_test1801z00_2749 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2060 + 1L)) == BgL_classz00_2052);
											}
										else
											{	/* Inline/app.scm 106 */
												bool_t BgL_res1727z00_2085;

												{	/* Inline/app.scm 106 */
													obj_t BgL_oclassz00_2068;

													{	/* Inline/app.scm 106 */
														obj_t BgL_arg1815z00_2076;
														long BgL_arg1816z00_2077;

														BgL_arg1815z00_2076 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Inline/app.scm 106 */
															long BgL_arg1817z00_2078;

															BgL_arg1817z00_2078 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2054);
															BgL_arg1816z00_2077 =
																(BgL_arg1817z00_2078 - OBJECT_TYPE);
														}
														BgL_oclassz00_2068 =
															VECTOR_REF(BgL_arg1815z00_2076,
															BgL_arg1816z00_2077);
													}
													{	/* Inline/app.scm 106 */
														bool_t BgL__ortest_1115z00_2069;

														BgL__ortest_1115z00_2069 =
															(BgL_classz00_2052 == BgL_oclassz00_2068);
														if (BgL__ortest_1115z00_2069)
															{	/* Inline/app.scm 106 */
																BgL_res1727z00_2085 = BgL__ortest_1115z00_2069;
															}
														else
															{	/* Inline/app.scm 106 */
																long BgL_odepthz00_2070;

																{	/* Inline/app.scm 106 */
																	obj_t BgL_arg1804z00_2071;

																	BgL_arg1804z00_2071 = (BgL_oclassz00_2068);
																	BgL_odepthz00_2070 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2071);
																}
																if ((1L < BgL_odepthz00_2070))
																	{	/* Inline/app.scm 106 */
																		obj_t BgL_arg1802z00_2073;

																		{	/* Inline/app.scm 106 */
																			obj_t BgL_arg1803z00_2074;

																			BgL_arg1803z00_2074 =
																				(BgL_oclassz00_2068);
																			BgL_arg1802z00_2073 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2074, 1L);
																		}
																		BgL_res1727z00_2085 =
																			(BgL_arg1802z00_2073 ==
																			BgL_classz00_2052);
																	}
																else
																	{	/* Inline/app.scm 106 */
																		BgL_res1727z00_2085 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1801z00_2749 = BgL_res1727z00_2085;
											}
									}
								else
									{	/* Inline/app.scm 106 */
										BgL_test1801z00_2749 = ((bool_t) 0);
									}
							}
							if (BgL_test1801z00_2749)
								{	/* Inline/app.scm 106 */
									if (CBOOL(BGl_za2inliningzf3za2zf3zzengine_paramz00))
										{	/* Inline/app.scm 113 */
											bool_t BgL_test1810z00_2774;

											if (CBOOL
												(BGl_za2optimzd2loopzd2inliningzf3za2zf3zzengine_paramz00))
												{	/* Inline/app.scm 113 */
													if (CBOOL
														(BGl_za2optimzd2unrollzd2loopzf3za2zf3zzengine_paramz00))
														{	/* Inline/app.scm 114 */
															BgL_test1810z00_2774 = ((bool_t) 0);
														}
													else
														{	/* Inline/app.scm 114 */
															BgL_test1810z00_2774 =
																CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
																	((obj_t) BgL_varz00_10), BgL_stackz00_13));
														}
												}
											else
												{	/* Inline/app.scm 113 */
													BgL_test1810z00_2774 = ((bool_t) 0);
												}
											if (BgL_test1810z00_2774)
												{	/* Inline/app.scm 113 */
													return ((bool_t) 0);
												}
											else
												{	/* Inline/app.scm 113 */
													if (
														((((BgL_sfunz00_bglt) COBJECT(
																		((BgL_sfunz00_bglt) BgL_sfunz00_1513)))->
																BgL_classz00) == CNST_TABLE_REF(2)))
														{	/* Inline/app.scm 119 */
															return ((bool_t) 0);
														}
													else
														{	/* Inline/app.scm 123 */
															bool_t BgL_test1814z00_2787;

															if (
																(BGl_za2inlinezd2modeza2zd2zzinline_walkz00 ==
																	CNST_TABLE_REF(3)))
																{	/* Inline/app.scm 123 */
																	BgL_test1814z00_2787 = ((bool_t) 0);
																}
															else
																{	/* Inline/app.scm 123 */
																	if (
																		((((BgL_sfunz00_bglt) COBJECT(
																						((BgL_sfunz00_bglt)
																							BgL_sfunz00_1513)))->
																				BgL_classz00) == CNST_TABLE_REF(0)))
																		{	/* Inline/app.scm 124 */
																			BgL_test1814z00_2787 =
																				BGl_iszd2recursivezf3z21zzinline_recursionz00
																				(BgL_varz00_10);
																		}
																	else
																		{	/* Inline/app.scm 124 */
																			BgL_test1814z00_2787 = ((bool_t) 0);
																		}
																}
															if (BgL_test1814z00_2787)
																{	/* Inline/app.scm 123 */
																	return ((bool_t) 0);
																}
															else
																{	/* Inline/app.scm 123 */
																	if (
																		((((BgL_sfunz00_bglt) COBJECT(
																						((BgL_sfunz00_bglt)
																							BgL_sfunz00_1513)))->
																				BgL_classz00) == CNST_TABLE_REF(4)))
																		{	/* Inline/app.scm 129 */
																			return ((bool_t) 0);
																		}
																	else
																		{	/* Inline/app.scm 133 */
																			bool_t BgL_test1818z00_2802;

																			if (
																				((((BgL_sfunz00_bglt) COBJECT(
																								((BgL_sfunz00_bglt)
																									BgL_sfunz00_1513)))->
																						BgL_classz00) == CNST_TABLE_REF(0)))
																				{	/* Inline/app.scm 133 */
																					if (CBOOL
																						(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																							(((obj_t) BgL_varz00_10),
																								BgL_stackz00_13)))
																						{	/* Inline/app.scm 134 */
																							BgL_test1818z00_2802 =
																								((bool_t) 0);
																						}
																					else
																						{	/* Inline/app.scm 135 */
																							bool_t BgL_test1822z00_2812;

																							if (
																								(BGl_za2inlinezd2modeza2zd2zzinline_walkz00
																									== CNST_TABLE_REF(5)))
																								{	/* Inline/app.scm 136 */
																									bool_t BgL_test1825z00_2816;

																									{	/* Inline/app.scm 136 */
																										obj_t BgL_arg1540z00_1578;

																										BgL_arg1540z00_1578 =
																											(((BgL_sfunz00_bglt)
																												COBJECT((
																														(BgL_sfunz00_bglt)
																														BgL_sfunz00_1513)))->
																											BgL_bodyz00);
																										BgL_test1825z00_2816 =
																											CBOOL
																											(BGl_zc3z04exitza31330ze3ze70z60zzinline_appz00
																											(BgL_arg1540z00_1578));
																									}
																									if (BgL_test1825z00_2816)
																										{	/* Inline/app.scm 136 */
																											BgL_test1822z00_2812 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Inline/app.scm 136 */
																											BgL_test1822z00_2812 =
																												((bool_t) 1);
																										}
																								}
																							else
																								{	/* Inline/app.scm 135 */
																									BgL_test1822z00_2812 =
																										((bool_t) 1);
																								}
																							if (BgL_test1822z00_2812)
																								{	/* Inline/app.scm 137 */
																									bool_t
																										BgL__ortest_1118z00_1576;
																									if (
																										(BGl_za2inlinezd2modeza2zd2zzinline_walkz00
																											== CNST_TABLE_REF(6)))
																										{	/* Inline/app.scm 137 */
																											BgL__ortest_1118z00_1576 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Inline/app.scm 137 */
																											BgL__ortest_1118z00_1576 =
																												((bool_t) 1);
																										}
																									if (BgL__ortest_1118z00_1576)
																										{	/* Inline/app.scm 137 */
																											BgL_test1818z00_2802 =
																												BgL__ortest_1118z00_1576;
																										}
																									else
																										{	/* Inline/app.scm 137 */
																											BgL_test1818z00_2802 =
																												CBOOL(
																												(((BgL_funz00_bglt)
																														COBJECT((
																																(BgL_funz00_bglt)
																																BgL_sfunz00_1513)))->
																													BgL_predicatezd2ofzd2));
																										}
																								}
																							else
																								{	/* Inline/app.scm 135 */
																									BgL_test1818z00_2802 =
																										((bool_t) 0);
																								}
																						}
																				}
																			else
																				{	/* Inline/app.scm 133 */
																					BgL_test1818z00_2802 = ((bool_t) 0);
																				}
																			if (BgL_test1818z00_2802)
																				{	/* Inline/app.scm 133 */
																					return ((bool_t) 1);
																				}
																			else
																				{	/* Inline/app.scm 142 */
																					bool_t BgL_test1828z00_2828;

																					{	/* Inline/app.scm 142 */
																						bool_t BgL_test1829z00_2829;

																						{	/* Inline/app.scm 142 */
																							obj_t BgL_classz00_2092;

																							BgL_classz00_2092 =
																								BGl_globalz00zzast_varz00;
																							{	/* Inline/app.scm 142 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_2094;
																								{	/* Inline/app.scm 142 */
																									obj_t BgL_tmpz00_2830;

																									BgL_tmpz00_2830 =
																										((obj_t) BgL_varz00_10);
																									BgL_arg1807z00_2094 =
																										(BgL_objectz00_bglt)
																										(BgL_tmpz00_2830);
																								}
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* Inline/app.scm 142 */
																										long BgL_idxz00_2100;

																										BgL_idxz00_2100 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_2094);
																										BgL_test1829z00_2829 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_2100 +
																													2L)) ==
																											BgL_classz00_2092);
																									}
																								else
																									{	/* Inline/app.scm 142 */
																										bool_t BgL_res1728z00_2125;

																										{	/* Inline/app.scm 142 */
																											obj_t BgL_oclassz00_2108;

																											{	/* Inline/app.scm 142 */
																												obj_t
																													BgL_arg1815z00_2116;
																												long
																													BgL_arg1816z00_2117;
																												BgL_arg1815z00_2116 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* Inline/app.scm 142 */
																													long
																														BgL_arg1817z00_2118;
																													BgL_arg1817z00_2118 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_2094);
																													BgL_arg1816z00_2117 =
																														(BgL_arg1817z00_2118
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_2108 =
																													VECTOR_REF
																													(BgL_arg1815z00_2116,
																													BgL_arg1816z00_2117);
																											}
																											{	/* Inline/app.scm 142 */
																												bool_t
																													BgL__ortest_1115z00_2109;
																												BgL__ortest_1115z00_2109
																													=
																													(BgL_classz00_2092 ==
																													BgL_oclassz00_2108);
																												if (BgL__ortest_1115z00_2109)
																													{	/* Inline/app.scm 142 */
																														BgL_res1728z00_2125
																															=
																															BgL__ortest_1115z00_2109;
																													}
																												else
																													{	/* Inline/app.scm 142 */
																														long
																															BgL_odepthz00_2110;
																														{	/* Inline/app.scm 142 */
																															obj_t
																																BgL_arg1804z00_2111;
																															BgL_arg1804z00_2111
																																=
																																(BgL_oclassz00_2108);
																															BgL_odepthz00_2110
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_2111);
																														}
																														if (
																															(2L <
																																BgL_odepthz00_2110))
																															{	/* Inline/app.scm 142 */
																																obj_t
																																	BgL_arg1802z00_2113;
																																{	/* Inline/app.scm 142 */
																																	obj_t
																																		BgL_arg1803z00_2114;
																																	BgL_arg1803z00_2114
																																		=
																																		(BgL_oclassz00_2108);
																																	BgL_arg1802z00_2113
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_2114,
																																		2L);
																																}
																																BgL_res1728z00_2125
																																	=
																																	(BgL_arg1802z00_2113
																																	==
																																	BgL_classz00_2092);
																															}
																														else
																															{	/* Inline/app.scm 142 */
																																BgL_res1728z00_2125
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test1829z00_2829 =
																											BgL_res1728z00_2125;
																									}
																							}
																						}
																						if (BgL_test1829z00_2829)
																							{	/* Inline/app.scm 142 */
																								BgL_test1828z00_2828 =
																									(
																									(((BgL_globalz00_bglt)
																											COBJECT((
																													(BgL_globalz00_bglt)
																													BgL_varz00_10)))->
																										BgL_importz00) ==
																									CNST_TABLE_REF(7));
																							}
																						else
																							{	/* Inline/app.scm 142 */
																								BgL_test1828z00_2828 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test1828z00_2828)
																						{	/* Inline/app.scm 142 */
																							return ((bool_t) 0);
																						}
																					else
																						{	/* Inline/app.scm 142 */
																							if (CBOOL
																								(BGl_za2userzd2inliningzf3za2z21zzengine_paramz00))
																								{	/* Inline/app.scm 149 */
																									bool_t BgL_test1835z00_2858;

																									{	/* Inline/app.scm 149 */
																										long BgL_arg1514z00_1569;
																										long BgL_arg1516z00_1570;

																										BgL_arg1514z00_1569 =
																											BGl_nodezd2siza7ez75zzinline_siza7eza7
																											(((BgL_nodez00_bglt)
																												BgL_bodyz00_1514));
																										BgL_arg1516z00_1570 =
																											(BgL_kfactorz00_11 *
																											BgL_callzd2siza7ez75_12);
																										BgL_test1835z00_2858 =
																											(BgL_arg1514z00_1569 <
																											BgL_arg1516z00_1570);
																									}
																									if (BgL_test1835z00_2858)
																										{	/* Inline/app.scm 149 */
																											return ((bool_t) 1);
																										}
																									else
																										{	/* Inline/app.scm 157 */
																											bool_t
																												BgL_test1836z00_2863;
																											{	/* Inline/app.scm 157 */
																												bool_t
																													BgL_test1837z00_2864;
																												{	/* Inline/app.scm 157 */
																													long
																														BgL_arg1513z00_1568;
																													BgL_arg1513z00_1568 =
																														BGl_nodezd2siza7ez75zzinline_siza7eza7
																														(((BgL_nodez00_bglt)
																															BgL_bodyz00_1514));
																													BgL_test1837z00_2864 =
																														(BgL_arg1513z00_1568
																														==
																														BgL_callzd2siza7ez75_12);
																												}
																												if (BgL_test1837z00_2864)
																													{	/* Inline/app.scm 157 */
																														if (CBOOL
																															(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																(((obj_t)
																																		BgL_varz00_10),
																																	BgL_stackz00_13)))
																															{	/* Inline/app.scm 157 */
																																BgL_test1836z00_2863
																																	=
																																	((bool_t) 0);
																															}
																														else
																															{	/* Inline/app.scm 157 */
																																BgL_test1836z00_2863
																																	=
																																	((bool_t) 1);
																															}
																													}
																												else
																													{	/* Inline/app.scm 157 */
																														BgL_test1836z00_2863
																															= ((bool_t) 0);
																													}
																											}
																											if (BgL_test1836z00_2863)
																												{	/* Inline/app.scm 157 */
																													return ((bool_t) 1);
																												}
																											else
																												{	/* Inline/app.scm 165 */
																													bool_t
																														BgL_test1839z00_2872;
																													if (((((BgL_variablez00_bglt) COBJECT(BgL_varz00_10))->BgL_occurrencez00) == 1L))
																														{	/* Inline/app.scm 166 */
																															bool_t
																																BgL_test1841z00_2876;
																															{	/* Inline/app.scm 166 */
																																bool_t
																																	BgL_test1842z00_2877;
																																{	/* Inline/app.scm 166 */
																																	obj_t
																																		BgL_classz00_2135;
																																	BgL_classz00_2135
																																		=
																																		BGl_globalz00zzast_varz00;
																																	{	/* Inline/app.scm 166 */
																																		BgL_objectz00_bglt
																																			BgL_arg1807z00_2137;
																																		{	/* Inline/app.scm 166 */
																																			obj_t
																																				BgL_tmpz00_2878;
																																			BgL_tmpz00_2878
																																				=
																																				((obj_t)
																																				BgL_varz00_10);
																																			BgL_arg1807z00_2137
																																				=
																																				(BgL_objectz00_bglt)
																																				(BgL_tmpz00_2878);
																																		}
																																		if (BGL_CONDEXPAND_ISA_ARCH64())
																																			{	/* Inline/app.scm 166 */
																																				long
																																					BgL_idxz00_2143;
																																				BgL_idxz00_2143
																																					=
																																					BGL_OBJECT_INHERITANCE_NUM
																																					(BgL_arg1807z00_2137);
																																				BgL_test1842z00_2877
																																					=
																																					(VECTOR_REF
																																					(BGl_za2inheritancesza2z00zz__objectz00,
																																						(BgL_idxz00_2143
																																							+
																																							2L))
																																					==
																																					BgL_classz00_2135);
																																			}
																																		else
																																			{	/* Inline/app.scm 166 */
																																				bool_t
																																					BgL_res1729z00_2168;
																																				{	/* Inline/app.scm 166 */
																																					obj_t
																																						BgL_oclassz00_2151;
																																					{	/* Inline/app.scm 166 */
																																						obj_t
																																							BgL_arg1815z00_2159;
																																						long
																																							BgL_arg1816z00_2160;
																																						BgL_arg1815z00_2159
																																							=
																																							(BGl_za2classesza2z00zz__objectz00);
																																						{	/* Inline/app.scm 166 */
																																							long
																																								BgL_arg1817z00_2161;
																																							BgL_arg1817z00_2161
																																								=
																																								BGL_OBJECT_CLASS_NUM
																																								(BgL_arg1807z00_2137);
																																							BgL_arg1816z00_2160
																																								=
																																								(BgL_arg1817z00_2161
																																								-
																																								OBJECT_TYPE);
																																						}
																																						BgL_oclassz00_2151
																																							=
																																							VECTOR_REF
																																							(BgL_arg1815z00_2159,
																																							BgL_arg1816z00_2160);
																																					}
																																					{	/* Inline/app.scm 166 */
																																						bool_t
																																							BgL__ortest_1115z00_2152;
																																						BgL__ortest_1115z00_2152
																																							=
																																							(BgL_classz00_2135
																																							==
																																							BgL_oclassz00_2151);
																																						if (BgL__ortest_1115z00_2152)
																																							{	/* Inline/app.scm 166 */
																																								BgL_res1729z00_2168
																																									=
																																									BgL__ortest_1115z00_2152;
																																							}
																																						else
																																							{	/* Inline/app.scm 166 */
																																								long
																																									BgL_odepthz00_2153;
																																								{	/* Inline/app.scm 166 */
																																									obj_t
																																										BgL_arg1804z00_2154;
																																									BgL_arg1804z00_2154
																																										=
																																										(BgL_oclassz00_2151);
																																									BgL_odepthz00_2153
																																										=
																																										BGL_CLASS_DEPTH
																																										(BgL_arg1804z00_2154);
																																								}
																																								if ((2L < BgL_odepthz00_2153))
																																									{	/* Inline/app.scm 166 */
																																										obj_t
																																											BgL_arg1802z00_2156;
																																										{	/* Inline/app.scm 166 */
																																											obj_t
																																												BgL_arg1803z00_2157;
																																											BgL_arg1803z00_2157
																																												=
																																												(BgL_oclassz00_2151);
																																											BgL_arg1802z00_2156
																																												=
																																												BGL_CLASS_ANCESTORS_REF
																																												(BgL_arg1803z00_2157,
																																												2L);
																																										}
																																										BgL_res1729z00_2168
																																											=
																																											(BgL_arg1802z00_2156
																																											==
																																											BgL_classz00_2135);
																																									}
																																								else
																																									{	/* Inline/app.scm 166 */
																																										BgL_res1729z00_2168
																																											=
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																					}
																																				}
																																				BgL_test1842z00_2877
																																					=
																																					BgL_res1729z00_2168;
																																			}
																																	}
																																}
																																if (BgL_test1842z00_2877)
																																	{	/* Inline/app.scm 166 */
																																		BgL_test1841z00_2876
																																			=
																																			((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt) BgL_varz00_10)))->BgL_importz00) == CNST_TABLE_REF(1));
																																	}
																																else
																																	{	/* Inline/app.scm 166 */
																																		BgL_test1841z00_2876
																																			=
																																			((bool_t)
																																			1);
																																	}
																															}
																															if (BgL_test1841z00_2876)
																																{	/* Inline/app.scm 167 */
																																	obj_t
																																		BgL_arg1485z00_1561;
																																	BgL_arg1485z00_1561
																																		=
																																		(((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) (((BgL_variablez00_bglt) COBJECT(BgL_varz00_10))->BgL_valuez00))))->BgL_bodyz00);
																																	{	/* Inline/app.scm 167 */
																																		obj_t
																																			BgL_classz00_2172;
																																		BgL_classz00_2172
																																			=
																																			BGl_retblockz00zzast_nodez00;
																																		if (BGL_OBJECTP(BgL_arg1485z00_1561))
																																			{	/* Inline/app.scm 167 */
																																				BgL_objectz00_bglt
																																					BgL_arg1807z00_2174;
																																				BgL_arg1807z00_2174
																																					=
																																					(BgL_objectz00_bglt)
																																					(BgL_arg1485z00_1561);
																																				if (BGL_CONDEXPAND_ISA_ARCH64())
																																					{	/* Inline/app.scm 167 */
																																						long
																																							BgL_idxz00_2180;
																																						BgL_idxz00_2180
																																							=
																																							BGL_OBJECT_INHERITANCE_NUM
																																							(BgL_arg1807z00_2174);
																																						BgL_test1839z00_2872
																																							=
																																							(VECTOR_REF
																																							(BGl_za2inheritancesza2z00zz__objectz00,
																																								(BgL_idxz00_2180
																																									+
																																									2L))
																																							==
																																							BgL_classz00_2172);
																																					}
																																				else
																																					{	/* Inline/app.scm 167 */
																																						bool_t
																																							BgL_res1730z00_2205;
																																						{	/* Inline/app.scm 167 */
																																							obj_t
																																								BgL_oclassz00_2188;
																																							{	/* Inline/app.scm 167 */
																																								obj_t
																																									BgL_arg1815z00_2196;
																																								long
																																									BgL_arg1816z00_2197;
																																								BgL_arg1815z00_2196
																																									=
																																									(BGl_za2classesza2z00zz__objectz00);
																																								{	/* Inline/app.scm 167 */
																																									long
																																										BgL_arg1817z00_2198;
																																									BgL_arg1817z00_2198
																																										=
																																										BGL_OBJECT_CLASS_NUM
																																										(BgL_arg1807z00_2174);
																																									BgL_arg1816z00_2197
																																										=
																																										(BgL_arg1817z00_2198
																																										-
																																										OBJECT_TYPE);
																																								}
																																								BgL_oclassz00_2188
																																									=
																																									VECTOR_REF
																																									(BgL_arg1815z00_2196,
																																									BgL_arg1816z00_2197);
																																							}
																																							{	/* Inline/app.scm 167 */
																																								bool_t
																																									BgL__ortest_1115z00_2189;
																																								BgL__ortest_1115z00_2189
																																									=
																																									(BgL_classz00_2172
																																									==
																																									BgL_oclassz00_2188);
																																								if (BgL__ortest_1115z00_2189)
																																									{	/* Inline/app.scm 167 */
																																										BgL_res1730z00_2205
																																											=
																																											BgL__ortest_1115z00_2189;
																																									}
																																								else
																																									{	/* Inline/app.scm 167 */
																																										long
																																											BgL_odepthz00_2190;
																																										{	/* Inline/app.scm 167 */
																																											obj_t
																																												BgL_arg1804z00_2191;
																																											BgL_arg1804z00_2191
																																												=
																																												(BgL_oclassz00_2188);
																																											BgL_odepthz00_2190
																																												=
																																												BGL_CLASS_DEPTH
																																												(BgL_arg1804z00_2191);
																																										}
																																										if ((2L < BgL_odepthz00_2190))
																																											{	/* Inline/app.scm 167 */
																																												obj_t
																																													BgL_arg1802z00_2193;
																																												{	/* Inline/app.scm 167 */
																																													obj_t
																																														BgL_arg1803z00_2194;
																																													BgL_arg1803z00_2194
																																														=
																																														(BgL_oclassz00_2188);
																																													BgL_arg1802z00_2193
																																														=
																																														BGL_CLASS_ANCESTORS_REF
																																														(BgL_arg1803z00_2194,
																																														2L);
																																												}
																																												BgL_res1730z00_2205
																																													=
																																													(BgL_arg1802z00_2193
																																													==
																																													BgL_classz00_2172);
																																											}
																																										else
																																											{	/* Inline/app.scm 167 */
																																												BgL_res1730z00_2205
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																							}
																																						}
																																						BgL_test1839z00_2872
																																							=
																																							BgL_res1730z00_2205;
																																					}
																																			}
																																		else
																																			{	/* Inline/app.scm 167 */
																																				BgL_test1839z00_2872
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																}
																															else
																																{	/* Inline/app.scm 166 */
																																	BgL_test1839z00_2872
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																													else
																														{	/* Inline/app.scm 165 */
																															BgL_test1839z00_2872
																																= ((bool_t) 0);
																														}
																													if (BgL_test1839z00_2872)
																														{	/* Inline/app.scm 165 */
																															return ((bool_t)
																																1);
																														}
																													else
																														{	/* Inline/app.scm 165 */
																															return ((bool_t)
																																0);
																														}
																												}
																										}
																								}
																							else
																								{	/* Inline/app.scm 146 */
																									return ((bool_t) 0);
																								}
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* Inline/app.scm 109 */
											return ((bool_t) 0);
										}
								}
							else
								{	/* Inline/app.scm 106 */
									return ((bool_t) 0);
								}
						}
					}
				}
			}
		}

	}



/* &inline-app? */
	obj_t BGl_z62inlinezd2appzf3z43zzinline_appz00(obj_t BgL_envz00_2385,
		obj_t BgL_varz00_2386, obj_t BgL_kfactorz00_2387,
		obj_t BgL_callzd2siza7ez75_2388, obj_t BgL_stackz00_2389)
	{
		{	/* Inline/app.scm 96 */
			return
				BBOOL(BGl_inlinezd2appzf3z21zzinline_appz00(
					((BgL_variablez00_bglt) BgL_varz00_2386),
					(long) CINT(BgL_kfactorz00_2387),
					(long) CINT(BgL_callzd2siza7ez75_2388), BgL_stackz00_2389));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinline_appz00(void)
	{
		{	/* Inline/app.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinline_appz00(void)
	{
		{	/* Inline/app.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinline_appz00(void)
	{
		{	/* Inline/app.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinline_appz00(void)
	{
		{	/* Inline/app.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zzmodule_libraryz00(292140514L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zzinline_walkz00(385476819L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zzinline_inlinez00(20504962L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zzinline_siza7eza7(243191744L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			BGl_modulezd2initializa7ationz75zzinline_simplez00(221468907L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
			return
				BGl_modulezd2initializa7ationz75zzinline_recursionz00(101873001L,
				BSTRING_TO_STRING(BGl_string1737z00zzinline_appz00));
		}

	}

#ifdef __cplusplus
}
#endif
