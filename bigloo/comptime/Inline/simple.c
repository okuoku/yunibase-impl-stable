/*===========================================================================*/
/*   (Inline/simple.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Inline/simple.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INLINE_SIMPLE_TYPE_DEFINITIONS
#define BGL_INLINE_SIMPLE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_isfunz00_bgl
	{
		struct BgL_nodez00_bgl *BgL_originalzd2bodyzd2;
		obj_t BgL_recursivezd2callszd2;
		bool_t BgL_tailrecz00;
	}               *BgL_isfunz00_bglt;


#endif													// BGL_INLINE_SIMPLE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t BGl_requirezd2initializa7ationz75zzinline_simplez00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_alphatiza7eza7zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t, BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_atomz00zzast_nodez00;
	extern BgL_nodez00_bglt
		BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_nodez00_bglt, long, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzinline_simplez00(void);
	static obj_t BGl_objectzd2initzd2zzinline_simplez00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern BgL_localz00_bglt BGl_clonezd2localzd2zzast_localz00(BgL_localz00_bglt,
		BgL_valuez00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzinline_simplez00(void);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_inlinezd2appzd2simplez00zzinline_simplez00(BgL_nodez00_bglt, long,
		obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_svarz00zzast_varz00;
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern obj_t BGl_isfunz00zzinline_inlinez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_za2verboseza2z00zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzinline_simplez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_spreadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_siza7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_occurz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_za2inliningzd2reducezd2kfactorza2z00zzengine_paramz00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzinline_simplez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinline_simplez00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	extern obj_t BGl_nodezd2removez12zc0zzast_removez00(BgL_nodez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzinline_simplez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinline_simplez00(void);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern obj_t BGl_currentzd2functionzd2zztools_errorz00(void);
	static BgL_nodez00_bglt BGl_stackablez12z12zzinline_simplez00(BgL_appz00_bglt,
		BgL_nodez00_bglt);
	extern obj_t BGl_occurzd2nodez12zc0zzast_occurz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62inlinezd2appzd2simplez62zzinline_simplez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern bool_t
		BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(BgL_nodez00_bglt);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string1870z00zzinline_simplez00,
		BgL_bgl_string1870za700za7za7i1876za7, " (", 2);
	      DEFINE_STRING(BGl_string1871z00zzinline_simplez00,
		BgL_bgl_string1871za700za7za7i1877za7, " --> ", 5);
	      DEFINE_STRING(BGl_string1872z00zzinline_simplez00,
		BgL_bgl_string1872za700za7za7i1878za7, "         ", 9);
	      DEFINE_STRING(BGl_string1873z00zzinline_simplez00,
		BgL_bgl_string1873za700za7za7i1879za7, "inline_simple", 13);
	      DEFINE_STRING(BGl_string1874z00zzinline_simplez00,
		BgL_bgl_string1874za700za7za7i1880za7, "res (sifun sgfun) read ", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2appzd2simplezd2envzd2zzinline_simplez00,
		BgL_bgl_za762inlineza7d2appza71881za7,
		BGl_z62inlinezd2appzd2simplez62zzinline_simplez00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzinline_simplez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinline_simplez00(long
		BgL_checksumz00_2417, char *BgL_fromz00_2418)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinline_simplez00))
				{
					BGl_requirezd2initializa7ationz75zzinline_simplez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinline_simplez00();
					BGl_libraryzd2moduleszd2initz00zzinline_simplez00();
					BGl_cnstzd2initzd2zzinline_simplez00();
					BGl_importedzd2moduleszd2initz00zzinline_simplez00();
					return BGl_methodzd2initzd2zzinline_simplez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinline_simplez00(void)
	{
		{	/* Inline/simple.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "inline_simple");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"inline_simple");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "inline_simple");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"inline_simple");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"inline_simple");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"inline_simple");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"inline_simple");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"inline_simple");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"inline_simple");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"inline_simple");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinline_simplez00(void)
	{
		{	/* Inline/simple.scm 15 */
			{	/* Inline/simple.scm 15 */
				obj_t BgL_cportz00_2406;

				{	/* Inline/simple.scm 15 */
					obj_t BgL_stringz00_2413;

					BgL_stringz00_2413 = BGl_string1874z00zzinline_simplez00;
					{	/* Inline/simple.scm 15 */
						obj_t BgL_startz00_2414;

						BgL_startz00_2414 = BINT(0L);
						{	/* Inline/simple.scm 15 */
							obj_t BgL_endz00_2415;

							BgL_endz00_2415 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2413)));
							{	/* Inline/simple.scm 15 */

								BgL_cportz00_2406 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2413, BgL_startz00_2414, BgL_endz00_2415);
				}}}}
				{
					long BgL_iz00_2407;

					BgL_iz00_2407 = 2L;
				BgL_loopz00_2408:
					if ((BgL_iz00_2407 == -1L))
						{	/* Inline/simple.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Inline/simple.scm 15 */
							{	/* Inline/simple.scm 15 */
								obj_t BgL_arg1875z00_2409;

								{	/* Inline/simple.scm 15 */

									{	/* Inline/simple.scm 15 */
										obj_t BgL_locationz00_2411;

										BgL_locationz00_2411 = BBOOL(((bool_t) 0));
										{	/* Inline/simple.scm 15 */

											BgL_arg1875z00_2409 =
												BGl_readz00zz__readerz00(BgL_cportz00_2406,
												BgL_locationz00_2411);
										}
									}
								}
								{	/* Inline/simple.scm 15 */
									int BgL_tmpz00_2446;

									BgL_tmpz00_2446 = (int) (BgL_iz00_2407);
									CNST_TABLE_SET(BgL_tmpz00_2446, BgL_arg1875z00_2409);
							}}
							{	/* Inline/simple.scm 15 */
								int BgL_auxz00_2412;

								BgL_auxz00_2412 = (int) ((BgL_iz00_2407 - 1L));
								{
									long BgL_iz00_2451;

									BgL_iz00_2451 = (long) (BgL_auxz00_2412);
									BgL_iz00_2407 = BgL_iz00_2451;
									goto BgL_loopz00_2408;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinline_simplez00(void)
	{
		{	/* Inline/simple.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* inline-app-simple */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_inlinezd2appzd2simplez00zzinline_simplez00(BgL_nodez00_bglt
		BgL_nodez00_3, long BgL_kfactorz00_4, obj_t BgL_stackz00_5,
		obj_t BgL_msgz00_6)
	{
		{	/* Inline/simple.scm 46 */
			{	/* Inline/simple.scm 48 */
				BgL_variablez00_bglt BgL_calleez00_1446;

				BgL_calleez00_1446 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Inline/simple.scm 48 */
					BgL_valuez00_bglt BgL_sfunz00_1447;

					BgL_sfunz00_1447 =
						(((BgL_variablez00_bglt) COBJECT(BgL_calleez00_1446))->
						BgL_valuez00);
					{	/* Inline/simple.scm 49 */
						obj_t BgL_formalsz00_1448;

						BgL_formalsz00_1448 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_sfunz00_1447)))->BgL_argsz00);
						{	/* Inline/simple.scm 50 */
							obj_t BgL_actualsz00_1449;

							BgL_actualsz00_1449 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3)))->BgL_argsz00);
							{	/* Inline/simple.scm 51 */
								obj_t BgL_reductorsz00_1450;

								if (NULLP(BgL_formalsz00_1448))
									{	/* Inline/simple.scm 52 */
										BgL_reductorsz00_1450 = BNIL;
									}
								else
									{	/* Inline/simple.scm 52 */
										obj_t BgL_head1255z00_1569;

										BgL_head1255z00_1569 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_ll1253z00_1571;
											obj_t BgL_ll1254z00_1572;
											obj_t BgL_tail1256z00_1573;

											BgL_ll1253z00_1571 = BgL_formalsz00_1448;
											BgL_ll1254z00_1572 = BgL_actualsz00_1449;
											BgL_tail1256z00_1573 = BgL_head1255z00_1569;
										BgL_zc3z04anonymousza31574ze3z87_1574:
											if (NULLP(BgL_ll1253z00_1571))
												{	/* Inline/simple.scm 52 */
													BgL_reductorsz00_1450 = CDR(BgL_head1255z00_1569);
												}
											else
												{	/* Inline/simple.scm 52 */
													obj_t BgL_newtail1257z00_1576;

													{	/* Inline/simple.scm 52 */
														obj_t BgL_arg1585z00_1579;

														{	/* Inline/simple.scm 52 */
															obj_t BgL_fz00_1580;
															obj_t BgL_az00_1581;

															BgL_fz00_1580 = CAR(((obj_t) BgL_ll1253z00_1571));
															BgL_az00_1581 = CAR(((obj_t) BgL_ll1254z00_1572));
															{	/* Inline/simple.scm 54 */
																bool_t BgL_test1886z00_2472;

																{	/* Inline/simple.scm 54 */
																	bool_t BgL_test1887z00_2473;

																	{	/* Inline/simple.scm 54 */
																		obj_t BgL_classz00_1846;

																		BgL_classz00_1846 =
																			BGl_closurez00zzast_nodez00;
																		if (BGL_OBJECTP(BgL_az00_1581))
																			{	/* Inline/simple.scm 54 */
																				BgL_objectz00_bglt BgL_arg1807z00_1848;

																				BgL_arg1807z00_1848 =
																					(BgL_objectz00_bglt) (BgL_az00_1581);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Inline/simple.scm 54 */
																						long BgL_idxz00_1854;

																						BgL_idxz00_1854 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_1848);
																						BgL_test1887z00_2473 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_1854 + 3L)) ==
																							BgL_classz00_1846);
																					}
																				else
																					{	/* Inline/simple.scm 54 */
																						bool_t BgL_res1855z00_1879;

																						{	/* Inline/simple.scm 54 */
																							obj_t BgL_oclassz00_1862;

																							{	/* Inline/simple.scm 54 */
																								obj_t BgL_arg1815z00_1870;
																								long BgL_arg1816z00_1871;

																								BgL_arg1815z00_1870 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Inline/simple.scm 54 */
																									long BgL_arg1817z00_1872;

																									BgL_arg1817z00_1872 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_1848);
																									BgL_arg1816z00_1871 =
																										(BgL_arg1817z00_1872 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_1862 =
																									VECTOR_REF
																									(BgL_arg1815z00_1870,
																									BgL_arg1816z00_1871);
																							}
																							{	/* Inline/simple.scm 54 */
																								bool_t BgL__ortest_1115z00_1863;

																								BgL__ortest_1115z00_1863 =
																									(BgL_classz00_1846 ==
																									BgL_oclassz00_1862);
																								if (BgL__ortest_1115z00_1863)
																									{	/* Inline/simple.scm 54 */
																										BgL_res1855z00_1879 =
																											BgL__ortest_1115z00_1863;
																									}
																								else
																									{	/* Inline/simple.scm 54 */
																										long BgL_odepthz00_1864;

																										{	/* Inline/simple.scm 54 */
																											obj_t BgL_arg1804z00_1865;

																											BgL_arg1804z00_1865 =
																												(BgL_oclassz00_1862);
																											BgL_odepthz00_1864 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_1865);
																										}
																										if (
																											(3L < BgL_odepthz00_1864))
																											{	/* Inline/simple.scm 54 */
																												obj_t
																													BgL_arg1802z00_1867;
																												{	/* Inline/simple.scm 54 */
																													obj_t
																														BgL_arg1803z00_1868;
																													BgL_arg1803z00_1868 =
																														(BgL_oclassz00_1862);
																													BgL_arg1802z00_1867 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_1868,
																														3L);
																												}
																												BgL_res1855z00_1879 =
																													(BgL_arg1802z00_1867
																													== BgL_classz00_1846);
																											}
																										else
																											{	/* Inline/simple.scm 54 */
																												BgL_res1855z00_1879 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test1887z00_2473 =
																							BgL_res1855z00_1879;
																					}
																			}
																		else
																			{	/* Inline/simple.scm 54 */
																				BgL_test1887z00_2473 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test1887z00_2473)
																		{	/* Inline/simple.scm 54 */
																			if (
																				((((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										BgL_fz00_1580))))->
																						BgL_accessz00) ==
																					CNST_TABLE_REF(0)))
																				{	/* Inline/simple.scm 56 */
																					bool_t BgL__ortest_1108z00_1659;

																					{	/* Inline/simple.scm 56 */
																						BgL_typez00_bglt
																							BgL_arg1722z00_1663;
																						BgL_arg1722z00_1663 =
																							(((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										((BgL_localz00_bglt)
																											BgL_fz00_1580))))->
																							BgL_typez00);
																						BgL__ortest_1108z00_1659 =
																							(((obj_t) BgL_arg1722z00_1663) ==
																							BGl_za2procedureza2z00zztype_cachez00);
																					}
																					if (BgL__ortest_1108z00_1659)
																						{	/* Inline/simple.scm 56 */
																							BgL_test1886z00_2472 =
																								BgL__ortest_1108z00_1659;
																						}
																					else
																						{	/* Inline/simple.scm 57 */
																							bool_t BgL__ortest_1109z00_1660;

																							{	/* Inline/simple.scm 57 */
																								BgL_typez00_bglt
																									BgL_arg1720z00_1662;
																								BgL_arg1720z00_1662 =
																									(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_localz00_bglt)
																													BgL_fz00_1580))))->
																									BgL_typez00);
																								BgL__ortest_1109z00_1660 =
																									(((obj_t) BgL_arg1720z00_1662)
																									==
																									BGl_za2_za2z00zztype_cachez00);
																							}
																							if (BgL__ortest_1109z00_1660)
																								{	/* Inline/simple.scm 57 */
																									BgL_test1886z00_2472 =
																										BgL__ortest_1109z00_1660;
																								}
																							else
																								{	/* Inline/simple.scm 58 */
																									BgL_typez00_bglt
																										BgL_arg1718z00_1661;
																									BgL_arg1718z00_1661 =
																										(((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													((BgL_localz00_bglt)
																														BgL_fz00_1580))))->
																										BgL_typez00);
																									BgL_test1886z00_2472 =
																										(((obj_t)
																											BgL_arg1718z00_1661) ==
																										BGl_za2objza2z00zztype_cachez00);
																								}
																						}
																				}
																			else
																				{	/* Inline/simple.scm 55 */
																					BgL_test1886z00_2472 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Inline/simple.scm 54 */
																			BgL_test1886z00_2472 = ((bool_t) 0);
																		}
																}
																if (BgL_test1886z00_2472)
																	{	/* Inline/simple.scm 54 */
																		BgL_arg1585z00_1579 =
																			((obj_t)
																			(((BgL_varz00_bglt) COBJECT(
																						((BgL_varz00_bglt)
																							((BgL_closurez00_bglt)
																								BgL_az00_1581))))->
																				BgL_variablez00));
																	}
																else
																	{	/* Inline/simple.scm 64 */
																		bool_t BgL_test1895z00_2523;

																		if (
																			((((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_localz00_bglt)
																									BgL_fz00_1580))))->
																					BgL_accessz00) == CNST_TABLE_REF(0)))
																			{	/* Inline/simple.scm 65 */
																				bool_t BgL_test1897z00_2530;

																				{	/* Inline/simple.scm 65 */
																					bool_t BgL_test1898z00_2531;

																					{	/* Inline/simple.scm 65 */
																						BgL_typez00_bglt
																							BgL_arg1714z00_1655;
																						BgL_arg1714z00_1655 =
																							(((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										((BgL_localz00_bglt)
																											BgL_fz00_1580))))->
																							BgL_typez00);
																						BgL_test1898z00_2531 =
																							(((obj_t) BgL_arg1714z00_1655) ==
																							BGl_za2objza2z00zztype_cachez00);
																					}
																					if (BgL_test1898z00_2531)
																						{	/* Inline/simple.scm 65 */
																							BgL_test1897z00_2530 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Inline/simple.scm 66 */
																							BgL_typez00_bglt
																								BgL_arg1711z00_1654;
																							BgL_arg1711z00_1654 =
																								(((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt) (
																												(BgL_localz00_bglt)
																												BgL_fz00_1580))))->
																								BgL_typez00);
																							BgL_test1897z00_2530 =
																								(((obj_t) BgL_arg1711z00_1654)
																								==
																								BGl_za2_za2z00zztype_cachez00);
																						}
																				}
																				if (BgL_test1897z00_2530)
																					{	/* Inline/simple.scm 67 */
																						obj_t BgL_classz00_1888;

																						BgL_classz00_1888 =
																							BGl_atomz00zzast_nodez00;
																						if (BGL_OBJECTP(BgL_az00_1581))
																							{	/* Inline/simple.scm 67 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_1890;
																								BgL_arg1807z00_1890 =
																									(BgL_objectz00_bglt)
																									(BgL_az00_1581);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* Inline/simple.scm 67 */
																										long BgL_idxz00_1896;

																										BgL_idxz00_1896 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_1890);
																										BgL_test1895z00_2523 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_1896 +
																													2L)) ==
																											BgL_classz00_1888);
																									}
																								else
																									{	/* Inline/simple.scm 67 */
																										bool_t BgL_res1856z00_1921;

																										{	/* Inline/simple.scm 67 */
																											obj_t BgL_oclassz00_1904;

																											{	/* Inline/simple.scm 67 */
																												obj_t
																													BgL_arg1815z00_1912;
																												long
																													BgL_arg1816z00_1913;
																												BgL_arg1815z00_1912 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* Inline/simple.scm 67 */
																													long
																														BgL_arg1817z00_1914;
																													BgL_arg1817z00_1914 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_1890);
																													BgL_arg1816z00_1913 =
																														(BgL_arg1817z00_1914
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_1904 =
																													VECTOR_REF
																													(BgL_arg1815z00_1912,
																													BgL_arg1816z00_1913);
																											}
																											{	/* Inline/simple.scm 67 */
																												bool_t
																													BgL__ortest_1115z00_1905;
																												BgL__ortest_1115z00_1905
																													=
																													(BgL_classz00_1888 ==
																													BgL_oclassz00_1904);
																												if (BgL__ortest_1115z00_1905)
																													{	/* Inline/simple.scm 67 */
																														BgL_res1856z00_1921
																															=
																															BgL__ortest_1115z00_1905;
																													}
																												else
																													{	/* Inline/simple.scm 67 */
																														long
																															BgL_odepthz00_1906;
																														{	/* Inline/simple.scm 67 */
																															obj_t
																																BgL_arg1804z00_1907;
																															BgL_arg1804z00_1907
																																=
																																(BgL_oclassz00_1904);
																															BgL_odepthz00_1906
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_1907);
																														}
																														if (
																															(2L <
																																BgL_odepthz00_1906))
																															{	/* Inline/simple.scm 67 */
																																obj_t
																																	BgL_arg1802z00_1909;
																																{	/* Inline/simple.scm 67 */
																																	obj_t
																																		BgL_arg1803z00_1910;
																																	BgL_arg1803z00_1910
																																		=
																																		(BgL_oclassz00_1904);
																																	BgL_arg1802z00_1909
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_1910,
																																		2L);
																																}
																																BgL_res1856z00_1921
																																	=
																																	(BgL_arg1802z00_1909
																																	==
																																	BgL_classz00_1888);
																															}
																														else
																															{	/* Inline/simple.scm 67 */
																																BgL_res1856z00_1921
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test1895z00_2523 =
																											BgL_res1856z00_1921;
																									}
																							}
																						else
																							{	/* Inline/simple.scm 67 */
																								BgL_test1895z00_2523 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Inline/simple.scm 65 */
																						BgL_test1895z00_2523 = ((bool_t) 0);
																					}
																			}
																		else
																			{	/* Inline/simple.scm 64 */
																				BgL_test1895z00_2523 = ((bool_t) 0);
																			}
																		if (BgL_test1895z00_2523)
																			{	/* Inline/simple.scm 64 */
																				BgL_arg1585z00_1579 = BgL_az00_1581;
																			}
																		else
																			{	/* Inline/simple.scm 69 */
																				bool_t BgL_test1903z00_2564;

																				if (
																					((((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										((BgL_localz00_bglt)
																											BgL_fz00_1580))))->
																							BgL_accessz00) ==
																						CNST_TABLE_REF(0)))
																					{	/* Inline/simple.scm 70 */
																						bool_t BgL_test1905z00_2571;

																						{	/* Inline/simple.scm 70 */
																							bool_t BgL_test1906z00_2572;

																							{	/* Inline/simple.scm 70 */
																								BgL_typez00_bglt
																									BgL_arg1709z00_1649;
																								BgL_arg1709z00_1649 =
																									(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_localz00_bglt)
																													BgL_fz00_1580))))->
																									BgL_typez00);
																								BgL_test1906z00_2572 =
																									(((obj_t) BgL_arg1709z00_1649)
																									==
																									BGl_za2objza2z00zztype_cachez00);
																							}
																							if (BgL_test1906z00_2572)
																								{	/* Inline/simple.scm 70 */
																									BgL_test1905z00_2571 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Inline/simple.scm 71 */
																									BgL_typez00_bglt
																										BgL_arg1708z00_1648;
																									BgL_arg1708z00_1648 =
																										(((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													((BgL_localz00_bglt)
																														BgL_fz00_1580))))->
																										BgL_typez00);
																									BgL_test1905z00_2571 =
																										(((obj_t)
																											BgL_arg1708z00_1648) ==
																										BGl_za2_za2z00zztype_cachez00);
																								}
																						}
																						if (BgL_test1905z00_2571)
																							{	/* Inline/simple.scm 72 */
																								bool_t BgL_test1907z00_2583;

																								{	/* Inline/simple.scm 72 */
																									obj_t BgL_classz00_1925;

																									BgL_classz00_1925 =
																										BGl_varz00zzast_nodez00;
																									if (BGL_OBJECTP
																										(BgL_az00_1581))
																										{	/* Inline/simple.scm 72 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_1927;
																											BgL_arg1807z00_1927 =
																												(BgL_objectz00_bglt)
																												(BgL_az00_1581);
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Inline/simple.scm 72 */
																													long BgL_idxz00_1933;

																													BgL_idxz00_1933 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_1927);
																													BgL_test1907z00_2583 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_1933 +
																																2L)) ==
																														BgL_classz00_1925);
																												}
																											else
																												{	/* Inline/simple.scm 72 */
																													bool_t
																														BgL_res1857z00_1958;
																													{	/* Inline/simple.scm 72 */
																														obj_t
																															BgL_oclassz00_1941;
																														{	/* Inline/simple.scm 72 */
																															obj_t
																																BgL_arg1815z00_1949;
																															long
																																BgL_arg1816z00_1950;
																															BgL_arg1815z00_1949
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Inline/simple.scm 72 */
																																long
																																	BgL_arg1817z00_1951;
																																BgL_arg1817z00_1951
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_1927);
																																BgL_arg1816z00_1950
																																	=
																																	(BgL_arg1817z00_1951
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_1941
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_1949,
																																BgL_arg1816z00_1950);
																														}
																														{	/* Inline/simple.scm 72 */
																															bool_t
																																BgL__ortest_1115z00_1942;
																															BgL__ortest_1115z00_1942
																																=
																																(BgL_classz00_1925
																																==
																																BgL_oclassz00_1941);
																															if (BgL__ortest_1115z00_1942)
																																{	/* Inline/simple.scm 72 */
																																	BgL_res1857z00_1958
																																		=
																																		BgL__ortest_1115z00_1942;
																																}
																															else
																																{	/* Inline/simple.scm 72 */
																																	long
																																		BgL_odepthz00_1943;
																																	{	/* Inline/simple.scm 72 */
																																		obj_t
																																			BgL_arg1804z00_1944;
																																		BgL_arg1804z00_1944
																																			=
																																			(BgL_oclassz00_1941);
																																		BgL_odepthz00_1943
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_1944);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_1943))
																																		{	/* Inline/simple.scm 72 */
																																			obj_t
																																				BgL_arg1802z00_1946;
																																			{	/* Inline/simple.scm 72 */
																																				obj_t
																																					BgL_arg1803z00_1947;
																																				BgL_arg1803z00_1947
																																					=
																																					(BgL_oclassz00_1941);
																																				BgL_arg1802z00_1946
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_1947,
																																					2L);
																																			}
																																			BgL_res1857z00_1958
																																				=
																																				(BgL_arg1802z00_1946
																																				==
																																				BgL_classz00_1925);
																																		}
																																	else
																																		{	/* Inline/simple.scm 72 */
																																			BgL_res1857z00_1958
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test1907z00_2583 =
																														BgL_res1857z00_1958;
																												}
																										}
																									else
																										{	/* Inline/simple.scm 72 */
																											BgL_test1907z00_2583 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test1907z00_2583)
																									{	/* Inline/simple.scm 72 */
																										BgL_test1903z00_2564 =
																											(
																											(((BgL_variablez00_bglt)
																													COBJECT(((
																																(BgL_varz00_bglt)
																																COBJECT((
																																		(BgL_varz00_bglt)
																																		BgL_az00_1581)))->
																															BgL_variablez00)))->
																												BgL_accessz00) ==
																											CNST_TABLE_REF(0));
																									}
																								else
																									{	/* Inline/simple.scm 72 */
																										BgL_test1903z00_2564 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Inline/simple.scm 70 */
																								BgL_test1903z00_2564 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Inline/simple.scm 69 */
																						BgL_test1903z00_2564 = ((bool_t) 0);
																					}
																				if (BgL_test1903z00_2564)
																					{	/* Inline/simple.scm 69 */
																						BgL_arg1585z00_1579 =
																							((obj_t)
																							(((BgL_varz00_bglt) COBJECT(
																										((BgL_varz00_bglt)
																											BgL_az00_1581)))->
																								BgL_variablez00));
																					}
																				else
																					{	/* Inline/simple.scm 76 */
																						bool_t BgL_test1912z00_2614;

																						if (
																							((((BgL_variablez00_bglt) COBJECT(
																											((BgL_variablez00_bglt)
																												((BgL_localz00_bglt)
																													BgL_fz00_1580))))->
																									BgL_accessz00) ==
																								CNST_TABLE_REF(0)))
																							{	/* Inline/simple.scm 77 */
																								bool_t BgL_test1914z00_2621;

																								{	/* Inline/simple.scm 77 */
																									bool_t
																										BgL__ortest_1114z00_1638;
																									{	/* Inline/simple.scm 77 */
																										BgL_typez00_bglt
																											BgL_arg1701z00_1640;
																										BgL_arg1701z00_1640 =
																											(((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_localz00_bglt)
																															BgL_fz00_1580))))->
																											BgL_typez00);
																										BgL__ortest_1114z00_1638 =
																											(((obj_t)
																												BgL_arg1701z00_1640) ==
																											BGl_za2intza2z00zztype_cachez00);
																									}
																									if (BgL__ortest_1114z00_1638)
																										{	/* Inline/simple.scm 77 */
																											BgL_test1914z00_2621 =
																												BgL__ortest_1114z00_1638;
																										}
																									else
																										{	/* Inline/simple.scm 78 */
																											BgL_typez00_bglt
																												BgL_arg1700z00_1639;
																											BgL_arg1700z00_1639 =
																												(((BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															((BgL_localz00_bglt) BgL_fz00_1580))))->BgL_typez00);
																											BgL_test1914z00_2621 =
																												(((obj_t)
																													BgL_arg1700z00_1639)
																												==
																												BGl_za2longza2z00zztype_cachez00);
																										}
																								}
																								if (BgL_test1914z00_2621)
																									{	/* Inline/simple.scm 79 */
																										bool_t BgL_test1916z00_2633;

																										{	/* Inline/simple.scm 79 */
																											obj_t BgL_classz00_1965;

																											BgL_classz00_1965 =
																												BGl_atomz00zzast_nodez00;
																											if (BGL_OBJECTP
																												(BgL_az00_1581))
																												{	/* Inline/simple.scm 79 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_1967;
																													BgL_arg1807z00_1967 =
																														(BgL_objectz00_bglt)
																														(BgL_az00_1581);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Inline/simple.scm 79 */
																															long
																																BgL_idxz00_1973;
																															BgL_idxz00_1973 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_1967);
																															BgL_test1916z00_2633
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_1973
																																		+ 2L)) ==
																																BgL_classz00_1965);
																														}
																													else
																														{	/* Inline/simple.scm 79 */
																															bool_t
																																BgL_res1858z00_1998;
																															{	/* Inline/simple.scm 79 */
																																obj_t
																																	BgL_oclassz00_1981;
																																{	/* Inline/simple.scm 79 */
																																	obj_t
																																		BgL_arg1815z00_1989;
																																	long
																																		BgL_arg1816z00_1990;
																																	BgL_arg1815z00_1989
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Inline/simple.scm 79 */
																																		long
																																			BgL_arg1817z00_1991;
																																		BgL_arg1817z00_1991
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_1967);
																																		BgL_arg1816z00_1990
																																			=
																																			(BgL_arg1817z00_1991
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_1981
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_1989,
																																		BgL_arg1816z00_1990);
																																}
																																{	/* Inline/simple.scm 79 */
																																	bool_t
																																		BgL__ortest_1115z00_1982;
																																	BgL__ortest_1115z00_1982
																																		=
																																		(BgL_classz00_1965
																																		==
																																		BgL_oclassz00_1981);
																																	if (BgL__ortest_1115z00_1982)
																																		{	/* Inline/simple.scm 79 */
																																			BgL_res1858z00_1998
																																				=
																																				BgL__ortest_1115z00_1982;
																																		}
																																	else
																																		{	/* Inline/simple.scm 79 */
																																			long
																																				BgL_odepthz00_1983;
																																			{	/* Inline/simple.scm 79 */
																																				obj_t
																																					BgL_arg1804z00_1984;
																																				BgL_arg1804z00_1984
																																					=
																																					(BgL_oclassz00_1981);
																																				BgL_odepthz00_1983
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_1984);
																																			}
																																			if (
																																				(2L <
																																					BgL_odepthz00_1983))
																																				{	/* Inline/simple.scm 79 */
																																					obj_t
																																						BgL_arg1802z00_1986;
																																					{	/* Inline/simple.scm 79 */
																																						obj_t
																																							BgL_arg1803z00_1987;
																																						BgL_arg1803z00_1987
																																							=
																																							(BgL_oclassz00_1981);
																																						BgL_arg1802z00_1986
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_1987,
																																							2L);
																																					}
																																					BgL_res1858z00_1998
																																						=
																																						(BgL_arg1802z00_1986
																																						==
																																						BgL_classz00_1965);
																																				}
																																			else
																																				{	/* Inline/simple.scm 79 */
																																					BgL_res1858z00_1998
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test1916z00_2633
																																=
																																BgL_res1858z00_1998;
																														}
																												}
																											else
																												{	/* Inline/simple.scm 79 */
																													BgL_test1916z00_2633 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test1916z00_2633)
																											{	/* Inline/simple.scm 79 */
																												BgL_test1912z00_2614 =
																													INTEGERP(
																													(((BgL_atomz00_bglt)
																															COBJECT((
																																	(BgL_atomz00_bglt)
																																	BgL_az00_1581)))->
																														BgL_valuez00));
																											}
																										else
																											{	/* Inline/simple.scm 79 */
																												BgL_test1912z00_2614 =
																													((bool_t) 0);
																											}
																									}
																								else
																									{	/* Inline/simple.scm 77 */
																										BgL_test1912z00_2614 =
																											((bool_t) 0);
																									}
																							}
																						else
																							{	/* Inline/simple.scm 76 */
																								BgL_test1912z00_2614 =
																									((bool_t) 0);
																							}
																						if (BgL_test1912z00_2614)
																							{	/* Inline/simple.scm 76 */
																								BgL_arg1585z00_1579 =
																									BgL_az00_1581;
																							}
																						else
																							{	/* Inline/simple.scm 85 */
																								BgL_svarz00_bglt
																									BgL_arg1691z00_1629;
																								{	/* Inline/simple.scm 85 */
																									BgL_svarz00_bglt
																										BgL_duplicated1117z00_1630;
																									BgL_svarz00_bglt
																										BgL_new1115z00_1631;
																									BgL_duplicated1117z00_1630 =
																										((BgL_svarz00_bglt) ((
																												(BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_localz00_bglt)
																															BgL_fz00_1580))))->
																											BgL_valuez00));
																									{	/* Inline/simple.scm 85 */
																										BgL_svarz00_bglt
																											BgL_new1118z00_1632;
																										BgL_new1118z00_1632 =
																											((BgL_svarz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_svarz00_bgl))));
																										{	/* Inline/simple.scm 85 */
																											long BgL_arg1692z00_1633;

																											BgL_arg1692z00_1633 =
																												BGL_CLASS_NUM
																												(BGl_svarz00zzast_varz00);
																											BGL_OBJECT_CLASS_NUM_SET((
																													(BgL_objectz00_bglt)
																													BgL_new1118z00_1632),
																												BgL_arg1692z00_1633);
																										}
																										{	/* Inline/simple.scm 85 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_2667;
																											BgL_tmpz00_2667 =
																												((BgL_objectz00_bglt)
																												BgL_new1118z00_1632);
																											BGL_OBJECT_WIDENING_SET
																												(BgL_tmpz00_2667,
																												BFALSE);
																										}
																										((BgL_objectz00_bglt)
																											BgL_new1118z00_1632);
																										BgL_new1115z00_1631 =
																											BgL_new1118z00_1632;
																									}
																									((((BgL_svarz00_bglt)
																												COBJECT
																												(BgL_new1115z00_1631))->
																											BgL_locz00) =
																										((obj_t) ((
																													(BgL_svarz00_bglt)
																													COBJECT
																													(BgL_duplicated1117z00_1630))->
																												BgL_locz00)), BUNSPEC);
																									BgL_arg1691z00_1629 =
																										BgL_new1115z00_1631;
																								}
																								BgL_arg1585z00_1579 =
																									((obj_t)
																									BGl_clonezd2localzd2zzast_localz00
																									(((BgL_localz00_bglt)
																											BgL_fz00_1580),
																										((BgL_valuez00_bglt)
																											BgL_arg1691z00_1629)));
														}}}}}}
														BgL_newtail1257z00_1576 =
															MAKE_YOUNG_PAIR(BgL_arg1585z00_1579, BNIL);
													}
													SET_CDR(BgL_tail1256z00_1573,
														BgL_newtail1257z00_1576);
													{	/* Inline/simple.scm 52 */
														obj_t BgL_arg1576z00_1577;
														obj_t BgL_arg1584z00_1578;

														BgL_arg1576z00_1577 =
															CDR(((obj_t) BgL_ll1253z00_1571));
														BgL_arg1584z00_1578 =
															CDR(((obj_t) BgL_ll1254z00_1572));
														{
															obj_t BgL_tail1256z00_2685;
															obj_t BgL_ll1254z00_2684;
															obj_t BgL_ll1253z00_2683;

															BgL_ll1253z00_2683 = BgL_arg1576z00_1577;
															BgL_ll1254z00_2684 = BgL_arg1584z00_1578;
															BgL_tail1256z00_2685 = BgL_newtail1257z00_1576;
															BgL_tail1256z00_1573 = BgL_tail1256z00_2685;
															BgL_ll1254z00_1572 = BgL_ll1254z00_2684;
															BgL_ll1253z00_1571 = BgL_ll1253z00_2683;
															goto BgL_zc3z04anonymousza31574ze3z87_1574;
														}
													}
												}
										}
									}
								{	/* Inline/simple.scm 52 */
									obj_t BgL_bindingsz00_1451;

									{
										obj_t BgL_reductorsz00_1519;
										obj_t BgL_actualsz00_1520;
										obj_t BgL_resz00_1521;

										BgL_reductorsz00_1519 = BgL_reductorsz00_1450;
										BgL_actualsz00_1520 = BgL_actualsz00_1449;
										BgL_resz00_1521 = BNIL;
									BgL_zc3z04anonymousza31363ze3z87_1522:
										if (NULLP(BgL_actualsz00_1520))
											{	/* Inline/simple.scm 92 */
												BgL_bindingsz00_1451 =
													bgl_reverse_bang(BgL_resz00_1521);
											}
										else
											{	/* Inline/simple.scm 94 */
												bool_t BgL_test1922z00_2689;

												{	/* Inline/simple.scm 94 */
													bool_t BgL_test1923z00_2690;

													{	/* Inline/simple.scm 94 */
														obj_t BgL_arg1571z00_1564;

														BgL_arg1571z00_1564 =
															CAR(((obj_t) BgL_actualsz00_1520));
														{	/* Inline/simple.scm 94 */
															obj_t BgL_classz00_2009;

															BgL_classz00_2009 = BGl_closurez00zzast_nodez00;
															if (BGL_OBJECTP(BgL_arg1571z00_1564))
																{	/* Inline/simple.scm 94 */
																	BgL_objectz00_bglt BgL_arg1807z00_2011;

																	BgL_arg1807z00_2011 =
																		(BgL_objectz00_bglt) (BgL_arg1571z00_1564);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Inline/simple.scm 94 */
																			long BgL_idxz00_2017;

																			BgL_idxz00_2017 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2011);
																			BgL_test1923z00_2690 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2017 + 3L)) ==
																				BgL_classz00_2009);
																		}
																	else
																		{	/* Inline/simple.scm 94 */
																			bool_t BgL_res1859z00_2042;

																			{	/* Inline/simple.scm 94 */
																				obj_t BgL_oclassz00_2025;

																				{	/* Inline/simple.scm 94 */
																					obj_t BgL_arg1815z00_2033;
																					long BgL_arg1816z00_2034;

																					BgL_arg1815z00_2033 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Inline/simple.scm 94 */
																						long BgL_arg1817z00_2035;

																						BgL_arg1817z00_2035 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2011);
																						BgL_arg1816z00_2034 =
																							(BgL_arg1817z00_2035 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2025 =
																						VECTOR_REF(BgL_arg1815z00_2033,
																						BgL_arg1816z00_2034);
																				}
																				{	/* Inline/simple.scm 94 */
																					bool_t BgL__ortest_1115z00_2026;

																					BgL__ortest_1115z00_2026 =
																						(BgL_classz00_2009 ==
																						BgL_oclassz00_2025);
																					if (BgL__ortest_1115z00_2026)
																						{	/* Inline/simple.scm 94 */
																							BgL_res1859z00_2042 =
																								BgL__ortest_1115z00_2026;
																						}
																					else
																						{	/* Inline/simple.scm 94 */
																							long BgL_odepthz00_2027;

																							{	/* Inline/simple.scm 94 */
																								obj_t BgL_arg1804z00_2028;

																								BgL_arg1804z00_2028 =
																									(BgL_oclassz00_2025);
																								BgL_odepthz00_2027 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2028);
																							}
																							if ((3L < BgL_odepthz00_2027))
																								{	/* Inline/simple.scm 94 */
																									obj_t BgL_arg1802z00_2030;

																									{	/* Inline/simple.scm 94 */
																										obj_t BgL_arg1803z00_2031;

																										BgL_arg1803z00_2031 =
																											(BgL_oclassz00_2025);
																										BgL_arg1802z00_2030 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2031, 3L);
																									}
																									BgL_res1859z00_2042 =
																										(BgL_arg1802z00_2030 ==
																										BgL_classz00_2009);
																								}
																							else
																								{	/* Inline/simple.scm 94 */
																									BgL_res1859z00_2042 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1923z00_2690 =
																				BgL_res1859z00_2042;
																		}
																}
															else
																{	/* Inline/simple.scm 94 */
																	BgL_test1923z00_2690 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test1923z00_2690)
														{	/* Inline/simple.scm 94 */
															BgL_test1922z00_2689 =
																(CAR(
																	((obj_t) BgL_reductorsz00_1519)) ==
																((obj_t)
																	(((BgL_varz00_bglt) COBJECT(
																				((BgL_varz00_bglt)
																					((BgL_closurez00_bglt)
																						CAR(
																							((obj_t)
																								BgL_actualsz00_1520))))))->
																		BgL_variablez00)));
														}
													else
														{	/* Inline/simple.scm 94 */
															BgL_test1922z00_2689 = ((bool_t) 0);
														}
												}
												if (BgL_test1922z00_2689)
													{	/* Inline/simple.scm 97 */
														obj_t BgL_arg1408z00_1531;
														obj_t BgL_arg1410z00_1532;

														BgL_arg1408z00_1531 =
															CDR(((obj_t) BgL_reductorsz00_1519));
														BgL_arg1410z00_1532 =
															CDR(((obj_t) BgL_actualsz00_1520));
														{
															obj_t BgL_actualsz00_2729;
															obj_t BgL_reductorsz00_2728;

															BgL_reductorsz00_2728 = BgL_arg1408z00_1531;
															BgL_actualsz00_2729 = BgL_arg1410z00_1532;
															BgL_actualsz00_1520 = BgL_actualsz00_2729;
															BgL_reductorsz00_1519 = BgL_reductorsz00_2728;
															goto BgL_zc3z04anonymousza31363ze3z87_1522;
														}
													}
												else
													{	/* Inline/simple.scm 94 */
														if (
															(CAR(
																	((obj_t) BgL_reductorsz00_1519)) ==
																CAR(((obj_t) BgL_actualsz00_1520))))
															{	/* Inline/simple.scm 101 */
																obj_t BgL_arg1434z00_1536;
																obj_t BgL_arg1437z00_1537;

																BgL_arg1434z00_1536 =
																	CDR(((obj_t) BgL_reductorsz00_1519));
																BgL_arg1437z00_1537 =
																	CDR(((obj_t) BgL_actualsz00_1520));
																{
																	obj_t BgL_actualsz00_2741;
																	obj_t BgL_reductorsz00_2740;

																	BgL_reductorsz00_2740 = BgL_arg1434z00_1536;
																	BgL_actualsz00_2741 = BgL_arg1437z00_1537;
																	BgL_actualsz00_1520 = BgL_actualsz00_2741;
																	BgL_reductorsz00_1519 = BgL_reductorsz00_2740;
																	goto BgL_zc3z04anonymousza31363ze3z87_1522;
																}
															}
														else
															{	/* Inline/simple.scm 104 */
																bool_t BgL_test1929z00_2742;

																{	/* Inline/simple.scm 104 */
																	bool_t BgL_test1930z00_2743;

																	{	/* Inline/simple.scm 104 */
																		obj_t BgL_arg1552z00_1557;

																		BgL_arg1552z00_1557 =
																			CAR(((obj_t) BgL_actualsz00_1520));
																		{	/* Inline/simple.scm 104 */
																			obj_t BgL_classz00_2053;

																			BgL_classz00_2053 =
																				BGl_varz00zzast_nodez00;
																			if (BGL_OBJECTP(BgL_arg1552z00_1557))
																				{	/* Inline/simple.scm 104 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2055;
																					BgL_arg1807z00_2055 =
																						(BgL_objectz00_bglt)
																						(BgL_arg1552z00_1557);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Inline/simple.scm 104 */
																							long BgL_idxz00_2061;

																							BgL_idxz00_2061 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2055);
																							BgL_test1930z00_2743 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2061 + 2L)) ==
																								BgL_classz00_2053);
																						}
																					else
																						{	/* Inline/simple.scm 104 */
																							bool_t BgL_res1860z00_2086;

																							{	/* Inline/simple.scm 104 */
																								obj_t BgL_oclassz00_2069;

																								{	/* Inline/simple.scm 104 */
																									obj_t BgL_arg1815z00_2077;
																									long BgL_arg1816z00_2078;

																									BgL_arg1815z00_2077 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Inline/simple.scm 104 */
																										long BgL_arg1817z00_2079;

																										BgL_arg1817z00_2079 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2055);
																										BgL_arg1816z00_2078 =
																											(BgL_arg1817z00_2079 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2069 =
																										VECTOR_REF
																										(BgL_arg1815z00_2077,
																										BgL_arg1816z00_2078);
																								}
																								{	/* Inline/simple.scm 104 */
																									bool_t
																										BgL__ortest_1115z00_2070;
																									BgL__ortest_1115z00_2070 =
																										(BgL_classz00_2053 ==
																										BgL_oclassz00_2069);
																									if (BgL__ortest_1115z00_2070)
																										{	/* Inline/simple.scm 104 */
																											BgL_res1860z00_2086 =
																												BgL__ortest_1115z00_2070;
																										}
																									else
																										{	/* Inline/simple.scm 104 */
																											long BgL_odepthz00_2071;

																											{	/* Inline/simple.scm 104 */
																												obj_t
																													BgL_arg1804z00_2072;
																												BgL_arg1804z00_2072 =
																													(BgL_oclassz00_2069);
																												BgL_odepthz00_2071 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2072);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_2071))
																												{	/* Inline/simple.scm 104 */
																													obj_t
																														BgL_arg1802z00_2074;
																													{	/* Inline/simple.scm 104 */
																														obj_t
																															BgL_arg1803z00_2075;
																														BgL_arg1803z00_2075
																															=
																															(BgL_oclassz00_2069);
																														BgL_arg1802z00_2074
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2075,
																															2L);
																													}
																													BgL_res1860z00_2086 =
																														(BgL_arg1802z00_2074
																														==
																														BgL_classz00_2053);
																												}
																											else
																												{	/* Inline/simple.scm 104 */
																													BgL_res1860z00_2086 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test1930z00_2743 =
																								BgL_res1860z00_2086;
																						}
																				}
																			else
																				{	/* Inline/simple.scm 104 */
																					BgL_test1930z00_2743 = ((bool_t) 0);
																				}
																		}
																	}
																	if (BgL_test1930z00_2743)
																		{	/* Inline/simple.scm 104 */
																			BgL_test1929z00_2742 =
																				(CAR(
																					((obj_t) BgL_reductorsz00_1519)) ==
																				((obj_t)
																					(((BgL_varz00_bglt) COBJECT(
																								((BgL_varz00_bglt)
																									CAR(
																										((obj_t)
																											BgL_actualsz00_1520)))))->
																						BgL_variablez00)));
																		}
																	else
																		{	/* Inline/simple.scm 104 */
																			BgL_test1929z00_2742 = ((bool_t) 0);
																		}
																}
																if (BgL_test1929z00_2742)
																	{	/* Inline/simple.scm 106 */
																		obj_t BgL_arg1485z00_1545;
																		obj_t BgL_arg1489z00_1546;

																		BgL_arg1485z00_1545 =
																			CDR(((obj_t) BgL_reductorsz00_1519));
																		BgL_arg1489z00_1546 =
																			CDR(((obj_t) BgL_actualsz00_1520));
																		{
																			obj_t BgL_actualsz00_2781;
																			obj_t BgL_reductorsz00_2780;

																			BgL_reductorsz00_2780 =
																				BgL_arg1485z00_1545;
																			BgL_actualsz00_2781 = BgL_arg1489z00_1546;
																			BgL_actualsz00_1520 = BgL_actualsz00_2781;
																			BgL_reductorsz00_1519 =
																				BgL_reductorsz00_2780;
																			goto
																				BgL_zc3z04anonymousza31363ze3z87_1522;
																		}
																	}
																else
																	{	/* Inline/simple.scm 110 */
																		obj_t BgL_arg1502z00_1547;
																		obj_t BgL_arg1509z00_1548;
																		obj_t BgL_arg1513z00_1549;

																		BgL_arg1502z00_1547 =
																			CDR(((obj_t) BgL_reductorsz00_1519));
																		BgL_arg1509z00_1548 =
																			CDR(((obj_t) BgL_actualsz00_1520));
																		{	/* Inline/simple.scm 112 */
																			obj_t BgL_arg1514z00_1550;

																			{	/* Inline/simple.scm 112 */
																				obj_t BgL_arg1516z00_1551;
																				obj_t BgL_arg1535z00_1552;

																				BgL_arg1516z00_1551 =
																					CAR(((obj_t) BgL_reductorsz00_1519));
																				BgL_arg1535z00_1552 =
																					CAR(((obj_t) BgL_actualsz00_1520));
																				BgL_arg1514z00_1550 =
																					MAKE_YOUNG_PAIR(BgL_arg1516z00_1551,
																					BgL_arg1535z00_1552);
																			}
																			BgL_arg1513z00_1549 =
																				MAKE_YOUNG_PAIR(BgL_arg1514z00_1550,
																				BgL_resz00_1521);
																		}
																		{
																			obj_t BgL_resz00_2794;
																			obj_t BgL_actualsz00_2793;
																			obj_t BgL_reductorsz00_2792;

																			BgL_reductorsz00_2792 =
																				BgL_arg1502z00_1547;
																			BgL_actualsz00_2793 = BgL_arg1509z00_1548;
																			BgL_resz00_2794 = BgL_arg1513z00_1549;
																			BgL_resz00_1521 = BgL_resz00_2794;
																			BgL_actualsz00_1520 = BgL_actualsz00_2793;
																			BgL_reductorsz00_1519 =
																				BgL_reductorsz00_2792;
																			goto
																				BgL_zc3z04anonymousza31363ze3z87_1522;
																		}
																	}
															}
													}
											}
									}
									{	/* Inline/simple.scm 88 */
										obj_t BgL_bodyz00_1452;

										{	/* Inline/simple.scm 115 */
											bool_t BgL_test1935z00_2795;

											{	/* Inline/simple.scm 115 */
												obj_t BgL_classz00_2096;

												BgL_classz00_2096 = BGl_isfunz00zzinline_inlinez00;
												{	/* Inline/simple.scm 115 */
													BgL_objectz00_bglt BgL_arg1807z00_2098;

													{	/* Inline/simple.scm 115 */
														obj_t BgL_tmpz00_2796;

														BgL_tmpz00_2796 =
															((obj_t) ((BgL_objectz00_bglt) BgL_sfunz00_1447));
														BgL_arg1807z00_2098 =
															(BgL_objectz00_bglt) (BgL_tmpz00_2796);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Inline/simple.scm 115 */
															long BgL_idxz00_2104;

															BgL_idxz00_2104 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2098);
															BgL_test1935z00_2795 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2104 + 4L)) == BgL_classz00_2096);
														}
													else
														{	/* Inline/simple.scm 115 */
															bool_t BgL_res1861z00_2129;

															{	/* Inline/simple.scm 115 */
																obj_t BgL_oclassz00_2112;

																{	/* Inline/simple.scm 115 */
																	obj_t BgL_arg1815z00_2120;
																	long BgL_arg1816z00_2121;

																	BgL_arg1815z00_2120 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Inline/simple.scm 115 */
																		long BgL_arg1817z00_2122;

																		BgL_arg1817z00_2122 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2098);
																		BgL_arg1816z00_2121 =
																			(BgL_arg1817z00_2122 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2112 =
																		VECTOR_REF(BgL_arg1815z00_2120,
																		BgL_arg1816z00_2121);
																}
																{	/* Inline/simple.scm 115 */
																	bool_t BgL__ortest_1115z00_2113;

																	BgL__ortest_1115z00_2113 =
																		(BgL_classz00_2096 == BgL_oclassz00_2112);
																	if (BgL__ortest_1115z00_2113)
																		{	/* Inline/simple.scm 115 */
																			BgL_res1861z00_2129 =
																				BgL__ortest_1115z00_2113;
																		}
																	else
																		{	/* Inline/simple.scm 115 */
																			long BgL_odepthz00_2114;

																			{	/* Inline/simple.scm 115 */
																				obj_t BgL_arg1804z00_2115;

																				BgL_arg1804z00_2115 =
																					(BgL_oclassz00_2112);
																				BgL_odepthz00_2114 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2115);
																			}
																			if ((4L < BgL_odepthz00_2114))
																				{	/* Inline/simple.scm 115 */
																					obj_t BgL_arg1802z00_2117;

																					{	/* Inline/simple.scm 115 */
																						obj_t BgL_arg1803z00_2118;

																						BgL_arg1803z00_2118 =
																							(BgL_oclassz00_2112);
																						BgL_arg1802z00_2117 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2118, 4L);
																					}
																					BgL_res1861z00_2129 =
																						(BgL_arg1802z00_2117 ==
																						BgL_classz00_2096);
																				}
																			else
																				{	/* Inline/simple.scm 115 */
																					BgL_res1861z00_2129 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1935z00_2795 = BgL_res1861z00_2129;
														}
												}
											}
											if (BgL_test1935z00_2795)
												{
													BgL_nodez00_bglt BgL_auxz00_2819;

													{
														BgL_isfunz00_bglt BgL_auxz00_2820;

														{
															obj_t BgL_auxz00_2821;

															{	/* Inline/simple.scm 116 */
																BgL_objectz00_bglt BgL_tmpz00_2822;

																BgL_tmpz00_2822 =
																	((BgL_objectz00_bglt)
																	((BgL_sfunz00_bglt) BgL_sfunz00_1447));
																BgL_auxz00_2821 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2822);
															}
															BgL_auxz00_2820 =
																((BgL_isfunz00_bglt) BgL_auxz00_2821);
														}
														BgL_auxz00_2819 =
															(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_2820))->
															BgL_originalzd2bodyzd2);
													}
													BgL_bodyz00_1452 = ((obj_t) BgL_auxz00_2819);
												}
											else
												{	/* Inline/simple.scm 115 */
													BgL_bodyz00_1452 =
														(((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_sfunz00_1447)))->
														BgL_bodyz00);
												}
										}
										{	/* Inline/simple.scm 118 */
											obj_t BgL_newzd2kfactorzd2_1454;

											BgL_newzd2kfactorzd2_1454 =
												BGL_PROCEDURE_CALL1
												(BGl_za2inliningzd2reducezd2kfactorza2z00zzengine_paramz00,
												BINT(BgL_kfactorz00_4));
											{	/* Inline/simple.scm 119 */
												obj_t BgL_locz00_1455;

												BgL_locz00_1455 =
													(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_3))->
													BgL_locz00);
												{	/* Inline/simple.scm 120 */
													BgL_typez00_bglt BgL_typez00_1456;

													BgL_typez00_1456 =
														(((BgL_variablez00_bglt)
															COBJECT(BgL_calleez00_1446))->BgL_typez00);
													{	/* Inline/simple.scm 121 */

														((((BgL_variablez00_bglt)
																	COBJECT(BgL_calleez00_1446))->
																BgL_occurrencez00) =
															((long) ((((BgL_variablez00_bglt)
																			COBJECT(BgL_calleez00_1446))->
																		BgL_occurrencez00) - 1L)), BUNSPEC);
														if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00((
																		((BgL_sfunz00_bglt)
																			COBJECT(((BgL_sfunz00_bglt)
																					BgL_sfunz00_1447)))->BgL_classz00),
																	CNST_TABLE_REF(1))))
															{	/* Inline/simple.scm 125 */
																BFALSE;
															}
														else
															{	/* Inline/simple.scm 126 */
																bool_t BgL_test1940z00_2847;

																if (INTEGERP
																	(BGl_za2verboseza2z00zzengine_paramz00))
																	{	/* Inline/simple.scm 126 */
																		BgL_test1940z00_2847 =
																			(3L <=
																			(long)
																			CINT
																			(BGl_za2verboseza2z00zzengine_paramz00));
																	}
																else
																	{	/* Inline/simple.scm 126 */
																		BgL_test1940z00_2847 =
																			BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT
																			(3L),
																			BGl_za2verboseza2z00zzengine_paramz00);
																	}
																if (BgL_test1940z00_2847)
																	{	/* Inline/simple.scm 127 */
																		obj_t BgL_arg1310z00_1464;
																		obj_t BgL_arg1311z00_1465;

																		BgL_arg1310z00_1464 =
																			BGl_shapez00zztools_shapez00(
																			((obj_t) BgL_calleez00_1446));
																		BgL_arg1311z00_1465 =
																			BGl_currentzd2functionzd2zztools_errorz00
																			();
																		{	/* Inline/simple.scm 126 */
																			obj_t BgL_list1312z00_1466;

																			{	/* Inline/simple.scm 126 */
																				obj_t BgL_arg1314z00_1467;

																				{	/* Inline/simple.scm 126 */
																					obj_t BgL_arg1315z00_1468;

																					{	/* Inline/simple.scm 126 */
																						obj_t BgL_arg1316z00_1469;

																						{	/* Inline/simple.scm 126 */
																							obj_t BgL_arg1317z00_1470;

																							{	/* Inline/simple.scm 126 */
																								obj_t BgL_arg1318z00_1471;

																								{	/* Inline/simple.scm 126 */
																									obj_t BgL_arg1319z00_1472;

																									{	/* Inline/simple.scm 126 */
																										obj_t BgL_arg1320z00_1473;

																										BgL_arg1320z00_1473 =
																											MAKE_YOUNG_PAIR(BCHAR((
																													(unsigned char) 10)),
																											BNIL);
																										BgL_arg1319z00_1472 =
																											MAKE_YOUNG_PAIR(BCHAR((
																													(unsigned char) ')')),
																											BgL_arg1320z00_1473);
																									}
																									BgL_arg1318z00_1471 =
																										MAKE_YOUNG_PAIR
																										(BgL_msgz00_6,
																										BgL_arg1319z00_1472);
																								}
																								BgL_arg1317z00_1470 =
																									MAKE_YOUNG_PAIR
																									(BGl_string1870z00zzinline_simplez00,
																									BgL_arg1318z00_1471);
																							}
																							BgL_arg1316z00_1469 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1311z00_1465,
																								BgL_arg1317z00_1470);
																						}
																						BgL_arg1315z00_1468 =
																							MAKE_YOUNG_PAIR
																							(BGl_string1871z00zzinline_simplez00,
																							BgL_arg1316z00_1469);
																					}
																					BgL_arg1314z00_1467 =
																						MAKE_YOUNG_PAIR(BgL_arg1310z00_1464,
																						BgL_arg1315z00_1468);
																				}
																				BgL_list1312z00_1466 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1872z00zzinline_simplez00,
																					BgL_arg1314z00_1467);
																			}
																			BGl_verbosez00zztools_speekz00(BINT(3L),
																				BgL_list1312z00_1466);
																	}}
																else
																	{	/* Inline/simple.scm 126 */
																		BUNSPEC;
																	}
															}
														{	/* Inline/simple.scm 133 */
															BgL_nodez00_bglt BgL_alphazd2bodyzd2_1476;

															BgL_alphazd2bodyzd2_1476 =
																BGl_alphatiza7eza7zzast_alphatiza7eza7
																(BgL_formalsz00_1448, BgL_reductorsz00_1450,
																BgL_locz00_1455,
																((BgL_nodez00_bglt) BgL_bodyz00_1452));
															{	/* Inline/simple.scm 133 */

																BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00
																	(BgL_alphazd2bodyzd2_1476);
																{	/* Inline/simple.scm 137 */
																	BgL_nodez00_bglt BgL_inodez00_1477;

																	{	/* Inline/simple.scm 137 */
																		obj_t BgL_arg1361z00_1515;

																		BgL_arg1361z00_1515 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_calleez00_1446),
																			BgL_stackz00_5);
																		BgL_inodez00_1477 =
																			BGl_inlinezd2nodezd2zzinline_inlinez00
																			(BgL_alphazd2bodyzd2_1476,
																			(long) CINT(BgL_newzd2kfactorzd2_1454),
																			BgL_arg1361z00_1515);
																	}
																	{	/* Inline/simple.scm 137 */
																		BgL_letzd2varzd2_bglt BgL_ibodyz00_1478;

																		{	/* Inline/simple.scm 138 */
																			BgL_letzd2varzd2_bglt BgL_new1123z00_1511;

																			{	/* Inline/simple.scm 139 */
																				BgL_letzd2varzd2_bglt
																					BgL_new1122z00_1513;
																				BgL_new1122z00_1513 =
																					((BgL_letzd2varzd2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_letzd2varzd2_bgl))));
																				{	/* Inline/simple.scm 139 */
																					long BgL_arg1352z00_1514;

																					BgL_arg1352z00_1514 =
																						BGL_CLASS_NUM
																						(BGl_letzd2varzd2zzast_nodez00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1122z00_1513),
																						BgL_arg1352z00_1514);
																				}
																				{	/* Inline/simple.scm 139 */
																					BgL_objectz00_bglt BgL_tmpz00_2880;

																					BgL_tmpz00_2880 =
																						((BgL_objectz00_bglt)
																						BgL_new1122z00_1513);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_2880, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1122z00_1513);
																				BgL_new1123z00_1511 =
																					BgL_new1122z00_1513;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1123z00_1511)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_1455), BUNSPEC);
																			{
																				BgL_typez00_bglt BgL_auxz00_2886;

																				{	/* Inline/simple.scm 141 */
																					BgL_typez00_bglt BgL_arg1351z00_1512;

																					BgL_arg1351z00_1512 =
																						(((BgL_nodez00_bglt)
																							COBJECT(BgL_inodez00_1477))->
																						BgL_typez00);
																					BgL_auxz00_2886 =
																						BGl_strictzd2nodezd2typez00zzast_nodez00
																						(BgL_arg1351z00_1512,
																						BgL_typez00_1456);
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1123z00_1511)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) BgL_auxz00_2886),
																					BUNSPEC);
																			}
																			((((BgL_nodezf2effectzf2_bglt) COBJECT(
																							((BgL_nodezf2effectzf2_bglt)
																								BgL_new1123z00_1511)))->
																					BgL_sidezd2effectzd2) =
																				((obj_t)
																					BBOOL
																					(BGl_sidezd2effectzf3z21zzeffect_effectz00
																						(BgL_alphazd2bodyzd2_1476))),
																				BUNSPEC);
																			((((BgL_nodezf2effectzf2_bglt)
																						COBJECT(((BgL_nodezf2effectzf2_bglt)
																								BgL_new1123z00_1511)))->
																					BgL_keyz00) =
																				((obj_t) BINT(-1L)), BUNSPEC);
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1123z00_1511))->
																					BgL_bindingsz00) =
																				((obj_t) BgL_bindingsz00_1451),
																				BUNSPEC);
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1123z00_1511))->
																					BgL_bodyz00) =
																				((BgL_nodez00_bglt) BgL_inodez00_1477),
																				BUNSPEC);
																			((((BgL_letzd2varzd2_bglt)
																						COBJECT(BgL_new1123z00_1511))->
																					BgL_removablezf3zf3) =
																				((bool_t) ((bool_t) 1)), BUNSPEC);
																			BgL_ibodyz00_1478 = BgL_new1123z00_1511;
																		}
																		{	/* Inline/simple.scm 138 */

																			{
																				obj_t BgL_ll1259z00_1480;
																				obj_t BgL_ll1260z00_1481;

																				BgL_ll1259z00_1480 =
																					BgL_reductorsz00_1450;
																				BgL_ll1260z00_1481 =
																					BgL_formalsz00_1448;
																			BgL_zc3z04anonymousza31322ze3z87_1482:
																				if (NULLP(BgL_ll1259z00_1480))
																					{	/* Inline/simple.scm 145 */
																						((bool_t) 1);
																					}
																				else
																					{	/* Inline/simple.scm 145 */
																						{	/* Inline/simple.scm 146 */
																							obj_t BgL_reductorz00_1484;
																							obj_t BgL_formalz00_1485;

																							BgL_reductorz00_1484 =
																								CAR(
																								((obj_t) BgL_ll1259z00_1480));
																							BgL_formalz00_1485 =
																								CAR(
																								((obj_t) BgL_ll1260z00_1481));
																							{	/* Inline/simple.scm 146 */
																								bool_t BgL_test1943z00_2907;

																								{	/* Inline/simple.scm 146 */
																									obj_t BgL_classz00_2147;

																									BgL_classz00_2147 =
																										BGl_localz00zzast_varz00;
																									if (BGL_OBJECTP
																										(BgL_reductorz00_1484))
																										{	/* Inline/simple.scm 146 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_2149;
																											BgL_arg1807z00_2149 =
																												(BgL_objectz00_bglt)
																												(BgL_reductorz00_1484);
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Inline/simple.scm 146 */
																													long BgL_idxz00_2155;

																													BgL_idxz00_2155 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_2149);
																													BgL_test1943z00_2907 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_2155 +
																																2L)) ==
																														BgL_classz00_2147);
																												}
																											else
																												{	/* Inline/simple.scm 146 */
																													bool_t
																														BgL_res1862z00_2180;
																													{	/* Inline/simple.scm 146 */
																														obj_t
																															BgL_oclassz00_2163;
																														{	/* Inline/simple.scm 146 */
																															obj_t
																																BgL_arg1815z00_2171;
																															long
																																BgL_arg1816z00_2172;
																															BgL_arg1815z00_2171
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Inline/simple.scm 146 */
																																long
																																	BgL_arg1817z00_2173;
																																BgL_arg1817z00_2173
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_2149);
																																BgL_arg1816z00_2172
																																	=
																																	(BgL_arg1817z00_2173
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_2163
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_2171,
																																BgL_arg1816z00_2172);
																														}
																														{	/* Inline/simple.scm 146 */
																															bool_t
																																BgL__ortest_1115z00_2164;
																															BgL__ortest_1115z00_2164
																																=
																																(BgL_classz00_2147
																																==
																																BgL_oclassz00_2163);
																															if (BgL__ortest_1115z00_2164)
																																{	/* Inline/simple.scm 146 */
																																	BgL_res1862z00_2180
																																		=
																																		BgL__ortest_1115z00_2164;
																																}
																															else
																																{	/* Inline/simple.scm 146 */
																																	long
																																		BgL_odepthz00_2165;
																																	{	/* Inline/simple.scm 146 */
																																		obj_t
																																			BgL_arg1804z00_2166;
																																		BgL_arg1804z00_2166
																																			=
																																			(BgL_oclassz00_2163);
																																		BgL_odepthz00_2165
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_2166);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_2165))
																																		{	/* Inline/simple.scm 146 */
																																			obj_t
																																				BgL_arg1802z00_2168;
																																			{	/* Inline/simple.scm 146 */
																																				obj_t
																																					BgL_arg1803z00_2169;
																																				BgL_arg1803z00_2169
																																					=
																																					(BgL_oclassz00_2163);
																																				BgL_arg1802z00_2168
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_2169,
																																					2L);
																																			}
																																			BgL_res1862z00_2180
																																				=
																																				(BgL_arg1802z00_2168
																																				==
																																				BgL_classz00_2147);
																																		}
																																	else
																																		{	/* Inline/simple.scm 146 */
																																			BgL_res1862z00_2180
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test1943z00_2907 =
																														BgL_res1862z00_2180;
																												}
																										}
																									else
																										{	/* Inline/simple.scm 146 */
																											BgL_test1943z00_2907 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test1943z00_2907)
																									{	/* Inline/simple.scm 147 */
																										bool_t BgL_arg1325z00_1487;

																										BgL_arg1325z00_1487 =
																											(((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_localz00_bglt)
																															BgL_formalz00_1485))))->
																											BgL_userzf3zf3);
																										((((BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															((BgL_localz00_bglt) BgL_reductorz00_1484))))->BgL_userzf3zf3) = ((bool_t) BgL_arg1325z00_1487), BUNSPEC);
																									}
																								else
																									{	/* Inline/simple.scm 146 */
																										BFALSE;
																									}
																							}
																						}
																						{	/* Inline/simple.scm 145 */
																							obj_t BgL_arg1326z00_1488;
																							obj_t BgL_arg1327z00_1489;

																							BgL_arg1326z00_1488 =
																								CDR(
																								((obj_t) BgL_ll1259z00_1480));
																							BgL_arg1327z00_1489 =
																								CDR(
																								((obj_t) BgL_ll1260z00_1481));
																							{
																								obj_t BgL_ll1260z00_2941;
																								obj_t BgL_ll1259z00_2940;

																								BgL_ll1259z00_2940 =
																									BgL_arg1326z00_1488;
																								BgL_ll1260z00_2941 =
																									BgL_arg1327z00_1489;
																								BgL_ll1260z00_1481 =
																									BgL_ll1260z00_2941;
																								BgL_ll1259z00_1480 =
																									BgL_ll1259z00_2940;
																								goto
																									BgL_zc3z04anonymousza31322ze3z87_1482;
																							}
																						}
																					}
																			}
																			{	/* Inline/simple.scm 154 */
																				bool_t BgL_test1948z00_2942;

																				if (
																					(((obj_t) BgL_typez00_1456) ==
																						BGl_za2_za2z00zztype_cachez00))
																					{	/* Inline/simple.scm 154 */
																						BgL_test1948z00_2942 = ((bool_t) 1);
																					}
																				else
																					{	/* Inline/simple.scm 154 */
																						if (
																							(((obj_t) BgL_typez00_1456) ==
																								BGl_za2objza2z00zztype_cachez00))
																							{	/* Inline/simple.scm 155 */
																								BgL_test1948z00_2942 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Inline/simple.scm 155 */
																								if (
																									(((obj_t) BgL_typez00_1456) ==
																										((obj_t)
																											(((BgL_nodez00_bglt)
																													COBJECT
																													(BgL_alphazd2bodyzd2_1476))->
																												BgL_typez00))))
																									{	/* Inline/simple.scm 156 */
																										BgL_test1948z00_2942 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Inline/simple.scm 157 */
																										BgL_typez00_bglt
																											BgL_arg1348z00_1509;
																										BgL_arg1348z00_1509 =
																											BGl_getzd2typezd2zztype_typeofz00
																											(BgL_alphazd2bodyzd2_1476,
																											((bool_t) 1));
																										BgL_test1948z00_2942 =
																											(((obj_t)
																												BgL_typez00_1456) ==
																											((obj_t)
																												BgL_arg1348z00_1509));
																									}
																							}
																					}
																				if (BgL_test1948z00_2942)
																					{	/* Inline/simple.scm 154 */
																						if (CBOOL(
																								(((BgL_appz00_bglt) COBJECT(
																											((BgL_appz00_bglt)
																												BgL_nodez00_3)))->
																									BgL_stackablez00)))
																							{	/* Inline/simple.scm 159 */
																								return
																									BGl_stackablez12z12zzinline_simplez00
																									(((BgL_appz00_bglt)
																										BgL_nodez00_3),
																									((BgL_nodez00_bglt)
																										BgL_ibodyz00_1478));
																							}
																						else
																							{	/* Inline/simple.scm 159 */
																								return
																									((BgL_nodez00_bglt)
																									BgL_ibodyz00_1478);
																							}
																					}
																				else
																					{	/* Inline/simple.scm 162 */
																						BgL_localz00_bglt BgL_varz00_1498;

																						BgL_varz00_1498 =
																							BGl_makezd2localzd2svarz00zzast_localz00
																							(BGl_gensymz00zz__r4_symbols_6_4z00
																							(CNST_TABLE_REF(2)),
																							BgL_typez00_1456);
																						{	/* Inline/simple.scm 163 */
																							BgL_letzd2varzd2_bglt
																								BgL_new1127z00_1499;
																							{	/* Inline/simple.scm 164 */
																								BgL_letzd2varzd2_bglt
																									BgL_new1125z00_1505;
																								BgL_new1125z00_1505 =
																									((BgL_letzd2varzd2_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_letzd2varzd2_bgl))));
																								{	/* Inline/simple.scm 164 */
																									long BgL_arg1343z00_1506;

																									BgL_arg1343z00_1506 =
																										BGL_CLASS_NUM
																										(BGl_letzd2varzd2zzast_nodez00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1125z00_1505),
																										BgL_arg1343z00_1506);
																								}
																								{	/* Inline/simple.scm 164 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_2973;
																									BgL_tmpz00_2973 =
																										((BgL_objectz00_bglt)
																										BgL_new1125z00_1505);
																									BGL_OBJECT_WIDENING_SET
																										(BgL_tmpz00_2973, BFALSE);
																								}
																								((BgL_objectz00_bglt)
																									BgL_new1125z00_1505);
																								BgL_new1127z00_1499 =
																									BgL_new1125z00_1505;
																							}
																							((((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												BgL_new1127z00_1499)))->
																									BgL_locz00) =
																								((obj_t) BgL_locz00_1455),
																								BUNSPEC);
																							((((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												BgL_new1127z00_1499)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt)
																									BgL_typez00_1456), BUNSPEC);
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1127z00_1499)))->
																									BgL_sidezd2effectzd2) =
																								((obj_t)
																									BBOOL
																									(BGl_sidezd2effectzf3z21zzeffect_effectz00
																										(((BgL_nodez00_bglt)
																												BgL_ibodyz00_1478)))),
																								BUNSPEC);
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1127z00_1499)))->
																									BgL_keyz00) =
																								((obj_t) BINT(-1L)), BUNSPEC);
																							{
																								obj_t BgL_auxz00_2989;

																								{	/* Inline/simple.scm 167 */
																									obj_t BgL_arg1339z00_1500;

																									BgL_arg1339z00_1500 =
																										MAKE_YOUNG_PAIR(
																										((obj_t) BgL_varz00_1498),
																										((obj_t)
																											BgL_ibodyz00_1478));
																									{	/* Inline/simple.scm 167 */
																										obj_t BgL_list1340z00_1501;

																										BgL_list1340z00_1501 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1339z00_1500,
																											BNIL);
																										BgL_auxz00_2989 =
																											BgL_list1340z00_1501;
																								}}
																								((((BgL_letzd2varzd2_bglt)
																											COBJECT
																											(BgL_new1127z00_1499))->
																										BgL_bindingsz00) =
																									((obj_t) BgL_auxz00_2989),
																									BUNSPEC);
																							}
																							{
																								BgL_nodez00_bglt
																									BgL_auxz00_2995;
																								{	/* Inline/simple.scm 168 */
																									BgL_refz00_bglt
																										BgL_new1130z00_1502;
																									{	/* Inline/simple.scm 169 */
																										BgL_refz00_bglt
																											BgL_new1129z00_1503;
																										BgL_new1129z00_1503 =
																											((BgL_refz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_refz00_bgl))));
																										{	/* Inline/simple.scm 169 */
																											long BgL_arg1342z00_1504;

																											{	/* Inline/simple.scm 169 */
																												obj_t BgL_classz00_2192;

																												BgL_classz00_2192 =
																													BGl_refz00zzast_nodez00;
																												BgL_arg1342z00_1504 =
																													BGL_CLASS_NUM
																													(BgL_classz00_2192);
																											}
																											BGL_OBJECT_CLASS_NUM_SET(
																												((BgL_objectz00_bglt)
																													BgL_new1129z00_1503),
																												BgL_arg1342z00_1504);
																										}
																										{	/* Inline/simple.scm 169 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_3000;
																											BgL_tmpz00_3000 =
																												((BgL_objectz00_bglt)
																												BgL_new1129z00_1503);
																											BGL_OBJECT_WIDENING_SET
																												(BgL_tmpz00_3000,
																												BFALSE);
																										}
																										((BgL_objectz00_bglt)
																											BgL_new1129z00_1503);
																										BgL_new1130z00_1502 =
																											BgL_new1129z00_1503;
																									}
																									((((BgL_nodez00_bglt) COBJECT(
																													((BgL_nodez00_bglt)
																														BgL_new1130z00_1502)))->
																											BgL_locz00) =
																										((obj_t) BgL_locz00_1455),
																										BUNSPEC);
																									((((BgL_nodez00_bglt)
																												COBJECT((
																														(BgL_nodez00_bglt)
																														BgL_new1130z00_1502)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt)
																											BgL_typez00_1456),
																										BUNSPEC);
																									((((BgL_varz00_bglt)
																												COBJECT((
																														(BgL_varz00_bglt)
																														BgL_new1130z00_1502)))->
																											BgL_variablez00) =
																										((BgL_variablez00_bglt) (
																												(BgL_variablez00_bglt)
																												BgL_varz00_1498)),
																										BUNSPEC);
																									BgL_auxz00_2995 =
																										((BgL_nodez00_bglt)
																										BgL_new1130z00_1502);
																								}
																								((((BgL_letzd2varzd2_bglt)
																											COBJECT
																											(BgL_new1127z00_1499))->
																										BgL_bodyz00) =
																									((BgL_nodez00_bglt)
																										BgL_auxz00_2995), BUNSPEC);
																							}
																							((((BgL_letzd2varzd2_bglt)
																										COBJECT
																										(BgL_new1127z00_1499))->
																									BgL_removablezf3zf3) =
																								((bool_t) ((bool_t) 1)),
																								BUNSPEC);
																							return ((BgL_nodez00_bglt)
																								BgL_new1127z00_1499);
																						}
																					}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &inline-app-simple */
	BgL_nodez00_bglt BGl_z62inlinezd2appzd2simplez62zzinline_simplez00(obj_t
		BgL_envz00_2401, obj_t BgL_nodez00_2402, obj_t BgL_kfactorz00_2403,
		obj_t BgL_stackz00_2404, obj_t BgL_msgz00_2405)
	{
		{	/* Inline/simple.scm 46 */
			return
				BGl_inlinezd2appzd2simplez00zzinline_simplez00(
				((BgL_nodez00_bglt) BgL_nodez00_2402),
				(long) CINT(BgL_kfactorz00_2403), BgL_stackz00_2404, BgL_msgz00_2405);
		}

	}



/* stackable! */
	BgL_nodez00_bglt BGl_stackablez12z12zzinline_simplez00(BgL_appz00_bglt
		BgL_oldz00_7, BgL_nodez00_bglt BgL_newz00_8)
	{
		{	/* Inline/simple.scm 178 */
			BGl_occurzd2nodez12zc0zzast_occurz00(BgL_newz00_8);
			{	/* Inline/simple.scm 180 */
				obj_t BgL_rnewz00_1667;

				BgL_rnewz00_1667 = BGl_nodezd2removez12zc0zzast_removez00(BgL_newz00_8);
				{	/* Inline/simple.scm 181 */
					bool_t BgL_test1953z00_3020;

					{	/* Inline/simple.scm 181 */
						obj_t BgL_classz00_2196;

						BgL_classz00_2196 = BGl_appz00zzast_nodez00;
						if (BGL_OBJECTP(BgL_rnewz00_1667))
							{	/* Inline/simple.scm 181 */
								BgL_objectz00_bglt BgL_arg1807z00_2198;

								BgL_arg1807z00_2198 = (BgL_objectz00_bglt) (BgL_rnewz00_1667);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Inline/simple.scm 181 */
										long BgL_idxz00_2204;

										BgL_idxz00_2204 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2198);
										BgL_test1953z00_3020 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2204 + 3L)) == BgL_classz00_2196);
									}
								else
									{	/* Inline/simple.scm 181 */
										bool_t BgL_res1864z00_2229;

										{	/* Inline/simple.scm 181 */
											obj_t BgL_oclassz00_2212;

											{	/* Inline/simple.scm 181 */
												obj_t BgL_arg1815z00_2220;
												long BgL_arg1816z00_2221;

												BgL_arg1815z00_2220 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Inline/simple.scm 181 */
													long BgL_arg1817z00_2222;

													BgL_arg1817z00_2222 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2198);
													BgL_arg1816z00_2221 =
														(BgL_arg1817z00_2222 - OBJECT_TYPE);
												}
												BgL_oclassz00_2212 =
													VECTOR_REF(BgL_arg1815z00_2220, BgL_arg1816z00_2221);
											}
											{	/* Inline/simple.scm 181 */
												bool_t BgL__ortest_1115z00_2213;

												BgL__ortest_1115z00_2213 =
													(BgL_classz00_2196 == BgL_oclassz00_2212);
												if (BgL__ortest_1115z00_2213)
													{	/* Inline/simple.scm 181 */
														BgL_res1864z00_2229 = BgL__ortest_1115z00_2213;
													}
												else
													{	/* Inline/simple.scm 181 */
														long BgL_odepthz00_2214;

														{	/* Inline/simple.scm 181 */
															obj_t BgL_arg1804z00_2215;

															BgL_arg1804z00_2215 = (BgL_oclassz00_2212);
															BgL_odepthz00_2214 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2215);
														}
														if ((3L < BgL_odepthz00_2214))
															{	/* Inline/simple.scm 181 */
																obj_t BgL_arg1802z00_2217;

																{	/* Inline/simple.scm 181 */
																	obj_t BgL_arg1803z00_2218;

																	BgL_arg1803z00_2218 = (BgL_oclassz00_2212);
																	BgL_arg1802z00_2217 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2218,
																		3L);
																}
																BgL_res1864z00_2229 =
																	(BgL_arg1802z00_2217 == BgL_classz00_2196);
															}
														else
															{	/* Inline/simple.scm 181 */
																BgL_res1864z00_2229 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1953z00_3020 = BgL_res1864z00_2229;
									}
							}
						else
							{	/* Inline/simple.scm 181 */
								BgL_test1953z00_3020 = ((bool_t) 0);
							}
					}
					if (BgL_test1953z00_3020)
						{	/* Inline/simple.scm 181 */
							((((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_rnewz00_1667)))->
									BgL_stackablez00) = ((obj_t) BTRUE), BUNSPEC);
							BgL_rnewz00_1667;
						}
					else
						{	/* Inline/simple.scm 181 */
							((obj_t) BgL_newz00_8);
						}
				}
				return BgL_newz00_8;
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinline_simplez00(void)
	{
		{	/* Inline/simple.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinline_simplez00(void)
	{
		{	/* Inline/simple.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinline_simplez00(void)
	{
		{	/* Inline/simple.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinline_simplez00(void)
	{
		{	/* Inline/simple.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzast_occurz00(282085879L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzinline_inlinez00(20504962L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzinline_siza7eza7(243191744L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(53595773L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
			return
				BGl_modulezd2initializa7ationz75zzeffect_spreadz00(348216764L,
				BSTRING_TO_STRING(BGl_string1873z00zzinline_simplez00));
		}

	}

#ifdef __cplusplus
}
#endif
