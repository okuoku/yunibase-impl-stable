/*===========================================================================*/
/*   (Inline/loop.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Inline/loop.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INLINE_LOOP_TYPE_DEFINITIONS
#define BGL_INLINE_LOOP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_isfunz00_bgl
	{
		struct BgL_nodez00_bgl *BgL_originalzd2bodyzd2;
		obj_t BgL_recursivezd2callszd2;
		bool_t BgL_tailrecz00;
	}               *BgL_isfunz00_bglt;


#endif													// BGL_INLINE_LOOP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern bool_t
		BGl_iszd2recursivezf3z21zzinline_recursionz00(BgL_variablez00_bglt);
	extern obj_t BGl_setqz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2condition1342z70zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62findzd2letzd2funzf3zd2makezd2b1311z91zzinline_loopz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzinline_loopz00 = BUNSPEC;
	static obj_t
		BGl_z62findzd2letzd2funzf3zd2jumpzd2e1309z91zzinline_loopz00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static obj_t BGl_z62findzd2letzd2funzf3zd2extern1291z43zzinline_loopz00(obj_t,
		obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62findzd2letzd2funzf3zd2condit1297z43zzinline_loopz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2atom1319z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2letzd2fun1348za2zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_innerzd2loopzf3z21zzinline_loopz00(BgL_variablez00_bglt);
	static obj_t BGl_z62findzd2letzd2funzf3zd2app1285z43zzinline_loopz00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzinline_loopz00(void);
	static obj_t
		BGl_z62findzd2letzd2funzf3zd2boxzd2se1315z91zzinline_loopz00(obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_EXPORTED_DECL bool_t
		BGl_iszd2loopzf3z21zzinline_loopz00(BgL_variablez00_bglt);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzinline_loopz00(void);
	static obj_t BGl_findzd2letzd2funzf3zf3zzinline_loopz00(BgL_nodez00_bglt);
	static obj_t BGl_objectzd2initzd2zzinline_loopz00(void);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_nodez00_bglt, BgL_localz00_bglt,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2appzd2ly1331za2zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62nestzd2loopz12za2zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2jumpzd2exzd2i1354z70zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzinline_loopz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2extern1335z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2setzd2exzd2it1352z70zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62innerzd2loopzf3z43zzinline_loopz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2switch1346z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nestzd2loopz121316za2zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2funcall1333z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2kwote1321z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_nestzd2loopz12za2z62zzinline_loopz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2letzd2funzf3zd2letzd2va1305z91zzinline_loopz00(obj_t, obj_t);
	static obj_t
		BGl_z62findzd2letzd2funzf3zd2letzd2fu1303z91zzinline_loopz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2letzd2var1350za2zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2cast1337z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62findzd2letzd2funzf3zd2sequen1281z43zzinline_loopz00(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2makezd2box1356za2zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2boxzd2ref1358za2zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzinline_loopz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_recursionz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_variantz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2boxzd2setz121360zb0zzinline_loopz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2setq1340z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2sync1327z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static bool_t BGl_findzd2letzd2funzf3za2z51zzinline_loopz00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2fail1344z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzinline_loopz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinline_loopz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzinline_loopz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinline_loopz00(void);
	static obj_t
		BGl_z62findzd2letzd2funzf3zd2setzd2ex1307z91zzinline_loopz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2sequence1325z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2var1323z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62findzd2letzd2funzf3z91zzinline_loopz00(obj_t, obj_t);
	static obj_t BGl_z62findzd2letzd2funzf31278z91zzinline_loopz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62iszd2loopzf3z43zzinline_loopz00(obj_t, obj_t);
	static obj_t BGl_z62findzd2letzd2funzf3zd2switch1301z43zzinline_loopz00(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62findzd2letzd2funzf3zd2cast1293z43zzinline_loopz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62findzd2letzd2funzf3zd2appzd2ly1287z91zzinline_loopz00(obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62findzd2letzd2funzf3zd2setq1295z43zzinline_loopz00(obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2app1329z70zzinline_loopz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62findzd2letzd2funzf3zd2sync1283z43zzinline_loopz00(obj_t,
		obj_t);
	static obj_t BGl_z62findzd2letzd2funzf3zd2funcal1289z43zzinline_loopz00(obj_t,
		obj_t);
	static obj_t BGl_z62findzd2letzd2funzf3zd2fail1299z43zzinline_loopz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62findzd2letzd2funzf3zd2boxzd2re1313z91zzinline_loopz00(obj_t, obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21937za7,
		BGl_z62findzd2letzd2funzf3z91zzinline_loopz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_innerzd2loopzf3zd2envzf3zzinline_loopz00,
		BgL_bgl_za762innerza7d2loopza71938za7,
		BGl_z62innerzd2loopzf3z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_iszd2loopzf3zd2envzf3zzinline_loopz00,
		BgL_bgl_za762isza7d2loopza7f3za71939z00,
		BGl_z62iszd2loopzf3z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1900z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21940za7,
		BGl_z62findzd2letzd2funzf3zd2cast1293z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1901z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21941za7,
		BGl_z62findzd2letzd2funzf3zd2setq1295z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1902z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21942za7,
		BGl_z62findzd2letzd2funzf3zd2condit1297z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1903z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21943za7,
		BGl_z62findzd2letzd2funzf3zd2fail1299z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1904z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21944za7,
		BGl_z62findzd2letzd2funzf3zd2switch1301z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1905z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21945za7,
		BGl_z62findzd2letzd2funzf3zd2letzd2fu1303z91zzinline_loopz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1906z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21946za7,
		BGl_z62findzd2letzd2funzf3zd2letzd2va1305z91zzinline_loopz00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1913z00zzinline_loopz00,
		BgL_bgl_string1913za700za7za7i1947za7, "nest-loop!", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1907z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21948za7,
		BGl_z62findzd2letzd2funzf3zd2setzd2ex1307z91zzinline_loopz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1908z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21949za7,
		BGl_z62findzd2letzd2funzf3zd2jumpzd2e1309z91zzinline_loopz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1909z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21950za7,
		BGl_z62findzd2letzd2funzf3zd2makezd2b1311z91zzinline_loopz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1910z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21951za7,
		BGl_z62findzd2letzd2funzf3zd2boxzd2re1313z91zzinline_loopz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1911z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21952za7,
		BGl_z62findzd2letzd2funzf3zd2boxzd2se1315z91zzinline_loopz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1912z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711953za7,
		BGl_z62nestzd2loopz12zd2atom1319z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1914z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711954za7,
		BGl_z62nestzd2loopz12zd2kwote1321z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1915z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711955za7,
		BGl_z62nestzd2loopz12zd2var1323z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1916z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711956za7,
		BGl_z62nestzd2loopz12zd2sequence1325z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1917z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711957za7,
		BGl_z62nestzd2loopz12zd2sync1327z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1918z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711958za7,
		BGl_z62nestzd2loopz12zd2app1329z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1919z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711959za7,
		BGl_z62nestzd2loopz12zd2appzd2ly1331za2zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711960za7,
		BGl_z62nestzd2loopz12za2zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1920z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711961za7,
		BGl_z62nestzd2loopz12zd2funcall1333z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1921z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711962za7,
		BGl_z62nestzd2loopz12zd2extern1335z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1922z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711963za7,
		BGl_z62nestzd2loopz12zd2cast1337z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1923z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711964za7,
		BGl_z62nestzd2loopz12zd2setq1340z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1924z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711965za7,
		BGl_z62nestzd2loopz12zd2condition1342z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1925z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711966za7,
		BGl_z62nestzd2loopz12zd2fail1344z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1926z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711967za7,
		BGl_z62nestzd2loopz12zd2switch1346z70zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1927z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711968za7,
		BGl_z62nestzd2loopz12zd2letzd2fun1348za2zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1934z00zzinline_loopz00,
		BgL_bgl_string1934za700za7za7i1969za7, "inline_loop", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1928z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711970za7,
		BGl_z62nestzd2loopz12zd2letzd2var1350za2zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1935z00zzinline_loopz00,
		BgL_bgl_string1935za700za7za7i1971za7, "nest-loop!1316 done ", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1929z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711972za7,
		BGl_z62nestzd2loopz12zd2setzd2exzd2it1352z70zzinline_loopz00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1930z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711973za7,
		BGl_z62nestzd2loopz12zd2jumpzd2exzd2i1354z70zzinline_loopz00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1931z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711974za7,
		BGl_z62nestzd2loopz12zd2makezd2box1356za2zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1932z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711975za7,
		BGl_z62nestzd2loopz12zd2boxzd2ref1358za2zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1933z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711976za7,
		BGl_z62nestzd2loopz12zd2boxzd2setz121360zb0zzinline_loopz00, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string1889z00zzinline_loopz00,
		BgL_bgl_string1889za700za7za7i1977za7, "find-let-fun?1278", 17);
	      DEFINE_STRING(BGl_string1891z00zzinline_loopz00,
		BgL_bgl_string1891za700za7za7i1978za7, "nest-loop!1316", 14);
	      DEFINE_STRING(BGl_string1892z00zzinline_loopz00,
		BgL_bgl_string1892za700za7za7i1979za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1894z00zzinline_loopz00,
		BgL_bgl_string1894za700za7za7i1980za7, "find-let-fun?", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21981za7,
		BGl_z62findzd2letzd2funzf31278z91zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1890z00zzinline_loopz00,
		BgL_bgl_za762nestza7d2loopza711982za7,
		BGl_z62nestzd2loopz121316za2zzinline_loopz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1893z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21983za7,
		BGl_z62findzd2letzd2funzf3zd2sequen1281z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1895z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21984za7,
		BGl_z62findzd2letzd2funzf3zd2sync1283z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1896z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21985za7,
		BGl_z62findzd2letzd2funzf3zd2app1285z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1897z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21986za7,
		BGl_z62findzd2letzd2funzf3zd2appzd2ly1287z91zzinline_loopz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1898z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21987za7,
		BGl_z62findzd2letzd2funzf3zd2funcal1289z43zzinline_loopz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1899z00zzinline_loopz00,
		BgL_bgl_za762findza7d2letza7d21988za7,
		BGl_z62findzd2letzd2funzf3zd2extern1291z43zzinline_loopz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzinline_loopz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinline_loopz00(long
		BgL_checksumz00_2642, char *BgL_fromz00_2643)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinline_loopz00))
				{
					BGl_requirezd2initializa7ationz75zzinline_loopz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinline_loopz00();
					BGl_libraryzd2moduleszd2initz00zzinline_loopz00();
					BGl_cnstzd2initzd2zzinline_loopz00();
					BGl_importedzd2moduleszd2initz00zzinline_loopz00();
					BGl_genericzd2initzd2zzinline_loopz00();
					BGl_methodzd2initzd2zzinline_loopz00();
					return BGl_toplevelzd2initzd2zzinline_loopz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinline_loopz00(void)
	{
		{	/* Inline/loop.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "inline_loop");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "inline_loop");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "inline_loop");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "inline_loop");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"inline_loop");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "inline_loop");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"inline_loop");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "inline_loop");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "inline_loop");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"inline_loop");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinline_loopz00(void)
	{
		{	/* Inline/loop.scm 15 */
			{	/* Inline/loop.scm 15 */
				obj_t BgL_cportz00_2502;

				{	/* Inline/loop.scm 15 */
					obj_t BgL_stringz00_2509;

					BgL_stringz00_2509 = BGl_string1935z00zzinline_loopz00;
					{	/* Inline/loop.scm 15 */
						obj_t BgL_startz00_2510;

						BgL_startz00_2510 = BINT(0L);
						{	/* Inline/loop.scm 15 */
							obj_t BgL_endz00_2511;

							BgL_endz00_2511 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2509)));
							{	/* Inline/loop.scm 15 */

								BgL_cportz00_2502 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2509, BgL_startz00_2510, BgL_endz00_2511);
				}}}}
				{
					long BgL_iz00_2503;

					BgL_iz00_2503 = 1L;
				BgL_loopz00_2504:
					if ((BgL_iz00_2503 == -1L))
						{	/* Inline/loop.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Inline/loop.scm 15 */
							{	/* Inline/loop.scm 15 */
								obj_t BgL_arg1936z00_2505;

								{	/* Inline/loop.scm 15 */

									{	/* Inline/loop.scm 15 */
										obj_t BgL_locationz00_2507;

										BgL_locationz00_2507 = BBOOL(((bool_t) 0));
										{	/* Inline/loop.scm 15 */

											BgL_arg1936z00_2505 =
												BGl_readz00zz__readerz00(BgL_cportz00_2502,
												BgL_locationz00_2507);
										}
									}
								}
								{	/* Inline/loop.scm 15 */
									int BgL_tmpz00_2673;

									BgL_tmpz00_2673 = (int) (BgL_iz00_2503);
									CNST_TABLE_SET(BgL_tmpz00_2673, BgL_arg1936z00_2505);
							}}
							{	/* Inline/loop.scm 15 */
								int BgL_auxz00_2508;

								BgL_auxz00_2508 = (int) ((BgL_iz00_2503 - 1L));
								{
									long BgL_iz00_2678;

									BgL_iz00_2678 = (long) (BgL_auxz00_2508);
									BgL_iz00_2503 = BgL_iz00_2678;
									goto BgL_loopz00_2504;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinline_loopz00(void)
	{
		{	/* Inline/loop.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzinline_loopz00(void)
	{
		{	/* Inline/loop.scm 15 */
			return BUNSPEC;
		}

	}



/* is-loop? */
	BGL_EXPORTED_DEF bool_t
		BGl_iszd2loopzf3z21zzinline_loopz00(BgL_variablez00_bglt BgL_variablez00_3)
	{
		{	/* Inline/loop.scm 39 */
			if (BGl_iszd2recursivezf3z21zzinline_recursionz00(BgL_variablez00_3))
				{	/* Inline/loop.scm 42 */
					long BgL_arg1370z00_1568;

					{	/* Inline/loop.scm 42 */
						obj_t BgL_arg1371z00_1569;

						{	/* Inline/loop.scm 42 */
							BgL_sfunz00_bglt BgL_oz00_2127;

							BgL_oz00_2127 =
								((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_3))->
									BgL_valuez00));
							{
								BgL_isfunz00_bglt BgL_auxz00_2685;

								{
									obj_t BgL_auxz00_2686;

									{	/* Inline/loop.scm 42 */
										BgL_objectz00_bglt BgL_tmpz00_2687;

										BgL_tmpz00_2687 = ((BgL_objectz00_bglt) BgL_oz00_2127);
										BgL_auxz00_2686 = BGL_OBJECT_WIDENING(BgL_tmpz00_2687);
									}
									BgL_auxz00_2685 = ((BgL_isfunz00_bglt) BgL_auxz00_2686);
								}
								BgL_arg1371z00_1569 =
									(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_2685))->
									BgL_recursivezd2callszd2);
						}}
						BgL_arg1370z00_1568 = bgl_list_length(BgL_arg1371z00_1569);
					}
					return (BgL_arg1370z00_1568 == 1L);
				}
			else
				{	/* Inline/loop.scm 41 */
					return ((bool_t) 0);
				}
		}

	}



/* &is-loop? */
	obj_t BGl_z62iszd2loopzf3z43zzinline_loopz00(obj_t BgL_envz00_2325,
		obj_t BgL_variablez00_2326)
	{
		{	/* Inline/loop.scm 39 */
			return
				BBOOL(BGl_iszd2loopzf3z21zzinline_loopz00(
					((BgL_variablez00_bglt) BgL_variablez00_2326)));
		}

	}



/* inner-loop? */
	BGL_EXPORTED_DEF bool_t
		BGl_innerzd2loopzf3z21zzinline_loopz00(BgL_variablez00_bglt
		BgL_variablez00_4)
	{
		{	/* Inline/loop.scm 49 */
			{	/* Inline/loop.scm 50 */
				obj_t BgL_arg1376z00_2130;

				BgL_arg1376z00_2130 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_4))->
									BgL_valuez00))))->BgL_bodyz00);
				return
					CBOOL(BGl_findzd2letzd2funzf3zf3zzinline_loopz00(((BgL_nodez00_bglt)
							BgL_arg1376z00_2130)));
			}
		}

	}



/* &inner-loop? */
	obj_t BGl_z62innerzd2loopzf3z43zzinline_loopz00(obj_t BgL_envz00_2327,
		obj_t BgL_variablez00_2328)
	{
		{	/* Inline/loop.scm 49 */
			return
				BBOOL(BGl_innerzd2loopzf3z21zzinline_loopz00(
					((BgL_variablez00_bglt) BgL_variablez00_2328)));
		}

	}



/* find-let-fun?* */
	bool_t BGl_findzd2letzd2funzf3za2z51zzinline_loopz00(obj_t BgL_nodeza2za2_24)
	{
		{	/* Inline/loop.scm 198 */
			{
				obj_t BgL_nodeza2za2_1574;

				BgL_nodeza2za2_1574 = BgL_nodeza2za2_24;
			BgL_zc3z04anonymousza31378ze3z87_1575:
				if (NULLP(BgL_nodeza2za2_1574))
					{	/* Inline/loop.scm 201 */
						return ((bool_t) 0);
					}
				else
					{	/* Inline/loop.scm 203 */
						bool_t BgL_test1993z00_2708;

						{	/* Inline/loop.scm 203 */
							obj_t BgL_arg1422z00_1580;

							BgL_arg1422z00_1580 = CAR(((obj_t) BgL_nodeza2za2_1574));
							BgL_test1993z00_2708 =
								CBOOL(BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
									((BgL_nodez00_bglt) BgL_arg1422z00_1580)));
						}
						if (BgL_test1993z00_2708)
							{	/* Inline/loop.scm 203 */
								return ((bool_t) 1);
							}
						else
							{	/* Inline/loop.scm 206 */
								obj_t BgL_arg1421z00_1579;

								BgL_arg1421z00_1579 = CDR(((obj_t) BgL_nodeza2za2_1574));
								{
									obj_t BgL_nodeza2za2_2716;

									BgL_nodeza2za2_2716 = BgL_arg1421z00_1579;
									BgL_nodeza2za2_1574 = BgL_nodeza2za2_2716;
									goto BgL_zc3z04anonymousza31378ze3z87_1575;
								}
							}
					}
			}
		}

	}



/* nest-loop!* */
	obj_t BGl_nestzd2loopz12za2z62zzinline_loopz00(obj_t BgL_nodeza2za2_91,
		obj_t BgL_varz00_92, obj_t BgL_nesterz00_93)
	{
		{	/* Inline/loop.scm 401 */
			{
				obj_t BgL_nodeza2za2_1583;

				BgL_nodeza2za2_1583 = BgL_nodeza2za2_91;
			BgL_zc3z04anonymousza31423ze3z87_1584:
				if (NULLP(BgL_nodeza2za2_1583))
					{	/* Inline/loop.scm 403 */
						return CNST_TABLE_REF(0);
					}
				else
					{	/* Inline/loop.scm 403 */
						{	/* Inline/loop.scm 406 */
							BgL_nodez00_bglt BgL_arg1434z00_1586;

							{	/* Inline/loop.scm 406 */
								obj_t BgL_arg1437z00_1587;

								BgL_arg1437z00_1587 = CAR(((obj_t) BgL_nodeza2za2_1583));
								BgL_arg1434z00_1586 =
									BGl_nestzd2loopz12zc0zzinline_loopz00(
									((BgL_nodez00_bglt) BgL_arg1437z00_1587),
									((BgL_localz00_bglt) BgL_varz00_92), BgL_nesterz00_93);
							}
							{	/* Inline/loop.scm 406 */
								obj_t BgL_auxz00_2727;
								obj_t BgL_tmpz00_2725;

								BgL_auxz00_2727 = ((obj_t) BgL_arg1434z00_1586);
								BgL_tmpz00_2725 = ((obj_t) BgL_nodeza2za2_1583);
								SET_CAR(BgL_tmpz00_2725, BgL_auxz00_2727);
							}
						}
						{	/* Inline/loop.scm 407 */
							obj_t BgL_arg1448z00_1588;

							BgL_arg1448z00_1588 = CDR(((obj_t) BgL_nodeza2za2_1583));
							{
								obj_t BgL_nodeza2za2_2732;

								BgL_nodeza2za2_2732 = BgL_arg1448z00_1588;
								BgL_nodeza2za2_1583 = BgL_nodeza2za2_2732;
								goto BgL_zc3z04anonymousza31423ze3z87_1584;
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinline_loopz00(void)
	{
		{	/* Inline/loop.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinline_loopz00(void)
	{
		{	/* Inline/loop.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_proc1888z00zzinline_loopz00, BGl_nodez00zzast_nodez00,
				BGl_string1889z00zzinline_loopz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_proc1890z00zzinline_loopz00, BGl_nodez00zzast_nodez00,
				BGl_string1891z00zzinline_loopz00);
		}

	}



/* &nest-loop!1316 */
	obj_t BGl_z62nestzd2loopz121316za2zzinline_loopz00(obj_t BgL_envz00_2331,
		obj_t BgL_nodez00_2332, obj_t BgL_localz00_2333, obj_t BgL_nesterz00_2334)
	{
		{	/* Inline/loop.scm 211 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string1892z00zzinline_loopz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2332)));
		}

	}



/* &find-let-fun?1278 */
	obj_t BGl_z62findzd2letzd2funzf31278z91zzinline_loopz00(obj_t BgL_envz00_2335,
		obj_t BgL_nodez00_2336)
	{
		{	/* Inline/loop.scm 55 */
			return BBOOL(((bool_t) 0));
		}

	}



/* find-let-fun? */
	obj_t BGl_findzd2letzd2funzf3zf3zzinline_loopz00(BgL_nodez00_bglt
		BgL_nodez00_5)
	{
		{	/* Inline/loop.scm 55 */
			{	/* Inline/loop.scm 55 */
				obj_t BgL_method1279z00_1600;

				{	/* Inline/loop.scm 55 */
					obj_t BgL_res1880z00_2169;

					{	/* Inline/loop.scm 55 */
						long BgL_objzd2classzd2numz00_2140;

						BgL_objzd2classzd2numz00_2140 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_5));
						{	/* Inline/loop.scm 55 */
							obj_t BgL_arg1811z00_2141;

							BgL_arg1811z00_2141 =
								PROCEDURE_REF(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
								(int) (1L));
							{	/* Inline/loop.scm 55 */
								int BgL_offsetz00_2144;

								BgL_offsetz00_2144 = (int) (BgL_objzd2classzd2numz00_2140);
								{	/* Inline/loop.scm 55 */
									long BgL_offsetz00_2145;

									BgL_offsetz00_2145 =
										((long) (BgL_offsetz00_2144) - OBJECT_TYPE);
									{	/* Inline/loop.scm 55 */
										long BgL_modz00_2146;

										BgL_modz00_2146 =
											(BgL_offsetz00_2145 >> (int) ((long) ((int) (4L))));
										{	/* Inline/loop.scm 55 */
											long BgL_restz00_2148;

											BgL_restz00_2148 =
												(BgL_offsetz00_2145 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Inline/loop.scm 55 */

												{	/* Inline/loop.scm 55 */
													obj_t BgL_bucketz00_2150;

													BgL_bucketz00_2150 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2141), BgL_modz00_2146);
													BgL_res1880z00_2169 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2150), BgL_restz00_2148);
					}}}}}}}}
					BgL_method1279z00_1600 = BgL_res1880z00_2169;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1279z00_1600, ((obj_t) BgL_nodez00_5));
			}
		}

	}



/* &find-let-fun? */
	obj_t BGl_z62findzd2letzd2funzf3z91zzinline_loopz00(obj_t BgL_envz00_2337,
		obj_t BgL_nodez00_2338)
	{
		{	/* Inline/loop.scm 55 */
			return
				BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
				((BgL_nodez00_bglt) BgL_nodez00_2338));
		}

	}



/* nest-loop! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_nodez00_bglt BgL_nodez00_25,
		BgL_localz00_bglt BgL_localz00_26, obj_t BgL_nesterz00_27)
	{
		{	/* Inline/loop.scm 211 */
			{	/* Inline/loop.scm 211 */
				obj_t BgL_method1317z00_1601;

				{	/* Inline/loop.scm 211 */
					obj_t BgL_res1885z00_2200;

					{	/* Inline/loop.scm 211 */
						long BgL_objzd2classzd2numz00_2171;

						BgL_objzd2classzd2numz00_2171 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_25));
						{	/* Inline/loop.scm 211 */
							obj_t BgL_arg1811z00_2172;

							BgL_arg1811z00_2172 =
								PROCEDURE_REF(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
								(int) (1L));
							{	/* Inline/loop.scm 211 */
								int BgL_offsetz00_2175;

								BgL_offsetz00_2175 = (int) (BgL_objzd2classzd2numz00_2171);
								{	/* Inline/loop.scm 211 */
									long BgL_offsetz00_2176;

									BgL_offsetz00_2176 =
										((long) (BgL_offsetz00_2175) - OBJECT_TYPE);
									{	/* Inline/loop.scm 211 */
										long BgL_modz00_2177;

										BgL_modz00_2177 =
											(BgL_offsetz00_2176 >> (int) ((long) ((int) (4L))));
										{	/* Inline/loop.scm 211 */
											long BgL_restz00_2179;

											BgL_restz00_2179 =
												(BgL_offsetz00_2176 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Inline/loop.scm 211 */

												{	/* Inline/loop.scm 211 */
													obj_t BgL_bucketz00_2181;

													BgL_bucketz00_2181 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2172), BgL_modz00_2177);
													BgL_res1885z00_2200 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2181), BgL_restz00_2179);
					}}}}}}}}
					BgL_method1317z00_1601 = BgL_res1885z00_2200;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1317z00_1601,
						((obj_t) BgL_nodez00_25),
						((obj_t) BgL_localz00_26), BgL_nesterz00_27));
			}
		}

	}



/* &nest-loop! */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12za2zzinline_loopz00(obj_t
		BgL_envz00_2339, obj_t BgL_nodez00_2340, obj_t BgL_localz00_2341,
		obj_t BgL_nesterz00_2342)
	{
		{	/* Inline/loop.scm 211 */
			return
				BGl_nestzd2loopz12zc0zzinline_loopz00(
				((BgL_nodez00_bglt) BgL_nodez00_2340),
				((BgL_localz00_bglt) BgL_localz00_2341), BgL_nesterz00_2342);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinline_loopz00(void)
	{
		{	/* Inline/loop.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1893z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_syncz00zzast_nodez00, BGl_proc1895z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_appz00zzast_nodez00, BGl_proc1896z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1897z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1898z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_externz00zzast_nodez00, BGl_proc1899z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_castz00zzast_nodez00, BGl_proc1900z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_setqz00zzast_nodez00, BGl_proc1901z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1902z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_failz00zzast_nodez00, BGl_proc1903z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_switchz00zzast_nodez00, BGl_proc1904z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1905z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1906z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1907z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1908z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1909z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1910z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2letzd2funzf3zd2envz21zzinline_loopz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1911z00zzinline_loopz00,
				BGl_string1894z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00, BGl_atomz00zzast_nodez00,
				BGl_proc1912z00zzinline_loopz00, BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00, BGl_kwotez00zzast_nodez00,
				BGl_proc1914z00zzinline_loopz00, BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00, BGl_varz00zzast_nodez00,
				BGl_proc1915z00zzinline_loopz00, BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1916z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00, BGl_syncz00zzast_nodez00,
				BGl_proc1917z00zzinline_loopz00, BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00, BGl_appz00zzast_nodez00,
				BGl_proc1918z00zzinline_loopz00, BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1919z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1920z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_externz00zzast_nodez00, BGl_proc1921z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00, BGl_castz00zzast_nodez00,
				BGl_proc1922z00zzinline_loopz00, BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00, BGl_setqz00zzast_nodez00,
				BGl_proc1923z00zzinline_loopz00, BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1924z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00, BGl_failz00zzast_nodez00,
				BGl_proc1925z00zzinline_loopz00, BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_switchz00zzast_nodez00, BGl_proc1926z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1927z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1928z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1929z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1930z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1931z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1932z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nestzd2loopz12zd2envz12zzinline_loopz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1933z00zzinline_loopz00,
				BGl_string1913z00zzinline_loopz00);
		}

	}



/* &nest-loop!-box-set!1360 */
	BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2boxzd2setz121360zb0zzinline_loopz00(obj_t
		BgL_envz00_2382, obj_t BgL_nodez00_2383, obj_t BgL_vz00_2384,
		obj_t BgL_nesterz00_2385)
	{
		{	/* Inline/loop.scm 392 */
			{
				BgL_varz00_bglt BgL_auxz00_2848;

				{	/* Inline/loop.scm 394 */
					BgL_varz00_bglt BgL_arg1753z00_2518;

					BgL_arg1753z00_2518 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2383)))->BgL_varz00);
					BgL_auxz00_2848 =
						((BgL_varz00_bglt)
						BGl_nestzd2loopz12zc0zzinline_loopz00(
							((BgL_nodez00_bglt) BgL_arg1753z00_2518),
							((BgL_localz00_bglt) BgL_vz00_2384), BgL_nesterz00_2385));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2383)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_2848), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2857;

				{	/* Inline/loop.scm 395 */
					BgL_nodez00_bglt BgL_arg1754z00_2519;

					BgL_arg1754z00_2519 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2383)))->BgL_valuez00);
					BgL_auxz00_2857 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1754z00_2519,
						((BgL_localz00_bglt) BgL_vz00_2384), BgL_nesterz00_2385);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2383)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2857), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2383));
		}

	}



/* &nest-loop!-box-ref1358 */
	BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2boxzd2ref1358za2zzinline_loopz00(obj_t
		BgL_envz00_2386, obj_t BgL_nodez00_2387, obj_t BgL_vz00_2388,
		obj_t BgL_nesterz00_2389)
	{
		{	/* Inline/loop.scm 384 */
			{
				BgL_varz00_bglt BgL_auxz00_2866;

				{	/* Inline/loop.scm 386 */
					BgL_varz00_bglt BgL_arg1752z00_2521;

					BgL_arg1752z00_2521 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2387)))->BgL_varz00);
					BgL_auxz00_2866 =
						((BgL_varz00_bglt)
						BGl_nestzd2loopz12zc0zzinline_loopz00(
							((BgL_nodez00_bglt) BgL_arg1752z00_2521),
							((BgL_localz00_bglt) BgL_vz00_2388), BgL_nesterz00_2389));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2387)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_2866), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2387));
		}

	}



/* &nest-loop!-make-box1356 */
	BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2makezd2box1356za2zzinline_loopz00(obj_t
		BgL_envz00_2390, obj_t BgL_nodez00_2391, obj_t BgL_varz00_2392,
		obj_t BgL_nesterz00_2393)
	{
		{	/* Inline/loop.scm 376 */
			{
				BgL_nodez00_bglt BgL_auxz00_2877;

				{	/* Inline/loop.scm 378 */
					BgL_nodez00_bglt BgL_arg1751z00_2523;

					BgL_arg1751z00_2523 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2391)))->BgL_valuez00);
					BgL_auxz00_2877 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1751z00_2523,
						((BgL_localz00_bglt) BgL_varz00_2392), BgL_nesterz00_2393);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2391)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_2877), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2391));
		}

	}



/* &nest-loop!-jump-ex-i1354 */
	BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2jumpzd2exzd2i1354z70zzinline_loopz00(obj_t
		BgL_envz00_2394, obj_t BgL_nodez00_2395, obj_t BgL_vz00_2396,
		obj_t BgL_nesterz00_2397)
	{
		{	/* Inline/loop.scm 367 */
			{
				BgL_nodez00_bglt BgL_auxz00_2886;

				{	/* Inline/loop.scm 369 */
					BgL_nodez00_bglt BgL_arg1749z00_2525;

					BgL_arg1749z00_2525 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2395)))->BgL_exitz00);
					BgL_auxz00_2886 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1749z00_2525,
						((BgL_localz00_bglt) BgL_vz00_2396), BgL_nesterz00_2397);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2395)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_2886), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2893;

				{	/* Inline/loop.scm 370 */
					BgL_nodez00_bglt BgL_arg1750z00_2526;

					BgL_arg1750z00_2526 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2395)))->
						BgL_valuez00);
					BgL_auxz00_2893 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1750z00_2526,
						((BgL_localz00_bglt) BgL_vz00_2396), BgL_nesterz00_2397);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2395)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_2893), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2395));
		}

	}



/* &nest-loop!-set-ex-it1352 */
	BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2setzd2exzd2it1352z70zzinline_loopz00(obj_t
		BgL_envz00_2398, obj_t BgL_nodez00_2399, obj_t BgL_varz00_2400,
		obj_t BgL_nesterz00_2401)
	{
		{	/* Inline/loop.scm 358 */
			{
				BgL_nodez00_bglt BgL_auxz00_2902;

				{	/* Inline/loop.scm 360 */
					BgL_nodez00_bglt BgL_arg1747z00_2528;

					BgL_arg1747z00_2528 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2399)))->BgL_bodyz00);
					BgL_auxz00_2902 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1747z00_2528,
						((BgL_localz00_bglt) BgL_varz00_2400), BgL_nesterz00_2401);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2399)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2902), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_2909;

				{	/* Inline/loop.scm 361 */
					BgL_nodez00_bglt BgL_arg1748z00_2529;

					BgL_arg1748z00_2529 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2399)))->
						BgL_onexitz00);
					BgL_auxz00_2909 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1748z00_2529,
						((BgL_localz00_bglt) BgL_varz00_2400), BgL_nesterz00_2401);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2399)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_2909), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2399));
		}

	}



/* &nest-loop!-let-var1350 */
	BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2letzd2var1350za2zzinline_loopz00(obj_t
		BgL_envz00_2402, obj_t BgL_nodez00_2403, obj_t BgL_varz00_2404,
		obj_t BgL_nesterz00_2405)
	{
		{	/* Inline/loop.scm 347 */
			{
				BgL_nodez00_bglt BgL_auxz00_2918;

				{	/* Inline/loop.scm 349 */
					BgL_nodez00_bglt BgL_arg1736z00_2531;

					BgL_arg1736z00_2531 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2403)))->BgL_bodyz00);
					BgL_auxz00_2918 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1736z00_2531,
						((BgL_localz00_bglt) BgL_varz00_2404), BgL_nesterz00_2405);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2403)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2918), BUNSPEC);
			}
			{	/* Inline/loop.scm 350 */
				obj_t BgL_g1277z00_2532;

				BgL_g1277z00_2532 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2403)))->BgL_bindingsz00);
				{
					obj_t BgL_l1274z00_2534;

					BgL_l1274z00_2534 = BgL_g1277z00_2532;
				BgL_zc3z04anonymousza31737ze3z87_2533:
					if (PAIRP(BgL_l1274z00_2534))
						{	/* Inline/loop.scm 350 */
							{	/* Inline/loop.scm 351 */
								obj_t BgL_bindingz00_2535;

								BgL_bindingz00_2535 = CAR(BgL_l1274z00_2534);
								{	/* Inline/loop.scm 351 */
									BgL_nodez00_bglt BgL_arg1739z00_2536;

									{	/* Inline/loop.scm 351 */
										obj_t BgL_arg1740z00_2537;

										BgL_arg1740z00_2537 = CDR(((obj_t) BgL_bindingz00_2535));
										BgL_arg1739z00_2536 =
											BGl_nestzd2loopz12zc0zzinline_loopz00(
											((BgL_nodez00_bglt) BgL_arg1740z00_2537),
											((BgL_localz00_bglt) BgL_varz00_2404),
											BgL_nesterz00_2405);
									}
									{	/* Inline/loop.scm 351 */
										obj_t BgL_auxz00_2937;
										obj_t BgL_tmpz00_2935;

										BgL_auxz00_2937 = ((obj_t) BgL_arg1739z00_2536);
										BgL_tmpz00_2935 = ((obj_t) BgL_bindingz00_2535);
										SET_CDR(BgL_tmpz00_2935, BgL_auxz00_2937);
									}
								}
							}
							{
								obj_t BgL_l1274z00_2940;

								BgL_l1274z00_2940 = CDR(BgL_l1274z00_2534);
								BgL_l1274z00_2534 = BgL_l1274z00_2940;
								goto BgL_zc3z04anonymousza31737ze3z87_2533;
							}
						}
					else
						{	/* Inline/loop.scm 350 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2403));
		}

	}



/* &nest-loop!-let-fun1348 */
	BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2letzd2fun1348za2zzinline_loopz00(obj_t
		BgL_envz00_2406, obj_t BgL_nodez00_2407, obj_t BgL_varz00_2408,
		obj_t BgL_nesterz00_2409)
	{
		{	/* Inline/loop.scm 332 */
			{
				BgL_nodez00_bglt BgL_auxz00_2944;

				{	/* Inline/loop.scm 334 */
					BgL_nodez00_bglt BgL_arg1722z00_2539;

					BgL_arg1722z00_2539 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2407)))->BgL_bodyz00);
					BgL_auxz00_2944 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1722z00_2539,
						((BgL_localz00_bglt) BgL_varz00_2408), BgL_nesterz00_2409);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2407)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2944), BUNSPEC);
			}
			{	/* Inline/loop.scm 335 */
				obj_t BgL_g1273z00_2540;

				BgL_g1273z00_2540 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2407)))->BgL_localsz00);
				{
					obj_t BgL_l1271z00_2542;

					BgL_l1271z00_2542 = BgL_g1273z00_2540;
				BgL_zc3z04anonymousza31723ze3z87_2541:
					if (PAIRP(BgL_l1271z00_2542))
						{	/* Inline/loop.scm 335 */
							{	/* Inline/loop.scm 336 */
								obj_t BgL_localz00_2543;

								BgL_localz00_2543 = CAR(BgL_l1271z00_2542);
								{	/* Inline/loop.scm 336 */
									BgL_valuez00_bglt BgL_sfunz00_2544;

									BgL_sfunz00_2544 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2543))))->
										BgL_valuez00);
									{	/* Inline/loop.scm 338 */
										BgL_nodez00_bglt BgL_arg1733z00_2545;

										{	/* Inline/loop.scm 338 */
											obj_t BgL_arg1734z00_2546;

											BgL_arg1734z00_2546 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_sfunz00_2544)))->
												BgL_bodyz00);
											BgL_arg1733z00_2545 =
												BGl_nestzd2loopz12zc0zzinline_loopz00((
													(BgL_nodez00_bglt) BgL_arg1734z00_2546),
												((BgL_localz00_bglt) BgL_varz00_2408),
												BgL_nesterz00_2409);
										}
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_sfunz00_2544)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_arg1733z00_2545)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1271z00_2967;

								BgL_l1271z00_2967 = CDR(BgL_l1271z00_2542);
								BgL_l1271z00_2542 = BgL_l1271z00_2967;
								goto BgL_zc3z04anonymousza31723ze3z87_2541;
							}
						}
					else
						{	/* Inline/loop.scm 335 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2407));
		}

	}



/* &nest-loop!-switch1346 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2switch1346z70zzinline_loopz00(obj_t
		BgL_envz00_2410, obj_t BgL_nodez00_2411, obj_t BgL_varz00_2412,
		obj_t BgL_nesterz00_2413)
	{
		{	/* Inline/loop.scm 321 */
			{
				BgL_nodez00_bglt BgL_auxz00_2971;

				{	/* Inline/loop.scm 323 */
					BgL_nodez00_bglt BgL_arg1711z00_2548;

					BgL_arg1711z00_2548 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2411)))->BgL_testz00);
					BgL_auxz00_2971 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1711z00_2548,
						((BgL_localz00_bglt) BgL_varz00_2412), BgL_nesterz00_2413);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2411)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2971), BUNSPEC);
			}
			{	/* Inline/loop.scm 324 */
				obj_t BgL_g1270z00_2549;

				BgL_g1270z00_2549 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2411)))->BgL_clausesz00);
				{
					obj_t BgL_l1268z00_2551;

					BgL_l1268z00_2551 = BgL_g1270z00_2549;
				BgL_zc3z04anonymousza31712ze3z87_2550:
					if (PAIRP(BgL_l1268z00_2551))
						{	/* Inline/loop.scm 324 */
							{	/* Inline/loop.scm 325 */
								obj_t BgL_clausez00_2552;

								BgL_clausez00_2552 = CAR(BgL_l1268z00_2551);
								{	/* Inline/loop.scm 325 */
									BgL_nodez00_bglt BgL_arg1717z00_2553;

									{	/* Inline/loop.scm 325 */
										obj_t BgL_arg1718z00_2554;

										BgL_arg1718z00_2554 = CDR(((obj_t) BgL_clausez00_2552));
										BgL_arg1717z00_2553 =
											BGl_nestzd2loopz12zc0zzinline_loopz00(
											((BgL_nodez00_bglt) BgL_arg1718z00_2554),
											((BgL_localz00_bglt) BgL_varz00_2412),
											BgL_nesterz00_2413);
									}
									{	/* Inline/loop.scm 325 */
										obj_t BgL_auxz00_2990;
										obj_t BgL_tmpz00_2988;

										BgL_auxz00_2990 = ((obj_t) BgL_arg1717z00_2553);
										BgL_tmpz00_2988 = ((obj_t) BgL_clausez00_2552);
										SET_CDR(BgL_tmpz00_2988, BgL_auxz00_2990);
									}
								}
							}
							{
								obj_t BgL_l1268z00_2993;

								BgL_l1268z00_2993 = CDR(BgL_l1268z00_2551);
								BgL_l1268z00_2551 = BgL_l1268z00_2993;
								goto BgL_zc3z04anonymousza31712ze3z87_2550;
							}
						}
					else
						{	/* Inline/loop.scm 324 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2411));
		}

	}



/* &nest-loop!-fail1344 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2fail1344z70zzinline_loopz00(obj_t
		BgL_envz00_2414, obj_t BgL_nodez00_2415, obj_t BgL_varz00_2416,
		obj_t BgL_nesterz00_2417)
	{
		{	/* Inline/loop.scm 311 */
			{
				BgL_nodez00_bglt BgL_auxz00_2997;

				{	/* Inline/loop.scm 313 */
					BgL_nodez00_bglt BgL_arg1708z00_2556;

					BgL_arg1708z00_2556 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2415)))->BgL_procz00);
					BgL_auxz00_2997 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1708z00_2556,
						((BgL_localz00_bglt) BgL_varz00_2416), BgL_nesterz00_2417);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2415)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_2997), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3004;

				{	/* Inline/loop.scm 314 */
					BgL_nodez00_bglt BgL_arg1709z00_2557;

					BgL_arg1709z00_2557 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2415)))->BgL_msgz00);
					BgL_auxz00_3004 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1709z00_2557,
						((BgL_localz00_bglt) BgL_varz00_2416), BgL_nesterz00_2417);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2415)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3004), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3011;

				{	/* Inline/loop.scm 315 */
					BgL_nodez00_bglt BgL_arg1710z00_2558;

					BgL_arg1710z00_2558 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2415)))->BgL_objz00);
					BgL_auxz00_3011 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1710z00_2558,
						((BgL_localz00_bglt) BgL_varz00_2416), BgL_nesterz00_2417);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2415)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3011), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2415));
		}

	}



/* &nest-loop!-condition1342 */
	BgL_nodez00_bglt
		BGl_z62nestzd2loopz12zd2condition1342z70zzinline_loopz00(obj_t
		BgL_envz00_2418, obj_t BgL_nodez00_2419, obj_t BgL_varz00_2420,
		obj_t BgL_nesterz00_2421)
	{
		{	/* Inline/loop.scm 301 */
			{
				BgL_nodez00_bglt BgL_auxz00_3020;

				{	/* Inline/loop.scm 303 */
					BgL_nodez00_bglt BgL_arg1702z00_2560;

					BgL_arg1702z00_2560 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2419)))->BgL_testz00);
					BgL_auxz00_3020 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1702z00_2560,
						((BgL_localz00_bglt) BgL_varz00_2420), BgL_nesterz00_2421);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2419)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3020), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3027;

				{	/* Inline/loop.scm 304 */
					BgL_nodez00_bglt BgL_arg1703z00_2561;

					BgL_arg1703z00_2561 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2419)))->BgL_truez00);
					BgL_auxz00_3027 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1703z00_2561,
						((BgL_localz00_bglt) BgL_varz00_2420), BgL_nesterz00_2421);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2419)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3027), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3034;

				{	/* Inline/loop.scm 305 */
					BgL_nodez00_bglt BgL_arg1705z00_2562;

					BgL_arg1705z00_2562 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2419)))->BgL_falsez00);
					BgL_auxz00_3034 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1705z00_2562,
						((BgL_localz00_bglt) BgL_varz00_2420), BgL_nesterz00_2421);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2419)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3034), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2419));
		}

	}



/* &nest-loop!-setq1340 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2setq1340z70zzinline_loopz00(obj_t
		BgL_envz00_2422, obj_t BgL_nodez00_2423, obj_t BgL_varz00_2424,
		obj_t BgL_nesterz00_2425)
	{
		{	/* Inline/loop.scm 294 */
			{	/* Inline/loop.scm 295 */
				BgL_nodez00_bglt BgL_arg1701z00_2564;

				BgL_arg1701z00_2564 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2423)))->BgL_valuez00);
				BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1701z00_2564,
					((BgL_localz00_bglt) BgL_varz00_2424), BgL_nesterz00_2425);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2423));
		}

	}



/* &nest-loop!-cast1337 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2cast1337z70zzinline_loopz00(obj_t
		BgL_envz00_2426, obj_t BgL_nodez00_2427, obj_t BgL_varz00_2428,
		obj_t BgL_nesterz00_2429)
	{
		{	/* Inline/loop.scm 287 */
			{	/* Inline/loop.scm 288 */
				BgL_nodez00_bglt BgL_arg1700z00_2566;

				BgL_arg1700z00_2566 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2427)))->BgL_argz00);
				BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1700z00_2566,
					((BgL_localz00_bglt) BgL_varz00_2428), BgL_nesterz00_2429);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2427));
		}

	}



/* &nest-loop!-extern1335 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2extern1335z70zzinline_loopz00(obj_t
		BgL_envz00_2430, obj_t BgL_nodez00_2431, obj_t BgL_varz00_2432,
		obj_t BgL_nesterz00_2433)
	{
		{	/* Inline/loop.scm 280 */
			BGl_nestzd2loopz12za2z62zzinline_loopz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2431)))->BgL_exprza2za2),
				BgL_varz00_2432, BgL_nesterz00_2433);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2431));
		}

	}



/* &nest-loop!-funcall1333 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2funcall1333z70zzinline_loopz00(obj_t
		BgL_envz00_2434, obj_t BgL_nodez00_2435, obj_t BgL_varz00_2436,
		obj_t BgL_nesterz00_2437)
	{
		{	/* Inline/loop.scm 271 */
			{
				BgL_nodez00_bglt BgL_auxz00_3060;

				{	/* Inline/loop.scm 273 */
					BgL_nodez00_bglt BgL_arg1691z00_2569;

					BgL_arg1691z00_2569 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2435)))->BgL_funz00);
					BgL_auxz00_3060 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1691z00_2569,
						((BgL_localz00_bglt) BgL_varz00_2436), BgL_nesterz00_2437);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2435)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3060), BUNSPEC);
			}
			BGl_nestzd2loopz12za2z62zzinline_loopz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2435)))->BgL_argsz00),
				BgL_varz00_2436, BgL_nesterz00_2437);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2435));
		}

	}



/* &nest-loop!-app-ly1331 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2appzd2ly1331za2zzinline_loopz00(obj_t
		BgL_envz00_2438, obj_t BgL_nodez00_2439, obj_t BgL_varz00_2440,
		obj_t BgL_nesterz00_2441)
	{
		{	/* Inline/loop.scm 262 */
			{
				BgL_nodez00_bglt BgL_auxz00_3072;

				{	/* Inline/loop.scm 264 */
					BgL_nodez00_bglt BgL_arg1688z00_2571;

					BgL_arg1688z00_2571 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2439)))->BgL_funz00);
					BgL_auxz00_3072 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1688z00_2571,
						((BgL_localz00_bglt) BgL_varz00_2440), BgL_nesterz00_2441);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2439)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3072), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3079;

				{	/* Inline/loop.scm 265 */
					BgL_nodez00_bglt BgL_arg1689z00_2572;

					BgL_arg1689z00_2572 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2439)))->BgL_argz00);
					BgL_auxz00_3079 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1689z00_2572,
						((BgL_localz00_bglt) BgL_varz00_2440), BgL_nesterz00_2441);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2439)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3079), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2439));
		}

	}



/* &nest-loop!-app1329 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2app1329z70zzinline_loopz00(obj_t
		BgL_envz00_2442, obj_t BgL_nodez00_2443, obj_t BgL_varz00_2444,
		obj_t BgL_nesterz00_2445)
	{
		{	/* Inline/loop.scm 252 */
			BGl_nestzd2loopz12za2z62zzinline_loopz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2443)))->BgL_argsz00),
				BgL_varz00_2444, BgL_nesterz00_2445);
			{	/* Inline/loop.scm 255 */
				bool_t BgL_test1998z00_3091;

				{	/* Inline/loop.scm 255 */
					bool_t BgL_test1999z00_3092;

					{	/* Inline/loop.scm 255 */
						BgL_varz00_bglt BgL_arg1681z00_2574;

						BgL_arg1681z00_2574 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_2443)))->BgL_funz00);
						{	/* Inline/loop.scm 255 */
							obj_t BgL_classz00_2575;

							BgL_classz00_2575 = BGl_varz00zzast_nodez00;
							{	/* Inline/loop.scm 255 */
								BgL_objectz00_bglt BgL_arg1807z00_2576;

								{	/* Inline/loop.scm 255 */
									obj_t BgL_tmpz00_3095;

									BgL_tmpz00_3095 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1681z00_2574));
									BgL_arg1807z00_2576 = (BgL_objectz00_bglt) (BgL_tmpz00_3095);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Inline/loop.scm 255 */
										long BgL_idxz00_2577;

										BgL_idxz00_2577 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2576);
										BgL_test1999z00_3092 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2577 + 2L)) == BgL_classz00_2575);
									}
								else
									{	/* Inline/loop.scm 255 */
										bool_t BgL_res1886z00_2580;

										{	/* Inline/loop.scm 255 */
											obj_t BgL_oclassz00_2581;

											{	/* Inline/loop.scm 255 */
												obj_t BgL_arg1815z00_2582;
												long BgL_arg1816z00_2583;

												BgL_arg1815z00_2582 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Inline/loop.scm 255 */
													long BgL_arg1817z00_2584;

													BgL_arg1817z00_2584 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2576);
													BgL_arg1816z00_2583 =
														(BgL_arg1817z00_2584 - OBJECT_TYPE);
												}
												BgL_oclassz00_2581 =
													VECTOR_REF(BgL_arg1815z00_2582, BgL_arg1816z00_2583);
											}
											{	/* Inline/loop.scm 255 */
												bool_t BgL__ortest_1115z00_2585;

												BgL__ortest_1115z00_2585 =
													(BgL_classz00_2575 == BgL_oclassz00_2581);
												if (BgL__ortest_1115z00_2585)
													{	/* Inline/loop.scm 255 */
														BgL_res1886z00_2580 = BgL__ortest_1115z00_2585;
													}
												else
													{	/* Inline/loop.scm 255 */
														long BgL_odepthz00_2586;

														{	/* Inline/loop.scm 255 */
															obj_t BgL_arg1804z00_2587;

															BgL_arg1804z00_2587 = (BgL_oclassz00_2581);
															BgL_odepthz00_2586 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2587);
														}
														if ((2L < BgL_odepthz00_2586))
															{	/* Inline/loop.scm 255 */
																obj_t BgL_arg1802z00_2588;

																{	/* Inline/loop.scm 255 */
																	obj_t BgL_arg1803z00_2589;

																	BgL_arg1803z00_2589 = (BgL_oclassz00_2581);
																	BgL_arg1802z00_2588 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2589,
																		2L);
																}
																BgL_res1886z00_2580 =
																	(BgL_arg1802z00_2588 == BgL_classz00_2575);
															}
														else
															{	/* Inline/loop.scm 255 */
																BgL_res1886z00_2580 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1999z00_3092 = BgL_res1886z00_2580;
									}
							}
						}
					}
					if (BgL_test1999z00_3092)
						{	/* Inline/loop.scm 255 */
							BgL_test1998z00_3091 =
								(
								((obj_t)
									(((BgL_varz00_bglt) COBJECT(
												(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2443)))->
													BgL_funz00)))->BgL_variablez00)) == BgL_varz00_2444);
						}
					else
						{	/* Inline/loop.scm 255 */
							BgL_test1998z00_3091 = ((bool_t) 0);
						}
				}
				if (BgL_test1998z00_3091)
					{	/* Inline/loop.scm 255 */
						return
							((BgL_nodez00_bglt)
							BGL_PROCEDURE_CALL1(BgL_nesterz00_2445,
								((obj_t) ((BgL_appz00_bglt) BgL_nodez00_2443))));
					}
				else
					{	/* Inline/loop.scm 255 */
						return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2443));
					}
			}
		}

	}



/* &nest-loop!-sync1327 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2sync1327z70zzinline_loopz00(obj_t
		BgL_envz00_2446, obj_t BgL_nodez00_2447, obj_t BgL_varz00_2448,
		obj_t BgL_nesterz00_2449)
	{
		{	/* Inline/loop.scm 242 */
			{
				BgL_nodez00_bglt BgL_auxz00_3132;

				{	/* Inline/loop.scm 244 */
					BgL_nodez00_bglt BgL_arg1630z00_2591;

					BgL_arg1630z00_2591 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2447)))->BgL_mutexz00);
					BgL_auxz00_3132 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1630z00_2591,
						((BgL_localz00_bglt) BgL_varz00_2448), BgL_nesterz00_2449);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2447)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3132), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3139;

				{	/* Inline/loop.scm 245 */
					BgL_nodez00_bglt BgL_arg1642z00_2592;

					BgL_arg1642z00_2592 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2447)))->BgL_prelockz00);
					BgL_auxz00_3139 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1642z00_2592,
						((BgL_localz00_bglt) BgL_varz00_2448), BgL_nesterz00_2449);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2447)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3139), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3146;

				{	/* Inline/loop.scm 246 */
					BgL_nodez00_bglt BgL_arg1646z00_2593;

					BgL_arg1646z00_2593 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2447)))->BgL_bodyz00);
					BgL_auxz00_3146 =
						BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_arg1646z00_2593,
						((BgL_localz00_bglt) BgL_varz00_2448), BgL_nesterz00_2449);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2447)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3146), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2447));
		}

	}



/* &nest-loop!-sequence1325 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2sequence1325z70zzinline_loopz00(obj_t
		BgL_envz00_2450, obj_t BgL_nodez00_2451, obj_t BgL_varz00_2452,
		obj_t BgL_nesterz00_2453)
	{
		{	/* Inline/loop.scm 234 */
			BGl_nestzd2loopz12za2z62zzinline_loopz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2451)))->BgL_nodesz00),
				BgL_varz00_2452, BgL_nesterz00_2453);
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2451));
		}

	}



/* &nest-loop!-var1323 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2var1323z70zzinline_loopz00(obj_t
		BgL_envz00_2454, obj_t BgL_nodez00_2455, obj_t BgL_varz00_2456,
		obj_t BgL_nesterz00_2457)
	{
		{	/* Inline/loop.scm 228 */
			return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_2455));
		}

	}



/* &nest-loop!-kwote1321 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2kwote1321z70zzinline_loopz00(obj_t
		BgL_envz00_2458, obj_t BgL_nodez00_2459, obj_t BgL_varz00_2460,
		obj_t BgL_nesterz00_2461)
	{
		{	/* Inline/loop.scm 222 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_2459));
		}

	}



/* &nest-loop!-atom1319 */
	BgL_nodez00_bglt BGl_z62nestzd2loopz12zd2atom1319z70zzinline_loopz00(obj_t
		BgL_envz00_2462, obj_t BgL_nodez00_2463, obj_t BgL_varz00_2464,
		obj_t BgL_nesterz00_2465)
	{
		{	/* Inline/loop.scm 216 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_2463));
		}

	}



/* &find-let-fun?-box-se1315 */
	obj_t BGl_z62findzd2letzd2funzf3zd2boxzd2se1315z91zzinline_loopz00(obj_t
		BgL_envz00_2466, obj_t BgL_nodez00_2467)
	{
		{	/* Inline/loop.scm 191 */
			{	/* Inline/loop.scm 193 */
				obj_t BgL__ortest_1129z00_2599;

				{	/* Inline/loop.scm 193 */
					BgL_varz00_bglt BgL_arg1627z00_2600;

					BgL_arg1627z00_2600 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2467)))->BgL_varz00);
					BgL__ortest_1129z00_2599 =
						BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
						((BgL_nodez00_bglt) BgL_arg1627z00_2600));
				}
				if (CBOOL(BgL__ortest_1129z00_2599))
					{	/* Inline/loop.scm 193 */
						return BgL__ortest_1129z00_2599;
					}
				else
					{	/* Inline/loop.scm 193 */
						return
							BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2467)))->
								BgL_valuez00));
					}
			}
		}

	}



/* &find-let-fun?-box-re1313 */
	obj_t BGl_z62findzd2letzd2funzf3zd2boxzd2re1313z91zzinline_loopz00(obj_t
		BgL_envz00_2468, obj_t BgL_nodez00_2469)
	{
		{	/* Inline/loop.scm 185 */
			{	/* Inline/loop.scm 186 */
				BgL_varz00_bglt BgL_arg1625z00_2602;

				BgL_arg1625z00_2602 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2469)))->BgL_varz00);
				return
					BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
					((BgL_nodez00_bglt) BgL_arg1625z00_2602));
			}
		}

	}



/* &find-let-fun?-make-b1311 */
	obj_t BGl_z62findzd2letzd2funzf3zd2makezd2b1311z91zzinline_loopz00(obj_t
		BgL_envz00_2470, obj_t BgL_nodez00_2471)
	{
		{	/* Inline/loop.scm 179 */
			return
				BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2471)))->BgL_valuez00));
		}

	}



/* &find-let-fun?-jump-e1309 */
	obj_t BGl_z62findzd2letzd2funzf3zd2jumpzd2e1309z91zzinline_loopz00(obj_t
		BgL_envz00_2472, obj_t BgL_nodez00_2473)
	{
		{	/* Inline/loop.scm 172 */
			{	/* Inline/loop.scm 174 */
				obj_t BgL__ortest_1127z00_2605;

				BgL__ortest_1127z00_2605 =
					BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2473)))->
						BgL_exitz00));
				if (CBOOL(BgL__ortest_1127z00_2605))
					{	/* Inline/loop.scm 174 */
						return BgL__ortest_1127z00_2605;
					}
				else
					{	/* Inline/loop.scm 174 */
						return
							BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
										((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2473)))->
								BgL_valuez00));
					}
			}
		}

	}



/* &find-let-fun?-set-ex1307 */
	obj_t BGl_z62findzd2letzd2funzf3zd2setzd2ex1307z91zzinline_loopz00(obj_t
		BgL_envz00_2474, obj_t BgL_nodez00_2475)
	{
		{	/* Inline/loop.scm 165 */
			{	/* Inline/loop.scm 166 */
				obj_t BgL__ortest_1125z00_2607;

				BgL__ortest_1125z00_2607 =
					BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2475)))->BgL_bodyz00));
				if (CBOOL(BgL__ortest_1125z00_2607))
					{	/* Inline/loop.scm 166 */
						return BgL__ortest_1125z00_2607;
					}
				else
					{	/* Inline/loop.scm 166 */
						return
							BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2475)))->
								BgL_onexitz00));
					}
			}
		}

	}



/* &find-let-fun?-let-va1305 */
	obj_t BGl_z62findzd2letzd2funzf3zd2letzd2va1305z91zzinline_loopz00(obj_t
		BgL_envz00_2476, obj_t BgL_nodez00_2477)
	{
		{	/* Inline/loop.scm 152 */
			{	/* Inline/loop.scm 153 */
				obj_t BgL_g1124z00_2609;

				BgL_g1124z00_2609 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2477)))->BgL_bindingsz00);
				{
					obj_t BgL_bindingsz00_2611;

					BgL_bindingsz00_2611 = BgL_g1124z00_2609;
				BgL_loopz00_2610:
					if (NULLP(BgL_bindingsz00_2611))
						{	/* Inline/loop.scm 155 */
							return
								BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
								(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2477)))->
									BgL_bodyz00));
						}
					else
						{	/* Inline/loop.scm 157 */
							bool_t BgL_test2007z00_3205;

							{	/* Inline/loop.scm 157 */
								obj_t BgL_arg1605z00_2612;

								{	/* Inline/loop.scm 157 */
									obj_t BgL_pairz00_2613;

									BgL_pairz00_2613 = CAR(((obj_t) BgL_bindingsz00_2611));
									BgL_arg1605z00_2612 = CDR(BgL_pairz00_2613);
								}
								BgL_test2007z00_3205 =
									CBOOL(BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
										((BgL_nodez00_bglt) BgL_arg1605z00_2612)));
							}
							if (BgL_test2007z00_3205)
								{	/* Inline/loop.scm 157 */
									return BTRUE;
								}
							else
								{	/* Inline/loop.scm 160 */
									obj_t BgL_arg1602z00_2614;

									BgL_arg1602z00_2614 = CDR(((obj_t) BgL_bindingsz00_2611));
									{
										obj_t BgL_bindingsz00_3214;

										BgL_bindingsz00_3214 = BgL_arg1602z00_2614;
										BgL_bindingsz00_2611 = BgL_bindingsz00_3214;
										goto BgL_loopz00_2610;
									}
								}
						}
				}
			}
		}

	}



/* &find-let-fun?-let-fu1303 */
	obj_t BGl_z62findzd2letzd2funzf3zd2letzd2fu1303z91zzinline_loopz00(obj_t
		BgL_envz00_2478, obj_t BgL_nodez00_2479)
	{
		{	/* Inline/loop.scm 146 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &find-let-fun?-switch1301 */
	obj_t BGl_z62findzd2letzd2funzf3zd2switch1301z43zzinline_loopz00(obj_t
		BgL_envz00_2480, obj_t BgL_nodez00_2481)
	{
		{	/* Inline/loop.scm 133 */
			{	/* Inline/loop.scm 134 */
				obj_t BgL_g1123z00_2617;

				BgL_g1123z00_2617 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2481)))->BgL_clausesz00);
				{
					obj_t BgL_clausesz00_2619;

					BgL_clausesz00_2619 = BgL_g1123z00_2617;
				BgL_loopz00_2618:
					if (NULLP(BgL_clausesz00_2619))
						{	/* Inline/loop.scm 136 */
							return
								BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
								(((BgL_switchz00_bglt) COBJECT(
											((BgL_switchz00_bglt) BgL_nodez00_2481)))->BgL_testz00));
						}
					else
						{	/* Inline/loop.scm 138 */
							bool_t BgL_test2009z00_3223;

							{	/* Inline/loop.scm 138 */
								obj_t BgL_arg1585z00_2620;

								{	/* Inline/loop.scm 138 */
									obj_t BgL_pairz00_2621;

									BgL_pairz00_2621 = CAR(((obj_t) BgL_clausesz00_2619));
									BgL_arg1585z00_2620 = CDR(BgL_pairz00_2621);
								}
								BgL_test2009z00_3223 =
									CBOOL(BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
										((BgL_nodez00_bglt) BgL_arg1585z00_2620)));
							}
							if (BgL_test2009z00_3223)
								{	/* Inline/loop.scm 138 */
									return BTRUE;
								}
							else
								{	/* Inline/loop.scm 141 */
									obj_t BgL_arg1584z00_2622;

									BgL_arg1584z00_2622 = CDR(((obj_t) BgL_clausesz00_2619));
									{
										obj_t BgL_clausesz00_3232;

										BgL_clausesz00_3232 = BgL_arg1584z00_2622;
										BgL_clausesz00_2619 = BgL_clausesz00_3232;
										goto BgL_loopz00_2618;
									}
								}
						}
				}
			}
		}

	}



/* &find-let-fun?-fail1299 */
	obj_t BGl_z62findzd2letzd2funzf3zd2fail1299z43zzinline_loopz00(obj_t
		BgL_envz00_2482, obj_t BgL_nodez00_2483)
	{
		{	/* Inline/loop.scm 124 */
			{	/* Inline/loop.scm 126 */
				obj_t BgL__ortest_1121z00_2624;

				BgL__ortest_1121z00_2624 =
					BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2483)))->BgL_procz00));
				if (CBOOL(BgL__ortest_1121z00_2624))
					{	/* Inline/loop.scm 126 */
						return BgL__ortest_1121z00_2624;
					}
				else
					{	/* Inline/loop.scm 127 */
						obj_t BgL__ortest_1122z00_2625;

						BgL__ortest_1122z00_2625 =
							BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
							(((BgL_failz00_bglt) COBJECT(
										((BgL_failz00_bglt) BgL_nodez00_2483)))->BgL_msgz00));
						if (CBOOL(BgL__ortest_1122z00_2625))
							{	/* Inline/loop.scm 127 */
								return BgL__ortest_1122z00_2625;
							}
						else
							{	/* Inline/loop.scm 127 */
								return
									BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
									(((BgL_failz00_bglt) COBJECT(
												((BgL_failz00_bglt) BgL_nodez00_2483)))->BgL_objz00));
							}
					}
			}
		}

	}



/* &find-let-fun?-condit1297 */
	obj_t BGl_z62findzd2letzd2funzf3zd2condit1297z43zzinline_loopz00(obj_t
		BgL_envz00_2484, obj_t BgL_nodez00_2485)
	{
		{	/* Inline/loop.scm 115 */
			{	/* Inline/loop.scm 117 */
				obj_t BgL__ortest_1118z00_2627;

				BgL__ortest_1118z00_2627 =
					BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2485)))->BgL_testz00));
				if (CBOOL(BgL__ortest_1118z00_2627))
					{	/* Inline/loop.scm 117 */
						return BgL__ortest_1118z00_2627;
					}
				else
					{	/* Inline/loop.scm 118 */
						obj_t BgL__ortest_1119z00_2628;

						BgL__ortest_1119z00_2628 =
							BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_2485)))->
								BgL_truez00));
						if (CBOOL(BgL__ortest_1119z00_2628))
							{	/* Inline/loop.scm 118 */
								return BgL__ortest_1119z00_2628;
							}
						else
							{	/* Inline/loop.scm 118 */
								return
									BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
									(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_2485)))->
										BgL_falsez00));
							}
					}
			}
		}

	}



/* &find-let-fun?-setq1295 */
	obj_t BGl_z62findzd2letzd2funzf3zd2setq1295z43zzinline_loopz00(obj_t
		BgL_envz00_2486, obj_t BgL_nodez00_2487)
	{
		{	/* Inline/loop.scm 109 */
			return
				BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2487)))->BgL_valuez00));
		}

	}



/* &find-let-fun?-cast1293 */
	obj_t BGl_z62findzd2letzd2funzf3zd2cast1293z43zzinline_loopz00(obj_t
		BgL_envz00_2488, obj_t BgL_nodez00_2489)
	{
		{	/* Inline/loop.scm 103 */
			return
				BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2489)))->BgL_argz00));
		}

	}



/* &find-let-fun?-extern1291 */
	obj_t BGl_z62findzd2letzd2funzf3zd2extern1291z43zzinline_loopz00(obj_t
		BgL_envz00_2490, obj_t BgL_nodez00_2491)
	{
		{	/* Inline/loop.scm 97 */
			return
				BBOOL(BGl_findzd2letzd2funzf3za2z51zzinline_loopz00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2491)))->BgL_exprza2za2)));
		}

	}



/* &find-let-fun?-funcal1289 */
	obj_t BGl_z62findzd2letzd2funzf3zd2funcal1289z43zzinline_loopz00(obj_t
		BgL_envz00_2492, obj_t BgL_nodez00_2493)
	{
		{	/* Inline/loop.scm 89 */
			{	/* Inline/loop.scm 91 */
				obj_t BgL__ortest_1116z00_2633;

				BgL__ortest_1116z00_2633 =
					BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2493)))->BgL_funz00));
				if (CBOOL(BgL__ortest_1116z00_2633))
					{	/* Inline/loop.scm 91 */
						return BgL__ortest_1116z00_2633;
					}
				else
					{	/* Inline/loop.scm 92 */
						obj_t BgL_arg1514z00_2634;

						BgL_arg1514z00_2634 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_2493)))->BgL_argsz00);
						return
							BBOOL(BGl_findzd2letzd2funzf3za2z51zzinline_loopz00
							(BgL_arg1514z00_2634));
					}
			}
		}

	}



/* &find-let-fun?-app-ly1287 */
	obj_t BGl_z62findzd2letzd2funzf3zd2appzd2ly1287z91zzinline_loopz00(obj_t
		BgL_envz00_2494, obj_t BgL_nodez00_2495)
	{
		{	/* Inline/loop.scm 82 */
			{	/* Inline/loop.scm 84 */
				obj_t BgL__ortest_1114z00_2636;

				BgL__ortest_1114z00_2636 =
					BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2495)))->BgL_funz00));
				if (CBOOL(BgL__ortest_1114z00_2636))
					{	/* Inline/loop.scm 84 */
						return BgL__ortest_1114z00_2636;
					}
				else
					{	/* Inline/loop.scm 84 */
						return
							BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
							(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_2495)))->BgL_argz00));
					}
			}
		}

	}



/* &find-let-fun?-app1285 */
	obj_t BGl_z62findzd2letzd2funzf3zd2app1285z43zzinline_loopz00(obj_t
		BgL_envz00_2496, obj_t BgL_nodez00_2497)
	{
		{	/* Inline/loop.scm 75 */
			return
				BBOOL(BGl_findzd2letzd2funzf3za2z51zzinline_loopz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2497)))->BgL_argsz00)));
		}

	}



/* &find-let-fun?-sync1283 */
	obj_t BGl_z62findzd2letzd2funzf3zd2sync1283z43zzinline_loopz00(obj_t
		BgL_envz00_2498, obj_t BgL_nodez00_2499)
	{
		{	/* Inline/loop.scm 67 */
			{	/* Inline/loop.scm 68 */
				obj_t BgL__ortest_1109z00_2639;

				BgL__ortest_1109z00_2639 =
					BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2499)))->BgL_mutexz00));
				if (CBOOL(BgL__ortest_1109z00_2639))
					{	/* Inline/loop.scm 68 */
						return BgL__ortest_1109z00_2639;
					}
				else
					{	/* Inline/loop.scm 69 */
						obj_t BgL__ortest_1111z00_2640;

						BgL__ortest_1111z00_2640 =
							BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
							(((BgL_syncz00_bglt) COBJECT(
										((BgL_syncz00_bglt) BgL_nodez00_2499)))->BgL_prelockz00));
						if (CBOOL(BgL__ortest_1111z00_2640))
							{	/* Inline/loop.scm 69 */
								return BgL__ortest_1111z00_2640;
							}
						else
							{	/* Inline/loop.scm 69 */
								return
									BGl_findzd2letzd2funzf3zf3zzinline_loopz00(
									(((BgL_syncz00_bglt) COBJECT(
												((BgL_syncz00_bglt) BgL_nodez00_2499)))->BgL_bodyz00));
							}
					}
			}
		}

	}



/* &find-let-fun?-sequen1281 */
	obj_t BGl_z62findzd2letzd2funzf3zd2sequen1281z43zzinline_loopz00(obj_t
		BgL_envz00_2500, obj_t BgL_nodez00_2501)
	{
		{	/* Inline/loop.scm 61 */
			return
				BBOOL(BGl_findzd2letzd2funzf3za2z51zzinline_loopz00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2501)))->BgL_nodesz00)));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinline_loopz00(void)
	{
		{	/* Inline/loop.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zzinline_inlinez00(20504962L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zzinline_variantz00(347990278L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			BGl_modulezd2initializa7ationz75zzinline_recursionz00(101873001L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
			return
				BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string1934z00zzinline_loopz00));
		}

	}

#ifdef __cplusplus
}
#endif
