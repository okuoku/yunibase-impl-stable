/*===========================================================================*/
/*   (Inline/recursion.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Inline/recursion.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INLINE_RECURSION_TYPE_DEFINITIONS
#define BGL_INLINE_RECURSION_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_isfunz00_bgl
	{
		struct BgL_nodez00_bgl *BgL_originalzd2bodyzd2;
		obj_t BgL_recursivezd2callszd2;
		bool_t BgL_tailrecz00;
	}               *BgL_isfunz00_bglt;


#endif													// BGL_INLINE_RECURSION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL bool_t
		BGl_iszd2recursivezf3z21zzinline_recursionz00(BgL_variablez00_bglt);
	extern obj_t BGl_za2optimzd2unrollzd2loopzf3za2zf3zzengine_paramz00;
	extern obj_t BGl_nodezd2csez12zc0zzreduce_csez00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31595ze3ze5zzinline_recursionz00(obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzinline_recursionz00 = BUNSPEC;
	extern obj_t BGl_inlinezd2sfunz12zc0zzinline_inlinez00(BgL_variablez00_bglt,
		long, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	extern BgL_nodez00_bglt BGl_alphatiza7eza7zzast_alphatiza7eza7(obj_t, obj_t,
		obj_t, BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	extern bool_t BGl_innerzd2loopzf3z21zzinline_loopz00(BgL_variablez00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzinline_recursionz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern bool_t BGl_iszd2loopzf3z21zzinline_loopz00(BgL_variablez00_bglt);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzinline_recursionz00(void);
	static obj_t BGl_objectzd2initzd2zzinline_recursionz00(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern BgL_nodez00_bglt
		BGl_nestzd2loopz12zc0zzinline_loopz00(BgL_nodez00_bglt, BgL_localz00_bglt,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern BgL_localz00_bglt BGl_clonezd2localzd2zzast_localz00(BgL_localz00_bglt,
		BgL_valuez00_bglt);
	static obj_t BGl_z62findzd2recursivezd2callsz62zzinline_recursionz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern BgL_localz00_bglt BGl_makezd2localzd2sfunz00zzast_localz00(obj_t,
		BgL_typez00_bglt, BgL_sfunz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzinline_recursionz00(obj_t, obj_t);
	static obj_t
		BGl_findzd2recursivezd2callsz00zzinline_recursionz00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzinline_recursionz00(void);
	extern bool_t BGl_inlinezd2appzf3z21zzinline_appz00(BgL_variablez00_bglt,
		long, long, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t
		BGl_z62findzd2recursivezd2calls1303z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1306z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1308z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1310z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_alphatiza7ezd2sanszd2closureza7zzast_alphatiza7eza7(obj_t, obj_t, obj_t,
		BgL_nodez00_bglt, BgL_variablez00_bglt);
	static obj_t
		BGl_z62findzd2recursivezd2calls1312z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1314z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1316z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1318z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t
		BGl_z62findzd2recursivezd2calls1320z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1322z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1324z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1326z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t
		BGl_z62findzd2recursivezd2calls1328z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_inlinezd2appzd2simplez00zzinline_simplez00(BgL_nodez00_bglt, long,
		obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1330z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1332z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1334z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1336z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62findzd2recursivezd2calls1338z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_svarz00zzast_varz00;
	extern BgL_appz00_bglt
		BGl_removezd2invariantzd2argsz12z12zzinline_variantz00(BgL_appz00_bglt);
	extern obj_t BGl_isfunz00zzinline_inlinez00;
	static obj_t
		BGl_z62findzd2recursivezd2calls1340z62zzinline_recursionz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2verboseza2z00zzengine_paramz00;
	extern obj_t BGl_variantzd2argszd2zzinline_variantz00(BgL_variablez00_bglt);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzinline_recursionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreduce_csez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_loopz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_variantz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_simplez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_shrinkzd2argsz12zc0zzinline_variantz00(BgL_variablez00_bglt);
	static obj_t BGl_findzd2recursivezd2callsza2za2zzinline_recursionz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62iszd2recursivezf3z43zzinline_recursionz00(obj_t, obj_t);
	static obj_t
		BGl_inlinezd2appzd2labelsz00zzinline_recursionz00(BgL_nodez00_bglt, long,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_za2inliningzd2reducezd2kfactorza2z00zzengine_paramz00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62inlinezd2appzd2recursivez62zzinline_recursionz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzinline_recursionz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinline_recursionz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzinline_recursionz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinline_recursionz00(void);
	static BgL_letzd2funzd2_bglt
		BGl_plainzd2callzd2zzinline_recursionz00(BgL_variablez00_bglt,
		BgL_nodez00_bglt, BgL_localz00_bglt, BgL_appz00_bglt, obj_t);
	extern obj_t BGl_substitutionsz00zzinline_variantz00(BgL_variablez00_bglt,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_inlinezd2appzd2recursivez00zzinline_recursionz00(BgL_nodez00_bglt, long,
		obj_t);
	extern obj_t BGl_currentzd2functionzd2zztools_errorz00(void);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_unrollzd2callzd2zzinline_recursionz00(BgL_variablez00_bglt,
		BgL_nodez00_bglt, BgL_localz00_bglt, obj_t, BgL_appz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static bool_t BGl_tailreczf3zf3zzinline_recursionz00(BgL_variablez00_bglt);
	extern obj_t BGl_switchz00zzast_nodez00;
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	extern obj_t BGl_invariantzd2argszd2zzinline_variantz00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1960z00zzinline_recursionz00,
		BgL_bgl_string1960za700za7za7i1989za7, "simple", 6);
	      DEFINE_STRING(BGl_string1961z00zzinline_recursionz00,
		BgL_bgl_string1961za700za7za7i1990za7, " (recursive)", 12);
	      DEFINE_STRING(BGl_string1962z00zzinline_recursionz00,
		BgL_bgl_string1962za700za7za7i1991za7, " --> ", 5);
	      DEFINE_STRING(BGl_string1963z00zzinline_recursionz00,
		BgL_bgl_string1963za700za7za7i1992za7, "         ", 9);
	      DEFINE_STRING(BGl_string1964z00zzinline_recursionz00,
		BgL_bgl_string1964za700za7za7i1993za7, "unrolling", 9);
	      DEFINE_STRING(BGl_string1966z00zzinline_recursionz00,
		BgL_bgl_string1966za700za7za7i1994za7, "find-recursive-calls1303", 24);
	      DEFINE_STRING(BGl_string1968z00zzinline_recursionz00,
		BgL_bgl_string1968za700za7za7i1995za7, "find-recursive-calls", 20);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs1996z00,
		BGl_z62findzd2recursivezd2callsz62zzinline_recursionz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1965z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs1997z00,
		BGl_z62findzd2recursivezd2calls1303z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs1998z00,
		BGl_z62findzd2recursivezd2calls1306z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1969z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs1999z00,
		BGl_z62findzd2recursivezd2calls1308z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1970z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2000z00,
		BGl_z62findzd2recursivezd2calls1310z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2001z00,
		BGl_z62findzd2recursivezd2calls1312z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1972z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2002z00,
		BGl_z62findzd2recursivezd2calls1314z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1973z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2003z00,
		BGl_z62findzd2recursivezd2calls1316z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1974z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2004z00,
		BGl_z62findzd2recursivezd2calls1318z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1975z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2005z00,
		BGl_z62findzd2recursivezd2calls1320z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1976z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2006z00,
		BGl_z62findzd2recursivezd2calls1322z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1977z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2007z00,
		BGl_z62findzd2recursivezd2calls1324z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1978z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2008z00,
		BGl_z62findzd2recursivezd2calls1326z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1979z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2009z00,
		BGl_z62findzd2recursivezd2calls1328z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STRING(BGl_string1986z00zzinline_recursionz00,
		BgL_bgl_string1986za700za7za7i2010za7, "inline_recursion", 16);
	      DEFINE_STRING(BGl_string1987z00zzinline_recursionz00,
		BgL_bgl_string1987za700za7za7i2011za7, "(sifun sgfun) sfun ", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1980z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2012z00,
		BGl_z62findzd2recursivezd2calls1330z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1981z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2013z00,
		BGl_z62findzd2recursivezd2calls1332z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1982z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2014z00,
		BGl_z62findzd2recursivezd2calls1334z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1983z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2015z00,
		BGl_z62findzd2recursivezd2calls1336z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1984z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2016z00,
		BGl_z62findzd2recursivezd2calls1338z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1985z00zzinline_recursionz00,
		BgL_bgl_za762findza7d2recurs2017z00,
		BGl_z62findzd2recursivezd2calls1340z62zzinline_recursionz00, 0L, BUNSPEC,
		4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2appzd2recursivezd2envzd2zzinline_recursionz00,
		BgL_bgl_za762inlineza7d2appza72018za7,
		BGl_z62inlinezd2appzd2recursivez62zzinline_recursionz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_iszd2recursivezf3zd2envzf3zzinline_recursionz00,
		BgL_bgl_za762isza7d2recursiv2019z00,
		BGl_z62iszd2recursivezf3z43zzinline_recursionz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzinline_recursionz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzinline_recursionz00(long
		BgL_checksumz00_3131, char *BgL_fromz00_3132)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinline_recursionz00))
				{
					BGl_requirezd2initializa7ationz75zzinline_recursionz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinline_recursionz00();
					BGl_libraryzd2moduleszd2initz00zzinline_recursionz00();
					BGl_cnstzd2initzd2zzinline_recursionz00();
					BGl_importedzd2moduleszd2initz00zzinline_recursionz00();
					BGl_genericzd2initzd2zzinline_recursionz00();
					BGl_methodzd2initzd2zzinline_recursionz00();
					return BGl_toplevelzd2initzd2zzinline_recursionz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinline_recursionz00(void)
	{
		{	/* Inline/recursion.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "inline_recursion");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"inline_recursion");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "inline_recursion");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"inline_recursion");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"inline_recursion");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"inline_recursion");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "inline_recursion");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"inline_recursion");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "inline_recursion");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"inline_recursion");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"inline_recursion");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"inline_recursion");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinline_recursionz00(void)
	{
		{	/* Inline/recursion.scm 15 */
			{	/* Inline/recursion.scm 15 */
				obj_t BgL_cportz00_2998;

				{	/* Inline/recursion.scm 15 */
					obj_t BgL_stringz00_3005;

					BgL_stringz00_3005 = BGl_string1987z00zzinline_recursionz00;
					{	/* Inline/recursion.scm 15 */
						obj_t BgL_startz00_3006;

						BgL_startz00_3006 = BINT(0L);
						{	/* Inline/recursion.scm 15 */
							obj_t BgL_endz00_3007;

							BgL_endz00_3007 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3005)));
							{	/* Inline/recursion.scm 15 */

								BgL_cportz00_2998 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3005, BgL_startz00_3006, BgL_endz00_3007);
				}}}}
				{
					long BgL_iz00_2999;

					BgL_iz00_2999 = 1L;
				BgL_loopz00_3000:
					if ((BgL_iz00_2999 == -1L))
						{	/* Inline/recursion.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Inline/recursion.scm 15 */
							{	/* Inline/recursion.scm 15 */
								obj_t BgL_arg1988z00_3001;

								{	/* Inline/recursion.scm 15 */

									{	/* Inline/recursion.scm 15 */
										obj_t BgL_locationz00_3003;

										BgL_locationz00_3003 = BBOOL(((bool_t) 0));
										{	/* Inline/recursion.scm 15 */

											BgL_arg1988z00_3001 =
												BGl_readz00zz__readerz00(BgL_cportz00_2998,
												BgL_locationz00_3003);
										}
									}
								}
								{	/* Inline/recursion.scm 15 */
									int BgL_tmpz00_3164;

									BgL_tmpz00_3164 = (int) (BgL_iz00_2999);
									CNST_TABLE_SET(BgL_tmpz00_3164, BgL_arg1988z00_3001);
							}}
							{	/* Inline/recursion.scm 15 */
								int BgL_auxz00_3004;

								BgL_auxz00_3004 = (int) ((BgL_iz00_2999 - 1L));
								{
									long BgL_iz00_3169;

									BgL_iz00_3169 = (long) (BgL_auxz00_3004);
									BgL_iz00_2999 = BgL_iz00_3169;
									goto BgL_loopz00_3000;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinline_recursionz00(void)
	{
		{	/* Inline/recursion.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzinline_recursionz00(void)
	{
		{	/* Inline/recursion.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzinline_recursionz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1576;

				BgL_headz00_1576 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1577;
					obj_t BgL_tailz00_1578;

					BgL_prevz00_1577 = BgL_headz00_1576;
					BgL_tailz00_1578 = BgL_l1z00_1;
				BgL_loopz00_1579:
					if (PAIRP(BgL_tailz00_1578))
						{
							obj_t BgL_newzd2prevzd2_1581;

							BgL_newzd2prevzd2_1581 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1578), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1577, BgL_newzd2prevzd2_1581);
							{
								obj_t BgL_tailz00_3179;
								obj_t BgL_prevz00_3178;

								BgL_prevz00_3178 = BgL_newzd2prevzd2_1581;
								BgL_tailz00_3179 = CDR(BgL_tailz00_1578);
								BgL_tailz00_1578 = BgL_tailz00_3179;
								BgL_prevz00_1577 = BgL_prevz00_3178;
								goto BgL_loopz00_1579;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1576);
				}
			}
		}

	}



/* inline-app-recursive */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_inlinezd2appzd2recursivez00zzinline_recursionz00(BgL_nodez00_bglt
		BgL_nodez00_3, long BgL_kfactorz00_4, obj_t BgL_stackz00_5)
	{
		{	/* Inline/recursion.scm 41 */
			{	/* Inline/recursion.scm 42 */
				BgL_varz00_bglt BgL_funz00_1584;

				BgL_funz00_1584 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_3)))->BgL_funz00);
				{	/* Inline/recursion.scm 42 */
					BgL_variablez00_bglt BgL_calleez00_1585;

					BgL_calleez00_1585 =
						(((BgL_varz00_bglt) COBJECT(BgL_funz00_1584))->BgL_variablez00);
					{	/* Inline/recursion.scm 43 */

						if (BGl_tailreczf3zf3zzinline_recursionz00(BgL_calleez00_1585))
							{	/* Inline/recursion.scm 45 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
											((obj_t) BgL_calleez00_1585), BgL_stackz00_5)))
									{	/* Inline/recursion.scm 47 */
										return
											BGl_inlinezd2appzd2simplez00zzinline_simplez00
											(BgL_nodez00_3, BgL_kfactorz00_4, BgL_stackz00_5,
											BGl_string1960z00zzinline_recursionz00);
									}
								else
									{	/* Inline/recursion.scm 47 */
										return
											((BgL_nodez00_bglt)
											BGl_inlinezd2appzd2labelsz00zzinline_recursionz00
											(BgL_nodez00_3, BgL_kfactorz00_4, BgL_stackz00_5));
									}
							}
						else
							{	/* Inline/recursion.scm 45 */
								return BgL_nodez00_3;
							}
					}
				}
			}
		}

	}



/* &inline-app-recursive */
	BgL_nodez00_bglt BGl_z62inlinezd2appzd2recursivez62zzinline_recursionz00(obj_t
		BgL_envz00_2869, obj_t BgL_nodez00_2870, obj_t BgL_kfactorz00_2871,
		obj_t BgL_stackz00_2872)
	{
		{	/* Inline/recursion.scm 41 */
			return
				BGl_inlinezd2appzd2recursivez00zzinline_recursionz00(
				((BgL_nodez00_bglt) BgL_nodez00_2870),
				(long) CINT(BgL_kfactorz00_2871), BgL_stackz00_2872);
		}

	}



/* tailrec? */
	bool_t BGl_tailreczf3zf3zzinline_recursionz00(BgL_variablez00_bglt
		BgL_varz00_6)
	{
		{	/* Inline/recursion.scm 62 */
			{	/* Inline/recursion.scm 63 */
				bool_t BgL_test2025z00_3197;

				{	/* Inline/recursion.scm 63 */
					BgL_valuez00_bglt BgL_arg1348z00_1591;

					BgL_arg1348z00_1591 =
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_6))->BgL_valuez00);
					{	/* Inline/recursion.scm 63 */
						obj_t BgL_classz00_2224;

						BgL_classz00_2224 = BGl_isfunz00zzinline_inlinez00;
						{	/* Inline/recursion.scm 63 */
							BgL_objectz00_bglt BgL_arg1807z00_2226;

							{	/* Inline/recursion.scm 63 */
								obj_t BgL_tmpz00_3199;

								BgL_tmpz00_3199 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1348z00_1591));
								BgL_arg1807z00_2226 = (BgL_objectz00_bglt) (BgL_tmpz00_3199);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Inline/recursion.scm 63 */
									long BgL_idxz00_2232;

									BgL_idxz00_2232 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2226);
									BgL_test2025z00_3197 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2232 + 4L)) == BgL_classz00_2224);
								}
							else
								{	/* Inline/recursion.scm 63 */
									bool_t BgL_res1936z00_2257;

									{	/* Inline/recursion.scm 63 */
										obj_t BgL_oclassz00_2240;

										{	/* Inline/recursion.scm 63 */
											obj_t BgL_arg1815z00_2248;
											long BgL_arg1816z00_2249;

											BgL_arg1815z00_2248 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Inline/recursion.scm 63 */
												long BgL_arg1817z00_2250;

												BgL_arg1817z00_2250 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2226);
												BgL_arg1816z00_2249 =
													(BgL_arg1817z00_2250 - OBJECT_TYPE);
											}
											BgL_oclassz00_2240 =
												VECTOR_REF(BgL_arg1815z00_2248, BgL_arg1816z00_2249);
										}
										{	/* Inline/recursion.scm 63 */
											bool_t BgL__ortest_1115z00_2241;

											BgL__ortest_1115z00_2241 =
												(BgL_classz00_2224 == BgL_oclassz00_2240);
											if (BgL__ortest_1115z00_2241)
												{	/* Inline/recursion.scm 63 */
													BgL_res1936z00_2257 = BgL__ortest_1115z00_2241;
												}
											else
												{	/* Inline/recursion.scm 63 */
													long BgL_odepthz00_2242;

													{	/* Inline/recursion.scm 63 */
														obj_t BgL_arg1804z00_2243;

														BgL_arg1804z00_2243 = (BgL_oclassz00_2240);
														BgL_odepthz00_2242 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2243);
													}
													if ((4L < BgL_odepthz00_2242))
														{	/* Inline/recursion.scm 63 */
															obj_t BgL_arg1802z00_2245;

															{	/* Inline/recursion.scm 63 */
																obj_t BgL_arg1803z00_2246;

																BgL_arg1803z00_2246 = (BgL_oclassz00_2240);
																BgL_arg1802z00_2245 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2246,
																	4L);
															}
															BgL_res1936z00_2257 =
																(BgL_arg1802z00_2245 == BgL_classz00_2224);
														}
													else
														{	/* Inline/recursion.scm 63 */
															BgL_res1936z00_2257 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2025z00_3197 = BgL_res1936z00_2257;
								}
						}
					}
				}
				if (BgL_test2025z00_3197)
					{	/* Inline/recursion.scm 64 */
						BgL_sfunz00_bglt BgL_i1109z00_1590;

						BgL_i1109z00_1590 =
							((BgL_sfunz00_bglt)
							(((BgL_variablez00_bglt) COBJECT(BgL_varz00_6))->BgL_valuez00));
						{
							BgL_isfunz00_bglt BgL_auxz00_3224;

							{
								obj_t BgL_auxz00_3225;

								{	/* Inline/recursion.scm 64 */
									BgL_objectz00_bglt BgL_tmpz00_3226;

									BgL_tmpz00_3226 = ((BgL_objectz00_bglt) BgL_i1109z00_1590);
									BgL_auxz00_3225 = BGL_OBJECT_WIDENING(BgL_tmpz00_3226);
								}
								BgL_auxz00_3224 = ((BgL_isfunz00_bglt) BgL_auxz00_3225);
							}
							return
								(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3224))->
								BgL_tailrecz00);
						}
					}
				else
					{	/* Inline/recursion.scm 63 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* inline-app-labels */
	obj_t BGl_inlinezd2appzd2labelsz00zzinline_recursionz00(BgL_nodez00_bglt
		BgL_nodez00_7, long BgL_kfactorz00_8, obj_t BgL_stackz00_9)
	{
		{	/* Inline/recursion.scm 72 */
			{	/* Inline/recursion.scm 73 */
				BgL_variablez00_bglt BgL_variablez00_1592;

				BgL_variablez00_1592 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_7)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Inline/recursion.scm 73 */
					BgL_localz00_bglt BgL_localz00_1593;

					{	/* Inline/recursion.scm 74 */
						obj_t BgL_arg1571z00_1719;
						BgL_typez00_bglt BgL_arg1573z00_1720;
						BgL_valuez00_bglt BgL_arg1575z00_1721;

						BgL_arg1571z00_1719 =
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_1592))->
							BgL_idz00);
						BgL_arg1573z00_1720 =
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_1592))->
							BgL_typez00);
						BgL_arg1575z00_1721 =
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_1592))->
							BgL_valuez00);
						BgL_localz00_1593 =
							BGl_makezd2localzd2sfunz00zzast_localz00(BgL_arg1571z00_1719,
							BgL_arg1573z00_1720, ((BgL_sfunz00_bglt) BgL_arg1575z00_1721));
					}
					{	/* Inline/recursion.scm 74 */
						BgL_valuez00_bglt BgL_oldzd2sfunzd2_1594;

						BgL_oldzd2sfunzd2_1594 =
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_1592))->
							BgL_valuez00);
						{	/* Inline/recursion.scm 77 */
							obj_t BgL_oldzd2bodyzd2_1595;

							{	/* Inline/recursion.scm 78 */
								bool_t BgL_test2029z00_3240;

								{	/* Inline/recursion.scm 78 */
									obj_t BgL_classz00_2266;

									BgL_classz00_2266 = BGl_isfunz00zzinline_inlinez00;
									{	/* Inline/recursion.scm 78 */
										BgL_objectz00_bglt BgL_arg1807z00_2268;

										{	/* Inline/recursion.scm 78 */
											obj_t BgL_tmpz00_3241;

											BgL_tmpz00_3241 =
												((obj_t) ((BgL_objectz00_bglt) BgL_oldzd2sfunzd2_1594));
											BgL_arg1807z00_2268 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3241);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Inline/recursion.scm 78 */
												long BgL_idxz00_2274;

												BgL_idxz00_2274 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2268);
												BgL_test2029z00_3240 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2274 + 4L)) == BgL_classz00_2266);
											}
										else
											{	/* Inline/recursion.scm 78 */
												bool_t BgL_res1937z00_2299;

												{	/* Inline/recursion.scm 78 */
													obj_t BgL_oclassz00_2282;

													{	/* Inline/recursion.scm 78 */
														obj_t BgL_arg1815z00_2290;
														long BgL_arg1816z00_2291;

														BgL_arg1815z00_2290 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Inline/recursion.scm 78 */
															long BgL_arg1817z00_2292;

															BgL_arg1817z00_2292 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2268);
															BgL_arg1816z00_2291 =
																(BgL_arg1817z00_2292 - OBJECT_TYPE);
														}
														BgL_oclassz00_2282 =
															VECTOR_REF(BgL_arg1815z00_2290,
															BgL_arg1816z00_2291);
													}
													{	/* Inline/recursion.scm 78 */
														bool_t BgL__ortest_1115z00_2283;

														BgL__ortest_1115z00_2283 =
															(BgL_classz00_2266 == BgL_oclassz00_2282);
														if (BgL__ortest_1115z00_2283)
															{	/* Inline/recursion.scm 78 */
																BgL_res1937z00_2299 = BgL__ortest_1115z00_2283;
															}
														else
															{	/* Inline/recursion.scm 78 */
																long BgL_odepthz00_2284;

																{	/* Inline/recursion.scm 78 */
																	obj_t BgL_arg1804z00_2285;

																	BgL_arg1804z00_2285 = (BgL_oclassz00_2282);
																	BgL_odepthz00_2284 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2285);
																}
																if ((4L < BgL_odepthz00_2284))
																	{	/* Inline/recursion.scm 78 */
																		obj_t BgL_arg1802z00_2287;

																		{	/* Inline/recursion.scm 78 */
																			obj_t BgL_arg1803z00_2288;

																			BgL_arg1803z00_2288 =
																				(BgL_oclassz00_2282);
																			BgL_arg1802z00_2287 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2288, 4L);
																		}
																		BgL_res1937z00_2299 =
																			(BgL_arg1802z00_2287 ==
																			BgL_classz00_2266);
																	}
																else
																	{	/* Inline/recursion.scm 78 */
																		BgL_res1937z00_2299 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2029z00_3240 = BgL_res1937z00_2299;
											}
									}
								}
								if (BgL_test2029z00_3240)
									{
										BgL_nodez00_bglt BgL_auxz00_3264;

										{
											BgL_isfunz00_bglt BgL_auxz00_3265;

											{
												obj_t BgL_auxz00_3266;

												{	/* Inline/recursion.scm 79 */
													BgL_objectz00_bglt BgL_tmpz00_3267;

													BgL_tmpz00_3267 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_oldzd2sfunzd2_1594));
													BgL_auxz00_3266 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3267);
												}
												BgL_auxz00_3265 = ((BgL_isfunz00_bglt) BgL_auxz00_3266);
											}
											BgL_auxz00_3264 =
												(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3265))->
												BgL_originalzd2bodyzd2);
										}
										BgL_oldzd2bodyzd2_1595 = ((obj_t) BgL_auxz00_3264);
									}
								else
									{	/* Inline/recursion.scm 78 */
										BgL_oldzd2bodyzd2_1595 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_oldzd2sfunzd2_1594)))->
											BgL_bodyz00);
									}
							}
							{	/* Inline/recursion.scm 78 */
								obj_t BgL_reczd2callszd2_1596;

								{	/* Inline/recursion.scm 81 */
									obj_t BgL_arg1564z00_1717;

									BgL_arg1564z00_1717 = MAKE_YOUNG_CELL(BTRUE);
									BgL_reczd2callszd2_1596 =
										BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
										((BgL_nodez00_bglt) BgL_oldzd2bodyzd2_1595),
										BgL_variablez00_1592, BTRUE, BgL_arg1564z00_1717);
								}
								{	/* Inline/recursion.scm 81 */
									obj_t BgL_oldzd2argszd2_1597;

									BgL_oldzd2argszd2_1597 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_oldzd2sfunzd2_1594)))->
										BgL_argsz00);
									{	/* Inline/recursion.scm 82 */
										obj_t BgL_invzd2argszd2_1598;

										BgL_invzd2argszd2_1598 =
											BGl_invariantzd2argszd2zzinline_variantz00(BgL_nodez00_7,
											BgL_variablez00_1592, BgL_reczd2callszd2_1596);
										{	/* Inline/recursion.scm 83 */
											obj_t BgL_varzd2argszd2_1599;

											BgL_varzd2argszd2_1599 =
												BGl_variantzd2argszd2zzinline_variantz00
												(BgL_variablez00_1592);
											{	/* Inline/recursion.scm 84 */
												obj_t BgL_newzd2argszd2_1600;

												if (NULLP(BgL_varzd2argszd2_1599))
													{	/* Inline/recursion.scm 85 */
														BgL_newzd2argszd2_1600 = BNIL;
													}
												else
													{	/* Inline/recursion.scm 85 */
														obj_t BgL_head1287z00_1701;

														BgL_head1287z00_1701 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1285z00_1703;
															obj_t BgL_tail1288z00_1704;

															BgL_l1285z00_1703 = BgL_varzd2argszd2_1599;
															BgL_tail1288z00_1704 = BgL_head1287z00_1701;
														BgL_zc3z04anonymousza31547ze3z87_1705:
															if (NULLP(BgL_l1285z00_1703))
																{	/* Inline/recursion.scm 85 */
																	BgL_newzd2argszd2_1600 =
																		CDR(BgL_head1287z00_1701);
																}
															else
																{	/* Inline/recursion.scm 85 */
																	obj_t BgL_newtail1289z00_1707;

																	{	/* Inline/recursion.scm 85 */
																		BgL_localz00_bglt BgL_arg1553z00_1709;

																		{	/* Inline/recursion.scm 85 */
																			obj_t BgL_lz00_1710;

																			BgL_lz00_1710 =
																				CAR(((obj_t) BgL_l1285z00_1703));
																			{	/* Inline/recursion.scm 86 */
																				BgL_svarz00_bglt BgL_arg1559z00_1711;

																				{	/* Inline/recursion.scm 86 */
																					BgL_svarz00_bglt
																						BgL_duplicated1112z00_1712;
																					BgL_svarz00_bglt BgL_new1110z00_1713;

																					BgL_duplicated1112z00_1712 =
																						((BgL_svarz00_bglt)
																						(((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										((BgL_localz00_bglt)
																											BgL_lz00_1710))))->
																							BgL_valuez00));
																					{	/* Inline/recursion.scm 86 */
																						BgL_svarz00_bglt
																							BgL_new1113z00_1714;
																						BgL_new1113z00_1714 =
																							((BgL_svarz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_svarz00_bgl))));
																						{	/* Inline/recursion.scm 86 */
																							long BgL_arg1561z00_1715;

																							BgL_arg1561z00_1715 =
																								BGL_CLASS_NUM
																								(BGl_svarz00zzast_varz00);
																							BGL_OBJECT_CLASS_NUM_SET((
																									(BgL_objectz00_bglt)
																									BgL_new1113z00_1714),
																								BgL_arg1561z00_1715);
																						}
																						{	/* Inline/recursion.scm 86 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_3299;
																							BgL_tmpz00_3299 =
																								((BgL_objectz00_bglt)
																								BgL_new1113z00_1714);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_3299, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1113z00_1714);
																						BgL_new1110z00_1713 =
																							BgL_new1113z00_1714;
																					}
																					((((BgL_svarz00_bglt)
																								COBJECT(BgL_new1110z00_1713))->
																							BgL_locz00) =
																						((obj_t) (((BgL_svarz00_bglt)
																									COBJECT
																									(BgL_duplicated1112z00_1712))->
																								BgL_locz00)), BUNSPEC);
																					BgL_arg1559z00_1711 =
																						BgL_new1110z00_1713;
																				}
																				BgL_arg1553z00_1709 =
																					BGl_clonezd2localzd2zzast_localz00(
																					((BgL_localz00_bglt) BgL_lz00_1710),
																					((BgL_valuez00_bglt)
																						BgL_arg1559z00_1711));
																		}}
																		BgL_newtail1289z00_1707 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg1553z00_1709), BNIL);
																	}
																	SET_CDR(BgL_tail1288z00_1704,
																		BgL_newtail1289z00_1707);
																	{	/* Inline/recursion.scm 85 */
																		obj_t BgL_arg1552z00_1708;

																		BgL_arg1552z00_1708 =
																			CDR(((obj_t) BgL_l1285z00_1703));
																		{
																			obj_t BgL_tail1288z00_3314;
																			obj_t BgL_l1285z00_3313;

																			BgL_l1285z00_3313 = BgL_arg1552z00_1708;
																			BgL_tail1288z00_3314 =
																				BgL_newtail1289z00_1707;
																			BgL_tail1288z00_1704 =
																				BgL_tail1288z00_3314;
																			BgL_l1285z00_1703 = BgL_l1285z00_3313;
																			goto
																				BgL_zc3z04anonymousza31547ze3z87_1705;
																		}
																	}
																}
														}
													}
												{	/* Inline/recursion.scm 85 */
													obj_t BgL_substitutez00_1601;

													{	/* Inline/recursion.scm 88 */
														obj_t BgL_arg1544z00_1698;

														BgL_arg1544z00_1698 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_7)))->
															BgL_argsz00);
														BgL_substitutez00_1601 =
															BGl_substitutionsz00zzinline_variantz00
															(BgL_variablez00_1592, BgL_arg1544z00_1698,
															BgL_newzd2argszd2_1600);
													}
													{	/* Inline/recursion.scm 88 */
														obj_t BgL_svgzd2callszd2argsz00_1602;

														if (NULLP(BgL_reczd2callszd2_1596))
															{	/* Inline/recursion.scm 89 */
																BgL_svgzd2callszd2argsz00_1602 = BNIL;
															}
														else
															{	/* Inline/recursion.scm 89 */
																obj_t BgL_head1292z00_1687;

																BgL_head1292z00_1687 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1290z00_1689;
																	obj_t BgL_tail1293z00_1690;

																	BgL_l1290z00_1689 = BgL_reczd2callszd2_1596;
																	BgL_tail1293z00_1690 = BgL_head1292z00_1687;
																BgL_zc3z04anonymousza31515ze3z87_1691:
																	if (NULLP(BgL_l1290z00_1689))
																		{	/* Inline/recursion.scm 89 */
																			BgL_svgzd2callszd2argsz00_1602 =
																				CDR(BgL_head1292z00_1687);
																		}
																	else
																		{	/* Inline/recursion.scm 89 */
																			obj_t BgL_newtail1294z00_1693;

																			{	/* Inline/recursion.scm 89 */
																				obj_t BgL_arg1540z00_1695;

																				BgL_arg1540z00_1695 =
																					(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								CAR(
																									((obj_t)
																										BgL_l1290z00_1689)))))->
																					BgL_argsz00);
																				BgL_newtail1294z00_1693 =
																					MAKE_YOUNG_PAIR(BgL_arg1540z00_1695,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1293z00_1690,
																				BgL_newtail1294z00_1693);
																			{	/* Inline/recursion.scm 89 */
																				obj_t BgL_arg1535z00_1694;

																				BgL_arg1535z00_1694 =
																					CDR(((obj_t) BgL_l1290z00_1689));
																				{
																					obj_t BgL_tail1293z00_3333;
																					obj_t BgL_l1290z00_3332;

																					BgL_l1290z00_3332 =
																						BgL_arg1535z00_1694;
																					BgL_tail1293z00_3333 =
																						BgL_newtail1294z00_1693;
																					BgL_tail1293z00_1690 =
																						BgL_tail1293z00_3333;
																					BgL_l1290z00_1689 = BgL_l1290z00_3332;
																					goto
																						BgL_zc3z04anonymousza31515ze3z87_1691;
																				}
																			}
																		}
																}
															}
														{	/* Inline/recursion.scm 89 */
															bool_t BgL_removez12z12_1603;

															{
																obj_t BgL_l1295z00_1679;

																BgL_l1295z00_1679 = BgL_reczd2callszd2_1596;
															BgL_zc3z04anonymousza31503ze3z87_1680:
																if (PAIRP(BgL_l1295z00_1679))
																	{	/* Inline/recursion.scm 90 */
																		{	/* Inline/recursion.scm 90 */
																			obj_t BgL_arg1509z00_1682;

																			BgL_arg1509z00_1682 =
																				CAR(BgL_l1295z00_1679);
																			BGl_removezd2invariantzd2argsz12z12zzinline_variantz00
																				(((BgL_appz00_bglt)
																					BgL_arg1509z00_1682));
																		}
																		{
																			obj_t BgL_l1295z00_3339;

																			BgL_l1295z00_3339 =
																				CDR(BgL_l1295z00_1679);
																			BgL_l1295z00_1679 = BgL_l1295z00_3339;
																			goto
																				BgL_zc3z04anonymousza31503ze3z87_1680;
																		}
																	}
																else
																	{	/* Inline/recursion.scm 90 */
																		BgL_removez12z12_1603 = ((bool_t) 1);
																	}
															}
															{	/* Inline/recursion.scm 90 */
																obj_t BgL_ilocz00_1604;

																{	/* Inline/recursion.scm 91 */
																	bool_t BgL_test2038z00_3341;

																	{	/* Inline/recursion.scm 91 */
																		obj_t BgL_classz00_2321;

																		BgL_classz00_2321 =
																			BGl_globalz00zzast_varz00;
																		{	/* Inline/recursion.scm 91 */
																			BgL_objectz00_bglt BgL_arg1807z00_2323;

																			{	/* Inline/recursion.scm 91 */
																				obj_t BgL_tmpz00_3342;

																				BgL_tmpz00_3342 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_variablez00_1592));
																				BgL_arg1807z00_2323 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_3342);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Inline/recursion.scm 91 */
																					long BgL_idxz00_2329;

																					BgL_idxz00_2329 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2323);
																					BgL_test2038z00_3341 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2329 + 2L)) ==
																						BgL_classz00_2321);
																				}
																			else
																				{	/* Inline/recursion.scm 91 */
																					bool_t BgL_res1938z00_2354;

																					{	/* Inline/recursion.scm 91 */
																						obj_t BgL_oclassz00_2337;

																						{	/* Inline/recursion.scm 91 */
																							obj_t BgL_arg1815z00_2345;
																							long BgL_arg1816z00_2346;

																							BgL_arg1815z00_2345 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Inline/recursion.scm 91 */
																								long BgL_arg1817z00_2347;

																								BgL_arg1817z00_2347 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2323);
																								BgL_arg1816z00_2346 =
																									(BgL_arg1817z00_2347 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2337 =
																								VECTOR_REF(BgL_arg1815z00_2345,
																								BgL_arg1816z00_2346);
																						}
																						{	/* Inline/recursion.scm 91 */
																							bool_t BgL__ortest_1115z00_2338;

																							BgL__ortest_1115z00_2338 =
																								(BgL_classz00_2321 ==
																								BgL_oclassz00_2337);
																							if (BgL__ortest_1115z00_2338)
																								{	/* Inline/recursion.scm 91 */
																									BgL_res1938z00_2354 =
																										BgL__ortest_1115z00_2338;
																								}
																							else
																								{	/* Inline/recursion.scm 91 */
																									long BgL_odepthz00_2339;

																									{	/* Inline/recursion.scm 91 */
																										obj_t BgL_arg1804z00_2340;

																										BgL_arg1804z00_2340 =
																											(BgL_oclassz00_2337);
																										BgL_odepthz00_2339 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2340);
																									}
																									if ((2L < BgL_odepthz00_2339))
																										{	/* Inline/recursion.scm 91 */
																											obj_t BgL_arg1802z00_2342;

																											{	/* Inline/recursion.scm 91 */
																												obj_t
																													BgL_arg1803z00_2343;
																												BgL_arg1803z00_2343 =
																													(BgL_oclassz00_2337);
																												BgL_arg1802z00_2342 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2343,
																													2L);
																											}
																											BgL_res1938z00_2354 =
																												(BgL_arg1802z00_2342 ==
																												BgL_classz00_2321);
																										}
																									else
																										{	/* Inline/recursion.scm 91 */
																											BgL_res1938z00_2354 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2038z00_3341 =
																						BgL_res1938z00_2354;
																				}
																		}
																	}
																	if (BgL_test2038z00_3341)
																		{	/* Inline/recursion.scm 92 */
																			bool_t BgL_test2042z00_3365;

																			{	/* Inline/recursion.scm 92 */
																				obj_t BgL_arg1502z00_1677;

																				BgL_arg1502z00_1677 =
																					(((BgL_globalz00_bglt) COBJECT(
																							((BgL_globalz00_bglt)
																								BgL_variablez00_1592)))->
																					BgL_modulez00);
																				BgL_test2042z00_3365 =
																					(BgL_arg1502z00_1677 ==
																					BGl_za2moduleza2z00zzmodule_modulez00);
																			}
																			if (BgL_test2042z00_3365)
																				{	/* Inline/recursion.scm 92 */
																					BgL_ilocz00_1604 = BFALSE;
																				}
																			else
																				{	/* Inline/recursion.scm 92 */
																					BgL_ilocz00_1604 =
																						(((BgL_nodez00_bglt)
																							COBJECT(BgL_nodez00_7))->
																						BgL_locz00);
																				}
																		}
																	else
																		{	/* Inline/recursion.scm 91 */
																			BgL_ilocz00_1604 = BFALSE;
																		}
																}
																{	/* Inline/recursion.scm 91 */
																	BgL_nodez00_bglt BgL_newzd2bodyzd2_1605;

																	if (NULLP(BgL_invzd2argszd2_1598))
																		{	/* Inline/recursion.scm 96 */
																			obj_t BgL_arg1472z00_1671;
																			obj_t BgL_arg1473z00_1672;

																			BgL_arg1472z00_1671 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_variablez00_1592),
																				BgL_oldzd2argszd2_1597);
																			BgL_arg1473z00_1672 =
																				MAKE_YOUNG_PAIR(((obj_t)
																					BgL_localz00_1593),
																				BgL_substitutez00_1601);
																			BgL_newzd2bodyzd2_1605 =
																				BGl_alphatiza7eza7zzast_alphatiza7eza7
																				(BgL_arg1472z00_1671,
																				BgL_arg1473z00_1672, BgL_ilocz00_1604,
																				((BgL_nodez00_bglt)
																					BgL_oldzd2bodyzd2_1595));
																		}
																	else
																		{	/* Inline/recursion.scm 103 */
																			obj_t BgL_arg1485z00_1673;
																			obj_t BgL_arg1489z00_1674;

																			BgL_arg1485z00_1673 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_variablez00_1592),
																				BgL_oldzd2argszd2_1597);
																			BgL_arg1489z00_1674 =
																				MAKE_YOUNG_PAIR(((obj_t)
																					BgL_localz00_1593),
																				BgL_substitutez00_1601);
																			BgL_newzd2bodyzd2_1605 =
																				BGl_alphatiza7ezd2sanszd2closureza7zzast_alphatiza7eza7
																				(BgL_arg1485z00_1673,
																				BgL_arg1489z00_1674, BgL_ilocz00_1604,
																				((BgL_nodez00_bglt)
																					BgL_oldzd2bodyzd2_1595),
																				BgL_variablez00_1592);
																		}
																	{	/* Inline/recursion.scm 94 */
																		bool_t BgL_restorez12z12_1606;

																		{
																			obj_t BgL_ll1297z00_1661;
																			obj_t BgL_ll1298z00_1662;

																			BgL_ll1297z00_1661 =
																				BgL_reczd2callszd2_1596;
																			BgL_ll1298z00_1662 =
																				BgL_svgzd2callszd2argsz00_1602;
																		BgL_zc3z04anonymousza31449ze3z87_1663:
																			if (NULLP(BgL_ll1297z00_1661))
																				{	/* Inline/recursion.scm 108 */
																					BgL_restorez12z12_1606 = ((bool_t) 1);
																				}
																			else
																				{	/* Inline/recursion.scm 108 */
																					{	/* Inline/recursion.scm 109 */
																						obj_t BgL_appz00_1665;
																						obj_t BgL_argsz00_1666;

																						BgL_appz00_1665 =
																							CAR(((obj_t) BgL_ll1297z00_1661));
																						BgL_argsz00_1666 =
																							CAR(((obj_t) BgL_ll1298z00_1662));
																						((((BgL_appz00_bglt) COBJECT(
																										((BgL_appz00_bglt)
																											BgL_appz00_1665)))->
																								BgL_argsz00) =
																							((obj_t) BgL_argsz00_1666),
																							BUNSPEC);
																					}
																					{	/* Inline/recursion.scm 108 */
																						obj_t BgL_arg1453z00_1667;
																						obj_t BgL_arg1454z00_1668;

																						BgL_arg1453z00_1667 =
																							CDR(((obj_t) BgL_ll1297z00_1661));
																						BgL_arg1454z00_1668 =
																							CDR(((obj_t) BgL_ll1298z00_1662));
																						{
																							obj_t BgL_ll1298z00_3397;
																							obj_t BgL_ll1297z00_3396;

																							BgL_ll1297z00_3396 =
																								BgL_arg1453z00_1667;
																							BgL_ll1298z00_3397 =
																								BgL_arg1454z00_1668;
																							BgL_ll1298z00_1662 =
																								BgL_ll1298z00_3397;
																							BgL_ll1297z00_1661 =
																								BgL_ll1297z00_3396;
																							goto
																								BgL_zc3z04anonymousza31449ze3z87_1663;
																						}
																					}
																				}
																		}
																		{	/* Inline/recursion.scm 108 */
																			BgL_sfunz00_bglt BgL_newzd2sfunzd2_1607;

																			{	/* Inline/recursion.scm 112 */
																				BgL_sfunz00_bglt BgL_new1116z00_1648;

																				{	/* Inline/recursion.scm 112 */
																					BgL_sfunz00_bglt BgL_new1129z00_1658;

																					BgL_new1129z00_1658 =
																						((BgL_sfunz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_sfunz00_bgl))));
																					{	/* Inline/recursion.scm 112 */
																						long BgL_arg1448z00_1659;

																						BgL_arg1448z00_1659 =
																							BGL_CLASS_NUM
																							(BGl_sfunz00zzast_varz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1129z00_1658),
																							BgL_arg1448z00_1659);
																					}
																					{	/* Inline/recursion.scm 112 */
																						BgL_objectz00_bglt BgL_tmpz00_3402;

																						BgL_tmpz00_3402 =
																							((BgL_objectz00_bglt)
																							BgL_new1129z00_1658);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_3402, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1129z00_1658);
																					BgL_new1116z00_1648 =
																						BgL_new1129z00_1658;
																				}
																				((((BgL_funz00_bglt) COBJECT(
																								((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_arityz00) =
																					((long) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_arityz00)), BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_sidezd2effectzd2) =
																					((obj_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_sidezd2effectzd2)), BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_predicatezd2ofzd2) =
																					((obj_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_predicatezd2ofzd2)), BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_stackzd2allocatorzd2) =
																					((obj_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_stackzd2allocatorzd2)),
																					BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_topzf3zf3) =
																					((bool_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_topzf3zf3)), BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_thezd2closurezd2) =
																					((obj_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_thezd2closurezd2)), BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_effectz00) =
																					((obj_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_effectz00)), BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_failsafez00) =
																					((obj_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_failsafez00)), BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_argszd2noescapezd2) =
																					((obj_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_argszd2noescapezd2)),
																					BUNSPEC);
																				((((BgL_funz00_bglt)
																							COBJECT(((BgL_funz00_bglt)
																									BgL_new1116z00_1648)))->
																						BgL_argszd2retescapezd2) =
																					((obj_t) (((BgL_funz00_bglt)
																								COBJECT(((BgL_funz00_bglt)
																										BgL_oldzd2sfunzd2_1594)))->
																							BgL_argszd2retescapezd2)),
																					BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_propertyz00) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_propertyz00)), BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_argsz00) =
																					((obj_t) BgL_newzd2argszd2_1600),
																					BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_argszd2namezd2) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_argszd2namezd2)), BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_bodyz00) =
																					((obj_t) ((obj_t)
																							BgL_newzd2bodyzd2_1605)),
																					BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_classz00) =
																					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_dssslzd2keywordszd2) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_dssslzd2keywordszd2)),
																					BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_locz00) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_optionalsz00) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_optionalsz00)), BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_keysz00) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_keysz00)), BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_thezd2closurezd2globalz00) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_thezd2closurezd2globalz00)),
																					BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_strengthz00) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_strengthz00)), BUNSPEC);
																				((((BgL_sfunz00_bglt)
																							COBJECT(BgL_new1116z00_1648))->
																						BgL_stackablez00) =
																					((obj_t) (((BgL_sfunz00_bglt)
																								COBJECT(((BgL_sfunz00_bglt) (
																											(BgL_funz00_bglt)
																											BgL_oldzd2sfunzd2_1594))))->
																							BgL_stackablez00)), BUNSPEC);
																				BgL_newzd2sfunzd2_1607 =
																					BgL_new1116z00_1648;
																			}
																			{	/* Inline/recursion.scm 112 */
																				obj_t BgL_newzd2kfactorzd2_1608;

																				BgL_newzd2kfactorzd2_1608 =
																					BGL_PROCEDURE_CALL1
																					(BGl_za2inliningzd2reducezd2kfactorza2z00zzengine_paramz00,
																					BINT(BgL_kfactorz00_8));
																				{	/* Inline/recursion.scm 116 */

																					{
																						obj_t BgL_ll1300z00_1610;
																						obj_t BgL_ll1301z00_1611;

																						BgL_ll1300z00_1610 =
																							BgL_newzd2argszd2_1600;
																						BgL_ll1301z00_1611 =
																							BgL_varzd2argszd2_1599;
																					BgL_zc3z04anonymousza31349ze3z87_1612:
																						if (NULLP
																							(BgL_ll1300z00_1610))
																							{	/* Inline/recursion.scm 118 */
																								((bool_t) 1);
																							}
																						else
																							{	/* Inline/recursion.scm 118 */
																								{	/* Inline/recursion.scm 119 */
																									obj_t BgL_newz00_1614;
																									obj_t BgL_oldz00_1615;

																									BgL_newz00_1614 =
																										CAR(
																										((obj_t)
																											BgL_ll1300z00_1610));
																									BgL_oldz00_1615 =
																										CAR(((obj_t)
																											BgL_ll1301z00_1611));
																									{	/* Inline/recursion.scm 119 */
																										bool_t BgL_arg1351z00_1616;

																										BgL_arg1351z00_1616 =
																											(((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_localz00_bglt)
																															BgL_oldz00_1615))))->
																											BgL_userzf3zf3);
																										((((BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															((BgL_localz00_bglt) BgL_newz00_1614))))->BgL_userzf3zf3) = ((bool_t) BgL_arg1351z00_1616), BUNSPEC);
																									}
																								}
																								{	/* Inline/recursion.scm 118 */
																									obj_t BgL_arg1352z00_1617;
																									obj_t BgL_arg1361z00_1618;

																									BgL_arg1352z00_1617 =
																										CDR(
																										((obj_t)
																											BgL_ll1300z00_1610));
																									BgL_arg1361z00_1618 =
																										CDR(((obj_t)
																											BgL_ll1301z00_1611));
																									{
																										obj_t BgL_ll1301z00_3509;
																										obj_t BgL_ll1300z00_3508;

																										BgL_ll1300z00_3508 =
																											BgL_arg1352z00_1617;
																										BgL_ll1301z00_3509 =
																											BgL_arg1361z00_1618;
																										BgL_ll1301z00_1611 =
																											BgL_ll1301z00_3509;
																										BgL_ll1300z00_1610 =
																											BgL_ll1300z00_3508;
																										goto
																											BgL_zc3z04anonymousza31349ze3z87_1612;
																									}
																								}
																							}
																					}
																					{	/* Inline/recursion.scm 124 */
																						bool_t BgL_arg1364z00_1620;

																						{	/* Inline/recursion.scm 124 */
																							bool_t BgL_test2046z00_3510;

																							{	/* Inline/recursion.scm 124 */
																								obj_t BgL_classz00_2373;

																								BgL_classz00_2373 =
																									BGl_globalz00zzast_varz00;
																								{	/* Inline/recursion.scm 124 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_2375;
																									{	/* Inline/recursion.scm 124 */
																										obj_t BgL_tmpz00_3511;

																										BgL_tmpz00_3511 =
																											((obj_t)
																											((BgL_objectz00_bglt)
																												BgL_variablez00_1592));
																										BgL_arg1807z00_2375 =
																											(BgL_objectz00_bglt)
																											(BgL_tmpz00_3511);
																									}
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Inline/recursion.scm 124 */
																											long BgL_idxz00_2381;

																											BgL_idxz00_2381 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_2375);
																											BgL_test2046z00_3510 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_2381 +
																														2L)) ==
																												BgL_classz00_2373);
																										}
																									else
																										{	/* Inline/recursion.scm 124 */
																											bool_t
																												BgL_res1939z00_2406;
																											{	/* Inline/recursion.scm 124 */
																												obj_t
																													BgL_oclassz00_2389;
																												{	/* Inline/recursion.scm 124 */
																													obj_t
																														BgL_arg1815z00_2397;
																													long
																														BgL_arg1816z00_2398;
																													BgL_arg1815z00_2397 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Inline/recursion.scm 124 */
																														long
																															BgL_arg1817z00_2399;
																														BgL_arg1817z00_2399
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_2375);
																														BgL_arg1816z00_2398
																															=
																															(BgL_arg1817z00_2399
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_2389 =
																														VECTOR_REF
																														(BgL_arg1815z00_2397,
																														BgL_arg1816z00_2398);
																												}
																												{	/* Inline/recursion.scm 124 */
																													bool_t
																														BgL__ortest_1115z00_2390;
																													BgL__ortest_1115z00_2390
																														=
																														(BgL_classz00_2373
																														==
																														BgL_oclassz00_2389);
																													if (BgL__ortest_1115z00_2390)
																														{	/* Inline/recursion.scm 124 */
																															BgL_res1939z00_2406
																																=
																																BgL__ortest_1115z00_2390;
																														}
																													else
																														{	/* Inline/recursion.scm 124 */
																															long
																																BgL_odepthz00_2391;
																															{	/* Inline/recursion.scm 124 */
																																obj_t
																																	BgL_arg1804z00_2392;
																																BgL_arg1804z00_2392
																																	=
																																	(BgL_oclassz00_2389);
																																BgL_odepthz00_2391
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_2392);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_2391))
																																{	/* Inline/recursion.scm 124 */
																																	obj_t
																																		BgL_arg1802z00_2394;
																																	{	/* Inline/recursion.scm 124 */
																																		obj_t
																																			BgL_arg1803z00_2395;
																																		BgL_arg1803z00_2395
																																			=
																																			(BgL_oclassz00_2389);
																																		BgL_arg1802z00_2394
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_2395,
																																			2L);
																																	}
																																	BgL_res1939z00_2406
																																		=
																																		(BgL_arg1802z00_2394
																																		==
																																		BgL_classz00_2373);
																																}
																															else
																																{	/* Inline/recursion.scm 124 */
																																	BgL_res1939z00_2406
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2046z00_3510 =
																												BgL_res1939z00_2406;
																										}
																								}
																							}
																							if (BgL_test2046z00_3510)
																								{	/* Inline/recursion.scm 124 */
																									BgL_arg1364z00_1620 =
																										(((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													((BgL_globalz00_bglt)
																														BgL_variablez00_1592))))->
																										BgL_userzf3zf3);
																								}
																							else
																								{	/* Inline/recursion.scm 126 */
																									bool_t BgL_test2050z00_3537;

																									{	/* Inline/recursion.scm 126 */
																										obj_t BgL_classz00_2408;

																										BgL_classz00_2408 =
																											BGl_localz00zzast_varz00;
																										{	/* Inline/recursion.scm 126 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_2410;
																											{	/* Inline/recursion.scm 126 */
																												obj_t BgL_tmpz00_3538;

																												BgL_tmpz00_3538 =
																													((obj_t)
																													((BgL_objectz00_bglt)
																														BgL_variablez00_1592));
																												BgL_arg1807z00_2410 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_3538);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Inline/recursion.scm 126 */
																													long BgL_idxz00_2416;

																													BgL_idxz00_2416 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_2410);
																													BgL_test2050z00_3537 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_2416 +
																																2L)) ==
																														BgL_classz00_2408);
																												}
																											else
																												{	/* Inline/recursion.scm 126 */
																													bool_t
																														BgL_res1940z00_2441;
																													{	/* Inline/recursion.scm 126 */
																														obj_t
																															BgL_oclassz00_2424;
																														{	/* Inline/recursion.scm 126 */
																															obj_t
																																BgL_arg1815z00_2432;
																															long
																																BgL_arg1816z00_2433;
																															BgL_arg1815z00_2432
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Inline/recursion.scm 126 */
																																long
																																	BgL_arg1817z00_2434;
																																BgL_arg1817z00_2434
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_2410);
																																BgL_arg1816z00_2433
																																	=
																																	(BgL_arg1817z00_2434
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_2424
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_2432,
																																BgL_arg1816z00_2433);
																														}
																														{	/* Inline/recursion.scm 126 */
																															bool_t
																																BgL__ortest_1115z00_2425;
																															BgL__ortest_1115z00_2425
																																=
																																(BgL_classz00_2408
																																==
																																BgL_oclassz00_2424);
																															if (BgL__ortest_1115z00_2425)
																																{	/* Inline/recursion.scm 126 */
																																	BgL_res1940z00_2441
																																		=
																																		BgL__ortest_1115z00_2425;
																																}
																															else
																																{	/* Inline/recursion.scm 126 */
																																	long
																																		BgL_odepthz00_2426;
																																	{	/* Inline/recursion.scm 126 */
																																		obj_t
																																			BgL_arg1804z00_2427;
																																		BgL_arg1804z00_2427
																																			=
																																			(BgL_oclassz00_2424);
																																		BgL_odepthz00_2426
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_2427);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_2426))
																																		{	/* Inline/recursion.scm 126 */
																																			obj_t
																																				BgL_arg1802z00_2429;
																																			{	/* Inline/recursion.scm 126 */
																																				obj_t
																																					BgL_arg1803z00_2430;
																																				BgL_arg1803z00_2430
																																					=
																																					(BgL_oclassz00_2424);
																																				BgL_arg1802z00_2429
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_2430,
																																					2L);
																																			}
																																			BgL_res1940z00_2441
																																				=
																																				(BgL_arg1802z00_2429
																																				==
																																				BgL_classz00_2408);
																																		}
																																	else
																																		{	/* Inline/recursion.scm 126 */
																																			BgL_res1940z00_2441
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test2050z00_3537 =
																														BgL_res1940z00_2441;
																												}
																										}
																									}
																									if (BgL_test2050z00_3537)
																										{	/* Inline/recursion.scm 126 */
																											BgL_arg1364z00_1620 =
																												(((BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															((BgL_localz00_bglt) BgL_variablez00_1592))))->BgL_userzf3zf3);
																										}
																									else
																										{	/* Inline/recursion.scm 126 */
																											BgL_arg1364z00_1620 =
																												((bool_t) 0);
																										}
																								}
																						}
																						((((BgL_variablez00_bglt) COBJECT(
																										((BgL_variablez00_bglt)
																											BgL_localz00_1593)))->
																								BgL_userzf3zf3) =
																							((bool_t) BgL_arg1364z00_1620),
																							BUNSPEC);
																					}
																					((((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										BgL_localz00_1593)))->
																							BgL_valuez00) =
																						((BgL_valuez00_bglt) (
																								(BgL_valuez00_bglt)
																								BgL_newzd2sfunzd2_1607)),
																						BUNSPEC);
																					if (CBOOL
																						(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																							((((BgL_sfunz00_bglt)
																										COBJECT(((BgL_sfunz00_bglt)
																												BgL_oldzd2sfunzd2_1594)))->
																									BgL_classz00),
																								CNST_TABLE_REF(1))))
																						{	/* Inline/recursion.scm 139 */
																							BFALSE;
																						}
																					else
																						{	/* Inline/recursion.scm 140 */
																							bool_t BgL_test2055z00_3575;

																							if (INTEGERP
																								(BGl_za2verboseza2z00zzengine_paramz00))
																								{	/* Inline/recursion.scm 140 */
																									BgL_test2055z00_3575 =
																										(3L <=
																										(long)
																										CINT
																										(BGl_za2verboseza2z00zzengine_paramz00));
																								}
																							else
																								{	/* Inline/recursion.scm 140 */
																									BgL_test2055z00_3575 =
																										BGl_2zc3zd3z10zz__r4_numbers_6_5z00
																										(BINT(3L),
																										BGl_za2verboseza2z00zzengine_paramz00);
																								}
																							if (BgL_test2055z00_3575)
																								{	/* Inline/recursion.scm 141 */
																									obj_t BgL_arg1375z00_1628;
																									obj_t BgL_arg1376z00_1629;

																									BgL_arg1375z00_1628 =
																										BGl_shapez00zztools_shapez00
																										(((obj_t)
																											BgL_variablez00_1592));
																									BgL_arg1376z00_1629 =
																										BGl_currentzd2functionzd2zztools_errorz00
																										();
																									{	/* Inline/recursion.scm 140 */
																										obj_t BgL_list1377z00_1630;

																										{	/* Inline/recursion.scm 140 */
																											obj_t BgL_arg1378z00_1631;

																											{	/* Inline/recursion.scm 140 */
																												obj_t
																													BgL_arg1379z00_1632;
																												{	/* Inline/recursion.scm 140 */
																													obj_t
																														BgL_arg1380z00_1633;
																													{	/* Inline/recursion.scm 140 */
																														obj_t
																															BgL_arg1408z00_1634;
																														{	/* Inline/recursion.scm 140 */
																															obj_t
																																BgL_arg1410z00_1635;
																															BgL_arg1410z00_1635
																																=
																																MAKE_YOUNG_PAIR
																																(BCHAR((
																																		(unsigned
																																			char)
																																		10)), BNIL);
																															BgL_arg1408z00_1634
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string1961z00zzinline_recursionz00,
																																BgL_arg1410z00_1635);
																														}
																														BgL_arg1380z00_1633
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1376z00_1629,
																															BgL_arg1408z00_1634);
																													}
																													BgL_arg1379z00_1632 =
																														MAKE_YOUNG_PAIR
																														(BGl_string1962z00zzinline_recursionz00,
																														BgL_arg1380z00_1633);
																												}
																												BgL_arg1378z00_1631 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1375z00_1628,
																													BgL_arg1379z00_1632);
																											}
																											BgL_list1377z00_1630 =
																												MAKE_YOUNG_PAIR
																												(BGl_string1963z00zzinline_recursionz00,
																												BgL_arg1378z00_1631);
																										}
																										BGl_verbosez00zztools_speekz00
																											(BINT(3L),
																											BgL_list1377z00_1630);
																								}}
																							else
																								{	/* Inline/recursion.scm 140 */
																									BUNSPEC;
																								}
																						}
																					BGl_inlinezd2sfunz12zc0zzinline_inlinez00
																						(((BgL_variablez00_bglt)
																							BgL_localz00_1593),
																						(long)
																						CINT(BgL_newzd2kfactorzd2_1608),
																						BgL_stackz00_9);
																					{	/* Inline/recursion.scm 150 */
																						BgL_appz00_bglt
																							BgL_newzd2callzd2_1638;
																						long BgL_callzd2siza7ez75_1639;

																						BgL_newzd2callzd2_1638 =
																							BGl_removezd2invariantzd2argsz12z12zzinline_variantz00
																							(((BgL_appz00_bglt)
																								BgL_nodez00_7));
																						BgL_callzd2siza7ez75_1639 =
																							(1L +
																							bgl_list_length(((
																										(BgL_appz00_bglt)
																										COBJECT(((BgL_appz00_bglt)
																												BgL_nodez00_7)))->
																									BgL_argsz00)));
																						{	/* Inline/recursion.scm 152 */
																							bool_t BgL_test2057z00_3603;

																							if (CBOOL
																								(BGl_za2optimzd2unrollzd2loopzf3za2zf3zzengine_paramz00))
																								{	/* Inline/recursion.scm 152 */
																									if (BGl_iszd2loopzf3z21zzinline_loopz00(BgL_variablez00_1592))
																										{	/* Inline/recursion.scm 153 */
																											if (BGl_innerzd2loopzf3z21zzinline_loopz00(BgL_variablez00_1592))
																												{	/* Inline/recursion.scm 156 */
																													BgL_test2057z00_3603 =
																														((bool_t) 0);
																												}
																											else
																												{	/* Inline/recursion.scm 156 */
																													BgL_test2057z00_3603 =
																														BGl_inlinezd2appzf3z21zzinline_appz00
																														(((BgL_variablez00_bglt) BgL_localz00_1593), (long) CINT(BgL_newzd2kfactorzd2_1608), BgL_callzd2siza7ez75_1639, BgL_stackz00_9);
																										}}
																									else
																										{	/* Inline/recursion.scm 153 */
																											BgL_test2057z00_3603 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* Inline/recursion.scm 152 */
																									BgL_test2057z00_3603 =
																										((bool_t) 0);
																								}
																							if (BgL_test2057z00_3603)
																								{	/* Inline/recursion.scm 152 */
																									return
																										BGl_unrollzd2callzd2zzinline_recursionz00
																										(BgL_variablez00_1592,
																										BgL_nodez00_7,
																										BgL_localz00_1593,
																										BgL_newzd2kfactorzd2_1608,
																										BgL_newzd2callzd2_1638,
																										BgL_stackz00_9);
																								}
																							else
																								{	/* Inline/recursion.scm 152 */
																									return
																										((obj_t)
																										BGl_plainzd2callzd2zzinline_recursionz00
																										(BgL_variablez00_1592,
																											BgL_nodez00_7,
																											BgL_localz00_1593,
																											BgL_newzd2callzd2_1638,
																											BgL_stackz00_9));
																								}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* plain-call */
	BgL_letzd2funzd2_bglt
		BGl_plainzd2callzd2zzinline_recursionz00(BgL_variablez00_bglt
		BgL_variablez00_10, BgL_nodez00_bglt BgL_nodez00_11,
		BgL_localz00_bglt BgL_localz00_12, BgL_appz00_bglt BgL_newzd2callzd2_13,
		obj_t BgL_stackz00_14)
	{
		{	/* Inline/recursion.scm 164 */
			BGl_shrinkzd2argsz12zc0zzinline_variantz00(BgL_variablez00_10);
			{	/* Inline/recursion.scm 174 */
				obj_t BgL_ilocz00_1723;

				{	/* Inline/recursion.scm 174 */
					bool_t BgL_test2061z00_3617;

					{	/* Inline/recursion.scm 174 */
						obj_t BgL_classz00_2452;

						BgL_classz00_2452 = BGl_globalz00zzast_varz00;
						{	/* Inline/recursion.scm 174 */
							BgL_objectz00_bglt BgL_arg1807z00_2454;

							{	/* Inline/recursion.scm 174 */
								obj_t BgL_tmpz00_3618;

								BgL_tmpz00_3618 =
									((obj_t) ((BgL_objectz00_bglt) BgL_variablez00_10));
								BgL_arg1807z00_2454 = (BgL_objectz00_bglt) (BgL_tmpz00_3618);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Inline/recursion.scm 174 */
									long BgL_idxz00_2460;

									BgL_idxz00_2460 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2454);
									BgL_test2061z00_3617 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2460 + 2L)) == BgL_classz00_2452);
								}
							else
								{	/* Inline/recursion.scm 174 */
									bool_t BgL_res1941z00_2485;

									{	/* Inline/recursion.scm 174 */
										obj_t BgL_oclassz00_2468;

										{	/* Inline/recursion.scm 174 */
											obj_t BgL_arg1815z00_2476;
											long BgL_arg1816z00_2477;

											BgL_arg1815z00_2476 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Inline/recursion.scm 174 */
												long BgL_arg1817z00_2478;

												BgL_arg1817z00_2478 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2454);
												BgL_arg1816z00_2477 =
													(BgL_arg1817z00_2478 - OBJECT_TYPE);
											}
											BgL_oclassz00_2468 =
												VECTOR_REF(BgL_arg1815z00_2476, BgL_arg1816z00_2477);
										}
										{	/* Inline/recursion.scm 174 */
											bool_t BgL__ortest_1115z00_2469;

											BgL__ortest_1115z00_2469 =
												(BgL_classz00_2452 == BgL_oclassz00_2468);
											if (BgL__ortest_1115z00_2469)
												{	/* Inline/recursion.scm 174 */
													BgL_res1941z00_2485 = BgL__ortest_1115z00_2469;
												}
											else
												{	/* Inline/recursion.scm 174 */
													long BgL_odepthz00_2470;

													{	/* Inline/recursion.scm 174 */
														obj_t BgL_arg1804z00_2471;

														BgL_arg1804z00_2471 = (BgL_oclassz00_2468);
														BgL_odepthz00_2470 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2471);
													}
													if ((2L < BgL_odepthz00_2470))
														{	/* Inline/recursion.scm 174 */
															obj_t BgL_arg1802z00_2473;

															{	/* Inline/recursion.scm 174 */
																obj_t BgL_arg1803z00_2474;

																BgL_arg1803z00_2474 = (BgL_oclassz00_2468);
																BgL_arg1802z00_2473 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2474,
																	2L);
															}
															BgL_res1941z00_2485 =
																(BgL_arg1802z00_2473 == BgL_classz00_2452);
														}
													else
														{	/* Inline/recursion.scm 174 */
															BgL_res1941z00_2485 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2061z00_3617 = BgL_res1941z00_2485;
								}
						}
					}
					if (BgL_test2061z00_3617)
						{	/* Inline/recursion.scm 175 */
							bool_t BgL_test2065z00_3641;

							{	/* Inline/recursion.scm 175 */
								obj_t BgL_arg1591z00_1734;

								BgL_arg1591z00_1734 =
									(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_variablez00_10)))->
									BgL_modulez00);
								BgL_test2065z00_3641 =
									(BgL_arg1591z00_1734 ==
									BGl_za2moduleza2z00zzmodule_modulez00);
							}
							if (BgL_test2065z00_3641)
								{	/* Inline/recursion.scm 175 */
									BgL_ilocz00_1723 = BFALSE;
								}
							else
								{	/* Inline/recursion.scm 175 */
									BgL_ilocz00_1723 =
										(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_11))->BgL_locz00);
								}
						}
					else
						{	/* Inline/recursion.scm 174 */
							BgL_ilocz00_1723 = BFALSE;
						}
				}
				{	/* Inline/recursion.scm 177 */
					BgL_letzd2funzd2_bglt BgL_new1134z00_1724;

					{	/* Inline/recursion.scm 178 */
						BgL_letzd2funzd2_bglt BgL_new1133z00_1730;

						BgL_new1133z00_1730 =
							((BgL_letzd2funzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_letzd2funzd2_bgl))));
						{	/* Inline/recursion.scm 178 */
							long BgL_arg1589z00_1731;

							BgL_arg1589z00_1731 =
								BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1133z00_1730),
								BgL_arg1589z00_1731);
						}
						{	/* Inline/recursion.scm 178 */
							BgL_objectz00_bglt BgL_tmpz00_3650;

							BgL_tmpz00_3650 = ((BgL_objectz00_bglt) BgL_new1133z00_1730);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3650, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1133z00_1730);
						BgL_new1134z00_1724 = BgL_new1133z00_1730;
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1134z00_1724)))->BgL_locz00) =
						((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_11))->
								BgL_locz00)), BUNSPEC);
					((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_new1134z00_1724)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_11))->
								BgL_typez00)), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1134z00_1724)))->BgL_sidezd2effectzd2) =
						((obj_t) BUNSPEC), BUNSPEC);
					((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
										BgL_new1134z00_1724)))->BgL_keyz00) =
						((obj_t) BINT(-1L)), BUNSPEC);
					{
						obj_t BgL_auxz00_3665;

						{	/* Inline/recursion.scm 180 */
							obj_t BgL_list1577z00_1725;

							BgL_list1577z00_1725 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_localz00_12), BNIL);
							BgL_auxz00_3665 = BgL_list1577z00_1725;
						}
						((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1134z00_1724))->
								BgL_localsz00) = ((obj_t) BgL_auxz00_3665), BUNSPEC);
					}
					{
						BgL_nodez00_bglt BgL_auxz00_3669;

						{	/* Inline/recursion.scm 181 */
							obj_t BgL_arg1584z00_1726;
							obj_t BgL_arg1585z00_1727;

							{	/* Inline/recursion.scm 181 */
								obj_t BgL_list1586z00_1728;

								BgL_list1586z00_1728 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_variablez00_10), BNIL);
								BgL_arg1584z00_1726 = BgL_list1586z00_1728;
							}
							{	/* Inline/recursion.scm 181 */
								obj_t BgL_list1587z00_1729;

								BgL_list1587z00_1729 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_localz00_12), BNIL);
								BgL_arg1585z00_1727 = BgL_list1587z00_1729;
							}
							BgL_auxz00_3669 =
								BGl_alphatiza7eza7zzast_alphatiza7eza7(BgL_arg1584z00_1726,
								BgL_arg1585z00_1727, BgL_ilocz00_1723,
								((BgL_nodez00_bglt) BgL_newzd2callzd2_13));
						}
						((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1134z00_1724))->
								BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_3669), BUNSPEC);
					}
					return BgL_new1134z00_1724;
				}
			}
		}

	}



/* unroll-call */
	obj_t BGl_unrollzd2callzd2zzinline_recursionz00(BgL_variablez00_bglt
		BgL_variablez00_15, BgL_nodez00_bglt BgL_nodez00_16,
		BgL_localz00_bglt BgL_localz00_17, obj_t BgL_kfactorz00_18,
		BgL_appz00_bglt BgL_callz00_19, obj_t BgL_stackz00_20)
	{
		{	/* Inline/recursion.scm 187 */
			{	/* Inline/recursion.scm 193 */
				obj_t BgL_ilocz00_1735;

				{	/* Inline/recursion.scm 193 */
					bool_t BgL_test2066z00_3677;

					{	/* Inline/recursion.scm 193 */
						obj_t BgL_classz00_2497;

						BgL_classz00_2497 = BGl_globalz00zzast_varz00;
						{	/* Inline/recursion.scm 193 */
							BgL_objectz00_bglt BgL_arg1807z00_2499;

							{	/* Inline/recursion.scm 193 */
								obj_t BgL_tmpz00_3678;

								BgL_tmpz00_3678 =
									((obj_t) ((BgL_objectz00_bglt) BgL_variablez00_15));
								BgL_arg1807z00_2499 = (BgL_objectz00_bglt) (BgL_tmpz00_3678);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Inline/recursion.scm 193 */
									long BgL_idxz00_2505;

									BgL_idxz00_2505 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2499);
									BgL_test2066z00_3677 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2505 + 2L)) == BgL_classz00_2497);
								}
							else
								{	/* Inline/recursion.scm 193 */
									bool_t BgL_res1945z00_2530;

									{	/* Inline/recursion.scm 193 */
										obj_t BgL_oclassz00_2513;

										{	/* Inline/recursion.scm 193 */
											obj_t BgL_arg1815z00_2521;
											long BgL_arg1816z00_2522;

											BgL_arg1815z00_2521 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Inline/recursion.scm 193 */
												long BgL_arg1817z00_2523;

												BgL_arg1817z00_2523 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2499);
												BgL_arg1816z00_2522 =
													(BgL_arg1817z00_2523 - OBJECT_TYPE);
											}
											BgL_oclassz00_2513 =
												VECTOR_REF(BgL_arg1815z00_2521, BgL_arg1816z00_2522);
										}
										{	/* Inline/recursion.scm 193 */
											bool_t BgL__ortest_1115z00_2514;

											BgL__ortest_1115z00_2514 =
												(BgL_classz00_2497 == BgL_oclassz00_2513);
											if (BgL__ortest_1115z00_2514)
												{	/* Inline/recursion.scm 193 */
													BgL_res1945z00_2530 = BgL__ortest_1115z00_2514;
												}
											else
												{	/* Inline/recursion.scm 193 */
													long BgL_odepthz00_2515;

													{	/* Inline/recursion.scm 193 */
														obj_t BgL_arg1804z00_2516;

														BgL_arg1804z00_2516 = (BgL_oclassz00_2513);
														BgL_odepthz00_2515 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2516);
													}
													if ((2L < BgL_odepthz00_2515))
														{	/* Inline/recursion.scm 193 */
															obj_t BgL_arg1802z00_2518;

															{	/* Inline/recursion.scm 193 */
																obj_t BgL_arg1803z00_2519;

																BgL_arg1803z00_2519 = (BgL_oclassz00_2513);
																BgL_arg1802z00_2518 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2519,
																	2L);
															}
															BgL_res1945z00_2530 =
																(BgL_arg1802z00_2518 == BgL_classz00_2497);
														}
													else
														{	/* Inline/recursion.scm 193 */
															BgL_res1945z00_2530 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2066z00_3677 = BgL_res1945z00_2530;
								}
						}
					}
					if (BgL_test2066z00_3677)
						{	/* Inline/recursion.scm 194 */
							bool_t BgL_test2070z00_3701;

							{	/* Inline/recursion.scm 194 */
								obj_t BgL_arg1615z00_1758;

								BgL_arg1615z00_1758 =
									(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_variablez00_15)))->
									BgL_modulez00);
								BgL_test2070z00_3701 =
									(BgL_arg1615z00_1758 ==
									BGl_za2moduleza2z00zzmodule_modulez00);
							}
							if (BgL_test2070z00_3701)
								{	/* Inline/recursion.scm 194 */
									BgL_ilocz00_1735 = BFALSE;
								}
							else
								{	/* Inline/recursion.scm 194 */
									BgL_ilocz00_1735 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_callz00_19)))->BgL_locz00);
								}
						}
					else
						{	/* Inline/recursion.scm 193 */
							BgL_ilocz00_1735 = BFALSE;
						}
				}
				{	/* Inline/recursion.scm 193 */
					BgL_nodez00_bglt BgL_newzd2callzd2_1736;

					{	/* Inline/recursion.scm 196 */
						obj_t BgL_arg1609z00_1752;
						obj_t BgL_arg1611z00_1753;

						{	/* Inline/recursion.scm 196 */
							obj_t BgL_list1612z00_1754;

							BgL_list1612z00_1754 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_variablez00_15), BNIL);
							BgL_arg1609z00_1752 = BgL_list1612z00_1754;
						}
						{	/* Inline/recursion.scm 196 */
							obj_t BgL_list1613z00_1755;

							BgL_list1613z00_1755 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_localz00_17), BNIL);
							BgL_arg1611z00_1753 = BgL_list1613z00_1755;
						}
						BgL_newzd2callzd2_1736 =
							BGl_alphatiza7eza7zzast_alphatiza7eza7(BgL_arg1609z00_1752,
							BgL_arg1611z00_1753, BgL_ilocz00_1735,
							((BgL_nodez00_bglt) BgL_callz00_19));
					}
					{	/* Inline/recursion.scm 197 */
						BgL_nodez00_bglt BgL_newzd2bodyzd2_1738;

						{	/* Inline/recursion.scm 200 */
							obj_t BgL_arg1605z00_1750;

							BgL_arg1605z00_1750 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_localz00_17), BgL_stackz00_20);
							BgL_newzd2bodyzd2_1738 =
								BGl_inlinezd2appzd2simplez00zzinline_simplez00
								(BgL_newzd2callzd2_1736, (long) CINT(BgL_kfactorz00_18),
								BgL_arg1605z00_1750, BGl_string1964z00zzinline_recursionz00);
						}
						{	/* Inline/recursion.scm 198 */

							BGl_shrinkzd2argsz12zc0zzinline_variantz00(BgL_variablez00_15);
							{	/* Inline/recursion.scm 209 */
								obj_t BgL__z00_1739;

								{	/* Inline/recursion.scm 213 */
									BgL_nodez00_bglt BgL_arg1593z00_1741;

									{	/* Inline/recursion.scm 213 */
										obj_t BgL_zc3z04anonymousza31595ze3z87_2873;

										BgL_zc3z04anonymousza31595ze3z87_2873 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31595ze3ze5zzinline_recursionz00,
											(int) (1L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_2873,
											(int) (0L), ((obj_t) BgL_localz00_17));
										BgL_arg1593z00_1741 =
											BGl_nestzd2loopz12zc0zzinline_loopz00
											(BgL_newzd2bodyzd2_1738, BgL_localz00_17,
											BgL_zc3z04anonymousza31595ze3z87_2873);
									}
									BgL__z00_1739 =
										BGl_nodezd2csez12zc0zzreduce_csez00(BgL_arg1593z00_1741,
										BNIL);
								}
								{	/* Inline/recursion.scm 210 */
									obj_t BgL_nodez00_1740;

									{	/* Inline/recursion.scm 210 */
										obj_t BgL_tmpz00_2544;

										{	/* Inline/recursion.scm 210 */
											int BgL_tmpz00_3726;

											BgL_tmpz00_3726 = (int) (1L);
											BgL_tmpz00_2544 = BGL_MVALUES_VAL(BgL_tmpz00_3726);
										}
										{	/* Inline/recursion.scm 210 */
											int BgL_tmpz00_3729;

											BgL_tmpz00_3729 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_3729, BUNSPEC);
										}
										BgL_nodez00_1740 = BgL_tmpz00_2544;
									}
									return BgL_nodez00_1740;
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1595> */
	obj_t BGl_z62zc3z04anonymousza31595ze3ze5zzinline_recursionz00(obj_t
		BgL_envz00_2874, obj_t BgL_nodez00_2876)
	{
		{	/* Inline/recursion.scm 212 */
			{	/* Inline/recursion.scm 213 */
				BgL_localz00_bglt BgL_localz00_2875;

				BgL_localz00_2875 =
					((BgL_localz00_bglt) PROCEDURE_REF(BgL_envz00_2874, (int) (0L)));
				{
					BgL_letzd2funzd2_bglt BgL_auxz00_3735;

					{	/* Inline/recursion.scm 213 */
						BgL_letzd2funzd2_bglt BgL_new1138z00_3009;

						{	/* Inline/recursion.scm 214 */
							BgL_letzd2funzd2_bglt BgL_new1137z00_3010;

							BgL_new1137z00_3010 =
								((BgL_letzd2funzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_letzd2funzd2_bgl))));
							{	/* Inline/recursion.scm 214 */
								long BgL_arg1602z00_3011;

								BgL_arg1602z00_3011 =
									BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1137z00_3010),
									BgL_arg1602z00_3011);
							}
							{	/* Inline/recursion.scm 214 */
								BgL_objectz00_bglt BgL_tmpz00_3740;

								BgL_tmpz00_3740 = ((BgL_objectz00_bglt) BgL_new1137z00_3010);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3740, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1137z00_3010);
							BgL_new1138z00_3009 = BgL_new1137z00_3010;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1138z00_3009)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_nodez00_2876)))->BgL_locz00)), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1138z00_3009)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) BgL_nodez00_2876)))->
									BgL_typez00)), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1138z00_3009)))->BgL_sidezd2effectzd2) =
							((obj_t) BUNSPEC), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1138z00_3009)))->BgL_keyz00) =
							((obj_t) BINT(-1L)), BUNSPEC);
						{
							obj_t BgL_auxz00_3757;

							{	/* Inline/recursion.scm 217 */
								obj_t BgL_list1596z00_3012;

								BgL_list1596z00_3012 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_localz00_2875), BNIL);
								BgL_auxz00_3757 = BgL_list1596z00_3012;
							}
							((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1138z00_3009))->
									BgL_localsz00) = ((obj_t) BgL_auxz00_3757), BUNSPEC);
						}
						((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1138z00_3009))->
								BgL_bodyz00) =
							((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_nodez00_2876)),
							BUNSPEC);
						BgL_auxz00_3735 = BgL_new1138z00_3009;
					}
					return ((obj_t) BgL_auxz00_3735);
				}
			}
		}

	}



/* is-recursive? */
	BGL_EXPORTED_DEF bool_t
		BGl_iszd2recursivezf3z21zzinline_recursionz00(BgL_variablez00_bglt
		BgL_varz00_21)
	{
		{	/* Inline/recursion.scm 224 */
			{	/* Inline/recursion.scm 226 */
				BgL_valuez00_bglt BgL_sfunz00_1759;

				BgL_sfunz00_1759 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_21))->BgL_valuez00);
				{	/* Inline/recursion.scm 227 */
					bool_t BgL_test2071z00_3765;

					{	/* Inline/recursion.scm 227 */
						obj_t BgL_classz00_2546;

						BgL_classz00_2546 = BGl_isfunz00zzinline_inlinez00;
						{	/* Inline/recursion.scm 227 */
							BgL_objectz00_bglt BgL_arg1807z00_2548;

							{	/* Inline/recursion.scm 227 */
								obj_t BgL_tmpz00_3766;

								BgL_tmpz00_3766 =
									((obj_t) ((BgL_objectz00_bglt) BgL_sfunz00_1759));
								BgL_arg1807z00_2548 = (BgL_objectz00_bglt) (BgL_tmpz00_3766);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Inline/recursion.scm 227 */
									long BgL_idxz00_2554;

									BgL_idxz00_2554 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2548);
									BgL_test2071z00_3765 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2554 + 4L)) == BgL_classz00_2546);
								}
							else
								{	/* Inline/recursion.scm 227 */
									bool_t BgL_res1949z00_2579;

									{	/* Inline/recursion.scm 227 */
										obj_t BgL_oclassz00_2562;

										{	/* Inline/recursion.scm 227 */
											obj_t BgL_arg1815z00_2570;
											long BgL_arg1816z00_2571;

											BgL_arg1815z00_2570 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Inline/recursion.scm 227 */
												long BgL_arg1817z00_2572;

												BgL_arg1817z00_2572 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2548);
												BgL_arg1816z00_2571 =
													(BgL_arg1817z00_2572 - OBJECT_TYPE);
											}
											BgL_oclassz00_2562 =
												VECTOR_REF(BgL_arg1815z00_2570, BgL_arg1816z00_2571);
										}
										{	/* Inline/recursion.scm 227 */
											bool_t BgL__ortest_1115z00_2563;

											BgL__ortest_1115z00_2563 =
												(BgL_classz00_2546 == BgL_oclassz00_2562);
											if (BgL__ortest_1115z00_2563)
												{	/* Inline/recursion.scm 227 */
													BgL_res1949z00_2579 = BgL__ortest_1115z00_2563;
												}
											else
												{	/* Inline/recursion.scm 227 */
													long BgL_odepthz00_2564;

													{	/* Inline/recursion.scm 227 */
														obj_t BgL_arg1804z00_2565;

														BgL_arg1804z00_2565 = (BgL_oclassz00_2562);
														BgL_odepthz00_2564 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2565);
													}
													if ((4L < BgL_odepthz00_2564))
														{	/* Inline/recursion.scm 227 */
															obj_t BgL_arg1802z00_2567;

															{	/* Inline/recursion.scm 227 */
																obj_t BgL_arg1803z00_2568;

																BgL_arg1803z00_2568 = (BgL_oclassz00_2562);
																BgL_arg1802z00_2567 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2568,
																	4L);
															}
															BgL_res1949z00_2579 =
																(BgL_arg1802z00_2567 == BgL_classz00_2546);
														}
													else
														{	/* Inline/recursion.scm 227 */
															BgL_res1949z00_2579 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2071z00_3765 = BgL_res1949z00_2579;
								}
						}
					}
					if (BgL_test2071z00_3765)
						{	/* Inline/recursion.scm 236 */
							bool_t BgL_test2075z00_3789;

							{	/* Inline/recursion.scm 236 */
								obj_t BgL_arg1646z00_1770;

								{
									BgL_isfunz00_bglt BgL_auxz00_3790;

									{
										obj_t BgL_auxz00_3791;

										{	/* Inline/recursion.scm 236 */
											BgL_objectz00_bglt BgL_tmpz00_3792;

											BgL_tmpz00_3792 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_sfunz00_1759));
											BgL_auxz00_3791 = BGL_OBJECT_WIDENING(BgL_tmpz00_3792);
										}
										BgL_auxz00_3790 = ((BgL_isfunz00_bglt) BgL_auxz00_3791);
									}
									BgL_arg1646z00_1770 =
										(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3790))->
										BgL_recursivezd2callszd2);
								}
								BgL_test2075z00_3789 = PAIRP(BgL_arg1646z00_1770);
							}
							if (BgL_test2075z00_3789)
								{	/* Inline/recursion.scm 236 */
									return ((bool_t) 1);
								}
							else
								{	/* Inline/recursion.scm 238 */
									bool_t BgL_test2076z00_3799;

									{	/* Inline/recursion.scm 238 */
										obj_t BgL_arg1642z00_1769;

										{
											BgL_isfunz00_bglt BgL_auxz00_3800;

											{
												obj_t BgL_auxz00_3801;

												{	/* Inline/recursion.scm 238 */
													BgL_objectz00_bglt BgL_tmpz00_3802;

													BgL_tmpz00_3802 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_sfunz00_1759));
													BgL_auxz00_3801 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3802);
												}
												BgL_auxz00_3800 = ((BgL_isfunz00_bglt) BgL_auxz00_3801);
											}
											BgL_arg1642z00_1769 =
												(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3800))->
												BgL_recursivezd2callszd2);
										}
										BgL_test2076z00_3799 = NULLP(BgL_arg1642z00_1769);
									}
									if (BgL_test2076z00_3799)
										{	/* Inline/recursion.scm 238 */
											return ((bool_t) 0);
										}
									else
										{	/* Inline/recursion.scm 241 */
											obj_t BgL_trecz00_1765;

											BgL_trecz00_1765 = MAKE_YOUNG_CELL(BTRUE);
											{	/* Inline/recursion.scm 241 */
												obj_t BgL_callsz00_1766;

												{	/* Inline/recursion.scm 243 */
													BgL_nodez00_bglt BgL_arg1630z00_1768;

													{
														BgL_isfunz00_bglt BgL_auxz00_3810;

														{
															obj_t BgL_auxz00_3811;

															{	/* Inline/recursion.scm 243 */
																BgL_objectz00_bglt BgL_tmpz00_3812;

																BgL_tmpz00_3812 =
																	((BgL_objectz00_bglt)
																	((BgL_sfunz00_bglt) BgL_sfunz00_1759));
																BgL_auxz00_3811 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3812);
															}
															BgL_auxz00_3810 =
																((BgL_isfunz00_bglt) BgL_auxz00_3811);
														}
														BgL_arg1630z00_1768 =
															(((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3810))->
															BgL_originalzd2bodyzd2);
													}
													BgL_callsz00_1766 =
														BGl_findzd2recursivezd2callsz00zzinline_recursionz00
														(BgL_arg1630z00_1768, BgL_varz00_21, BTRUE,
														BgL_trecz00_1765);
												}
												{	/* Inline/recursion.scm 242 */

													{	/* Inline/recursion.scm 244 */
														obj_t BgL_arg1629z00_1767;

														BgL_arg1629z00_1767 = CELL_REF(BgL_trecz00_1765);
														{	/* Inline/recursion.scm 244 */
															bool_t BgL_vz00_2588;

															BgL_vz00_2588 = CBOOL(BgL_arg1629z00_1767);
															{
																BgL_isfunz00_bglt BgL_auxz00_3821;

																{
																	obj_t BgL_auxz00_3822;

																	{	/* Inline/recursion.scm 244 */
																		BgL_objectz00_bglt BgL_tmpz00_3823;

																		BgL_tmpz00_3823 =
																			((BgL_objectz00_bglt)
																			((BgL_sfunz00_bglt) BgL_sfunz00_1759));
																		BgL_auxz00_3822 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3823);
																	}
																	BgL_auxz00_3821 =
																		((BgL_isfunz00_bglt) BgL_auxz00_3822);
																}
																((((BgL_isfunz00_bglt)
																			COBJECT(BgL_auxz00_3821))->
																		BgL_tailrecz00) =
																	((bool_t) BgL_vz00_2588), BUNSPEC);
															}
														}
													}
													{
														BgL_isfunz00_bglt BgL_auxz00_3829;

														{
															obj_t BgL_auxz00_3830;

															{	/* Inline/recursion.scm 245 */
																BgL_objectz00_bglt BgL_tmpz00_3831;

																BgL_tmpz00_3831 =
																	((BgL_objectz00_bglt)
																	((BgL_sfunz00_bglt) BgL_sfunz00_1759));
																BgL_auxz00_3830 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3831);
															}
															BgL_auxz00_3829 =
																((BgL_isfunz00_bglt) BgL_auxz00_3830);
														}
														((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3829))->
																BgL_recursivezd2callszd2) =
															((obj_t) BgL_callsz00_1766), BUNSPEC);
													}
													return PAIRP(BgL_callsz00_1766);
												}
											}
										}
								}
						}
					else
						{	/* Inline/recursion.scm 228 */
							obj_t BgL_trecz00_1771;

							BgL_trecz00_1771 = MAKE_YOUNG_CELL(BTRUE);
							{	/* Inline/recursion.scm 228 */
								obj_t BgL_callsz00_1772;

								{	/* Inline/recursion.scm 229 */
									obj_t BgL_arg1651z00_1778;

									BgL_arg1651z00_1778 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_sfunz00_1759)))->BgL_bodyz00);
									BgL_callsz00_1772 =
										BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
										((BgL_nodez00_bglt) BgL_arg1651z00_1778), BgL_varz00_21,
										BTRUE, BgL_trecz00_1771);
								}
								{	/* Inline/recursion.scm 229 */

									{	/* Inline/recursion.scm 230 */
										BgL_isfunz00_bglt BgL_wide1141z00_1775;

										BgL_wide1141z00_1775 =
											((BgL_isfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_isfunz00_bgl))));
										{	/* Inline/recursion.scm 230 */
											obj_t BgL_auxz00_3848;
											BgL_objectz00_bglt BgL_tmpz00_3844;

											BgL_auxz00_3848 = ((obj_t) BgL_wide1141z00_1775);
											BgL_tmpz00_3844 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt)
													((BgL_sfunz00_bglt) BgL_sfunz00_1759)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3844, BgL_auxz00_3848);
										}
										((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_sfunz00_1759)));
										{	/* Inline/recursion.scm 230 */
											long BgL_arg1650z00_1776;

											BgL_arg1650z00_1776 =
												BGL_CLASS_NUM(BGl_isfunz00zzinline_inlinez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_sfunz00_1759))),
												BgL_arg1650z00_1776);
										}
										((BgL_sfunz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_sfunz00_1759)));
									}
									{
										BgL_isfunz00_bglt BgL_auxz00_3862;

										{
											obj_t BgL_auxz00_3863;

											{	/* Inline/recursion.scm 232 */
												BgL_objectz00_bglt BgL_tmpz00_3864;

												BgL_tmpz00_3864 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_sfunz00_1759)));
												BgL_auxz00_3863 = BGL_OBJECT_WIDENING(BgL_tmpz00_3864);
											}
											BgL_auxz00_3862 = ((BgL_isfunz00_bglt) BgL_auxz00_3863);
										}
										((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3862))->
												BgL_originalzd2bodyzd2) =
											((BgL_nodez00_bglt) ((BgL_nodez00_bglt) ((
															(BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
																	BgL_sfunz00_1759)))->BgL_bodyz00))), BUNSPEC);
									}
									{
										BgL_isfunz00_bglt BgL_auxz00_3874;

										{
											obj_t BgL_auxz00_3875;

											{	/* Inline/recursion.scm 233 */
												BgL_objectz00_bglt BgL_tmpz00_3876;

												BgL_tmpz00_3876 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_sfunz00_1759)));
												BgL_auxz00_3875 = BGL_OBJECT_WIDENING(BgL_tmpz00_3876);
											}
											BgL_auxz00_3874 = ((BgL_isfunz00_bglt) BgL_auxz00_3875);
										}
										((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3874))->
												BgL_recursivezd2callszd2) =
											((obj_t) BgL_callsz00_1772), BUNSPEC);
									}
									{
										BgL_isfunz00_bglt BgL_auxz00_3883;

										{
											obj_t BgL_auxz00_3884;

											{	/* Inline/recursion.scm 231 */
												BgL_objectz00_bglt BgL_tmpz00_3885;

												BgL_tmpz00_3885 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_sfunz00_1759)));
												BgL_auxz00_3884 = BGL_OBJECT_WIDENING(BgL_tmpz00_3885);
											}
											BgL_auxz00_3883 = ((BgL_isfunz00_bglt) BgL_auxz00_3884);
										}
										((((BgL_isfunz00_bglt) COBJECT(BgL_auxz00_3883))->
												BgL_tailrecz00) =
											((bool_t) CBOOL(CELL_REF(BgL_trecz00_1771))), BUNSPEC);
									}
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_sfunz00_1759));
									return PAIRP(BgL_callsz00_1772);
								}
							}
						}
				}
			}
		}

	}



/* &is-recursive? */
	obj_t BGl_z62iszd2recursivezf3z43zzinline_recursionz00(obj_t BgL_envz00_2877,
		obj_t BgL_varz00_2878)
	{
		{	/* Inline/recursion.scm 224 */
			return
				BBOOL(BGl_iszd2recursivezf3z21zzinline_recursionz00(
					((BgL_variablez00_bglt) BgL_varz00_2878)));
		}

	}



/* find-recursive-calls* */
	obj_t BGl_findzd2recursivezd2callsza2za2zzinline_recursionz00(obj_t
		BgL_nodeza2za2_98, obj_t BgL_varz00_99, obj_t BgL_tailz00_100,
		obj_t BgL_tresz00_101)
	{
		{	/* Inline/recursion.scm 411 */
			{
				obj_t BgL_nodeza2za2_1781;
				obj_t BgL_callsz00_1782;

				BgL_nodeza2za2_1781 = BgL_nodeza2za2_98;
				BgL_callsz00_1782 = BNIL;
			BgL_zc3z04anonymousza31652ze3z87_1783:
				if (NULLP(BgL_nodeza2za2_1781))
					{	/* Inline/recursion.scm 415 */
						return BgL_callsz00_1782;
					}
				else
					{	/* Inline/recursion.scm 415 */
						if (NULLP(CDR(((obj_t) BgL_nodeza2za2_1781))))
							{	/* Inline/recursion.scm 418 */
								obj_t BgL_arg1661z00_1787;

								{	/* Inline/recursion.scm 418 */
									obj_t BgL_arg1663z00_1788;

									BgL_arg1663z00_1788 = CAR(((obj_t) BgL_nodeza2za2_1781));
									BgL_arg1661z00_1787 =
										BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
										((BgL_nodez00_bglt) BgL_arg1663z00_1788),
										((BgL_variablez00_bglt) BgL_varz00_99), BgL_tailz00_100,
										BgL_tresz00_101);
								}
								return
									BGl_appendzd221011zd2zzinline_recursionz00
									(BgL_arg1661z00_1787, BgL_callsz00_1782);
							}
						else
							{	/* Inline/recursion.scm 420 */
								obj_t BgL_arg1675z00_1789;
								obj_t BgL_arg1678z00_1790;

								BgL_arg1675z00_1789 = CDR(((obj_t) BgL_nodeza2za2_1781));
								{	/* Inline/recursion.scm 421 */
									obj_t BgL_arg1681z00_1791;

									{	/* Inline/recursion.scm 421 */
										obj_t BgL_arg1688z00_1792;

										BgL_arg1688z00_1792 = CAR(((obj_t) BgL_nodeza2za2_1781));
										BgL_arg1681z00_1791 =
											BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
											((BgL_nodez00_bglt) BgL_arg1688z00_1792),
											((BgL_variablez00_bglt) BgL_varz00_99), BFALSE,
											BgL_tresz00_101);
									}
									BgL_arg1678z00_1790 =
										BGl_appendzd221011zd2zzinline_recursionz00
										(BgL_arg1681z00_1791, BgL_callsz00_1782);
								}
								{
									obj_t BgL_callsz00_3921;
									obj_t BgL_nodeza2za2_3920;

									BgL_nodeza2za2_3920 = BgL_arg1675z00_1789;
									BgL_callsz00_3921 = BgL_arg1678z00_1790;
									BgL_callsz00_1782 = BgL_callsz00_3921;
									BgL_nodeza2za2_1781 = BgL_nodeza2za2_3920;
									goto BgL_zc3z04anonymousza31652ze3z87_1783;
								}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinline_recursionz00(void)
	{
		{	/* Inline/recursion.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinline_recursionz00(void)
	{
		{	/* Inline/recursion.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_proc1965z00zzinline_recursionz00, BGl_nodez00zzast_nodez00,
				BGl_string1966z00zzinline_recursionz00);
		}

	}



/* &find-recursive-calls1303 */
	obj_t BGl_z62findzd2recursivezd2calls1303z62zzinline_recursionz00(obj_t
		BgL_envz00_2880, obj_t BgL_nodez00_2881, obj_t BgL_varz00_2882,
		obj_t BgL_tailz00_2883, obj_t BgL_tresz00_2884)
	{
		{	/* Inline/recursion.scm 251 */
			return BNIL;
		}

	}



/* find-recursive-calls */
	obj_t BGl_findzd2recursivezd2callsz00zzinline_recursionz00(BgL_nodez00_bglt
		BgL_nodez00_22, BgL_variablez00_bglt BgL_varz00_23, obj_t BgL_tailz00_24,
		obj_t BgL_tresz00_25)
	{
		{	/* Inline/recursion.scm 251 */
			{	/* Inline/recursion.scm 251 */
				obj_t BgL_method1304z00_1802;

				{	/* Inline/recursion.scm 251 */
					obj_t BgL_res1954z00_2636;

					{	/* Inline/recursion.scm 251 */
						long BgL_objzd2classzd2numz00_2607;

						BgL_objzd2classzd2numz00_2607 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_22));
						{	/* Inline/recursion.scm 251 */
							obj_t BgL_arg1811z00_2608;

							BgL_arg1811z00_2608 =
								PROCEDURE_REF
								(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
								(int) (1L));
							{	/* Inline/recursion.scm 251 */
								int BgL_offsetz00_2611;

								BgL_offsetz00_2611 = (int) (BgL_objzd2classzd2numz00_2607);
								{	/* Inline/recursion.scm 251 */
									long BgL_offsetz00_2612;

									BgL_offsetz00_2612 =
										((long) (BgL_offsetz00_2611) - OBJECT_TYPE);
									{	/* Inline/recursion.scm 251 */
										long BgL_modz00_2613;

										BgL_modz00_2613 =
											(BgL_offsetz00_2612 >> (int) ((long) ((int) (4L))));
										{	/* Inline/recursion.scm 251 */
											long BgL_restz00_2615;

											BgL_restz00_2615 =
												(BgL_offsetz00_2612 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Inline/recursion.scm 251 */

												{	/* Inline/recursion.scm 251 */
													obj_t BgL_bucketz00_2617;

													BgL_bucketz00_2617 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2608), BgL_modz00_2613);
													BgL_res1954z00_2636 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2617), BgL_restz00_2615);
					}}}}}}}}
					BgL_method1304z00_1802 = BgL_res1954z00_2636;
				}
				return
					BGL_PROCEDURE_CALL4(BgL_method1304z00_1802,
					((obj_t) BgL_nodez00_22),
					((obj_t) BgL_varz00_23), BgL_tailz00_24, BgL_tresz00_25);
			}
		}

	}



/* &find-recursive-calls */
	obj_t BGl_z62findzd2recursivezd2callsz62zzinline_recursionz00(obj_t
		BgL_envz00_2885, obj_t BgL_nodez00_2886, obj_t BgL_varz00_2887,
		obj_t BgL_tailz00_2888, obj_t BgL_tresz00_2889)
	{
		{	/* Inline/recursion.scm 251 */
			return
				BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
				((BgL_nodez00_bglt) BgL_nodez00_2886),
				((BgL_variablez00_bglt) BgL_varz00_2887), BgL_tailz00_2888,
				BgL_tresz00_2889);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinline_recursionz00(void)
	{
		{	/* Inline/recursion.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1967z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_syncz00zzast_nodez00, BGl_proc1969z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_appz00zzast_nodez00, BGl_proc1970z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1971z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1972z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_externz00zzast_nodez00, BGl_proc1973z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_castz00zzast_nodez00, BGl_proc1974z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_setqz00zzast_nodez00, BGl_proc1975z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1976z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_failz00zzast_nodez00, BGl_proc1977z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_switchz00zzast_nodez00, BGl_proc1978z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1979z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1980z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1981z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1982z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1983z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1984z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_findzd2recursivezd2callszd2envzd2zzinline_recursionz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1985z00zzinline_recursionz00,
				BGl_string1968z00zzinline_recursionz00);
		}

	}



/* &find-recursive-calls1340 */
	obj_t BGl_z62findzd2recursivezd2calls1340z62zzinline_recursionz00(obj_t
		BgL_envz00_2908, obj_t BgL_nodez00_2909, obj_t BgL_vz00_2910,
		obj_t BgL_tailz00_2911, obj_t BgL_tresz00_2912)
	{
		{	/* Inline/recursion.scm 403 */
			{	/* Inline/recursion.scm 405 */
				obj_t BgL_arg1859z00_3016;
				obj_t BgL_arg1860z00_3017;

				{	/* Inline/recursion.scm 405 */
					BgL_varz00_bglt BgL_arg1862z00_3018;

					BgL_arg1862z00_3018 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2909)))->BgL_varz00);
					BgL_arg1859z00_3016 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
						((BgL_nodez00_bglt) BgL_arg1862z00_3018),
						((BgL_variablez00_bglt) BgL_vz00_2910), BFALSE, BgL_tresz00_2912);
				}
				{	/* Inline/recursion.scm 406 */
					BgL_nodez00_bglt BgL_arg1863z00_3019;

					BgL_arg1863z00_3019 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2909)))->BgL_valuez00);
					BgL_arg1860z00_3017 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1863z00_3019, ((BgL_variablez00_bglt) BgL_vz00_2910),
						BFALSE, BgL_tresz00_2912);
				}
				return
					BGl_appendzd221011zd2zzinline_recursionz00(BgL_arg1859z00_3016,
					BgL_arg1860z00_3017);
			}
		}

	}



/* &find-recursive-calls1338 */
	obj_t BGl_z62findzd2recursivezd2calls1338z62zzinline_recursionz00(obj_t
		BgL_envz00_2913, obj_t BgL_nodez00_2914, obj_t BgL_varz00_2915,
		obj_t BgL_tailz00_2916, obj_t BgL_tresz00_2917)
	{
		{	/* Inline/recursion.scm 397 */
			{	/* Inline/recursion.scm 398 */
				BgL_varz00_bglt BgL_arg1858z00_3021;

				BgL_arg1858z00_3021 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2914)))->BgL_varz00);
				return
					BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
					((BgL_nodez00_bglt) BgL_arg1858z00_3021),
					((BgL_variablez00_bglt) BgL_varz00_2915), BFALSE, BgL_tresz00_2917);
			}
		}

	}



/* &find-recursive-calls1336 */
	obj_t BGl_z62findzd2recursivezd2calls1336z62zzinline_recursionz00(obj_t
		BgL_envz00_2918, obj_t BgL_nodez00_2919, obj_t BgL_varz00_2920,
		obj_t BgL_tailz00_2921, obj_t BgL_tresz00_2922)
	{
		{	/* Inline/recursion.scm 391 */
			{	/* Inline/recursion.scm 392 */
				BgL_nodez00_bglt BgL_arg1857z00_3023;

				BgL_arg1857z00_3023 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2919)))->BgL_valuez00);
				return
					BGl_findzd2recursivezd2callsz00zzinline_recursionz00
					(BgL_arg1857z00_3023, ((BgL_variablez00_bglt) BgL_varz00_2920),
					BFALSE, BgL_tresz00_2922);
			}
		}

	}



/* &find-recursive-calls1334 */
	obj_t BGl_z62findzd2recursivezd2calls1334z62zzinline_recursionz00(obj_t
		BgL_envz00_2923, obj_t BgL_nodez00_2924, obj_t BgL_vz00_2925,
		obj_t BgL_tailz00_2926, obj_t BgL_tresz00_2927)
	{
		{	/* Inline/recursion.scm 383 */
			{	/* Inline/recursion.scm 385 */
				obj_t BgL_arg1852z00_3025;
				obj_t BgL_arg1853z00_3026;

				{	/* Inline/recursion.scm 385 */
					BgL_nodez00_bglt BgL_arg1854z00_3027;

					BgL_arg1854z00_3027 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2924)))->BgL_exitz00);
					BgL_arg1852z00_3025 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1854z00_3027, ((BgL_variablez00_bglt) BgL_vz00_2925),
						BFALSE, BgL_tresz00_2927);
				}
				{	/* Inline/recursion.scm 386 */
					BgL_nodez00_bglt BgL_arg1856z00_3028;

					BgL_arg1856z00_3028 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2924)))->
						BgL_valuez00);
					BgL_arg1853z00_3026 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1856z00_3028, ((BgL_variablez00_bglt) BgL_vz00_2925),
						BFALSE, BgL_tresz00_2927);
				}
				return
					BGl_appendzd221011zd2zzinline_recursionz00(BgL_arg1852z00_3025,
					BgL_arg1853z00_3026);
			}
		}

	}



/* &find-recursive-calls1332 */
	obj_t BGl_z62findzd2recursivezd2calls1332z62zzinline_recursionz00(obj_t
		BgL_envz00_2928, obj_t BgL_nodez00_2929, obj_t BgL_varz00_2930,
		obj_t BgL_tailz00_2931, obj_t BgL_tresz00_2932)
	{
		{	/* Inline/recursion.scm 376 */
			{	/* Inline/recursion.scm 377 */
				BgL_nodez00_bglt BgL_arg1849z00_3030;
				obj_t BgL_arg1850z00_3031;

				BgL_arg1849z00_3030 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2929)))->BgL_bodyz00);
				{	/* Inline/recursion.scm 378 */
					BgL_nodez00_bglt BgL_arg1851z00_3032;

					BgL_arg1851z00_3032 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2929)))->
						BgL_onexitz00);
					BgL_arg1850z00_3031 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1851z00_3032, ((BgL_variablez00_bglt) BgL_varz00_2930),
						BFALSE, BgL_tresz00_2932);
				}
				return
					BGl_findzd2recursivezd2callsz00zzinline_recursionz00
					(BgL_arg1849z00_3030, ((BgL_variablez00_bglt) BgL_varz00_2930),
					BFALSE, BgL_arg1850z00_3031);
			}
		}

	}



/* &find-recursive-calls1330 */
	obj_t BGl_z62findzd2recursivezd2calls1330z62zzinline_recursionz00(obj_t
		BgL_envz00_2933, obj_t BgL_nodez00_2934, obj_t BgL_varz00_2935,
		obj_t BgL_tailz00_2936, obj_t BgL_tresz00_2937)
	{
		{	/* Inline/recursion.scm 364 */
			{	/* Inline/recursion.scm 365 */
				obj_t BgL_g1153z00_3034;
				obj_t BgL_g1154z00_3035;

				BgL_g1153z00_3034 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2934)))->BgL_bindingsz00);
				{	/* Inline/recursion.scm 366 */
					BgL_nodez00_bglt BgL_arg1848z00_3036;

					BgL_arg1848z00_3036 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2934)))->BgL_bodyz00);
					BgL_g1154z00_3035 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1848z00_3036, ((BgL_variablez00_bglt) BgL_varz00_2935),
						BgL_tailz00_2936, BgL_tresz00_2937);
				}
				{
					obj_t BgL_bindingsz00_3038;
					obj_t BgL_callsz00_3039;

					BgL_bindingsz00_3038 = BgL_g1153z00_3034;
					BgL_callsz00_3039 = BgL_g1154z00_3035;
				BgL_loopz00_3037:
					if (NULLP(BgL_bindingsz00_3038))
						{	/* Inline/recursion.scm 367 */
							return BgL_callsz00_3039;
						}
					else
						{	/* Inline/recursion.scm 369 */
							obj_t BgL_arg1843z00_3040;
							obj_t BgL_arg1844z00_3041;

							BgL_arg1843z00_3040 = CDR(((obj_t) BgL_bindingsz00_3038));
							{	/* Inline/recursion.scm 371 */
								obj_t BgL_arg1845z00_3042;

								{	/* Inline/recursion.scm 371 */
									obj_t BgL_arg1846z00_3043;

									{	/* Inline/recursion.scm 371 */
										obj_t BgL_pairz00_3044;

										BgL_pairz00_3044 = CAR(((obj_t) BgL_bindingsz00_3038));
										BgL_arg1846z00_3043 = CDR(BgL_pairz00_3044);
									}
									BgL_arg1845z00_3042 =
										BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
										((BgL_nodez00_bglt) BgL_arg1846z00_3043),
										((BgL_variablez00_bglt) BgL_varz00_2935), BFALSE,
										BgL_tresz00_2937);
								}
								BgL_arg1844z00_3041 =
									BGl_appendzd221011zd2zzinline_recursionz00(BgL_callsz00_3039,
									BgL_arg1845z00_3042);
							}
							{
								obj_t BgL_callsz00_4032;
								obj_t BgL_bindingsz00_4031;

								BgL_bindingsz00_4031 = BgL_arg1843z00_3040;
								BgL_callsz00_4032 = BgL_arg1844z00_3041;
								BgL_callsz00_3039 = BgL_callsz00_4032;
								BgL_bindingsz00_3038 = BgL_bindingsz00_4031;
								goto BgL_loopz00_3037;
							}
						}
				}
			}
		}

	}



/* &find-recursive-calls1328 */
	obj_t BGl_z62findzd2recursivezd2calls1328z62zzinline_recursionz00(obj_t
		BgL_envz00_2938, obj_t BgL_nodez00_2939, obj_t BgL_varz00_2940,
		obj_t BgL_tailz00_2941, obj_t BgL_tresz00_2942)
	{
		{	/* Inline/recursion.scm 350 */
			{	/* Inline/recursion.scm 351 */
				obj_t BgL_g1150z00_3046;
				obj_t BgL_g1152z00_3047;

				BgL_g1150z00_3046 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2939)))->BgL_localsz00);
				{	/* Inline/recursion.scm 352 */
					BgL_nodez00_bglt BgL_arg1840z00_3048;

					BgL_arg1840z00_3048 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2939)))->BgL_bodyz00);
					BgL_g1152z00_3047 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1840z00_3048, ((BgL_variablez00_bglt) BgL_varz00_2940),
						BgL_tailz00_2941, BgL_tresz00_2942);
				}
				{
					obj_t BgL_localsz00_3050;
					obj_t BgL_callsz00_3051;

					BgL_localsz00_3050 = BgL_g1150z00_3046;
					BgL_callsz00_3051 = BgL_g1152z00_3047;
				BgL_loopz00_3049:
					if (NULLP(BgL_localsz00_3050))
						{	/* Inline/recursion.scm 353 */
							return BgL_callsz00_3051;
						}
					else
						{	/* Inline/recursion.scm 355 */
							obj_t BgL_arg1834z00_3052;
							obj_t BgL_arg1835z00_3053;

							BgL_arg1834z00_3052 = CDR(((obj_t) BgL_localsz00_3050));
							{	/* Inline/recursion.scm 358 */
								obj_t BgL_arg1836z00_3054;

								{	/* Inline/recursion.scm 358 */
									obj_t BgL_arg1837z00_3055;

									BgL_arg1837z00_3055 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt)
																		CAR(
																			((obj_t) BgL_localsz00_3050))))))->
														BgL_valuez00))))->BgL_bodyz00);
									BgL_arg1836z00_3054 =
										BGl_findzd2recursivezd2callsz00zzinline_recursionz00((
											(BgL_nodez00_bglt) BgL_arg1837z00_3055),
										((BgL_variablez00_bglt) BgL_varz00_2940), BTRUE,
										BgL_tresz00_2942);
								}
								BgL_arg1835z00_3053 =
									BGl_appendzd221011zd2zzinline_recursionz00(BgL_callsz00_3051,
									BgL_arg1836z00_3054);
							}
							{
								obj_t BgL_callsz00_4055;
								obj_t BgL_localsz00_4054;

								BgL_localsz00_4054 = BgL_arg1834z00_3052;
								BgL_callsz00_4055 = BgL_arg1835z00_3053;
								BgL_callsz00_3051 = BgL_callsz00_4055;
								BgL_localsz00_3050 = BgL_localsz00_4054;
								goto BgL_loopz00_3049;
							}
						}
				}
			}
		}

	}



/* &find-recursive-calls1326 */
	obj_t BGl_z62findzd2recursivezd2calls1326z62zzinline_recursionz00(obj_t
		BgL_envz00_2943, obj_t BgL_nodez00_2944, obj_t BgL_varz00_2945,
		obj_t BgL_tailz00_2946, obj_t BgL_tresz00_2947)
	{
		{	/* Inline/recursion.scm 338 */
			{	/* Inline/recursion.scm 339 */
				obj_t BgL_g1148z00_3057;
				obj_t BgL_g1149z00_3058;

				BgL_g1148z00_3057 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2944)))->BgL_clausesz00);
				{	/* Inline/recursion.scm 340 */
					BgL_nodez00_bglt BgL_arg1831z00_3059;

					BgL_arg1831z00_3059 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2944)))->BgL_testz00);
					BgL_g1149z00_3058 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1831z00_3059, ((BgL_variablez00_bglt) BgL_varz00_2945),
						BFALSE, BgL_tresz00_2947);
				}
				{
					obj_t BgL_clausesz00_3061;
					obj_t BgL_callsz00_3062;

					BgL_clausesz00_3061 = BgL_g1148z00_3057;
					BgL_callsz00_3062 = BgL_g1149z00_3058;
				BgL_loopz00_3060:
					if (NULLP(BgL_clausesz00_3061))
						{	/* Inline/recursion.scm 341 */
							return BgL_callsz00_3062;
						}
					else
						{	/* Inline/recursion.scm 343 */
							obj_t BgL_arg1808z00_3063;
							obj_t BgL_arg1812z00_3064;

							BgL_arg1808z00_3063 = CDR(((obj_t) BgL_clausesz00_3061));
							{	/* Inline/recursion.scm 344 */
								obj_t BgL_arg1820z00_3065;

								{	/* Inline/recursion.scm 344 */
									obj_t BgL_arg1822z00_3066;

									{	/* Inline/recursion.scm 344 */
										obj_t BgL_pairz00_3067;

										BgL_pairz00_3067 = CAR(((obj_t) BgL_clausesz00_3061));
										BgL_arg1822z00_3066 = CDR(BgL_pairz00_3067);
									}
									BgL_arg1820z00_3065 =
										BGl_findzd2recursivezd2callsz00zzinline_recursionz00(
										((BgL_nodez00_bglt) BgL_arg1822z00_3066),
										((BgL_variablez00_bglt) BgL_varz00_2945), BgL_tailz00_2946,
										BgL_tresz00_2947);
								}
								BgL_arg1812z00_3064 =
									BGl_appendzd221011zd2zzinline_recursionz00
									(BgL_arg1820z00_3065, BgL_callsz00_3062);
							}
							{
								obj_t BgL_callsz00_4074;
								obj_t BgL_clausesz00_4073;

								BgL_clausesz00_4073 = BgL_arg1808z00_3063;
								BgL_callsz00_4074 = BgL_arg1812z00_3064;
								BgL_callsz00_3062 = BgL_callsz00_4074;
								BgL_clausesz00_3061 = BgL_clausesz00_4073;
								goto BgL_loopz00_3060;
							}
						}
				}
			}
		}

	}



/* &find-recursive-calls1324 */
	obj_t BGl_z62findzd2recursivezd2calls1324z62zzinline_recursionz00(obj_t
		BgL_envz00_2948, obj_t BgL_nodez00_2949, obj_t BgL_varz00_2950,
		obj_t BgL_tailz00_2951, obj_t BgL_tresz00_2952)
	{
		{	/* Inline/recursion.scm 329 */
			{	/* Inline/recursion.scm 331 */
				obj_t BgL_arg1767z00_3069;
				obj_t BgL_arg1770z00_3070;
				obj_t BgL_arg1771z00_3071;

				{	/* Inline/recursion.scm 331 */
					BgL_nodez00_bglt BgL_arg1798z00_3072;

					BgL_arg1798z00_3072 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2949)))->BgL_procz00);
					BgL_arg1767z00_3069 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1798z00_3072, ((BgL_variablez00_bglt) BgL_varz00_2950),
						BFALSE, BgL_tresz00_2952);
				}
				{	/* Inline/recursion.scm 332 */
					BgL_nodez00_bglt BgL_arg1799z00_3073;

					BgL_arg1799z00_3073 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2949)))->BgL_msgz00);
					BgL_arg1770z00_3070 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1799z00_3073, ((BgL_variablez00_bglt) BgL_varz00_2950),
						BFALSE, BgL_tresz00_2952);
				}
				{	/* Inline/recursion.scm 333 */
					BgL_nodez00_bglt BgL_arg1805z00_3074;

					BgL_arg1805z00_3074 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2949)))->BgL_objz00);
					BgL_arg1771z00_3071 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1805z00_3074, ((BgL_variablez00_bglt) BgL_varz00_2950),
						BFALSE, BgL_tresz00_2952);
				}
				{	/* Inline/recursion.scm 331 */
					obj_t BgL_list1772z00_3075;

					{	/* Inline/recursion.scm 331 */
						obj_t BgL_arg1773z00_3076;

						{	/* Inline/recursion.scm 331 */
							obj_t BgL_arg1775z00_3077;

							BgL_arg1775z00_3077 = MAKE_YOUNG_PAIR(BgL_arg1771z00_3071, BNIL);
							BgL_arg1773z00_3076 =
								MAKE_YOUNG_PAIR(BgL_arg1770z00_3070, BgL_arg1775z00_3077);
						}
						BgL_list1772z00_3075 =
							MAKE_YOUNG_PAIR(BgL_arg1767z00_3069, BgL_arg1773z00_3076);
					}
					return
						BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list1772z00_3075);
				}
			}
		}

	}



/* &find-recursive-calls1322 */
	obj_t BGl_z62findzd2recursivezd2calls1322z62zzinline_recursionz00(obj_t
		BgL_envz00_2953, obj_t BgL_nodez00_2954, obj_t BgL_varz00_2955,
		obj_t BgL_tailz00_2956, obj_t BgL_tresz00_2957)
	{
		{	/* Inline/recursion.scm 320 */
			{	/* Inline/recursion.scm 322 */
				obj_t BgL_arg1750z00_3079;
				obj_t BgL_arg1751z00_3080;
				obj_t BgL_arg1752z00_3081;

				{	/* Inline/recursion.scm 322 */
					BgL_nodez00_bglt BgL_arg1761z00_3082;

					BgL_arg1761z00_3082 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2954)))->BgL_testz00);
					BgL_arg1750z00_3079 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1761z00_3082, ((BgL_variablez00_bglt) BgL_varz00_2955),
						BFALSE, BgL_tresz00_2957);
				}
				{	/* Inline/recursion.scm 323 */
					BgL_nodez00_bglt BgL_arg1762z00_3083;

					BgL_arg1762z00_3083 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2954)))->BgL_truez00);
					BgL_arg1751z00_3080 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1762z00_3083, ((BgL_variablez00_bglt) BgL_varz00_2955),
						BgL_tailz00_2956, BgL_tresz00_2957);
				}
				{	/* Inline/recursion.scm 324 */
					BgL_nodez00_bglt BgL_arg1765z00_3084;

					BgL_arg1765z00_3084 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2954)))->BgL_falsez00);
					BgL_arg1752z00_3081 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1765z00_3084, ((BgL_variablez00_bglt) BgL_varz00_2955),
						BgL_tailz00_2956, BgL_tresz00_2957);
				}
				{	/* Inline/recursion.scm 322 */
					obj_t BgL_list1753z00_3085;

					{	/* Inline/recursion.scm 322 */
						obj_t BgL_arg1754z00_3086;

						{	/* Inline/recursion.scm 322 */
							obj_t BgL_arg1755z00_3087;

							BgL_arg1755z00_3087 = MAKE_YOUNG_PAIR(BgL_arg1752z00_3081, BNIL);
							BgL_arg1754z00_3086 =
								MAKE_YOUNG_PAIR(BgL_arg1751z00_3080, BgL_arg1755z00_3087);
						}
						BgL_list1753z00_3085 =
							MAKE_YOUNG_PAIR(BgL_arg1750z00_3079, BgL_arg1754z00_3086);
					}
					return
						BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list1753z00_3085);
				}
			}
		}

	}



/* &find-recursive-calls1320 */
	obj_t BGl_z62findzd2recursivezd2calls1320z62zzinline_recursionz00(obj_t
		BgL_envz00_2958, obj_t BgL_nodez00_2959, obj_t BgL_varz00_2960,
		obj_t BgL_tailz00_2961, obj_t BgL_tresz00_2962)
	{
		{	/* Inline/recursion.scm 314 */
			{	/* Inline/recursion.scm 315 */
				BgL_nodez00_bglt BgL_arg1749z00_3089;

				BgL_arg1749z00_3089 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2959)))->BgL_valuez00);
				return
					BGl_findzd2recursivezd2callsz00zzinline_recursionz00
					(BgL_arg1749z00_3089, ((BgL_variablez00_bglt) BgL_varz00_2960),
					BFALSE, BgL_tresz00_2962);
			}
		}

	}



/* &find-recursive-calls1318 */
	obj_t BGl_z62findzd2recursivezd2calls1318z62zzinline_recursionz00(obj_t
		BgL_envz00_2963, obj_t BgL_nodez00_2964, obj_t BgL_varz00_2965,
		obj_t BgL_tailz00_2966, obj_t BgL_tresz00_2967)
	{
		{	/* Inline/recursion.scm 308 */
			{	/* Inline/recursion.scm 309 */
				BgL_nodez00_bglt BgL_arg1748z00_3091;

				BgL_arg1748z00_3091 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2964)))->BgL_argz00);
				return
					BGl_findzd2recursivezd2callsz00zzinline_recursionz00
					(BgL_arg1748z00_3091, ((BgL_variablez00_bglt) BgL_varz00_2965),
					BFALSE, BgL_tresz00_2967);
			}
		}

	}



/* &find-recursive-calls1316 */
	obj_t BGl_z62findzd2recursivezd2calls1316z62zzinline_recursionz00(obj_t
		BgL_envz00_2968, obj_t BgL_nodez00_2969, obj_t BgL_varz00_2970,
		obj_t BgL_tailz00_2971, obj_t BgL_tresz00_2972)
	{
		{	/* Inline/recursion.scm 302 */
			return
				BGl_findzd2recursivezd2callsza2za2zzinline_recursionz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2969)))->BgL_exprza2za2),
				BgL_varz00_2970, BFALSE, BgL_tresz00_2972);
		}

	}



/* &find-recursive-calls1314 */
	obj_t BGl_z62findzd2recursivezd2calls1314z62zzinline_recursionz00(obj_t
		BgL_envz00_2973, obj_t BgL_nodez00_2974, obj_t BgL_varz00_2975,
		obj_t BgL_tailz00_2976, obj_t BgL_tresz00_2977)
	{
		{	/* Inline/recursion.scm 294 */
			{	/* Inline/recursion.scm 296 */
				obj_t BgL_arg1738z00_3094;
				obj_t BgL_arg1739z00_3095;

				{	/* Inline/recursion.scm 296 */
					BgL_nodez00_bglt BgL_arg1740z00_3096;

					BgL_arg1740z00_3096 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2974)))->BgL_funz00);
					BgL_arg1738z00_3094 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1740z00_3096, ((BgL_variablez00_bglt) BgL_varz00_2975),
						BFALSE, BgL_tresz00_2977);
				}
				BgL_arg1739z00_3095 =
					BGl_findzd2recursivezd2callsza2za2zzinline_recursionz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2974)))->BgL_argsz00),
					BgL_varz00_2975, BFALSE, BgL_tresz00_2977);
				return BGl_appendzd221011zd2zzinline_recursionz00(BgL_arg1738z00_3094,
					BgL_arg1739z00_3095);
			}
		}

	}



/* &find-recursive-calls1312 */
	obj_t BGl_z62findzd2recursivezd2calls1312z62zzinline_recursionz00(obj_t
		BgL_envz00_2978, obj_t BgL_nodez00_2979, obj_t BgL_varz00_2980,
		obj_t BgL_tailz00_2981, obj_t BgL_tresz00_2982)
	{
		{	/* Inline/recursion.scm 286 */
			{	/* Inline/recursion.scm 288 */
				obj_t BgL_arg1734z00_3098;
				obj_t BgL_arg1735z00_3099;

				{	/* Inline/recursion.scm 288 */
					BgL_nodez00_bglt BgL_arg1736z00_3100;

					BgL_arg1736z00_3100 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2979)))->BgL_funz00);
					BgL_arg1734z00_3098 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1736z00_3100, ((BgL_variablez00_bglt) BgL_varz00_2980),
						BFALSE, BgL_tresz00_2982);
				}
				{	/* Inline/recursion.scm 289 */
					BgL_nodez00_bglt BgL_arg1737z00_3101;

					BgL_arg1737z00_3101 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2979)))->BgL_argz00);
					BgL_arg1735z00_3099 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1737z00_3101, ((BgL_variablez00_bglt) BgL_varz00_2980),
						BFALSE, BgL_tresz00_2982);
				}
				return
					BGl_appendzd221011zd2zzinline_recursionz00(BgL_arg1734z00_3098,
					BgL_arg1735z00_3099);
			}
		}

	}



/* &find-recursive-calls1310 */
	obj_t BGl_z62findzd2recursivezd2calls1310z62zzinline_recursionz00(obj_t
		BgL_envz00_2983, obj_t BgL_nodez00_2984, obj_t BgL_varz00_2985,
		obj_t BgL_tailz00_2986, obj_t BgL_tresz00_2987)
	{
		{	/* Inline/recursion.scm 274 */
			{	/* Inline/recursion.scm 276 */
				obj_t BgL_argszd2callszd2_3103;

				BgL_argszd2callszd2_3103 =
					BGl_findzd2recursivezd2callsza2za2zzinline_recursionz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2984)))->BgL_argsz00),
					BgL_varz00_2985, BgL_tailz00_2986, BgL_tresz00_2987);
				{	/* Inline/recursion.scm 277 */
					bool_t BgL_test2082z00_4138;

					{	/* Inline/recursion.scm 277 */
						bool_t BgL_test2083z00_4139;

						{	/* Inline/recursion.scm 277 */
							BgL_varz00_bglt BgL_arg1724z00_3104;

							BgL_arg1724z00_3104 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2984)))->BgL_funz00);
							{	/* Inline/recursion.scm 277 */
								obj_t BgL_classz00_3105;

								BgL_classz00_3105 = BGl_varz00zzast_nodez00;
								{	/* Inline/recursion.scm 277 */
									BgL_objectz00_bglt BgL_arg1807z00_3106;

									{	/* Inline/recursion.scm 277 */
										obj_t BgL_tmpz00_4142;

										BgL_tmpz00_4142 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1724z00_3104));
										BgL_arg1807z00_3106 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4142);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Inline/recursion.scm 277 */
											long BgL_idxz00_3107;

											BgL_idxz00_3107 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3106);
											BgL_test2083z00_4139 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3107 + 2L)) == BgL_classz00_3105);
										}
									else
										{	/* Inline/recursion.scm 277 */
											bool_t BgL_res1955z00_3110;

											{	/* Inline/recursion.scm 277 */
												obj_t BgL_oclassz00_3111;

												{	/* Inline/recursion.scm 277 */
													obj_t BgL_arg1815z00_3112;
													long BgL_arg1816z00_3113;

													BgL_arg1815z00_3112 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Inline/recursion.scm 277 */
														long BgL_arg1817z00_3114;

														BgL_arg1817z00_3114 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3106);
														BgL_arg1816z00_3113 =
															(BgL_arg1817z00_3114 - OBJECT_TYPE);
													}
													BgL_oclassz00_3111 =
														VECTOR_REF(BgL_arg1815z00_3112,
														BgL_arg1816z00_3113);
												}
												{	/* Inline/recursion.scm 277 */
													bool_t BgL__ortest_1115z00_3115;

													BgL__ortest_1115z00_3115 =
														(BgL_classz00_3105 == BgL_oclassz00_3111);
													if (BgL__ortest_1115z00_3115)
														{	/* Inline/recursion.scm 277 */
															BgL_res1955z00_3110 = BgL__ortest_1115z00_3115;
														}
													else
														{	/* Inline/recursion.scm 277 */
															long BgL_odepthz00_3116;

															{	/* Inline/recursion.scm 277 */
																obj_t BgL_arg1804z00_3117;

																BgL_arg1804z00_3117 = (BgL_oclassz00_3111);
																BgL_odepthz00_3116 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3117);
															}
															if ((2L < BgL_odepthz00_3116))
																{	/* Inline/recursion.scm 277 */
																	obj_t BgL_arg1802z00_3118;

																	{	/* Inline/recursion.scm 277 */
																		obj_t BgL_arg1803z00_3119;

																		BgL_arg1803z00_3119 = (BgL_oclassz00_3111);
																		BgL_arg1802z00_3118 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3119, 2L);
																	}
																	BgL_res1955z00_3110 =
																		(BgL_arg1802z00_3118 == BgL_classz00_3105);
																}
															else
																{	/* Inline/recursion.scm 277 */
																	BgL_res1955z00_3110 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2083z00_4139 = BgL_res1955z00_3110;
										}
								}
							}
						}
						if (BgL_test2083z00_4139)
							{	/* Inline/recursion.scm 277 */
								BgL_test2082z00_4138 =
									(
									((obj_t)
										(((BgL_varz00_bglt) COBJECT(
													(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2984)))->
														BgL_funz00)))->BgL_variablez00)) ==
									BgL_varz00_2985);
							}
						else
							{	/* Inline/recursion.scm 277 */
								BgL_test2082z00_4138 = ((bool_t) 0);
							}
					}
					if (BgL_test2082z00_4138)
						{	/* Inline/recursion.scm 277 */
							if (CBOOL(BgL_tailz00_2986))
								{	/* Inline/recursion.scm 279 */
									BFALSE;
								}
							else
								{	/* Inline/recursion.scm 279 */
									CELL_SET(((obj_t) BgL_tresz00_2987), BFALSE);
								}
							return
								MAKE_YOUNG_PAIR(
								((obj_t)
									((BgL_appz00_bglt) BgL_nodez00_2984)),
								BgL_argszd2callszd2_3103);
						}
					else
						{	/* Inline/recursion.scm 277 */
							return BgL_argszd2callszd2_3103;
						}
				}
			}
		}

	}



/* &find-recursive-calls1308 */
	obj_t BGl_z62findzd2recursivezd2calls1308z62zzinline_recursionz00(obj_t
		BgL_envz00_2988, obj_t BgL_nodez00_2989, obj_t BgL_varz00_2990,
		obj_t BgL_tailz00_2991, obj_t BgL_tresz00_2992)
	{
		{	/* Inline/recursion.scm 263 */
			{	/* Inline/recursion.scm 264 */
				obj_t BgL_arg1699z00_3121;
				obj_t BgL_arg1700z00_3122;
				obj_t BgL_arg1701z00_3123;

				{	/* Inline/recursion.scm 264 */
					BgL_nodez00_bglt BgL_arg1708z00_3124;

					BgL_arg1708z00_3124 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2989)))->BgL_mutexz00);
					BgL_arg1699z00_3121 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1708z00_3124, ((BgL_variablez00_bglt) BgL_varz00_2990),
						BFALSE, BgL_tresz00_2992);
				}
				{	/* Inline/recursion.scm 265 */
					BgL_nodez00_bglt BgL_arg1709z00_3125;

					BgL_arg1709z00_3125 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2989)))->BgL_prelockz00);
					BgL_arg1700z00_3122 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1709z00_3125, ((BgL_variablez00_bglt) BgL_varz00_2990),
						BFALSE, BgL_tresz00_2992);
				}
				{	/* Inline/recursion.scm 266 */
					BgL_nodez00_bglt BgL_arg1710z00_3126;

					BgL_arg1710z00_3126 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2989)))->BgL_bodyz00);
					BgL_arg1701z00_3123 =
						BGl_findzd2recursivezd2callsz00zzinline_recursionz00
						(BgL_arg1710z00_3126, ((BgL_variablez00_bglt) BgL_varz00_2990),
						BFALSE, BgL_tresz00_2992);
				}
				{	/* Inline/recursion.scm 264 */
					obj_t BgL_list1702z00_3127;

					{	/* Inline/recursion.scm 264 */
						obj_t BgL_arg1703z00_3128;

						{	/* Inline/recursion.scm 264 */
							obj_t BgL_arg1705z00_3129;

							BgL_arg1705z00_3129 = MAKE_YOUNG_PAIR(BgL_arg1701z00_3123, BNIL);
							BgL_arg1703z00_3128 =
								MAKE_YOUNG_PAIR(BgL_arg1700z00_3122, BgL_arg1705z00_3129);
						}
						BgL_list1702z00_3127 =
							MAKE_YOUNG_PAIR(BgL_arg1699z00_3121, BgL_arg1703z00_3128);
					}
					return
						BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list1702z00_3127);
				}
			}
		}

	}



/* &find-recursive-calls1306 */
	obj_t BGl_z62findzd2recursivezd2calls1306z62zzinline_recursionz00(obj_t
		BgL_envz00_2993, obj_t BgL_nodez00_2994, obj_t BgL_varz00_2995,
		obj_t BgL_tailz00_2996, obj_t BgL_tresz00_2997)
	{
		{	/* Inline/recursion.scm 257 */
			return
				BGl_findzd2recursivezd2callsza2za2zzinline_recursionz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2994)))->BgL_nodesz00),
				BgL_varz00_2995, BgL_tailz00_2996, BgL_tresz00_2997);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinline_recursionz00(void)
	{
		{	/* Inline/recursion.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzast_alphatiza7eza7(53595773L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzinline_inlinez00(20504962L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzinline_simplez00(221468907L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzinline_variantz00(347990278L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzinline_loopz00(342478691L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			BGl_modulezd2initializa7ationz75zzinline_appz00(148760855L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
			return
				BGl_modulezd2initializa7ationz75zzreduce_csez00(347839157L,
				BSTRING_TO_STRING(BGl_string1986z00zzinline_recursionz00));
		}

	}

#ifdef __cplusplus
}
#endif
