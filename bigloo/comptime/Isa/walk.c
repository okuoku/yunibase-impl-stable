/*===========================================================================*/
/*   (Isa/walk.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Isa/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_ISA_WALK_TYPE_DEFINITIONS
#define BGL_ISA_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;


#endif													// BGL_ISA_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62isaz121289z70zzisa_walkz00(obj_t, obj_t);
	static obj_t BGl_z62isazd2walkz12za2zzisa_walkz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzisa_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62isaz12zd2app1292za2zzisa_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2isazf2cdepthza2zf2zzisa_walkz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzisa_walkz00(void);
	static obj_t BGl_z62clearzd2isazd2cachez12z70zzisa_walkz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_isazd2walkz12zc0zzisa_walkz00(obj_t);
	static obj_t BGl_za2isazf2finalza2zf2zzisa_walkz00 = BUNSPEC;
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzisa_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	extern obj_t BGl_getzd2objectzd2typez00zztype_cachez00(void);
	static obj_t BGl_objectzd2initzd2zzisa_walkz00(void);
	static BgL_nodez00_bglt BGl_z62isaz12z70zzisa_walkz00(obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static BgL_nodez00_bglt BGl_isaz12z12zzisa_walkz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzisa_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzisa_walkz00(void);
	static obj_t BGl_z62initzd2isazd2cachez12z70zzisa_walkz00(obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern BgL_typez00_bglt BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	static BgL_typez00_bglt
		BGl_uncastedzd2typeze70z35zzisa_walkz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzisa_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzisa_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzisa_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzisa_walkz00(void);
	static obj_t BGl_za2isazd2objectzf2finalza2z20zzisa_walkz00 = BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zzisa_walkz00(void);
	extern BgL_nodez00_bglt BGl_walk0z12z12zzast_walkz00(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	static obj_t BGl_za2isazd2objectzf2cdepthza2z20zzisa_walkz00 = BUNSPEC;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_isazd2funz12zc0zzisa_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_isazd2ofzd2zztype_miscz00(BgL_nodez00_bglt);
	extern obj_t BGl_getzd2classzd2listz00zzobject_classz00(void);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static long BGl_setzd2classzd2depthz12z12zzisa_walkz00(BgL_typez00_bglt);
	static obj_t __cnst[10];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_clearzd2isazd2cachez12zd2envzc0zzisa_walkz00,
		BgL_bgl_za762clearza7d2isaza7d1799za7,
		BGl_z62clearzd2isazd2cachez12z70zzisa_walkz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_initzd2isazd2cachez12zd2envzc0zzisa_walkz00,
		BgL_bgl_za762initza7d2isaza7d21800za7,
		BGl_z62initzd2isazd2cachez12z70zzisa_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_isazd2walkz12zd2envz12zzisa_walkz00,
		BgL_bgl_za762isaza7d2walkza7121801za7, BGl_z62isazd2walkz12za2zzisa_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1773z00zzisa_walkz00,
		BgL_bgl_string1773za700za7za7i1802za7, "Isa", 3);
	      DEFINE_STRING(BGl_string1774z00zzisa_walkz00,
		BgL_bgl_string1774za700za7za7i1803za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1775z00zzisa_walkz00,
		BgL_bgl_string1775za700za7za7i1804za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1776z00zzisa_walkz00,
		BgL_bgl_string1776za700za7za7i1805za7, " error", 6);
	      DEFINE_STRING(BGl_string1777z00zzisa_walkz00,
		BgL_bgl_string1777za700za7za7i1806za7, "s", 1);
	      DEFINE_STRING(BGl_string1778z00zzisa_walkz00,
		BgL_bgl_string1778za700za7za7i1807za7, "", 0);
	      DEFINE_STRING(BGl_string1779z00zzisa_walkz00,
		BgL_bgl_string1779za700za7za7i1808za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1780z00zzisa_walkz00,
		BgL_bgl_string1780za700za7za7i1809za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1782z00zzisa_walkz00,
		BgL_bgl_string1782za700za7za7i1810za7, "isa!1289", 8);
	      DEFINE_STRING(BGl_string1784z00zzisa_walkz00,
		BgL_bgl_string1784za700za7za7i1811za7, "isa!::node", 10);
	      DEFINE_STRING(BGl_string1785z00zzisa_walkz00,
		BgL_bgl_string1785za700za7za7i1812za7, "isa_walk", 8);
	      DEFINE_STRING(BGl_string1786z00zzisa_walkz00,
		BgL_bgl_string1786za700za7za7i1813za7,
		"class-display-min-size static %isa-object/final? %isa/final? %isa-object/cdepth? __object %isa/cdepth? (clear-isa-cache!) pass-started (init-isa-cache!) ",
		153);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1781z00zzisa_walkz00,
		BgL_bgl_za762isaza7121289za7701814za7, BGl_z62isaz121289z70zzisa_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1783z00zzisa_walkz00,
		BgL_bgl_za762isaza712za7d2app11815za7,
		BGl_z62isaz12zd2app1292za2zzisa_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_isaz12zd2envzc0zzisa_walkz00,
		BgL_bgl_za762isaza712za770za7za7is1816za7, BGl_z62isaz12z70zzisa_walkz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzisa_walkz00));
		     ADD_ROOT((void *) (&BGl_za2isazf2cdepthza2zf2zzisa_walkz00));
		     ADD_ROOT((void *) (&BGl_za2isazf2finalza2zf2zzisa_walkz00));
		     ADD_ROOT((void *) (&BGl_za2isazd2objectzf2finalza2z20zzisa_walkz00));
		     ADD_ROOT((void *) (&BGl_za2isazd2objectzf2cdepthza2z20zzisa_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzisa_walkz00(long
		BgL_checksumz00_2487, char *BgL_fromz00_2488)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzisa_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzisa_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzisa_walkz00();
					BGl_libraryzd2moduleszd2initz00zzisa_walkz00();
					BGl_cnstzd2initzd2zzisa_walkz00();
					BGl_importedzd2moduleszd2initz00zzisa_walkz00();
					BGl_genericzd2initzd2zzisa_walkz00();
					BGl_methodzd2initzd2zzisa_walkz00();
					return BGl_toplevelzd2initzd2zzisa_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzisa_walkz00(void)
	{
		{	/* Isa/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"isa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "isa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"isa_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "isa_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzisa_walkz00(void)
	{
		{	/* Isa/walk.scm 15 */
			{	/* Isa/walk.scm 15 */
				obj_t BgL_cportz00_2366;

				{	/* Isa/walk.scm 15 */
					obj_t BgL_stringz00_2373;

					BgL_stringz00_2373 = BGl_string1786z00zzisa_walkz00;
					{	/* Isa/walk.scm 15 */
						obj_t BgL_startz00_2374;

						BgL_startz00_2374 = BINT(0L);
						{	/* Isa/walk.scm 15 */
							obj_t BgL_endz00_2375;

							BgL_endz00_2375 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2373)));
							{	/* Isa/walk.scm 15 */

								BgL_cportz00_2366 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2373, BgL_startz00_2374, BgL_endz00_2375);
				}}}}
				{
					long BgL_iz00_2367;

					BgL_iz00_2367 = 9L;
				BgL_loopz00_2368:
					if ((BgL_iz00_2367 == -1L))
						{	/* Isa/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Isa/walk.scm 15 */
							{	/* Isa/walk.scm 15 */
								obj_t BgL_arg1798z00_2369;

								{	/* Isa/walk.scm 15 */

									{	/* Isa/walk.scm 15 */
										obj_t BgL_locationz00_2371;

										BgL_locationz00_2371 = BBOOL(((bool_t) 0));
										{	/* Isa/walk.scm 15 */

											BgL_arg1798z00_2369 =
												BGl_readz00zz__readerz00(BgL_cportz00_2366,
												BgL_locationz00_2371);
										}
									}
								}
								{	/* Isa/walk.scm 15 */
									int BgL_tmpz00_2522;

									BgL_tmpz00_2522 = (int) (BgL_iz00_2367);
									CNST_TABLE_SET(BgL_tmpz00_2522, BgL_arg1798z00_2369);
							}}
							{	/* Isa/walk.scm 15 */
								int BgL_auxz00_2372;

								BgL_auxz00_2372 = (int) ((BgL_iz00_2367 - 1L));
								{
									long BgL_iz00_2527;

									BgL_iz00_2527 = (long) (BgL_auxz00_2372);
									BgL_iz00_2367 = BgL_iz00_2527;
									goto BgL_loopz00_2368;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzisa_walkz00(void)
	{
		{	/* Isa/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzisa_walkz00(void)
	{
		{	/* Isa/walk.scm 15 */
			BGl_za2isazf2cdepthza2zf2zzisa_walkz00 = BFALSE;
			BGl_za2isazd2objectzf2cdepthza2z20zzisa_walkz00 = BFALSE;
			BGl_za2isazf2finalza2zf2zzisa_walkz00 = BFALSE;
			BGl_za2isazd2objectzf2finalza2z20zzisa_walkz00 = BFALSE;
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzisa_walkz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1588;

				BgL_headz00_1588 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1589;
					obj_t BgL_tailz00_1590;

					BgL_prevz00_1589 = BgL_headz00_1588;
					BgL_tailz00_1590 = BgL_l1z00_1;
				BgL_loopz00_1591:
					if (PAIRP(BgL_tailz00_1590))
						{
							obj_t BgL_newzd2prevzd2_1593;

							BgL_newzd2prevzd2_1593 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1590), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1589, BgL_newzd2prevzd2_1593);
							{
								obj_t BgL_tailz00_2537;
								obj_t BgL_prevz00_2536;

								BgL_prevz00_2536 = BgL_newzd2prevzd2_1593;
								BgL_tailz00_2537 = CDR(BgL_tailz00_1590);
								BgL_tailz00_1590 = BgL_tailz00_2537;
								BgL_prevz00_1589 = BgL_prevz00_2536;
								goto BgL_loopz00_1591;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1588);
				}
			}
		}

	}



/* isa-walk! */
	BGL_EXPORTED_DEF obj_t BGl_isazd2walkz12zc0zzisa_walkz00(obj_t
		BgL_globalsz00_17)
	{
		{	/* Isa/walk.scm 41 */
			{	/* Isa/walk.scm 42 */
				obj_t BgL_list1318z00_1608;

				{	/* Isa/walk.scm 42 */
					obj_t BgL_arg1319z00_1609;

					{	/* Isa/walk.scm 42 */
						obj_t BgL_arg1320z00_1610;

						BgL_arg1320z00_1610 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1319z00_1609 =
							MAKE_YOUNG_PAIR(BGl_string1773z00zzisa_walkz00,
							BgL_arg1320z00_1610);
					}
					BgL_list1318z00_1608 =
						MAKE_YOUNG_PAIR(BGl_string1774z00zzisa_walkz00,
						BgL_arg1319z00_1609);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1318z00_1608);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1773z00zzisa_walkz00;
			{	/* Isa/walk.scm 42 */
				obj_t BgL_g1119z00_1611;
				obj_t BgL_g1120z00_1612;

				{	/* Isa/walk.scm 42 */
					obj_t BgL_list1329z00_1625;

					BgL_list1329z00_1625 =
						MAKE_YOUNG_PAIR(BGl_initzd2isazd2cachez12zd2envzc0zzisa_walkz00,
						BNIL);
					BgL_g1119z00_1611 = BgL_list1329z00_1625;
				}
				BgL_g1120z00_1612 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_1614;
					obj_t BgL_hnamesz00_1615;

					BgL_hooksz00_1614 = BgL_g1119z00_1611;
					BgL_hnamesz00_1615 = BgL_g1120z00_1612;
				BgL_zc3z04anonymousza31321ze3z87_1616:
					if (NULLP(BgL_hooksz00_1614))
						{	/* Isa/walk.scm 42 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Isa/walk.scm 42 */
							bool_t BgL_test1822z00_2552;

							{	/* Isa/walk.scm 42 */
								obj_t BgL_fun1328z00_1623;

								BgL_fun1328z00_1623 = CAR(((obj_t) BgL_hooksz00_1614));
								BgL_test1822z00_2552 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1328z00_1623));
							}
							if (BgL_test1822z00_2552)
								{	/* Isa/walk.scm 42 */
									obj_t BgL_arg1325z00_1620;
									obj_t BgL_arg1326z00_1621;

									BgL_arg1325z00_1620 = CDR(((obj_t) BgL_hooksz00_1614));
									BgL_arg1326z00_1621 = CDR(((obj_t) BgL_hnamesz00_1615));
									{
										obj_t BgL_hnamesz00_2564;
										obj_t BgL_hooksz00_2563;

										BgL_hooksz00_2563 = BgL_arg1325z00_1620;
										BgL_hnamesz00_2564 = BgL_arg1326z00_1621;
										BgL_hnamesz00_1615 = BgL_hnamesz00_2564;
										BgL_hooksz00_1614 = BgL_hooksz00_2563;
										goto BgL_zc3z04anonymousza31321ze3z87_1616;
									}
								}
							else
								{	/* Isa/walk.scm 42 */
									obj_t BgL_arg1327z00_1622;

									BgL_arg1327z00_1622 = CAR(((obj_t) BgL_hnamesz00_1615));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1773z00zzisa_walkz00,
										BGl_string1775z00zzisa_walkz00, BgL_arg1327z00_1622);
								}
						}
				}
			}
			{	/* Isa/walk.scm 43 */
				obj_t BgL_g1283z00_1626;

				BgL_g1283z00_1626 = BGl_getzd2classzd2listz00zzobject_classz00();
				{
					obj_t BgL_l1281z00_1628;

					BgL_l1281z00_1628 = BgL_g1283z00_1626;
				BgL_zc3z04anonymousza31330ze3z87_1629:
					if (PAIRP(BgL_l1281z00_1628))
						{	/* Isa/walk.scm 43 */
							{	/* Isa/walk.scm 43 */
								obj_t BgL_arg1332z00_1631;

								BgL_arg1332z00_1631 = CAR(BgL_l1281z00_1628);
								BGl_setzd2classzd2depthz12z12zzisa_walkz00(
									((BgL_typez00_bglt) BgL_arg1332z00_1631));
							}
							{
								obj_t BgL_l1281z00_2574;

								BgL_l1281z00_2574 = CDR(BgL_l1281z00_1628);
								BgL_l1281z00_1628 = BgL_l1281z00_2574;
								goto BgL_zc3z04anonymousza31330ze3z87_1629;
							}
						}
					else
						{	/* Isa/walk.scm 43 */
							((bool_t) 1);
						}
				}
			}
			{
				obj_t BgL_l1284z00_1635;

				BgL_l1284z00_1635 = BgL_globalsz00_17;
			BgL_zc3z04anonymousza31334ze3z87_1636:
				if (PAIRP(BgL_l1284z00_1635))
					{	/* Isa/walk.scm 44 */
						BGl_isazd2funz12zc0zzisa_walkz00(CAR(BgL_l1284z00_1635));
						{
							obj_t BgL_l1284z00_2580;

							BgL_l1284z00_2580 = CDR(BgL_l1284z00_1635);
							BgL_l1284z00_1635 = BgL_l1284z00_2580;
							goto BgL_zc3z04anonymousza31334ze3z87_1636;
						}
					}
				else
					{	/* Isa/walk.scm 44 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Isa/walk.scm 45 */
					{	/* Isa/walk.scm 45 */
						obj_t BgL_port1286z00_1643;

						{	/* Isa/walk.scm 45 */
							obj_t BgL_tmpz00_2585;

							BgL_tmpz00_2585 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1286z00_1643 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2585);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1286z00_1643);
						bgl_display_string(BGl_string1776z00zzisa_walkz00,
							BgL_port1286z00_1643);
						{	/* Isa/walk.scm 45 */
							obj_t BgL_arg1342z00_1644;

							{	/* Isa/walk.scm 45 */
								bool_t BgL_test1827z00_2590;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Isa/walk.scm 45 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Isa/walk.scm 45 */
												BgL_test1827z00_2590 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Isa/walk.scm 45 */
												BgL_test1827z00_2590 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Isa/walk.scm 45 */
										BgL_test1827z00_2590 = ((bool_t) 0);
									}
								if (BgL_test1827z00_2590)
									{	/* Isa/walk.scm 45 */
										BgL_arg1342z00_1644 = BGl_string1777z00zzisa_walkz00;
									}
								else
									{	/* Isa/walk.scm 45 */
										BgL_arg1342z00_1644 = BGl_string1778z00zzisa_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1342z00_1644, BgL_port1286z00_1643);
						}
						bgl_display_string(BGl_string1779z00zzisa_walkz00,
							BgL_port1286z00_1643);
						bgl_display_char(((unsigned char) 10), BgL_port1286z00_1643);
					}
					{	/* Isa/walk.scm 45 */
						obj_t BgL_list1346z00_1648;

						BgL_list1346z00_1648 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1346z00_1648);
					}
				}
			else
				{	/* Isa/walk.scm 45 */
					obj_t BgL_g1121z00_1649;
					obj_t BgL_g1122z00_1650;

					{	/* Isa/walk.scm 45 */
						obj_t BgL_list1363z00_1663;

						BgL_list1363z00_1663 =
							MAKE_YOUNG_PAIR(BGl_clearzd2isazd2cachez12zd2envzc0zzisa_walkz00,
							BNIL);
						BgL_g1121z00_1649 = BgL_list1363z00_1663;
					}
					BgL_g1122z00_1650 = CNST_TABLE_REF(2);
					{
						obj_t BgL_hooksz00_1652;
						obj_t BgL_hnamesz00_1653;

						BgL_hooksz00_1652 = BgL_g1121z00_1649;
						BgL_hnamesz00_1653 = BgL_g1122z00_1650;
					BgL_zc3z04anonymousza31347ze3z87_1654:
						if (NULLP(BgL_hooksz00_1652))
							{	/* Isa/walk.scm 45 */
								return BgL_globalsz00_17;
							}
						else
							{	/* Isa/walk.scm 45 */
								bool_t BgL_test1832z00_2609;

								{	/* Isa/walk.scm 45 */
									obj_t BgL_fun1362z00_1661;

									BgL_fun1362z00_1661 = CAR(((obj_t) BgL_hooksz00_1652));
									BgL_test1832z00_2609 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1362z00_1661));
								}
								if (BgL_test1832z00_2609)
									{	/* Isa/walk.scm 45 */
										obj_t BgL_arg1351z00_1658;
										obj_t BgL_arg1352z00_1659;

										BgL_arg1351z00_1658 = CDR(((obj_t) BgL_hooksz00_1652));
										BgL_arg1352z00_1659 = CDR(((obj_t) BgL_hnamesz00_1653));
										{
											obj_t BgL_hnamesz00_2621;
											obj_t BgL_hooksz00_2620;

											BgL_hooksz00_2620 = BgL_arg1351z00_1658;
											BgL_hnamesz00_2621 = BgL_arg1352z00_1659;
											BgL_hnamesz00_1653 = BgL_hnamesz00_2621;
											BgL_hooksz00_1652 = BgL_hooksz00_2620;
											goto BgL_zc3z04anonymousza31347ze3z87_1654;
										}
									}
								else
									{	/* Isa/walk.scm 45 */
										obj_t BgL_arg1361z00_1660;

										BgL_arg1361z00_1660 = CAR(((obj_t) BgL_hnamesz00_1653));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string1780z00zzisa_walkz00, BgL_arg1361z00_1660);
									}
							}
					}
				}
		}

	}



/* &isa-walk! */
	obj_t BGl_z62isazd2walkz12za2zzisa_walkz00(obj_t BgL_envz00_2353,
		obj_t BgL_globalsz00_2354)
	{
		{	/* Isa/walk.scm 41 */
			return BGl_isazd2walkz12zc0zzisa_walkz00(BgL_globalsz00_2354);
		}

	}



/* &init-isa-cache! */
	obj_t BGl_z62initzd2isazd2cachez12z70zzisa_walkz00(obj_t BgL_envz00_2355)
	{
		{	/* Isa/walk.scm 58 */
			{	/* Isa/walk.scm 59 */
				bool_t BgL_test1833z00_2626;

				{	/* Isa/walk.scm 59 */
					obj_t BgL_objz00_2377;

					BgL_objz00_2377 = BGl_za2isazf2cdepthza2zf2zzisa_walkz00;
					{	/* Isa/walk.scm 59 */
						obj_t BgL_classz00_2378;

						BgL_classz00_2378 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_objz00_2377))
							{	/* Isa/walk.scm 59 */
								BgL_objectz00_bglt BgL_arg1807z00_2379;

								BgL_arg1807z00_2379 = (BgL_objectz00_bglt) (BgL_objz00_2377);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Isa/walk.scm 59 */
										long BgL_idxz00_2380;

										BgL_idxz00_2380 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2379);
										BgL_test1833z00_2626 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2380 + 2L)) == BgL_classz00_2378);
									}
								else
									{	/* Isa/walk.scm 59 */
										bool_t BgL_res1759z00_2383;

										{	/* Isa/walk.scm 59 */
											obj_t BgL_oclassz00_2384;

											{	/* Isa/walk.scm 59 */
												obj_t BgL_arg1815z00_2385;
												long BgL_arg1816z00_2386;

												BgL_arg1815z00_2385 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Isa/walk.scm 59 */
													long BgL_arg1817z00_2387;

													BgL_arg1817z00_2387 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2379);
													BgL_arg1816z00_2386 =
														(BgL_arg1817z00_2387 - OBJECT_TYPE);
												}
												BgL_oclassz00_2384 =
													VECTOR_REF(BgL_arg1815z00_2385, BgL_arg1816z00_2386);
											}
											{	/* Isa/walk.scm 59 */
												bool_t BgL__ortest_1115z00_2388;

												BgL__ortest_1115z00_2388 =
													(BgL_classz00_2378 == BgL_oclassz00_2384);
												if (BgL__ortest_1115z00_2388)
													{	/* Isa/walk.scm 59 */
														BgL_res1759z00_2383 = BgL__ortest_1115z00_2388;
													}
												else
													{	/* Isa/walk.scm 59 */
														long BgL_odepthz00_2389;

														{	/* Isa/walk.scm 59 */
															obj_t BgL_arg1804z00_2390;

															BgL_arg1804z00_2390 = (BgL_oclassz00_2384);
															BgL_odepthz00_2389 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2390);
														}
														if ((2L < BgL_odepthz00_2389))
															{	/* Isa/walk.scm 59 */
																obj_t BgL_arg1802z00_2391;

																{	/* Isa/walk.scm 59 */
																	obj_t BgL_arg1803z00_2392;

																	BgL_arg1803z00_2392 = (BgL_oclassz00_2384);
																	BgL_arg1802z00_2391 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2392,
																		2L);
																}
																BgL_res1759z00_2383 =
																	(BgL_arg1802z00_2391 == BgL_classz00_2378);
															}
														else
															{	/* Isa/walk.scm 59 */
																BgL_res1759z00_2383 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1833z00_2626 = BgL_res1759z00_2383;
									}
							}
						else
							{	/* Isa/walk.scm 59 */
								BgL_test1833z00_2626 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1833z00_2626)
					{	/* Isa/walk.scm 59 */
						BFALSE;
					}
				else
					{	/* Isa/walk.scm 59 */
						BGl_za2isazf2cdepthza2zf2zzisa_walkz00 =
							BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(3),
							CNST_TABLE_REF(4));
						BGl_za2isazd2objectzf2cdepthza2z20zzisa_walkz00 =
							BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(5),
							CNST_TABLE_REF(4));
						BGl_za2isazf2finalza2zf2zzisa_walkz00 =
							BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(6),
							CNST_TABLE_REF(4));
						BGl_za2isazd2objectzf2finalza2z20zzisa_walkz00 =
							BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(7),
							CNST_TABLE_REF(4));
					}
			}
			return BUNSPEC;
		}

	}



/* &clear-isa-cache! */
	obj_t BGl_z62clearzd2isazd2cachez12z70zzisa_walkz00(obj_t BgL_envz00_2356)
	{
		{	/* Isa/walk.scm 69 */
			BGl_za2isazf2cdepthza2zf2zzisa_walkz00 = BFALSE;
			BGl_za2isazd2objectzf2cdepthza2z20zzisa_walkz00 = BFALSE;
			BGl_za2isazf2finalza2zf2zzisa_walkz00 = BFALSE;
			return (BGl_za2isazd2objectzf2finalza2z20zzisa_walkz00 = BFALSE, BUNSPEC);
		}

	}



/* set-class-depth! */
	long BGl_setzd2classzd2depthz12z12zzisa_walkz00(BgL_typez00_bglt
		BgL_claza7za7z00_18)
	{
		{	/* Isa/walk.scm 78 */
			{	/* Isa/walk.scm 80 */
				bool_t BgL_test1838z00_2661;

				{	/* Isa/walk.scm 80 */
					long BgL_arg1378z00_1673;

					{
						BgL_tclassz00_bglt BgL_auxz00_2662;

						{
							obj_t BgL_auxz00_2663;

							{	/* Isa/walk.scm 80 */
								BgL_objectz00_bglt BgL_tmpz00_2664;

								BgL_tmpz00_2664 = ((BgL_objectz00_bglt) BgL_claza7za7z00_18);
								BgL_auxz00_2663 = BGL_OBJECT_WIDENING(BgL_tmpz00_2664);
							}
							BgL_auxz00_2662 = ((BgL_tclassz00_bglt) BgL_auxz00_2663);
						}
						BgL_arg1378z00_1673 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2662))->BgL_depthz00);
					}
					BgL_test1838z00_2661 = (BgL_arg1378z00_1673 == 0L);
				}
				if (BgL_test1838z00_2661)
					{	/* Isa/walk.scm 81 */
						bool_t BgL_test1839z00_2670;

						{	/* Isa/walk.scm 81 */
							obj_t BgL_arg1377z00_1672;

							{
								BgL_tclassz00_bglt BgL_auxz00_2671;

								{
									obj_t BgL_auxz00_2672;

									{	/* Isa/walk.scm 81 */
										BgL_objectz00_bglt BgL_tmpz00_2673;

										BgL_tmpz00_2673 =
											((BgL_objectz00_bglt) BgL_claza7za7z00_18);
										BgL_auxz00_2672 = BGL_OBJECT_WIDENING(BgL_tmpz00_2673);
									}
									BgL_auxz00_2671 = ((BgL_tclassz00_bglt) BgL_auxz00_2672);
								}
								BgL_arg1377z00_1672 =
									(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2671))->
									BgL_itszd2superzd2);
							}
							{	/* Isa/walk.scm 81 */
								obj_t BgL_classz00_2042;

								BgL_classz00_2042 = BGl_tclassz00zzobject_classz00;
								if (BGL_OBJECTP(BgL_arg1377z00_1672))
									{	/* Isa/walk.scm 81 */
										BgL_objectz00_bglt BgL_arg1807z00_2044;

										BgL_arg1807z00_2044 =
											(BgL_objectz00_bglt) (BgL_arg1377z00_1672);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Isa/walk.scm 81 */
												long BgL_idxz00_2050;

												BgL_idxz00_2050 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2044);
												BgL_test1839z00_2670 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2050 + 2L)) == BgL_classz00_2042);
											}
										else
											{	/* Isa/walk.scm 81 */
												bool_t BgL_res1760z00_2075;

												{	/* Isa/walk.scm 81 */
													obj_t BgL_oclassz00_2058;

													{	/* Isa/walk.scm 81 */
														obj_t BgL_arg1815z00_2066;
														long BgL_arg1816z00_2067;

														BgL_arg1815z00_2066 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Isa/walk.scm 81 */
															long BgL_arg1817z00_2068;

															BgL_arg1817z00_2068 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2044);
															BgL_arg1816z00_2067 =
																(BgL_arg1817z00_2068 - OBJECT_TYPE);
														}
														BgL_oclassz00_2058 =
															VECTOR_REF(BgL_arg1815z00_2066,
															BgL_arg1816z00_2067);
													}
													{	/* Isa/walk.scm 81 */
														bool_t BgL__ortest_1115z00_2059;

														BgL__ortest_1115z00_2059 =
															(BgL_classz00_2042 == BgL_oclassz00_2058);
														if (BgL__ortest_1115z00_2059)
															{	/* Isa/walk.scm 81 */
																BgL_res1760z00_2075 = BgL__ortest_1115z00_2059;
															}
														else
															{	/* Isa/walk.scm 81 */
																long BgL_odepthz00_2060;

																{	/* Isa/walk.scm 81 */
																	obj_t BgL_arg1804z00_2061;

																	BgL_arg1804z00_2061 = (BgL_oclassz00_2058);
																	BgL_odepthz00_2060 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2061);
																}
																if ((2L < BgL_odepthz00_2060))
																	{	/* Isa/walk.scm 81 */
																		obj_t BgL_arg1802z00_2063;

																		{	/* Isa/walk.scm 81 */
																			obj_t BgL_arg1803z00_2064;

																			BgL_arg1803z00_2064 =
																				(BgL_oclassz00_2058);
																			BgL_arg1802z00_2063 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2064, 2L);
																		}
																		BgL_res1760z00_2075 =
																			(BgL_arg1802z00_2063 ==
																			BgL_classz00_2042);
																	}
																else
																	{	/* Isa/walk.scm 81 */
																		BgL_res1760z00_2075 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1839z00_2670 = BgL_res1760z00_2075;
											}
									}
								else
									{	/* Isa/walk.scm 81 */
										BgL_test1839z00_2670 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test1839z00_2670)
							{
								long BgL_auxz00_2706;
								BgL_tclassz00_bglt BgL_auxz00_2700;

								{	/* Isa/walk.scm 82 */
									long BgL_arg1375z00_1670;

									{	/* Isa/walk.scm 82 */
										obj_t BgL_arg1376z00_1671;

										{
											BgL_tclassz00_bglt BgL_auxz00_2707;

											{
												obj_t BgL_auxz00_2708;

												{	/* Isa/walk.scm 82 */
													BgL_objectz00_bglt BgL_tmpz00_2709;

													BgL_tmpz00_2709 =
														((BgL_objectz00_bglt) BgL_claza7za7z00_18);
													BgL_auxz00_2708 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2709);
												}
												BgL_auxz00_2707 =
													((BgL_tclassz00_bglt) BgL_auxz00_2708);
											}
											BgL_arg1376z00_1671 =
												(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2707))->
												BgL_itszd2superzd2);
										}
										BgL_arg1375z00_1670 =
											BGl_setzd2classzd2depthz12z12zzisa_walkz00(
											((BgL_typez00_bglt) BgL_arg1376z00_1671));
									}
									BgL_auxz00_2706 = (1L + BgL_arg1375z00_1670);
								}
								{
									obj_t BgL_auxz00_2701;

									{	/* Isa/walk.scm 82 */
										BgL_objectz00_bglt BgL_tmpz00_2702;

										BgL_tmpz00_2702 =
											((BgL_objectz00_bglt) BgL_claza7za7z00_18);
										BgL_auxz00_2701 = BGL_OBJECT_WIDENING(BgL_tmpz00_2702);
									}
									BgL_auxz00_2700 = ((BgL_tclassz00_bglt) BgL_auxz00_2701);
								}
								((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2700))->
										BgL_depthz00) = ((long) BgL_auxz00_2706), BUNSPEC);
							}
						else
							{	/* Isa/walk.scm 81 */
								BINT(0L);
							}
					}
				else
					{	/* Isa/walk.scm 80 */
						BFALSE;
					}
			}
			{
				BgL_tclassz00_bglt BgL_auxz00_2719;

				{
					obj_t BgL_auxz00_2720;

					{	/* Isa/walk.scm 84 */
						BgL_objectz00_bglt BgL_tmpz00_2721;

						BgL_tmpz00_2721 = ((BgL_objectz00_bglt) BgL_claza7za7z00_18);
						BgL_auxz00_2720 = BGL_OBJECT_WIDENING(BgL_tmpz00_2721);
					}
					BgL_auxz00_2719 = ((BgL_tclassz00_bglt) BgL_auxz00_2720);
				}
				return (((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2719))->BgL_depthz00);
			}
		}

	}



/* isa-fun! */
	obj_t BGl_isazd2funz12zc0zzisa_walkz00(obj_t BgL_varz00_19)
	{
		{	/* Isa/walk.scm 89 */
			{	/* Isa/walk.scm 90 */
				obj_t BgL_arg1379z00_1674;

				BgL_arg1379z00_1674 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_19)))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1379z00_1674);
			}
			{	/* Isa/walk.scm 91 */
				BgL_valuez00_bglt BgL_funz00_1675;

				BgL_funz00_1675 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_19)))->BgL_valuez00);
				{	/* Isa/walk.scm 91 */
					obj_t BgL_bodyz00_1676;

					BgL_bodyz00_1676 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1675)))->BgL_bodyz00);
					{	/* Isa/walk.scm 92 */

						BgL_bodyz00_1676 =
							((obj_t)
							BGl_isaz12z12zzisa_walkz00(
								((BgL_nodez00_bglt) BgL_bodyz00_1676)));
						BGl_leavezd2functionzd2zztools_errorz00();
						return BgL_varz00_19;
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzisa_walkz00(void)
	{
		{	/* Isa/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzisa_walkz00(void)
	{
		{	/* Isa/walk.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_isaz12zd2envzc0zzisa_walkz00, BGl_proc1781z00zzisa_walkz00,
				BGl_nodez00zzast_nodez00, BGl_string1782z00zzisa_walkz00);
		}

	}



/* &isa!1289 */
	obj_t BGl_z62isaz121289z70zzisa_walkz00(obj_t BgL_envz00_2360,
		obj_t BgL_nodez00_2361)
	{
		{	/* Isa/walk.scm 100 */
			return
				((obj_t)
				BGl_walk0z12z12zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_2361),
					BGl_isaz12zd2envzc0zzisa_walkz00));
		}

	}



/* isa! */
	BgL_nodez00_bglt BGl_isaz12z12zzisa_walkz00(BgL_nodez00_bglt BgL_nodez00_20)
	{
		{	/* Isa/walk.scm 100 */
			{	/* Isa/walk.scm 100 */
				obj_t BgL_method1290z00_1692;

				{	/* Isa/walk.scm 100 */
					obj_t BgL_res1765z00_2114;

					{	/* Isa/walk.scm 100 */
						long BgL_objzd2classzd2numz00_2085;

						BgL_objzd2classzd2numz00_2085 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_20));
						{	/* Isa/walk.scm 100 */
							obj_t BgL_arg1811z00_2086;

							BgL_arg1811z00_2086 =
								PROCEDURE_REF(BGl_isaz12zd2envzc0zzisa_walkz00, (int) (1L));
							{	/* Isa/walk.scm 100 */
								int BgL_offsetz00_2089;

								BgL_offsetz00_2089 = (int) (BgL_objzd2classzd2numz00_2085);
								{	/* Isa/walk.scm 100 */
									long BgL_offsetz00_2090;

									BgL_offsetz00_2090 =
										((long) (BgL_offsetz00_2089) - OBJECT_TYPE);
									{	/* Isa/walk.scm 100 */
										long BgL_modz00_2091;

										BgL_modz00_2091 =
											(BgL_offsetz00_2090 >> (int) ((long) ((int) (4L))));
										{	/* Isa/walk.scm 100 */
											long BgL_restz00_2093;

											BgL_restz00_2093 =
												(BgL_offsetz00_2090 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Isa/walk.scm 100 */

												{	/* Isa/walk.scm 100 */
													obj_t BgL_bucketz00_2095;

													BgL_bucketz00_2095 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2086), BgL_modz00_2091);
													BgL_res1765z00_2114 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2095), BgL_restz00_2093);
					}}}}}}}}
					BgL_method1290z00_1692 = BgL_res1765z00_2114;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1290z00_1692,
						((obj_t) BgL_nodez00_20)));
			}
		}

	}



/* &isa! */
	BgL_nodez00_bglt BGl_z62isaz12z70zzisa_walkz00(obj_t BgL_envz00_2358,
		obj_t BgL_nodez00_2359)
	{
		{	/* Isa/walk.scm 100 */
			return BGl_isaz12z12zzisa_walkz00(((BgL_nodez00_bglt) BgL_nodez00_2359));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzisa_walkz00(void)
	{
		{	/* Isa/walk.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_isaz12zd2envzc0zzisa_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc1783z00zzisa_walkz00, BGl_string1784z00zzisa_walkz00);
		}

	}



/* &isa!-app1292 */
	BgL_nodez00_bglt BGl_z62isaz12zd2app1292za2zzisa_walkz00(obj_t
		BgL_envz00_2363, obj_t BgL_nodez00_2364)
	{
		{	/* Isa/walk.scm 106 */
			{
				obj_t BgL_typz00_2396;

				{	/* Isa/walk.scm 118 */
					obj_t BgL_typz00_2400;

					BgL_typz00_2400 =
						BGl_isazd2ofzd2zztype_miscz00(
						((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2364)));
					if (CBOOL(BgL_typz00_2400))
						{	/* Isa/walk.scm 119 */
							{	/* Isa/walk.scm 124 */
								bool_t BgL_test1845z00_2780;

								BgL_typz00_2396 = BgL_typz00_2400;
								{	/* Isa/walk.scm 115 */
									bool_t BgL_test1846z00_2781;

									{	/* Isa/walk.scm 115 */
										obj_t BgL_arg1602z00_2397;

										{	/* Isa/walk.scm 115 */
											BgL_globalz00_bglt BgL_arg1605z00_2398;

											{
												BgL_tclassz00_bglt BgL_auxz00_2782;

												{
													obj_t BgL_auxz00_2783;

													{	/* Isa/walk.scm 115 */
														BgL_objectz00_bglt BgL_tmpz00_2784;

														BgL_tmpz00_2784 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typz00_2396));
														BgL_auxz00_2783 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2784);
													}
													BgL_auxz00_2782 =
														((BgL_tclassz00_bglt) BgL_auxz00_2783);
												}
												BgL_arg1605z00_2398 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2782))->
													BgL_holderz00);
											}
											BgL_arg1602z00_2397 =
												(((BgL_globalz00_bglt) COBJECT(BgL_arg1605z00_2398))->
												BgL_importz00);
										}
										BgL_test1846z00_2781 =
											(BgL_arg1602z00_2397 == CNST_TABLE_REF(8));
									}
									if (BgL_test1846z00_2781)
										{	/* Isa/walk.scm 116 */
											obj_t BgL_arg1595z00_2399;

											{
												BgL_tclassz00_bglt BgL_auxz00_2793;

												{
													obj_t BgL_auxz00_2794;

													{	/* Isa/walk.scm 116 */
														BgL_objectz00_bglt BgL_tmpz00_2795;

														BgL_tmpz00_2795 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typz00_2396));
														BgL_auxz00_2794 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2795);
													}
													BgL_auxz00_2793 =
														((BgL_tclassz00_bglt) BgL_auxz00_2794);
												}
												BgL_arg1595z00_2399 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2793))->
													BgL_subclassesz00);
											}
											BgL_test1845z00_2780 = NULLP(BgL_arg1595z00_2399);
										}
									else
										{	/* Isa/walk.scm 115 */
											BgL_test1845z00_2780 = ((bool_t) 0);
										}
								}
								if (BgL_test1845z00_2780)
									{	/* Isa/walk.scm 126 */
										bool_t BgL_test1847z00_2802;

										{	/* Isa/walk.scm 126 */
											BgL_typez00_bglt BgL_arg1473z00_2401;

											{	/* Isa/walk.scm 126 */
												obj_t BgL_arg1485z00_2402;

												BgL_arg1485z00_2402 =
													CAR(
													(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2364)))->
														BgL_argsz00));
												BgL_arg1473z00_2401 =
													BGl_uncastedzd2typeze70z35zzisa_walkz00((
														(BgL_nodez00_bglt) BgL_arg1485z00_2402));
											}
											{	/* Isa/walk.scm 126 */
												obj_t BgL_classz00_2403;

												BgL_classz00_2403 = BGl_tclassz00zzobject_classz00;
												{	/* Isa/walk.scm 126 */
													BgL_objectz00_bglt BgL_arg1807z00_2404;

													{	/* Isa/walk.scm 126 */
														obj_t BgL_tmpz00_2808;

														BgL_tmpz00_2808 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg1473z00_2401));
														BgL_arg1807z00_2404 =
															(BgL_objectz00_bglt) (BgL_tmpz00_2808);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Isa/walk.scm 126 */
															long BgL_idxz00_2405;

															BgL_idxz00_2405 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2404);
															BgL_test1847z00_2802 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2405 + 2L)) == BgL_classz00_2403);
														}
													else
														{	/* Isa/walk.scm 126 */
															bool_t BgL_res1767z00_2408;

															{	/* Isa/walk.scm 126 */
																obj_t BgL_oclassz00_2409;

																{	/* Isa/walk.scm 126 */
																	obj_t BgL_arg1815z00_2410;
																	long BgL_arg1816z00_2411;

																	BgL_arg1815z00_2410 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Isa/walk.scm 126 */
																		long BgL_arg1817z00_2412;

																		BgL_arg1817z00_2412 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2404);
																		BgL_arg1816z00_2411 =
																			(BgL_arg1817z00_2412 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2409 =
																		VECTOR_REF(BgL_arg1815z00_2410,
																		BgL_arg1816z00_2411);
																}
																{	/* Isa/walk.scm 126 */
																	bool_t BgL__ortest_1115z00_2413;

																	BgL__ortest_1115z00_2413 =
																		(BgL_classz00_2403 == BgL_oclassz00_2409);
																	if (BgL__ortest_1115z00_2413)
																		{	/* Isa/walk.scm 126 */
																			BgL_res1767z00_2408 =
																				BgL__ortest_1115z00_2413;
																		}
																	else
																		{	/* Isa/walk.scm 126 */
																			long BgL_odepthz00_2414;

																			{	/* Isa/walk.scm 126 */
																				obj_t BgL_arg1804z00_2415;

																				BgL_arg1804z00_2415 =
																					(BgL_oclassz00_2409);
																				BgL_odepthz00_2414 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2415);
																			}
																			if ((2L < BgL_odepthz00_2414))
																				{	/* Isa/walk.scm 126 */
																					obj_t BgL_arg1802z00_2416;

																					{	/* Isa/walk.scm 126 */
																						obj_t BgL_arg1803z00_2417;

																						BgL_arg1803z00_2417 =
																							(BgL_oclassz00_2409);
																						BgL_arg1802z00_2416 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2417, 2L);
																					}
																					BgL_res1767z00_2408 =
																						(BgL_arg1802z00_2416 ==
																						BgL_classz00_2403);
																				}
																			else
																				{	/* Isa/walk.scm 126 */
																					BgL_res1767z00_2408 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1847z00_2802 = BgL_res1767z00_2408;
														}
												}
											}
										}
										if (BgL_test1847z00_2802)
											{	/* Isa/walk.scm 127 */
												BgL_refz00_bglt BgL_nfunz00_2418;

												{	/* Isa/walk.scm 127 */
													BgL_nodez00_bglt BgL_duplicated1131z00_2419;
													BgL_refz00_bglt BgL_new1129z00_2420;

													BgL_duplicated1131z00_2419 =
														((BgL_nodez00_bglt)
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2364)))->
															BgL_funz00));
													{	/* Isa/walk.scm 127 */
														BgL_refz00_bglt BgL_new1133z00_2421;

														BgL_new1133z00_2421 =
															((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_refz00_bgl))));
														{	/* Isa/walk.scm 127 */
															long BgL_arg1454z00_2422;

															BgL_arg1454z00_2422 =
																BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1133z00_2421),
																BgL_arg1454z00_2422);
														}
														{	/* Isa/walk.scm 127 */
															BgL_objectz00_bglt BgL_tmpz00_2838;

															BgL_tmpz00_2838 =
																((BgL_objectz00_bglt) BgL_new1133z00_2421);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2838, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1133z00_2421);
														BgL_new1129z00_2420 = BgL_new1133z00_2421;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1129z00_2420)))->
															BgL_locz00) =
														((obj_t) (((BgL_nodez00_bglt)
																	COBJECT(BgL_duplicated1131z00_2419))->
																BgL_locz00)), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1129z00_2420)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																	COBJECT(BgL_duplicated1131z00_2419))->
																BgL_typez00)), BUNSPEC);
													((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																		BgL_new1129z00_2420)))->BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BGl_za2isazd2objectzf2finalza2z20zzisa_walkz00)),
														BUNSPEC);
													BgL_nfunz00_2418 = BgL_new1129z00_2420;
												}
												{	/* Isa/walk.scm 129 */
													obj_t BgL_arg1434z00_2423;
													BgL_castz00_bglt BgL_arg1437z00_2424;

													BgL_arg1434z00_2423 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2364)))->
														BgL_argsz00);
													{	/* Isa/walk.scm 130 */
														BgL_castz00_bglt BgL_new1135z00_2425;

														{	/* Isa/walk.scm 130 */
															BgL_castz00_bglt BgL_new1134z00_2426;

															BgL_new1134z00_2426 =
																((BgL_castz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_castz00_bgl))));
															{	/* Isa/walk.scm 130 */
																long BgL_arg1453z00_2427;

																BgL_arg1453z00_2427 =
																	BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1134z00_2426),
																	BgL_arg1453z00_2427);
															}
															{	/* Isa/walk.scm 130 */
																BgL_objectz00_bglt BgL_tmpz00_2857;

																BgL_tmpz00_2857 =
																	((BgL_objectz00_bglt) BgL_new1134z00_2426);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2857,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1134z00_2426);
															BgL_new1135z00_2425 = BgL_new1134z00_2426;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1135z00_2425)))->
																BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1135z00_2425)))->BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BGl_getzd2objectzd2typez00zztype_cachez00())),
															BUNSPEC);
														{
															BgL_nodez00_bglt BgL_auxz00_2867;

															{	/* Isa/walk.scm 131 */
																obj_t BgL_pairz00_2428;

																BgL_pairz00_2428 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_2364)))->
																	BgL_argsz00);
																BgL_auxz00_2867 =
																	((BgL_nodez00_bglt) CAR(BgL_pairz00_2428));
															}
															((((BgL_castz00_bglt)
																		COBJECT(BgL_new1135z00_2425))->BgL_argz00) =
																((BgL_nodez00_bglt) BgL_auxz00_2867), BUNSPEC);
														}
														BgL_arg1437z00_2424 = BgL_new1135z00_2425;
													}
													{	/* Isa/walk.scm 129 */
														obj_t BgL_auxz00_2875;
														obj_t BgL_tmpz00_2873;

														BgL_auxz00_2875 = ((obj_t) BgL_arg1437z00_2424);
														BgL_tmpz00_2873 = ((obj_t) BgL_arg1434z00_2423);
														SET_CAR(BgL_tmpz00_2873, BgL_auxz00_2875);
												}}
												((((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2364)))->
														BgL_funz00) =
													((BgL_varz00_bglt) ((BgL_varz00_bglt)
															BgL_nfunz00_2418)), BUNSPEC);
											}
										else
											{	/* Isa/walk.scm 134 */
												BgL_refz00_bglt BgL_nfunz00_2429;

												{	/* Isa/walk.scm 134 */
													BgL_nodez00_bglt BgL_duplicated1138z00_2430;
													BgL_refz00_bglt BgL_new1136z00_2431;

													BgL_duplicated1138z00_2430 =
														((BgL_nodez00_bglt)
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2364)))->
															BgL_funz00));
													{	/* Isa/walk.scm 134 */
														BgL_refz00_bglt BgL_new1139z00_2432;

														BgL_new1139z00_2432 =
															((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_refz00_bgl))));
														{	/* Isa/walk.scm 134 */
															long BgL_arg1472z00_2433;

															BgL_arg1472z00_2433 =
																BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1139z00_2432),
																BgL_arg1472z00_2433);
														}
														{	/* Isa/walk.scm 134 */
															BgL_objectz00_bglt BgL_tmpz00_2888;

															BgL_tmpz00_2888 =
																((BgL_objectz00_bglt) BgL_new1139z00_2432);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2888, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1139z00_2432);
														BgL_new1136z00_2431 = BgL_new1139z00_2432;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1136z00_2431)))->
															BgL_locz00) =
														((obj_t) (((BgL_nodez00_bglt)
																	COBJECT(BgL_duplicated1138z00_2430))->
																BgL_locz00)), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1136z00_2431)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																	COBJECT(BgL_duplicated1138z00_2430))->
																BgL_typez00)), BUNSPEC);
													((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																		BgL_new1136z00_2431)))->BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BGl_za2isazf2finalza2zf2zzisa_walkz00)),
														BUNSPEC);
													BgL_nfunz00_2429 = BgL_new1136z00_2431;
												}
												((((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_2364)))->
														BgL_funz00) =
													((BgL_varz00_bglt) ((BgL_varz00_bglt)
															BgL_nfunz00_2429)), BUNSPEC);
									}}
								else
									{	/* Isa/walk.scm 137 */
										bool_t BgL_test1851z00_2904;

										{	/* Isa/walk.scm 137 */
											long BgL_a1287z00_2434;

											{
												BgL_tclassz00_bglt BgL_auxz00_2905;

												{
													obj_t BgL_auxz00_2906;

													{	/* Isa/walk.scm 137 */
														BgL_objectz00_bglt BgL_tmpz00_2907;

														BgL_tmpz00_2907 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typz00_2400));
														BgL_auxz00_2906 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2907);
													}
													BgL_auxz00_2905 =
														((BgL_tclassz00_bglt) BgL_auxz00_2906);
												}
												BgL_a1287z00_2434 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2905))->
													BgL_depthz00);
											}
											{	/* Isa/walk.scm 137 */
												obj_t BgL_b1288z00_2435;

												BgL_b1288z00_2435 =
													BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
													(9));
												{	/* Isa/walk.scm 137 */

													if (INTEGERP(BgL_b1288z00_2435))
														{	/* Isa/walk.scm 137 */
															BgL_test1851z00_2904 =
																(BgL_a1287z00_2434 <
																(long) CINT(BgL_b1288z00_2435));
														}
													else
														{	/* Isa/walk.scm 137 */
															BgL_test1851z00_2904 =
																BGl_2zc3zc3zz__r4_numbers_6_5z00(BINT
																(BgL_a1287z00_2434), BgL_b1288z00_2435);
														}
												}
											}
										}
										if (BgL_test1851z00_2904)
											{	/* Isa/walk.scm 138 */
												bool_t BgL_test1853z00_2921;

												{	/* Isa/walk.scm 138 */
													BgL_typez00_bglt BgL_arg1576z00_2436;

													{	/* Isa/walk.scm 138 */
														obj_t BgL_arg1584z00_2437;

														BgL_arg1584z00_2437 =
															CAR(
															(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_nodez00_2364)))->
																BgL_argsz00));
														BgL_arg1576z00_2436 =
															BGl_uncastedzd2typeze70z35zzisa_walkz00((
																(BgL_nodez00_bglt) BgL_arg1584z00_2437));
													}
													{	/* Isa/walk.scm 138 */
														obj_t BgL_classz00_2438;

														BgL_classz00_2438 = BGl_tclassz00zzobject_classz00;
														{	/* Isa/walk.scm 138 */
															BgL_objectz00_bglt BgL_arg1807z00_2439;

															{	/* Isa/walk.scm 138 */
																obj_t BgL_tmpz00_2927;

																BgL_tmpz00_2927 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_arg1576z00_2436));
																BgL_arg1807z00_2439 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_2927);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Isa/walk.scm 138 */
																	long BgL_idxz00_2440;

																	BgL_idxz00_2440 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2439);
																	BgL_test1853z00_2921 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2440 + 2L)) ==
																		BgL_classz00_2438);
																}
															else
																{	/* Isa/walk.scm 138 */
																	bool_t BgL_res1768z00_2443;

																	{	/* Isa/walk.scm 138 */
																		obj_t BgL_oclassz00_2444;

																		{	/* Isa/walk.scm 138 */
																			obj_t BgL_arg1815z00_2445;
																			long BgL_arg1816z00_2446;

																			BgL_arg1815z00_2445 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Isa/walk.scm 138 */
																				long BgL_arg1817z00_2447;

																				BgL_arg1817z00_2447 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2439);
																				BgL_arg1816z00_2446 =
																					(BgL_arg1817z00_2447 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2444 =
																				VECTOR_REF(BgL_arg1815z00_2445,
																				BgL_arg1816z00_2446);
																		}
																		{	/* Isa/walk.scm 138 */
																			bool_t BgL__ortest_1115z00_2448;

																			BgL__ortest_1115z00_2448 =
																				(BgL_classz00_2438 ==
																				BgL_oclassz00_2444);
																			if (BgL__ortest_1115z00_2448)
																				{	/* Isa/walk.scm 138 */
																					BgL_res1768z00_2443 =
																						BgL__ortest_1115z00_2448;
																				}
																			else
																				{	/* Isa/walk.scm 138 */
																					long BgL_odepthz00_2449;

																					{	/* Isa/walk.scm 138 */
																						obj_t BgL_arg1804z00_2450;

																						BgL_arg1804z00_2450 =
																							(BgL_oclassz00_2444);
																						BgL_odepthz00_2449 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2450);
																					}
																					if ((2L < BgL_odepthz00_2449))
																						{	/* Isa/walk.scm 138 */
																							obj_t BgL_arg1802z00_2451;

																							{	/* Isa/walk.scm 138 */
																								obj_t BgL_arg1803z00_2452;

																								BgL_arg1803z00_2452 =
																									(BgL_oclassz00_2444);
																								BgL_arg1802z00_2451 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2452, 2L);
																							}
																							BgL_res1768z00_2443 =
																								(BgL_arg1802z00_2451 ==
																								BgL_classz00_2438);
																						}
																					else
																						{	/* Isa/walk.scm 138 */
																							BgL_res1768z00_2443 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1853z00_2921 = BgL_res1768z00_2443;
																}
														}
													}
												}
												if (BgL_test1853z00_2921)
													{	/* Isa/walk.scm 139 */
														BgL_refz00_bglt BgL_nfunz00_2453;
														BgL_literalz00_bglt BgL_depthz00_2454;
														BgL_castz00_bglt BgL_arg0z00_2455;

														{	/* Isa/walk.scm 139 */
															BgL_nodez00_bglt BgL_duplicated1142z00_2456;
															BgL_refz00_bglt BgL_new1140z00_2457;

															BgL_duplicated1142z00_2456 =
																((BgL_nodez00_bglt)
																(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_2364)))->
																	BgL_funz00));
															{	/* Isa/walk.scm 139 */
																BgL_refz00_bglt BgL_new1143z00_2458;

																BgL_new1143z00_2458 =
																	((BgL_refz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_refz00_bgl))));
																{	/* Isa/walk.scm 139 */
																	long BgL_arg1546z00_2459;

																	BgL_arg1546z00_2459 =
																		BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1143z00_2458),
																		BgL_arg1546z00_2459);
																}
																{	/* Isa/walk.scm 139 */
																	BgL_objectz00_bglt BgL_tmpz00_2957;

																	BgL_tmpz00_2957 =
																		((BgL_objectz00_bglt) BgL_new1143z00_2458);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2957,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1143z00_2458);
																BgL_new1140z00_2457 = BgL_new1143z00_2458;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1140z00_2457)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(BgL_duplicated1142z00_2456))->
																		BgL_locz00)), BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1140z00_2457)))->BgL_typez00) =
																((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																			COBJECT(BgL_duplicated1142z00_2456))->
																		BgL_typez00)), BUNSPEC);
															((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																				BgL_new1140z00_2457)))->
																	BgL_variablez00) =
																((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																		BGl_za2isazd2objectzf2cdepthza2z20zzisa_walkz00)),
																BUNSPEC);
															BgL_nfunz00_2453 = BgL_new1140z00_2457;
														}
														{	/* Isa/walk.scm 141 */
															BgL_literalz00_bglt BgL_new1145z00_2460;

															{	/* Isa/walk.scm 142 */
																BgL_literalz00_bglt BgL_new1144z00_2461;

																BgL_new1144z00_2461 =
																	((BgL_literalz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_literalz00_bgl))));
																{	/* Isa/walk.scm 142 */
																	long BgL_arg1553z00_2462;

																	BgL_arg1553z00_2462 =
																		BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1144z00_2461),
																		BgL_arg1553z00_2462);
																}
																{	/* Isa/walk.scm 142 */
																	BgL_objectz00_bglt BgL_tmpz00_2974;

																	BgL_tmpz00_2974 =
																		((BgL_objectz00_bglt) BgL_new1144z00_2461);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2974,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1144z00_2461);
																BgL_new1145z00_2460 = BgL_new1144z00_2461;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1145z00_2460)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_appz00_bglt)
																						BgL_nodez00_2364))))->BgL_locz00)),
																BUNSPEC);
															{
																BgL_typez00_bglt BgL_auxz00_2983;

																{	/* Isa/walk.scm 143 */
																	long BgL_arg1552z00_2463;

																	{
																		BgL_tclassz00_bglt BgL_auxz00_2985;

																		{
																			obj_t BgL_auxz00_2986;

																			{	/* Isa/walk.scm 143 */
																				BgL_objectz00_bglt BgL_tmpz00_2987;

																				BgL_tmpz00_2987 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt) BgL_typz00_2400));
																				BgL_auxz00_2986 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_2987);
																			}
																			BgL_auxz00_2985 =
																				((BgL_tclassz00_bglt) BgL_auxz00_2986);
																		}
																		BgL_arg1552z00_2463 =
																			(((BgL_tclassz00_bglt)
																				COBJECT(BgL_auxz00_2985))->
																			BgL_depthz00);
																	}
																	BgL_auxz00_2983 =
																		BGl_getzd2typezd2atomz00zztype_typeofz00
																		(BINT(BgL_arg1552z00_2463));
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1145z00_2460)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_auxz00_2983),
																	BUNSPEC);
															}
															{
																obj_t BgL_auxz00_2996;

																{	/* Isa/walk.scm 144 */
																	long BgL_tmpz00_2998;

																	{
																		BgL_tclassz00_bglt BgL_auxz00_2999;

																		{
																			obj_t BgL_auxz00_3000;

																			{	/* Isa/walk.scm 144 */
																				BgL_objectz00_bglt BgL_tmpz00_3001;

																				BgL_tmpz00_3001 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt) BgL_typz00_2400));
																				BgL_auxz00_3000 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_3001);
																			}
																			BgL_auxz00_2999 =
																				((BgL_tclassz00_bglt) BgL_auxz00_3000);
																		}
																		BgL_tmpz00_2998 =
																			(((BgL_tclassz00_bglt)
																				COBJECT(BgL_auxz00_2999))->
																			BgL_depthz00);
																	}
																	BgL_auxz00_2996 = BINT(BgL_tmpz00_2998);
																}
																((((BgL_atomz00_bglt) COBJECT(
																				((BgL_atomz00_bglt)
																					BgL_new1145z00_2460)))->
																		BgL_valuez00) =
																	((obj_t) BgL_auxz00_2996), BUNSPEC);
															}
															BgL_depthz00_2454 = BgL_new1145z00_2460;
														}
														{	/* Isa/walk.scm 145 */
															BgL_castz00_bglt BgL_new1147z00_2464;

															{	/* Isa/walk.scm 145 */
																BgL_castz00_bglt BgL_new1146z00_2465;

																BgL_new1146z00_2465 =
																	((BgL_castz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_castz00_bgl))));
																{	/* Isa/walk.scm 145 */
																	long BgL_arg1561z00_2466;

																	BgL_arg1561z00_2466 =
																		BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1146z00_2465),
																		BgL_arg1561z00_2466);
																}
																{	/* Isa/walk.scm 145 */
																	BgL_objectz00_bglt BgL_tmpz00_3013;

																	BgL_tmpz00_3013 =
																		((BgL_objectz00_bglt) BgL_new1146z00_2465);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3013,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1146z00_2465);
																BgL_new1147z00_2464 = BgL_new1146z00_2465;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1147z00_2464)))->BgL_locz00) =
																((obj_t) BFALSE), BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1147z00_2464)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BGl_getzd2objectzd2typez00zztype_cachez00
																		())), BUNSPEC);
															{
																BgL_nodez00_bglt BgL_auxz00_3023;

																{	/* Isa/walk.scm 146 */
																	obj_t BgL_pairz00_2467;

																	BgL_pairz00_2467 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_nodez00_2364)))->
																		BgL_argsz00);
																	BgL_auxz00_3023 =
																		((BgL_nodez00_bglt) CAR(BgL_pairz00_2467));
																}
																((((BgL_castz00_bglt)
																			COBJECT(BgL_new1147z00_2464))->
																		BgL_argz00) =
																	((BgL_nodez00_bglt) BgL_auxz00_3023),
																	BUNSPEC);
															}
															BgL_arg0z00_2455 = BgL_new1147z00_2464;
														}
														((((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_nodez00_2364)))->
																BgL_funz00) =
															((BgL_varz00_bglt) ((BgL_varz00_bglt)
																	BgL_nfunz00_2453)), BUNSPEC);
														{
															obj_t BgL_auxz00_3032;

															{	/* Isa/walk.scm 149 */
																obj_t BgL_arg1516z00_2468;

																{	/* Isa/walk.scm 149 */
																	obj_t BgL_pairz00_2469;

																	BgL_pairz00_2469 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_nodez00_2364)))->
																		BgL_argsz00);
																	{	/* Isa/walk.scm 149 */
																		obj_t BgL_pairz00_2470;

																		BgL_pairz00_2470 = CDR(BgL_pairz00_2469);
																		BgL_arg1516z00_2468 = CAR(BgL_pairz00_2470);
																}}
																{	/* Isa/walk.scm 149 */
																	obj_t BgL_list1517z00_2471;

																	{	/* Isa/walk.scm 149 */
																		obj_t BgL_arg1535z00_2472;

																		{	/* Isa/walk.scm 149 */
																			obj_t BgL_arg1540z00_2473;

																			BgL_arg1540z00_2473 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_depthz00_2454), BNIL);
																			BgL_arg1535z00_2472 =
																				MAKE_YOUNG_PAIR(BgL_arg1516z00_2468,
																				BgL_arg1540z00_2473);
																		}
																		BgL_list1517z00_2471 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg0z00_2455),
																			BgL_arg1535z00_2472);
																	}
																	BgL_auxz00_3032 = BgL_list1517z00_2471;
															}}
															((((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_2364)))->
																	BgL_argsz00) =
																((obj_t) BgL_auxz00_3032), BUNSPEC);
													}}
												else
													{	/* Isa/walk.scm 150 */
														BgL_refz00_bglt BgL_nfunz00_2474;

														{	/* Isa/walk.scm 150 */
															BgL_nodez00_bglt BgL_duplicated1150z00_2475;
															BgL_refz00_bglt BgL_new1148z00_2476;

															BgL_duplicated1150z00_2475 =
																((BgL_nodez00_bglt)
																(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_2364)))->
																	BgL_funz00));
															{	/* Isa/walk.scm 150 */
																BgL_refz00_bglt BgL_new1151z00_2477;

																BgL_new1151z00_2477 =
																	((BgL_refz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_refz00_bgl))));
																{	/* Isa/walk.scm 150 */
																	long BgL_arg1575z00_2478;

																	BgL_arg1575z00_2478 =
																		BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1151z00_2477),
																		BgL_arg1575z00_2478);
																}
																{	/* Isa/walk.scm 150 */
																	BgL_objectz00_bglt BgL_tmpz00_3051;

																	BgL_tmpz00_3051 =
																		((BgL_objectz00_bglt) BgL_new1151z00_2477);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3051,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1151z00_2477);
																BgL_new1148z00_2476 = BgL_new1151z00_2477;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1148z00_2476)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(BgL_duplicated1150z00_2475))->
																		BgL_locz00)), BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1148z00_2476)))->BgL_typez00) =
																((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																			COBJECT(BgL_duplicated1150z00_2475))->
																		BgL_typez00)), BUNSPEC);
															((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																				BgL_new1148z00_2476)))->
																	BgL_variablez00) =
																((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																		BGl_za2isazf2cdepthza2zf2zzisa_walkz00)),
																BUNSPEC);
															BgL_nfunz00_2474 = BgL_new1148z00_2476;
														}
														{	/* Isa/walk.scm 150 */
															BgL_literalz00_bglt BgL_depthz00_2479;

															{	/* Isa/walk.scm 152 */
																BgL_literalz00_bglt BgL_new1153z00_2480;

																{	/* Isa/walk.scm 153 */
																	BgL_literalz00_bglt BgL_new1152z00_2481;

																	BgL_new1152z00_2481 =
																		((BgL_literalz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_literalz00_bgl))));
																	{	/* Isa/walk.scm 153 */
																		long BgL_arg1573z00_2482;

																		BgL_arg1573z00_2482 =
																			BGL_CLASS_NUM
																			(BGl_literalz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1152z00_2481),
																			BgL_arg1573z00_2482);
																	}
																	{	/* Isa/walk.scm 153 */
																		BgL_objectz00_bglt BgL_tmpz00_3068;

																		BgL_tmpz00_3068 =
																			((BgL_objectz00_bglt)
																			BgL_new1152z00_2481);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3068,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1152z00_2481);
																	BgL_new1153z00_2480 = BgL_new1152z00_2481;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1153z00_2480)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_appz00_bglt)
																							BgL_nodez00_2364))))->
																			BgL_locz00)), BUNSPEC);
																{
																	BgL_typez00_bglt BgL_auxz00_3077;

																	{	/* Isa/walk.scm 154 */
																		long BgL_arg1571z00_2483;

																		{
																			BgL_tclassz00_bglt BgL_auxz00_3079;

																			{
																				obj_t BgL_auxz00_3080;

																				{	/* Isa/walk.scm 154 */
																					BgL_objectz00_bglt BgL_tmpz00_3081;

																					BgL_tmpz00_3081 =
																						((BgL_objectz00_bglt)
																						((BgL_typez00_bglt)
																							BgL_typz00_2400));
																					BgL_auxz00_3080 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_3081);
																				}
																				BgL_auxz00_3079 =
																					((BgL_tclassz00_bglt)
																					BgL_auxz00_3080);
																			}
																			BgL_arg1571z00_2483 =
																				(((BgL_tclassz00_bglt)
																					COBJECT(BgL_auxz00_3079))->
																				BgL_depthz00);
																		}
																		BgL_auxz00_3077 =
																			BGl_getzd2typezd2atomz00zztype_typeofz00
																			(BINT(BgL_arg1571z00_2483));
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1153z00_2480)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_auxz00_3077),
																		BUNSPEC);
																}
																{
																	obj_t BgL_auxz00_3090;

																	{	/* Isa/walk.scm 155 */
																		long BgL_tmpz00_3092;

																		{
																			BgL_tclassz00_bglt BgL_auxz00_3093;

																			{
																				obj_t BgL_auxz00_3094;

																				{	/* Isa/walk.scm 155 */
																					BgL_objectz00_bglt BgL_tmpz00_3095;

																					BgL_tmpz00_3095 =
																						((BgL_objectz00_bglt)
																						((BgL_typez00_bglt)
																							BgL_typz00_2400));
																					BgL_auxz00_3094 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_3095);
																				}
																				BgL_auxz00_3093 =
																					((BgL_tclassz00_bglt)
																					BgL_auxz00_3094);
																			}
																			BgL_tmpz00_3092 =
																				(((BgL_tclassz00_bglt)
																					COBJECT(BgL_auxz00_3093))->
																				BgL_depthz00);
																		}
																		BgL_auxz00_3090 = BINT(BgL_tmpz00_3092);
																	}
																	((((BgL_atomz00_bglt) COBJECT(
																					((BgL_atomz00_bglt)
																						BgL_new1153z00_2480)))->
																			BgL_valuez00) =
																		((obj_t) BgL_auxz00_3090), BUNSPEC);
																}
																BgL_depthz00_2479 = BgL_new1153z00_2480;
															}
															{	/* Isa/walk.scm 152 */

																((((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt) BgL_nodez00_2364)))->
																		BgL_funz00) =
																	((BgL_varz00_bglt) ((BgL_varz00_bglt)
																			BgL_nfunz00_2474)), BUNSPEC);
																{
																	obj_t BgL_auxz00_3106;

																	{	/* Isa/walk.scm 157 */
																		obj_t BgL_arg1564z00_2484;
																		obj_t BgL_arg1565z00_2485;

																		BgL_arg1564z00_2484 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_nodez00_2364)))->BgL_argsz00);
																		{	/* Isa/walk.scm 157 */
																			obj_t BgL_list1566z00_2486;

																			BgL_list1566z00_2486 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_depthz00_2479), BNIL);
																			BgL_arg1565z00_2485 =
																				BgL_list1566z00_2486;
																		}
																		BgL_auxz00_3106 =
																			BGl_appendzd221011zd2zzisa_walkz00
																			(BgL_arg1564z00_2484,
																			BgL_arg1565z00_2485);
																	}
																	((((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_nodez00_2364)))->BgL_argsz00) =
																		((obj_t) BgL_auxz00_3106), BUNSPEC);
											}}}}}
										else
											{	/* Isa/walk.scm 137 */
												BFALSE;
											}
									}
							}
							return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2364));
						}
					else
						{	/* Isa/walk.scm 119 */
							return
								BGl_walk0z12z12zzast_walkz00(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_2364)),
								BGl_isaz12zd2envzc0zzisa_walkz00);
						}
				}
			}
		}

	}



/* uncasted-type~0 */
	BgL_typez00_bglt BGl_uncastedzd2typeze70z35zzisa_walkz00(BgL_nodez00_bglt
		BgL_nodez00_1783)
	{
		{	/* Isa/walk.scm 112 */
			{	/* Isa/walk.scm 109 */
				bool_t BgL_test1857z00_3119;

				{	/* Isa/walk.scm 109 */
					obj_t BgL_classz00_2115;

					BgL_classz00_2115 = BGl_castz00zzast_nodez00;
					{	/* Isa/walk.scm 109 */
						BgL_objectz00_bglt BgL_arg1807z00_2117;

						{	/* Isa/walk.scm 109 */
							obj_t BgL_tmpz00_3120;

							BgL_tmpz00_3120 = ((obj_t) BgL_nodez00_1783);
							BgL_arg1807z00_2117 = (BgL_objectz00_bglt) (BgL_tmpz00_3120);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Isa/walk.scm 109 */
								long BgL_idxz00_2123;

								BgL_idxz00_2123 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2117);
								BgL_test1857z00_3119 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2123 + 2L)) == BgL_classz00_2115);
							}
						else
							{	/* Isa/walk.scm 109 */
								bool_t BgL_res1766z00_2148;

								{	/* Isa/walk.scm 109 */
									obj_t BgL_oclassz00_2131;

									{	/* Isa/walk.scm 109 */
										obj_t BgL_arg1815z00_2139;
										long BgL_arg1816z00_2140;

										BgL_arg1815z00_2139 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Isa/walk.scm 109 */
											long BgL_arg1817z00_2141;

											BgL_arg1817z00_2141 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2117);
											BgL_arg1816z00_2140 = (BgL_arg1817z00_2141 - OBJECT_TYPE);
										}
										BgL_oclassz00_2131 =
											VECTOR_REF(BgL_arg1815z00_2139, BgL_arg1816z00_2140);
									}
									{	/* Isa/walk.scm 109 */
										bool_t BgL__ortest_1115z00_2132;

										BgL__ortest_1115z00_2132 =
											(BgL_classz00_2115 == BgL_oclassz00_2131);
										if (BgL__ortest_1115z00_2132)
											{	/* Isa/walk.scm 109 */
												BgL_res1766z00_2148 = BgL__ortest_1115z00_2132;
											}
										else
											{	/* Isa/walk.scm 109 */
												long BgL_odepthz00_2133;

												{	/* Isa/walk.scm 109 */
													obj_t BgL_arg1804z00_2134;

													BgL_arg1804z00_2134 = (BgL_oclassz00_2131);
													BgL_odepthz00_2133 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2134);
												}
												if ((2L < BgL_odepthz00_2133))
													{	/* Isa/walk.scm 109 */
														obj_t BgL_arg1802z00_2136;

														{	/* Isa/walk.scm 109 */
															obj_t BgL_arg1803z00_2137;

															BgL_arg1803z00_2137 = (BgL_oclassz00_2131);
															BgL_arg1802z00_2136 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2137,
																2L);
														}
														BgL_res1766z00_2148 =
															(BgL_arg1802z00_2136 == BgL_classz00_2115);
													}
												else
													{	/* Isa/walk.scm 109 */
														BgL_res1766z00_2148 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test1857z00_3119 = BgL_res1766z00_2148;
							}
					}
				}
				if (BgL_test1857z00_3119)
					{	/* Isa/walk.scm 109 */
						return
							(((BgL_nodez00_bglt) COBJECT(
									(((BgL_castz00_bglt) COBJECT(
												((BgL_castz00_bglt) BgL_nodez00_1783)))->BgL_argz00)))->
							BgL_typez00);
					}
				else
					{	/* Isa/walk.scm 109 */
						return
							(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_1783))->BgL_typez00);
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzisa_walkz00(void)
	{
		{	/* Isa/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1785z00zzisa_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
