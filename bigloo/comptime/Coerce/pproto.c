/*===========================================================================*/
/*   (Coerce/pproto.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Coerce/pproto.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_COERCE_PPROTO_TYPE_DEFINITIONS
#define BGL_COERCE_PPROTO_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;


#endif													// BGL_COERCE_PPROTO_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzcoerce_pprotoz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_pvariablezd2protozd2zzcoerce_pprotoz00(long,
		BgL_variablez00_bglt);
	static obj_t BGl_z62pvariablezd2protozb0zzcoerce_pprotoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcoerce_pprotoz00(void);
	static obj_t BGl_z62inczd2ppmargez12za2zzcoerce_pprotoz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzcoerce_pprotoz00(void);
	BGL_EXPORTED_DECL obj_t BGl_deczd2ppmargez12zc0zzcoerce_pprotoz00(void);
	static obj_t BGl_objectzd2initzd2zzcoerce_pprotoz00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_pfunctionzd2protozd2zzcoerce_pprotoz00(long,
		BgL_variablez00_bglt);
	static obj_t BGl_z62pfunctionzd2protozb0zzcoerce_pprotoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzcoerce_pprotoz00(void);
	static obj_t BGl_oldzd2margezd2stringz00zzcoerce_pprotoz00 = BUNSPEC;
	BGL_IMPORT obj_t make_string(long, unsigned char);
	extern obj_t
		BGl_variablezd2typezd2ze3stringze3zztype_pptypez00(BgL_variablez00_bglt);
	extern obj_t BGl_za2verboseza2z00zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcoerce_pprotoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_pptypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	static obj_t BGl_z62deczd2ppmargez12za2zzcoerce_pprotoz00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_pprotoz00(void);
	static obj_t BGl_z62resetzd2ppmargez12za2zzcoerce_pprotoz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzcoerce_pprotoz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcoerce_pprotoz00(void);
	extern obj_t
		BGl_functionzd2typezd2ze3stringze3zztype_pptypez00(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_inczd2ppmargez12zc0zzcoerce_pprotoz00(void);
	static long BGl_oldzd2margezd2zzcoerce_pprotoz00 = 0L;
	static long BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00 = 0L;
	BGL_EXPORTED_DECL obj_t BGl_resetzd2ppmargez12zc0zzcoerce_pprotoz00(void);
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1159z00zzcoerce_pprotoz00,
		BgL_bgl_string1159za700za7za7c1162za7, "", 0);
	      DEFINE_STRING(BGl_string1160z00zzcoerce_pprotoz00,
		BgL_bgl_string1160za700za7za7c1163za7, " : ", 3);
	      DEFINE_STRING(BGl_string1161z00zzcoerce_pprotoz00,
		BgL_bgl_string1161za700za7za7c1164za7, "coerce_pproto", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pfunctionzd2protozd2envz00zzcoerce_pprotoz00,
		BgL_bgl_za762pfunctionza7d2p1165z00,
		BGl_z62pfunctionzd2protozb0zzcoerce_pprotoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_inczd2ppmargez12zd2envz12zzcoerce_pprotoz00,
		BgL_bgl_za762incza7d2ppmarge1166z00,
		BGl_z62inczd2ppmargez12za2zzcoerce_pprotoz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_resetzd2ppmargez12zd2envz12zzcoerce_pprotoz00,
		BgL_bgl_za762resetza7d2ppmar1167z00,
		BGl_z62resetzd2ppmargez12za2zzcoerce_pprotoz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pvariablezd2protozd2envz00zzcoerce_pprotoz00,
		BgL_bgl_za762pvariableza7d2p1168z00,
		BGl_z62pvariablezd2protozb0zzcoerce_pprotoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_deczd2ppmargez12zd2envz12zzcoerce_pprotoz00,
		BgL_bgl_za762decza7d2ppmarge1169z00,
		BGl_z62deczd2ppmargez12za2zzcoerce_pprotoz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcoerce_pprotoz00));
		     ADD_ROOT((void *) (&BGl_oldzd2margezd2stringz00zzcoerce_pprotoz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcoerce_pprotoz00(long
		BgL_checksumz00_530, char *BgL_fromz00_531)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcoerce_pprotoz00))
				{
					BGl_requirezd2initializa7ationz75zzcoerce_pprotoz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcoerce_pprotoz00();
					BGl_libraryzd2moduleszd2initz00zzcoerce_pprotoz00();
					BGl_importedzd2moduleszd2initz00zzcoerce_pprotoz00();
					return BGl_toplevelzd2initzd2zzcoerce_pprotoz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"coerce_pproto");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"coerce_pproto");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"coerce_pproto");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "coerce_pproto");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 15 */
			BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00 = 8L;
			BGl_oldzd2margezd2zzcoerce_pprotoz00 = -1L;
			return (BGl_oldzd2margezd2stringz00zzcoerce_pprotoz00 =
				BGl_string1159z00zzcoerce_pprotoz00, BUNSPEC);
		}

	}



/* reset-ppmarge! */
	BGL_EXPORTED_DEF obj_t BGl_resetzd2ppmargez12zc0zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 31 */
			return (BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00 = 8L, BUNSPEC);
		}

	}



/* &reset-ppmarge! */
	obj_t BGl_z62resetzd2ppmargez12za2zzcoerce_pprotoz00(obj_t BgL_envz00_521)
	{
		{	/* Coerce/pproto.scm 31 */
			return BGl_resetzd2ppmargez12zc0zzcoerce_pprotoz00();
		}

	}



/* inc-ppmarge! */
	BGL_EXPORTED_DEF obj_t BGl_inczd2ppmargez12zc0zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 37 */
			return (BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00 =
				(1L + BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00), BUNSPEC);
		}

	}



/* &inc-ppmarge! */
	obj_t BGl_z62inczd2ppmargez12za2zzcoerce_pprotoz00(obj_t BgL_envz00_522)
	{
		{	/* Coerce/pproto.scm 37 */
			return BGl_inczd2ppmargez12zc0zzcoerce_pprotoz00();
		}

	}



/* dec-ppmarge! */
	BGL_EXPORTED_DEF obj_t BGl_deczd2ppmargez12zc0zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 43 */
			return (BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00 =
				(BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00 - 1L), BUNSPEC);
		}

	}



/* &dec-ppmarge! */
	obj_t BGl_z62deczd2ppmargez12za2zzcoerce_pprotoz00(obj_t BgL_envz00_523)
	{
		{	/* Coerce/pproto.scm 43 */
			return BGl_deczd2ppmargez12zc0zzcoerce_pprotoz00();
		}

	}



/* pfunction-proto */
	BGL_EXPORTED_DEF obj_t BGl_pfunctionzd2protozd2zzcoerce_pprotoz00(long
		BgL_levelz00_3, BgL_variablez00_bglt BgL_variablez00_4)
	{
		{	/* Coerce/pproto.scm 56 */
			{	/* Coerce/pproto.scm 57 */
				obj_t BgL_margez00_446;

				if (
					(BGl_oldzd2margezd2zzcoerce_pprotoz00 ==
						BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00))
					{	/* Coerce/pproto.scm 57 */
						BgL_margez00_446 = BGl_oldzd2margezd2stringz00zzcoerce_pprotoz00;
					}
				else
					{	/* Coerce/pproto.scm 59 */
						obj_t BgL_margez00_459;

						BgL_margez00_459 =
							make_string(BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00,
							((unsigned char) ' '));
						BGl_oldzd2margezd2zzcoerce_pprotoz00 =
							BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00;
						BGl_oldzd2margezd2stringz00zzcoerce_pprotoz00 = BgL_margez00_459;
						BgL_margez00_446 = BgL_margez00_459;
					}
				{	/* Coerce/pproto.scm 63 */
					bool_t BgL_test1172z00_552;

					if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
						{	/* Coerce/pproto.scm 63 */
							BgL_test1172z00_552 =
								(BgL_levelz00_3 <=
								(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
						}
					else
						{	/* Coerce/pproto.scm 63 */
							BgL_test1172z00_552 =
								BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(BgL_levelz00_3),
								BGl_za2verboseza2z00zzengine_paramz00);
						}
					if (BgL_test1172z00_552)
						{	/* Coerce/pproto.scm 65 */
							obj_t BgL_arg1097z00_450;
							obj_t BgL_arg1102z00_451;

							BgL_arg1097z00_450 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_variablez00_4));
							BgL_arg1102z00_451 =
								BGl_functionzd2typezd2ze3stringze3zztype_pptypez00
								(BgL_variablez00_4);
							{	/* Coerce/pproto.scm 63 */
								obj_t BgL_list1103z00_452;

								{	/* Coerce/pproto.scm 63 */
									obj_t BgL_arg1104z00_453;

									{	/* Coerce/pproto.scm 63 */
										obj_t BgL_arg1114z00_454;

										{	/* Coerce/pproto.scm 63 */
											obj_t BgL_arg1115z00_455;

											{	/* Coerce/pproto.scm 63 */
												obj_t BgL_arg1122z00_456;

												BgL_arg1122z00_456 =
													MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
												BgL_arg1115z00_455 =
													MAKE_YOUNG_PAIR(BgL_arg1102z00_451,
													BgL_arg1122z00_456);
											}
											BgL_arg1114z00_454 =
												MAKE_YOUNG_PAIR(BGl_string1160z00zzcoerce_pprotoz00,
												BgL_arg1115z00_455);
										}
										BgL_arg1104z00_453 =
											MAKE_YOUNG_PAIR(BgL_arg1097z00_450, BgL_arg1114z00_454);
									}
									BgL_list1103z00_452 =
										MAKE_YOUNG_PAIR(BgL_margez00_446, BgL_arg1104z00_453);
								}
								return
									BGl_verbosez00zztools_speekz00(BINT(BgL_levelz00_3),
									BgL_list1103z00_452);
							}
						}
					else
						{	/* Coerce/pproto.scm 63 */
							return BUNSPEC;
						}
				}
			}
		}

	}



/* &pfunction-proto */
	obj_t BGl_z62pfunctionzd2protozb0zzcoerce_pprotoz00(obj_t BgL_envz00_524,
		obj_t BgL_levelz00_525, obj_t BgL_variablez00_526)
	{
		{	/* Coerce/pproto.scm 56 */
			return
				BGl_pfunctionzd2protozd2zzcoerce_pprotoz00(
				(long) CINT(BgL_levelz00_525),
				((BgL_variablez00_bglt) BgL_variablez00_526));
		}

	}



/* pvariable-proto */
	BGL_EXPORTED_DEF obj_t BGl_pvariablezd2protozd2zzcoerce_pprotoz00(long
		BgL_levelz00_5, BgL_variablez00_bglt BgL_variablez00_6)
	{
		{	/* Coerce/pproto.scm 72 */
			{	/* Coerce/pproto.scm 73 */
				obj_t BgL_margez00_460;

				BgL_margez00_460 =
					make_string(BGl_za2ppzd2margeza2zd2zzcoerce_pprotoz00,
					((unsigned char) ' '));
				{	/* Coerce/pproto.scm 74 */
					bool_t BgL_test1174z00_574;

					if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
						{	/* Coerce/pproto.scm 74 */
							BgL_test1174z00_574 =
								(BgL_levelz00_5 <=
								(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
						}
					else
						{	/* Coerce/pproto.scm 74 */
							BgL_test1174z00_574 =
								BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(BgL_levelz00_5),
								BGl_za2verboseza2z00zzengine_paramz00);
						}
					if (BgL_test1174z00_574)
						{	/* Coerce/pproto.scm 76 */
							obj_t BgL_arg1126z00_464;
							obj_t BgL_arg1127z00_465;

							BgL_arg1126z00_464 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_variablez00_6));
							BgL_arg1127z00_465 =
								BGl_variablezd2typezd2ze3stringze3zztype_pptypez00
								(BgL_variablez00_6);
							{	/* Coerce/pproto.scm 74 */
								obj_t BgL_list1128z00_466;

								{	/* Coerce/pproto.scm 74 */
									obj_t BgL_arg1129z00_467;

									{	/* Coerce/pproto.scm 74 */
										obj_t BgL_arg1131z00_468;

										{	/* Coerce/pproto.scm 74 */
											obj_t BgL_arg1132z00_469;

											{	/* Coerce/pproto.scm 74 */
												obj_t BgL_arg1137z00_470;

												BgL_arg1137z00_470 =
													MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
												BgL_arg1132z00_469 =
													MAKE_YOUNG_PAIR(BgL_arg1127z00_465,
													BgL_arg1137z00_470);
											}
											BgL_arg1131z00_468 =
												MAKE_YOUNG_PAIR(BGl_string1160z00zzcoerce_pprotoz00,
												BgL_arg1132z00_469);
										}
										BgL_arg1129z00_467 =
											MAKE_YOUNG_PAIR(BgL_arg1126z00_464, BgL_arg1131z00_468);
									}
									BgL_list1128z00_466 =
										MAKE_YOUNG_PAIR(BgL_margez00_460, BgL_arg1129z00_467);
								}
								return
									BGl_verbosez00zztools_speekz00(BINT(BgL_levelz00_5),
									BgL_list1128z00_466);
							}
						}
					else
						{	/* Coerce/pproto.scm 74 */
							return BUNSPEC;
						}
				}
			}
		}

	}



/* &pvariable-proto */
	obj_t BGl_z62pvariablezd2protozb0zzcoerce_pprotoz00(obj_t BgL_envz00_527,
		obj_t BgL_levelz00_528, obj_t BgL_variablez00_529)
	{
		{	/* Coerce/pproto.scm 72 */
			return
				BGl_pvariablezd2protozd2zzcoerce_pprotoz00(
				(long) CINT(BgL_levelz00_528),
				((BgL_variablez00_bglt) BgL_variablez00_529));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcoerce_pprotoz00(void)
	{
		{	/* Coerce/pproto.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1161z00zzcoerce_pprotoz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1161z00zzcoerce_pprotoz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1161z00zzcoerce_pprotoz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1161z00zzcoerce_pprotoz00));
			BGl_modulezd2initializa7ationz75zztype_pptypez00(220178216L,
				BSTRING_TO_STRING(BGl_string1161z00zzcoerce_pprotoz00));
			return
				BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1161z00zzcoerce_pprotoz00));
		}

	}

#ifdef __cplusplus
}
#endif
