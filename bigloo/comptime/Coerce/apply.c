/*===========================================================================*/
/*   (Coerce/apply.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Coerce/apply.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_COERCE_APPLY_TYPE_DEFINITIONS
#define BGL_COERCE_APPLY_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;


#endif													// BGL_COERCE_APPLY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2unsafezd2arityza2zd2zzengine_paramz00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcoerce_applyz00 = BUNSPEC;
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzcoerce_applyz00(void);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzcoerce_applyz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcoerce_applyz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern BgL_nodez00_bglt BGl_lvtypezd2nodezd2zzast_lvtypez00(BgL_nodez00_bglt);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcoerce_applyz00(void);
	extern BgL_nodez00_bglt
		BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2appzd2ly1236z70zzcoerce_applyz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static BgL_nodez00_bglt BGl_makezd2errorzd2nodez00zzcoerce_applyz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcoerce_applyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_convertz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcoerce_applyz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_applyz00(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzcoerce_applyz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcoerce_applyz00(void);
	extern obj_t BGl_currentzd2functionzd2zztools_errorz00(void);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_convertz12z12zzcoerce_convertz00(BgL_nodez00_bglt,
		BgL_typez00_bglt, BgL_typez00_bglt, bool_t);
	static obj_t __cnst[17];


	   
		 
		DEFINE_STRING(BGl_string1766z00zzcoerce_applyz00,
		BgL_bgl_string1766za700za7za7c1772za7, "Wrong number of arguments", 25);
	      DEFINE_STRING(BGl_string1768z00zzcoerce_applyz00,
		BgL_bgl_string1768za700za7za7c1773za7, "coerce!", 7);
	      DEFINE_STRING(BGl_string1769z00zzcoerce_applyz00,
		BgL_bgl_string1769za700za7za7c1774za7, "coerce_apply", 12);
	      DEFINE_STRING(BGl_string1770z00zzcoerce_applyz00,
		BgL_bgl_string1770za700za7za7c1775za7,
		"let if correct-arity? ::int len length val fun begin failure _ quote @ error/location __error location bdb ",
		107);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1767z00zzcoerce_applyz00,
		BgL_bgl_za762coerceza712za7d2a1776za7,
		BGl_z62coercez12zd2appzd2ly1236z70zzcoerce_applyz00, 0L, BUNSPEC, 4);
	extern obj_t BGl_coercez12zd2envzc0zzcoerce_coercez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcoerce_applyz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcoerce_applyz00(long
		BgL_checksumz00_2013, char *BgL_fromz00_2014)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcoerce_applyz00))
				{
					BGl_requirezd2initializa7ationz75zzcoerce_applyz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcoerce_applyz00();
					BGl_libraryzd2moduleszd2initz00zzcoerce_applyz00();
					BGl_cnstzd2initzd2zzcoerce_applyz00();
					BGl_importedzd2moduleszd2initz00zzcoerce_applyz00();
					BGl_methodzd2initzd2zzcoerce_applyz00();
					return BGl_toplevelzd2initzd2zzcoerce_applyz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_applyz00(void)
	{
		{	/* Coerce/apply.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "coerce_apply");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "coerce_apply");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"coerce_apply");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "coerce_apply");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"coerce_apply");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "coerce_apply");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"coerce_apply");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "coerce_apply");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"coerce_apply");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "coerce_apply");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "coerce_apply");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcoerce_applyz00(void)
	{
		{	/* Coerce/apply.scm 15 */
			{	/* Coerce/apply.scm 15 */
				obj_t BgL_cportz00_1915;

				{	/* Coerce/apply.scm 15 */
					obj_t BgL_stringz00_1922;

					BgL_stringz00_1922 = BGl_string1770z00zzcoerce_applyz00;
					{	/* Coerce/apply.scm 15 */
						obj_t BgL_startz00_1923;

						BgL_startz00_1923 = BINT(0L);
						{	/* Coerce/apply.scm 15 */
							obj_t BgL_endz00_1924;

							BgL_endz00_1924 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1922)));
							{	/* Coerce/apply.scm 15 */

								BgL_cportz00_1915 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1922, BgL_startz00_1923, BgL_endz00_1924);
				}}}}
				{
					long BgL_iz00_1916;

					BgL_iz00_1916 = 16L;
				BgL_loopz00_1917:
					if ((BgL_iz00_1916 == -1L))
						{	/* Coerce/apply.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Coerce/apply.scm 15 */
							{	/* Coerce/apply.scm 15 */
								obj_t BgL_arg1771z00_1918;

								{	/* Coerce/apply.scm 15 */

									{	/* Coerce/apply.scm 15 */
										obj_t BgL_locationz00_1920;

										BgL_locationz00_1920 = BBOOL(((bool_t) 0));
										{	/* Coerce/apply.scm 15 */

											BgL_arg1771z00_1918 =
												BGl_readz00zz__readerz00(BgL_cportz00_1915,
												BgL_locationz00_1920);
										}
									}
								}
								{	/* Coerce/apply.scm 15 */
									int BgL_tmpz00_2044;

									BgL_tmpz00_2044 = (int) (BgL_iz00_1916);
									CNST_TABLE_SET(BgL_tmpz00_2044, BgL_arg1771z00_1918);
							}}
							{	/* Coerce/apply.scm 15 */
								int BgL_auxz00_1921;

								BgL_auxz00_1921 = (int) ((BgL_iz00_1916 - 1L));
								{
									long BgL_iz00_2049;

									BgL_iz00_2049 = (long) (BgL_auxz00_1921);
									BgL_iz00_1916 = BgL_iz00_2049;
									goto BgL_loopz00_1917;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcoerce_applyz00(void)
	{
		{	/* Coerce/apply.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcoerce_applyz00(void)
	{
		{	/* Coerce/apply.scm 15 */
			return BUNSPEC;
		}

	}



/* make-error-node */
	BgL_nodez00_bglt BGl_makezd2errorzd2nodez00zzcoerce_applyz00(obj_t
		BgL_errorzd2msgzd2_21, obj_t BgL_locz00_22, obj_t BgL_callerz00_23,
		obj_t BgL_toz00_24)
	{
		{	/* Coerce/apply.scm 97 */
			{	/* Coerce/apply.scm 98 */
				BgL_nodez00_bglt BgL_nodez00_1483;

				{	/* Coerce/apply.scm 99 */
					obj_t BgL_arg1268z00_1484;

					{	/* Coerce/apply.scm 99 */
						bool_t BgL_test1779z00_2052;

						{	/* Coerce/apply.scm 99 */
							bool_t BgL_test1780z00_2053;

							{	/* Coerce/apply.scm 99 */
								bool_t BgL_test1781z00_2054;

								if (
									((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) > 0L))
									{	/* Coerce/apply.scm 102 */
										obj_t BgL_arg1371z00_1535;

										{	/* Coerce/apply.scm 102 */
											obj_t BgL_arg1375z00_1536;

											BgL_arg1375z00_1536 =
												BGl_thezd2backendzd2zzbackend_backendz00();
											BgL_arg1371z00_1535 =
												(((BgL_backendz00_bglt) COBJECT(
														((BgL_backendz00_bglt) BgL_arg1375z00_1536)))->
												BgL_debugzd2supportzd2);
										}
										BgL_test1781z00_2054 =
											CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(CNST_TABLE_REF(0), BgL_arg1371z00_1535));
									}
								else
									{	/* Coerce/apply.scm 99 */
										BgL_test1781z00_2054 = ((bool_t) 0);
									}
								if (BgL_test1781z00_2054)
									{	/* Coerce/apply.scm 99 */
										BgL_test1780z00_2053 = ((bool_t) 1);
									}
								else
									{	/* Coerce/apply.scm 99 */
										BgL_test1780z00_2053 =
											(
											(long)
											CINT(BGl_za2compilerzd2debugza2zd2zzengine_paramz00) >
											0L);
							}}
							if (BgL_test1780z00_2053)
								{	/* Coerce/apply.scm 99 */
									if (STRUCTP(BgL_locz00_22))
										{	/* Coerce/apply.scm 104 */
											BgL_test1779z00_2052 =
												(STRUCT_KEY(BgL_locz00_22) == CNST_TABLE_REF(1));
										}
									else
										{	/* Coerce/apply.scm 104 */
											BgL_test1779z00_2052 = ((bool_t) 0);
										}
								}
							else
								{	/* Coerce/apply.scm 99 */
									BgL_test1779z00_2052 = ((bool_t) 0);
								}
						}
						if (BgL_test1779z00_2052)
							{	/* Coerce/apply.scm 106 */
								obj_t BgL_arg1312z00_1498;

								{	/* Coerce/apply.scm 106 */
									obj_t BgL_arg1314z00_1499;
									obj_t BgL_arg1315z00_1500;

									{	/* Coerce/apply.scm 106 */
										obj_t BgL_arg1316z00_1501;
										obj_t BgL_arg1317z00_1502;

										{	/* Coerce/apply.scm 106 */
											obj_t BgL_arg1318z00_1503;

											{	/* Coerce/apply.scm 106 */
												obj_t BgL_arg1319z00_1504;

												BgL_arg1319z00_1504 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BNIL);
												BgL_arg1318z00_1503 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
													BgL_arg1319z00_1504);
											}
											BgL_arg1316z00_1501 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1318z00_1503);
										}
										{	/* Coerce/apply.scm 107 */
											obj_t BgL_arg1320z00_1505;
											obj_t BgL_arg1321z00_1506;

											{	/* Coerce/apply.scm 107 */
												obj_t BgL_arg1322z00_1507;

												BgL_arg1322z00_1507 =
													BGl_currentzd2functionzd2zztools_errorz00();
												{	/* Coerce/apply.scm 107 */
													obj_t BgL_list1323z00_1508;

													{	/* Coerce/apply.scm 107 */
														obj_t BgL_arg1325z00_1509;

														BgL_arg1325z00_1509 =
															MAKE_YOUNG_PAIR(BgL_arg1322z00_1507, BNIL);
														BgL_list1323z00_1508 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
															BgL_arg1325z00_1509);
													}
													BgL_arg1320z00_1505 = BgL_list1323z00_1508;
												}
											}
											{	/* Coerce/apply.scm 110 */
												obj_t BgL_arg1326z00_1510;

												{	/* Coerce/apply.scm 110 */
													obj_t BgL_arg1327z00_1511;

													{	/* Coerce/apply.scm 110 */
														obj_t BgL_arg1328z00_1512;
														obj_t BgL_arg1329z00_1513;

														BgL_arg1328z00_1512 =
															BGl_locationzd2fullzd2fnamez00zztools_locationz00
															(BgL_locz00_22);
														BgL_arg1329z00_1513 =
															MAKE_YOUNG_PAIR(STRUCT_REF(BgL_locz00_22,
																(int) (1L)), BNIL);
														BgL_arg1327z00_1511 =
															MAKE_YOUNG_PAIR(BgL_arg1328z00_1512,
															BgL_arg1329z00_1513);
													}
													BgL_arg1326z00_1510 =
														MAKE_YOUNG_PAIR(BgL_errorzd2msgzd2_21,
														BgL_arg1327z00_1511);
												}
												BgL_arg1321z00_1506 =
													MAKE_YOUNG_PAIR(BGl_string1766z00zzcoerce_applyz00,
													BgL_arg1326z00_1510);
											}
											BgL_arg1317z00_1502 =
												MAKE_YOUNG_PAIR(BgL_arg1320z00_1505,
												BgL_arg1321z00_1506);
										}
										BgL_arg1314z00_1499 =
											MAKE_YOUNG_PAIR(BgL_arg1316z00_1501, BgL_arg1317z00_1502);
									}
									{	/* Coerce/apply.scm 112 */
										obj_t BgL_arg1332z00_1515;

										{	/* Coerce/apply.scm 112 */
											obj_t BgL_arg1333z00_1516;

											{	/* Coerce/apply.scm 112 */
												obj_t BgL_arg1335z00_1517;
												obj_t BgL_arg1339z00_1518;

												{	/* Coerce/apply.scm 112 */
													obj_t BgL_arg1340z00_1519;

													BgL_arg1340z00_1519 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BNIL);
													BgL_arg1335z00_1517 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
														BgL_arg1340z00_1519);
												}
												{	/* Coerce/apply.scm 112 */
													obj_t BgL_arg1342z00_1520;
													obj_t BgL_arg1343z00_1521;

													{	/* Coerce/apply.scm 112 */
														obj_t BgL_arg1346z00_1522;

														BgL_arg1346z00_1522 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BNIL);
														BgL_arg1342z00_1520 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
															BgL_arg1346z00_1522);
													}
													{	/* Coerce/apply.scm 112 */
														obj_t BgL_arg1348z00_1523;

														{	/* Coerce/apply.scm 112 */
															obj_t BgL_arg1349z00_1524;

															BgL_arg1349z00_1524 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BNIL);
															BgL_arg1348z00_1523 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																BgL_arg1349z00_1524);
														}
														BgL_arg1343z00_1521 =
															MAKE_YOUNG_PAIR(BgL_arg1348z00_1523, BNIL);
													}
													BgL_arg1339z00_1518 =
														MAKE_YOUNG_PAIR(BgL_arg1342z00_1520,
														BgL_arg1343z00_1521);
												}
												BgL_arg1333z00_1516 =
													MAKE_YOUNG_PAIR(BgL_arg1335z00_1517,
													BgL_arg1339z00_1518);
											}
											BgL_arg1332z00_1515 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1333z00_1516);
										}
										BgL_arg1315z00_1500 =
											MAKE_YOUNG_PAIR(BgL_arg1332z00_1515, BNIL);
									}
									BgL_arg1312z00_1498 =
										MAKE_YOUNG_PAIR(BgL_arg1314z00_1499, BgL_arg1315z00_1500);
								}
								BgL_arg1268z00_1484 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BgL_arg1312z00_1498);
							}
						else
							{	/* Coerce/apply.scm 113 */
								obj_t BgL_arg1351z00_1525;

								{	/* Coerce/apply.scm 113 */
									obj_t BgL_arg1352z00_1526;
									obj_t BgL_arg1361z00_1527;

									{	/* Coerce/apply.scm 113 */
										obj_t BgL_arg1364z00_1528;

										BgL_arg1364z00_1528 =
											BGl_currentzd2functionzd2zztools_errorz00();
										{	/* Coerce/apply.scm 113 */
											obj_t BgL_list1365z00_1529;

											{	/* Coerce/apply.scm 113 */
												obj_t BgL_arg1367z00_1530;

												BgL_arg1367z00_1530 =
													MAKE_YOUNG_PAIR(BgL_arg1364z00_1528, BNIL);
												BgL_list1365z00_1529 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
													BgL_arg1367z00_1530);
											}
											BgL_arg1352z00_1526 = BgL_list1365z00_1529;
										}
									}
									{	/* Coerce/apply.scm 113 */
										obj_t BgL_arg1370z00_1531;

										BgL_arg1370z00_1531 =
											MAKE_YOUNG_PAIR(BgL_errorzd2msgzd2_21, BNIL);
										BgL_arg1361z00_1527 =
											MAKE_YOUNG_PAIR(BGl_string1766z00zzcoerce_applyz00,
											BgL_arg1370z00_1531);
									}
									BgL_arg1351z00_1525 =
										MAKE_YOUNG_PAIR(BgL_arg1352z00_1526, BgL_arg1361z00_1527);
								}
								BgL_arg1268z00_1484 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1351z00_1525);
							}
					}
					BgL_nodez00_1483 =
						BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
						(BgL_arg1268z00_1484, BgL_locz00_22);
				}
				return
					BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_1483, BgL_callerz00_23,
					((BgL_typez00_bglt) BgL_toz00_24), ((bool_t) 0));
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcoerce_applyz00(void)
	{
		{	/* Coerce/apply.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcoerce_applyz00(void)
	{
		{	/* Coerce/apply.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcoerce_applyz00(void)
	{
		{	/* Coerce/apply.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc1767z00zzcoerce_applyz00, BGl_string1768z00zzcoerce_applyz00);
		}

	}



/* &coerce!-app-ly1236 */
	BgL_nodez00_bglt BGl_z62coercez12zd2appzd2ly1236z70zzcoerce_applyz00(obj_t
		BgL_envz00_1904, obj_t BgL_nodez00_1905, obj_t BgL_callerz00_1906,
		obj_t BgL_toz00_1907, obj_t BgL_safez00_1908)
	{
		{	/* Coerce/apply.scm 36 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_errorzd2msgzd2_1927;

				{	/* Tools/trace.sch 53 */
					obj_t BgL_arg1606z00_1928;

					BgL_arg1606z00_1928 =
						BGl_shapez00zztools_shapez00(
						((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905)));
					{	/* Tools/trace.sch 53 */
						obj_t BgL_list1607z00_1929;

						{	/* Tools/trace.sch 53 */
							obj_t BgL_arg1609z00_1930;

							BgL_arg1609z00_1930 = MAKE_YOUNG_PAIR(BgL_arg1606z00_1928, BNIL);
							BgL_list1607z00_1929 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1609z00_1930);
						}
						BgL_errorzd2msgzd2_1927 = BgL_list1607z00_1929;
					}
				}
				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg1376z00_1931;

					{	/* Tools/trace.sch 53 */
						BgL_nodez00_bglt BgL_arg1377z00_1932;

						BgL_arg1377z00_1932 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905)))->BgL_argz00);
						BgL_arg1376z00_1931 =
							BGl_coercez12z12zzcoerce_coercez00(BgL_arg1377z00_1932,
							BgL_callerz00_1906,
							((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
							CBOOL(BgL_safez00_1908));
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905)))->BgL_argz00) =
						((BgL_nodez00_bglt) BgL_arg1376z00_1931), BUNSPEC);
				}
				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_czd2funzd2_1933;

					{	/* Tools/trace.sch 53 */
						BgL_nodez00_bglt BgL_arg1605z00_1934;

						BgL_arg1605z00_1934 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905)))->BgL_funz00);
						BgL_czd2funzd2_1933 =
							BGl_coercez12z12zzcoerce_coercez00(BgL_arg1605z00_1934,
							BgL_callerz00_1906,
							((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00),
							CBOOL(BgL_safez00_1908));
					}
					if (CBOOL(BGl_za2unsafezd2arityza2zd2zzengine_paramz00))
						{	/* Tools/trace.sch 53 */
							bool_t BgL_test1785z00_2144;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_classz00_1935;

								BgL_classz00_1935 = BGl_varz00zzast_nodez00;
								{	/* Tools/trace.sch 53 */
									BgL_objectz00_bglt BgL_arg1807z00_1936;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_tmpz00_2145;

										BgL_tmpz00_2145 =
											((obj_t) ((BgL_objectz00_bglt) BgL_czd2funzd2_1933));
										BgL_arg1807z00_1936 =
											(BgL_objectz00_bglt) (BgL_tmpz00_2145);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Tools/trace.sch 53 */
											long BgL_idxz00_1937;

											BgL_idxz00_1937 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1936);
											BgL_test1785z00_2144 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_1937 + 2L)) == BgL_classz00_1935);
										}
									else
										{	/* Tools/trace.sch 53 */
											bool_t BgL_res1758z00_1940;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_oclassz00_1941;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1815z00_1942;
													long BgL_arg1816z00_1943;

													BgL_arg1815z00_1942 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Tools/trace.sch 53 */
														long BgL_arg1817z00_1944;

														BgL_arg1817z00_1944 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1936);
														BgL_arg1816z00_1943 =
															(BgL_arg1817z00_1944 - OBJECT_TYPE);
													}
													BgL_oclassz00_1941 =
														VECTOR_REF(BgL_arg1815z00_1942,
														BgL_arg1816z00_1943);
												}
												{	/* Tools/trace.sch 53 */
													bool_t BgL__ortest_1115z00_1945;

													BgL__ortest_1115z00_1945 =
														(BgL_classz00_1935 == BgL_oclassz00_1941);
													if (BgL__ortest_1115z00_1945)
														{	/* Tools/trace.sch 53 */
															BgL_res1758z00_1940 = BgL__ortest_1115z00_1945;
														}
													else
														{	/* Tools/trace.sch 53 */
															long BgL_odepthz00_1946;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1804z00_1947;

																BgL_arg1804z00_1947 = (BgL_oclassz00_1941);
																BgL_odepthz00_1946 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_1947);
															}
															if ((2L < BgL_odepthz00_1946))
																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1802z00_1948;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1803z00_1949;

																		BgL_arg1803z00_1949 = (BgL_oclassz00_1941);
																		BgL_arg1802z00_1948 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_1949, 2L);
																	}
																	BgL_res1758z00_1940 =
																		(BgL_arg1802z00_1948 == BgL_classz00_1935);
																}
															else
																{	/* Tools/trace.sch 53 */
																	BgL_res1758z00_1940 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1785z00_2144 = BgL_res1758z00_1940;
										}
								}
							}
							if (BgL_test1785z00_2144)
								{	/* Tools/trace.sch 53 */
									((((BgL_appzd2lyzd2_bglt) COBJECT(
													((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905)))->
											BgL_funz00) =
										((BgL_nodez00_bglt) BgL_czd2funzd2_1933), BUNSPEC);
									return
										BGl_convertz12z12zzcoerce_convertz00(((BgL_nodez00_bglt) (
												(BgL_appzd2lyzd2_bglt) BgL_nodez00_1905)),
										((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
										((BgL_typez00_bglt) BgL_toz00_1907),
										CBOOL(BgL_safez00_1908));
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_localz00_bglt BgL_funz00_1950;

									BgL_funz00_1950 =
										BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF(9),
										((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00));
									{	/* Tools/trace.sch 53 */
										BgL_refz00_bglt BgL_arg1408z00_1951;

										{	/* Tools/trace.sch 53 */
											BgL_refz00_bglt BgL_new1108z00_1952;

											{	/* Tools/trace.sch 53 */
												BgL_refz00_bglt BgL_new1107z00_1953;

												BgL_new1107z00_1953 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Tools/trace.sch 53 */
													long BgL_arg1410z00_1954;

													BgL_arg1410z00_1954 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1107z00_1953),
														BgL_arg1410z00_1954);
												}
												{	/* Tools/trace.sch 53 */
													BgL_objectz00_bglt BgL_tmpz00_2183;

													BgL_tmpz00_2183 =
														((BgL_objectz00_bglt) BgL_new1107z00_1953);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2183, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1107z00_1953);
												BgL_new1108z00_1952 = BgL_new1107z00_1953;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1108z00_1952)))->
													BgL_locz00) =
												((obj_t) (((BgL_nodez00_bglt)
															COBJECT(BgL_czd2funzd2_1933))->BgL_locz00)),
												BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1108z00_1952)))->BgL_typez00) =
												((BgL_typez00_bglt) ((BgL_typez00_bglt)
														BGl_za2procedureza2z00zztype_cachez00)), BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1108z00_1952)))->BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_funz00_1950)), BUNSPEC);
											BgL_arg1408z00_1951 = BgL_new1108z00_1952;
										}
										((((BgL_appzd2lyzd2_bglt) COBJECT(
														((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905)))->
												BgL_funz00) =
											((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
													BgL_arg1408z00_1951)), BUNSPEC);
									}
									{	/* Tools/trace.sch 53 */
										BgL_letzd2varzd2_bglt BgL_new1110z00_1955;

										{	/* Tools/trace.sch 53 */
											BgL_letzd2varzd2_bglt BgL_new1109z00_1956;

											BgL_new1109z00_1956 =
												((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_letzd2varzd2_bgl))));
											{	/* Tools/trace.sch 53 */
												long BgL_arg1434z00_1957;

												BgL_arg1434z00_1957 =
													BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1109z00_1956),
													BgL_arg1434z00_1957);
											}
											{	/* Tools/trace.sch 53 */
												BgL_objectz00_bglt BgL_tmpz00_2203;

												BgL_tmpz00_2203 =
													((BgL_objectz00_bglt) BgL_new1109z00_1956);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2203, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1109z00_1956);
											BgL_new1110z00_1955 = BgL_new1109z00_1956;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1110z00_1955)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt)
																	BgL_nodez00_1905))))->BgL_locz00)), BUNSPEC);
										{
											BgL_typez00_bglt BgL_auxz00_2212;

											{	/* Tools/trace.sch 53 */
												BgL_typez00_bglt BgL_arg1421z00_1958;

												BgL_arg1421z00_1958 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905))))->
													BgL_typez00);
												BgL_auxz00_2212 =
													BGl_strictzd2nodezd2typez00zzast_nodez00((
														(BgL_typez00_bglt) BgL_toz00_1907),
													BgL_arg1421z00_1958);
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1110z00_1955)))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_auxz00_2212), BUNSPEC);
										}
										((((BgL_nodezf2effectzf2_bglt) COBJECT(
														((BgL_nodezf2effectzf2_bglt)
															BgL_new1110z00_1955)))->BgL_sidezd2effectzd2) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_nodezf2effectzf2_bglt)
													COBJECT(((BgL_nodezf2effectzf2_bglt)
															BgL_new1110z00_1955)))->BgL_keyz00) =
											((obj_t) BINT(-1L)), BUNSPEC);
										{
											obj_t BgL_auxz00_2225;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1422z00_1959;

												BgL_arg1422z00_1959 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_funz00_1950),
													((obj_t) BgL_czd2funzd2_1933));
												{	/* Tools/trace.sch 53 */
													obj_t BgL_list1423z00_1960;

													BgL_list1423z00_1960 =
														MAKE_YOUNG_PAIR(BgL_arg1422z00_1959, BNIL);
													BgL_auxz00_2225 = BgL_list1423z00_1960;
											}}
											((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1110z00_1955))->
													BgL_bindingsz00) =
												((obj_t) BgL_auxz00_2225), BUNSPEC);
										}
										((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1110z00_1955))->
												BgL_bodyz00) =
											((BgL_nodez00_bglt)
												BGl_convertz12z12zzcoerce_convertz00(((BgL_nodez00_bglt)
														((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905)),
													((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
													((BgL_typez00_bglt) BgL_toz00_1907),
													CBOOL(BgL_safez00_1908))), BUNSPEC);
										((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1110z00_1955))->
												BgL_removablezf3zf3) =
											((bool_t) ((bool_t) 1)), BUNSPEC);
										return ((BgL_nodez00_bglt) BgL_new1110z00_1955);
									}
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_localz00_bglt BgL_funz00_1961;

							BgL_funz00_1961 =
								BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF(9),
								((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00));
							{	/* Tools/trace.sch 53 */
								BgL_localz00_bglt BgL_valz00_1962;

								BgL_valz00_1962 =
									BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF(10),
									((BgL_typez00_bglt) BGl_za2pairzd2nilza2zd2zztype_cachez00));
								{	/* Tools/trace.sch 53 */
									obj_t BgL_locz00_1963;

									BgL_locz00_1963 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_appzd2lyzd2_bglt) BgL_nodez00_1905))))->
										BgL_locz00);
									{	/* Tools/trace.sch 53 */
										BgL_nodez00_bglt BgL_lvalz00_1964;

										{	/* Tools/trace.sch 53 */
											BgL_nodez00_bglt BgL_arg1594z00_1965;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1595z00_1966;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1602z00_1967;

													BgL_arg1602z00_1967 =
														MAKE_YOUNG_PAIR(((obj_t) BgL_valz00_1962), BNIL);
													BgL_arg1595z00_1966 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
														BgL_arg1602z00_1967);
												}
												BgL_arg1594z00_1965 =
													BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg1595z00_1966, BgL_locz00_1963);
											}
											BgL_lvalz00_1964 =
												BGl_lvtypezd2nodezd2zzast_lvtypez00
												(BgL_arg1594z00_1965);
										}
										{	/* Tools/trace.sch 53 */
											obj_t BgL_lenz00_1968;

											BgL_lenz00_1968 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(12));
											{	/* Tools/trace.sch 53 */
												BgL_nodez00_bglt BgL_bodyz00_1969;

												{	/* Tools/trace.sch 53 */
													BgL_nodez00_bglt BgL_arg1514z00_1970;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1516z00_1971;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1535z00_1972;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1540z00_1973;
																obj_t BgL_arg1544z00_1974;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1546z00_1975;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1552z00_1976;
																		obj_t BgL_arg1553z00_1977;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1559z00_1978;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1561z00_1979;
																				obj_t BgL_arg1564z00_1980;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1455z00_1981;

																					BgL_arg1455z00_1981 =
																						SYMBOL_TO_STRING(BgL_lenz00_1968);
																					BgL_arg1561z00_1979 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1455z00_1981);
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_symbolz00_1982;

																					BgL_symbolz00_1982 =
																						CNST_TABLE_REF(13);
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1983;

																						BgL_arg1455z00_1983 =
																							SYMBOL_TO_STRING
																							(BgL_symbolz00_1982);
																						BgL_arg1564z00_1980 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1983);
																					}
																				}
																				BgL_arg1559z00_1978 =
																					string_append(BgL_arg1561z00_1979,
																					BgL_arg1564z00_1980);
																			}
																			BgL_arg1552z00_1976 =
																				bstring_to_symbol(BgL_arg1559z00_1978);
																		}
																		{	/* Tools/trace.sch 53 */
																			BgL_nodez00_bglt BgL_arg1565z00_1984;

																			BgL_arg1565z00_1984 =
																				BGl_coercez12z12zzcoerce_coercez00
																				(BgL_lvalz00_1964, BgL_callerz00_1906,
																				((BgL_typez00_bglt)
																					BGl_za2intza2z00zztype_cachez00),
																				CBOOL(BgL_safez00_1908));
																			BgL_arg1553z00_1977 =
																				MAKE_YOUNG_PAIR(((obj_t)
																					BgL_arg1565z00_1984), BNIL);
																		}
																		BgL_arg1546z00_1975 =
																			MAKE_YOUNG_PAIR(BgL_arg1552z00_1976,
																			BgL_arg1553z00_1977);
																	}
																	BgL_arg1540z00_1973 =
																		MAKE_YOUNG_PAIR(BgL_arg1546z00_1975, BNIL);
																}
																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1571z00_1985;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1573z00_1986;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1575z00_1987;
																			obj_t BgL_arg1576z00_1988;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1584z00_1989;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1585z00_1990;

																					BgL_arg1585z00_1990 =
																						MAKE_YOUNG_PAIR(BgL_lenz00_1968,
																						BNIL);
																					BgL_arg1584z00_1989 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_funz00_1961),
																						BgL_arg1585z00_1990);
																				}
																				BgL_arg1575z00_1987 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																					BgL_arg1584z00_1989);
																			}
																			{	/* Tools/trace.sch 53 */
																				BgL_nodez00_bglt BgL_arg1589z00_1991;
																				obj_t BgL_arg1591z00_1992;

																				BgL_arg1589z00_1991 =
																					BGl_convertz12z12zzcoerce_convertz00(
																					((BgL_nodez00_bglt)
																						((BgL_appzd2lyzd2_bglt)
																							BgL_nodez00_1905)),
																					((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00),
																					((BgL_typez00_bglt) BgL_toz00_1907),
																					CBOOL(BgL_safez00_1908));
																				{	/* Tools/trace.sch 53 */
																					BgL_nodez00_bglt BgL_arg1593z00_1993;

																					BgL_arg1593z00_1993 =
																						BGl_makezd2errorzd2nodez00zzcoerce_applyz00
																						(BgL_errorzd2msgzd2_1927,
																						BgL_locz00_1963, BgL_callerz00_1906,
																						BgL_toz00_1907);
																					BgL_arg1591z00_1992 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_arg1593z00_1993), BNIL);
																				}
																				BgL_arg1576z00_1988 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg1589z00_1991),
																					BgL_arg1591z00_1992);
																			}
																			BgL_arg1573z00_1986 =
																				MAKE_YOUNG_PAIR(BgL_arg1575z00_1987,
																				BgL_arg1576z00_1988);
																		}
																		BgL_arg1571z00_1985 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																			BgL_arg1573z00_1986);
																	}
																	BgL_arg1544z00_1974 =
																		MAKE_YOUNG_PAIR(BgL_arg1571z00_1985, BNIL);
																}
																BgL_arg1535z00_1972 =
																	MAKE_YOUNG_PAIR(BgL_arg1540z00_1973,
																	BgL_arg1544z00_1974);
															}
															BgL_arg1516z00_1971 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																BgL_arg1535z00_1972);
														}
														BgL_arg1514z00_1970 =
															BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
															(BgL_arg1516z00_1971, BgL_locz00_1963);
													}
													BgL_bodyz00_1969 =
														BGl_lvtypezd2nodezd2zzast_lvtypez00
														(BgL_arg1514z00_1970);
												}
												{	/* Tools/trace.sch 53 */
													BgL_letzd2varzd2_bglt BgL_lnodez00_1994;

													{	/* Tools/trace.sch 53 */
														BgL_letzd2varzd2_bglt BgL_new1113z00_1995;

														{	/* Tools/trace.sch 53 */
															BgL_letzd2varzd2_bglt BgL_new1111z00_1996;

															BgL_new1111z00_1996 =
																((BgL_letzd2varzd2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_letzd2varzd2_bgl))));
															{	/* Tools/trace.sch 53 */
																long BgL_arg1513z00_1997;

																BgL_arg1513z00_1997 =
																	BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1111z00_1996),
																	BgL_arg1513z00_1997);
															}
															{	/* Tools/trace.sch 53 */
																BgL_objectz00_bglt BgL_tmpz00_2300;

																BgL_tmpz00_2300 =
																	((BgL_objectz00_bglt) BgL_new1111z00_1996);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2300,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1111z00_1996);
															BgL_new1113z00_1995 = BgL_new1111z00_1996;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1113z00_1995)))->
																BgL_locz00) =
															((obj_t) BgL_locz00_1963), BUNSPEC);
														{
															BgL_typez00_bglt BgL_auxz00_2306;

															{	/* Tools/trace.sch 53 */
																BgL_typez00_bglt BgL_arg1473z00_1998;

																BgL_arg1473z00_1998 =
																	(((BgL_nodez00_bglt)
																		COBJECT(BgL_bodyz00_1969))->BgL_typez00);
																BgL_auxz00_2306 =
																	BGl_strictzd2nodezd2typez00zzast_nodez00
																	(BgL_arg1473z00_1998,
																	((BgL_typez00_bglt)
																		BGl_za2objza2z00zztype_cachez00));
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1113z00_1995)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_auxz00_2306), BUNSPEC);
														}
														((((BgL_nodezf2effectzf2_bglt) COBJECT(
																		((BgL_nodezf2effectzf2_bglt)
																			BgL_new1113z00_1995)))->
																BgL_sidezd2effectzd2) =
															((obj_t) BUNSPEC), BUNSPEC);
														((((BgL_nodezf2effectzf2_bglt)
																	COBJECT(((BgL_nodezf2effectzf2_bglt)
																			BgL_new1113z00_1995)))->BgL_keyz00) =
															((obj_t) BINT(-1L)), BUNSPEC);
														{
															obj_t BgL_auxz00_2317;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1485z00_1999;
																obj_t BgL_arg1489z00_2000;

																BgL_arg1485z00_1999 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_funz00_1961),
																	((obj_t) BgL_czd2funzd2_1933));
																{	/* Tools/trace.sch 53 */
																	BgL_nodez00_bglt BgL_arg1509z00_2001;

																	BgL_arg1509z00_2001 =
																		(((BgL_appzd2lyzd2_bglt) COBJECT(
																				((BgL_appzd2lyzd2_bglt)
																					BgL_nodez00_1905)))->BgL_argz00);
																	BgL_arg1489z00_2000 =
																		MAKE_YOUNG_PAIR(((obj_t) BgL_valz00_1962),
																		((obj_t) BgL_arg1509z00_2001));
																}
																{	/* Tools/trace.sch 53 */
																	obj_t BgL_list1490z00_2002;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1502z00_2003;

																		BgL_arg1502z00_2003 =
																			MAKE_YOUNG_PAIR(BgL_arg1489z00_2000,
																			BNIL);
																		BgL_list1490z00_2002 =
																			MAKE_YOUNG_PAIR(BgL_arg1485z00_1999,
																			BgL_arg1502z00_2003);
																	}
																	BgL_auxz00_2317 = BgL_list1490z00_2002;
															}}
															((((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_new1113z00_1995))->
																	BgL_bindingsz00) =
																((obj_t) BgL_auxz00_2317), BUNSPEC);
														}
														((((BgL_letzd2varzd2_bglt)
																	COBJECT(BgL_new1113z00_1995))->BgL_bodyz00) =
															((BgL_nodez00_bglt) BgL_bodyz00_1969), BUNSPEC);
														((((BgL_letzd2varzd2_bglt)
																	COBJECT(BgL_new1113z00_1995))->
																BgL_removablezf3zf3) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
														BgL_lnodez00_1994 = BgL_new1113z00_1995;
													}
													{	/* Tools/trace.sch 53 */

														{	/* Tools/trace.sch 53 */
															BgL_refz00_bglt BgL_arg1437z00_2004;

															{	/* Tools/trace.sch 53 */
																BgL_refz00_bglt BgL_new1115z00_2005;

																{	/* Tools/trace.sch 53 */
																	BgL_refz00_bglt BgL_new1114z00_2006;

																	BgL_new1114z00_2006 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg1448z00_2007;

																		BgL_arg1448z00_2007 =
																			BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1114z00_2006),
																			BgL_arg1448z00_2007);
																	}
																	{	/* Tools/trace.sch 53 */
																		BgL_objectz00_bglt BgL_tmpz00_2335;

																		BgL_tmpz00_2335 =
																			((BgL_objectz00_bglt)
																			BgL_new1114z00_2006);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2335,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1114z00_2006);
																	BgL_new1115z00_2005 = BgL_new1114z00_2006;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1115z00_2005)))->BgL_locz00) =
																	((obj_t) BgL_locz00_1963), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1115z00_2005)))->BgL_typez00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BGl_za2procedureza2z00zztype_cachez00)),
																	BUNSPEC);
																((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																					BgL_new1115z00_2005)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt) BgL_funz00_1961)),
																	BUNSPEC);
																BgL_arg1437z00_2004 = BgL_new1115z00_2005;
															}
															((((BgL_appzd2lyzd2_bglt) COBJECT(
																			((BgL_appzd2lyzd2_bglt)
																				BgL_nodez00_1905)))->BgL_funz00) =
																((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																		BgL_arg1437z00_2004)), BUNSPEC);
														}
														{	/* Tools/trace.sch 53 */
															BgL_refz00_bglt BgL_arg1453z00_2008;

															{	/* Tools/trace.sch 53 */
																BgL_refz00_bglt BgL_new1118z00_2009;

																{	/* Tools/trace.sch 53 */
																	BgL_refz00_bglt BgL_new1116z00_2010;

																	BgL_new1116z00_2010 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg1472z00_2011;

																		BgL_arg1472z00_2011 =
																			BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1116z00_2010),
																			BgL_arg1472z00_2011);
																	}
																	{	/* Tools/trace.sch 53 */
																		BgL_objectz00_bglt BgL_tmpz00_2354;

																		BgL_tmpz00_2354 =
																			((BgL_objectz00_bglt)
																			BgL_new1116z00_2010);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2354,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1116z00_2010);
																	BgL_new1118z00_2009 = BgL_new1116z00_2010;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1118z00_2009)))->BgL_locz00) =
																	((obj_t) BgL_locz00_1963), BUNSPEC);
																{
																	BgL_typez00_bglt BgL_auxz00_2360;

																	{	/* Tools/trace.sch 53 */
																		BgL_typez00_bglt BgL_arg1454z00_2012;

																		BgL_arg1454z00_2012 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						BgL_valz00_1962)))->BgL_typez00);
																		BgL_auxz00_2360 =
																			BGl_strictzd2nodezd2typez00zzast_nodez00
																			(BgL_arg1454z00_2012,
																			((BgL_typez00_bglt)
																				BGl_za2objza2z00zztype_cachez00));
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1118z00_2009)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_auxz00_2360),
																		BUNSPEC);
																}
																((((BgL_varz00_bglt) COBJECT(
																				((BgL_varz00_bglt)
																					BgL_new1118z00_2009)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt) BgL_valz00_1962)),
																	BUNSPEC);
																BgL_arg1453z00_2008 = BgL_new1118z00_2009;
															}
															((((BgL_appzd2lyzd2_bglt) COBJECT(
																			((BgL_appzd2lyzd2_bglt)
																				BgL_nodez00_1905)))->BgL_argz00) =
																((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																		BgL_arg1453z00_2008)), BUNSPEC);
														}
														return ((BgL_nodez00_bglt) BgL_lnodez00_1994);
													}
												}
											}
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcoerce_applyz00(void)
	{
		{	/* Coerce/apply.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
			return
				BGl_modulezd2initializa7ationz75zzcoerce_convertz00(87995645L,
				BSTRING_TO_STRING(BGl_string1769z00zzcoerce_applyz00));
		}

	}

#ifdef __cplusplus
}
#endif
