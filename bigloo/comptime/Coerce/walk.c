/*===========================================================================*/
/*   (Coerce/walk.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Coerce/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_COERCE_WALK_TYPE_DEFINITIONS
#define BGL_COERCE_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_COERCE_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzcoerce_walkz00 = BUNSPEC;
	extern obj_t BGl_pvariablezd2protozd2zzcoerce_pprotoz00(long,
		BgL_variablez00_bglt);
	extern obj_t BGl_funz00zzast_varz00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_getzd2stackzd2checkz00zzcoerce_convertz00(void);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcoerce_walkz00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	extern obj_t
		BGl_coercezd2functionz12zc0zzcoerce_coercez00(BgL_variablez00_bglt, bool_t);
	static obj_t BGl_objectzd2initzd2zzcoerce_walkz00(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31306ze3ze5zzcoerce_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcoerce_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_coercezd2walkz12zc0zzcoerce_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcoerce_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_convertz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_pprotoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzcoerce_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_walkz00(void);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzcoerce_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcoerce_walkz00(void);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_resetzd2ppmargez12zc0zzcoerce_pprotoz00(void);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_z62coercezd2walkz12za2zzcoerce_walkz00(obj_t, obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_STRING(BGl_string1600z00zzcoerce_walkz00,
		BgL_bgl_string1600za700za7za7c1603za7, "export static coerce pass-started ",
		34);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_coercezd2walkz12zd2envz12zzcoerce_walkz00,
		BgL_bgl_za762coerceza7d2walk1604z00,
		BGl_z62coercezd2walkz12za2zzcoerce_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1589z00zzcoerce_walkz00,
		BgL_bgl_string1589za700za7za7c1605za7, "Coercions & Checks", 18);
	      DEFINE_STRING(BGl_string1590z00zzcoerce_walkz00,
		BgL_bgl_string1590za700za7za7c1606za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1591z00zzcoerce_walkz00,
		BgL_bgl_string1591za700za7za7c1607za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1593z00zzcoerce_walkz00,
		BgL_bgl_string1593za700za7za7c1608za7, "      type tests introduced: ", 29);
	      DEFINE_STRING(BGl_string1594z00zzcoerce_walkz00,
		BgL_bgl_string1594za700za7za7c1609za7, " error", 6);
	      DEFINE_STRING(BGl_string1595z00zzcoerce_walkz00,
		BgL_bgl_string1595za700za7za7c1610za7, "s", 1);
	      DEFINE_STRING(BGl_string1596z00zzcoerce_walkz00,
		BgL_bgl_string1596za700za7za7c1611za7, "", 0);
	      DEFINE_STRING(BGl_string1597z00zzcoerce_walkz00,
		BgL_bgl_string1597za700za7za7c1612za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1598z00zzcoerce_walkz00,
		BgL_bgl_string1598za700za7za7c1613za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1599z00zzcoerce_walkz00,
		BgL_bgl_string1599za700za7za7c1614za7, "coerce_walk", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1592z00zzcoerce_walkz00,
		BgL_bgl_za762za7c3za704anonymo1615za7,
		BGl_z62zc3z04anonymousza31306ze3ze5zzcoerce_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcoerce_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcoerce_walkz00(long
		BgL_checksumz00_1708, char *BgL_fromz00_1709)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcoerce_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzcoerce_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcoerce_walkz00();
					BGl_libraryzd2moduleszd2initz00zzcoerce_walkz00();
					BGl_cnstzd2initzd2zzcoerce_walkz00();
					BGl_importedzd2moduleszd2initz00zzcoerce_walkz00();
					return BGl_methodzd2initzd2zzcoerce_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_walkz00(void)
	{
		{	/* Coerce/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"coerce_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "coerce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"coerce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "coerce_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "coerce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "coerce_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "coerce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "coerce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"coerce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "coerce_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"coerce_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcoerce_walkz00(void)
	{
		{	/* Coerce/walk.scm 15 */
			{	/* Coerce/walk.scm 15 */
				obj_t BgL_cportz00_1680;

				{	/* Coerce/walk.scm 15 */
					obj_t BgL_stringz00_1687;

					BgL_stringz00_1687 = BGl_string1600z00zzcoerce_walkz00;
					{	/* Coerce/walk.scm 15 */
						obj_t BgL_startz00_1688;

						BgL_startz00_1688 = BINT(0L);
						{	/* Coerce/walk.scm 15 */
							obj_t BgL_endz00_1689;

							BgL_endz00_1689 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1687)));
							{	/* Coerce/walk.scm 15 */

								BgL_cportz00_1680 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1687, BgL_startz00_1688, BgL_endz00_1689);
				}}}}
				{
					long BgL_iz00_1681;

					BgL_iz00_1681 = 3L;
				BgL_loopz00_1682:
					if ((BgL_iz00_1681 == -1L))
						{	/* Coerce/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Coerce/walk.scm 15 */
							{	/* Coerce/walk.scm 15 */
								obj_t BgL_arg1602z00_1683;

								{	/* Coerce/walk.scm 15 */

									{	/* Coerce/walk.scm 15 */
										obj_t BgL_locationz00_1685;

										BgL_locationz00_1685 = BBOOL(((bool_t) 0));
										{	/* Coerce/walk.scm 15 */

											BgL_arg1602z00_1683 =
												BGl_readz00zz__readerz00(BgL_cportz00_1680,
												BgL_locationz00_1685);
										}
									}
								}
								{	/* Coerce/walk.scm 15 */
									int BgL_tmpz00_1738;

									BgL_tmpz00_1738 = (int) (BgL_iz00_1681);
									CNST_TABLE_SET(BgL_tmpz00_1738, BgL_arg1602z00_1683);
							}}
							{	/* Coerce/walk.scm 15 */
								int BgL_auxz00_1686;

								BgL_auxz00_1686 = (int) ((BgL_iz00_1681 - 1L));
								{
									long BgL_iz00_1743;

									BgL_iz00_1743 = (long) (BgL_auxz00_1686);
									BgL_iz00_1681 = BgL_iz00_1743;
									goto BgL_loopz00_1682;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcoerce_walkz00(void)
	{
		{	/* Coerce/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* coerce-walk! */
	BGL_EXPORTED_DEF obj_t BGl_coercezd2walkz12zc0zzcoerce_walkz00(obj_t
		BgL_astz00_3)
	{
		{	/* Coerce/walk.scm 35 */
			{	/* Coerce/walk.scm 36 */
				obj_t BgL_list1235z00_1353;

				{	/* Coerce/walk.scm 36 */
					obj_t BgL_arg1236z00_1354;

					{	/* Coerce/walk.scm 36 */
						obj_t BgL_arg1238z00_1355;

						BgL_arg1238z00_1355 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1236z00_1354 =
							MAKE_YOUNG_PAIR(BGl_string1589z00zzcoerce_walkz00,
							BgL_arg1238z00_1355);
					}
					BgL_list1235z00_1353 =
						MAKE_YOUNG_PAIR(BGl_string1590z00zzcoerce_walkz00,
						BgL_arg1236z00_1354);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1235z00_1353);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1589z00zzcoerce_walkz00;
			{	/* Coerce/walk.scm 36 */
				obj_t BgL_g1106z00_1356;

				BgL_g1106z00_1356 = BNIL;
				{
					obj_t BgL_hooksz00_1359;
					obj_t BgL_hnamesz00_1360;

					BgL_hooksz00_1359 = BgL_g1106z00_1356;
					BgL_hnamesz00_1360 = BNIL;
				BgL_zc3z04anonymousza31239ze3z87_1361:
					if (NULLP(BgL_hooksz00_1359))
						{	/* Coerce/walk.scm 36 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Coerce/walk.scm 36 */
							bool_t BgL_test1620z00_1756;

							{	/* Coerce/walk.scm 36 */
								obj_t BgL_fun1251z00_1368;

								BgL_fun1251z00_1368 = CAR(((obj_t) BgL_hooksz00_1359));
								BgL_test1620z00_1756 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1251z00_1368));
							}
							if (BgL_test1620z00_1756)
								{	/* Coerce/walk.scm 36 */
									obj_t BgL_arg1244z00_1365;
									obj_t BgL_arg1248z00_1366;

									BgL_arg1244z00_1365 = CDR(((obj_t) BgL_hooksz00_1359));
									BgL_arg1248z00_1366 = CDR(((obj_t) BgL_hnamesz00_1360));
									{
										obj_t BgL_hnamesz00_1768;
										obj_t BgL_hooksz00_1767;

										BgL_hooksz00_1767 = BgL_arg1244z00_1365;
										BgL_hnamesz00_1768 = BgL_arg1248z00_1366;
										BgL_hnamesz00_1360 = BgL_hnamesz00_1768;
										BgL_hooksz00_1359 = BgL_hooksz00_1767;
										goto BgL_zc3z04anonymousza31239ze3z87_1361;
									}
								}
							else
								{	/* Coerce/walk.scm 36 */
									obj_t BgL_arg1249z00_1367;

									BgL_arg1249z00_1367 = CAR(((obj_t) BgL_hnamesz00_1360));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1589z00zzcoerce_walkz00,
										BGl_string1591z00zzcoerce_walkz00, BgL_arg1249z00_1367);
								}
						}
				}
			}
			{
				obj_t BgL_l1229z00_1372;

				BgL_l1229z00_1372 = BgL_astz00_3;
			BgL_zc3z04anonymousza31254ze3z87_1373:
				if (PAIRP(BgL_l1229z00_1372))
					{	/* Coerce/walk.scm 37 */
						{	/* Coerce/walk.scm 38 */
							obj_t BgL_globalz00_1375;

							BgL_globalz00_1375 = CAR(BgL_l1229z00_1372);
							BGl_resetzd2ppmargez12zc0zzcoerce_pprotoz00();
							{	/* Coerce/walk.scm 39 */
								obj_t BgL_arg1268z00_1376;

								BgL_arg1268z00_1376 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1375))))->
									BgL_idz00);
								BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1268z00_1376);
							}
							{	/* Coerce/walk.scm 40 */
								bool_t BgL_arg1272z00_1377;

								if (CBOOL(BGl_za2unsafezd2typeza2zd2zzengine_paramz00))
									{	/* Coerce/walk.scm 40 */
										BgL_arg1272z00_1377 = ((bool_t) 0);
									}
								else
									{	/* Coerce/walk.scm 40 */
										BgL_arg1272z00_1377 = ((bool_t) 1);
									}
								BGl_coercezd2functionz12zc0zzcoerce_coercez00(
									((BgL_variablez00_bglt) BgL_globalz00_1375),
									BgL_arg1272z00_1377);
							}
							BGl_leavezd2functionzd2zztools_errorz00();
						}
						{
							obj_t BgL_l1229z00_1785;

							BgL_l1229z00_1785 = CDR(BgL_l1229z00_1372);
							BgL_l1229z00_1372 = BgL_l1229z00_1785;
							goto BgL_zc3z04anonymousza31254ze3z87_1373;
						}
					}
				else
					{	/* Coerce/walk.scm 37 */
						((bool_t) 1);
					}
			}
			BGl_resetzd2ppmargez12zc0zzcoerce_pprotoz00();
			BGl_forzd2eachzd2globalz12z12zzast_envz00(BGl_proc1592z00zzcoerce_walkz00,
				BNIL);
			{	/* Coerce/walk.scm 49 */
				obj_t BgL_arg1317z00_1397;

				BgL_arg1317z00_1397 = BGl_getzd2stackzd2checkz00zzcoerce_convertz00();
				{	/* Coerce/walk.scm 49 */
					obj_t BgL_list1318z00_1398;

					{	/* Coerce/walk.scm 49 */
						obj_t BgL_arg1319z00_1399;

						{	/* Coerce/walk.scm 49 */
							obj_t BgL_arg1320z00_1400;

							BgL_arg1320z00_1400 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_arg1319z00_1399 =
								MAKE_YOUNG_PAIR(BgL_arg1317z00_1397, BgL_arg1320z00_1400);
						}
						BgL_list1318z00_1398 =
							MAKE_YOUNG_PAIR(BGl_string1593z00zzcoerce_walkz00,
							BgL_arg1319z00_1399);
					}
					BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1318z00_1398);
			}}
			{	/* Coerce/walk.scm 50 */
				obj_t BgL_valuez00_1401;

				BgL_valuez00_1401 =
					BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(1), BgL_astz00_3);
				if (
					((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
						0L))
					{	/* Coerce/walk.scm 50 */
						{	/* Coerce/walk.scm 50 */
							obj_t BgL_port1231z00_1403;

							{	/* Coerce/walk.scm 50 */
								obj_t BgL_tmpz00_1801;

								BgL_tmpz00_1801 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1231z00_1403 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1801);
							}
							bgl_display_obj
								(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
								BgL_port1231z00_1403);
							bgl_display_string(BGl_string1594z00zzcoerce_walkz00,
								BgL_port1231z00_1403);
							{	/* Coerce/walk.scm 50 */
								obj_t BgL_arg1322z00_1404;

								{	/* Coerce/walk.scm 50 */
									bool_t BgL_test1624z00_1806;

									if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
										(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
										{	/* Coerce/walk.scm 50 */
											if (INTEGERP
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
												{	/* Coerce/walk.scm 50 */
													BgL_test1624z00_1806 =
														(
														(long)
														CINT
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
														> 1L);
												}
											else
												{	/* Coerce/walk.scm 50 */
													BgL_test1624z00_1806 =
														BGl_2ze3ze3zz__r4_numbers_6_5z00
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
														BINT(1L));
												}
										}
									else
										{	/* Coerce/walk.scm 50 */
											BgL_test1624z00_1806 = ((bool_t) 0);
										}
									if (BgL_test1624z00_1806)
										{	/* Coerce/walk.scm 50 */
											BgL_arg1322z00_1404 = BGl_string1595z00zzcoerce_walkz00;
										}
									else
										{	/* Coerce/walk.scm 50 */
											BgL_arg1322z00_1404 = BGl_string1596z00zzcoerce_walkz00;
										}
								}
								bgl_display_obj(BgL_arg1322z00_1404, BgL_port1231z00_1403);
							}
							bgl_display_string(BGl_string1597z00zzcoerce_walkz00,
								BgL_port1231z00_1403);
							bgl_display_char(((unsigned char) 10), BgL_port1231z00_1403);
						}
						{	/* Coerce/walk.scm 50 */
							obj_t BgL_list1325z00_1408;

							BgL_list1325z00_1408 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
							BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1325z00_1408);
						}
					}
				else
					{	/* Coerce/walk.scm 50 */
						obj_t BgL_g1109z00_1409;

						BgL_g1109z00_1409 = BNIL;
						{
							obj_t BgL_hooksz00_1412;
							obj_t BgL_hnamesz00_1413;

							BgL_hooksz00_1412 = BgL_g1109z00_1409;
							BgL_hnamesz00_1413 = BNIL;
						BgL_zc3z04anonymousza31326ze3z87_1414:
							if (NULLP(BgL_hooksz00_1412))
								{	/* Coerce/walk.scm 50 */
									return BgL_valuez00_1401;
								}
							else
								{	/* Coerce/walk.scm 50 */
									bool_t BgL_test1629z00_1823;

									{	/* Coerce/walk.scm 50 */
										obj_t BgL_fun1334z00_1421;

										BgL_fun1334z00_1421 = CAR(((obj_t) BgL_hooksz00_1412));
										BgL_test1629z00_1823 =
											CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1334z00_1421));
									}
									if (BgL_test1629z00_1823)
										{	/* Coerce/walk.scm 50 */
											obj_t BgL_arg1331z00_1418;
											obj_t BgL_arg1332z00_1419;

											BgL_arg1331z00_1418 = CDR(((obj_t) BgL_hooksz00_1412));
											BgL_arg1332z00_1419 = CDR(((obj_t) BgL_hnamesz00_1413));
											{
												obj_t BgL_hnamesz00_1835;
												obj_t BgL_hooksz00_1834;

												BgL_hooksz00_1834 = BgL_arg1331z00_1418;
												BgL_hnamesz00_1835 = BgL_arg1332z00_1419;
												BgL_hnamesz00_1413 = BgL_hnamesz00_1835;
												BgL_hooksz00_1412 = BgL_hooksz00_1834;
												goto BgL_zc3z04anonymousza31326ze3z87_1414;
											}
										}
									else
										{	/* Coerce/walk.scm 50 */
											obj_t BgL_arg1333z00_1420;

											BgL_arg1333z00_1420 = CAR(((obj_t) BgL_hnamesz00_1413));
											return
												BGl_internalzd2errorzd2zztools_errorz00
												(BGl_za2currentzd2passza2zd2zzengine_passz00,
												BGl_string1598z00zzcoerce_walkz00, BgL_arg1333z00_1420);
										}
								}
						}
					}
			}
		}

	}



/* &coerce-walk! */
	obj_t BGl_z62coercezd2walkz12za2zzcoerce_walkz00(obj_t BgL_envz00_1676,
		obj_t BgL_astz00_1677)
	{
		{	/* Coerce/walk.scm 35 */
			return BGl_coercezd2walkz12zc0zzcoerce_walkz00(BgL_astz00_1677);
		}

	}



/* &<@anonymous:1306> */
	obj_t BGl_z62zc3z04anonymousza31306ze3ze5zzcoerce_walkz00(obj_t
		BgL_envz00_1678, obj_t BgL_globalz00_1679)
	{
		{	/* Coerce/walk.scm 44 */
			{	/* Coerce/walk.scm 45 */
				bool_t BgL_test1630z00_1840;

				{	/* Coerce/walk.scm 45 */
					bool_t BgL_test1631z00_1841;

					{	/* Coerce/walk.scm 45 */
						BgL_valuez00_bglt BgL_arg1316z00_1691;

						BgL_arg1316z00_1691 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_globalz00_1679))))->BgL_valuez00);
						{	/* Coerce/walk.scm 45 */
							obj_t BgL_classz00_1692;

							BgL_classz00_1692 = BGl_funz00zzast_varz00;
							{	/* Coerce/walk.scm 45 */
								BgL_objectz00_bglt BgL_arg1807z00_1693;

								{	/* Coerce/walk.scm 45 */
									obj_t BgL_tmpz00_1845;

									BgL_tmpz00_1845 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1316z00_1691));
									BgL_arg1807z00_1693 = (BgL_objectz00_bglt) (BgL_tmpz00_1845);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Coerce/walk.scm 45 */
										long BgL_idxz00_1694;

										BgL_idxz00_1694 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1693);
										BgL_test1631z00_1841 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_1694 + 2L)) == BgL_classz00_1692);
									}
								else
									{	/* Coerce/walk.scm 45 */
										bool_t BgL_res1586z00_1697;

										{	/* Coerce/walk.scm 45 */
											obj_t BgL_oclassz00_1698;

											{	/* Coerce/walk.scm 45 */
												obj_t BgL_arg1815z00_1699;
												long BgL_arg1816z00_1700;

												BgL_arg1815z00_1699 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Coerce/walk.scm 45 */
													long BgL_arg1817z00_1701;

													BgL_arg1817z00_1701 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1693);
													BgL_arg1816z00_1700 =
														(BgL_arg1817z00_1701 - OBJECT_TYPE);
												}
												BgL_oclassz00_1698 =
													VECTOR_REF(BgL_arg1815z00_1699, BgL_arg1816z00_1700);
											}
											{	/* Coerce/walk.scm 45 */
												bool_t BgL__ortest_1115z00_1702;

												BgL__ortest_1115z00_1702 =
													(BgL_classz00_1692 == BgL_oclassz00_1698);
												if (BgL__ortest_1115z00_1702)
													{	/* Coerce/walk.scm 45 */
														BgL_res1586z00_1697 = BgL__ortest_1115z00_1702;
													}
												else
													{	/* Coerce/walk.scm 45 */
														long BgL_odepthz00_1703;

														{	/* Coerce/walk.scm 45 */
															obj_t BgL_arg1804z00_1704;

															BgL_arg1804z00_1704 = (BgL_oclassz00_1698);
															BgL_odepthz00_1703 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_1704);
														}
														if ((2L < BgL_odepthz00_1703))
															{	/* Coerce/walk.scm 45 */
																obj_t BgL_arg1802z00_1705;

																{	/* Coerce/walk.scm 45 */
																	obj_t BgL_arg1803z00_1706;

																	BgL_arg1803z00_1706 = (BgL_oclassz00_1698);
																	BgL_arg1802z00_1705 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1706,
																		2L);
																}
																BgL_res1586z00_1697 =
																	(BgL_arg1802z00_1705 == BgL_classz00_1692);
															}
														else
															{	/* Coerce/walk.scm 45 */
																BgL_res1586z00_1697 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1631z00_1841 = BgL_res1586z00_1697;
									}
							}
						}
					}
					if (BgL_test1631z00_1841)
						{	/* Coerce/walk.scm 45 */
							BgL_test1630z00_1840 = ((bool_t) 0);
						}
					else
						{	/* Coerce/walk.scm 46 */
							bool_t BgL__ortest_1108z00_1707;

							BgL__ortest_1108z00_1707 =
								(
								(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_globalz00_1679)))->
									BgL_importz00) == CNST_TABLE_REF(2));
							if (BgL__ortest_1108z00_1707)
								{	/* Coerce/walk.scm 46 */
									BgL_test1630z00_1840 = BgL__ortest_1108z00_1707;
								}
							else
								{	/* Coerce/walk.scm 46 */
									BgL_test1630z00_1840 =
										(
										(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_globalz00_1679)))->
											BgL_importz00) == CNST_TABLE_REF(3));
								}
						}
				}
				if (BgL_test1630z00_1840)
					{	/* Coerce/walk.scm 45 */
						return
							BGl_pvariablezd2protozd2zzcoerce_pprotoz00(3L,
							((BgL_variablez00_bglt) BgL_globalz00_1679));
					}
				else
					{	/* Coerce/walk.scm 45 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcoerce_walkz00(void)
	{
		{	/* Coerce/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcoerce_walkz00(void)
	{
		{	/* Coerce/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcoerce_walkz00(void)
	{
		{	/* Coerce/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcoerce_walkz00(void)
	{
		{	/* Coerce/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zzcoerce_pprotoz00(44915300L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzcoerce_convertz00(87995645L,
				BSTRING_TO_STRING(BGl_string1599z00zzcoerce_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
