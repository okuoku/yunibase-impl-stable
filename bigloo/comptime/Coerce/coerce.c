/*===========================================================================*/
/*   (Coerce/coerce.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Coerce/coerce.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_COERCE_COERCE_TYPE_DEFINITIONS
#define BGL_COERCE_COERCE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_wideningz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_wideningz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_COERCE_COERCE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62coercez12zd2sequence1301za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzcoerce_coercez00 = BUNSPEC;
	extern obj_t BGl_pvariablezd2protozd2zzcoerce_pprotoz00(long,
		BgL_variablez00_bglt);
	static BgL_nodez00_bglt BGl_z62coercez12z70zzcoerce_coercez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_newz00zzast_nodez00;
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2setzd2exzd2it1337za2zzcoerce_coercez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzcoerce_coercez00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2vref1317za2zzcoerce_coercez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static BgL_castz00_bglt BGl_castzd2nodezd2zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzcoerce_coercez00(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_coercezd2functionz12zc0zzcoerce_coercez00(BgL_variablez00_bglt, bool_t);
	extern obj_t BGl_deczd2ppmargez12zc0zzcoerce_pprotoz00(void);
	static obj_t BGl_objectzd2initzd2zzcoerce_coercez00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	extern obj_t BGl_castz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2letzd2var1335z70zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_za2magicza2z00zztype_cachez00;
	extern obj_t BGl_pfunctionzd2protozd2zzcoerce_pprotoz00(long,
		BgL_variablez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2valloc1315za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_thezd2coercedzd2functionz00zzcoerce_coercez00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62coercez12zd2new1313za2zzcoerce_coercez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2boxzd2ref1344z70zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2exitza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcoerce_coercez00(void);
	extern bool_t BGl_typezd2disjointzf3z21zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62coercez121290z70zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62coercezd2functionz12za2zzcoerce_coercez00(obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2kwote1295za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2getfield1309za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2mutexza2z00zztype_cachez00;
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2closure1299za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern bool_t BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2extern1305za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00;
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt, obj_t,
		BgL_typez00_bglt, bool_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2atom1293za2zzcoerce_coercez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2setfield1311za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_funcallz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_applyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_convertz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_pprotoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2predicatezd2ofz00zztype_miscz00(BgL_appz00_bglt);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_wideningz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2switch1331za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2vsetz121319zb0zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2conditional1327za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern bool_t BGl_coercerzd2existszf3z21zztype_coercionz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcoerce_coercez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_coercez00(void);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzcoerce_coercez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcoerce_coercez00(void);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2vlength1321za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_inczd2ppmargez12zc0zzcoerce_pprotoz00(void);
	static BgL_nodez00_bglt BGl_z62coercez12zd2var1297za2zzcoerce_coercez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2cast1323za2zzcoerce_coercez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2unsafezd2evalza2zd2zzengine_paramz00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2boxzd2setz121346z62zzcoerce_coercez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2makezd2box1342z70zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2sync1303za2zzcoerce_coercez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_runtimezd2typezd2errorz00zzcoerce_convertz00(obj_t, obj_t,
		BgL_nodez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2setq1325za2zzcoerce_coercez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2jumpzd2exzd2it1340za2zzcoerce_coercez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2letzd2fun1333z70zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2fail1329za2zzcoerce_coercez00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_isazd2ofzd2zztype_miscz00(BgL_nodez00_bglt);
	extern BgL_nodez00_bglt BGl_convertz12z12zzcoerce_convertz00(BgL_nodez00_bglt,
		BgL_typez00_bglt, BgL_typez00_bglt, bool_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2widening1307za2zzcoerce_coercez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2112z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2s2143za7,
		BGl_z62coercez12zd2sync1303za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2113z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2e2144za7,
		BGl_z62coercez12zd2extern1305za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2114z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2w2145za7,
		BGl_z62coercez12zd2widening1307za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2115z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2g2146za7,
		BGl_z62coercez12zd2getfield1309za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2116z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2s2147za7,
		BGl_z62coercez12zd2setfield1311za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2117z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2n2148za7,
		BGl_z62coercez12zd2new1313za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2118z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2v2149za7,
		BGl_z62coercez12zd2valloc1315za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2119z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2v2150za7,
		BGl_z62coercez12zd2vref1317za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2120z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2v2151za7,
		BGl_z62coercez12zd2vsetz121319zb0zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2121z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2v2152za7,
		BGl_z62coercez12zd2vlength1321za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2122z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2c2153za7,
		BGl_z62coercez12zd2cast1323za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2s2154za7,
		BGl_z62coercez12zd2setq1325za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2c2155za7,
		BGl_z62coercez12zd2conditional1327za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2f2156za7,
		BGl_z62coercez12zd2fail1329za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2s2157za7,
		BGl_z62coercez12zd2switch1331za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2l2158za7,
		BGl_z62coercez12zd2letzd2fun1333z70zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2134z00zzcoerce_coercez00,
		BgL_bgl_string2134za700za7za7c2159za7, " test-node-type=", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2l2160za7,
		BGl_z62coercez12zd2letzd2var1335z70zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2135z00zzcoerce_coercez00,
		BgL_bgl_string2135za700za7za7c2161za7, "switch test-type=", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2s2162za7,
		BGl_z62coercez12zd2setzd2exzd2it1337za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2136z00zzcoerce_coercez00,
		BgL_bgl_string2136za700za7za7c2163za7, ":", 1);
	      DEFINE_STRING(BGl_string2137z00zzcoerce_coercez00,
		BgL_bgl_string2137za700za7za7c2164za7, ",", 1);
	      DEFINE_STRING(BGl_string2138z00zzcoerce_coercez00,
		BgL_bgl_string2138za700za7za7c2165za7, "Coerce/coerce.scm", 17);
	      DEFINE_STRING(BGl_string2139z00zzcoerce_coercez00,
		BgL_bgl_string2139za700za7za7c2166za7, "Unexepected `closure' node", 26);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2j2167za7,
		BGl_z62coercez12zd2jumpzd2exzd2it1340za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2m2168za7,
		BGl_z62coercez12zd2makezd2box1342z70zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2b2169za7,
		BGl_z62coercez12zd2boxzd2ref1344z70zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2b2170za7,
		BGl_z62coercez12zd2boxzd2setz121346z62zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2140z00zzcoerce_coercez00,
		BgL_bgl_string2140za700za7za7c2171za7, "coerce_coerce", 13);
	      DEFINE_STRING(BGl_string2141z00zzcoerce_coercez00,
		BgL_bgl_string2141za700za7za7c2172za7, "false true coerce!1290 ", 23);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_coercez12zd2envzc0zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za770za72173z00,
		BGl_z62coercez12z70zzcoerce_coercez00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_coercezd2functionz12zd2envz12zzcoerce_coercez00,
		BgL_bgl_za762coerceza7d2func2174z00,
		BGl_z62coercezd2functionz12za2zzcoerce_coercez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2104z00zzcoerce_coercez00,
		BgL_bgl_string2104za700za7za7c2175za7, "coerce!1290", 11);
	      DEFINE_STRING(BGl_string2105z00zzcoerce_coercez00,
		BgL_bgl_string2105za700za7za7c2176za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2107z00zzcoerce_coercez00,
		BgL_bgl_string2107za700za7za7c2177za7, "coerce!", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2103z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza71212902178z00,
		BGl_z62coercez121290z70zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2106z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2a2179za7,
		BGl_z62coercez12zd2atom1293za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2108z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2k2180za7,
		BGl_z62coercez12zd2kwote1295za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2109z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2v2181za7,
		BGl_z62coercez12zd2var1297za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2110z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2c2182za7,
		BGl_z62coercez12zd2closure1299za2zzcoerce_coercez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2111z00zzcoerce_coercez00,
		BgL_bgl_za762coerceza712za7d2s2183za7,
		BGl_z62coercez12zd2sequence1301za2zzcoerce_coercez00, 0L, BUNSPEC, 4);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcoerce_coercez00));
		     ADD_ROOT((void
				*) (&BGl_thezd2coercedzd2functionz00zzcoerce_coercez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long
		BgL_checksumz00_4158, char *BgL_fromz00_4159)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcoerce_coercez00))
				{
					BGl_requirezd2initializa7ationz75zzcoerce_coercez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcoerce_coercez00();
					BGl_libraryzd2moduleszd2initz00zzcoerce_coercez00();
					BGl_cnstzd2initzd2zzcoerce_coercez00();
					BGl_importedzd2moduleszd2initz00zzcoerce_coercez00();
					BGl_genericzd2initzd2zzcoerce_coercez00();
					BGl_methodzd2initzd2zzcoerce_coercez00();
					return BGl_toplevelzd2initzd2zzcoerce_coercez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_coercez00(void)
	{
		{	/* Coerce/coerce.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"coerce_coerce");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"coerce_coerce");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcoerce_coercez00(void)
	{
		{	/* Coerce/coerce.scm 15 */
			{	/* Coerce/coerce.scm 15 */
				obj_t BgL_cportz00_3680;

				{	/* Coerce/coerce.scm 15 */
					obj_t BgL_stringz00_3687;

					BgL_stringz00_3687 = BGl_string2141z00zzcoerce_coercez00;
					{	/* Coerce/coerce.scm 15 */
						obj_t BgL_startz00_3688;

						BgL_startz00_3688 = BINT(0L);
						{	/* Coerce/coerce.scm 15 */
							obj_t BgL_endz00_3689;

							BgL_endz00_3689 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3687)));
							{	/* Coerce/coerce.scm 15 */

								BgL_cportz00_3680 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3687, BgL_startz00_3688, BgL_endz00_3689);
				}}}}
				{
					long BgL_iz00_3681;

					BgL_iz00_3681 = 2L;
				BgL_loopz00_3682:
					if ((BgL_iz00_3681 == -1L))
						{	/* Coerce/coerce.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Coerce/coerce.scm 15 */
							{	/* Coerce/coerce.scm 15 */
								obj_t BgL_arg2142z00_3683;

								{	/* Coerce/coerce.scm 15 */

									{	/* Coerce/coerce.scm 15 */
										obj_t BgL_locationz00_3685;

										BgL_locationz00_3685 = BBOOL(((bool_t) 0));
										{	/* Coerce/coerce.scm 15 */

											BgL_arg2142z00_3683 =
												BGl_readz00zz__readerz00(BgL_cportz00_3680,
												BgL_locationz00_3685);
										}
									}
								}
								{	/* Coerce/coerce.scm 15 */
									int BgL_tmpz00_4190;

									BgL_tmpz00_4190 = (int) (BgL_iz00_3681);
									CNST_TABLE_SET(BgL_tmpz00_4190, BgL_arg2142z00_3683);
							}}
							{	/* Coerce/coerce.scm 15 */
								int BgL_auxz00_3686;

								BgL_auxz00_3686 = (int) ((BgL_iz00_3681 - 1L));
								{
									long BgL_iz00_4195;

									BgL_iz00_4195 = (long) (BgL_auxz00_3686);
									BgL_iz00_3681 = BgL_iz00_4195;
									goto BgL_loopz00_3682;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcoerce_coercez00(void)
	{
		{	/* Coerce/coerce.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcoerce_coercez00(void)
	{
		{	/* Coerce/coerce.scm 15 */
			BGl_thezd2coercedzd2functionz00zzcoerce_coercez00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* coerce-function! */
	BGL_EXPORTED_DEF obj_t
		BGl_coercezd2functionz12zc0zzcoerce_coercez00(BgL_variablez00_bglt
		BgL_variablez00_3, bool_t BgL_typezd2safezd2_4)
	{
		{	/* Coerce/coerce.scm 46 */
			BGl_enterzd2functionzd2zztools_errorz00(
				(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_3))->BgL_idz00));
			{	/* Coerce/coerce.scm 49 */
				BgL_valuez00_bglt BgL_funz00_1694;

				BgL_funz00_1694 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_3))->BgL_valuez00);
				{	/* Coerce/coerce.scm 49 */
					obj_t BgL_bodyz00_1695;

					BgL_bodyz00_1695 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1694)))->BgL_bodyz00);
					{	/* Coerce/coerce.scm 50 */
						BgL_typez00_bglt BgL_tresz00_1696;

						BgL_tresz00_1696 =
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_3))->
							BgL_typez00);
						{	/* Coerce/coerce.scm 51 */
							obj_t BgL_cloz00_1697;

							BgL_cloz00_1697 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_funz00_1694)))->
								BgL_thezd2closurezd2globalz00);
							{	/* Coerce/coerce.scm 52 */
								bool_t BgL_typezd2safetyzd2enforcedz00_1698;

								{	/* Coerce/coerce.scm 53 */
									obj_t BgL__andtest_1109z00_1708;

									BgL__andtest_1109z00_1708 =
										BGl_za2unsafezd2evalza2zd2zzengine_paramz00;
									if (CBOOL(BgL__andtest_1109z00_1708))
										{	/* Coerce/coerce.scm 53 */
											BgL_typezd2safetyzd2enforcedz00_1698 = ((bool_t) 0);
										}
									else
										{	/* Coerce/coerce.scm 54 */
											bool_t BgL_test2187z00_4208;

											{	/* Coerce/coerce.scm 54 */
												obj_t BgL_classz00_2489;

												BgL_classz00_2489 = BGl_globalz00zzast_varz00;
												{	/* Coerce/coerce.scm 54 */
													BgL_objectz00_bglt BgL_arg1807z00_2491;

													{	/* Coerce/coerce.scm 54 */
														obj_t BgL_tmpz00_4209;

														BgL_tmpz00_4209 = ((obj_t) BgL_variablez00_3);
														BgL_arg1807z00_2491 =
															(BgL_objectz00_bglt) (BgL_tmpz00_4209);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Coerce/coerce.scm 54 */
															long BgL_idxz00_2497;

															BgL_idxz00_2497 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2491);
															BgL_test2187z00_4208 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2497 + 2L)) == BgL_classz00_2489);
														}
													else
														{	/* Coerce/coerce.scm 54 */
															bool_t BgL_res2073z00_2522;

															{	/* Coerce/coerce.scm 54 */
																obj_t BgL_oclassz00_2505;

																{	/* Coerce/coerce.scm 54 */
																	obj_t BgL_arg1815z00_2513;
																	long BgL_arg1816z00_2514;

																	BgL_arg1815z00_2513 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Coerce/coerce.scm 54 */
																		long BgL_arg1817z00_2515;

																		BgL_arg1817z00_2515 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2491);
																		BgL_arg1816z00_2514 =
																			(BgL_arg1817z00_2515 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2505 =
																		VECTOR_REF(BgL_arg1815z00_2513,
																		BgL_arg1816z00_2514);
																}
																{	/* Coerce/coerce.scm 54 */
																	bool_t BgL__ortest_1115z00_2506;

																	BgL__ortest_1115z00_2506 =
																		(BgL_classz00_2489 == BgL_oclassz00_2505);
																	if (BgL__ortest_1115z00_2506)
																		{	/* Coerce/coerce.scm 54 */
																			BgL_res2073z00_2522 =
																				BgL__ortest_1115z00_2506;
																		}
																	else
																		{	/* Coerce/coerce.scm 54 */
																			long BgL_odepthz00_2507;

																			{	/* Coerce/coerce.scm 54 */
																				obj_t BgL_arg1804z00_2508;

																				BgL_arg1804z00_2508 =
																					(BgL_oclassz00_2505);
																				BgL_odepthz00_2507 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2508);
																			}
																			if ((2L < BgL_odepthz00_2507))
																				{	/* Coerce/coerce.scm 54 */
																					obj_t BgL_arg1802z00_2510;

																					{	/* Coerce/coerce.scm 54 */
																						obj_t BgL_arg1803z00_2511;

																						BgL_arg1803z00_2511 =
																							(BgL_oclassz00_2505);
																						BgL_arg1802z00_2510 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2511, 2L);
																					}
																					BgL_res2073z00_2522 =
																						(BgL_arg1802z00_2510 ==
																						BgL_classz00_2489);
																				}
																			else
																				{	/* Coerce/coerce.scm 54 */
																					BgL_res2073z00_2522 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2187z00_4208 = BgL_res2073z00_2522;
														}
												}
											}
											if (BgL_test2187z00_4208)
												{	/* Coerce/coerce.scm 55 */
													bool_t BgL_test2191z00_4231;

													{	/* Coerce/coerce.scm 55 */
														obj_t BgL_classz00_2523;

														BgL_classz00_2523 = BGl_globalz00zzast_varz00;
														if (BGL_OBJECTP(BgL_cloz00_1697))
															{	/* Coerce/coerce.scm 55 */
																BgL_objectz00_bglt BgL_arg1807z00_2525;

																BgL_arg1807z00_2525 =
																	(BgL_objectz00_bglt) (BgL_cloz00_1697);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Coerce/coerce.scm 55 */
																		long BgL_idxz00_2531;

																		BgL_idxz00_2531 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2525);
																		BgL_test2191z00_4231 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2531 + 2L)) ==
																			BgL_classz00_2523);
																	}
																else
																	{	/* Coerce/coerce.scm 55 */
																		bool_t BgL_res2074z00_2556;

																		{	/* Coerce/coerce.scm 55 */
																			obj_t BgL_oclassz00_2539;

																			{	/* Coerce/coerce.scm 55 */
																				obj_t BgL_arg1815z00_2547;
																				long BgL_arg1816z00_2548;

																				BgL_arg1815z00_2547 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Coerce/coerce.scm 55 */
																					long BgL_arg1817z00_2549;

																					BgL_arg1817z00_2549 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2525);
																					BgL_arg1816z00_2548 =
																						(BgL_arg1817z00_2549 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2539 =
																					VECTOR_REF(BgL_arg1815z00_2547,
																					BgL_arg1816z00_2548);
																			}
																			{	/* Coerce/coerce.scm 55 */
																				bool_t BgL__ortest_1115z00_2540;

																				BgL__ortest_1115z00_2540 =
																					(BgL_classz00_2523 ==
																					BgL_oclassz00_2539);
																				if (BgL__ortest_1115z00_2540)
																					{	/* Coerce/coerce.scm 55 */
																						BgL_res2074z00_2556 =
																							BgL__ortest_1115z00_2540;
																					}
																				else
																					{	/* Coerce/coerce.scm 55 */
																						long BgL_odepthz00_2541;

																						{	/* Coerce/coerce.scm 55 */
																							obj_t BgL_arg1804z00_2542;

																							BgL_arg1804z00_2542 =
																								(BgL_oclassz00_2539);
																							BgL_odepthz00_2541 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2542);
																						}
																						if ((2L < BgL_odepthz00_2541))
																							{	/* Coerce/coerce.scm 55 */
																								obj_t BgL_arg1802z00_2544;

																								{	/* Coerce/coerce.scm 55 */
																									obj_t BgL_arg1803z00_2545;

																									BgL_arg1803z00_2545 =
																										(BgL_oclassz00_2539);
																									BgL_arg1802z00_2544 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2545, 2L);
																								}
																								BgL_res2074z00_2556 =
																									(BgL_arg1802z00_2544 ==
																									BgL_classz00_2523);
																							}
																						else
																							{	/* Coerce/coerce.scm 55 */
																								BgL_res2074z00_2556 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2191z00_4231 = BgL_res2074z00_2556;
																	}
															}
														else
															{	/* Coerce/coerce.scm 55 */
																BgL_test2191z00_4231 = ((bool_t) 0);
															}
													}
													if (BgL_test2191z00_4231)
														{	/* Coerce/coerce.scm 55 */
															if (
																(((BgL_globalz00_bglt) COBJECT(
																			((BgL_globalz00_bglt) BgL_cloz00_1697)))->
																	BgL_evaluablezf3zf3))
																{	/* Coerce/coerce.scm 56 */
																	BgL_typezd2safetyzd2enforcedz00_1698 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_cloz00_1697))))->
																		BgL_userzf3zf3);
																}
															else
																{	/* Coerce/coerce.scm 56 */
																	BgL_typezd2safetyzd2enforcedz00_1698 =
																		((bool_t) 0);
																}
														}
													else
														{	/* Coerce/coerce.scm 55 */
															BgL_typezd2safetyzd2enforcedz00_1698 =
																((bool_t) 0);
														}
												}
											else
												{	/* Coerce/coerce.scm 54 */
													BgL_typezd2safetyzd2enforcedz00_1698 = ((bool_t) 0);
												}
										}
								}
								{	/* Coerce/coerce.scm 58 */

									{	/* Coerce/coerce.scm 59 */
										bool_t BgL_test2197z00_4260;

										{	/* Coerce/coerce.scm 59 */
											obj_t BgL_classz00_2559;

											BgL_classz00_2559 = BGl_globalz00zzast_varz00;
											{	/* Coerce/coerce.scm 59 */
												BgL_objectz00_bglt BgL_arg1807z00_2561;

												{	/* Coerce/coerce.scm 59 */
													obj_t BgL_tmpz00_4261;

													BgL_tmpz00_4261 = ((obj_t) BgL_variablez00_3);
													BgL_arg1807z00_2561 =
														(BgL_objectz00_bglt) (BgL_tmpz00_4261);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Coerce/coerce.scm 59 */
														long BgL_idxz00_2567;

														BgL_idxz00_2567 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2561);
														BgL_test2197z00_4260 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2567 + 2L)) == BgL_classz00_2559);
													}
												else
													{	/* Coerce/coerce.scm 59 */
														bool_t BgL_res2075z00_2592;

														{	/* Coerce/coerce.scm 59 */
															obj_t BgL_oclassz00_2575;

															{	/* Coerce/coerce.scm 59 */
																obj_t BgL_arg1815z00_2583;
																long BgL_arg1816z00_2584;

																BgL_arg1815z00_2583 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Coerce/coerce.scm 59 */
																	long BgL_arg1817z00_2585;

																	BgL_arg1817z00_2585 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2561);
																	BgL_arg1816z00_2584 =
																		(BgL_arg1817z00_2585 - OBJECT_TYPE);
																}
																BgL_oclassz00_2575 =
																	VECTOR_REF(BgL_arg1815z00_2583,
																	BgL_arg1816z00_2584);
															}
															{	/* Coerce/coerce.scm 59 */
																bool_t BgL__ortest_1115z00_2576;

																BgL__ortest_1115z00_2576 =
																	(BgL_classz00_2559 == BgL_oclassz00_2575);
																if (BgL__ortest_1115z00_2576)
																	{	/* Coerce/coerce.scm 59 */
																		BgL_res2075z00_2592 =
																			BgL__ortest_1115z00_2576;
																	}
																else
																	{	/* Coerce/coerce.scm 59 */
																		long BgL_odepthz00_2577;

																		{	/* Coerce/coerce.scm 59 */
																			obj_t BgL_arg1804z00_2578;

																			BgL_arg1804z00_2578 =
																				(BgL_oclassz00_2575);
																			BgL_odepthz00_2577 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2578);
																		}
																		if ((2L < BgL_odepthz00_2577))
																			{	/* Coerce/coerce.scm 59 */
																				obj_t BgL_arg1802z00_2580;

																				{	/* Coerce/coerce.scm 59 */
																					obj_t BgL_arg1803z00_2581;

																					BgL_arg1803z00_2581 =
																						(BgL_oclassz00_2575);
																					BgL_arg1802z00_2580 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2581, 2L);
																				}
																				BgL_res2075z00_2592 =
																					(BgL_arg1802z00_2580 ==
																					BgL_classz00_2559);
																			}
																		else
																			{	/* Coerce/coerce.scm 59 */
																				BgL_res2075z00_2592 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2197z00_4260 = BgL_res2075z00_2592;
													}
											}
										}
										if (BgL_test2197z00_4260)
											{	/* Coerce/coerce.scm 59 */
												BNIL;
											}
										else
											{	/* Coerce/coerce.scm 59 */
												BNIL;
											}
									}
									{	/* Coerce/coerce.scm 67 */
										obj_t BgL_notifyz00_1701;

										BgL_notifyz00_1701 =
											BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00;
										if ((((BgL_variablez00_bglt) COBJECT(BgL_variablez00_3))->
												BgL_userzf3zf3))
											{	/* Coerce/coerce.scm 70 */
												bool_t BgL_test2202z00_4285;

												{	/* Coerce/coerce.scm 70 */
													obj_t BgL_arg1364z00_1705;

													BgL_arg1364z00_1705 =
														(((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_funz00_1694)))->
														BgL_thezd2closurezd2globalz00);
													{	/* Coerce/coerce.scm 70 */
														obj_t BgL_classz00_2595;

														BgL_classz00_2595 = BGl_globalz00zzast_varz00;
														if (BGL_OBJECTP(BgL_arg1364z00_1705))
															{	/* Coerce/coerce.scm 70 */
																BgL_objectz00_bglt BgL_arg1807z00_2597;

																BgL_arg1807z00_2597 =
																	(BgL_objectz00_bglt) (BgL_arg1364z00_1705);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Coerce/coerce.scm 70 */
																		long BgL_idxz00_2603;

																		BgL_idxz00_2603 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2597);
																		BgL_test2202z00_4285 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2603 + 2L)) ==
																			BgL_classz00_2595);
																	}
																else
																	{	/* Coerce/coerce.scm 70 */
																		bool_t BgL_res2076z00_2628;

																		{	/* Coerce/coerce.scm 70 */
																			obj_t BgL_oclassz00_2611;

																			{	/* Coerce/coerce.scm 70 */
																				obj_t BgL_arg1815z00_2619;
																				long BgL_arg1816z00_2620;

																				BgL_arg1815z00_2619 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Coerce/coerce.scm 70 */
																					long BgL_arg1817z00_2621;

																					BgL_arg1817z00_2621 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2597);
																					BgL_arg1816z00_2620 =
																						(BgL_arg1817z00_2621 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2611 =
																					VECTOR_REF(BgL_arg1815z00_2619,
																					BgL_arg1816z00_2620);
																			}
																			{	/* Coerce/coerce.scm 70 */
																				bool_t BgL__ortest_1115z00_2612;

																				BgL__ortest_1115z00_2612 =
																					(BgL_classz00_2595 ==
																					BgL_oclassz00_2611);
																				if (BgL__ortest_1115z00_2612)
																					{	/* Coerce/coerce.scm 70 */
																						BgL_res2076z00_2628 =
																							BgL__ortest_1115z00_2612;
																					}
																				else
																					{	/* Coerce/coerce.scm 70 */
																						long BgL_odepthz00_2613;

																						{	/* Coerce/coerce.scm 70 */
																							obj_t BgL_arg1804z00_2614;

																							BgL_arg1804z00_2614 =
																								(BgL_oclassz00_2611);
																							BgL_odepthz00_2613 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2614);
																						}
																						if ((2L < BgL_odepthz00_2613))
																							{	/* Coerce/coerce.scm 70 */
																								obj_t BgL_arg1802z00_2616;

																								{	/* Coerce/coerce.scm 70 */
																									obj_t BgL_arg1803z00_2617;

																									BgL_arg1803z00_2617 =
																										(BgL_oclassz00_2611);
																									BgL_arg1802z00_2616 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2617, 2L);
																								}
																								BgL_res2076z00_2628 =
																									(BgL_arg1802z00_2616 ==
																									BgL_classz00_2595);
																							}
																						else
																							{	/* Coerce/coerce.scm 70 */
																								BgL_res2076z00_2628 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2202z00_4285 = BgL_res2076z00_2628;
																	}
															}
														else
															{	/* Coerce/coerce.scm 70 */
																BgL_test2202z00_4285 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test2202z00_4285)
													{	/* Coerce/coerce.scm 70 */
														BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00
															= BFALSE;
													}
												else
													{	/* Coerce/coerce.scm 70 */
														BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00
															= BTRUE;
													}
											}
										else
											{	/* Coerce/coerce.scm 69 */
												BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00 =
													BFALSE;
											}
										BGl_pfunctionzd2protozd2zzcoerce_pprotoz00(3L,
											BgL_variablez00_3);
										BGl_thezd2coercedzd2functionz00zzcoerce_coercez00 =
											((obj_t) BgL_variablez00_3);
										{	/* Coerce/coerce.scm 73 */
											BgL_nodez00_bglt BgL_arg1367z00_1706;

											{	/* Coerce/coerce.scm 73 */
												bool_t BgL_auxz00_4312;

												if (BgL_typezd2safezd2_4)
													{	/* Coerce/coerce.scm 58 */
														BgL_auxz00_4312 = BgL_typezd2safezd2_4;
													}
												else
													{	/* Coerce/coerce.scm 58 */
														BgL_auxz00_4312 =
															BgL_typezd2safetyzd2enforcedz00_1698;
													}
												BgL_arg1367z00_1706 =
													BGl_coercez12z12zzcoerce_coercez00(
													((BgL_nodez00_bglt) BgL_bodyz00_1695),
													((obj_t) BgL_variablez00_3), BgL_tresz00_1696,
													BgL_auxz00_4312);
											}
											((((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_1694)))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_arg1367z00_1706)), BUNSPEC);
										}
										BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00 =
											BgL_notifyz00_1701;
									}
									return BGl_leavezd2functionzd2zztools_errorz00();
								}
							}
						}
					}
				}
			}
		}

	}



/* &coerce-function! */
	obj_t BGl_z62coercezd2functionz12za2zzcoerce_coercez00(obj_t BgL_envz00_3504,
		obj_t BgL_variablez00_3505, obj_t BgL_typezd2safezd2_3506)
	{
		{	/* Coerce/coerce.scm 46 */
			return
				BGl_coercezd2functionz12zc0zzcoerce_coercez00(
				((BgL_variablez00_bglt) BgL_variablez00_3505),
				CBOOL(BgL_typezd2safezd2_3506));
		}

	}



/* cast-node */
	BgL_castz00_bglt BGl_castzd2nodezd2zzcoerce_coercez00(BgL_nodez00_bglt
		BgL_nodez00_105, obj_t BgL_toz00_106)
	{
		{	/* Coerce/coerce.scm 491 */
			{	/* Coerce/coerce.scm 493 */
				BgL_castz00_bglt BgL_new1151z00_1713;

				{	/* Coerce/coerce.scm 494 */
					BgL_castz00_bglt BgL_new1150z00_1714;

					BgL_new1150z00_1714 =
						((BgL_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_castz00_bgl))));
					{	/* Coerce/coerce.scm 494 */
						long BgL_arg1370z00_1715;

						BgL_arg1370z00_1715 = BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1150z00_1714), BgL_arg1370z00_1715);
					}
					{	/* Coerce/coerce.scm 494 */
						BgL_objectz00_bglt BgL_tmpz00_4328;

						BgL_tmpz00_4328 = ((BgL_objectz00_bglt) BgL_new1150z00_1714);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4328, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1150z00_1714);
					BgL_new1151z00_1713 = BgL_new1150z00_1714;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1151z00_1713)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_105))->BgL_locz00)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1151z00_1713)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_toz00_106)), BUNSPEC);
				((((BgL_castz00_bglt) COBJECT(BgL_new1151z00_1713))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_nodez00_105), BUNSPEC);
				return BgL_new1151z00_1713;
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcoerce_coercez00(void)
	{
		{	/* Coerce/coerce.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcoerce_coercez00(void)
	{
		{	/* Coerce/coerce.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_proc2103z00zzcoerce_coercez00, BGl_nodez00zzast_nodez00,
				BGl_string2104z00zzcoerce_coercez00);
		}

	}



/* &coerce!1290 */
	obj_t BGl_z62coercez121290z70zzcoerce_coercez00(obj_t BgL_envz00_3508,
		obj_t BgL_nodez00_3509, obj_t BgL_callerz00_3510, obj_t BgL_toz00_3511,
		obj_t BgL_safez00_3512)
	{
		{	/* Coerce/coerce.scm 80 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string2105z00zzcoerce_coercez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3509)));
		}

	}



/* coerce! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt BgL_nodez00_5,
		obj_t BgL_callerz00_6, BgL_typez00_bglt BgL_toz00_7, bool_t BgL_safez00_8)
	{
		{	/* Coerce/coerce.scm 80 */
			{	/* Coerce/coerce.scm 80 */
				obj_t BgL_method1291z00_1723;

				{	/* Coerce/coerce.scm 80 */
					obj_t BgL_res2081z00_2664;

					{	/* Coerce/coerce.scm 80 */
						long BgL_objzd2classzd2numz00_2635;

						BgL_objzd2classzd2numz00_2635 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_5));
						{	/* Coerce/coerce.scm 80 */
							obj_t BgL_arg1811z00_2636;

							BgL_arg1811z00_2636 =
								PROCEDURE_REF(BGl_coercez12zd2envzc0zzcoerce_coercez00,
								(int) (1L));
							{	/* Coerce/coerce.scm 80 */
								int BgL_offsetz00_2639;

								BgL_offsetz00_2639 = (int) (BgL_objzd2classzd2numz00_2635);
								{	/* Coerce/coerce.scm 80 */
									long BgL_offsetz00_2640;

									BgL_offsetz00_2640 =
										((long) (BgL_offsetz00_2639) - OBJECT_TYPE);
									{	/* Coerce/coerce.scm 80 */
										long BgL_modz00_2641;

										BgL_modz00_2641 =
											(BgL_offsetz00_2640 >> (int) ((long) ((int) (4L))));
										{	/* Coerce/coerce.scm 80 */
											long BgL_restz00_2643;

											BgL_restz00_2643 =
												(BgL_offsetz00_2640 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Coerce/coerce.scm 80 */

												{	/* Coerce/coerce.scm 80 */
													obj_t BgL_bucketz00_2645;

													BgL_bucketz00_2645 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2636), BgL_modz00_2641);
													BgL_res2081z00_2664 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2645), BgL_restz00_2643);
					}}}}}}}}
					BgL_method1291z00_1723 = BgL_res2081z00_2664;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL4(BgL_method1291z00_1723,
						((obj_t) BgL_nodez00_5), BgL_callerz00_6,
						((obj_t) BgL_toz00_7), BBOOL(BgL_safez00_8)));
			}
		}

	}



/* &coerce! */
	BgL_nodez00_bglt BGl_z62coercez12z70zzcoerce_coercez00(obj_t BgL_envz00_3513,
		obj_t BgL_nodez00_3514, obj_t BgL_callerz00_3515, obj_t BgL_toz00_3516,
		obj_t BgL_safez00_3517)
	{
		{	/* Coerce/coerce.scm 80 */
			return
				BGl_coercez12z12zzcoerce_coercez00(
				((BgL_nodez00_bglt) BgL_nodez00_3514), BgL_callerz00_3515,
				((BgL_typez00_bglt) BgL_toz00_3516), CBOOL(BgL_safez00_3517));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcoerce_coercez00(void)
	{
		{	/* Coerce/coerce.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_atomz00zzast_nodez00,
				BGl_proc2106z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_kwotez00zzast_nodez00,
				BGl_proc2108z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_varz00zzast_nodez00,
				BGl_proc2109z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_closurez00zzast_nodez00,
				BGl_proc2110z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_sequencez00zzast_nodez00,
				BGl_proc2111z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_syncz00zzast_nodez00,
				BGl_proc2112z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_externz00zzast_nodez00,
				BGl_proc2113z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_wideningz00zzast_nodez00,
				BGl_proc2114z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_getfieldz00zzast_nodez00,
				BGl_proc2115z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_setfieldz00zzast_nodez00,
				BGl_proc2116z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_newz00zzast_nodez00,
				BGl_proc2117z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_vallocz00zzast_nodez00,
				BGl_proc2118z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_vrefz00zzast_nodez00,
				BGl_proc2119z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_vsetz12z12zzast_nodez00,
				BGl_proc2120z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_vlengthz00zzast_nodez00,
				BGl_proc2121z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_castz00zzast_nodez00,
				BGl_proc2122z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_setqz00zzast_nodez00,
				BGl_proc2123z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2124z00zzcoerce_coercez00,
				BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_failz00zzast_nodez00,
				BGl_proc2125z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_switchz00zzast_nodez00,
				BGl_proc2126z00zzcoerce_coercez00, BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2127z00zzcoerce_coercez00,
				BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2128z00zzcoerce_coercez00,
				BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2129z00zzcoerce_coercez00,
				BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2130z00zzcoerce_coercez00,
				BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2131z00zzcoerce_coercez00,
				BGl_string2107z00zzcoerce_coercez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2132z00zzcoerce_coercez00,
				BGl_string2107z00zzcoerce_coercez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2133z00zzcoerce_coercez00,
				BGl_string2107z00zzcoerce_coercez00);
		}

	}



/* &coerce!-box-set!1346 */
	BgL_nodez00_bglt
		BGl_z62coercez12zd2boxzd2setz121346z62zzcoerce_coercez00(obj_t
		BgL_envz00_3545, obj_t BgL_nodez00_3546, obj_t BgL_callerz00_3547,
		obj_t BgL_toz00_3548, obj_t BgL_safez00_3549)
	{
		{	/* Coerce/coerce.scm 523 */
			{	/* Coerce/coerce.scm 525 */
				BgL_nodez00_bglt BgL_vtz00_3695;

				BgL_vtz00_3695 =
					BGl_coercez12z12zzcoerce_coercez00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3546)))->BgL_valuez00),
					BgL_callerz00_3547,
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_nodez00_3546)))->BgL_vtypez00), CBOOL(BgL_safez00_3549));
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(((BgL_boxzd2setz12zc0_bglt)
									BgL_nodez00_3546)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BGl_coercez12z12zzcoerce_coercez00(BgL_vtz00_3695,
							BgL_callerz00_3547,
							((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
							CBOOL(BgL_safez00_3549))), BUNSPEC);
			}
			{	/* Coerce/coerce.scm 527 */
				BgL_typez00_bglt BgL_arg2003z00_3696;

				BgL_arg2003z00_3696 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3546))))->BgL_typez00);
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3546)), BgL_arg2003z00_3696,
					((BgL_typez00_bglt) BgL_toz00_3548), CBOOL(BgL_safez00_3549));
			}
		}

	}



/* &coerce!-box-ref1344 */
	BgL_nodez00_bglt BGl_z62coercez12zd2boxzd2ref1344z70zzcoerce_coercez00(obj_t
		BgL_envz00_3550, obj_t BgL_nodez00_3551, obj_t BgL_callerz00_3552,
		obj_t BgL_toz00_3553, obj_t BgL_safez00_3554)
	{
		{	/* Coerce/coerce.scm 512 */
			{	/* Coerce/coerce.scm 514 */
				BgL_nodez00_bglt BgL_tnodez00_3698;

				{	/* Coerce/coerce.scm 514 */
					bool_t BgL_test2208z00_4430;

					{	/* Coerce/coerce.scm 514 */
						BgL_typez00_bglt BgL_arg2000z00_3699;

						BgL_arg2000z00_3699 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nodez00_3551))))->BgL_typez00);
						BgL_test2208z00_4430 =
							(
							((obj_t) BgL_arg2000z00_3699) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2208z00_4430)
						{	/* Coerce/coerce.scm 514 */
							BgL_tnodez00_3698 =
								((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3551));
						}
					else
						{	/* Coerce/coerce.scm 514 */
							BgL_typez00_bglt BgL_arg1999z00_3700;

							BgL_arg1999z00_3700 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_boxzd2refzd2_bglt) BgL_nodez00_3551))))->
								BgL_typez00);
							BgL_tnodez00_3698 =
								((BgL_nodez00_bglt)
								BGl_castzd2nodezd2zzcoerce_coercez00(((BgL_nodez00_bglt) (
											(BgL_boxzd2refzd2_bglt) BgL_nodez00_3551)),
									((obj_t) BgL_arg1999z00_3700)));
						}
				}
				{	/* Coerce/coerce.scm 514 */
					BgL_nodez00_bglt BgL_cnodez00_3701;

					{	/* Coerce/coerce.scm 515 */
						BgL_typez00_bglt BgL_arg1996z00_3702;

						BgL_arg1996z00_3702 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3551)))->BgL_vtypez00);
						BgL_cnodez00_3701 =
							BGl_convertz12z12zzcoerce_convertz00(BgL_tnodez00_3698,
							BgL_arg1996z00_3702, ((BgL_typez00_bglt) BgL_toz00_3553),
							CBOOL(BgL_safez00_3554));
					}
					{	/* Coerce/coerce.scm 515 */

						{	/* Coerce/coerce.scm 516 */
							bool_t BgL_test2209z00_4451;

							{	/* Coerce/coerce.scm 516 */
								obj_t BgL_classz00_3703;

								BgL_classz00_3703 = BGl_tclassz00zzobject_classz00;
								if (BGL_OBJECTP(BgL_toz00_3553))
									{	/* Coerce/coerce.scm 516 */
										BgL_objectz00_bglt BgL_arg1807z00_3704;

										BgL_arg1807z00_3704 = (BgL_objectz00_bglt) (BgL_toz00_3553);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Coerce/coerce.scm 516 */
												long BgL_idxz00_3705;

												BgL_idxz00_3705 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3704);
												BgL_test2209z00_4451 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3705 + 2L)) == BgL_classz00_3703);
											}
										else
											{	/* Coerce/coerce.scm 516 */
												bool_t BgL_res2095z00_3708;

												{	/* Coerce/coerce.scm 516 */
													obj_t BgL_oclassz00_3709;

													{	/* Coerce/coerce.scm 516 */
														obj_t BgL_arg1815z00_3710;
														long BgL_arg1816z00_3711;

														BgL_arg1815z00_3710 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Coerce/coerce.scm 516 */
															long BgL_arg1817z00_3712;

															BgL_arg1817z00_3712 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3704);
															BgL_arg1816z00_3711 =
																(BgL_arg1817z00_3712 - OBJECT_TYPE);
														}
														BgL_oclassz00_3709 =
															VECTOR_REF(BgL_arg1815z00_3710,
															BgL_arg1816z00_3711);
													}
													{	/* Coerce/coerce.scm 516 */
														bool_t BgL__ortest_1115z00_3713;

														BgL__ortest_1115z00_3713 =
															(BgL_classz00_3703 == BgL_oclassz00_3709);
														if (BgL__ortest_1115z00_3713)
															{	/* Coerce/coerce.scm 516 */
																BgL_res2095z00_3708 = BgL__ortest_1115z00_3713;
															}
														else
															{	/* Coerce/coerce.scm 516 */
																long BgL_odepthz00_3714;

																{	/* Coerce/coerce.scm 516 */
																	obj_t BgL_arg1804z00_3715;

																	BgL_arg1804z00_3715 = (BgL_oclassz00_3709);
																	BgL_odepthz00_3714 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3715);
																}
																if ((2L < BgL_odepthz00_3714))
																	{	/* Coerce/coerce.scm 516 */
																		obj_t BgL_arg1802z00_3716;

																		{	/* Coerce/coerce.scm 516 */
																			obj_t BgL_arg1803z00_3717;

																			BgL_arg1803z00_3717 =
																				(BgL_oclassz00_3709);
																			BgL_arg1802z00_3716 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3717, 2L);
																		}
																		BgL_res2095z00_3708 =
																			(BgL_arg1802z00_3716 ==
																			BgL_classz00_3703);
																	}
																else
																	{	/* Coerce/coerce.scm 516 */
																		BgL_res2095z00_3708 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2209z00_4451 = BgL_res2095z00_3708;
											}
									}
								else
									{	/* Coerce/coerce.scm 516 */
										BgL_test2209z00_4451 = ((bool_t) 0);
									}
							}
							if (BgL_test2209z00_4451)
								{	/* Coerce/coerce.scm 516 */
									return
										((BgL_nodez00_bglt)
										BGl_castzd2nodezd2zzcoerce_coercez00(BgL_cnodez00_3701,
											BgL_toz00_3553));
								}
							else
								{	/* Coerce/coerce.scm 516 */
									return BgL_cnodez00_3701;
								}
						}
					}
				}
			}
		}

	}



/* &coerce!-make-box1342 */
	BgL_nodez00_bglt BGl_z62coercez12zd2makezd2box1342z70zzcoerce_coercez00(obj_t
		BgL_envz00_3555, obj_t BgL_nodez00_3556, obj_t BgL_callerz00_3557,
		obj_t BgL_toz00_3558, obj_t BgL_safez00_3559)
	{
		{	/* Coerce/coerce.scm 501 */
			{	/* Coerce/coerce.scm 503 */
				BgL_nodez00_bglt BgL_nodevz00_3719;

				BgL_nodevz00_3719 =
					BGl_coercez12z12zzcoerce_coercez00(
					(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_3556)))->BgL_valuez00),
					BgL_callerz00_3557,
					(((BgL_makezd2boxzd2_bglt) COBJECT(((BgL_makezd2boxzd2_bglt)
									BgL_nodez00_3556)))->BgL_vtypez00), CBOOL(BgL_safez00_3559));
				{	/* Coerce/coerce.scm 504 */
					bool_t BgL_test2214z00_4482;

					{	/* Coerce/coerce.scm 504 */
						BgL_typez00_bglt BgL_arg1992z00_3720;

						BgL_arg1992z00_3720 =
							BGl_getzd2typezd2zztype_typeofz00(BgL_nodevz00_3719,
							((bool_t) 0));
						{	/* Coerce/coerce.scm 504 */
							obj_t BgL_classz00_3721;

							BgL_classz00_3721 = BGl_tclassz00zzobject_classz00;
							{	/* Coerce/coerce.scm 504 */
								BgL_objectz00_bglt BgL_arg1807z00_3722;

								{	/* Coerce/coerce.scm 504 */
									obj_t BgL_tmpz00_4484;

									BgL_tmpz00_4484 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1992z00_3720));
									BgL_arg1807z00_3722 = (BgL_objectz00_bglt) (BgL_tmpz00_4484);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Coerce/coerce.scm 504 */
										long BgL_idxz00_3723;

										BgL_idxz00_3723 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3722);
										BgL_test2214z00_4482 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3723 + 2L)) == BgL_classz00_3721);
									}
								else
									{	/* Coerce/coerce.scm 504 */
										bool_t BgL_res2094z00_3726;

										{	/* Coerce/coerce.scm 504 */
											obj_t BgL_oclassz00_3727;

											{	/* Coerce/coerce.scm 504 */
												obj_t BgL_arg1815z00_3728;
												long BgL_arg1816z00_3729;

												BgL_arg1815z00_3728 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Coerce/coerce.scm 504 */
													long BgL_arg1817z00_3730;

													BgL_arg1817z00_3730 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3722);
													BgL_arg1816z00_3729 =
														(BgL_arg1817z00_3730 - OBJECT_TYPE);
												}
												BgL_oclassz00_3727 =
													VECTOR_REF(BgL_arg1815z00_3728, BgL_arg1816z00_3729);
											}
											{	/* Coerce/coerce.scm 504 */
												bool_t BgL__ortest_1115z00_3731;

												BgL__ortest_1115z00_3731 =
													(BgL_classz00_3721 == BgL_oclassz00_3727);
												if (BgL__ortest_1115z00_3731)
													{	/* Coerce/coerce.scm 504 */
														BgL_res2094z00_3726 = BgL__ortest_1115z00_3731;
													}
												else
													{	/* Coerce/coerce.scm 504 */
														long BgL_odepthz00_3732;

														{	/* Coerce/coerce.scm 504 */
															obj_t BgL_arg1804z00_3733;

															BgL_arg1804z00_3733 = (BgL_oclassz00_3727);
															BgL_odepthz00_3732 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3733);
														}
														if ((2L < BgL_odepthz00_3732))
															{	/* Coerce/coerce.scm 504 */
																obj_t BgL_arg1802z00_3734;

																{	/* Coerce/coerce.scm 504 */
																	obj_t BgL_arg1803z00_3735;

																	BgL_arg1803z00_3735 = (BgL_oclassz00_3727);
																	BgL_arg1802z00_3734 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3735,
																		2L);
																}
																BgL_res2094z00_3726 =
																	(BgL_arg1802z00_3734 == BgL_classz00_3721);
															}
														else
															{	/* Coerce/coerce.scm 504 */
																BgL_res2094z00_3726 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2214z00_4482 = BgL_res2094z00_3726;
									}
							}
						}
					}
					if (BgL_test2214z00_4482)
						{	/* Coerce/coerce.scm 504 */
							((((BgL_makezd2boxzd2_bglt) COBJECT(
											((BgL_makezd2boxzd2_bglt) BgL_nodez00_3556)))->
									BgL_valuez00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
										BGl_castzd2nodezd2zzcoerce_coercez00(BgL_nodevz00_3719,
											BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
						}
					else
						{	/* Coerce/coerce.scm 504 */
							((((BgL_makezd2boxzd2_bglt) COBJECT(
											((BgL_makezd2boxzd2_bglt) BgL_nodez00_3556)))->
									BgL_valuez00) =
								((BgL_nodez00_bglt) BgL_nodevz00_3719), BUNSPEC);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3556));
		}

	}



/* &coerce!-jump-ex-it1340 */
	BgL_nodez00_bglt
		BGl_z62coercez12zd2jumpzd2exzd2it1340za2zzcoerce_coercez00(obj_t
		BgL_envz00_3560, obj_t BgL_nodez00_3561, obj_t BgL_callerz00_3562,
		obj_t BgL_toz00_3563, obj_t BgL_safez00_3564)
	{
		{	/* Coerce/coerce.scm 482 */
			{
				BgL_nodez00_bglt BgL_auxz00_4515;

				{	/* Coerce/coerce.scm 484 */
					BgL_nodez00_bglt BgL_arg1985z00_3737;

					BgL_arg1985z00_3737 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3561)))->BgL_exitz00);
					BgL_auxz00_4515 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1985z00_3737,
						BgL_callerz00_3562,
						((BgL_typez00_bglt) BGl_za2exitza2z00zztype_cachez00),
						CBOOL(BgL_safez00_3564));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3561)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4515), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4523;

				{	/* Coerce/coerce.scm 485 */
					BgL_nodez00_bglt BgL_arg1986z00_3738;
					BgL_typez00_bglt BgL_arg1987z00_3739;

					BgL_arg1986z00_3738 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3561)))->
						BgL_valuez00);
					{	/* Coerce/coerce.scm 485 */
						BgL_nodez00_bglt BgL_arg1988z00_3740;

						BgL_arg1988z00_3740 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3561)))->
							BgL_valuez00);
						BgL_arg1987z00_3739 =
							BGl_getzd2typezd2zztype_typeofz00(BgL_arg1988z00_3740,
							((bool_t) 0));
					}
					BgL_auxz00_4523 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1986z00_3738,
						BgL_callerz00_3562, BgL_arg1987z00_3739, CBOOL(BgL_safez00_3564));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3561)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_4523), BUNSPEC);
			}
			{	/* Coerce/coerce.scm 486 */
				BgL_typez00_bglt BgL_arg1989z00_3741;

				BgL_arg1989z00_3741 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3561))))->
					BgL_typez00);
				return
					BGl_convertz12z12zzcoerce_convertz00(((BgL_nodez00_bglt) (
							(BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3561)),
					BgL_arg1989z00_3741, ((BgL_typez00_bglt) BgL_toz00_3563),
					CBOOL(BgL_safez00_3564));
			}
		}

	}



/* &coerce!-set-ex-it1337 */
	BgL_nodez00_bglt
		BGl_z62coercez12zd2setzd2exzd2it1337za2zzcoerce_coercez00(obj_t
		BgL_envz00_3565, obj_t BgL_nodez00_3566, obj_t BgL_callerz00_3567,
		obj_t BgL_toz00_3568, obj_t BgL_safez00_3569)
	{
		{	/* Coerce/coerce.scm 471 */
			{
				BgL_varz00_bglt BgL_auxz00_4541;

				{	/* Coerce/coerce.scm 473 */
					BgL_varz00_bglt BgL_arg1980z00_3743;

					BgL_arg1980z00_3743 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3566)))->BgL_varz00);
					BgL_auxz00_4541 =
						((BgL_varz00_bglt)
						BGl_coercez12z12zzcoerce_coercez00(
							((BgL_nodez00_bglt) BgL_arg1980z00_3743), BgL_callerz00_3567,
							((BgL_typez00_bglt) BGl_za2exitza2z00zztype_cachez00),
							CBOOL(BgL_safez00_3569)));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3566)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_4541), BUNSPEC);
			}
			BGl_pvariablezd2protozd2zzcoerce_pprotoz00(3L,
				(((BgL_varz00_bglt) COBJECT(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3566)))->
								BgL_varz00)))->BgL_variablez00));
			{
				BgL_nodez00_bglt BgL_auxz00_4555;

				{	/* Coerce/coerce.scm 475 */
					BgL_nodez00_bglt BgL_arg1983z00_3744;

					BgL_arg1983z00_3744 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3566)))->BgL_bodyz00);
					BgL_auxz00_4555 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1983z00_3744,
						BgL_callerz00_3567, ((BgL_typez00_bglt) BgL_toz00_3568),
						CBOOL(BgL_safez00_3569));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3566)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4555), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4563;

				{	/* Coerce/coerce.scm 476 */
					BgL_nodez00_bglt BgL_arg1984z00_3745;

					BgL_arg1984z00_3745 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3566)))->
						BgL_onexitz00);
					BgL_auxz00_4563 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1984z00_3745,
						BgL_callerz00_3567, ((BgL_typez00_bglt) BgL_toz00_3568),
						CBOOL(BgL_safez00_3569));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3566)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4563), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3566));
		}

	}



/* &coerce!-let-var1335 */
	BgL_nodez00_bglt BGl_z62coercez12zd2letzd2var1335z70zzcoerce_coercez00(obj_t
		BgL_envz00_3570, obj_t BgL_nodez00_3571, obj_t BgL_callerz00_3572,
		obj_t BgL_toz00_3573, obj_t BgL_safez00_3574)
	{
		{	/* Coerce/coerce.scm 451 */
			BGl_inczd2ppmargez12zc0zzcoerce_pprotoz00();
			{	/* Tools/trace.sch 53 */
				obj_t BgL_g1289z00_3747;

				BgL_g1289z00_3747 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_3571)))->BgL_bindingsz00);
				{
					obj_t BgL_l1287z00_3749;

					BgL_l1287z00_3749 = BgL_g1289z00_3747;
				BgL_zc3z04anonymousza31968ze3z87_3748:
					if (PAIRP(BgL_l1287z00_3749))
						{	/* Tools/trace.sch 53 */
							{	/* Tools/trace.sch 53 */
								obj_t BgL_bindingz00_3750;

								BgL_bindingz00_3750 = CAR(BgL_l1287z00_3749);
								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1970z00_3751;

									BgL_arg1970z00_3751 = CAR(((obj_t) BgL_bindingz00_3750));
									BGl_pvariablezd2protozd2zzcoerce_pprotoz00(3L,
										((BgL_variablez00_bglt) BgL_arg1970z00_3751));
								}
								{	/* Tools/trace.sch 53 */
									BgL_nodez00_bglt BgL_arg1971z00_3752;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1972z00_3753;
										BgL_typez00_bglt BgL_arg1973z00_3754;

										BgL_arg1972z00_3753 = CDR(((obj_t) BgL_bindingz00_3750));
										BgL_arg1973z00_3754 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt)
															CAR(
																((obj_t) BgL_bindingz00_3750))))))->
											BgL_typez00);
										BgL_arg1971z00_3752 =
											BGl_coercez12z12zzcoerce_coercez00(((BgL_nodez00_bglt)
												BgL_arg1972z00_3753), BgL_callerz00_3572,
											BgL_arg1973z00_3754, CBOOL(BgL_safez00_3574));
									}
									{	/* Tools/trace.sch 53 */
										obj_t BgL_auxz00_4595;
										obj_t BgL_tmpz00_4593;

										BgL_auxz00_4595 = ((obj_t) BgL_arg1971z00_3752);
										BgL_tmpz00_4593 = ((obj_t) BgL_bindingz00_3750);
										SET_CDR(BgL_tmpz00_4593, BgL_auxz00_4595);
									}
								}
							}
							{
								obj_t BgL_l1287z00_4598;

								BgL_l1287z00_4598 = CDR(BgL_l1287z00_3749);
								BgL_l1287z00_3749 = BgL_l1287z00_4598;
								goto BgL_zc3z04anonymousza31968ze3z87_3748;
							}
						}
					else
						{	/* Tools/trace.sch 53 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4600;

				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg1976z00_3755;

					BgL_arg1976z00_3755 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3571)))->BgL_bodyz00);
					BgL_auxz00_4600 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1976z00_3755,
						BgL_callerz00_3572, ((BgL_typez00_bglt) BgL_toz00_3573),
						CBOOL(BgL_safez00_3574));
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3571)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4600), BUNSPEC);
			}
			{
				BgL_typez00_bglt BgL_auxz00_4608;

				{	/* Tools/trace.sch 53 */
					BgL_typez00_bglt BgL_arg1977z00_3756;
					BgL_typez00_bglt BgL_arg1978z00_3757;

					BgL_arg1977z00_3756 =
						(((BgL_nodez00_bglt) COBJECT(
								(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_3571)))->
									BgL_bodyz00)))->BgL_typez00);
					BgL_arg1978z00_3757 =
						(((BgL_nodez00_bglt)
							COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
										BgL_nodez00_3571))))->BgL_typez00);
					BgL_auxz00_4608 =
						BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1977z00_3756,
						BgL_arg1978z00_3757);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_3571))))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_auxz00_4608), BUNSPEC);
			}
			BGl_deczd2ppmargez12zc0zzcoerce_pprotoz00();
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_3571));
		}

	}



/* &coerce!-let-fun1333 */
	BgL_nodez00_bglt BGl_z62coercez12zd2letzd2fun1333z70zzcoerce_coercez00(obj_t
		BgL_envz00_3575, obj_t BgL_nodez00_3576, obj_t BgL_callerz00_3577,
		obj_t BgL_toz00_3578, obj_t BgL_safez00_3579)
	{
		{	/* Coerce/coerce.scm 439 */
			BGl_inczd2ppmargez12zc0zzcoerce_pprotoz00();
			{	/* Coerce/coerce.scm 442 */
				obj_t BgL_g1286z00_3759;

				BgL_g1286z00_3759 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_3576)))->BgL_localsz00);
				{
					obj_t BgL_l1284z00_3761;

					BgL_l1284z00_3761 = BgL_g1286z00_3759;
				BgL_zc3z04anonymousza31960ze3z87_3760:
					if (PAIRP(BgL_l1284z00_3761))
						{	/* Coerce/coerce.scm 442 */
							{	/* Coerce/coerce.scm 442 */
								obj_t BgL_fz00_3762;

								BgL_fz00_3762 = CAR(BgL_l1284z00_3761);
								{	/* Coerce/coerce.scm 442 */
									bool_t BgL_arg1962z00_3763;

									if (CBOOL(BGl_za2unsafezd2typeza2zd2zzengine_paramz00))
										{	/* Coerce/coerce.scm 442 */
											BgL_arg1962z00_3763 = ((bool_t) 0);
										}
									else
										{	/* Coerce/coerce.scm 442 */
											BgL_arg1962z00_3763 = ((bool_t) 1);
										}
									BGl_coercezd2functionz12zc0zzcoerce_coercez00(
										((BgL_variablez00_bglt) BgL_fz00_3762),
										BgL_arg1962z00_3763);
								}
							}
							{
								obj_t BgL_l1284z00_4632;

								BgL_l1284z00_4632 = CDR(BgL_l1284z00_3761);
								BgL_l1284z00_3761 = BgL_l1284z00_4632;
								goto BgL_zc3z04anonymousza31960ze3z87_3760;
							}
						}
					else
						{	/* Coerce/coerce.scm 442 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4634;

				{	/* Coerce/coerce.scm 443 */
					BgL_nodez00_bglt BgL_arg1964z00_3764;

					BgL_arg1964z00_3764 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3576)))->BgL_bodyz00);
					BgL_auxz00_4634 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1964z00_3764,
						BgL_callerz00_3577, ((BgL_typez00_bglt) BgL_toz00_3578),
						CBOOL(BgL_safez00_3579));
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3576)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4634), BUNSPEC);
			}
			{
				BgL_typez00_bglt BgL_auxz00_4642;

				{	/* Coerce/coerce.scm 444 */
					BgL_typez00_bglt BgL_arg1965z00_3765;
					BgL_typez00_bglt BgL_arg1966z00_3766;

					BgL_arg1965z00_3765 =
						(((BgL_nodez00_bglt) COBJECT(
								(((BgL_letzd2funzd2_bglt) COBJECT(
											((BgL_letzd2funzd2_bglt) BgL_nodez00_3576)))->
									BgL_bodyz00)))->BgL_typez00);
					BgL_arg1966z00_3766 =
						(((BgL_nodez00_bglt)
							COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
										BgL_nodez00_3576))))->BgL_typez00);
					BgL_auxz00_4642 =
						BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1965z00_3765,
						BgL_arg1966z00_3766);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_nodez00_3576))))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_auxz00_4642), BUNSPEC);
			}
			BGl_deczd2ppmargez12zc0zzcoerce_pprotoz00();
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_3576));
		}

	}



/* &coerce!-switch1331 */
	BgL_nodez00_bglt BGl_z62coercez12zd2switch1331za2zzcoerce_coercez00(obj_t
		BgL_envz00_3580, obj_t BgL_nodez00_3581, obj_t BgL_callerz00_3582,
		obj_t BgL_toz00_3583, obj_t BgL_safez00_3584)
	{
		{	/* Coerce/coerce.scm 409 */
			((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_switchz00_bglt) BgL_nodez00_3581))))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_toz00_3583)), BUNSPEC);
			{	/* Coerce/coerce.scm 412 */
				obj_t BgL_clausesz00_3768;
				BgL_typez00_bglt BgL_testzd2typezd2_3769;
				BgL_typez00_bglt BgL_testzd2nodezd2typez00_3770;

				BgL_clausesz00_3768 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_3581)))->BgL_clausesz00);
				BgL_testzd2typezd2_3769 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_3581)))->BgL_itemzd2typezd2);
				BgL_testzd2nodezd2typez00_3770 =
					BGl_getzd2typezd2zztype_typeofz00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_3581)))->BgL_testz00),
					((bool_t) 0));
				if (BGl_coercerzd2existszf3z21zztype_coercionz00
					(BgL_testzd2nodezd2typez00_3770, BgL_testzd2typezd2_3769))
					{	/* Coerce/coerce.scm 415 */
						BFALSE;
					}
				else
					{	/* Coerce/coerce.scm 416 */
						obj_t BgL_arg1936z00_3771;
						obj_t BgL_arg1937z00_3772;
						obj_t BgL_arg1938z00_3773;

						{	/* Coerce/coerce.scm 416 */
							obj_t BgL_tmpz00_4669;

							BgL_tmpz00_4669 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1936z00_3771 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4669);
						}
						BgL_arg1937z00_3772 =
							BGl_shapez00zztools_shapez00(((obj_t) BgL_testzd2typezd2_3769));
						BgL_arg1938z00_3773 =
							BGl_shapez00zztools_shapez00(
							((obj_t) BgL_testzd2nodezd2typez00_3770));
						{	/* Coerce/coerce.scm 416 */
							obj_t BgL_list1939z00_3774;

							{	/* Coerce/coerce.scm 416 */
								obj_t BgL_arg1940z00_3775;

								{	/* Coerce/coerce.scm 416 */
									obj_t BgL_arg1941z00_3776;

									{	/* Coerce/coerce.scm 416 */
										obj_t BgL_arg1942z00_3777;

										{	/* Coerce/coerce.scm 416 */
											obj_t BgL_arg1943z00_3778;

											{	/* Coerce/coerce.scm 416 */
												obj_t BgL_arg1944z00_3779;

												{	/* Coerce/coerce.scm 416 */
													obj_t BgL_arg1945z00_3780;

													{	/* Coerce/coerce.scm 416 */
														obj_t BgL_arg1946z00_3781;

														BgL_arg1946z00_3781 =
															MAKE_YOUNG_PAIR(BgL_arg1938z00_3773, BNIL);
														BgL_arg1945z00_3780 =
															MAKE_YOUNG_PAIR
															(BGl_string2134z00zzcoerce_coercez00,
															BgL_arg1946z00_3781);
													}
													BgL_arg1944z00_3779 =
														MAKE_YOUNG_PAIR(BgL_arg1937z00_3772,
														BgL_arg1945z00_3780);
												}
												BgL_arg1943z00_3778 =
													MAKE_YOUNG_PAIR(BGl_string2135z00zzcoerce_coercez00,
													BgL_arg1944z00_3779);
											}
											BgL_arg1942z00_3777 =
												MAKE_YOUNG_PAIR(BGl_string2136z00zzcoerce_coercez00,
												BgL_arg1943z00_3778);
										}
										BgL_arg1941z00_3776 =
											MAKE_YOUNG_PAIR(BINT(416L), BgL_arg1942z00_3777);
									}
									BgL_arg1940z00_3775 =
										MAKE_YOUNG_PAIR(BGl_string2137z00zzcoerce_coercez00,
										BgL_arg1941z00_3776);
								}
								BgL_list1939z00_3774 =
									MAKE_YOUNG_PAIR(BGl_string2138z00zzcoerce_coercez00,
									BgL_arg1940z00_3775);
							}
							BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1936z00_3771,
								BgL_list1939z00_3774);
						}
					}
				if (BGl_coercerzd2existszf3z21zztype_coercionz00
					(BgL_testzd2nodezd2typez00_3770, BgL_testzd2typezd2_3769))
					{	/* Coerce/coerce.scm 425 */
						((((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_3581)))->BgL_testz00) =
							((BgL_nodez00_bglt)
								BGl_coercez12z12zzcoerce_coercez00((((BgL_switchz00_bglt)
											COBJECT(((BgL_switchz00_bglt) BgL_nodez00_3581)))->
										BgL_testz00), BgL_callerz00_3582, BgL_testzd2typezd2_3769,
									CBOOL(BgL_safez00_3584))), BUNSPEC);
						{
							obj_t BgL_l1282z00_3783;

							BgL_l1282z00_3783 = BgL_clausesz00_3768;
						BgL_zc3z04anonymousza31950ze3z87_3782:
							if (PAIRP(BgL_l1282z00_3783))
								{	/* Coerce/coerce.scm 428 */
									{	/* Coerce/coerce.scm 429 */
										obj_t BgL_clausez00_3784;

										BgL_clausez00_3784 = CAR(BgL_l1282z00_3783);
										{	/* Coerce/coerce.scm 430 */
											BgL_nodez00_bglt BgL_arg1952z00_3785;

											{	/* Coerce/coerce.scm 430 */
												obj_t BgL_arg1953z00_3786;

												BgL_arg1953z00_3786 = CDR(((obj_t) BgL_clausez00_3784));
												BgL_arg1952z00_3785 =
													BGl_coercez12z12zzcoerce_coercez00(
													((BgL_nodez00_bglt) BgL_arg1953z00_3786),
													BgL_callerz00_3582,
													((BgL_typez00_bglt) BgL_toz00_3583),
													CBOOL(BgL_safez00_3584));
											}
											{	/* Coerce/coerce.scm 429 */
												obj_t BgL_auxz00_4705;
												obj_t BgL_tmpz00_4703;

												BgL_auxz00_4705 = ((obj_t) BgL_arg1952z00_3785);
												BgL_tmpz00_4703 = ((obj_t) BgL_clausez00_3784);
												SET_CDR(BgL_tmpz00_4703, BgL_auxz00_4705);
											}
										}
									}
									{
										obj_t BgL_l1282z00_4708;

										BgL_l1282z00_4708 = CDR(BgL_l1282z00_3783);
										BgL_l1282z00_3783 = BgL_l1282z00_4708;
										goto BgL_zc3z04anonymousza31950ze3z87_3782;
									}
								}
							else
								{	/* Coerce/coerce.scm 428 */
									((bool_t) 1);
								}
						}
						return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_3581));
					}
				else
					{	/* Coerce/coerce.scm 433 */
						obj_t BgL_arg1955z00_3787;

						{	/* Coerce/coerce.scm 433 */
							obj_t BgL_arg1956z00_3788;
							obj_t BgL_arg1957z00_3789;
							BgL_nodez00_bglt BgL_arg1958z00_3790;

							BgL_arg1956z00_3788 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_switchz00_bglt) BgL_nodez00_3581))))->BgL_locz00);
							BgL_arg1957z00_3789 =
								(((BgL_typez00_bglt) COBJECT(BgL_testzd2typezd2_3769))->
								BgL_idz00);
							BgL_arg1958z00_3790 =
								(((BgL_switchz00_bglt) COBJECT(((BgL_switchz00_bglt)
											BgL_nodez00_3581)))->BgL_testz00);
							BgL_arg1955z00_3787 =
								BGl_runtimezd2typezd2errorz00zzcoerce_convertz00
								(BgL_arg1956z00_3788, BgL_arg1957z00_3789, BgL_arg1958z00_3790);
						}
						return
							BGl_coercez12z12zzcoerce_coercez00(
							((BgL_nodez00_bglt) BgL_arg1955z00_3787), BgL_callerz00_3582,
							((BgL_typez00_bglt) BgL_toz00_3583), CBOOL(BgL_safez00_3584));
					}
			}
		}

	}



/* &coerce!-fail1329 */
	BgL_nodez00_bglt BGl_z62coercez12zd2fail1329za2zzcoerce_coercez00(obj_t
		BgL_envz00_3585, obj_t BgL_nodez00_3586, obj_t BgL_callerz00_3587,
		obj_t BgL_toz00_3588, obj_t BgL_safez00_3589)
	{
		{	/* Coerce/coerce.scm 399 */
			{
				BgL_nodez00_bglt BgL_auxz00_4723;

				{	/* Coerce/coerce.scm 401 */
					BgL_nodez00_bglt BgL_arg1932z00_3792;

					BgL_arg1932z00_3792 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3586)))->BgL_procz00);
					BgL_auxz00_4723 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1932z00_3792,
						BgL_callerz00_3587,
						((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
						CBOOL(BgL_safez00_3589));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3586)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4723), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4731;

				{	/* Coerce/coerce.scm 402 */
					BgL_nodez00_bglt BgL_arg1933z00_3793;

					BgL_arg1933z00_3793 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3586)))->BgL_msgz00);
					BgL_auxz00_4731 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1933z00_3793,
						BgL_callerz00_3587,
						((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
						CBOOL(BgL_safez00_3589));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3586)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4731), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4739;

				{	/* Coerce/coerce.scm 403 */
					BgL_nodez00_bglt BgL_arg1934z00_3794;

					BgL_arg1934z00_3794 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3586)))->BgL_objz00);
					BgL_auxz00_4739 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1934z00_3794,
						BgL_callerz00_3587,
						((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
						CBOOL(BgL_safez00_3589));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3586)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4739), BUNSPEC);
			}
			return
				BGl_convertz12z12zzcoerce_convertz00(
				((BgL_nodez00_bglt)
					((BgL_failz00_bglt) BgL_nodez00_3586)),
				((BgL_typez00_bglt) BGl_za2magicza2z00zztype_cachez00),
				((BgL_typez00_bglt) BgL_toz00_3588), CBOOL(BgL_safez00_3589));
		}

	}



/* &coerce!-conditional1327 */
	BgL_nodez00_bglt BGl_z62coercez12zd2conditional1327za2zzcoerce_coercez00(obj_t
		BgL_envz00_3590, obj_t BgL_nodez00_3591, obj_t BgL_callerz00_3592,
		obj_t BgL_toz00_3593, obj_t BgL_safez00_3594)
	{
		{	/* Coerce/coerce.scm 288 */
			{
				obj_t BgL_nodez00_4002;
				obj_t BgL_nodez00_3906;
				obj_t BgL_nodez00_3832;
				obj_t BgL_typecz00_3833;
				obj_t BgL_nodez00_3800;

				{
					BgL_nodez00_bglt BgL_auxz00_4753;

					{	/* Coerce/coerce.scm 384 */
						BgL_nodez00_bglt BgL_arg1751z00_4025;

						BgL_arg1751z00_4025 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->BgL_testz00);
						BgL_auxz00_4753 =
							BGl_coercez12z12zzcoerce_coercez00(BgL_arg1751z00_4025,
							BgL_callerz00_3592,
							((BgL_typez00_bglt) BGl_za2boolza2z00zztype_cachez00),
							CBOOL(BgL_safez00_3594));
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->BgL_testz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4753), BUNSPEC);
				}
				{	/* Coerce/coerce.scm 385 */
					obj_t BgL_casezd2valuezd2_4026;

					{	/* Coerce/coerce.scm 385 */
						BgL_nodez00_bglt BgL_arg1765z00_4027;

						BgL_arg1765z00_4027 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->BgL_testz00);
						BgL_nodez00_3800 = ((obj_t) BgL_arg1765z00_4027);
					BgL_testzd2staticzd2valuez00_3796:
						{	/* Coerce/coerce.scm 373 */
							obj_t BgL_g1140z00_3801;

							BgL_g1140z00_3801 =
								BGl_isazd2ofzd2zztype_miscz00(
								((BgL_nodez00_bglt) BgL_nodez00_3800));
							if (CBOOL(BgL_g1140z00_3801))
								{	/* Coerce/coerce.scm 373 */
									BgL_nodez00_3832 = BgL_nodez00_3800;
									BgL_typecz00_3833 = BgL_g1140z00_3801;
									{	/* Coerce/coerce.scm 355 */
										obj_t BgL_typevz00_3834;

										{	/* Coerce/coerce.scm 356 */
											bool_t BgL_test2225z00_4767;

											{	/* Coerce/coerce.scm 356 */
												obj_t BgL_arg1927z00_3835;

												{	/* Coerce/coerce.scm 356 */
													obj_t BgL_pairz00_3836;

													BgL_pairz00_3836 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_3832)))->
														BgL_argsz00);
													BgL_arg1927z00_3835 = CAR(BgL_pairz00_3836);
												}
												{	/* Coerce/coerce.scm 356 */
													obj_t BgL_classz00_3837;

													BgL_classz00_3837 = BGl_varz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_arg1927z00_3835))
														{	/* Coerce/coerce.scm 356 */
															BgL_objectz00_bglt BgL_arg1807z00_3838;

															BgL_arg1807z00_3838 =
																(BgL_objectz00_bglt) (BgL_arg1927z00_3835);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Coerce/coerce.scm 356 */
																	long BgL_idxz00_3839;

																	BgL_idxz00_3839 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3838);
																	BgL_test2225z00_4767 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3839 + 2L)) ==
																		BgL_classz00_3837);
																}
															else
																{	/* Coerce/coerce.scm 356 */
																	bool_t BgL_res2088z00_3842;

																	{	/* Coerce/coerce.scm 356 */
																		obj_t BgL_oclassz00_3843;

																		{	/* Coerce/coerce.scm 356 */
																			obj_t BgL_arg1815z00_3844;
																			long BgL_arg1816z00_3845;

																			BgL_arg1815z00_3844 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Coerce/coerce.scm 356 */
																				long BgL_arg1817z00_3846;

																				BgL_arg1817z00_3846 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3838);
																				BgL_arg1816z00_3845 =
																					(BgL_arg1817z00_3846 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3843 =
																				VECTOR_REF(BgL_arg1815z00_3844,
																				BgL_arg1816z00_3845);
																		}
																		{	/* Coerce/coerce.scm 356 */
																			bool_t BgL__ortest_1115z00_3847;

																			BgL__ortest_1115z00_3847 =
																				(BgL_classz00_3837 ==
																				BgL_oclassz00_3843);
																			if (BgL__ortest_1115z00_3847)
																				{	/* Coerce/coerce.scm 356 */
																					BgL_res2088z00_3842 =
																						BgL__ortest_1115z00_3847;
																				}
																			else
																				{	/* Coerce/coerce.scm 356 */
																					long BgL_odepthz00_3848;

																					{	/* Coerce/coerce.scm 356 */
																						obj_t BgL_arg1804z00_3849;

																						BgL_arg1804z00_3849 =
																							(BgL_oclassz00_3843);
																						BgL_odepthz00_3848 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3849);
																					}
																					if ((2L < BgL_odepthz00_3848))
																						{	/* Coerce/coerce.scm 356 */
																							obj_t BgL_arg1802z00_3850;

																							{	/* Coerce/coerce.scm 356 */
																								obj_t BgL_arg1803z00_3851;

																								BgL_arg1803z00_3851 =
																									(BgL_oclassz00_3843);
																								BgL_arg1802z00_3850 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3851, 2L);
																							}
																							BgL_res2088z00_3842 =
																								(BgL_arg1802z00_3850 ==
																								BgL_classz00_3837);
																						}
																					else
																						{	/* Coerce/coerce.scm 356 */
																							BgL_res2088z00_3842 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2225z00_4767 = BgL_res2088z00_3842;
																}
														}
													else
														{	/* Coerce/coerce.scm 356 */
															BgL_test2225z00_4767 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2225z00_4767)
												{	/* Coerce/coerce.scm 357 */
													obj_t BgL_arg1904z00_3852;

													{	/* Coerce/coerce.scm 357 */
														obj_t BgL_pairz00_3853;

														BgL_pairz00_3853 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_3832)))->
															BgL_argsz00);
														BgL_arg1904z00_3852 = CAR(BgL_pairz00_3853);
													}
													BgL_typevz00_3834 =
														((obj_t)
														BGl_getzd2typezd2zztype_typeofz00(
															((BgL_nodez00_bglt) BgL_arg1904z00_3852),
															((bool_t) 0)));
												}
											else
												{	/* Coerce/coerce.scm 358 */
													bool_t BgL_test2230z00_4799;

													{	/* Coerce/coerce.scm 358 */
														obj_t BgL_arg1925z00_3854;

														{	/* Coerce/coerce.scm 358 */
															obj_t BgL_pairz00_3855;

															BgL_pairz00_3855 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_nodez00_3832)))->
																BgL_argsz00);
															BgL_arg1925z00_3854 = CAR(BgL_pairz00_3855);
														}
														{	/* Coerce/coerce.scm 358 */
															obj_t BgL_classz00_3856;

															BgL_classz00_3856 = BGl_castz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_arg1925z00_3854))
																{	/* Coerce/coerce.scm 358 */
																	BgL_objectz00_bglt BgL_arg1807z00_3857;

																	BgL_arg1807z00_3857 =
																		(BgL_objectz00_bglt) (BgL_arg1925z00_3854);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Coerce/coerce.scm 358 */
																			long BgL_idxz00_3858;

																			BgL_idxz00_3858 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3857);
																			BgL_test2230z00_4799 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3858 + 2L)) ==
																				BgL_classz00_3856);
																		}
																	else
																		{	/* Coerce/coerce.scm 358 */
																			bool_t BgL_res2089z00_3861;

																			{	/* Coerce/coerce.scm 358 */
																				obj_t BgL_oclassz00_3862;

																				{	/* Coerce/coerce.scm 358 */
																					obj_t BgL_arg1815z00_3863;
																					long BgL_arg1816z00_3864;

																					BgL_arg1815z00_3863 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Coerce/coerce.scm 358 */
																						long BgL_arg1817z00_3865;

																						BgL_arg1817z00_3865 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3857);
																						BgL_arg1816z00_3864 =
																							(BgL_arg1817z00_3865 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3862 =
																						VECTOR_REF(BgL_arg1815z00_3863,
																						BgL_arg1816z00_3864);
																				}
																				{	/* Coerce/coerce.scm 358 */
																					bool_t BgL__ortest_1115z00_3866;

																					BgL__ortest_1115z00_3866 =
																						(BgL_classz00_3856 ==
																						BgL_oclassz00_3862);
																					if (BgL__ortest_1115z00_3866)
																						{	/* Coerce/coerce.scm 358 */
																							BgL_res2089z00_3861 =
																								BgL__ortest_1115z00_3866;
																						}
																					else
																						{	/* Coerce/coerce.scm 358 */
																							long BgL_odepthz00_3867;

																							{	/* Coerce/coerce.scm 358 */
																								obj_t BgL_arg1804z00_3868;

																								BgL_arg1804z00_3868 =
																									(BgL_oclassz00_3862);
																								BgL_odepthz00_3867 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3868);
																							}
																							if ((2L < BgL_odepthz00_3867))
																								{	/* Coerce/coerce.scm 358 */
																									obj_t BgL_arg1802z00_3869;

																									{	/* Coerce/coerce.scm 358 */
																										obj_t BgL_arg1803z00_3870;

																										BgL_arg1803z00_3870 =
																											(BgL_oclassz00_3862);
																										BgL_arg1802z00_3869 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3870, 2L);
																									}
																									BgL_res2089z00_3861 =
																										(BgL_arg1802z00_3869 ==
																										BgL_classz00_3856);
																								}
																							else
																								{	/* Coerce/coerce.scm 358 */
																									BgL_res2089z00_3861 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2230z00_4799 =
																				BgL_res2089z00_3861;
																		}
																}
															else
																{	/* Coerce/coerce.scm 358 */
																	BgL_test2230z00_4799 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2230z00_4799)
														{	/* Coerce/coerce.scm 359 */
															BgL_castz00_bglt BgL_i1139z00_3871;

															{	/* Coerce/coerce.scm 359 */
																obj_t BgL_pairz00_3872;

																BgL_pairz00_3872 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_3832)))->
																	BgL_argsz00);
																BgL_i1139z00_3871 =
																	((BgL_castz00_bglt) CAR(BgL_pairz00_3872));
															}
															{	/* Coerce/coerce.scm 360 */
																bool_t BgL_test2235z00_4829;

																{	/* Coerce/coerce.scm 360 */
																	bool_t BgL_test2236z00_4830;

																	{	/* Coerce/coerce.scm 360 */
																		BgL_typez00_bglt BgL_arg1923z00_3873;

																		BgL_arg1923z00_3873 =
																			(((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_i1139z00_3871)))->BgL_typez00);
																		BgL_test2236z00_4830 =
																			(((obj_t) BgL_arg1923z00_3873) ==
																			BGl_za2objza2z00zztype_cachez00);
																	}
																	if (BgL_test2236z00_4830)
																		{	/* Coerce/coerce.scm 360 */
																			BgL_nodez00_bglt BgL_arg1920z00_3874;

																			BgL_arg1920z00_3874 =
																				(((BgL_castz00_bglt)
																					COBJECT(BgL_i1139z00_3871))->
																				BgL_argz00);
																			{	/* Coerce/coerce.scm 360 */
																				obj_t BgL_classz00_3875;

																				BgL_classz00_3875 =
																					BGl_varz00zzast_nodez00;
																				{	/* Coerce/coerce.scm 360 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_3876;
																					{	/* Coerce/coerce.scm 360 */
																						obj_t BgL_tmpz00_4836;

																						BgL_tmpz00_4836 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_arg1920z00_3874));
																						BgL_arg1807z00_3876 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_4836);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Coerce/coerce.scm 360 */
																							long BgL_idxz00_3877;

																							BgL_idxz00_3877 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_3876);
																							BgL_test2235z00_4829 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_3877 + 2L)) ==
																								BgL_classz00_3875);
																						}
																					else
																						{	/* Coerce/coerce.scm 360 */
																							bool_t BgL_res2090z00_3880;

																							{	/* Coerce/coerce.scm 360 */
																								obj_t BgL_oclassz00_3881;

																								{	/* Coerce/coerce.scm 360 */
																									obj_t BgL_arg1815z00_3882;
																									long BgL_arg1816z00_3883;

																									BgL_arg1815z00_3882 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Coerce/coerce.scm 360 */
																										long BgL_arg1817z00_3884;

																										BgL_arg1817z00_3884 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_3876);
																										BgL_arg1816z00_3883 =
																											(BgL_arg1817z00_3884 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_3881 =
																										VECTOR_REF
																										(BgL_arg1815z00_3882,
																										BgL_arg1816z00_3883);
																								}
																								{	/* Coerce/coerce.scm 360 */
																									bool_t
																										BgL__ortest_1115z00_3885;
																									BgL__ortest_1115z00_3885 =
																										(BgL_classz00_3875 ==
																										BgL_oclassz00_3881);
																									if (BgL__ortest_1115z00_3885)
																										{	/* Coerce/coerce.scm 360 */
																											BgL_res2090z00_3880 =
																												BgL__ortest_1115z00_3885;
																										}
																									else
																										{	/* Coerce/coerce.scm 360 */
																											long BgL_odepthz00_3886;

																											{	/* Coerce/coerce.scm 360 */
																												obj_t
																													BgL_arg1804z00_3887;
																												BgL_arg1804z00_3887 =
																													(BgL_oclassz00_3881);
																												BgL_odepthz00_3886 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_3887);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_3886))
																												{	/* Coerce/coerce.scm 360 */
																													obj_t
																														BgL_arg1802z00_3888;
																													{	/* Coerce/coerce.scm 360 */
																														obj_t
																															BgL_arg1803z00_3889;
																														BgL_arg1803z00_3889
																															=
																															(BgL_oclassz00_3881);
																														BgL_arg1802z00_3888
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_3889,
																															2L);
																													}
																													BgL_res2090z00_3880 =
																														(BgL_arg1802z00_3888
																														==
																														BgL_classz00_3875);
																												}
																											else
																												{	/* Coerce/coerce.scm 360 */
																													BgL_res2090z00_3880 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2235z00_4829 =
																								BgL_res2090z00_3880;
																						}
																				}
																			}
																		}
																	else
																		{	/* Coerce/coerce.scm 360 */
																			BgL_test2235z00_4829 = ((bool_t) 0);
																		}
																}
																if (BgL_test2235z00_4829)
																	{	/* Coerce/coerce.scm 361 */
																		BgL_nodez00_bglt BgL_arg1919z00_3890;

																		BgL_arg1919z00_3890 =
																			(((BgL_castz00_bglt)
																				COBJECT(BgL_i1139z00_3871))->
																			BgL_argz00);
																		BgL_typevz00_3834 =
																			((obj_t)
																			BGl_getzd2typezd2zztype_typeofz00
																			(BgL_arg1919z00_3890, ((bool_t) 0)));
																	}
																else
																	{	/* Coerce/coerce.scm 360 */
																		BgL_typevz00_3834 = BFALSE;
																	}
															}
														}
													else
														{	/* Coerce/coerce.scm 358 */
															BgL_typevz00_3834 = BFALSE;
														}
												}
										}
										{	/* Coerce/coerce.scm 363 */
											bool_t BgL_test2240z00_4862;

											{	/* Coerce/coerce.scm 363 */
												obj_t BgL_classz00_3891;

												BgL_classz00_3891 = BGl_typez00zztype_typez00;
												if (BGL_OBJECTP(BgL_typevz00_3834))
													{	/* Coerce/coerce.scm 363 */
														BgL_objectz00_bglt BgL_arg1807z00_3892;

														BgL_arg1807z00_3892 =
															(BgL_objectz00_bglt) (BgL_typevz00_3834);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Coerce/coerce.scm 363 */
																long BgL_idxz00_3893;

																BgL_idxz00_3893 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3892);
																BgL_test2240z00_4862 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3893 + 1L)) ==
																	BgL_classz00_3891);
															}
														else
															{	/* Coerce/coerce.scm 363 */
																bool_t BgL_res2091z00_3896;

																{	/* Coerce/coerce.scm 363 */
																	obj_t BgL_oclassz00_3897;

																	{	/* Coerce/coerce.scm 363 */
																		obj_t BgL_arg1815z00_3898;
																		long BgL_arg1816z00_3899;

																		BgL_arg1815z00_3898 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Coerce/coerce.scm 363 */
																			long BgL_arg1817z00_3900;

																			BgL_arg1817z00_3900 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3892);
																			BgL_arg1816z00_3899 =
																				(BgL_arg1817z00_3900 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3897 =
																			VECTOR_REF(BgL_arg1815z00_3898,
																			BgL_arg1816z00_3899);
																	}
																	{	/* Coerce/coerce.scm 363 */
																		bool_t BgL__ortest_1115z00_3901;

																		BgL__ortest_1115z00_3901 =
																			(BgL_classz00_3891 == BgL_oclassz00_3897);
																		if (BgL__ortest_1115z00_3901)
																			{	/* Coerce/coerce.scm 363 */
																				BgL_res2091z00_3896 =
																					BgL__ortest_1115z00_3901;
																			}
																		else
																			{	/* Coerce/coerce.scm 363 */
																				long BgL_odepthz00_3902;

																				{	/* Coerce/coerce.scm 363 */
																					obj_t BgL_arg1804z00_3903;

																					BgL_arg1804z00_3903 =
																						(BgL_oclassz00_3897);
																					BgL_odepthz00_3902 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3903);
																				}
																				if ((1L < BgL_odepthz00_3902))
																					{	/* Coerce/coerce.scm 363 */
																						obj_t BgL_arg1802z00_3904;

																						{	/* Coerce/coerce.scm 363 */
																							obj_t BgL_arg1803z00_3905;

																							BgL_arg1803z00_3905 =
																								(BgL_oclassz00_3897);
																							BgL_arg1802z00_3904 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3905, 1L);
																						}
																						BgL_res2091z00_3896 =
																							(BgL_arg1802z00_3904 ==
																							BgL_classz00_3891);
																					}
																				else
																					{	/* Coerce/coerce.scm 363 */
																						BgL_res2091z00_3896 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2240z00_4862 = BgL_res2091z00_3896;
															}
													}
												else
													{	/* Coerce/coerce.scm 363 */
														BgL_test2240z00_4862 = ((bool_t) 0);
													}
											}
											if (BgL_test2240z00_4862)
												{	/* Coerce/coerce.scm 363 */
													if (BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(
															((BgL_typez00_bglt) BgL_typecz00_3833),
															((BgL_typez00_bglt) BgL_typevz00_3834)))
														{	/* Coerce/coerce.scm 365 */
															BgL_casezd2valuezd2_4026 = CNST_TABLE_REF(1);
														}
													else
														{	/* Coerce/coerce.scm 365 */
															if (BGl_typezd2disjointzf3z21zztype_miscz00(
																	((BgL_typez00_bglt) BgL_typecz00_3833),
																	((BgL_typez00_bglt) BgL_typevz00_3834)))
																{	/* Coerce/coerce.scm 367 */
																	BgL_casezd2valuezd2_4026 = CNST_TABLE_REF(2);
																}
															else
																{	/* Coerce/coerce.scm 367 */
																	BgL_casezd2valuezd2_4026 = BFALSE;
																}
														}
												}
											else
												{	/* Coerce/coerce.scm 363 */
													BgL_casezd2valuezd2_4026 = BFALSE;
												}
										}
									}
								}
							else
								{	/* Coerce/coerce.scm 377 */
									bool_t BgL_test2247z00_4895;

									{	/* Coerce/coerce.scm 377 */
										obj_t BgL_classz00_3802;

										BgL_classz00_3802 = BGl_appz00zzast_nodez00;
										if (BGL_OBJECTP(BgL_nodez00_3800))
											{	/* Coerce/coerce.scm 377 */
												BgL_objectz00_bglt BgL_arg1807z00_3803;

												BgL_arg1807z00_3803 =
													(BgL_objectz00_bglt) (BgL_nodez00_3800);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Coerce/coerce.scm 377 */
														long BgL_idxz00_3804;

														BgL_idxz00_3804 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3803);
														BgL_test2247z00_4895 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3804 + 3L)) == BgL_classz00_3802);
													}
												else
													{	/* Coerce/coerce.scm 377 */
														bool_t BgL_res2092z00_3807;

														{	/* Coerce/coerce.scm 377 */
															obj_t BgL_oclassz00_3808;

															{	/* Coerce/coerce.scm 377 */
																obj_t BgL_arg1815z00_3809;
																long BgL_arg1816z00_3810;

																BgL_arg1815z00_3809 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Coerce/coerce.scm 377 */
																	long BgL_arg1817z00_3811;

																	BgL_arg1817z00_3811 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3803);
																	BgL_arg1816z00_3810 =
																		(BgL_arg1817z00_3811 - OBJECT_TYPE);
																}
																BgL_oclassz00_3808 =
																	VECTOR_REF(BgL_arg1815z00_3809,
																	BgL_arg1816z00_3810);
															}
															{	/* Coerce/coerce.scm 377 */
																bool_t BgL__ortest_1115z00_3812;

																BgL__ortest_1115z00_3812 =
																	(BgL_classz00_3802 == BgL_oclassz00_3808);
																if (BgL__ortest_1115z00_3812)
																	{	/* Coerce/coerce.scm 377 */
																		BgL_res2092z00_3807 =
																			BgL__ortest_1115z00_3812;
																	}
																else
																	{	/* Coerce/coerce.scm 377 */
																		long BgL_odepthz00_3813;

																		{	/* Coerce/coerce.scm 377 */
																			obj_t BgL_arg1804z00_3814;

																			BgL_arg1804z00_3814 =
																				(BgL_oclassz00_3808);
																			BgL_odepthz00_3813 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3814);
																		}
																		if ((3L < BgL_odepthz00_3813))
																			{	/* Coerce/coerce.scm 377 */
																				obj_t BgL_arg1802z00_3815;

																				{	/* Coerce/coerce.scm 377 */
																					obj_t BgL_arg1803z00_3816;

																					BgL_arg1803z00_3816 =
																						(BgL_oclassz00_3808);
																					BgL_arg1802z00_3815 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3816, 3L);
																				}
																				BgL_res2092z00_3807 =
																					(BgL_arg1802z00_3815 ==
																					BgL_classz00_3802);
																			}
																		else
																			{	/* Coerce/coerce.scm 377 */
																				BgL_res2092z00_3807 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2247z00_4895 = BgL_res2092z00_3807;
													}
											}
										else
											{	/* Coerce/coerce.scm 377 */
												BgL_test2247z00_4895 = ((bool_t) 0);
											}
									}
									if (BgL_test2247z00_4895)
										{	/* Coerce/coerce.scm 377 */
											BgL_nodez00_4002 = BgL_nodez00_3800;
											{	/* Coerce/coerce.scm 291 */
												bool_t BgL_test2252z00_4918;

												{	/* Coerce/coerce.scm 291 */
													obj_t BgL_tmpz00_4919;

													BgL_tmpz00_4919 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_4002)))->
														BgL_argsz00);
													BgL_test2252z00_4918 = PAIRP(BgL_tmpz00_4919);
												}
												if (BgL_test2252z00_4918)
													{	/* Coerce/coerce.scm 292 */
														bool_t BgL_test2253z00_4923;

														{	/* Coerce/coerce.scm 292 */
															obj_t BgL_tmpz00_4924;

															{	/* Coerce/coerce.scm 292 */
																obj_t BgL_pairz00_4003;

																BgL_pairz00_4003 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_4002)))->
																	BgL_argsz00);
																BgL_tmpz00_4924 = CDR(BgL_pairz00_4003);
															}
															BgL_test2253z00_4923 = NULLP(BgL_tmpz00_4924);
														}
														if (BgL_test2253z00_4923)
															{	/* Coerce/coerce.scm 293 */
																bool_t BgL_test2254z00_4929;

																{	/* Coerce/coerce.scm 293 */
																	obj_t BgL_arg1773z00_4004;

																	{	/* Coerce/coerce.scm 293 */
																		obj_t BgL_pairz00_4005;

																		BgL_pairz00_4005 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_nodez00_4002)))->BgL_argsz00);
																		BgL_arg1773z00_4004 = CAR(BgL_pairz00_4005);
																	}
																	BgL_test2254z00_4929 =
																		BGl_sidezd2effectzf3z21zzeffect_effectz00(
																		((BgL_nodez00_bglt) BgL_arg1773z00_4004));
																}
																if (BgL_test2254z00_4929)
																	{	/* Coerce/coerce.scm 293 */
																		BgL_casezd2valuezd2_4026 = BFALSE;
																	}
																else
																	{	/* Coerce/coerce.scm 294 */
																		obj_t BgL_typecz00_4006;

																		BgL_typecz00_4006 =
																			BGl_appzd2predicatezd2ofz00zztype_miscz00(
																			((BgL_appz00_bglt) BgL_nodez00_4002));
																		{	/* Coerce/coerce.scm 294 */
																			BgL_typez00_bglt BgL_typevz00_4007;

																			{	/* Coerce/coerce.scm 295 */
																				obj_t BgL_arg1770z00_4008;

																				{	/* Coerce/coerce.scm 295 */
																					obj_t BgL_pairz00_4009;

																					BgL_pairz00_4009 =
																						(((BgL_appz00_bglt) COBJECT(
																								((BgL_appz00_bglt)
																									BgL_nodez00_4002)))->
																						BgL_argsz00);
																					BgL_arg1770z00_4008 =
																						CAR(BgL_pairz00_4009);
																				}
																				BgL_typevz00_4007 =
																					BGl_getzd2typezd2zztype_typeofz00(
																					((BgL_nodez00_bglt)
																						BgL_arg1770z00_4008), ((bool_t) 0));
																			}
																			{	/* Coerce/coerce.scm 295 */

																				{	/* Coerce/coerce.scm 297 */
																					bool_t BgL_test2255z00_4942;

																					{	/* Coerce/coerce.scm 297 */
																						obj_t BgL_classz00_4010;

																						BgL_classz00_4010 =
																							BGl_typez00zztype_typez00;
																						if (BGL_OBJECTP(BgL_typecz00_4006))
																							{	/* Coerce/coerce.scm 297 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_4011;
																								BgL_arg1807z00_4011 =
																									(BgL_objectz00_bglt)
																									(BgL_typecz00_4006);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* Coerce/coerce.scm 297 */
																										long BgL_idxz00_4012;

																										BgL_idxz00_4012 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_4011);
																										BgL_test2255z00_4942 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_4012 +
																													1L)) ==
																											BgL_classz00_4010);
																									}
																								else
																									{	/* Coerce/coerce.scm 297 */
																										bool_t BgL_res2082z00_4015;

																										{	/* Coerce/coerce.scm 297 */
																											obj_t BgL_oclassz00_4016;

																											{	/* Coerce/coerce.scm 297 */
																												obj_t
																													BgL_arg1815z00_4017;
																												long
																													BgL_arg1816z00_4018;
																												BgL_arg1815z00_4017 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* Coerce/coerce.scm 297 */
																													long
																														BgL_arg1817z00_4019;
																													BgL_arg1817z00_4019 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_4011);
																													BgL_arg1816z00_4018 =
																														(BgL_arg1817z00_4019
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_4016 =
																													VECTOR_REF
																													(BgL_arg1815z00_4017,
																													BgL_arg1816z00_4018);
																											}
																											{	/* Coerce/coerce.scm 297 */
																												bool_t
																													BgL__ortest_1115z00_4020;
																												BgL__ortest_1115z00_4020
																													=
																													(BgL_classz00_4010 ==
																													BgL_oclassz00_4016);
																												if (BgL__ortest_1115z00_4020)
																													{	/* Coerce/coerce.scm 297 */
																														BgL_res2082z00_4015
																															=
																															BgL__ortest_1115z00_4020;
																													}
																												else
																													{	/* Coerce/coerce.scm 297 */
																														long
																															BgL_odepthz00_4021;
																														{	/* Coerce/coerce.scm 297 */
																															obj_t
																																BgL_arg1804z00_4022;
																															BgL_arg1804z00_4022
																																=
																																(BgL_oclassz00_4016);
																															BgL_odepthz00_4021
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_4022);
																														}
																														if (
																															(1L <
																																BgL_odepthz00_4021))
																															{	/* Coerce/coerce.scm 297 */
																																obj_t
																																	BgL_arg1802z00_4023;
																																{	/* Coerce/coerce.scm 297 */
																																	obj_t
																																		BgL_arg1803z00_4024;
																																	BgL_arg1803z00_4024
																																		=
																																		(BgL_oclassz00_4016);
																																	BgL_arg1802z00_4023
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_4024,
																																		1L);
																																}
																																BgL_res2082z00_4015
																																	=
																																	(BgL_arg1802z00_4023
																																	==
																																	BgL_classz00_4010);
																															}
																														else
																															{	/* Coerce/coerce.scm 297 */
																																BgL_res2082z00_4015
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test2255z00_4942 =
																											BgL_res2082z00_4015;
																									}
																							}
																						else
																							{	/* Coerce/coerce.scm 297 */
																								BgL_test2255z00_4942 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test2255z00_4942)
																						{	/* Coerce/coerce.scm 297 */
																							if (
																								(((obj_t) BgL_typevz00_4007) ==
																									BGl_za2objza2z00zztype_cachez00))
																								{	/* Coerce/coerce.scm 300 */
																									BgL_casezd2valuezd2_4026 =
																										BFALSE;
																								}
																							else
																								{	/* Coerce/coerce.scm 300 */
																									if (BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(((BgL_typez00_bglt) BgL_typecz00_4006), BgL_typevz00_4007))
																										{	/* Coerce/coerce.scm 303 */
																											BgL_casezd2valuezd2_4026 =
																												CNST_TABLE_REF(1);
																										}
																									else
																										{	/* Coerce/coerce.scm 303 */
																											if (BGl_typezd2disjointzf3z21zztype_miscz00(((BgL_typez00_bglt) BgL_typecz00_4006), BgL_typevz00_4007))
																												{	/* Coerce/coerce.scm 305 */
																													BgL_casezd2valuezd2_4026
																														= CNST_TABLE_REF(2);
																												}
																											else
																												{	/* Coerce/coerce.scm 305 */
																													BgL_casezd2valuezd2_4026
																														= BFALSE;
																												}
																										}
																								}
																						}
																					else
																						{	/* Coerce/coerce.scm 297 */
																							BgL_casezd2valuezd2_4026 = BFALSE;
																						}
																				}
																			}
																		}
																	}
															}
														else
															{	/* Coerce/coerce.scm 292 */
																BgL_casezd2valuezd2_4026 = BFALSE;
															}
													}
												else
													{	/* Coerce/coerce.scm 291 */
														BgL_casezd2valuezd2_4026 = BFALSE;
													}
											}
										}
									else
										{	/* Coerce/coerce.scm 379 */
											bool_t BgL_test2263z00_4976;

											{	/* Coerce/coerce.scm 379 */
												obj_t BgL_classz00_3817;

												BgL_classz00_3817 = BGl_letzd2varzd2zzast_nodez00;
												if (BGL_OBJECTP(BgL_nodez00_3800))
													{	/* Coerce/coerce.scm 379 */
														BgL_objectz00_bglt BgL_arg1807z00_3818;

														BgL_arg1807z00_3818 =
															(BgL_objectz00_bglt) (BgL_nodez00_3800);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Coerce/coerce.scm 379 */
																long BgL_idxz00_3819;

																BgL_idxz00_3819 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3818);
																BgL_test2263z00_4976 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3819 + 3L)) ==
																	BgL_classz00_3817);
															}
														else
															{	/* Coerce/coerce.scm 379 */
																bool_t BgL_res2093z00_3822;

																{	/* Coerce/coerce.scm 379 */
																	obj_t BgL_oclassz00_3823;

																	{	/* Coerce/coerce.scm 379 */
																		obj_t BgL_arg1815z00_3824;
																		long BgL_arg1816z00_3825;

																		BgL_arg1815z00_3824 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Coerce/coerce.scm 379 */
																			long BgL_arg1817z00_3826;

																			BgL_arg1817z00_3826 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3818);
																			BgL_arg1816z00_3825 =
																				(BgL_arg1817z00_3826 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3823 =
																			VECTOR_REF(BgL_arg1815z00_3824,
																			BgL_arg1816z00_3825);
																	}
																	{	/* Coerce/coerce.scm 379 */
																		bool_t BgL__ortest_1115z00_3827;

																		BgL__ortest_1115z00_3827 =
																			(BgL_classz00_3817 == BgL_oclassz00_3823);
																		if (BgL__ortest_1115z00_3827)
																			{	/* Coerce/coerce.scm 379 */
																				BgL_res2093z00_3822 =
																					BgL__ortest_1115z00_3827;
																			}
																		else
																			{	/* Coerce/coerce.scm 379 */
																				long BgL_odepthz00_3828;

																				{	/* Coerce/coerce.scm 379 */
																					obj_t BgL_arg1804z00_3829;

																					BgL_arg1804z00_3829 =
																						(BgL_oclassz00_3823);
																					BgL_odepthz00_3828 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3829);
																				}
																				if ((3L < BgL_odepthz00_3828))
																					{	/* Coerce/coerce.scm 379 */
																						obj_t BgL_arg1802z00_3830;

																						{	/* Coerce/coerce.scm 379 */
																							obj_t BgL_arg1803z00_3831;

																							BgL_arg1803z00_3831 =
																								(BgL_oclassz00_3823);
																							BgL_arg1802z00_3830 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3831, 3L);
																						}
																						BgL_res2093z00_3822 =
																							(BgL_arg1802z00_3830 ==
																							BgL_classz00_3817);
																					}
																				else
																					{	/* Coerce/coerce.scm 379 */
																						BgL_res2093z00_3822 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2263z00_4976 = BgL_res2093z00_3822;
															}
													}
												else
													{	/* Coerce/coerce.scm 379 */
														BgL_test2263z00_4976 = ((bool_t) 0);
													}
											}
											if (BgL_test2263z00_4976)
												{	/* Coerce/coerce.scm 379 */
													BgL_nodez00_3906 = BgL_nodez00_3800;
													{	/* Coerce/coerce.scm 311 */
														bool_t BgL_test2268z00_4999;

														{	/* Coerce/coerce.scm 311 */
															bool_t BgL_test2269z00_5000;

															{	/* Coerce/coerce.scm 311 */
																obj_t BgL_tmpz00_5001;

																BgL_tmpz00_5001 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_nodez00_3906)))->BgL_bindingsz00);
																BgL_test2269z00_5000 = PAIRP(BgL_tmpz00_5001);
															}
															if (BgL_test2269z00_5000)
																{	/* Coerce/coerce.scm 311 */
																	obj_t BgL_tmpz00_5005;

																	{	/* Coerce/coerce.scm 311 */
																		obj_t BgL_pairz00_3907;

																		BgL_pairz00_3907 =
																			(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_nodez00_3906)))->
																			BgL_bindingsz00);
																		BgL_tmpz00_5005 = CDR(BgL_pairz00_3907);
																	}
																	BgL_test2268z00_4999 = NULLP(BgL_tmpz00_5005);
																}
															else
																{	/* Coerce/coerce.scm 311 */
																	BgL_test2268z00_4999 = ((bool_t) 0);
																}
														}
														if (BgL_test2268z00_4999)
															{	/* Coerce/coerce.scm 313 */
																bool_t BgL_test2270z00_5010;

																{	/* Coerce/coerce.scm 313 */
																	BgL_nodez00_bglt BgL_arg1892z00_3908;

																	BgL_arg1892z00_3908 =
																		(((BgL_letzd2varzd2_bglt) COBJECT(
																				((BgL_letzd2varzd2_bglt)
																					BgL_nodez00_3906)))->BgL_bodyz00);
																	{	/* Coerce/coerce.scm 313 */
																		obj_t BgL_classz00_3909;

																		BgL_classz00_3909 = BGl_appz00zzast_nodez00;
																		{	/* Coerce/coerce.scm 313 */
																			BgL_objectz00_bglt BgL_arg1807z00_3910;

																			{	/* Coerce/coerce.scm 313 */
																				obj_t BgL_tmpz00_5013;

																				BgL_tmpz00_5013 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_arg1892z00_3908));
																				BgL_arg1807z00_3910 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_5013);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Coerce/coerce.scm 313 */
																					long BgL_idxz00_3911;

																					BgL_idxz00_3911 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_3910);
																					BgL_test2270z00_5010 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_3911 + 3L)) ==
																						BgL_classz00_3909);
																				}
																			else
																				{	/* Coerce/coerce.scm 313 */
																					bool_t BgL_res2083z00_3914;

																					{	/* Coerce/coerce.scm 313 */
																						obj_t BgL_oclassz00_3915;

																						{	/* Coerce/coerce.scm 313 */
																							obj_t BgL_arg1815z00_3916;
																							long BgL_arg1816z00_3917;

																							BgL_arg1815z00_3916 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Coerce/coerce.scm 313 */
																								long BgL_arg1817z00_3918;

																								BgL_arg1817z00_3918 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_3910);
																								BgL_arg1816z00_3917 =
																									(BgL_arg1817z00_3918 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_3915 =
																								VECTOR_REF(BgL_arg1815z00_3916,
																								BgL_arg1816z00_3917);
																						}
																						{	/* Coerce/coerce.scm 313 */
																							bool_t BgL__ortest_1115z00_3919;

																							BgL__ortest_1115z00_3919 =
																								(BgL_classz00_3909 ==
																								BgL_oclassz00_3915);
																							if (BgL__ortest_1115z00_3919)
																								{	/* Coerce/coerce.scm 313 */
																									BgL_res2083z00_3914 =
																										BgL__ortest_1115z00_3919;
																								}
																							else
																								{	/* Coerce/coerce.scm 313 */
																									long BgL_odepthz00_3920;

																									{	/* Coerce/coerce.scm 313 */
																										obj_t BgL_arg1804z00_3921;

																										BgL_arg1804z00_3921 =
																											(BgL_oclassz00_3915);
																										BgL_odepthz00_3920 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_3921);
																									}
																									if ((3L < BgL_odepthz00_3920))
																										{	/* Coerce/coerce.scm 313 */
																											obj_t BgL_arg1802z00_3922;

																											{	/* Coerce/coerce.scm 313 */
																												obj_t
																													BgL_arg1803z00_3923;
																												BgL_arg1803z00_3923 =
																													(BgL_oclassz00_3915);
																												BgL_arg1802z00_3922 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_3923,
																													3L);
																											}
																											BgL_res2083z00_3914 =
																												(BgL_arg1802z00_3922 ==
																												BgL_classz00_3909);
																										}
																									else
																										{	/* Coerce/coerce.scm 313 */
																											BgL_res2083z00_3914 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2270z00_5010 =
																						BgL_res2083z00_3914;
																				}
																		}
																	}
																}
																if (BgL_test2270z00_5010)
																	{	/* Coerce/coerce.scm 314 */
																		obj_t BgL_varz00_3924;

																		{	/* Coerce/coerce.scm 314 */
																			obj_t BgL_argsz00_3925;

																			BgL_argsz00_3925 =
																				(((BgL_appz00_bglt) COBJECT(
																						((BgL_appz00_bglt)
																							(((BgL_letzd2varzd2_bglt) COBJECT(
																										((BgL_letzd2varzd2_bglt)
																											BgL_nodez00_3906)))->
																								BgL_bodyz00))))->BgL_argsz00);
																			if (PAIRP(BgL_argsz00_3925))
																				{	/* Coerce/coerce.scm 318 */
																					bool_t BgL_test2275z00_5042;

																					if (NULLP(CDR(BgL_argsz00_3925)))
																						{	/* Coerce/coerce.scm 318 */
																							obj_t BgL_arg1872z00_3926;

																							BgL_arg1872z00_3926 =
																								CAR(BgL_argsz00_3925);
																							{	/* Coerce/coerce.scm 318 */
																								obj_t BgL_classz00_3927;

																								BgL_classz00_3927 =
																									BGl_varz00zzast_nodez00;
																								if (BGL_OBJECTP
																									(BgL_arg1872z00_3926))
																									{	/* Coerce/coerce.scm 318 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_3928;
																										BgL_arg1807z00_3928 =
																											(BgL_objectz00_bglt)
																											(BgL_arg1872z00_3926);
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Coerce/coerce.scm 318 */
																												long BgL_idxz00_3929;

																												BgL_idxz00_3929 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_3928);
																												BgL_test2275z00_5042 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_3929 +
																															2L)) ==
																													BgL_classz00_3927);
																											}
																										else
																											{	/* Coerce/coerce.scm 318 */
																												bool_t
																													BgL_res2084z00_3932;
																												{	/* Coerce/coerce.scm 318 */
																													obj_t
																														BgL_oclassz00_3933;
																													{	/* Coerce/coerce.scm 318 */
																														obj_t
																															BgL_arg1815z00_3934;
																														long
																															BgL_arg1816z00_3935;
																														BgL_arg1815z00_3934
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Coerce/coerce.scm 318 */
																															long
																																BgL_arg1817z00_3936;
																															BgL_arg1817z00_3936
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_3928);
																															BgL_arg1816z00_3935
																																=
																																(BgL_arg1817z00_3936
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_3933 =
																															VECTOR_REF
																															(BgL_arg1815z00_3934,
																															BgL_arg1816z00_3935);
																													}
																													{	/* Coerce/coerce.scm 318 */
																														bool_t
																															BgL__ortest_1115z00_3937;
																														BgL__ortest_1115z00_3937
																															=
																															(BgL_classz00_3927
																															==
																															BgL_oclassz00_3933);
																														if (BgL__ortest_1115z00_3937)
																															{	/* Coerce/coerce.scm 318 */
																																BgL_res2084z00_3932
																																	=
																																	BgL__ortest_1115z00_3937;
																															}
																														else
																															{	/* Coerce/coerce.scm 318 */
																																long
																																	BgL_odepthz00_3938;
																																{	/* Coerce/coerce.scm 318 */
																																	obj_t
																																		BgL_arg1804z00_3939;
																																	BgL_arg1804z00_3939
																																		=
																																		(BgL_oclassz00_3933);
																																	BgL_odepthz00_3938
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_3939);
																																}
																																if (
																																	(2L <
																																		BgL_odepthz00_3938))
																																	{	/* Coerce/coerce.scm 318 */
																																		obj_t
																																			BgL_arg1802z00_3940;
																																		{	/* Coerce/coerce.scm 318 */
																																			obj_t
																																				BgL_arg1803z00_3941;
																																			BgL_arg1803z00_3941
																																				=
																																				(BgL_oclassz00_3933);
																																			BgL_arg1802z00_3940
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_3941,
																																				2L);
																																		}
																																		BgL_res2084z00_3932
																																			=
																																			(BgL_arg1802z00_3940
																																			==
																																			BgL_classz00_3927);
																																	}
																																else
																																	{	/* Coerce/coerce.scm 318 */
																																		BgL_res2084z00_3932
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test2275z00_5042 =
																													BgL_res2084z00_3932;
																											}
																									}
																								else
																									{	/* Coerce/coerce.scm 318 */
																										BgL_test2275z00_5042 =
																											((bool_t) 0);
																									}
																							}
																						}
																					else
																						{	/* Coerce/coerce.scm 318 */
																							BgL_test2275z00_5042 =
																								((bool_t) 0);
																						}
																					if (BgL_test2275z00_5042)
																						{	/* Coerce/coerce.scm 318 */
																							BgL_varz00_3924 =
																								((obj_t)
																								(((BgL_varz00_bglt) COBJECT(
																											((BgL_varz00_bglt)
																												CAR
																												(BgL_argsz00_3925))))->
																									BgL_variablez00));
																						}
																					else
																						{	/* Coerce/coerce.scm 321 */
																							bool_t BgL_test2281z00_5073;

																							{	/* Coerce/coerce.scm 321 */
																								obj_t BgL_tmpz00_5074;

																								BgL_tmpz00_5074 =
																									CDR(BgL_argsz00_3925);
																								BgL_test2281z00_5073 =
																									PAIRP(BgL_tmpz00_5074);
																							}
																							if (BgL_test2281z00_5073)
																								{	/* Coerce/coerce.scm 323 */
																									bool_t BgL_test2282z00_5077;

																									if (NULLP(CDR(CDR
																												(BgL_argsz00_3925))))
																										{	/* Coerce/coerce.scm 323 */
																											obj_t BgL_arg1868z00_3942;

																											BgL_arg1868z00_3942 =
																												CAR(BgL_argsz00_3925);
																											{	/* Coerce/coerce.scm 323 */
																												obj_t BgL_classz00_3943;

																												BgL_classz00_3943 =
																													BGl_varz00zzast_nodez00;
																												if (BGL_OBJECTP
																													(BgL_arg1868z00_3942))
																													{	/* Coerce/coerce.scm 323 */
																														BgL_objectz00_bglt
																															BgL_arg1807z00_3944;
																														BgL_arg1807z00_3944
																															=
																															(BgL_objectz00_bglt)
																															(BgL_arg1868z00_3942);
																														if (BGL_CONDEXPAND_ISA_ARCH64())
																															{	/* Coerce/coerce.scm 323 */
																																long
																																	BgL_idxz00_3945;
																																BgL_idxz00_3945
																																	=
																																	BGL_OBJECT_INHERITANCE_NUM
																																	(BgL_arg1807z00_3944);
																																BgL_test2282z00_5077
																																	=
																																	(VECTOR_REF
																																	(BGl_za2inheritancesza2z00zz__objectz00,
																																		(BgL_idxz00_3945
																																			+ 2L)) ==
																																	BgL_classz00_3943);
																															}
																														else
																															{	/* Coerce/coerce.scm 323 */
																																bool_t
																																	BgL_res2085z00_3948;
																																{	/* Coerce/coerce.scm 323 */
																																	obj_t
																																		BgL_oclassz00_3949;
																																	{	/* Coerce/coerce.scm 323 */
																																		obj_t
																																			BgL_arg1815z00_3950;
																																		long
																																			BgL_arg1816z00_3951;
																																		BgL_arg1815z00_3950
																																			=
																																			(BGl_za2classesza2z00zz__objectz00);
																																		{	/* Coerce/coerce.scm 323 */
																																			long
																																				BgL_arg1817z00_3952;
																																			BgL_arg1817z00_3952
																																				=
																																				BGL_OBJECT_CLASS_NUM
																																				(BgL_arg1807z00_3944);
																																			BgL_arg1816z00_3951
																																				=
																																				(BgL_arg1817z00_3952
																																				-
																																				OBJECT_TYPE);
																																		}
																																		BgL_oclassz00_3949
																																			=
																																			VECTOR_REF
																																			(BgL_arg1815z00_3950,
																																			BgL_arg1816z00_3951);
																																	}
																																	{	/* Coerce/coerce.scm 323 */
																																		bool_t
																																			BgL__ortest_1115z00_3953;
																																		BgL__ortest_1115z00_3953
																																			=
																																			(BgL_classz00_3943
																																			==
																																			BgL_oclassz00_3949);
																																		if (BgL__ortest_1115z00_3953)
																																			{	/* Coerce/coerce.scm 323 */
																																				BgL_res2085z00_3948
																																					=
																																					BgL__ortest_1115z00_3953;
																																			}
																																		else
																																			{	/* Coerce/coerce.scm 323 */
																																				long
																																					BgL_odepthz00_3954;
																																				{	/* Coerce/coerce.scm 323 */
																																					obj_t
																																						BgL_arg1804z00_3955;
																																					BgL_arg1804z00_3955
																																						=
																																						(BgL_oclassz00_3949);
																																					BgL_odepthz00_3954
																																						=
																																						BGL_CLASS_DEPTH
																																						(BgL_arg1804z00_3955);
																																				}
																																				if (
																																					(2L <
																																						BgL_odepthz00_3954))
																																					{	/* Coerce/coerce.scm 323 */
																																						obj_t
																																							BgL_arg1802z00_3956;
																																						{	/* Coerce/coerce.scm 323 */
																																							obj_t
																																								BgL_arg1803z00_3957;
																																							BgL_arg1803z00_3957
																																								=
																																								(BgL_oclassz00_3949);
																																							BgL_arg1802z00_3956
																																								=
																																								BGL_CLASS_ANCESTORS_REF
																																								(BgL_arg1803z00_3957,
																																								2L);
																																						}
																																						BgL_res2085z00_3948
																																							=
																																							(BgL_arg1802z00_3956
																																							==
																																							BgL_classz00_3943);
																																					}
																																				else
																																					{	/* Coerce/coerce.scm 323 */
																																						BgL_res2085z00_3948
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																	}
																																}
																																BgL_test2282z00_5077
																																	=
																																	BgL_res2085z00_3948;
																															}
																													}
																												else
																													{	/* Coerce/coerce.scm 323 */
																														BgL_test2282z00_5077
																															= ((bool_t) 0);
																													}
																											}
																										}
																									else
																										{	/* Coerce/coerce.scm 323 */
																											BgL_test2282z00_5077 =
																												((bool_t) 0);
																										}
																									if (BgL_test2282z00_5077)
																										{	/* Coerce/coerce.scm 323 */
																											BgL_varz00_3924 =
																												((obj_t)
																												(((BgL_varz00_bglt)
																														COBJECT((
																																(BgL_varz00_bglt)
																																CAR
																																(BgL_argsz00_3925))))->
																													BgL_variablez00));
																										}
																									else
																										{	/* Coerce/coerce.scm 323 */
																											BgL_varz00_3924 = BFALSE;
																										}
																								}
																							else
																								{	/* Coerce/coerce.scm 321 */
																									BgL_varz00_3924 = BFALSE;
																								}
																						}
																				}
																			else
																				{	/* Coerce/coerce.scm 316 */
																					BgL_varz00_3924 = BFALSE;
																				}
																		}
																		{	/* Coerce/coerce.scm 314 */
																			obj_t BgL_typecz00_3958;

																			{	/* Coerce/coerce.scm 328 */
																				BgL_nodez00_bglt BgL_arg1848z00_3959;

																				BgL_arg1848z00_3959 =
																					(((BgL_letzd2varzd2_bglt) COBJECT(
																							((BgL_letzd2varzd2_bglt)
																								BgL_nodez00_3906)))->
																					BgL_bodyz00);
																				BgL_typecz00_3958 =
																					BGl_appzd2predicatezd2ofz00zztype_miscz00
																					(((BgL_appz00_bglt)
																						BgL_arg1848z00_3959));
																			}
																			{	/* Coerce/coerce.scm 328 */
																				BgL_typez00_bglt BgL_typepz00_3960;

																				{
																					BgL_variablez00_bglt BgL_auxz00_5113;

																					{	/* Coerce/coerce.scm 329 */
																						obj_t BgL_pairz00_3961;

																						{	/* Coerce/coerce.scm 329 */
																							obj_t BgL_pairz00_3962;

																							BgL_pairz00_3962 =
																								(((BgL_letzd2varzd2_bglt)
																									COBJECT((
																											(BgL_letzd2varzd2_bglt)
																											BgL_nodez00_3906)))->
																								BgL_bindingsz00);
																							BgL_pairz00_3961 =
																								CAR(BgL_pairz00_3962);
																						}
																						BgL_auxz00_5113 =
																							((BgL_variablez00_bglt)
																							CAR(BgL_pairz00_3961));
																					}
																					BgL_typepz00_3960 =
																						(((BgL_variablez00_bglt)
																							COBJECT(BgL_auxz00_5113))->
																						BgL_typez00);
																				}
																				{	/* Coerce/coerce.scm 329 */
																					BgL_typez00_bglt BgL_typevz00_3963;

																					if (
																						(((obj_t) BgL_typepz00_3960) ==
																							BGl_za2objza2z00zztype_cachez00))
																						{	/* Coerce/coerce.scm 331 */
																							obj_t BgL_arg1842z00_3964;

																							{	/* Coerce/coerce.scm 331 */
																								obj_t BgL_pairz00_3965;

																								{	/* Coerce/coerce.scm 331 */
																									obj_t BgL_pairz00_3966;

																									BgL_pairz00_3966 =
																										(((BgL_letzd2varzd2_bglt)
																											COBJECT((
																													(BgL_letzd2varzd2_bglt)
																													BgL_nodez00_3906)))->
																										BgL_bindingsz00);
																									BgL_pairz00_3965 =
																										CAR(BgL_pairz00_3966);
																								}
																								BgL_arg1842z00_3964 =
																									CDR(BgL_pairz00_3965);
																							}
																							BgL_typevz00_3963 =
																								BGl_getzd2typezd2zztype_typeofz00
																								(((BgL_nodez00_bglt)
																									BgL_arg1842z00_3964),
																								((bool_t) 0));
																						}
																					else
																						{	/* Coerce/coerce.scm 330 */
																							BgL_typevz00_3963 =
																								BgL_typepz00_3960;
																						}
																					{	/* Coerce/coerce.scm 330 */

																						{	/* Coerce/coerce.scm 334 */
																							bool_t BgL_test2289z00_5129;

																							{	/* Coerce/coerce.scm 334 */
																								obj_t BgL_classz00_3967;

																								BgL_classz00_3967 =
																									BGl_typez00zztype_typez00;
																								if (BGL_OBJECTP
																									(BgL_typecz00_3958))
																									{	/* Coerce/coerce.scm 334 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_3968;
																										BgL_arg1807z00_3968 =
																											(BgL_objectz00_bglt)
																											(BgL_typecz00_3958);
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Coerce/coerce.scm 334 */
																												long BgL_idxz00_3969;

																												BgL_idxz00_3969 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_3968);
																												BgL_test2289z00_5129 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_3969 +
																															1L)) ==
																													BgL_classz00_3967);
																											}
																										else
																											{	/* Coerce/coerce.scm 334 */
																												bool_t
																													BgL_res2086z00_3972;
																												{	/* Coerce/coerce.scm 334 */
																													obj_t
																														BgL_oclassz00_3973;
																													{	/* Coerce/coerce.scm 334 */
																														obj_t
																															BgL_arg1815z00_3974;
																														long
																															BgL_arg1816z00_3975;
																														BgL_arg1815z00_3974
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Coerce/coerce.scm 334 */
																															long
																																BgL_arg1817z00_3976;
																															BgL_arg1817z00_3976
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_3968);
																															BgL_arg1816z00_3975
																																=
																																(BgL_arg1817z00_3976
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_3973 =
																															VECTOR_REF
																															(BgL_arg1815z00_3974,
																															BgL_arg1816z00_3975);
																													}
																													{	/* Coerce/coerce.scm 334 */
																														bool_t
																															BgL__ortest_1115z00_3977;
																														BgL__ortest_1115z00_3977
																															=
																															(BgL_classz00_3967
																															==
																															BgL_oclassz00_3973);
																														if (BgL__ortest_1115z00_3977)
																															{	/* Coerce/coerce.scm 334 */
																																BgL_res2086z00_3972
																																	=
																																	BgL__ortest_1115z00_3977;
																															}
																														else
																															{	/* Coerce/coerce.scm 334 */
																																long
																																	BgL_odepthz00_3978;
																																{	/* Coerce/coerce.scm 334 */
																																	obj_t
																																		BgL_arg1804z00_3979;
																																	BgL_arg1804z00_3979
																																		=
																																		(BgL_oclassz00_3973);
																																	BgL_odepthz00_3978
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_3979);
																																}
																																if (
																																	(1L <
																																		BgL_odepthz00_3978))
																																	{	/* Coerce/coerce.scm 334 */
																																		obj_t
																																			BgL_arg1802z00_3980;
																																		{	/* Coerce/coerce.scm 334 */
																																			obj_t
																																				BgL_arg1803z00_3981;
																																			BgL_arg1803z00_3981
																																				=
																																				(BgL_oclassz00_3973);
																																			BgL_arg1802z00_3980
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_3981,
																																				1L);
																																		}
																																		BgL_res2086z00_3972
																																			=
																																			(BgL_arg1802z00_3980
																																			==
																																			BgL_classz00_3967);
																																	}
																																else
																																	{	/* Coerce/coerce.scm 334 */
																																		BgL_res2086z00_3972
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test2289z00_5129 =
																													BgL_res2086z00_3972;
																											}
																									}
																								else
																									{	/* Coerce/coerce.scm 334 */
																										BgL_test2289z00_5129 =
																											((bool_t) 0);
																									}
																							}
																							if (BgL_test2289z00_5129)
																								{	/* Coerce/coerce.scm 337 */
																									bool_t BgL_test2294z00_5152;

																									{	/* Coerce/coerce.scm 337 */
																										obj_t BgL_tmpz00_5153;

																										{	/* Coerce/coerce.scm 337 */
																											obj_t BgL_pairz00_3982;

																											BgL_pairz00_3982 =
																												(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_3906)))->BgL_bindingsz00);
																											BgL_tmpz00_5153 =
																												CAR(CAR
																												(BgL_pairz00_3982));
																										}
																										BgL_test2294z00_5152 =
																											(BgL_varz00_3924 ==
																											BgL_tmpz00_5153);
																									}
																									if (BgL_test2294z00_5152)
																										{	/* Coerce/coerce.scm 337 */
																											if (
																												(((obj_t)
																														BgL_typevz00_3963)
																													==
																													BGl_za2objza2z00zztype_cachez00))
																												{	/* Coerce/coerce.scm 340 */
																													BgL_casezd2valuezd2_4026
																														= BFALSE;
																												}
																											else
																												{	/* Coerce/coerce.scm 340 */
																													if (BGl_typezd2lesszd2specificzf3zf3zztype_miscz00(((BgL_typez00_bglt) BgL_typecz00_3958), BgL_typevz00_3963))
																														{	/* Coerce/coerce.scm 343 */
																															BgL_casezd2valuezd2_4026
																																=
																																CNST_TABLE_REF
																																(1);
																														}
																													else
																														{	/* Coerce/coerce.scm 343 */
																															if (BGl_typezd2disjointzf3z21zztype_miscz00(((BgL_typez00_bglt) BgL_typecz00_3958), BgL_typevz00_3963))
																																{	/* Coerce/coerce.scm 345 */
																																	BgL_casezd2valuezd2_4026
																																		=
																																		CNST_TABLE_REF
																																		(2);
																																}
																															else
																																{	/* Coerce/coerce.scm 345 */
																																	BgL_casezd2valuezd2_4026
																																		= BFALSE;
																																}
																														}
																												}
																										}
																									else
																										{	/* Coerce/coerce.scm 337 */
																											BgL_casezd2valuezd2_4026 =
																												BFALSE;
																										}
																								}
																							else
																								{	/* Coerce/coerce.scm 334 */
																									BgL_casezd2valuezd2_4026 =
																										BFALSE;
																								}
																						}
																					}
																				}
																			}
																		}
																	}
																else
																	{	/* Coerce/coerce.scm 349 */
																		bool_t BgL_test2298z00_5170;

																		{	/* Coerce/coerce.scm 349 */
																			bool_t BgL_test2299z00_5171;

																			{	/* Coerce/coerce.scm 349 */
																				BgL_nodez00_bglt BgL_arg1891z00_3983;

																				BgL_arg1891z00_3983 =
																					(((BgL_letzd2varzd2_bglt) COBJECT(
																							((BgL_letzd2varzd2_bglt)
																								BgL_nodez00_3906)))->
																					BgL_bodyz00);
																				{	/* Coerce/coerce.scm 349 */
																					obj_t BgL_classz00_3984;

																					BgL_classz00_3984 =
																						BGl_varz00zzast_nodez00;
																					{	/* Coerce/coerce.scm 349 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_3985;
																						{	/* Coerce/coerce.scm 349 */
																							obj_t BgL_tmpz00_5174;

																							BgL_tmpz00_5174 =
																								((obj_t)
																								((BgL_objectz00_bglt)
																									BgL_arg1891z00_3983));
																							BgL_arg1807z00_3985 =
																								(BgL_objectz00_bglt)
																								(BgL_tmpz00_5174);
																						}
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Coerce/coerce.scm 349 */
																								long BgL_idxz00_3986;

																								BgL_idxz00_3986 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_3985);
																								BgL_test2299z00_5171 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_3986 + 2L)) ==
																									BgL_classz00_3984);
																							}
																						else
																							{	/* Coerce/coerce.scm 349 */
																								bool_t BgL_res2087z00_3989;

																								{	/* Coerce/coerce.scm 349 */
																									obj_t BgL_oclassz00_3990;

																									{	/* Coerce/coerce.scm 349 */
																										obj_t BgL_arg1815z00_3991;
																										long BgL_arg1816z00_3992;

																										BgL_arg1815z00_3991 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Coerce/coerce.scm 349 */
																											long BgL_arg1817z00_3993;

																											BgL_arg1817z00_3993 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_3985);
																											BgL_arg1816z00_3992 =
																												(BgL_arg1817z00_3993 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_3990 =
																											VECTOR_REF
																											(BgL_arg1815z00_3991,
																											BgL_arg1816z00_3992);
																									}
																									{	/* Coerce/coerce.scm 349 */
																										bool_t
																											BgL__ortest_1115z00_3994;
																										BgL__ortest_1115z00_3994 =
																											(BgL_classz00_3984 ==
																											BgL_oclassz00_3990);
																										if (BgL__ortest_1115z00_3994)
																											{	/* Coerce/coerce.scm 349 */
																												BgL_res2087z00_3989 =
																													BgL__ortest_1115z00_3994;
																											}
																										else
																											{	/* Coerce/coerce.scm 349 */
																												long BgL_odepthz00_3995;

																												{	/* Coerce/coerce.scm 349 */
																													obj_t
																														BgL_arg1804z00_3996;
																													BgL_arg1804z00_3996 =
																														(BgL_oclassz00_3990);
																													BgL_odepthz00_3995 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_3996);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_3995))
																													{	/* Coerce/coerce.scm 349 */
																														obj_t
																															BgL_arg1802z00_3997;
																														{	/* Coerce/coerce.scm 349 */
																															obj_t
																																BgL_arg1803z00_3998;
																															BgL_arg1803z00_3998
																																=
																																(BgL_oclassz00_3990);
																															BgL_arg1802z00_3997
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_3998,
																																2L);
																														}
																														BgL_res2087z00_3989
																															=
																															(BgL_arg1802z00_3997
																															==
																															BgL_classz00_3984);
																													}
																												else
																													{	/* Coerce/coerce.scm 349 */
																														BgL_res2087z00_3989
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2299z00_5171 =
																									BgL_res2087z00_3989;
																							}
																					}
																				}
																			}
																			if (BgL_test2299z00_5171)
																				{	/* Coerce/coerce.scm 349 */
																					obj_t BgL_tmpz00_5197;

																					{	/* Coerce/coerce.scm 349 */
																						obj_t BgL_pairz00_3999;

																						BgL_pairz00_3999 =
																							(((BgL_letzd2varzd2_bglt) COBJECT(
																									((BgL_letzd2varzd2_bglt)
																										BgL_nodez00_3906)))->
																							BgL_bindingsz00);
																						BgL_tmpz00_5197 =
																							CAR(CAR(BgL_pairz00_3999));
																					}
																					BgL_test2298z00_5170 =
																						(
																						((obj_t)
																							(((BgL_varz00_bglt) COBJECT(
																										((BgL_varz00_bglt)
																											(((BgL_letzd2varzd2_bglt)
																													COBJECT((
																															(BgL_letzd2varzd2_bglt)
																															BgL_nodez00_3906)))->
																												BgL_bodyz00))))->
																								BgL_variablez00)) ==
																						BgL_tmpz00_5197);
																				}
																			else
																				{	/* Coerce/coerce.scm 349 */
																					BgL_test2298z00_5170 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test2298z00_5170)
																			{	/* Coerce/coerce.scm 350 */
																				obj_t BgL_arg1884z00_4000;

																				{	/* Coerce/coerce.scm 350 */
																					obj_t BgL_pairz00_4001;

																					BgL_pairz00_4001 =
																						(((BgL_letzd2varzd2_bglt) COBJECT(
																								((BgL_letzd2varzd2_bglt)
																									BgL_nodez00_3906)))->
																						BgL_bindingsz00);
																					BgL_arg1884z00_4000 =
																						CDR(CAR(BgL_pairz00_4001));
																				}
																				{
																					obj_t BgL_nodez00_5212;

																					BgL_nodez00_5212 =
																						BgL_arg1884z00_4000;
																					BgL_nodez00_3800 = BgL_nodez00_5212;
																					goto
																						BgL_testzd2staticzd2valuez00_3796;
																				}
																			}
																		else
																			{	/* Coerce/coerce.scm 349 */
																				BgL_casezd2valuezd2_4026 = BFALSE;
																			}
																	}
															}
														else
															{	/* Coerce/coerce.scm 311 */
																BgL_casezd2valuezd2_4026 = BFALSE;
															}
													}
												}
											else
												{	/* Coerce/coerce.scm 379 */
													BgL_casezd2valuezd2_4026 = BFALSE;
												}
										}
								}
						}
					}
					if ((BgL_casezd2valuezd2_4026 == CNST_TABLE_REF(1)))
						{	/* Coerce/coerce.scm 387 */
							BgL_nodez00_bglt BgL_arg1753z00_4028;

							BgL_arg1753z00_4028 =
								(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->
								BgL_truez00);
							return BGl_coercez12z12zzcoerce_coercez00(BgL_arg1753z00_4028,
								BgL_callerz00_3592, ((BgL_typez00_bglt) BgL_toz00_3593),
								CBOOL(BgL_safez00_3594));
						}
					else
						{	/* Coerce/coerce.scm 385 */
							if ((BgL_casezd2valuezd2_4026 == CNST_TABLE_REF(2)))
								{	/* Coerce/coerce.scm 389 */
									BgL_nodez00_bglt BgL_arg1755z00_4029;

									BgL_arg1755z00_4029 =
										(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->
										BgL_falsez00);
									return BGl_coercez12z12zzcoerce_coercez00(BgL_arg1755z00_4029,
										BgL_callerz00_3592, ((BgL_typez00_bglt) BgL_toz00_3593),
										CBOOL(BgL_safez00_3594));
								}
							else
								{	/* Coerce/coerce.scm 385 */
									{
										BgL_nodez00_bglt BgL_auxz00_5230;

										{	/* Coerce/coerce.scm 391 */
											BgL_nodez00_bglt BgL_arg1761z00_4030;

											BgL_arg1761z00_4030 =
												(((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->
												BgL_truez00);
											BgL_auxz00_5230 =
												BGl_coercez12z12zzcoerce_coercez00(BgL_arg1761z00_4030,
												BgL_callerz00_3592, ((BgL_typez00_bglt) BgL_toz00_3593),
												CBOOL(BgL_safez00_3594));
										}
										((((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->
												BgL_truez00) =
											((BgL_nodez00_bglt) BgL_auxz00_5230), BUNSPEC);
									}
									{
										BgL_nodez00_bglt BgL_auxz00_5238;

										{	/* Coerce/coerce.scm 392 */
											BgL_nodez00_bglt BgL_arg1762z00_4031;

											BgL_arg1762z00_4031 =
												(((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->
												BgL_falsez00);
											BgL_auxz00_5238 =
												BGl_coercez12z12zzcoerce_coercez00(BgL_arg1762z00_4031,
												BgL_callerz00_3592, ((BgL_typez00_bglt) BgL_toz00_3593),
												CBOOL(BgL_safez00_3594));
										}
										((((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_3591)))->
												BgL_falsez00) =
											((BgL_nodez00_bglt) BgL_auxz00_5238), BUNSPEC);
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_conditionalz00_bglt) BgL_nodez00_3591))))->
											BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_toz00_3593)),
										BUNSPEC);
									return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt)
											BgL_nodez00_3591));
								}
						}
				}
			}
		}

	}



/* &coerce!-setq1325 */
	BgL_nodez00_bglt BGl_z62coercez12zd2setq1325za2zzcoerce_coercez00(obj_t
		BgL_envz00_3595, obj_t BgL_nodez00_3596, obj_t BgL_callerz00_3597,
		obj_t BgL_toz00_3598, obj_t BgL_safez00_3599)
	{
		{	/* Coerce/coerce.scm 264 */
			{
				BgL_nodez00_bglt BgL_auxz00_5252;

				{	/* Coerce/coerce.scm 266 */
					BgL_nodez00_bglt BgL_arg1747z00_4033;
					BgL_typez00_bglt BgL_arg1748z00_4034;

					BgL_arg1747z00_4033 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_3596)))->BgL_valuez00);
					BgL_arg1748z00_4034 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											(((BgL_setqz00_bglt) COBJECT(
														((BgL_setqz00_bglt) BgL_nodez00_3596)))->
												BgL_varz00)))->BgL_variablez00)))->BgL_typez00);
					BgL_auxz00_5252 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1747z00_4033,
						BgL_callerz00_3597, BgL_arg1748z00_4034, CBOOL(BgL_safez00_3599));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_3596)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_5252), BUNSPEC);
			}
			return
				BGl_convertz12z12zzcoerce_convertz00(
				((BgL_nodez00_bglt)
					((BgL_setqz00_bglt) BgL_nodez00_3596)),
				((BgL_typez00_bglt) BGl_za2unspecza2z00zztype_cachez00),
				((BgL_typez00_bglt) BgL_toz00_3598), CBOOL(BgL_safez00_3599));
		}

	}



/* &coerce!-cast1323 */
	BgL_nodez00_bglt BGl_z62coercez12zd2cast1323za2zzcoerce_coercez00(obj_t
		BgL_envz00_3600, obj_t BgL_nodez00_3601, obj_t BgL_callerz00_3602,
		obj_t BgL_toz00_3603, obj_t BgL_safez00_3604)
	{
		{	/* Coerce/coerce.scm 254 */
			{
				BgL_nodez00_bglt BgL_auxz00_5269;

				{	/* Coerce/coerce.scm 258 */
					BgL_nodez00_bglt BgL_arg1738z00_4036;
					BgL_typez00_bglt BgL_arg1739z00_4037;

					BgL_arg1738z00_4036 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_3601)))->BgL_argz00);
					{	/* Coerce/coerce.scm 258 */
						BgL_nodez00_bglt BgL_arg1740z00_4038;

						BgL_arg1740z00_4038 =
							(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nodez00_3601)))->BgL_argz00);
						BgL_arg1739z00_4037 =
							BGl_getzd2typezd2zztype_typeofz00(BgL_arg1740z00_4038,
							((bool_t) 0));
					}
					BgL_auxz00_5269 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1738z00_4036,
						BgL_callerz00_3602, BgL_arg1739z00_4037, CBOOL(BgL_safez00_3604));
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_3601)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5269), BUNSPEC);
			}
			{	/* Coerce/coerce.scm 259 */
				BgL_typez00_bglt BgL_arg1746z00_4039;

				BgL_arg1746z00_4039 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_castz00_bglt) BgL_nodez00_3601))))->BgL_typez00);
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_castz00_bglt) BgL_nodez00_3601)), BgL_arg1746z00_4039,
					((BgL_typez00_bglt) BgL_toz00_3603), CBOOL(BgL_safez00_3604));
			}
		}

	}



/* &coerce!-vlength1321 */
	BgL_nodez00_bglt BGl_z62coercez12zd2vlength1321za2zzcoerce_coercez00(obj_t
		BgL_envz00_3605, obj_t BgL_nodez00_3606, obj_t BgL_callerz00_3607,
		obj_t BgL_toz00_3608, obj_t BgL_safez00_3609)
	{
		{	/* Coerce/coerce.scm 246 */
			{	/* Coerce/coerce.scm 248 */
				obj_t BgL_arg1724z00_4041;
				BgL_nodez00_bglt BgL_arg1733z00_4042;

				BgL_arg1724z00_4041 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vlengthz00_bglt) BgL_nodez00_3606))))->BgL_exprza2za2);
				{	/* Coerce/coerce.scm 248 */
					obj_t BgL_arg1734z00_4043;
					BgL_typez00_bglt BgL_arg1735z00_4044;

					{	/* Coerce/coerce.scm 248 */
						obj_t BgL_pairz00_4045;

						BgL_pairz00_4045 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vlengthz00_bglt) BgL_nodez00_3606))))->
							BgL_exprza2za2);
						BgL_arg1734z00_4043 = CAR(BgL_pairz00_4045);
					}
					BgL_arg1735z00_4044 =
						(((BgL_vlengthz00_bglt) COBJECT(
								((BgL_vlengthz00_bglt) BgL_nodez00_3606)))->BgL_vtypez00);
					BgL_arg1733z00_4042 =
						BGl_coercez12z12zzcoerce_coercez00(
						((BgL_nodez00_bglt) BgL_arg1734z00_4043), BgL_callerz00_3607,
						BgL_arg1735z00_4044, CBOOL(BgL_safez00_3609));
				}
				{	/* Coerce/coerce.scm 248 */
					obj_t BgL_auxz00_5301;
					obj_t BgL_tmpz00_5299;

					BgL_auxz00_5301 = ((obj_t) BgL_arg1733z00_4042);
					BgL_tmpz00_5299 = ((obj_t) BgL_arg1724z00_4041);
					SET_CAR(BgL_tmpz00_5299, BgL_auxz00_5301);
				}
			}
			{	/* Coerce/coerce.scm 249 */
				BgL_typez00_bglt BgL_arg1737z00_4046;

				BgL_arg1737z00_4046 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_vlengthz00_bglt) BgL_nodez00_3606))))->BgL_typez00);
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_vlengthz00_bglt) BgL_nodez00_3606)), BgL_arg1737z00_4046,
					((BgL_typez00_bglt) BgL_toz00_3608), CBOOL(BgL_safez00_3609));
			}
		}

	}



/* &coerce!-vset!1319 */
	BgL_nodez00_bglt BGl_z62coercez12zd2vsetz121319zb0zzcoerce_coercez00(obj_t
		BgL_envz00_3610, obj_t BgL_nodez00_3611, obj_t BgL_callerz00_3612,
		obj_t BgL_toz00_3613, obj_t BgL_safez00_3614)
	{
		{	/* Coerce/coerce.scm 234 */
			{	/* Coerce/coerce.scm 237 */
				obj_t BgL_ftypez00_4048;

				{	/* Coerce/coerce.scm 237 */
					bool_t BgL_test2305z00_5312;

					{	/* Coerce/coerce.scm 237 */
						BgL_typez00_bglt BgL_arg1722z00_4049;

						BgL_arg1722z00_4049 =
							(((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_3611)))->BgL_ftypez00);
						BgL_test2305z00_5312 =
							(((obj_t) BgL_arg1722z00_4049) == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test2305z00_5312)
						{	/* Coerce/coerce.scm 237 */
							BgL_ftypez00_4048 = BGl_za2objza2z00zztype_cachez00;
						}
					else
						{	/* Coerce/coerce.scm 237 */
							BgL_ftypez00_4048 =
								((obj_t)
								(((BgL_vsetz12z12_bglt) COBJECT(
											((BgL_vsetz12z12_bglt) BgL_nodez00_3611)))->
									BgL_ftypez00));
						}
				}
				{	/* Coerce/coerce.scm 238 */
					obj_t BgL_arg1688z00_4050;
					BgL_nodez00_bglt BgL_arg1689z00_4051;

					BgL_arg1688z00_4050 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vsetz12z12_bglt) BgL_nodez00_3611))))->BgL_exprza2za2);
					{	/* Coerce/coerce.scm 238 */
						obj_t BgL_arg1691z00_4052;
						BgL_typez00_bglt BgL_arg1692z00_4053;

						{	/* Coerce/coerce.scm 238 */
							obj_t BgL_pairz00_4054;

							BgL_pairz00_4054 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_vsetz12z12_bglt) BgL_nodez00_3611))))->
								BgL_exprza2za2);
							BgL_arg1691z00_4052 = CAR(BgL_pairz00_4054);
						}
						BgL_arg1692z00_4053 =
							(((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_3611)))->BgL_vtypez00);
						BgL_arg1689z00_4051 =
							BGl_coercez12z12zzcoerce_coercez00(
							((BgL_nodez00_bglt) BgL_arg1691z00_4052), BgL_callerz00_3612,
							BgL_arg1692z00_4053, CBOOL(BgL_safez00_3614));
					}
					{	/* Coerce/coerce.scm 238 */
						obj_t BgL_auxz00_5334;
						obj_t BgL_tmpz00_5332;

						BgL_auxz00_5334 = ((obj_t) BgL_arg1689z00_4051);
						BgL_tmpz00_5332 = ((obj_t) BgL_arg1688z00_4050);
						SET_CAR(BgL_tmpz00_5332, BgL_auxz00_5334);
					}
				}
				{	/* Coerce/coerce.scm 239 */
					obj_t BgL_arg1700z00_4055;
					BgL_nodez00_bglt BgL_arg1701z00_4056;

					{	/* Coerce/coerce.scm 239 */
						obj_t BgL_pairz00_4057;

						BgL_pairz00_4057 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_nodez00_3611))))->
							BgL_exprza2za2);
						BgL_arg1700z00_4055 = CDR(BgL_pairz00_4057);
					}
					{	/* Coerce/coerce.scm 239 */
						obj_t BgL_arg1703z00_4058;
						BgL_typez00_bglt BgL_arg1705z00_4059;

						{	/* Coerce/coerce.scm 239 */
							obj_t BgL_pairz00_4060;

							BgL_pairz00_4060 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_vsetz12z12_bglt) BgL_nodez00_3611))))->
								BgL_exprza2za2);
							BgL_arg1703z00_4058 = CAR(CDR(BgL_pairz00_4060));
						}
						BgL_arg1705z00_4059 =
							(((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_3611)))->BgL_otypez00);
						BgL_arg1701z00_4056 =
							BGl_coercez12z12zzcoerce_coercez00(
							((BgL_nodez00_bglt) BgL_arg1703z00_4058), BgL_callerz00_3612,
							BgL_arg1705z00_4059, CBOOL(BgL_safez00_3614));
					}
					{	/* Coerce/coerce.scm 239 */
						obj_t BgL_auxz00_5353;
						obj_t BgL_tmpz00_5351;

						BgL_auxz00_5353 = ((obj_t) BgL_arg1701z00_4056);
						BgL_tmpz00_5351 = ((obj_t) BgL_arg1700z00_4055);
						SET_CAR(BgL_tmpz00_5351, BgL_auxz00_5353);
					}
				}
				{	/* Coerce/coerce.scm 240 */
					obj_t BgL_arg1709z00_4061;
					BgL_nodez00_bglt BgL_arg1710z00_4062;

					{	/* Coerce/coerce.scm 240 */
						obj_t BgL_pairz00_4063;

						BgL_pairz00_4063 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_nodez00_3611))))->
							BgL_exprza2za2);
						BgL_arg1709z00_4061 = CDR(CDR(BgL_pairz00_4063));
					}
					{	/* Coerce/coerce.scm 240 */
						obj_t BgL_arg1714z00_4064;

						{	/* Coerce/coerce.scm 240 */
							obj_t BgL_pairz00_4065;

							BgL_pairz00_4065 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_vsetz12z12_bglt) BgL_nodez00_3611))))->
								BgL_exprza2za2);
							BgL_arg1714z00_4064 = CAR(CDR(CDR(BgL_pairz00_4065)));
						}
						BgL_arg1710z00_4062 =
							BGl_coercez12z12zzcoerce_coercez00(
							((BgL_nodez00_bglt) BgL_arg1714z00_4064), BgL_callerz00_3612,
							((BgL_typez00_bglt) BgL_ftypez00_4048), CBOOL(BgL_safez00_3614));
					}
					{	/* Coerce/coerce.scm 240 */
						obj_t BgL_auxz00_5373;
						obj_t BgL_tmpz00_5371;

						BgL_auxz00_5373 = ((obj_t) BgL_arg1710z00_4062);
						BgL_tmpz00_5371 = ((obj_t) BgL_arg1709z00_4061);
						SET_CAR(BgL_tmpz00_5371, BgL_auxz00_5373);
					}
				}
				{	/* Coerce/coerce.scm 241 */
					BgL_typez00_bglt BgL_arg1718z00_4066;

					BgL_arg1718z00_4066 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vsetz12z12_bglt) BgL_nodez00_3611))))->BgL_typez00);
					return
						BGl_convertz12z12zzcoerce_convertz00(
						((BgL_nodez00_bglt)
							((BgL_vsetz12z12_bglt) BgL_nodez00_3611)), BgL_arg1718z00_4066,
						((BgL_typez00_bglt) BgL_toz00_3613), CBOOL(BgL_safez00_3614));
				}
			}
		}

	}



/* &coerce!-vref1317 */
	BgL_nodez00_bglt BGl_z62coercez12zd2vref1317za2zzcoerce_coercez00(obj_t
		BgL_envz00_3615, obj_t BgL_nodez00_3616, obj_t BgL_callerz00_3617,
		obj_t BgL_toz00_3618, obj_t BgL_safez00_3619)
	{
		{	/* Coerce/coerce.scm 218 */
			{	/* Coerce/coerce.scm 226 */
				obj_t BgL_ftypez00_4068;

				{	/* Coerce/coerce.scm 226 */
					bool_t BgL_test2306z00_5384;

					{	/* Coerce/coerce.scm 226 */
						BgL_typez00_bglt BgL_arg1681z00_4069;

						BgL_arg1681z00_4069 =
							(((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_3616)))->BgL_ftypez00);
						BgL_test2306z00_5384 =
							(((obj_t) BgL_arg1681z00_4069) == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test2306z00_5384)
						{	/* Coerce/coerce.scm 226 */
							BgL_ftypez00_4068 = BGl_za2objza2z00zztype_cachez00;
						}
					else
						{	/* Coerce/coerce.scm 226 */
							BgL_ftypez00_4068 =
								((obj_t)
								(((BgL_vrefz00_bglt) COBJECT(
											((BgL_vrefz00_bglt) BgL_nodez00_3616)))->BgL_ftypez00));
						}
				}
				{	/* Coerce/coerce.scm 227 */
					obj_t BgL_arg1627z00_4070;
					BgL_nodez00_bglt BgL_arg1629z00_4071;

					BgL_arg1627z00_4070 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vrefz00_bglt) BgL_nodez00_3616))))->BgL_exprza2za2);
					{	/* Coerce/coerce.scm 227 */
						obj_t BgL_arg1630z00_4072;
						BgL_typez00_bglt BgL_arg1642z00_4073;

						{	/* Coerce/coerce.scm 227 */
							obj_t BgL_pairz00_4074;

							BgL_pairz00_4074 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_vrefz00_bglt) BgL_nodez00_3616))))->BgL_exprza2za2);
							BgL_arg1630z00_4072 = CAR(BgL_pairz00_4074);
						}
						BgL_arg1642z00_4073 =
							(((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_3616)))->BgL_vtypez00);
						BgL_arg1629z00_4071 =
							BGl_coercez12z12zzcoerce_coercez00(
							((BgL_nodez00_bglt) BgL_arg1630z00_4072), BgL_callerz00_3617,
							BgL_arg1642z00_4073, CBOOL(BgL_safez00_3619));
					}
					{	/* Coerce/coerce.scm 227 */
						obj_t BgL_auxz00_5406;
						obj_t BgL_tmpz00_5404;

						BgL_auxz00_5406 = ((obj_t) BgL_arg1629z00_4071);
						BgL_tmpz00_5404 = ((obj_t) BgL_arg1627z00_4070);
						SET_CAR(BgL_tmpz00_5404, BgL_auxz00_5406);
					}
				}
				{	/* Coerce/coerce.scm 228 */
					obj_t BgL_arg1650z00_4075;
					BgL_nodez00_bglt BgL_arg1651z00_4076;

					{	/* Coerce/coerce.scm 228 */
						obj_t BgL_pairz00_4077;

						BgL_pairz00_4077 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vrefz00_bglt) BgL_nodez00_3616))))->BgL_exprza2za2);
						BgL_arg1650z00_4075 = CDR(BgL_pairz00_4077);
					}
					{	/* Coerce/coerce.scm 228 */
						obj_t BgL_arg1661z00_4078;
						BgL_typez00_bglt BgL_arg1663z00_4079;

						{	/* Coerce/coerce.scm 228 */
							obj_t BgL_pairz00_4080;

							BgL_pairz00_4080 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_vrefz00_bglt) BgL_nodez00_3616))))->BgL_exprza2za2);
							BgL_arg1661z00_4078 = CAR(CDR(BgL_pairz00_4080));
						}
						BgL_arg1663z00_4079 =
							(((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_3616)))->BgL_otypez00);
						BgL_arg1651z00_4076 =
							BGl_coercez12z12zzcoerce_coercez00(
							((BgL_nodez00_bglt) BgL_arg1661z00_4078), BgL_callerz00_3617,
							BgL_arg1663z00_4079, CBOOL(BgL_safez00_3619));
					}
					{	/* Coerce/coerce.scm 228 */
						obj_t BgL_auxz00_5425;
						obj_t BgL_tmpz00_5423;

						BgL_auxz00_5425 = ((obj_t) BgL_arg1651z00_4076);
						BgL_tmpz00_5423 = ((obj_t) BgL_arg1650z00_4075);
						SET_CAR(BgL_tmpz00_5423, BgL_auxz00_5425);
					}
				}
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_vrefz00_bglt) BgL_nodez00_3616)),
					((BgL_typez00_bglt) BgL_ftypez00_4068),
					((BgL_typez00_bglt) BgL_toz00_3618), CBOOL(BgL_safez00_3619));
			}
		}

	}



/* &coerce!-valloc1315 */
	BgL_nodez00_bglt BGl_z62coercez12zd2valloc1315za2zzcoerce_coercez00(obj_t
		BgL_envz00_3620, obj_t BgL_nodez00_3621, obj_t BgL_callerz00_3622,
		obj_t BgL_toz00_3623, obj_t BgL_safez00_3624)
	{
		{	/* Coerce/coerce.scm 210 */
			{	/* Coerce/coerce.scm 212 */
				obj_t BgL_arg1611z00_4082;
				BgL_nodez00_bglt BgL_arg1613z00_4083;

				BgL_arg1611z00_4082 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_nodez00_3621))))->BgL_exprza2za2);
				{	/* Coerce/coerce.scm 212 */
					obj_t BgL_arg1615z00_4084;
					BgL_typez00_bglt BgL_arg1616z00_4085;

					{	/* Coerce/coerce.scm 212 */
						obj_t BgL_pairz00_4086;

						BgL_pairz00_4086 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_3621))))->BgL_exprza2za2);
						BgL_arg1615z00_4084 = CAR(BgL_pairz00_4086);
					}
					BgL_arg1616z00_4085 =
						(((BgL_vallocz00_bglt) COBJECT(
								((BgL_vallocz00_bglt) BgL_nodez00_3621)))->BgL_otypez00);
					BgL_arg1613z00_4083 =
						BGl_coercez12z12zzcoerce_coercez00(
						((BgL_nodez00_bglt) BgL_arg1615z00_4084), BgL_callerz00_3622,
						BgL_arg1616z00_4085, CBOOL(BgL_safez00_3624));
				}
				{	/* Coerce/coerce.scm 212 */
					obj_t BgL_auxz00_5448;
					obj_t BgL_tmpz00_5446;

					BgL_auxz00_5448 = ((obj_t) BgL_arg1613z00_4083);
					BgL_tmpz00_5446 = ((obj_t) BgL_arg1611z00_4082);
					SET_CAR(BgL_tmpz00_5446, BgL_auxz00_5448);
				}
			}
			{	/* Coerce/coerce.scm 213 */
				BgL_typez00_bglt BgL_arg1626z00_4087;

				BgL_arg1626z00_4087 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_vallocz00_bglt) BgL_nodez00_3621))))->BgL_typez00);
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_vallocz00_bglt) BgL_nodez00_3621)), BgL_arg1626z00_4087,
					((BgL_typez00_bglt) BgL_toz00_3623), CBOOL(BgL_safez00_3624));
			}
		}

	}



/* &coerce!-new1313 */
	BgL_nodez00_bglt BGl_z62coercez12zd2new1313za2zzcoerce_coercez00(obj_t
		BgL_envz00_3625, obj_t BgL_nodez00_3626, obj_t BgL_callerz00_3627,
		obj_t BgL_toz00_3628, obj_t BgL_safez00_3629)
	{
		{	/* Coerce/coerce.scm 196 */
			{
				obj_t BgL_lz00_4090;

				{	/* Coerce/coerce.scm 198 */
					obj_t BgL_arg1595z00_4096;

					BgL_arg1595z00_4096 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_newz00_bglt) BgL_nodez00_3626))))->BgL_exprza2za2);
					BgL_lz00_4090 = BgL_arg1595z00_4096;
				BgL_loopz00_4089:
					if (NULLP(BgL_lz00_4090))
						{	/* Coerce/coerce.scm 200 */
							BgL_typez00_bglt BgL_arg1602z00_4091;

							BgL_arg1602z00_4091 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_newz00_bglt) BgL_nodez00_3626))))->BgL_typez00);
							BGl_convertz12z12zzcoerce_convertz00(
								((BgL_nodez00_bglt)
									((BgL_newz00_bglt) BgL_nodez00_3626)), BgL_arg1602z00_4091,
								((BgL_typez00_bglt) BgL_toz00_3628), CBOOL(BgL_safez00_3629));
						}
					else
						{	/* Coerce/coerce.scm 201 */
							obj_t BgL_vz00_4092;

							BgL_vz00_4092 = CAR(((obj_t) BgL_lz00_4090));
							{	/* Coerce/coerce.scm 201 */
								BgL_nodez00_bglt BgL_nvz00_4093;

								{	/* Coerce/coerce.scm 202 */
									BgL_typez00_bglt BgL_arg1606z00_4094;

									BgL_arg1606z00_4094 =
										BGl_getzd2typezd2zztype_typeofz00(
										((BgL_nodez00_bglt) BgL_vz00_4092), ((bool_t) 0));
									BgL_nvz00_4093 =
										BGl_coercez12z12zzcoerce_coercez00(
										((BgL_nodez00_bglt) BgL_vz00_4092), BgL_callerz00_3627,
										BgL_arg1606z00_4094, CBOOL(BgL_safez00_3629));
								}
								{	/* Coerce/coerce.scm 202 */

									{	/* Coerce/coerce.scm 203 */
										obj_t BgL_auxz00_5481;
										obj_t BgL_tmpz00_5479;

										BgL_auxz00_5481 = ((obj_t) BgL_nvz00_4093);
										BgL_tmpz00_5479 = ((obj_t) BgL_lz00_4090);
										SET_CAR(BgL_tmpz00_5479, BgL_auxz00_5481);
									}
									{	/* Coerce/coerce.scm 204 */
										obj_t BgL_arg1605z00_4095;

										BgL_arg1605z00_4095 = CDR(((obj_t) BgL_lz00_4090));
										{
											obj_t BgL_lz00_5486;

											BgL_lz00_5486 = BgL_arg1605z00_4095;
											BgL_lz00_4090 = BgL_lz00_5486;
											goto BgL_loopz00_4089;
										}
									}
								}
							}
						}
				}
			}
			{	/* Coerce/coerce.scm 205 */
				BgL_typez00_bglt BgL_arg1609z00_4097;

				BgL_arg1609z00_4097 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_newz00_bglt) BgL_nodez00_3626))))->BgL_typez00);
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_newz00_bglt) BgL_nodez00_3626)), BgL_arg1609z00_4097,
					((BgL_typez00_bglt) BgL_toz00_3628), CBOOL(BgL_safez00_3629));
			}
		}

	}



/* &coerce!-setfield1311 */
	BgL_nodez00_bglt BGl_z62coercez12zd2setfield1311za2zzcoerce_coercez00(obj_t
		BgL_envz00_3630, obj_t BgL_nodez00_3631, obj_t BgL_callerz00_3632,
		obj_t BgL_toz00_3633, obj_t BgL_safez00_3634)
	{
		{	/* Coerce/coerce.scm 187 */
			{	/* Coerce/coerce.scm 189 */
				obj_t BgL_arg1565z00_4099;
				BgL_nodez00_bglt BgL_arg1571z00_4100;

				BgL_arg1565z00_4099 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_setfieldz00_bglt) BgL_nodez00_3631))))->BgL_exprza2za2);
				{	/* Coerce/coerce.scm 189 */
					obj_t BgL_arg1573z00_4101;
					BgL_typez00_bglt BgL_arg1575z00_4102;

					{	/* Coerce/coerce.scm 189 */
						obj_t BgL_pairz00_4103;

						BgL_pairz00_4103 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_setfieldz00_bglt) BgL_nodez00_3631))))->
							BgL_exprza2za2);
						BgL_arg1573z00_4101 = CAR(BgL_pairz00_4103);
					}
					BgL_arg1575z00_4102 =
						(((BgL_setfieldz00_bglt) COBJECT(
								((BgL_setfieldz00_bglt) BgL_nodez00_3631)))->BgL_otypez00);
					BgL_arg1571z00_4100 =
						BGl_coercez12z12zzcoerce_coercez00(
						((BgL_nodez00_bglt) BgL_arg1573z00_4101), BgL_callerz00_3632,
						BgL_arg1575z00_4102, CBOOL(BgL_safez00_3634));
				}
				{	/* Coerce/coerce.scm 189 */
					obj_t BgL_auxz00_5509;
					obj_t BgL_tmpz00_5507;

					BgL_auxz00_5509 = ((obj_t) BgL_arg1571z00_4100);
					BgL_tmpz00_5507 = ((obj_t) BgL_arg1565z00_4099);
					SET_CAR(BgL_tmpz00_5507, BgL_auxz00_5509);
				}
			}
			{	/* Coerce/coerce.scm 190 */
				obj_t BgL_arg1584z00_4104;
				BgL_nodez00_bglt BgL_arg1585z00_4105;

				{	/* Coerce/coerce.scm 190 */
					obj_t BgL_pairz00_4106;

					BgL_pairz00_4106 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_setfieldz00_bglt) BgL_nodez00_3631))))->BgL_exprza2za2);
					BgL_arg1584z00_4104 = CDR(BgL_pairz00_4106);
				}
				{	/* Coerce/coerce.scm 190 */
					obj_t BgL_arg1591z00_4107;
					BgL_typez00_bglt BgL_arg1593z00_4108;

					{	/* Coerce/coerce.scm 190 */
						obj_t BgL_pairz00_4109;

						BgL_pairz00_4109 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_setfieldz00_bglt) BgL_nodez00_3631))))->
							BgL_exprza2za2);
						BgL_arg1591z00_4107 = CAR(CDR(BgL_pairz00_4109));
					}
					BgL_arg1593z00_4108 =
						(((BgL_setfieldz00_bglt) COBJECT(
								((BgL_setfieldz00_bglt) BgL_nodez00_3631)))->BgL_ftypez00);
					BgL_arg1585z00_4105 =
						BGl_coercez12z12zzcoerce_coercez00(
						((BgL_nodez00_bglt) BgL_arg1591z00_4107), BgL_callerz00_3632,
						BgL_arg1593z00_4108, CBOOL(BgL_safez00_3634));
				}
				{	/* Coerce/coerce.scm 190 */
					obj_t BgL_auxz00_5528;
					obj_t BgL_tmpz00_5526;

					BgL_auxz00_5528 = ((obj_t) BgL_arg1585z00_4105);
					BgL_tmpz00_5526 = ((obj_t) BgL_arg1584z00_4104);
					SET_CAR(BgL_tmpz00_5526, BgL_auxz00_5528);
				}
			}
			return
				BGl_convertz12z12zzcoerce_convertz00(
				((BgL_nodez00_bglt)
					((BgL_setfieldz00_bglt) BgL_nodez00_3631)),
				((BgL_typez00_bglt) BGl_za2unspecza2z00zztype_cachez00),
				((BgL_typez00_bglt) BgL_toz00_3633), CBOOL(BgL_safez00_3634));
		}

	}



/* &coerce!-getfield1309 */
	BgL_nodez00_bglt BGl_z62coercez12zd2getfield1309za2zzcoerce_coercez00(obj_t
		BgL_envz00_3635, obj_t BgL_nodez00_3636, obj_t BgL_callerz00_3637,
		obj_t BgL_toz00_3638, obj_t BgL_safez00_3639)
	{
		{	/* Coerce/coerce.scm 179 */
			{	/* Coerce/coerce.scm 181 */
				obj_t BgL_arg1546z00_4111;
				BgL_nodez00_bglt BgL_arg1552z00_4112;

				BgL_arg1546z00_4111 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_getfieldz00_bglt) BgL_nodez00_3636))))->BgL_exprza2za2);
				{	/* Coerce/coerce.scm 181 */
					obj_t BgL_arg1553z00_4113;
					BgL_typez00_bglt BgL_arg1559z00_4114;

					{	/* Coerce/coerce.scm 181 */
						obj_t BgL_pairz00_4115;

						BgL_pairz00_4115 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_getfieldz00_bglt) BgL_nodez00_3636))))->
							BgL_exprza2za2);
						BgL_arg1553z00_4113 = CAR(BgL_pairz00_4115);
					}
					BgL_arg1559z00_4114 =
						(((BgL_getfieldz00_bglt) COBJECT(
								((BgL_getfieldz00_bglt) BgL_nodez00_3636)))->BgL_otypez00);
					BgL_arg1552z00_4112 =
						BGl_coercez12z12zzcoerce_coercez00(
						((BgL_nodez00_bglt) BgL_arg1553z00_4113), BgL_callerz00_3637,
						BgL_arg1559z00_4114, CBOOL(BgL_safez00_3639));
				}
				{	/* Coerce/coerce.scm 181 */
					obj_t BgL_auxz00_5551;
					obj_t BgL_tmpz00_5549;

					BgL_auxz00_5551 = ((obj_t) BgL_arg1552z00_4112);
					BgL_tmpz00_5549 = ((obj_t) BgL_arg1546z00_4111);
					SET_CAR(BgL_tmpz00_5549, BgL_auxz00_5551);
				}
			}
			{	/* Coerce/coerce.scm 182 */
				BgL_typez00_bglt BgL_arg1564z00_4116;

				BgL_arg1564z00_4116 =
					(((BgL_getfieldz00_bglt) COBJECT(
							((BgL_getfieldz00_bglt) BgL_nodez00_3636)))->BgL_ftypez00);
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_getfieldz00_bglt) BgL_nodez00_3636)), BgL_arg1564z00_4116,
					((BgL_typez00_bglt) BgL_toz00_3638), CBOOL(BgL_safez00_3639));
			}
		}

	}



/* &coerce!-widening1307 */
	BgL_nodez00_bglt BGl_z62coercez12zd2widening1307za2zzcoerce_coercez00(obj_t
		BgL_envz00_3640, obj_t BgL_nodez00_3641, obj_t BgL_callerz00_3642,
		obj_t BgL_toz00_3643, obj_t BgL_safez00_3644)
	{
		{	/* Coerce/coerce.scm 168 */
			{	/* Coerce/coerce.scm 172 */
				obj_t BgL_superz00_4118;

				{	/* Coerce/coerce.scm 172 */
					BgL_typez00_bglt BgL_oz00_4119;

					BgL_oz00_4119 =
						((BgL_typez00_bglt)
						(((BgL_wideningz00_bglt) COBJECT(
									((BgL_wideningz00_bglt) BgL_nodez00_3641)))->BgL_otypez00));
					{
						BgL_tclassz00_bglt BgL_auxz00_5564;

						{
							obj_t BgL_auxz00_5565;

							{	/* Coerce/coerce.scm 172 */
								BgL_objectz00_bglt BgL_tmpz00_5566;

								BgL_tmpz00_5566 = ((BgL_objectz00_bglt) BgL_oz00_4119);
								BgL_auxz00_5565 = BGL_OBJECT_WIDENING(BgL_tmpz00_5566);
							}
							BgL_auxz00_5564 = ((BgL_tclassz00_bglt) BgL_auxz00_5565);
						}
						BgL_superz00_4118 =
							(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_5564))->
							BgL_itszd2superzd2);
					}
				}
				{	/* Coerce/coerce.scm 173 */
					obj_t BgL_arg1514z00_4120;
					BgL_nodez00_bglt BgL_arg1516z00_4121;

					BgL_arg1514z00_4120 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_wideningz00_bglt) BgL_nodez00_3641))))->BgL_exprza2za2);
					{	/* Coerce/coerce.scm 173 */
						obj_t BgL_arg1535z00_4122;

						{	/* Coerce/coerce.scm 173 */
							obj_t BgL_pairz00_4123;

							BgL_pairz00_4123 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_wideningz00_bglt) BgL_nodez00_3641))))->
								BgL_exprza2za2);
							BgL_arg1535z00_4122 = CAR(BgL_pairz00_4123);
						}
						BgL_arg1516z00_4121 =
							BGl_coercez12z12zzcoerce_coercez00(
							((BgL_nodez00_bglt) BgL_arg1535z00_4122), BgL_callerz00_3642,
							((BgL_typez00_bglt) BgL_superz00_4118), CBOOL(BgL_safez00_3644));
					}
					{	/* Coerce/coerce.scm 173 */
						obj_t BgL_auxz00_5584;
						obj_t BgL_tmpz00_5582;

						BgL_auxz00_5584 = ((obj_t) BgL_arg1516z00_4121);
						BgL_tmpz00_5582 = ((obj_t) BgL_arg1514z00_4120);
						SET_CAR(BgL_tmpz00_5582, BgL_auxz00_5584);
					}
				}
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_wideningz00_bglt) BgL_nodez00_3641)),
					((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
					((BgL_typez00_bglt) BgL_toz00_3643), CBOOL(BgL_safez00_3644));
			}
		}

	}



/* &coerce!-extern1305 */
	BgL_nodez00_bglt BGl_z62coercez12zd2extern1305za2zzcoerce_coercez00(obj_t
		BgL_envz00_3645, obj_t BgL_nodez00_3646, obj_t BgL_callerz00_3647,
		obj_t BgL_toz00_3648, obj_t BgL_safez00_3649)
	{
		{	/* Coerce/coerce.scm 155 */
			{
				obj_t BgL_valuesz00_4126;

				BgL_valuesz00_4126 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_3646)))->BgL_exprza2za2);
			BgL_loopz00_4125:
				if (NULLP(BgL_valuesz00_4126))
					{	/* Coerce/coerce.scm 159 */
						BgL_typez00_bglt BgL_arg1502z00_4127;

						BgL_arg1502z00_4127 =
							BGl_getzd2typezd2zztype_typeofz00(
							((BgL_nodez00_bglt)
								((BgL_externz00_bglt) BgL_nodez00_3646)), ((bool_t) 0));
						return
							BGl_convertz12z12zzcoerce_convertz00(
							((BgL_nodez00_bglt)
								((BgL_externz00_bglt) BgL_nodez00_3646)), BgL_arg1502z00_4127,
							((BgL_typez00_bglt) BgL_toz00_3648), CBOOL(BgL_safez00_3649));
					}
				else
					{	/* Coerce/coerce.scm 160 */
						obj_t BgL_vz00_4128;

						BgL_vz00_4128 = CAR(((obj_t) BgL_valuesz00_4126));
						{	/* Coerce/coerce.scm 160 */
							BgL_nodez00_bglt BgL_nvz00_4129;

							{	/* Coerce/coerce.scm 161 */
								BgL_typez00_bglt BgL_arg1513z00_4130;

								BgL_arg1513z00_4130 =
									BGl_getzd2typezd2zztype_typeofz00(
									((BgL_nodez00_bglt) BgL_vz00_4128), ((bool_t) 0));
								BgL_nvz00_4129 =
									BGl_coercez12z12zzcoerce_coercez00(
									((BgL_nodez00_bglt) BgL_vz00_4128), BgL_callerz00_3647,
									BgL_arg1513z00_4130, CBOOL(BgL_safez00_3649));
							}
							{	/* Coerce/coerce.scm 161 */

								{	/* Coerce/coerce.scm 162 */
									obj_t BgL_auxz00_5612;
									obj_t BgL_tmpz00_5610;

									BgL_auxz00_5612 = ((obj_t) BgL_nvz00_4129);
									BgL_tmpz00_5610 = ((obj_t) BgL_valuesz00_4126);
									SET_CAR(BgL_tmpz00_5610, BgL_auxz00_5612);
								}
								{	/* Coerce/coerce.scm 163 */
									obj_t BgL_arg1509z00_4131;

									BgL_arg1509z00_4131 = CDR(((obj_t) BgL_valuesz00_4126));
									{
										obj_t BgL_valuesz00_5617;

										BgL_valuesz00_5617 = BgL_arg1509z00_4131;
										BgL_valuesz00_4126 = BgL_valuesz00_5617;
										goto BgL_loopz00_4125;
									}
								}
							}
						}
					}
			}
		}

	}



/* &coerce!-sync1303 */
	BgL_nodez00_bglt BGl_z62coercez12zd2sync1303za2zzcoerce_coercez00(obj_t
		BgL_envz00_3650, obj_t BgL_nodez00_3651, obj_t BgL_callerz00_3652,
		obj_t BgL_toz00_3653, obj_t BgL_safez00_3654)
	{
		{	/* Coerce/coerce.scm 144 */
			{
				BgL_nodez00_bglt BgL_auxz00_5620;

				{	/* Coerce/coerce.scm 146 */
					BgL_nodez00_bglt BgL_arg1454z00_4133;

					BgL_arg1454z00_4133 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3651)))->BgL_mutexz00);
					BgL_auxz00_5620 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1454z00_4133,
						BgL_callerz00_3652,
						((BgL_typez00_bglt) BGl_za2mutexza2z00zztype_cachez00),
						CBOOL(BgL_safez00_3654));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3651)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5620), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5628;

				{	/* Coerce/coerce.scm 147 */
					BgL_nodez00_bglt BgL_arg1472z00_4134;

					BgL_arg1472z00_4134 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3651)))->BgL_prelockz00);
					BgL_auxz00_5628 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1472z00_4134,
						BgL_callerz00_3652,
						((BgL_typez00_bglt) BGl_za2pairzd2nilza2zd2zztype_cachez00),
						CBOOL(BgL_safez00_3654));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3651)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5628), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_5636;

				{	/* Coerce/coerce.scm 148 */
					BgL_nodez00_bglt BgL_arg1473z00_4135;

					BgL_arg1473z00_4135 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3651)))->BgL_bodyz00);
					BgL_auxz00_5636 =
						BGl_coercez12z12zzcoerce_coercez00(BgL_arg1473z00_4135,
						BgL_callerz00_3652, ((BgL_typez00_bglt) BgL_toz00_3653),
						CBOOL(BgL_safez00_3654));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3651)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_5636), BUNSPEC);
			}
			{
				BgL_typez00_bglt BgL_auxz00_5644;

				{	/* Coerce/coerce.scm 149 */
					BgL_typez00_bglt BgL_arg1485z00_4136;

					BgL_arg1485z00_4136 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_3651))))->BgL_typez00);
					BgL_auxz00_5644 =
						BGl_strictzd2nodezd2typez00zzast_nodez00(
						((BgL_typez00_bglt) BgL_toz00_3653), BgL_arg1485z00_4136);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_3651))))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_auxz00_5644), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_3651));
		}

	}



/* &coerce!-sequence1301 */
	BgL_nodez00_bglt BGl_z62coercez12zd2sequence1301za2zzcoerce_coercez00(obj_t
		BgL_envz00_3655, obj_t BgL_nodez00_3656, obj_t BgL_callerz00_3657,
		obj_t BgL_toz00_3658, obj_t BgL_safez00_3659)
	{
		{	/* Coerce/coerce.scm 127 */
			{	/* Coerce/coerce.scm 129 */
				obj_t BgL_sz00_4138;

				if (
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_3656)))->BgL_unsafez00))
					{	/* Coerce/coerce.scm 129 */
						BgL_sz00_4138 = BFALSE;
					}
				else
					{	/* Coerce/coerce.scm 129 */
						BgL_sz00_4138 = BgL_safez00_3659;
					}
				{
					obj_t BgL_nodesz00_4140;

					{	/* Coerce/coerce.scm 130 */
						obj_t BgL_arg1380z00_4147;

						BgL_arg1380z00_4147 =
							(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nodez00_3656)))->BgL_nodesz00);
						{
							BgL_sequencez00_bglt BgL_auxz00_5660;

							BgL_nodesz00_4140 = BgL_arg1380z00_4147;
						BgL_loopz00_4139:
							{	/* Coerce/coerce.scm 131 */
								obj_t BgL_nz00_4141;

								BgL_nz00_4141 = CAR(((obj_t) BgL_nodesz00_4140));
								if (NULLP(CDR(((obj_t) BgL_nodesz00_4140))))
									{	/* Coerce/coerce.scm 132 */
										{	/* Coerce/coerce.scm 134 */
											BgL_nodez00_bglt BgL_arg1421z00_4142;

											BgL_arg1421z00_4142 =
												BGl_coercez12z12zzcoerce_coercez00(
												((BgL_nodez00_bglt) BgL_nz00_4141), BgL_callerz00_3657,
												((BgL_typez00_bglt) BgL_toz00_3658),
												CBOOL(BgL_sz00_4138));
											{	/* Coerce/coerce.scm 134 */
												obj_t BgL_auxz00_5673;
												obj_t BgL_tmpz00_5671;

												BgL_auxz00_5673 = ((obj_t) BgL_arg1421z00_4142);
												BgL_tmpz00_5671 = ((obj_t) BgL_nodesz00_4140);
												SET_CAR(BgL_tmpz00_5671, BgL_auxz00_5673);
											}
										}
										{
											BgL_typez00_bglt BgL_auxz00_5676;

											{	/* Coerce/coerce.scm 135 */
												BgL_typez00_bglt BgL_arg1422z00_4143;

												BgL_arg1422z00_4143 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_sequencez00_bglt) BgL_nodez00_3656))))->
													BgL_typez00);
												BgL_auxz00_5676 =
													BGl_strictzd2nodezd2typez00zzast_nodez00((
														(BgL_typez00_bglt) BgL_toz00_3658),
													BgL_arg1422z00_4143);
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_sequencez00_bglt) BgL_nodez00_3656))))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_auxz00_5676), BUNSPEC);
										}
										BgL_auxz00_5660 = ((BgL_sequencez00_bglt) BgL_nodez00_3656);
									}
								else
									{	/* Coerce/coerce.scm 132 */
										{	/* Coerce/coerce.scm 138 */
											BgL_nodez00_bglt BgL_arg1434z00_4144;

											{	/* Coerce/coerce.scm 138 */
												BgL_typez00_bglt BgL_arg1437z00_4145;

												BgL_arg1437z00_4145 =
													BGl_getzd2typezd2zztype_typeofz00(
													((BgL_nodez00_bglt) BgL_nz00_4141), ((bool_t) 0));
												BgL_arg1434z00_4144 =
													BGl_coercez12z12zzcoerce_coercez00(
													((BgL_nodez00_bglt) BgL_nz00_4141),
													BgL_callerz00_3657, BgL_arg1437z00_4145,
													CBOOL(BgL_sz00_4138));
											}
											{	/* Coerce/coerce.scm 138 */
												obj_t BgL_auxz00_5693;
												obj_t BgL_tmpz00_5691;

												BgL_auxz00_5693 = ((obj_t) BgL_arg1434z00_4144);
												BgL_tmpz00_5691 = ((obj_t) BgL_nodesz00_4140);
												SET_CAR(BgL_tmpz00_5691, BgL_auxz00_5693);
											}
										}
										{	/* Coerce/coerce.scm 139 */
											obj_t BgL_arg1448z00_4146;

											BgL_arg1448z00_4146 = CDR(((obj_t) BgL_nodesz00_4140));
											{
												obj_t BgL_nodesz00_5698;

												BgL_nodesz00_5698 = BgL_arg1448z00_4146;
												BgL_nodesz00_4140 = BgL_nodesz00_5698;
												goto BgL_loopz00_4139;
											}
										}
									}
							}
							return ((BgL_nodez00_bglt) BgL_auxz00_5660);
						}
					}
				}
			}
		}

	}



/* &coerce!-closure1299 */
	BgL_nodez00_bglt BGl_z62coercez12zd2closure1299za2zzcoerce_coercez00(obj_t
		BgL_envz00_3660, obj_t BgL_nodez00_3661, obj_t BgL_callerz00_3662,
		obj_t BgL_toz00_3663, obj_t BgL_safez00_3664)
	{
		{	/* Coerce/coerce.scm 121 */
			{	/* Coerce/coerce.scm 122 */
				obj_t BgL_arg1379z00_4149;

				BgL_arg1379z00_4149 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_3661)));
				return
					((BgL_nodez00_bglt)
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2107z00zzcoerce_coercez00,
						BGl_string2139z00zzcoerce_coercez00, BgL_arg1379z00_4149));
			}
		}

	}



/* &coerce!-var1297 */
	BgL_nodez00_bglt BGl_z62coercez12zd2var1297za2zzcoerce_coercez00(obj_t
		BgL_envz00_3665, obj_t BgL_nodez00_3666, obj_t BgL_callerz00_3667,
		obj_t BgL_toz00_3668, obj_t BgL_safez00_3669)
	{
		{	/* Coerce/coerce.scm 106 */
			{	/* Coerce/coerce.scm 108 */
				BgL_typez00_bglt BgL_ntypez00_4151;
				BgL_typez00_bglt BgL_vtypez00_4152;

				BgL_ntypez00_4151 =
					BGl_getzd2typezd2zztype_typeofz00(
					((BgL_nodez00_bglt)
						((BgL_varz00_bglt) BgL_nodez00_3666)), ((bool_t) 0));
				BgL_vtypez00_4152 =
					(((BgL_variablez00_bglt) COBJECT(
							(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_3666)))->BgL_variablez00)))->
					BgL_typez00);
				if ((((obj_t) BgL_vtypez00_4152) == BgL_toz00_3668))
					{	/* Coerce/coerce.scm 111 */
						return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_3666));
					}
				else
					{	/* Coerce/coerce.scm 111 */
						if ((((obj_t) BgL_ntypez00_4151) == ((obj_t) BgL_vtypez00_4152)))
							{	/* Coerce/coerce.scm 113 */
								return
									BGl_convertz12z12zzcoerce_convertz00(
									((BgL_nodez00_bglt)
										((BgL_varz00_bglt) BgL_nodez00_3666)), BgL_ntypez00_4151,
									((BgL_typez00_bglt) BgL_toz00_3668), CBOOL(BgL_safez00_3669));
							}
						else
							{	/* Coerce/coerce.scm 116 */
								BgL_nodez00_bglt BgL_arg1377z00_4153;

								BgL_arg1377z00_4153 =
									BGl_convertz12z12zzcoerce_convertz00(
									((BgL_nodez00_bglt)
										((BgL_varz00_bglt) BgL_nodez00_3666)), BgL_vtypez00_4152,
									BgL_ntypez00_4151, ((bool_t) 0));
								return BGl_convertz12z12zzcoerce_convertz00(BgL_arg1377z00_4153,
									BgL_ntypez00_4151, ((BgL_typez00_bglt) BgL_toz00_3668),
									CBOOL(BgL_safez00_3669));
							}
					}
			}
		}

	}



/* &coerce!-kwote1295 */
	BgL_nodez00_bglt BGl_z62coercez12zd2kwote1295za2zzcoerce_coercez00(obj_t
		BgL_envz00_3670, obj_t BgL_nodez00_3671, obj_t BgL_callerz00_3672,
		obj_t BgL_toz00_3673, obj_t BgL_safez00_3674)
	{
		{	/* Coerce/coerce.scm 91 */
			{	/* Coerce/coerce.scm 92 */
				BgL_typez00_bglt BgL_arg1376z00_4155;

				BgL_arg1376z00_4155 =
					BGl_getzd2typezd2zztype_typeofz00(
					((BgL_nodez00_bglt)
						((BgL_kwotez00_bglt) BgL_nodez00_3671)), ((bool_t) 0));
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_kwotez00_bglt) BgL_nodez00_3671)), BgL_arg1376z00_4155,
					((BgL_typez00_bglt) BgL_toz00_3673), CBOOL(BgL_safez00_3674));
			}
		}

	}



/* &coerce!-atom1293 */
	BgL_nodez00_bglt BGl_z62coercez12zd2atom1293za2zzcoerce_coercez00(obj_t
		BgL_envz00_3675, obj_t BgL_nodez00_3676, obj_t BgL_callerz00_3677,
		obj_t BgL_toz00_3678, obj_t BgL_safez00_3679)
	{
		{	/* Coerce/coerce.scm 85 */
			{	/* Coerce/coerce.scm 86 */
				BgL_typez00_bglt BgL_arg1375z00_4157;

				BgL_arg1375z00_4157 =
					BGl_getzd2typezd2zztype_typeofz00(
					((BgL_nodez00_bglt)
						((BgL_atomz00_bglt) BgL_nodez00_3676)), ((bool_t) 0));
				return
					BGl_convertz12z12zzcoerce_convertz00(
					((BgL_nodez00_bglt)
						((BgL_atomz00_bglt) BgL_nodez00_3676)), BgL_arg1375z00_4157,
					((BgL_typez00_bglt) BgL_toz00_3678), CBOOL(BgL_safez00_3679));
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcoerce_coercez00(void)
	{
		{	/* Coerce/coerce.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzcoerce_pprotoz00(44915300L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzcoerce_convertz00(87995645L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzcoerce_appz00(419328946L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzcoerce_applyz00(435902431L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			BGl_modulezd2initializa7ationz75zzcoerce_funcallz00(40876382L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
			return
				BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string2140z00zzcoerce_coercez00));
		}

	}

#ifdef __cplusplus
}
#endif
