/*===========================================================================*/
/*   (Coerce/funcall.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Coerce/funcall.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_COERCE_FUNCALL_TYPE_DEFINITIONS
#define BGL_COERCE_FUNCALL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;


#endif													// BGL_COERCE_FUNCALL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2unsafezd2arityza2zd2zzengine_paramz00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcoerce_funcallz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzcoerce_funcallz00(void);
	static BgL_nodez00_bglt
		BGl_z62coercez12zd2funcall1237za2zzcoerce_funcallz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzcoerce_funcallz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcoerce_funcallz00(void);
	static obj_t
		BGl_coercezd2funcallzd2elightzd2argsz12zc0zzcoerce_funcallz00
		(BgL_funcallz00_bglt, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static BgL_nodez00_bglt
		BGl_makezd2arityzd2errorzd2nodezd2zzcoerce_funcallz00(BgL_localz00_bglt,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcoerce_funcallz00(void);
	extern BgL_nodez00_bglt
		BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcoerce_funcallz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_convertz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzcoerce_funcallz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_funcallz00(void);
	static obj_t
		BGl_coercezd2funcallzd2argsz12z12zzcoerce_funcallz00(BgL_funcallz00_bglt,
		obj_t, obj_t, obj_t);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzcoerce_funcallz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcoerce_funcallz00(void);
	extern obj_t BGl_currentzd2functionzd2zztools_errorz00(void);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	extern BgL_nodez00_bglt BGl_convertz12z12zzcoerce_convertz00(BgL_nodez00_bglt,
		BgL_typez00_bglt, BgL_typez00_bglt, bool_t);
	static BgL_nodez00_bglt
		BGl_toplevelzd2expze70z35zzcoerce_funcallz00(BgL_funcallz00_bglt);
	static BgL_nodez00_bglt
		BGl_toplevelzd2expze71z35zzcoerce_funcallz00(BgL_funcallz00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t __cnst[19];


	   
		 
		DEFINE_STRING(BGl_string1813z00zzcoerce_funcallz00,
		BgL_bgl_string1813za700za7za7c1821za7, ":Wrong number of arguments", 26);
	      DEFINE_STRING(BGl_string1815z00zzcoerce_funcallz00,
		BgL_bgl_string1815za700za7za7c1822za7, "coerce!", 7);
	      DEFINE_STRING(BGl_string1816z00zzcoerce_funcallz00,
		BgL_bgl_string1816za700za7za7c1823za7, "coerce_funcall", 14);
	      DEFINE_STRING(BGl_string1817z00zzcoerce_funcallz00,
		BgL_bgl_string1817za700za7za7c1824za7,
		"let if correct-arity? ::int len fun light elight __eoa__ dummy begin failure quote _ @ error/location __error location bdb ",
		123);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1814z00zzcoerce_funcallz00,
		BgL_bgl_za762coerceza712za7d2f1825za7,
		BGl_z62coercez12zd2funcall1237za2zzcoerce_funcallz00, 0L, BUNSPEC, 4);
	extern obj_t BGl_coercez12zd2envzc0zzcoerce_coercez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcoerce_funcallz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzcoerce_funcallz00(long
		BgL_checksumz00_1975, char *BgL_fromz00_1976)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcoerce_funcallz00))
				{
					BGl_requirezd2initializa7ationz75zzcoerce_funcallz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcoerce_funcallz00();
					BGl_libraryzd2moduleszd2initz00zzcoerce_funcallz00();
					BGl_cnstzd2initzd2zzcoerce_funcallz00();
					BGl_importedzd2moduleszd2initz00zzcoerce_funcallz00();
					BGl_methodzd2initzd2zzcoerce_funcallz00();
					return BGl_toplevelzd2initzd2zzcoerce_funcallz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_funcallz00(void)
	{
		{	/* Coerce/funcall.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "coerce_funcall");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "coerce_funcall");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcoerce_funcallz00(void)
	{
		{	/* Coerce/funcall.scm 15 */
			{	/* Coerce/funcall.scm 15 */
				obj_t BgL_cportz00_1914;

				{	/* Coerce/funcall.scm 15 */
					obj_t BgL_stringz00_1921;

					BgL_stringz00_1921 = BGl_string1817z00zzcoerce_funcallz00;
					{	/* Coerce/funcall.scm 15 */
						obj_t BgL_startz00_1922;

						BgL_startz00_1922 = BINT(0L);
						{	/* Coerce/funcall.scm 15 */
							obj_t BgL_endz00_1923;

							BgL_endz00_1923 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1921)));
							{	/* Coerce/funcall.scm 15 */

								BgL_cportz00_1914 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1921, BgL_startz00_1922, BgL_endz00_1923);
				}}}}
				{
					long BgL_iz00_1915;

					BgL_iz00_1915 = 18L;
				BgL_loopz00_1916:
					if ((BgL_iz00_1915 == -1L))
						{	/* Coerce/funcall.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Coerce/funcall.scm 15 */
							{	/* Coerce/funcall.scm 15 */
								obj_t BgL_arg1820z00_1917;

								{	/* Coerce/funcall.scm 15 */

									{	/* Coerce/funcall.scm 15 */
										obj_t BgL_locationz00_1919;

										BgL_locationz00_1919 = BBOOL(((bool_t) 0));
										{	/* Coerce/funcall.scm 15 */

											BgL_arg1820z00_1917 =
												BGl_readz00zz__readerz00(BgL_cportz00_1914,
												BgL_locationz00_1919);
										}
									}
								}
								{	/* Coerce/funcall.scm 15 */
									int BgL_tmpz00_2005;

									BgL_tmpz00_2005 = (int) (BgL_iz00_1915);
									CNST_TABLE_SET(BgL_tmpz00_2005, BgL_arg1820z00_1917);
							}}
							{	/* Coerce/funcall.scm 15 */
								int BgL_auxz00_1920;

								BgL_auxz00_1920 = (int) ((BgL_iz00_1915 - 1L));
								{
									long BgL_iz00_2010;

									BgL_iz00_2010 = (long) (BgL_auxz00_1920);
									BgL_iz00_1915 = BgL_iz00_2010;
									goto BgL_loopz00_1916;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcoerce_funcallz00(void)
	{
		{	/* Coerce/funcall.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcoerce_funcallz00(void)
	{
		{	/* Coerce/funcall.scm 15 */
			return BUNSPEC;
		}

	}



/* make-arity-error-node */
	BgL_nodez00_bglt
		BGl_makezd2arityzd2errorzd2nodezd2zzcoerce_funcallz00(BgL_localz00_bglt
		BgL_funz00_21, obj_t BgL_errorzd2msgzd2_22, obj_t BgL_locz00_23,
		obj_t BgL_callerz00_24, obj_t BgL_toz00_25)
	{
		{	/* Coerce/funcall.scm 102 */
			{	/* Coerce/funcall.scm 103 */
				BgL_nodez00_bglt BgL_nodez00_1496;

				{	/* Coerce/funcall.scm 104 */
					obj_t BgL_arg1284z00_1497;

					{	/* Coerce/funcall.scm 104 */
						bool_t BgL_test1828z00_2013;

						{	/* Coerce/funcall.scm 104 */
							bool_t BgL_test1829z00_2014;

							if (
								((long) CINT(BGl_za2compilerzd2debugza2zd2zzengine_paramz00) >
									0L))
								{	/* Coerce/funcall.scm 104 */
									BgL_test1829z00_2014 = ((bool_t) 1);
								}
							else
								{	/* Coerce/funcall.scm 104 */
									if (
										((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >
											0L))
										{	/* Coerce/funcall.scm 108 */
											obj_t BgL_arg1370z00_1543;

											{	/* Coerce/funcall.scm 108 */
												obj_t BgL_arg1371z00_1544;

												BgL_arg1371z00_1544 =
													BGl_thezd2backendzd2zzbackend_backendz00();
												BgL_arg1370z00_1543 =
													(((BgL_backendz00_bglt) COBJECT(
															((BgL_backendz00_bglt) BgL_arg1371z00_1544)))->
													BgL_debugzd2supportzd2);
											}
											BgL_test1829z00_2014 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(CNST_TABLE_REF(0), BgL_arg1370z00_1543));
										}
									else
										{	/* Coerce/funcall.scm 105 */
											BgL_test1829z00_2014 = ((bool_t) 0);
										}
								}
							if (BgL_test1829z00_2014)
								{	/* Coerce/funcall.scm 104 */
									if (STRUCTP(BgL_locz00_23))
										{	/* Coerce/funcall.scm 109 */
											BgL_test1828z00_2013 =
												(STRUCT_KEY(BgL_locz00_23) == CNST_TABLE_REF(1));
										}
									else
										{	/* Coerce/funcall.scm 109 */
											BgL_test1828z00_2013 = ((bool_t) 0);
										}
								}
							else
								{	/* Coerce/funcall.scm 104 */
									BgL_test1828z00_2013 = ((bool_t) 0);
								}
						}
						if (BgL_test1828z00_2013)
							{	/* Coerce/funcall.scm 111 */
								obj_t BgL_arg1312z00_1508;

								{	/* Coerce/funcall.scm 111 */
									obj_t BgL_arg1314z00_1509;
									obj_t BgL_arg1315z00_1510;

									{	/* Coerce/funcall.scm 111 */
										obj_t BgL_arg1316z00_1511;
										obj_t BgL_arg1317z00_1512;

										{	/* Coerce/funcall.scm 111 */
											obj_t BgL_arg1318z00_1513;

											{	/* Coerce/funcall.scm 111 */
												obj_t BgL_arg1319z00_1514;

												BgL_arg1319z00_1514 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BNIL);
												BgL_arg1318z00_1513 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
													BgL_arg1319z00_1514);
											}
											BgL_arg1316z00_1511 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1318z00_1513);
										}
										{	/* Coerce/funcall.scm 113 */
											obj_t BgL_arg1320z00_1515;
											obj_t BgL_arg1321z00_1516;

											{	/* Coerce/funcall.scm 113 */
												obj_t BgL_arg1322z00_1517;

												{	/* Coerce/funcall.scm 113 */
													obj_t BgL_arg1323z00_1518;

													BgL_arg1323z00_1518 =
														BGl_currentzd2functionzd2zztools_errorz00();
													{	/* Coerce/funcall.scm 112 */
														obj_t BgL_arg1455z00_1838;

														BgL_arg1455z00_1838 =
															SYMBOL_TO_STRING(((obj_t) BgL_arg1323z00_1518));
														BgL_arg1322z00_1517 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1838);
													}
												}
												BgL_arg1320z00_1515 =
													string_append(BgL_arg1322z00_1517,
													BGl_string1813z00zzcoerce_funcallz00);
											}
											{	/* Coerce/funcall.scm 117 */
												obj_t BgL_arg1325z00_1519;

												{	/* Coerce/funcall.scm 117 */
													obj_t BgL_arg1326z00_1520;

													{	/* Coerce/funcall.scm 117 */
														obj_t BgL_arg1327z00_1521;
														obj_t BgL_arg1328z00_1522;

														BgL_arg1327z00_1521 =
															BGl_locationzd2fullzd2fnamez00zztools_locationz00
															(BgL_locz00_23);
														BgL_arg1328z00_1522 =
															MAKE_YOUNG_PAIR(STRUCT_REF(BgL_locz00_23,
																(int) (1L)), BNIL);
														BgL_arg1326z00_1520 =
															MAKE_YOUNG_PAIR(BgL_arg1327z00_1521,
															BgL_arg1328z00_1522);
													}
													BgL_arg1325z00_1519 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_funz00_21), BgL_arg1326z00_1520);
												}
												BgL_arg1321z00_1516 =
													MAKE_YOUNG_PAIR(BgL_errorzd2msgzd2_22,
													BgL_arg1325z00_1519);
											}
											BgL_arg1317z00_1512 =
												MAKE_YOUNG_PAIR(BgL_arg1320z00_1515,
												BgL_arg1321z00_1516);
										}
										BgL_arg1314z00_1509 =
											MAKE_YOUNG_PAIR(BgL_arg1316z00_1511, BgL_arg1317z00_1512);
									}
									{	/* Coerce/funcall.scm 119 */
										obj_t BgL_arg1331z00_1524;

										{	/* Coerce/funcall.scm 119 */
											obj_t BgL_arg1332z00_1525;

											{	/* Coerce/funcall.scm 119 */
												obj_t BgL_arg1333z00_1526;
												obj_t BgL_arg1335z00_1527;

												{	/* Coerce/funcall.scm 119 */
													obj_t BgL_arg1339z00_1528;

													BgL_arg1339z00_1528 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BNIL);
													BgL_arg1333z00_1526 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
														BgL_arg1339z00_1528);
												}
												{	/* Coerce/funcall.scm 119 */
													obj_t BgL_arg1340z00_1529;
													obj_t BgL_arg1342z00_1530;

													{	/* Coerce/funcall.scm 119 */
														obj_t BgL_arg1343z00_1531;

														BgL_arg1343z00_1531 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BNIL);
														BgL_arg1340z00_1529 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
															BgL_arg1343z00_1531);
													}
													{	/* Coerce/funcall.scm 119 */
														obj_t BgL_arg1346z00_1532;

														{	/* Coerce/funcall.scm 119 */
															obj_t BgL_arg1348z00_1533;

															BgL_arg1348z00_1533 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BNIL);
															BgL_arg1346z00_1532 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																BgL_arg1348z00_1533);
														}
														BgL_arg1342z00_1530 =
															MAKE_YOUNG_PAIR(BgL_arg1346z00_1532, BNIL);
													}
													BgL_arg1335z00_1527 =
														MAKE_YOUNG_PAIR(BgL_arg1340z00_1529,
														BgL_arg1342z00_1530);
												}
												BgL_arg1332z00_1525 =
													MAKE_YOUNG_PAIR(BgL_arg1333z00_1526,
													BgL_arg1335z00_1527);
											}
											BgL_arg1331z00_1524 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1332z00_1525);
										}
										BgL_arg1315z00_1510 =
											MAKE_YOUNG_PAIR(BgL_arg1331z00_1524, BNIL);
									}
									BgL_arg1312z00_1508 =
										MAKE_YOUNG_PAIR(BgL_arg1314z00_1509, BgL_arg1315z00_1510);
								}
								BgL_arg1284z00_1497 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(8), BgL_arg1312z00_1508);
							}
						else
							{	/* Coerce/funcall.scm 121 */
								obj_t BgL_arg1349z00_1534;

								{	/* Coerce/funcall.scm 121 */
									obj_t BgL_arg1351z00_1535;
									obj_t BgL_arg1352z00_1536;

									{	/* Coerce/funcall.scm 121 */
										obj_t BgL_arg1361z00_1537;

										{	/* Coerce/funcall.scm 121 */
											obj_t BgL_arg1364z00_1538;

											BgL_arg1364z00_1538 =
												BGl_currentzd2functionzd2zztools_errorz00();
											{	/* Coerce/funcall.scm 120 */
												obj_t BgL_arg1455z00_1841;

												BgL_arg1455z00_1841 =
													SYMBOL_TO_STRING(((obj_t) BgL_arg1364z00_1538));
												BgL_arg1361z00_1537 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1841);
											}
										}
										BgL_arg1351z00_1535 =
											string_append(BgL_arg1361z00_1537,
											BGl_string1813z00zzcoerce_funcallz00);
									}
									{	/* Coerce/funcall.scm 120 */
										obj_t BgL_arg1367z00_1539;

										BgL_arg1367z00_1539 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_funz00_21), BNIL);
										BgL_arg1352z00_1536 =
											MAKE_YOUNG_PAIR(BgL_errorzd2msgzd2_22,
											BgL_arg1367z00_1539);
									}
									BgL_arg1349z00_1534 =
										MAKE_YOUNG_PAIR(BgL_arg1351z00_1535, BgL_arg1352z00_1536);
								}
								BgL_arg1284z00_1497 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1349z00_1534);
							}
					}
					BgL_nodez00_1496 =
						BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
						(BgL_arg1284z00_1497, BgL_locz00_23);
				}
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_1496);
				return
					BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_1496, BgL_callerz00_24,
					((BgL_typez00_bglt) BgL_toz00_25), ((bool_t) 0));
			}
		}

	}



/* coerce-funcall-args! */
	obj_t BGl_coercezd2funcallzd2argsz12z12zzcoerce_funcallz00(BgL_funcallz00_bglt
		BgL_nodez00_26, obj_t BgL_callerz00_27, obj_t BgL_toz00_28,
		obj_t BgL_safez00_29)
	{
		{	/* Coerce/funcall.scm 132 */
			if (NULLP((((BgL_funcallz00_bglt) COBJECT(BgL_nodez00_26))->BgL_argsz00)))
				{	/* Coerce/funcall.scm 140 */
					obj_t BgL_arg1408z00_1548;

					{	/* Coerce/funcall.scm 140 */
						BgL_nodez00_bglt BgL_arg1410z00_1549;

						BgL_arg1410z00_1549 =
							BGl_toplevelzd2expze71z35zzcoerce_funcallz00(BgL_nodez00_26);
						{	/* Coerce/funcall.scm 140 */
							obj_t BgL_list1411z00_1550;

							BgL_list1411z00_1550 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1410z00_1549), BNIL);
							BgL_arg1408z00_1548 = BgL_list1411z00_1550;
						}
					}
					return
						((((BgL_funcallz00_bglt) COBJECT(BgL_nodez00_26))->BgL_argsz00) =
						((obj_t) BgL_arg1408z00_1548), BUNSPEC);
				}
			else
				{	/* Coerce/funcall.scm 141 */
					obj_t BgL_g1115z00_1551;
					obj_t BgL_g1116z00_1552;

					BgL_g1115z00_1551 =
						(((BgL_funcallz00_bglt) COBJECT(BgL_nodez00_26))->BgL_argsz00);
					BgL_g1116z00_1552 = CNST_TABLE_REF(9);
					{
						obj_t BgL_actualsz00_1554;
						obj_t BgL_prevz00_1555;

						BgL_actualsz00_1554 = BgL_g1115z00_1551;
						BgL_prevz00_1555 = BgL_g1116z00_1552;
					BgL_zc3z04anonymousza31412ze3z87_1556:
						if (NULLP(BgL_actualsz00_1554))
							{	/* Coerce/funcall.scm 144 */
								obj_t BgL_arg1421z00_1558;

								{	/* Coerce/funcall.scm 144 */
									BgL_nodez00_bglt BgL_arg1422z00_1559;

									BgL_arg1422z00_1559 =
										BGl_toplevelzd2expze71z35zzcoerce_funcallz00
										(BgL_nodez00_26);
									{	/* Coerce/funcall.scm 144 */
										obj_t BgL_list1423z00_1560;

										BgL_list1423z00_1560 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg1422z00_1559), BNIL);
										BgL_arg1421z00_1558 = BgL_list1423z00_1560;
									}
								}
								{	/* Coerce/funcall.scm 144 */
									obj_t BgL_tmpz00_2103;

									BgL_tmpz00_2103 = ((obj_t) BgL_prevz00_1555);
									return SET_CDR(BgL_tmpz00_2103, BgL_arg1421z00_1558);
								}
							}
						else
							{	/* Coerce/funcall.scm 143 */
								{	/* Coerce/funcall.scm 146 */
									BgL_nodez00_bglt BgL_arg1434z00_1561;

									{	/* Coerce/funcall.scm 146 */
										obj_t BgL_arg1437z00_1562;

										BgL_arg1437z00_1562 = CAR(((obj_t) BgL_actualsz00_1554));
										BgL_arg1434z00_1561 =
											BGl_coercez12z12zzcoerce_coercez00(
											((BgL_nodez00_bglt) BgL_arg1437z00_1562),
											BgL_callerz00_27,
											((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
											CBOOL(BgL_safez00_29));
									}
									{	/* Coerce/funcall.scm 146 */
										obj_t BgL_auxz00_2114;
										obj_t BgL_tmpz00_2112;

										BgL_auxz00_2114 = ((obj_t) BgL_arg1434z00_1561);
										BgL_tmpz00_2112 = ((obj_t) BgL_actualsz00_1554);
										SET_CAR(BgL_tmpz00_2112, BgL_auxz00_2114);
									}
								}
								{	/* Coerce/funcall.scm 147 */
									obj_t BgL_arg1448z00_1563;

									BgL_arg1448z00_1563 = CDR(((obj_t) BgL_actualsz00_1554));
									{
										obj_t BgL_prevz00_2120;
										obj_t BgL_actualsz00_2119;

										BgL_actualsz00_2119 = BgL_arg1448z00_1563;
										BgL_prevz00_2120 = BgL_actualsz00_1554;
										BgL_prevz00_1555 = BgL_prevz00_2120;
										BgL_actualsz00_1554 = BgL_actualsz00_2119;
										goto BgL_zc3z04anonymousza31412ze3z87_1556;
									}
								}
							}
					}
				}
		}

	}



/* toplevel-exp~1 */
	BgL_nodez00_bglt
		BGl_toplevelzd2expze71z35zzcoerce_funcallz00(BgL_funcallz00_bglt
		BgL_nodez00_1566)
	{
		{	/* Coerce/funcall.scm 137 */
			{	/* Coerce/funcall.scm 135 */
				BgL_nodez00_bglt BgL_nz00_1568;

				{	/* Coerce/funcall.scm 135 */
					obj_t BgL_arg1472z00_1569;

					BgL_arg1472z00_1569 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_nodez00_1566)))->BgL_locz00);
					BgL_nz00_1568 =
						BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(10),
						BgL_arg1472z00_1569);
				}
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nz00_1568);
				return BgL_nz00_1568;
			}
		}

	}



/* coerce-funcall-elight-args! */
	obj_t
		BGl_coercezd2funcallzd2elightzd2argsz12zc0zzcoerce_funcallz00
		(BgL_funcallz00_bglt BgL_nodez00_30, obj_t BgL_callerz00_31,
		obj_t BgL_toz00_32, obj_t BgL_safez00_33)
	{
		{	/* Coerce/funcall.scm 152 */
			if (NULLP((((BgL_funcallz00_bglt) COBJECT(BgL_nodez00_30))->BgL_argsz00)))
				{	/* Coerce/funcall.scm 160 */
					obj_t BgL_arg1485z00_1574;

					{	/* Coerce/funcall.scm 160 */
						BgL_nodez00_bglt BgL_arg1489z00_1575;

						BgL_arg1489z00_1575 =
							BGl_toplevelzd2expze70z35zzcoerce_funcallz00(BgL_nodez00_30);
						{	/* Coerce/funcall.scm 160 */
							obj_t BgL_list1490z00_1576;

							BgL_list1490z00_1576 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1489z00_1575), BNIL);
							BgL_arg1485z00_1574 = BgL_list1490z00_1576;
						}
					}
					return
						((((BgL_funcallz00_bglt) COBJECT(BgL_nodez00_30))->BgL_argsz00) =
						((obj_t) BgL_arg1485z00_1574), BUNSPEC);
				}
			else
				{	/* Coerce/funcall.scm 161 */
					BgL_variablez00_bglt BgL_calleez00_1577;

					{
						BgL_varz00_bglt BgL_auxz00_2133;

						{	/* Coerce/funcall.scm 161 */
							obj_t BgL_pairz00_1857;

							BgL_pairz00_1857 =
								(((BgL_funcallz00_bglt) COBJECT(BgL_nodez00_30))->
								BgL_functionsz00);
							BgL_auxz00_2133 = ((BgL_varz00_bglt) CAR(BgL_pairz00_1857));
						}
						BgL_calleez00_1577 =
							(((BgL_varz00_bglt) COBJECT(BgL_auxz00_2133))->BgL_variablez00);
					}
					{	/* Coerce/funcall.scm 162 */
						obj_t BgL_g1117z00_1578;
						obj_t BgL_g1118z00_1579;
						obj_t BgL_g1119z00_1580;

						BgL_g1117z00_1578 =
							(((BgL_funcallz00_bglt) COBJECT(BgL_nodez00_30))->BgL_argsz00);
						BgL_g1118z00_1579 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(BgL_calleez00_1577))->
											BgL_valuez00))))->BgL_argsz00);
						BgL_g1119z00_1580 = CNST_TABLE_REF(9);
						{
							obj_t BgL_actualsz00_1582;
							obj_t BgL_formalsz00_1583;
							obj_t BgL_prevz00_1584;

							BgL_actualsz00_1582 = BgL_g1117z00_1578;
							BgL_formalsz00_1583 = BgL_g1118z00_1579;
							BgL_prevz00_1584 = BgL_g1119z00_1580;
						BgL_zc3z04anonymousza31491ze3z87_1585:
							if (NULLP(BgL_actualsz00_1582))
								{	/* Coerce/funcall.scm 166 */
									obj_t BgL_arg1502z00_1587;

									{	/* Coerce/funcall.scm 166 */
										BgL_nodez00_bglt BgL_arg1509z00_1588;

										BgL_arg1509z00_1588 =
											BGl_toplevelzd2expze70z35zzcoerce_funcallz00
											(BgL_nodez00_30);
										{	/* Coerce/funcall.scm 166 */
											obj_t BgL_list1510z00_1589;

											BgL_list1510z00_1589 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1509z00_1588), BNIL);
											BgL_arg1502z00_1587 = BgL_list1510z00_1589;
										}
									}
									{	/* Coerce/funcall.scm 166 */
										obj_t BgL_tmpz00_2148;

										BgL_tmpz00_2148 = ((obj_t) BgL_prevz00_1584);
										return SET_CDR(BgL_tmpz00_2148, BgL_arg1502z00_1587);
									}
								}
							else
								{	/* Coerce/funcall.scm 165 */
									{	/* Coerce/funcall.scm 169 */
										BgL_nodez00_bglt BgL_arg1513z00_1590;

										{	/* Coerce/funcall.scm 169 */
											obj_t BgL_arg1514z00_1591;
											BgL_typez00_bglt BgL_arg1516z00_1592;

											BgL_arg1514z00_1591 = CAR(((obj_t) BgL_actualsz00_1582));
											BgL_arg1516z00_1592 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_formalsz00_1583))))))->
												BgL_typez00);
											BgL_arg1513z00_1590 =
												BGl_coercez12z12zzcoerce_coercez00(((BgL_nodez00_bglt)
													BgL_arg1514z00_1591), BgL_callerz00_31,
												BgL_arg1516z00_1592, CBOOL(BgL_safez00_33));
										}
										{	/* Coerce/funcall.scm 168 */
											obj_t BgL_auxz00_2163;
											obj_t BgL_tmpz00_2161;

											BgL_auxz00_2163 = ((obj_t) BgL_arg1513z00_1590);
											BgL_tmpz00_2161 = ((obj_t) BgL_actualsz00_1582);
											SET_CAR(BgL_tmpz00_2161, BgL_auxz00_2163);
										}
									}
									{	/* Coerce/funcall.scm 171 */
										obj_t BgL_arg1540z00_1594;
										obj_t BgL_arg1544z00_1595;

										BgL_arg1540z00_1594 = CDR(((obj_t) BgL_actualsz00_1582));
										BgL_arg1544z00_1595 = CDR(((obj_t) BgL_formalsz00_1583));
										{
											obj_t BgL_prevz00_2172;
											obj_t BgL_formalsz00_2171;
											obj_t BgL_actualsz00_2170;

											BgL_actualsz00_2170 = BgL_arg1540z00_1594;
											BgL_formalsz00_2171 = BgL_arg1544z00_1595;
											BgL_prevz00_2172 = BgL_actualsz00_1582;
											BgL_prevz00_1584 = BgL_prevz00_2172;
											BgL_formalsz00_1583 = BgL_formalsz00_2171;
											BgL_actualsz00_1582 = BgL_actualsz00_2170;
											goto BgL_zc3z04anonymousza31491ze3z87_1585;
										}
									}
								}
						}
					}
				}
		}

	}



/* toplevel-exp~0 */
	BgL_nodez00_bglt
		BGl_toplevelzd2expze70z35zzcoerce_funcallz00(BgL_funcallz00_bglt
		BgL_nodez00_1601)
	{
		{	/* Coerce/funcall.scm 157 */
			{	/* Coerce/funcall.scm 155 */
				BgL_nodez00_bglt BgL_nz00_1603;

				{	/* Coerce/funcall.scm 155 */
					obj_t BgL_arg1561z00_1604;

					BgL_arg1561z00_1604 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_nodez00_1601)))->BgL_locz00);
					BgL_nz00_1603 =
						BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00(CNST_TABLE_REF(10),
						BgL_arg1561z00_1604);
				}
				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nz00_1603);
				return BgL_nz00_1603;
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcoerce_funcallz00(void)
	{
		{	/* Coerce/funcall.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcoerce_funcallz00(void)
	{
		{	/* Coerce/funcall.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcoerce_funcallz00(void)
	{
		{	/* Coerce/funcall.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_funcallz00zzast_nodez00,
				BGl_proc1814z00zzcoerce_funcallz00,
				BGl_string1815z00zzcoerce_funcallz00);
		}

	}



/* &coerce!-funcall1237 */
	BgL_nodez00_bglt BGl_z62coercez12zd2funcall1237za2zzcoerce_funcallz00(obj_t
		BgL_envz00_1903, obj_t BgL_nodez00_1904, obj_t BgL_callerz00_1905,
		obj_t BgL_toz00_1906, obj_t BgL_safez00_1907)
	{
		{	/* Coerce/funcall.scm 37 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_errorzd2msgzd2_1926;
				obj_t BgL_strengthz00_1927;
				BgL_typez00_bglt BgL_ntyz00_1928;

				{	/* Tools/trace.sch 53 */
					obj_t BgL_arg1651z00_1929;

					BgL_arg1651z00_1929 =
						BGl_shapez00zztools_shapez00(
						((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_1904)));
					{	/* Tools/trace.sch 53 */
						obj_t BgL_list1652z00_1930;

						{	/* Tools/trace.sch 53 */
							obj_t BgL_arg1654z00_1931;

							BgL_arg1654z00_1931 = MAKE_YOUNG_PAIR(BgL_arg1651z00_1929, BNIL);
							BgL_list1652z00_1930 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BgL_arg1654z00_1931);
						}
						BgL_errorzd2msgzd2_1926 = BgL_list1652z00_1930;
					}
				}
				BgL_strengthz00_1927 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_1904)))->BgL_strengthz00);
				BgL_ntyz00_1928 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_funcallz00_bglt) BgL_nodez00_1904))))->BgL_typez00);
				if ((BgL_strengthz00_1927 == CNST_TABLE_REF(11)))
					{	/* Tools/trace.sch 53 */
						if (CBOOL
							(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
							{	/* Tools/trace.sch 53 */
								BGl_coercezd2funcallzd2elightzd2argsz12zc0zzcoerce_funcallz00(
									((BgL_funcallz00_bglt) BgL_nodez00_1904), BgL_callerz00_1905,
									BgL_toz00_1906, BgL_safez00_1907);
							}
						else
							{	/* Tools/trace.sch 53 */
								BGl_coercezd2funcallzd2argsz12z12zzcoerce_funcallz00(
									((BgL_funcallz00_bglt) BgL_nodez00_1904), BgL_callerz00_1905,
									BgL_toz00_1906, BgL_safez00_1907);
							}
						return
							BGl_convertz12z12zzcoerce_convertz00(
							((BgL_nodez00_bglt)
								((BgL_funcallz00_bglt) BgL_nodez00_1904)), BgL_ntyz00_1928,
							((BgL_typez00_bglt) BgL_toz00_1906), CBOOL(BgL_safez00_1907));
					}
				else
					{	/* Tools/trace.sch 53 */
						if ((BgL_strengthz00_1927 == CNST_TABLE_REF(12)))
							{	/* Tools/trace.sch 53 */
								if (CBOOL
									(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
									{	/* Tools/trace.sch 53 */
										BGl_coercezd2funcallzd2elightzd2argsz12zc0zzcoerce_funcallz00
											(((BgL_funcallz00_bglt) BgL_nodez00_1904),
											BgL_callerz00_1905, BgL_toz00_1906, BgL_safez00_1907);
									}
								else
									{	/* Tools/trace.sch 53 */
										BGl_coercezd2funcallzd2argsz12z12zzcoerce_funcallz00(
											((BgL_funcallz00_bglt) BgL_nodez00_1904),
											BgL_callerz00_1905, BgL_toz00_1906, BgL_safez00_1907);
									}
								return
									BGl_convertz12z12zzcoerce_convertz00(
									((BgL_nodez00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_1904)), BgL_ntyz00_1928,
									((BgL_typez00_bglt) BgL_toz00_1906), CBOOL(BgL_safez00_1907));
							}
						else
							{	/* Tools/trace.sch 53 */
								BgL_nodez00_bglt BgL_czd2funzd2_1932;

								{	/* Tools/trace.sch 53 */
									BgL_nodez00_bglt BgL_arg1650z00_1933;

									BgL_arg1650z00_1933 =
										(((BgL_funcallz00_bglt) COBJECT(
												((BgL_funcallz00_bglt) BgL_nodez00_1904)))->BgL_funz00);
									BgL_czd2funzd2_1932 =
										BGl_coercez12z12zzcoerce_coercez00(BgL_arg1650z00_1933,
										BgL_callerz00_1905,
										((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00),
										CBOOL(BgL_safez00_1907));
								}
								BGl_coercezd2funcallzd2argsz12z12zzcoerce_funcallz00(
									((BgL_funcallz00_bglt) BgL_nodez00_1904), BgL_callerz00_1905,
									BgL_toz00_1906, BgL_safez00_1907);
								if (CBOOL(BGl_za2unsafezd2arityza2zd2zzengine_paramz00))
									{	/* Tools/trace.sch 53 */
										((((BgL_funcallz00_bglt) COBJECT(
														((BgL_funcallz00_bglt) BgL_nodez00_1904)))->
												BgL_funz00) =
											((BgL_nodez00_bglt) BgL_czd2funzd2_1932), BUNSPEC);
										return
											BGl_convertz12z12zzcoerce_convertz00(((BgL_nodez00_bglt) (
													(BgL_funcallz00_bglt) BgL_nodez00_1904)),
											BgL_ntyz00_1928, ((BgL_typez00_bglt) BgL_toz00_1906),
											CBOOL(BgL_safez00_1907));
									}
								else
									{	/* Tools/trace.sch 53 */
										BgL_localz00_bglt BgL_funz00_1934;

										BgL_funz00_1934 =
											BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF
											(13),
											((BgL_typez00_bglt)
												BGl_za2procedureza2z00zztype_cachez00));
										{	/* Tools/trace.sch 53 */
											obj_t BgL_locz00_1935;

											BgL_locz00_1935 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_funcallz00_bglt) BgL_nodez00_1904))))->
												BgL_locz00);
											{	/* Tools/trace.sch 53 */
												BgL_literalz00_bglt BgL_lenz00_1936;

												{	/* Tools/trace.sch 53 */
													BgL_literalz00_bglt BgL_new1108z00_1937;

													{	/* Tools/trace.sch 53 */
														BgL_literalz00_bglt BgL_new1107z00_1938;

														BgL_new1107z00_1938 =
															((BgL_literalz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_literalz00_bgl))));
														{	/* Tools/trace.sch 53 */
															long BgL_arg1646z00_1939;

															BgL_arg1646z00_1939 =
																BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1107z00_1938),
																BgL_arg1646z00_1939);
														}
														{	/* Tools/trace.sch 53 */
															BgL_objectz00_bglt BgL_tmpz00_2244;

															BgL_tmpz00_2244 =
																((BgL_objectz00_bglt) BgL_new1107z00_1938);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2244, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1107z00_1938);
														BgL_new1108z00_1937 = BgL_new1107z00_1938;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1108z00_1937)))->
															BgL_locz00) = ((obj_t) BgL_locz00_1935), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1108z00_1937)))->BgL_typez00) =
														((BgL_typez00_bglt) ((BgL_typez00_bglt)
																BGl_za2intza2z00zztype_cachez00)), BUNSPEC);
													((((BgL_atomz00_bglt) COBJECT(((BgL_atomz00_bglt)
																		BgL_new1108z00_1937)))->BgL_valuez00) =
														((obj_t)
															BINT((bgl_list_length((((BgL_funcallz00_bglt)
																				COBJECT(((BgL_funcallz00_bglt)
																						BgL_nodez00_1904)))->BgL_argsz00)) -
																	2L))), BUNSPEC);
													BgL_lenz00_1936 = BgL_new1108z00_1937;
												}
												{	/* Tools/trace.sch 53 */
													obj_t BgL_azd2lenzd2_1940;

													BgL_azd2lenzd2_1940 =
														BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
														(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
															(14)));
													{	/* Tools/trace.sch 53 */
														obj_t BgL_azd2tlenzd2_1941;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1616z00_1942;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1625z00_1943;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1626z00_1944;
																	obj_t BgL_arg1627z00_1945;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1455z00_1946;

																		BgL_arg1455z00_1946 =
																			SYMBOL_TO_STRING(BgL_azd2lenzd2_1940);
																		BgL_arg1626z00_1944 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_1946);
																	}
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_symbolz00_1947;

																		BgL_symbolz00_1947 = CNST_TABLE_REF(15);
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1455z00_1948;

																			BgL_arg1455z00_1948 =
																				SYMBOL_TO_STRING(BgL_symbolz00_1947);
																			BgL_arg1627z00_1945 =
																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																				(BgL_arg1455z00_1948);
																	}}
																	BgL_arg1625z00_1943 =
																		string_append(BgL_arg1626z00_1944,
																		BgL_arg1627z00_1945);
																}
																BgL_arg1616z00_1942 =
																	bstring_to_symbol(BgL_arg1625z00_1943);
															}
															BgL_azd2tlenzd2_1941 =
																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																(BgL_arg1616z00_1942);
														}
														{	/* Tools/trace.sch 53 */
															BgL_letzd2varzd2_bglt BgL_lnodez00_1949;

															{	/* Tools/trace.sch 53 */
																BgL_letzd2varzd2_bglt BgL_new1110z00_1950;

																{	/* Tools/trace.sch 53 */
																	BgL_letzd2varzd2_bglt BgL_new1109z00_1951;

																	BgL_new1109z00_1951 =
																		((BgL_letzd2varzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_letzd2varzd2_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg1615z00_1952;

																		BgL_arg1615z00_1952 =
																			BGL_CLASS_NUM
																			(BGl_letzd2varzd2zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1109z00_1951),
																			BgL_arg1615z00_1952);
																	}
																	{	/* Tools/trace.sch 53 */
																		BgL_objectz00_bglt BgL_tmpz00_2275;

																		BgL_tmpz00_2275 =
																			((BgL_objectz00_bglt)
																			BgL_new1109z00_1951);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2275,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1109z00_1951);
																	BgL_new1110z00_1950 = BgL_new1109z00_1951;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1110z00_1950)))->BgL_locz00) =
																	((obj_t) BgL_locz00_1935), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1110z00_1950)))->BgL_typez00) =
																	((BgL_typez00_bglt)
																		BGl_strictzd2nodezd2typez00zzast_nodez00((
																				(BgL_typez00_bglt) BgL_toz00_1906),
																			BgL_ntyz00_1928)), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1110z00_1950)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1110z00_1950)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																{
																	obj_t BgL_auxz00_2290;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1573z00_1953;

																		BgL_arg1573z00_1953 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_funz00_1934),
																			((obj_t) BgL_czd2funzd2_1932));
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_list1574z00_1954;

																			BgL_list1574z00_1954 =
																				MAKE_YOUNG_PAIR(BgL_arg1573z00_1953,
																				BNIL);
																			BgL_auxz00_2290 = BgL_list1574z00_1954;
																	}}
																	((((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_new1110z00_1950))->
																			BgL_bindingsz00) =
																		((obj_t) BgL_auxz00_2290), BUNSPEC);
																}
																{
																	BgL_nodez00_bglt BgL_auxz00_2296;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1575z00_1955;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1576z00_1956;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1584z00_1957;
																				obj_t BgL_arg1585z00_1958;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1589z00_1959;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1591z00_1960;

																						BgL_arg1591z00_1960 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_lenz00_1936), BNIL);
																						BgL_arg1589z00_1959 =
																							MAKE_YOUNG_PAIR
																							(BgL_azd2tlenzd2_1941,
																							BgL_arg1591z00_1960);
																					}
																					BgL_arg1584z00_1957 =
																						MAKE_YOUNG_PAIR(BgL_arg1589z00_1959,
																						BNIL);
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1593z00_1961;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1594z00_1962;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1595z00_1963;
																							obj_t BgL_arg1602z00_1964;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1605z00_1965;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1606z00_1966;

																									BgL_arg1606z00_1966 =
																										MAKE_YOUNG_PAIR
																										(BgL_azd2lenzd2_1940, BNIL);
																									BgL_arg1605z00_1965 =
																										MAKE_YOUNG_PAIR(((obj_t)
																											BgL_funz00_1934),
																										BgL_arg1606z00_1966);
																								}
																								BgL_arg1595z00_1963 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(16), BgL_arg1605z00_1965);
																							}
																							{	/* Tools/trace.sch 53 */
																								BgL_nodez00_bglt
																									BgL_arg1609z00_1967;
																								obj_t BgL_arg1611z00_1968;

																								BgL_arg1609z00_1967 =
																									BGl_convertz12z12zzcoerce_convertz00
																									(((BgL_nodez00_bglt) (
																											(BgL_funcallz00_bglt)
																											BgL_nodez00_1904)),
																									BgL_ntyz00_1928,
																									((BgL_typez00_bglt)
																										BgL_toz00_1906),
																									CBOOL(BgL_safez00_1907));
																								{	/* Tools/trace.sch 53 */
																									BgL_nodez00_bglt
																										BgL_arg1613z00_1969;
																									BgL_arg1613z00_1969 =
																										BGl_makezd2arityzd2errorzd2nodezd2zzcoerce_funcallz00
																										(BgL_funz00_1934,
																										BgL_errorzd2msgzd2_1926,
																										BgL_locz00_1935,
																										BgL_callerz00_1905,
																										BgL_toz00_1906);
																									BgL_arg1611z00_1968 =
																										MAKE_YOUNG_PAIR(((obj_t)
																											BgL_arg1613z00_1969),
																										BNIL);
																								}
																								BgL_arg1602z00_1964 =
																									MAKE_YOUNG_PAIR(
																									((obj_t) BgL_arg1609z00_1967),
																									BgL_arg1611z00_1968);
																							}
																							BgL_arg1594z00_1962 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1595z00_1963,
																								BgL_arg1602z00_1964);
																						}
																						BgL_arg1593z00_1961 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(17), BgL_arg1594z00_1962);
																					}
																					BgL_arg1585z00_1958 =
																						MAKE_YOUNG_PAIR(BgL_arg1593z00_1961,
																						BNIL);
																				}
																				BgL_arg1576z00_1956 =
																					MAKE_YOUNG_PAIR(BgL_arg1584z00_1957,
																					BgL_arg1585z00_1958);
																			}
																			BgL_arg1575z00_1955 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																				BgL_arg1576z00_1956);
																		}
																		BgL_auxz00_2296 =
																			BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
																			(BgL_arg1575z00_1955, BgL_locz00_1935);
																	}
																	((((BgL_letzd2varzd2_bglt)
																				COBJECT(BgL_new1110z00_1950))->
																			BgL_bodyz00) =
																		((BgL_nodez00_bglt) BgL_auxz00_2296),
																		BUNSPEC);
																}
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1110z00_1950))->
																		BgL_removablezf3zf3) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
																BgL_lnodez00_1949 = BgL_new1110z00_1950;
															}
															{	/* Tools/trace.sch 53 */

																{	/* Tools/trace.sch 53 */
																	BgL_refz00_bglt BgL_arg1564z00_1970;

																	{	/* Tools/trace.sch 53 */
																		BgL_refz00_bglt BgL_new1113z00_1971;

																		{	/* Tools/trace.sch 53 */
																			BgL_refz00_bglt BgL_new1111z00_1972;

																			BgL_new1111z00_1972 =
																				((BgL_refz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_refz00_bgl))));
																			{	/* Tools/trace.sch 53 */
																				long BgL_arg1571z00_1973;

																				BgL_arg1571z00_1973 =
																					BGL_CLASS_NUM
																					(BGl_refz00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1111z00_1972),
																					BgL_arg1571z00_1973);
																			}
																			{	/* Tools/trace.sch 53 */
																				BgL_objectz00_bglt BgL_tmpz00_2330;

																				BgL_tmpz00_2330 =
																					((BgL_objectz00_bglt)
																					BgL_new1111z00_1972);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2330,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1111z00_1972);
																			BgL_new1113z00_1971 = BgL_new1111z00_1972;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1113z00_1971)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_1935), BUNSPEC);
																		{
																			BgL_typez00_bglt BgL_auxz00_2336;

																			{	/* Tools/trace.sch 53 */
																				BgL_typez00_bglt BgL_arg1565z00_1974;

																				BgL_arg1565z00_1974 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								BgL_funz00_1934)))->
																					BgL_typez00);
																				BgL_auxz00_2336 =
																					BGl_strictzd2nodezd2typez00zzast_nodez00
																					(BgL_arg1565z00_1974,
																					((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00));
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1113z00_1971)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_auxz00_2336),
																				BUNSPEC);
																		}
																		((((BgL_varz00_bglt) COBJECT(
																						((BgL_varz00_bglt)
																							BgL_new1113z00_1971)))->
																				BgL_variablez00) =
																			((BgL_variablez00_bglt) (
																					(BgL_variablez00_bglt)
																					BgL_funz00_1934)), BUNSPEC);
																		BgL_arg1564z00_1970 = BgL_new1113z00_1971;
																	}
																	((((BgL_funcallz00_bglt) COBJECT(
																					((BgL_funcallz00_bglt)
																						BgL_nodez00_1904)))->BgL_funz00) =
																		((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																				BgL_arg1564z00_1970)), BUNSPEC);
																}
																BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
																	((BgL_nodez00_bglt) BgL_lnodez00_1949));
																return ((BgL_nodez00_bglt) BgL_lnodez00_1949);
															}
														}
													}
												}
											}
										}
									}
							}
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcoerce_funcallz00(void)
	{
		{	/* Coerce/funcall.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
			return
				BGl_modulezd2initializa7ationz75zzcoerce_convertz00(87995645L,
				BSTRING_TO_STRING(BGl_string1816z00zzcoerce_funcallz00));
		}

	}

#ifdef __cplusplus
}
#endif
