/*===========================================================================*/
/*   (Coerce/convert.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Coerce/convert.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_COERCE_CONVERT_TYPE_DEFINITIONS
#define BGL_COERCE_CONVERT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;


#endif													// BGL_COERCE_CONVERT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2uint32za2z00zztype_cachez00;
	extern obj_t BGl_za2uint16za2z00zztype_cachez00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcoerce_convertz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static BgL_nodez00_bglt BGl_dozd2convertzd2zzcoerce_convertz00(obj_t,
		BgL_nodez00_bglt, BgL_typez00_bglt, BgL_typez00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzcoerce_convertz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2stackzd2checkz00zzcoerce_convertz00(void);
	static obj_t BGl_z62getzd2stackzd2checkz62zzcoerce_convertz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_notifyzd2typezd2testz00zzcoerce_convertz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt);
	static long BGl_za2notifyzd2counterza2zd2zzcoerce_convertz00 = 0L;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzcoerce_convertz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcoerce_convertz00(void);
	static BgL_nodez00_bglt
		BGl_makezd2onezd2conversionz00zzcoerce_convertz00(obj_t, obj_t, obj_t,
		obj_t, BgL_nodez00_bglt, bool_t);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	extern obj_t BGl_castz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_makezd2onezd2typezd2conversionzd2zzcoerce_convertz00(obj_t, obj_t,
		obj_t, obj_t, BgL_nodez00_bglt);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	static long BGl_za2checkza2z00zzcoerce_convertz00 = 0L;
	extern bool_t BGl_subzd2typezf3z21zztype_envz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2uint64za2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_warningzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_notifiedzd2locationszd2zzcoerce_convertz00 = BUNSPEC;
	extern obj_t BGl_za2stringza2z00zztype_cachez00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_za2warningzd2typesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_convertzd2errorzd2zzcoerce_convertz00(BgL_typez00_bglt,
		BgL_typez00_bglt, obj_t, BgL_nodez00_bglt);
	static obj_t BGl_methodzd2initzd2zzcoerce_convertz00(void);
	static BgL_nodez00_bglt
		BGl_makezd2onezd2classzd2conversionzd2zzcoerce_convertz00(obj_t, obj_t,
		obj_t, obj_t, BgL_nodez00_bglt);
	static obj_t BGl_z62runtimezd2typezd2errorz62zzcoerce_convertz00(obj_t, obj_t,
		obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2int32za2z00zztype_cachez00;
	extern obj_t BGl_za2int16za2z00zztype_cachez00;
	extern obj_t BGl_za2uint8za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_findzd2coercerzd2zztype_coercionz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2scharza2z00zztype_cachez00;
	BGL_EXPORTED_DEF obj_t BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00 =
		BUNSPEC;
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcoerce_convertz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_spreadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	static BgL_nodez00_bglt BGl_z62convertz12z70zzcoerce_convertz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2elongza2z00zztype_cachez00;
	extern bool_t BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2int8za2z00zztype_cachez00;
	static obj_t BGl_cnstzd2initzd2zzcoerce_convertz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_convertz00(void);
	static obj_t BGl_runtimezd2typezd2errorzf2idzf2zzcoerce_convertz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzcoerce_convertz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcoerce_convertz00(void);
	extern obj_t BGl_za2llongza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_currentzd2functionzd2zztools_errorz00(void);
	extern obj_t BGl_za2int64za2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_skipzd2letzd2varz00zzcoerce_convertz00(BgL_nodez00_bglt);
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_runtimezd2typezd2errorz00zzcoerce_convertz00(obj_t, obj_t,
		BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	BGL_IMPORT int BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(void);
	BGL_EXPORTED_DECL BgL_nodez00_bglt
		BGl_convertz12z12zzcoerce_convertz00(BgL_nodez00_bglt, BgL_typez00_bglt,
		BgL_typez00_bglt, bool_t);
	extern bool_t
		BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(BgL_nodez00_bglt);
	extern BgL_typez00_bglt
		BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t
		BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(int);
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_za2warningzd2typezd2errorza2z00zzengine_paramz00;
	extern obj_t BGl_za2charza2z00zztype_cachez00;
	static obj_t __cnst[11];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_runtimezd2typezd2errorzd2envzd2zzcoerce_convertz00,
		BgL_bgl_za762runtimeza7d2typ1996z00,
		BGl_z62runtimezd2typezd2errorz62zzcoerce_convertz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_convertz12zd2envzc0zzcoerce_convertz00,
		BgL_bgl_za762convertza712za7701997za7,
		BGl_z62convertz12z70zzcoerce_convertz00, 0L, BUNSPEC, 4);
	      DEFINE_REAL(BGl_real1991z00zzcoerce_convertz00,
		BgL_bgl_real1991za700za7za7coe1998za7, 0.0);
	      DEFINE_STRING(BGl_string1988z00zzcoerce_convertz00,
		BgL_bgl_string1988za700za7za7c1999za7,
		" ~a. Type test inserted \"~a\" -> \"~a\"", 36);
	      DEFINE_STRING(BGl_string1989z00zzcoerce_convertz00,
		BgL_bgl_string1989za700za7za7c2000za7, "", 0);
	      DEFINE_STRING(BGl_string1990z00zzcoerce_convertz00,
		BgL_bgl_string1990za700za7za7c2001za7, "Type error", 10);
	      DEFINE_STRING(BGl_string1992z00zzcoerce_convertz00,
		BgL_bgl_string1992za700za7za7c2002za7, "Illegal conversion", 18);
	      DEFINE_STRING(BGl_string1993z00zzcoerce_convertz00,
		BgL_bgl_string1993za700za7za7c2003za7, "coerce_convert", 14);
	      DEFINE_STRING(BGl_string1994z00zzcoerce_convertz00,
		BgL_bgl_string1994za700za7za7c2004za7,
		"aux2 if coercer let ::obj aux failure @ type-error __error location ", 68);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2stackzd2checkzd2envzd2zzcoerce_convertz00,
		BgL_bgl_za762getza7d2stackza7d2005za7,
		BGl_z62getzd2stackzd2checkz62zzcoerce_convertz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcoerce_convertz00));
		     ADD_ROOT((void *) (&BGl_notifiedzd2locationszd2zzcoerce_convertz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzcoerce_convertz00(long
		BgL_checksumz00_2786, char *BgL_fromz00_2787)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcoerce_convertz00))
				{
					BGl_requirezd2initializa7ationz75zzcoerce_convertz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcoerce_convertz00();
					BGl_libraryzd2moduleszd2initz00zzcoerce_convertz00();
					BGl_cnstzd2initzd2zzcoerce_convertz00();
					BGl_importedzd2moduleszd2initz00zzcoerce_convertz00();
					return BGl_toplevelzd2initzd2zzcoerce_convertz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "coerce_convert");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "coerce_convert");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"coerce_convert");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"coerce_convert");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "coerce_convert");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"coerce_convert");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"coerce_convert");
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "coerce_convert");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"coerce_convert");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"coerce_convert");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"coerce_convert");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"coerce_convert");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "coerce_convert");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "coerce_convert");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 16 */
			{	/* Coerce/convert.scm 16 */
				obj_t BgL_cportz00_2775;

				{	/* Coerce/convert.scm 16 */
					obj_t BgL_stringz00_2782;

					BgL_stringz00_2782 = BGl_string1994z00zzcoerce_convertz00;
					{	/* Coerce/convert.scm 16 */
						obj_t BgL_startz00_2783;

						BgL_startz00_2783 = BINT(0L);
						{	/* Coerce/convert.scm 16 */
							obj_t BgL_endz00_2784;

							BgL_endz00_2784 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2782)));
							{	/* Coerce/convert.scm 16 */

								BgL_cportz00_2775 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2782, BgL_startz00_2783, BgL_endz00_2784);
				}}}}
				{
					long BgL_iz00_2776;

					BgL_iz00_2776 = 10L;
				BgL_loopz00_2777:
					if ((BgL_iz00_2776 == -1L))
						{	/* Coerce/convert.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Coerce/convert.scm 16 */
							{	/* Coerce/convert.scm 16 */
								obj_t BgL_arg1995z00_2778;

								{	/* Coerce/convert.scm 16 */

									{	/* Coerce/convert.scm 16 */
										obj_t BgL_locationz00_2780;

										BgL_locationz00_2780 = BBOOL(((bool_t) 0));
										{	/* Coerce/convert.scm 16 */

											BgL_arg1995z00_2778 =
												BGl_readz00zz__readerz00(BgL_cportz00_2775,
												BgL_locationz00_2780);
										}
									}
								}
								{	/* Coerce/convert.scm 16 */
									int BgL_tmpz00_2819;

									BgL_tmpz00_2819 = (int) (BgL_iz00_2776);
									CNST_TABLE_SET(BgL_tmpz00_2819, BgL_arg1995z00_2778);
							}}
							{	/* Coerce/convert.scm 16 */
								int BgL_auxz00_2781;

								BgL_auxz00_2781 = (int) ((BgL_iz00_2776 - 1L));
								{
									long BgL_iz00_2824;

									BgL_iz00_2824 = (long) (BgL_auxz00_2781);
									BgL_iz00_2776 = BgL_iz00_2824;
									goto BgL_loopz00_2777;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 16 */
			BGl_za2checkza2z00zzcoerce_convertz00 = 0L;
			BGl_notifiedzd2locationszd2zzcoerce_convertz00 = BNIL;
			BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00 = BTRUE;
			return (BGl_za2notifyzd2counterza2zd2zzcoerce_convertz00 = 0L, BUNSPEC);
		}

	}



/* notify-type-test */
	obj_t BGl_notifyzd2typezd2testz00zzcoerce_convertz00(obj_t BgL_fromz00_35,
		obj_t BgL_toz00_36, obj_t BgL_locz00_37)
	{
		{	/* Coerce/convert.scm 59 */
			if (CBOOL(BGl_za2notifyzd2typezd2testza2z00zzcoerce_convertz00))
				{	/* Coerce/convert.scm 61 */
					int BgL_stz00_1780;

					BgL_stz00_1780 = BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00();
					BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(
						(int) (0L));
					if (CBOOL(BgL_locz00_37))
						{	/* Coerce/convert.scm 64 */
							if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
									(BgL_locz00_37,
										BGl_notifiedzd2locationszd2zzcoerce_convertz00)))
								{	/* Coerce/convert.scm 69 */
									BFALSE;
								}
							else
								{	/* Coerce/convert.scm 69 */
									BGl_za2notifyzd2counterza2zd2zzcoerce_convertz00 =
										(1L + BGl_za2notifyzd2counterza2zd2zzcoerce_convertz00);
									BGl_notifiedzd2locationszd2zzcoerce_convertz00 =
										MAKE_YOUNG_PAIR(BgL_locz00_37,
										BGl_notifiedzd2locationszd2zzcoerce_convertz00);
									{	/* Coerce/convert.scm 72 */
										obj_t BgL_arg1331z00_1782;
										obj_t BgL_arg1332z00_1783;
										obj_t BgL_arg1333z00_1784;

										BgL_arg1331z00_1782 =
											BGl_locationzd2fullzd2fnamez00zztools_locationz00
											(BgL_locz00_37);
										BgL_arg1332z00_1783 =
											STRUCT_REF(((obj_t) BgL_locz00_37), (int) (1L));
										{	/* Coerce/convert.scm 75 */
											obj_t BgL_arg1335z00_1786;
											obj_t BgL_arg1339z00_1787;

											BgL_arg1335z00_1786 =
												BGl_shapez00zztools_shapez00(BgL_fromz00_35);
											BgL_arg1339z00_1787 =
												BGl_shapez00zztools_shapez00(BgL_toz00_36);
											{	/* Coerce/convert.scm 74 */
												obj_t BgL_list1340z00_1788;

												{	/* Coerce/convert.scm 74 */
													obj_t BgL_arg1342z00_1789;

													{	/* Coerce/convert.scm 74 */
														obj_t BgL_arg1343z00_1790;

														BgL_arg1343z00_1790 =
															MAKE_YOUNG_PAIR(BgL_arg1339z00_1787, BNIL);
														BgL_arg1342z00_1789 =
															MAKE_YOUNG_PAIR(BgL_arg1335z00_1786,
															BgL_arg1343z00_1790);
													}
													BgL_list1340z00_1788 =
														MAKE_YOUNG_PAIR(BINT
														(BGl_za2notifyzd2counterza2zd2zzcoerce_convertz00),
														BgL_arg1342z00_1789);
												}
												BgL_arg1333z00_1784 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string1988z00zzcoerce_convertz00,
													BgL_list1340z00_1788);
										}}
										{	/* Coerce/convert.scm 72 */
											obj_t BgL_list1334z00_1785;

											BgL_list1334z00_1785 =
												MAKE_YOUNG_PAIR(BgL_arg1333z00_1784, BNIL);
											BGl_warningzf2locationzf2zz__errorz00(BgL_arg1331z00_1782,
												BgL_arg1332z00_1783, BgL_list1334z00_1785);
						}}}}
					else
						{	/* Coerce/convert.scm 64 */
							BGl_za2notifyzd2counterza2zd2zzcoerce_convertz00 =
								(1L + BGl_za2notifyzd2counterza2zd2zzcoerce_convertz00);
							{	/* Coerce/convert.scm 68 */
								obj_t BgL_arg1346z00_1791;

								{	/* Coerce/convert.scm 68 */
									obj_t BgL_arg1348z00_1793;
									obj_t BgL_arg1349z00_1794;

									BgL_arg1348z00_1793 =
										BGl_shapez00zztools_shapez00(BgL_fromz00_35);
									BgL_arg1349z00_1794 =
										BGl_shapez00zztools_shapez00(BgL_toz00_36);
									{	/* Coerce/convert.scm 67 */
										obj_t BgL_list1350z00_1795;

										{	/* Coerce/convert.scm 67 */
											obj_t BgL_arg1351z00_1796;

											{	/* Coerce/convert.scm 67 */
												obj_t BgL_arg1352z00_1797;

												BgL_arg1352z00_1797 =
													MAKE_YOUNG_PAIR(BgL_arg1349z00_1794, BNIL);
												BgL_arg1351z00_1796 =
													MAKE_YOUNG_PAIR(BgL_arg1348z00_1793,
													BgL_arg1352z00_1797);
											}
											BgL_list1350z00_1795 =
												MAKE_YOUNG_PAIR(BINT
												(BGl_za2notifyzd2counterza2zd2zzcoerce_convertz00),
												BgL_arg1351z00_1796);
										}
										BgL_arg1346z00_1791 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string1988z00zzcoerce_convertz00,
											BgL_list1350z00_1795);
									}
								}
								{	/* Coerce/convert.scm 66 */
									obj_t BgL_list1347z00_1792;

									BgL_list1347z00_1792 =
										MAKE_YOUNG_PAIR(BgL_arg1346z00_1791, BNIL);
									BGl_warningz00zz__errorz00(BgL_list1347z00_1792);
								}
							}
						}
					return
						BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00
						(BgL_stz00_1780);
				}
			else
				{	/* Coerce/convert.scm 60 */
					return BFALSE;
				}
		}

	}



/* get-stack-check */
	BGL_EXPORTED_DEF obj_t BGl_getzd2stackzd2checkz00zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 88 */
			return BINT(BGl_za2checkza2z00zzcoerce_convertz00);
		}

	}



/* &get-stack-check */
	obj_t BGl_z62getzd2stackzd2checkz62zzcoerce_convertz00(obj_t BgL_envz00_2763)
	{
		{	/* Coerce/convert.scm 88 */
			return BGl_getzd2stackzd2checkz00zzcoerce_convertz00();
		}

	}



/* runtime-type-error/id */
	obj_t BGl_runtimezd2typezd2errorzf2idzf2zzcoerce_convertz00(obj_t
		BgL_locz00_49, obj_t BgL_tiz00_50, obj_t BgL_idz00_51)
	{
		{	/* Coerce/convert.scm 115 */
			{	/* Coerce/convert.scm 117 */
				obj_t BgL_fnamez00_1805;
				obj_t BgL_posz00_1806;

				{	/* Coerce/convert.scm 117 */
					bool_t BgL_test2011z00_2865;

					if (STRUCTP(BgL_locz00_49))
						{	/* Coerce/convert.scm 117 */
							BgL_test2011z00_2865 =
								(STRUCT_KEY(BgL_locz00_49) == CNST_TABLE_REF(0));
						}
					else
						{	/* Coerce/convert.scm 117 */
							BgL_test2011z00_2865 = ((bool_t) 0);
						}
					if (BgL_test2011z00_2865)
						{	/* Coerce/convert.scm 117 */
							BgL_fnamez00_1805 =
								BGl_locationzd2fullzd2fnamez00zztools_locationz00
								(BgL_locz00_49);
						}
					else
						{	/* Coerce/convert.scm 117 */
							BgL_fnamez00_1805 = BFALSE;
						}
				}
				{	/* Coerce/convert.scm 118 */
					bool_t BgL_test2013z00_2872;

					if (STRUCTP(BgL_locz00_49))
						{	/* Coerce/convert.scm 118 */
							BgL_test2013z00_2872 =
								(STRUCT_KEY(BgL_locz00_49) == CNST_TABLE_REF(0));
						}
					else
						{	/* Coerce/convert.scm 118 */
							BgL_test2013z00_2872 = ((bool_t) 0);
						}
					if (BgL_test2013z00_2872)
						{	/* Coerce/convert.scm 118 */
							BgL_posz00_1806 = STRUCT_REF(BgL_locz00_49, (int) (1L));
						}
					else
						{	/* Coerce/convert.scm 118 */
							BgL_posz00_1806 = BFALSE;
						}
				}
				{	/* Coerce/convert.scm 120 */
					obj_t BgL_arg1376z00_1807;

					{	/* Coerce/convert.scm 120 */
						obj_t BgL_arg1377z00_1808;
						obj_t BgL_arg1378z00_1809;

						{	/* Coerce/convert.scm 120 */
							obj_t BgL_arg1379z00_1810;
							obj_t BgL_arg1380z00_1811;

							{	/* Coerce/convert.scm 120 */
								obj_t BgL_arg1408z00_1812;

								{	/* Coerce/convert.scm 120 */
									obj_t BgL_arg1410z00_1813;

									BgL_arg1410z00_1813 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BNIL);
									BgL_arg1408z00_1812 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1410z00_1813);
								}
								BgL_arg1379z00_1810 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1408z00_1812);
							}
							{	/* Coerce/convert.scm 121 */
								obj_t BgL_arg1421z00_1814;

								{	/* Coerce/convert.scm 121 */
									obj_t BgL_arg1422z00_1815;

									{	/* Coerce/convert.scm 121 */
										obj_t BgL_arg1434z00_1816;
										obj_t BgL_arg1437z00_1817;

										{	/* Coerce/convert.scm 121 */
											obj_t BgL_arg1448z00_1818;

											BgL_arg1448z00_1818 =
												BGl_currentzd2functionzd2zztools_errorz00();
											{	/* Coerce/convert.scm 121 */
												obj_t BgL_arg1455z00_2283;

												BgL_arg1455z00_2283 =
													SYMBOL_TO_STRING(((obj_t) BgL_arg1448z00_1818));
												BgL_arg1434z00_1816 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2283);
											}
										}
										{	/* Coerce/convert.scm 122 */
											obj_t BgL_arg1453z00_1819;
											obj_t BgL_arg1454z00_1820;

											{	/* Coerce/convert.scm 122 */
												obj_t BgL_arg1455z00_2285;

												BgL_arg1455z00_2285 =
													SYMBOL_TO_STRING(((obj_t) BgL_tiz00_50));
												BgL_arg1453z00_1819 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2285);
											}
											BgL_arg1454z00_1820 = MAKE_YOUNG_PAIR(BgL_idz00_51, BNIL);
											BgL_arg1437z00_1817 =
												MAKE_YOUNG_PAIR(BgL_arg1453z00_1819,
												BgL_arg1454z00_1820);
										}
										BgL_arg1422z00_1815 =
											MAKE_YOUNG_PAIR(BgL_arg1434z00_1816, BgL_arg1437z00_1817);
									}
									BgL_arg1421z00_1814 =
										MAKE_YOUNG_PAIR(BgL_posz00_1806, BgL_arg1422z00_1815);
								}
								BgL_arg1380z00_1811 =
									MAKE_YOUNG_PAIR(BgL_fnamez00_1805, BgL_arg1421z00_1814);
							}
							BgL_arg1377z00_1808 =
								MAKE_YOUNG_PAIR(BgL_arg1379z00_1810, BgL_arg1380z00_1811);
						}
						{	/* Coerce/convert.scm 119 */
							obj_t BgL_arg1472z00_1821;

							BgL_arg1472z00_1821 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
							BgL_arg1378z00_1809 =
								MAKE_YOUNG_PAIR(BFALSE, BgL_arg1472z00_1821);
						}
						BgL_arg1376z00_1807 =
							MAKE_YOUNG_PAIR(BgL_arg1377z00_1808, BgL_arg1378z00_1809);
					}
					return MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1376z00_1807);
				}
			}
		}

	}



/* runtime-type-error */
	BGL_EXPORTED_DEF obj_t BGl_runtimezd2typezd2errorz00zzcoerce_convertz00(obj_t
		BgL_locz00_52, obj_t BgL_tiz00_53, BgL_nodez00_bglt BgL_valuez00_54)
	{
		{	/* Coerce/convert.scm 129 */
			{	/* Coerce/convert.scm 131 */
				obj_t BgL_auxz00_1824;

				BgL_auxz00_1824 = BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(5));
				{	/* Coerce/convert.scm 131 */
					BgL_nodez00_bglt BgL_uvaluez00_1825;

					{	/* Coerce/convert.scm 132 */
						bool_t BgL_test2015z00_2906;

						{	/* Coerce/convert.scm 132 */
							obj_t BgL_classz00_2286;

							BgL_classz00_2286 = BGl_varz00zzast_nodez00;
							{	/* Coerce/convert.scm 132 */
								BgL_objectz00_bglt BgL_arg1807z00_2288;

								{	/* Coerce/convert.scm 132 */
									obj_t BgL_tmpz00_2907;

									BgL_tmpz00_2907 = ((obj_t) BgL_valuez00_54);
									BgL_arg1807z00_2288 = (BgL_objectz00_bglt) (BgL_tmpz00_2907);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Coerce/convert.scm 132 */
										long BgL_idxz00_2294;

										BgL_idxz00_2294 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2288);
										BgL_test2015z00_2906 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2294 + 2L)) == BgL_classz00_2286);
									}
								else
									{	/* Coerce/convert.scm 132 */
										bool_t BgL_res1973z00_2319;

										{	/* Coerce/convert.scm 132 */
											obj_t BgL_oclassz00_2302;

											{	/* Coerce/convert.scm 132 */
												obj_t BgL_arg1815z00_2310;
												long BgL_arg1816z00_2311;

												BgL_arg1815z00_2310 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Coerce/convert.scm 132 */
													long BgL_arg1817z00_2312;

													BgL_arg1817z00_2312 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2288);
													BgL_arg1816z00_2311 =
														(BgL_arg1817z00_2312 - OBJECT_TYPE);
												}
												BgL_oclassz00_2302 =
													VECTOR_REF(BgL_arg1815z00_2310, BgL_arg1816z00_2311);
											}
											{	/* Coerce/convert.scm 132 */
												bool_t BgL__ortest_1115z00_2303;

												BgL__ortest_1115z00_2303 =
													(BgL_classz00_2286 == BgL_oclassz00_2302);
												if (BgL__ortest_1115z00_2303)
													{	/* Coerce/convert.scm 132 */
														BgL_res1973z00_2319 = BgL__ortest_1115z00_2303;
													}
												else
													{	/* Coerce/convert.scm 132 */
														long BgL_odepthz00_2304;

														{	/* Coerce/convert.scm 132 */
															obj_t BgL_arg1804z00_2305;

															BgL_arg1804z00_2305 = (BgL_oclassz00_2302);
															BgL_odepthz00_2304 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2305);
														}
														if ((2L < BgL_odepthz00_2304))
															{	/* Coerce/convert.scm 132 */
																obj_t BgL_arg1802z00_2307;

																{	/* Coerce/convert.scm 132 */
																	obj_t BgL_arg1803z00_2308;

																	BgL_arg1803z00_2308 = (BgL_oclassz00_2302);
																	BgL_arg1802z00_2307 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2308,
																		2L);
																}
																BgL_res1973z00_2319 =
																	(BgL_arg1802z00_2307 == BgL_classz00_2286);
															}
														else
															{	/* Coerce/convert.scm 132 */
																BgL_res1973z00_2319 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2015z00_2906 = BgL_res1973z00_2319;
									}
							}
						}
						if (BgL_test2015z00_2906)
							{	/* Coerce/convert.scm 133 */
								BgL_refz00_bglt BgL_new1110z00_1843;

								{	/* Coerce/convert.scm 133 */
									BgL_refz00_bglt BgL_new1114z00_1845;

									BgL_new1114z00_1845 =
										((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_refz00_bgl))));
									{	/* Coerce/convert.scm 133 */
										long BgL_arg1564z00_1846;

										BgL_arg1564z00_1846 =
											BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1114z00_1845),
											BgL_arg1564z00_1846);
									}
									{	/* Coerce/convert.scm 133 */
										BgL_objectz00_bglt BgL_tmpz00_2933;

										BgL_tmpz00_2933 =
											((BgL_objectz00_bglt) BgL_new1114z00_1845);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2933, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1114z00_1845);
									BgL_new1110z00_1843 = BgL_new1114z00_1845;
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt) BgL_new1110z00_1843)))->
										BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_valuez00_54))->
											BgL_locz00)), BUNSPEC);
								((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_new1110z00_1843)))->BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt)
											BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
								((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
													BgL_new1110z00_1843)))->BgL_variablez00) =
									((BgL_variablez00_bglt) (((BgL_varz00_bglt)
												COBJECT(((BgL_varz00_bglt) BgL_valuez00_54)))->
											BgL_variablez00)), BUNSPEC);
								BgL_uvaluez00_1825 = ((BgL_nodez00_bglt) BgL_new1110z00_1843);
							}
						else
							{	/* Coerce/convert.scm 132 */
								BgL_uvaluez00_1825 = BgL_valuez00_54;
							}
					}
					{	/* Coerce/convert.scm 132 */
						BgL_nodez00_bglt BgL_resz00_1826;

						{	/* Coerce/convert.scm 137 */
							obj_t BgL_arg1502z00_1829;

							{	/* Coerce/convert.scm 137 */
								obj_t BgL_arg1509z00_1830;

								{	/* Coerce/convert.scm 137 */
									obj_t BgL_arg1513z00_1831;
									obj_t BgL_arg1514z00_1832;

									{	/* Coerce/convert.scm 137 */
										obj_t BgL_arg1516z00_1833;

										{	/* Coerce/convert.scm 137 */
											obj_t BgL_arg1535z00_1834;
											obj_t BgL_arg1540z00_1835;

											{	/* Coerce/convert.scm 137 */
												obj_t BgL_arg1544z00_1836;

												{	/* Coerce/convert.scm 137 */
													obj_t BgL_arg1546z00_1837;

													{	/* Coerce/convert.scm 137 */
														obj_t BgL_arg1552z00_1838;
														obj_t BgL_arg1553z00_1839;

														{	/* Coerce/convert.scm 137 */
															obj_t BgL_arg1455z00_2325;

															BgL_arg1455z00_2325 =
																SYMBOL_TO_STRING(BgL_auxz00_1824);
															BgL_arg1552z00_1838 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_2325);
														}
														{	/* Coerce/convert.scm 137 */
															obj_t BgL_symbolz00_2326;

															BgL_symbolz00_2326 = CNST_TABLE_REF(6);
															{	/* Coerce/convert.scm 137 */
																obj_t BgL_arg1455z00_2327;

																BgL_arg1455z00_2327 =
																	SYMBOL_TO_STRING(BgL_symbolz00_2326);
																BgL_arg1553z00_1839 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_2327);
															}
														}
														BgL_arg1546z00_1837 =
															string_append(BgL_arg1552z00_1838,
															BgL_arg1553z00_1839);
													}
													BgL_arg1544z00_1836 =
														bstring_to_symbol(BgL_arg1546z00_1837);
												}
												BgL_arg1535z00_1834 =
													BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
													(BgL_arg1544z00_1836);
											}
											BgL_arg1540z00_1835 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
											BgL_arg1516z00_1833 =
												MAKE_YOUNG_PAIR(BgL_arg1535z00_1834,
												BgL_arg1540z00_1835);
										}
										BgL_arg1513z00_1831 =
											MAKE_YOUNG_PAIR(BgL_arg1516z00_1833, BNIL);
									}
									BgL_arg1514z00_1832 =
										MAKE_YOUNG_PAIR
										(BGl_runtimezd2typezd2errorzf2idzf2zzcoerce_convertz00
										(BgL_locz00_52, BgL_tiz00_53, BgL_auxz00_1824), BNIL);
									BgL_arg1509z00_1830 =
										MAKE_YOUNG_PAIR(BgL_arg1513z00_1831, BgL_arg1514z00_1832);
								}
								BgL_arg1502z00_1829 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1509z00_1830);
							}
							BgL_resz00_1826 =
								BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
								(BgL_arg1502z00_1829, BgL_locz00_52);
						}
						{	/* Coerce/convert.scm 136 */

							{	/* Coerce/convert.scm 141 */
								obj_t BgL_arg1485z00_1827;

								{	/* Coerce/convert.scm 141 */
									obj_t BgL_pairz00_2330;

									BgL_pairz00_2330 =
										(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_resz00_1826)))->
										BgL_bindingsz00);
									BgL_arg1485z00_1827 = CAR(BgL_pairz00_2330);
								}
								{	/* Coerce/convert.scm 141 */
									obj_t BgL_auxz00_2970;
									obj_t BgL_tmpz00_2968;

									BgL_auxz00_2970 = ((obj_t) BgL_uvaluez00_1825);
									BgL_tmpz00_2968 = ((obj_t) BgL_arg1485z00_1827);
									SET_CDR(BgL_tmpz00_2968, BgL_auxz00_2970);
								}
							}
							return ((obj_t) BgL_resz00_1826);
						}
					}
				}
			}
		}

	}



/* &runtime-type-error */
	obj_t BGl_z62runtimezd2typezd2errorz62zzcoerce_convertz00(obj_t
		BgL_envz00_2764, obj_t BgL_locz00_2765, obj_t BgL_tiz00_2766,
		obj_t BgL_valuez00_2767)
	{
		{	/* Coerce/convert.scm 129 */
			return
				BGl_runtimezd2typezd2errorz00zzcoerce_convertz00(BgL_locz00_2765,
				BgL_tiz00_2766, ((BgL_nodez00_bglt) BgL_valuez00_2767));
		}

	}



/* convert-error */
	obj_t BGl_convertzd2errorzd2zzcoerce_convertz00(BgL_typez00_bglt
		BgL_fromz00_55, BgL_typez00_bglt BgL_toz00_56, obj_t BgL_locz00_57,
		BgL_nodez00_bglt BgL_nodez00_58)
	{
		{	/* Coerce/convert.scm 151 */
			{	/* Coerce/convert.scm 155 */
				bool_t BgL_test2019z00_2976;

				if ((((obj_t) BgL_toz00_56) == BGl_za2objza2z00zztype_cachez00))
					{	/* Coerce/convert.scm 155 */
						BgL_test2019z00_2976 = ((bool_t) 0);
					}
				else
					{	/* Coerce/convert.scm 155 */
						BgL_test2019z00_2976 =
							BGl_subzd2typezf3z21zztype_envz00(BgL_toz00_56,
							((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
					}
				if (BgL_test2019z00_2976)
					{	/* Coerce/convert.scm 156 */
						obj_t BgL_nodez00_1848;

						BgL_nodez00_1848 =
							BGl_runtimezd2typezd2errorz00zzcoerce_convertz00(BgL_locz00_57,
							(((BgL_typez00_bglt) COBJECT(BgL_toz00_56))->BgL_idz00),
							BgL_nodez00_58);
						{	/* Coerce/convert.scm 157 */
							obj_t BgL_arg1571z00_1849;

							BgL_arg1571z00_1849 = BGl_currentzd2functionzd2zztools_errorz00();
							if (CBOOL(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
								{	/* Coerce/convert.scm 109 */
									obj_t BgL_arg1370z00_2333;

									{	/* Coerce/convert.scm 109 */
										obj_t BgL_arg1371z00_2334;
										obj_t BgL_arg1375z00_2335;

										BgL_arg1371z00_2334 =
											BGl_shapez00zztools_shapez00(((obj_t) BgL_toz00_56));
										BgL_arg1375z00_2335 =
											BGl_shapez00zztools_shapez00(((obj_t) BgL_fromz00_55));
										BgL_arg1370z00_2333 =
											BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
											(BGl_string1989z00zzcoerce_convertz00,
											BgL_arg1371z00_2334, BgL_arg1375z00_2335);
									}
									BGl_userzd2warningzf2locationz20zztools_errorz00
										(BgL_locz00_57, BgL_arg1571z00_1849,
										BGl_string1990z00zzcoerce_convertz00, BgL_arg1370z00_2333);
								}
							else
								{	/* Coerce/convert.scm 103 */
									BFALSE;
								}
						}
						BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
							((BgL_nodez00_bglt) BgL_nodez00_1848));
						return
							((obj_t)
							BGl_coercez12z12zzcoerce_coercez00(
								((BgL_nodez00_bglt) BgL_nodez00_1848), BUNSPEC, BgL_fromz00_55,
								((bool_t) 0)));
					}
				else
					{	/* Coerce/convert.scm 160 */
						bool_t BgL_test2022z00_2998;

						{	/* Coerce/convert.scm 160 */
							obj_t BgL_classz00_2336;

							BgL_classz00_2336 = BGl_tclassz00zzobject_classz00;
							{	/* Coerce/convert.scm 160 */
								BgL_objectz00_bglt BgL_arg1807z00_2338;

								{	/* Coerce/convert.scm 160 */
									obj_t BgL_tmpz00_2999;

									BgL_tmpz00_2999 =
										((obj_t) ((BgL_objectz00_bglt) BgL_toz00_56));
									BgL_arg1807z00_2338 = (BgL_objectz00_bglt) (BgL_tmpz00_2999);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Coerce/convert.scm 160 */
										long BgL_idxz00_2344;

										BgL_idxz00_2344 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2338);
										BgL_test2022z00_2998 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2344 + 2L)) == BgL_classz00_2336);
									}
								else
									{	/* Coerce/convert.scm 160 */
										bool_t BgL_res1974z00_2369;

										{	/* Coerce/convert.scm 160 */
											obj_t BgL_oclassz00_2352;

											{	/* Coerce/convert.scm 160 */
												obj_t BgL_arg1815z00_2360;
												long BgL_arg1816z00_2361;

												BgL_arg1815z00_2360 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Coerce/convert.scm 160 */
													long BgL_arg1817z00_2362;

													BgL_arg1817z00_2362 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2338);
													BgL_arg1816z00_2361 =
														(BgL_arg1817z00_2362 - OBJECT_TYPE);
												}
												BgL_oclassz00_2352 =
													VECTOR_REF(BgL_arg1815z00_2360, BgL_arg1816z00_2361);
											}
											{	/* Coerce/convert.scm 160 */
												bool_t BgL__ortest_1115z00_2353;

												BgL__ortest_1115z00_2353 =
													(BgL_classz00_2336 == BgL_oclassz00_2352);
												if (BgL__ortest_1115z00_2353)
													{	/* Coerce/convert.scm 160 */
														BgL_res1974z00_2369 = BgL__ortest_1115z00_2353;
													}
												else
													{	/* Coerce/convert.scm 160 */
														long BgL_odepthz00_2354;

														{	/* Coerce/convert.scm 160 */
															obj_t BgL_arg1804z00_2355;

															BgL_arg1804z00_2355 = (BgL_oclassz00_2352);
															BgL_odepthz00_2354 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2355);
														}
														if ((2L < BgL_odepthz00_2354))
															{	/* Coerce/convert.scm 160 */
																obj_t BgL_arg1802z00_2357;

																{	/* Coerce/convert.scm 160 */
																	obj_t BgL_arg1803z00_2358;

																	BgL_arg1803z00_2358 = (BgL_oclassz00_2352);
																	BgL_arg1802z00_2357 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2358,
																		2L);
																}
																BgL_res1974z00_2369 =
																	(BgL_arg1802z00_2357 == BgL_classz00_2336);
															}
														else
															{	/* Coerce/convert.scm 160 */
																BgL_res1974z00_2369 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2022z00_2998 = BgL_res1974z00_2369;
									}
							}
						}
						if (BgL_test2022z00_2998)
							{	/* Coerce/convert.scm 161 */
								obj_t BgL_nodez00_1852;

								BgL_nodez00_1852 =
									BGl_runtimezd2typezd2errorz00zzcoerce_convertz00
									(BgL_locz00_57,
									(((BgL_typez00_bglt) COBJECT(BgL_toz00_56))->BgL_idz00),
									BgL_nodez00_58);
								{	/* Coerce/convert.scm 162 */
									obj_t BgL_arg1575z00_1853;

									BgL_arg1575z00_1853 =
										BGl_currentzd2functionzd2zztools_errorz00();
									if (CBOOL(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
										{	/* Coerce/convert.scm 109 */
											obj_t BgL_arg1370z00_2371;

											{	/* Coerce/convert.scm 109 */
												obj_t BgL_arg1371z00_2372;
												obj_t BgL_arg1375z00_2373;

												BgL_arg1371z00_2372 =
													BGl_shapez00zztools_shapez00(((obj_t) BgL_toz00_56));
												BgL_arg1375z00_2373 =
													BGl_shapez00zztools_shapez00(
													((obj_t) BgL_fromz00_55));
												BgL_arg1370z00_2371 =
													BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
													(BGl_string1989z00zzcoerce_convertz00,
													BgL_arg1371z00_2372, BgL_arg1375z00_2373);
											}
											BGl_userzd2warningzf2locationz20zztools_errorz00
												(BgL_locz00_57, BgL_arg1575z00_1853,
												BGl_string1990z00zzcoerce_convertz00,
												BgL_arg1370z00_2371);
										}
									else
										{	/* Coerce/convert.scm 103 */
											BFALSE;
										}
								}
								BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
									((BgL_nodez00_bglt) BgL_nodez00_1852));
								return
									((obj_t)
									BGl_coercez12z12zzcoerce_coercez00(
										((BgL_nodez00_bglt) BgL_nodez00_1852), BUNSPEC,
										BgL_fromz00_55, ((bool_t) 0)));
							}
						else
							{	/* Coerce/convert.scm 160 */
								if (CBOOL(BGl_za2warningzd2typezd2errorza2z00zzengine_paramz00))
									{	/* Coerce/convert.scm 234 */
										obj_t BgL_arg1584z00_1855;

										BgL_arg1584z00_1855 =
											BGl_currentzd2functionzd2zztools_errorz00();
										{	/* Coerce/convert.scm 97 */
											obj_t BgL_arg1361z00_2374;

											{	/* Coerce/convert.scm 97 */
												obj_t BgL_arg1364z00_2375;
												obj_t BgL_arg1367z00_2376;

												BgL_arg1364z00_2375 =
													BGl_shapez00zztools_shapez00(((obj_t) BgL_toz00_56));
												BgL_arg1367z00_2376 =
													BGl_shapez00zztools_shapez00(
													((obj_t) BgL_fromz00_55));
												BgL_arg1361z00_2374 =
													BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
													(BGl_string1989z00zzcoerce_convertz00,
													BgL_arg1364z00_2375, BgL_arg1367z00_2376);
											}
											return
												BGl_userzd2errorzf2locationz20zztools_errorz00
												(BgL_locz00_57, BgL_arg1584z00_1855,
												BGl_string1990z00zzcoerce_convertz00,
												BgL_arg1361z00_2374, BNIL);
										}
									}
								else
									{	/* Coerce/convert.scm 167 */
										bool_t BgL_test2028z00_3047;

										if (
											(((obj_t) BgL_toz00_56) ==
												BGl_za2intza2z00zztype_cachez00))
											{	/* Coerce/convert.scm 167 */
												BgL_test2028z00_3047 = ((bool_t) 1);
											}
										else
											{	/* Coerce/convert.scm 167 */
												if (
													(((obj_t) BgL_toz00_56) ==
														BGl_za2longza2z00zztype_cachez00))
													{	/* Coerce/convert.scm 167 */
														BgL_test2028z00_3047 = ((bool_t) 1);
													}
												else
													{	/* Coerce/convert.scm 167 */
														if (
															(((obj_t) BgL_toz00_56) ==
																BGl_za2elongza2z00zztype_cachez00))
															{	/* Coerce/convert.scm 168 */
																BgL_test2028z00_3047 = ((bool_t) 1);
															}
														else
															{	/* Coerce/convert.scm 168 */
																if (
																	(((obj_t) BgL_toz00_56) ==
																		BGl_za2llongza2z00zztype_cachez00))
																	{	/* Coerce/convert.scm 169 */
																		BgL_test2028z00_3047 = ((bool_t) 1);
																	}
																else
																	{	/* Coerce/convert.scm 169 */
																		if (
																			(((obj_t) BgL_toz00_56) ==
																				BGl_za2int8za2z00zztype_cachez00))
																			{	/* Coerce/convert.scm 170 */
																				BgL_test2028z00_3047 = ((bool_t) 1);
																			}
																		else
																			{	/* Coerce/convert.scm 170 */
																				if (
																					(((obj_t) BgL_toz00_56) ==
																						BGl_za2uint8za2z00zztype_cachez00))
																					{	/* Coerce/convert.scm 171 */
																						BgL_test2028z00_3047 = ((bool_t) 1);
																					}
																				else
																					{	/* Coerce/convert.scm 171 */
																						if (
																							(((obj_t) BgL_toz00_56) ==
																								BGl_za2int16za2z00zztype_cachez00))
																							{	/* Coerce/convert.scm 172 */
																								BgL_test2028z00_3047 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Coerce/convert.scm 172 */
																								if (
																									(((obj_t) BgL_toz00_56) ==
																										BGl_za2uint16za2z00zztype_cachez00))
																									{	/* Coerce/convert.scm 173 */
																										BgL_test2028z00_3047 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Coerce/convert.scm 173 */
																										if (
																											(((obj_t) BgL_toz00_56) ==
																												BGl_za2int32za2z00zztype_cachez00))
																											{	/* Coerce/convert.scm 174 */
																												BgL_test2028z00_3047 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Coerce/convert.scm 174 */
																												if (
																													(((obj_t)
																															BgL_toz00_56) ==
																														BGl_za2uint32za2z00zztype_cachez00))
																													{	/* Coerce/convert.scm 175 */
																														BgL_test2028z00_3047
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Coerce/convert.scm 175 */
																														if (
																															(((obj_t)
																																	BgL_toz00_56)
																																==
																																BGl_za2int64za2z00zztype_cachez00))
																															{	/* Coerce/convert.scm 176 */
																																BgL_test2028z00_3047
																																	=
																																	((bool_t) 1);
																															}
																														else
																															{	/* Coerce/convert.scm 176 */
																																BgL_test2028z00_3047
																																	=
																																	(((obj_t)
																																		BgL_toz00_56)
																																	==
																																	BGl_za2uint64za2z00zztype_cachez00);
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
										if (BgL_test2028z00_3047)
											{	/* Coerce/convert.scm 178 */
												obj_t BgL_nodez00_1857;

												BgL_nodez00_1857 =
													BGl_runtimezd2typezd2errorz00zzcoerce_convertz00
													(BgL_locz00_57,
													(((BgL_typez00_bglt) COBJECT(BgL_toz00_56))->
														BgL_idz00), BgL_nodez00_58);
												{	/* Coerce/convert.scm 179 */
													obj_t BgL_arg1589z00_1858;

													BgL_arg1589z00_1858 =
														BGl_currentzd2functionzd2zztools_errorz00();
													if (CBOOL
														(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
														{	/* Coerce/convert.scm 109 */
															obj_t BgL_arg1370z00_2379;

															{	/* Coerce/convert.scm 109 */
																obj_t BgL_arg1371z00_2380;
																obj_t BgL_arg1375z00_2381;

																BgL_arg1371z00_2380 =
																	BGl_shapez00zztools_shapez00(
																	((obj_t) BgL_toz00_56));
																BgL_arg1375z00_2381 =
																	BGl_shapez00zztools_shapez00(
																	((obj_t) BgL_fromz00_55));
																BgL_arg1370z00_2379 =
																	BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																	(BGl_string1989z00zzcoerce_convertz00,
																	BgL_arg1371z00_2380, BgL_arg1375z00_2381);
															}
															BGl_userzd2warningzf2locationz20zztools_errorz00
																(BgL_locz00_57, BgL_arg1589z00_1858,
																BGl_string1990z00zzcoerce_convertz00,
																BgL_arg1370z00_2379);
														}
													else
														{	/* Coerce/convert.scm 103 */
															BFALSE;
														}
												}
												BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
													((BgL_nodez00_bglt) BgL_nodez00_1857));
												{	/* Coerce/convert.scm 181 */
													BgL_sequencez00_bglt BgL_new1116z00_1859;

													{	/* Coerce/convert.scm 181 */
														BgL_sequencez00_bglt BgL_new1115z00_1867;

														BgL_new1115z00_1867 =
															((BgL_sequencez00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_sequencez00_bgl))));
														{	/* Coerce/convert.scm 181 */
															long BgL_arg1605z00_1868;

															BgL_arg1605z00_1868 =
																BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1115z00_1867),
																BgL_arg1605z00_1868);
														}
														{	/* Coerce/convert.scm 181 */
															BgL_objectz00_bglt BgL_tmpz00_3100;

															BgL_tmpz00_3100 =
																((BgL_objectz00_bglt) BgL_new1115z00_1867);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3100, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1115z00_1867);
														BgL_new1116z00_1859 = BgL_new1115z00_1867;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1116z00_1859)))->
															BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1116z00_1859)))->BgL_typez00) =
														((BgL_typez00_bglt) BgL_toz00_56), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1116z00_1859)))->
															BgL_sidezd2effectzd2) =
														((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1116z00_1859)))->BgL_keyz00) =
														((obj_t) BINT(-1L)), BUNSPEC);
													{
														obj_t BgL_auxz00_3113;

														{	/* Coerce/convert.scm 184 */
															BgL_nodez00_bglt BgL_arg1591z00_1860;
															BgL_literalz00_bglt BgL_arg1593z00_1861;

															BgL_arg1591z00_1860 =
																BGl_coercez12z12zzcoerce_coercez00(
																((BgL_nodez00_bglt) BgL_nodez00_1857), BUNSPEC,
																BgL_fromz00_55, ((bool_t) 0));
															{	/* Coerce/convert.scm 185 */
																BgL_literalz00_bglt BgL_new1119z00_1864;

																{	/* Coerce/convert.scm 185 */
																	BgL_literalz00_bglt BgL_new1118z00_1865;

																	BgL_new1118z00_1865 =
																		((BgL_literalz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_literalz00_bgl))));
																	{	/* Coerce/convert.scm 185 */
																		long BgL_arg1602z00_1866;

																		{	/* Coerce/convert.scm 185 */
																			obj_t BgL_classz00_2386;

																			BgL_classz00_2386 =
																				BGl_literalz00zzast_nodez00;
																			BgL_arg1602z00_1866 =
																				BGL_CLASS_NUM(BgL_classz00_2386);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1118z00_1865),
																			BgL_arg1602z00_1866);
																	}
																	{	/* Coerce/convert.scm 185 */
																		BgL_objectz00_bglt BgL_tmpz00_3120;

																		BgL_tmpz00_3120 =
																			((BgL_objectz00_bglt)
																			BgL_new1118z00_1865);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3120,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1118z00_1865);
																	BgL_new1119z00_1864 = BgL_new1118z00_1865;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1119z00_1864)))->BgL_locz00) =
																	((obj_t) BFALSE), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1119z00_1864)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_toz00_56), BUNSPEC);
																((((BgL_atomz00_bglt)
																			COBJECT(((BgL_atomz00_bglt)
																					BgL_new1119z00_1864)))->
																		BgL_valuez00) =
																	((obj_t) BINT(0L)), BUNSPEC);
																BgL_arg1593z00_1861 = BgL_new1119z00_1864;
															}
															{	/* Coerce/convert.scm 183 */
																obj_t BgL_list1594z00_1862;

																{	/* Coerce/convert.scm 183 */
																	obj_t BgL_arg1595z00_1863;

																	BgL_arg1595z00_1863 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1593z00_1861), BNIL);
																	BgL_list1594z00_1862 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1591z00_1860),
																		BgL_arg1595z00_1863);
																}
																BgL_auxz00_3113 = BgL_list1594z00_1862;
														}}
														((((BgL_sequencez00_bglt)
																	COBJECT(BgL_new1116z00_1859))->BgL_nodesz00) =
															((obj_t) BgL_auxz00_3113), BUNSPEC);
													}
													((((BgL_sequencez00_bglt)
																COBJECT(BgL_new1116z00_1859))->BgL_unsafez00) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
													((((BgL_sequencez00_bglt)
																COBJECT(BgL_new1116z00_1859))->BgL_metaz00) =
														((obj_t) BNIL), BUNSPEC);
													return ((obj_t) BgL_new1116z00_1859);
												}
											}
										else
											{	/* Coerce/convert.scm 167 */
												if (
													(((obj_t) BgL_toz00_56) ==
														BGl_za2boolza2z00zztype_cachez00))
													{	/* Coerce/convert.scm 187 */
														obj_t BgL_nodez00_1870;

														BgL_nodez00_1870 =
															BGl_runtimezd2typezd2errorz00zzcoerce_convertz00
															(BgL_locz00_57,
															(((BgL_typez00_bglt) COBJECT(BgL_toz00_56))->
																BgL_idz00), BgL_nodez00_58);
														{	/* Coerce/convert.scm 188 */
															obj_t BgL_arg1609z00_1871;

															BgL_arg1609z00_1871 =
																BGl_currentzd2functionzd2zztools_errorz00();
															if (CBOOL
																(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
																{	/* Coerce/convert.scm 109 */
																	obj_t BgL_arg1370z00_2392;

																	{	/* Coerce/convert.scm 109 */
																		obj_t BgL_arg1371z00_2393;
																		obj_t BgL_arg1375z00_2394;

																		BgL_arg1371z00_2393 =
																			BGl_shapez00zztools_shapez00(
																			((obj_t) BgL_toz00_56));
																		BgL_arg1375z00_2394 =
																			BGl_shapez00zztools_shapez00(
																			((obj_t) BgL_fromz00_55));
																		BgL_arg1370z00_2392 =
																			BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																			(BGl_string1989z00zzcoerce_convertz00,
																			BgL_arg1371z00_2393, BgL_arg1375z00_2394);
																	}
																	BGl_userzd2warningzf2locationz20zztools_errorz00
																		(BgL_locz00_57, BgL_arg1609z00_1871,
																		BGl_string1990z00zzcoerce_convertz00,
																		BgL_arg1370z00_2392);
																}
															else
																{	/* Coerce/convert.scm 103 */
																	BFALSE;
																}
														}
														BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
															((BgL_nodez00_bglt) BgL_nodez00_1870));
														{	/* Coerce/convert.scm 190 */
															BgL_sequencez00_bglt BgL_new1122z00_1872;

															{	/* Coerce/convert.scm 190 */
																BgL_sequencez00_bglt BgL_new1120z00_1880;

																BgL_new1120z00_1880 =
																	((BgL_sequencez00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_sequencez00_bgl))));
																{	/* Coerce/convert.scm 190 */
																	long BgL_arg1625z00_1881;

																	BgL_arg1625z00_1881 =
																		BGL_CLASS_NUM(BGl_sequencez00zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1120z00_1880),
																		BgL_arg1625z00_1881);
																}
																{	/* Coerce/convert.scm 190 */
																	BgL_objectz00_bglt BgL_tmpz00_3159;

																	BgL_tmpz00_3159 =
																		((BgL_objectz00_bglt) BgL_new1120z00_1880);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3159,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1120z00_1880);
																BgL_new1122z00_1872 = BgL_new1120z00_1880;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1122z00_1872)))->BgL_locz00) =
																((obj_t) BFALSE), BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1122z00_1872)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_toz00_56), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1122z00_1872)))->
																	BgL_sidezd2effectzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1122z00_1872)))->BgL_keyz00) =
																((obj_t) BINT(-1L)), BUNSPEC);
															{
																obj_t BgL_auxz00_3172;

																{	/* Coerce/convert.scm 193 */
																	BgL_nodez00_bglt BgL_arg1611z00_1873;
																	BgL_literalz00_bglt BgL_arg1613z00_1874;

																	BgL_arg1611z00_1873 =
																		BGl_coercez12z12zzcoerce_coercez00(
																		((BgL_nodez00_bglt) BgL_nodez00_1870),
																		BUNSPEC, BgL_fromz00_55, ((bool_t) 0));
																	{	/* Coerce/convert.scm 194 */
																		BgL_literalz00_bglt BgL_new1124z00_1877;

																		{	/* Coerce/convert.scm 194 */
																			BgL_literalz00_bglt BgL_new1123z00_1878;

																			BgL_new1123z00_1878 =
																				((BgL_literalz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_literalz00_bgl))));
																			{	/* Coerce/convert.scm 194 */
																				long BgL_arg1616z00_1879;

																				{	/* Coerce/convert.scm 194 */
																					obj_t BgL_classz00_2399;

																					BgL_classz00_2399 =
																						BGl_literalz00zzast_nodez00;
																					BgL_arg1616z00_1879 =
																						BGL_CLASS_NUM(BgL_classz00_2399);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1123z00_1878),
																					BgL_arg1616z00_1879);
																			}
																			{	/* Coerce/convert.scm 194 */
																				BgL_objectz00_bglt BgL_tmpz00_3179;

																				BgL_tmpz00_3179 =
																					((BgL_objectz00_bglt)
																					BgL_new1123z00_1878);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3179,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1123z00_1878);
																			BgL_new1124z00_1877 = BgL_new1123z00_1878;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1124z00_1877)))->
																				BgL_locz00) =
																			((obj_t) BFALSE), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1124z00_1877)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_toz00_56),
																			BUNSPEC);
																		((((BgL_atomz00_bglt)
																					COBJECT(((BgL_atomz00_bglt)
																							BgL_new1124z00_1877)))->
																				BgL_valuez00) =
																			((obj_t) BINT(0L)), BUNSPEC);
																		BgL_arg1613z00_1874 = BgL_new1124z00_1877;
																	}
																	{	/* Coerce/convert.scm 192 */
																		obj_t BgL_list1614z00_1875;

																		{	/* Coerce/convert.scm 192 */
																			obj_t BgL_arg1615z00_1876;

																			BgL_arg1615z00_1876 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_arg1613z00_1874), BNIL);
																			BgL_list1614z00_1875 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_arg1611z00_1873),
																				BgL_arg1615z00_1876);
																		}
																		BgL_auxz00_3172 = BgL_list1614z00_1875;
																}}
																((((BgL_sequencez00_bglt)
																			COBJECT(BgL_new1122z00_1872))->
																		BgL_nodesz00) =
																	((obj_t) BgL_auxz00_3172), BUNSPEC);
															}
															((((BgL_sequencez00_bglt)
																		COBJECT(BgL_new1122z00_1872))->
																	BgL_unsafez00) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
															((((BgL_sequencez00_bglt)
																		COBJECT(BgL_new1122z00_1872))->
																	BgL_metaz00) = ((obj_t) BNIL), BUNSPEC);
															return ((obj_t) BgL_new1122z00_1872);
														}
													}
												else
													{	/* Coerce/convert.scm 186 */
														if (
															(((obj_t) BgL_toz00_56) ==
																BGl_za2realza2z00zztype_cachez00))
															{	/* Coerce/convert.scm 196 */
																obj_t BgL_nodez00_1883;

																BgL_nodez00_1883 =
																	BGl_runtimezd2typezd2errorz00zzcoerce_convertz00
																	(BgL_locz00_57,
																	(((BgL_typez00_bglt) COBJECT(BgL_toz00_56))->
																		BgL_idz00), BgL_nodez00_58);
																{	/* Coerce/convert.scm 197 */
																	obj_t BgL_arg1627z00_1884;

																	BgL_arg1627z00_1884 =
																		BGl_currentzd2functionzd2zztools_errorz00();
																	if (CBOOL
																		(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
																		{	/* Coerce/convert.scm 109 */
																			obj_t BgL_arg1370z00_2405;

																			{	/* Coerce/convert.scm 109 */
																				obj_t BgL_arg1371z00_2406;
																				obj_t BgL_arg1375z00_2407;

																				BgL_arg1371z00_2406 =
																					BGl_shapez00zztools_shapez00(
																					((obj_t) BgL_toz00_56));
																				BgL_arg1375z00_2407 =
																					BGl_shapez00zztools_shapez00(
																					((obj_t) BgL_fromz00_55));
																				BgL_arg1370z00_2405 =
																					BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																					(BGl_string1989z00zzcoerce_convertz00,
																					BgL_arg1371z00_2406,
																					BgL_arg1375z00_2407);
																			}
																			BGl_userzd2warningzf2locationz20zztools_errorz00
																				(BgL_locz00_57, BgL_arg1627z00_1884,
																				BGl_string1990z00zzcoerce_convertz00,
																				BgL_arg1370z00_2405);
																		}
																	else
																		{	/* Coerce/convert.scm 103 */
																			BFALSE;
																		}
																}
																BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
																	((BgL_nodez00_bglt) BgL_nodez00_1883));
																{	/* Coerce/convert.scm 199 */
																	BgL_sequencez00_bglt BgL_new1127z00_1885;

																	{	/* Coerce/convert.scm 199 */
																		BgL_sequencez00_bglt BgL_new1125z00_1893;

																		BgL_new1125z00_1893 =
																			((BgL_sequencez00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_sequencez00_bgl))));
																		{	/* Coerce/convert.scm 199 */
																			long BgL_arg1650z00_1894;

																			BgL_arg1650z00_1894 =
																				BGL_CLASS_NUM
																				(BGl_sequencez00zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1125z00_1893),
																				BgL_arg1650z00_1894);
																		}
																		{	/* Coerce/convert.scm 199 */
																			BgL_objectz00_bglt BgL_tmpz00_3218;

																			BgL_tmpz00_3218 =
																				((BgL_objectz00_bglt)
																				BgL_new1125z00_1893);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3218,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1125z00_1893);
																		BgL_new1127z00_1885 = BgL_new1125z00_1893;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1127z00_1885)))->
																			BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1127z00_1885)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_toz00_56), BUNSPEC);
																	((((BgL_nodezf2effectzf2_bglt)
																				COBJECT(((BgL_nodezf2effectzf2_bglt)
																						BgL_new1127z00_1885)))->
																			BgL_sidezd2effectzd2) =
																		((obj_t) BUNSPEC), BUNSPEC);
																	((((BgL_nodezf2effectzf2_bglt)
																				COBJECT(((BgL_nodezf2effectzf2_bglt)
																						BgL_new1127z00_1885)))->
																			BgL_keyz00) =
																		((obj_t) BINT(-1L)), BUNSPEC);
																	{
																		obj_t BgL_auxz00_3231;

																		{	/* Coerce/convert.scm 202 */
																			BgL_nodez00_bglt BgL_arg1629z00_1886;
																			BgL_literalz00_bglt BgL_arg1630z00_1887;

																			BgL_arg1629z00_1886 =
																				BGl_coercez12z12zzcoerce_coercez00(
																				((BgL_nodez00_bglt) BgL_nodez00_1883),
																				BUNSPEC, BgL_fromz00_55, ((bool_t) 0));
																			{	/* Coerce/convert.scm 203 */
																				BgL_literalz00_bglt BgL_new1130z00_1890;

																				{	/* Coerce/convert.scm 203 */
																					BgL_literalz00_bglt
																						BgL_new1129z00_1891;
																					BgL_new1129z00_1891 =
																						((BgL_literalz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_literalz00_bgl))));
																					{	/* Coerce/convert.scm 203 */
																						long BgL_arg1646z00_1892;

																						{	/* Coerce/convert.scm 203 */
																							obj_t BgL_classz00_2412;

																							BgL_classz00_2412 =
																								BGl_literalz00zzast_nodez00;
																							BgL_arg1646z00_1892 =
																								BGL_CLASS_NUM
																								(BgL_classz00_2412);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1129z00_1891),
																							BgL_arg1646z00_1892);
																					}
																					{	/* Coerce/convert.scm 203 */
																						BgL_objectz00_bglt BgL_tmpz00_3238;

																						BgL_tmpz00_3238 =
																							((BgL_objectz00_bglt)
																							BgL_new1129z00_1891);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_3238, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1129z00_1891);
																					BgL_new1130z00_1890 =
																						BgL_new1129z00_1891;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1130z00_1890)))->
																						BgL_locz00) =
																					((obj_t) BFALSE), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1130z00_1890)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) BgL_toz00_56),
																					BUNSPEC);
																				((((BgL_atomz00_bglt)
																							COBJECT(((BgL_atomz00_bglt)
																									BgL_new1130z00_1890)))->
																						BgL_valuez00) =
																					((obj_t)
																						BGL_REAL_CNST
																						(BGl_real1991z00zzcoerce_convertz00)),
																					BUNSPEC);
																				BgL_arg1630z00_1887 =
																					BgL_new1130z00_1890;
																			}
																			{	/* Coerce/convert.scm 201 */
																				obj_t BgL_list1631z00_1888;

																				{	/* Coerce/convert.scm 201 */
																					obj_t BgL_arg1642z00_1889;

																					BgL_arg1642z00_1889 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_arg1630z00_1887),
																						BNIL);
																					BgL_list1631z00_1888 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_arg1629z00_1886),
																						BgL_arg1642z00_1889);
																				}
																				BgL_auxz00_3231 = BgL_list1631z00_1888;
																		}}
																		((((BgL_sequencez00_bglt)
																					COBJECT(BgL_new1127z00_1885))->
																				BgL_nodesz00) =
																			((obj_t) BgL_auxz00_3231), BUNSPEC);
																	}
																	((((BgL_sequencez00_bglt)
																				COBJECT(BgL_new1127z00_1885))->
																			BgL_unsafez00) =
																		((bool_t) ((bool_t) 0)), BUNSPEC);
																	((((BgL_sequencez00_bglt)
																				COBJECT(BgL_new1127z00_1885))->
																			BgL_metaz00) = ((obj_t) BNIL), BUNSPEC);
																	return ((obj_t) BgL_new1127z00_1885);
																}
															}
														else
															{	/* Coerce/convert.scm 195 */
																if (
																	(((obj_t) BgL_toz00_56) ==
																		BGl_za2charza2z00zztype_cachez00))
																	{	/* Coerce/convert.scm 205 */
																		obj_t BgL_nodez00_1896;

																		BgL_nodez00_1896 =
																			BGl_runtimezd2typezd2errorz00zzcoerce_convertz00
																			(BgL_locz00_57,
																			(((BgL_typez00_bglt)
																					COBJECT(BgL_toz00_56))->BgL_idz00),
																			BgL_nodez00_58);
																		{	/* Coerce/convert.scm 206 */
																			obj_t BgL_arg1654z00_1897;

																			BgL_arg1654z00_1897 =
																				BGl_currentzd2functionzd2zztools_errorz00
																				();
																			if (CBOOL
																				(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
																				{	/* Coerce/convert.scm 109 */
																					obj_t BgL_arg1370z00_2418;

																					{	/* Coerce/convert.scm 109 */
																						obj_t BgL_arg1371z00_2419;
																						obj_t BgL_arg1375z00_2420;

																						BgL_arg1371z00_2419 =
																							BGl_shapez00zztools_shapez00(
																							((obj_t) BgL_toz00_56));
																						BgL_arg1375z00_2420 =
																							BGl_shapez00zztools_shapez00(
																							((obj_t) BgL_fromz00_55));
																						BgL_arg1370z00_2418 =
																							BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																							(BGl_string1989z00zzcoerce_convertz00,
																							BgL_arg1371z00_2419,
																							BgL_arg1375z00_2420);
																					}
																					BGl_userzd2warningzf2locationz20zztools_errorz00
																						(BgL_locz00_57, BgL_arg1654z00_1897,
																						BGl_string1990z00zzcoerce_convertz00,
																						BgL_arg1370z00_2418);
																				}
																			else
																				{	/* Coerce/convert.scm 103 */
																					BFALSE;
																				}
																		}
																		BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
																			((BgL_nodez00_bglt) BgL_nodez00_1896));
																		{	/* Coerce/convert.scm 208 */
																			BgL_sequencez00_bglt BgL_new1133z00_1898;

																			{	/* Coerce/convert.scm 208 */
																				BgL_sequencez00_bglt
																					BgL_new1131z00_1906;
																				BgL_new1131z00_1906 =
																					((BgL_sequencez00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_sequencez00_bgl))));
																				{	/* Coerce/convert.scm 208 */
																					long BgL_arg1681z00_1907;

																					BgL_arg1681z00_1907 =
																						BGL_CLASS_NUM
																						(BGl_sequencez00zzast_nodez00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1131z00_1906),
																						BgL_arg1681z00_1907);
																				}
																				{	/* Coerce/convert.scm 208 */
																					BgL_objectz00_bglt BgL_tmpz00_3276;

																					BgL_tmpz00_3276 =
																						((BgL_objectz00_bglt)
																						BgL_new1131z00_1906);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_3276, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1131z00_1906);
																				BgL_new1133z00_1898 =
																					BgL_new1131z00_1906;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1133z00_1898)))->
																					BgL_locz00) =
																				((obj_t) BFALSE), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1133z00_1898)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_toz00_56),
																				BUNSPEC);
																			((((BgL_nodezf2effectzf2_bglt)
																						COBJECT(((BgL_nodezf2effectzf2_bglt)
																								BgL_new1133z00_1898)))->
																					BgL_sidezd2effectzd2) =
																				((obj_t) BUNSPEC), BUNSPEC);
																			((((BgL_nodezf2effectzf2_bglt)
																						COBJECT(((BgL_nodezf2effectzf2_bglt)
																								BgL_new1133z00_1898)))->
																					BgL_keyz00) =
																				((obj_t) BINT(-1L)), BUNSPEC);
																			{
																				obj_t BgL_auxz00_3289;

																				{	/* Coerce/convert.scm 211 */
																					BgL_nodez00_bglt BgL_arg1661z00_1899;
																					BgL_literalz00_bglt
																						BgL_arg1663z00_1900;
																					BgL_arg1661z00_1899 =
																						BGl_coercez12z12zzcoerce_coercez00((
																							(BgL_nodez00_bglt)
																							BgL_nodez00_1896), BUNSPEC,
																						BgL_fromz00_55, ((bool_t) 0));
																					{	/* Coerce/convert.scm 212 */
																						BgL_literalz00_bglt
																							BgL_new1135z00_1903;
																						{	/* Coerce/convert.scm 212 */
																							BgL_literalz00_bglt
																								BgL_new1134z00_1904;
																							BgL_new1134z00_1904 =
																								((BgL_literalz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_literalz00_bgl))));
																							{	/* Coerce/convert.scm 212 */
																								long BgL_arg1678z00_1905;

																								{	/* Coerce/convert.scm 212 */
																									obj_t BgL_classz00_2425;

																									BgL_classz00_2425 =
																										BGl_literalz00zzast_nodez00;
																									BgL_arg1678z00_1905 =
																										BGL_CLASS_NUM
																										(BgL_classz00_2425);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1134z00_1904),
																									BgL_arg1678z00_1905);
																							}
																							{	/* Coerce/convert.scm 212 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_3296;
																								BgL_tmpz00_3296 =
																									((BgL_objectz00_bglt)
																									BgL_new1134z00_1904);
																								BGL_OBJECT_WIDENING_SET
																									(BgL_tmpz00_3296, BFALSE);
																							}
																							((BgL_objectz00_bglt)
																								BgL_new1134z00_1904);
																							BgL_new1135z00_1903 =
																								BgL_new1134z00_1904;
																						}
																						((((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_new1135z00_1903)))->
																								BgL_locz00) =
																							((obj_t) BFALSE), BUNSPEC);
																						((((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_new1135z00_1903)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) BgL_toz00_56),
																							BUNSPEC);
																						((((BgL_atomz00_bglt)
																									COBJECT(((BgL_atomz00_bglt)
																											BgL_new1135z00_1903)))->
																								BgL_valuez00) =
																							((obj_t) BCHAR(((unsigned char)
																										'\000'))), BUNSPEC);
																						BgL_arg1663z00_1900 =
																							BgL_new1135z00_1903;
																					}
																					{	/* Coerce/convert.scm 210 */
																						obj_t BgL_list1664z00_1901;

																						{	/* Coerce/convert.scm 210 */
																							obj_t BgL_arg1675z00_1902;

																							BgL_arg1675z00_1902 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_arg1663z00_1900),
																								BNIL);
																							BgL_list1664z00_1901 =
																								MAKE_YOUNG_PAIR(((obj_t)
																									BgL_arg1661z00_1899),
																								BgL_arg1675z00_1902);
																						}
																						BgL_auxz00_3289 =
																							BgL_list1664z00_1901;
																				}}
																				((((BgL_sequencez00_bglt)
																							COBJECT(BgL_new1133z00_1898))->
																						BgL_nodesz00) =
																					((obj_t) BgL_auxz00_3289), BUNSPEC);
																			}
																			((((BgL_sequencez00_bglt)
																						COBJECT(BgL_new1133z00_1898))->
																					BgL_unsafez00) =
																				((bool_t) ((bool_t) 0)), BUNSPEC);
																			((((BgL_sequencez00_bglt)
																						COBJECT(BgL_new1133z00_1898))->
																					BgL_metaz00) =
																				((obj_t) BNIL), BUNSPEC);
																			return ((obj_t) BgL_new1133z00_1898);
																		}
																	}
																else
																	{	/* Coerce/convert.scm 204 */
																		if (
																			(((obj_t) BgL_toz00_56) ==
																				BGl_za2scharza2z00zztype_cachez00))
																			{	/* Coerce/convert.scm 214 */
																				obj_t BgL_nodez00_1909;

																				BgL_nodez00_1909 =
																					BGl_runtimezd2typezd2errorz00zzcoerce_convertz00
																					(BgL_locz00_57,
																					(((BgL_typez00_bglt)
																							COBJECT(BgL_toz00_56))->
																						BgL_idz00), BgL_nodez00_58);
																				{	/* Coerce/convert.scm 215 */
																					obj_t BgL_arg1689z00_1910;

																					BgL_arg1689z00_1910 =
																						BGl_currentzd2functionzd2zztools_errorz00
																						();
																					if (CBOOL
																						(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
																						{	/* Coerce/convert.scm 109 */
																							obj_t BgL_arg1370z00_2431;

																							{	/* Coerce/convert.scm 109 */
																								obj_t BgL_arg1371z00_2432;
																								obj_t BgL_arg1375z00_2433;

																								BgL_arg1371z00_2432 =
																									BGl_shapez00zztools_shapez00(
																									((obj_t) BgL_toz00_56));
																								BgL_arg1375z00_2433 =
																									BGl_shapez00zztools_shapez00(
																									((obj_t) BgL_fromz00_55));
																								BgL_arg1370z00_2431 =
																									BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																									(BGl_string1989z00zzcoerce_convertz00,
																									BgL_arg1371z00_2432,
																									BgL_arg1375z00_2433);
																							}
																							BGl_userzd2warningzf2locationz20zztools_errorz00
																								(BgL_locz00_57,
																								BgL_arg1689z00_1910,
																								BGl_string1990z00zzcoerce_convertz00,
																								BgL_arg1370z00_2431);
																						}
																					else
																						{	/* Coerce/convert.scm 103 */
																							BFALSE;
																						}
																				}
																				BGl_lvtypezd2nodez12zc0zzast_lvtypez00(
																					((BgL_nodez00_bglt)
																						BgL_nodez00_1909));
																				{	/* Coerce/convert.scm 217 */
																					BgL_sequencez00_bglt
																						BgL_new1137z00_1911;
																					{	/* Coerce/convert.scm 217 */
																						BgL_sequencez00_bglt
																							BgL_new1136z00_1919;
																						BgL_new1136z00_1919 =
																							((BgL_sequencez00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_sequencez00_bgl))));
																						{	/* Coerce/convert.scm 217 */
																							long BgL_arg1701z00_1920;

																							BgL_arg1701z00_1920 =
																								BGL_CLASS_NUM
																								(BGl_sequencez00zzast_nodez00);
																							BGL_OBJECT_CLASS_NUM_SET((
																									(BgL_objectz00_bglt)
																									BgL_new1136z00_1919),
																								BgL_arg1701z00_1920);
																						}
																						{	/* Coerce/convert.scm 217 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_3335;
																							BgL_tmpz00_3335 =
																								((BgL_objectz00_bglt)
																								BgL_new1136z00_1919);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_3335, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1136z00_1919);
																						BgL_new1137z00_1911 =
																							BgL_new1136z00_1919;
																					}
																					((((BgL_nodez00_bglt) COBJECT(
																									((BgL_nodez00_bglt)
																										BgL_new1137z00_1911)))->
																							BgL_locz00) =
																						((obj_t) BFALSE), BUNSPEC);
																					((((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_new1137z00_1911)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2charza2z00zztype_cachez00)),
																						BUNSPEC);
																					((((BgL_nodezf2effectzf2_bglt)
																								COBJECT((
																										(BgL_nodezf2effectzf2_bglt)
																										BgL_new1137z00_1911)))->
																							BgL_sidezd2effectzd2) =
																						((obj_t) BUNSPEC), BUNSPEC);
																					((((BgL_nodezf2effectzf2_bglt)
																								COBJECT((
																										(BgL_nodezf2effectzf2_bglt)
																										BgL_new1137z00_1911)))->
																							BgL_keyz00) =
																						((obj_t) BINT(-1L)), BUNSPEC);
																					{
																						obj_t BgL_auxz00_3349;

																						{	/* Coerce/convert.scm 220 */
																							BgL_nodez00_bglt
																								BgL_arg1691z00_1912;
																							BgL_literalz00_bglt
																								BgL_arg1692z00_1913;
																							BgL_arg1691z00_1912 =
																								BGl_coercez12z12zzcoerce_coercez00
																								(((BgL_nodez00_bglt)
																									BgL_nodez00_1909), BUNSPEC,
																								BgL_fromz00_55, ((bool_t) 0));
																							{	/* Coerce/convert.scm 221 */
																								BgL_literalz00_bglt
																									BgL_new1139z00_1916;
																								{	/* Coerce/convert.scm 221 */
																									BgL_literalz00_bglt
																										BgL_new1138z00_1917;
																									BgL_new1138z00_1917 =
																										((BgL_literalz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_literalz00_bgl))));
																									{	/* Coerce/convert.scm 221 */
																										long BgL_arg1700z00_1918;

																										{	/* Coerce/convert.scm 221 */
																											obj_t BgL_classz00_2438;

																											BgL_classz00_2438 =
																												BGl_literalz00zzast_nodez00;
																											BgL_arg1700z00_1918 =
																												BGL_CLASS_NUM
																												(BgL_classz00_2438);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1138z00_1917),
																											BgL_arg1700z00_1918);
																									}
																									{	/* Coerce/convert.scm 221 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_3356;
																										BgL_tmpz00_3356 =
																											((BgL_objectz00_bglt)
																											BgL_new1138z00_1917);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_3356, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1138z00_1917);
																									BgL_new1139z00_1916 =
																										BgL_new1138z00_1917;
																								}
																								((((BgL_nodez00_bglt) COBJECT(
																												((BgL_nodez00_bglt)
																													BgL_new1139z00_1916)))->
																										BgL_locz00) =
																									((obj_t) BFALSE), BUNSPEC);
																								((((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_new1139z00_1916)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt) (
																											(BgL_typez00_bglt)
																											BGl_za2charza2z00zztype_cachez00)),
																									BUNSPEC);
																								((((BgL_atomz00_bglt)
																											COBJECT((
																													(BgL_atomz00_bglt)
																													BgL_new1139z00_1916)))->
																										BgL_valuez00) =
																									((obj_t) BCHAR(((unsigned
																													char) '\000'))),
																									BUNSPEC);
																								BgL_arg1692z00_1913 =
																									BgL_new1139z00_1916;
																							}
																							{	/* Coerce/convert.scm 219 */
																								obj_t BgL_list1693z00_1914;

																								{	/* Coerce/convert.scm 219 */
																									obj_t BgL_arg1699z00_1915;

																									BgL_arg1699z00_1915 =
																										MAKE_YOUNG_PAIR(
																										((obj_t)
																											BgL_arg1692z00_1913),
																										BNIL);
																									BgL_list1693z00_1914 =
																										MAKE_YOUNG_PAIR(((obj_t)
																											BgL_arg1691z00_1912),
																										BgL_arg1699z00_1915);
																								}
																								BgL_auxz00_3349 =
																									BgL_list1693z00_1914;
																						}}
																						((((BgL_sequencez00_bglt)
																									COBJECT
																									(BgL_new1137z00_1911))->
																								BgL_nodesz00) =
																							((obj_t) BgL_auxz00_3349),
																							BUNSPEC);
																					}
																					((((BgL_sequencez00_bglt)
																								COBJECT(BgL_new1137z00_1911))->
																							BgL_unsafez00) =
																						((bool_t) ((bool_t) 0)), BUNSPEC);
																					((((BgL_sequencez00_bglt)
																								COBJECT(BgL_new1137z00_1911))->
																							BgL_metaz00) =
																						((obj_t) BNIL), BUNSPEC);
																					return ((obj_t) BgL_new1137z00_1911);
																				}
																			}
																		else
																			{	/* Coerce/convert.scm 213 */
																				if (
																					(((obj_t) BgL_toz00_56) ==
																						BGl_za2stringza2z00zztype_cachez00))
																					{	/* Coerce/convert.scm 223 */
																						obj_t BgL_nodez00_1922;

																						BgL_nodez00_1922 =
																							BGl_runtimezd2typezd2errorz00zzcoerce_convertz00
																							(BgL_locz00_57,
																							(((BgL_typez00_bglt)
																									COBJECT(BgL_toz00_56))->
																								BgL_idz00), BgL_nodez00_58);
																						{	/* Coerce/convert.scm 224 */
																							obj_t BgL_arg1703z00_1923;

																							BgL_arg1703z00_1923 =
																								BGl_currentzd2functionzd2zztools_errorz00
																								();
																							if (CBOOL
																								(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
																								{	/* Coerce/convert.scm 109 */
																									obj_t BgL_arg1370z00_2444;

																									{	/* Coerce/convert.scm 109 */
																										obj_t BgL_arg1371z00_2445;
																										obj_t BgL_arg1375z00_2446;

																										BgL_arg1371z00_2445 =
																											BGl_shapez00zztools_shapez00
																											(((obj_t) BgL_toz00_56));
																										BgL_arg1375z00_2446 =
																											BGl_shapez00zztools_shapez00
																											(((obj_t)
																												BgL_fromz00_55));
																										BgL_arg1370z00_2444 =
																											BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																											(BGl_string1989z00zzcoerce_convertz00,
																											BgL_arg1371z00_2445,
																											BgL_arg1375z00_2446);
																									}
																									BGl_userzd2warningzf2locationz20zztools_errorz00
																										(BgL_locz00_57,
																										BgL_arg1703z00_1923,
																										BGl_string1990z00zzcoerce_convertz00,
																										BgL_arg1370z00_2444);
																								}
																							else
																								{	/* Coerce/convert.scm 103 */
																									BFALSE;
																								}
																						}
																						BGl_lvtypezd2nodez12zc0zzast_lvtypez00
																							(((BgL_nodez00_bglt)
																								BgL_nodez00_1922));
																						{	/* Coerce/convert.scm 226 */
																							BgL_sequencez00_bglt
																								BgL_new1141z00_1924;
																							{	/* Coerce/convert.scm 226 */
																								BgL_sequencez00_bglt
																									BgL_new1140z00_1932;
																								BgL_new1140z00_1932 =
																									((BgL_sequencez00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_sequencez00_bgl))));
																								{	/* Coerce/convert.scm 226 */
																									long BgL_arg1714z00_1933;

																									BgL_arg1714z00_1933 =
																										BGL_CLASS_NUM
																										(BGl_sequencez00zzast_nodez00);
																									BGL_OBJECT_CLASS_NUM_SET((
																											(BgL_objectz00_bglt)
																											BgL_new1140z00_1932),
																										BgL_arg1714z00_1933);
																								}
																								{	/* Coerce/convert.scm 226 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_3396;
																									BgL_tmpz00_3396 =
																										((BgL_objectz00_bglt)
																										BgL_new1140z00_1932);
																									BGL_OBJECT_WIDENING_SET
																										(BgL_tmpz00_3396, BFALSE);
																								}
																								((BgL_objectz00_bglt)
																									BgL_new1140z00_1932);
																								BgL_new1141z00_1924 =
																									BgL_new1140z00_1932;
																							}
																							((((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												BgL_new1141z00_1924)))->
																									BgL_locz00) =
																								((obj_t) BFALSE), BUNSPEC);
																							((((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												BgL_new1141z00_1924)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt)
																									BgL_toz00_56), BUNSPEC);
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1141z00_1924)))->
																									BgL_sidezd2effectzd2) =
																								((obj_t) BUNSPEC), BUNSPEC);
																							((((BgL_nodezf2effectzf2_bglt)
																										COBJECT((
																												(BgL_nodezf2effectzf2_bglt)
																												BgL_new1141z00_1924)))->
																									BgL_keyz00) =
																								((obj_t) BINT(-1L)), BUNSPEC);
																							{
																								obj_t BgL_auxz00_3409;

																								{	/* Coerce/convert.scm 229 */
																									BgL_nodez00_bglt
																										BgL_arg1705z00_1925;
																									BgL_literalz00_bglt
																										BgL_arg1708z00_1926;
																									BgL_arg1705z00_1925 =
																										BGl_coercez12z12zzcoerce_coercez00
																										(((BgL_nodez00_bglt)
																											BgL_nodez00_1922),
																										BUNSPEC, BgL_fromz00_55,
																										((bool_t) 0));
																									{	/* Coerce/convert.scm 230 */
																										BgL_literalz00_bglt
																											BgL_new1143z00_1929;
																										{	/* Coerce/convert.scm 230 */
																											BgL_literalz00_bglt
																												BgL_new1142z00_1930;
																											BgL_new1142z00_1930 =
																												((BgL_literalz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_literalz00_bgl))));
																											{	/* Coerce/convert.scm 230 */
																												long
																													BgL_arg1711z00_1931;
																												{	/* Coerce/convert.scm 230 */
																													obj_t
																														BgL_classz00_2451;
																													BgL_classz00_2451 =
																														BGl_literalz00zzast_nodez00;
																													BgL_arg1711z00_1931 =
																														BGL_CLASS_NUM
																														(BgL_classz00_2451);
																												}
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1142z00_1930),
																													BgL_arg1711z00_1931);
																											}
																											{	/* Coerce/convert.scm 230 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_3416;
																												BgL_tmpz00_3416 =
																													((BgL_objectz00_bglt)
																													BgL_new1142z00_1930);
																												BGL_OBJECT_WIDENING_SET
																													(BgL_tmpz00_3416,
																													BFALSE);
																											}
																											((BgL_objectz00_bglt)
																												BgL_new1142z00_1930);
																											BgL_new1143z00_1929 =
																												BgL_new1142z00_1930;
																										}
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1143z00_1929)))->
																												BgL_locz00) =
																											((obj_t) BFALSE),
																											BUNSPEC);
																										((((BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															BgL_new1143z00_1929)))->
																												BgL_typez00) =
																											((BgL_typez00_bglt)
																												BgL_toz00_56), BUNSPEC);
																										((((BgL_atomz00_bglt)
																													COBJECT((
																															(BgL_atomz00_bglt)
																															BgL_new1143z00_1929)))->
																												BgL_valuez00) =
																											((obj_t)
																												BGl_string1989z00zzcoerce_convertz00),
																											BUNSPEC);
																										BgL_arg1708z00_1926 =
																											BgL_new1143z00_1929;
																									}
																									{	/* Coerce/convert.scm 228 */
																										obj_t BgL_list1709z00_1927;

																										{	/* Coerce/convert.scm 228 */
																											obj_t BgL_arg1710z00_1928;

																											BgL_arg1710z00_1928 =
																												MAKE_YOUNG_PAIR(
																												((obj_t)
																													BgL_arg1708z00_1926),
																												BNIL);
																											BgL_list1709z00_1927 =
																												MAKE_YOUNG_PAIR(((obj_t)
																													BgL_arg1705z00_1925),
																												BgL_arg1710z00_1928);
																										}
																										BgL_auxz00_3409 =
																											BgL_list1709z00_1927;
																								}}
																								((((BgL_sequencez00_bglt)
																											COBJECT
																											(BgL_new1141z00_1924))->
																										BgL_nodesz00) =
																									((obj_t) BgL_auxz00_3409),
																									BUNSPEC);
																							}
																							((((BgL_sequencez00_bglt)
																										COBJECT
																										(BgL_new1141z00_1924))->
																									BgL_unsafez00) =
																								((bool_t) ((bool_t) 0)),
																								BUNSPEC);
																							((((BgL_sequencez00_bglt)
																										COBJECT
																										(BgL_new1141z00_1924))->
																									BgL_metaz00) =
																								((obj_t) BNIL), BUNSPEC);
																							return ((obj_t)
																								BgL_new1141z00_1924);
																						}
																					}
																				else
																					{	/* Coerce/convert.scm 232 */
																						obj_t BgL_arg1718z00_1935;

																						BgL_arg1718z00_1935 =
																							BGl_currentzd2functionzd2zztools_errorz00
																							();
																						{	/* Coerce/convert.scm 97 */
																							obj_t BgL_arg1361z00_2456;

																							{	/* Coerce/convert.scm 97 */
																								obj_t BgL_arg1364z00_2457;
																								obj_t BgL_arg1367z00_2458;

																								BgL_arg1364z00_2457 =
																									BGl_shapez00zztools_shapez00(
																									((obj_t) BgL_toz00_56));
																								BgL_arg1367z00_2458 =
																									BGl_shapez00zztools_shapez00(
																									((obj_t) BgL_fromz00_55));
																								BgL_arg1361z00_2456 =
																									BGl_bigloozd2typezd2errorzd2msgzd2zz__errorz00
																									(BGl_string1989z00zzcoerce_convertz00,
																									BgL_arg1364z00_2457,
																									BgL_arg1367z00_2458);
																							}
																							return
																								BGl_userzd2errorzf2locationz20zztools_errorz00
																								(BgL_locz00_57,
																								BgL_arg1718z00_1935,
																								BGl_string1990z00zzcoerce_convertz00,
																								BgL_arg1361z00_2456, BNIL);
																						}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* convert! */
	BGL_EXPORTED_DEF BgL_nodez00_bglt
		BGl_convertz12z12zzcoerce_convertz00(BgL_nodez00_bglt BgL_nodez00_59,
		BgL_typez00_bglt BgL_fromz00_60, BgL_typez00_bglt BgL_toz00_61,
		bool_t BgL_safez00_62)
	{
		{	/* Coerce/convert.scm 242 */
			if ((((obj_t) BgL_fromz00_60) == ((obj_t) BgL_toz00_61)))
				{	/* Coerce/convert.scm 246 */
					return BgL_nodez00_59;
				}
			else
				{	/* Coerce/convert.scm 248 */
					BgL_typez00_bglt BgL_toz00_1936;
					BgL_typez00_bglt BgL_froz00_1937;

					BgL_toz00_1936 =
						BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_toz00_61);
					BgL_froz00_1937 =
						BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_fromz00_60);
					{	/* Coerce/convert.scm 250 */
						bool_t BgL_test2052z00_3447;

						if ((((obj_t) BgL_froz00_1937) == ((obj_t) BgL_toz00_1936)))
							{	/* Coerce/convert.scm 250 */
								BgL_test2052z00_3447 = ((bool_t) 1);
							}
						else
							{	/* Coerce/convert.scm 250 */
								BgL_test2052z00_3447 =
									(((BgL_typez00_bglt) COBJECT(BgL_froz00_1937))->
									BgL_magiczf3zf3);
							}
						if (BgL_test2052z00_3447)
							{	/* Coerce/convert.scm 250 */
								return BgL_nodez00_59;
							}
						else
							{	/* Coerce/convert.scm 252 */
								obj_t BgL_coercerz00_1939;
								obj_t BgL_locz00_1940;

								BgL_coercerz00_1939 =
									BGl_findzd2coercerzd2zztype_coercionz00(BgL_froz00_1937,
									BgL_toz00_1936);
								BgL_locz00_1940 =
									(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_59))->BgL_locz00);
								{	/* Coerce/convert.scm 254 */
									bool_t BgL_test2054z00_3455;

									if (STRUCTP(BgL_coercerz00_1939))
										{	/* Coerce/convert.scm 254 */
											BgL_test2054z00_3455 =
												(STRUCT_KEY(BgL_coercerz00_1939) == CNST_TABLE_REF(8));
										}
									else
										{	/* Coerce/convert.scm 254 */
											BgL_test2054z00_3455 = ((bool_t) 0);
										}
									if (BgL_test2054z00_3455)
										{	/* Coerce/convert.scm 258 */
											obj_t BgL_g1144z00_1942;
											obj_t BgL_g1145z00_1943;

											BgL_g1144z00_1942 =
												STRUCT_REF(BgL_coercerz00_1939, (int) (2L));
											BgL_g1145z00_1943 =
												STRUCT_REF(BgL_coercerz00_1939, (int) (3L));
											{
												obj_t BgL_checksz00_1945;
												obj_t BgL_coercesz00_1946;
												BgL_nodez00_bglt BgL_nodez00_1947;

												{
													obj_t BgL_auxz00_3465;

													BgL_checksz00_1945 = BgL_g1144z00_1942;
													BgL_coercesz00_1946 = BgL_g1145z00_1943;
													BgL_nodez00_1947 = BgL_nodez00_59;
												BgL_zc3z04anonymousza31721ze3z87_1948:
													if (NULLP(BgL_checksz00_1945))
														{	/* Coerce/convert.scm 262 */
															if (NULLP(BgL_coercesz00_1946))
																{	/* Coerce/convert.scm 263 */
																	BgL_auxz00_3465 = ((obj_t) BgL_nodez00_1947);
																}
															else
																{	/* Coerce/convert.scm 266 */
																	obj_t BgL_arg1733z00_1951;
																	obj_t BgL_arg1734z00_1952;

																	BgL_arg1733z00_1951 =
																		BGl_shapez00zztools_shapez00(
																		((obj_t) BgL_fromz00_60));
																	BgL_arg1734z00_1952 =
																		BGl_shapez00zztools_shapez00(
																		((obj_t) BgL_toz00_1936));
																	BgL_auxz00_3465 =
																		BGl_internalzd2errorzd2zztools_errorz00
																		(BGl_string1992z00zzcoerce_convertz00,
																		BgL_arg1733z00_1951, BgL_arg1734z00_1952);
																}
														}
													else
														{	/* Coerce/convert.scm 262 */
															if (NULLP(BgL_coercesz00_1946))
																{	/* Coerce/convert.scm 270 */
																	obj_t BgL_arg1737z00_1954;
																	obj_t BgL_arg1738z00_1955;

																	BgL_arg1737z00_1954 =
																		BGl_shapez00zztools_shapez00(
																		((obj_t) BgL_fromz00_60));
																	BgL_arg1738z00_1955 =
																		BGl_shapez00zztools_shapez00(
																		((obj_t) BgL_toz00_1936));
																	BgL_auxz00_3465 =
																		BGl_internalzd2errorzd2zztools_errorz00
																		(BGl_string1992z00zzcoerce_convertz00,
																		BgL_arg1737z00_1954, BgL_arg1738z00_1955);
																}
															else
																{	/* Coerce/convert.scm 273 */
																	obj_t BgL_arg1739z00_1956;
																	obj_t BgL_arg1740z00_1957;
																	BgL_nodez00_bglt BgL_arg1746z00_1958;

																	BgL_arg1739z00_1956 =
																		CDR(((obj_t) BgL_checksz00_1945));
																	BgL_arg1740z00_1957 =
																		CDR(((obj_t) BgL_coercesz00_1946));
																	{	/* Coerce/convert.scm 275 */
																		obj_t BgL_arg1747z00_1959;
																		obj_t BgL_arg1748z00_1960;
																		obj_t BgL_arg1749z00_1961;
																		obj_t BgL_arg1750z00_1962;

																		{	/* Coerce/convert.scm 275 */
																			obj_t BgL_pairz00_2473;

																			BgL_pairz00_2473 =
																				CAR(((obj_t) BgL_checksz00_1945));
																			BgL_arg1747z00_1959 =
																				CDR(BgL_pairz00_2473);
																		}
																		{	/* Coerce/convert.scm 276 */
																			obj_t BgL_pairz00_2477;

																			BgL_pairz00_2477 =
																				CAR(((obj_t) BgL_coercesz00_1946));
																			BgL_arg1748z00_1960 =
																				CDR(BgL_pairz00_2477);
																		}
																		{	/* Coerce/convert.scm 277 */
																			obj_t BgL_pairz00_2481;

																			BgL_pairz00_2481 =
																				CAR(((obj_t) BgL_checksz00_1945));
																			BgL_arg1749z00_1961 =
																				CAR(BgL_pairz00_2481);
																		}
																		{	/* Coerce/convert.scm 278 */
																			obj_t BgL_pairz00_2485;

																			BgL_pairz00_2485 =
																				CAR(((obj_t) BgL_coercesz00_1946));
																			BgL_arg1750z00_1962 =
																				CAR(BgL_pairz00_2485);
																		}
																		BgL_arg1746z00_1958 =
																			BGl_makezd2onezd2conversionz00zzcoerce_convertz00
																			(BgL_arg1747z00_1959, BgL_arg1748z00_1960,
																			BgL_arg1749z00_1961, BgL_arg1750z00_1962,
																			BgL_nodez00_1947, BgL_safez00_62);
																	}
																	{
																		BgL_nodez00_bglt BgL_nodez00_3502;
																		obj_t BgL_coercesz00_3501;
																		obj_t BgL_checksz00_3500;

																		BgL_checksz00_3500 = BgL_arg1739z00_1956;
																		BgL_coercesz00_3501 = BgL_arg1740z00_1957;
																		BgL_nodez00_3502 = BgL_arg1746z00_1958;
																		BgL_nodez00_1947 = BgL_nodez00_3502;
																		BgL_coercesz00_1946 = BgL_coercesz00_3501;
																		BgL_checksz00_1945 = BgL_checksz00_3500;
																		goto BgL_zc3z04anonymousza31721ze3z87_1948;
																	}
																}
														}
													return ((BgL_nodez00_bglt) BgL_auxz00_3465);
												}
											}
										}
									else
										{	/* Coerce/convert.scm 254 */
											return
												((BgL_nodez00_bglt)
												BGl_convertzd2errorzd2zzcoerce_convertz00
												(BgL_froz00_1937, BgL_toz00_1936, BgL_locz00_1940,
													BgL_nodez00_59));
										}
								}
							}
					}
				}
		}

	}



/* &convert! */
	BgL_nodez00_bglt BGl_z62convertz12z70zzcoerce_convertz00(obj_t
		BgL_envz00_2768, obj_t BgL_nodez00_2769, obj_t BgL_fromz00_2770,
		obj_t BgL_toz00_2771, obj_t BgL_safez00_2772)
	{
		{	/* Coerce/convert.scm 242 */
			return
				BGl_convertz12z12zzcoerce_convertz00(
				((BgL_nodez00_bglt) BgL_nodez00_2769),
				((BgL_typez00_bglt) BgL_fromz00_2770),
				((BgL_typez00_bglt) BgL_toz00_2771), CBOOL(BgL_safez00_2772));
		}

	}



/* make-one-conversion */
	BgL_nodez00_bglt BGl_makezd2onezd2conversionz00zzcoerce_convertz00(obj_t
		BgL_fromz00_63, obj_t BgL_toz00_64, obj_t BgL_checkopz00_65,
		obj_t BgL_coerceopz00_66, BgL_nodez00_bglt BgL_nodez00_67,
		bool_t BgL_safez00_68)
	{
		{	/* Coerce/convert.scm 291 */
			{	/* Coerce/convert.scm 292 */
				bool_t BgL_test2059z00_3511;

				if ((BgL_checkopz00_65 == BTRUE))
					{	/* Coerce/convert.scm 292 */
						BgL_test2059z00_3511 = ((bool_t) 1);
					}
				else
					{	/* Coerce/convert.scm 292 */
						if (BgL_safez00_68)
							{	/* Coerce/convert.scm 292 */
								BgL_test2059z00_3511 = ((bool_t) 0);
							}
						else
							{	/* Coerce/convert.scm 292 */
								BgL_test2059z00_3511 = ((bool_t) 1);
							}
					}
				if (BgL_test2059z00_3511)
					{	/* Coerce/convert.scm 292 */
						return
							BGl_dozd2convertzd2zzcoerce_convertz00(BgL_coerceopz00_66,
							BgL_nodez00_67, ((BgL_typez00_bglt) BgL_fromz00_63),
							((BgL_typez00_bglt) BgL_toz00_64));
					}
				else
					{	/* Coerce/convert.scm 294 */
						bool_t BgL_test2062z00_3518;

						{	/* Coerce/convert.scm 294 */
							obj_t BgL_classz00_2486;

							BgL_classz00_2486 = BGl_tclassz00zzobject_classz00;
							if (BGL_OBJECTP(BgL_fromz00_63))
								{	/* Coerce/convert.scm 294 */
									BgL_objectz00_bglt BgL_arg1807z00_2488;

									BgL_arg1807z00_2488 = (BgL_objectz00_bglt) (BgL_fromz00_63);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Coerce/convert.scm 294 */
											long BgL_idxz00_2494;

											BgL_idxz00_2494 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2488);
											BgL_test2062z00_3518 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2494 + 2L)) == BgL_classz00_2486);
										}
									else
										{	/* Coerce/convert.scm 294 */
											bool_t BgL_res1982z00_2519;

											{	/* Coerce/convert.scm 294 */
												obj_t BgL_oclassz00_2502;

												{	/* Coerce/convert.scm 294 */
													obj_t BgL_arg1815z00_2510;
													long BgL_arg1816z00_2511;

													BgL_arg1815z00_2510 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Coerce/convert.scm 294 */
														long BgL_arg1817z00_2512;

														BgL_arg1817z00_2512 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2488);
														BgL_arg1816z00_2511 =
															(BgL_arg1817z00_2512 - OBJECT_TYPE);
													}
													BgL_oclassz00_2502 =
														VECTOR_REF(BgL_arg1815z00_2510,
														BgL_arg1816z00_2511);
												}
												{	/* Coerce/convert.scm 294 */
													bool_t BgL__ortest_1115z00_2503;

													BgL__ortest_1115z00_2503 =
														(BgL_classz00_2486 == BgL_oclassz00_2502);
													if (BgL__ortest_1115z00_2503)
														{	/* Coerce/convert.scm 294 */
															BgL_res1982z00_2519 = BgL__ortest_1115z00_2503;
														}
													else
														{	/* Coerce/convert.scm 294 */
															long BgL_odepthz00_2504;

															{	/* Coerce/convert.scm 294 */
																obj_t BgL_arg1804z00_2505;

																BgL_arg1804z00_2505 = (BgL_oclassz00_2502);
																BgL_odepthz00_2504 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2505);
															}
															if ((2L < BgL_odepthz00_2504))
																{	/* Coerce/convert.scm 294 */
																	obj_t BgL_arg1802z00_2507;

																	{	/* Coerce/convert.scm 294 */
																		obj_t BgL_arg1803z00_2508;

																		BgL_arg1803z00_2508 = (BgL_oclassz00_2502);
																		BgL_arg1802z00_2507 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2508, 2L);
																	}
																	BgL_res1982z00_2519 =
																		(BgL_arg1802z00_2507 == BgL_classz00_2486);
																}
															else
																{	/* Coerce/convert.scm 294 */
																	BgL_res1982z00_2519 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2062z00_3518 = BgL_res1982z00_2519;
										}
								}
							else
								{	/* Coerce/convert.scm 294 */
									BgL_test2062z00_3518 = ((bool_t) 0);
								}
						}
						if (BgL_test2062z00_3518)
							{	/* Coerce/convert.scm 294 */
								return
									BGl_makezd2onezd2classzd2conversionzd2zzcoerce_convertz00
									(BgL_fromz00_63, BgL_toz00_64, BgL_checkopz00_65,
									BgL_coerceopz00_66, BgL_nodez00_67);
							}
						else
							{	/* Coerce/convert.scm 294 */
								return
									BGl_makezd2onezd2typezd2conversionzd2zzcoerce_convertz00
									(BgL_fromz00_63, BgL_toz00_64, BgL_checkopz00_65,
									BgL_coerceopz00_66, BgL_nodez00_67);
							}
					}
			}
		}

	}



/* skip-let-var */
	BgL_nodez00_bglt BGl_skipzd2letzd2varz00zzcoerce_convertz00(BgL_nodez00_bglt
		BgL_nodez00_69)
	{
		{	/* Coerce/convert.scm 301 */
		BGl_skipzd2letzd2varz00zzcoerce_convertz00:
			{	/* Coerce/convert.scm 302 */
				bool_t BgL_test2067z00_3543;

				{	/* Coerce/convert.scm 302 */
					obj_t BgL_classz00_2520;

					BgL_classz00_2520 = BGl_letzd2varzd2zzast_nodez00;
					{	/* Coerce/convert.scm 302 */
						BgL_objectz00_bglt BgL_arg1807z00_2522;

						{	/* Coerce/convert.scm 302 */
							obj_t BgL_tmpz00_3544;

							BgL_tmpz00_3544 = ((obj_t) ((BgL_objectz00_bglt) BgL_nodez00_69));
							BgL_arg1807z00_2522 = (BgL_objectz00_bglt) (BgL_tmpz00_3544);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Coerce/convert.scm 302 */
								long BgL_idxz00_2528;

								BgL_idxz00_2528 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2522);
								BgL_test2067z00_3543 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2528 + 3L)) == BgL_classz00_2520);
							}
						else
							{	/* Coerce/convert.scm 302 */
								bool_t BgL_res1983z00_2553;

								{	/* Coerce/convert.scm 302 */
									obj_t BgL_oclassz00_2536;

									{	/* Coerce/convert.scm 302 */
										obj_t BgL_arg1815z00_2544;
										long BgL_arg1816z00_2545;

										BgL_arg1815z00_2544 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Coerce/convert.scm 302 */
											long BgL_arg1817z00_2546;

											BgL_arg1817z00_2546 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2522);
											BgL_arg1816z00_2545 = (BgL_arg1817z00_2546 - OBJECT_TYPE);
										}
										BgL_oclassz00_2536 =
											VECTOR_REF(BgL_arg1815z00_2544, BgL_arg1816z00_2545);
									}
									{	/* Coerce/convert.scm 302 */
										bool_t BgL__ortest_1115z00_2537;

										BgL__ortest_1115z00_2537 =
											(BgL_classz00_2520 == BgL_oclassz00_2536);
										if (BgL__ortest_1115z00_2537)
											{	/* Coerce/convert.scm 302 */
												BgL_res1983z00_2553 = BgL__ortest_1115z00_2537;
											}
										else
											{	/* Coerce/convert.scm 302 */
												long BgL_odepthz00_2538;

												{	/* Coerce/convert.scm 302 */
													obj_t BgL_arg1804z00_2539;

													BgL_arg1804z00_2539 = (BgL_oclassz00_2536);
													BgL_odepthz00_2538 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2539);
												}
												if ((3L < BgL_odepthz00_2538))
													{	/* Coerce/convert.scm 302 */
														obj_t BgL_arg1802z00_2541;

														{	/* Coerce/convert.scm 302 */
															obj_t BgL_arg1803z00_2542;

															BgL_arg1803z00_2542 = (BgL_oclassz00_2536);
															BgL_arg1802z00_2541 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2542,
																3L);
														}
														BgL_res1983z00_2553 =
															(BgL_arg1802z00_2541 == BgL_classz00_2520);
													}
												else
													{	/* Coerce/convert.scm 302 */
														BgL_res1983z00_2553 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test2067z00_3543 = BgL_res1983z00_2553;
							}
					}
				}
				if (BgL_test2067z00_3543)
					{	/* Coerce/convert.scm 303 */
						BgL_nodez00_bglt BgL_arg1754z00_1967;

						BgL_arg1754z00_1967 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_69)))->BgL_bodyz00);
						{
							BgL_nodez00_bglt BgL_nodez00_3569;

							BgL_nodez00_3569 = BgL_arg1754z00_1967;
							BgL_nodez00_69 = BgL_nodez00_3569;
							goto BGl_skipzd2letzd2varz00zzcoerce_convertz00;
						}
					}
				else
					{	/* Coerce/convert.scm 302 */
						return BgL_nodez00_69;
					}
			}
		}

	}



/* make-one-type-conversion */
	BgL_nodez00_bglt
		BGl_makezd2onezd2typezd2conversionzd2zzcoerce_convertz00(obj_t
		BgL_fromz00_70, obj_t BgL_toz00_71, obj_t BgL_checkzd2opzd2_72,
		obj_t BgL_coercezd2opzd2_73, BgL_nodez00_bglt BgL_nodez00_74)
	{
		{	/* Coerce/convert.scm 309 */
			{	/* Coerce/convert.scm 312 */
				obj_t BgL_auxz00_1968;

				BgL_auxz00_1968 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(5)));
				{	/* Coerce/convert.scm 312 */
					obj_t BgL_locz00_1969;

					BgL_locz00_1969 =
						(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_74))->BgL_locz00);
					{	/* Coerce/convert.scm 313 */
						BgL_nodez00_bglt BgL_lnodez00_1970;

						{	/* Coerce/convert.scm 321 */
							obj_t BgL_arg1775z00_1984;

							{	/* Coerce/convert.scm 321 */
								obj_t BgL_arg1798z00_1985;

								{	/* Coerce/convert.scm 321 */
									obj_t BgL_arg1799z00_1986;
									obj_t BgL_arg1805z00_1987;

									{	/* Coerce/convert.scm 321 */
										obj_t BgL_arg1806z00_1988;

										{	/* Coerce/convert.scm 321 */
											obj_t BgL_arg1808z00_1989;
											obj_t BgL_arg1812z00_1990;

											{	/* Coerce/convert.scm 321 */
												obj_t BgL_arg1820z00_1991;

												{	/* Coerce/convert.scm 321 */
													obj_t BgL_arg1822z00_1992;

													BgL_arg1822z00_1992 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_fromz00_70)))->
														BgL_idz00);
													BgL_arg1820z00_1991 =
														BGl_makezd2typedzd2identz00zzast_identz00
														(BgL_auxz00_1968, BgL_arg1822z00_1992);
												}
												BgL_arg1808z00_1989 =
													BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
													(BgL_arg1820z00_1991);
											}
											BgL_arg1812z00_1990 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
											BgL_arg1806z00_1988 =
												MAKE_YOUNG_PAIR(BgL_arg1808z00_1989,
												BgL_arg1812z00_1990);
										}
										BgL_arg1799z00_1986 =
											MAKE_YOUNG_PAIR(BgL_arg1806z00_1988, BNIL);
									}
									{	/* Coerce/convert.scm 323 */
										obj_t BgL_arg1823z00_1993;

										{	/* Coerce/convert.scm 323 */
											obj_t BgL_arg1831z00_1994;

											{	/* Coerce/convert.scm 323 */
												obj_t BgL_arg1832z00_1995;
												obj_t BgL_arg1833z00_1996;

												{	/* Coerce/convert.scm 323 */
													obj_t BgL_arg1834z00_1997;

													BgL_arg1834z00_1997 =
														MAKE_YOUNG_PAIR(BgL_auxz00_1968, BNIL);
													BgL_arg1832z00_1995 =
														MAKE_YOUNG_PAIR(BgL_checkzd2opzd2_72,
														BgL_arg1834z00_1997);
												}
												{	/* Coerce/convert.scm 325 */
													obj_t BgL_arg1835z00_1998;

													{	/* Coerce/convert.scm 325 */
														obj_t BgL_arg1836z00_1999;

														{	/* Coerce/convert.scm 325 */
															obj_t BgL_arg1837z00_2000;

															BgL_arg1837z00_2000 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt) BgL_toz00_71)))->
																BgL_idz00);
															BgL_arg1836z00_1999 =
																BGl_runtimezd2typezd2errorzf2idzf2zzcoerce_convertz00
																(BgL_locz00_1969, BgL_arg1837z00_2000,
																BgL_auxz00_1968);
														}
														BgL_arg1835z00_1998 =
															MAKE_YOUNG_PAIR(BgL_arg1836z00_1999, BNIL);
													}
													BgL_arg1833z00_1996 =
														MAKE_YOUNG_PAIR(BgL_auxz00_1968,
														BgL_arg1835z00_1998);
												}
												BgL_arg1831z00_1994 =
													MAKE_YOUNG_PAIR(BgL_arg1832z00_1995,
													BgL_arg1833z00_1996);
											}
											BgL_arg1823z00_1993 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1831z00_1994);
										}
										BgL_arg1805z00_1987 =
											MAKE_YOUNG_PAIR(BgL_arg1823z00_1993, BNIL);
									}
									BgL_arg1798z00_1985 =
										MAKE_YOUNG_PAIR(BgL_arg1799z00_1986, BgL_arg1805z00_1987);
								}
								BgL_arg1775z00_1984 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1798z00_1985);
							}
							BgL_lnodez00_1970 =
								BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
								(BgL_arg1775z00_1984, BgL_locz00_1969);
						}
						{	/* Coerce/convert.scm 314 */

							BGl_za2checkza2z00zzcoerce_convertz00 =
								(1L + BGl_za2checkza2z00zzcoerce_convertz00);
							if (CBOOL(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
								{	/* Coerce/convert.scm 83 */
									BGl_notifyzd2typezd2testz00zzcoerce_convertz00(BgL_fromz00_70,
										BgL_toz00_71, BgL_locz00_1969);
								}
							else
								{	/* Coerce/convert.scm 83 */
									BFALSE;
								}
							BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00
								(BgL_lnodez00_1970);
							{	/* Coerce/convert.scm 329 */
								obj_t BgL_varz00_1971;

								{	/* Coerce/convert.scm 329 */
									obj_t BgL_pairz00_2561;

									{	/* Coerce/convert.scm 329 */
										obj_t BgL_pairz00_2560;

										BgL_pairz00_2560 =
											(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_lnodez00_1970)))->
											BgL_bindingsz00);
										BgL_pairz00_2561 = CAR(BgL_pairz00_2560);
									}
									BgL_varz00_1971 = CAR(BgL_pairz00_2561);
								}
								{	/* Coerce/convert.scm 329 */
									BgL_nodez00_bglt BgL_coercezd2appzd2_1972;

									{	/* Coerce/convert.scm 331 */
										BgL_refz00_bglt BgL_arg1767z00_1978;

										{	/* Coerce/convert.scm 331 */
											BgL_refz00_bglt BgL_new1147z00_1979;

											{	/* Coerce/convert.scm 332 */
												BgL_refz00_bglt BgL_new1146z00_1980;

												BgL_new1146z00_1980 =
													((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_refz00_bgl))));
												{	/* Coerce/convert.scm 332 */
													long BgL_arg1770z00_1981;

													BgL_arg1770z00_1981 =
														BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1146z00_1980),
														BgL_arg1770z00_1981);
												}
												{	/* Coerce/convert.scm 332 */
													BgL_objectz00_bglt BgL_tmpz00_3609;

													BgL_tmpz00_3609 =
														((BgL_objectz00_bglt) BgL_new1146z00_1980);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3609, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1146z00_1980);
												BgL_new1147z00_1979 = BgL_new1146z00_1980;
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1147z00_1979)))->
													BgL_locz00) = ((obj_t) BgL_locz00_1969), BUNSPEC);
											((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_new1147z00_1979)))->BgL_typez00) =
												((BgL_typez00_bglt)
													BGl_strictzd2nodezd2typez00zzast_nodez00((
															(BgL_typez00_bglt) BgL_toz00_71),
														((BgL_typez00_bglt) BgL_fromz00_70))), BUNSPEC);
											((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_new1147z00_1979)))->BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_varz00_1971)), BUNSPEC);
											BgL_arg1767z00_1978 = BgL_new1147z00_1979;
										}
										BgL_coercezd2appzd2_1972 =
											BGl_dozd2convertzd2zzcoerce_convertz00
											(BgL_coercezd2opzd2_73,
											((BgL_nodez00_bglt) BgL_arg1767z00_1978),
											((BgL_typez00_bglt) BgL_fromz00_70),
											((BgL_typez00_bglt) BgL_toz00_71));
									}
									{	/* Coerce/convert.scm 330 */
										BgL_nodez00_bglt BgL_condnz00_1973;

										BgL_condnz00_1973 =
											BGl_skipzd2letzd2varz00zzcoerce_convertz00
											(BgL_lnodez00_1970);
										{	/* Coerce/convert.scm 336 */

											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_varz00_1971))))->
													BgL_typez00) =
												((BgL_typez00_bglt) ((BgL_typez00_bglt)
														BgL_fromz00_70)), BUNSPEC);
											{	/* Coerce/convert.scm 340 */
												obj_t BgL_arg1755z00_1974;

												{	/* Coerce/convert.scm 340 */
													obj_t BgL_pairz00_2569;

													BgL_pairz00_2569 =
														(((BgL_letzd2varzd2_bglt) COBJECT(
																((BgL_letzd2varzd2_bglt) BgL_lnodez00_1970)))->
														BgL_bindingsz00);
													BgL_arg1755z00_1974 = CAR(BgL_pairz00_2569);
												}
												{	/* Coerce/convert.scm 340 */
													obj_t BgL_auxz00_3637;
													obj_t BgL_tmpz00_3635;

													BgL_auxz00_3637 = ((obj_t) BgL_nodez00_74);
													BgL_tmpz00_3635 = ((obj_t) BgL_arg1755z00_1974);
													SET_CDR(BgL_tmpz00_3635, BgL_auxz00_3637);
											}}
											((((BgL_nodez00_bglt) COBJECT(BgL_nodez00_74))->
													BgL_typez00) =
												((BgL_typez00_bglt) ((BgL_typez00_bglt)
														BgL_fromz00_70)), BUNSPEC);
											((((BgL_conditionalz00_bglt)
														COBJECT(((BgL_conditionalz00_bglt)
																BgL_condnz00_1973)))->BgL_truez00) =
												((BgL_nodez00_bglt) BgL_coercezd2appzd2_1972), BUNSPEC);
											{	/* Coerce/convert.scm 344 */
												BgL_nodez00_bglt BgL_arg1762z00_1976;

												{	/* Coerce/convert.scm 344 */
													BgL_nodez00_bglt BgL_arg1765z00_1977;

													BgL_arg1765z00_1977 =
														(((BgL_conditionalz00_bglt) COBJECT(
																((BgL_conditionalz00_bglt)
																	BgL_condnz00_1973)))->BgL_falsez00);
													BgL_arg1762z00_1976 =
														BGl_coercez12z12zzcoerce_coercez00
														(BgL_arg1765z00_1977, BUNSPEC,
														((BgL_typez00_bglt) BgL_fromz00_70), ((bool_t) 0));
												}
												((((BgL_conditionalz00_bglt) COBJECT(
																((BgL_conditionalz00_bglt)
																	BgL_condnz00_1973)))->BgL_falsez00) =
													((BgL_nodez00_bglt) BgL_arg1762z00_1976), BUNSPEC);
											}
											BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_lnodez00_1970);
											return BgL_lnodez00_1970;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-one-class-conversion */
	BgL_nodez00_bglt
		BGl_makezd2onezd2classzd2conversionzd2zzcoerce_convertz00(obj_t
		BgL_fromz00_75, obj_t BgL_toz00_76, obj_t BgL_checkzd2opzd2_77,
		obj_t BgL_coercezd2opzd2_78, BgL_nodez00_bglt BgL_nodez00_79)
	{
		{	/* Coerce/convert.scm 354 */
			{	/* Coerce/convert.scm 357 */
				bool_t BgL_test2072z00_3651;

				{	/* Coerce/convert.scm 357 */
					bool_t BgL_test2073z00_3652;

					{	/* Coerce/convert.scm 357 */
						obj_t BgL_classz00_2578;

						BgL_classz00_2578 = BGl_tclassz00zzobject_classz00;
						if (BGL_OBJECTP(BgL_toz00_76))
							{	/* Coerce/convert.scm 357 */
								BgL_objectz00_bglt BgL_arg1807z00_2580;

								BgL_arg1807z00_2580 = (BgL_objectz00_bglt) (BgL_toz00_76);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Coerce/convert.scm 357 */
										long BgL_idxz00_2586;

										BgL_idxz00_2586 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2580);
										BgL_test2073z00_3652 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2586 + 2L)) == BgL_classz00_2578);
									}
								else
									{	/* Coerce/convert.scm 357 */
										bool_t BgL_res1984z00_2611;

										{	/* Coerce/convert.scm 357 */
											obj_t BgL_oclassz00_2594;

											{	/* Coerce/convert.scm 357 */
												obj_t BgL_arg1815z00_2602;
												long BgL_arg1816z00_2603;

												BgL_arg1815z00_2602 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Coerce/convert.scm 357 */
													long BgL_arg1817z00_2604;

													BgL_arg1817z00_2604 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2580);
													BgL_arg1816z00_2603 =
														(BgL_arg1817z00_2604 - OBJECT_TYPE);
												}
												BgL_oclassz00_2594 =
													VECTOR_REF(BgL_arg1815z00_2602, BgL_arg1816z00_2603);
											}
											{	/* Coerce/convert.scm 357 */
												bool_t BgL__ortest_1115z00_2595;

												BgL__ortest_1115z00_2595 =
													(BgL_classz00_2578 == BgL_oclassz00_2594);
												if (BgL__ortest_1115z00_2595)
													{	/* Coerce/convert.scm 357 */
														BgL_res1984z00_2611 = BgL__ortest_1115z00_2595;
													}
												else
													{	/* Coerce/convert.scm 357 */
														long BgL_odepthz00_2596;

														{	/* Coerce/convert.scm 357 */
															obj_t BgL_arg1804z00_2597;

															BgL_arg1804z00_2597 = (BgL_oclassz00_2594);
															BgL_odepthz00_2596 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2597);
														}
														if ((2L < BgL_odepthz00_2596))
															{	/* Coerce/convert.scm 357 */
																obj_t BgL_arg1802z00_2599;

																{	/* Coerce/convert.scm 357 */
																	obj_t BgL_arg1803z00_2600;

																	BgL_arg1803z00_2600 = (BgL_oclassz00_2594);
																	BgL_arg1802z00_2599 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2600,
																		2L);
																}
																BgL_res1984z00_2611 =
																	(BgL_arg1802z00_2599 == BgL_classz00_2578);
															}
														else
															{	/* Coerce/convert.scm 357 */
																BgL_res1984z00_2611 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2073z00_3652 = BgL_res1984z00_2611;
									}
							}
						else
							{	/* Coerce/convert.scm 357 */
								BgL_test2073z00_3652 = ((bool_t) 0);
							}
					}
					if (BgL_test2073z00_3652)
						{	/* Coerce/convert.scm 357 */
							BgL_test2072z00_3651 =
								BGl_typezd2subclasszf3z21zzobject_classz00(
								((BgL_typez00_bglt) BgL_fromz00_75),
								((BgL_typez00_bglt) BgL_toz00_76));
						}
					else
						{	/* Coerce/convert.scm 357 */
							BgL_test2072z00_3651 = ((bool_t) 0);
						}
				}
				if (BgL_test2072z00_3651)
					{	/* Coerce/convert.scm 357 */
						return
							BGl_dozd2convertzd2zzcoerce_convertz00(BgL_coercezd2opzd2_78,
							BgL_nodez00_79, ((BgL_typez00_bglt) BgL_fromz00_75),
							((BgL_typez00_bglt) BgL_toz00_76));
					}
				else
					{	/* Coerce/convert.scm 359 */
						obj_t BgL_auxz00_2004;

						BgL_auxz00_2004 =
							BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(5));
						{	/* Coerce/convert.scm 359 */
							obj_t BgL_aux2z00_2005;

							BgL_aux2z00_2005 =
								BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(10));
							{	/* Coerce/convert.scm 360 */
								obj_t BgL_locz00_2006;

								BgL_locz00_2006 =
									(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_79))->BgL_locz00);
								{	/* Coerce/convert.scm 361 */
									BgL_nodez00_bglt BgL_lnodez00_2007;

									{	/* Coerce/convert.scm 364 */
										obj_t BgL_arg1856z00_2031;

										{	/* Coerce/convert.scm 364 */
											obj_t BgL_arg1857z00_2032;

											{	/* Coerce/convert.scm 364 */
												obj_t BgL_arg1858z00_2033;
												obj_t BgL_arg1859z00_2034;

												{	/* Coerce/convert.scm 364 */
													obj_t BgL_arg1860z00_2035;

													{	/* Coerce/convert.scm 364 */
														obj_t BgL_arg1862z00_2036;
														obj_t BgL_arg1863z00_2037;

														{	/* Coerce/convert.scm 364 */
															obj_t BgL_arg1864z00_2038;

															{	/* Coerce/convert.scm 364 */
																obj_t BgL_arg1866z00_2039;

																{	/* Coerce/convert.scm 364 */
																	obj_t BgL_arg1868z00_2040;
																	obj_t BgL_arg1869z00_2041;

																	{	/* Coerce/convert.scm 364 */
																		obj_t BgL_arg1455z00_2614;

																		BgL_arg1455z00_2614 =
																			SYMBOL_TO_STRING(BgL_auxz00_2004);
																		BgL_arg1868z00_2040 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_2614);
																	}
																	{	/* Coerce/convert.scm 364 */
																		obj_t BgL_symbolz00_2615;

																		BgL_symbolz00_2615 = CNST_TABLE_REF(6);
																		{	/* Coerce/convert.scm 364 */
																			obj_t BgL_arg1455z00_2616;

																			BgL_arg1455z00_2616 =
																				SYMBOL_TO_STRING(BgL_symbolz00_2615);
																			BgL_arg1869z00_2041 =
																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																				(BgL_arg1455z00_2616);
																		}
																	}
																	BgL_arg1866z00_2039 =
																		string_append(BgL_arg1868z00_2040,
																		BgL_arg1869z00_2041);
																}
																BgL_arg1864z00_2038 =
																	bstring_to_symbol(BgL_arg1866z00_2039);
															}
															BgL_arg1862z00_2036 =
																BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																(BgL_arg1864z00_2038);
														}
														BgL_arg1863z00_2037 =
															MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
														BgL_arg1860z00_2035 =
															MAKE_YOUNG_PAIR(BgL_arg1862z00_2036,
															BgL_arg1863z00_2037);
													}
													BgL_arg1858z00_2033 =
														MAKE_YOUNG_PAIR(BgL_arg1860z00_2035, BNIL);
												}
												{	/* Coerce/convert.scm 366 */
													obj_t BgL_arg1870z00_2042;

													{	/* Coerce/convert.scm 366 */
														obj_t BgL_arg1872z00_2043;

														{	/* Coerce/convert.scm 366 */
															obj_t BgL_arg1873z00_2044;
															obj_t BgL_arg1874z00_2045;

															{	/* Coerce/convert.scm 366 */
																obj_t BgL_arg1875z00_2046;

																{	/* Coerce/convert.scm 366 */
																	obj_t BgL_arg1876z00_2047;
																	obj_t BgL_arg1877z00_2048;

																	{	/* Coerce/convert.scm 366 */
																		obj_t BgL_arg1878z00_2049;

																		{	/* Coerce/convert.scm 366 */
																			obj_t BgL_arg1879z00_2050;

																			{	/* Coerce/convert.scm 366 */
																				obj_t BgL_arg1880z00_2051;
																				obj_t BgL_arg1882z00_2052;

																				{	/* Coerce/convert.scm 366 */
																					obj_t BgL_arg1455z00_2619;

																					BgL_arg1455z00_2619 =
																						SYMBOL_TO_STRING(BgL_aux2z00_2005);
																					BgL_arg1880z00_2051 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1455z00_2619);
																				}
																				{	/* Coerce/convert.scm 366 */
																					obj_t BgL_symbolz00_2620;

																					BgL_symbolz00_2620 =
																						CNST_TABLE_REF(6);
																					{	/* Coerce/convert.scm 366 */
																						obj_t BgL_arg1455z00_2621;

																						BgL_arg1455z00_2621 =
																							SYMBOL_TO_STRING
																							(BgL_symbolz00_2620);
																						BgL_arg1882z00_2052 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_2621);
																					}
																				}
																				BgL_arg1879z00_2050 =
																					string_append(BgL_arg1880z00_2051,
																					BgL_arg1882z00_2052);
																			}
																			BgL_arg1878z00_2049 =
																				bstring_to_symbol(BgL_arg1879z00_2050);
																		}
																		BgL_arg1876z00_2047 =
																			BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
																			(BgL_arg1878z00_2049);
																	}
																	BgL_arg1877z00_2048 =
																		MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
																	BgL_arg1875z00_2046 =
																		MAKE_YOUNG_PAIR(BgL_arg1876z00_2047,
																		BgL_arg1877z00_2048);
																}
																BgL_arg1873z00_2044 =
																	MAKE_YOUNG_PAIR(BgL_arg1875z00_2046, BNIL);
															}
															{	/* Coerce/convert.scm 367 */
																obj_t BgL_arg1883z00_2053;

																{	/* Coerce/convert.scm 367 */
																	obj_t BgL_arg1884z00_2054;

																	{	/* Coerce/convert.scm 367 */
																		obj_t BgL_arg1885z00_2055;
																		obj_t BgL_arg1887z00_2056;

																		{	/* Coerce/convert.scm 367 */
																			obj_t BgL_arg1888z00_2057;

																			BgL_arg1888z00_2057 =
																				MAKE_YOUNG_PAIR(BgL_aux2z00_2005, BNIL);
																			BgL_arg1885z00_2055 =
																				MAKE_YOUNG_PAIR(BgL_checkzd2opzd2_77,
																				BgL_arg1888z00_2057);
																		}
																		{	/* Coerce/convert.scm 369 */
																			obj_t BgL_arg1889z00_2058;

																			{	/* Coerce/convert.scm 369 */
																				obj_t BgL_arg1890z00_2059;

																				{	/* Coerce/convert.scm 369 */
																					obj_t BgL_arg1891z00_2060;

																					BgL_arg1891z00_2060 =
																						(((BgL_typez00_bglt) COBJECT(
																								((BgL_typez00_bglt)
																									BgL_toz00_76)))->BgL_idz00);
																					BgL_arg1890z00_2059 =
																						BGl_runtimezd2typezd2errorzf2idzf2zzcoerce_convertz00
																						(BgL_locz00_2006,
																						BgL_arg1891z00_2060,
																						BgL_auxz00_2004);
																				}
																				BgL_arg1889z00_2058 =
																					MAKE_YOUNG_PAIR(BgL_arg1890z00_2059,
																					BNIL);
																			}
																			BgL_arg1887z00_2056 =
																				MAKE_YOUNG_PAIR(BgL_auxz00_2004,
																				BgL_arg1889z00_2058);
																		}
																		BgL_arg1884z00_2054 =
																			MAKE_YOUNG_PAIR(BgL_arg1885z00_2055,
																			BgL_arg1887z00_2056);
																	}
																	BgL_arg1883z00_2053 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																		BgL_arg1884z00_2054);
																}
																BgL_arg1874z00_2045 =
																	MAKE_YOUNG_PAIR(BgL_arg1883z00_2053, BNIL);
															}
															BgL_arg1872z00_2043 =
																MAKE_YOUNG_PAIR(BgL_arg1873z00_2044,
																BgL_arg1874z00_2045);
														}
														BgL_arg1870z00_2042 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
															BgL_arg1872z00_2043);
													}
													BgL_arg1859z00_2034 =
														MAKE_YOUNG_PAIR(BgL_arg1870z00_2042, BNIL);
												}
												BgL_arg1857z00_2032 =
													MAKE_YOUNG_PAIR(BgL_arg1858z00_2033,
													BgL_arg1859z00_2034);
											}
											BgL_arg1856z00_2031 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(7), BgL_arg1857z00_2032);
										}
										BgL_lnodez00_2007 =
											BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
											(BgL_arg1856z00_2031, BgL_locz00_2006);
									}
									{	/* Coerce/convert.scm 362 */

										BGl_za2checkza2z00zzcoerce_convertz00 =
											(1L + BGl_za2checkza2z00zzcoerce_convertz00);
										if (CBOOL(BGl_za2warningzd2typesza2zd2zzengine_paramz00))
											{	/* Coerce/convert.scm 83 */
												BGl_notifyzd2typezd2testz00zzcoerce_convertz00
													(BgL_fromz00_75, BgL_toz00_76, BgL_locz00_2006);
											}
										else
											{	/* Coerce/convert.scm 83 */
												BFALSE;
											}
										BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00
											(BgL_lnodez00_2007);
										{	/* Coerce/convert.scm 373 */
											obj_t BgL_varz00_2008;

											{	/* Coerce/convert.scm 373 */
												obj_t BgL_pairz00_2627;

												{	/* Coerce/convert.scm 373 */
													obj_t BgL_pairz00_2626;

													BgL_pairz00_2626 =
														(((BgL_letzd2varzd2_bglt) COBJECT(
																((BgL_letzd2varzd2_bglt) BgL_lnodez00_2007)))->
														BgL_bindingsz00);
													BgL_pairz00_2627 = CAR(BgL_pairz00_2626);
												}
												BgL_varz00_2008 = CAR(BgL_pairz00_2627);
											}
											{	/* Coerce/convert.scm 373 */
												BgL_nodez00_bglt BgL_coercezd2appzd2_2009;

												{	/* Coerce/convert.scm 375 */
													BgL_refz00_bglt BgL_arg1851z00_2025;

													{	/* Coerce/convert.scm 375 */
														BgL_refz00_bglt BgL_new1149z00_2026;

														{	/* Coerce/convert.scm 376 */
															BgL_refz00_bglt BgL_new1148z00_2027;

															BgL_new1148z00_2027 =
																((BgL_refz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_refz00_bgl))));
															{	/* Coerce/convert.scm 376 */
																long BgL_arg1852z00_2028;

																BgL_arg1852z00_2028 =
																	BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1148z00_2027),
																	BgL_arg1852z00_2028);
															}
															{	/* Coerce/convert.scm 376 */
																BgL_objectz00_bglt BgL_tmpz00_3740;

																BgL_tmpz00_3740 =
																	((BgL_objectz00_bglt) BgL_new1148z00_2027);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3740,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_new1148z00_2027);
															BgL_new1149z00_2026 = BgL_new1148z00_2027;
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1149z00_2026)))->
																BgL_locz00) =
															((obj_t) BgL_locz00_2006), BUNSPEC);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_new1149z00_2026)))->BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BgL_fromz00_75)), BUNSPEC);
														((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																			BgL_new1149z00_2026)))->BgL_variablez00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	BgL_varz00_2008)), BUNSPEC);
														BgL_arg1851z00_2025 = BgL_new1149z00_2026;
													}
													BgL_coercezd2appzd2_2009 =
														BGl_dozd2convertzd2zzcoerce_convertz00
														(BgL_coercezd2opzd2_78,
														((BgL_nodez00_bglt) BgL_arg1851z00_2025),
														((BgL_typez00_bglt) BgL_fromz00_75),
														((BgL_typez00_bglt) BgL_toz00_76));
												}
												{	/* Coerce/convert.scm 374 */
													BgL_nodez00_bglt BgL_condnz00_2010;

													BgL_condnz00_2010 =
														BGl_skipzd2letzd2varz00zzcoerce_convertz00
														(BgL_lnodez00_2007);
													{	/* Coerce/convert.scm 380 */

														((((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_varz00_2008))))->
																BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BgL_fromz00_75)), BUNSPEC);
														{	/* Coerce/convert.scm 384 */
															obj_t BgL_arg1842z00_2011;

															{	/* Coerce/convert.scm 384 */
																obj_t BgL_pairz00_2635;

																BgL_pairz00_2635 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_lnodez00_2007)))->BgL_bindingsz00);
																BgL_arg1842z00_2011 = CAR(BgL_pairz00_2635);
															}
															{	/* Coerce/convert.scm 384 */
																obj_t BgL_auxz00_3766;
																obj_t BgL_tmpz00_3764;

																BgL_auxz00_3766 = ((obj_t) BgL_nodez00_79);
																BgL_tmpz00_3764 = ((obj_t) BgL_arg1842z00_2011);
																SET_CDR(BgL_tmpz00_3764, BgL_auxz00_3766);
														}}
														{	/* Coerce/convert.scm 385 */
															obj_t BgL_binding2z00_2013;

															{	/* Coerce/convert.scm 385 */
																obj_t BgL_pairz00_2639;

																BgL_pairz00_2639 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				(((BgL_letzd2varzd2_bglt) COBJECT(
																							((BgL_letzd2varzd2_bglt)
																								BgL_lnodez00_2007)))->
																					BgL_bodyz00))))->BgL_bindingsz00);
																BgL_binding2z00_2013 = CAR(BgL_pairz00_2639);
															}
															{	/* Coerce/convert.scm 386 */
																BgL_castz00_bglt BgL_arg1844z00_2014;

																{	/* Coerce/convert.scm 386 */
																	BgL_castz00_bglt BgL_new1151z00_2015;

																	{	/* Coerce/convert.scm 387 */
																		BgL_castz00_bglt BgL_new1150z00_2019;

																		BgL_new1150z00_2019 =
																			((BgL_castz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_castz00_bgl))));
																		{	/* Coerce/convert.scm 387 */
																			long BgL_arg1846z00_2020;

																			BgL_arg1846z00_2020 =
																				BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1150z00_2019),
																				BgL_arg1846z00_2020);
																		}
																		{	/* Coerce/convert.scm 387 */
																			BgL_objectz00_bglt BgL_tmpz00_3778;

																			BgL_tmpz00_3778 =
																				((BgL_objectz00_bglt)
																				BgL_new1150z00_2019);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3778,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1150z00_2019);
																		BgL_new1151z00_2015 = BgL_new1150z00_2019;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1151z00_2015)))->
																			BgL_locz00) =
																		((obj_t) BgL_locz00_2006), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1151z00_2015)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2objza2z00zztype_cachez00)),
																		BUNSPEC);
																	{
																		BgL_nodez00_bglt BgL_auxz00_3787;

																		{	/* Coerce/convert.scm 389 */
																			BgL_refz00_bglt BgL_new1153z00_2016;

																			{	/* Coerce/convert.scm 390 */
																				BgL_refz00_bglt BgL_new1152z00_2017;

																				BgL_new1152z00_2017 =
																					((BgL_refz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_refz00_bgl))));
																				{	/* Coerce/convert.scm 390 */
																					long BgL_arg1845z00_2018;

																					{	/* Coerce/convert.scm 390 */
																						obj_t BgL_classz00_2644;

																						BgL_classz00_2644 =
																							BGl_refz00zzast_nodez00;
																						BgL_arg1845z00_2018 =
																							BGL_CLASS_NUM(BgL_classz00_2644);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1152z00_2017),
																						BgL_arg1845z00_2018);
																				}
																				{	/* Coerce/convert.scm 390 */
																					BgL_objectz00_bglt BgL_tmpz00_3792;

																					BgL_tmpz00_3792 =
																						((BgL_objectz00_bglt)
																						BgL_new1152z00_2017);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_3792, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1152z00_2017);
																				BgL_new1153z00_2016 =
																					BgL_new1152z00_2017;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1153z00_2016)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_2006), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1153z00_2016)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BgL_fromz00_75)), BUNSPEC);
																			((((BgL_varz00_bglt)
																						COBJECT(((BgL_varz00_bglt)
																								BgL_new1153z00_2016)))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) (
																						(BgL_variablez00_bglt)
																						BgL_varz00_2008)), BUNSPEC);
																			BgL_auxz00_3787 =
																				((BgL_nodez00_bglt)
																				BgL_new1153z00_2016);
																		}
																		((((BgL_castz00_bglt)
																					COBJECT(BgL_new1151z00_2015))->
																				BgL_argz00) =
																			((BgL_nodez00_bglt) BgL_auxz00_3787),
																			BUNSPEC);
																	}
																	BgL_arg1844z00_2014 = BgL_new1151z00_2015;
																}
																{	/* Coerce/convert.scm 386 */
																	obj_t BgL_auxz00_3808;
																	obj_t BgL_tmpz00_3806;

																	BgL_auxz00_3808 =
																		((obj_t) BgL_arg1844z00_2014);
																	BgL_tmpz00_3806 =
																		((obj_t) BgL_binding2z00_2013);
																	SET_CDR(BgL_tmpz00_3806, BgL_auxz00_3808);
														}}}
														((((BgL_conditionalz00_bglt) COBJECT(
																		((BgL_conditionalz00_bglt)
																			BgL_condnz00_2010)))->BgL_truez00) =
															((BgL_nodez00_bglt) BgL_coercezd2appzd2_2009),
															BUNSPEC);
														{	/* Coerce/convert.scm 395 */
															BgL_nodez00_bglt BgL_arg1849z00_2023;

															{	/* Coerce/convert.scm 395 */
																BgL_nodez00_bglt BgL_arg1850z00_2024;

																BgL_arg1850z00_2024 =
																	(((BgL_conditionalz00_bglt) COBJECT(
																			((BgL_conditionalz00_bglt)
																				BgL_condnz00_2010)))->BgL_falsez00);
																BgL_arg1849z00_2023 =
																	BGl_coercez12z12zzcoerce_coercez00
																	(BgL_arg1850z00_2024, BUNSPEC,
																	((BgL_typez00_bglt) BgL_fromz00_75),
																	((bool_t) 0));
															}
															((((BgL_conditionalz00_bglt) COBJECT(
																			((BgL_conditionalz00_bglt)
																				BgL_condnz00_2010)))->BgL_falsez00) =
																((BgL_nodez00_bglt) BgL_arg1849z00_2023),
																BUNSPEC);
														}
														BGl_lvtypezd2nodez12zc0zzast_lvtypez00
															(BgL_lnodez00_2007);
														return BgL_lnodez00_2007;
													}
												}
											}
										}
									}
								}
							}
						}
					}
			}
		}

	}



/* do-convert */
	BgL_nodez00_bglt BGl_dozd2convertzd2zzcoerce_convertz00(obj_t
		BgL_coercezd2opzd2_80, BgL_nodez00_bglt BgL_nodez00_81,
		BgL_typez00_bglt BgL_fromz00_82, BgL_typez00_bglt BgL_toz00_83)
	{
		{	/* Coerce/convert.scm 405 */
			if ((BgL_coercezd2opzd2_80 == BTRUE))
				{	/* Coerce/convert.scm 409 */
					bool_t BgL_test2080z00_3822;

					{	/* Coerce/convert.scm 409 */
						obj_t BgL_arg1896z00_2067;

						BgL_arg1896z00_2067 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_test2080z00_3822 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1896z00_2067)))->
							BgL_strictzd2typezd2castz00);
					}
					if (BgL_test2080z00_3822)
						{	/* Coerce/convert.scm 416 */
							BgL_castz00_bglt BgL_new1156z00_2064;

							{	/* Coerce/convert.scm 417 */
								BgL_castz00_bglt BgL_new1154z00_2065;

								BgL_new1154z00_2065 =
									((BgL_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_castz00_bgl))));
								{	/* Coerce/convert.scm 417 */
									long BgL_arg1894z00_2066;

									BgL_arg1894z00_2066 = BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1154z00_2065),
										BgL_arg1894z00_2066);
								}
								{	/* Coerce/convert.scm 417 */
									BgL_objectz00_bglt BgL_tmpz00_3830;

									BgL_tmpz00_3830 = ((BgL_objectz00_bglt) BgL_new1154z00_2065);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3830, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1154z00_2065);
								BgL_new1156z00_2064 = BgL_new1154z00_2065;
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1156z00_2064)))->BgL_locz00) =
								((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_81))->
										BgL_locz00)), BUNSPEC);
							((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_new1156z00_2064)))->BgL_typez00) =
								((BgL_typez00_bglt) BgL_toz00_83), BUNSPEC);
							((((BgL_castz00_bglt) COBJECT(BgL_new1156z00_2064))->BgL_argz00) =
								((BgL_nodez00_bglt) BgL_nodez00_81), BUNSPEC);
							return ((BgL_nodez00_bglt) BgL_new1156z00_2064);
						}
					else
						{	/* Coerce/convert.scm 409 */
							return BgL_nodez00_81;
						}
				}
			else
				{	/* Coerce/convert.scm 421 */
					BgL_nodez00_bglt BgL_nnodez00_2068;

					{	/* Coerce/convert.scm 421 */
						obj_t BgL_arg1897z00_2069;
						obj_t BgL_arg1898z00_2070;

						{	/* Coerce/convert.scm 421 */
							obj_t BgL_arg1899z00_2071;

							BgL_arg1899z00_2071 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_nodez00_81), BNIL);
							BgL_arg1897z00_2069 =
								MAKE_YOUNG_PAIR(BgL_coercezd2opzd2_80, BgL_arg1899z00_2071);
						}
						BgL_arg1898z00_2070 =
							(((BgL_nodez00_bglt) COBJECT(BgL_nodez00_81))->BgL_locz00);
						BgL_nnodez00_2068 =
							BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
							(BgL_arg1897z00_2069, BgL_arg1898z00_2070);
					}
					BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nnodez00_2068);
					BGl_spreadzd2sidezd2effectz12z12zzeffect_spreadz00(BgL_nnodez00_2068);
					return BgL_nnodez00_2068;
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 16 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 16 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcoerce_convertz00(void)
	{
		{	/* Coerce/convert.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
			return
				BGl_modulezd2initializa7ationz75zzeffect_spreadz00(348216764L,
				BSTRING_TO_STRING(BGl_string1993z00zzcoerce_convertz00));
		}

	}

#ifdef __cplusplus
}
#endif
