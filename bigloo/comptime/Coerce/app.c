/*===========================================================================*/
/*   (Coerce/app.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Coerce/app.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_COERCE_APP_TYPE_DEFINITIONS
#define BGL_COERCE_APP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;


#endif													// BGL_COERCE_APP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzcoerce_appz00 = BUNSPEC;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzcoerce_appz00(void);
	static BgL_nodez00_bglt
		BGl_coercezd2foreignzd2vazd2appz12zc0zzcoerce_appz00(BgL_valuez00_bglt,
		BgL_variablez00_bglt, obj_t, BgL_appz00_bglt, obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcoerce_appz00(void);
	static obj_t BGl_objectzd2initzd2zzcoerce_appz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_typez00zztype_typez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcoerce_appz00(void);
	static BgL_nodez00_bglt
		BGl_coercezd2bigloozd2externzd2appz12zc0zzcoerce_appz00
		(BgL_variablez00_bglt, obj_t, BgL_appz00_bglt, obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	static bool_t BGl_coercezd2argsz12ze70z27zzcoerce_appz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_coercezd2bigloozd2appz12z12zzcoerce_appz00(BgL_variablez00_bglt, obj_t,
		BgL_appz00_bglt, obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	static BgL_nodez00_bglt
		BGl_coercezd2foreignzd2appz12z12zzcoerce_appz00(BgL_variablez00_bglt, obj_t,
		BgL_appz00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcoerce_appz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_convertz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzcoerce_appz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_appz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzcoerce_appz00(void);
	static BgL_nodez00_bglt BGl_z62coercez12zd2app1232za2zzcoerce_appz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcoerce_appz00(void);
	static BgL_nodez00_bglt
		BGl_coercezd2foreignzd2fxzd2appz12zc0zzcoerce_appz00(BgL_valuez00_bglt,
		BgL_variablez00_bglt, obj_t, BgL_appz00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_coercezd2bigloozd2internzd2appz12zc0zzcoerce_appz00
		(BgL_variablez00_bglt, obj_t, BgL_appz00_bglt, obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_convertz12z12zzcoerce_convertz00(BgL_nodez00_bglt,
		BgL_typez00_bglt, BgL_typez00_bglt, bool_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_makezd2procedurezd2idsz00zzcoerce_appz00 = BUNSPEC;
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1734z00zzcoerce_appz00,
		BgL_bgl_string1734za700za7za7c1747za7, "procedure", 9);
	      DEFINE_STRING(BGl_string1735z00zzcoerce_appz00,
		BgL_bgl_string1735za700za7za7c1748za7, "app", 3);
	      DEFINE_STRING(BGl_string1736z00zzcoerce_appz00,
		BgL_bgl_string1736za700za7za7c1749za7, "formals/actuals mismatch", 24);
	      DEFINE_STRING(BGl_string1738z00zzcoerce_appz00,
		BgL_bgl_string1738za700za7za7c1750za7, "coerce!", 7);
	      DEFINE_STRING(BGl_string1739z00zzcoerce_appz00,
		BgL_bgl_string1739za700za7za7c1751za7, "coerce_app", 10);
	      DEFINE_STRING(BGl_string1740z00zzcoerce_appz00,
		BgL_bgl_string1740za700za7za7c1752za7,
		"import (make-fx-procedure make-va-procedure make-l-procedure) ", 62);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1737z00zzcoerce_appz00,
		BgL_bgl_za762coerceza712za7d2a1753za7,
		BGl_z62coercez12zd2app1232za2zzcoerce_appz00, 0L, BUNSPEC, 4);
	extern obj_t BGl_coercez12zd2envzc0zzcoerce_coercez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcoerce_appz00));
		     ADD_ROOT((void *) (&BGl_makezd2procedurezd2idsz00zzcoerce_appz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcoerce_appz00(long
		BgL_checksumz00_2198, char *BgL_fromz00_2199)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcoerce_appz00))
				{
					BGl_requirezd2initializa7ationz75zzcoerce_appz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcoerce_appz00();
					BGl_libraryzd2moduleszd2initz00zzcoerce_appz00();
					BGl_cnstzd2initzd2zzcoerce_appz00();
					BGl_importedzd2moduleszd2initz00zzcoerce_appz00();
					BGl_methodzd2initzd2zzcoerce_appz00();
					return BGl_toplevelzd2initzd2zzcoerce_appz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcoerce_appz00(void)
	{
		{	/* Coerce/app.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "coerce_app");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"coerce_app");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "coerce_app");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "coerce_app");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "coerce_app");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "coerce_app");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"coerce_app");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "coerce_app");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"coerce_app");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcoerce_appz00(void)
	{
		{	/* Coerce/app.scm 15 */
			{	/* Coerce/app.scm 15 */
				obj_t BgL_cportz00_2154;

				{	/* Coerce/app.scm 15 */
					obj_t BgL_stringz00_2161;

					BgL_stringz00_2161 = BGl_string1740z00zzcoerce_appz00;
					{	/* Coerce/app.scm 15 */
						obj_t BgL_startz00_2162;

						BgL_startz00_2162 = BINT(0L);
						{	/* Coerce/app.scm 15 */
							obj_t BgL_endz00_2163;

							BgL_endz00_2163 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2161)));
							{	/* Coerce/app.scm 15 */

								BgL_cportz00_2154 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2161, BgL_startz00_2162, BgL_endz00_2163);
				}}}}
				{
					long BgL_iz00_2155;

					BgL_iz00_2155 = 1L;
				BgL_loopz00_2156:
					if ((BgL_iz00_2155 == -1L))
						{	/* Coerce/app.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Coerce/app.scm 15 */
							{	/* Coerce/app.scm 15 */
								obj_t BgL_arg1746z00_2157;

								{	/* Coerce/app.scm 15 */

									{	/* Coerce/app.scm 15 */
										obj_t BgL_locationz00_2159;

										BgL_locationz00_2159 = BBOOL(((bool_t) 0));
										{	/* Coerce/app.scm 15 */

											BgL_arg1746z00_2157 =
												BGl_readz00zz__readerz00(BgL_cportz00_2154,
												BgL_locationz00_2159);
										}
									}
								}
								{	/* Coerce/app.scm 15 */
									int BgL_tmpz00_2227;

									BgL_tmpz00_2227 = (int) (BgL_iz00_2155);
									CNST_TABLE_SET(BgL_tmpz00_2227, BgL_arg1746z00_2157);
							}}
							{	/* Coerce/app.scm 15 */
								int BgL_auxz00_2160;

								BgL_auxz00_2160 = (int) ((BgL_iz00_2155 - 1L));
								{
									long BgL_iz00_2232;

									BgL_iz00_2232 = (long) (BgL_auxz00_2160);
									BgL_iz00_2155 = BgL_iz00_2232;
									goto BgL_loopz00_2156;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcoerce_appz00(void)
	{
		{	/* Coerce/app.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcoerce_appz00(void)
	{
		{	/* Coerce/app.scm 15 */
			return (BGl_makezd2procedurezd2idsz00zzcoerce_appz00 =
				CNST_TABLE_REF(0), BUNSPEC);
		}

	}



/* coerce-foreign-app! */
	BgL_nodez00_bglt
		BGl_coercezd2foreignzd2appz12z12zzcoerce_appz00(BgL_variablez00_bglt
		BgL_calleez00_7, obj_t BgL_callerz00_8, BgL_appz00_bglt BgL_nodez00_9,
		obj_t BgL_toz00_10, obj_t BgL_safez00_11)
	{
		{	/* Coerce/app.scm 42 */
			{	/* Coerce/app.scm 45 */
				BgL_valuez00_bglt BgL_ffunz00_1388;

				BgL_ffunz00_1388 =
					(((BgL_variablez00_bglt) COBJECT(BgL_calleez00_7))->BgL_valuez00);
				{	/* Coerce/app.scm 45 */
					long BgL_arityz00_1389;

					BgL_arityz00_1389 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_ffunz00_1388)))->BgL_arityz00);
					{	/* Coerce/app.scm 46 */

						if ((BgL_arityz00_1389 >= 0L))
							{	/* Coerce/app.scm 47 */
								return
									BGl_coercezd2foreignzd2fxzd2appz12zc0zzcoerce_appz00
									(BgL_ffunz00_1388, BgL_calleez00_7, BgL_callerz00_8,
									BgL_nodez00_9, BgL_toz00_10, BgL_safez00_11);
							}
						else
							{	/* Coerce/app.scm 47 */
								return
									BGl_coercezd2foreignzd2vazd2appz12zc0zzcoerce_appz00
									(BgL_ffunz00_1388, BgL_calleez00_7, BgL_callerz00_8,
									BgL_nodez00_9, BgL_toz00_10, BgL_safez00_11);
							}
					}
				}
			}
		}

	}



/* coerce-foreign-fx-app! */
	BgL_nodez00_bglt
		BGl_coercezd2foreignzd2fxzd2appz12zc0zzcoerce_appz00(BgL_valuez00_bglt
		BgL_funz00_12, BgL_variablez00_bglt BgL_calleez00_13,
		obj_t BgL_callerz00_14, BgL_appz00_bglt BgL_nodez00_15, obj_t BgL_toz00_16,
		obj_t BgL_safez00_17)
	{
		{	/* Coerce/app.scm 60 */
			{
				obj_t BgL_argsz00_1417;
				obj_t BgL_typesz00_1418;
				obj_t BgL_locz00_1419;

				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_calleez00_13))))->BgL_idz00),
							CNST_TABLE_REF(0))))
					{	/* Coerce/app.scm 82 */
						obj_t BgL_arg1242z00_1396;
						obj_t BgL_arg1244z00_1397;
						obj_t BgL_arg1248z00_1398;

						BgL_arg1242z00_1396 =
							(((BgL_appz00_bglt) COBJECT(BgL_nodez00_15))->BgL_argsz00);
						BgL_arg1244z00_1397 =
							(((BgL_cfunz00_bglt) COBJECT(
									((BgL_cfunz00_bglt) BgL_funz00_12)))->BgL_argszd2typezd2);
						BgL_arg1248z00_1398 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_nodez00_15)))->BgL_locz00);
						BgL_argsz00_1417 = BgL_arg1242z00_1396;
						BgL_typesz00_1418 = BgL_arg1244z00_1397;
						BgL_locz00_1419 = BgL_arg1248z00_1398;
						{	/* Coerce/app.scm 68 */
							obj_t BgL_arg1310z00_1421;
							obj_t BgL_arg1311z00_1422;

							BgL_arg1310z00_1421 = CDR(((obj_t) BgL_argsz00_1417));
							BgL_arg1311z00_1422 = CDR(((obj_t) BgL_typesz00_1418));
							BGl_coercezd2argsz12ze70z27zzcoerce_appz00(BgL_safez00_17,
								BgL_callerz00_14, BgL_arg1310z00_1421, BgL_arg1311z00_1422);
						}
						{	/* Coerce/app.scm 69 */
							obj_t BgL_cloz00_1423;

							BgL_cloz00_1423 = CAR(((obj_t) BgL_argsz00_1417));
							{	/* Coerce/app.scm 70 */
								bool_t BgL_test1759z00_2262;

								{	/* Coerce/app.scm 70 */
									obj_t BgL_classz00_1713;

									BgL_classz00_1713 = BGl_varz00zzast_nodez00;
									if (BGL_OBJECTP(BgL_cloz00_1423))
										{	/* Coerce/app.scm 70 */
											BgL_objectz00_bglt BgL_arg1807z00_1715;

											BgL_arg1807z00_1715 =
												(BgL_objectz00_bglt) (BgL_cloz00_1423);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Coerce/app.scm 70 */
													long BgL_idxz00_1721;

													BgL_idxz00_1721 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1715);
													BgL_test1759z00_2262 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1721 + 2L)) == BgL_classz00_1713);
												}
											else
												{	/* Coerce/app.scm 70 */
													bool_t BgL_res1719z00_1746;

													{	/* Coerce/app.scm 70 */
														obj_t BgL_oclassz00_1729;

														{	/* Coerce/app.scm 70 */
															obj_t BgL_arg1815z00_1737;
															long BgL_arg1816z00_1738;

															BgL_arg1815z00_1737 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Coerce/app.scm 70 */
																long BgL_arg1817z00_1739;

																BgL_arg1817z00_1739 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1715);
																BgL_arg1816z00_1738 =
																	(BgL_arg1817z00_1739 - OBJECT_TYPE);
															}
															BgL_oclassz00_1729 =
																VECTOR_REF(BgL_arg1815z00_1737,
																BgL_arg1816z00_1738);
														}
														{	/* Coerce/app.scm 70 */
															bool_t BgL__ortest_1115z00_1730;

															BgL__ortest_1115z00_1730 =
																(BgL_classz00_1713 == BgL_oclassz00_1729);
															if (BgL__ortest_1115z00_1730)
																{	/* Coerce/app.scm 70 */
																	BgL_res1719z00_1746 =
																		BgL__ortest_1115z00_1730;
																}
															else
																{	/* Coerce/app.scm 70 */
																	long BgL_odepthz00_1731;

																	{	/* Coerce/app.scm 70 */
																		obj_t BgL_arg1804z00_1732;

																		BgL_arg1804z00_1732 = (BgL_oclassz00_1729);
																		BgL_odepthz00_1731 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1732);
																	}
																	if ((2L < BgL_odepthz00_1731))
																		{	/* Coerce/app.scm 70 */
																			obj_t BgL_arg1802z00_1734;

																			{	/* Coerce/app.scm 70 */
																				obj_t BgL_arg1803z00_1735;

																				BgL_arg1803z00_1735 =
																					(BgL_oclassz00_1729);
																				BgL_arg1802z00_1734 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1735, 2L);
																			}
																			BgL_res1719z00_1746 =
																				(BgL_arg1802z00_1734 ==
																				BgL_classz00_1713);
																		}
																	else
																		{	/* Coerce/app.scm 70 */
																			BgL_res1719z00_1746 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1759z00_2262 = BgL_res1719z00_1746;
												}
										}
									else
										{	/* Coerce/app.scm 70 */
											BgL_test1759z00_2262 = ((bool_t) 0);
										}
								}
								if (BgL_test1759z00_2262)
									{	/* Coerce/app.scm 71 */
										bool_t BgL_test1765z00_2285;

										{	/* Coerce/app.scm 71 */
											BgL_valuez00_bglt BgL_arg1323z00_1434;

											BgL_arg1323z00_1434 =
												(((BgL_variablez00_bglt) COBJECT(
														(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_cloz00_1423)))->
															BgL_variablez00)))->BgL_valuez00);
											{	/* Coerce/app.scm 71 */
												obj_t BgL_classz00_1749;

												BgL_classz00_1749 = BGl_sfunz00zzast_varz00;
												{	/* Coerce/app.scm 71 */
													BgL_objectz00_bglt BgL_arg1807z00_1751;

													{	/* Coerce/app.scm 71 */
														obj_t BgL_tmpz00_2289;

														BgL_tmpz00_2289 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg1323z00_1434));
														BgL_arg1807z00_1751 =
															(BgL_objectz00_bglt) (BgL_tmpz00_2289);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Coerce/app.scm 71 */
															long BgL_idxz00_1757;

															BgL_idxz00_1757 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1751);
															BgL_test1765z00_2285 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_1757 + 3L)) == BgL_classz00_1749);
														}
													else
														{	/* Coerce/app.scm 71 */
															bool_t BgL_res1720z00_1782;

															{	/* Coerce/app.scm 71 */
																obj_t BgL_oclassz00_1765;

																{	/* Coerce/app.scm 71 */
																	obj_t BgL_arg1815z00_1773;
																	long BgL_arg1816z00_1774;

																	BgL_arg1815z00_1773 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Coerce/app.scm 71 */
																		long BgL_arg1817z00_1775;

																		BgL_arg1817z00_1775 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1751);
																		BgL_arg1816z00_1774 =
																			(BgL_arg1817z00_1775 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_1765 =
																		VECTOR_REF(BgL_arg1815z00_1773,
																		BgL_arg1816z00_1774);
																}
																{	/* Coerce/app.scm 71 */
																	bool_t BgL__ortest_1115z00_1766;

																	BgL__ortest_1115z00_1766 =
																		(BgL_classz00_1749 == BgL_oclassz00_1765);
																	if (BgL__ortest_1115z00_1766)
																		{	/* Coerce/app.scm 71 */
																			BgL_res1720z00_1782 =
																				BgL__ortest_1115z00_1766;
																		}
																	else
																		{	/* Coerce/app.scm 71 */
																			long BgL_odepthz00_1767;

																			{	/* Coerce/app.scm 71 */
																				obj_t BgL_arg1804z00_1768;

																				BgL_arg1804z00_1768 =
																					(BgL_oclassz00_1765);
																				BgL_odepthz00_1767 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_1768);
																			}
																			if ((3L < BgL_odepthz00_1767))
																				{	/* Coerce/app.scm 71 */
																					obj_t BgL_arg1802z00_1770;

																					{	/* Coerce/app.scm 71 */
																						obj_t BgL_arg1803z00_1771;

																						BgL_arg1803z00_1771 =
																							(BgL_oclassz00_1765);
																						BgL_arg1802z00_1770 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_1771, 3L);
																					}
																					BgL_res1720z00_1782 =
																						(BgL_arg1802z00_1770 ==
																						BgL_classz00_1749);
																				}
																			else
																				{	/* Coerce/app.scm 71 */
																					BgL_res1720z00_1782 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1765z00_2285 = BgL_res1720z00_1782;
														}
												}
											}
										}
										if (BgL_test1765z00_2285)
											{	/* Coerce/app.scm 71 */
												BFALSE;
											}
										else
											{	/* Coerce/app.scm 73 */
												obj_t BgL_arg1317z00_1428;
												obj_t BgL_arg1318z00_1429;

												BgL_arg1317z00_1428 =
													(((BgL_variablez00_bglt) COBJECT(
															(((BgL_varz00_bglt) COBJECT(
																		((BgL_varz00_bglt) BgL_cloz00_1423)))->
																BgL_variablez00)))->BgL_namez00);
												{	/* Coerce/app.scm 74 */
													BgL_typez00_bglt BgL_arg1321z00_1432;

													BgL_arg1321z00_1432 =
														(((BgL_variablez00_bglt) COBJECT(
																(((BgL_varz00_bglt) COBJECT(
																			((BgL_varz00_bglt) BgL_cloz00_1423)))->
																	BgL_variablez00)))->BgL_typez00);
													BgL_arg1318z00_1429 =
														BGl_shapez00zztools_shapez00(((obj_t)
															BgL_arg1321z00_1432));
												}
												BGl_userzd2errorzf2locationz20zztools_errorz00
													(BgL_locz00_1419, BgL_arg1317z00_1428,
													BgL_arg1318z00_1429, BGl_string1734z00zzcoerce_appz00,
													BNIL);
											}
									}
								else
									{	/* Coerce/app.scm 76 */
										BgL_nodez00_bglt BgL_arg1326z00_1436;

										BgL_arg1326z00_1436 =
											BGl_coercez12z12zzcoerce_coercez00(
											((BgL_nodez00_bglt) BgL_cloz00_1423), BgL_callerz00_14,
											((BgL_typez00_bglt)
												BGl_za2procedureza2z00zztype_cachez00),
											CBOOL(BgL_safez00_17));
										{	/* Coerce/app.scm 76 */
											obj_t BgL_auxz00_2327;
											obj_t BgL_tmpz00_2325;

											BgL_auxz00_2327 = ((obj_t) BgL_arg1326z00_1436);
											BgL_tmpz00_2325 = ((obj_t) BgL_argsz00_1417);
											SET_CAR(BgL_tmpz00_2325, BgL_auxz00_2327);
										}
									}
							}
						}
					}
				else
					{	/* Coerce/app.scm 83 */
						obj_t BgL_arg1249z00_1399;
						obj_t BgL_arg1252z00_1400;

						BgL_arg1249z00_1399 =
							(((BgL_appz00_bglt) COBJECT(BgL_nodez00_15))->BgL_argsz00);
						BgL_arg1252z00_1400 =
							(((BgL_cfunz00_bglt) COBJECT(
									((BgL_cfunz00_bglt) BgL_funz00_12)))->BgL_argszd2typezd2);
						BBOOL(BGl_coercezd2argsz12ze70z27zzcoerce_appz00(BgL_safez00_17,
								BgL_callerz00_14, BgL_arg1249z00_1399, BgL_arg1252z00_1400));
					}
				{	/* Coerce/app.scm 84 */
					BgL_typez00_bglt BgL_arg1272z00_1402;

					BgL_arg1272z00_1402 =
						BGl_getzd2typezd2zztype_typeofz00(
						((BgL_nodez00_bglt) BgL_nodez00_15), ((bool_t) 0));
					return
						BGl_convertz12z12zzcoerce_convertz00(
						((BgL_nodez00_bglt) BgL_nodez00_15), BgL_arg1272z00_1402,
						((BgL_typez00_bglt) BgL_toz00_16), CBOOL(BgL_safez00_17));
				}
			}
		}

	}



/* coerce-args!~0 */
	bool_t BGl_coercezd2argsz12ze70z27zzcoerce_appz00(obj_t BgL_safez00_2153,
		obj_t BgL_callerz00_2152, obj_t BgL_argsz00_1403, obj_t BgL_typesz00_1404)
	{
		{	/* Coerce/app.scm 66 */
			{
				obj_t BgL_actualsz00_1407;
				obj_t BgL_typesz00_1408;

				BgL_actualsz00_1407 = BgL_argsz00_1403;
				BgL_typesz00_1408 = BgL_typesz00_1404;
			BgL_zc3z04anonymousza31274ze3z87_1409:
				if (PAIRP(BgL_actualsz00_1407))
					{	/* Coerce/app.scm 64 */
						{	/* Coerce/app.scm 65 */
							BgL_nodez00_bglt BgL_arg1284z00_1411;

							{	/* Coerce/app.scm 65 */
								obj_t BgL_arg1304z00_1412;
								obj_t BgL_arg1305z00_1413;

								BgL_arg1304z00_1412 = CAR(BgL_actualsz00_1407);
								BgL_arg1305z00_1413 = CAR(((obj_t) BgL_typesz00_1408));
								BgL_arg1284z00_1411 =
									BGl_coercez12z12zzcoerce_coercez00(
									((BgL_nodez00_bglt) BgL_arg1304z00_1412), BgL_callerz00_2152,
									((BgL_typez00_bglt) BgL_arg1305z00_1413),
									CBOOL(BgL_safez00_2153));
							}
							{	/* Coerce/app.scm 65 */
								obj_t BgL_tmpz00_2350;

								BgL_tmpz00_2350 = ((obj_t) BgL_arg1284z00_1411);
								SET_CAR(BgL_actualsz00_1407, BgL_tmpz00_2350);
							}
						}
						{	/* Coerce/app.scm 66 */
							obj_t BgL_arg1306z00_1414;
							obj_t BgL_arg1307z00_1415;

							BgL_arg1306z00_1414 = CDR(BgL_actualsz00_1407);
							BgL_arg1307z00_1415 = CDR(((obj_t) BgL_typesz00_1408));
							{
								obj_t BgL_typesz00_2357;
								obj_t BgL_actualsz00_2356;

								BgL_actualsz00_2356 = BgL_arg1306z00_1414;
								BgL_typesz00_2357 = BgL_arg1307z00_1415;
								BgL_typesz00_1408 = BgL_typesz00_2357;
								BgL_actualsz00_1407 = BgL_actualsz00_2356;
								goto BgL_zc3z04anonymousza31274ze3z87_1409;
							}
						}
					}
				else
					{	/* Coerce/app.scm 64 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* coerce-foreign-va-app! */
	BgL_nodez00_bglt
		BGl_coercezd2foreignzd2vazd2appz12zc0zzcoerce_appz00(BgL_valuez00_bglt
		BgL_funz00_18, BgL_variablez00_bglt BgL_calleez00_19,
		obj_t BgL_callerz00_20, BgL_appz00_bglt BgL_nodez00_21, obj_t BgL_toz00_22,
		obj_t BgL_safez00_23)
	{
		{	/* Coerce/app.scm 89 */
			{	/* Coerce/app.scm 90 */
				obj_t BgL_g1107z00_1439;
				obj_t BgL_g1108z00_1440;
				long BgL_g1109z00_1441;

				BgL_g1107z00_1439 =
					(((BgL_appz00_bglt) COBJECT(BgL_nodez00_21))->BgL_argsz00);
				BgL_g1108z00_1440 =
					(((BgL_cfunz00_bglt) COBJECT(
							((BgL_cfunz00_bglt) BgL_funz00_18)))->BgL_argszd2typezd2);
				BgL_g1109z00_1441 =
					(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_funz00_18)))->BgL_arityz00);
				{
					obj_t BgL_actualsz00_1443;
					obj_t BgL_typesz00_1444;
					long BgL_counterz00_1445;

					BgL_actualsz00_1443 = BgL_g1107z00_1439;
					BgL_typesz00_1444 = BgL_g1108z00_1440;
					BgL_counterz00_1445 = BgL_g1109z00_1441;
				BgL_zc3z04anonymousza31327ze3z87_1446:
					if ((BgL_counterz00_1445 == -1L))
						{
							obj_t BgL_actualsz00_1449;

							BgL_actualsz00_1449 = BgL_actualsz00_1443;
						BgL_zc3z04anonymousza31329ze3z87_1450:
							if (NULLP(BgL_actualsz00_1449))
								{	/* Coerce/app.scm 97 */
									BgL_typez00_bglt BgL_arg1331z00_1452;

									BgL_arg1331z00_1452 =
										BGl_getzd2typezd2zztype_typeofz00(
										((BgL_nodez00_bglt) BgL_nodez00_21), ((bool_t) 0));
									return
										BGl_convertz12z12zzcoerce_convertz00(
										((BgL_nodez00_bglt) BgL_nodez00_21), BgL_arg1331z00_1452,
										((BgL_typez00_bglt) BgL_toz00_22), CBOOL(BgL_safez00_23));
								}
							else
								{	/* Coerce/app.scm 96 */
									{	/* Coerce/app.scm 99 */
										BgL_nodez00_bglt BgL_arg1332z00_1453;

										{	/* Coerce/app.scm 99 */
											obj_t BgL_arg1333z00_1454;
											obj_t BgL_arg1335z00_1455;

											BgL_arg1333z00_1454 = CAR(((obj_t) BgL_actualsz00_1449));
											BgL_arg1335z00_1455 = CAR(((obj_t) BgL_typesz00_1444));
											BgL_arg1332z00_1453 =
												BGl_coercez12z12zzcoerce_coercez00(
												((BgL_nodez00_bglt) BgL_arg1333z00_1454),
												BgL_callerz00_20,
												((BgL_typez00_bglt) BgL_arg1335z00_1455),
												CBOOL(BgL_safez00_23));
										}
										{	/* Coerce/app.scm 99 */
											obj_t BgL_auxz00_2383;
											obj_t BgL_tmpz00_2381;

											BgL_auxz00_2383 = ((obj_t) BgL_arg1332z00_1453);
											BgL_tmpz00_2381 = ((obj_t) BgL_actualsz00_1449);
											SET_CAR(BgL_tmpz00_2381, BgL_auxz00_2383);
										}
									}
									{	/* Coerce/app.scm 103 */
										obj_t BgL_arg1339z00_1456;

										BgL_arg1339z00_1456 = CDR(((obj_t) BgL_actualsz00_1449));
										{
											obj_t BgL_actualsz00_2388;

											BgL_actualsz00_2388 = BgL_arg1339z00_1456;
											BgL_actualsz00_1449 = BgL_actualsz00_2388;
											goto BgL_zc3z04anonymousza31329ze3z87_1450;
										}
									}
								}
						}
					else
						{	/* Coerce/app.scm 93 */
							{	/* Coerce/app.scm 105 */
								BgL_nodez00_bglt BgL_arg1340z00_1458;

								{	/* Coerce/app.scm 105 */
									obj_t BgL_arg1342z00_1459;
									obj_t BgL_arg1343z00_1460;

									BgL_arg1342z00_1459 = CAR(((obj_t) BgL_actualsz00_1443));
									BgL_arg1343z00_1460 = CAR(((obj_t) BgL_typesz00_1444));
									BgL_arg1340z00_1458 =
										BGl_coercez12z12zzcoerce_coercez00(
										((BgL_nodez00_bglt) BgL_arg1342z00_1459), BgL_callerz00_20,
										((BgL_typez00_bglt) BgL_arg1343z00_1460),
										CBOOL(BgL_safez00_23));
								}
								{	/* Coerce/app.scm 105 */
									obj_t BgL_auxz00_2399;
									obj_t BgL_tmpz00_2397;

									BgL_auxz00_2399 = ((obj_t) BgL_arg1340z00_1458);
									BgL_tmpz00_2397 = ((obj_t) BgL_actualsz00_1443);
									SET_CAR(BgL_tmpz00_2397, BgL_auxz00_2399);
								}
							}
							{	/* Coerce/app.scm 109 */
								obj_t BgL_arg1346z00_1461;
								obj_t BgL_arg1348z00_1462;
								long BgL_arg1349z00_1463;

								BgL_arg1346z00_1461 = CDR(((obj_t) BgL_actualsz00_1443));
								BgL_arg1348z00_1462 = CDR(((obj_t) BgL_typesz00_1444));
								BgL_arg1349z00_1463 = (BgL_counterz00_1445 + 1L);
								{
									long BgL_counterz00_2409;
									obj_t BgL_typesz00_2408;
									obj_t BgL_actualsz00_2407;

									BgL_actualsz00_2407 = BgL_arg1346z00_1461;
									BgL_typesz00_2408 = BgL_arg1348z00_1462;
									BgL_counterz00_2409 = BgL_arg1349z00_1463;
									BgL_counterz00_1445 = BgL_counterz00_2409;
									BgL_typesz00_1444 = BgL_typesz00_2408;
									BgL_actualsz00_1443 = BgL_actualsz00_2407;
									goto BgL_zc3z04anonymousza31327ze3z87_1446;
								}
							}
						}
				}
			}
		}

	}



/* coerce-bigloo-app! */
	BgL_nodez00_bglt
		BGl_coercezd2bigloozd2appz12z12zzcoerce_appz00(BgL_variablez00_bglt
		BgL_calleez00_24, obj_t BgL_callerz00_25, BgL_appz00_bglt BgL_nodez00_26,
		obj_t BgL_toz00_27, obj_t BgL_safez00_28)
	{
		{	/* Coerce/app.scm 114 */
			{	/* Coerce/app.scm 118 */
				bool_t BgL_test1772z00_2410;

				{	/* Coerce/app.scm 118 */
					bool_t BgL_test1773z00_2411;

					{	/* Coerce/app.scm 118 */
						obj_t BgL_classz00_1805;

						BgL_classz00_1805 = BGl_globalz00zzast_varz00;
						{	/* Coerce/app.scm 118 */
							BgL_objectz00_bglt BgL_arg1807z00_1807;

							{	/* Coerce/app.scm 118 */
								obj_t BgL_tmpz00_2412;

								BgL_tmpz00_2412 = ((obj_t) BgL_calleez00_24);
								BgL_arg1807z00_1807 = (BgL_objectz00_bglt) (BgL_tmpz00_2412);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Coerce/app.scm 118 */
									long BgL_idxz00_1813;

									BgL_idxz00_1813 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1807);
									BgL_test1773z00_2411 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_1813 + 2L)) == BgL_classz00_1805);
								}
							else
								{	/* Coerce/app.scm 118 */
									bool_t BgL_res1721z00_1838;

									{	/* Coerce/app.scm 118 */
										obj_t BgL_oclassz00_1821;

										{	/* Coerce/app.scm 118 */
											obj_t BgL_arg1815z00_1829;
											long BgL_arg1816z00_1830;

											BgL_arg1815z00_1829 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Coerce/app.scm 118 */
												long BgL_arg1817z00_1831;

												BgL_arg1817z00_1831 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1807);
												BgL_arg1816z00_1830 =
													(BgL_arg1817z00_1831 - OBJECT_TYPE);
											}
											BgL_oclassz00_1821 =
												VECTOR_REF(BgL_arg1815z00_1829, BgL_arg1816z00_1830);
										}
										{	/* Coerce/app.scm 118 */
											bool_t BgL__ortest_1115z00_1822;

											BgL__ortest_1115z00_1822 =
												(BgL_classz00_1805 == BgL_oclassz00_1821);
											if (BgL__ortest_1115z00_1822)
												{	/* Coerce/app.scm 118 */
													BgL_res1721z00_1838 = BgL__ortest_1115z00_1822;
												}
											else
												{	/* Coerce/app.scm 118 */
													long BgL_odepthz00_1823;

													{	/* Coerce/app.scm 118 */
														obj_t BgL_arg1804z00_1824;

														BgL_arg1804z00_1824 = (BgL_oclassz00_1821);
														BgL_odepthz00_1823 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_1824);
													}
													if ((2L < BgL_odepthz00_1823))
														{	/* Coerce/app.scm 118 */
															obj_t BgL_arg1802z00_1826;

															{	/* Coerce/app.scm 118 */
																obj_t BgL_arg1803z00_1827;

																BgL_arg1803z00_1827 = (BgL_oclassz00_1821);
																BgL_arg1802z00_1826 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1827,
																	2L);
															}
															BgL_res1721z00_1838 =
																(BgL_arg1802z00_1826 == BgL_classz00_1805);
														}
													else
														{	/* Coerce/app.scm 118 */
															BgL_res1721z00_1838 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1773z00_2411 = BgL_res1721z00_1838;
								}
						}
					}
					if (BgL_test1773z00_2411)
						{	/* Coerce/app.scm 118 */
							if (
								((((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt) BgL_calleez00_24)))->
										BgL_importz00) == CNST_TABLE_REF(1)))
								{	/* Coerce/app.scm 120 */
									bool_t BgL_test1779z00_2439;

									{	/* Coerce/app.scm 120 */
										obj_t BgL_tmpz00_2440;

										BgL_tmpz00_2440 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt)
																COBJECT(BgL_calleez00_24))->BgL_valuez00))))->
											BgL_argsz00);
										BgL_test1779z00_2439 = PAIRP(BgL_tmpz00_2440);
									}
									if (BgL_test1779z00_2439)
										{	/* Coerce/app.scm 121 */
											obj_t BgL_arg1408z00_1481;

											{	/* Coerce/app.scm 121 */
												obj_t BgL_pairz00_1844;

												BgL_pairz00_1844 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt)
																		COBJECT(BgL_calleez00_24))->
																	BgL_valuez00))))->BgL_argsz00);
												BgL_arg1408z00_1481 = CAR(BgL_pairz00_1844);
											}
											{	/* Coerce/app.scm 121 */
												obj_t BgL_classz00_1845;

												BgL_classz00_1845 = BGl_typez00zztype_typez00;
												if (BGL_OBJECTP(BgL_arg1408z00_1481))
													{	/* Coerce/app.scm 121 */
														BgL_objectz00_bglt BgL_arg1807z00_1847;

														BgL_arg1807z00_1847 =
															(BgL_objectz00_bglt) (BgL_arg1408z00_1481);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Coerce/app.scm 121 */
																long BgL_idxz00_1853;

																BgL_idxz00_1853 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_1847);
																BgL_test1772z00_2410 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_1853 + 1L)) ==
																	BgL_classz00_1845);
															}
														else
															{	/* Coerce/app.scm 121 */
																bool_t BgL_res1722z00_1878;

																{	/* Coerce/app.scm 121 */
																	obj_t BgL_oclassz00_1861;

																	{	/* Coerce/app.scm 121 */
																		obj_t BgL_arg1815z00_1869;
																		long BgL_arg1816z00_1870;

																		BgL_arg1815z00_1869 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Coerce/app.scm 121 */
																			long BgL_arg1817z00_1871;

																			BgL_arg1817z00_1871 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_1847);
																			BgL_arg1816z00_1870 =
																				(BgL_arg1817z00_1871 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_1861 =
																			VECTOR_REF(BgL_arg1815z00_1869,
																			BgL_arg1816z00_1870);
																	}
																	{	/* Coerce/app.scm 121 */
																		bool_t BgL__ortest_1115z00_1862;

																		BgL__ortest_1115z00_1862 =
																			(BgL_classz00_1845 == BgL_oclassz00_1861);
																		if (BgL__ortest_1115z00_1862)
																			{	/* Coerce/app.scm 121 */
																				BgL_res1722z00_1878 =
																					BgL__ortest_1115z00_1862;
																			}
																		else
																			{	/* Coerce/app.scm 121 */
																				long BgL_odepthz00_1863;

																				{	/* Coerce/app.scm 121 */
																					obj_t BgL_arg1804z00_1864;

																					BgL_arg1804z00_1864 =
																						(BgL_oclassz00_1861);
																					BgL_odepthz00_1863 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_1864);
																				}
																				if ((1L < BgL_odepthz00_1863))
																					{	/* Coerce/app.scm 121 */
																						obj_t BgL_arg1802z00_1866;

																						{	/* Coerce/app.scm 121 */
																							obj_t BgL_arg1803z00_1867;

																							BgL_arg1803z00_1867 =
																								(BgL_oclassz00_1861);
																							BgL_arg1802z00_1866 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_1867, 1L);
																						}
																						BgL_res1722z00_1878 =
																							(BgL_arg1802z00_1866 ==
																							BgL_classz00_1845);
																					}
																				else
																					{	/* Coerce/app.scm 121 */
																						BgL_res1722z00_1878 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1772z00_2410 = BgL_res1722z00_1878;
															}
													}
												else
													{	/* Coerce/app.scm 121 */
														BgL_test1772z00_2410 = ((bool_t) 0);
													}
											}
										}
									else
										{	/* Coerce/app.scm 120 */
											BgL_test1772z00_2410 = ((bool_t) 0);
										}
								}
							else
								{	/* Coerce/app.scm 119 */
									BgL_test1772z00_2410 = ((bool_t) 0);
								}
						}
					else
						{	/* Coerce/app.scm 118 */
							BgL_test1772z00_2410 = ((bool_t) 0);
						}
				}
				if (BgL_test1772z00_2410)
					{	/* Coerce/app.scm 118 */
						BGL_TAIL return
							BGl_coercezd2bigloozd2externzd2appz12zc0zzcoerce_appz00
							(BgL_calleez00_24, BgL_callerz00_25, BgL_nodez00_26, BgL_toz00_27,
							BgL_safez00_28);
					}
				else
					{	/* Coerce/app.scm 118 */
						BGL_TAIL return
							BGl_coercezd2bigloozd2internzd2appz12zc0zzcoerce_appz00
							(BgL_calleez00_24, BgL_callerz00_25, BgL_nodez00_26, BgL_toz00_27,
							BgL_safez00_28);
					}
			}
		}

	}



/* coerce-bigloo-intern-app! */
	BgL_nodez00_bglt
		BGl_coercezd2bigloozd2internzd2appz12zc0zzcoerce_appz00(BgL_variablez00_bglt
		BgL_calleez00_29, obj_t BgL_callerz00_30, BgL_appz00_bglt BgL_nodez00_31,
		obj_t BgL_toz00_32, obj_t BgL_safez00_33)
	{
		{	/* Coerce/app.scm 128 */
			{	/* Coerce/app.scm 132 */
				BgL_valuez00_bglt BgL_funz00_1487;

				BgL_funz00_1487 =
					(((BgL_variablez00_bglt) COBJECT(BgL_calleez00_29))->BgL_valuez00);
				{	/* Coerce/app.scm 133 */
					obj_t BgL_shz00_1489;

					BgL_shz00_1489 =
						BGl_shapez00zztools_shapez00(((obj_t) BgL_calleez00_29));
					{	/* Coerce/app.scm 134 */
						BgL_typez00_bglt BgL_ntypez00_1490;

						BgL_ntypez00_1490 =
							BGl_getzd2typezd2zztype_typeofz00(
							((BgL_nodez00_bglt) BgL_nodez00_31), ((bool_t) 0));
						{	/* Coerce/app.scm 135 */

							{	/* Coerce/app.scm 136 */
								obj_t BgL_g1110z00_1491;
								obj_t BgL_g1111z00_1492;

								BgL_g1110z00_1491 =
									(((BgL_appz00_bglt) COBJECT(BgL_nodez00_31))->BgL_argsz00);
								BgL_g1111z00_1492 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_funz00_1487)))->BgL_argsz00);
								{
									obj_t BgL_actualsz00_1494;
									obj_t BgL_formalsz00_1495;

									BgL_actualsz00_1494 = BgL_g1110z00_1491;
									BgL_formalsz00_1495 = BgL_g1111z00_1492;
								BgL_zc3z04anonymousza31438ze3z87_1496:
									if (
										(bgl_list_length(BgL_actualsz00_1494) ==
											bgl_list_length(BgL_formalsz00_1495)))
										{	/* Coerce/app.scm 138 */
											((bool_t) 0);
										}
									else
										{	/* Coerce/app.scm 138 */
											{	/* Coerce/app.scm 140 */
												obj_t BgL_arg1472z00_1500;

												BgL_arg1472z00_1500 =
													BGl_shapez00zztools_shapez00(
													((obj_t) BgL_nodez00_31));
												BGl_internalzd2errorzd2zztools_errorz00
													(BGl_string1735z00zzcoerce_appz00,
													BGl_string1736z00zzcoerce_appz00,
													BgL_arg1472z00_1500);
											}
											{	/* Coerce/app.scm 141 */
												obj_t BgL_list1473z00_1501;

												BgL_list1473z00_1501 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
												BGl_exitz00zz__errorz00(BgL_list1473z00_1501);
											}
										}
									if (NULLP(BgL_actualsz00_1494))
										{	/* Coerce/app.scm 144 */
											bool_t BgL_test1786z00_2493;

											if ((BgL_callerz00_30 == ((obj_t) BgL_calleez00_29)))
												{	/* Coerce/app.scm 144 */
													if (CBOOL
														(BGl_za2unsafezd2typeza2zd2zzengine_paramz00))
														{	/* Coerce/app.scm 144 */
															BgL_test1786z00_2493 = ((bool_t) 0);
														}
													else
														{	/* Coerce/app.scm 144 */
															BgL_test1786z00_2493 = ((bool_t) 1);
														}
												}
											else
												{	/* Coerce/app.scm 144 */
													BgL_test1786z00_2493 = ((bool_t) 0);
												}
											if (BgL_test1786z00_2493)
												{	/* Coerce/app.scm 144 */
													return
														BGl_convertz12z12zzcoerce_convertz00(
														((BgL_nodez00_bglt) BgL_nodez00_31),
														BgL_ntypez00_1490,
														((BgL_typez00_bglt) BgL_toz00_32), ((bool_t) 0));
												}
											else
												{	/* Coerce/app.scm 144 */
													return
														BGl_convertz12z12zzcoerce_convertz00(
														((BgL_nodez00_bglt) BgL_nodez00_31),
														BgL_ntypez00_1490,
														((BgL_typez00_bglt) BgL_toz00_32),
														CBOOL(BgL_safez00_33));
												}
										}
									else
										{	/* Coerce/app.scm 151 */
											BgL_typez00_bglt BgL_typez00_1506;

											BgL_typez00_1506 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_formalsz00_1495))))))->
												BgL_typez00);
											{	/* Coerce/app.scm 152 */
												BgL_nodez00_bglt BgL_arg1502z00_1507;

												{	/* Coerce/app.scm 152 */
													obj_t BgL_arg1509z00_1508;

													BgL_arg1509z00_1508 =
														CAR(((obj_t) BgL_actualsz00_1494));
													BgL_arg1502z00_1507 =
														BGl_coercez12z12zzcoerce_coercez00(
														((BgL_nodez00_bglt) BgL_arg1509z00_1508),
														BgL_callerz00_30, BgL_typez00_1506,
														CBOOL(BgL_safez00_33));
												}
												{	/* Coerce/app.scm 152 */
													obj_t BgL_auxz00_2518;
													obj_t BgL_tmpz00_2516;

													BgL_auxz00_2518 = ((obj_t) BgL_arg1502z00_1507);
													BgL_tmpz00_2516 = ((obj_t) BgL_actualsz00_1494);
													SET_CAR(BgL_tmpz00_2516, BgL_auxz00_2518);
												}
											}
											{	/* Coerce/app.scm 153 */
												obj_t BgL_arg1513z00_1509;
												obj_t BgL_arg1514z00_1510;

												BgL_arg1513z00_1509 =
													CDR(((obj_t) BgL_actualsz00_1494));
												BgL_arg1514z00_1510 =
													CDR(((obj_t) BgL_formalsz00_1495));
												{
													obj_t BgL_formalsz00_2526;
													obj_t BgL_actualsz00_2525;

													BgL_actualsz00_2525 = BgL_arg1513z00_1509;
													BgL_formalsz00_2526 = BgL_arg1514z00_1510;
													BgL_formalsz00_1495 = BgL_formalsz00_2526;
													BgL_actualsz00_1494 = BgL_actualsz00_2525;
													goto BgL_zc3z04anonymousza31438ze3z87_1496;
												}
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* coerce-bigloo-extern-app! */
	BgL_nodez00_bglt
		BGl_coercezd2bigloozd2externzd2appz12zc0zzcoerce_appz00(BgL_variablez00_bglt
		BgL_calleez00_34, obj_t BgL_callerz00_35, BgL_appz00_bglt BgL_nodez00_36,
		obj_t BgL_toz00_37, obj_t BgL_safez00_38)
	{
		{	/* Coerce/app.scm 158 */
			{	/* Coerce/app.scm 159 */
				BgL_valuez00_bglt BgL_funz00_1513;

				BgL_funz00_1513 =
					(((BgL_variablez00_bglt) COBJECT(BgL_calleez00_34))->BgL_valuez00);
				{	/* Coerce/app.scm 160 */
					BgL_typez00_bglt BgL_ntypez00_1515;

					BgL_ntypez00_1515 =
						BGl_getzd2typezd2zztype_typeofz00(
						((BgL_nodez00_bglt) BgL_nodez00_36), ((bool_t) 0));
					{	/* Coerce/app.scm 161 */

						{	/* Coerce/app.scm 162 */
							obj_t BgL_g1112z00_1516;
							obj_t BgL_g1113z00_1517;

							BgL_g1112z00_1516 =
								(((BgL_appz00_bglt) COBJECT(BgL_nodez00_36))->BgL_argsz00);
							BgL_g1113z00_1517 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_funz00_1513)))->BgL_argsz00);
							{
								obj_t BgL_actualsz00_1519;
								obj_t BgL_formalsz00_1520;

								BgL_actualsz00_1519 = BgL_g1112z00_1516;
								BgL_formalsz00_1520 = BgL_g1113z00_1517;
							BgL_zc3z04anonymousza31517ze3z87_1521:
								if (NULLP(BgL_actualsz00_1519))
									{	/* Coerce/app.scm 164 */
										return
											BGl_convertz12z12zzcoerce_convertz00(
											((BgL_nodez00_bglt) BgL_nodez00_36), BgL_ntypez00_1515,
											((BgL_typez00_bglt) BgL_toz00_37), CBOOL(BgL_safez00_38));
									}
								else
									{	/* Coerce/app.scm 166 */
										obj_t BgL_typez00_1523;

										BgL_typez00_1523 = CAR(((obj_t) BgL_formalsz00_1520));
										{	/* Coerce/app.scm 167 */
											BgL_nodez00_bglt BgL_arg1535z00_1524;

											{	/* Coerce/app.scm 167 */
												obj_t BgL_arg1540z00_1525;

												BgL_arg1540z00_1525 =
													CAR(((obj_t) BgL_actualsz00_1519));
												BgL_arg1535z00_1524 =
													BGl_coercez12z12zzcoerce_coercez00(
													((BgL_nodez00_bglt) BgL_arg1540z00_1525),
													BgL_callerz00_35,
													((BgL_typez00_bglt) BgL_typez00_1523),
													CBOOL(BgL_safez00_38));
											}
											{	/* Coerce/app.scm 167 */
												obj_t BgL_auxz00_2549;
												obj_t BgL_tmpz00_2547;

												BgL_auxz00_2549 = ((obj_t) BgL_arg1535z00_1524);
												BgL_tmpz00_2547 = ((obj_t) BgL_actualsz00_1519);
												SET_CAR(BgL_tmpz00_2547, BgL_auxz00_2549);
											}
										}
										{	/* Coerce/app.scm 168 */
											obj_t BgL_arg1544z00_1526;
											obj_t BgL_arg1546z00_1527;

											BgL_arg1544z00_1526 = CDR(((obj_t) BgL_actualsz00_1519));
											BgL_arg1546z00_1527 = CDR(((obj_t) BgL_formalsz00_1520));
											{
												obj_t BgL_formalsz00_2557;
												obj_t BgL_actualsz00_2556;

												BgL_actualsz00_2556 = BgL_arg1544z00_1526;
												BgL_formalsz00_2557 = BgL_arg1546z00_1527;
												BgL_formalsz00_1520 = BgL_formalsz00_2557;
												BgL_actualsz00_1519 = BgL_actualsz00_2556;
												goto BgL_zc3z04anonymousza31517ze3z87_1521;
											}
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcoerce_appz00(void)
	{
		{	/* Coerce/app.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcoerce_appz00(void)
	{
		{	/* Coerce/app.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcoerce_appz00(void)
	{
		{	/* Coerce/app.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_coercez12zd2envzc0zzcoerce_coercez00, BGl_appz00zzast_nodez00,
				BGl_proc1737z00zzcoerce_appz00, BGl_string1738z00zzcoerce_appz00);
		}

	}



/* &coerce!-app1232 */
	BgL_nodez00_bglt BGl_z62coercez12zd2app1232za2zzcoerce_appz00(obj_t
		BgL_envz00_2142, obj_t BgL_nodez00_2143, obj_t BgL_callerz00_2144,
		obj_t BgL_toz00_2145, obj_t BgL_safez00_2146)
	{
		{	/* Coerce/app.scm 31 */
			{	/* Tools/trace.sch 53 */
				BgL_variablez00_bglt BgL_funz00_2166;

				BgL_funz00_2166 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2143)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Tools/trace.sch 53 */
					bool_t BgL_test1790z00_2562;

					{	/* Tools/trace.sch 53 */
						bool_t BgL_test1791z00_2563;

						{	/* Tools/trace.sch 53 */
							obj_t BgL_classz00_2167;

							BgL_classz00_2167 = BGl_globalz00zzast_varz00;
							{	/* Tools/trace.sch 53 */
								BgL_objectz00_bglt BgL_arg1807z00_2168;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_tmpz00_2564;

									BgL_tmpz00_2564 =
										((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2166));
									BgL_arg1807z00_2168 = (BgL_objectz00_bglt) (BgL_tmpz00_2564);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Tools/trace.sch 53 */
										long BgL_idxz00_2169;

										BgL_idxz00_2169 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2168);
										BgL_test1791z00_2563 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2169 + 2L)) == BgL_classz00_2167);
									}
								else
									{	/* Tools/trace.sch 53 */
										bool_t BgL_res1723z00_2172;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_oclassz00_2173;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1815z00_2174;
												long BgL_arg1816z00_2175;

												BgL_arg1815z00_2174 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Tools/trace.sch 53 */
													long BgL_arg1817z00_2176;

													BgL_arg1817z00_2176 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2168);
													BgL_arg1816z00_2175 =
														(BgL_arg1817z00_2176 - OBJECT_TYPE);
												}
												BgL_oclassz00_2173 =
													VECTOR_REF(BgL_arg1815z00_2174, BgL_arg1816z00_2175);
											}
											{	/* Tools/trace.sch 53 */
												bool_t BgL__ortest_1115z00_2177;

												BgL__ortest_1115z00_2177 =
													(BgL_classz00_2167 == BgL_oclassz00_2173);
												if (BgL__ortest_1115z00_2177)
													{	/* Tools/trace.sch 53 */
														BgL_res1723z00_2172 = BgL__ortest_1115z00_2177;
													}
												else
													{	/* Tools/trace.sch 53 */
														long BgL_odepthz00_2178;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1804z00_2179;

															BgL_arg1804z00_2179 = (BgL_oclassz00_2173);
															BgL_odepthz00_2178 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2179);
														}
														if ((2L < BgL_odepthz00_2178))
															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1802z00_2180;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1803z00_2181;

																	BgL_arg1803z00_2181 = (BgL_oclassz00_2173);
																	BgL_arg1802z00_2180 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2181,
																		2L);
																}
																BgL_res1723z00_2172 =
																	(BgL_arg1802z00_2180 == BgL_classz00_2167);
															}
														else
															{	/* Tools/trace.sch 53 */
																BgL_res1723z00_2172 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1791z00_2563 = BgL_res1723z00_2172;
									}
							}
						}
						if (BgL_test1791z00_2563)
							{	/* Tools/trace.sch 53 */
								BgL_valuez00_bglt BgL_arg1559z00_2182;

								BgL_arg1559z00_2182 =
									(((BgL_variablez00_bglt) COBJECT(BgL_funz00_2166))->
									BgL_valuez00);
								{	/* Tools/trace.sch 53 */
									obj_t BgL_classz00_2183;

									BgL_classz00_2183 = BGl_cfunz00zzast_varz00;
									{	/* Tools/trace.sch 53 */
										BgL_objectz00_bglt BgL_arg1807z00_2184;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_tmpz00_2588;

											BgL_tmpz00_2588 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1559z00_2182));
											BgL_arg1807z00_2184 =
												(BgL_objectz00_bglt) (BgL_tmpz00_2588);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Tools/trace.sch 53 */
												long BgL_idxz00_2185;

												BgL_idxz00_2185 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2184);
												BgL_test1790z00_2562 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2185 + 3L)) == BgL_classz00_2183);
											}
										else
											{	/* Tools/trace.sch 53 */
												bool_t BgL_res1724z00_2188;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_oclassz00_2189;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1815z00_2190;
														long BgL_arg1816z00_2191;

														BgL_arg1815z00_2190 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Tools/trace.sch 53 */
															long BgL_arg1817z00_2192;

															BgL_arg1817z00_2192 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2184);
															BgL_arg1816z00_2191 =
																(BgL_arg1817z00_2192 - OBJECT_TYPE);
														}
														BgL_oclassz00_2189 =
															VECTOR_REF(BgL_arg1815z00_2190,
															BgL_arg1816z00_2191);
													}
													{	/* Tools/trace.sch 53 */
														bool_t BgL__ortest_1115z00_2193;

														BgL__ortest_1115z00_2193 =
															(BgL_classz00_2183 == BgL_oclassz00_2189);
														if (BgL__ortest_1115z00_2193)
															{	/* Tools/trace.sch 53 */
																BgL_res1724z00_2188 = BgL__ortest_1115z00_2193;
															}
														else
															{	/* Tools/trace.sch 53 */
																long BgL_odepthz00_2194;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1804z00_2195;

																	BgL_arg1804z00_2195 = (BgL_oclassz00_2189);
																	BgL_odepthz00_2194 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2195);
																}
																if ((3L < BgL_odepthz00_2194))
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1802z00_2196;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1803z00_2197;

																			BgL_arg1803z00_2197 =
																				(BgL_oclassz00_2189);
																			BgL_arg1802z00_2196 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2197, 3L);
																		}
																		BgL_res1724z00_2188 =
																			(BgL_arg1802z00_2196 ==
																			BgL_classz00_2183);
																	}
																else
																	{	/* Tools/trace.sch 53 */
																		BgL_res1724z00_2188 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1790z00_2562 = BgL_res1724z00_2188;
											}
									}
								}
							}
						else
							{	/* Tools/trace.sch 53 */
								BgL_test1790z00_2562 = ((bool_t) 0);
							}
					}
					if (BgL_test1790z00_2562)
						{	/* Tools/trace.sch 53 */
							return
								BGl_coercezd2foreignzd2appz12z12zzcoerce_appz00(BgL_funz00_2166,
								BgL_callerz00_2144, ((BgL_appz00_bglt) BgL_nodez00_2143),
								BgL_toz00_2145, BgL_safez00_2146);
						}
					else
						{	/* Tools/trace.sch 53 */
							return
								BGl_coercezd2bigloozd2appz12z12zzcoerce_appz00(BgL_funz00_2166,
								BgL_callerz00_2144, ((BgL_appz00_bglt) BgL_nodez00_2143),
								BgL_toz00_2145, BgL_safez00_2146);
						}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcoerce_appz00(void)
	{
		{	/* Coerce/app.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
			return
				BGl_modulezd2initializa7ationz75zzcoerce_convertz00(87995645L,
				BSTRING_TO_STRING(BGl_string1739z00zzcoerce_appz00));
		}

	}

#ifdef __cplusplus
}
#endif
