/*===========================================================================*/
/*   (SawJvm/compile.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawJvm/compile.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_JVM_COMPILE_TYPE_DEFINITIONS
#define BGL_SAW_JVM_COMPILE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;

	typedef struct BgL_tvecz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}              *BgL_tvecz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_jvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
		obj_t BgL_qnamez00;
		obj_t BgL_classesz00;
		obj_t BgL_currentzd2classzd2;
		obj_t BgL_declarationsz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_codez00;
		obj_t BgL_lightzd2funcallszd2;
		obj_t BgL_inlinez00;
	}             *BgL_jvmz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_indexedz00_bgl
	{
		int BgL_indexz00;
	}                 *BgL_indexedz00_bglt;


#endif													// BGL_SAW_JVM_COMPILE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzsaw_jvm_compilez00 = BUNSPEC;
	extern obj_t BGl_getzd2declaredzd2classesz00zzbackend_cplibz00(void);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_pushzd2stringzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	extern obj_t BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt);
	extern obj_t
		BGl_getzd2declaredzd2fieldsz00zzbackend_cplibz00(BgL_typez00_bglt);
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t
		BGl_getzd2globalzd2variableszd2tozd2bezd2initializa7edz75zzbackend_cplibz00
		(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_jvm_compilez00(void);
	static obj_t BGl_modulezd2mainzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt);
	static obj_t BGl_constructorz00zzsaw_jvm_compilez00(BgL_jvmz00_bglt);
	static bool_t BGl_keyzd2optzf3z21zzsaw_jvm_compilez00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_compilezd2modulezd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_jvm_compilez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_jvm_compilez00(void);
	extern obj_t BGl_bbvz00zzsaw_bbvz00(BgL_backendz00_bglt, BgL_globalz00_bglt,
		obj_t, obj_t);
	extern obj_t BGl_buildzd2treezd2zzsaw_exprz00(BgL_backendz00_bglt, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2debugzd2moduleza2zd2zzengine_paramz00;
	extern obj_t BGl_compilezd2typezd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_typez00_bglt);
	extern BgL_rtl_regz00_bglt
		BGl_localzd2ze3regz31zzsaw_node2rtlz00(BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_modulezd2initzd2cnstz00zzsaw_jvm_compilez00(BgL_jvmz00_bglt,
		obj_t, BgL_valuez00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_compilezd2slotzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_slotz00_bglt);
	extern obj_t BGl_openzd2globalzd2methodz00zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	static obj_t BGl_methodzd2initzd2zzsaw_jvm_compilez00(void);
	extern obj_t BGl_compilezd2globalzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t
		BGl_modulezd2funcallzf2applyz20zzsaw_jvm_funcallz00(BgL_jvmz00_bglt);
	extern obj_t BGl_closezd2classzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_globalzd2ze3blocksz31zzsaw_woodcutterz00(BgL_backendz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_compilezd2classzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt,
		obj_t);
	extern obj_t
		BGl_registerzd2allocationzd2zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt, BgL_globalz00_bglt, obj_t, obj_t);
	extern obj_t BGl_za2jvmzd2cinitzd2moduleza2z00zzengine_paramz00;
	extern obj_t
		BGl_getzd2declaredzd2globalzd2variableszd2zzbackend_cplibz00(obj_t);
	extern BgL_globalz00_bglt
		BGl_globalzd2entryzd2zzbackend_cplibz00(BgL_globalz00_bglt);
	extern obj_t BGl_callzd2globalzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_pushzd2numzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t, obj_t);
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_saw_jvmzd2compilezd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt);
	static obj_t BGl_z62saw_jvmzd2compilezb0zzsaw_jvm_compilez00(obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2itszd2superz00zzbackend_cplibz00(BgL_typez00_bglt);
	extern obj_t BGl_declarezd2globalzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_za2jvmzd2catchza2zd2zzengine_paramz00;
	extern obj_t BGl_getzd2cnstzd2offsetz00zzcnst_allocz00(void);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	extern obj_t BGl_closezd2modulezd2zzsaw_jvm_outz00(BgL_jvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_compilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_codez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_funcallz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_outz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_namesz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzsaw_registerzd2allocationzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_exprz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_woodcutterz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_modulezd2initzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt);
	extern obj_t BGl_codez12z12zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_jvm_compilez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_compilez00(void);
	extern obj_t BGl_globalzd2arityzd2zzbackend_cplibz00(BgL_globalz00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_compilez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_compilez00(void);
	extern obj_t
		BGl_nameszd2initializa7ationz75zzsaw_jvm_namesz00(BgL_jvmz00_bglt);
	static obj_t BGl_modulezd2functionzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt,
		obj_t);
	static bool_t BGl_modulezd2functionszd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt);
	extern obj_t BGl_getzd2cnstzd2tablez00zzcnst_allocz00(void);
	extern obj_t BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		obj_t);
	extern obj_t BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_modulezd2codezd2zzsaw_jvm_codez00(BgL_jvmz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_openzd2classzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_typez00_bglt, obj_t);
	extern obj_t BGl_pushzd2intzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_za2mainza2z00zzmodule_modulez00;
	extern obj_t
		BGl_modulezd2lightzd2funcallz00zzsaw_jvm_funcallz00(BgL_jvmz00_bglt);
	extern obj_t BGl_openzd2modulezd2zzsaw_jvm_outz00(BgL_jvmz00_bglt);
	static obj_t BGl_modulezd2dlopenzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt);
	static obj_t __cnst[74];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_saw_jvmzd2compilezd2envz00zzsaw_jvm_compilez00,
		BgL_bgl_za762saw_jvmza7d2com1997z00,
		BGl_z62saw_jvmzd2compilezb0zzsaw_jvm_compilez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1992z00zzsaw_jvm_compilez00,
		BgL_bgl_string1992za700za7za7s1998za7, "emit-cnst", 9);
	      DEFINE_STRING(BGl_string1993z00zzsaw_jvm_compilez00,
		BgL_bgl_string1993za700za7za7s1999za7, "Unknown cnst class", 18);
	      DEFINE_STRING(BGl_string1994z00zzsaw_jvm_compilez00,
		BgL_bgl_string1994za700za7za7s2000za7, "saw_jvm_compile", 15);
	      DEFINE_STRING(BGl_string1995z00zzsaw_jvm_compilez00,
		BgL_bgl_string1995za700za7za7s2001za7,
		"dlopen (aastore) (dastore) (fastore) (lastore) (iastore) (sastore) (castore) (bastore) anewarray newarray float char boolean stvector sgfun (putfield procenv) slfun selfun (putfield procarity) (putfield procindex) (invokespecial init) (dup) (new me) sfun (invokestatic BGL_INT8_TO_BINT8) byte suint8 sint8 (invokestatic BGL_INT16_TO_BINT16) short suint16 sint16 (invokestatic BGL_INT32_TO_BINT32) int suint32 sint32 (invokestatic BGL_INT64_TO_BINT64) suint64 sint64 (invokestatic llong_to_bllong) sllong (invokestatic elong_to_belong) long selong (invokestatic double_to_real) double sreal (invokestatic getbytes) sstring module-initialization (aconst_null) (iconst_0) putstatic (anewarray obj) (putstatic myname) (invokestatic foreign-print) ldc clinit (invokespecial super-init) (aload this) (this) init (invokestatic internalerror) catch (return) (pop) bigloo_main (invokestatic listargv) (aload argv) from (handler from catch catch throwable) (argv) main ",
		959);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_jvm_compilez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_compilez00(long
		BgL_checksumz00_3622, char *BgL_fromz00_3623)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_jvm_compilez00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_jvm_compilez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_jvm_compilez00();
					BGl_libraryzd2moduleszd2initz00zzsaw_jvm_compilez00();
					BGl_cnstzd2initzd2zzsaw_jvm_compilez00();
					BGl_importedzd2moduleszd2initz00zzsaw_jvm_compilez00();
					return BGl_toplevelzd2initzd2zzsaw_jvm_compilez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_compilez00(void)
	{
		{	/* SawJvm/compile.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "saw_jvm_compile");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_jvm_compile");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_jvm_compilez00(void)
	{
		{	/* SawJvm/compile.scm 1 */
			{	/* SawJvm/compile.scm 1 */
				obj_t BgL_cportz00_3611;

				{	/* SawJvm/compile.scm 1 */
					obj_t BgL_stringz00_3618;

					BgL_stringz00_3618 = BGl_string1995z00zzsaw_jvm_compilez00;
					{	/* SawJvm/compile.scm 1 */
						obj_t BgL_startz00_3619;

						BgL_startz00_3619 = BINT(0L);
						{	/* SawJvm/compile.scm 1 */
							obj_t BgL_endz00_3620;

							BgL_endz00_3620 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3618)));
							{	/* SawJvm/compile.scm 1 */

								BgL_cportz00_3611 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3618, BgL_startz00_3619, BgL_endz00_3620);
				}}}}
				{
					long BgL_iz00_3612;

					BgL_iz00_3612 = 73L;
				BgL_loopz00_3613:
					if ((BgL_iz00_3612 == -1L))
						{	/* SawJvm/compile.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawJvm/compile.scm 1 */
							{	/* SawJvm/compile.scm 1 */
								obj_t BgL_arg1996z00_3614;

								{	/* SawJvm/compile.scm 1 */

									{	/* SawJvm/compile.scm 1 */
										obj_t BgL_locationz00_3616;

										BgL_locationz00_3616 = BBOOL(((bool_t) 0));
										{	/* SawJvm/compile.scm 1 */

											BgL_arg1996z00_3614 =
												BGl_readz00zz__readerz00(BgL_cportz00_3611,
												BgL_locationz00_3616);
										}
									}
								}
								{	/* SawJvm/compile.scm 1 */
									int BgL_tmpz00_3653;

									BgL_tmpz00_3653 = (int) (BgL_iz00_3612);
									CNST_TABLE_SET(BgL_tmpz00_3653, BgL_arg1996z00_3614);
							}}
							{	/* SawJvm/compile.scm 1 */
								int BgL_auxz00_3617;

								BgL_auxz00_3617 = (int) ((BgL_iz00_3612 - 1L));
								{
									long BgL_iz00_3658;

									BgL_iz00_3658 = (long) (BgL_auxz00_3617);
									BgL_iz00_3612 = BgL_iz00_3658;
									goto BgL_loopz00_3613;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_compilez00(void)
	{
		{	/* SawJvm/compile.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_jvm_compilez00(void)
	{
		{	/* SawJvm/compile.scm 1 */
			return BUNSPEC;
		}

	}



/* saw_jvm-compile */
	BGL_EXPORTED_DEF obj_t
		BGl_saw_jvmzd2compilezd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt BgL_mez00_13)
	{
		{	/* SawJvm/compile.scm 40 */
			BGl_nameszd2initializa7ationz75zzsaw_jvm_namesz00(BgL_mez00_13);
			{	/* SawJvm/compile.scm 42 */
				obj_t BgL_g1573z00_2700;

				BgL_g1573z00_2700 = BGl_getzd2declaredzd2classesz00zzbackend_cplibz00();
				{
					obj_t BgL_l1571z00_2702;

					BgL_l1571z00_2702 = BgL_g1573z00_2700;
				BgL_zc3z04anonymousza31612ze3z87_2703:
					if (PAIRP(BgL_l1571z00_2702))
						{	/* SawJvm/compile.scm 42 */
							BGl_compilezd2classzd2zzsaw_jvm_compilez00(BgL_mez00_13,
								CAR(BgL_l1571z00_2702));
							{
								obj_t BgL_l1571z00_3667;

								BgL_l1571z00_3667 = CDR(BgL_l1571z00_2702);
								BgL_l1571z00_2702 = BgL_l1571z00_3667;
								goto BgL_zc3z04anonymousza31612ze3z87_2703;
							}
						}
					else
						{	/* SawJvm/compile.scm 42 */
							((bool_t) 1);
						}
				}
			}
			BGL_TAIL return BGl_compilezd2modulezd2zzsaw_jvm_compilez00(BgL_mez00_13);
		}

	}



/* &saw_jvm-compile */
	obj_t BGl_z62saw_jvmzd2compilezb0zzsaw_jvm_compilez00(obj_t BgL_envz00_3608,
		obj_t BgL_mez00_3609)
	{
		{	/* SawJvm/compile.scm 40 */
			return
				BGl_saw_jvmzd2compilezd2zzsaw_jvm_compilez00(
				((BgL_jvmz00_bglt) BgL_mez00_3609));
		}

	}



/* compile-class */
	obj_t BGl_compilezd2classzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt BgL_mez00_14,
		obj_t BgL_cz00_15)
	{
		{	/* SawJvm/compile.scm 48 */
			{	/* SawJvm/compile.scm 49 */
				BgL_typez00_bglt BgL_arg1616z00_2708;

				BgL_arg1616z00_2708 =
					BGl_getzd2itszd2superz00zzbackend_cplibz00(
					((BgL_typez00_bglt) BgL_cz00_15));
				BGl_openzd2classzd2zzsaw_jvm_outz00(BgL_mez00_14,
					((BgL_typez00_bglt) BgL_cz00_15), ((obj_t) BgL_arg1616z00_2708));
			}
			{	/* SawJvm/compile.scm 50 */
				obj_t BgL_g1576z00_2709;

				BgL_g1576z00_2709 =
					BGl_getzd2declaredzd2fieldsz00zzbackend_cplibz00(
					((BgL_typez00_bglt) BgL_cz00_15));
				{
					obj_t BgL_l1574z00_2711;

					BgL_l1574z00_2711 = BgL_g1576z00_2709;
				BgL_zc3z04anonymousza31617ze3z87_2712:
					if (PAIRP(BgL_l1574z00_2711))
						{	/* SawJvm/compile.scm 50 */
							{	/* SawJvm/compile.scm 50 */
								obj_t BgL_fz00_2714;

								BgL_fz00_2714 = CAR(BgL_l1574z00_2711);
								BGl_compilezd2slotzd2zzsaw_jvm_outz00(BgL_mez00_14,
									((BgL_slotz00_bglt) BgL_fz00_2714));
							}
							{
								obj_t BgL_l1574z00_3684;

								BgL_l1574z00_3684 = CDR(BgL_l1574z00_2711);
								BgL_l1574z00_2711 = BgL_l1574z00_3684;
								goto BgL_zc3z04anonymousza31617ze3z87_2712;
							}
						}
					else
						{	/* SawJvm/compile.scm 50 */
							((bool_t) 1);
						}
				}
			}
			BGl_constructorz00zzsaw_jvm_compilez00(BgL_mez00_14);
			return
				BGl_closezd2classzd2zzsaw_jvm_outz00(BgL_mez00_14,
				((BgL_typez00_bglt) BgL_cz00_15));
		}

	}



/* compile-module */
	obj_t BGl_compilezd2modulezd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt
		BgL_mez00_16)
	{
		{	/* SawJvm/compile.scm 57 */
			BGl_openzd2modulezd2zzsaw_jvm_outz00(BgL_mez00_16);
			{	/* SawJvm/compile.scm 59 */
				obj_t BgL_g1579z00_2717;

				BgL_g1579z00_2717 =
					BGl_getzd2declaredzd2globalzd2variableszd2zzbackend_cplibz00
					(BGl_za2moduleza2z00zzmodule_modulez00);
				{
					obj_t BgL_l1577z00_2719;

					BgL_l1577z00_2719 = BgL_g1579z00_2717;
				BgL_zc3z04anonymousza31626ze3z87_2720:
					if (PAIRP(BgL_l1577z00_2719))
						{	/* SawJvm/compile.scm 60 */
							{	/* SawJvm/compile.scm 59 */
								obj_t BgL_vz00_2722;

								BgL_vz00_2722 = CAR(BgL_l1577z00_2719);
								BGl_compilezd2globalzd2zzsaw_jvm_outz00(BgL_mez00_16,
									((BgL_globalz00_bglt) BgL_vz00_2722));
							}
							{
								obj_t BgL_l1577z00_3696;

								BgL_l1577z00_3696 = CDR(BgL_l1577z00_2719);
								BgL_l1577z00_2719 = BgL_l1577z00_3696;
								goto BgL_zc3z04anonymousza31626ze3z87_2720;
							}
						}
					else
						{	/* SawJvm/compile.scm 60 */
							((bool_t) 1);
						}
				}
			}
			{	/* SawJvm/compile.scm 61 */
				bool_t BgL_test2007z00_3698;

				{	/* SawJvm/compile.scm 61 */
					obj_t BgL_arg1646z00_2728;

					BgL_arg1646z00_2728 = BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
					BgL_test2007z00_3698 = ((long) CINT(BgL_arg1646z00_2728) > 0L);
				}
				if (BgL_test2007z00_3698)
					{	/* SawJvm/compile.scm 61 */
						obj_t BgL_arg1642z00_2727;

						BgL_arg1642z00_2727 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
						BGl_compilezd2globalzd2zzsaw_jvm_outz00(BgL_mez00_16,
							((BgL_globalz00_bglt) BgL_arg1642z00_2727));
					}
				else
					{	/* SawJvm/compile.scm 61 */
						BFALSE;
					}
			}
			{	/* SawJvm/compile.scm 62 */
				bool_t BgL_test2008z00_3705;

				{	/* SawJvm/compile.scm 62 */
					obj_t BgL_objz00_3412;

					BgL_objz00_3412 = BGl_za2mainza2z00zzmodule_modulez00;
					{	/* SawJvm/compile.scm 62 */
						obj_t BgL_classz00_3413;

						BgL_classz00_3413 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_objz00_3412))
							{	/* SawJvm/compile.scm 62 */
								BgL_objectz00_bglt BgL_arg1807z00_3415;

								BgL_arg1807z00_3415 = (BgL_objectz00_bglt) (BgL_objz00_3412);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawJvm/compile.scm 62 */
										long BgL_idxz00_3421;

										BgL_idxz00_3421 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3415);
										BgL_test2008z00_3705 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3421 + 2L)) == BgL_classz00_3413);
									}
								else
									{	/* SawJvm/compile.scm 62 */
										bool_t BgL_res1988z00_3446;

										{	/* SawJvm/compile.scm 62 */
											obj_t BgL_oclassz00_3429;

											{	/* SawJvm/compile.scm 62 */
												obj_t BgL_arg1815z00_3437;
												long BgL_arg1816z00_3438;

												BgL_arg1815z00_3437 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawJvm/compile.scm 62 */
													long BgL_arg1817z00_3439;

													BgL_arg1817z00_3439 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3415);
													BgL_arg1816z00_3438 =
														(BgL_arg1817z00_3439 - OBJECT_TYPE);
												}
												BgL_oclassz00_3429 =
													VECTOR_REF(BgL_arg1815z00_3437, BgL_arg1816z00_3438);
											}
											{	/* SawJvm/compile.scm 62 */
												bool_t BgL__ortest_1115z00_3430;

												BgL__ortest_1115z00_3430 =
													(BgL_classz00_3413 == BgL_oclassz00_3429);
												if (BgL__ortest_1115z00_3430)
													{	/* SawJvm/compile.scm 62 */
														BgL_res1988z00_3446 = BgL__ortest_1115z00_3430;
													}
												else
													{	/* SawJvm/compile.scm 62 */
														long BgL_odepthz00_3431;

														{	/* SawJvm/compile.scm 62 */
															obj_t BgL_arg1804z00_3432;

															BgL_arg1804z00_3432 = (BgL_oclassz00_3429);
															BgL_odepthz00_3431 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3432);
														}
														if ((2L < BgL_odepthz00_3431))
															{	/* SawJvm/compile.scm 62 */
																obj_t BgL_arg1802z00_3434;

																{	/* SawJvm/compile.scm 62 */
																	obj_t BgL_arg1803z00_3435;

																	BgL_arg1803z00_3435 = (BgL_oclassz00_3429);
																	BgL_arg1802z00_3434 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3435,
																		2L);
																}
																BgL_res1988z00_3446 =
																	(BgL_arg1802z00_3434 == BgL_classz00_3413);
															}
														else
															{	/* SawJvm/compile.scm 62 */
																BgL_res1988z00_3446 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2008z00_3705 = BgL_res1988z00_3446;
									}
							}
						else
							{	/* SawJvm/compile.scm 62 */
								BgL_test2008z00_3705 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2008z00_3705)
					{	/* SawJvm/compile.scm 62 */
						BGl_modulezd2mainzd2zzsaw_jvm_compilez00(BgL_mez00_16);
					}
				else
					{	/* SawJvm/compile.scm 62 */
						BFALSE;
					}
			}
			BGl_constructorz00zzsaw_jvm_compilez00(BgL_mez00_16);
			BGl_modulezd2funcallzf2applyz20zzsaw_jvm_funcallz00(BgL_mez00_16);
			BGl_modulezd2initzd2zzsaw_jvm_compilez00(BgL_mez00_16);
			BGl_modulezd2dlopenzd2zzsaw_jvm_compilez00(BgL_mez00_16);
			BGl_modulezd2functionszd2zzsaw_jvm_compilez00(BgL_mez00_16);
			BGl_modulezd2lightzd2funcallz00zzsaw_jvm_funcallz00(BgL_mez00_16);
			BGL_TAIL return BGl_closezd2modulezd2zzsaw_jvm_outz00(BgL_mez00_16);
		}

	}



/* module-main */
	obj_t BGl_modulezd2mainzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt BgL_mez00_17)
	{
		{	/* SawJvm/compile.scm 74 */
			BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_mez00_17,
				CNST_TABLE_REF(0));
			BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_mez00_17,
				CNST_TABLE_REF(1), BNIL);
			if (CBOOL(BGl_za2jvmzd2catchza2zd2zzengine_paramz00))
				{	/* SawJvm/compile.scm 77 */
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(2));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(3));
				}
			else
				{	/* SawJvm/compile.scm 77 */
					BFALSE;
				}
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(4));
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(5));
			{	/* SawJvm/compile.scm 82 */
				obj_t BgL_arg1650z00_2730;

				{	/* SawJvm/compile.scm 82 */
					obj_t BgL_list1651z00_2731;

					BgL_list1651z00_2731 =
						MAKE_YOUNG_PAIR(BGl_za2moduleza2z00zzmodule_modulez00, BNIL);
					BgL_arg1650z00_2730 =
						BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(6),
						BgL_list1651z00_2731);
				}
				BGl_callzd2globalzd2zzsaw_jvm_outz00(BgL_mez00_17,
					((BgL_globalz00_bglt) BgL_arg1650z00_2730));
			}
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(7));
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(8));
			if (CBOOL(BGl_za2jvmzd2catchza2zd2zzengine_paramz00))
				{	/* SawJvm/compile.scm 85 */
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(9));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(10));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_17, CNST_TABLE_REF(8));
				}
			else
				{	/* SawJvm/compile.scm 85 */
					BFALSE;
				}
			BGL_TAIL return BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_17);
		}

	}



/* constructor */
	obj_t BGl_constructorz00zzsaw_jvm_compilez00(BgL_jvmz00_bglt BgL_mez00_18)
	{
		{	/* SawJvm/compile.scm 94 */
			BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_mez00_18,
				CNST_TABLE_REF(11));
			BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_mez00_18,
				CNST_TABLE_REF(12), BNIL);
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_18, CNST_TABLE_REF(13));
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_18, CNST_TABLE_REF(14));
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_18, CNST_TABLE_REF(8));
			BGL_TAIL return BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_18);
		}

	}



/* module-init */
	obj_t BGl_modulezd2initzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt BgL_mez00_19)
	{
		{	/* SawJvm/compile.scm 105 */
			BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_mez00_19,
				CNST_TABLE_REF(15));
			BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_mez00_19, BNIL, BNIL);
			{	/* SawJvm/compile.scm 109 */
				bool_t BgL_test2015z00_3782;

				if (INTEGERP(BGl_za2debugzd2moduleza2zd2zzengine_paramz00))
					{	/* SawJvm/compile.scm 109 */
						BgL_test2015z00_3782 =
							((long) CINT(BGl_za2debugzd2moduleza2zd2zzengine_paramz00) > 0L);
					}
				else
					{	/* SawJvm/compile.scm 109 */
						BgL_test2015z00_3782 =
							BGl_2ze3ze3zz__r4_numbers_6_5z00
							(BGl_za2debugzd2moduleza2zd2zzengine_paramz00, BINT(0L));
					}
				if (BgL_test2015z00_3782)
					{	/* SawJvm/compile.scm 109 */
						{	/* SawJvm/compile.scm 110 */
							obj_t BgL_arg1654z00_2733;

							{	/* SawJvm/compile.scm 110 */
								obj_t BgL_arg1661z00_2734;

								{	/* SawJvm/compile.scm 110 */
									obj_t BgL_arg1663z00_2735;

									{	/* SawJvm/compile.scm 110 */
										obj_t BgL_symbolz00_3448;

										BgL_symbolz00_3448 = BGl_za2moduleza2z00zzmodule_modulez00;
										{	/* SawJvm/compile.scm 110 */
											obj_t BgL_arg1455z00_3449;

											BgL_arg1455z00_3449 =
												SYMBOL_TO_STRING(BgL_symbolz00_3448);
											BgL_arg1663z00_2735 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_3449);
										}
									}
									BgL_arg1661z00_2734 =
										MAKE_YOUNG_PAIR(BgL_arg1663z00_2735, BNIL);
								}
								BgL_arg1654z00_2733 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1661z00_2734);
							}
							BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, BgL_arg1654z00_2733);
						}
						BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, CNST_TABLE_REF(17));
					}
				else
					{	/* SawJvm/compile.scm 109 */
						BFALSE;
					}
			}
			{	/* SawJvm/compile.scm 113 */
				obj_t BgL_arg1675z00_2736;

				{	/* SawJvm/compile.scm 113 */
					obj_t BgL_arg1678z00_2737;

					{	/* SawJvm/compile.scm 113 */
						obj_t BgL_arg1681z00_2738;

						{	/* SawJvm/compile.scm 113 */
							obj_t BgL_symbolz00_3450;

							BgL_symbolz00_3450 = BGl_za2moduleza2z00zzmodule_modulez00;
							{	/* SawJvm/compile.scm 113 */
								obj_t BgL_arg1455z00_3451;

								BgL_arg1455z00_3451 = SYMBOL_TO_STRING(BgL_symbolz00_3450);
								BgL_arg1681z00_2738 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_3451);
							}
						}
						BgL_arg1678z00_2737 = MAKE_YOUNG_PAIR(BgL_arg1681z00_2738, BNIL);
					}
					BgL_arg1675z00_2736 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1678z00_2737);
				}
				BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, BgL_arg1675z00_2736);
			}
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, CNST_TABLE_REF(18));
			{	/* SawJvm/compile.scm 116 */
				obj_t BgL_siza7eza7_2739;

				BgL_siza7eza7_2739 = BGl_getzd2cnstzd2offsetz00zzcnst_allocz00();
				if (((long) CINT(BgL_siza7eza7_2739) > 0L))
					{	/* SawJvm/compile.scm 117 */
						BGl_pushzd2intzd2zzsaw_jvm_outz00(BgL_mez00_19, BgL_siza7eza7_2739);
						BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, CNST_TABLE_REF(19));
						{	/* SawJvm/compile.scm 120 */
							obj_t BgL_arg1688z00_2741;

							{	/* SawJvm/compile.scm 120 */
								obj_t BgL_arg1689z00_2742;

								{	/* SawJvm/compile.scm 120 */
									obj_t BgL_arg1691z00_2743;

									{	/* SawJvm/compile.scm 120 */
										obj_t BgL_arg1692z00_2744;

										BgL_arg1692z00_2744 =
											BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
										BgL_arg1691z00_2743 =
											BGl_declarezd2globalzd2zzsaw_jvm_outz00(BgL_mez00_19,
											((BgL_globalz00_bglt) BgL_arg1692z00_2744));
									}
									BgL_arg1689z00_2742 =
										MAKE_YOUNG_PAIR(BgL_arg1691z00_2743, BNIL);
								}
								BgL_arg1688z00_2741 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1689z00_2742);
							}
							BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, BgL_arg1688z00_2741);
					}}
				else
					{	/* SawJvm/compile.scm 117 */
						BFALSE;
					}
			}
			{	/* SawJvm/compile.scm 122 */
				obj_t BgL_g1582z00_2745;

				BgL_g1582z00_2745 =
					BGl_getzd2globalzd2variableszd2tozd2bezd2initializa7edz75zzbackend_cplibz00
					(BGl_za2moduleza2z00zzmodule_modulez00);
				{
					obj_t BgL_l1580z00_2747;

					BgL_l1580z00_2747 = BgL_g1582z00_2745;
				BgL_zc3z04anonymousza31693ze3z87_2748:
					if (PAIRP(BgL_l1580z00_2747))
						{	/* SawJvm/compile.scm 123 */
							{	/* SawJvm/compile.scm 122 */
								obj_t BgL_vz00_2750;

								BgL_vz00_2750 = CAR(BgL_l1580z00_2747);
								{	/* SawJvm/compile.scm 122 */
									BgL_valuez00_bglt BgL_arg1699z00_2751;

									BgL_arg1699z00_2751 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_vz00_2750))))->
										BgL_valuez00);
									BGl_modulezd2initzd2cnstz00zzsaw_jvm_compilez00(BgL_mez00_19,
										BgL_vz00_2750, BgL_arg1699z00_2751);
								}
							}
							{
								obj_t BgL_l1580z00_3827;

								BgL_l1580z00_3827 = CDR(BgL_l1580z00_2747);
								BgL_l1580z00_2747 = BgL_l1580z00_3827;
								goto BgL_zc3z04anonymousza31693ze3z87_2748;
							}
						}
					else
						{	/* SawJvm/compile.scm 123 */
							((bool_t) 1);
						}
				}
			}
			if (CBOOL(BGl_za2jvmzd2cinitzd2moduleza2z00zzengine_paramz00))
				{	/* SawJvm/compile.scm 124 */
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, CNST_TABLE_REF(21));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, CNST_TABLE_REF(22));
					{	/* SawJvm/compile.scm 128 */
						obj_t BgL_arg1701z00_2754;

						{	/* SawJvm/compile.scm 128 */
							obj_t BgL_list1702z00_2755;

							BgL_list1702z00_2755 =
								MAKE_YOUNG_PAIR(BGl_za2moduleza2z00zzmodule_modulez00, BNIL);
							BgL_arg1701z00_2754 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(23),
								BgL_list1702z00_2755);
						}
						BGl_callzd2globalzd2zzsaw_jvm_outz00(BgL_mez00_19,
							((BgL_globalz00_bglt) BgL_arg1701z00_2754));
					}
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, CNST_TABLE_REF(7));
				}
			else
				{	/* SawJvm/compile.scm 124 */
					BFALSE;
				}
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_19, CNST_TABLE_REF(8));
			BGL_TAIL return BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_19);
		}

	}



/* key-opt? */
	bool_t BGl_keyzd2optzf3z21zzsaw_jvm_compilez00(obj_t BgL_vz00_20)
	{
		{	/* SawJvm/compile.scm 133 */
			{	/* SawJvm/compile.scm 134 */
				BgL_globalz00_bglt BgL_vz00_2756;

				BgL_vz00_2756 =
					BGl_globalzd2entryzd2zzbackend_cplibz00(
					((BgL_globalz00_bglt) BgL_vz00_20));
				{	/* SawJvm/compile.scm 135 */
					BgL_valuez00_bglt BgL_valz00_2757;

					BgL_valz00_2757 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_vz00_2756)))->BgL_valuez00);
					{	/* SawJvm/compile.scm 136 */
						bool_t BgL_test2020z00_3849;

						{	/* SawJvm/compile.scm 136 */
							obj_t BgL_classz00_3457;

							BgL_classz00_3457 = BGl_sfunz00zzast_varz00;
							{	/* SawJvm/compile.scm 136 */
								BgL_objectz00_bglt BgL_arg1807z00_3459;

								{	/* SawJvm/compile.scm 136 */
									obj_t BgL_tmpz00_3850;

									BgL_tmpz00_3850 =
										((obj_t) ((BgL_objectz00_bglt) BgL_valz00_2757));
									BgL_arg1807z00_3459 = (BgL_objectz00_bglt) (BgL_tmpz00_3850);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawJvm/compile.scm 136 */
										long BgL_idxz00_3465;

										BgL_idxz00_3465 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3459);
										BgL_test2020z00_3849 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3465 + 3L)) == BgL_classz00_3457);
									}
								else
									{	/* SawJvm/compile.scm 136 */
										bool_t BgL_res1989z00_3490;

										{	/* SawJvm/compile.scm 136 */
											obj_t BgL_oclassz00_3473;

											{	/* SawJvm/compile.scm 136 */
												obj_t BgL_arg1815z00_3481;
												long BgL_arg1816z00_3482;

												BgL_arg1815z00_3481 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawJvm/compile.scm 136 */
													long BgL_arg1817z00_3483;

													BgL_arg1817z00_3483 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3459);
													BgL_arg1816z00_3482 =
														(BgL_arg1817z00_3483 - OBJECT_TYPE);
												}
												BgL_oclassz00_3473 =
													VECTOR_REF(BgL_arg1815z00_3481, BgL_arg1816z00_3482);
											}
											{	/* SawJvm/compile.scm 136 */
												bool_t BgL__ortest_1115z00_3474;

												BgL__ortest_1115z00_3474 =
													(BgL_classz00_3457 == BgL_oclassz00_3473);
												if (BgL__ortest_1115z00_3474)
													{	/* SawJvm/compile.scm 136 */
														BgL_res1989z00_3490 = BgL__ortest_1115z00_3474;
													}
												else
													{	/* SawJvm/compile.scm 136 */
														long BgL_odepthz00_3475;

														{	/* SawJvm/compile.scm 136 */
															obj_t BgL_arg1804z00_3476;

															BgL_arg1804z00_3476 = (BgL_oclassz00_3473);
															BgL_odepthz00_3475 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3476);
														}
														if ((3L < BgL_odepthz00_3475))
															{	/* SawJvm/compile.scm 136 */
																obj_t BgL_arg1802z00_3478;

																{	/* SawJvm/compile.scm 136 */
																	obj_t BgL_arg1803z00_3479;

																	BgL_arg1803z00_3479 = (BgL_oclassz00_3473);
																	BgL_arg1802z00_3478 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3479,
																		3L);
																}
																BgL_res1989z00_3490 =
																	(BgL_arg1802z00_3478 == BgL_classz00_3457);
															}
														else
															{	/* SawJvm/compile.scm 136 */
																BgL_res1989z00_3490 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2020z00_3849 = BgL_res1989z00_3490;
									}
							}
						}
						if (BgL_test2020z00_3849)
							{	/* SawJvm/compile.scm 137 */
								obj_t BgL_cloz00_2759;

								BgL_cloz00_2759 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_valz00_2757)))->
									BgL_thezd2closurezd2globalz00);
								{	/* SawJvm/compile.scm 138 */
									bool_t BgL_oz00_2760;
									bool_t BgL_kz00_2761;

									BgL_oz00_2760 =
										BGl_globalzd2optionalzf3z21zzast_varz00(BgL_cloz00_2759);
									BgL_kz00_2761 =
										BGl_globalzd2keyzf3z21zzast_varz00(BgL_cloz00_2759);
									if (BgL_oz00_2760)
										{	/* SawJvm/compile.scm 139 */
											return BgL_oz00_2760;
										}
									else
										{	/* SawJvm/compile.scm 139 */
											return BgL_kz00_2761;
										}
								}
							}
						else
							{	/* SawJvm/compile.scm 136 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* module-init-cnst */
	obj_t BGl_modulezd2initzd2cnstz00zzsaw_jvm_compilez00(BgL_jvmz00_bglt
		BgL_mez00_21, obj_t BgL_varz00_22, BgL_valuez00_bglt BgL_valz00_23)
	{
		{	/* SawJvm/compile.scm 141 */
			{	/* SawJvm/compile.scm 143 */
				obj_t BgL_casezd2valuezd2_2764;

				BgL_casezd2valuezd2_2764 =
					(((BgL_scnstz00_bglt) COBJECT(
							((BgL_scnstz00_bglt) BgL_valz00_23)))->BgL_classz00);
				if ((BgL_casezd2valuezd2_2764 == CNST_TABLE_REF(24)))
					{	/* SawJvm/compile.scm 143 */
						{	/* SawJvm/compile.scm 145 */
							obj_t BgL_arg1705z00_2766;

							BgL_arg1705z00_2766 =
								(((BgL_scnstz00_bglt) COBJECT(
										((BgL_scnstz00_bglt) BgL_valz00_23)))->BgL_nodez00);
							BGl_pushzd2stringzd2zzsaw_jvm_outz00(BgL_mez00_21,
								BgL_arg1705z00_2766);
						}
						BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_21, CNST_TABLE_REF(25));
					}
				else
					{	/* SawJvm/compile.scm 143 */
						if ((BgL_casezd2valuezd2_2764 == CNST_TABLE_REF(26)))
							{	/* SawJvm/compile.scm 143 */
								{	/* SawJvm/compile.scm 148 */
									obj_t BgL_arg1708z00_2768;

									BgL_arg1708z00_2768 =
										(((BgL_scnstz00_bglt) COBJECT(
												((BgL_scnstz00_bglt) BgL_valz00_23)))->BgL_nodez00);
									BGl_pushzd2numzd2zzsaw_jvm_outz00(BgL_mez00_21,
										BgL_arg1708z00_2768, CNST_TABLE_REF(27));
								}
								BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_21,
									CNST_TABLE_REF(28));
							}
						else
							{	/* SawJvm/compile.scm 143 */
								if ((BgL_casezd2valuezd2_2764 == CNST_TABLE_REF(29)))
									{	/* SawJvm/compile.scm 143 */
										{	/* SawJvm/compile.scm 151 */
											obj_t BgL_arg1714z00_2770;

											BgL_arg1714z00_2770 =
												(((BgL_scnstz00_bglt) COBJECT(
														((BgL_scnstz00_bglt) BgL_valz00_23)))->BgL_nodez00);
											BGl_pushzd2numzd2zzsaw_jvm_outz00(BgL_mez00_21,
												BgL_arg1714z00_2770, CNST_TABLE_REF(30));
										}
										BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_21,
											CNST_TABLE_REF(31));
									}
								else
									{	/* SawJvm/compile.scm 143 */
										if ((BgL_casezd2valuezd2_2764 == CNST_TABLE_REF(32)))
											{	/* SawJvm/compile.scm 143 */
												{	/* SawJvm/compile.scm 154 */
													obj_t BgL_arg1717z00_2772;

													BgL_arg1717z00_2772 =
														(((BgL_scnstz00_bglt) COBJECT(
																((BgL_scnstz00_bglt) BgL_valz00_23)))->
														BgL_nodez00);
													BGl_pushzd2numzd2zzsaw_jvm_outz00(BgL_mez00_21,
														BgL_arg1717z00_2772, CNST_TABLE_REF(30));
												}
												BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_21,
													CNST_TABLE_REF(33));
											}
										else
											{	/* SawJvm/compile.scm 143 */
												bool_t BgL_test2029z00_3915;

												{	/* SawJvm/compile.scm 143 */
													bool_t BgL__ortest_1170z00_2865;

													BgL__ortest_1170z00_2865 =
														(BgL_casezd2valuezd2_2764 == CNST_TABLE_REF(34));
													if (BgL__ortest_1170z00_2865)
														{	/* SawJvm/compile.scm 143 */
															BgL_test2029z00_3915 = BgL__ortest_1170z00_2865;
														}
													else
														{	/* SawJvm/compile.scm 143 */
															BgL_test2029z00_3915 =
																(BgL_casezd2valuezd2_2764 ==
																CNST_TABLE_REF(35));
														}
												}
												if (BgL_test2029z00_3915)
													{	/* SawJvm/compile.scm 143 */
														{	/* SawJvm/compile.scm 157 */
															obj_t BgL_arg1720z00_2775;

															BgL_arg1720z00_2775 =
																(((BgL_scnstz00_bglt) COBJECT(
																		((BgL_scnstz00_bglt) BgL_valz00_23)))->
																BgL_nodez00);
															BGl_pushzd2numzd2zzsaw_jvm_outz00(BgL_mez00_21,
																BgL_arg1720z00_2775, CNST_TABLE_REF(30));
														}
														BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_21,
															CNST_TABLE_REF(36));
													}
												else
													{	/* SawJvm/compile.scm 143 */
														bool_t BgL_test2031z00_3927;

														{	/* SawJvm/compile.scm 143 */
															bool_t BgL__ortest_1171z00_2864;

															BgL__ortest_1171z00_2864 =
																(BgL_casezd2valuezd2_2764 ==
																CNST_TABLE_REF(37));
															if (BgL__ortest_1171z00_2864)
																{	/* SawJvm/compile.scm 143 */
																	BgL_test2031z00_3927 =
																		BgL__ortest_1171z00_2864;
																}
															else
																{	/* SawJvm/compile.scm 143 */
																	BgL_test2031z00_3927 =
																		(BgL_casezd2valuezd2_2764 ==
																		CNST_TABLE_REF(38));
																}
														}
														if (BgL_test2031z00_3927)
															{	/* SawJvm/compile.scm 143 */
																{	/* SawJvm/compile.scm 160 */
																	obj_t BgL_arg1722z00_2778;

																	BgL_arg1722z00_2778 =
																		(((BgL_scnstz00_bglt) COBJECT(
																				((BgL_scnstz00_bglt) BgL_valz00_23)))->
																		BgL_nodez00);
																	BGl_pushzd2numzd2zzsaw_jvm_outz00
																		(BgL_mez00_21, BgL_arg1722z00_2778,
																		CNST_TABLE_REF(39));
																}
																BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_21,
																	CNST_TABLE_REF(40));
															}
														else
															{	/* SawJvm/compile.scm 143 */
																bool_t BgL_test2033z00_3939;

																{	/* SawJvm/compile.scm 143 */
																	bool_t BgL__ortest_1172z00_2863;

																	BgL__ortest_1172z00_2863 =
																		(BgL_casezd2valuezd2_2764 ==
																		CNST_TABLE_REF(41));
																	if (BgL__ortest_1172z00_2863)
																		{	/* SawJvm/compile.scm 143 */
																			BgL_test2033z00_3939 =
																				BgL__ortest_1172z00_2863;
																		}
																	else
																		{	/* SawJvm/compile.scm 143 */
																			BgL_test2033z00_3939 =
																				(BgL_casezd2valuezd2_2764 ==
																				CNST_TABLE_REF(42));
																		}
																}
																if (BgL_test2033z00_3939)
																	{	/* SawJvm/compile.scm 143 */
																		{	/* SawJvm/compile.scm 163 */
																			obj_t BgL_arg1724z00_2781;

																			BgL_arg1724z00_2781 =
																				(((BgL_scnstz00_bglt) COBJECT(
																						((BgL_scnstz00_bglt)
																							BgL_valz00_23)))->BgL_nodez00);
																			BGl_pushzd2numzd2zzsaw_jvm_outz00
																				(BgL_mez00_21, BgL_arg1724z00_2781,
																				CNST_TABLE_REF(43));
																		}
																		BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_21,
																			CNST_TABLE_REF(44));
																	}
																else
																	{	/* SawJvm/compile.scm 143 */
																		bool_t BgL_test2035z00_3951;

																		{	/* SawJvm/compile.scm 143 */
																			bool_t BgL__ortest_1173z00_2862;

																			BgL__ortest_1173z00_2862 =
																				(BgL_casezd2valuezd2_2764 ==
																				CNST_TABLE_REF(45));
																			if (BgL__ortest_1173z00_2862)
																				{	/* SawJvm/compile.scm 143 */
																					BgL_test2035z00_3951 =
																						BgL__ortest_1173z00_2862;
																				}
																			else
																				{	/* SawJvm/compile.scm 143 */
																					BgL_test2035z00_3951 =
																						(BgL_casezd2valuezd2_2764 ==
																						CNST_TABLE_REF(46));
																				}
																		}
																		if (BgL_test2035z00_3951)
																			{	/* SawJvm/compile.scm 143 */
																				{	/* SawJvm/compile.scm 166 */
																					obj_t BgL_arg1733z00_2784;

																					BgL_arg1733z00_2784 =
																						(((BgL_scnstz00_bglt) COBJECT(
																								((BgL_scnstz00_bglt)
																									BgL_valz00_23)))->
																						BgL_nodez00);
																					BGl_pushzd2numzd2zzsaw_jvm_outz00
																						(BgL_mez00_21, BgL_arg1733z00_2784,
																						CNST_TABLE_REF(47));
																				}
																				BGl_codez12z12zzsaw_jvm_outz00
																					(BgL_mez00_21, CNST_TABLE_REF(48));
																			}
																		else
																			{	/* SawJvm/compile.scm 143 */
																				if (
																					(BgL_casezd2valuezd2_2764 ==
																						CNST_TABLE_REF(49)))
																					{	/* SawJvm/compile.scm 143 */
																						BGl_codez12z12zzsaw_jvm_outz00
																							(BgL_mez00_21,
																							CNST_TABLE_REF(50));
																						BGl_codez12z12zzsaw_jvm_outz00
																							(BgL_mez00_21,
																							CNST_TABLE_REF(51));
																						BGl_codez12z12zzsaw_jvm_outz00
																							(BgL_mez00_21,
																							CNST_TABLE_REF(52));
																						BGl_codez12z12zzsaw_jvm_outz00
																							(BgL_mez00_21,
																							CNST_TABLE_REF(51));
																						{	/* SawJvm/compile.scm 173 */
																							int BgL_arg1735z00_2786;

																							{
																								BgL_indexedz00_bglt
																									BgL_auxz00_3974;
																								{
																									obj_t BgL_auxz00_3975;

																									{	/* SawJvm/compile.scm 173 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_3976;
																										BgL_tmpz00_3976 =
																											((BgL_objectz00_bglt) (
																												(BgL_globalz00_bglt)
																												BgL_varz00_22));
																										BgL_auxz00_3975 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_3976);
																									}
																									BgL_auxz00_3974 =
																										((BgL_indexedz00_bglt)
																										BgL_auxz00_3975);
																								}
																								BgL_arg1735z00_2786 =
																									(((BgL_indexedz00_bglt)
																										COBJECT(BgL_auxz00_3974))->
																									BgL_indexz00);
																							}
																							BGl_pushzd2intzd2zzsaw_jvm_outz00
																								(BgL_mez00_21,
																								BINT(BgL_arg1735z00_2786));
																						}
																						BGl_codez12z12zzsaw_jvm_outz00
																							(BgL_mez00_21,
																							CNST_TABLE_REF(53));
																						BGl_codez12z12zzsaw_jvm_outz00
																							(BgL_mez00_21,
																							CNST_TABLE_REF(51));
																						{	/* SawJvm/compile.scm 176 */
																							obj_t BgL_arg1736z00_2787;

																							if (BGl_keyzd2optzf3z21zzsaw_jvm_compilez00(BgL_varz00_22))
																								{	/* SawJvm/compile.scm 176 */
																									BgL_arg1736z00_2787 =
																										BINT(-1L);
																								}
																							else
																								{	/* SawJvm/compile.scm 176 */
																									BgL_arg1736z00_2787 =
																										BGl_globalzd2arityzd2zzbackend_cplibz00
																										(((BgL_globalz00_bglt)
																											BgL_varz00_22));
																								}
																							BGl_pushzd2intzd2zzsaw_jvm_outz00
																								(BgL_mez00_21,
																								BgL_arg1736z00_2787);
																						}
																						BGl_codez12z12zzsaw_jvm_outz00
																							(BgL_mez00_21,
																							CNST_TABLE_REF(54));
																					}
																				else
																					{	/* SawJvm/compile.scm 143 */
																						if (
																							(BgL_casezd2valuezd2_2764 ==
																								CNST_TABLE_REF(55)))
																							{	/* SawJvm/compile.scm 143 */
																								BGl_codez12z12zzsaw_jvm_outz00
																									(BgL_mez00_21,
																									CNST_TABLE_REF(22));
																							}
																						else
																							{	/* SawJvm/compile.scm 143 */
																								if (
																									(BgL_casezd2valuezd2_2764 ==
																										CNST_TABLE_REF(56)))
																									{	/* SawJvm/compile.scm 143 */
																										BGl_codez12z12zzsaw_jvm_outz00
																											(BgL_mez00_21,
																											CNST_TABLE_REF(50));
																										BGl_codez12z12zzsaw_jvm_outz00
																											(BgL_mez00_21,
																											CNST_TABLE_REF(51));
																										BGl_codez12z12zzsaw_jvm_outz00
																											(BgL_mez00_21,
																											CNST_TABLE_REF(52));
																										BGl_codez12z12zzsaw_jvm_outz00
																											(BgL_mez00_21,
																											CNST_TABLE_REF(51));
																										{	/* SawJvm/compile.scm 186 */
																											int BgL_arg1740z00_2791;

																											{
																												BgL_indexedz00_bglt
																													BgL_auxz00_4012;
																												{
																													obj_t BgL_auxz00_4013;

																													{	/* SawJvm/compile.scm 186 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_4014;
																														BgL_tmpz00_4014 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_globalz00_bglt) BgL_varz00_22));
																														BgL_auxz00_4013 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_4014);
																													}
																													BgL_auxz00_4012 =
																														(
																														(BgL_indexedz00_bglt)
																														BgL_auxz00_4013);
																												}
																												BgL_arg1740z00_2791 =
																													(((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_4012))->BgL_indexz00);
																											}
																											BGl_pushzd2intzd2zzsaw_jvm_outz00
																												(BgL_mez00_21,
																												BINT
																												(BgL_arg1740z00_2791));
																										}
																										BGl_codez12z12zzsaw_jvm_outz00
																											(BgL_mez00_21,
																											CNST_TABLE_REF(53));
																										BGl_codez12z12zzsaw_jvm_outz00
																											(BgL_mez00_21,
																											CNST_TABLE_REF(51));
																										BGl_pushzd2intzd2zzsaw_jvm_outz00
																											(BgL_mez00_21, BINT(0L));
																										BGl_codez12z12zzsaw_jvm_outz00
																											(BgL_mez00_21,
																											CNST_TABLE_REF(19));
																										BGl_codez12z12zzsaw_jvm_outz00
																											(BgL_mez00_21,
																											CNST_TABLE_REF(57));
																									}
																								else
																									{	/* SawJvm/compile.scm 143 */
																										if (
																											(BgL_casezd2valuezd2_2764
																												== CNST_TABLE_REF(58)))
																											{	/* SawJvm/compile.scm 143 */
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(50));
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(51));
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(52));
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(51));
																												{	/* SawJvm/compile.scm 197 */
																													int
																														BgL_arg1746z00_2793;
																													{
																														BgL_indexedz00_bglt
																															BgL_auxz00_4043;
																														{
																															obj_t
																																BgL_auxz00_4044;
																															{	/* SawJvm/compile.scm 197 */
																																BgL_objectz00_bglt
																																	BgL_tmpz00_4045;
																																BgL_tmpz00_4045
																																	=
																																	(
																																	(BgL_objectz00_bglt)
																																	((BgL_globalz00_bglt) BgL_varz00_22));
																																BgL_auxz00_4044
																																	=
																																	BGL_OBJECT_WIDENING
																																	(BgL_tmpz00_4045);
																															}
																															BgL_auxz00_4043 =
																																(
																																(BgL_indexedz00_bglt)
																																BgL_auxz00_4044);
																														}
																														BgL_arg1746z00_2793
																															=
																															(((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_4043))->BgL_indexz00);
																													}
																													BGl_pushzd2intzd2zzsaw_jvm_outz00
																														(BgL_mez00_21,
																														BINT
																														(BgL_arg1746z00_2793));
																												}
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(53));
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(51));
																												{	/* SawJvm/compile.scm 200 */
																													obj_t
																														BgL_arg1747z00_2794;
																													if (BGl_keyzd2optzf3z21zzsaw_jvm_compilez00(BgL_varz00_22))
																														{	/* SawJvm/compile.scm 200 */
																															BgL_arg1747z00_2794
																																= BINT(-1L);
																														}
																													else
																														{	/* SawJvm/compile.scm 200 */
																															BgL_arg1747z00_2794
																																=
																																BGl_globalzd2arityzd2zzbackend_cplibz00
																																(((BgL_globalz00_bglt) BgL_varz00_22));
																														}
																													BGl_pushzd2intzd2zzsaw_jvm_outz00
																														(BgL_mez00_21,
																														BgL_arg1747z00_2794);
																												}
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(54));
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(51));
																												BGl_pushzd2intzd2zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													BINT(3L));
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(19));
																												BGl_codez12z12zzsaw_jvm_outz00
																													(BgL_mez00_21,
																													CNST_TABLE_REF(57));
																											}
																										else
																											{	/* SawJvm/compile.scm 143 */
																												if (
																													(BgL_casezd2valuezd2_2764
																														==
																														CNST_TABLE_REF(59)))
																													{	/* SawJvm/compile.scm 208 */
																														obj_t
																															BgL_vecz00_2797;
																														{	/* SawJvm/compile.scm 208 */
																															obj_t
																																BgL_sz00_3507;
																															BgL_sz00_3507 =
																																(((BgL_scnstz00_bglt) COBJECT(((BgL_scnstz00_bglt) BgL_valz00_23)))->BgL_nodez00);
																															BgL_vecz00_2797 =
																																STRUCT_REF
																																(BgL_sz00_3507,
																																(int) (1L));
																														}
																														{	/* SawJvm/compile.scm 208 */
																															BgL_typez00_bglt
																																BgL_typez00_2798;
																															{	/* SawJvm/compile.scm 209 */
																																BgL_typez00_bglt
																																	BgL_oz00_3509;
																																{	/* SawJvm/compile.scm 209 */
																																	obj_t
																																		BgL_sz00_3508;
																																	BgL_sz00_3508
																																		=
																																		(((BgL_scnstz00_bglt) COBJECT(((BgL_scnstz00_bglt) BgL_valz00_23)))->BgL_nodez00);
																																	BgL_oz00_3509
																																		=
																																		(
																																		(BgL_typez00_bglt)
																																		STRUCT_REF
																																		(BgL_sz00_3508,
																																			(int)
																																			(0L)));
																																}
																																{
																																	BgL_tvecz00_bglt
																																		BgL_auxz00_4085;
																																	{
																																		obj_t
																																			BgL_auxz00_4086;
																																		{	/* SawJvm/compile.scm 209 */
																																			BgL_objectz00_bglt
																																				BgL_tmpz00_4087;
																																			BgL_tmpz00_4087
																																				=
																																				(
																																				(BgL_objectz00_bglt)
																																				BgL_oz00_3509);
																																			BgL_auxz00_4086
																																				=
																																				BGL_OBJECT_WIDENING
																																				(BgL_tmpz00_4087);
																																		}
																																		BgL_auxz00_4085
																																			=
																																			(
																																			(BgL_tvecz00_bglt)
																																			BgL_auxz00_4086);
																																	}
																																	BgL_typez00_2798
																																		=
																																		(((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_4085))->BgL_itemzd2typezd2);
																															}}
																															{	/* SawJvm/compile.scm 209 */
																																obj_t
																																	BgL_idz00_2799;
																																BgL_idz00_2799 =
																																	(((BgL_typez00_bglt) COBJECT(BgL_typez00_2798))->BgL_namez00);
																																{	/* SawJvm/compile.scm 211 */

																																	BGl_pushzd2intzd2zzsaw_jvm_outz00
																																		(BgL_mez00_21,
																																		BINT
																																		(VECTOR_LENGTH
																																			(((obj_t)
																																					BgL_vecz00_2797))));
																																	{	/* SawJvm/compile.scm 213 */
																																		bool_t
																																			BgL_test2044z00_4097;
																																		{	/* SawJvm/compile.scm 213 */
																																			bool_t
																																				BgL__ortest_1174z00_2816;
																																			BgL__ortest_1174z00_2816
																																				=
																																				(BgL_idz00_2799
																																				==
																																				CNST_TABLE_REF
																																				(60));
																																			if (BgL__ortest_1174z00_2816)
																																				{	/* SawJvm/compile.scm 213 */
																																					BgL_test2044z00_4097
																																						=
																																						BgL__ortest_1174z00_2816;
																																				}
																																			else
																																				{	/* SawJvm/compile.scm 213 */
																																					bool_t
																																						BgL__ortest_1175z00_2817;
																																					BgL__ortest_1175z00_2817
																																						=
																																						(BgL_idz00_2799
																																						==
																																						CNST_TABLE_REF
																																						(47));
																																					if (BgL__ortest_1175z00_2817)
																																						{	/* SawJvm/compile.scm 213 */
																																							BgL_test2044z00_4097
																																								=
																																								BgL__ortest_1175z00_2817;
																																						}
																																					else
																																						{	/* SawJvm/compile.scm 213 */
																																							bool_t
																																								BgL__ortest_1176z00_2818;
																																							BgL__ortest_1176z00_2818
																																								=
																																								(BgL_idz00_2799
																																								==
																																								CNST_TABLE_REF
																																								(61));
																																							if (BgL__ortest_1176z00_2818)
																																								{	/* SawJvm/compile.scm 213 */
																																									BgL_test2044z00_4097
																																										=
																																										BgL__ortest_1176z00_2818;
																																								}
																																							else
																																								{	/* SawJvm/compile.scm 213 */
																																									bool_t
																																										BgL__ortest_1177z00_2819;
																																									BgL__ortest_1177z00_2819
																																										=
																																										(BgL_idz00_2799
																																										==
																																										CNST_TABLE_REF
																																										(43));
																																									if (BgL__ortest_1177z00_2819)
																																										{	/* SawJvm/compile.scm 213 */
																																											BgL_test2044z00_4097
																																												=
																																												BgL__ortest_1177z00_2819;
																																										}
																																									else
																																										{	/* SawJvm/compile.scm 213 */
																																											bool_t
																																												BgL__ortest_1178z00_2820;
																																											BgL__ortest_1178z00_2820
																																												=
																																												(BgL_idz00_2799
																																												==
																																												CNST_TABLE_REF
																																												(39));
																																											if (BgL__ortest_1178z00_2820)
																																												{	/* SawJvm/compile.scm 213 */
																																													BgL_test2044z00_4097
																																														=
																																														BgL__ortest_1178z00_2820;
																																												}
																																											else
																																												{	/* SawJvm/compile.scm 213 */
																																													bool_t
																																														BgL__ortest_1179z00_2821;
																																													BgL__ortest_1179z00_2821
																																														=
																																														(BgL_idz00_2799
																																														==
																																														CNST_TABLE_REF
																																														(30));
																																													if (BgL__ortest_1179z00_2821)
																																														{	/* SawJvm/compile.scm 213 */
																																															BgL_test2044z00_4097
																																																=
																																																BgL__ortest_1179z00_2821;
																																														}
																																													else
																																														{	/* SawJvm/compile.scm 213 */
																																															bool_t
																																																BgL__ortest_1180z00_2822;
																																															BgL__ortest_1180z00_2822
																																																=
																																																(BgL_idz00_2799
																																																==
																																																CNST_TABLE_REF
																																																(62));
																																															if (BgL__ortest_1180z00_2822)
																																																{	/* SawJvm/compile.scm 213 */
																																																	BgL_test2044z00_4097
																																																		=
																																																		BgL__ortest_1180z00_2822;
																																																}
																																															else
																																																{	/* SawJvm/compile.scm 213 */
																																																	BgL_test2044z00_4097
																																																		=
																																																		(BgL_idz00_2799
																																																		==
																																																		CNST_TABLE_REF
																																																		(27));
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																				}
																																		}
																																		if (BgL_test2044z00_4097)
																																			{	/* SawJvm/compile.scm 215 */
																																				obj_t
																																					BgL_arg1752z00_2810;
																																				{	/* SawJvm/compile.scm 215 */
																																					obj_t
																																						BgL_arg1753z00_2811;
																																					BgL_arg1753z00_2811
																																						=
																																						MAKE_YOUNG_PAIR
																																						((((BgL_typez00_bglt) COBJECT(BgL_typez00_2798))->BgL_namez00), BNIL);
																																					BgL_arg1752z00_2810
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(63),
																																						BgL_arg1753z00_2811);
																																				}
																																				BGl_codez12z12zzsaw_jvm_outz00
																																					(BgL_mez00_21,
																																					BgL_arg1752z00_2810);
																																			}
																																		else
																																			{	/* SawJvm/compile.scm 217 */
																																				obj_t
																																					BgL_arg1755z00_2813;
																																				{	/* SawJvm/compile.scm 217 */
																																					obj_t
																																						BgL_arg1761z00_2814;
																																					BgL_arg1761z00_2814
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BGl_compilezd2typezd2zzsaw_jvm_outz00
																																						(BgL_mez00_21,
																																							BgL_typez00_2798),
																																						BNIL);
																																					BgL_arg1755z00_2813
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(64),
																																						BgL_arg1761z00_2814);
																																				}
																																				BGl_codez12z12zzsaw_jvm_outz00
																																					(BgL_mez00_21,
																																					BgL_arg1755z00_2813);
																																			}
																																	}
																																	{
																																		long
																																			BgL_iz00_2824;
																																		{	/* SawJvm/compile.scm 218 */
																																			bool_t
																																				BgL_tmpz00_4131;
																																			BgL_iz00_2824
																																				= 0L;
																																		BgL_zc3z04anonymousza31763ze3z87_2825:
																																			if (
																																				(BgL_iz00_2824
																																					==
																																					VECTOR_LENGTH
																																					(((obj_t) BgL_vecz00_2797))))
																																				{	/* SawJvm/compile.scm 219 */
																																					BgL_tmpz00_4131
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																			else
																																				{	/* SawJvm/compile.scm 219 */
																																					BGl_codez12z12zzsaw_jvm_outz00
																																						(BgL_mez00_21,
																																						CNST_TABLE_REF
																																						(51));
																																					BGl_pushzd2intzd2zzsaw_jvm_outz00
																																						(BgL_mez00_21,
																																						BINT
																																						(BgL_iz00_2824));
																																					{	/* SawJvm/compile.scm 223 */
																																						bool_t
																																							BgL_test2053z00_4140;
																																						{	/* SawJvm/compile.scm 223 */
																																							bool_t
																																								BgL__ortest_1181z00_2838;
																																							BgL__ortest_1181z00_2838
																																								=
																																								(BgL_idz00_2799
																																								==
																																								CNST_TABLE_REF
																																								(60));
																																							if (BgL__ortest_1181z00_2838)
																																								{	/* SawJvm/compile.scm 223 */
																																									BgL_test2053z00_4140
																																										=
																																										BgL__ortest_1181z00_2838;
																																								}
																																							else
																																								{	/* SawJvm/compile.scm 223 */
																																									bool_t
																																										BgL__ortest_1182z00_2839;
																																									BgL__ortest_1182z00_2839
																																										=
																																										(BgL_idz00_2799
																																										==
																																										CNST_TABLE_REF
																																										(47));
																																									if (BgL__ortest_1182z00_2839)
																																										{	/* SawJvm/compile.scm 223 */
																																											BgL_test2053z00_4140
																																												=
																																												BgL__ortest_1182z00_2839;
																																										}
																																									else
																																										{	/* SawJvm/compile.scm 223 */
																																											bool_t
																																												BgL__ortest_1183z00_2840;
																																											BgL__ortest_1183z00_2840
																																												=
																																												(BgL_idz00_2799
																																												==
																																												CNST_TABLE_REF
																																												(61));
																																											if (BgL__ortest_1183z00_2840)
																																												{	/* SawJvm/compile.scm 223 */
																																													BgL_test2053z00_4140
																																														=
																																														BgL__ortest_1183z00_2840;
																																												}
																																											else
																																												{	/* SawJvm/compile.scm 223 */
																																													bool_t
																																														BgL__ortest_1184z00_2841;
																																													BgL__ortest_1184z00_2841
																																														=
																																														(BgL_idz00_2799
																																														==
																																														CNST_TABLE_REF
																																														(43));
																																													if (BgL__ortest_1184z00_2841)
																																														{	/* SawJvm/compile.scm 223 */
																																															BgL_test2053z00_4140
																																																=
																																																BgL__ortest_1184z00_2841;
																																														}
																																													else
																																														{	/* SawJvm/compile.scm 223 */
																																															bool_t
																																																BgL__ortest_1185z00_2842;
																																															BgL__ortest_1185z00_2842
																																																=
																																																(BgL_idz00_2799
																																																==
																																																CNST_TABLE_REF
																																																(39));
																																															if (BgL__ortest_1185z00_2842)
																																																{	/* SawJvm/compile.scm 223 */
																																																	BgL_test2053z00_4140
																																																		=
																																																		BgL__ortest_1185z00_2842;
																																																}
																																															else
																																																{	/* SawJvm/compile.scm 223 */
																																																	bool_t
																																																		BgL__ortest_1186z00_2843;
																																																	BgL__ortest_1186z00_2843
																																																		=
																																																		(BgL_idz00_2799
																																																		==
																																																		CNST_TABLE_REF
																																																		(30));
																																																	if (BgL__ortest_1186z00_2843)
																																																		{	/* SawJvm/compile.scm 223 */
																																																			BgL_test2053z00_4140
																																																				=
																																																				BgL__ortest_1186z00_2843;
																																																		}
																																																	else
																																																		{	/* SawJvm/compile.scm 223 */
																																																			bool_t
																																																				BgL__ortest_1187z00_2844;
																																																			BgL__ortest_1187z00_2844
																																																				=
																																																				(BgL_idz00_2799
																																																				==
																																																				CNST_TABLE_REF
																																																				(62));
																																																			if (BgL__ortest_1187z00_2844)
																																																				{	/* SawJvm/compile.scm 223 */
																																																					BgL_test2053z00_4140
																																																						=
																																																						BgL__ortest_1187z00_2844;
																																																				}
																																																			else
																																																				{	/* SawJvm/compile.scm 223 */
																																																					BgL_test2053z00_4140
																																																						=
																																																						(BgL_idz00_2799
																																																						==
																																																						CNST_TABLE_REF
																																																						(27));
																																																				}
																																																		}
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																						if (BgL_test2053z00_4140)
																																							{	/* SawJvm/compile.scm 225 */
																																								obj_t
																																									BgL_arg1767z00_2836;
																																								BgL_arg1767z00_2836
																																									=
																																									VECTOR_REF
																																									(
																																									((obj_t) BgL_vecz00_2797), BgL_iz00_2824);
																																								BGl_pushzd2numzd2zzsaw_jvm_outz00
																																									(BgL_mez00_21,
																																									BgL_arg1767z00_2836,
																																									BgL_idz00_2799);
																																							}
																																						else
																																							{	/* SawJvm/compile.scm 223 */
																																								{	/* SawJvm/compile.scm 227 */
																																									obj_t
																																										BgL_arg1770z00_2837;
																																									BgL_arg1770z00_2837
																																										=
																																										VECTOR_REF
																																										(
																																										((obj_t) BgL_vecz00_2797), BgL_iz00_2824);
																																									BGl_pushzd2stringzd2zzsaw_jvm_outz00
																																										(BgL_mez00_21,
																																										BgL_arg1770z00_2837);
																																								}
																																								BGl_codez12z12zzsaw_jvm_outz00
																																									(BgL_mez00_21,
																																									CNST_TABLE_REF
																																									(25));
																																							}
																																					}
																																					{	/* SawJvm/compile.scm 229 */
																																						obj_t
																																							BgL_arg1771z00_2845;
																																						{	/* SawJvm/compile.scm 229 */
																																							bool_t
																																								BgL_test2061z00_4172;
																																							{	/* SawJvm/compile.scm 229 */
																																								bool_t
																																									BgL__ortest_1188z00_2855;
																																								BgL__ortest_1188z00_2855
																																									=
																																									(BgL_idz00_2799
																																									==
																																									CNST_TABLE_REF
																																									(60));
																																								if (BgL__ortest_1188z00_2855)
																																									{	/* SawJvm/compile.scm 229 */
																																										BgL_test2061z00_4172
																																											=
																																											BgL__ortest_1188z00_2855;
																																									}
																																								else
																																									{	/* SawJvm/compile.scm 229 */
																																										BgL_test2061z00_4172
																																											=
																																											(BgL_idz00_2799
																																											==
																																											CNST_TABLE_REF
																																											(47));
																																									}
																																							}
																																							if (BgL_test2061z00_4172)
																																								{	/* SawJvm/compile.scm 229 */
																																									BgL_arg1771z00_2845
																																										=
																																										CNST_TABLE_REF
																																										(65);
																																								}
																																							else
																																								{	/* SawJvm/compile.scm 229 */
																																									if ((BgL_idz00_2799 == CNST_TABLE_REF(61)))
																																										{	/* SawJvm/compile.scm 229 */
																																											BgL_arg1771z00_2845
																																												=
																																												CNST_TABLE_REF
																																												(66);
																																										}
																																									else
																																										{	/* SawJvm/compile.scm 229 */
																																											if ((BgL_idz00_2799 == CNST_TABLE_REF(43)))
																																												{	/* SawJvm/compile.scm 229 */
																																													BgL_arg1771z00_2845
																																														=
																																														CNST_TABLE_REF
																																														(67);
																																												}
																																											else
																																												{	/* SawJvm/compile.scm 229 */
																																													if ((BgL_idz00_2799 == CNST_TABLE_REF(39)))
																																														{	/* SawJvm/compile.scm 229 */
																																															BgL_arg1771z00_2845
																																																=
																																																CNST_TABLE_REF
																																																(68);
																																														}
																																													else
																																														{	/* SawJvm/compile.scm 229 */
																																															if ((BgL_idz00_2799 == CNST_TABLE_REF(30)))
																																																{	/* SawJvm/compile.scm 229 */
																																																	BgL_arg1771z00_2845
																																																		=
																																																		CNST_TABLE_REF
																																																		(69);
																																																}
																																															else
																																																{	/* SawJvm/compile.scm 229 */
																																																	if ((BgL_idz00_2799 == CNST_TABLE_REF(62)))
																																																		{	/* SawJvm/compile.scm 229 */
																																																			BgL_arg1771z00_2845
																																																				=
																																																				CNST_TABLE_REF
																																																				(70);
																																																		}
																																																	else
																																																		{	/* SawJvm/compile.scm 229 */
																																																			if ((BgL_idz00_2799 == CNST_TABLE_REF(27)))
																																																				{	/* SawJvm/compile.scm 229 */
																																																					BgL_arg1771z00_2845
																																																						=
																																																						CNST_TABLE_REF
																																																						(71);
																																																				}
																																																			else
																																																				{	/* SawJvm/compile.scm 229 */
																																																					BgL_arg1771z00_2845
																																																						=
																																																						CNST_TABLE_REF
																																																						(72);
																																																				}
																																																		}
																																																}
																																														}
																																												}
																																										}
																																								}
																																						}
																																						BGl_codez12z12zzsaw_jvm_outz00
																																							(BgL_mez00_21,
																																							BgL_arg1771z00_2845);
																																					}
																																					{
																																						long
																																							BgL_iz00_4205;
																																						BgL_iz00_4205
																																							=
																																							(BgL_iz00_2824
																																							+
																																							1L);
																																						BgL_iz00_2824
																																							=
																																							BgL_iz00_4205;
																																						goto
																																							BgL_zc3z04anonymousza31763ze3z87_2825;
																																					}
																																				}
																																			BBOOL
																																				(BgL_tmpz00_4131);
																																		}
																																	}
																																}
																															}
																														}
																													}
																												else
																													{	/* SawJvm/compile.scm 240 */
																														obj_t
																															BgL_arg1808z00_2861;
																														BgL_arg1808z00_2861
																															=
																															(((BgL_scnstz00_bglt) COBJECT(((BgL_scnstz00_bglt) BgL_valz00_23)))->BgL_classz00);
																														BGl_internalzd2errorzd2zztools_errorz00
																															(BGl_string1992z00zzsaw_jvm_compilez00,
																															BGl_string1993z00zzsaw_jvm_compilez00,
																															BgL_arg1808z00_2861);
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
			{	/* SawJvm/compile.scm 241 */
				obj_t BgL_arg1812z00_2866;

				{	/* SawJvm/compile.scm 241 */
					obj_t BgL_arg1820z00_2867;

					{	/* SawJvm/compile.scm 241 */
						obj_t BgL_arg1822z00_2868;

						BgL_arg1822z00_2868 =
							BGl_declarezd2globalzd2zzsaw_jvm_outz00(BgL_mez00_21,
							((BgL_globalz00_bglt) BgL_varz00_22));
						BgL_arg1820z00_2867 = MAKE_YOUNG_PAIR(BgL_arg1822z00_2868, BNIL);
					}
					BgL_arg1812z00_2866 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1820z00_2867);
				}
				return
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_21, BgL_arg1812z00_2866);
			}
		}

	}



/* module-dlopen */
	obj_t BGl_modulezd2dlopenzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt BgL_mez00_24)
	{
		{	/* SawJvm/compile.scm 246 */
			BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_mez00_24,
				CNST_TABLE_REF(73));
			BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_mez00_24, BNIL, BNIL);
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_24, CNST_TABLE_REF(21));
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_24, CNST_TABLE_REF(22));
			{	/* SawJvm/compile.scm 251 */
				obj_t BgL_arg1823z00_2869;

				{	/* SawJvm/compile.scm 251 */
					obj_t BgL_list1824z00_2870;

					BgL_list1824z00_2870 =
						MAKE_YOUNG_PAIR(BGl_za2moduleza2z00zzmodule_modulez00, BNIL);
					BgL_arg1823z00_2869 =
						BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(23),
						BgL_list1824z00_2870);
				}
				BGl_callzd2globalzd2zzsaw_jvm_outz00(BgL_mez00_24,
					((BgL_globalz00_bglt) BgL_arg1823z00_2869));
			}
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_24, CNST_TABLE_REF(7));
			BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_24, CNST_TABLE_REF(8));
			BGL_TAIL return BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_24);
		}

	}



/* module-functions */
	bool_t BGl_modulezd2functionszd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt
		BgL_mez00_25)
	{
		{	/* SawJvm/compile.scm 259 */
			{	/* SawJvm/compile.scm 260 */
				obj_t BgL_g1585z00_2871;

				BgL_g1585z00_2871 =
					(((BgL_backendz00_bglt) COBJECT(
							((BgL_backendz00_bglt) BgL_mez00_25)))->BgL_functionsz00);
				{
					obj_t BgL_l1583z00_2873;

					BgL_l1583z00_2873 = BgL_g1585z00_2871;
				BgL_zc3z04anonymousza31825ze3z87_2874:
					if (PAIRP(BgL_l1583z00_2873))
						{	/* SawJvm/compile.scm 260 */
							BGl_modulezd2functionzd2zzsaw_jvm_compilez00(BgL_mez00_25,
								CAR(BgL_l1583z00_2873));
							{
								obj_t BgL_l1583z00_4240;

								BgL_l1583z00_4240 = CDR(BgL_l1583z00_2873);
								BgL_l1583z00_2873 = BgL_l1583z00_4240;
								goto BgL_zc3z04anonymousza31825ze3z87_2874;
							}
						}
					else
						{	/* SawJvm/compile.scm 260 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* module-function */
	obj_t BGl_modulezd2functionzd2zzsaw_jvm_compilez00(BgL_jvmz00_bglt
		BgL_mez00_26, obj_t BgL_vz00_27)
	{
		{	/* SawJvm/compile.scm 262 */
			{	/* SawJvm/compile.scm 264 */
				obj_t BgL_codez00_2880;
				obj_t BgL_paramsz00_2881;

				BgL_codez00_2880 =
					BGl_globalzd2ze3blocksz31zzsaw_woodcutterz00(
					((BgL_backendz00_bglt) BgL_mez00_26),
					((BgL_globalz00_bglt) BgL_vz00_27));
				{	/* SawJvm/compile.scm 265 */
					obj_t BgL_l1586z00_2882;

					BgL_l1586z00_2882 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_vz00_27))))->
										BgL_valuez00))))->BgL_argsz00);
					if (NULLP(BgL_l1586z00_2882))
						{	/* SawJvm/compile.scm 265 */
							BgL_paramsz00_2881 = BNIL;
						}
					else
						{	/* SawJvm/compile.scm 265 */
							obj_t BgL_head1588z00_2884;

							{	/* SawJvm/compile.scm 265 */
								BgL_rtl_regz00_bglt BgL_arg1838z00_2896;

								{	/* SawJvm/compile.scm 265 */
									obj_t BgL_arg1839z00_2897;

									BgL_arg1839z00_2897 = CAR(((obj_t) BgL_l1586z00_2882));
									BgL_arg1838z00_2896 =
										BGl_localzd2ze3regz31zzsaw_node2rtlz00(
										((BgL_localz00_bglt) BgL_arg1839z00_2897));
								}
								BgL_head1588z00_2884 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg1838z00_2896), BNIL);
							}
							{	/* SawJvm/compile.scm 265 */
								obj_t BgL_g1591z00_2885;

								BgL_g1591z00_2885 = CDR(((obj_t) BgL_l1586z00_2882));
								{
									obj_t BgL_l1586z00_2887;
									obj_t BgL_tail1589z00_2888;

									BgL_l1586z00_2887 = BgL_g1591z00_2885;
									BgL_tail1589z00_2888 = BgL_head1588z00_2884;
								BgL_zc3z04anonymousza31833ze3z87_2889:
									if (NULLP(BgL_l1586z00_2887))
										{	/* SawJvm/compile.scm 265 */
											BgL_paramsz00_2881 = BgL_head1588z00_2884;
										}
									else
										{	/* SawJvm/compile.scm 265 */
											obj_t BgL_newtail1590z00_2891;

											{	/* SawJvm/compile.scm 265 */
												BgL_rtl_regz00_bglt BgL_arg1836z00_2893;

												{	/* SawJvm/compile.scm 265 */
													obj_t BgL_arg1837z00_2894;

													BgL_arg1837z00_2894 =
														CAR(((obj_t) BgL_l1586z00_2887));
													BgL_arg1836z00_2893 =
														BGl_localzd2ze3regz31zzsaw_node2rtlz00(
														((BgL_localz00_bglt) BgL_arg1837z00_2894));
												}
												BgL_newtail1590z00_2891 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1836z00_2893), BNIL);
											}
											SET_CDR(BgL_tail1589z00_2888, BgL_newtail1590z00_2891);
											{	/* SawJvm/compile.scm 265 */
												obj_t BgL_arg1835z00_2892;

												BgL_arg1835z00_2892 = CDR(((obj_t) BgL_l1586z00_2887));
												{
													obj_t BgL_tail1589z00_4272;
													obj_t BgL_l1586z00_4271;

													BgL_l1586z00_4271 = BgL_arg1835z00_2892;
													BgL_tail1589z00_4272 = BgL_newtail1590z00_2891;
													BgL_tail1589z00_2888 = BgL_tail1589z00_4272;
													BgL_l1586z00_2887 = BgL_l1586z00_4271;
													goto BgL_zc3z04anonymousza31833ze3z87_2889;
												}
											}
										}
								}
							}
						}
				}
				BGl_openzd2globalzd2methodz00zzsaw_jvm_outz00(BgL_mez00_26,
					((BgL_globalz00_bglt) BgL_vz00_27));
				BGl_buildzd2treezd2zzsaw_exprz00(
					((BgL_backendz00_bglt) BgL_mez00_26), BgL_paramsz00_2881,
					BgL_codez00_2880);
				BgL_codez00_2880 =
					BGl_registerzd2allocationzd2zzsaw_registerzd2allocationzd2((
						(BgL_backendz00_bglt) BgL_mez00_26),
					((BgL_globalz00_bglt) BgL_vz00_27), BgL_paramsz00_2881,
					BgL_codez00_2880);
				BgL_codez00_2880 =
					BGl_bbvz00zzsaw_bbvz00(((BgL_backendz00_bglt) BgL_mez00_26),
					((BgL_globalz00_bglt) BgL_vz00_27), BgL_paramsz00_2881,
					BgL_codez00_2880);
				BGl_modulezd2codezd2zzsaw_jvm_codez00(BgL_mez00_26, BgL_paramsz00_2881,
					BgL_codez00_2880);
				return BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_26);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_jvm_compilez00(void)
	{
		{	/* SawJvm/compile.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_jvm_compilez00(void)
	{
		{	/* SawJvm/compile.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_jvm_compilez00(void)
	{
		{	/* SawJvm/compile.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_compilez00(void)
	{
		{	/* SawJvm/compile.scm 1 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzcnst_nodez00(89013043L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzcnst_allocz00(192699986L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzbackend_bvmz00(336068337L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(0L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzbackend_libz00(321177283L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(20930040L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_woodcutterz00(352965511L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_exprz00(142709001L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_registerzd2allocationzd2(250697396L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvz00(263563890L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_namesz00(179871433L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_outz00(441641707L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_funcallz00(282450876L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_jvm_codez00(44765844L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_jvm_compilez00));
		}

	}

#ifdef __cplusplus
}
#endif
