/*===========================================================================*/
/*   (SawJvm/funcall.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawJvm/funcall.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_JVM_FUNCALL_TYPE_DEFINITIONS
#define BGL_SAW_JVM_FUNCALL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_jvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
		obj_t BgL_qnamez00;
		obj_t BgL_classesz00;
		obj_t BgL_currentzd2classzd2;
		obj_t BgL_declarationsz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_codez00;
		obj_t BgL_lightzd2funcallszd2;
		obj_t BgL_inlinez00;
	}             *BgL_jvmz00_bglt;

	typedef struct BgL_rtl_lightfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_funsz00;
		obj_t BgL_rettypez00;
	}                          *BgL_rtl_lightfuncallz00_bglt;

	typedef struct BgL_indexedz00_bgl
	{
		int BgL_indexz00;
	}                 *BgL_indexedz00_bglt;


#endif													// BGL_SAW_JVM_FUNCALL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62indexedzd2evalzf3z43zzsaw_jvm_funcallz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2removablezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_z62indexedzd2initzb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2importzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_indexedzd2nilzd2zzsaw_jvm_funcallz00(void);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_jvm_funcallz00 = BUNSPEC;
	static obj_t BGl_compilezd2dispatchzd2zzsaw_jvm_funcallz00(BgL_jvmz00_bglt,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2fastzd2alphaz00zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_getzd2labsze70z35zzsaw_jvm_funcallz00(long, obj_t, obj_t);
	static obj_t BGl_getzd2labsze71z35zzsaw_jvm_funcallz00(long, obj_t, obj_t);
	static obj_t BGl_getzd2labsze72z35zzsaw_jvm_funcallz00(long, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_pushzd2stringzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	extern obj_t BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2aliaszd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	extern obj_t BGl_sfunz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2pragmazd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_indexedzd2indexzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32111ze3ze5zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	static obj_t BGl_xx_globalzd2arityze70z35zzsaw_jvm_funcallz00(BgL_jvmz00_bglt,
		obj_t);
	static obj_t BGl_z62indexedzd2evaluablezf3z43zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	static obj_t BGl_z62indexedzd2indexzd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	static bool_t BGl_keyzd2optzf3z21zzsaw_jvm_funcallz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2evalzf3zd2setz12ze1zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		bool_t);
	static BgL_valuez00_bglt BGl_z62indexedzd2valuezb0zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	static obj_t BGl_z62indexedzd2removablezb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	static obj_t BGl_z62indexedzd2userzf3z43zzsaw_jvm_funcallz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_jvm_funcallz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_compilezd2forzd2applyz00zzsaw_jvm_funcallz00(obj_t, obj_t,
		obj_t);
	static BgL_globalz00_bglt BGl_z62makezd2indexedzb0zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_jvm_funcallz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2accesszd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2namezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t
		BGl_z62indexedzd2occurrencewzd2setz12z70zzsaw_jvm_funcallz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62zc3z04anonymousza32050ze3ze5zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_compilezd2typezd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_compilezd2applyzd2zzsaw_jvm_funcallz00(BgL_jvmz00_bglt,
		obj_t);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	static BgL_globalz00_bglt BGl_z62indexedzd2nilzb0zzsaw_jvm_funcallz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t
		BGl_z62indexedzd2evaluablezf3zd2setz12z83zzsaw_jvm_funcallz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62modulezd2funcallzf2applyz42zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_jvm_funcallz00(void);
	static obj_t BGl_z62indexedzd2valuezd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62indexedzd2removablezd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_funcalliz00zzsaw_jvm_funcallz00(BgL_jvmz00_bglt, long,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2namezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_z62indexedzd2modulezd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_indexedzd2evalzf3z21zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2funcallzf2applyz20zzsaw_jvm_funcallz00(BgL_jvmz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2aliaszd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_z62indexedzd2occurrencezb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	extern obj_t BGl_labelz00zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	BGL_IMPORT obj_t bgl_bignum_sub(obj_t, obj_t);
	static bool_t
		BGl_iszd2lightzd2procedurezf3zf3zzsaw_jvm_funcallz00(BgL_jvmz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2initzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_forzd2eachzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t BGl_funcallzd2lightzd2zzsaw_jvm_funcallz00(BgL_jvmz00_bglt,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t
		BGl_z62indexedzd2fastzd2alphazd2setz12za2zzsaw_jvm_funcallz00(obj_t, obj_t,
		obj_t);
	extern BgL_globalz00_bglt
		BGl_globalzd2entryzd2zzbackend_cplibz00(BgL_globalz00_bglt);
	static obj_t
		BGl_z62indexedzd2occurrencezd2setz12z70zzsaw_jvm_funcallz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2jvmzd2typezd2namezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	extern obj_t BGl_callzd2globalzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2idzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62indexedzd2pragmazb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	static obj_t BGl_z62indexedzd2accesszb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	static obj_t BGl_z62indexedzf3z91zzsaw_jvm_funcallz00(obj_t, obj_t);
	static obj_t BGl_z62indexedzd2userzf3zd2setz12z83zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_globalz00_bglt BGl_z62lambda2106z62zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_neededzf3ze70z14zzsaw_jvm_funcallz00(long, BgL_jvmz00_bglt,
		obj_t);
	static BgL_globalz00_bglt BGl_z62lambda2109z62zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	static obj_t BGl_z62indexedzd2idzb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2occurrencewzd2setz12z12zzsaw_jvm_funcallz00
		(BgL_globalz00_bglt, long);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_indexedzd2valuezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_indexedzd2userzf3z21zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static BgL_globalz00_bglt BGl_z62lambda2112z62zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	extern obj_t BGl_declarezd2globalzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	static obj_t BGl_nameze70ze7zzsaw_jvm_funcallz00(long);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_indexedzd2typezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_z62lambda2119z62zzsaw_jvm_funcallz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_indexedz00zzsaw_jvm_funcallz00 = BUNSPEC;
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2typezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62lambda2120z62zzsaw_jvm_funcallz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62indexedzd2occurrencewzb0zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_funcallz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_outz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_proceduresz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62indexedzd2jvmzd2typezd2namezb0zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_indexedzd2evaluablezf3z21zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2indexzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		int);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2initzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2modulezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_z62indexedzd2namezd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2srczd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_compilezd2funizd2zzsaw_jvm_funcallz00(BgL_jvmz00_bglt, long,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_z62indexedzd2fastzd2alphaz62zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2srczd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_Lze70ze7zzsaw_jvm_funcallz00(long);
	extern obj_t BGl_codez12z12zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2pragmazd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzsaw_jvm_funcallz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_funcallz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2accesszd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	extern obj_t BGl_globalzd2arityzd2zzbackend_cplibz00(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_funcallz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_funcallz00(void);
	static obj_t BGl_z62indexedzd2importzd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31813ze3ze5zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2jvmzd2typezd2namezd2setz12z12zzsaw_jvm_funcallz00
		(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62indexedzd2evalzf3zd2setz12z83zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62indexedzd2aliaszb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	static obj_t BGl_z62indexedzd2indexzb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_makezd2indexedzd2zzsaw_jvm_funcallz00(obj_t, obj_t, BgL_typez00_bglt,
		BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long, bool_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int);
	static obj_t BGl_z62indexedzd2modulezb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2valuezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		BgL_valuez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2removablezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_z62indexedzd2namezb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	static obj_t BGl_z62indexedzd2srczd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62indexedzd2jvmzd2typezd2namezd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2evaluablezf3zd2setz12ze1zzsaw_jvm_funcallz00
		(BgL_globalz00_bglt, bool_t);
	static obj_t BGl_getzd2typeze70z35zzsaw_jvm_funcallz00(obj_t);
	extern obj_t BGl_valuez00zzast_varz00;
	BGL_EXPORTED_DECL bool_t BGl_indexedzf3zf3zzsaw_jvm_funcallz00(obj_t);
	static obj_t BGl_z62indexedzd2initzd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_indexedzd2occurrencewzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL long
		BGl_indexedzd2occurrencezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_z62indexedzd2libraryzd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		obj_t);
	static obj_t BGl_z62indexedzd2importzb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	extern obj_t BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62indexedzd2pragmazd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	static bool_t
		BGl_funcallzd2lightzd2switchz00zzsaw_jvm_funcallz00(BgL_jvmz00_bglt, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_getzd2procedureszd2zzsaw_proceduresz00(obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62indexedzd2srczb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2libraryzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	extern obj_t BGl_declarezd2methodzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62modulezd2lightzd2funcallz62zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2libraryzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_z62indexedzd2libraryzb0zzsaw_jvm_funcallz00(obj_t, obj_t);
	static obj_t BGl_compilezd2forzd2funcalliz00zzsaw_jvm_funcallz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2userzf3zd2setz12ze1zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2fastzd2alphazd2setz12zc0zzsaw_jvm_funcallz00
		(BgL_globalz00_bglt, obj_t);
	BGL_IMPORT bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62indexedzd2typezd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2occurrencezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2modulezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt);
	static obj_t BGl_z62indexedzd2aliaszd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2lightzd2funcallz00zzsaw_jvm_funcallz00(BgL_jvmz00_bglt);
	static obj_t BGl_z62indexedzd2accesszd2setz12z70zzsaw_jvm_funcallz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_indexedzd2importzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62indexedzd2typezb0zzsaw_jvm_funcallz00(obj_t,
		obj_t);
	static obj_t __cnst[55];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2userzf3zd2envzf3zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2use2262z00,
		BGl_z62indexedzd2userzf3z43zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2evalzf3zd2setz12zd2envz33zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2eva2263z00,
		BGl_z62indexedzd2evalzf3zd2setz12z83zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2fastzd2alphazd2setz12zd2envz12zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2fas2264z00,
		BGl_z62indexedzd2fastzd2alphazd2setz12za2zzsaw_jvm_funcallz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2occurrencezd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2occ2265z00,
		BGl_z62indexedzd2occurrencezd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2aliaszd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2ali2266z00,
		BGl_z62indexedzd2aliaszb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2pragmazd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2pra2267z00,
		BGl_z62indexedzd2pragmazd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2importzd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2imp2268z00,
		BGl_z62indexedzd2importzb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2removablezd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2rem2269z00,
		BGl_z62indexedzd2removablezb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2245z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2245za700za7za7s2270za7, "unknown type", 12);
	      DEFINE_STRING(BGl_string2246z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2246za700za7za7s2271za7, "funcall light", 13);
	      DEFINE_STRING(BGl_string2247z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2247za700za7za7s2272za7, "internal error", 14);
	      DEFINE_STRING(BGl_string2248z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2248za700za7za7s2273za7, "L", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_indexedzd2nilzd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2nil2274z00,
		BGl_z62indexedzd2nilzb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2249z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2249za700za7za7s2275za7, "funcall", 7);
	      DEFINE_STRING(BGl_string2250z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2250za700za7za7s2276za7, "p", 1);
	      DEFINE_STRING(BGl_string2251z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2251za700za7za7s2277za7, "make_vector", 11);
	      DEFINE_STRING(BGl_string2258z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2258za700za7za7s2278za7, "", 0);
	      DEFINE_STRING(BGl_string2259z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2259za700za7za7s2279za7, "saw_jvm_funcall", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2252z00zzsaw_jvm_funcallz00,
		BgL_bgl_za762lambda2120za7622280z00,
		BGl_z62lambda2120z62zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2253z00zzsaw_jvm_funcallz00,
		BgL_bgl_za762lambda2119za7622281z00,
		BGl_z62lambda2119z62zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2260z00zzsaw_jvm_funcallz00,
		BgL_bgl_string2260za700za7za7s2282za7,
		"_ saw_jvm_funcall indexed index (invokestatic list_to_vector) (astore l) (dup) (getfield cdr) (getfield car) (checkcast pair) (invokespecial papply) (aload l) (this l) apply #z1 (getstatic *nil*) invokestatic putstatic getstatic (pop) (invokestatic cons) invokespecial #z-1 (aload this) this light checkcast dload fload lload iload (athrow) (invokestatic fail) tableswitch err (areturn) (dreturn) double (freturn) float (lreturn) long (ireturn) int short char byte boolean (return) void compile-bad-type (getfield procindex) aload (private static) me ",
		551);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2254z00zzsaw_jvm_funcallz00,
		BgL_bgl_za762lambda2112za7622283z00,
		BGl_z62lambda2112z62zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2255z00zzsaw_jvm_funcallz00,
		BgL_bgl_za762za7c3za704anonymo2284za7,
		BGl_z62zc3z04anonymousza32111ze3ze5zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2256z00zzsaw_jvm_funcallz00,
		BgL_bgl_za762lambda2109za7622285z00,
		BGl_z62lambda2109z62zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2257z00zzsaw_jvm_funcallz00,
		BgL_bgl_za762lambda2106za7622286z00,
		BGl_z62lambda2106z62zzsaw_jvm_funcallz00, 0L, BUNSPEC, 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_indexedzd2namezd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2nam2287z00,
		BGl_z62indexedzd2namezb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2indexedzd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762makeza7d2indexe2288z00,
		BGl_z62makezd2indexedzb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2libraryzd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2lib2289z00,
		BGl_z62indexedzd2libraryzb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2evaluablezf3zd2envzf3zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2eva2290z00,
		BGl_z62indexedzd2evaluablezf3z43zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2importzd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2imp2291z00,
		BGl_z62indexedzd2importzd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2namezd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2nam2292z00,
		BGl_z62indexedzd2namezd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2modulezd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2mod2293z00,
		BGl_z62indexedzd2modulezd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_indexedzd2srczd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2src2294z00,
		BGl_z62indexedzd2srczb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2valuezd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2val2295z00,
		BGl_z62indexedzd2valuezd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2pragmazd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2pra2296z00,
		BGl_z62indexedzd2pragmazb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2modulezd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2mod2297z00,
		BGl_z62indexedzd2modulezb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2jvmzd2typezd2namezd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2jvm2298z00,
		BGl_z62indexedzd2jvmzd2typezd2namezb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2accesszd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2acc2299z00,
		BGl_z62indexedzd2accesszd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2libraryzd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2lib2300z00,
		BGl_z62indexedzd2libraryzd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2occurrencewzd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2occ2301z00,
		BGl_z62indexedzd2occurrencewzd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_indexedzd2typezd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2typ2302z00,
		BGl_z62indexedzd2typezb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2occurrencewzd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2occ2303z00,
		BGl_z62indexedzd2occurrencewzb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2lightzd2funcallzd2envzd2zzsaw_jvm_funcallz00,
		BgL_bgl_za762moduleza7d2ligh2304z00,
		BGl_z62modulezd2lightzd2funcallz62zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2indexzd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2ind2305z00,
		BGl_z62indexedzd2indexzd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2srczd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2src2306z00,
		BGl_z62indexedzd2srczd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2aliaszd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2ali2307z00,
		BGl_z62indexedzd2aliaszd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2removablezd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2rem2308z00,
		BGl_z62indexedzd2removablezd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2accesszd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2acc2309z00,
		BGl_z62indexedzd2accesszb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2userzf3zd2setz12zd2envz33zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2use2310z00,
		BGl_z62indexedzd2userzf3zd2setz12z83zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2occurrencezd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2occ2311z00,
		BGl_z62indexedzd2occurrencezb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2evaluablezf3zd2setz12zd2envz33zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2eva2312z00,
		BGl_z62indexedzd2evaluablezf3zd2setz12z83zzsaw_jvm_funcallz00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_indexedzf3zd2envz21zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7f3za7912313za7,
		BGl_z62indexedzf3z91zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2typezd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2typ2314z00,
		BGl_z62indexedzd2typezd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_indexedzd2idzd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2idza72315za7,
		BGl_z62indexedzd2idzb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_indexedzd2initzd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2ini2316z00,
		BGl_z62indexedzd2initzb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2indexzd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2ind2317z00,
		BGl_z62indexedzd2indexzb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2valuezd2envz00zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2val2318z00,
		BGl_z62indexedzd2valuezb0zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2initzd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2ini2319z00,
		BGl_z62indexedzd2initzd2setz12z70zzsaw_jvm_funcallz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2fastzd2alphazd2envzd2zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2fas2320z00,
		BGl_z62indexedzd2fastzd2alphaz62zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_zc3zd2envz11zz__r4_numbers_6_5z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2jvmzd2typezd2namezd2setz12zd2envzc0zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2jvm2321z00,
		BGl_z62indexedzd2jvmzd2typezd2namezd2setz12z70zzsaw_jvm_funcallz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_indexedzd2evalzf3zd2envzf3zzsaw_jvm_funcallz00,
		BgL_bgl_za762indexedza7d2eva2322z00,
		BGl_z62indexedzd2evalzf3z43zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2funcallzf2applyzd2envzf2zzsaw_jvm_funcallz00,
		BgL_bgl_za762moduleza7d2func2323z00,
		BGl_z62modulezd2funcallzf2applyz42zzsaw_jvm_funcallz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_jvm_funcallz00));
		     ADD_ROOT((void *) (&BGl_indexedz00zzsaw_jvm_funcallz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_funcallz00(long
		BgL_checksumz00_4566, char *BgL_fromz00_4567)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_jvm_funcallz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_jvm_funcallz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_jvm_funcallz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_jvm_funcallz00();
					BGl_cnstzd2initzd2zzsaw_jvm_funcallz00();
					BGl_importedzd2moduleszd2initz00zzsaw_jvm_funcallz00();
					BGl_objectzd2initzd2zzsaw_jvm_funcallz00();
					return BGl_methodzd2initzd2zzsaw_jvm_funcallz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_funcallz00(void)
	{
		{	/* SawJvm/funcall.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"saw_jvm_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_jvm_funcall");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_jvm_funcallz00(void)
	{
		{	/* SawJvm/funcall.scm 1 */
			{	/* SawJvm/funcall.scm 1 */
				obj_t BgL_cportz00_4470;

				{	/* SawJvm/funcall.scm 1 */
					obj_t BgL_stringz00_4477;

					BgL_stringz00_4477 = BGl_string2260z00zzsaw_jvm_funcallz00;
					{	/* SawJvm/funcall.scm 1 */
						obj_t BgL_startz00_4478;

						BgL_startz00_4478 = BINT(0L);
						{	/* SawJvm/funcall.scm 1 */
							obj_t BgL_endz00_4479;

							BgL_endz00_4479 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4477)));
							{	/* SawJvm/funcall.scm 1 */

								BgL_cportz00_4470 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4477, BgL_startz00_4478, BgL_endz00_4479);
				}}}}
				{
					long BgL_iz00_4471;

					BgL_iz00_4471 = 54L;
				BgL_loopz00_4472:
					if ((BgL_iz00_4471 == -1L))
						{	/* SawJvm/funcall.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawJvm/funcall.scm 1 */
							{	/* SawJvm/funcall.scm 1 */
								obj_t BgL_arg2261z00_4473;

								{	/* SawJvm/funcall.scm 1 */

									{	/* SawJvm/funcall.scm 1 */
										obj_t BgL_locationz00_4475;

										BgL_locationz00_4475 = BBOOL(((bool_t) 0));
										{	/* SawJvm/funcall.scm 1 */

											BgL_arg2261z00_4473 =
												BGl_readz00zz__readerz00(BgL_cportz00_4470,
												BgL_locationz00_4475);
										}
									}
								}
								{	/* SawJvm/funcall.scm 1 */
									int BgL_tmpz00_4598;

									BgL_tmpz00_4598 = (int) (BgL_iz00_4471);
									CNST_TABLE_SET(BgL_tmpz00_4598, BgL_arg2261z00_4473);
							}}
							{	/* SawJvm/funcall.scm 1 */
								int BgL_auxz00_4476;

								BgL_auxz00_4476 = (int) ((BgL_iz00_4471 - 1L));
								{
									long BgL_iz00_4603;

									BgL_iz00_4603 = (long) (BgL_auxz00_4476);
									BgL_iz00_4471 = BgL_iz00_4603;
									goto BgL_loopz00_4472;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_funcallz00(void)
	{
		{	/* SawJvm/funcall.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* make-indexed */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_makezd2indexedzd2zzsaw_jvm_funcallz00(obj_t BgL_id1175z00_3,
		obj_t BgL_name1176z00_4, BgL_typez00_bglt BgL_type1177z00_5,
		BgL_valuez00_bglt BgL_value1178z00_6, obj_t BgL_access1179z00_7,
		obj_t BgL_fastzd2alpha1180zd2_8, obj_t BgL_removable1181z00_9,
		long BgL_occurrence1182z00_10, long BgL_occurrencew1183z00_11,
		bool_t BgL_userzf31184zf3_12, obj_t BgL_module1185z00_13,
		obj_t BgL_import1186z00_14, bool_t BgL_evaluablezf31187zf3_15,
		bool_t BgL_evalzf31188zf3_16, obj_t BgL_library1189z00_17,
		obj_t BgL_pragma1190z00_18, obj_t BgL_src1191z00_19,
		obj_t BgL_jvmzd2typezd2name1192z00_20, obj_t BgL_init1193z00_21,
		obj_t BgL_alias1194z00_22, int BgL_index1195z00_23)
	{
		{	/* SawJvm/funcall.sch 62 */
			{	/* SawJvm/funcall.sch 62 */
				BgL_globalz00_bglt BgL_new1185z00_4481;

				{	/* SawJvm/funcall.sch 62 */
					BgL_globalz00_bglt BgL_tmp1183z00_4482;
					BgL_indexedz00_bglt BgL_wide1184z00_4483;

					{
						BgL_globalz00_bglt BgL_auxz00_4606;

						{	/* SawJvm/funcall.sch 62 */
							BgL_globalz00_bglt BgL_new1182z00_4484;

							BgL_new1182z00_4484 =
								((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_globalz00_bgl))));
							{	/* SawJvm/funcall.sch 62 */
								long BgL_arg1678z00_4485;

								BgL_arg1678z00_4485 = BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1182z00_4484),
									BgL_arg1678z00_4485);
							}
							{	/* SawJvm/funcall.sch 62 */
								BgL_objectz00_bglt BgL_tmpz00_4611;

								BgL_tmpz00_4611 = ((BgL_objectz00_bglt) BgL_new1182z00_4484);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4611, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1182z00_4484);
							BgL_auxz00_4606 = BgL_new1182z00_4484;
						}
						BgL_tmp1183z00_4482 = ((BgL_globalz00_bglt) BgL_auxz00_4606);
					}
					BgL_wide1184z00_4483 =
						((BgL_indexedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_indexedz00_bgl))));
					{	/* SawJvm/funcall.sch 62 */
						obj_t BgL_auxz00_4619;
						BgL_objectz00_bglt BgL_tmpz00_4617;

						BgL_auxz00_4619 = ((obj_t) BgL_wide1184z00_4483);
						BgL_tmpz00_4617 = ((BgL_objectz00_bglt) BgL_tmp1183z00_4482);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4617, BgL_auxz00_4619);
					}
					((BgL_objectz00_bglt) BgL_tmp1183z00_4482);
					{	/* SawJvm/funcall.sch 62 */
						long BgL_arg1675z00_4486;

						BgL_arg1675z00_4486 =
							BGL_CLASS_NUM(BGl_indexedz00zzsaw_jvm_funcallz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1183z00_4482), BgL_arg1675z00_4486);
					}
					BgL_new1185z00_4481 = ((BgL_globalz00_bglt) BgL_tmp1183z00_4482);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1185z00_4481)))->BgL_idz00) =
					((obj_t) BgL_id1175z00_3), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_namez00) =
					((obj_t) BgL_name1176z00_4), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1177z00_5), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1178z00_6), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_accessz00) =
					((obj_t) BgL_access1179z00_7), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1180zd2_8), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_removablez00) =
					((obj_t) BgL_removable1181z00_9), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_occurrencez00) =
					((long) BgL_occurrence1182z00_10), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1183z00_11), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1185z00_4481)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31184zf3_12), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_modulez00) =
					((obj_t) BgL_module1185z00_13), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_importz00) =
					((obj_t) BgL_import1186z00_14), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_evaluablezf3zf3) =
					((bool_t) BgL_evaluablezf31187zf3_15), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_evalzf3zf3) =
					((bool_t) BgL_evalzf31188zf3_16), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_libraryz00) =
					((obj_t) BgL_library1189z00_17), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_pragmaz00) =
					((obj_t) BgL_pragma1190z00_18), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_srcz00) =
					((obj_t) BgL_src1191z00_19), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BgL_jvmzd2typezd2name1192z00_20), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_initz00) =
					((obj_t) BgL_init1193z00_21), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1185z00_4481)))->BgL_aliasz00) =
					((obj_t) BgL_alias1194z00_22), BUNSPEC);
				{
					BgL_indexedz00_bglt BgL_auxz00_4667;

					{
						obj_t BgL_auxz00_4668;

						{	/* SawJvm/funcall.sch 62 */
							BgL_objectz00_bglt BgL_tmpz00_4669;

							BgL_tmpz00_4669 = ((BgL_objectz00_bglt) BgL_new1185z00_4481);
							BgL_auxz00_4668 = BGL_OBJECT_WIDENING(BgL_tmpz00_4669);
						}
						BgL_auxz00_4667 = ((BgL_indexedz00_bglt) BgL_auxz00_4668);
					}
					((((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_4667))->BgL_indexz00) =
						((int) BgL_index1195z00_23), BUNSPEC);
				}
				return BgL_new1185z00_4481;
			}
		}

	}



/* &make-indexed */
	BgL_globalz00_bglt BGl_z62makezd2indexedzb0zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4273, obj_t BgL_id1175z00_4274, obj_t BgL_name1176z00_4275,
		obj_t BgL_type1177z00_4276, obj_t BgL_value1178z00_4277,
		obj_t BgL_access1179z00_4278, obj_t BgL_fastzd2alpha1180zd2_4279,
		obj_t BgL_removable1181z00_4280, obj_t BgL_occurrence1182z00_4281,
		obj_t BgL_occurrencew1183z00_4282, obj_t BgL_userzf31184zf3_4283,
		obj_t BgL_module1185z00_4284, obj_t BgL_import1186z00_4285,
		obj_t BgL_evaluablezf31187zf3_4286, obj_t BgL_evalzf31188zf3_4287,
		obj_t BgL_library1189z00_4288, obj_t BgL_pragma1190z00_4289,
		obj_t BgL_src1191z00_4290, obj_t BgL_jvmzd2typezd2name1192z00_4291,
		obj_t BgL_init1193z00_4292, obj_t BgL_alias1194z00_4293,
		obj_t BgL_index1195z00_4294)
	{
		{	/* SawJvm/funcall.sch 62 */
			return
				BGl_makezd2indexedzd2zzsaw_jvm_funcallz00(BgL_id1175z00_4274,
				BgL_name1176z00_4275, ((BgL_typez00_bglt) BgL_type1177z00_4276),
				((BgL_valuez00_bglt) BgL_value1178z00_4277), BgL_access1179z00_4278,
				BgL_fastzd2alpha1180zd2_4279, BgL_removable1181z00_4280,
				(long) CINT(BgL_occurrence1182z00_4281),
				(long) CINT(BgL_occurrencew1183z00_4282),
				CBOOL(BgL_userzf31184zf3_4283), BgL_module1185z00_4284,
				BgL_import1186z00_4285, CBOOL(BgL_evaluablezf31187zf3_4286),
				CBOOL(BgL_evalzf31188zf3_4287), BgL_library1189z00_4288,
				BgL_pragma1190z00_4289, BgL_src1191z00_4290,
				BgL_jvmzd2typezd2name1192z00_4291, BgL_init1193z00_4292,
				BgL_alias1194z00_4293, CINT(BgL_index1195z00_4294));
		}

	}



/* indexed? */
	BGL_EXPORTED_DEF bool_t BGl_indexedzf3zf3zzsaw_jvm_funcallz00(obj_t
		BgL_objz00_24)
	{
		{	/* SawJvm/funcall.sch 63 */
			{	/* SawJvm/funcall.sch 63 */
				obj_t BgL_classz00_4487;

				BgL_classz00_4487 = BGl_indexedz00zzsaw_jvm_funcallz00;
				if (BGL_OBJECTP(BgL_objz00_24))
					{	/* SawJvm/funcall.sch 63 */
						BgL_objectz00_bglt BgL_arg1807z00_4488;

						BgL_arg1807z00_4488 = (BgL_objectz00_bglt) (BgL_objz00_24);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawJvm/funcall.sch 63 */
								long BgL_idxz00_4489;

								BgL_idxz00_4489 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4488);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4489 + 3L)) == BgL_classz00_4487);
							}
						else
							{	/* SawJvm/funcall.sch 63 */
								bool_t BgL_res2233z00_4492;

								{	/* SawJvm/funcall.sch 63 */
									obj_t BgL_oclassz00_4493;

									{	/* SawJvm/funcall.sch 63 */
										obj_t BgL_arg1815z00_4494;
										long BgL_arg1816z00_4495;

										BgL_arg1815z00_4494 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawJvm/funcall.sch 63 */
											long BgL_arg1817z00_4496;

											BgL_arg1817z00_4496 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4488);
											BgL_arg1816z00_4495 = (BgL_arg1817z00_4496 - OBJECT_TYPE);
										}
										BgL_oclassz00_4493 =
											VECTOR_REF(BgL_arg1815z00_4494, BgL_arg1816z00_4495);
									}
									{	/* SawJvm/funcall.sch 63 */
										bool_t BgL__ortest_1115z00_4497;

										BgL__ortest_1115z00_4497 =
											(BgL_classz00_4487 == BgL_oclassz00_4493);
										if (BgL__ortest_1115z00_4497)
											{	/* SawJvm/funcall.sch 63 */
												BgL_res2233z00_4492 = BgL__ortest_1115z00_4497;
											}
										else
											{	/* SawJvm/funcall.sch 63 */
												long BgL_odepthz00_4498;

												{	/* SawJvm/funcall.sch 63 */
													obj_t BgL_arg1804z00_4499;

													BgL_arg1804z00_4499 = (BgL_oclassz00_4493);
													BgL_odepthz00_4498 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4499);
												}
												if ((3L < BgL_odepthz00_4498))
													{	/* SawJvm/funcall.sch 63 */
														obj_t BgL_arg1802z00_4500;

														{	/* SawJvm/funcall.sch 63 */
															obj_t BgL_arg1803z00_4501;

															BgL_arg1803z00_4501 = (BgL_oclassz00_4493);
															BgL_arg1802z00_4500 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4501,
																3L);
														}
														BgL_res2233z00_4492 =
															(BgL_arg1802z00_4500 == BgL_classz00_4487);
													}
												else
													{	/* SawJvm/funcall.sch 63 */
														BgL_res2233z00_4492 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2233z00_4492;
							}
					}
				else
					{	/* SawJvm/funcall.sch 63 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &indexed? */
	obj_t BGl_z62indexedzf3z91zzsaw_jvm_funcallz00(obj_t BgL_envz00_4295,
		obj_t BgL_objz00_4296)
	{
		{	/* SawJvm/funcall.sch 63 */
			return BBOOL(BGl_indexedzf3zf3zzsaw_jvm_funcallz00(BgL_objz00_4296));
		}

	}



/* indexed-nil */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_indexedzd2nilzd2zzsaw_jvm_funcallz00(void)
	{
		{	/* SawJvm/funcall.sch 64 */
			{	/* SawJvm/funcall.sch 64 */
				obj_t BgL_classz00_3731;

				BgL_classz00_3731 = BGl_indexedz00zzsaw_jvm_funcallz00;
				{	/* SawJvm/funcall.sch 64 */
					obj_t BgL__ortest_1117z00_3732;

					BgL__ortest_1117z00_3732 = BGL_CLASS_NIL(BgL_classz00_3731);
					if (CBOOL(BgL__ortest_1117z00_3732))
						{	/* SawJvm/funcall.sch 64 */
							return ((BgL_globalz00_bglt) BgL__ortest_1117z00_3732);
						}
					else
						{	/* SawJvm/funcall.sch 64 */
							return
								((BgL_globalz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3731));
						}
				}
			}
		}

	}



/* &indexed-nil */
	BgL_globalz00_bglt BGl_z62indexedzd2nilzb0zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4297)
	{
		{	/* SawJvm/funcall.sch 64 */
			return BGl_indexedzd2nilzd2zzsaw_jvm_funcallz00();
		}

	}



/* indexed-index */
	BGL_EXPORTED_DEF int
		BGl_indexedzd2indexzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_25)
	{
		{	/* SawJvm/funcall.sch 65 */
			{
				BgL_indexedz00_bglt BgL_auxz00_4714;

				{
					obj_t BgL_auxz00_4715;

					{	/* SawJvm/funcall.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_4716;

						BgL_tmpz00_4716 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_4715 = BGL_OBJECT_WIDENING(BgL_tmpz00_4716);
					}
					BgL_auxz00_4714 = ((BgL_indexedz00_bglt) BgL_auxz00_4715);
				}
				return (((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_4714))->BgL_indexz00);
			}
		}

	}



/* &indexed-index */
	obj_t BGl_z62indexedzd2indexzb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4298,
		obj_t BgL_oz00_4299)
	{
		{	/* SawJvm/funcall.sch 65 */
			return
				BINT(BGl_indexedzd2indexzd2zzsaw_jvm_funcallz00(
					((BgL_globalz00_bglt) BgL_oz00_4299)));
		}

	}



/* indexed-index-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2indexzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_26, int BgL_vz00_27)
	{
		{	/* SawJvm/funcall.sch 66 */
			{
				BgL_indexedz00_bglt BgL_auxz00_4724;

				{
					obj_t BgL_auxz00_4725;

					{	/* SawJvm/funcall.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_4726;

						BgL_tmpz00_4726 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_4725 = BGL_OBJECT_WIDENING(BgL_tmpz00_4726);
					}
					BgL_auxz00_4724 = ((BgL_indexedz00_bglt) BgL_auxz00_4725);
				}
				return
					((((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_4724))->BgL_indexz00) =
					((int) BgL_vz00_27), BUNSPEC);
		}}

	}



/* &indexed-index-set! */
	obj_t BGl_z62indexedzd2indexzd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4300, obj_t BgL_oz00_4301, obj_t BgL_vz00_4302)
	{
		{	/* SawJvm/funcall.sch 66 */
			return
				BGl_indexedzd2indexzd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4301), CINT(BgL_vz00_4302));
		}

	}



/* indexed-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2aliaszd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_28)
	{
		{	/* SawJvm/funcall.sch 67 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_28)))->BgL_aliasz00);
		}

	}



/* &indexed-alias */
	obj_t BGl_z62indexedzd2aliaszb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4303,
		obj_t BgL_oz00_4304)
	{
		{	/* SawJvm/funcall.sch 67 */
			return
				BGl_indexedzd2aliaszd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4304));
		}

	}



/* indexed-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2aliaszd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawJvm/funcall.sch 68 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_29)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_30), BUNSPEC);
		}

	}



/* &indexed-alias-set! */
	obj_t BGl_z62indexedzd2aliaszd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4305, obj_t BgL_oz00_4306, obj_t BgL_vz00_4307)
	{
		{	/* SawJvm/funcall.sch 68 */
			return
				BGl_indexedzd2aliaszd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4306), BgL_vz00_4307);
		}

	}



/* indexed-init */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2initzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_31)
	{
		{	/* SawJvm/funcall.sch 69 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_31)))->BgL_initz00);
		}

	}



/* &indexed-init */
	obj_t BGl_z62indexedzd2initzb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4308,
		obj_t BgL_oz00_4309)
	{
		{	/* SawJvm/funcall.sch 69 */
			return
				BGl_indexedzd2initzd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4309));
		}

	}



/* indexed-init-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2initzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_32, obj_t BgL_vz00_33)
	{
		{	/* SawJvm/funcall.sch 70 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_32)))->BgL_initz00) =
				((obj_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &indexed-init-set! */
	obj_t BGl_z62indexedzd2initzd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4310, obj_t BgL_oz00_4311, obj_t BgL_vz00_4312)
	{
		{	/* SawJvm/funcall.sch 70 */
			return
				BGl_indexedzd2initzd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4311), BgL_vz00_4312);
		}

	}



/* indexed-jvm-type-name */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2jvmzd2typezd2namezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_34)
	{
		{	/* SawJvm/funcall.sch 71 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_34)))->BgL_jvmzd2typezd2namez00);
		}

	}



/* &indexed-jvm-type-name */
	obj_t BGl_z62indexedzd2jvmzd2typezd2namezb0zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4313, obj_t BgL_oz00_4314)
	{
		{	/* SawJvm/funcall.sch 71 */
			return
				BGl_indexedzd2jvmzd2typezd2namezd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4314));
		}

	}



/* indexed-jvm-type-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2jvmzd2typezd2namezd2setz12z12zzsaw_jvm_funcallz00
		(BgL_globalz00_bglt BgL_oz00_35, obj_t BgL_vz00_36)
	{
		{	/* SawJvm/funcall.sch 72 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_35)))->BgL_jvmzd2typezd2namez00) =
				((obj_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &indexed-jvm-type-name-set! */
	obj_t BGl_z62indexedzd2jvmzd2typezd2namezd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4315, obj_t BgL_oz00_4316, obj_t BgL_vz00_4317)
	{
		{	/* SawJvm/funcall.sch 72 */
			return
				BGl_indexedzd2jvmzd2typezd2namezd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4316), BgL_vz00_4317);
		}

	}



/* indexed-src */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2srczd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_37)
	{
		{	/* SawJvm/funcall.sch 73 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_37)))->BgL_srcz00);
		}

	}



/* &indexed-src */
	obj_t BGl_z62indexedzd2srczb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4318,
		obj_t BgL_oz00_4319)
	{
		{	/* SawJvm/funcall.sch 73 */
			return
				BGl_indexedzd2srczd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4319));
		}

	}



/* indexed-src-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2srczd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_38, obj_t BgL_vz00_39)
	{
		{	/* SawJvm/funcall.sch 74 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_38)))->BgL_srcz00) =
				((obj_t) BgL_vz00_39), BUNSPEC);
		}

	}



/* &indexed-src-set! */
	obj_t BGl_z62indexedzd2srczd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4320, obj_t BgL_oz00_4321, obj_t BgL_vz00_4322)
	{
		{	/* SawJvm/funcall.sch 74 */
			return
				BGl_indexedzd2srczd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4321), BgL_vz00_4322);
		}

	}



/* indexed-pragma */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2pragmazd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_40)
	{
		{	/* SawJvm/funcall.sch 75 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_40)))->BgL_pragmaz00);
		}

	}



/* &indexed-pragma */
	obj_t BGl_z62indexedzd2pragmazb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4323,
		obj_t BgL_oz00_4324)
	{
		{	/* SawJvm/funcall.sch 75 */
			return
				BGl_indexedzd2pragmazd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4324));
		}

	}



/* indexed-pragma-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2pragmazd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_41, obj_t BgL_vz00_42)
	{
		{	/* SawJvm/funcall.sch 76 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_41)))->BgL_pragmaz00) =
				((obj_t) BgL_vz00_42), BUNSPEC);
		}

	}



/* &indexed-pragma-set! */
	obj_t BGl_z62indexedzd2pragmazd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4325, obj_t BgL_oz00_4326, obj_t BgL_vz00_4327)
	{
		{	/* SawJvm/funcall.sch 76 */
			return
				BGl_indexedzd2pragmazd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4326), BgL_vz00_4327);
		}

	}



/* indexed-library */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2libraryzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_43)
	{
		{	/* SawJvm/funcall.sch 77 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_43)))->BgL_libraryz00);
		}

	}



/* &indexed-library */
	obj_t BGl_z62indexedzd2libraryzb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4328,
		obj_t BgL_oz00_4329)
	{
		{	/* SawJvm/funcall.sch 77 */
			return
				BGl_indexedzd2libraryzd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4329));
		}

	}



/* indexed-library-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2libraryzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawJvm/funcall.sch 78 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_44)))->BgL_libraryz00) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &indexed-library-set! */
	obj_t BGl_z62indexedzd2libraryzd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4330, obj_t BgL_oz00_4331, obj_t BgL_vz00_4332)
	{
		{	/* SawJvm/funcall.sch 78 */
			return
				BGl_indexedzd2libraryzd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4331), BgL_vz00_4332);
		}

	}



/* indexed-eval? */
	BGL_EXPORTED_DEF bool_t
		BGl_indexedzd2evalzf3z21zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_46)
	{
		{	/* SawJvm/funcall.sch 79 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_46)))->BgL_evalzf3zf3);
		}

	}



/* &indexed-eval? */
	obj_t BGl_z62indexedzd2evalzf3z43zzsaw_jvm_funcallz00(obj_t BgL_envz00_4333,
		obj_t BgL_oz00_4334)
	{
		{	/* SawJvm/funcall.sch 79 */
			return
				BBOOL(BGl_indexedzd2evalzf3z21zzsaw_jvm_funcallz00(
					((BgL_globalz00_bglt) BgL_oz00_4334)));
		}

	}



/* indexed-eval?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2evalzf3zd2setz12ze1zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_47, bool_t BgL_vz00_48)
	{
		{	/* SawJvm/funcall.sch 80 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_47)))->BgL_evalzf3zf3) =
				((bool_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &indexed-eval?-set! */
	obj_t BGl_z62indexedzd2evalzf3zd2setz12z83zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4335, obj_t BgL_oz00_4336, obj_t BgL_vz00_4337)
	{
		{	/* SawJvm/funcall.sch 80 */
			return
				BGl_indexedzd2evalzf3zd2setz12ze1zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4336), CBOOL(BgL_vz00_4337));
		}

	}



/* indexed-evaluable? */
	BGL_EXPORTED_DEF bool_t
		BGl_indexedzd2evaluablezf3z21zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_49)
	{
		{	/* SawJvm/funcall.sch 81 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_49)))->BgL_evaluablezf3zf3);
		}

	}



/* &indexed-evaluable? */
	obj_t BGl_z62indexedzd2evaluablezf3z43zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4338, obj_t BgL_oz00_4339)
	{
		{	/* SawJvm/funcall.sch 81 */
			return
				BBOOL(BGl_indexedzd2evaluablezf3z21zzsaw_jvm_funcallz00(
					((BgL_globalz00_bglt) BgL_oz00_4339)));
		}

	}



/* indexed-evaluable?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2evaluablezf3zd2setz12ze1zzsaw_jvm_funcallz00
		(BgL_globalz00_bglt BgL_oz00_50, bool_t BgL_vz00_51)
	{
		{	/* SawJvm/funcall.sch 82 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_50)))->BgL_evaluablezf3zf3) =
				((bool_t) BgL_vz00_51), BUNSPEC);
		}

	}



/* &indexed-evaluable?-set! */
	obj_t BGl_z62indexedzd2evaluablezf3zd2setz12z83zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4340, obj_t BgL_oz00_4341, obj_t BgL_vz00_4342)
	{
		{	/* SawJvm/funcall.sch 82 */
			return
				BGl_indexedzd2evaluablezf3zd2setz12ze1zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4341), CBOOL(BgL_vz00_4342));
		}

	}



/* indexed-import */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2importzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_52)
	{
		{	/* SawJvm/funcall.sch 83 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_52)))->BgL_importz00);
		}

	}



/* &indexed-import */
	obj_t BGl_z62indexedzd2importzb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4343,
		obj_t BgL_oz00_4344)
	{
		{	/* SawJvm/funcall.sch 83 */
			return
				BGl_indexedzd2importzd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4344));
		}

	}



/* indexed-import-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2importzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_53, obj_t BgL_vz00_54)
	{
		{	/* SawJvm/funcall.sch 84 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_53)))->BgL_importz00) =
				((obj_t) BgL_vz00_54), BUNSPEC);
		}

	}



/* &indexed-import-set! */
	obj_t BGl_z62indexedzd2importzd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4345, obj_t BgL_oz00_4346, obj_t BgL_vz00_4347)
	{
		{	/* SawJvm/funcall.sch 84 */
			return
				BGl_indexedzd2importzd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4346), BgL_vz00_4347);
		}

	}



/* indexed-module */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2modulezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_55)
	{
		{	/* SawJvm/funcall.sch 85 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_55)))->BgL_modulez00);
		}

	}



/* &indexed-module */
	obj_t BGl_z62indexedzd2modulezb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4348,
		obj_t BgL_oz00_4349)
	{
		{	/* SawJvm/funcall.sch 85 */
			return
				BGl_indexedzd2modulezd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4349));
		}

	}



/* indexed-module-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2modulezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_56, obj_t BgL_vz00_57)
	{
		{	/* SawJvm/funcall.sch 86 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_56)))->BgL_modulez00) =
				((obj_t) BgL_vz00_57), BUNSPEC);
		}

	}



/* &indexed-module-set! */
	obj_t BGl_z62indexedzd2modulezd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4350, obj_t BgL_oz00_4351, obj_t BgL_vz00_4352)
	{
		{	/* SawJvm/funcall.sch 86 */
			return
				BGl_indexedzd2modulezd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4351), BgL_vz00_4352);
		}

	}



/* indexed-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_indexedzd2userzf3z21zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_58)
	{
		{	/* SawJvm/funcall.sch 87 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_58)))->BgL_userzf3zf3);
		}

	}



/* &indexed-user? */
	obj_t BGl_z62indexedzd2userzf3z43zzsaw_jvm_funcallz00(obj_t BgL_envz00_4353,
		obj_t BgL_oz00_4354)
	{
		{	/* SawJvm/funcall.sch 87 */
			return
				BBOOL(BGl_indexedzd2userzf3z21zzsaw_jvm_funcallz00(
					((BgL_globalz00_bglt) BgL_oz00_4354)));
		}

	}



/* indexed-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2userzf3zd2setz12ze1zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_59, bool_t BgL_vz00_60)
	{
		{	/* SawJvm/funcall.sch 88 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_59)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &indexed-user?-set! */
	obj_t BGl_z62indexedzd2userzf3zd2setz12z83zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4355, obj_t BgL_oz00_4356, obj_t BgL_vz00_4357)
	{
		{	/* SawJvm/funcall.sch 88 */
			return
				BGl_indexedzd2userzf3zd2setz12ze1zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4356), CBOOL(BgL_vz00_4357));
		}

	}



/* indexed-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_indexedzd2occurrencewzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_61)
	{
		{	/* SawJvm/funcall.sch 89 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_61)))->BgL_occurrencewz00);
		}

	}



/* &indexed-occurrencew */
	obj_t BGl_z62indexedzd2occurrencewzb0zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4358, obj_t BgL_oz00_4359)
	{
		{	/* SawJvm/funcall.sch 89 */
			return
				BINT(BGl_indexedzd2occurrencewzd2zzsaw_jvm_funcallz00(
					((BgL_globalz00_bglt) BgL_oz00_4359)));
		}

	}



/* indexed-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2occurrencewzd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_62, long BgL_vz00_63)
	{
		{	/* SawJvm/funcall.sch 90 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_62)))->BgL_occurrencewz00) =
				((long) BgL_vz00_63), BUNSPEC);
		}

	}



/* &indexed-occurrencew-set! */
	obj_t BGl_z62indexedzd2occurrencewzd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4360, obj_t BgL_oz00_4361, obj_t BgL_vz00_4362)
	{
		{	/* SawJvm/funcall.sch 90 */
			return
				BGl_indexedzd2occurrencewzd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4361), (long) CINT(BgL_vz00_4362));
		}

	}



/* indexed-occurrence */
	BGL_EXPORTED_DEF long
		BGl_indexedzd2occurrencezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_64)
	{
		{	/* SawJvm/funcall.sch 91 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_64)))->BgL_occurrencez00);
		}

	}



/* &indexed-occurrence */
	obj_t BGl_z62indexedzd2occurrencezb0zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4363, obj_t BgL_oz00_4364)
	{
		{	/* SawJvm/funcall.sch 91 */
			return
				BINT(BGl_indexedzd2occurrencezd2zzsaw_jvm_funcallz00(
					((BgL_globalz00_bglt) BgL_oz00_4364)));
		}

	}



/* indexed-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2occurrencezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_65, long BgL_vz00_66)
	{
		{	/* SawJvm/funcall.sch 92 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_65)))->BgL_occurrencez00) =
				((long) BgL_vz00_66), BUNSPEC);
		}

	}



/* &indexed-occurrence-set! */
	obj_t BGl_z62indexedzd2occurrencezd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4365, obj_t BgL_oz00_4366, obj_t BgL_vz00_4367)
	{
		{	/* SawJvm/funcall.sch 92 */
			return
				BGl_indexedzd2occurrencezd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4366), (long) CINT(BgL_vz00_4367));
		}

	}



/* indexed-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2removablezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_67)
	{
		{	/* SawJvm/funcall.sch 93 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_67)))->BgL_removablez00);
		}

	}



/* &indexed-removable */
	obj_t BGl_z62indexedzd2removablezb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4368,
		obj_t BgL_oz00_4369)
	{
		{	/* SawJvm/funcall.sch 93 */
			return
				BGl_indexedzd2removablezd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4369));
		}

	}



/* indexed-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2removablezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_68, obj_t BgL_vz00_69)
	{
		{	/* SawJvm/funcall.sch 94 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_68)))->BgL_removablez00) =
				((obj_t) BgL_vz00_69), BUNSPEC);
		}

	}



/* &indexed-removable-set! */
	obj_t BGl_z62indexedzd2removablezd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4370, obj_t BgL_oz00_4371, obj_t BgL_vz00_4372)
	{
		{	/* SawJvm/funcall.sch 94 */
			return
				BGl_indexedzd2removablezd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4371), BgL_vz00_4372);
		}

	}



/* indexed-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2fastzd2alphaz00zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_70)
	{
		{	/* SawJvm/funcall.sch 95 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_70)))->BgL_fastzd2alphazd2);
		}

	}



/* &indexed-fast-alpha */
	obj_t BGl_z62indexedzd2fastzd2alphaz62zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4373, obj_t BgL_oz00_4374)
	{
		{	/* SawJvm/funcall.sch 95 */
			return
				BGl_indexedzd2fastzd2alphaz00zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4374));
		}

	}



/* indexed-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2fastzd2alphazd2setz12zc0zzsaw_jvm_funcallz00
		(BgL_globalz00_bglt BgL_oz00_71, obj_t BgL_vz00_72)
	{
		{	/* SawJvm/funcall.sch 96 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_71)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_72), BUNSPEC);
		}

	}



/* &indexed-fast-alpha-set! */
	obj_t BGl_z62indexedzd2fastzd2alphazd2setz12za2zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4375, obj_t BgL_oz00_4376, obj_t BgL_vz00_4377)
	{
		{	/* SawJvm/funcall.sch 96 */
			return
				BGl_indexedzd2fastzd2alphazd2setz12zc0zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4376), BgL_vz00_4377);
		}

	}



/* indexed-access */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2accesszd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_73)
	{
		{	/* SawJvm/funcall.sch 97 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_73)))->BgL_accessz00);
		}

	}



/* &indexed-access */
	obj_t BGl_z62indexedzd2accesszb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4378,
		obj_t BgL_oz00_4379)
	{
		{	/* SawJvm/funcall.sch 97 */
			return
				BGl_indexedzd2accesszd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4379));
		}

	}



/* indexed-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2accesszd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_74, obj_t BgL_vz00_75)
	{
		{	/* SawJvm/funcall.sch 98 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_74)))->BgL_accessz00) =
				((obj_t) BgL_vz00_75), BUNSPEC);
		}

	}



/* &indexed-access-set! */
	obj_t BGl_z62indexedzd2accesszd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4380, obj_t BgL_oz00_4381, obj_t BgL_vz00_4382)
	{
		{	/* SawJvm/funcall.sch 98 */
			return
				BGl_indexedzd2accesszd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4381), BgL_vz00_4382);
		}

	}



/* indexed-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_indexedzd2valuezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_76)
	{
		{	/* SawJvm/funcall.sch 99 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_76)))->BgL_valuez00);
		}

	}



/* &indexed-value */
	BgL_valuez00_bglt BGl_z62indexedzd2valuezb0zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4383, obj_t BgL_oz00_4384)
	{
		{	/* SawJvm/funcall.sch 99 */
			return
				BGl_indexedzd2valuezd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4384));
		}

	}



/* indexed-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2valuezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_77, BgL_valuez00_bglt BgL_vz00_78)
	{
		{	/* SawJvm/funcall.sch 100 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_77)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_78), BUNSPEC);
		}

	}



/* &indexed-value-set! */
	obj_t BGl_z62indexedzd2valuezd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4385, obj_t BgL_oz00_4386, obj_t BgL_vz00_4387)
	{
		{	/* SawJvm/funcall.sch 100 */
			return
				BGl_indexedzd2valuezd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4386),
				((BgL_valuez00_bglt) BgL_vz00_4387));
		}

	}



/* indexed-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_indexedzd2typezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_79)
	{
		{	/* SawJvm/funcall.sch 101 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_79)))->BgL_typez00);
		}

	}



/* &indexed-type */
	BgL_typez00_bglt BGl_z62indexedzd2typezb0zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4388, obj_t BgL_oz00_4389)
	{
		{	/* SawJvm/funcall.sch 101 */
			return
				BGl_indexedzd2typezd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4389));
		}

	}



/* indexed-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2typezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_80, BgL_typez00_bglt BgL_vz00_81)
	{
		{	/* SawJvm/funcall.sch 102 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_80)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_81), BUNSPEC);
		}

	}



/* &indexed-type-set! */
	obj_t BGl_z62indexedzd2typezd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4390, obj_t BgL_oz00_4391, obj_t BgL_vz00_4392)
	{
		{	/* SawJvm/funcall.sch 102 */
			return
				BGl_indexedzd2typezd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4391),
				((BgL_typez00_bglt) BgL_vz00_4392));
		}

	}



/* indexed-name */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2namezd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_82)
	{
		{	/* SawJvm/funcall.sch 103 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_82)))->BgL_namez00);
		}

	}



/* &indexed-name */
	obj_t BGl_z62indexedzd2namezb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4393,
		obj_t BgL_oz00_4394)
	{
		{	/* SawJvm/funcall.sch 103 */
			return
				BGl_indexedzd2namezd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4394));
		}

	}



/* indexed-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2namezd2setz12z12zzsaw_jvm_funcallz00(BgL_globalz00_bglt
		BgL_oz00_83, obj_t BgL_vz00_84)
	{
		{	/* SawJvm/funcall.sch 104 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_83)))->BgL_namez00) =
				((obj_t) BgL_vz00_84), BUNSPEC);
		}

	}



/* &indexed-name-set! */
	obj_t BGl_z62indexedzd2namezd2setz12z70zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4395, obj_t BgL_oz00_4396, obj_t BgL_vz00_4397)
	{
		{	/* SawJvm/funcall.sch 104 */
			return
				BGl_indexedzd2namezd2setz12z12zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4396), BgL_vz00_4397);
		}

	}



/* indexed-id */
	BGL_EXPORTED_DEF obj_t
		BGl_indexedzd2idzd2zzsaw_jvm_funcallz00(BgL_globalz00_bglt BgL_oz00_85)
	{
		{	/* SawJvm/funcall.sch 105 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_85)))->BgL_idz00);
		}

	}



/* &indexed-id */
	obj_t BGl_z62indexedzd2idzb0zzsaw_jvm_funcallz00(obj_t BgL_envz00_4398,
		obj_t BgL_oz00_4399)
	{
		{	/* SawJvm/funcall.sch 105 */
			return
				BGl_indexedzd2idzd2zzsaw_jvm_funcallz00(
				((BgL_globalz00_bglt) BgL_oz00_4399));
		}

	}



/* key-opt? */
	bool_t BGl_keyzd2optzf3z21zzsaw_jvm_funcallz00(obj_t BgL_vz00_88)
	{
		{	/* SawJvm/funcall.scm 22 */
			{	/* SawJvm/funcall.scm 23 */
				BgL_globalz00_bglt BgL_vz00_2522;

				BgL_vz00_2522 =
					BGl_globalzd2entryzd2zzbackend_cplibz00(
					((BgL_globalz00_bglt) BgL_vz00_88));
				{	/* SawJvm/funcall.scm 24 */
					BgL_valuez00_bglt BgL_valz00_2523;

					BgL_valz00_2523 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_vz00_2522)))->BgL_valuez00);
					{	/* SawJvm/funcall.scm 25 */
						bool_t BgL_test2331z00_4906;

						{	/* SawJvm/funcall.scm 25 */
							obj_t BgL_classz00_3736;

							BgL_classz00_3736 = BGl_sfunz00zzast_varz00;
							{	/* SawJvm/funcall.scm 25 */
								BgL_objectz00_bglt BgL_arg1807z00_3738;

								{	/* SawJvm/funcall.scm 25 */
									obj_t BgL_tmpz00_4907;

									BgL_tmpz00_4907 =
										((obj_t) ((BgL_objectz00_bglt) BgL_valz00_2523));
									BgL_arg1807z00_3738 = (BgL_objectz00_bglt) (BgL_tmpz00_4907);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawJvm/funcall.scm 25 */
										long BgL_idxz00_3744;

										BgL_idxz00_3744 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3738);
										BgL_test2331z00_4906 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3744 + 3L)) == BgL_classz00_3736);
									}
								else
									{	/* SawJvm/funcall.scm 25 */
										bool_t BgL_res2234z00_3769;

										{	/* SawJvm/funcall.scm 25 */
											obj_t BgL_oclassz00_3752;

											{	/* SawJvm/funcall.scm 25 */
												obj_t BgL_arg1815z00_3760;
												long BgL_arg1816z00_3761;

												BgL_arg1815z00_3760 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawJvm/funcall.scm 25 */
													long BgL_arg1817z00_3762;

													BgL_arg1817z00_3762 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3738);
													BgL_arg1816z00_3761 =
														(BgL_arg1817z00_3762 - OBJECT_TYPE);
												}
												BgL_oclassz00_3752 =
													VECTOR_REF(BgL_arg1815z00_3760, BgL_arg1816z00_3761);
											}
											{	/* SawJvm/funcall.scm 25 */
												bool_t BgL__ortest_1115z00_3753;

												BgL__ortest_1115z00_3753 =
													(BgL_classz00_3736 == BgL_oclassz00_3752);
												if (BgL__ortest_1115z00_3753)
													{	/* SawJvm/funcall.scm 25 */
														BgL_res2234z00_3769 = BgL__ortest_1115z00_3753;
													}
												else
													{	/* SawJvm/funcall.scm 25 */
														long BgL_odepthz00_3754;

														{	/* SawJvm/funcall.scm 25 */
															obj_t BgL_arg1804z00_3755;

															BgL_arg1804z00_3755 = (BgL_oclassz00_3752);
															BgL_odepthz00_3754 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3755);
														}
														if ((3L < BgL_odepthz00_3754))
															{	/* SawJvm/funcall.scm 25 */
																obj_t BgL_arg1802z00_3757;

																{	/* SawJvm/funcall.scm 25 */
																	obj_t BgL_arg1803z00_3758;

																	BgL_arg1803z00_3758 = (BgL_oclassz00_3752);
																	BgL_arg1802z00_3757 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3758,
																		3L);
																}
																BgL_res2234z00_3769 =
																	(BgL_arg1802z00_3757 == BgL_classz00_3736);
															}
														else
															{	/* SawJvm/funcall.scm 25 */
																BgL_res2234z00_3769 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2331z00_4906 = BgL_res2234z00_3769;
									}
							}
						}
						if (BgL_test2331z00_4906)
							{	/* SawJvm/funcall.scm 26 */
								obj_t BgL_cloz00_2525;

								BgL_cloz00_2525 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_valz00_2523)))->
									BgL_thezd2closurezd2globalz00);
								{	/* SawJvm/funcall.scm 27 */
									bool_t BgL_oz00_2526;
									bool_t BgL_kz00_2527;

									BgL_oz00_2526 =
										BGl_globalzd2optionalzf3z21zzast_varz00(BgL_cloz00_2525);
									BgL_kz00_2527 =
										BGl_globalzd2keyzf3z21zzast_varz00(BgL_cloz00_2525);
									if (BgL_oz00_2526)
										{	/* SawJvm/funcall.scm 28 */
											return BgL_oz00_2526;
										}
									else
										{	/* SawJvm/funcall.scm 28 */
											return BgL_kz00_2527;
										}
								}
							}
						else
							{	/* SawJvm/funcall.scm 25 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* module-light-funcall */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2lightzd2funcallz00zzsaw_jvm_funcallz00(BgL_jvmz00_bglt
		BgL_mez00_89)
	{
		{	/* SawJvm/funcall.scm 32 */
			{	/* SawJvm/funcall.scm 34 */
				obj_t BgL_g1585z00_2530;

				BgL_g1585z00_2530 =
					(((BgL_jvmz00_bglt) COBJECT(BgL_mez00_89))->BgL_lightzd2funcallszd2);
				{
					obj_t BgL_l1583z00_2532;

					{	/* SawJvm/funcall.scm 34 */
						bool_t BgL_tmpz00_4936;

						BgL_l1583z00_2532 = BgL_g1585z00_2530;
					BgL_zc3z04anonymousza31680ze3z87_2533:
						if (PAIRP(BgL_l1583z00_2532))
							{	/* SawJvm/funcall.scm 34 */
								{	/* SawJvm/funcall.scm 35 */
									obj_t BgL_insz00_2535;

									BgL_insz00_2535 = CAR(BgL_l1583z00_2532);
									{	/* SawJvm/funcall.scm 36 */
										obj_t BgL_arg1688z00_2537;
										obj_t BgL_arg1689z00_2538;
										obj_t BgL_arg1691z00_2539;

										BgL_arg1688z00_2537 =
											(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
													((BgL_rtl_lightfuncallz00_bglt) BgL_insz00_2535)))->
											BgL_namez00);
										BgL_arg1689z00_2538 =
											(((BgL_rtl_lightfuncallz00_bglt)
												COBJECT(((BgL_rtl_lightfuncallz00_bglt)
														BgL_insz00_2535)))->BgL_funsz00);
										BgL_arg1691z00_2539 =
											(((BgL_rtl_lightfuncallz00_bglt)
												COBJECT(((BgL_rtl_lightfuncallz00_bglt)
														BgL_insz00_2535)))->BgL_rettypez00);
										BGl_funcallzd2lightzd2zzsaw_jvm_funcallz00(BgL_mez00_89,
											BgL_arg1688z00_2537, BgL_arg1689z00_2538,
											BgL_arg1691z00_2539);
									}
								}
								{
									obj_t BgL_l1583z00_4947;

									BgL_l1583z00_4947 = CDR(BgL_l1583z00_2532);
									BgL_l1583z00_2532 = BgL_l1583z00_4947;
									goto BgL_zc3z04anonymousza31680ze3z87_2533;
								}
							}
						else
							{	/* SawJvm/funcall.scm 34 */
								BgL_tmpz00_4936 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_4936);
					}
				}
			}
		}

	}



/* &module-light-funcall */
	obj_t BGl_z62modulezd2lightzd2funcallz62zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4400, obj_t BgL_mez00_4401)
	{
		{	/* SawJvm/funcall.scm 32 */
			return
				BGl_modulezd2lightzd2funcallz00zzsaw_jvm_funcallz00(
				((BgL_jvmz00_bglt) BgL_mez00_4401));
		}

	}



/* funcall-light */
	obj_t BGl_funcallzd2lightzd2zzsaw_jvm_funcallz00(BgL_jvmz00_bglt BgL_mez00_90,
		obj_t BgL_namez00_91, obj_t BgL_funsz00_92, obj_t BgL_rettypez00_93)
	{
		{	/* SawJvm/funcall.scm 39 */
			{
				obj_t BgL_tz00_2572;
				BgL_variablez00_bglt BgL_gz00_2611;

				{	/* SawJvm/funcall.scm 70 */
					obj_t BgL_fz00_2549;

					BgL_fz00_2549 = CAR(((obj_t) BgL_funsz00_92));
					{	/* SawJvm/funcall.scm 70 */
						BgL_variablez00_bglt BgL_gz00_2550;

						BgL_gz00_2550 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt) BgL_fz00_2549)))->BgL_variablez00);
						{	/* SawJvm/funcall.scm 71 */
							obj_t BgL_trz00_2551;

							BgL_trz00_2551 =
								BGl_compilezd2typezd2zzsaw_jvm_outz00(BgL_mez00_90,
								((BgL_typez00_bglt) BgL_rettypez00_93));
							{	/* SawJvm/funcall.scm 72 */
								obj_t BgL_talz00_2552;

								BgL_gz00_2611 = BgL_gz00_2550;
								{	/* SawJvm/funcall.scm 66 */
									obj_t BgL_argsz00_2613;

									BgL_argsz00_2613 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_gz00_2611))))->
														BgL_valuez00))))->BgL_argsz00);
									{	/* SawJvm/funcall.scm 67 */
										obj_t BgL_arg1748z00_2614;
										obj_t BgL_arg1749z00_2615;

										{	/* SawJvm/funcall.scm 67 */
											obj_t BgL_arg1750z00_2616;

											BgL_arg1750z00_2616 = CAR(((obj_t) BgL_argsz00_2613));
											{	/* SawJvm/funcall.scm 52 */
												obj_t BgL_arg1717z00_3851;

												BgL_arg1717z00_3851 =
													BGl_getzd2typeze70z35zzsaw_jvm_funcallz00
													(BgL_arg1750z00_2616);
												BgL_arg1748z00_2614 =
													BGl_compilezd2typezd2zzsaw_jvm_outz00(BgL_mez00_90,
													((BgL_typez00_bglt) BgL_arg1717z00_3851));
											}
										}
										{	/* SawJvm/funcall.scm 68 */
											obj_t BgL_l1586z00_2617;

											BgL_l1586z00_2617 = CDR(((obj_t) BgL_argsz00_2613));
											if (NULLP(BgL_l1586z00_2617))
												{	/* SawJvm/funcall.scm 68 */
													BgL_arg1749z00_2615 = BNIL;
												}
											else
												{	/* SawJvm/funcall.scm 68 */
													obj_t BgL_head1588z00_2619;

													BgL_head1588z00_2619 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													{
														obj_t BgL_l1586z00_2621;
														obj_t BgL_tail1589z00_2622;

														BgL_l1586z00_2621 = BgL_l1586z00_2617;
														BgL_tail1589z00_2622 = BgL_head1588z00_2619;
													BgL_zc3z04anonymousza31752ze3z87_2623:
														if (NULLP(BgL_l1586z00_2621))
															{	/* SawJvm/funcall.scm 68 */
																BgL_arg1749z00_2615 = CDR(BgL_head1588z00_2619);
															}
														else
															{	/* SawJvm/funcall.scm 68 */
																obj_t BgL_newtail1590z00_2625;

																{	/* SawJvm/funcall.scm 68 */
																	obj_t BgL_arg1755z00_2627;

																	{	/* SawJvm/funcall.scm 68 */
																		obj_t BgL_az00_2628;

																		BgL_az00_2628 =
																			CAR(((obj_t) BgL_l1586z00_2621));
																		{	/* SawJvm/funcall.scm 54 */
																			obj_t BgL_arg1720z00_3856;

																			BgL_tz00_2572 =
																				BGl_getzd2typeze70z35zzsaw_jvm_funcallz00
																				(BgL_az00_2628);
																			if (BGl_bigloozd2typezf3z21zztype_typez00(
																					((BgL_typez00_bglt) BgL_tz00_2572)))
																				{	/* SawJvm/funcall.scm 41 */
																					BgL_arg1720z00_3856 =
																						BGl_za2objza2z00zztype_cachez00;
																				}
																			else
																				{	/* SawJvm/funcall.scm 41 */
																					BgL_arg1720z00_3856 = BgL_tz00_2572;
																				}
																			BgL_arg1755z00_2627 =
																				BGl_compilezd2typezd2zzsaw_jvm_outz00
																				(BgL_mez00_90,
																				((BgL_typez00_bglt)
																					BgL_arg1720z00_3856));
																		}
																	}
																	BgL_newtail1590z00_2625 =
																		MAKE_YOUNG_PAIR(BgL_arg1755z00_2627, BNIL);
																}
																SET_CDR(BgL_tail1589z00_2622,
																	BgL_newtail1590z00_2625);
																{	/* SawJvm/funcall.scm 68 */
																	obj_t BgL_arg1754z00_2626;

																	BgL_arg1754z00_2626 =
																		CDR(((obj_t) BgL_l1586z00_2621));
																	{
																		obj_t BgL_tail1589z00_4989;
																		obj_t BgL_l1586z00_4988;

																		BgL_l1586z00_4988 = BgL_arg1754z00_2626;
																		BgL_tail1589z00_4989 =
																			BgL_newtail1590z00_2625;
																		BgL_tail1589z00_2622 = BgL_tail1589z00_4989;
																		BgL_l1586z00_2621 = BgL_l1586z00_4988;
																		goto BgL_zc3z04anonymousza31752ze3z87_2623;
																	}
																}
															}
													}
												}
										}
										BgL_talz00_2552 =
											MAKE_YOUNG_PAIR(BgL_arg1748z00_2614, BgL_arg1749z00_2615);
									}
								}
								{	/* SawJvm/funcall.scm 73 */
									obj_t BgL_pz00_2553;

									{	/* SawJvm/funcall.scm 74 */
										obj_t BgL_head1593z00_2560;

										BgL_head1593z00_2560 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1591z00_2562;
											obj_t BgL_tail1594z00_2563;

											BgL_l1591z00_2562 = BgL_talz00_2552;
											BgL_tail1594z00_2563 = BgL_head1593z00_2560;
										BgL_zc3z04anonymousza31704ze3z87_2564:
											if (NULLP(BgL_l1591z00_2562))
												{	/* SawJvm/funcall.scm 74 */
													BgL_pz00_2553 = CDR(BgL_head1593z00_2560);
												}
											else
												{	/* SawJvm/funcall.scm 74 */
													obj_t BgL_newtail1595z00_2566;

													{	/* SawJvm/funcall.scm 74 */
														obj_t BgL_arg1709z00_2568;

														{	/* SawJvm/funcall.scm 74 */

															{	/* SawJvm/funcall.scm 74 */

																BgL_arg1709z00_2568 =
																	BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
															}
														}
														BgL_newtail1595z00_2566 =
															MAKE_YOUNG_PAIR(BgL_arg1709z00_2568, BNIL);
													}
													SET_CDR(BgL_tail1594z00_2563,
														BgL_newtail1595z00_2566);
													{	/* SawJvm/funcall.scm 74 */
														obj_t BgL_arg1708z00_2567;

														BgL_arg1708z00_2567 =
															CDR(((obj_t) BgL_l1591z00_2562));
														{
															obj_t BgL_tail1594z00_5001;
															obj_t BgL_l1591z00_5000;

															BgL_l1591z00_5000 = BgL_arg1708z00_2567;
															BgL_tail1594z00_5001 = BgL_newtail1595z00_2566;
															BgL_tail1594z00_2563 = BgL_tail1594z00_5001;
															BgL_l1591z00_2562 = BgL_l1591z00_5000;
															goto BgL_zc3z04anonymousza31704ze3z87_2564;
														}
													}
												}
										}
									}
									{	/* SawJvm/funcall.scm 74 */

										{	/* SawJvm/funcall.scm 76 */
											obj_t BgL_arg1699z00_2554;

											{	/* SawJvm/funcall.scm 76 */
												obj_t BgL_arg1455z00_3867;

												BgL_arg1455z00_3867 = SYMBOL_TO_STRING(BgL_namez00_91);
												BgL_arg1699z00_2554 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_3867);
											}
											BGl_declarezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_90,
												BgL_namez00_91, CNST_TABLE_REF(0), CNST_TABLE_REF(1),
												BgL_trz00_2551, BgL_arg1699z00_2554, BgL_talz00_2552);
										}
										BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_mez00_90,
											BgL_namez00_91);
										BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_mez00_90,
											BgL_pz00_2553, BNIL);
										{	/* SawJvm/funcall.scm 80 */
											obj_t BgL_arg1700z00_2555;

											{	/* SawJvm/funcall.scm 80 */
												obj_t BgL_arg1701z00_2556;

												{	/* SawJvm/funcall.scm 80 */
													obj_t BgL_arg1702z00_2557;

													BgL_arg1702z00_2557 = CAR(((obj_t) BgL_pz00_2553));
													BgL_arg1701z00_2556 =
														MAKE_YOUNG_PAIR(BgL_arg1702z00_2557, BNIL);
												}
												BgL_arg1700z00_2555 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
													BgL_arg1701z00_2556);
											}
											BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_90,
												BgL_arg1700z00_2555);
										}
										BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_90,
											CNST_TABLE_REF(3));
										BGl_funcallzd2lightzd2switchz00zzsaw_jvm_funcallz00
											(BgL_mez00_90, BgL_funsz00_92, BgL_trz00_2551,
											BgL_pz00_2553, BgL_talz00_2552);
										return BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_90);
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* get-type~0 */
	obj_t BGl_getzd2typeze70z35zzsaw_jvm_funcallz00(obj_t BgL_tz00_2575)
	{
		{	/* SawJvm/funcall.scm 50 */
			{	/* SawJvm/funcall.scm 46 */
				bool_t BgL_test2341z00_5019;

				{	/* SawJvm/funcall.scm 46 */
					obj_t BgL_classz00_3774;

					BgL_classz00_3774 = BGl_localz00zzast_varz00;
					if (BGL_OBJECTP(BgL_tz00_2575))
						{	/* SawJvm/funcall.scm 46 */
							BgL_objectz00_bglt BgL_arg1807z00_3776;

							BgL_arg1807z00_3776 = (BgL_objectz00_bglt) (BgL_tz00_2575);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/funcall.scm 46 */
									long BgL_idxz00_3782;

									BgL_idxz00_3782 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3776);
									BgL_test2341z00_5019 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3782 + 2L)) == BgL_classz00_3774);
								}
							else
								{	/* SawJvm/funcall.scm 46 */
									bool_t BgL_res2235z00_3807;

									{	/* SawJvm/funcall.scm 46 */
										obj_t BgL_oclassz00_3790;

										{	/* SawJvm/funcall.scm 46 */
											obj_t BgL_arg1815z00_3798;
											long BgL_arg1816z00_3799;

											BgL_arg1815z00_3798 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/funcall.scm 46 */
												long BgL_arg1817z00_3800;

												BgL_arg1817z00_3800 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3776);
												BgL_arg1816z00_3799 =
													(BgL_arg1817z00_3800 - OBJECT_TYPE);
											}
											BgL_oclassz00_3790 =
												VECTOR_REF(BgL_arg1815z00_3798, BgL_arg1816z00_3799);
										}
										{	/* SawJvm/funcall.scm 46 */
											bool_t BgL__ortest_1115z00_3791;

											BgL__ortest_1115z00_3791 =
												(BgL_classz00_3774 == BgL_oclassz00_3790);
											if (BgL__ortest_1115z00_3791)
												{	/* SawJvm/funcall.scm 46 */
													BgL_res2235z00_3807 = BgL__ortest_1115z00_3791;
												}
											else
												{	/* SawJvm/funcall.scm 46 */
													long BgL_odepthz00_3792;

													{	/* SawJvm/funcall.scm 46 */
														obj_t BgL_arg1804z00_3793;

														BgL_arg1804z00_3793 = (BgL_oclassz00_3790);
														BgL_odepthz00_3792 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3793);
													}
													if ((2L < BgL_odepthz00_3792))
														{	/* SawJvm/funcall.scm 46 */
															obj_t BgL_arg1802z00_3795;

															{	/* SawJvm/funcall.scm 46 */
																obj_t BgL_arg1803z00_3796;

																BgL_arg1803z00_3796 = (BgL_oclassz00_3790);
																BgL_arg1802z00_3795 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3796,
																	2L);
															}
															BgL_res2235z00_3807 =
																(BgL_arg1802z00_3795 == BgL_classz00_3774);
														}
													else
														{	/* SawJvm/funcall.scm 46 */
															BgL_res2235z00_3807 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2341z00_5019 = BgL_res2235z00_3807;
								}
						}
					else
						{	/* SawJvm/funcall.scm 46 */
							BgL_test2341z00_5019 = ((bool_t) 0);
						}
				}
				if (BgL_test2341z00_5019)
					{	/* SawJvm/funcall.scm 46 */
						return
							((obj_t)
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_localz00_bglt) BgL_tz00_2575))))->BgL_typez00));
					}
				else
					{	/* SawJvm/funcall.scm 48 */
						bool_t BgL_test2346z00_5046;

						{	/* SawJvm/funcall.scm 48 */
							obj_t BgL_classz00_3809;

							BgL_classz00_3809 = BGl_typez00zztype_typez00;
							if (BGL_OBJECTP(BgL_tz00_2575))
								{	/* SawJvm/funcall.scm 48 */
									BgL_objectz00_bglt BgL_arg1807z00_3811;

									BgL_arg1807z00_3811 = (BgL_objectz00_bglt) (BgL_tz00_2575);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawJvm/funcall.scm 48 */
											long BgL_idxz00_3817;

											BgL_idxz00_3817 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3811);
											BgL_test2346z00_5046 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3817 + 1L)) == BgL_classz00_3809);
										}
									else
										{	/* SawJvm/funcall.scm 48 */
											bool_t BgL_res2236z00_3842;

											{	/* SawJvm/funcall.scm 48 */
												obj_t BgL_oclassz00_3825;

												{	/* SawJvm/funcall.scm 48 */
													obj_t BgL_arg1815z00_3833;
													long BgL_arg1816z00_3834;

													BgL_arg1815z00_3833 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawJvm/funcall.scm 48 */
														long BgL_arg1817z00_3835;

														BgL_arg1817z00_3835 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3811);
														BgL_arg1816z00_3834 =
															(BgL_arg1817z00_3835 - OBJECT_TYPE);
													}
													BgL_oclassz00_3825 =
														VECTOR_REF(BgL_arg1815z00_3833,
														BgL_arg1816z00_3834);
												}
												{	/* SawJvm/funcall.scm 48 */
													bool_t BgL__ortest_1115z00_3826;

													BgL__ortest_1115z00_3826 =
														(BgL_classz00_3809 == BgL_oclassz00_3825);
													if (BgL__ortest_1115z00_3826)
														{	/* SawJvm/funcall.scm 48 */
															BgL_res2236z00_3842 = BgL__ortest_1115z00_3826;
														}
													else
														{	/* SawJvm/funcall.scm 48 */
															long BgL_odepthz00_3827;

															{	/* SawJvm/funcall.scm 48 */
																obj_t BgL_arg1804z00_3828;

																BgL_arg1804z00_3828 = (BgL_oclassz00_3825);
																BgL_odepthz00_3827 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3828);
															}
															if ((1L < BgL_odepthz00_3827))
																{	/* SawJvm/funcall.scm 48 */
																	obj_t BgL_arg1802z00_3830;

																	{	/* SawJvm/funcall.scm 48 */
																		obj_t BgL_arg1803z00_3831;

																		BgL_arg1803z00_3831 = (BgL_oclassz00_3825);
																		BgL_arg1802z00_3830 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3831, 1L);
																	}
																	BgL_res2236z00_3842 =
																		(BgL_arg1802z00_3830 == BgL_classz00_3809);
																}
															else
																{	/* SawJvm/funcall.scm 48 */
																	BgL_res2236z00_3842 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2346z00_5046 = BgL_res2236z00_3842;
										}
								}
							else
								{	/* SawJvm/funcall.scm 48 */
									BgL_test2346z00_5046 = ((bool_t) 0);
								}
						}
						if (BgL_test2346z00_5046)
							{	/* SawJvm/funcall.scm 48 */
								return BgL_tz00_2575;
							}
						else
							{	/* SawJvm/funcall.scm 48 */
								return
									BGl_errorz00zz__errorz00(CNST_TABLE_REF(4),
									BGl_string2245z00zzsaw_jvm_funcallz00, BgL_tz00_2575);
							}
					}
			}
		}

	}



/* funcall-light-switch */
	bool_t BGl_funcallzd2lightzd2switchz00zzsaw_jvm_funcallz00(BgL_jvmz00_bglt
		BgL_mez00_94, obj_t BgL_funsz00_95, obj_t BgL_trz00_96, obj_t BgL_pz00_97,
		obj_t BgL_talz00_98)
	{
		{	/* SawJvm/funcall.scm 85 */
			{
				obj_t BgL_tz00_2683;
				obj_t BgL_lz00_2700;

				{	/* SawJvm/funcall.scm 113 */
					obj_t BgL_thisz00_2643;

					BgL_thisz00_2643 = CAR(((obj_t) BgL_pz00_97));
					{	/* SawJvm/funcall.scm 113 */
						obj_t BgL_iza2za2_2644;

						BgL_lz00_2700 = BgL_funsz00_95;
						if (NULLP(BgL_funsz00_95))
							{	/* SawJvm/funcall.scm 96 */
								BgL_iza2za2_2644 = BNIL;
							}
						else
							{	/* SawJvm/funcall.scm 96 */
								obj_t BgL_head1598z00_2704;

								BgL_head1598z00_2704 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1596z00_2706;
									obj_t BgL_tail1599z00_2707;

									BgL_l1596z00_2706 = BgL_funsz00_95;
									BgL_tail1599z00_2707 = BgL_head1598z00_2704;
								BgL_zc3z04anonymousza31844ze3z87_2708:
									if (NULLP(BgL_l1596z00_2706))
										{	/* SawJvm/funcall.scm 96 */
											BgL_iza2za2_2644 = CDR(BgL_head1598z00_2704);
										}
									else
										{	/* SawJvm/funcall.scm 96 */
											obj_t BgL_newtail1600z00_2710;

											{	/* SawJvm/funcall.scm 96 */
												int BgL_arg1847z00_2712;

												{	/* SawJvm/funcall.scm 96 */
													BgL_globalz00_bglt BgL_oz00_3876;

													BgL_oz00_3876 =
														((BgL_globalz00_bglt)
														(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt)
																		CAR(
																			((obj_t) BgL_l1596z00_2706)))))->
															BgL_variablez00));
													{
														BgL_indexedz00_bglt BgL_auxz00_5084;

														{
															obj_t BgL_auxz00_5085;

															{	/* SawJvm/funcall.scm 96 */
																BgL_objectz00_bglt BgL_tmpz00_5086;

																BgL_tmpz00_5086 =
																	((BgL_objectz00_bglt) BgL_oz00_3876);
																BgL_auxz00_5085 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5086);
															}
															BgL_auxz00_5084 =
																((BgL_indexedz00_bglt) BgL_auxz00_5085);
														}
														BgL_arg1847z00_2712 =
															(((BgL_indexedz00_bglt)
																COBJECT(BgL_auxz00_5084))->BgL_indexz00);
												}}
												BgL_newtail1600z00_2710 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg1847z00_2712), BNIL);
											}
											SET_CDR(BgL_tail1599z00_2707, BgL_newtail1600z00_2710);
											{	/* SawJvm/funcall.scm 96 */
												obj_t BgL_arg1846z00_2711;

												BgL_arg1846z00_2711 = CDR(((obj_t) BgL_l1596z00_2706));
												{
													obj_t BgL_tail1599z00_5097;
													obj_t BgL_l1596z00_5096;

													BgL_l1596z00_5096 = BgL_arg1846z00_2711;
													BgL_tail1599z00_5097 = BgL_newtail1600z00_2710;
													BgL_tail1599z00_2707 = BgL_tail1599z00_5097;
													BgL_l1596z00_2706 = BgL_l1596z00_5096;
													goto BgL_zc3z04anonymousza31844ze3z87_2708;
												}
											}
										}
								}
							}
						{	/* SawJvm/funcall.scm 114 */
							obj_t BgL_labsz00_2645;

							BgL_labsz00_2645 =
								BGl_getzd2labsze72z35zzsaw_jvm_funcallz00(0L,
								BGl_sortz00zz__r4_vectors_6_8z00(BgL_iza2za2_2644,
									BGl_zc3zd2envz11zz__r4_numbers_6_5z00));
							{	/* SawJvm/funcall.scm 115 */

								{	/* SawJvm/funcall.scm 118 */
									obj_t BgL_arg1762z00_2646;

									{	/* SawJvm/funcall.scm 118 */
										obj_t BgL_arg1765z00_2647;

										{	/* SawJvm/funcall.scm 118 */
											obj_t BgL_arg1767z00_2648;

											BgL_arg1767z00_2648 =
												MAKE_YOUNG_PAIR(BINT(0L),
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_labsz00_2645, BNIL));
											BgL_arg1765z00_2647 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
												BgL_arg1767z00_2648);
										}
										BgL_arg1762z00_2646 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(21), BgL_arg1765z00_2647);
									}
									BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_94,
										BgL_arg1762z00_2646);
								}
								BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_94, CNST_TABLE_REF(20));
								BGl_pushzd2stringzd2zzsaw_jvm_outz00(BgL_mez00_94,
									BGl_string2246z00zzsaw_jvm_funcallz00);
								BGl_pushzd2stringzd2zzsaw_jvm_outz00(BgL_mez00_94,
									BGl_string2247z00zzsaw_jvm_funcallz00);
								{	/* SawJvm/funcall.scm 122 */
									obj_t BgL_arg1771z00_2650;

									{	/* SawJvm/funcall.scm 122 */
										obj_t BgL_arg1773z00_2651;

										BgL_arg1773z00_2651 =
											MAKE_YOUNG_PAIR(BgL_thisz00_2643, BNIL);
										BgL_arg1771z00_2650 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1773z00_2651);
									}
									BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_94,
										BgL_arg1771z00_2650);
								}
								BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_94,
									CNST_TABLE_REF(22));
								BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_94,
									CNST_TABLE_REF(23));
								{
									obj_t BgL_l1601z00_2653;

									BgL_l1601z00_2653 = BgL_funsz00_95;
								BgL_zc3z04anonymousza31774ze3z87_2654:
									if (PAIRP(BgL_l1601z00_2653))
										{	/* SawJvm/funcall.scm 125 */
											{	/* SawJvm/funcall.scm 126 */
												obj_t BgL_fz00_2656;

												BgL_fz00_2656 = CAR(BgL_l1601z00_2653);
												{	/* SawJvm/funcall.scm 126 */
													BgL_variablez00_bglt BgL_gz00_2657;

													BgL_gz00_2657 =
														(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_fz00_2656)))->
														BgL_variablez00);
													{	/* SawJvm/funcall.scm 127 */
														obj_t BgL_arg1798z00_2658;

														{	/* SawJvm/funcall.scm 127 */
															int BgL_arg1799z00_2659;

															{
																BgL_indexedz00_bglt BgL_auxz00_5125;

																{
																	obj_t BgL_auxz00_5126;

																	{	/* SawJvm/funcall.scm 127 */
																		BgL_objectz00_bglt BgL_tmpz00_5127;

																		BgL_tmpz00_5127 =
																			((BgL_objectz00_bglt)
																			((BgL_globalz00_bglt) BgL_gz00_2657));
																		BgL_auxz00_5126 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5127);
																	}
																	BgL_auxz00_5125 =
																		((BgL_indexedz00_bglt) BgL_auxz00_5126);
																}
																BgL_arg1799z00_2659 =
																	(((BgL_indexedz00_bglt)
																		COBJECT(BgL_auxz00_5125))->BgL_indexz00);
															}
															BgL_arg1798z00_2658 =
																BGl_Lze70ze7zzsaw_jvm_funcallz00(
																(long) (BgL_arg1799z00_2659));
														}
														BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_94,
															BgL_arg1798z00_2658);
													}
													{	/* SawJvm/funcall.scm 128 */
														obj_t BgL_arg1806z00_2661;

														BgL_arg1806z00_2661 =
															(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_gz00_2657))))->
																			BgL_valuez00))))->BgL_argsz00);
														{	/* SawJvm/funcall.scm 129 */
															obj_t BgL_zc3z04anonymousza31813ze3z87_4402;

															BgL_zc3z04anonymousza31813ze3z87_4402 =
																MAKE_FX_PROCEDURE
																(BGl_z62zc3z04anonymousza31813ze3ze5zzsaw_jvm_funcallz00,
																(int) (3L), (int) (1L));
															PROCEDURE_SET
																(BgL_zc3z04anonymousza31813ze3z87_4402,
																(int) (0L), ((obj_t) BgL_mez00_94));
															{	/* SawJvm/funcall.scm 128 */
																obj_t BgL_list1807z00_2662;

																{	/* SawJvm/funcall.scm 128 */
																	obj_t BgL_arg1808z00_2663;

																	{	/* SawJvm/funcall.scm 128 */
																		obj_t BgL_arg1812z00_2664;

																		BgL_arg1812z00_2664 =
																			MAKE_YOUNG_PAIR(BgL_arg1806z00_2661,
																			BNIL);
																		BgL_arg1808z00_2663 =
																			MAKE_YOUNG_PAIR(BgL_pz00_97,
																			BgL_arg1812z00_2664);
																	}
																	BgL_list1807z00_2662 =
																		MAKE_YOUNG_PAIR(BgL_talz00_98,
																		BgL_arg1808z00_2663);
																}
																BGl_forzd2eachzd2zz__r4_control_features_6_9z00
																	(BgL_zc3z04anonymousza31813ze3z87_4402,
																	BgL_list1807z00_2662);
													}}}
													{	/* SawJvm/funcall.scm 134 */
														BgL_globalz00_bglt BgL_arg1832z00_2679;

														BgL_arg1832z00_2679 =
															BGl_globalzd2entryzd2zzbackend_cplibz00(
															((BgL_globalz00_bglt) BgL_gz00_2657));
														BGl_callzd2globalzd2zzsaw_jvm_outz00(BgL_mez00_94,
															BgL_arg1832z00_2679);
													}
													BgL_tz00_2683 = BgL_trz00_96;
													{	/* SawJvm/funcall.scm 88 */
														obj_t BgL_arg1836z00_2685;

														if ((BgL_tz00_2683 == CNST_TABLE_REF(5)))
															{	/* SawJvm/funcall.scm 88 */
																BgL_arg1836z00_2685 = CNST_TABLE_REF(6);
															}
														else
															{	/* SawJvm/funcall.scm 88 */
																bool_t BgL_test2355z00_5158;

																{	/* SawJvm/funcall.scm 88 */
																	bool_t BgL__ortest_1193z00_2696;

																	BgL__ortest_1193z00_2696 =
																		(BgL_tz00_2683 == CNST_TABLE_REF(7));
																	if (BgL__ortest_1193z00_2696)
																		{	/* SawJvm/funcall.scm 88 */
																			BgL_test2355z00_5158 =
																				BgL__ortest_1193z00_2696;
																		}
																	else
																		{	/* SawJvm/funcall.scm 88 */
																			bool_t BgL__ortest_1194z00_2697;

																			BgL__ortest_1194z00_2697 =
																				(BgL_tz00_2683 == CNST_TABLE_REF(8));
																			if (BgL__ortest_1194z00_2697)
																				{	/* SawJvm/funcall.scm 88 */
																					BgL_test2355z00_5158 =
																						BgL__ortest_1194z00_2697;
																				}
																			else
																				{	/* SawJvm/funcall.scm 88 */
																					bool_t BgL__ortest_1195z00_2698;

																					BgL__ortest_1195z00_2698 =
																						(BgL_tz00_2683 ==
																						CNST_TABLE_REF(9));
																					if (BgL__ortest_1195z00_2698)
																						{	/* SawJvm/funcall.scm 88 */
																							BgL_test2355z00_5158 =
																								BgL__ortest_1195z00_2698;
																						}
																					else
																						{	/* SawJvm/funcall.scm 88 */
																							bool_t BgL__ortest_1196z00_2699;

																							BgL__ortest_1196z00_2699 =
																								(BgL_tz00_2683 ==
																								CNST_TABLE_REF(10));
																							if (BgL__ortest_1196z00_2699)
																								{	/* SawJvm/funcall.scm 88 */
																									BgL_test2355z00_5158 =
																										BgL__ortest_1196z00_2699;
																								}
																							else
																								{	/* SawJvm/funcall.scm 88 */
																									BgL_test2355z00_5158 =
																										(BgL_tz00_2683 ==
																										CNST_TABLE_REF(11));
																								}
																						}
																				}
																		}
																}
																if (BgL_test2355z00_5158)
																	{	/* SawJvm/funcall.scm 88 */
																		BgL_arg1836z00_2685 = CNST_TABLE_REF(12);
																	}
																else
																	{	/* SawJvm/funcall.scm 88 */
																		if ((BgL_tz00_2683 == CNST_TABLE_REF(13)))
																			{	/* SawJvm/funcall.scm 88 */
																				BgL_arg1836z00_2685 =
																					CNST_TABLE_REF(14);
																			}
																		else
																			{	/* SawJvm/funcall.scm 88 */
																				if (
																					(BgL_tz00_2683 == CNST_TABLE_REF(15)))
																					{	/* SawJvm/funcall.scm 88 */
																						BgL_arg1836z00_2685 =
																							CNST_TABLE_REF(16);
																					}
																				else
																					{	/* SawJvm/funcall.scm 88 */
																						if (
																							(BgL_tz00_2683 ==
																								CNST_TABLE_REF(17)))
																							{	/* SawJvm/funcall.scm 88 */
																								BgL_arg1836z00_2685 =
																									CNST_TABLE_REF(18);
																							}
																						else
																							{	/* SawJvm/funcall.scm 88 */
																								BgL_arg1836z00_2685 =
																									CNST_TABLE_REF(19);
																							}
																					}
																			}
																	}
															}
														BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_94,
															BgL_arg1836z00_2685);
													}
												}
											}
											{
												obj_t BgL_l1601z00_5188;

												BgL_l1601z00_5188 = CDR(BgL_l1601z00_2653);
												BgL_l1601z00_2653 = BgL_l1601z00_5188;
												goto BgL_zc3z04anonymousza31774ze3z87_2654;
											}
										}
									else
										{	/* SawJvm/funcall.scm 125 */
											return ((bool_t) 1);
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* get-labs~2 */
	obj_t BGl_getzd2labsze72z35zzsaw_jvm_funcallz00(long BgL_iz00_2722,
		obj_t BgL_lz00_2723)
	{
		{	/* SawJvm/funcall.scm 103 */
			if (NULLP(BgL_lz00_2723))
				{	/* SawJvm/funcall.scm 100 */
					return BNIL;
				}
			else
				{	/* SawJvm/funcall.scm 100 */
					if ((BgL_iz00_2722 == (long) CINT(CAR(((obj_t) BgL_lz00_2723)))))
						{	/* SawJvm/funcall.scm 102 */
							obj_t BgL_arg1856z00_2728;
							obj_t BgL_arg1857z00_2729;

							BgL_arg1856z00_2728 =
								BGl_Lze70ze7zzsaw_jvm_funcallz00(BgL_iz00_2722);
							{	/* SawJvm/funcall.scm 102 */
								long BgL_arg1858z00_2730;
								obj_t BgL_arg1859z00_2731;

								BgL_arg1858z00_2730 = (BgL_iz00_2722 + 1L);
								BgL_arg1859z00_2731 = CDR(((obj_t) BgL_lz00_2723));
								BgL_arg1857z00_2729 =
									BGl_getzd2labsze72z35zzsaw_jvm_funcallz00(BgL_arg1858z00_2730,
									BgL_arg1859z00_2731);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1856z00_2728, BgL_arg1857z00_2729);
						}
					else
						{	/* SawJvm/funcall.scm 103 */
							obj_t BgL_arg1860z00_2732;

							BgL_arg1860z00_2732 =
								BGl_getzd2labsze72z35zzsaw_jvm_funcallz00(
								(BgL_iz00_2722 + 1L), BgL_lz00_2723);
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1860z00_2732);
						}
				}
		}

	}



/* L~0 */
	obj_t BGl_Lze70ze7zzsaw_jvm_funcallz00(long BgL_nz00_2716)
	{
		{	/* SawJvm/funcall.scm 97 */
			{	/* SawJvm/funcall.scm 97 */
				obj_t BgL_arg1850z00_2718;

				{	/* SawJvm/funcall.scm 97 */
					obj_t BgL_arg1851z00_2719;

					{	/* SawJvm/funcall.scm 97 */

						BgL_arg1851z00_2719 =
							BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
							(BgL_nz00_2716, 10L);
					}
					BgL_arg1850z00_2718 =
						string_append(BGl_string2248z00zzsaw_jvm_funcallz00,
						BgL_arg1851z00_2719);
				}
				return bstring_to_symbol(BgL_arg1850z00_2718);
			}
		}

	}



/* &<@anonymous:1813> */
	obj_t BGl_z62zc3z04anonymousza31813ze3ze5zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4403, obj_t BgL_tz00_4405, obj_t BgL_vz00_4406,
		obj_t BgL_az00_4407)
	{
		{	/* SawJvm/funcall.scm 128 */
			{	/* SawJvm/funcall.scm 129 */
				BgL_jvmz00_bglt BgL_mez00_4404;

				BgL_mez00_4404 =
					((BgL_jvmz00_bglt) PROCEDURE_REF(BgL_envz00_4403, (int) (0L)));
				{
					obj_t BgL_tz00_4503;
					obj_t BgL_vz00_4504;

					{	/* SawJvm/funcall.scm 129 */
						obj_t BgL_taz00_4512;

						{	/* SawJvm/funcall.scm 129 */
							bool_t BgL_test2365z00_5213;

							{	/* SawJvm/funcall.scm 129 */
								obj_t BgL_classz00_4513;

								BgL_classz00_4513 = BGl_localz00zzast_varz00;
								if (BGL_OBJECTP(BgL_az00_4407))
									{	/* SawJvm/funcall.scm 129 */
										BgL_objectz00_bglt BgL_arg1807z00_4514;

										BgL_arg1807z00_4514 = (BgL_objectz00_bglt) (BgL_az00_4407);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawJvm/funcall.scm 129 */
												long BgL_idxz00_4515;

												BgL_idxz00_4515 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4514);
												BgL_test2365z00_5213 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4515 + 2L)) == BgL_classz00_4513);
											}
										else
											{	/* SawJvm/funcall.scm 129 */
												bool_t BgL_res2237z00_4518;

												{	/* SawJvm/funcall.scm 129 */
													obj_t BgL_oclassz00_4519;

													{	/* SawJvm/funcall.scm 129 */
														obj_t BgL_arg1815z00_4520;
														long BgL_arg1816z00_4521;

														BgL_arg1815z00_4520 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawJvm/funcall.scm 129 */
															long BgL_arg1817z00_4522;

															BgL_arg1817z00_4522 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4514);
															BgL_arg1816z00_4521 =
																(BgL_arg1817z00_4522 - OBJECT_TYPE);
														}
														BgL_oclassz00_4519 =
															VECTOR_REF(BgL_arg1815z00_4520,
															BgL_arg1816z00_4521);
													}
													{	/* SawJvm/funcall.scm 129 */
														bool_t BgL__ortest_1115z00_4523;

														BgL__ortest_1115z00_4523 =
															(BgL_classz00_4513 == BgL_oclassz00_4519);
														if (BgL__ortest_1115z00_4523)
															{	/* SawJvm/funcall.scm 129 */
																BgL_res2237z00_4518 = BgL__ortest_1115z00_4523;
															}
														else
															{	/* SawJvm/funcall.scm 129 */
																long BgL_odepthz00_4524;

																{	/* SawJvm/funcall.scm 129 */
																	obj_t BgL_arg1804z00_4525;

																	BgL_arg1804z00_4525 = (BgL_oclassz00_4519);
																	BgL_odepthz00_4524 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4525);
																}
																if ((2L < BgL_odepthz00_4524))
																	{	/* SawJvm/funcall.scm 129 */
																		obj_t BgL_arg1802z00_4526;

																		{	/* SawJvm/funcall.scm 129 */
																			obj_t BgL_arg1803z00_4527;

																			BgL_arg1803z00_4527 =
																				(BgL_oclassz00_4519);
																			BgL_arg1802z00_4526 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4527, 2L);
																		}
																		BgL_res2237z00_4518 =
																			(BgL_arg1802z00_4526 ==
																			BgL_classz00_4513);
																	}
																else
																	{	/* SawJvm/funcall.scm 129 */
																		BgL_res2237z00_4518 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2365z00_5213 = BgL_res2237z00_4518;
											}
									}
								else
									{	/* SawJvm/funcall.scm 129 */
										BgL_test2365z00_5213 = ((bool_t) 0);
									}
							}
							if (BgL_test2365z00_5213)
								{	/* SawJvm/funcall.scm 129 */
									BgL_taz00_4512 =
										((obj_t)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_az00_4407))))->
											BgL_typez00));
								}
							else
								{	/* SawJvm/funcall.scm 129 */
									BgL_taz00_4512 = BgL_az00_4407;
								}
						}
						BgL_tz00_4503 = BgL_tz00_4405;
						BgL_vz00_4504 = BgL_vz00_4406;
						{	/* SawJvm/funcall.scm 106 */
							obj_t BgL_arg1866z00_4505;

							{	/* SawJvm/funcall.scm 106 */
								obj_t BgL_arg1868z00_4506;
								obj_t BgL_arg1869z00_4507;

								{	/* SawJvm/funcall.scm 106 */
									bool_t BgL_test2370z00_5240;

									{	/* SawJvm/funcall.scm 106 */
										bool_t BgL__ortest_1197z00_4508;

										BgL__ortest_1197z00_4508 =
											(BgL_tz00_4503 == CNST_TABLE_REF(7));
										if (BgL__ortest_1197z00_4508)
											{	/* SawJvm/funcall.scm 106 */
												BgL_test2370z00_5240 = BgL__ortest_1197z00_4508;
											}
										else
											{	/* SawJvm/funcall.scm 106 */
												bool_t BgL__ortest_1198z00_4509;

												BgL__ortest_1198z00_4509 =
													(BgL_tz00_4503 == CNST_TABLE_REF(8));
												if (BgL__ortest_1198z00_4509)
													{	/* SawJvm/funcall.scm 106 */
														BgL_test2370z00_5240 = BgL__ortest_1198z00_4509;
													}
												else
													{	/* SawJvm/funcall.scm 106 */
														bool_t BgL__ortest_1199z00_4510;

														BgL__ortest_1199z00_4510 =
															(BgL_tz00_4503 == CNST_TABLE_REF(9));
														if (BgL__ortest_1199z00_4510)
															{	/* SawJvm/funcall.scm 106 */
																BgL_test2370z00_5240 = BgL__ortest_1199z00_4510;
															}
														else
															{	/* SawJvm/funcall.scm 106 */
																bool_t BgL__ortest_1200z00_4511;

																BgL__ortest_1200z00_4511 =
																	(BgL_tz00_4503 == CNST_TABLE_REF(10));
																if (BgL__ortest_1200z00_4511)
																	{	/* SawJvm/funcall.scm 106 */
																		BgL_test2370z00_5240 =
																			BgL__ortest_1200z00_4511;
																	}
																else
																	{	/* SawJvm/funcall.scm 106 */
																		BgL_test2370z00_5240 =
																			(BgL_tz00_4503 == CNST_TABLE_REF(11));
																	}
															}
													}
											}
									}
									if (BgL_test2370z00_5240)
										{	/* SawJvm/funcall.scm 106 */
											BgL_arg1868z00_4506 = CNST_TABLE_REF(24);
										}
									else
										{	/* SawJvm/funcall.scm 106 */
											if ((BgL_tz00_4503 == CNST_TABLE_REF(13)))
												{	/* SawJvm/funcall.scm 106 */
													BgL_arg1868z00_4506 = CNST_TABLE_REF(25);
												}
											else
												{	/* SawJvm/funcall.scm 106 */
													if ((BgL_tz00_4503 == CNST_TABLE_REF(15)))
														{	/* SawJvm/funcall.scm 106 */
															BgL_arg1868z00_4506 = CNST_TABLE_REF(26);
														}
													else
														{	/* SawJvm/funcall.scm 106 */
															if ((BgL_tz00_4503 == CNST_TABLE_REF(17)))
																{	/* SawJvm/funcall.scm 106 */
																	BgL_arg1868z00_4506 = CNST_TABLE_REF(27);
																}
															else
																{	/* SawJvm/funcall.scm 106 */
																	BgL_arg1868z00_4506 = CNST_TABLE_REF(2);
																}
														}
												}
										}
								}
								BgL_arg1869z00_4507 = MAKE_YOUNG_PAIR(BgL_vz00_4504, BNIL);
								BgL_arg1866z00_4505 =
									MAKE_YOUNG_PAIR(BgL_arg1868z00_4506, BgL_arg1869z00_4507);
							}
							BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_4404,
								BgL_arg1866z00_4505);
						}
						{	/* SawJvm/funcall.scm 131 */
							bool_t BgL_test2378z00_5272;

							if (BGl_bigloozd2typezf3z21zztype_typez00(
									((BgL_typez00_bglt) BgL_taz00_4512)))
								{	/* SawJvm/funcall.scm 131 */
									if ((BgL_taz00_4512 == BGl_za2objza2z00zztype_cachez00))
										{	/* SawJvm/funcall.scm 131 */
											BgL_test2378z00_5272 = ((bool_t) 0);
										}
									else
										{	/* SawJvm/funcall.scm 131 */
											BgL_test2378z00_5272 = ((bool_t) 1);
										}
								}
							else
								{	/* SawJvm/funcall.scm 131 */
									BgL_test2378z00_5272 = ((bool_t) 0);
								}
							if (BgL_test2378z00_5272)
								{	/* SawJvm/funcall.scm 132 */
									obj_t BgL_arg1820z00_4528;

									{	/* SawJvm/funcall.scm 132 */
										obj_t BgL_arg1822z00_4529;

										{	/* SawJvm/funcall.scm 132 */
											obj_t BgL_arg1823z00_4530;

											BgL_arg1823z00_4530 =
												BGl_compilezd2typezd2zzsaw_jvm_outz00(BgL_mez00_4404,
												((BgL_typez00_bglt) BgL_taz00_4512));
											BgL_arg1822z00_4529 =
												MAKE_YOUNG_PAIR(BgL_arg1823z00_4530, BNIL);
										}
										BgL_arg1820z00_4528 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(28), BgL_arg1822z00_4529);
									}
									return
										BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_4404,
										BgL_arg1820z00_4528);
								}
							else
								{	/* SawJvm/funcall.scm 131 */
									return BFALSE;
								}
						}
					}
				}
			}
		}

	}



/* module-funcall/apply */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2funcallzf2applyz20zzsaw_jvm_funcallz00(BgL_jvmz00_bglt
		BgL_mez00_99)
	{
		{	/* SawJvm/funcall.scm 141 */
			{	/* SawJvm/funcall.scm 142 */
				obj_t BgL_pz00_2760;

				{	/* SawJvm/funcall.scm 142 */
					obj_t BgL_arg1880z00_2780;

					{	/* SawJvm/funcall.scm 142 */
						obj_t BgL_arg1882z00_2781;

						BgL_arg1882z00_2781 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_mez00_99)))->BgL_functionsz00);
						BgL_arg1880z00_2780 =
							BGl_getzd2procedureszd2zzsaw_proceduresz00(BgL_arg1882z00_2781);
					}
					BgL_pz00_2760 = bgl_reverse_bang(BgL_arg1880z00_2780);
				}
				{	/* SawJvm/funcall.scm 144 */
					long BgL_nz00_2761;

					BgL_nz00_2761 = 0L;
					{
						obj_t BgL_l1603z00_2763;

						BgL_l1603z00_2763 = BgL_pz00_2760;
					BgL_zc3z04anonymousza31874ze3z87_2764:
						if (PAIRP(BgL_l1603z00_2763))
							{	/* SawJvm/funcall.scm 145 */
								{	/* SawJvm/funcall.scm 146 */
									obj_t BgL_varz00_2766;

									BgL_varz00_2766 = CAR(BgL_l1603z00_2763);
									{	/* SawJvm/funcall.scm 146 */
										BgL_indexedz00_bglt BgL_wide1203z00_2769;

										BgL_wide1203z00_2769 =
											((BgL_indexedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_indexedz00_bgl))));
										{	/* SawJvm/funcall.scm 146 */
											obj_t BgL_auxz00_5296;
											BgL_objectz00_bglt BgL_tmpz00_5292;

											BgL_auxz00_5296 = ((obj_t) BgL_wide1203z00_2769);
											BgL_tmpz00_5292 =
												((BgL_objectz00_bglt)
												((BgL_globalz00_bglt)
													((BgL_globalz00_bglt) BgL_varz00_2766)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5292, BgL_auxz00_5296);
										}
										((BgL_objectz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_varz00_2766)));
										{	/* SawJvm/funcall.scm 146 */
											long BgL_arg1876z00_2770;

											BgL_arg1876z00_2770 =
												BGL_CLASS_NUM(BGl_indexedz00zzsaw_jvm_funcallz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_globalz00_bglt)
														((BgL_globalz00_bglt) BgL_varz00_2766))),
												BgL_arg1876z00_2770);
										}
										((BgL_globalz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_varz00_2766)));
									}
									{
										BgL_indexedz00_bglt BgL_auxz00_5310;

										{
											obj_t BgL_auxz00_5311;

											{	/* SawJvm/funcall.scm 146 */
												BgL_objectz00_bglt BgL_tmpz00_5312;

												BgL_tmpz00_5312 =
													((BgL_objectz00_bglt)
													((BgL_globalz00_bglt)
														((BgL_globalz00_bglt) BgL_varz00_2766)));
												BgL_auxz00_5311 = BGL_OBJECT_WIDENING(BgL_tmpz00_5312);
											}
											BgL_auxz00_5310 = ((BgL_indexedz00_bglt) BgL_auxz00_5311);
										}
										((((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_5310))->
												BgL_indexz00) = ((int) (int) (BgL_nz00_2761)), BUNSPEC);
									}
									((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_varz00_2766));
									{	/* SawJvm/funcall.scm 147 */
										BgL_globalz00_bglt BgL_tmp1205z00_2772;

										BgL_tmp1205z00_2772 =
											BGl_globalzd2entryzd2zzbackend_cplibz00(
											((BgL_globalz00_bglt) BgL_varz00_2766));
										{	/* SawJvm/funcall.scm 147 */
											BgL_indexedz00_bglt BgL_wide1207z00_2774;

											BgL_wide1207z00_2774 =
												((BgL_indexedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_indexedz00_bgl))));
											{	/* SawJvm/funcall.scm 147 */
												obj_t BgL_auxz00_5328;
												BgL_objectz00_bglt BgL_tmpz00_5325;

												BgL_auxz00_5328 = ((obj_t) BgL_wide1207z00_2774);
												BgL_tmpz00_5325 =
													((BgL_objectz00_bglt)
													((BgL_globalz00_bglt) BgL_tmp1205z00_2772));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5325,
													BgL_auxz00_5328);
											}
											((BgL_objectz00_bglt)
												((BgL_globalz00_bglt) BgL_tmp1205z00_2772));
											{	/* SawJvm/funcall.scm 147 */
												long BgL_arg1877z00_2775;

												BgL_arg1877z00_2775 =
													BGL_CLASS_NUM(BGl_indexedz00zzsaw_jvm_funcallz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt)
														((BgL_globalz00_bglt) BgL_tmp1205z00_2772)),
													BgL_arg1877z00_2775);
											}
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_tmp1205z00_2772));
										}
										{
											BgL_indexedz00_bglt BgL_auxz00_5339;

											{
												obj_t BgL_auxz00_5340;

												{	/* SawJvm/funcall.scm 147 */
													BgL_objectz00_bglt BgL_tmpz00_5341;

													BgL_tmpz00_5341 =
														((BgL_objectz00_bglt)
														((BgL_globalz00_bglt) BgL_tmp1205z00_2772));
													BgL_auxz00_5340 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5341);
												}
												BgL_auxz00_5339 =
													((BgL_indexedz00_bglt) BgL_auxz00_5340);
											}
											((((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_5339))->
													BgL_indexz00) =
												((int) (int) (BgL_nz00_2761)), BUNSPEC);
										}
										((BgL_globalz00_bglt) BgL_tmp1205z00_2772);
									}
									BgL_nz00_2761 = (BgL_nz00_2761 + 1L);
								}
								{
									obj_t BgL_l1603z00_5350;

									BgL_l1603z00_5350 = CDR(BgL_l1603z00_2763);
									BgL_l1603z00_2763 = BgL_l1603z00_5350;
									goto BgL_zc3z04anonymousza31874ze3z87_2764;
								}
							}
						else
							{	/* SawJvm/funcall.scm 145 */
								((bool_t) 1);
							}
					}
				}
				if (NULLP(BgL_pz00_2760))
					{	/* SawJvm/funcall.scm 150 */
						return BFALSE;
					}
				else
					{	/* SawJvm/funcall.scm 150 */
						BGl_funcalliz00zzsaw_jvm_funcallz00(BgL_mez00_99, 0L,
							BgL_pz00_2760);
						BGl_funcalliz00zzsaw_jvm_funcallz00(BgL_mez00_99, 1L,
							BgL_pz00_2760);
						BGl_funcalliz00zzsaw_jvm_funcallz00(BgL_mez00_99, 2L,
							BgL_pz00_2760);
						BGl_funcalliz00zzsaw_jvm_funcallz00(BgL_mez00_99, 3L,
							BgL_pz00_2760);
						BGl_funcalliz00zzsaw_jvm_funcallz00(BgL_mez00_99, 4L,
							BgL_pz00_2760);
						return BGl_compilezd2applyzd2zzsaw_jvm_funcallz00(BgL_mez00_99,
							BgL_pz00_2760);
					}
			}
		}

	}



/* &module-funcall/apply */
	obj_t BGl_z62modulezd2funcallzf2applyz42zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4412, obj_t BgL_mez00_4413)
	{
		{	/* SawJvm/funcall.scm 141 */
			return
				BGl_modulezd2funcallzf2applyz20zzsaw_jvm_funcallz00(
				((BgL_jvmz00_bglt) BgL_mez00_4413));
		}

	}



/* is-light-procedure? */
	bool_t BGl_iszd2lightzd2procedurezf3zf3zzsaw_jvm_funcallz00(BgL_jvmz00_bglt
		BgL_mez00_103, obj_t BgL_gz00_104)
	{
		{	/* SawJvm/funcall.scm 164 */
			{	/* SawJvm/funcall.scm 165 */
				BgL_valuez00_bglt BgL_vz00_2786;

				BgL_vz00_2786 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_gz00_104))))->BgL_valuez00);
				{	/* SawJvm/funcall.scm 166 */
					bool_t BgL_test2383z00_5365;

					{	/* SawJvm/funcall.scm 166 */
						obj_t BgL_classz00_3949;

						BgL_classz00_3949 = BGl_sfunz00zzast_varz00;
						{	/* SawJvm/funcall.scm 166 */
							BgL_objectz00_bglt BgL_arg1807z00_3951;

							{	/* SawJvm/funcall.scm 166 */
								obj_t BgL_tmpz00_5366;

								BgL_tmpz00_5366 =
									((obj_t) ((BgL_objectz00_bglt) BgL_vz00_2786));
								BgL_arg1807z00_3951 = (BgL_objectz00_bglt) (BgL_tmpz00_5366);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/funcall.scm 166 */
									long BgL_idxz00_3957;

									BgL_idxz00_3957 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3951);
									BgL_test2383z00_5365 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3957 + 3L)) == BgL_classz00_3949);
								}
							else
								{	/* SawJvm/funcall.scm 166 */
									bool_t BgL_res2238z00_3982;

									{	/* SawJvm/funcall.scm 166 */
										obj_t BgL_oclassz00_3965;

										{	/* SawJvm/funcall.scm 166 */
											obj_t BgL_arg1815z00_3973;
											long BgL_arg1816z00_3974;

											BgL_arg1815z00_3973 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/funcall.scm 166 */
												long BgL_arg1817z00_3975;

												BgL_arg1817z00_3975 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3951);
												BgL_arg1816z00_3974 =
													(BgL_arg1817z00_3975 - OBJECT_TYPE);
											}
											BgL_oclassz00_3965 =
												VECTOR_REF(BgL_arg1815z00_3973, BgL_arg1816z00_3974);
										}
										{	/* SawJvm/funcall.scm 166 */
											bool_t BgL__ortest_1115z00_3966;

											BgL__ortest_1115z00_3966 =
												(BgL_classz00_3949 == BgL_oclassz00_3965);
											if (BgL__ortest_1115z00_3966)
												{	/* SawJvm/funcall.scm 166 */
													BgL_res2238z00_3982 = BgL__ortest_1115z00_3966;
												}
											else
												{	/* SawJvm/funcall.scm 166 */
													long BgL_odepthz00_3967;

													{	/* SawJvm/funcall.scm 166 */
														obj_t BgL_arg1804z00_3968;

														BgL_arg1804z00_3968 = (BgL_oclassz00_3965);
														BgL_odepthz00_3967 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3968);
													}
													if ((3L < BgL_odepthz00_3967))
														{	/* SawJvm/funcall.scm 166 */
															obj_t BgL_arg1802z00_3970;

															{	/* SawJvm/funcall.scm 166 */
																obj_t BgL_arg1803z00_3971;

																BgL_arg1803z00_3971 = (BgL_oclassz00_3965);
																BgL_arg1802z00_3970 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3971,
																	3L);
															}
															BgL_res2238z00_3982 =
																(BgL_arg1802z00_3970 == BgL_classz00_3949);
														}
													else
														{	/* SawJvm/funcall.scm 166 */
															BgL_res2238z00_3982 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2383z00_5365 = BgL_res2238z00_3982;
								}
						}
					}
					if (BgL_test2383z00_5365)
						{	/* SawJvm/funcall.scm 166 */
							return
								(
								(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_vz00_2786)))->BgL_strengthz00) ==
								CNST_TABLE_REF(29));
						}
					else
						{	/* SawJvm/funcall.scm 166 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* funcalli */
	obj_t BGl_funcalliz00zzsaw_jvm_funcallz00(BgL_jvmz00_bglt BgL_mez00_105,
		long BgL_iz00_106, obj_t BgL_procsz00_107)
	{
		{	/* SawJvm/funcall.scm 172 */
			{	/* SawJvm/funcall.scm 184 */
				obj_t BgL_needz00_2791;

				if (NULLP(BgL_procsz00_107))
					{	/* SawJvm/funcall.scm 184 */
						BgL_needz00_2791 = BNIL;
					}
				else
					{	/* SawJvm/funcall.scm 184 */
						obj_t BgL_head1608z00_2820;

						{	/* SawJvm/funcall.scm 184 */
							bool_t BgL_arg1904z00_2832;

							{	/* SawJvm/funcall.scm 184 */
								obj_t BgL_arg1906z00_2833;

								BgL_arg1906z00_2833 = CAR(((obj_t) BgL_procsz00_107));
								BgL_arg1904z00_2832 =
									BGl_neededzf3ze70z14zzsaw_jvm_funcallz00(BgL_iz00_106,
									BgL_mez00_105, BgL_arg1906z00_2833);
							}
							BgL_head1608z00_2820 =
								MAKE_YOUNG_PAIR(BBOOL(BgL_arg1904z00_2832), BNIL);
						}
						{	/* SawJvm/funcall.scm 184 */
							obj_t BgL_g1611z00_2821;

							BgL_g1611z00_2821 = CDR(((obj_t) BgL_procsz00_107));
							{
								obj_t BgL_l1606z00_2823;
								obj_t BgL_tail1609z00_2824;

								BgL_l1606z00_2823 = BgL_g1611z00_2821;
								BgL_tail1609z00_2824 = BgL_head1608z00_2820;
							BgL_zc3z04anonymousza31898ze3z87_2825:
								if (NULLP(BgL_l1606z00_2823))
									{	/* SawJvm/funcall.scm 184 */
										BgL_needz00_2791 = BgL_head1608z00_2820;
									}
								else
									{	/* SawJvm/funcall.scm 184 */
										obj_t BgL_newtail1610z00_2827;

										{	/* SawJvm/funcall.scm 184 */
											bool_t BgL_arg1902z00_2829;

											{	/* SawJvm/funcall.scm 184 */
												obj_t BgL_arg1903z00_2830;

												BgL_arg1903z00_2830 = CAR(((obj_t) BgL_l1606z00_2823));
												BgL_arg1902z00_2829 =
													BGl_neededzf3ze70z14zzsaw_jvm_funcallz00(BgL_iz00_106,
													BgL_mez00_105, BgL_arg1903z00_2830);
											}
											BgL_newtail1610z00_2827 =
												MAKE_YOUNG_PAIR(BBOOL(BgL_arg1902z00_2829), BNIL);
										}
										SET_CDR(BgL_tail1609z00_2824, BgL_newtail1610z00_2827);
										{	/* SawJvm/funcall.scm 184 */
											obj_t BgL_arg1901z00_2828;

											BgL_arg1901z00_2828 = CDR(((obj_t) BgL_l1606z00_2823));
											{
												obj_t BgL_tail1609z00_5413;
												obj_t BgL_l1606z00_5412;

												BgL_l1606z00_5412 = BgL_arg1901z00_2828;
												BgL_tail1609z00_5413 = BgL_newtail1610z00_2827;
												BgL_tail1609z00_2824 = BgL_tail1609z00_5413;
												BgL_l1606z00_2823 = BgL_l1606z00_5412;
												goto BgL_zc3z04anonymousza31898ze3z87_2825;
											}
										}
									}
							}
						}
					}
				{	/* SawJvm/funcall.scm 185 */
					bool_t BgL_test2389z00_5414;

					{
						obj_t BgL_l1612z00_2810;

						BgL_l1612z00_2810 = BgL_needz00_2791;
					BgL_zc3z04anonymousza31894ze3z87_2811:
						if (NULLP(BgL_l1612z00_2810))
							{	/* SawJvm/funcall.scm 185 */
								BgL_test2389z00_5414 = ((bool_t) 1);
							}
						else
							{	/* SawJvm/funcall.scm 185 */
								bool_t BgL_test2391z00_5417;

								if (CBOOL(CAR(((obj_t) BgL_l1612z00_2810))))
									{	/* SawJvm/funcall.scm 185 */
										BgL_test2391z00_5417 = ((bool_t) 0);
									}
								else
									{	/* SawJvm/funcall.scm 185 */
										BgL_test2391z00_5417 = ((bool_t) 1);
									}
								if (BgL_test2391z00_5417)
									{
										obj_t BgL_l1612z00_5422;

										BgL_l1612z00_5422 = CDR(((obj_t) BgL_l1612z00_2810));
										BgL_l1612z00_2810 = BgL_l1612z00_5422;
										goto BgL_zc3z04anonymousza31894ze3z87_2811;
									}
								else
									{	/* SawJvm/funcall.scm 185 */
										BgL_test2389z00_5414 = ((bool_t) 0);
									}
							}
					}
					if (BgL_test2389z00_5414)
						{	/* SawJvm/funcall.scm 185 */
							return BFALSE;
						}
					else
						{	/* SawJvm/funcall.scm 186 */
							obj_t BgL_fnamez00_2802;

							{	/* SawJvm/funcall.scm 186 */
								obj_t BgL_arg1893z00_2806;

								{	/* SawJvm/funcall.scm 186 */

									BgL_arg1893z00_2806 =
										BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
										(BgL_iz00_106, 10L);
								}
								BgL_fnamez00_2802 =
									string_append(BGl_string2249z00zzsaw_jvm_funcallz00,
									BgL_arg1893z00_2806);
							}
							{	/* SawJvm/funcall.scm 186 */
								obj_t BgL_pz00_2803;

								{	/* SawJvm/funcall.scm 187 */
									obj_t BgL_arg1892z00_2805;

									BgL_arg1892z00_2805 =
										BGl_nameze70ze7zzsaw_jvm_funcallz00(BgL_iz00_106);
									BgL_pz00_2803 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(30), BgL_arg1892z00_2805);
								}
								{	/* SawJvm/funcall.scm 187 */

									BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_mez00_105,
										bstring_to_symbol(BgL_fnamez00_2802));
									BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_mez00_105,
										BgL_pz00_2803, BNIL);
									BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_105,
										CNST_TABLE_REF(31));
									BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_105,
										CNST_TABLE_REF(3));
									BGl_compilezd2funizd2zzsaw_jvm_funcallz00(BgL_mez00_105,
										BgL_iz00_106, BgL_needz00_2791, BgL_procsz00_107,
										BgL_fnamez00_2802, BgL_pz00_2803);
									return BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_105);
								}
							}
						}
				}
			}
		}

	}



/* needed?~0 */
	bool_t BGl_neededzf3ze70z14zzsaw_jvm_funcallz00(long BgL_iz00_4469,
		BgL_jvmz00_bglt BgL_mez00_4468, obj_t BgL_pz00_2834)
	{
		{	/* SawJvm/funcall.scm 182 */
			{	/* SawJvm/funcall.scm 174 */
				bool_t BgL_test2393z00_5439;

				{	/* SawJvm/funcall.scm 174 */
					BgL_globalz00_bglt BgL_arg1911z00_2845;

					BgL_arg1911z00_2845 =
						BGl_globalzd2entryzd2zzbackend_cplibz00(
						((BgL_globalz00_bglt) BgL_pz00_2834));
					BgL_test2393z00_5439 =
						BGl_iszd2lightzd2procedurezf3zf3zzsaw_jvm_funcallz00(BgL_mez00_4468,
						((obj_t) BgL_arg1911z00_2845));
				}
				if (BgL_test2393z00_5439)
					{	/* SawJvm/funcall.scm 174 */
						return ((bool_t) 0);
					}
				else
					{	/* SawJvm/funcall.scm 175 */
						obj_t BgL_arityz00_2837;

						BgL_arityz00_2837 =
							BGl_globalzd2arityzd2zzbackend_cplibz00(
							((BgL_globalz00_bglt) BgL_pz00_2834));
						if (CBOOL(BgL_arityz00_2837))
							{	/* SawJvm/funcall.scm 177 */
								bool_t BgL__ortest_1210z00_2838;

								if (((long) CINT(BgL_arityz00_2837) >= 0L))
									{	/* SawJvm/funcall.scm 177 */
										if (BGl_keyzd2optzf3z21zzsaw_jvm_funcallz00(BgL_pz00_2834))
											{	/* SawJvm/funcall.scm 178 */
												if (INTEGERP(BgL_arityz00_2837))
													{	/* SawJvm/funcall.scm 179 */
														BgL__ortest_1210z00_2838 =
															((long) CINT(BgL_arityz00_2837) <= BgL_iz00_4469);
													}
												else
													{	/* SawJvm/funcall.scm 179 */
														BgL__ortest_1210z00_2838 =
															BGl_2zc3zd3z10zz__r4_numbers_6_5z00
															(BgL_arityz00_2837, BINT(BgL_iz00_4469));
													}
											}
										else
											{	/* SawJvm/funcall.scm 178 */
												BgL__ortest_1210z00_2838 =
													((long) CINT(BgL_arityz00_2837) == BgL_iz00_4469);
									}}
								else
									{	/* SawJvm/funcall.scm 177 */
										BgL__ortest_1210z00_2838 = ((bool_t) 0);
									}
								if (BgL__ortest_1210z00_2838)
									{	/* SawJvm/funcall.scm 177 */
										return BgL__ortest_1210z00_2838;
									}
								else
									{	/* SawJvm/funcall.scm 177 */
										if (((long) CINT(BgL_arityz00_2837) < 0L))
											{	/* SawJvm/funcall.scm 181 */
												obj_t BgL_b1605z00_2840;

												{	/* SawJvm/funcall.scm 181 */
													obj_t BgL_za72za7_3991;

													BgL_za72za7_3991 = BINT(BgL_iz00_4469);
													{	/* SawJvm/funcall.scm 181 */
														obj_t BgL_tmpz00_3992;

														BgL_tmpz00_3992 = BINT(0L);
														{	/* SawJvm/funcall.scm 181 */
															bool_t BgL_test2400z00_5467;

															{	/* SawJvm/funcall.scm 181 */
																obj_t BgL_tmpz00_5468;

																BgL_tmpz00_5468 = BINT(-1L);
																BgL_test2400z00_5467 =
																	BGL_SUBFX_OV(BgL_tmpz00_5468,
																	BgL_za72za7_3991, BgL_tmpz00_3992);
															}
															if (BgL_test2400z00_5467)
																{	/* SawJvm/funcall.scm 181 */
																	BgL_b1605z00_2840 =
																		bgl_bignum_sub(CNST_TABLE_REF(32),
																		bgl_long_to_bignum(
																			(long) CINT(BgL_za72za7_3991)));
																}
															else
																{	/* SawJvm/funcall.scm 181 */
																	BgL_b1605z00_2840 = BgL_tmpz00_3992;
																}
														}
													}
												}
												{	/* SawJvm/funcall.scm 181 */
													bool_t BgL_test2401z00_5475;

													if (INTEGERP(BgL_arityz00_2837))
														{	/* SawJvm/funcall.scm 181 */
															BgL_test2401z00_5475 =
																INTEGERP(BgL_b1605z00_2840);
														}
													else
														{	/* SawJvm/funcall.scm 181 */
															BgL_test2401z00_5475 = ((bool_t) 0);
														}
													if (BgL_test2401z00_5475)
														{	/* SawJvm/funcall.scm 181 */
															return
																(
																(long) CINT(BgL_arityz00_2837) >=
																(long) CINT(BgL_b1605z00_2840));
														}
													else
														{	/* SawJvm/funcall.scm 181 */
															return
																BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																(BgL_arityz00_2837, BgL_b1605z00_2840);
														}
												}
											}
										else
											{	/* SawJvm/funcall.scm 181 */
												return ((bool_t) 0);
											}
									}
							}
						else
							{	/* SawJvm/funcall.scm 176 */
								return (BgL_iz00_4469 <= 1L);
							}
					}
			}
		}

	}



/* name~0 */
	obj_t BGl_nameze70ze7zzsaw_jvm_funcallz00(long BgL_nz00_2846)
	{
		{	/* SawJvm/funcall.scm 183 */
			if ((BgL_nz00_2846 == 0L))
				{	/* SawJvm/funcall.scm 183 */
					return BNIL;
				}
			else
				{	/* SawJvm/funcall.scm 183 */
					obj_t BgL_arg1914z00_2849;
					obj_t BgL_arg1916z00_2850;

					{	/* SawJvm/funcall.scm 183 */

						{	/* SawJvm/funcall.scm 183 */

							BgL_arg1914z00_2849 = BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
						}
					}
					BgL_arg1916z00_2850 =
						BGl_nameze70ze7zzsaw_jvm_funcallz00((BgL_nz00_2846 - 1L));
					return MAKE_YOUNG_PAIR(BgL_arg1914z00_2849, BgL_arg1916z00_2850);
				}
		}

	}



/* compile-funi */
	obj_t BGl_compilezd2funizd2zzsaw_jvm_funcallz00(BgL_jvmz00_bglt BgL_mez00_108,
		long BgL_iz00_109, obj_t BgL_needz00_110, obj_t BgL_procsz00_111,
		obj_t BgL_fnamez00_112, obj_t BgL_paramsz00_113)
	{
		{	/* SawJvm/funcall.scm 196 */
			{	/* SawJvm/funcall.scm 203 */
				obj_t BgL_labsz00_2857;

				BgL_labsz00_2857 =
					BGl_getzd2labsze71z35zzsaw_jvm_funcallz00(0L, BgL_needz00_110,
					BgL_procsz00_111);
				{	/* SawJvm/funcall.scm 204 */
					obj_t BgL_arg1918z00_2858;

					{	/* SawJvm/funcall.scm 204 */
						obj_t BgL_arg1919z00_2859;

						{	/* SawJvm/funcall.scm 204 */
							obj_t BgL_arg1920z00_2860;

							BgL_arg1920z00_2860 =
								MAKE_YOUNG_PAIR(BINT(0L),
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_labsz00_2857, BNIL));
							BgL_arg1919z00_2859 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg1920z00_2860);
						}
						BgL_arg1918z00_2858 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(21), BgL_arg1919z00_2859);
					}
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_108, BgL_arg1918z00_2858);
				}
				BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_108, CNST_TABLE_REF(20));
				{
					obj_t BgL_l1615z00_2863;

					BgL_l1615z00_2863 = BgL_paramsz00_113;
				BgL_zc3z04anonymousza31924ze3z87_2864:
					if (PAIRP(BgL_l1615z00_2863))
						{	/* SawJvm/funcall.scm 206 */
							{	/* SawJvm/funcall.scm 206 */
								obj_t BgL_vz00_2866;

								BgL_vz00_2866 = CAR(BgL_l1615z00_2863);
								{	/* SawJvm/funcall.scm 206 */
									obj_t BgL_arg1926z00_2867;

									{	/* SawJvm/funcall.scm 206 */
										obj_t BgL_arg1927z00_2868;

										BgL_arg1927z00_2868 = MAKE_YOUNG_PAIR(BgL_vz00_2866, BNIL);
										BgL_arg1926z00_2867 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1927z00_2868);
									}
									BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_108,
										BgL_arg1926z00_2867);
								}
							}
							{
								obj_t BgL_l1615z00_5508;

								BgL_l1615z00_5508 = CDR(BgL_l1615z00_2863);
								BgL_l1615z00_2863 = BgL_l1615z00_5508;
								goto BgL_zc3z04anonymousza31924ze3z87_2864;
							}
						}
					else
						{	/* SawJvm/funcall.scm 206 */
							((bool_t) 1);
						}
				}
				{	/* SawJvm/funcall.scm 207 */
					obj_t BgL_arg1929z00_2871;

					{	/* SawJvm/funcall.scm 207 */
						obj_t BgL_arg1930z00_2872;

						BgL_arg1930z00_2872 =
							MAKE_YOUNG_PAIR(bstring_to_symbol(string_append
								(BGl_string2250z00zzsaw_jvm_funcallz00, BgL_fnamez00_112)),
							BNIL);
						BgL_arg1929z00_2871 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(33), BgL_arg1930z00_2872);
					}
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_108, BgL_arg1929z00_2871);
				}
				BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_108, CNST_TABLE_REF(19));
				{	/* SawJvm/funcall.scm 210 */
					obj_t BgL_zc3z04anonymousza31937ze3z87_4414;

					BgL_zc3z04anonymousza31937ze3z87_4414 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_jvm_funcallz00,
						(int) (3L), (int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31937ze3z87_4414, (int) (0L),
						((obj_t) BgL_mez00_108));
					PROCEDURE_SET(BgL_zc3z04anonymousza31937ze3z87_4414, (int) (1L),
						BINT(BgL_iz00_109));
					PROCEDURE_SET(BgL_zc3z04anonymousza31937ze3z87_4414, (int) (2L),
						BgL_paramsz00_113);
					{	/* SawJvm/funcall.scm 209 */
						obj_t BgL_list1934z00_2876;

						{	/* SawJvm/funcall.scm 209 */
							obj_t BgL_arg1935z00_2877;

							{	/* SawJvm/funcall.scm 209 */
								obj_t BgL_arg1936z00_2878;

								BgL_arg1936z00_2878 = MAKE_YOUNG_PAIR(BgL_procsz00_111, BNIL);
								BgL_arg1935z00_2877 =
									MAKE_YOUNG_PAIR(BgL_labsz00_2857, BgL_arg1936z00_2878);
							}
							BgL_list1934z00_2876 =
								MAKE_YOUNG_PAIR(BgL_needz00_110, BgL_arg1935z00_2877);
						}
						return
							BGl_forzd2eachzd2zz__r4_control_features_6_9z00
							(BgL_zc3z04anonymousza31937ze3z87_4414, BgL_list1934z00_2876);
					}
				}
			}
		}

	}



/* get-labs~1 */
	obj_t BGl_getzd2labsze71z35zzsaw_jvm_funcallz00(long BgL_iz00_2890,
		obj_t BgL_nsz00_2891, obj_t BgL_psz00_2892)
	{
		{	/* SawJvm/funcall.scm 202 */
			{
				long BgL_nz00_2884;

				if (NULLP(BgL_nsz00_2891))
					{	/* SawJvm/funcall.scm 199 */
						return BNIL;
					}
				else
					{	/* SawJvm/funcall.scm 201 */
						obj_t BgL_arg1943z00_2895;
						obj_t BgL_arg1944z00_2896;

						if (CBOOL(CAR(((obj_t) BgL_nsz00_2891))))
							{	/* SawJvm/funcall.scm 201 */
								BgL_nz00_2884 = BgL_iz00_2890;
								{	/* SawJvm/funcall.scm 197 */
									obj_t BgL_arg1939z00_2886;

									{	/* SawJvm/funcall.scm 197 */
										obj_t BgL_arg1940z00_2887;

										{	/* SawJvm/funcall.scm 197 */

											BgL_arg1940z00_2887 =
												BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
												(BgL_nz00_2884, 10L);
										}
										BgL_arg1939z00_2886 =
											string_append(BGl_string2248z00zzsaw_jvm_funcallz00,
											BgL_arg1940z00_2887);
									}
									BgL_arg1943z00_2895 = bstring_to_symbol(BgL_arg1939z00_2886);
								}
							}
						else
							{	/* SawJvm/funcall.scm 201 */
								BgL_arg1943z00_2895 = CNST_TABLE_REF(20);
							}
						{	/* SawJvm/funcall.scm 202 */
							long BgL_arg1946z00_2898;
							obj_t BgL_arg1947z00_2899;
							obj_t BgL_arg1948z00_2900;

							BgL_arg1946z00_2898 = (BgL_iz00_2890 + 1L);
							BgL_arg1947z00_2899 = CDR(((obj_t) BgL_nsz00_2891));
							BgL_arg1948z00_2900 = CDR(((obj_t) BgL_psz00_2892));
							BgL_arg1944z00_2896 =
								BGl_getzd2labsze71z35zzsaw_jvm_funcallz00(BgL_arg1946z00_2898,
								BgL_arg1947z00_2899, BgL_arg1948z00_2900);
						}
						return MAKE_YOUNG_PAIR(BgL_arg1943z00_2895, BgL_arg1944z00_2896);
					}
			}
		}

	}



/* &<@anonymous:1937> */
	obj_t BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4415, obj_t BgL_nzf3zf3_4419, obj_t BgL_labz00_4420,
		obj_t BgL_pz00_4421)
	{
		{	/* SawJvm/funcall.scm 209 */
			{	/* SawJvm/funcall.scm 210 */
				BgL_jvmz00_bglt BgL_mez00_4416;
				long BgL_iz00_4417;
				obj_t BgL_paramsz00_4418;

				BgL_mez00_4416 =
					((BgL_jvmz00_bglt) PROCEDURE_REF(BgL_envz00_4415, (int) (0L)));
				BgL_iz00_4417 = (long) CINT(PROCEDURE_REF(BgL_envz00_4415, (int) (1L)));
				BgL_paramsz00_4418 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4415, (int) (2L)));
				if (CBOOL(BgL_nzf3zf3_4419))
					{	/* SawJvm/funcall.scm 210 */
						return
							BGl_compilezd2forzd2funcalliz00zzsaw_jvm_funcallz00(
							((obj_t) BgL_mez00_4416),
							BINT(BgL_iz00_4417), BgL_labz00_4420, BgL_pz00_4421,
							BgL_paramsz00_4418);
					}
				else
					{	/* SawJvm/funcall.scm 210 */
						return BFALSE;
					}
			}
		}

	}



/* compile-for-funcalli */
	obj_t BGl_compilezd2forzd2funcalliz00zzsaw_jvm_funcallz00(obj_t BgL_mez00_114,
		obj_t BgL_iz00_115, obj_t BgL_labz00_116, obj_t BgL_pz00_117,
		obj_t BgL_paramsz00_118)
	{
		{	/* SawJvm/funcall.scm 216 */
			{	/* SawJvm/funcall.scm 217 */
				obj_t BgL_arityz00_2903;

				BgL_arityz00_2903 =
					BGl_globalzd2arityzd2zzbackend_cplibz00(
					((BgL_globalz00_bglt) BgL_pz00_117));
				{	/* SawJvm/funcall.scm 217 */
					BgL_globalz00_bglt BgL_entryz00_2904;

					BgL_entryz00_2904 =
						BGl_globalzd2entryzd2zzbackend_cplibz00(
						((BgL_globalz00_bglt) BgL_pz00_117));
					{	/* SawJvm/funcall.scm 218 */

						{
							obj_t BgL_nz00_3028;
							obj_t BgL_iz00_3022;

							BGl_labelz00zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_114), BgL_labz00_116);
							if ((BgL_arityz00_2903 == BFALSE))
								{	/* SawJvm/funcall.scm 228 */
									{
										obj_t BgL_l1617z00_2908;

										BgL_l1617z00_2908 = BgL_paramsz00_118;
									BgL_zc3z04anonymousza31949ze3z87_2909:
										if (PAIRP(BgL_l1617z00_2908))
											{	/* SawJvm/funcall.scm 229 */
												{	/* SawJvm/funcall.scm 229 */
													obj_t BgL_vz00_2911;

													BgL_vz00_2911 = CAR(BgL_l1617z00_2908);
													{	/* SawJvm/funcall.scm 229 */
														obj_t BgL_arg1951z00_2912;

														{	/* SawJvm/funcall.scm 229 */
															obj_t BgL_arg1952z00_2913;

															BgL_arg1952z00_2913 =
																MAKE_YOUNG_PAIR(BgL_vz00_2911, BNIL);
															BgL_arg1951z00_2912 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																BgL_arg1952z00_2913);
														}
														BGl_codez12z12zzsaw_jvm_outz00(
															((BgL_jvmz00_bglt) BgL_mez00_114),
															BgL_arg1951z00_2912);
													}
												}
												{
													obj_t BgL_l1617z00_5580;

													BgL_l1617z00_5580 = CDR(BgL_l1617z00_2908);
													BgL_l1617z00_2908 = BgL_l1617z00_5580;
													goto BgL_zc3z04anonymousza31949ze3z87_2909;
												}
											}
										else
											{	/* SawJvm/funcall.scm 229 */
												((bool_t) 1);
											}
									}
									if (((long) CINT(BgL_iz00_115) == 0L))
										{	/* SawJvm/funcall.scm 230 */
											BGl_codez12z12zzsaw_jvm_outz00(
												((BgL_jvmz00_bglt) BgL_mez00_114), CNST_TABLE_REF(35));
											{	/* SawJvm/funcall.scm 232 */
												obj_t BgL_arg1955z00_2917;

												{	/* SawJvm/funcall.scm 232 */
													obj_t BgL_arg1956z00_2918;

													{	/* SawJvm/funcall.scm 232 */
														obj_t BgL_arg1957z00_2919;

														BgL_arg1957z00_2919 =
															BGl_declarezd2globalzd2zzsaw_jvm_outz00(
															((BgL_jvmz00_bglt) BgL_mez00_114),
															((BgL_globalz00_bglt) BgL_pz00_117));
														BgL_arg1956z00_2918 =
															MAKE_YOUNG_PAIR(BgL_arg1957z00_2919, BNIL);
													}
													BgL_arg1955z00_2917 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
														BgL_arg1956z00_2918);
												}
												BGl_codez12z12zzsaw_jvm_outz00(
													((BgL_jvmz00_bglt) BgL_mez00_114),
													BgL_arg1955z00_2917);
										}}
									else
										{	/* SawJvm/funcall.scm 233 */
											obj_t BgL_arg1958z00_2920;

											{	/* SawJvm/funcall.scm 233 */
												obj_t BgL_arg1959z00_2921;

												{	/* SawJvm/funcall.scm 233 */
													obj_t BgL_arg1960z00_2922;

													BgL_arg1960z00_2922 =
														BGl_declarezd2globalzd2zzsaw_jvm_outz00(
														((BgL_jvmz00_bglt) BgL_mez00_114),
														((BgL_globalz00_bglt) BgL_pz00_117));
													BgL_arg1959z00_2921 =
														MAKE_YOUNG_PAIR(BgL_arg1960z00_2922, BNIL);
												}
												BgL_arg1958z00_2920 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(37),
													BgL_arg1959z00_2921);
											}
											BGl_codez12z12zzsaw_jvm_outz00(
												((BgL_jvmz00_bglt) BgL_mez00_114), BgL_arg1958z00_2920);
										}
								}
							else
								{	/* SawJvm/funcall.scm 228 */
									if (((long) CINT(BgL_arityz00_2903) >= 0L))
										{	/* SawJvm/funcall.scm 234 */
											if (BGl_keyzd2optzf3z21zzsaw_jvm_funcallz00(BgL_pz00_117))
												{	/* SawJvm/funcall.scm 235 */
													{
														obj_t BgL_l1619z00_2927;

														BgL_l1619z00_2927 = BgL_paramsz00_118;
													BgL_zc3z04anonymousza31963ze3z87_2928:
														if (PAIRP(BgL_l1619z00_2927))
															{	/* SawJvm/funcall.scm 237 */
																{	/* SawJvm/funcall.scm 237 */
																	obj_t BgL_vz00_2930;

																	BgL_vz00_2930 = CAR(BgL_l1619z00_2927);
																	{	/* SawJvm/funcall.scm 237 */
																		obj_t BgL_arg1965z00_2931;

																		{	/* SawJvm/funcall.scm 237 */
																			obj_t BgL_arg1966z00_2932;

																			BgL_arg1966z00_2932 =
																				MAKE_YOUNG_PAIR(BgL_vz00_2930, BNIL);
																			BgL_arg1965z00_2931 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																				BgL_arg1966z00_2932);
																		}
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_114),
																			BgL_arg1965z00_2931);
																	}
																}
																{
																	obj_t BgL_l1619z00_5617;

																	BgL_l1619z00_5617 = CDR(BgL_l1619z00_2927);
																	BgL_l1619z00_2927 = BgL_l1619z00_5617;
																	goto BgL_zc3z04anonymousza31963ze3z87_2928;
																}
															}
														else
															{	/* SawJvm/funcall.scm 237 */
																((bool_t) 1);
															}
													}
													{	/* SawJvm/funcall.scm 238 */
														obj_t BgL_arg1968z00_2935;

														{	/* SawJvm/funcall.scm 238 */
															obj_t BgL_arg1969z00_2936;

															{	/* SawJvm/funcall.scm 238 */
																obj_t BgL_tmpz00_5619;

																BgL_iz00_3022 = BgL_iz00_115;
																{	/* SawJvm/funcall.scm 220 */
																	obj_t BgL_arg2017z00_3024;

																	{	/* SawJvm/funcall.scm 220 */
																		obj_t BgL_arg2018z00_3025;

																		{	/* SawJvm/funcall.scm 220 */

																			BgL_arg2018z00_3025 =
																				BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																				((long) CINT(BgL_iz00_3022), 10L);
																		}
																		BgL_arg2017z00_3024 =
																			string_append
																			(BGl_string2251z00zzsaw_jvm_funcallz00,
																			BgL_arg2018z00_3025);
																	}
																	BgL_tmpz00_5619 =
																		bstring_to_symbol(BgL_arg2017z00_3024);
																}
																BgL_arg1969z00_2936 =
																	MAKE_YOUNG_PAIR(BgL_tmpz00_5619, BNIL);
															}
															BgL_arg1968z00_2935 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(38),
																BgL_arg1969z00_2936);
														}
														BGl_codez12z12zzsaw_jvm_outz00(
															((BgL_jvmz00_bglt) BgL_mez00_114),
															BgL_arg1968z00_2935);
													}
													BGl_callzd2globalzd2zzsaw_jvm_outz00(
														((BgL_jvmz00_bglt) BgL_mez00_114),
														BgL_entryz00_2904);
												}
											else
												{	/* SawJvm/funcall.scm 240 */
													obj_t BgL_argsz00_2938;

													BgL_argsz00_2938 =
														(((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_entryz00_2904)))->
																		BgL_valuez00))))->BgL_argsz00);
													{	/* SawJvm/funcall.scm 240 */
														obj_t BgL_typesz00_2939;

														if (NULLP(BgL_argsz00_2938))
															{	/* SawJvm/funcall.scm 241 */
																BgL_typesz00_2939 = BNIL;
															}
														else
															{	/* SawJvm/funcall.scm 241 */
																obj_t BgL_head1623z00_2957;

																{	/* SawJvm/funcall.scm 241 */
																	BgL_typez00_bglt BgL_arg1986z00_2969;

																	BgL_arg1986z00_2969 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						CAR(
																							((obj_t) BgL_argsz00_2938))))))->
																		BgL_typez00);
																	BgL_head1623z00_2957 =
																		MAKE_YOUNG_PAIR(((obj_t)
																			BgL_arg1986z00_2969), BNIL);
																}
																{	/* SawJvm/funcall.scm 241 */
																	obj_t BgL_g1626z00_2958;

																	BgL_g1626z00_2958 =
																		CDR(((obj_t) BgL_argsz00_2938));
																	{
																		obj_t BgL_l1621z00_2960;
																		obj_t BgL_tail1624z00_2961;

																		BgL_l1621z00_2960 = BgL_g1626z00_2958;
																		BgL_tail1624z00_2961 = BgL_head1623z00_2957;
																	BgL_zc3z04anonymousza31981ze3z87_2962:
																		if (NULLP(BgL_l1621z00_2960))
																			{	/* SawJvm/funcall.scm 241 */
																				BgL_typesz00_2939 =
																					BgL_head1623z00_2957;
																			}
																		else
																			{	/* SawJvm/funcall.scm 241 */
																				obj_t BgL_newtail1625z00_2964;

																				{	/* SawJvm/funcall.scm 241 */
																					BgL_typez00_bglt BgL_arg1984z00_2966;

																					BgL_arg1984z00_2966 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										CAR(
																											((obj_t)
																												BgL_l1621z00_2960))))))->
																						BgL_typez00);
																					BgL_newtail1625z00_2964 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_arg1984z00_2966), BNIL);
																				}
																				SET_CDR(BgL_tail1624z00_2961,
																					BgL_newtail1625z00_2964);
																				{	/* SawJvm/funcall.scm 241 */
																					obj_t BgL_arg1983z00_2965;

																					BgL_arg1983z00_2965 =
																						CDR(((obj_t) BgL_l1621z00_2960));
																					{
																						obj_t BgL_tail1624z00_5659;
																						obj_t BgL_l1621z00_5658;

																						BgL_l1621z00_5658 =
																							BgL_arg1983z00_2965;
																						BgL_tail1624z00_5659 =
																							BgL_newtail1625z00_2964;
																						BgL_tail1624z00_2961 =
																							BgL_tail1624z00_5659;
																						BgL_l1621z00_2960 =
																							BgL_l1621z00_5658;
																						goto
																							BgL_zc3z04anonymousza31981ze3z87_2962;
																					}
																				}
																			}
																	}
																}
															}
														{	/* SawJvm/funcall.scm 241 */

															{
																obj_t BgL_ll1627z00_2941;
																obj_t BgL_ll1628z00_2942;

																BgL_ll1627z00_2941 = BgL_paramsz00_118;
																BgL_ll1628z00_2942 = BgL_typesz00_2939;
															BgL_zc3z04anonymousza31971ze3z87_2943:
																if (NULLP(BgL_ll1627z00_2941))
																	{	/* SawJvm/funcall.scm 242 */
																		((bool_t) 1);
																	}
																else
																	{	/* SawJvm/funcall.scm 242 */
																		{	/* SawJvm/funcall.scm 243 */
																			obj_t BgL_vz00_2945;
																			obj_t BgL_tz00_2946;

																			BgL_vz00_2945 =
																				CAR(((obj_t) BgL_ll1627z00_2941));
																			BgL_tz00_2946 =
																				CAR(((obj_t) BgL_ll1628z00_2942));
																			{	/* SawJvm/funcall.scm 243 */
																				obj_t BgL_arg1973z00_2947;

																				{	/* SawJvm/funcall.scm 243 */
																					obj_t BgL_arg1974z00_2948;

																					BgL_arg1974z00_2948 =
																						MAKE_YOUNG_PAIR(BgL_vz00_2945,
																						BNIL);
																					BgL_arg1973z00_2947 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																						BgL_arg1974z00_2948);
																				}
																				BGl_codez12z12zzsaw_jvm_outz00(
																					((BgL_jvmz00_bglt) BgL_mez00_114),
																					BgL_arg1973z00_2947);
																			}
																			if (
																				(BgL_tz00_2946 ==
																					BGl_za2objza2z00zztype_cachez00))
																				{	/* SawJvm/funcall.scm 244 */
																					BFALSE;
																				}
																			else
																				{	/* SawJvm/funcall.scm 245 */
																					obj_t BgL_arg1975z00_2949;

																					{	/* SawJvm/funcall.scm 245 */
																						obj_t BgL_arg1976z00_2950;

																						{	/* SawJvm/funcall.scm 245 */
																							obj_t BgL_arg1977z00_2951;

																							BgL_arg1977z00_2951 =
																								BGl_compilezd2typezd2zzsaw_jvm_outz00
																								(((BgL_jvmz00_bglt)
																									BgL_mez00_114),
																								((BgL_typez00_bglt)
																									BgL_tz00_2946));
																							BgL_arg1976z00_2950 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1977z00_2951, BNIL);
																						}
																						BgL_arg1975z00_2949 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(28), BgL_arg1976z00_2950);
																					}
																					BGl_codez12z12zzsaw_jvm_outz00(
																						((BgL_jvmz00_bglt) BgL_mez00_114),
																						BgL_arg1975z00_2949);
																				}
																		}
																		{	/* SawJvm/funcall.scm 242 */
																			obj_t BgL_arg1978z00_2952;
																			obj_t BgL_arg1979z00_2953;

																			BgL_arg1978z00_2952 =
																				CDR(((obj_t) BgL_ll1627z00_2941));
																			BgL_arg1979z00_2953 =
																				CDR(((obj_t) BgL_ll1628z00_2942));
																			{
																				obj_t BgL_ll1628z00_5686;
																				obj_t BgL_ll1627z00_5685;

																				BgL_ll1627z00_5685 =
																					BgL_arg1978z00_2952;
																				BgL_ll1628z00_5686 =
																					BgL_arg1979z00_2953;
																				BgL_ll1628z00_2942 = BgL_ll1628z00_5686;
																				BgL_ll1627z00_2941 = BgL_ll1627z00_5685;
																				goto
																					BgL_zc3z04anonymousza31971ze3z87_2943;
																			}
																		}
																	}
															}
															BGl_callzd2globalzd2zzsaw_jvm_outz00(
																((BgL_jvmz00_bglt) BgL_mez00_114),
																BgL_entryz00_2904);
														}
													}
												}
										}
									else
										{	/* SawJvm/funcall.scm 234 */
											{	/* SawJvm/funcall.scm 249 */
												obj_t BgL_argsz00_2972;

												BgL_argsz00_2972 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_entryz00_2904)))->BgL_valuez00))))->
													BgL_argsz00);
												{	/* SawJvm/funcall.scm 249 */
													obj_t BgL_typesz00_2973;

													if (NULLP(BgL_argsz00_2972))
														{	/* SawJvm/funcall.scm 250 */
															BgL_typesz00_2973 = BNIL;
														}
													else
														{	/* SawJvm/funcall.scm 250 */
															obj_t BgL_head1632z00_3004;

															{	/* SawJvm/funcall.scm 250 */
																BgL_typez00_bglt BgL_arg2011z00_3016;

																BgL_arg2011z00_3016 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					CAR(
																						((obj_t) BgL_argsz00_2972))))))->
																	BgL_typez00);
																BgL_head1632z00_3004 =
																	MAKE_YOUNG_PAIR(((obj_t) BgL_arg2011z00_3016),
																	BNIL);
															}
															{	/* SawJvm/funcall.scm 250 */
																obj_t BgL_g1635z00_3005;

																BgL_g1635z00_3005 =
																	CDR(((obj_t) BgL_argsz00_2972));
																{
																	obj_t BgL_l1630z00_3007;
																	obj_t BgL_tail1633z00_3008;

																	BgL_l1630z00_3007 = BgL_g1635z00_3005;
																	BgL_tail1633z00_3008 = BgL_head1632z00_3004;
																BgL_zc3z04anonymousza32006ze3z87_3009:
																	if (NULLP(BgL_l1630z00_3007))
																		{	/* SawJvm/funcall.scm 250 */
																			BgL_typesz00_2973 = BgL_head1632z00_3004;
																		}
																	else
																		{	/* SawJvm/funcall.scm 250 */
																			obj_t BgL_newtail1634z00_3011;

																			{	/* SawJvm/funcall.scm 250 */
																				BgL_typez00_bglt BgL_arg2009z00_3013;

																				BgL_arg2009z00_3013 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_localz00_bglt)
																									CAR(
																										((obj_t)
																											BgL_l1630z00_3007))))))->
																					BgL_typez00);
																				BgL_newtail1634z00_3011 =
																					MAKE_YOUNG_PAIR(((obj_t)
																						BgL_arg2009z00_3013), BNIL);
																			}
																			SET_CDR(BgL_tail1633z00_3008,
																				BgL_newtail1634z00_3011);
																			{	/* SawJvm/funcall.scm 250 */
																				obj_t BgL_arg2008z00_3012;

																				BgL_arg2008z00_3012 =
																					CDR(((obj_t) BgL_l1630z00_3007));
																				{
																					obj_t BgL_tail1633z00_5717;
																					obj_t BgL_l1630z00_5716;

																					BgL_l1630z00_5716 =
																						BgL_arg2008z00_3012;
																					BgL_tail1633z00_5717 =
																						BgL_newtail1634z00_3011;
																					BgL_tail1633z00_3008 =
																						BgL_tail1633z00_5717;
																					BgL_l1630z00_3007 = BgL_l1630z00_5716;
																					goto
																						BgL_zc3z04anonymousza32006ze3z87_3009;
																				}
																			}
																		}
																}
															}
														}
													{	/* SawJvm/funcall.scm 250 */

														{	/* SawJvm/funcall.scm 251 */
															obj_t BgL_g1214z00_2974;

															BgL_g1214z00_2974 =
																BGl_zd2zd2zz__r4_numbers_6_5z00
																(BgL_arityz00_2903, BNIL);
															{
																obj_t BgL_iz00_2976;
																obj_t BgL_paramsz00_2977;
																obj_t BgL_typesz00_2978;

																BgL_iz00_2976 = BgL_g1214z00_2974;
																BgL_paramsz00_2977 = BgL_paramsz00_118;
																BgL_typesz00_2978 = BgL_typesz00_2973;
															BgL_zc3z04anonymousza31989ze3z87_2979:
																if (((long) CINT(BgL_iz00_2976) == 0L))
																	{
																		obj_t BgL_l1636z00_2982;

																		BgL_l1636z00_2982 = BgL_paramsz00_2977;
																	BgL_zc3z04anonymousza31991ze3z87_2983:
																		if (PAIRP(BgL_l1636z00_2982))
																			{	/* SawJvm/funcall.scm 255 */
																				{	/* SawJvm/funcall.scm 255 */
																					obj_t BgL_vz00_2985;

																					BgL_vz00_2985 =
																						CAR(BgL_l1636z00_2982);
																					{	/* SawJvm/funcall.scm 255 */
																						obj_t BgL_arg1993z00_2986;

																						{	/* SawJvm/funcall.scm 255 */
																							obj_t BgL_arg1994z00_2987;

																							BgL_arg1994z00_2987 =
																								MAKE_YOUNG_PAIR(BgL_vz00_2985,
																								BNIL);
																							BgL_arg1993z00_2986 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(2), BgL_arg1994z00_2987);
																						}
																						BGl_codez12z12zzsaw_jvm_outz00(
																							((BgL_jvmz00_bglt) BgL_mez00_114),
																							BgL_arg1993z00_2986);
																					}
																				}
																				{
																					obj_t BgL_l1636z00_5730;

																					BgL_l1636z00_5730 =
																						CDR(BgL_l1636z00_2982);
																					BgL_l1636z00_2982 = BgL_l1636z00_5730;
																					goto
																						BgL_zc3z04anonymousza31991ze3z87_2983;
																				}
																			}
																		else
																			{	/* SawJvm/funcall.scm 255 */
																				((bool_t) 1);
																			}
																	}
																else
																	{	/* SawJvm/funcall.scm 256 */
																		obj_t BgL_vz00_2990;
																		obj_t BgL_tz00_2991;

																		BgL_vz00_2990 =
																			CAR(((obj_t) BgL_paramsz00_2977));
																		BgL_tz00_2991 =
																			CAR(((obj_t) BgL_typesz00_2978));
																		{	/* SawJvm/funcall.scm 258 */
																			obj_t BgL_arg1996z00_2992;

																			{	/* SawJvm/funcall.scm 258 */
																				obj_t BgL_arg1997z00_2993;

																				BgL_arg1997z00_2993 =
																					MAKE_YOUNG_PAIR(BgL_vz00_2990, BNIL);
																				BgL_arg1996z00_2992 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																					BgL_arg1997z00_2993);
																			}
																			BGl_codez12z12zzsaw_jvm_outz00(
																				((BgL_jvmz00_bglt) BgL_mez00_114),
																				BgL_arg1996z00_2992);
																		}
																		if (
																			(BgL_tz00_2991 ==
																				BGl_za2objza2z00zztype_cachez00))
																			{	/* SawJvm/funcall.scm 259 */
																				BFALSE;
																			}
																		else
																			{	/* SawJvm/funcall.scm 260 */
																				obj_t BgL_arg1998z00_2994;

																				{	/* SawJvm/funcall.scm 260 */
																					obj_t BgL_arg1999z00_2995;

																					{	/* SawJvm/funcall.scm 260 */
																						obj_t BgL_arg2000z00_2996;

																						BgL_arg2000z00_2996 =
																							BGl_compilezd2typezd2zzsaw_jvm_outz00
																							(((BgL_jvmz00_bglt)
																								BgL_mez00_114),
																							((BgL_typez00_bglt)
																								BgL_tz00_2991));
																						BgL_arg1999z00_2995 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2000z00_2996, BNIL);
																					}
																					BgL_arg1998z00_2994 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(28),
																						BgL_arg1999z00_2995);
																				}
																				BGl_codez12z12zzsaw_jvm_outz00(
																					((BgL_jvmz00_bglt) BgL_mez00_114),
																					BgL_arg1998z00_2994);
																			}
																		{	/* SawJvm/funcall.scm 261 */
																			long BgL_arg2001z00_2997;
																			obj_t BgL_arg2002z00_2998;
																			obj_t BgL_arg2003z00_2999;

																			BgL_arg2001z00_2997 =
																				((long) CINT(BgL_iz00_2976) - 1L);
																			BgL_arg2002z00_2998 =
																				CDR(((obj_t) BgL_paramsz00_2977));
																			BgL_arg2003z00_2999 =
																				CDR(((obj_t) BgL_typesz00_2978));
																			{
																				obj_t BgL_typesz00_5760;
																				obj_t BgL_paramsz00_5759;
																				obj_t BgL_iz00_5757;

																				BgL_iz00_5757 =
																					BINT(BgL_arg2001z00_2997);
																				BgL_paramsz00_5759 =
																					BgL_arg2002z00_2998;
																				BgL_typesz00_5760 = BgL_arg2003z00_2999;
																				BgL_typesz00_2978 = BgL_typesz00_5760;
																				BgL_paramsz00_2977 = BgL_paramsz00_5759;
																				BgL_iz00_2976 = BgL_iz00_5757;
																				goto
																					BgL_zc3z04anonymousza31989ze3z87_2979;
																			}
																		}
																	}
															}
														}
													}
												}
											}
											BGl_codez12z12zzsaw_jvm_outz00(
												((BgL_jvmz00_bglt) BgL_mez00_114), CNST_TABLE_REF(39));
											{	/* SawJvm/funcall.scm 263 */
												obj_t BgL_arg2014z00_3019;

												{	/* SawJvm/funcall.scm 263 */
													obj_t BgL_b1638z00_3020;

													if (INTEGERP(BgL_arityz00_2903))
														{	/* SawJvm/funcall.scm 263 */
															obj_t BgL_tmpz00_4062;

															BgL_tmpz00_4062 = BINT(0L);
															{	/* SawJvm/funcall.scm 263 */
																bool_t BgL_test2424z00_5767;

																{	/* SawJvm/funcall.scm 263 */
																	obj_t BgL_tmpz00_5768;

																	BgL_tmpz00_5768 = BINT(1L);
																	BgL_test2424z00_5767 =
																		BGL_ADDFX_OV(BgL_tmpz00_5768,
																		BgL_arityz00_2903, BgL_tmpz00_4062);
																}
																if (BgL_test2424z00_5767)
																	{	/* SawJvm/funcall.scm 263 */
																		BgL_b1638z00_3020 =
																			bgl_bignum_add(CNST_TABLE_REF(40),
																			bgl_long_to_bignum(
																				(long) CINT(BgL_arityz00_2903)));
																	}
																else
																	{	/* SawJvm/funcall.scm 263 */
																		BgL_b1638z00_3020 = BgL_tmpz00_4062;
																	}
															}
														}
													else
														{	/* SawJvm/funcall.scm 263 */
															BgL_b1638z00_3020 =
																BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(1L),
																BgL_arityz00_2903);
														}
													{	/* SawJvm/funcall.scm 263 */
														bool_t BgL_test2425z00_5777;

														if (INTEGERP(BgL_iz00_115))
															{	/* SawJvm/funcall.scm 263 */
																BgL_test2425z00_5777 =
																	INTEGERP(BgL_b1638z00_3020);
															}
														else
															{	/* SawJvm/funcall.scm 263 */
																BgL_test2425z00_5777 = ((bool_t) 0);
															}
														if (BgL_test2425z00_5777)
															{	/* SawJvm/funcall.scm 263 */
																obj_t BgL_tmpz00_4072;

																BgL_tmpz00_4072 = BINT(0L);
																if (BGL_ADDFX_OV(BgL_iz00_115,
																		BgL_b1638z00_3020, BgL_tmpz00_4072))
																	{	/* SawJvm/funcall.scm 263 */
																		BgL_arg2014z00_3019 =
																			bgl_bignum_add(bgl_long_to_bignum(
																				(long) CINT(BgL_iz00_115)),
																			bgl_long_to_bignum(
																				(long) CINT(BgL_b1638z00_3020)));
																	}
																else
																	{	/* SawJvm/funcall.scm 263 */
																		BgL_arg2014z00_3019 = BgL_tmpz00_4072;
																	}
															}
														else
															{	/* SawJvm/funcall.scm 263 */
																BgL_arg2014z00_3019 =
																	BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_iz00_115,
																	BgL_b1638z00_3020);
															}
													}
												}
												BgL_nz00_3028 = BgL_arg2014z00_3019;
											BgL_zc3z04anonymousza32019ze3z87_3029:
												{	/* SawJvm/funcall.scm 222 */
													bool_t BgL_test2428z00_5790;

													if (INTEGERP(BgL_nz00_3028))
														{	/* SawJvm/funcall.scm 222 */
															BgL_test2428z00_5790 =
																((long) CINT(BgL_nz00_3028) == 0L);
														}
													else
														{	/* SawJvm/funcall.scm 222 */
															BgL_test2428z00_5790 =
																BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_nz00_3028,
																BINT(0L));
														}
													if (BgL_test2428z00_5790)
														{	/* SawJvm/funcall.scm 222 */
															BGl_callzd2globalzd2zzsaw_jvm_outz00(
																((BgL_jvmz00_bglt) BgL_mez00_114),
																BgL_entryz00_2904);
														}
													else
														{	/* SawJvm/funcall.scm 222 */
															BGl_codez12z12zzsaw_jvm_outz00(
																((BgL_jvmz00_bglt) BgL_mez00_114),
																CNST_TABLE_REF(34));
															{	/* SawJvm/funcall.scm 225 */
																long BgL_arg2021z00_3031;

																BgL_arg2021z00_3031 =
																	((long) CINT(BgL_nz00_3028) - 1L);
																{
																	obj_t BgL_nz00_5804;

																	BgL_nz00_5804 = BINT(BgL_arg2021z00_3031);
																	BgL_nz00_3028 = BgL_nz00_5804;
																	goto BgL_zc3z04anonymousza32019ze3z87_3029;
																}
															}
														}
												}
											}
										}
								}
							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_114), CNST_TABLE_REF(19));
						}
					}
				}
			}
		}

	}



/* compile-apply */
	obj_t BGl_compilezd2applyzd2zzsaw_jvm_funcallz00(BgL_jvmz00_bglt
		BgL_mez00_119, obj_t BgL_procsz00_120)
	{
		{	/* SawJvm/funcall.scm 269 */
			{	/* SawJvm/funcall.scm 274 */
				obj_t BgL_needz00_3035;

				if (NULLP(BgL_procsz00_120))
					{	/* SawJvm/funcall.scm 274 */
						BgL_needz00_3035 = BNIL;
					}
				else
					{	/* SawJvm/funcall.scm 274 */
						obj_t BgL_head1641z00_3057;

						{	/* SawJvm/funcall.scm 274 */
							obj_t BgL_arg2037z00_3069;

							{	/* SawJvm/funcall.scm 274 */
								obj_t BgL_arg2038z00_3070;

								BgL_arg2038z00_3070 = CAR(((obj_t) BgL_procsz00_120));
								BgL_arg2037z00_3069 =
									BGl_xx_globalzd2arityze70z35zzsaw_jvm_funcallz00
									(BgL_mez00_119, BgL_arg2038z00_3070);
							}
							BgL_head1641z00_3057 = MAKE_YOUNG_PAIR(BgL_arg2037z00_3069, BNIL);
						}
						{	/* SawJvm/funcall.scm 274 */
							obj_t BgL_g1644z00_3058;

							BgL_g1644z00_3058 = CDR(((obj_t) BgL_procsz00_120));
							{
								obj_t BgL_l1639z00_3060;
								obj_t BgL_tail1642z00_3061;

								BgL_l1639z00_3060 = BgL_g1644z00_3058;
								BgL_tail1642z00_3061 = BgL_head1641z00_3057;
							BgL_zc3z04anonymousza32031ze3z87_3062:
								if (NULLP(BgL_l1639z00_3060))
									{	/* SawJvm/funcall.scm 274 */
										BgL_needz00_3035 = BgL_head1641z00_3057;
									}
								else
									{	/* SawJvm/funcall.scm 274 */
										obj_t BgL_newtail1643z00_3064;

										{	/* SawJvm/funcall.scm 274 */
											obj_t BgL_arg2034z00_3066;

											{	/* SawJvm/funcall.scm 274 */
												obj_t BgL_arg2036z00_3067;

												BgL_arg2036z00_3067 = CAR(((obj_t) BgL_l1639z00_3060));
												BgL_arg2034z00_3066 =
													BGl_xx_globalzd2arityze70z35zzsaw_jvm_funcallz00
													(BgL_mez00_119, BgL_arg2036z00_3067);
											}
											BgL_newtail1643z00_3064 =
												MAKE_YOUNG_PAIR(BgL_arg2034z00_3066, BNIL);
										}
										SET_CDR(BgL_tail1642z00_3061, BgL_newtail1643z00_3064);
										{	/* SawJvm/funcall.scm 274 */
											obj_t BgL_arg2033z00_3065;

											BgL_arg2033z00_3065 = CDR(((obj_t) BgL_l1639z00_3060));
											{
												obj_t BgL_tail1642z00_5827;
												obj_t BgL_l1639z00_5826;

												BgL_l1639z00_5826 = BgL_arg2033z00_3065;
												BgL_tail1642z00_5827 = BgL_newtail1643z00_3064;
												BgL_tail1642z00_3061 = BgL_tail1642z00_5827;
												BgL_l1639z00_3060 = BgL_l1639z00_5826;
												goto BgL_zc3z04anonymousza32031ze3z87_3062;
											}
										}
									}
							}
						}
					}
				{	/* SawJvm/funcall.scm 275 */
					bool_t BgL_test2432z00_5828;

					{
						obj_t BgL_l1645z00_3047;

						BgL_l1645z00_3047 = BgL_needz00_3035;
					BgL_zc3z04anonymousza32027ze3z87_3048:
						if (NULLP(BgL_l1645z00_3047))
							{	/* SawJvm/funcall.scm 275 */
								BgL_test2432z00_5828 = ((bool_t) 1);
							}
						else
							{	/* SawJvm/funcall.scm 275 */
								bool_t BgL_test2434z00_5831;

								if (CBOOL(CAR(((obj_t) BgL_l1645z00_3047))))
									{	/* SawJvm/funcall.scm 275 */
										BgL_test2434z00_5831 = ((bool_t) 0);
									}
								else
									{	/* SawJvm/funcall.scm 275 */
										BgL_test2434z00_5831 = ((bool_t) 1);
									}
								if (BgL_test2434z00_5831)
									{
										obj_t BgL_l1645z00_5836;

										BgL_l1645z00_5836 = CDR(((obj_t) BgL_l1645z00_3047));
										BgL_l1645z00_3047 = BgL_l1645z00_5836;
										goto BgL_zc3z04anonymousza32027ze3z87_3048;
									}
								else
									{	/* SawJvm/funcall.scm 275 */
										BgL_test2432z00_5828 = ((bool_t) 0);
									}
							}
					}
					if (BgL_test2432z00_5828)
						{	/* SawJvm/funcall.scm 275 */
							return BFALSE;
						}
					else
						{	/* SawJvm/funcall.scm 275 */
							BGl_openzd2libzd2methodz00zzsaw_jvm_outz00(BgL_mez00_119,
								CNST_TABLE_REF(41));
							BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_mez00_119,
								CNST_TABLE_REF(42), BNIL);
							BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_119, CNST_TABLE_REF(31));
							BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_119, CNST_TABLE_REF(31));
							BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_119, CNST_TABLE_REF(3));
							BGl_compilezd2dispatchzd2zzsaw_jvm_funcallz00(BgL_mez00_119,
								BgL_needz00_3035, BgL_procsz00_120);
							return BGl_closezd2methodzd2zzsaw_jvm_outz00(BgL_mez00_119);
						}
				}
			}
		}

	}



/* xx_global-arity~0 */
	obj_t BGl_xx_globalzd2arityze70z35zzsaw_jvm_funcallz00(BgL_jvmz00_bglt
		BgL_mez00_4467, obj_t BgL_pz00_3071)
	{
		{	/* SawJvm/funcall.scm 273 */
			if (BGl_iszd2lightzd2procedurezf3zf3zzsaw_jvm_funcallz00(BgL_mez00_4467,
					BgL_pz00_3071))
				{	/* SawJvm/funcall.scm 271 */
					return BFALSE;
				}
			else
				{	/* SawJvm/funcall.scm 272 */
					bool_t BgL_test2437z00_5853;

					{	/* SawJvm/funcall.scm 272 */
						BgL_globalz00_bglt BgL_arg2040z00_3075;

						BgL_arg2040z00_3075 =
							BGl_globalzd2entryzd2zzbackend_cplibz00(
							((BgL_globalz00_bglt) BgL_pz00_3071));
						BgL_test2437z00_5853 =
							BGl_iszd2lightzd2procedurezf3zf3zzsaw_jvm_funcallz00
							(BgL_mez00_4467, ((obj_t) BgL_arg2040z00_3075));
					}
					if (BgL_test2437z00_5853)
						{	/* SawJvm/funcall.scm 272 */
							return BFALSE;
						}
					else
						{	/* SawJvm/funcall.scm 272 */
							return
								BGl_globalzd2arityzd2zzbackend_cplibz00(
								((BgL_globalz00_bglt) BgL_pz00_3071));
						}
				}
		}

	}



/* compile-dispatch */
	obj_t BGl_compilezd2dispatchzd2zzsaw_jvm_funcallz00(BgL_jvmz00_bglt
		BgL_mez00_121, obj_t BgL_needz00_122, obj_t BgL_procsz00_123)
	{
		{	/* SawJvm/funcall.scm 284 */
			{	/* SawJvm/funcall.scm 291 */
				obj_t BgL_labsz00_3079;

				BgL_labsz00_3079 =
					BGl_getzd2labsze70z35zzsaw_jvm_funcallz00(0L, BgL_needz00_122,
					BgL_procsz00_123);
				{	/* SawJvm/funcall.scm 292 */
					obj_t BgL_arg2041z00_3080;

					{	/* SawJvm/funcall.scm 292 */
						obj_t BgL_arg2042z00_3081;

						{	/* SawJvm/funcall.scm 292 */
							obj_t BgL_arg2044z00_3082;

							BgL_arg2044z00_3082 =
								MAKE_YOUNG_PAIR(BINT(0L),
								BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
								(BgL_labsz00_3079, BNIL));
							BgL_arg2042z00_3081 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BgL_arg2044z00_3082);
						}
						BgL_arg2041z00_3080 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(21), BgL_arg2042z00_3081);
					}
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_121, BgL_arg2041z00_3080);
				}
				BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_121, CNST_TABLE_REF(20));
				BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_121, CNST_TABLE_REF(43));
				BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_121, CNST_TABLE_REF(44));
				BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_121, CNST_TABLE_REF(19));
				{	/* SawJvm/funcall.scm 297 */
					obj_t BgL_zc3z04anonymousza32050ze3z87_4422;

					BgL_zc3z04anonymousza32050ze3z87_4422 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza32050ze3ze5zzsaw_jvm_funcallz00,
						(int) (3L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32050ze3z87_4422, (int) (0L),
						((obj_t) BgL_mez00_121));
					{	/* SawJvm/funcall.scm 297 */
						obj_t BgL_list2047z00_3085;

						{	/* SawJvm/funcall.scm 297 */
							obj_t BgL_arg2048z00_3086;

							{	/* SawJvm/funcall.scm 297 */
								obj_t BgL_arg2049z00_3087;

								BgL_arg2049z00_3087 = MAKE_YOUNG_PAIR(BgL_procsz00_123, BNIL);
								BgL_arg2048z00_3086 =
									MAKE_YOUNG_PAIR(BgL_labsz00_3079, BgL_arg2049z00_3087);
							}
							BgL_list2047z00_3085 =
								MAKE_YOUNG_PAIR(BgL_needz00_122, BgL_arg2048z00_3086);
						}
						return
							BGl_forzd2eachzd2zz__r4_control_features_6_9z00
							(BgL_zc3z04anonymousza32050ze3z87_4422, BgL_list2047z00_3085);
					}
				}
			}
		}

	}



/* get-labs~0 */
	obj_t BGl_getzd2labsze70z35zzsaw_jvm_funcallz00(long BgL_iz00_3099,
		obj_t BgL_nsz00_3100, obj_t BgL_psz00_3101)
	{
		{	/* SawJvm/funcall.scm 290 */
			{
				long BgL_nz00_3093;

				if (NULLP(BgL_nsz00_3100))
					{	/* SawJvm/funcall.scm 287 */
						return BNIL;
					}
				else
					{	/* SawJvm/funcall.scm 289 */
						obj_t BgL_arg2058z00_3104;
						obj_t BgL_arg2059z00_3105;

						if (CBOOL(CAR(((obj_t) BgL_nsz00_3100))))
							{	/* SawJvm/funcall.scm 289 */
								BgL_nz00_3093 = BgL_iz00_3099;
								{	/* SawJvm/funcall.scm 285 */
									obj_t BgL_arg2052z00_3095;

									{	/* SawJvm/funcall.scm 285 */
										obj_t BgL_arg2055z00_3096;

										{	/* SawJvm/funcall.scm 285 */

											BgL_arg2055z00_3096 =
												BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
												(BgL_nz00_3093, 10L);
										}
										BgL_arg2052z00_3095 =
											string_append(BGl_string2248z00zzsaw_jvm_funcallz00,
											BgL_arg2055z00_3096);
									}
									BgL_arg2058z00_3104 = bstring_to_symbol(BgL_arg2052z00_3095);
								}
							}
						else
							{	/* SawJvm/funcall.scm 289 */
								BgL_arg2058z00_3104 = CNST_TABLE_REF(20);
							}
						{	/* SawJvm/funcall.scm 290 */
							long BgL_arg2061z00_3107;
							obj_t BgL_arg2062z00_3108;
							obj_t BgL_arg2063z00_3109;

							BgL_arg2061z00_3107 = (BgL_iz00_3099 + 1L);
							BgL_arg2062z00_3108 = CDR(((obj_t) BgL_nsz00_3100));
							BgL_arg2063z00_3109 = CDR(((obj_t) BgL_psz00_3101));
							BgL_arg2059z00_3105 =
								BGl_getzd2labsze70z35zzsaw_jvm_funcallz00(BgL_arg2061z00_3107,
								BgL_arg2062z00_3108, BgL_arg2063z00_3109);
						}
						return MAKE_YOUNG_PAIR(BgL_arg2058z00_3104, BgL_arg2059z00_3105);
					}
			}
		}

	}



/* &<@anonymous:2050> */
	obj_t BGl_z62zc3z04anonymousza32050ze3ze5zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4423, obj_t BgL_nzf3zf3_4425, obj_t BgL_labz00_4426,
		obj_t BgL_pz00_4427)
	{
		{	/* SawJvm/funcall.scm 297 */
			{	/* SawJvm/funcall.scm 297 */
				BgL_jvmz00_bglt BgL_mez00_4424;

				BgL_mez00_4424 =
					((BgL_jvmz00_bglt) PROCEDURE_REF(BgL_envz00_4423, (int) (0L)));
				if (CBOOL(BgL_nzf3zf3_4425))
					{	/* SawJvm/funcall.scm 297 */
						return
							BGl_compilezd2forzd2applyz00zzsaw_jvm_funcallz00(
							((obj_t) BgL_mez00_4424), BgL_labz00_4426, BgL_pz00_4427);
					}
				else
					{	/* SawJvm/funcall.scm 297 */
						return BFALSE;
					}
			}
		}

	}



/* compile-for-apply */
	obj_t BGl_compilezd2forzd2applyz00zzsaw_jvm_funcallz00(obj_t BgL_mez00_124,
		obj_t BgL_labz00_125, obj_t BgL_pz00_126)
	{
		{	/* SawJvm/funcall.scm 302 */
			{
				obj_t BgL_mez00_3137;
				obj_t BgL_nz00_3138;
				bool_t BgL_fixedarityzf3zf3_3139;
				obj_t BgL_typesz00_3140;

				{	/* SawJvm/funcall.scm 328 */
					obj_t BgL_arityz00_3113;

					BgL_arityz00_3113 =
						BGl_globalzd2arityzd2zzbackend_cplibz00(
						((BgL_globalz00_bglt) BgL_pz00_126));
					{	/* SawJvm/funcall.scm 328 */
						BgL_globalz00_bglt BgL_entryz00_3114;

						BgL_entryz00_3114 =
							BGl_globalzd2entryzd2zzbackend_cplibz00(
							((BgL_globalz00_bglt) BgL_pz00_126));
						{	/* SawJvm/funcall.scm 329 */
							obj_t BgL_argsz00_3115;

							BgL_argsz00_3115 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_entryz00_3114)))->
												BgL_valuez00))))->BgL_argsz00);
							{	/* SawJvm/funcall.scm 330 */
								obj_t BgL_typesz00_3116;

								{	/* SawJvm/funcall.scm 331 */
									obj_t BgL_l1648z00_3120;

									BgL_l1648z00_3120 = CDR(((obj_t) BgL_argsz00_3115));
									if (NULLP(BgL_l1648z00_3120))
										{	/* SawJvm/funcall.scm 331 */
											BgL_typesz00_3116 = BNIL;
										}
									else
										{	/* SawJvm/funcall.scm 331 */
											obj_t BgL_head1650z00_3122;

											{	/* SawJvm/funcall.scm 331 */
												BgL_typez00_bglt BgL_arg2076z00_3134;

												BgL_arg2076z00_3134 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt)
																	CAR(
																		((obj_t) BgL_l1648z00_3120))))))->
													BgL_typez00);
												BgL_head1650z00_3122 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg2076z00_3134), BNIL);
											}
											{	/* SawJvm/funcall.scm 331 */
												obj_t BgL_g1653z00_3123;

												BgL_g1653z00_3123 = CDR(((obj_t) BgL_l1648z00_3120));
												{
													obj_t BgL_l1648z00_3125;
													obj_t BgL_tail1651z00_3126;

													BgL_l1648z00_3125 = BgL_g1653z00_3123;
													BgL_tail1651z00_3126 = BgL_head1650z00_3122;
												BgL_zc3z04anonymousza32069ze3z87_3127:
													if (NULLP(BgL_l1648z00_3125))
														{	/* SawJvm/funcall.scm 331 */
															BgL_typesz00_3116 = BgL_head1650z00_3122;
														}
													else
														{	/* SawJvm/funcall.scm 331 */
															obj_t BgL_newtail1652z00_3129;

															{	/* SawJvm/funcall.scm 331 */
																BgL_typez00_bglt BgL_arg2074z00_3131;

																BgL_arg2074z00_3131 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					CAR(
																						((obj_t) BgL_l1648z00_3125))))))->
																	BgL_typez00);
																BgL_newtail1652z00_3129 =
																	MAKE_YOUNG_PAIR(((obj_t) BgL_arg2074z00_3131),
																	BNIL);
															}
															SET_CDR(BgL_tail1651z00_3126,
																BgL_newtail1652z00_3129);
															{	/* SawJvm/funcall.scm 331 */
																obj_t BgL_arg2072z00_3130;

																BgL_arg2072z00_3130 =
																	CDR(((obj_t) BgL_l1648z00_3125));
																{
																	obj_t BgL_tail1651z00_5945;
																	obj_t BgL_l1648z00_5944;

																	BgL_l1648z00_5944 = BgL_arg2072z00_3130;
																	BgL_tail1651z00_5945 =
																		BgL_newtail1652z00_3129;
																	BgL_tail1651z00_3126 = BgL_tail1651z00_5945;
																	BgL_l1648z00_3125 = BgL_l1648z00_5944;
																	goto BgL_zc3z04anonymousza32069ze3z87_3127;
																}
															}
														}
												}
											}
										}
								}
								{	/* SawJvm/funcall.scm 331 */

									BGl_labelz00zzsaw_jvm_outz00(
										((BgL_jvmz00_bglt) BgL_mez00_124), BgL_labz00_125);
									{	/* SawJvm/funcall.scm 333 */
										bool_t BgL_test2443z00_5948;

										if (INTEGERP(BgL_arityz00_3113))
											{	/* SawJvm/funcall.scm 333 */
												BgL_test2443z00_5948 =
													((long) CINT(BgL_arityz00_3113) >= 0L);
											}
										else
											{	/* SawJvm/funcall.scm 333 */
												BgL_test2443z00_5948 =
													BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_arityz00_3113,
													BINT(0L));
											}
										if (BgL_test2443z00_5948)
											{	/* SawJvm/funcall.scm 333 */
												if (BGl_keyzd2optzf3z21zzsaw_jvm_funcallz00
													(BgL_pz00_126))
													{	/* SawJvm/funcall.scm 334 */
														BGl_codez12z12zzsaw_jvm_outz00(
															((BgL_jvmz00_bglt) BgL_mez00_124),
															CNST_TABLE_REF(43));
														BGl_codez12z12zzsaw_jvm_outz00(
															((BgL_jvmz00_bglt) BgL_mez00_124),
															CNST_TABLE_REF(50));
													}
												else
													{	/* SawJvm/funcall.scm 334 */
														BgL_mez00_3137 = BgL_mez00_124;
														BgL_nz00_3138 = BgL_arityz00_3113;
														BgL_fixedarityzf3zf3_3139 = ((bool_t) 1);
														BgL_typesz00_3140 = BgL_typesz00_3116;
													BgL_zc3z04anonymousza32079ze3z87_3141:
														if (((long) CINT(BgL_nz00_3138) == 0L))
															{	/* SawJvm/funcall.scm 305 */
																if (BgL_fixedarityzf3zf3_3139)
																	{	/* SawJvm/funcall.scm 306 */
																		BFALSE;
																	}
																else
																	{	/* SawJvm/funcall.scm 306 */
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(43));
																	}
															}
														else
															{	/* SawJvm/funcall.scm 305 */
																if (((long) CINT(BgL_nz00_3138) == 1L))
																	{	/* SawJvm/funcall.scm 308 */
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(43));
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(45));
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(46));
																		{	/* SawJvm/funcall.scm 312 */
																			bool_t BgL_test2449z00_5982;

																			{	/* SawJvm/funcall.scm 312 */
																				obj_t BgL_arg2089z00_3150;

																				BgL_arg2089z00_3150 =
																					CAR(((obj_t) BgL_typesz00_3140));
																				BgL_test2449z00_5982 =
																					(BgL_arg2089z00_3150 ==
																					BGl_za2objza2z00zztype_cachez00);
																			}
																			if (BgL_test2449z00_5982)
																				{	/* SawJvm/funcall.scm 312 */
																					BFALSE;
																				}
																			else
																				{	/* SawJvm/funcall.scm 313 */
																					obj_t BgL_arg2084z00_3146;

																					{	/* SawJvm/funcall.scm 313 */
																						obj_t BgL_arg2086z00_3147;

																						{	/* SawJvm/funcall.scm 313 */
																							obj_t BgL_arg2087z00_3148;

																							{	/* SawJvm/funcall.scm 313 */
																								obj_t BgL_arg2088z00_3149;

																								BgL_arg2088z00_3149 =
																									CAR(
																									((obj_t) BgL_typesz00_3140));
																								BgL_arg2087z00_3148 =
																									BGl_compilezd2typezd2zzsaw_jvm_outz00
																									(((BgL_jvmz00_bglt)
																										BgL_mez00_3137),
																									((BgL_typez00_bglt)
																										BgL_arg2088z00_3149));
																							}
																							BgL_arg2086z00_3147 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2087z00_3148, BNIL);
																						}
																						BgL_arg2084z00_3146 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(28), BgL_arg2086z00_3147);
																					}
																					BGl_codez12z12zzsaw_jvm_outz00(
																						((BgL_jvmz00_bglt) BgL_mez00_3137),
																						BgL_arg2084z00_3146);
																				}
																		}
																		if (BgL_fixedarityzf3zf3_3139)
																			{	/* SawJvm/funcall.scm 314 */
																				BFALSE;
																			}
																		else
																			{	/* SawJvm/funcall.scm 314 */
																				BGl_codez12z12zzsaw_jvm_outz00(
																					((BgL_jvmz00_bglt) BgL_mez00_3137),
																					CNST_TABLE_REF(43));
																				BGl_codez12z12zzsaw_jvm_outz00(
																					((BgL_jvmz00_bglt) BgL_mez00_3137),
																					CNST_TABLE_REF(45));
																				BGl_codez12z12zzsaw_jvm_outz00(
																					((BgL_jvmz00_bglt) BgL_mez00_3137),
																					CNST_TABLE_REF(47));
																			}
																	}
																else
																	{	/* SawJvm/funcall.scm 308 */
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(43));
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(45));
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(48));
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(47));
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(49));
																		BGl_codez12z12zzsaw_jvm_outz00(
																			((BgL_jvmz00_bglt) BgL_mez00_3137),
																			CNST_TABLE_REF(46));
																		{	/* SawJvm/funcall.scm 325 */
																			bool_t BgL_test2451z00_6024;

																			{	/* SawJvm/funcall.scm 325 */
																				obj_t BgL_arg2097z00_3157;

																				BgL_arg2097z00_3157 =
																					CAR(((obj_t) BgL_typesz00_3140));
																				BgL_test2451z00_6024 =
																					(BgL_arg2097z00_3157 ==
																					BGl_za2objza2z00zztype_cachez00);
																			}
																			if (BgL_test2451z00_6024)
																				{	/* SawJvm/funcall.scm 325 */
																					BFALSE;
																				}
																			else
																				{	/* SawJvm/funcall.scm 326 */
																					obj_t BgL_arg2093z00_3153;

																					{	/* SawJvm/funcall.scm 326 */
																						obj_t BgL_arg2094z00_3154;

																						{	/* SawJvm/funcall.scm 326 */
																							obj_t BgL_arg2095z00_3155;

																							{	/* SawJvm/funcall.scm 326 */
																								obj_t BgL_arg2096z00_3156;

																								BgL_arg2096z00_3156 =
																									CAR(
																									((obj_t) BgL_typesz00_3140));
																								BgL_arg2095z00_3155 =
																									BGl_compilezd2typezd2zzsaw_jvm_outz00
																									(((BgL_jvmz00_bglt)
																										BgL_mez00_3137),
																									((BgL_typez00_bglt)
																										BgL_arg2096z00_3156));
																							}
																							BgL_arg2094z00_3154 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2095z00_3155, BNIL);
																						}
																						BgL_arg2093z00_3153 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(28), BgL_arg2094z00_3154);
																					}
																					BGl_codez12z12zzsaw_jvm_outz00(
																						((BgL_jvmz00_bglt) BgL_mez00_3137),
																						BgL_arg2093z00_3153);
																				}
																		}
																		{	/* SawJvm/funcall.scm 327 */
																			obj_t BgL_arg2098z00_3158;
																			obj_t BgL_arg2099z00_3159;

																			if (INTEGERP(BgL_nz00_3138))
																				{	/* SawJvm/funcall.scm 327 */
																					obj_t BgL_tmpz00_4100;

																					BgL_tmpz00_4100 = BINT(0L);
																					{	/* SawJvm/funcall.scm 327 */
																						bool_t BgL_test2453z00_6041;

																						{	/* SawJvm/funcall.scm 327 */
																							obj_t BgL_tmpz00_6042;

																							BgL_tmpz00_6042 = BINT(1L);
																							BgL_test2453z00_6041 =
																								BGL_SUBFX_OV(BgL_nz00_3138,
																								BgL_tmpz00_6042,
																								BgL_tmpz00_4100);
																						}
																						if (BgL_test2453z00_6041)
																							{	/* SawJvm/funcall.scm 327 */
																								BgL_arg2098z00_3158 =
																									bgl_bignum_sub
																									(bgl_long_to_bignum((long)
																										CINT(BgL_nz00_3138)),
																									CNST_TABLE_REF(40));
																							}
																						else
																							{	/* SawJvm/funcall.scm 327 */
																								BgL_arg2098z00_3158 =
																									BgL_tmpz00_4100;
																							}
																					}
																				}
																			else
																				{	/* SawJvm/funcall.scm 327 */
																					BgL_arg2098z00_3158 =
																						BGl_2zd2zd2zz__r4_numbers_6_5z00
																						(BgL_nz00_3138, BINT(1L));
																				}
																			BgL_arg2099z00_3159 =
																				CDR(((obj_t) BgL_typesz00_3140));
																			{
																				obj_t BgL_typesz00_6054;
																				obj_t BgL_nz00_6053;

																				BgL_nz00_6053 = BgL_arg2098z00_3158;
																				BgL_typesz00_6054 = BgL_arg2099z00_3159;
																				BgL_typesz00_3140 = BgL_typesz00_6054;
																				BgL_nz00_3138 = BgL_nz00_6053;
																				goto
																					BgL_zc3z04anonymousza32079ze3z87_3141;
																			}
																		}
																	}
															}
													}
											}
										else
											{	/* SawJvm/funcall.scm 338 */
												obj_t BgL_arg2067z00_3119;

												if (INTEGERP(BgL_arityz00_3113))
													{	/* SawJvm/funcall.scm 338 */
														obj_t BgL_tmpz00_4122;

														BgL_tmpz00_4122 = BINT(0L);
														{	/* SawJvm/funcall.scm 338 */
															bool_t BgL_test2455z00_6058;

															{	/* SawJvm/funcall.scm 338 */
																obj_t BgL_tmpz00_6059;

																BgL_tmpz00_6059 = BINT(-1L);
																BgL_test2455z00_6058 =
																	BGL_SUBFX_OV(BgL_tmpz00_6059,
																	BgL_arityz00_3113, BgL_tmpz00_4122);
															}
															if (BgL_test2455z00_6058)
																{	/* SawJvm/funcall.scm 338 */
																	BgL_arg2067z00_3119 =
																		bgl_bignum_sub(CNST_TABLE_REF(32),
																		bgl_long_to_bignum(
																			(long) CINT(BgL_arityz00_3113)));
																}
															else
																{	/* SawJvm/funcall.scm 338 */
																	BgL_arg2067z00_3119 = BgL_tmpz00_4122;
																}
														}
													}
												else
													{	/* SawJvm/funcall.scm 338 */
														BgL_arg2067z00_3119 =
															BGl_2zd2zd2zz__r4_numbers_6_5z00(BINT(-1L),
															BgL_arityz00_3113);
													}
												{
													obj_t BgL_typesz00_6071;
													bool_t BgL_fixedarityzf3zf3_6070;
													obj_t BgL_nz00_6069;
													obj_t BgL_mez00_6068;

													BgL_mez00_6068 = BgL_mez00_124;
													BgL_nz00_6069 = BgL_arg2067z00_3119;
													BgL_fixedarityzf3zf3_6070 = ((bool_t) 0);
													BgL_typesz00_6071 = BgL_typesz00_3116;
													BgL_typesz00_3140 = BgL_typesz00_6071;
													BgL_fixedarityzf3zf3_3139 = BgL_fixedarityzf3zf3_6070;
													BgL_nz00_3138 = BgL_nz00_6069;
													BgL_mez00_3137 = BgL_mez00_6068;
													goto BgL_zc3z04anonymousza32079ze3z87_3141;
												}
											}
									}
									BGl_callzd2globalzd2zzsaw_jvm_outz00(
										((BgL_jvmz00_bglt) BgL_mez00_124), BgL_entryz00_3114);
									return
										BGl_codez12z12zzsaw_jvm_outz00(
										((BgL_jvmz00_bglt) BgL_mez00_124), CNST_TABLE_REF(19));
								}
							}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_jvm_funcallz00(void)
	{
		{	/* SawJvm/funcall.scm 1 */
			{	/* SawJvm/funcall.scm 19 */
				obj_t BgL_arg2104z00_3165;
				obj_t BgL_arg2105z00_3166;

				{	/* SawJvm/funcall.scm 19 */
					obj_t BgL_v1654z00_3208;

					BgL_v1654z00_3208 = create_vector(1L);
					{	/* SawJvm/funcall.scm 19 */
						obj_t BgL_arg2116z00_3209;

						BgL_arg2116z00_3209 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(51),
							BGl_proc2253z00zzsaw_jvm_funcallz00,
							BGl_proc2252z00zzsaw_jvm_funcallz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(11));
						VECTOR_SET(BgL_v1654z00_3208, 0L, BgL_arg2116z00_3209);
					}
					BgL_arg2104z00_3165 = BgL_v1654z00_3208;
				}
				{	/* SawJvm/funcall.scm 19 */
					obj_t BgL_v1655z00_3219;

					BgL_v1655z00_3219 = create_vector(0L);
					BgL_arg2105z00_3166 = BgL_v1655z00_3219;
				}
				return (BGl_indexedz00zzsaw_jvm_funcallz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(52),
						CNST_TABLE_REF(53), BGl_globalz00zzast_varz00, 1316L,
						BGl_proc2257z00zzsaw_jvm_funcallz00,
						BGl_proc2256z00zzsaw_jvm_funcallz00, BFALSE,
						BGl_proc2255z00zzsaw_jvm_funcallz00,
						BGl_proc2254z00zzsaw_jvm_funcallz00, BgL_arg2104z00_3165,
						BgL_arg2105z00_3166), BUNSPEC);
			}
		}

	}



/* &lambda2112 */
	BgL_globalz00_bglt BGl_z62lambda2112z62zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4434, obj_t BgL_o1181z00_4435)
	{
		{	/* SawJvm/funcall.scm 19 */
			{	/* SawJvm/funcall.scm 19 */
				long BgL_arg2113z00_4532;

				{	/* SawJvm/funcall.scm 19 */
					obj_t BgL_arg2114z00_4533;

					{	/* SawJvm/funcall.scm 19 */
						obj_t BgL_arg2115z00_4534;

						{	/* SawJvm/funcall.scm 19 */
							obj_t BgL_arg1815z00_4535;
							long BgL_arg1816z00_4536;

							BgL_arg1815z00_4535 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawJvm/funcall.scm 19 */
								long BgL_arg1817z00_4537;

								BgL_arg1817z00_4537 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_globalz00_bglt) BgL_o1181z00_4435)));
								BgL_arg1816z00_4536 = (BgL_arg1817z00_4537 - OBJECT_TYPE);
							}
							BgL_arg2115z00_4534 =
								VECTOR_REF(BgL_arg1815z00_4535, BgL_arg1816z00_4536);
						}
						BgL_arg2114z00_4533 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2115z00_4534);
					}
					{	/* SawJvm/funcall.scm 19 */
						obj_t BgL_tmpz00_6093;

						BgL_tmpz00_6093 = ((obj_t) BgL_arg2114z00_4533);
						BgL_arg2113z00_4532 = BGL_CLASS_NUM(BgL_tmpz00_6093);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) BgL_o1181z00_4435)), BgL_arg2113z00_4532);
			}
			{	/* SawJvm/funcall.scm 19 */
				BgL_objectz00_bglt BgL_tmpz00_6099;

				BgL_tmpz00_6099 =
					((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_o1181z00_4435));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6099, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_o1181z00_4435));
			return ((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1181z00_4435));
		}

	}



/* &<@anonymous:2111> */
	obj_t BGl_z62zc3z04anonymousza32111ze3ze5zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4436, obj_t BgL_new1180z00_4437)
	{
		{	/* SawJvm/funcall.scm 19 */
			{
				BgL_globalz00_bglt BgL_auxz00_6107;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1180z00_4437))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(54)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_6115;

					{	/* SawJvm/funcall.scm 19 */
						obj_t BgL_classz00_4539;

						BgL_classz00_4539 = BGl_typez00zztype_typez00;
						{	/* SawJvm/funcall.scm 19 */
							obj_t BgL__ortest_1117z00_4540;

							BgL__ortest_1117z00_4540 = BGL_CLASS_NIL(BgL_classz00_4539);
							if (CBOOL(BgL__ortest_1117z00_4540))
								{	/* SawJvm/funcall.scm 19 */
									BgL_auxz00_6115 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4540);
								}
							else
								{	/* SawJvm/funcall.scm 19 */
									BgL_auxz00_6115 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4539));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1180z00_4437))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_6115), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_6125;

					{	/* SawJvm/funcall.scm 19 */
						obj_t BgL_classz00_4541;

						BgL_classz00_4541 = BGl_valuez00zzast_varz00;
						{	/* SawJvm/funcall.scm 19 */
							obj_t BgL__ortest_1117z00_4542;

							BgL__ortest_1117z00_4542 = BGL_CLASS_NIL(BgL_classz00_4541);
							if (CBOOL(BgL__ortest_1117z00_4542))
								{	/* SawJvm/funcall.scm 19 */
									BgL_auxz00_6125 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_4542);
								}
							else
								{	/* SawJvm/funcall.scm 19 */
									BgL_auxz00_6125 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4541));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1180z00_4437))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_6125), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1180z00_4437))))->
						BgL_accessz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_modulez00) =
					((obj_t) CNST_TABLE_REF(54)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_importz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_evaluablezf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_evalzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_libraryz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_pragmaz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_srcz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BGl_string2258z00zzsaw_jvm_funcallz00), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_initz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1180z00_4437))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_indexedz00_bglt BgL_auxz00_6184;

					{
						obj_t BgL_auxz00_6185;

						{	/* SawJvm/funcall.scm 19 */
							BgL_objectz00_bglt BgL_tmpz00_6186;

							BgL_tmpz00_6186 =
								((BgL_objectz00_bglt)
								((BgL_globalz00_bglt) BgL_new1180z00_4437));
							BgL_auxz00_6185 = BGL_OBJECT_WIDENING(BgL_tmpz00_6186);
						}
						BgL_auxz00_6184 = ((BgL_indexedz00_bglt) BgL_auxz00_6185);
					}
					((((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_6184))->BgL_indexz00) =
						((int) (int) (0L)), BUNSPEC);
				}
				BgL_auxz00_6107 = ((BgL_globalz00_bglt) BgL_new1180z00_4437);
				return ((obj_t) BgL_auxz00_6107);
			}
		}

	}



/* &lambda2109 */
	BgL_globalz00_bglt BGl_z62lambda2109z62zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4438, obj_t BgL_o1177z00_4439)
	{
		{	/* SawJvm/funcall.scm 19 */
			{	/* SawJvm/funcall.scm 19 */
				BgL_indexedz00_bglt BgL_wide1179z00_4544;

				BgL_wide1179z00_4544 =
					((BgL_indexedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_indexedz00_bgl))));
				{	/* SawJvm/funcall.scm 19 */
					obj_t BgL_auxz00_6200;
					BgL_objectz00_bglt BgL_tmpz00_6196;

					BgL_auxz00_6200 = ((obj_t) BgL_wide1179z00_4544);
					BgL_tmpz00_6196 =
						((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1177z00_4439)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6196, BgL_auxz00_6200);
				}
				((BgL_objectz00_bglt)
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1177z00_4439)));
				{	/* SawJvm/funcall.scm 19 */
					long BgL_arg2110z00_4545;

					BgL_arg2110z00_4545 =
						BGL_CLASS_NUM(BGl_indexedz00zzsaw_jvm_funcallz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_globalz00_bglt)
								((BgL_globalz00_bglt) BgL_o1177z00_4439))),
						BgL_arg2110z00_4545);
				}
				return
					((BgL_globalz00_bglt)
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1177z00_4439)));
			}
		}

	}



/* &lambda2106 */
	BgL_globalz00_bglt BGl_z62lambda2106z62zzsaw_jvm_funcallz00(obj_t
		BgL_envz00_4440, obj_t BgL_id1156z00_4441, obj_t BgL_name1157z00_4442,
		obj_t BgL_type1158z00_4443, obj_t BgL_value1159z00_4444,
		obj_t BgL_access1160z00_4445, obj_t BgL_fastzd2alpha1161zd2_4446,
		obj_t BgL_removable1162z00_4447, obj_t BgL_occurrence1163z00_4448,
		obj_t BgL_occurrencew1164z00_4449, obj_t BgL_userzf31165zf3_4450,
		obj_t BgL_module1166z00_4451, obj_t BgL_import1167z00_4452,
		obj_t BgL_evaluablezf31168zf3_4453, obj_t BgL_evalzf31169zf3_4454,
		obj_t BgL_library1170z00_4455, obj_t BgL_pragma1171z00_4456,
		obj_t BgL_src1172z00_4457, obj_t BgL_jvmzd2typezd2name1173z00_4458,
		obj_t BgL_init1174z00_4459, obj_t BgL_alias1175z00_4460,
		obj_t BgL_index1176z00_4461)
	{
		{	/* SawJvm/funcall.scm 19 */
			{	/* SawJvm/funcall.scm 19 */
				long BgL_occurrence1163z00_4549;
				long BgL_occurrencew1164z00_4550;
				bool_t BgL_userzf31165zf3_4551;
				bool_t BgL_evaluablezf31168zf3_4553;
				bool_t BgL_evalzf31169zf3_4554;
				int BgL_index1176z00_4556;

				BgL_occurrence1163z00_4549 = (long) CINT(BgL_occurrence1163z00_4448);
				BgL_occurrencew1164z00_4550 = (long) CINT(BgL_occurrencew1164z00_4449);
				BgL_userzf31165zf3_4551 = CBOOL(BgL_userzf31165zf3_4450);
				BgL_evaluablezf31168zf3_4553 = CBOOL(BgL_evaluablezf31168zf3_4453);
				BgL_evalzf31169zf3_4554 = CBOOL(BgL_evalzf31169zf3_4454);
				BgL_index1176z00_4556 = CINT(BgL_index1176z00_4461);
				{	/* SawJvm/funcall.scm 19 */
					BgL_globalz00_bglt BgL_new1220z00_4557;

					{	/* SawJvm/funcall.scm 19 */
						BgL_globalz00_bglt BgL_tmp1218z00_4558;
						BgL_indexedz00_bglt BgL_wide1219z00_4559;

						{
							BgL_globalz00_bglt BgL_auxz00_6220;

							{	/* SawJvm/funcall.scm 19 */
								BgL_globalz00_bglt BgL_new1217z00_4560;

								BgL_new1217z00_4560 =
									((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_globalz00_bgl))));
								{	/* SawJvm/funcall.scm 19 */
									long BgL_arg2108z00_4561;

									BgL_arg2108z00_4561 =
										BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1217z00_4560),
										BgL_arg2108z00_4561);
								}
								{	/* SawJvm/funcall.scm 19 */
									BgL_objectz00_bglt BgL_tmpz00_6225;

									BgL_tmpz00_6225 = ((BgL_objectz00_bglt) BgL_new1217z00_4560);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6225, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1217z00_4560);
								BgL_auxz00_6220 = BgL_new1217z00_4560;
							}
							BgL_tmp1218z00_4558 = ((BgL_globalz00_bglt) BgL_auxz00_6220);
						}
						BgL_wide1219z00_4559 =
							((BgL_indexedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_indexedz00_bgl))));
						{	/* SawJvm/funcall.scm 19 */
							obj_t BgL_auxz00_6233;
							BgL_objectz00_bglt BgL_tmpz00_6231;

							BgL_auxz00_6233 = ((obj_t) BgL_wide1219z00_4559);
							BgL_tmpz00_6231 = ((BgL_objectz00_bglt) BgL_tmp1218z00_4558);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6231, BgL_auxz00_6233);
						}
						((BgL_objectz00_bglt) BgL_tmp1218z00_4558);
						{	/* SawJvm/funcall.scm 19 */
							long BgL_arg2107z00_4562;

							BgL_arg2107z00_4562 =
								BGL_CLASS_NUM(BGl_indexedz00zzsaw_jvm_funcallz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1218z00_4558),
								BgL_arg2107z00_4562);
						}
						BgL_new1220z00_4557 = ((BgL_globalz00_bglt) BgL_tmp1218z00_4558);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1220z00_4557)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1156z00_4441)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_namez00) =
						((obj_t) BgL_name1157z00_4442), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1158z00_4443)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1159z00_4444)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_accessz00) =
						((obj_t) BgL_access1160z00_4445), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1161zd2_4446), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_removablez00) =
						((obj_t) BgL_removable1162z00_4447), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_occurrencez00) =
						((long) BgL_occurrence1163z00_4549), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1164z00_4550), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1220z00_4557)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31165zf3_4551), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_modulez00) =
						((obj_t) ((obj_t) BgL_module1166z00_4451)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_importz00) =
						((obj_t) BgL_import1167z00_4452), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_evaluablezf3zf3) =
						((bool_t) BgL_evaluablezf31168zf3_4553), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_evalzf3zf3) =
						((bool_t) BgL_evalzf31169zf3_4554), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_libraryz00) =
						((obj_t) BgL_library1170z00_4455), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_pragmaz00) =
						((obj_t) BgL_pragma1171z00_4456), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_srcz00) =
						((obj_t) BgL_src1172z00_4457), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_jvmzd2typezd2namez00) =
						((obj_t) ((obj_t) BgL_jvmzd2typezd2name1173z00_4458)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_initz00) =
						((obj_t) BgL_init1174z00_4459), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1220z00_4557)))->BgL_aliasz00) =
						((obj_t) BgL_alias1175z00_4460), BUNSPEC);
					{
						BgL_indexedz00_bglt BgL_auxz00_6286;

						{
							obj_t BgL_auxz00_6287;

							{	/* SawJvm/funcall.scm 19 */
								BgL_objectz00_bglt BgL_tmpz00_6288;

								BgL_tmpz00_6288 = ((BgL_objectz00_bglt) BgL_new1220z00_4557);
								BgL_auxz00_6287 = BGL_OBJECT_WIDENING(BgL_tmpz00_6288);
							}
							BgL_auxz00_6286 = ((BgL_indexedz00_bglt) BgL_auxz00_6287);
						}
						((((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_6286))->BgL_indexz00) =
							((int) BgL_index1176z00_4556), BUNSPEC);
					}
					return BgL_new1220z00_4557;
				}
			}
		}

	}



/* &lambda2120 */
	obj_t BGl_z62lambda2120z62zzsaw_jvm_funcallz00(obj_t BgL_envz00_4462,
		obj_t BgL_oz00_4463, obj_t BgL_vz00_4464)
	{
		{	/* SawJvm/funcall.scm 19 */
			{	/* SawJvm/funcall.scm 19 */
				int BgL_vz00_4564;

				BgL_vz00_4564 = CINT(BgL_vz00_4464);
				{
					BgL_indexedz00_bglt BgL_auxz00_6294;

					{
						obj_t BgL_auxz00_6295;

						{	/* SawJvm/funcall.scm 19 */
							BgL_objectz00_bglt BgL_tmpz00_6296;

							BgL_tmpz00_6296 =
								((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_4463));
							BgL_auxz00_6295 = BGL_OBJECT_WIDENING(BgL_tmpz00_6296);
						}
						BgL_auxz00_6294 = ((BgL_indexedz00_bglt) BgL_auxz00_6295);
					}
					return
						((((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_6294))->BgL_indexz00) =
						((int) BgL_vz00_4564), BUNSPEC);
		}}}

	}



/* &lambda2119 */
	obj_t BGl_z62lambda2119z62zzsaw_jvm_funcallz00(obj_t BgL_envz00_4465,
		obj_t BgL_oz00_4466)
	{
		{	/* SawJvm/funcall.scm 19 */
			{	/* SawJvm/funcall.scm 19 */
				int BgL_tmpz00_6302;

				{
					BgL_indexedz00_bglt BgL_auxz00_6303;

					{
						obj_t BgL_auxz00_6304;

						{	/* SawJvm/funcall.scm 19 */
							BgL_objectz00_bglt BgL_tmpz00_6305;

							BgL_tmpz00_6305 =
								((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_4466));
							BgL_auxz00_6304 = BGL_OBJECT_WIDENING(BgL_tmpz00_6305);
						}
						BgL_auxz00_6303 = ((BgL_indexedz00_bglt) BgL_auxz00_6304);
					}
					BgL_tmpz00_6302 =
						(((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_6303))->BgL_indexz00);
				}
				return BINT(BgL_tmpz00_6302);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_jvm_funcallz00(void)
	{
		{	/* SawJvm/funcall.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_jvm_funcallz00(void)
	{
		{	/* SawJvm/funcall.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_funcallz00(void)
	{
		{	/* SawJvm/funcall.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzbackend_bvmz00(336068337L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(0L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzbackend_libz00(321177283L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			BGl_modulezd2initializa7ationz75zzsaw_proceduresz00(401547344L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_jvm_outz00(441641707L,
				BSTRING_TO_STRING(BGl_string2259z00zzsaw_jvm_funcallz00));
		}

	}

#ifdef __cplusplus
}
#endif
