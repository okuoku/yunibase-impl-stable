/*===========================================================================*/
/*   (SawJvm/names.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawJvm/names.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_JVM_NAMES_TYPE_DEFINITIONS
#define BGL_SAW_JVM_NAMES_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;

	typedef struct BgL_wclassz00_bgl
	{
		obj_t BgL_itszd2classzd2;
	}                *BgL_wclassz00_bglt;

	typedef struct BgL_tvecz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}              *BgL_tvecz00_bglt;

	typedef struct BgL_jarrayz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_jarrayz00_bglt;

	typedef struct BgL_jvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
		obj_t BgL_qnamez00;
		obj_t BgL_classesz00;
		obj_t BgL_currentzd2classzd2;
		obj_t BgL_declarationsz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_codez00;
		obj_t BgL_lightzd2funcallszd2;
		obj_t BgL_inlinez00;
	}             *BgL_jvmz00_bglt;

	typedef struct BgL_jvmbasicz00_bgl
	{
	}                  *BgL_jvmbasicz00_bglt;


#endif													// BGL_SAW_JVM_NAMES_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2pointedzd2tozd2byzd2setz12z12zzsaw_jvm_namesz00
		(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2importzd2locationzd2setz12zc0zzsaw_jvm_namesz00
		(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2jvmbasiczd2zzsaw_jvm_namesz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62jvmbasiczd2initzf3zd2setz12z83zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_jvm_namesz00 = BUNSPEC;
	static obj_t BGl_z62jvmbasiczf3z91zzsaw_jvm_namesz00(obj_t, obj_t);
	static obj_t BGl_z62jvmbasiczd2parentszd2setz12z70zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_qualifiedzd2wclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt);
	static obj_t BGl_z62jvmbasiczd2tvectorzd2setz12z70zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jvmbasiczd2classzd2setz12z70zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jvmbasiczd2namezd2setz12z70zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62jvmbasiczd2namezb0zzsaw_jvm_namesz00(obj_t, obj_t);
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern obj_t
		BGl_qualifiedzd2tclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2aliaszd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62jvmbasiczd2tvectorzb0zzsaw_jvm_namesz00(obj_t, obj_t);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62jvmbasiczd2nilzb0zzsaw_jvm_namesz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_jvmbasiczf3zf3zzsaw_jvm_namesz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2locationzd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_jvm_namesz00(void);
	static obj_t BGl_objectzd2initzd2zzsaw_jvm_namesz00(void);
	static obj_t BGl_z62jvmbasiczd2magiczf3zd2setz12z83zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2z42z90zzsaw_jvm_namesz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62jvmbasiczd2initzf3z43zzsaw_jvm_namesz00(obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t
		BGl_z62jvmbasiczd2pointedzd2tozd2byzd2setz12z70zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2magiczf3zd2setz12ze1zzsaw_jvm_namesz00(BgL_typez00_bglt,
		bool_t);
	static obj_t BGl_z62jvmbasiczd2z42zf2zzsaw_jvm_namesz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2initzf3zd2setz12ze1zzsaw_jvm_namesz00(BgL_typez00_bglt,
		bool_t);
	static obj_t BGl_z62jvmbasiczd2locationzd2setz12z70zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62jvmbasiczd2occurrencezb0zzsaw_jvm_namesz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2classzd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_jvm_namesz00(void);
	extern obj_t BGl_jarrayz00zzforeign_jtypez00;
	BGL_EXPORTED_DECL bool_t
		BGl_jvmbasiczd2magiczf3z21zzsaw_jvm_namesz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2coercezd2toz00zzsaw_jvm_namesz00(BgL_typez00_bglt);
	static obj_t BGl_z62jvmbasiczd2magiczf3z43zzsaw_jvm_namesz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2pointedzd2tozd2byzd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	static obj_t BGl_getzd2jvmtypezd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_jvmbasiczd2initzf3z21zzsaw_jvm_namesz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2coercezd2tozd2setz12zc0zzsaw_jvm_namesz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31610ze3ze5zzsaw_jvm_namesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2namezd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	static obj_t BGl_z62jvmbasiczd2aliaszb0zzsaw_jvm_namesz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_jvmbasicz00zzsaw_jvm_namesz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2parentszd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	static obj_t BGl_z62jvmbasiczd2siza7ez17zzsaw_jvm_namesz00(obj_t, obj_t);
	static obj_t
		BGl_z62saw_jvmzd2setzd2typezd2namesz12za2zzsaw_jvm_namesz00(obj_t, obj_t);
	static obj_t BGl_buildzd2typezd2namez00zzsaw_jvm_namesz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2namezd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt, obj_t);
	extern obj_t BGl_jclassz00zzobject_classz00;
	static obj_t BGl_z62jvmbasiczd2siza7ezd2setz12zd7zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jvmbasiczd2locationzb0zzsaw_jvm_namesz00(obj_t, obj_t);
	static obj_t BGl_z62jvmbasiczd2occurrencezd2setz12z70zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_jvmbasiczd2occurrencezd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	extern obj_t BGl_wclassz00zzobject_classz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_namesz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_jtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t
		BGl_z62jvmbasiczd2importzd2locationzd2setz12za2zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62makezd2jvmbasiczb0zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62jvmbasiczd2importzd2locationz62zzsaw_jvm_namesz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2aliaszd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	static obj_t BGl_z62nameszd2initializa7ationz17zzsaw_jvm_namesz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_jvm_namesz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_namesz00(void);
	static obj_t BGl_z62jvmbasiczd2parentszb0zzsaw_jvm_namesz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2idzd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_namesz00(void);
	static obj_t BGl_z62jvmbasiczd2pointedzd2tozd2byzb0zzsaw_jvm_namesz00(obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_namesz00(void);
	extern obj_t BGl_tvecz00zztvector_tvectorz00;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_jvmbasiczd2nilzd2zzsaw_jvm_namesz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_nameszd2initializa7ationz75zzsaw_jvm_namesz00(BgL_jvmz00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1606z62zzsaw_jvm_namesz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2z42zd2setz12z50zzsaw_jvm_namesz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62jvmbasiczd2z42zd2setz12z32zzsaw_jvm_namesz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1611z62zzsaw_jvm_namesz00(obj_t, obj_t);
	static obj_t BGl_z62jvmbasiczd2classzb0zzsaw_jvm_namesz00(obj_t, obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	static bool_t BGl_resetzd2jvmstdzd2typez12z12zzsaw_jvm_namesz00(void);
	extern obj_t BGl_forzd2eachzd2typez12z12zztype_envz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2siza7ez75zzsaw_jvm_namesz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2occurrencezd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt,
		int);
	static obj_t BGl_z62jvmbasiczd2aliaszd2setz12z70zzsaw_jvm_namesz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2parentszd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt,
		obj_t);
	static obj_t
		BGl_z62jvmbasiczd2coercezd2tozd2setz12za2zzsaw_jvm_namesz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2tvectorzd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62jvmbasiczd2coercezd2toz62zzsaw_jvm_namesz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2tvectorzd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	static obj_t BGl_z62jvmbasiczd2idzb0zzsaw_jvm_namesz00(obj_t, obj_t);
	extern obj_t
		BGl_qualifiedzd2typezd2namez00zzbackend_cplibz00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1596z62zzsaw_jvm_namesz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2importzd2locationz00zzsaw_jvm_namesz00(BgL_typez00_bglt);
	extern obj_t
		BGl_qualifiedzd2jclasszd2namez00zzbackend_cplibz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2locationzd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2siza7ezd2setz12zb5zzsaw_jvm_namesz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jvmbasiczd2classzd2zzsaw_jvm_namesz00(BgL_typez00_bglt);
	static obj_t __cnst[10];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmbasiczd2namezd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2na1832z00,
		BGl_z62jvmbasiczd2namezb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_resetzd2globalz12zd2envz12zzbackend_cplibz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2siza7ezd2setz12zd2envz67zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2si1833z00,
		BGl_z62jvmbasiczd2siza7ezd2setz12zd7zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2aliaszd2setz12zd2envzc0zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2al1834z00,
		BGl_z62jvmbasiczd2aliaszd2setz12z70zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_saw_jvmzd2setzd2typezd2namesz12zd2envz12zzsaw_jvm_namesz00,
		BgL_bgl_za762saw_jvmza7d2set1835z00,
		BGl_z62saw_jvmzd2setzd2typezd2namesz12za2zzsaw_jvm_namesz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2pointedzd2tozd2byzd2setz12zd2envzc0zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2po1836z00,
		BGl_z62jvmbasiczd2pointedzd2tozd2byzd2setz12z70zzsaw_jvm_namesz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2occurrencezd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2oc1837z00,
		BGl_z62jvmbasiczd2occurrencezb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmbasiczd2z42zd2envz42zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2za741838za7,
		BGl_z62jvmbasiczd2z42zf2zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmbasiczd2classzd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2cl1839z00,
		BGl_z62jvmbasiczd2classzb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2pointedzd2tozd2byzd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2po1840z00,
		BGl_z62jvmbasiczd2pointedzd2tozd2byzb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2jvmbasiczd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762makeza7d2jvmbas1841z00,
		BGl_z62makezd2jvmbasiczb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2initzf3zd2envzf3zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2in1842z00,
		BGl_z62jvmbasiczd2initzf3z43zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2parentszd2setz12zd2envzc0zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2pa1843z00,
		BGl_z62jvmbasiczd2parentszd2setz12z70zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2coercezd2tozd2setz12zd2envz12zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2co1844z00,
		BGl_z62jvmbasiczd2coercezd2tozd2setz12za2zzsaw_jvm_namesz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2tvectorzd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2tv1845z00,
		BGl_z62jvmbasiczd2tvectorzb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1819z00zzsaw_jvm_namesz00,
		BgL_bgl_string1819za700za7za7s1846za7, "Zector", 6);
	      DEFINE_STRING(BGl_string1824z00zzsaw_jvm_namesz00,
		BgL_bgl_string1824za700za7za7s1847za7, "saw_jvm_names", 13);
	      DEFINE_STRING(BGl_string1825z00zzsaw_jvm_namesz00,
		BgL_bgl_string1825za700za7za7s1848za7,
		"_ saw_jvm_names jvmbasic object ((obj . obj) (magic . obj) (pair-nil . obj) (void* . obj) (tvector . obj) (class . class) (class-field . obj) (output-port . output-port) (input-port . input-port) (binary-port . binary-port) (datagram-socket . datagram-socket) (regexp . regexp) (epair . extended_pair) (dynamic-env . bgldynamic) (procedure . procedure)) (char char ucs2 obj obj obj) (bstring string ucs2string vector cnst* procedure-el) (boolean byte byte byte char short int int long long long long int byte byte short short int int long long) (bool char byte ubyte ucs2 ushort long uchar llong ullong elong uelong ulong int8 uint8 int16 uint16 int32 uint32 int64 uint64) (void short int float double) ",
		703);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2namezd2setz12zd2envzc0zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2na1849z00,
		BGl_z62jvmbasiczd2namezd2setz12z70zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmbasiczd2aliaszd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2al1850z00,
		BGl_z62jvmbasiczd2aliaszb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1820z00zzsaw_jvm_namesz00,
		BgL_bgl_za762lambda1611za7621851z00, BGl_z62lambda1611z62zzsaw_jvm_namesz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2initzf3zd2setz12zd2envz33zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2in1852z00,
		BGl_z62jvmbasiczd2initzf3zd2setz12z83zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1821z00zzsaw_jvm_namesz00,
		BgL_bgl_za762za7c3za704anonymo1853za7,
		BGl_z62zc3z04anonymousza31610ze3ze5zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1822z00zzsaw_jvm_namesz00,
		BgL_bgl_za762lambda1606za7621854z00, BGl_z62lambda1606z62zzsaw_jvm_namesz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1823z00zzsaw_jvm_namesz00,
		BgL_bgl_za762lambda1596za7621855z00, BGl_z62lambda1596z62zzsaw_jvm_namesz00,
		0L, BUNSPEC, 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2tvectorzd2setz12zd2envzc0zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2tv1856z00,
		BGl_z62jvmbasiczd2tvectorzd2setz12z70zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2importzd2locationzd2envzd2zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2im1857z00,
		BGl_z62jvmbasiczd2importzd2locationz62zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmbasiczd2nilzd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2ni1858z00,
		BGl_z62jvmbasiczd2nilzb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2parentszd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2pa1859z00,
		BGl_z62jvmbasiczd2parentszb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2siza7ezd2envza7zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2si1860z00,
		BGl_z62jvmbasiczd2siza7ez17zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2occurrencezd2setz12zd2envzc0zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2oc1861z00,
		BGl_z62jvmbasiczd2occurrencezd2setz12z70zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2locationzd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2lo1862z00,
		BGl_z62jvmbasiczd2locationzb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmbasiczd2idzd2envz00zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2id1863z00,
		BGl_z62jvmbasiczd2idzb0zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2classzd2setz12zd2envzc0zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2cl1864z00,
		BGl_z62jvmbasiczd2classzd2setz12z70zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_nameszd2initializa7ationzd2envza7zzsaw_jvm_namesz00,
		BgL_bgl_za762namesza7d2initi1865z00,
		BGl_z62nameszd2initializa7ationz17zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2locationzd2setz12zd2envzc0zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2lo1866z00,
		BGl_z62jvmbasiczd2locationzd2setz12z70zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2magiczf3zd2envzf3zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2ma1867z00,
		BGl_z62jvmbasiczd2magiczf3z43zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2importzd2locationzd2setz12zd2envz12zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2im1868z00,
		BGl_z62jvmbasiczd2importzd2locationzd2setz12za2zzsaw_jvm_namesz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmbasiczf3zd2envz21zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7f3za791869za7,
		BGl_z62jvmbasiczf3z91zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2z42zd2setz12zd2envz82zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2za741870za7,
		BGl_z62jvmbasiczd2z42zd2setz12z32zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2coercezd2tozd2envzd2zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2co1871z00,
		BGl_z62jvmbasiczd2coercezd2toz62zzsaw_jvm_namesz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jvmbasiczd2magiczf3zd2setz12zd2envz33zzsaw_jvm_namesz00,
		BgL_bgl_za762jvmbasicza7d2ma1872z00,
		BGl_z62jvmbasiczd2magiczf3zd2setz12z83zzsaw_jvm_namesz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_jvm_namesz00));
		     ADD_ROOT((void *) (&BGl_jvmbasicz00zzsaw_jvm_namesz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_namesz00(long
		BgL_checksumz00_3232, char *BgL_fromz00_3233)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_jvm_namesz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_jvm_namesz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_jvm_namesz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_jvm_namesz00();
					BGl_cnstzd2initzd2zzsaw_jvm_namesz00();
					BGl_importedzd2moduleszd2initz00zzsaw_jvm_namesz00();
					BGl_objectzd2initzd2zzsaw_jvm_namesz00();
					return BGl_methodzd2initzd2zzsaw_jvm_namesz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_jvm_names");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_jvm_names");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_jvm_names");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_jvm_names");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"saw_jvm_names");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_jvm_names");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_jvm_names");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_jvm_names");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_jvm_names");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.scm 1 */
			{	/* SawJvm/names.scm 1 */
				obj_t BgL_cportz00_3178;

				{	/* SawJvm/names.scm 1 */
					obj_t BgL_stringz00_3185;

					BgL_stringz00_3185 = BGl_string1825z00zzsaw_jvm_namesz00;
					{	/* SawJvm/names.scm 1 */
						obj_t BgL_startz00_3186;

						BgL_startz00_3186 = BINT(0L);
						{	/* SawJvm/names.scm 1 */
							obj_t BgL_endz00_3187;

							BgL_endz00_3187 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3185)));
							{	/* SawJvm/names.scm 1 */

								BgL_cportz00_3178 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3185, BgL_startz00_3186, BgL_endz00_3187);
				}}}}
				{
					long BgL_iz00_3179;

					BgL_iz00_3179 = 9L;
				BgL_loopz00_3180:
					if ((BgL_iz00_3179 == -1L))
						{	/* SawJvm/names.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawJvm/names.scm 1 */
							{	/* SawJvm/names.scm 1 */
								obj_t BgL_arg1831z00_3181;

								{	/* SawJvm/names.scm 1 */

									{	/* SawJvm/names.scm 1 */
										obj_t BgL_locationz00_3183;

										BgL_locationz00_3183 = BBOOL(((bool_t) 0));
										{	/* SawJvm/names.scm 1 */

											BgL_arg1831z00_3181 =
												BGl_readz00zz__readerz00(BgL_cportz00_3178,
												BgL_locationz00_3183);
										}
									}
								}
								{	/* SawJvm/names.scm 1 */
									int BgL_tmpz00_3261;

									BgL_tmpz00_3261 = (int) (BgL_iz00_3179);
									CNST_TABLE_SET(BgL_tmpz00_3261, BgL_arg1831z00_3181);
							}}
							{	/* SawJvm/names.scm 1 */
								int BgL_auxz00_3184;

								BgL_auxz00_3184 = (int) ((BgL_iz00_3179 - 1L));
								{
									long BgL_iz00_3266;

									BgL_iz00_3266 = (long) (BgL_auxz00_3184);
									BgL_iz00_3179 = BgL_iz00_3266;
									goto BgL_loopz00_3180;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* make-jvmbasic */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2jvmbasiczd2zzsaw_jvm_namesz00(obj_t BgL_id1126z00_3,
		obj_t BgL_name1127z00_4, obj_t BgL_siza7e1128za7_5,
		obj_t BgL_class1129z00_6, obj_t BgL_coercezd2to1130zd2_7,
		obj_t BgL_parents1131z00_8, bool_t BgL_initzf31132zf3_9,
		bool_t BgL_magiczf31133zf3_10, obj_t BgL_z421134z42_11,
		obj_t BgL_alias1135z00_12, obj_t BgL_pointedzd2tozd2by1136z00_13,
		obj_t BgL_tvector1137z00_14, obj_t BgL_location1138z00_15,
		obj_t BgL_importzd2location1139zd2_16, int BgL_occurrence1140z00_17)
	{
		{	/* SawJvm/names.sch 50 */
			{	/* SawJvm/names.sch 50 */
				BgL_typez00_bglt BgL_new1136z00_3189;

				{	/* SawJvm/names.sch 50 */
					BgL_typez00_bglt BgL_tmp1134z00_3190;
					BgL_jvmbasicz00_bglt BgL_wide1135z00_3191;

					{
						BgL_typez00_bglt BgL_auxz00_3269;

						{	/* SawJvm/names.sch 50 */
							BgL_typez00_bglt BgL_new1133z00_3192;

							BgL_new1133z00_3192 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* SawJvm/names.sch 50 */
								long BgL_arg1472z00_3193;

								BgL_arg1472z00_3193 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1133z00_3192),
									BgL_arg1472z00_3193);
							}
							{	/* SawJvm/names.sch 50 */
								BgL_objectz00_bglt BgL_tmpz00_3274;

								BgL_tmpz00_3274 = ((BgL_objectz00_bglt) BgL_new1133z00_3192);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3274, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1133z00_3192);
							BgL_auxz00_3269 = BgL_new1133z00_3192;
						}
						BgL_tmp1134z00_3190 = ((BgL_typez00_bglt) BgL_auxz00_3269);
					}
					BgL_wide1135z00_3191 =
						((BgL_jvmbasicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jvmbasicz00_bgl))));
					{	/* SawJvm/names.sch 50 */
						obj_t BgL_auxz00_3282;
						BgL_objectz00_bglt BgL_tmpz00_3280;

						BgL_auxz00_3282 = ((obj_t) BgL_wide1135z00_3191);
						BgL_tmpz00_3280 = ((BgL_objectz00_bglt) BgL_tmp1134z00_3190);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3280, BgL_auxz00_3282);
					}
					((BgL_objectz00_bglt) BgL_tmp1134z00_3190);
					{	/* SawJvm/names.sch 50 */
						long BgL_arg1454z00_3194;

						BgL_arg1454z00_3194 =
							BGL_CLASS_NUM(BGl_jvmbasicz00zzsaw_jvm_namesz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1134z00_3190), BgL_arg1454z00_3194);
					}
					BgL_new1136z00_3189 = ((BgL_typez00_bglt) BgL_tmp1134z00_3190);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1136z00_3189)))->BgL_idz00) =
					((obj_t) BgL_id1126z00_3), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_namez00) =
					((obj_t) BgL_name1127z00_4), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1128za7_5), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_classz00) =
					((obj_t) BgL_class1129z00_6), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1130zd2_7), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_parentsz00) =
					((obj_t) BgL_parents1131z00_8), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31132zf3_9), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31133zf3_10), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_z42z42) =
					((obj_t) BgL_z421134z42_11), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_aliasz00) =
					((obj_t) BgL_alias1135z00_12), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1136z00_13), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1137z00_14), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_locationz00) =
					((obj_t) BgL_location1138z00_15), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1139zd2_16), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1136z00_3189)))->BgL_occurrencez00) =
					((int) BgL_occurrence1140z00_17), BUNSPEC);
				return BgL_new1136z00_3189;
			}
		}

	}



/* &make-jvmbasic */
	BgL_typez00_bglt BGl_z62makezd2jvmbasiczb0zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3054, obj_t BgL_id1126z00_3055, obj_t BgL_name1127z00_3056,
		obj_t BgL_siza7e1128za7_3057, obj_t BgL_class1129z00_3058,
		obj_t BgL_coercezd2to1130zd2_3059, obj_t BgL_parents1131z00_3060,
		obj_t BgL_initzf31132zf3_3061, obj_t BgL_magiczf31133zf3_3062,
		obj_t BgL_z421134z42_3063, obj_t BgL_alias1135z00_3064,
		obj_t BgL_pointedzd2tozd2by1136z00_3065, obj_t BgL_tvector1137z00_3066,
		obj_t BgL_location1138z00_3067, obj_t BgL_importzd2location1139zd2_3068,
		obj_t BgL_occurrence1140z00_3069)
	{
		{	/* SawJvm/names.sch 50 */
			return
				BGl_makezd2jvmbasiczd2zzsaw_jvm_namesz00(BgL_id1126z00_3055,
				BgL_name1127z00_3056, BgL_siza7e1128za7_3057, BgL_class1129z00_3058,
				BgL_coercezd2to1130zd2_3059, BgL_parents1131z00_3060,
				CBOOL(BgL_initzf31132zf3_3061), CBOOL(BgL_magiczf31133zf3_3062),
				BgL_z421134z42_3063, BgL_alias1135z00_3064,
				BgL_pointedzd2tozd2by1136z00_3065, BgL_tvector1137z00_3066,
				BgL_location1138z00_3067, BgL_importzd2location1139zd2_3068,
				CINT(BgL_occurrence1140z00_3069));
		}

	}



/* jvmbasic? */
	BGL_EXPORTED_DEF bool_t BGl_jvmbasiczf3zf3zzsaw_jvm_namesz00(obj_t
		BgL_objz00_18)
	{
		{	/* SawJvm/names.sch 51 */
			{	/* SawJvm/names.sch 51 */
				obj_t BgL_classz00_3195;

				BgL_classz00_3195 = BGl_jvmbasicz00zzsaw_jvm_namesz00;
				if (BGL_OBJECTP(BgL_objz00_18))
					{	/* SawJvm/names.sch 51 */
						BgL_objectz00_bglt BgL_arg1807z00_3196;

						BgL_arg1807z00_3196 = (BgL_objectz00_bglt) (BgL_objz00_18);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawJvm/names.sch 51 */
								long BgL_idxz00_3197;

								BgL_idxz00_3197 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3196);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3197 + 2L)) == BgL_classz00_3195);
							}
						else
							{	/* SawJvm/names.sch 51 */
								bool_t BgL_res1807z00_3200;

								{	/* SawJvm/names.sch 51 */
									obj_t BgL_oclassz00_3201;

									{	/* SawJvm/names.sch 51 */
										obj_t BgL_arg1815z00_3202;
										long BgL_arg1816z00_3203;

										BgL_arg1815z00_3202 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawJvm/names.sch 51 */
											long BgL_arg1817z00_3204;

											BgL_arg1817z00_3204 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3196);
											BgL_arg1816z00_3203 = (BgL_arg1817z00_3204 - OBJECT_TYPE);
										}
										BgL_oclassz00_3201 =
											VECTOR_REF(BgL_arg1815z00_3202, BgL_arg1816z00_3203);
									}
									{	/* SawJvm/names.sch 51 */
										bool_t BgL__ortest_1115z00_3205;

										BgL__ortest_1115z00_3205 =
											(BgL_classz00_3195 == BgL_oclassz00_3201);
										if (BgL__ortest_1115z00_3205)
											{	/* SawJvm/names.sch 51 */
												BgL_res1807z00_3200 = BgL__ortest_1115z00_3205;
											}
										else
											{	/* SawJvm/names.sch 51 */
												long BgL_odepthz00_3206;

												{	/* SawJvm/names.sch 51 */
													obj_t BgL_arg1804z00_3207;

													BgL_arg1804z00_3207 = (BgL_oclassz00_3201);
													BgL_odepthz00_3206 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3207);
												}
												if ((2L < BgL_odepthz00_3206))
													{	/* SawJvm/names.sch 51 */
														obj_t BgL_arg1802z00_3208;

														{	/* SawJvm/names.sch 51 */
															obj_t BgL_arg1803z00_3209;

															BgL_arg1803z00_3209 = (BgL_oclassz00_3201);
															BgL_arg1802z00_3208 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3209,
																2L);
														}
														BgL_res1807z00_3200 =
															(BgL_arg1802z00_3208 == BgL_classz00_3195);
													}
												else
													{	/* SawJvm/names.sch 51 */
														BgL_res1807z00_3200 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1807z00_3200;
							}
					}
				else
					{	/* SawJvm/names.sch 51 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &jvmbasic? */
	obj_t BGl_z62jvmbasiczf3z91zzsaw_jvm_namesz00(obj_t BgL_envz00_3070,
		obj_t BgL_objz00_3071)
	{
		{	/* SawJvm/names.sch 51 */
			return BBOOL(BGl_jvmbasiczf3zf3zzsaw_jvm_namesz00(BgL_objz00_3071));
		}

	}



/* jvmbasic-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_jvmbasiczd2nilzd2zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.sch 52 */
			{	/* SawJvm/names.sch 52 */
				obj_t BgL_classz00_2647;

				BgL_classz00_2647 = BGl_jvmbasicz00zzsaw_jvm_namesz00;
				{	/* SawJvm/names.sch 52 */
					obj_t BgL__ortest_1117z00_2648;

					BgL__ortest_1117z00_2648 = BGL_CLASS_NIL(BgL_classz00_2647);
					if (CBOOL(BgL__ortest_1117z00_2648))
						{	/* SawJvm/names.sch 52 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_2648);
						}
					else
						{	/* SawJvm/names.sch 52 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2647));
						}
				}
			}
		}

	}



/* &jvmbasic-nil */
	BgL_typez00_bglt BGl_z62jvmbasiczd2nilzb0zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3072)
	{
		{	/* SawJvm/names.sch 52 */
			return BGl_jvmbasiczd2nilzd2zzsaw_jvm_namesz00();
		}

	}



/* jvmbasic-occurrence */
	BGL_EXPORTED_DEF int
		BGl_jvmbasiczd2occurrencezd2zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_19)
	{
		{	/* SawJvm/names.sch 53 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_19)))->BgL_occurrencez00);
		}

	}



/* &jvmbasic-occurrence */
	obj_t BGl_z62jvmbasiczd2occurrencezb0zzsaw_jvm_namesz00(obj_t BgL_envz00_3073,
		obj_t BgL_oz00_3074)
	{
		{	/* SawJvm/names.sch 53 */
			return
				BINT(BGl_jvmbasiczd2occurrencezd2zzsaw_jvm_namesz00(
					((BgL_typez00_bglt) BgL_oz00_3074)));
		}

	}



/* jvmbasic-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2occurrencezd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_20, int BgL_vz00_21)
	{
		{	/* SawJvm/names.sch 54 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_20)))->BgL_occurrencez00) =
				((int) BgL_vz00_21), BUNSPEC);
		}

	}



/* &jvmbasic-occurrence-set! */
	obj_t BGl_z62jvmbasiczd2occurrencezd2setz12z70zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3075, obj_t BgL_oz00_3076, obj_t BgL_vz00_3077)
	{
		{	/* SawJvm/names.sch 54 */
			return
				BGl_jvmbasiczd2occurrencezd2setz12z12zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3076), CINT(BgL_vz00_3077));
		}

	}



/* jvmbasic-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2importzd2locationz00zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_22)
	{
		{	/* SawJvm/names.sch 55 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_22)))->BgL_importzd2locationzd2);
		}

	}



/* &jvmbasic-import-location */
	obj_t BGl_z62jvmbasiczd2importzd2locationz62zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3078, obj_t BgL_oz00_3079)
	{
		{	/* SawJvm/names.sch 55 */
			return
				BGl_jvmbasiczd2importzd2locationz00zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3079));
		}

	}



/* jvmbasic-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2importzd2locationzd2setz12zc0zzsaw_jvm_namesz00
		(BgL_typez00_bglt BgL_oz00_23, obj_t BgL_vz00_24)
	{
		{	/* SawJvm/names.sch 56 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_23)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_24), BUNSPEC);
		}

	}



/* &jvmbasic-import-location-set! */
	obj_t BGl_z62jvmbasiczd2importzd2locationzd2setz12za2zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3080, obj_t BgL_oz00_3081, obj_t BgL_vz00_3082)
	{
		{	/* SawJvm/names.sch 56 */
			return
				BGl_jvmbasiczd2importzd2locationzd2setz12zc0zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3081), BgL_vz00_3082);
		}

	}



/* jvmbasic-location */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2locationzd2zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_25)
	{
		{	/* SawJvm/names.sch 57 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_25)))->BgL_locationz00);
		}

	}



/* &jvmbasic-location */
	obj_t BGl_z62jvmbasiczd2locationzb0zzsaw_jvm_namesz00(obj_t BgL_envz00_3083,
		obj_t BgL_oz00_3084)
	{
		{	/* SawJvm/names.sch 57 */
			return
				BGl_jvmbasiczd2locationzd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3084));
		}

	}



/* jvmbasic-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2locationzd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawJvm/names.sch 58 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_26)))->BgL_locationz00) =
				((obj_t) BgL_vz00_27), BUNSPEC);
		}

	}



/* &jvmbasic-location-set! */
	obj_t BGl_z62jvmbasiczd2locationzd2setz12z70zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3085, obj_t BgL_oz00_3086, obj_t BgL_vz00_3087)
	{
		{	/* SawJvm/names.sch 58 */
			return
				BGl_jvmbasiczd2locationzd2setz12z12zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3086), BgL_vz00_3087);
		}

	}



/* jvmbasic-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2tvectorzd2zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_28)
	{
		{	/* SawJvm/names.sch 59 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_28)))->BgL_tvectorz00);
		}

	}



/* &jvmbasic-tvector */
	obj_t BGl_z62jvmbasiczd2tvectorzb0zzsaw_jvm_namesz00(obj_t BgL_envz00_3088,
		obj_t BgL_oz00_3089)
	{
		{	/* SawJvm/names.sch 59 */
			return
				BGl_jvmbasiczd2tvectorzd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3089));
		}

	}



/* jvmbasic-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2tvectorzd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawJvm/names.sch 60 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_29)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_30), BUNSPEC);
		}

	}



/* &jvmbasic-tvector-set! */
	obj_t BGl_z62jvmbasiczd2tvectorzd2setz12z70zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3090, obj_t BgL_oz00_3091, obj_t BgL_vz00_3092)
	{
		{	/* SawJvm/names.sch 60 */
			return
				BGl_jvmbasiczd2tvectorzd2setz12z12zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3091), BgL_vz00_3092);
		}

	}



/* jvmbasic-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2pointedzd2tozd2byzd2zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_31)
	{
		{	/* SawJvm/names.sch 61 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_31)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &jvmbasic-pointed-to-by */
	obj_t BGl_z62jvmbasiczd2pointedzd2tozd2byzb0zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3093, obj_t BgL_oz00_3094)
	{
		{	/* SawJvm/names.sch 61 */
			return
				BGl_jvmbasiczd2pointedzd2tozd2byzd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3094));
		}

	}



/* jvmbasic-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2pointedzd2tozd2byzd2setz12z12zzsaw_jvm_namesz00
		(BgL_typez00_bglt BgL_oz00_32, obj_t BgL_vz00_33)
	{
		{	/* SawJvm/names.sch 62 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_32)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_33), BUNSPEC);
		}

	}



/* &jvmbasic-pointed-to-by-set! */
	obj_t BGl_z62jvmbasiczd2pointedzd2tozd2byzd2setz12z70zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3095, obj_t BgL_oz00_3096, obj_t BgL_vz00_3097)
	{
		{	/* SawJvm/names.sch 62 */
			return
				BGl_jvmbasiczd2pointedzd2tozd2byzd2setz12z12zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3096), BgL_vz00_3097);
		}

	}



/* jvmbasic-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2aliaszd2zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_34)
	{
		{	/* SawJvm/names.sch 63 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_34)))->BgL_aliasz00);
		}

	}



/* &jvmbasic-alias */
	obj_t BGl_z62jvmbasiczd2aliaszb0zzsaw_jvm_namesz00(obj_t BgL_envz00_3098,
		obj_t BgL_oz00_3099)
	{
		{	/* SawJvm/names.sch 63 */
			return
				BGl_jvmbasiczd2aliaszd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3099));
		}

	}



/* jvmbasic-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2aliaszd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_35, obj_t BgL_vz00_36)
	{
		{	/* SawJvm/names.sch 64 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_35)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &jvmbasic-alias-set! */
	obj_t BGl_z62jvmbasiczd2aliaszd2setz12z70zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3100, obj_t BgL_oz00_3101, obj_t BgL_vz00_3102)
	{
		{	/* SawJvm/names.sch 64 */
			return
				BGl_jvmbasiczd2aliaszd2setz12z12zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3101), BgL_vz00_3102);
		}

	}



/* jvmbasic-$ */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2z42z90zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_37)
	{
		{	/* SawJvm/names.sch 65 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_37)))->BgL_z42z42);
		}

	}



/* &jvmbasic-$ */
	obj_t BGl_z62jvmbasiczd2z42zf2zzsaw_jvm_namesz00(obj_t BgL_envz00_3103,
		obj_t BgL_oz00_3104)
	{
		{	/* SawJvm/names.sch 65 */
			return
				BGl_jvmbasiczd2z42z90zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3104));
		}

	}



/* jvmbasic-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2z42zd2setz12z50zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_38, obj_t BgL_vz00_39)
	{
		{	/* SawJvm/names.sch 66 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_38)))->BgL_z42z42) =
				((obj_t) BgL_vz00_39), BUNSPEC);
		}

	}



/* &jvmbasic-$-set! */
	obj_t BGl_z62jvmbasiczd2z42zd2setz12z32zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3105, obj_t BgL_oz00_3106, obj_t BgL_vz00_3107)
	{
		{	/* SawJvm/names.sch 66 */
			return
				BGl_jvmbasiczd2z42zd2setz12z50zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3106), BgL_vz00_3107);
		}

	}



/* jvmbasic-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_jvmbasiczd2magiczf3z21zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_40)
	{
		{	/* SawJvm/names.sch 67 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_40)))->BgL_magiczf3zf3);
		}

	}



/* &jvmbasic-magic? */
	obj_t BGl_z62jvmbasiczd2magiczf3z43zzsaw_jvm_namesz00(obj_t BgL_envz00_3108,
		obj_t BgL_oz00_3109)
	{
		{	/* SawJvm/names.sch 67 */
			return
				BBOOL(BGl_jvmbasiczd2magiczf3z21zzsaw_jvm_namesz00(
					((BgL_typez00_bglt) BgL_oz00_3109)));
		}

	}



/* jvmbasic-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2magiczf3zd2setz12ze1zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_41, bool_t BgL_vz00_42)
	{
		{	/* SawJvm/names.sch 68 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_41)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_42), BUNSPEC);
		}

	}



/* &jvmbasic-magic?-set! */
	obj_t BGl_z62jvmbasiczd2magiczf3zd2setz12z83zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3110, obj_t BgL_oz00_3111, obj_t BgL_vz00_3112)
	{
		{	/* SawJvm/names.sch 68 */
			return
				BGl_jvmbasiczd2magiczf3zd2setz12ze1zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3111), CBOOL(BgL_vz00_3112));
		}

	}



/* jvmbasic-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_jvmbasiczd2initzf3z21zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_43)
	{
		{	/* SawJvm/names.sch 69 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_43)))->BgL_initzf3zf3);
		}

	}



/* &jvmbasic-init? */
	obj_t BGl_z62jvmbasiczd2initzf3z43zzsaw_jvm_namesz00(obj_t BgL_envz00_3113,
		obj_t BgL_oz00_3114)
	{
		{	/* SawJvm/names.sch 69 */
			return
				BBOOL(BGl_jvmbasiczd2initzf3z21zzsaw_jvm_namesz00(
					((BgL_typez00_bglt) BgL_oz00_3114)));
		}

	}



/* jvmbasic-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2initzf3zd2setz12ze1zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_44, bool_t BgL_vz00_45)
	{
		{	/* SawJvm/names.sch 70 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_44)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &jvmbasic-init?-set! */
	obj_t BGl_z62jvmbasiczd2initzf3zd2setz12z83zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3115, obj_t BgL_oz00_3116, obj_t BgL_vz00_3117)
	{
		{	/* SawJvm/names.sch 70 */
			return
				BGl_jvmbasiczd2initzf3zd2setz12ze1zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3116), CBOOL(BgL_vz00_3117));
		}

	}



/* jvmbasic-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2parentszd2zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_46)
	{
		{	/* SawJvm/names.sch 71 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_46)))->BgL_parentsz00);
		}

	}



/* &jvmbasic-parents */
	obj_t BGl_z62jvmbasiczd2parentszb0zzsaw_jvm_namesz00(obj_t BgL_envz00_3118,
		obj_t BgL_oz00_3119)
	{
		{	/* SawJvm/names.sch 71 */
			return
				BGl_jvmbasiczd2parentszd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3119));
		}

	}



/* jvmbasic-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2parentszd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawJvm/names.sch 72 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_47)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &jvmbasic-parents-set! */
	obj_t BGl_z62jvmbasiczd2parentszd2setz12z70zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3120, obj_t BgL_oz00_3121, obj_t BgL_vz00_3122)
	{
		{	/* SawJvm/names.sch 72 */
			return
				BGl_jvmbasiczd2parentszd2setz12z12zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3121), BgL_vz00_3122);
		}

	}



/* jvmbasic-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2coercezd2toz00zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_49)
	{
		{	/* SawJvm/names.sch 73 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_49)))->BgL_coercezd2tozd2);
		}

	}



/* &jvmbasic-coerce-to */
	obj_t BGl_z62jvmbasiczd2coercezd2toz62zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3123, obj_t BgL_oz00_3124)
	{
		{	/* SawJvm/names.sch 73 */
			return
				BGl_jvmbasiczd2coercezd2toz00zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3124));
		}

	}



/* jvmbasic-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2coercezd2tozd2setz12zc0zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_50, obj_t BgL_vz00_51)
	{
		{	/* SawJvm/names.sch 74 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_50)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_51), BUNSPEC);
		}

	}



/* &jvmbasic-coerce-to-set! */
	obj_t BGl_z62jvmbasiczd2coercezd2tozd2setz12za2zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3125, obj_t BgL_oz00_3126, obj_t BgL_vz00_3127)
	{
		{	/* SawJvm/names.sch 74 */
			return
				BGl_jvmbasiczd2coercezd2tozd2setz12zc0zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3126), BgL_vz00_3127);
		}

	}



/* jvmbasic-class */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2classzd2zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_52)
	{
		{	/* SawJvm/names.sch 75 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_52)))->BgL_classz00);
		}

	}



/* &jvmbasic-class */
	obj_t BGl_z62jvmbasiczd2classzb0zzsaw_jvm_namesz00(obj_t BgL_envz00_3128,
		obj_t BgL_oz00_3129)
	{
		{	/* SawJvm/names.sch 75 */
			return
				BGl_jvmbasiczd2classzd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3129));
		}

	}



/* jvmbasic-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2classzd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_53, obj_t BgL_vz00_54)
	{
		{	/* SawJvm/names.sch 76 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_53)))->BgL_classz00) =
				((obj_t) BgL_vz00_54), BUNSPEC);
		}

	}



/* &jvmbasic-class-set! */
	obj_t BGl_z62jvmbasiczd2classzd2setz12z70zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3130, obj_t BgL_oz00_3131, obj_t BgL_vz00_3132)
	{
		{	/* SawJvm/names.sch 76 */
			return
				BGl_jvmbasiczd2classzd2setz12z12zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3131), BgL_vz00_3132);
		}

	}



/* jvmbasic-size */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2siza7ez75zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_55)
	{
		{	/* SawJvm/names.sch 77 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_55)))->BgL_siza7eza7);
		}

	}



/* &jvmbasic-size */
	obj_t BGl_z62jvmbasiczd2siza7ez17zzsaw_jvm_namesz00(obj_t BgL_envz00_3133,
		obj_t BgL_oz00_3134)
	{
		{	/* SawJvm/names.sch 77 */
			return
				BGl_jvmbasiczd2siza7ez75zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3134));
		}

	}



/* jvmbasic-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2siza7ezd2setz12zb5zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_56, obj_t BgL_vz00_57)
	{
		{	/* SawJvm/names.sch 78 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_56)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_57), BUNSPEC);
		}

	}



/* &jvmbasic-size-set! */
	obj_t BGl_z62jvmbasiczd2siza7ezd2setz12zd7zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3135, obj_t BgL_oz00_3136, obj_t BgL_vz00_3137)
	{
		{	/* SawJvm/names.sch 78 */
			return
				BGl_jvmbasiczd2siza7ezd2setz12zb5zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3136), BgL_vz00_3137);
		}

	}



/* jvmbasic-name */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2namezd2zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_oz00_58)
	{
		{	/* SawJvm/names.sch 79 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_58)))->BgL_namez00);
		}

	}



/* &jvmbasic-name */
	obj_t BGl_z62jvmbasiczd2namezb0zzsaw_jvm_namesz00(obj_t BgL_envz00_3138,
		obj_t BgL_oz00_3139)
	{
		{	/* SawJvm/names.sch 79 */
			return
				BGl_jvmbasiczd2namezd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3139));
		}

	}



/* jvmbasic-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jvmbasiczd2namezd2setz12z12zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawJvm/names.sch 80 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_59)))->BgL_namez00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &jvmbasic-name-set! */
	obj_t BGl_z62jvmbasiczd2namezd2setz12z70zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3140, obj_t BgL_oz00_3141, obj_t BgL_vz00_3142)
	{
		{	/* SawJvm/names.sch 80 */
			return
				BGl_jvmbasiczd2namezd2setz12z12zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3141), BgL_vz00_3142);
		}

	}



/* jvmbasic-id */
	BGL_EXPORTED_DEF obj_t BGl_jvmbasiczd2idzd2zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_oz00_61)
	{
		{	/* SawJvm/names.sch 81 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_61)))->BgL_idz00);
		}

	}



/* &jvmbasic-id */
	obj_t BGl_z62jvmbasiczd2idzb0zzsaw_jvm_namesz00(obj_t BgL_envz00_3143,
		obj_t BgL_oz00_3144)
	{
		{	/* SawJvm/names.sch 81 */
			return
				BGl_jvmbasiczd2idzd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_oz00_3144));
		}

	}



/* names-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_nameszd2initializa7ationz75zzsaw_jvm_namesz00(BgL_jvmz00_bglt
		BgL_mez00_64)
	{
		{	/* SawJvm/names.scm 22 */
			BGl_resetzd2jvmstdzd2typez12z12zzsaw_jvm_namesz00();
			BGl_forzd2eachzd2globalz12z12zzast_envz00
				(BGl_resetzd2globalz12zd2envz12zzbackend_cplibz00, BNIL);
			return
				BGl_forzd2eachzd2typez12z12zztype_envz00
				(BGl_saw_jvmzd2setzd2typezd2namesz12zd2envz12zzsaw_jvm_namesz00);
		}

	}



/* &names-initialization */
	obj_t BGl_z62nameszd2initializa7ationz17zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3145, obj_t BgL_mez00_3146)
	{
		{	/* SawJvm/names.scm 22 */
			return
				BGl_nameszd2initializa7ationz75zzsaw_jvm_namesz00(
				((BgL_jvmz00_bglt) BgL_mez00_3146));
		}

	}



/* reset-jvmstd-type! */
	bool_t BGl_resetzd2jvmstdzd2typez12z12zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.scm 31 */
			{	/* SawJvm/names.scm 33 */
				obj_t BgL_g1427z00_2146;

				BgL_g1427z00_2146 = CNST_TABLE_REF(0);
				{
					obj_t BgL_l1425z00_2148;

					BgL_l1425z00_2148 = BgL_g1427z00_2146;
				BgL_zc3z04anonymousza31474ze3z87_2149:
					if (PAIRP(BgL_l1425z00_2148))
						{	/* SawJvm/names.scm 34 */
							{	/* SawJvm/names.scm 33 */
								obj_t BgL_xz00_2151;

								BgL_xz00_2151 = CAR(BgL_l1425z00_2148);
								{	/* SawJvm/names.scm 33 */
									BgL_typez00_bglt BgL_arg1485z00_2152;

									{	/* SawJvm/names.scm 33 */
										BgL_typez00_bglt BgL_tmp1137z00_2153;

										BgL_tmp1137z00_2153 =
											BGl_findzd2typezd2zztype_envz00(BgL_xz00_2151);
										{	/* SawJvm/names.scm 33 */
											BgL_jvmbasicz00_bglt BgL_wide1139z00_2155;

											BgL_wide1139z00_2155 =
												((BgL_jvmbasicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_jvmbasicz00_bgl))));
											{	/* SawJvm/names.scm 33 */
												obj_t BgL_auxz00_3493;
												BgL_objectz00_bglt BgL_tmpz00_3490;

												BgL_auxz00_3493 = ((obj_t) BgL_wide1139z00_2155);
												BgL_tmpz00_3490 =
													((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_tmp1137z00_2153));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3490,
													BgL_auxz00_3493);
											}
											((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_tmp1137z00_2153));
											{	/* SawJvm/names.scm 33 */
												long BgL_arg1489z00_2156;

												BgL_arg1489z00_2156 =
													BGL_CLASS_NUM(BGl_jvmbasicz00zzsaw_jvm_namesz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_tmp1137z00_2153)),
													BgL_arg1489z00_2156);
											}
											((BgL_typez00_bglt)
												((BgL_typez00_bglt) BgL_tmp1137z00_2153));
										}
										BgL_arg1485z00_2152 =
											((BgL_typez00_bglt)
											((BgL_typez00_bglt) BgL_tmp1137z00_2153));
									}
									((((BgL_typez00_bglt) COBJECT(BgL_arg1485z00_2152))->
											BgL_namez00) = ((obj_t) BgL_xz00_2151), BUNSPEC);
							}}
							{
								obj_t BgL_l1425z00_3507;

								BgL_l1425z00_3507 = CDR(BgL_l1425z00_2148);
								BgL_l1425z00_2148 = BgL_l1425z00_3507;
								goto BgL_zc3z04anonymousza31474ze3z87_2149;
							}
						}
					else
						{	/* SawJvm/names.scm 34 */
							((bool_t) 1);
						}
				}
			}
			{	/* SawJvm/names.scm 36 */
				obj_t BgL_g1431z00_2160;
				obj_t BgL_g1432z00_2161;

				BgL_g1431z00_2160 = CNST_TABLE_REF(1);
				BgL_g1432z00_2161 = CNST_TABLE_REF(2);
				{
					obj_t BgL_ll1428z00_2163;
					obj_t BgL_ll1429z00_2164;

					BgL_ll1428z00_2163 = BgL_g1431z00_2160;
					BgL_ll1429z00_2164 = BgL_g1432z00_2161;
				BgL_zc3z04anonymousza31503ze3z87_2165:
					if (NULLP(BgL_ll1428z00_2163))
						{	/* SawJvm/names.scm 37 */
							((bool_t) 1);
						}
					else
						{	/* SawJvm/names.scm 37 */
							{	/* SawJvm/names.scm 36 */
								obj_t BgL_xz00_2167;
								obj_t BgL_sz00_2168;

								BgL_xz00_2167 = CAR(((obj_t) BgL_ll1428z00_2163));
								BgL_sz00_2168 = CAR(((obj_t) BgL_ll1429z00_2164));
								{	/* SawJvm/names.scm 36 */
									BgL_typez00_bglt BgL_arg1509z00_2169;

									{	/* SawJvm/names.scm 36 */
										BgL_typez00_bglt BgL_tmp1141z00_2170;

										BgL_tmp1141z00_2170 =
											BGl_findzd2typezd2zztype_envz00(BgL_xz00_2167);
										{	/* SawJvm/names.scm 36 */
											BgL_jvmbasicz00_bglt BgL_wide1143z00_2172;

											BgL_wide1143z00_2172 =
												((BgL_jvmbasicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_jvmbasicz00_bgl))));
											{	/* SawJvm/names.scm 36 */
												obj_t BgL_auxz00_3522;
												BgL_objectz00_bglt BgL_tmpz00_3519;

												BgL_auxz00_3522 = ((obj_t) BgL_wide1143z00_2172);
												BgL_tmpz00_3519 =
													((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_tmp1141z00_2170));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3519,
													BgL_auxz00_3522);
											}
											((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_tmp1141z00_2170));
											{	/* SawJvm/names.scm 36 */
												long BgL_arg1513z00_2173;

												BgL_arg1513z00_2173 =
													BGL_CLASS_NUM(BGl_jvmbasicz00zzsaw_jvm_namesz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_tmp1141z00_2170)),
													BgL_arg1513z00_2173);
											}
											((BgL_typez00_bglt)
												((BgL_typez00_bglt) BgL_tmp1141z00_2170));
										}
										BgL_arg1509z00_2169 =
											((BgL_typez00_bglt)
											((BgL_typez00_bglt) BgL_tmp1141z00_2170));
									}
									((((BgL_typez00_bglt) COBJECT(BgL_arg1509z00_2169))->
											BgL_namez00) = ((obj_t) BgL_sz00_2168), BUNSPEC);
							}}
							{	/* SawJvm/names.scm 37 */
								obj_t BgL_arg1514z00_2175;
								obj_t BgL_arg1516z00_2176;

								BgL_arg1514z00_2175 = CDR(((obj_t) BgL_ll1428z00_2163));
								BgL_arg1516z00_2176 = CDR(((obj_t) BgL_ll1429z00_2164));
								{
									obj_t BgL_ll1429z00_3541;
									obj_t BgL_ll1428z00_3540;

									BgL_ll1428z00_3540 = BgL_arg1514z00_2175;
									BgL_ll1429z00_3541 = BgL_arg1516z00_2176;
									BgL_ll1429z00_2164 = BgL_ll1429z00_3541;
									BgL_ll1428z00_2163 = BgL_ll1428z00_3540;
									goto BgL_zc3z04anonymousza31503ze3z87_2165;
								}
							}
						}
				}
			}
			{	/* SawJvm/names.scm 40 */
				obj_t BgL_g1436z00_2178;
				obj_t BgL_g1437z00_2179;

				BgL_g1436z00_2178 = CNST_TABLE_REF(3);
				BgL_g1437z00_2179 = CNST_TABLE_REF(4);
				{
					obj_t BgL_ll1433z00_2181;
					obj_t BgL_ll1434z00_2182;

					BgL_ll1433z00_2181 = BgL_g1436z00_2178;
					BgL_ll1434z00_2182 = BgL_g1437z00_2179;
				BgL_zc3z04anonymousza31517ze3z87_2183:
					if (NULLP(BgL_ll1433z00_2181))
						{	/* SawJvm/names.scm 42 */
							((bool_t) 1);
						}
					else
						{	/* SawJvm/names.scm 42 */
							{	/* SawJvm/names.scm 41 */
								obj_t BgL_vz00_2185;
								obj_t BgL_iz00_2186;

								BgL_vz00_2185 = CAR(((obj_t) BgL_ll1433z00_2181));
								BgL_iz00_2186 = CAR(((obj_t) BgL_ll1434z00_2182));
								{	/* SawJvm/names.scm 41 */
									BgL_typez00_bglt BgL_tmp1145z00_2187;

									BgL_tmp1145z00_2187 =
										BGl_findzd2typezd2zztype_envz00(BgL_vz00_2185);
									{	/* SawJvm/names.scm 41 */
										BgL_tvecz00_bglt BgL_wide1147z00_2189;

										BgL_wide1147z00_2189 =
											((BgL_tvecz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_tvecz00_bgl))));
										{	/* SawJvm/names.scm 41 */
											obj_t BgL_auxz00_3555;
											BgL_objectz00_bglt BgL_tmpz00_3552;

											BgL_auxz00_3555 = ((obj_t) BgL_wide1147z00_2189);
											BgL_tmpz00_3552 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_tmp1145z00_2187));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3552, BgL_auxz00_3555);
										}
										((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_tmp1145z00_2187));
										{	/* SawJvm/names.scm 41 */
											long BgL_arg1535z00_2190;

											BgL_arg1535z00_2190 =
												BGL_CLASS_NUM(BGl_tvecz00zztvector_tvectorz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_tmp1145z00_2187)),
												BgL_arg1535z00_2190);
										}
										((BgL_typez00_bglt)
											((BgL_typez00_bglt) BgL_tmp1145z00_2187));
									}
									{
										BgL_tvecz00_bglt BgL_auxz00_3566;

										{
											obj_t BgL_auxz00_3567;

											{	/* SawJvm/names.scm 41 */
												BgL_objectz00_bglt BgL_tmpz00_3568;

												BgL_tmpz00_3568 =
													((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_tmp1145z00_2187));
												BgL_auxz00_3567 = BGL_OBJECT_WIDENING(BgL_tmpz00_3568);
											}
											BgL_auxz00_3566 = ((BgL_tvecz00_bglt) BgL_auxz00_3567);
										}
										((((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_3566))->
												BgL_itemzd2typezd2) =
											((BgL_typez00_bglt)
												BGl_findzd2typezd2zztype_envz00(BgL_iz00_2186)),
											BUNSPEC);
									}
									((BgL_typez00_bglt) BgL_tmp1145z00_2187);
							}}
							{	/* SawJvm/names.scm 42 */
								obj_t BgL_arg1540z00_2192;
								obj_t BgL_arg1544z00_2193;

								BgL_arg1540z00_2192 = CDR(((obj_t) BgL_ll1433z00_2181));
								BgL_arg1544z00_2193 = CDR(((obj_t) BgL_ll1434z00_2182));
								{
									obj_t BgL_ll1434z00_3581;
									obj_t BgL_ll1433z00_3580;

									BgL_ll1433z00_3580 = BgL_arg1540z00_2192;
									BgL_ll1434z00_3581 = BgL_arg1544z00_2193;
									BgL_ll1434z00_2182 = BgL_ll1434z00_3581;
									BgL_ll1433z00_2181 = BgL_ll1433z00_3580;
									goto BgL_zc3z04anonymousza31517ze3z87_2183;
								}
							}
						}
				}
			}
			{	/* SawJvm/names.scm 45 */
				obj_t BgL_g1440z00_2195;

				BgL_g1440z00_2195 = CNST_TABLE_REF(5);
				{
					obj_t BgL_l1438z00_2197;

					BgL_l1438z00_2197 = BgL_g1440z00_2195;
				BgL_zc3z04anonymousza31545ze3z87_2198:
					if (PAIRP(BgL_l1438z00_2197))
						{	/* SawJvm/names.scm 46 */
							{	/* SawJvm/names.scm 45 */
								obj_t BgL_sz00_2200;

								BgL_sz00_2200 = CAR(BgL_l1438z00_2197);
								{	/* SawJvm/names.scm 45 */
									BgL_typez00_bglt BgL_arg1552z00_2201;
									obj_t BgL_arg1553z00_2202;

									{	/* SawJvm/names.scm 45 */
										obj_t BgL_arg1559z00_2203;

										BgL_arg1559z00_2203 = CAR(((obj_t) BgL_sz00_2200));
										BgL_arg1552z00_2201 =
											BGl_findzd2typezd2zztype_envz00(BgL_arg1559z00_2203);
									}
									BgL_arg1553z00_2202 = CDR(((obj_t) BgL_sz00_2200));
									((((BgL_typez00_bglt) COBJECT(BgL_arg1552z00_2201))->
											BgL_namez00) = ((obj_t) BgL_arg1553z00_2202), BUNSPEC);
								}
							}
							{
								obj_t BgL_l1438z00_3592;

								BgL_l1438z00_3592 = CDR(BgL_l1438z00_2197);
								BgL_l1438z00_2197 = BgL_l1438z00_3592;
								goto BgL_zc3z04anonymousza31545ze3z87_2198;
							}
						}
					else
						{	/* SawJvm/names.scm 46 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* &saw_jvm-set-type-names! */
	obj_t BGl_z62saw_jvmzd2setzd2typezd2namesz12za2zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3149, obj_t BgL_typez00_3150)
	{
		{	/* SawJvm/names.scm 67 */
			return
				BGl_getzd2jvmtypezd2zzsaw_jvm_namesz00(
				((BgL_typez00_bglt) BgL_typez00_3150));
		}

	}



/* get-jvmtype */
	obj_t BGl_getzd2jvmtypezd2zzsaw_jvm_namesz00(BgL_typez00_bglt BgL_typez00_66)
	{
		{	/* SawJvm/names.scm 70 */
			{	/* SawJvm/names.scm 71 */
				obj_t BgL_namez00_2206;

				BgL_namez00_2206 =
					(((BgL_typez00_bglt) COBJECT(BgL_typez00_66))->BgL_namez00);
				if (SYMBOLP(BgL_namez00_2206))
					{	/* SawJvm/names.scm 72 */
						return BgL_namez00_2206;
					}
				else
					{	/* SawJvm/names.scm 74 */
						obj_t BgL_jtypez00_2208;

						BgL_jtypez00_2208 =
							BGl_buildzd2typezd2namez00zzsaw_jvm_namesz00(BgL_typez00_66);
						((((BgL_typez00_bglt) COBJECT(BgL_typez00_66))->BgL_namez00) =
							((obj_t) BgL_jtypez00_2208), BUNSPEC);
						return BgL_jtypez00_2208;
					}
			}
		}

	}



/* build-type-name */
	obj_t BGl_buildzd2typezd2namez00zzsaw_jvm_namesz00(BgL_typez00_bglt
		BgL_typez00_67)
	{
		{	/* SawJvm/names.scm 78 */
			{	/* SawJvm/names.scm 80 */
				bool_t BgL_test1885z00_3601;

				{	/* SawJvm/names.scm 80 */
					obj_t BgL_classz00_2682;

					BgL_classz00_2682 = BGl_tclassz00zzobject_classz00;
					{	/* SawJvm/names.scm 80 */
						BgL_objectz00_bglt BgL_arg1807z00_2684;

						{	/* SawJvm/names.scm 80 */
							obj_t BgL_tmpz00_3602;

							BgL_tmpz00_3602 = ((obj_t) BgL_typez00_67);
							BgL_arg1807z00_2684 = (BgL_objectz00_bglt) (BgL_tmpz00_3602);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawJvm/names.scm 80 */
								long BgL_idxz00_2690;

								BgL_idxz00_2690 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2684);
								BgL_test1885z00_3601 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2690 + 2L)) == BgL_classz00_2682);
							}
						else
							{	/* SawJvm/names.scm 80 */
								bool_t BgL_res1808z00_2715;

								{	/* SawJvm/names.scm 80 */
									obj_t BgL_oclassz00_2698;

									{	/* SawJvm/names.scm 80 */
										obj_t BgL_arg1815z00_2706;
										long BgL_arg1816z00_2707;

										BgL_arg1815z00_2706 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawJvm/names.scm 80 */
											long BgL_arg1817z00_2708;

											BgL_arg1817z00_2708 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2684);
											BgL_arg1816z00_2707 = (BgL_arg1817z00_2708 - OBJECT_TYPE);
										}
										BgL_oclassz00_2698 =
											VECTOR_REF(BgL_arg1815z00_2706, BgL_arg1816z00_2707);
									}
									{	/* SawJvm/names.scm 80 */
										bool_t BgL__ortest_1115z00_2699;

										BgL__ortest_1115z00_2699 =
											(BgL_classz00_2682 == BgL_oclassz00_2698);
										if (BgL__ortest_1115z00_2699)
											{	/* SawJvm/names.scm 80 */
												BgL_res1808z00_2715 = BgL__ortest_1115z00_2699;
											}
										else
											{	/* SawJvm/names.scm 80 */
												long BgL_odepthz00_2700;

												{	/* SawJvm/names.scm 80 */
													obj_t BgL_arg1804z00_2701;

													BgL_arg1804z00_2701 = (BgL_oclassz00_2698);
													BgL_odepthz00_2700 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2701);
												}
												if ((2L < BgL_odepthz00_2700))
													{	/* SawJvm/names.scm 80 */
														obj_t BgL_arg1802z00_2703;

														{	/* SawJvm/names.scm 80 */
															obj_t BgL_arg1803z00_2704;

															BgL_arg1803z00_2704 = (BgL_oclassz00_2698);
															BgL_arg1802z00_2703 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2704,
																2L);
														}
														BgL_res1808z00_2715 =
															(BgL_arg1802z00_2703 == BgL_classz00_2682);
													}
												else
													{	/* SawJvm/names.scm 80 */
														BgL_res1808z00_2715 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test1885z00_3601 = BgL_res1808z00_2715;
							}
					}
				}
				if (BgL_test1885z00_3601)
					{	/* SawJvm/names.scm 80 */
						if (
							((((BgL_typez00_bglt) COBJECT(BgL_typez00_67))->BgL_idz00) ==
								CNST_TABLE_REF(6)))
							{	/* SawJvm/names.scm 81 */
								return CNST_TABLE_REF(6);
							}
						else
							{	/* SawJvm/names.scm 81 */
								return
									BGl_qualifiedzd2tclasszd2namez00zzbackend_cplibz00(
									((BgL_typez00_bglt) BgL_typez00_67));
							}
					}
				else
					{	/* SawJvm/names.scm 84 */
						bool_t BgL_test1890z00_3631;

						{	/* SawJvm/names.scm 84 */
							obj_t BgL_classz00_2717;

							BgL_classz00_2717 = BGl_wclassz00zzobject_classz00;
							{	/* SawJvm/names.scm 84 */
								BgL_objectz00_bglt BgL_arg1807z00_2719;

								{	/* SawJvm/names.scm 84 */
									obj_t BgL_tmpz00_3632;

									BgL_tmpz00_3632 = ((obj_t) BgL_typez00_67);
									BgL_arg1807z00_2719 = (BgL_objectz00_bglt) (BgL_tmpz00_3632);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawJvm/names.scm 84 */
										long BgL_idxz00_2725;

										BgL_idxz00_2725 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2719);
										BgL_test1890z00_3631 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2725 + 2L)) == BgL_classz00_2717);
									}
								else
									{	/* SawJvm/names.scm 84 */
										bool_t BgL_res1809z00_2750;

										{	/* SawJvm/names.scm 84 */
											obj_t BgL_oclassz00_2733;

											{	/* SawJvm/names.scm 84 */
												obj_t BgL_arg1815z00_2741;
												long BgL_arg1816z00_2742;

												BgL_arg1815z00_2741 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawJvm/names.scm 84 */
													long BgL_arg1817z00_2743;

													BgL_arg1817z00_2743 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2719);
													BgL_arg1816z00_2742 =
														(BgL_arg1817z00_2743 - OBJECT_TYPE);
												}
												BgL_oclassz00_2733 =
													VECTOR_REF(BgL_arg1815z00_2741, BgL_arg1816z00_2742);
											}
											{	/* SawJvm/names.scm 84 */
												bool_t BgL__ortest_1115z00_2734;

												BgL__ortest_1115z00_2734 =
													(BgL_classz00_2717 == BgL_oclassz00_2733);
												if (BgL__ortest_1115z00_2734)
													{	/* SawJvm/names.scm 84 */
														BgL_res1809z00_2750 = BgL__ortest_1115z00_2734;
													}
												else
													{	/* SawJvm/names.scm 84 */
														long BgL_odepthz00_2735;

														{	/* SawJvm/names.scm 84 */
															obj_t BgL_arg1804z00_2736;

															BgL_arg1804z00_2736 = (BgL_oclassz00_2733);
															BgL_odepthz00_2735 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2736);
														}
														if ((2L < BgL_odepthz00_2735))
															{	/* SawJvm/names.scm 84 */
																obj_t BgL_arg1802z00_2738;

																{	/* SawJvm/names.scm 84 */
																	obj_t BgL_arg1803z00_2739;

																	BgL_arg1803z00_2739 = (BgL_oclassz00_2733);
																	BgL_arg1802z00_2738 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2739,
																		2L);
																}
																BgL_res1809z00_2750 =
																	(BgL_arg1802z00_2738 == BgL_classz00_2717);
															}
														else
															{	/* SawJvm/names.scm 84 */
																BgL_res1809z00_2750 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1890z00_3631 = BgL_res1809z00_2750;
									}
							}
						}
						if (BgL_test1890z00_3631)
							{	/* SawJvm/names.scm 84 */
								return
									BGl_qualifiedzd2wclasszd2namez00zzbackend_cplibz00(
									((BgL_typez00_bglt) BgL_typez00_67));
							}
						else
							{	/* SawJvm/names.scm 86 */
								bool_t BgL_test1894z00_3656;

								{	/* SawJvm/names.scm 86 */
									obj_t BgL_classz00_2751;

									BgL_classz00_2751 = BGl_jclassz00zzobject_classz00;
									{	/* SawJvm/names.scm 86 */
										BgL_objectz00_bglt BgL_arg1807z00_2753;

										{	/* SawJvm/names.scm 86 */
											obj_t BgL_tmpz00_3657;

											BgL_tmpz00_3657 = ((obj_t) BgL_typez00_67);
											BgL_arg1807z00_2753 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3657);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawJvm/names.scm 86 */
												long BgL_idxz00_2759;

												BgL_idxz00_2759 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2753);
												BgL_test1894z00_3656 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2759 + 2L)) == BgL_classz00_2751);
											}
										else
											{	/* SawJvm/names.scm 86 */
												bool_t BgL_res1810z00_2784;

												{	/* SawJvm/names.scm 86 */
													obj_t BgL_oclassz00_2767;

													{	/* SawJvm/names.scm 86 */
														obj_t BgL_arg1815z00_2775;
														long BgL_arg1816z00_2776;

														BgL_arg1815z00_2775 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawJvm/names.scm 86 */
															long BgL_arg1817z00_2777;

															BgL_arg1817z00_2777 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2753);
															BgL_arg1816z00_2776 =
																(BgL_arg1817z00_2777 - OBJECT_TYPE);
														}
														BgL_oclassz00_2767 =
															VECTOR_REF(BgL_arg1815z00_2775,
															BgL_arg1816z00_2776);
													}
													{	/* SawJvm/names.scm 86 */
														bool_t BgL__ortest_1115z00_2768;

														BgL__ortest_1115z00_2768 =
															(BgL_classz00_2751 == BgL_oclassz00_2767);
														if (BgL__ortest_1115z00_2768)
															{	/* SawJvm/names.scm 86 */
																BgL_res1810z00_2784 = BgL__ortest_1115z00_2768;
															}
														else
															{	/* SawJvm/names.scm 86 */
																long BgL_odepthz00_2769;

																{	/* SawJvm/names.scm 86 */
																	obj_t BgL_arg1804z00_2770;

																	BgL_arg1804z00_2770 = (BgL_oclassz00_2767);
																	BgL_odepthz00_2769 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2770);
																}
																if ((2L < BgL_odepthz00_2769))
																	{	/* SawJvm/names.scm 86 */
																		obj_t BgL_arg1802z00_2772;

																		{	/* SawJvm/names.scm 86 */
																			obj_t BgL_arg1803z00_2773;

																			BgL_arg1803z00_2773 =
																				(BgL_oclassz00_2767);
																			BgL_arg1802z00_2772 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2773, 2L);
																		}
																		BgL_res1810z00_2784 =
																			(BgL_arg1802z00_2772 ==
																			BgL_classz00_2751);
																	}
																else
																	{	/* SawJvm/names.scm 86 */
																		BgL_res1810z00_2784 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1894z00_3656 = BgL_res1810z00_2784;
											}
									}
								}
								if (BgL_test1894z00_3656)
									{	/* SawJvm/names.scm 86 */
										return
											BGl_qualifiedzd2jclasszd2namez00zzbackend_cplibz00(
											((BgL_typez00_bglt) BgL_typez00_67));
									}
								else
									{	/* SawJvm/names.scm 87 */
										bool_t BgL_test1898z00_3681;

										{	/* SawJvm/names.scm 87 */
											obj_t BgL_classz00_2785;

											BgL_classz00_2785 = BGl_tvecz00zztvector_tvectorz00;
											{	/* SawJvm/names.scm 87 */
												BgL_objectz00_bglt BgL_arg1807z00_2787;

												{	/* SawJvm/names.scm 87 */
													obj_t BgL_tmpz00_3682;

													BgL_tmpz00_3682 = ((obj_t) BgL_typez00_67);
													BgL_arg1807z00_2787 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3682);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawJvm/names.scm 87 */
														long BgL_idxz00_2793;

														BgL_idxz00_2793 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2787);
														BgL_test1898z00_3681 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2793 + 2L)) == BgL_classz00_2785);
													}
												else
													{	/* SawJvm/names.scm 87 */
														bool_t BgL_res1811z00_2818;

														{	/* SawJvm/names.scm 87 */
															obj_t BgL_oclassz00_2801;

															{	/* SawJvm/names.scm 87 */
																obj_t BgL_arg1815z00_2809;
																long BgL_arg1816z00_2810;

																BgL_arg1815z00_2809 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawJvm/names.scm 87 */
																	long BgL_arg1817z00_2811;

																	BgL_arg1817z00_2811 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2787);
																	BgL_arg1816z00_2810 =
																		(BgL_arg1817z00_2811 - OBJECT_TYPE);
																}
																BgL_oclassz00_2801 =
																	VECTOR_REF(BgL_arg1815z00_2809,
																	BgL_arg1816z00_2810);
															}
															{	/* SawJvm/names.scm 87 */
																bool_t BgL__ortest_1115z00_2802;

																BgL__ortest_1115z00_2802 =
																	(BgL_classz00_2785 == BgL_oclassz00_2801);
																if (BgL__ortest_1115z00_2802)
																	{	/* SawJvm/names.scm 87 */
																		BgL_res1811z00_2818 =
																			BgL__ortest_1115z00_2802;
																	}
																else
																	{	/* SawJvm/names.scm 87 */
																		long BgL_odepthz00_2803;

																		{	/* SawJvm/names.scm 87 */
																			obj_t BgL_arg1804z00_2804;

																			BgL_arg1804z00_2804 =
																				(BgL_oclassz00_2801);
																			BgL_odepthz00_2803 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2804);
																		}
																		if ((2L < BgL_odepthz00_2803))
																			{	/* SawJvm/names.scm 87 */
																				obj_t BgL_arg1802z00_2806;

																				{	/* SawJvm/names.scm 87 */
																					obj_t BgL_arg1803z00_2807;

																					BgL_arg1803z00_2807 =
																						(BgL_oclassz00_2801);
																					BgL_arg1802z00_2806 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2807, 2L);
																				}
																				BgL_res1811z00_2818 =
																					(BgL_arg1802z00_2806 ==
																					BgL_classz00_2785);
																			}
																		else
																			{	/* SawJvm/names.scm 87 */
																				BgL_res1811z00_2818 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1898z00_3681 = BgL_res1811z00_2818;
													}
											}
										}
										if (BgL_test1898z00_3681)
											{	/* SawJvm/names.scm 87 */
												{	/* SawJvm/names.scm 88 */
													BgL_typez00_bglt BgL_arg1576z00_2216;

													{
														BgL_tvecz00_bglt BgL_auxz00_3704;

														{
															obj_t BgL_auxz00_3705;

															{	/* SawJvm/names.scm 88 */
																BgL_objectz00_bglt BgL_tmpz00_3706;

																BgL_tmpz00_3706 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_typez00_67));
																BgL_auxz00_3705 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3706);
															}
															BgL_auxz00_3704 =
																((BgL_tvecz00_bglt) BgL_auxz00_3705);
														}
														BgL_arg1576z00_2216 =
															(((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_3704))->
															BgL_itemzd2typezd2);
													}
													BGl_getzd2jvmtypezd2zzsaw_jvm_namesz00
														(BgL_arg1576z00_2216);
												}
												return BGl_string1819z00zzsaw_jvm_namesz00;
											}
										else
											{	/* SawJvm/names.scm 90 */
												bool_t BgL_test1902z00_3713;

												{	/* SawJvm/names.scm 90 */
													obj_t BgL_classz00_2821;

													BgL_classz00_2821 = BGl_jarrayz00zzforeign_jtypez00;
													{	/* SawJvm/names.scm 90 */
														BgL_objectz00_bglt BgL_arg1807z00_2823;

														{	/* SawJvm/names.scm 90 */
															obj_t BgL_tmpz00_3714;

															BgL_tmpz00_3714 = ((obj_t) BgL_typez00_67);
															BgL_arg1807z00_2823 =
																(BgL_objectz00_bglt) (BgL_tmpz00_3714);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawJvm/names.scm 90 */
																long BgL_idxz00_2829;

																BgL_idxz00_2829 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2823);
																BgL_test1902z00_3713 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2829 + 2L)) ==
																	BgL_classz00_2821);
															}
														else
															{	/* SawJvm/names.scm 90 */
																bool_t BgL_res1812z00_2854;

																{	/* SawJvm/names.scm 90 */
																	obj_t BgL_oclassz00_2837;

																	{	/* SawJvm/names.scm 90 */
																		obj_t BgL_arg1815z00_2845;
																		long BgL_arg1816z00_2846;

																		BgL_arg1815z00_2845 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawJvm/names.scm 90 */
																			long BgL_arg1817z00_2847;

																			BgL_arg1817z00_2847 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2823);
																			BgL_arg1816z00_2846 =
																				(BgL_arg1817z00_2847 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2837 =
																			VECTOR_REF(BgL_arg1815z00_2845,
																			BgL_arg1816z00_2846);
																	}
																	{	/* SawJvm/names.scm 90 */
																		bool_t BgL__ortest_1115z00_2838;

																		BgL__ortest_1115z00_2838 =
																			(BgL_classz00_2821 == BgL_oclassz00_2837);
																		if (BgL__ortest_1115z00_2838)
																			{	/* SawJvm/names.scm 90 */
																				BgL_res1812z00_2854 =
																					BgL__ortest_1115z00_2838;
																			}
																		else
																			{	/* SawJvm/names.scm 90 */
																				long BgL_odepthz00_2839;

																				{	/* SawJvm/names.scm 90 */
																					obj_t BgL_arg1804z00_2840;

																					BgL_arg1804z00_2840 =
																						(BgL_oclassz00_2837);
																					BgL_odepthz00_2839 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2840);
																				}
																				if ((2L < BgL_odepthz00_2839))
																					{	/* SawJvm/names.scm 90 */
																						obj_t BgL_arg1802z00_2842;

																						{	/* SawJvm/names.scm 90 */
																							obj_t BgL_arg1803z00_2843;

																							BgL_arg1803z00_2843 =
																								(BgL_oclassz00_2837);
																							BgL_arg1802z00_2842 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2843, 2L);
																						}
																						BgL_res1812z00_2854 =
																							(BgL_arg1802z00_2842 ==
																							BgL_classz00_2821);
																					}
																				else
																					{	/* SawJvm/names.scm 90 */
																						BgL_res1812z00_2854 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1902z00_3713 = BgL_res1812z00_2854;
															}
													}
												}
												if (BgL_test1902z00_3713)
													{	/* SawJvm/names.scm 90 */
														{	/* SawJvm/names.scm 91 */
															BgL_typez00_bglt BgL_arg1584z00_2218;

															{
																BgL_jarrayz00_bglt BgL_auxz00_3736;

																{
																	obj_t BgL_auxz00_3737;

																	{	/* SawJvm/names.scm 91 */
																		BgL_objectz00_bglt BgL_tmpz00_3738;

																		BgL_tmpz00_3738 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_typez00_67));
																		BgL_auxz00_3737 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3738);
																	}
																	BgL_auxz00_3736 =
																		((BgL_jarrayz00_bglt) BgL_auxz00_3737);
																}
																BgL_arg1584z00_2218 =
																	(((BgL_jarrayz00_bglt)
																		COBJECT(BgL_auxz00_3736))->
																	BgL_itemzd2typezd2);
															}
															BGl_getzd2jvmtypezd2zzsaw_jvm_namesz00
																(BgL_arg1584z00_2218);
														}
														return BGl_string1819z00zzsaw_jvm_namesz00;
													}
												else
													{	/* SawJvm/names.scm 90 */
														BGL_TAIL return
															BGl_qualifiedzd2typezd2namez00zzbackend_cplibz00
															(BgL_typez00_67);
													}
											}
									}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.scm 1 */
			{	/* SawJvm/names.scm 17 */
				obj_t BgL_arg1594z00_2223;
				obj_t BgL_arg1595z00_2224;

				{	/* SawJvm/names.scm 17 */
					obj_t BgL_v1441z00_2261;

					BgL_v1441z00_2261 = create_vector(0L);
					BgL_arg1594z00_2223 = BgL_v1441z00_2261;
				}
				{	/* SawJvm/names.scm 17 */
					obj_t BgL_v1442z00_2262;

					BgL_v1442z00_2262 = create_vector(0L);
					BgL_arg1595z00_2224 = BgL_v1442z00_2262;
				}
				return (BGl_jvmbasicz00zzsaw_jvm_namesz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(7),
						CNST_TABLE_REF(8), BGl_typez00zztype_typez00, 2192L,
						BGl_proc1823z00zzsaw_jvm_namesz00,
						BGl_proc1822z00zzsaw_jvm_namesz00, BFALSE,
						BGl_proc1821z00zzsaw_jvm_namesz00,
						BGl_proc1820z00zzsaw_jvm_namesz00, BgL_arg1594z00_2223,
						BgL_arg1595z00_2224), BUNSPEC);
			}
		}

	}



/* &lambda1611 */
	BgL_typez00_bglt BGl_z62lambda1611z62zzsaw_jvm_namesz00(obj_t BgL_envz00_3155,
		obj_t BgL_o1132z00_3156)
	{
		{	/* SawJvm/names.scm 17 */
			{	/* SawJvm/names.scm 17 */
				long BgL_arg1613z00_3212;

				{	/* SawJvm/names.scm 17 */
					obj_t BgL_arg1615z00_3213;

					{	/* SawJvm/names.scm 17 */
						obj_t BgL_arg1616z00_3214;

						{	/* SawJvm/names.scm 17 */
							obj_t BgL_arg1815z00_3215;
							long BgL_arg1816z00_3216;

							BgL_arg1815z00_3215 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawJvm/names.scm 17 */
								long BgL_arg1817z00_3217;

								BgL_arg1817z00_3217 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1132z00_3156)));
								BgL_arg1816z00_3216 = (BgL_arg1817z00_3217 - OBJECT_TYPE);
							}
							BgL_arg1616z00_3214 =
								VECTOR_REF(BgL_arg1815z00_3215, BgL_arg1816z00_3216);
						}
						BgL_arg1615z00_3213 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1616z00_3214);
					}
					{	/* SawJvm/names.scm 17 */
						obj_t BgL_tmpz00_3758;

						BgL_tmpz00_3758 = ((obj_t) BgL_arg1615z00_3213);
						BgL_arg1613z00_3212 = BGL_CLASS_NUM(BgL_tmpz00_3758);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1132z00_3156)), BgL_arg1613z00_3212);
			}
			{	/* SawJvm/names.scm 17 */
				BgL_objectz00_bglt BgL_tmpz00_3764;

				BgL_tmpz00_3764 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1132z00_3156));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3764, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1132z00_3156));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1132z00_3156));
		}

	}



/* &<@anonymous:1610> */
	obj_t BGl_z62zc3z04anonymousza31610ze3ze5zzsaw_jvm_namesz00(obj_t
		BgL_envz00_3157, obj_t BgL_new1131z00_3158)
	{
		{	/* SawJvm/names.scm 17 */
			{
				BgL_typez00_bglt BgL_auxz00_3772;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1131z00_3158))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(9)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1131z00_3158))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				BgL_auxz00_3772 = ((BgL_typez00_bglt) BgL_new1131z00_3158);
				return ((obj_t) BgL_auxz00_3772);
			}
		}

	}



/* &lambda1606 */
	BgL_typez00_bglt BGl_z62lambda1606z62zzsaw_jvm_namesz00(obj_t BgL_envz00_3159,
		obj_t BgL_o1128z00_3160)
	{
		{	/* SawJvm/names.scm 17 */
			{	/* SawJvm/names.scm 17 */
				BgL_jvmbasicz00_bglt BgL_wide1130z00_3220;

				BgL_wide1130z00_3220 =
					((BgL_jvmbasicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jvmbasicz00_bgl))));
				{	/* SawJvm/names.scm 17 */
					obj_t BgL_auxz00_3830;
					BgL_objectz00_bglt BgL_tmpz00_3826;

					BgL_auxz00_3830 = ((obj_t) BgL_wide1130z00_3220);
					BgL_tmpz00_3826 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1128z00_3160)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3826, BgL_auxz00_3830);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1128z00_3160)));
				{	/* SawJvm/names.scm 17 */
					long BgL_arg1609z00_3221;

					BgL_arg1609z00_3221 =
						BGL_CLASS_NUM(BGl_jvmbasicz00zzsaw_jvm_namesz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1128z00_3160))), BgL_arg1609z00_3221);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1128z00_3160)));
			}
		}

	}



/* &lambda1596 */
	BgL_typez00_bglt BGl_z62lambda1596z62zzsaw_jvm_namesz00(obj_t BgL_envz00_3161,
		obj_t BgL_id1112z00_3162, obj_t BgL_name1113z00_3163,
		obj_t BgL_siza7e1114za7_3164, obj_t BgL_class1115z00_3165,
		obj_t BgL_coercezd2to1116zd2_3166, obj_t BgL_parents1117z00_3167,
		obj_t BgL_initzf31118zf3_3168, obj_t BgL_magiczf31119zf3_3169,
		obj_t BgL_null1120z00_3170, obj_t BgL_z421121z42_3171,
		obj_t BgL_alias1122z00_3172, obj_t BgL_pointedzd2tozd2by1123z00_3173,
		obj_t BgL_tvector1124z00_3174, obj_t BgL_location1125z00_3175,
		obj_t BgL_importzd2location1126zd2_3176, obj_t BgL_occurrence1127z00_3177)
	{
		{	/* SawJvm/names.scm 17 */
			{	/* SawJvm/names.scm 17 */
				bool_t BgL_initzf31118zf3_3223;
				bool_t BgL_magiczf31119zf3_3224;
				int BgL_occurrence1127z00_3225;

				BgL_initzf31118zf3_3223 = CBOOL(BgL_initzf31118zf3_3168);
				BgL_magiczf31119zf3_3224 = CBOOL(BgL_magiczf31119zf3_3169);
				BgL_occurrence1127z00_3225 = CINT(BgL_occurrence1127z00_3177);
				{	/* SawJvm/names.scm 17 */
					BgL_typez00_bglt BgL_new1152z00_3226;

					{	/* SawJvm/names.scm 17 */
						BgL_typez00_bglt BgL_tmp1150z00_3227;
						BgL_jvmbasicz00_bglt BgL_wide1151z00_3228;

						{
							BgL_typez00_bglt BgL_auxz00_3847;

							{	/* SawJvm/names.scm 17 */
								BgL_typez00_bglt BgL_new1149z00_3229;

								BgL_new1149z00_3229 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* SawJvm/names.scm 17 */
									long BgL_arg1605z00_3230;

									BgL_arg1605z00_3230 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1149z00_3229),
										BgL_arg1605z00_3230);
								}
								{	/* SawJvm/names.scm 17 */
									BgL_objectz00_bglt BgL_tmpz00_3852;

									BgL_tmpz00_3852 = ((BgL_objectz00_bglt) BgL_new1149z00_3229);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3852, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1149z00_3229);
								BgL_auxz00_3847 = BgL_new1149z00_3229;
							}
							BgL_tmp1150z00_3227 = ((BgL_typez00_bglt) BgL_auxz00_3847);
						}
						BgL_wide1151z00_3228 =
							((BgL_jvmbasicz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_jvmbasicz00_bgl))));
						{	/* SawJvm/names.scm 17 */
							obj_t BgL_auxz00_3860;
							BgL_objectz00_bglt BgL_tmpz00_3858;

							BgL_auxz00_3860 = ((obj_t) BgL_wide1151z00_3228);
							BgL_tmpz00_3858 = ((BgL_objectz00_bglt) BgL_tmp1150z00_3227);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3858, BgL_auxz00_3860);
						}
						((BgL_objectz00_bglt) BgL_tmp1150z00_3227);
						{	/* SawJvm/names.scm 17 */
							long BgL_arg1602z00_3231;

							BgL_arg1602z00_3231 =
								BGL_CLASS_NUM(BGl_jvmbasicz00zzsaw_jvm_namesz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1150z00_3227),
								BgL_arg1602z00_3231);
						}
						BgL_new1152z00_3226 = ((BgL_typez00_bglt) BgL_tmp1150z00_3227);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1152z00_3226)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1112z00_3162)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_namez00) =
						((obj_t) BgL_name1113z00_3163), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1114za7_3164), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_classz00) =
						((obj_t) BgL_class1115z00_3165), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1116zd2_3166), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_parentsz00) =
						((obj_t) BgL_parents1117z00_3167), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31118zf3_3223), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31119zf3_3224), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_nullz00) =
						((obj_t) BgL_null1120z00_3170), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_z42z42) =
						((obj_t) BgL_z421121z42_3171), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_aliasz00) =
						((obj_t) BgL_alias1122z00_3172), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1123z00_3173), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1124z00_3174), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_locationz00) =
						((obj_t) BgL_location1125z00_3175), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1126zd2_3176), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1152z00_3226)))->BgL_occurrencez00) =
						((int) BgL_occurrence1127z00_3225), BUNSPEC);
					return BgL_new1152z00_3226;
				}
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_namesz00(void)
	{
		{	/* SawJvm/names.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzforeign_jtypez00(287572863L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzbackend_bvmz00(336068337L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(0L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string1824z00zzsaw_jvm_namesz00));
		}

	}

#ifdef __cplusplus
}
#endif
