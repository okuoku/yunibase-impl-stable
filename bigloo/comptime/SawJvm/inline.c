/*===========================================================================*/
/*   (SawJvm/inline.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawJvm/inline.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_JVM_INLINE_TYPE_DEFINITIONS
#define BGL_SAW_JVM_INLINE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_jvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
		obj_t BgL_qnamez00;
		obj_t BgL_classesz00;
		obj_t BgL_currentzd2classzd2;
		obj_t BgL_declarationsz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_codez00;
		obj_t BgL_lightzd2funcallszd2;
		obj_t BgL_inlinez00;
	}             *BgL_jvmz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;


#endif													// BGL_SAW_JVM_INLINE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31951ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31943ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31692ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31935ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31927ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31595ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31838ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31919ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	extern obj_t BGl_genzd2exprzd2zzsaw_jvm_codez00(BgL_jvmz00_bglt, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_jvm_inlinez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31871ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31863ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31774ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31855ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31766ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31847ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62inlinezd2predicatezf3z43zzsaw_jvm_inlinez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31961ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31953ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31945ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31929ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31881ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31873ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31865ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31776ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31857ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31768ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_jvm_inlinez00(void);
	static obj_t BGl_z62zc3z04anonymousza31890ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31963ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31955ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31947ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31939ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31972ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31875ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31867ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31859ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_jvm_inlinez00(void);
	static obj_t BGl_z62zc3z04anonymousza31965ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_jvm_inlinez00(void);
	static obj_t BGl_z62zc3z04anonymousza31957ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31949ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_computezd2booleanzd2zzsaw_jvm_inlinez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31893ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31885ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31877ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_za2jvmzd2inlinesza2zd2zzsaw_jvm_inlinez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31967ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31959ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	static obj_t BGl_z62zc3z04anonymousza31879ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31888ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31799ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_jvm_inlinez00(void);
	static obj_t BGl_z62zc3z04anonymousza31897ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62inlinezd2callzf3z43zzsaw_jvm_inlinez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_branchz00zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t, obj_t);
	static obj_t BGl_skipzd2movzd2zzsaw_jvm_inlinez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31610ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_checkzd2jvmzd2inlinesz00zzsaw_jvm_inlinez00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inlinezd2callzd2withzd2argszf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31701ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_declarezd2globalzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_za2optimzd2jvmzd2fasteqza2z00zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31630ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31541ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_codez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_outz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62zc3z04anonymousza31712ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31704ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31721ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	extern obj_t BGl_rtl_loadiz00zzsaw_defsz00;
	extern obj_t BGl_codez12z12zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31900ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31706ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_jvm_inlinez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_inlinez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_inlinezd2predicatezf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_inlinez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_inlinez00(void);
	static obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31715ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31740ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31821ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31813ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31562ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31643ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31911ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31903ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31806ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31628ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31823ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31734ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_getzd2cnstzd2tablez00zzcnst_allocz00(void);
	static obj_t
		BGl_z62inlinezd2callzd2withzd2argszf3z43zzsaw_jvm_inlinez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31921ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31751ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31832ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31913ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62checkzd2jvmzd2inlinesz62zzsaw_jvm_inlinez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31850ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31931ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31753ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31915ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31907ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31737ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31835ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31941ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_za2toozd2hardza2zd2zzsaw_jvm_inlinez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31933ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31844ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31925ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31755ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31917ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31585ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inlinezd2callzf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31861ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31853ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31748ze3ze5zzsaw_jvm_inlinez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t __cnst[184];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2200z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2234za7,
		BGl_z62zc3z04anonymousza31911ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2201z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2235za7,
		BGl_z62zc3z04anonymousza31913ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2202z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2236za7,
		BGl_z62zc3z04anonymousza31915ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2203z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2237za7,
		BGl_z62zc3z04anonymousza31917ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2204z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2238za7,
		BGl_z62zc3z04anonymousza31919ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2205z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2239za7,
		BGl_z62zc3z04anonymousza31921ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2206z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2240za7,
		BGl_z62zc3z04anonymousza31925ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2207z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2241za7,
		BGl_z62zc3z04anonymousza31927ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2208z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2242za7,
		BGl_z62zc3z04anonymousza31929ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2209z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2243za7,
		BGl_z62zc3z04anonymousza31931ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2210z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2244za7,
		BGl_z62zc3z04anonymousza31933ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2211z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2245za7,
		BGl_z62zc3z04anonymousza31935ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2212z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2246za7,
		BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2247za7,
		BGl_z62zc3z04anonymousza31541ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2213z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2248za7,
		BGl_z62zc3z04anonymousza31939ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2249za7,
		BGl_z62zc3z04anonymousza31562ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2214z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2250za7,
		BGl_z62zc3z04anonymousza31941ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2251za7,
		BGl_z62zc3z04anonymousza31585ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2215z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2252za7,
		BGl_z62zc3z04anonymousza31943ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2134z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2253za7,
		BGl_z62zc3z04anonymousza31595ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2216z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2254za7,
		BGl_z62zc3z04anonymousza31945ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2255za7,
		BGl_z62zc3z04anonymousza31610ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2217z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2256za7,
		BGl_z62zc3z04anonymousza31947ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2136z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2257za7,
		BGl_z62zc3z04anonymousza31616ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2218z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2258za7,
		BGl_z62zc3z04anonymousza31949ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2137z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2259za7,
		BGl_z62zc3z04anonymousza31626ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2219z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2260za7,
		BGl_z62zc3z04anonymousza31951ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2138z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2261za7,
		BGl_z62zc3z04anonymousza31628ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2139z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2262za7,
		BGl_z62zc3z04anonymousza31630ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2229z00zzsaw_jvm_inlinez00,
		BgL_bgl_string2229za700za7za7s2263za7, "I", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2220z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2264za7,
		BGl_z62zc3z04anonymousza31953ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2221z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2265za7,
		BGl_z62zc3z04anonymousza31955ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2140z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2266za7,
		BGl_z62zc3z04anonymousza31643ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2222z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2267za7,
		BGl_z62zc3z04anonymousza31957ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2141z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2268za7,
		BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2223z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2269za7,
		BGl_z62zc3z04anonymousza31959ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2142z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2270za7,
		BGl_z62zc3z04anonymousza31692ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2230z00zzsaw_jvm_inlinez00,
		BgL_bgl_string2230za700za7za7s2271za7, "Cannot find inlined functions", 29);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2224z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2272za7,
		BGl_z62zc3z04anonymousza31961ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2143z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2273za7,
		BGl_z62zc3z04anonymousza31701ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2231z00zzsaw_jvm_inlinez00,
		BgL_bgl_string2231za700za7za7s2274za7, "saw_jvm_inline", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2225z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2275za7,
		BGl_z62zc3z04anonymousza31963ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2276za7,
		BGl_z62zc3z04anonymousza31704ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2232z00zzsaw_jvm_inlinez00,
		BgL_bgl_string2232za700za7za7s2277za7,
		"(iconst_0) SawJvm (putfield procarity) (putfield procenv) (putfield procindex) (invokespecial init) (dup) (new me) (anewarray obj) (instanceof bbool) goto (getstatic faux) (getstatic vrai) not-inlined (instanceof bchar) getstatic bchar_allocated (getfield bchar_value) getfield bchar_value (instanceof bint) (instanceof belong) (i2s) (i2b) (getfield bint_value) if_icmpne if_icmpeq if_icmple if_icmpgt if_icmplt if_icmpge (iadd) (isub) (imul) (idiv) (irem) (ior) (ixor) (ineg) (iconst_1) (instanceof real) (getfield real_value) (i2d) (d2i) (l2d) (d2l) ifne ifeq (dcmpl) ifle ifgt iflt ifge (dcmpg) (dadd) (dsub) (dmul) (ddiv) (dneg) if_acmpne if_acmpeq (getstatic *nil*) (instanceof (vector byte)) ok (iand) (sipush 255) (baload) (bastore) (arraylength) (newarray byte) (instanceof symbol) (getfield symbol_string) (putfield ccar) (getfield ccar) (instanceof pair) (getfield car) (getfield cdr) (putfield car) (putfield cdr) (instanceof extended_pair) (getfield cer) (putfield cer) (instanceof (vector obj)) (instanceof proc"
		"edure) no-value (aastore) (aaload) (getfield procenv) procedure-ref procedure-set! c-procedure? $vector? $set-cer! $cer $epair? $set-cdr! $set-car! $cdr $car $pair? cell-ref cell-set! c-symbol->string c-symbol? $make-string/wo-fill $string-length $string-set! $string-ref $bstring->string $string->bstring $string? $null? $negfl $/fl $*fl $-fl $+fl $>=fl $>fl $<=fl $<fl $=fl $flonum->llong $llong->flonum $flonum->elong $elong->flonum $flonum->fixnum $fixnum->flonum $real->double $flonum? $bitnot $bitxor $bitand $bitor $remainderfx $quotientfx $/fx $*fx $-fx $+fx $oddfx? $evenfx? $>=fx $>fx $<=fx $<fx $=fx $bint->int $long->int $int->long $bint->long $short->int $int->byte $int->short char->integer $elong? $fixnum? c-char>=? c-char>? c-char<=? c-char<? c-char=? $uchar->char $bchar->uchar $bchar->char $uchar->bchar c-char? c-eq? c-boxed-eq? $bool->bbool saw_jvm_inline_predicate $obj->bool $boolean? procedure-el-set! procedure-el-ref saw_jvm_inline_function make-el-procedure make-l-procedure make-va-procedure make"
		"-fx-procedure cnst-table-set! saw_jvm_inline_function_args cnst-table-ref (%exit long->bint $cons c-write-char) ",
		2160);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2226z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2278za7,
		BGl_z62zc3z04anonymousza31965ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2145z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2279za7,
		BGl_z62zc3z04anonymousza31706ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2227z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2280za7,
		BGl_z62zc3z04anonymousza31967ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2281za7,
		BGl_z62zc3z04anonymousza31712ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2228z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2282za7,
		BGl_z62zc3z04anonymousza31972ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2283za7,
		BGl_z62zc3z04anonymousza31715ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2284za7,
		BGl_z62zc3z04anonymousza31721ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2285za7,
		BGl_z62zc3z04anonymousza31723ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2286za7,
		BGl_z62zc3z04anonymousza31734ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2287za7,
		BGl_z62zc3z04anonymousza31737ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2288za7,
		BGl_z62zc3z04anonymousza31740ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2153z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2289za7,
		BGl_z62zc3z04anonymousza31748ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2290za7,
		BGl_z62zc3z04anonymousza31751ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2291za7,
		BGl_z62zc3z04anonymousza31753ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2292za7,
		BGl_z62zc3z04anonymousza31755ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2293za7,
		BGl_z62zc3z04anonymousza31762ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2294za7,
		BGl_z62zc3z04anonymousza31766ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2295za7,
		BGl_z62zc3z04anonymousza31768ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2predicatezf3zd2envzf3zzsaw_jvm_inlinez00,
		BgL_bgl_za762inlineza7d2pred2296z00,
		BGl_z62inlinezd2predicatezf3z43zzsaw_jvm_inlinez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2297za7,
		BGl_z62zc3z04anonymousza31771ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2298za7,
		BGl_z62zc3z04anonymousza31774ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2162z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2299za7,
		BGl_z62zc3z04anonymousza31776ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2300za7,
		BGl_z62zc3z04anonymousza31799ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2301za7,
		BGl_z62zc3z04anonymousza31806ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2165z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2302za7,
		BGl_z62zc3z04anonymousza31809ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2303za7,
		BGl_z62zc3z04anonymousza31813ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2304za7,
		BGl_z62zc3z04anonymousza31821ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2305za7,
		BGl_z62zc3z04anonymousza31823ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2306za7,
		BGl_z62zc3z04anonymousza31832ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2307za7,
		BGl_z62zc3z04anonymousza31835ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2308za7,
		BGl_z62zc3z04anonymousza31838ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2172z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2309za7,
		BGl_z62zc3z04anonymousza31841ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2173z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2310za7,
		BGl_z62zc3z04anonymousza31844ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2174z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2311za7,
		BGl_z62zc3z04anonymousza31847ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2175z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2312za7,
		BGl_z62zc3z04anonymousza31850ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2176z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2313za7,
		BGl_z62zc3z04anonymousza31853ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2177z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2314za7,
		BGl_z62zc3z04anonymousza31855ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2178z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2315za7,
		BGl_z62zc3z04anonymousza31857ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2179z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2316za7,
		BGl_z62zc3z04anonymousza31859ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2180z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2317za7,
		BGl_z62zc3z04anonymousza31861ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2181z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2318za7,
		BGl_z62zc3z04anonymousza31863ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2182z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2319za7,
		BGl_z62zc3z04anonymousza31865ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2183z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2320za7,
		BGl_z62zc3z04anonymousza31867ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2184z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2321za7,
		BGl_z62zc3z04anonymousza31869ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2185z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2322za7,
		BGl_z62zc3z04anonymousza31871ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2186z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2323za7,
		BGl_z62zc3z04anonymousza31873ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2187z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2324za7,
		BGl_z62zc3z04anonymousza31875ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2188z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2325za7,
		BGl_z62zc3z04anonymousza31877ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2189z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2326za7,
		BGl_z62zc3z04anonymousza31879ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2190z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2327za7,
		BGl_z62zc3z04anonymousza31881ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2191z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2328za7,
		BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2192z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2329za7,
		BGl_z62zc3z04anonymousza31885ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2193z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2330za7,
		BGl_z62zc3z04anonymousza31888ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2194z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2331za7,
		BGl_z62zc3z04anonymousza31890ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2195z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2332za7,
		BGl_z62zc3z04anonymousza31893ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2196z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2333za7,
		BGl_z62zc3z04anonymousza31897ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2197z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2334za7,
		BGl_z62zc3z04anonymousza31900ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2198z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2335za7,
		BGl_z62zc3z04anonymousza31903ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2199z00zzsaw_jvm_inlinez00,
		BgL_bgl_za762za7c3za704anonymo2336za7,
		BGl_z62zc3z04anonymousza31907ze3ze5zzsaw_jvm_inlinez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_checkzd2jvmzd2inlineszd2envzd2zzsaw_jvm_inlinez00,
		BgL_bgl_za762checkza7d2jvmza7d2337za7,
		BGl_z62checkzd2jvmzd2inlinesz62zzsaw_jvm_inlinez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2callzd2withzd2argszf3zd2envzf3zzsaw_jvm_inlinez00,
		BgL_bgl_za762inlineza7d2call2338z00,
		BGl_z62inlinezd2callzd2withzd2argszf3z43zzsaw_jvm_inlinez00, 0L, BUNSPEC,
		3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2callzf3zd2envzf3zzsaw_jvm_inlinez00,
		BgL_bgl_za762inlineza7d2call2339z00,
		BGl_z62inlinezd2callzf3z43zzsaw_jvm_inlinez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_jvm_inlinez00));
		     ADD_ROOT((void *) (&BGl_za2jvmzd2inlinesza2zd2zzsaw_jvm_inlinez00));
		     ADD_ROOT((void *) (&BGl_za2toozd2hardza2zd2zzsaw_jvm_inlinez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_inlinez00(long
		BgL_checksumz00_4181, char *BgL_fromz00_4182)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_jvm_inlinez00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_jvm_inlinez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_jvm_inlinez00();
					BGl_libraryzd2moduleszd2initz00zzsaw_jvm_inlinez00();
					BGl_cnstzd2initzd2zzsaw_jvm_inlinez00();
					BGl_importedzd2moduleszd2initz00zzsaw_jvm_inlinez00();
					return BGl_toplevelzd2initzd2zzsaw_jvm_inlinez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_jvm_inline");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_jvm_inline");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 1 */
			{	/* SawJvm/inline.scm 1 */
				obj_t BgL_cportz00_4084;

				{	/* SawJvm/inline.scm 1 */
					obj_t BgL_stringz00_4091;

					BgL_stringz00_4091 = BGl_string2232z00zzsaw_jvm_inlinez00;
					{	/* SawJvm/inline.scm 1 */
						obj_t BgL_startz00_4092;

						BgL_startz00_4092 = BINT(0L);
						{	/* SawJvm/inline.scm 1 */
							obj_t BgL_endz00_4093;

							BgL_endz00_4093 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4091)));
							{	/* SawJvm/inline.scm 1 */

								BgL_cportz00_4084 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4091, BgL_startz00_4092, BgL_endz00_4093);
				}}}}
				{
					long BgL_iz00_4085;

					BgL_iz00_4085 = 183L;
				BgL_loopz00_4086:
					if ((BgL_iz00_4085 == -1L))
						{	/* SawJvm/inline.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawJvm/inline.scm 1 */
							{	/* SawJvm/inline.scm 1 */
								obj_t BgL_arg2233z00_4087;

								{	/* SawJvm/inline.scm 1 */

									{	/* SawJvm/inline.scm 1 */
										obj_t BgL_locationz00_4089;

										BgL_locationz00_4089 = BBOOL(((bool_t) 0));
										{	/* SawJvm/inline.scm 1 */

											BgL_arg2233z00_4087 =
												BGl_readz00zz__readerz00(BgL_cportz00_4084,
												BgL_locationz00_4089);
										}
									}
								}
								{	/* SawJvm/inline.scm 1 */
									int BgL_tmpz00_4210;

									BgL_tmpz00_4210 = (int) (BgL_iz00_4085);
									CNST_TABLE_SET(BgL_tmpz00_4210, BgL_arg2233z00_4087);
							}}
							{	/* SawJvm/inline.scm 1 */
								int BgL_auxz00_4090;

								BgL_auxz00_4090 = (int) ((BgL_iz00_4085 - 1L));
								{
									long BgL_iz00_4215;

									BgL_iz00_4215 = (long) (BgL_auxz00_4090);
									BgL_iz00_4085 = BgL_iz00_4215;
									goto BgL_loopz00_4086;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 1 */
			BGl_za2jvmzd2inlinesza2zd2zzsaw_jvm_inlinez00 = BNIL;
			BGl_za2toozd2hardza2zd2zzsaw_jvm_inlinez00 = CNST_TABLE_REF(0);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(1),
				CNST_TABLE_REF(2), BGl_proc2131z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(3),
				CNST_TABLE_REF(2), BGl_proc2132z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(4),
				CNST_TABLE_REF(2), BGl_proc2133z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(5),
				CNST_TABLE_REF(2), BGl_proc2134z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(6),
				CNST_TABLE_REF(2), BGl_proc2135z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(7),
				CNST_TABLE_REF(8), BGl_proc2136z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(9),
				CNST_TABLE_REF(8), BGl_proc2137z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(10),
				CNST_TABLE_REF(8), BGl_proc2138z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(11),
				CNST_TABLE_REF(8), BGl_proc2139z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(12),
				CNST_TABLE_REF(13), BGl_proc2140z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(14),
				CNST_TABLE_REF(2), BGl_proc2141z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(15),
				CNST_TABLE_REF(13), BGl_proc2142z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(16),
				CNST_TABLE_REF(13), BGl_proc2143z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(17),
				CNST_TABLE_REF(8), BGl_proc2144z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(18),
				CNST_TABLE_REF(2), BGl_proc2145z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(19),
				CNST_TABLE_REF(8), BGl_proc2146z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(20),
				CNST_TABLE_REF(8), BGl_proc2147z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(21),
				CNST_TABLE_REF(8), BGl_proc2148z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(22),
				CNST_TABLE_REF(13), BGl_proc2149z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(23),
				CNST_TABLE_REF(13), BGl_proc2150z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(24),
				CNST_TABLE_REF(13), BGl_proc2151z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(25),
				CNST_TABLE_REF(13), BGl_proc2152z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(26),
				CNST_TABLE_REF(13), BGl_proc2153z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(27),
				CNST_TABLE_REF(8), BGl_proc2154z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(28),
				CNST_TABLE_REF(8), BGl_proc2155z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(29),
				CNST_TABLE_REF(8), BGl_proc2156z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(30),
				CNST_TABLE_REF(8), BGl_proc2157z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(31),
				CNST_TABLE_REF(8), BGl_proc2158z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(32),
				CNST_TABLE_REF(8), BGl_proc2159z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(33),
				CNST_TABLE_REF(8), BGl_proc2160z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(34),
				CNST_TABLE_REF(8), BGl_proc2161z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(35),
				CNST_TABLE_REF(8), BGl_proc2162z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(36),
				CNST_TABLE_REF(8), BGl_proc2163z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(37),
				CNST_TABLE_REF(8), BGl_proc2164z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(38),
				CNST_TABLE_REF(8), BGl_proc2165z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(39),
				CNST_TABLE_REF(8), BGl_proc2166z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(40),
				CNST_TABLE_REF(8), BGl_proc2167z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(41),
				CNST_TABLE_REF(8), BGl_proc2168z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(37),
				CNST_TABLE_REF(13), BGl_proc2169z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(38),
				CNST_TABLE_REF(13), BGl_proc2170z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(39),
				CNST_TABLE_REF(13), BGl_proc2171z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(40),
				CNST_TABLE_REF(13), BGl_proc2172z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(41),
				CNST_TABLE_REF(13), BGl_proc2173z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(42),
				CNST_TABLE_REF(13), BGl_proc2174z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(43),
				CNST_TABLE_REF(13), BGl_proc2175z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(44),
				CNST_TABLE_REF(8), BGl_proc2176z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(45),
				CNST_TABLE_REF(8), BGl_proc2177z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(46),
				CNST_TABLE_REF(8), BGl_proc2178z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(47),
				CNST_TABLE_REF(8), BGl_proc2179z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(48),
				CNST_TABLE_REF(8), BGl_proc2180z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(49),
				CNST_TABLE_REF(8), BGl_proc2181z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(50),
				CNST_TABLE_REF(8), BGl_proc2182z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(51),
				CNST_TABLE_REF(8), BGl_proc2183z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(52),
				CNST_TABLE_REF(8), BGl_proc2184z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(53),
				CNST_TABLE_REF(8), BGl_proc2185z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(54),
				CNST_TABLE_REF(8), BGl_proc2186z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(55),
				CNST_TABLE_REF(8), BGl_proc2187z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(56),
				CNST_TABLE_REF(8), BGl_proc2188z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(57),
				CNST_TABLE_REF(8), BGl_proc2189z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(58),
				CNST_TABLE_REF(8), BGl_proc2190z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(59),
				CNST_TABLE_REF(8), BGl_proc2191z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(60),
				CNST_TABLE_REF(8), BGl_proc2192z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(61),
				CNST_TABLE_REF(8), BGl_proc2193z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(62),
				CNST_TABLE_REF(13), BGl_proc2194z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(63),
				CNST_TABLE_REF(13), BGl_proc2195z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(64),
				CNST_TABLE_REF(13), BGl_proc2196z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(65),
				CNST_TABLE_REF(13), BGl_proc2197z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(66),
				CNST_TABLE_REF(13), BGl_proc2198z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(67),
				CNST_TABLE_REF(8), BGl_proc2199z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(68),
				CNST_TABLE_REF(8), BGl_proc2200z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(69),
				CNST_TABLE_REF(8), BGl_proc2201z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(70),
				CNST_TABLE_REF(8), BGl_proc2202z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(71),
				CNST_TABLE_REF(8), BGl_proc2203z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(72),
				CNST_TABLE_REF(8), BGl_proc2204z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(72),
				CNST_TABLE_REF(13), BGl_proc2205z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(73),
				CNST_TABLE_REF(8), BGl_proc2206z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(74),
				CNST_TABLE_REF(8), BGl_proc2207z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(75),
				CNST_TABLE_REF(8), BGl_proc2208z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(76),
				CNST_TABLE_REF(8), BGl_proc2209z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(77),
				CNST_TABLE_REF(8), BGl_proc2210z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(78),
				CNST_TABLE_REF(8), BGl_proc2211z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(79),
				CNST_TABLE_REF(8), BGl_proc2212z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(80),
				CNST_TABLE_REF(8), BGl_proc2213z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(81),
				CNST_TABLE_REF(8), BGl_proc2214z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(82),
				CNST_TABLE_REF(8), BGl_proc2215z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(83),
				CNST_TABLE_REF(8), BGl_proc2216z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(84),
				CNST_TABLE_REF(8), BGl_proc2217z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(85),
				CNST_TABLE_REF(8), BGl_proc2218z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(86),
				CNST_TABLE_REF(8), BGl_proc2219z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(87),
				CNST_TABLE_REF(8), BGl_proc2220z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(88),
				CNST_TABLE_REF(8), BGl_proc2221z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(89),
				CNST_TABLE_REF(8), BGl_proc2222z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(90),
				CNST_TABLE_REF(8), BGl_proc2223z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(91),
				CNST_TABLE_REF(8), BGl_proc2224z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(92),
				CNST_TABLE_REF(8), BGl_proc2225z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(93),
				CNST_TABLE_REF(8), BGl_proc2226z00zzsaw_jvm_inlinez00);
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(94),
				CNST_TABLE_REF(2), BGl_proc2227z00zzsaw_jvm_inlinez00);
			return
				BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(95),
				CNST_TABLE_REF(2), BGl_proc2228z00zzsaw_jvm_inlinez00);
		}

	}



/* &<@anonymous:1972> */
	obj_t BGl_z62zc3z04anonymousza31972ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3824, obj_t BgL_mez00_3825, obj_t BgL_argsz00_3826)
	{
		{	/* SawJvm/inline.scm 548 */
			{	/* SawJvm/inline.scm 549 */
				obj_t BgL_arg1973z00_4095;

				BgL_arg1973z00_4095 = CAR(((obj_t) BgL_argsz00_3826));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_3825), BgL_arg1973z00_4095);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3825), CNST_TABLE_REF(96));
			{	/* SawJvm/inline.scm 551 */
				obj_t BgL_arg1974z00_4096;

				{	/* SawJvm/inline.scm 551 */
					obj_t BgL_pairz00_4097;

					BgL_pairz00_4097 = CDR(((obj_t) BgL_argsz00_3826));
					BgL_arg1974z00_4096 = CAR(BgL_pairz00_4097);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_3825), BgL_arg1974z00_4096);
			}
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3825), CNST_TABLE_REF(97));
		}

	}



/* &<@anonymous:1967> */
	obj_t BGl_z62zc3z04anonymousza31967ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3827, obj_t BgL_mez00_3828, obj_t BgL_argsz00_3829)
	{
		{	/* SawJvm/inline.scm 540 */
			{	/* SawJvm/inline.scm 541 */
				obj_t BgL_arg1968z00_4098;

				BgL_arg1968z00_4098 = CAR(((obj_t) BgL_argsz00_3829));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_3828), BgL_arg1968z00_4098);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3828), CNST_TABLE_REF(96));
			{	/* SawJvm/inline.scm 543 */
				obj_t BgL_arg1969z00_4099;

				{	/* SawJvm/inline.scm 543 */
					obj_t BgL_pairz00_4100;

					BgL_pairz00_4100 = CDR(((obj_t) BgL_argsz00_3829));
					BgL_arg1969z00_4099 = CAR(BgL_pairz00_4100);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_3828), BgL_arg1969z00_4099);
			}
			{	/* SawJvm/inline.scm 544 */
				obj_t BgL_arg1970z00_4101;

				{	/* SawJvm/inline.scm 544 */
					obj_t BgL_pairz00_4102;

					{	/* SawJvm/inline.scm 544 */
						obj_t BgL_pairz00_4103;

						BgL_pairz00_4103 = CDR(((obj_t) BgL_argsz00_3829));
						BgL_pairz00_4102 = CDR(BgL_pairz00_4103);
					}
					BgL_arg1970z00_4101 = CAR(BgL_pairz00_4102);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_3828), BgL_arg1970z00_4101);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3828), CNST_TABLE_REF(98));
			return CNST_TABLE_REF(99);
		}

	}



/* &<@anonymous:1965> */
	obj_t BGl_z62zc3z04anonymousza31965ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3830, obj_t BgL_mez00_3831)
	{
		{	/* SawJvm/inline.scm 537 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3831), CNST_TABLE_REF(100));
		}

	}



/* &<@anonymous:1963> */
	obj_t BGl_z62zc3z04anonymousza31963ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3832, obj_t BgL_mez00_3833)
	{
		{	/* SawJvm/inline.scm 519 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3833), CNST_TABLE_REF(101));
		}

	}



/* &<@anonymous:1961> */
	obj_t BGl_z62zc3z04anonymousza31961ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3834, obj_t BgL_mez00_3835)
	{
		{	/* SawJvm/inline.scm 512 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3835), CNST_TABLE_REF(102));
			return CNST_TABLE_REF(99);
		}

	}



/* &<@anonymous:1959> */
	obj_t BGl_z62zc3z04anonymousza31959ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3836, obj_t BgL_mez00_3837)
	{
		{	/* SawJvm/inline.scm 509 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3837), CNST_TABLE_REF(103));
		}

	}



/* &<@anonymous:1957> */
	obj_t BGl_z62zc3z04anonymousza31957ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3838, obj_t BgL_mez00_3839)
	{
		{	/* SawJvm/inline.scm 506 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3839), CNST_TABLE_REF(104));
		}

	}



/* &<@anonymous:1955> */
	obj_t BGl_z62zc3z04anonymousza31955ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3840, obj_t BgL_mez00_3841)
	{
		{	/* SawJvm/inline.scm 499 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3841), CNST_TABLE_REF(105));
			return CNST_TABLE_REF(99);
		}

	}



/* &<@anonymous:1953> */
	obj_t BGl_z62zc3z04anonymousza31953ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3842, obj_t BgL_mez00_3843)
	{
		{	/* SawJvm/inline.scm 495 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3843), CNST_TABLE_REF(106));
			return CNST_TABLE_REF(99);
		}

	}



/* &<@anonymous:1951> */
	obj_t BGl_z62zc3z04anonymousza31951ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3844, obj_t BgL_mez00_3845)
	{
		{	/* SawJvm/inline.scm 492 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3845), CNST_TABLE_REF(107));
		}

	}



/* &<@anonymous:1949> */
	obj_t BGl_z62zc3z04anonymousza31949ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3846, obj_t BgL_mez00_3847)
	{
		{	/* SawJvm/inline.scm 489 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3847), CNST_TABLE_REF(108));
		}

	}



/* &<@anonymous:1947> */
	obj_t BGl_z62zc3z04anonymousza31947ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3848, obj_t BgL_mez00_3849)
	{
		{	/* SawJvm/inline.scm 486 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3849), CNST_TABLE_REF(109));
		}

	}



/* &<@anonymous:1945> */
	obj_t BGl_z62zc3z04anonymousza31945ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3850, obj_t BgL_mez00_3851)
	{
		{	/* SawJvm/inline.scm 470 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3851), CNST_TABLE_REF(110));
		}

	}



/* &<@anonymous:1943> */
	obj_t BGl_z62zc3z04anonymousza31943ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3852, obj_t BgL_mez00_3853)
	{
		{	/* SawJvm/inline.scm 466 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3853), CNST_TABLE_REF(111));
			return CNST_TABLE_REF(99);
		}

	}



/* &<@anonymous:1941> */
	obj_t BGl_z62zc3z04anonymousza31941ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3854, obj_t BgL_mez00_3855)
	{
		{	/* SawJvm/inline.scm 460 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3855), CNST_TABLE_REF(112));
		}

	}



/* &<@anonymous:1939> */
	obj_t BGl_z62zc3z04anonymousza31939ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3856, obj_t BgL_mez00_3857)
	{
		{	/* SawJvm/inline.scm 457 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3857), CNST_TABLE_REF(113));
		}

	}



/* &<@anonymous:1937> */
	obj_t BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3858, obj_t BgL_mez00_3859)
	{
		{	/* SawJvm/inline.scm 438 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3859), CNST_TABLE_REF(114));
		}

	}



/* &<@anonymous:1935> */
	obj_t BGl_z62zc3z04anonymousza31935ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3860, obj_t BgL_mez00_3861)
	{
		{	/* SawJvm/inline.scm 435 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3861), CNST_TABLE_REF(115));
		}

	}



/* &<@anonymous:1933> */
	obj_t BGl_z62zc3z04anonymousza31933ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3862, obj_t BgL_mez00_3863)
	{
		{	/* SawJvm/inline.scm 431 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3863), CNST_TABLE_REF(116));
			return CNST_TABLE_REF(99);
		}

	}



/* &<@anonymous:1931> */
	obj_t BGl_z62zc3z04anonymousza31931ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3864, obj_t BgL_mez00_3865)
	{
		{	/* SawJvm/inline.scm 426 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3865), CNST_TABLE_REF(117));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3865), CNST_TABLE_REF(118));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3865), CNST_TABLE_REF(119));
		}

	}



/* &<@anonymous:1929> */
	obj_t BGl_z62zc3z04anonymousza31929ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3866, obj_t BgL_mez00_3867)
	{
		{	/* SawJvm/inline.scm 423 */
			return CNST_TABLE_REF(120);
		}

	}



/* &<@anonymous:1927> */
	obj_t BGl_z62zc3z04anonymousza31927ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3868, obj_t BgL_mez00_3869)
	{
		{	/* SawJvm/inline.scm 420 */
			return CNST_TABLE_REF(120);
		}

	}



/* &<@anonymous:1925> */
	obj_t BGl_z62zc3z04anonymousza31925ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3870, obj_t BgL_mez00_3871)
	{
		{	/* SawJvm/inline.scm 417 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3871), CNST_TABLE_REF(121));
		}

	}



/* &<@anonymous:1921> */
	obj_t BGl_z62zc3z04anonymousza31921ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3872, obj_t BgL_mez00_3873, obj_t BgL_onzf3zf3_3874,
		obj_t BgL_labz00_3875)
	{
		{	/* SawJvm/inline.scm 392 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3873), CNST_TABLE_REF(122));
			{	/* SawJvm/inline.scm 394 */
				obj_t BgL_arg1923z00_4104;

				if (CBOOL(BgL_onzf3zf3_3874))
					{	/* SawJvm/inline.scm 394 */
						BgL_arg1923z00_4104 = CNST_TABLE_REF(123);
					}
				else
					{	/* SawJvm/inline.scm 394 */
						BgL_arg1923z00_4104 = CNST_TABLE_REF(124);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3873), BgL_arg1923z00_4104,
					BgL_labz00_3875);
			}
		}

	}



/* &<@anonymous:1919> */
	obj_t BGl_z62zc3z04anonymousza31919ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3876, obj_t BgL_mez00_3877)
	{
		{	/* SawJvm/inline.scm 388 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3877), CNST_TABLE_REF(122));
			return
				BGl_computezd2booleanzd2zzsaw_jvm_inlinez00(BgL_mez00_3877,
				CNST_TABLE_REF(123));
		}

	}



/* &<@anonymous:1917> */
	obj_t BGl_z62zc3z04anonymousza31917ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3878, obj_t BgL_mez00_3879)
	{
		{	/* SawJvm/inline.scm 380 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3879), CNST_TABLE_REF(125));
		}

	}



/* &<@anonymous:1915> */
	obj_t BGl_z62zc3z04anonymousza31915ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3880, obj_t BgL_mez00_3881)
	{
		{	/* SawJvm/inline.scm 379 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3881), CNST_TABLE_REF(126));
		}

	}



/* &<@anonymous:1913> */
	obj_t BGl_z62zc3z04anonymousza31913ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3882, obj_t BgL_mez00_3883)
	{
		{	/* SawJvm/inline.scm 378 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3883), CNST_TABLE_REF(127));
		}

	}



/* &<@anonymous:1911> */
	obj_t BGl_z62zc3z04anonymousza31911ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3884, obj_t BgL_mez00_3885)
	{
		{	/* SawJvm/inline.scm 377 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3885), CNST_TABLE_REF(128));
		}

	}



/* &<@anonymous:1907> */
	obj_t BGl_z62zc3z04anonymousza31907ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3886, obj_t BgL_mez00_3887)
	{
		{	/* SawJvm/inline.scm 376 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3887), CNST_TABLE_REF(129));
		}

	}



/* &<@anonymous:1903> */
	obj_t BGl_z62zc3z04anonymousza31903ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3888, obj_t BgL_mez00_3889, obj_t BgL_onzf3zf3_3890,
		obj_t BgL_labz00_3891)
	{
		{	/* SawJvm/inline.scm 372 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3889), CNST_TABLE_REF(130));
			{	/* SawJvm/inline.scm 374 */
				obj_t BgL_arg1904z00_4105;

				if (CBOOL(BgL_onzf3zf3_3890))
					{	/* SawJvm/inline.scm 374 */
						BgL_arg1904z00_4105 = CNST_TABLE_REF(131);
					}
				else
					{	/* SawJvm/inline.scm 374 */
						BgL_arg1904z00_4105 = CNST_TABLE_REF(132);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3889), BgL_arg1904z00_4105,
					BgL_labz00_3891);
			}
		}

	}



/* &<@anonymous:1900> */
	obj_t BGl_z62zc3z04anonymousza31900ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3892, obj_t BgL_mez00_3893, obj_t BgL_onzf3zf3_3894,
		obj_t BgL_labz00_3895)
	{
		{	/* SawJvm/inline.scm 368 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3893), CNST_TABLE_REF(130));
			{	/* SawJvm/inline.scm 370 */
				obj_t BgL_arg1901z00_4106;

				if (CBOOL(BgL_onzf3zf3_3894))
					{	/* SawJvm/inline.scm 370 */
						BgL_arg1901z00_4106 = CNST_TABLE_REF(133);
					}
				else
					{	/* SawJvm/inline.scm 370 */
						BgL_arg1901z00_4106 = CNST_TABLE_REF(134);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3893), BgL_arg1901z00_4106,
					BgL_labz00_3895);
			}
		}

	}



/* &<@anonymous:1897> */
	obj_t BGl_z62zc3z04anonymousza31897ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3896, obj_t BgL_mez00_3897, obj_t BgL_onzf3zf3_3898,
		obj_t BgL_labz00_3899)
	{
		{	/* SawJvm/inline.scm 364 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3897), CNST_TABLE_REF(135));
			{	/* SawJvm/inline.scm 366 */
				obj_t BgL_arg1898z00_4107;

				if (CBOOL(BgL_onzf3zf3_3898))
					{	/* SawJvm/inline.scm 366 */
						BgL_arg1898z00_4107 = CNST_TABLE_REF(134);
					}
				else
					{	/* SawJvm/inline.scm 366 */
						BgL_arg1898z00_4107 = CNST_TABLE_REF(133);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3897), BgL_arg1898z00_4107,
					BgL_labz00_3899);
			}
		}

	}



/* &<@anonymous:1893> */
	obj_t BGl_z62zc3z04anonymousza31893ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3900, obj_t BgL_mez00_3901, obj_t BgL_onzf3zf3_3902,
		obj_t BgL_labz00_3903)
	{
		{	/* SawJvm/inline.scm 360 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3901), CNST_TABLE_REF(135));
			{	/* SawJvm/inline.scm 362 */
				obj_t BgL_arg1894z00_4108;

				if (CBOOL(BgL_onzf3zf3_3902))
					{	/* SawJvm/inline.scm 362 */
						BgL_arg1894z00_4108 = CNST_TABLE_REF(132);
					}
				else
					{	/* SawJvm/inline.scm 362 */
						BgL_arg1894z00_4108 = CNST_TABLE_REF(131);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3901), BgL_arg1894z00_4108,
					BgL_labz00_3903);
			}
		}

	}



/* &<@anonymous:1890> */
	obj_t BGl_z62zc3z04anonymousza31890ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3904, obj_t BgL_mez00_3905, obj_t BgL_onzf3zf3_3906,
		obj_t BgL_labz00_3907)
	{
		{	/* SawJvm/inline.scm 356 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3905), CNST_TABLE_REF(130));
			{	/* SawJvm/inline.scm 358 */
				obj_t BgL_arg1891z00_4109;

				if (CBOOL(BgL_onzf3zf3_3906))
					{	/* SawJvm/inline.scm 358 */
						BgL_arg1891z00_4109 = CNST_TABLE_REF(136);
					}
				else
					{	/* SawJvm/inline.scm 358 */
						BgL_arg1891z00_4109 = CNST_TABLE_REF(137);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3905), BgL_arg1891z00_4109,
					BgL_labz00_3907);
			}
		}

	}



/* &<@anonymous:1888> */
	obj_t BGl_z62zc3z04anonymousza31888ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3908, obj_t BgL_mez00_3909)
	{
		{	/* SawJvm/inline.scm 353 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3909), CNST_TABLE_REF(138));
		}

	}



/* &<@anonymous:1885> */
	obj_t BGl_z62zc3z04anonymousza31885ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3910, obj_t BgL_mez00_3911)
	{
		{	/* SawJvm/inline.scm 350 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3911), CNST_TABLE_REF(139));
		}

	}



/* &<@anonymous:1883> */
	obj_t BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3912, obj_t BgL_mez00_3913)
	{
		{	/* SawJvm/inline.scm 347 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3913), CNST_TABLE_REF(138));
		}

	}



/* &<@anonymous:1881> */
	obj_t BGl_z62zc3z04anonymousza31881ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3914, obj_t BgL_mez00_3915)
	{
		{	/* SawJvm/inline.scm 344 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3915), CNST_TABLE_REF(139));
		}

	}



/* &<@anonymous:1879> */
	obj_t BGl_z62zc3z04anonymousza31879ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3916, obj_t BgL_mez00_3917)
	{
		{	/* SawJvm/inline.scm 341 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3917), CNST_TABLE_REF(140));
		}

	}



/* &<@anonymous:1877> */
	obj_t BGl_z62zc3z04anonymousza31877ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3918, obj_t BgL_mez00_3919)
	{
		{	/* SawJvm/inline.scm 338 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3919), CNST_TABLE_REF(141));
		}

	}



/* &<@anonymous:1875> */
	obj_t BGl_z62zc3z04anonymousza31875ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3920, obj_t BgL_mez00_3921)
	{
		{	/* SawJvm/inline.scm 335 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3921), CNST_TABLE_REF(142));
		}

	}



/* &<@anonymous:1873> */
	obj_t BGl_z62zc3z04anonymousza31873ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3922, obj_t BgL_mez00_3923)
	{
		{	/* SawJvm/inline.scm 332 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3923), CNST_TABLE_REF(143));
		}

	}



/* &<@anonymous:1871> */
	obj_t BGl_z62zc3z04anonymousza31871ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3924, obj_t BgL_mez00_3925)
	{
		{	/* SawJvm/inline.scm 324 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3925), CNST_TABLE_REF(144));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3925), CNST_TABLE_REF(145));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3925), CNST_TABLE_REF(146));
		}

	}



/* &<@anonymous:1869> */
	obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3926, obj_t BgL_mez00_3927)
	{
		{	/* SawJvm/inline.scm 323 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3927), CNST_TABLE_REF(146));
		}

	}



/* &<@anonymous:1867> */
	obj_t BGl_z62zc3z04anonymousza31867ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3928, obj_t BgL_mez00_3929)
	{
		{	/* SawJvm/inline.scm 322 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3929), CNST_TABLE_REF(119));
		}

	}



/* &<@anonymous:1865> */
	obj_t BGl_z62zc3z04anonymousza31865ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3930, obj_t BgL_mez00_3931)
	{
		{	/* SawJvm/inline.scm 321 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3931), CNST_TABLE_REF(147));
		}

	}



/* &<@anonymous:1863> */
	obj_t BGl_z62zc3z04anonymousza31863ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3932, obj_t BgL_mez00_3933)
	{
		{	/* SawJvm/inline.scm 320 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3933), CNST_TABLE_REF(148));
		}

	}



/* &<@anonymous:1861> */
	obj_t BGl_z62zc3z04anonymousza31861ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3934, obj_t BgL_mez00_3935)
	{
		{	/* SawJvm/inline.scm 319 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3935), CNST_TABLE_REF(149));
		}

	}



/* &<@anonymous:1859> */
	obj_t BGl_z62zc3z04anonymousza31859ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3936, obj_t BgL_mez00_3937)
	{
		{	/* SawJvm/inline.scm 318 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3937), CNST_TABLE_REF(149));
		}

	}



/* &<@anonymous:1857> */
	obj_t BGl_z62zc3z04anonymousza31857ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3938, obj_t BgL_mez00_3939)
	{
		{	/* SawJvm/inline.scm 317 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3939), CNST_TABLE_REF(150));
		}

	}



/* &<@anonymous:1855> */
	obj_t BGl_z62zc3z04anonymousza31855ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3940, obj_t BgL_mez00_3941)
	{
		{	/* SawJvm/inline.scm 316 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3941), CNST_TABLE_REF(151));
		}

	}



/* &<@anonymous:1853> */
	obj_t BGl_z62zc3z04anonymousza31853ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3942, obj_t BgL_mez00_3943)
	{
		{	/* SawJvm/inline.scm 315 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3943), CNST_TABLE_REF(152));
		}

	}



/* &<@anonymous:1850> */
	obj_t BGl_z62zc3z04anonymousza31850ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3944, obj_t BgL_mez00_3945, obj_t BgL_onzf3zf3_3946,
		obj_t BgL_labz00_3947)
	{
		{	/* SawJvm/inline.scm 310 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3945), CNST_TABLE_REF(144));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3945), CNST_TABLE_REF(119));
			{	/* SawJvm/inline.scm 313 */
				obj_t BgL_arg1851z00_4110;

				if (CBOOL(BgL_onzf3zf3_3946))
					{	/* SawJvm/inline.scm 313 */
						BgL_arg1851z00_4110 = CNST_TABLE_REF(137);
					}
				else
					{	/* SawJvm/inline.scm 313 */
						BgL_arg1851z00_4110 = CNST_TABLE_REF(136);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3945), BgL_arg1851z00_4110,
					BgL_labz00_3947);
			}
		}

	}



/* &<@anonymous:1847> */
	obj_t BGl_z62zc3z04anonymousza31847ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3948, obj_t BgL_mez00_3949, obj_t BgL_onzf3zf3_3950,
		obj_t BgL_labz00_3951)
	{
		{	/* SawJvm/inline.scm 305 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3949), CNST_TABLE_REF(144));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3949), CNST_TABLE_REF(119));
			{	/* SawJvm/inline.scm 308 */
				obj_t BgL_arg1848z00_4111;

				if (CBOOL(BgL_onzf3zf3_3950))
					{	/* SawJvm/inline.scm 308 */
						BgL_arg1848z00_4111 = CNST_TABLE_REF(136);
					}
				else
					{	/* SawJvm/inline.scm 308 */
						BgL_arg1848z00_4111 = CNST_TABLE_REF(137);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3949), BgL_arg1848z00_4111,
					BgL_labz00_3951);
			}
		}

	}



/* &<@anonymous:1844> */
	obj_t BGl_z62zc3z04anonymousza31844ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3952, obj_t BgL_mez00_3953, obj_t BgL_onzf3zf3_3954,
		obj_t BgL_labz00_3955)
	{
		{	/* SawJvm/inline.scm 302 */
			{	/* SawJvm/inline.scm 303 */
				obj_t BgL_arg1845z00_4112;

				if (CBOOL(BgL_onzf3zf3_3954))
					{	/* SawJvm/inline.scm 303 */
						BgL_arg1845z00_4112 = CNST_TABLE_REF(153);
					}
				else
					{	/* SawJvm/inline.scm 303 */
						BgL_arg1845z00_4112 = CNST_TABLE_REF(154);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3953), BgL_arg1845z00_4112,
					BgL_labz00_3955);
			}
		}

	}



/* &<@anonymous:1841> */
	obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3956, obj_t BgL_mez00_3957, obj_t BgL_onzf3zf3_3958,
		obj_t BgL_labz00_3959)
	{
		{	/* SawJvm/inline.scm 299 */
			{	/* SawJvm/inline.scm 300 */
				obj_t BgL_arg1842z00_4113;

				if (CBOOL(BgL_onzf3zf3_3958))
					{	/* SawJvm/inline.scm 300 */
						BgL_arg1842z00_4113 = CNST_TABLE_REF(155);
					}
				else
					{	/* SawJvm/inline.scm 300 */
						BgL_arg1842z00_4113 = CNST_TABLE_REF(156);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3957), BgL_arg1842z00_4113,
					BgL_labz00_3959);
			}
		}

	}



/* &<@anonymous:1838> */
	obj_t BGl_z62zc3z04anonymousza31838ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3960, obj_t BgL_mez00_3961, obj_t BgL_onzf3zf3_3962,
		obj_t BgL_labz00_3963)
	{
		{	/* SawJvm/inline.scm 296 */
			{	/* SawJvm/inline.scm 297 */
				obj_t BgL_arg1839z00_4114;

				if (CBOOL(BgL_onzf3zf3_3962))
					{	/* SawJvm/inline.scm 297 */
						BgL_arg1839z00_4114 = CNST_TABLE_REF(156);
					}
				else
					{	/* SawJvm/inline.scm 297 */
						BgL_arg1839z00_4114 = CNST_TABLE_REF(155);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3961), BgL_arg1839z00_4114,
					BgL_labz00_3963);
			}
		}

	}



/* &<@anonymous:1835> */
	obj_t BGl_z62zc3z04anonymousza31835ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3964, obj_t BgL_mez00_3965, obj_t BgL_onzf3zf3_3966,
		obj_t BgL_labz00_3967)
	{
		{	/* SawJvm/inline.scm 293 */
			{	/* SawJvm/inline.scm 294 */
				obj_t BgL_arg1836z00_4115;

				if (CBOOL(BgL_onzf3zf3_3966))
					{	/* SawJvm/inline.scm 294 */
						BgL_arg1836z00_4115 = CNST_TABLE_REF(154);
					}
				else
					{	/* SawJvm/inline.scm 294 */
						BgL_arg1836z00_4115 = CNST_TABLE_REF(153);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3965), BgL_arg1836z00_4115,
					BgL_labz00_3967);
			}
		}

	}



/* &<@anonymous:1832> */
	obj_t BGl_z62zc3z04anonymousza31832ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3968, obj_t BgL_mez00_3969, obj_t BgL_onzf3zf3_3970,
		obj_t BgL_labz00_3971)
	{
		{	/* SawJvm/inline.scm 290 */
			{	/* SawJvm/inline.scm 291 */
				obj_t BgL_arg1833z00_4116;

				if (CBOOL(BgL_onzf3zf3_3970))
					{	/* SawJvm/inline.scm 291 */
						BgL_arg1833z00_4116 = CNST_TABLE_REF(157);
					}
				else
					{	/* SawJvm/inline.scm 291 */
						BgL_arg1833z00_4116 = CNST_TABLE_REF(158);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_3969), BgL_arg1833z00_4116,
					BgL_labz00_3971);
			}
		}

	}



/* &<@anonymous:1823> */
	obj_t BGl_z62zc3z04anonymousza31823ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3972, obj_t BgL_mez00_3973)
	{
		{	/* SawJvm/inline.scm 287 */
			return
				BGl_computezd2booleanzd2zzsaw_jvm_inlinez00(BgL_mez00_3973,
				CNST_TABLE_REF(153));
		}

	}



/* &<@anonymous:1821> */
	obj_t BGl_z62zc3z04anonymousza31821ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3974, obj_t BgL_mez00_3975)
	{
		{	/* SawJvm/inline.scm 284 */
			return
				BGl_computezd2booleanzd2zzsaw_jvm_inlinez00(BgL_mez00_3975,
				CNST_TABLE_REF(155));
		}

	}



/* &<@anonymous:1813> */
	obj_t BGl_z62zc3z04anonymousza31813ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3976, obj_t BgL_mez00_3977)
	{
		{	/* SawJvm/inline.scm 281 */
			return
				BGl_computezd2booleanzd2zzsaw_jvm_inlinez00(BgL_mez00_3977,
				CNST_TABLE_REF(156));
		}

	}



/* &<@anonymous:1809> */
	obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3978, obj_t BgL_mez00_3979)
	{
		{	/* SawJvm/inline.scm 278 */
			return
				BGl_computezd2booleanzd2zzsaw_jvm_inlinez00(BgL_mez00_3979,
				CNST_TABLE_REF(154));
		}

	}



/* &<@anonymous:1806> */
	obj_t BGl_z62zc3z04anonymousza31806ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3980, obj_t BgL_mez00_3981)
	{
		{	/* SawJvm/inline.scm 275 */
			return
				BGl_computezd2booleanzd2zzsaw_jvm_inlinez00(BgL_mez00_3981,
				CNST_TABLE_REF(157));
		}

	}



/* &<@anonymous:1799> */
	obj_t BGl_z62zc3z04anonymousza31799ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3982, obj_t BgL_mez00_3983)
	{
		{	/* SawJvm/inline.scm 262 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3983), CNST_TABLE_REF(159));
		}

	}



/* &<@anonymous:1776> */
	obj_t BGl_z62zc3z04anonymousza31776ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3984, obj_t BgL_mez00_3985)
	{
		{	/* SawJvm/inline.scm 259 */
			return CNST_TABLE_REF(120);
		}

	}



/* &<@anonymous:1774> */
	obj_t BGl_z62zc3z04anonymousza31774ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3986, obj_t BgL_mez00_3987)
	{
		{	/* SawJvm/inline.scm 256 */
			return CNST_TABLE_REF(120);
		}

	}



/* &<@anonymous:1771> */
	obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3988, obj_t BgL_mez00_3989)
	{
		{	/* SawJvm/inline.scm 250 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3989), CNST_TABLE_REF(159));
		}

	}



/* &<@anonymous:1768> */
	obj_t BGl_z62zc3z04anonymousza31768ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3990, obj_t BgL_mez00_3991)
	{
		{	/* SawJvm/inline.scm 247 */
			return CNST_TABLE_REF(120);
		}

	}



/* &<@anonymous:1766> */
	obj_t BGl_z62zc3z04anonymousza31766ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3992, obj_t BgL_mez00_3993)
	{
		{	/* SawJvm/inline.scm 244 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3993), CNST_TABLE_REF(160));
		}

	}



/* &<@anonymous:1762> */
	obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3994, obj_t BgL_mez00_3995)
	{
		{	/* SawJvm/inline.scm 241 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3995), CNST_TABLE_REF(161));
		}

	}



/* &<@anonymous:1755> */
	obj_t BGl_z62zc3z04anonymousza31755ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3996, obj_t BgL_mez00_3997)
	{
		{	/* SawJvm/inline.scm 237 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3997), CNST_TABLE_REF(118));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3997), CNST_TABLE_REF(119));
		}

	}



/* &<@anonymous:1753> */
	obj_t BGl_z62zc3z04anonymousza31753ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_3998, obj_t BgL_mez00_3999)
	{
		{	/* SawJvm/inline.scm 234 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_3999), CNST_TABLE_REF(162));
		}

	}



/* &<@anonymous:1751> */
	obj_t BGl_z62zc3z04anonymousza31751ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4000, obj_t BgL_mez00_4001)
	{
		{	/* SawJvm/inline.scm 231 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4001), CNST_TABLE_REF(163));
		}

	}



/* &<@anonymous:1748> */
	obj_t BGl_z62zc3z04anonymousza31748ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4002, obj_t BgL_mez00_4003, obj_t BgL_onzf3zf3_4004,
		obj_t BgL_labz00_4005)
	{
		{	/* SawJvm/inline.scm 225 */
			{	/* SawJvm/inline.scm 226 */
				obj_t BgL_arg1749z00_4117;

				if (CBOOL(BgL_onzf3zf3_4004))
					{	/* SawJvm/inline.scm 226 */
						BgL_arg1749z00_4117 = CNST_TABLE_REF(153);
					}
				else
					{	/* SawJvm/inline.scm 226 */
						BgL_arg1749z00_4117 = CNST_TABLE_REF(154);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4003), BgL_arg1749z00_4117,
					BgL_labz00_4005);
			}
		}

	}



/* &<@anonymous:1740> */
	obj_t BGl_z62zc3z04anonymousza31740ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4006, obj_t BgL_mez00_4007, obj_t BgL_onzf3zf3_4008,
		obj_t BgL_labz00_4009)
	{
		{	/* SawJvm/inline.scm 222 */
			{	/* SawJvm/inline.scm 223 */
				obj_t BgL_arg1746z00_4118;

				if (CBOOL(BgL_onzf3zf3_4008))
					{	/* SawJvm/inline.scm 223 */
						BgL_arg1746z00_4118 = CNST_TABLE_REF(155);
					}
				else
					{	/* SawJvm/inline.scm 223 */
						BgL_arg1746z00_4118 = CNST_TABLE_REF(156);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4007), BgL_arg1746z00_4118,
					BgL_labz00_4009);
			}
		}

	}



/* &<@anonymous:1737> */
	obj_t BGl_z62zc3z04anonymousza31737ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4010, obj_t BgL_mez00_4011, obj_t BgL_onzf3zf3_4012,
		obj_t BgL_labz00_4013)
	{
		{	/* SawJvm/inline.scm 219 */
			{	/* SawJvm/inline.scm 220 */
				obj_t BgL_arg1738z00_4119;

				if (CBOOL(BgL_onzf3zf3_4012))
					{	/* SawJvm/inline.scm 220 */
						BgL_arg1738z00_4119 = CNST_TABLE_REF(156);
					}
				else
					{	/* SawJvm/inline.scm 220 */
						BgL_arg1738z00_4119 = CNST_TABLE_REF(155);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4011), BgL_arg1738z00_4119,
					BgL_labz00_4013);
			}
		}

	}



/* &<@anonymous:1734> */
	obj_t BGl_z62zc3z04anonymousza31734ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4014, obj_t BgL_mez00_4015, obj_t BgL_onzf3zf3_4016,
		obj_t BgL_labz00_4017)
	{
		{	/* SawJvm/inline.scm 216 */
			{	/* SawJvm/inline.scm 217 */
				obj_t BgL_arg1735z00_4120;

				if (CBOOL(BgL_onzf3zf3_4016))
					{	/* SawJvm/inline.scm 217 */
						BgL_arg1735z00_4120 = CNST_TABLE_REF(154);
					}
				else
					{	/* SawJvm/inline.scm 217 */
						BgL_arg1735z00_4120 = CNST_TABLE_REF(153);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4015), BgL_arg1735z00_4120,
					BgL_labz00_4017);
			}
		}

	}



/* &<@anonymous:1723> */
	obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4018, obj_t BgL_mez00_4019, obj_t BgL_onzf3zf3_4020,
		obj_t BgL_labz00_4021)
	{
		{	/* SawJvm/inline.scm 213 */
			{	/* SawJvm/inline.scm 214 */
				obj_t BgL_arg1724z00_4121;

				if (CBOOL(BgL_onzf3zf3_4020))
					{	/* SawJvm/inline.scm 214 */
						BgL_arg1724z00_4121 = CNST_TABLE_REF(157);
					}
				else
					{	/* SawJvm/inline.scm 214 */
						BgL_arg1724z00_4121 = CNST_TABLE_REF(158);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4019), BgL_arg1724z00_4121,
					BgL_labz00_4021);
			}
		}

	}



/* &<@anonymous:1721> */
	obj_t BGl_z62zc3z04anonymousza31721ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4022, obj_t BgL_mez00_4023)
	{
		{	/* SawJvm/inline.scm 210 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4023), CNST_TABLE_REF(160));
		}

	}



/* &<@anonymous:1715> */
	obj_t BGl_z62zc3z04anonymousza31715ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4024, obj_t BgL_mez00_4025)
	{
		{	/* SawJvm/inline.scm 205 */
			{	/* SawJvm/inline.scm 206 */
				obj_t BgL_arg1717z00_4122;

				{	/* SawJvm/inline.scm 206 */
					obj_t BgL_arg1718z00_4123;

					BgL_arg1718z00_4123 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(164), BNIL);
					BgL_arg1717z00_4122 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(165), BgL_arg1718z00_4123);
				}
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4025), BgL_arg1717z00_4122);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4025), CNST_TABLE_REF(118));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4025), CNST_TABLE_REF(119));
		}

	}



/* &<@anonymous:1712> */
	obj_t BGl_z62zc3z04anonymousza31712ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4026, obj_t BgL_mez00_4027)
	{
		{	/* SawJvm/inline.scm 202 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4027), CNST_TABLE_REF(166));
		}

	}



/* &<@anonymous:1706> */
	obj_t BGl_z62zc3z04anonymousza31706ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4028, obj_t BgL_mez00_4029, obj_t BgL_argsz00_4030)
	{
		{	/* SawJvm/inline.scm 195 */
			{	/* SawJvm/inline.scm 196 */
				obj_t BgL_arg1708z00_4124;

				{	/* SawJvm/inline.scm 196 */
					obj_t BgL_arg1709z00_4125;

					BgL_arg1709z00_4125 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(167), BNIL);
					BgL_arg1708z00_4124 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(168), BgL_arg1709z00_4125);
				}
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4029), BgL_arg1708z00_4124);
			}
			{	/* SawJvm/inline.scm 197 */
				obj_t BgL_arg1710z00_4126;

				BgL_arg1710z00_4126 = CAR(((obj_t) BgL_argsz00_4030));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4029), BgL_arg1710z00_4126);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4029), CNST_TABLE_REF(118));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4029), CNST_TABLE_REF(119));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4029), CNST_TABLE_REF(97));
		}

	}



/* &<@anonymous:1704> */
	obj_t BGl_z62zc3z04anonymousza31704ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4031, obj_t BgL_mez00_4032)
	{
		{	/* SawJvm/inline.scm 192 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4032), CNST_TABLE_REF(169));
		}

	}



/* &<@anonymous:1701> */
	obj_t BGl_z62zc3z04anonymousza31701ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4033, obj_t BgL_mez00_4034, obj_t BgL_onzf3zf3_4035,
		obj_t BgL_labz00_4036)
	{
		{	/* SawJvm/inline.scm 184 */
			if (CBOOL(BGl_za2optimzd2jvmzd2fasteqza2z00zzengine_paramz00))
				{	/* SawJvm/inline.scm 186 */
					obj_t BgL_arg1702z00_4127;

					if (CBOOL(BgL_onzf3zf3_4035))
						{	/* SawJvm/inline.scm 186 */
							BgL_arg1702z00_4127 = CNST_TABLE_REF(123);
						}
					else
						{	/* SawJvm/inline.scm 186 */
							BgL_arg1702z00_4127 = CNST_TABLE_REF(124);
						}
					return
						BGl_branchz00zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_4034), BgL_arg1702z00_4127,
						BgL_labz00_4036);
				}
			else
				{	/* SawJvm/inline.scm 185 */
					return CNST_TABLE_REF(170);
				}
		}

	}



/* &<@anonymous:1692> */
	obj_t BGl_z62zc3z04anonymousza31692ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4037, obj_t BgL_mez00_4038, obj_t BgL_onzf3zf3_4039,
		obj_t BgL_labz00_4040)
	{
		{	/* SawJvm/inline.scm 181 */
			{	/* SawJvm/inline.scm 182 */
				obj_t BgL_arg1699z00_4128;

				if (CBOOL(BgL_onzf3zf3_4039))
					{	/* SawJvm/inline.scm 182 */
						BgL_arg1699z00_4128 = CNST_TABLE_REF(123);
					}
				else
					{	/* SawJvm/inline.scm 182 */
						BgL_arg1699z00_4128 = CNST_TABLE_REF(124);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4038), BgL_arg1699z00_4128,
					BgL_labz00_4040);
			}
		}

	}



/* &<@anonymous:1651> */
	obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4041, obj_t BgL_mez00_4042, obj_t BgL_argsz00_4043)
	{
		{	/* SawJvm/inline.scm 166 */
			{	/* SawJvm/inline.scm 167 */
				obj_t BgL_funz00_4129;

				{	/* SawJvm/inline.scm 167 */
					obj_t BgL_arg1689z00_4130;

					BgL_arg1689z00_4130 = CAR(((obj_t) BgL_argsz00_4043));
					BgL_funz00_4129 =
						BGl_skipzd2movzd2zzsaw_jvm_inlinez00(BgL_arg1689z00_4130);
				}
				{	/* SawJvm/inline.scm 168 */
					bool_t BgL_test2363z00_4933;

					{	/* SawJvm/inline.scm 168 */
						obj_t BgL_classz00_4131;

						BgL_classz00_4131 = BGl_rtl_loadiz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_funz00_4129))
							{	/* SawJvm/inline.scm 168 */
								BgL_objectz00_bglt BgL_arg1807z00_4132;

								BgL_arg1807z00_4132 = (BgL_objectz00_bglt) (BgL_funz00_4129);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawJvm/inline.scm 168 */
										long BgL_idxz00_4133;

										BgL_idxz00_4133 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4132);
										BgL_test2363z00_4933 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4133 + 3L)) == BgL_classz00_4131);
									}
								else
									{	/* SawJvm/inline.scm 168 */
										bool_t BgL_res2125z00_4136;

										{	/* SawJvm/inline.scm 168 */
											obj_t BgL_oclassz00_4137;

											{	/* SawJvm/inline.scm 168 */
												obj_t BgL_arg1815z00_4138;
												long BgL_arg1816z00_4139;

												BgL_arg1815z00_4138 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawJvm/inline.scm 168 */
													long BgL_arg1817z00_4140;

													BgL_arg1817z00_4140 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4132);
													BgL_arg1816z00_4139 =
														(BgL_arg1817z00_4140 - OBJECT_TYPE);
												}
												BgL_oclassz00_4137 =
													VECTOR_REF(BgL_arg1815z00_4138, BgL_arg1816z00_4139);
											}
											{	/* SawJvm/inline.scm 168 */
												bool_t BgL__ortest_1115z00_4141;

												BgL__ortest_1115z00_4141 =
													(BgL_classz00_4131 == BgL_oclassz00_4137);
												if (BgL__ortest_1115z00_4141)
													{	/* SawJvm/inline.scm 168 */
														BgL_res2125z00_4136 = BgL__ortest_1115z00_4141;
													}
												else
													{	/* SawJvm/inline.scm 168 */
														long BgL_odepthz00_4142;

														{	/* SawJvm/inline.scm 168 */
															obj_t BgL_arg1804z00_4143;

															BgL_arg1804z00_4143 = (BgL_oclassz00_4137);
															BgL_odepthz00_4142 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4143);
														}
														if ((3L < BgL_odepthz00_4142))
															{	/* SawJvm/inline.scm 168 */
																obj_t BgL_arg1802z00_4144;

																{	/* SawJvm/inline.scm 168 */
																	obj_t BgL_arg1803z00_4145;

																	BgL_arg1803z00_4145 = (BgL_oclassz00_4137);
																	BgL_arg1802z00_4144 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4145,
																		3L);
																}
																BgL_res2125z00_4136 =
																	(BgL_arg1802z00_4144 == BgL_classz00_4131);
															}
														else
															{	/* SawJvm/inline.scm 168 */
																BgL_res2125z00_4136 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2363z00_4933 = BgL_res2125z00_4136;
									}
							}
						else
							{	/* SawJvm/inline.scm 168 */
								BgL_test2363z00_4933 = ((bool_t) 0);
							}
					}
					if (BgL_test2363z00_4933)
						{	/* SawJvm/inline.scm 168 */
							if (CBOOL(
									(((BgL_atomz00_bglt) COBJECT(
												(((BgL_rtl_loadiz00_bglt) COBJECT(
															((BgL_rtl_loadiz00_bglt) BgL_funz00_4129)))->
													BgL_constantz00)))->BgL_valuez00)))
								{	/* SawJvm/inline.scm 169 */
									return
										BGl_codez12z12zzsaw_jvm_outz00(
										((BgL_jvmz00_bglt) BgL_mez00_4042), CNST_TABLE_REF(171));
								}
							else
								{	/* SawJvm/inline.scm 169 */
									return
										BGl_codez12z12zzsaw_jvm_outz00(
										((BgL_jvmz00_bglt) BgL_mez00_4042), CNST_TABLE_REF(172));
								}
						}
					else
						{	/* SawJvm/inline.scm 172 */
							obj_t BgL_l1z00_4146;
							obj_t BgL_l2z00_4147;

							BgL_l1z00_4146 =
								BGl_gensymz00zz__r4_symbols_6_4z00
								(BGl_string2229z00zzsaw_jvm_inlinez00);
							BgL_l2z00_4147 =
								BGl_gensymz00zz__r4_symbols_6_4z00
								(BGl_string2229z00zzsaw_jvm_inlinez00);
							{	/* SawJvm/inline.scm 173 */
								obj_t BgL_arg1663z00_4148;

								BgL_arg1663z00_4148 = CAR(((obj_t) BgL_argsz00_4043));
								BGl_genzd2exprzd2zzsaw_jvm_codez00(
									((BgL_jvmz00_bglt) BgL_mez00_4042), BgL_arg1663z00_4148);
							}
							{	/* SawJvm/inline.scm 174 */
								obj_t BgL_arg1675z00_4149;

								{	/* SawJvm/inline.scm 174 */
									obj_t BgL_arg1678z00_4150;

									BgL_arg1678z00_4150 = MAKE_YOUNG_PAIR(BgL_l1z00_4146, BNIL);
									BgL_arg1675z00_4149 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(136), BgL_arg1678z00_4150);
								}
								BGl_codez12z12zzsaw_jvm_outz00(
									((BgL_jvmz00_bglt) BgL_mez00_4042), BgL_arg1675z00_4149);
							}
							BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_4042), CNST_TABLE_REF(171));
							{	/* SawJvm/inline.scm 176 */
								obj_t BgL_arg1681z00_4151;

								{	/* SawJvm/inline.scm 176 */
									obj_t BgL_arg1688z00_4152;

									BgL_arg1688z00_4152 = MAKE_YOUNG_PAIR(BgL_l2z00_4147, BNIL);
									BgL_arg1681z00_4151 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(173), BgL_arg1688z00_4152);
								}
								BGl_codez12z12zzsaw_jvm_outz00(
									((BgL_jvmz00_bglt) BgL_mez00_4042), BgL_arg1681z00_4151);
							}
							BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_4042), BgL_l1z00_4146);
							BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_4042), CNST_TABLE_REF(172));
							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_4042), BgL_l2z00_4147);
						}
				}
			}
		}

	}



/* &<@anonymous:1643> */
	obj_t BGl_z62zc3z04anonymousza31643ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4044, obj_t BgL_mez00_4045, obj_t BgL_onzf3zf3_4046,
		obj_t BgL_labz00_4047)
	{
		{	/* SawJvm/inline.scm 162 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4045), CNST_TABLE_REF(172));
			{	/* SawJvm/inline.scm 164 */
				obj_t BgL_arg1646z00_4153;

				if (CBOOL(BgL_onzf3zf3_4046))
					{	/* SawJvm/inline.scm 164 */
						BgL_arg1646z00_4153 = CNST_TABLE_REF(124);
					}
				else
					{	/* SawJvm/inline.scm 164 */
						BgL_arg1646z00_4153 = CNST_TABLE_REF(123);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4045), BgL_arg1646z00_4153,
					BgL_labz00_4047);
			}
		}

	}



/* &<@anonymous:1630> */
	obj_t BGl_z62zc3z04anonymousza31630ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4048, obj_t BgL_mez00_4049)
	{
		{	/* SawJvm/inline.scm 159 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4049), CNST_TABLE_REF(174));
		}

	}



/* &<@anonymous:1628> */
	obj_t BGl_z62zc3z04anonymousza31628ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4050, obj_t BgL_mez00_4051)
	{
		{	/* SawJvm/inline.scm 151 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4051), CNST_TABLE_REF(98));
			return CNST_TABLE_REF(99);
		}

	}



/* &<@anonymous:1626> */
	obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4052, obj_t BgL_mez00_4053)
	{
		{	/* SawJvm/inline.scm 148 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4053), CNST_TABLE_REF(97));
		}

	}



/* &<@anonymous:1616> */
	obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4054, obj_t BgL_mez00_4055)
	{
		{	/* SawJvm/inline.scm 145 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4055), CNST_TABLE_REF(175));
		}

	}



/* &<@anonymous:1610> */
	obj_t BGl_z62zc3z04anonymousza31610ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4056, obj_t BgL_mez00_4057, obj_t BgL_argsz00_4058)
	{
		{	/* SawJvm/inline.scm 133 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4057), CNST_TABLE_REF(176));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4057), CNST_TABLE_REF(177));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4057), CNST_TABLE_REF(178));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4057), CNST_TABLE_REF(177));
			{	/* SawJvm/inline.scm 138 */
				obj_t BgL_arg1611z00_4154;

				BgL_arg1611z00_4154 = CAR(((obj_t) BgL_argsz00_4058));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4057), BgL_arg1611z00_4154);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4057), CNST_TABLE_REF(179));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4057), CNST_TABLE_REF(177));
			{	/* SawJvm/inline.scm 141 */
				obj_t BgL_arg1613z00_4155;

				{	/* SawJvm/inline.scm 141 */
					obj_t BgL_pairz00_4156;

					BgL_pairz00_4156 = CDR(((obj_t) BgL_argsz00_4058));
					BgL_arg1613z00_4155 = CAR(BgL_pairz00_4156);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4057), BgL_arg1613z00_4155);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4057), CNST_TABLE_REF(175));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4057), CNST_TABLE_REF(180));
		}

	}



/* &<@anonymous:1595> */
	obj_t BGl_z62zc3z04anonymousza31595ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4059, obj_t BgL_mez00_4060, obj_t BgL_argsz00_4061)
	{
		{	/* SawJvm/inline.scm 118 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(176));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(177));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(178));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(177));
			{	/* SawJvm/inline.scm 123 */
				obj_t BgL_arg1602z00_4157;

				BgL_arg1602z00_4157 = CAR(((obj_t) BgL_argsz00_4061));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4060), BgL_arg1602z00_4157);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(179));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(177));
			{	/* SawJvm/inline.scm 126 */
				obj_t BgL_arg1605z00_4158;

				{	/* SawJvm/inline.scm 126 */
					obj_t BgL_pairz00_4159;

					BgL_pairz00_4159 = CDR(((obj_t) BgL_argsz00_4061));
					BgL_arg1605z00_4158 = CAR(BgL_pairz00_4159);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4060), BgL_arg1605z00_4158);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(181));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(177));
			{	/* SawJvm/inline.scm 129 */
				obj_t BgL_arg1606z00_4160;

				{	/* SawJvm/inline.scm 129 */
					obj_t BgL_pairz00_4161;

					{	/* SawJvm/inline.scm 129 */
						obj_t BgL_pairz00_4162;

						BgL_pairz00_4162 = CDR(((obj_t) BgL_argsz00_4061));
						BgL_pairz00_4161 = CDR(BgL_pairz00_4162);
					}
					BgL_arg1606z00_4160 = CAR(BgL_pairz00_4161);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4060), BgL_arg1606z00_4160);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(175));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4060), CNST_TABLE_REF(180));
		}

	}



/* &<@anonymous:1585> */
	obj_t BGl_z62zc3z04anonymousza31585ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4062, obj_t BgL_mez00_4063, obj_t BgL_argsz00_4064)
	{
		{	/* SawJvm/inline.scm 103 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(176));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(177));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(178));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(177));
			{	/* SawJvm/inline.scm 108 */
				obj_t BgL_arg1589z00_4163;

				BgL_arg1589z00_4163 = CAR(((obj_t) BgL_argsz00_4064));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4063), BgL_arg1589z00_4163);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(179));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(177));
			{	/* SawJvm/inline.scm 111 */
				obj_t BgL_arg1591z00_4164;

				{	/* SawJvm/inline.scm 111 */
					obj_t BgL_pairz00_4165;

					BgL_pairz00_4165 = CDR(((obj_t) BgL_argsz00_4064));
					BgL_arg1591z00_4164 = CAR(BgL_pairz00_4165);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4063), BgL_arg1591z00_4164);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(181));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(177));
			{	/* SawJvm/inline.scm 114 */
				obj_t BgL_arg1593z00_4166;

				{	/* SawJvm/inline.scm 114 */
					obj_t BgL_pairz00_4167;

					{	/* SawJvm/inline.scm 114 */
						obj_t BgL_pairz00_4168;

						BgL_pairz00_4168 = CDR(((obj_t) BgL_argsz00_4064));
						BgL_pairz00_4167 = CDR(BgL_pairz00_4168);
					}
					BgL_arg1593z00_4166 = CAR(BgL_pairz00_4167);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4063), BgL_arg1593z00_4166);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(175));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4063), CNST_TABLE_REF(180));
		}

	}



/* &<@anonymous:1562> */
	obj_t BGl_z62zc3z04anonymousza31562ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4065, obj_t BgL_mez00_4066, obj_t BgL_argsz00_4067)
	{
		{	/* SawJvm/inline.scm 96 */
			{	/* SawJvm/inline.scm 97 */
				obj_t BgL_arg1564z00_4169;

				{	/* SawJvm/inline.scm 97 */
					obj_t BgL_arg1565z00_4170;

					{	/* SawJvm/inline.scm 97 */
						obj_t BgL_arg1571z00_4171;

						{	/* SawJvm/inline.scm 97 */
							obj_t BgL_arg1573z00_4172;

							BgL_arg1573z00_4172 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
							BgL_arg1571z00_4171 =
								BGl_declarezd2globalzd2zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_4066),
								((BgL_globalz00_bglt) BgL_arg1573z00_4172));
						}
						BgL_arg1565z00_4170 = MAKE_YOUNG_PAIR(BgL_arg1571z00_4171, BNIL);
					}
					BgL_arg1564z00_4169 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(168), BgL_arg1565z00_4170);
				}
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4066), BgL_arg1564z00_4169);
			}
			{	/* SawJvm/inline.scm 98 */
				obj_t BgL_arg1575z00_4173;

				BgL_arg1575z00_4173 = CAR(((obj_t) BgL_argsz00_4067));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4066), BgL_arg1575z00_4173);
			}
			{	/* SawJvm/inline.scm 99 */
				obj_t BgL_arg1576z00_4174;

				{	/* SawJvm/inline.scm 99 */
					obj_t BgL_pairz00_4175;

					BgL_pairz00_4175 = CDR(((obj_t) BgL_argsz00_4067));
					BgL_arg1576z00_4174 = CAR(BgL_pairz00_4175);
				}
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4066), BgL_arg1576z00_4174);
			}
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4066), CNST_TABLE_REF(98));
			return CNST_TABLE_REF(99);
		}

	}



/* &<@anonymous:1541> */
	obj_t BGl_z62zc3z04anonymousza31541ze3ze5zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4068, obj_t BgL_mez00_4069, obj_t BgL_argsz00_4070)
	{
		{	/* SawJvm/inline.scm 91 */
			{	/* SawJvm/inline.scm 92 */
				obj_t BgL_arg1544z00_4176;

				{	/* SawJvm/inline.scm 92 */
					obj_t BgL_arg1546z00_4177;

					{	/* SawJvm/inline.scm 92 */
						obj_t BgL_arg1552z00_4178;

						{	/* SawJvm/inline.scm 92 */
							obj_t BgL_arg1553z00_4179;

							BgL_arg1553z00_4179 = BGl_getzd2cnstzd2tablez00zzcnst_allocz00();
							BgL_arg1552z00_4178 =
								BGl_declarezd2globalzd2zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_4069),
								((BgL_globalz00_bglt) BgL_arg1553z00_4179));
						}
						BgL_arg1546z00_4177 = MAKE_YOUNG_PAIR(BgL_arg1552z00_4178, BNIL);
					}
					BgL_arg1544z00_4176 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(168), BgL_arg1546z00_4177);
				}
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_4069), BgL_arg1544z00_4176);
			}
			{	/* SawJvm/inline.scm 93 */
				obj_t BgL_arg1559z00_4180;

				BgL_arg1559z00_4180 = CAR(((obj_t) BgL_argsz00_4070));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_4069), BgL_arg1559z00_4180);
			}
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_4069), CNST_TABLE_REF(97));
		}

	}



/* check-jvm-inlines */
	BGL_EXPORTED_DEF obj_t BGl_checkzd2jvmzd2inlinesz00zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 24 */
			{	/* SawJvm/inline.scm 25 */
				obj_t BgL_unresolvedz00_2919;

				{	/* SawJvm/inline.scm 25 */
					obj_t BgL_hook1539z00_2921;

					BgL_hook1539z00_2921 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					{
						obj_t BgL_l1536z00_2923;
						obj_t BgL_h1537z00_2924;

						BgL_l1536z00_2923 = BNIL;
						BgL_h1537z00_2924 = BgL_hook1539z00_2921;
					BgL_zc3z04anonymousza31979ze3z87_2925:
						if (NULLP(BgL_l1536z00_2923))
							{	/* SawJvm/inline.scm 25 */
								BgL_unresolvedz00_2919 = CDR(BgL_hook1539z00_2921);
							}
						else
							{	/* SawJvm/inline.scm 25 */
								bool_t BgL_test2371z00_5180;

								if (CBOOL(BGl_findzd2globalzd2zzast_envz00(CAR(
												((obj_t) BgL_l1536z00_2923)), BNIL)))
									{	/* SawJvm/inline.scm 25 */
										BgL_test2371z00_5180 = ((bool_t) 0);
									}
								else
									{	/* SawJvm/inline.scm 25 */
										BgL_test2371z00_5180 = ((bool_t) 1);
									}
								if (BgL_test2371z00_5180)
									{	/* SawJvm/inline.scm 25 */
										obj_t BgL_nh1538z00_2932;

										{	/* SawJvm/inline.scm 25 */
											obj_t BgL_arg1986z00_2934;

											BgL_arg1986z00_2934 = CAR(((obj_t) BgL_l1536z00_2923));
											BgL_nh1538z00_2932 =
												MAKE_YOUNG_PAIR(BgL_arg1986z00_2934, BNIL);
										}
										SET_CDR(BgL_h1537z00_2924, BgL_nh1538z00_2932);
										{	/* SawJvm/inline.scm 25 */
											obj_t BgL_arg1985z00_2933;

											BgL_arg1985z00_2933 = CDR(((obj_t) BgL_l1536z00_2923));
											{
												obj_t BgL_h1537z00_5193;
												obj_t BgL_l1536z00_5192;

												BgL_l1536z00_5192 = BgL_arg1985z00_2933;
												BgL_h1537z00_5193 = BgL_nh1538z00_2932;
												BgL_h1537z00_2924 = BgL_h1537z00_5193;
												BgL_l1536z00_2923 = BgL_l1536z00_5192;
												goto BgL_zc3z04anonymousza31979ze3z87_2925;
											}
										}
									}
								else
									{	/* SawJvm/inline.scm 25 */
										obj_t BgL_arg1987z00_2935;

										BgL_arg1987z00_2935 = CDR(((obj_t) BgL_l1536z00_2923));
										{
											obj_t BgL_l1536z00_5196;

											BgL_l1536z00_5196 = BgL_arg1987z00_2935;
											BgL_l1536z00_2923 = BgL_l1536z00_5196;
											goto BgL_zc3z04anonymousza31979ze3z87_2925;
										}
									}
							}
					}
				}
				if (PAIRP(BgL_unresolvedz00_2919))
					{	/* SawJvm/inline.scm 27 */
						return
							BGl_errorz00zz__errorz00(CNST_TABLE_REF(182),
							BGl_string2230z00zzsaw_jvm_inlinez00, BgL_unresolvedz00_2919);
					}
				else
					{	/* SawJvm/inline.scm 27 */
						return BFALSE;
					}
			}
		}

	}



/* &check-jvm-inlines */
	obj_t BGl_z62checkzd2jvmzd2inlinesz62zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4071)
	{
		{	/* SawJvm/inline.scm 24 */
			return BGl_checkzd2jvmzd2inlinesz00zzsaw_jvm_inlinez00();
		}

	}



/* inline-call? */
	BGL_EXPORTED_DEF obj_t
		BGl_inlinezd2callzf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt BgL_mez00_3,
		BgL_globalz00_bglt BgL_varz00_4)
	{
		{	/* SawJvm/inline.scm 35 */
			if (CBOOL((((BgL_jvmz00_bglt) COBJECT(BgL_mez00_3))->BgL_inlinez00)))
				{	/* SawJvm/inline.scm 37 */
					obj_t BgL_funz00_2941;

					BgL_funz00_2941 =
						BGl_getpropz00zz__r4_symbols_6_4z00(
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_4)))->BgL_idz00),
						CNST_TABLE_REF(8));
					if (CBOOL(BgL_funz00_2941))
						{	/* SawJvm/inline.scm 38 */
							return
								BGL_PROCEDURE_CALL1(BgL_funz00_2941, ((obj_t) BgL_mez00_3));
						}
					else
						{	/* SawJvm/inline.scm 38 */
							return CNST_TABLE_REF(170);
						}
				}
			else
				{	/* SawJvm/inline.scm 36 */
					return BFALSE;
				}
		}

	}



/* &inline-call? */
	obj_t BGl_z62inlinezd2callzf3z43zzsaw_jvm_inlinez00(obj_t BgL_envz00_4072,
		obj_t BgL_mez00_4073, obj_t BgL_varz00_4074)
	{
		{	/* SawJvm/inline.scm 35 */
			return
				BGl_inlinezd2callzf3z21zzsaw_jvm_inlinez00(
				((BgL_jvmz00_bglt) BgL_mez00_4073),
				((BgL_globalz00_bglt) BgL_varz00_4074));
		}

	}



/* inline-call-with-args? */
	BGL_EXPORTED_DEF obj_t
		BGl_inlinezd2callzd2withzd2argszf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt
		BgL_mez00_5, BgL_globalz00_bglt BgL_varz00_6, obj_t BgL_argsz00_7)
	{
		{	/* SawJvm/inline.scm 52 */
			if (CBOOL((((BgL_jvmz00_bglt) COBJECT(BgL_mez00_5))->BgL_inlinez00)))
				{	/* SawJvm/inline.scm 54 */
					obj_t BgL_funz00_2946;

					BgL_funz00_2946 =
						BGl_getpropz00zz__r4_symbols_6_4z00(
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_6)))->BgL_idz00),
						CNST_TABLE_REF(2));
					if (CBOOL(BgL_funz00_2946))
						{	/* SawJvm/inline.scm 55 */
							return
								BGL_PROCEDURE_CALL2(BgL_funz00_2946,
								((obj_t) BgL_mez00_5), BgL_argsz00_7);
						}
					else
						{	/* SawJvm/inline.scm 55 */
							return CNST_TABLE_REF(170);
						}
				}
			else
				{	/* SawJvm/inline.scm 53 */
					return BFALSE;
				}
		}

	}



/* &inline-call-with-args? */
	obj_t BGl_z62inlinezd2callzd2withzd2argszf3z43zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4075, obj_t BgL_mez00_4076, obj_t BgL_varz00_4077,
		obj_t BgL_argsz00_4078)
	{
		{	/* SawJvm/inline.scm 52 */
			return
				BGl_inlinezd2callzd2withzd2argszf3z21zzsaw_jvm_inlinez00(
				((BgL_jvmz00_bglt) BgL_mez00_4076),
				((BgL_globalz00_bglt) BgL_varz00_4077), BgL_argsz00_4078);
		}

	}



/* inline-predicate? */
	BGL_EXPORTED_DEF obj_t
		BGl_inlinezd2predicatezf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt BgL_mez00_8,
		BgL_globalz00_bglt BgL_varz00_9, obj_t BgL_onzf3zf3_10, obj_t BgL_labz00_11)
	{
		{	/* SawJvm/inline.scm 66 */
			if (CBOOL((((BgL_jvmz00_bglt) COBJECT(BgL_mez00_8))->BgL_inlinez00)))
				{	/* SawJvm/inline.scm 68 */
					obj_t BgL_funz00_3547;

					BgL_funz00_3547 =
						BGl_getpropz00zz__r4_symbols_6_4z00(
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_9)))->BgL_idz00),
						CNST_TABLE_REF(13));
					if (CBOOL(BgL_funz00_3547))
						{	/* SawJvm/inline.scm 69 */
							return
								BGL_PROCEDURE_CALL3(BgL_funz00_3547,
								((obj_t) BgL_mez00_8), BgL_onzf3zf3_10, BgL_labz00_11);
						}
					else
						{	/* SawJvm/inline.scm 69 */
							return CNST_TABLE_REF(170);
						}
				}
			else
				{	/* SawJvm/inline.scm 67 */
					return BFALSE;
				}
		}

	}



/* &inline-predicate? */
	obj_t BGl_z62inlinezd2predicatezf3z43zzsaw_jvm_inlinez00(obj_t
		BgL_envz00_4079, obj_t BgL_mez00_4080, obj_t BgL_varz00_4081,
		obj_t BgL_onzf3zf3_4082, obj_t BgL_labz00_4083)
	{
		{	/* SawJvm/inline.scm 66 */
			return
				BGl_inlinezd2predicatezf3z21zzsaw_jvm_inlinez00(
				((BgL_jvmz00_bglt) BgL_mez00_4080),
				((BgL_globalz00_bglt) BgL_varz00_4081), BgL_onzf3zf3_4082,
				BgL_labz00_4083);
		}

	}



/* skip-mov */
	obj_t BGl_skipzd2movzd2zzsaw_jvm_inlinez00(obj_t BgL_argz00_12)
	{
		{	/* SawJvm/inline.scm 80 */
		BGl_skipzd2movzd2zzsaw_jvm_inlinez00:
			{	/* SawJvm/inline.scm 81 */
				bool_t BgL_test2380z00_5259;

				{	/* SawJvm/inline.scm 81 */
					obj_t BgL_classz00_3552;

					BgL_classz00_3552 = BGl_rtl_insz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_argz00_12))
						{	/* SawJvm/inline.scm 81 */
							BgL_objectz00_bglt BgL_arg1807z00_3554;

							BgL_arg1807z00_3554 = (BgL_objectz00_bglt) (BgL_argz00_12);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/inline.scm 81 */
									long BgL_idxz00_3560;

									BgL_idxz00_3560 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3554);
									BgL_test2380z00_5259 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3560 + 1L)) == BgL_classz00_3552);
								}
							else
								{	/* SawJvm/inline.scm 81 */
									bool_t BgL_res2126z00_3585;

									{	/* SawJvm/inline.scm 81 */
										obj_t BgL_oclassz00_3568;

										{	/* SawJvm/inline.scm 81 */
											obj_t BgL_arg1815z00_3576;
											long BgL_arg1816z00_3577;

											BgL_arg1815z00_3576 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/inline.scm 81 */
												long BgL_arg1817z00_3578;

												BgL_arg1817z00_3578 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3554);
												BgL_arg1816z00_3577 =
													(BgL_arg1817z00_3578 - OBJECT_TYPE);
											}
											BgL_oclassz00_3568 =
												VECTOR_REF(BgL_arg1815z00_3576, BgL_arg1816z00_3577);
										}
										{	/* SawJvm/inline.scm 81 */
											bool_t BgL__ortest_1115z00_3569;

											BgL__ortest_1115z00_3569 =
												(BgL_classz00_3552 == BgL_oclassz00_3568);
											if (BgL__ortest_1115z00_3569)
												{	/* SawJvm/inline.scm 81 */
													BgL_res2126z00_3585 = BgL__ortest_1115z00_3569;
												}
											else
												{	/* SawJvm/inline.scm 81 */
													long BgL_odepthz00_3570;

													{	/* SawJvm/inline.scm 81 */
														obj_t BgL_arg1804z00_3571;

														BgL_arg1804z00_3571 = (BgL_oclassz00_3568);
														BgL_odepthz00_3570 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3571);
													}
													if ((1L < BgL_odepthz00_3570))
														{	/* SawJvm/inline.scm 81 */
															obj_t BgL_arg1802z00_3573;

															{	/* SawJvm/inline.scm 81 */
																obj_t BgL_arg1803z00_3574;

																BgL_arg1803z00_3574 = (BgL_oclassz00_3568);
																BgL_arg1802z00_3573 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3574,
																	1L);
															}
															BgL_res2126z00_3585 =
																(BgL_arg1802z00_3573 == BgL_classz00_3552);
														}
													else
														{	/* SawJvm/inline.scm 81 */
															BgL_res2126z00_3585 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2380z00_5259 = BgL_res2126z00_3585;
								}
						}
					else
						{	/* SawJvm/inline.scm 81 */
							BgL_test2380z00_5259 = ((bool_t) 0);
						}
				}
				if (BgL_test2380z00_5259)
					{	/* SawJvm/inline.scm 82 */
						BgL_rtl_funz00_bglt BgL_funz00_2952;

						BgL_funz00_2952 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_argz00_12)))->BgL_funz00);
						{	/* SawJvm/inline.scm 83 */
							bool_t BgL_test2385z00_5284;

							{	/* SawJvm/inline.scm 83 */
								obj_t BgL_classz00_3587;

								BgL_classz00_3587 = BGl_rtl_movz00zzsaw_defsz00;
								{	/* SawJvm/inline.scm 83 */
									BgL_objectz00_bglt BgL_arg1807z00_3589;

									{	/* SawJvm/inline.scm 83 */
										obj_t BgL_tmpz00_5285;

										BgL_tmpz00_5285 =
											((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2952));
										BgL_arg1807z00_3589 =
											(BgL_objectz00_bglt) (BgL_tmpz00_5285);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawJvm/inline.scm 83 */
											long BgL_idxz00_3595;

											BgL_idxz00_3595 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3589);
											BgL_test2385z00_5284 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3595 + 3L)) == BgL_classz00_3587);
										}
									else
										{	/* SawJvm/inline.scm 83 */
											bool_t BgL_res2127z00_3620;

											{	/* SawJvm/inline.scm 83 */
												obj_t BgL_oclassz00_3603;

												{	/* SawJvm/inline.scm 83 */
													obj_t BgL_arg1815z00_3611;
													long BgL_arg1816z00_3612;

													BgL_arg1815z00_3611 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawJvm/inline.scm 83 */
														long BgL_arg1817z00_3613;

														BgL_arg1817z00_3613 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3589);
														BgL_arg1816z00_3612 =
															(BgL_arg1817z00_3613 - OBJECT_TYPE);
													}
													BgL_oclassz00_3603 =
														VECTOR_REF(BgL_arg1815z00_3611,
														BgL_arg1816z00_3612);
												}
												{	/* SawJvm/inline.scm 83 */
													bool_t BgL__ortest_1115z00_3604;

													BgL__ortest_1115z00_3604 =
														(BgL_classz00_3587 == BgL_oclassz00_3603);
													if (BgL__ortest_1115z00_3604)
														{	/* SawJvm/inline.scm 83 */
															BgL_res2127z00_3620 = BgL__ortest_1115z00_3604;
														}
													else
														{	/* SawJvm/inline.scm 83 */
															long BgL_odepthz00_3605;

															{	/* SawJvm/inline.scm 83 */
																obj_t BgL_arg1804z00_3606;

																BgL_arg1804z00_3606 = (BgL_oclassz00_3603);
																BgL_odepthz00_3605 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3606);
															}
															if ((3L < BgL_odepthz00_3605))
																{	/* SawJvm/inline.scm 83 */
																	obj_t BgL_arg1802z00_3608;

																	{	/* SawJvm/inline.scm 83 */
																		obj_t BgL_arg1803z00_3609;

																		BgL_arg1803z00_3609 = (BgL_oclassz00_3603);
																		BgL_arg1802z00_3608 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3609, 3L);
																	}
																	BgL_res2127z00_3620 =
																		(BgL_arg1802z00_3608 == BgL_classz00_3587);
																}
															else
																{	/* SawJvm/inline.scm 83 */
																	BgL_res2127z00_3620 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2385z00_5284 = BgL_res2127z00_3620;
										}
								}
							}
							if (BgL_test2385z00_5284)
								{	/* SawJvm/inline.scm 84 */
									obj_t BgL_arg1997z00_2954;

									{	/* SawJvm/inline.scm 84 */
										obj_t BgL_pairz00_3622;

										BgL_pairz00_3622 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_argz00_12)))->BgL_argsz00);
										BgL_arg1997z00_2954 = CAR(BgL_pairz00_3622);
									}
									{
										obj_t BgL_argz00_5311;

										BgL_argz00_5311 = BgL_arg1997z00_2954;
										BgL_argz00_12 = BgL_argz00_5311;
										goto BGl_skipzd2movzd2zzsaw_jvm_inlinez00;
									}
								}
							else
								{	/* SawJvm/inline.scm 83 */
									return ((obj_t) BgL_funz00_2952);
								}
						}
					}
				else
					{	/* SawJvm/inline.scm 81 */
						return BgL_argz00_12;
					}
			}
		}

	}



/* compute-boolean */
	obj_t BGl_computezd2booleanzd2zzsaw_jvm_inlinez00(obj_t BgL_mez00_13,
		obj_t BgL_copz00_14)
	{
		{	/* SawJvm/inline.scm 265 */
			{	/* SawJvm/inline.scm 267 */
				obj_t BgL_l1z00_2956;
				obj_t BgL_l2z00_2957;

				BgL_l1z00_2956 =
					BGl_gensymz00zz__r4_symbols_6_4z00
					(BGl_string2229z00zzsaw_jvm_inlinez00);
				BgL_l2z00_2957 =
					BGl_gensymz00zz__r4_symbols_6_4z00
					(BGl_string2229z00zzsaw_jvm_inlinez00);
				{	/* SawJvm/inline.scm 268 */
					obj_t BgL_arg1999z00_2958;

					{	/* SawJvm/inline.scm 268 */
						obj_t BgL_arg2000z00_2959;

						BgL_arg2000z00_2959 = MAKE_YOUNG_PAIR(BgL_l1z00_2956, BNIL);
						BgL_arg1999z00_2958 =
							MAKE_YOUNG_PAIR(BgL_copz00_14, BgL_arg2000z00_2959);
					}
					BGl_codez12z12zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_13), BgL_arg1999z00_2958);
				}
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_13), CNST_TABLE_REF(183));
				{	/* SawJvm/inline.scm 270 */
					obj_t BgL_arg2001z00_2960;

					{	/* SawJvm/inline.scm 270 */
						obj_t BgL_arg2002z00_2961;

						BgL_arg2002z00_2961 = MAKE_YOUNG_PAIR(BgL_l2z00_2957, BNIL);
						BgL_arg2001z00_2960 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(173), BgL_arg2002z00_2961);
					}
					BGl_codez12z12zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_13), BgL_arg2001z00_2960);
				}
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_13), BgL_l1z00_2956);
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_13), CNST_TABLE_REF(144));
				return
					BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_13), BgL_l2z00_2957);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_inlinez00(void)
	{
		{	/* SawJvm/inline.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzcnst_allocz00(192699986L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzbackend_bvmz00(336068337L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(0L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_outz00(441641707L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_codez00(44765844L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2231z00zzsaw_jvm_inlinez00));
		}

	}

#ifdef __cplusplus
}
#endif
