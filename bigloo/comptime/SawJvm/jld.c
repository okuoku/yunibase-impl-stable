/*===========================================================================*/
/*   (SawJvm/jld.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawJvm/jld.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_JVM_LD_TYPE_DEFINITIONS
#define BGL_SAW_JVM_LD_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_SAW_JVM_LD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_libraryzd2ze3za7ipsz96zzsaw_jvm_ldz00(obj_t);
	extern obj_t BGl_za2jvmzd2javaza2zd2zzengine_paramz00;
	extern obj_t BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31757ze3ze5zzsaw_jvm_ldz00(obj_t);
	static obj_t BGl_listzd2ze3pathzd2stringze3zzsaw_jvm_ldz00(obj_t, obj_t);
	extern obj_t BGl_za2jvmzd2bigloozd2classpathza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t,
		unsigned char, unsigned char);
	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	static obj_t BGl_z62generatezd2jvmzd2env2161z62zzsaw_jvm_ldz00(void);
	static obj_t BGl_generatezd2shzd2jvmzd2manifestzd2zzsaw_jvm_ldz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_jvm_ldz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31952ze3ze5zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2sharedzd2libzd2namezd2zz__osz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static bool_t BGl_generatezd2jvmzd2msdoszd2scriptzd2zzsaw_jvm_ldz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_generatezd2msdoszd2jvmzd2manifestzd2zzsaw_jvm_ldz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_zc3z04anonymousza31327ze3ze70z60zzsaw_jvm_ldz00(obj_t);
	extern obj_t BGl_jvmzd2classzd2withzd2directoryzd2zzread_jvmz00(obj_t);
	BGL_IMPORT obj_t BGl_systemz00zz__osz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32030ze3ze5zzsaw_jvm_ldz00(obj_t);
	static obj_t BGl_findzd2jvmzd2mainz00zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_unprofzd2srczd2namez00zzengine_linkz00(obj_t);
	extern obj_t BGl_uncygdrivez00zztools_miscz00(obj_t);
	extern obj_t BGl_za2additionalzd2bigloozd2za7ipsza2za7zzengine_paramz00;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_jvm_ldz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_za2jvmzd2jarpathza2zd2zzengine_paramz00;
	static obj_t BGl_objectzd2initzd2zzsaw_jvm_ldz00(void);
	BGL_IMPORT obj_t BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00(obj_t,
		unsigned char, unsigned char);
	extern obj_t BGl_za2jvmzd2shellza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_dirnamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_objectszd2ze3classesz31zzsaw_jvm_ldz00(obj_t);
	static bool_t BGl_generatezd2jvmzd2scriptz00zzsaw_jvm_ldz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_basenamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_jvm_ldz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_jvm_ldz00(void);
	extern obj_t BGl_za2jvmzd2classpathza2zd2zzengine_paramz00;
	extern obj_t BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00;
	static obj_t BGl_uniquez00zzsaw_jvm_ldz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31996ze3ze5zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_userzd2libraryzd2zzsaw_jvm_ldz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_jvmzd2ldzd2zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31514ze3ze5zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2ze3listz31zz__hashz00(obj_t);
	static obj_t BGl_zc3z04exitza31710ze3ze70z60zzsaw_jvm_ldz00(void);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	extern obj_t BGl_za2jvmzd2mainclassza2zd2zzengine_paramz00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_IMPORT obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_ldz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_linkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_configurez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__libraryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	extern obj_t BGl_za2bigloozd2nameza2zd2zzengine_paramz00;
	extern obj_t BGl_sourcezd2ze3qualifiedzd2typeze3zzread_jvmz00(obj_t);
	static obj_t BGl_listzd2ze3shzd2pathzd2stringz31zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT bool_t fexists(char *);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_findzd2jvmzd2mainclassz00zzsaw_jvm_ldz00(obj_t);
	static obj_t BGl_generatezd2jvmzd2manifestz00zzsaw_jvm_ldz00(obj_t, obj_t,
		obj_t, obj_t);
	static bool_t BGl_generatezd2jvmzd2shzd2scriptzd2zzsaw_jvm_ldz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_makezd2manifestzd2namez00zzsaw_jvm_ldz00(void);
	BGL_IMPORT obj_t BGl_suffixz00zz__osz00(obj_t);
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	static obj_t BGl_z62generatezd2jvmzd2envz62zzsaw_jvm_ldz00(void);
	extern obj_t BGl_za2jvmzd2jarzf3za2z21zzengine_paramz00;
	extern obj_t BGl_za2unsafezd2libraryza2zd2zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzsaw_jvm_ldz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_ldz00(void);
	extern obj_t BGl_compilerzd2readzd2zzread_readerz00(obj_t);
	static obj_t BGl_listzd2ze3msdoszd2pathzd2stringz31zzsaw_jvm_ldz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_ldz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_ldz00(void);
	static obj_t BGl_manifestzd2classpathzd2formatz00zzsaw_jvm_ldz00(obj_t);
	extern obj_t BGl_findzd2srczd2filez00zzengine_linkz00(obj_t, obj_t);
	extern obj_t BGl_za2ozd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	static obj_t BGl_splitzd272zd2zzsaw_jvm_ldz00(obj_t);
	extern obj_t BGl_za2jvmzd2directoryza2zd2zzengine_paramz00;
	static obj_t BGl_z62zc3z04anonymousza31911ze3ze5zzsaw_jvm_ldz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31709ze3ze5zzsaw_jvm_ldz00(obj_t);
	BGL_IMPORT obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
	static obj_t BGl_z62jvmzd2ldzb0zzsaw_jvm_ldz00(obj_t, obj_t);
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	static obj_t BGl_z62zc3z04anonymousza31735ze3ze5zzsaw_jvm_ldz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	static obj_t BGl_jvmzd2jarzd2zzsaw_jvm_ldz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	static obj_t BGl_sourcezd2ze3jvmzd2classze3zzsaw_jvm_ldz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31930ze3ze5zzsaw_jvm_ldz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31736ze3ze5zzsaw_jvm_ldz00(obj_t);
	extern obj_t BGl_jvmzd2classzd2sanszd2directoryzd2zzread_jvmz00(obj_t);
	extern obj_t BGl_za2jvmzd2optionsza2zd2zzengine_paramz00;
	static obj_t BGl_jvmzd2bigloozd2classpathz00zzsaw_jvm_ldz00(void);
	static obj_t BGl_loopze70ze7zzsaw_jvm_ldz00(obj_t, long);
	BGL_IMPORT obj_t BGl_libraryzd2filezd2namez00zz__libraryz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	extern obj_t BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00;
	extern obj_t BGl_za2jvmzd2envza2zd2zzengine_paramz00;
	extern obj_t BGl_classzd2idzd2ze3typezd2namez31zzbackend_cplibz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_chmodz00zz__osz00(obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_za2purifyza2z00zzengine_paramz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	extern obj_t BGl_za2mainza2z00zzmodule_modulez00;
	static obj_t __cnst[16];


	   
		 
		DEFINE_STRING(BGl_string2200z00zzsaw_jvm_ldz00,
		BgL_bgl_string2200za700za7za7s2241za7, "manifest-classpath-format", 25);
	      DEFINE_STRING(BGl_string2201z00zzsaw_jvm_ldz00,
		BgL_bgl_string2201za700za7za7s2242za7, "Manifest-Version: 1.0", 21);
	      DEFINE_STRING(BGl_string2202z00zzsaw_jvm_ldz00,
		BgL_bgl_string2202za700za7za7s2243za7, "Main-Class: ", 12);
	      DEFINE_STRING(BGl_string2203z00zzsaw_jvm_ldz00,
		BgL_bgl_string2203za700za7za7s2244za7, "Class-Path: ", 12);
	      DEFINE_STRING(BGl_string2204z00zzsaw_jvm_ldz00,
		BgL_bgl_string2204za700za7za7s2245za7, "\n  ", 3);
	      DEFINE_STRING(BGl_string2205z00zzsaw_jvm_ldz00,
		BgL_bgl_string2205za700za7za7s2246za7, "bigloo_u.zip", 12);
	      DEFINE_STRING(BGl_string2206z00zzsaw_jvm_ldz00,
		BgL_bgl_string2206za700za7za7s2247za7, "bigloo_s.zip", 12);
	      DEFINE_STRING(BGl_string2207z00zzsaw_jvm_ldz00,
		BgL_bgl_string2207za700za7za7s2248za7, "Created-By: ", 12);
	      DEFINE_STRING(BGl_string2208z00zzsaw_jvm_ldz00,
		BgL_bgl_string2208za700za7za7s2249za7, "=$", 2);
	      DEFINE_STRING(BGl_string2209z00zzsaw_jvm_ldz00,
		BgL_bgl_string2209za700za7za7s2250za7, "-Dbigloo.", 9);
	      DEFINE_STRING(BGl_string2210z00zzsaw_jvm_ldz00,
		BgL_bgl_string2210za700za7za7s2251za7, "#!/bin/sh", 9);
	      DEFINE_STRING(BGl_string2211z00zzsaw_jvm_ldz00,
		BgL_bgl_string2211za700za7za7s2252za7, "CLASSPATH=", 10);
	      DEFINE_STRING(BGl_string2212z00zzsaw_jvm_ldz00,
		BgL_bgl_string2212za700za7za7s2253za7, "'", 1);
	      DEFINE_STRING(BGl_string2213z00zzsaw_jvm_ldz00,
		BgL_bgl_string2213za700za7za7s2254za7, "$BUGLOOCLASSPATH", 16);
	      DEFINE_STRING(BGl_string2214z00zzsaw_jvm_ldz00,
		BgL_bgl_string2214za700za7za7s2255za7, "$BIGLOOCLASSPATH", 16);
	      DEFINE_STRING(BGl_string2215z00zzsaw_jvm_ldz00,
		BgL_bgl_string2215za700za7za7s2256za7, "export CLASSPATH", 16);
	      DEFINE_STRING(BGl_string2216z00zzsaw_jvm_ldz00,
		BgL_bgl_string2216za700za7za7s2257za7, "exec ", 5);
	      DEFINE_STRING(BGl_string2217z00zzsaw_jvm_ldz00,
		BgL_bgl_string2217za700za7za7s2258za7, " $BIGLOOJAVAOPT $BUGLOOJAVAOPT ",
		31);
	      DEFINE_STRING(BGl_string2218z00zzsaw_jvm_ldz00,
		BgL_bgl_string2218za700za7za7s2259za7, " $*", 3);
	      DEFINE_STRING(BGl_string2219z00zzsaw_jvm_ldz00,
		BgL_bgl_string2219za700za7za7s2260za7, "CLASSPATH=\"", 11);
	      DEFINE_STRING(BGl_string2220z00zzsaw_jvm_ldz00,
		BgL_bgl_string2220za700za7za7s2261za7, "\"", 1);
	      DEFINE_STRING(BGl_string2221z00zzsaw_jvm_ldz00,
		BgL_bgl_string2221za700za7za7s2262za7, " -jar ", 6);
	      DEFINE_STRING(BGl_string2222z00zzsaw_jvm_ldz00,
		BgL_bgl_string2222za700za7za7s2263za7, "`", 1);
	      DEFINE_STRING(BGl_string2223z00zzsaw_jvm_ldz00,
		BgL_bgl_string2223za700za7za7s2264za7, " $0`", 4);
	      DEFINE_STRING(BGl_string2224z00zzsaw_jvm_ldz00,
		BgL_bgl_string2224za700za7za7s2265za7, ".jar $*", 7);
	      DEFINE_STRING(BGl_string2225z00zzsaw_jvm_ldz00,
		BgL_bgl_string2225za700za7za7s2266za7, "@", 1);
	      DEFINE_STRING(BGl_string2226z00zzsaw_jvm_ldz00,
		BgL_bgl_string2226za700za7za7s2267za7, " -cp \"", 6);
	      DEFINE_STRING(BGl_string2227z00zzsaw_jvm_ldz00,
		BgL_bgl_string2227za700za7za7s2268za7, "\\bigloo_u.zip", 13);
	      DEFINE_STRING(BGl_string2228z00zzsaw_jvm_ldz00,
		BgL_bgl_string2228za700za7za7s2269za7, "\\bigloo_s.zip", 13);
	      DEFINE_STRING(BGl_string2229z00zzsaw_jvm_ldz00,
		BgL_bgl_string2229za700za7za7s2270za7, "%BUGLOOCLASSPATH%", 17);
	      DEFINE_STRING(BGl_string2230z00zzsaw_jvm_ldz00,
		BgL_bgl_string2230za700za7za7s2271za7, "\" ", 2);
	      DEFINE_STRING(BGl_string2231z00zzsaw_jvm_ldz00,
		BgL_bgl_string2231za700za7za7s2272za7, " %BIGLOOJAVAOPT% %BUGLOOJAVAOPT% ",
		33);
	      DEFINE_STRING(BGl_string2232z00zzsaw_jvm_ldz00,
		BgL_bgl_string2232za700za7za7s2273za7, " %*", 3);
	      DEFINE_STRING(BGl_string2233z00zzsaw_jvm_ldz00,
		BgL_bgl_string2233za700za7za7s2274za7, "\\", 1);
	      DEFINE_STRING(BGl_string2234z00zzsaw_jvm_ldz00,
		BgL_bgl_string2234za700za7za7s2275za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2235z00zzsaw_jvm_ldz00,
		BgL_bgl_string2235za700za7za7s2276za7, "generate-jvm-script", 19);
	      DEFINE_STRING(BGl_string2236z00zzsaw_jvm_ldz00,
		BgL_bgl_string2236za700za7za7s2277za7, ":", 1);
	      DEFINE_STRING(BGl_string2237z00zzsaw_jvm_ldz00,
		BgL_bgl_string2237za700za7za7s2278za7, ";", 1);
	      DEFINE_STRING(BGl_string2238z00zzsaw_jvm_ldz00,
		BgL_bgl_string2238za700za7za7s2279za7, "saw_jvm_ld", 10);
	      DEFINE_STRING(BGl_string2239z00zzsaw_jvm_ldz00,
		BgL_bgl_string2239za700za7za7s2280za7,
		"dirname-cmd jflags jvflags read write execute zip-directory jar static export wide-class final-class abstract-class class main module ",
		134);
	      DEFINE_STRING(BGl_string2162z00zzsaw_jvm_ldz00,
		BgL_bgl_string2162za700za7za7s2281za7, ".jar", 4);
	      DEFINE_STRING(BGl_string2163z00zzsaw_jvm_ldz00,
		BgL_bgl_string2163za700za7za7s2282za7, "   . jar", 8);
	      DEFINE_STRING(BGl_string2164z00zzsaw_jvm_ldz00,
		BgL_bgl_string2164za700za7za7s2283za7, ")", 1);
	      DEFINE_STRING(BGl_string2165z00zzsaw_jvm_ldz00,
		BgL_bgl_string2165za700za7za7s2284za7, " (", 2);
	      DEFINE_STRING(BGl_string2166z00zzsaw_jvm_ldz00,
		BgL_bgl_string2166za700za7za7s2285za7, ".class", 6);
	      DEFINE_STRING(BGl_string2167z00zzsaw_jvm_ldz00,
		BgL_bgl_string2167za700za7za7s2286za7, "ld", 2);
	      DEFINE_STRING(BGl_string2168z00zzsaw_jvm_ldz00,
		BgL_bgl_string2168za700za7za7s2287za7, "Can't established JVM main class",
		32);
	      DEFINE_STRING(BGl_string2169z00zzsaw_jvm_ldz00,
		BgL_bgl_string2169za700za7za7s2288za7, "see option -jvm-mainclass", 25);
	      DEFINE_STRING(BGl_string2170z00zzsaw_jvm_ldz00,
		BgL_bgl_string2170za700za7za7s2289za7, "msdos", 5);
	      DEFINE_STRING(BGl_string2171z00zzsaw_jvm_ldz00,
		BgL_bgl_string2171za700za7za7s2290za7, "_u", 2);
	      DEFINE_STRING(BGl_string2172z00zzsaw_jvm_ldz00,
		BgL_bgl_string2172za700za7za7s2291za7, "_s", 2);
	      DEFINE_STRING(BGl_string2173z00zzsaw_jvm_ldz00,
		BgL_bgl_string2173za700za7za7s2292za7, "_eu", 3);
	      DEFINE_STRING(BGl_string2174z00zzsaw_jvm_ldz00,
		BgL_bgl_string2174za700za7za7s2293za7, "_es", 3);
	      DEFINE_STRING(BGl_string2175z00zzsaw_jvm_ldz00,
		BgL_bgl_string2175za700za7za7s2294za7,
		"Can't find ~a zip file (~a) for library -- ", 43);
	      DEFINE_STRING(BGl_string2176z00zzsaw_jvm_ldz00,
		BgL_bgl_string2176za700za7za7s2295za7, "ld:", 3);
	      DEFINE_STRING(BGl_string2177z00zzsaw_jvm_ldz00,
		BgL_bgl_string2177za700za7za7s2296za7,
		"Can't find zip file (~a) for library -- ", 40);
	      DEFINE_STRING(BGl_string2178z00zzsaw_jvm_ldz00,
		BgL_bgl_string2178za700za7za7s2297za7, "jar", 3);
	      DEFINE_STRING(BGl_string2179z00zzsaw_jvm_ldz00,
		BgL_bgl_string2179za700za7za7s2298za7, "No main clause found", 20);
	      DEFINE_STRING(BGl_string2181z00zzsaw_jvm_ldz00,
		BgL_bgl_string2181za700za7za7s2299za7, "mco", 3);
	      DEFINE_STRING(BGl_string2183z00zzsaw_jvm_ldz00,
		BgL_bgl_string2183za700za7za7s2300za7, ".", 1);
	      DEFINE_STRING(BGl_string2184z00zzsaw_jvm_ldz00,
		BgL_bgl_string2184za700za7za7s2301za7, "", 0);
	      DEFINE_STRING(BGl_string2186z00zzsaw_jvm_ldz00,
		BgL_bgl_string2186za700za7za7s2302za7, " ", 1);
	      DEFINE_STRING(BGl_string2187z00zzsaw_jvm_ldz00,
		BgL_bgl_string2187za700za7za7s2303za7, "-C ", 3);
	      DEFINE_STRING(BGl_string2188z00zzsaw_jvm_ldz00,
		BgL_bgl_string2188za700za7za7s2304za7, "]\n", 2);
	      DEFINE_STRING(BGl_string2189z00zzsaw_jvm_ldz00,
		BgL_bgl_string2189za700za7za7s2305za7, "      [", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2180z00zzsaw_jvm_ldz00,
		BgL_bgl_za762za7c3za704anonymo2306za7,
		BGl_z62zc3z04anonymousza31514ze3ze5zzsaw_jvm_ldz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2182z00zzsaw_jvm_ldz00,
		BgL_bgl_za762za7c3za704anonymo2307za7,
		BGl_z62zc3z04anonymousza31709ze3ze5zzsaw_jvm_ldz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2190z00zzsaw_jvm_ldz00,
		BgL_bgl_string2190za700za7za7s2308za7, "Can't create jar file", 21);
	      DEFINE_STRING(BGl_string2191z00zzsaw_jvm_ldz00,
		BgL_bgl_string2191za700za7za7s2309za7, "X", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2185z00zzsaw_jvm_ldz00,
		BgL_bgl_za762za7c3za704anonymo2310za7,
		BGl_z62zc3z04anonymousza31736ze3ze5zzsaw_jvm_ldz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2192z00zzsaw_jvm_ldz00,
		BgL_bgl_string2192za700za7za7s2311za7, "Manifest", 8);
	      DEFINE_STRING(BGl_string2193z00zzsaw_jvm_ldz00,
		BgL_bgl_string2193za700za7za7s2312za7, "sh", 2);
	      DEFINE_STRING(BGl_string2194z00zzsaw_jvm_ldz00,
		BgL_bgl_string2194za700za7za7s2313za7, "' -- using `sh'", 15);
	      DEFINE_STRING(BGl_string2195z00zzsaw_jvm_ldz00,
		BgL_bgl_string2195za700za7za7s2314za7, "Illegal shell `", 15);
	      DEFINE_STRING(BGl_string2196z00zzsaw_jvm_ldz00,
		BgL_bgl_string2196za700za7za7s2315za7, "generate-jvm-manifest", 21);
	      DEFINE_STRING(BGl_string2197z00zzsaw_jvm_ldz00,
		BgL_bgl_string2197za700za7za7s2316za7, "\n ", 2);
	      DEFINE_STRING(BGl_string2198z00zzsaw_jvm_ldz00,
		BgL_bgl_string2198za700za7za7s2317za7, "' --using `sh'", 14);
	      DEFINE_STRING(BGl_string2199z00zzsaw_jvm_ldz00,
		BgL_bgl_string2199za700za7za7s2318za7, "Illegal shell `:", 16);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jvmzd2ldzd2envz00zzsaw_jvm_ldz00,
		BgL_bgl_za762jvmza7d2ldza7b0za7za72319za7,
		BGl_z62jvmzd2ldzb0zzsaw_jvm_ldz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_jvm_ldz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_ldz00(long
		BgL_checksumz00_3259, char *BgL_fromz00_3260)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_jvm_ldz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_jvm_ldz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_jvm_ldz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_jvm_ldz00();
					BGl_cnstzd2initzd2zzsaw_jvm_ldz00();
					BGl_importedzd2moduleszd2initz00zzsaw_jvm_ldz00();
					return BGl_methodzd2initzd2zzsaw_jvm_ldz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 16 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__libraryz00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_jvm_ld");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "saw_jvm_ld");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 16 */
			{	/* SawJvm/jld.scm 16 */
				obj_t BgL_cportz00_3132;

				{	/* SawJvm/jld.scm 16 */
					obj_t BgL_stringz00_3139;

					BgL_stringz00_3139 = BGl_string2239z00zzsaw_jvm_ldz00;
					{	/* SawJvm/jld.scm 16 */
						obj_t BgL_startz00_3140;

						BgL_startz00_3140 = BINT(0L);
						{	/* SawJvm/jld.scm 16 */
							obj_t BgL_endz00_3141;

							BgL_endz00_3141 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3139)));
							{	/* SawJvm/jld.scm 16 */

								BgL_cportz00_3132 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3139, BgL_startz00_3140, BgL_endz00_3141);
				}}}}
				{
					long BgL_iz00_3133;

					BgL_iz00_3133 = 15L;
				BgL_loopz00_3134:
					if ((BgL_iz00_3133 == -1L))
						{	/* SawJvm/jld.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* SawJvm/jld.scm 16 */
							{	/* SawJvm/jld.scm 16 */
								obj_t BgL_arg2240z00_3135;

								{	/* SawJvm/jld.scm 16 */

									{	/* SawJvm/jld.scm 16 */
										obj_t BgL_locationz00_3137;

										BgL_locationz00_3137 = BBOOL(((bool_t) 0));
										{	/* SawJvm/jld.scm 16 */

											BgL_arg2240z00_3135 =
												BGl_readz00zz__readerz00(BgL_cportz00_3132,
												BgL_locationz00_3137);
										}
									}
								}
								{	/* SawJvm/jld.scm 16 */
									int BgL_tmpz00_3295;

									BgL_tmpz00_3295 = (int) (BgL_iz00_3133);
									CNST_TABLE_SET(BgL_tmpz00_3295, BgL_arg2240z00_3135);
							}}
							{	/* SawJvm/jld.scm 16 */
								int BgL_auxz00_3138;

								BgL_auxz00_3138 = (int) ((BgL_iz00_3133 - 1L));
								{
									long BgL_iz00_3300;

									BgL_iz00_3300 = (long) (BgL_auxz00_3138);
									BgL_iz00_3133 = BgL_iz00_3300;
									goto BgL_loopz00_3134;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_jvm_ldz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1747;

				BgL_headz00_1747 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1748;
					obj_t BgL_tailz00_1749;

					BgL_prevz00_1748 = BgL_headz00_1747;
					BgL_tailz00_1749 = BgL_l1z00_1;
				BgL_loopz00_1750:
					if (PAIRP(BgL_tailz00_1749))
						{
							obj_t BgL_newzd2prevzd2_1752;

							BgL_newzd2prevzd2_1752 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1749), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1748, BgL_newzd2prevzd2_1752);
							{
								obj_t BgL_tailz00_3310;
								obj_t BgL_prevz00_3309;

								BgL_prevz00_3309 = BgL_newzd2prevzd2_1752;
								BgL_tailz00_3310 = CDR(BgL_tailz00_1749);
								BgL_tailz00_1749 = BgL_tailz00_3310;
								BgL_prevz00_1748 = BgL_prevz00_3309;
								goto BgL_loopz00_1750;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1747);
				}
			}
		}

	}



/* jvm-ld */
	BGL_EXPORTED_DEF obj_t BGl_jvmzd2ldzd2zzsaw_jvm_ldz00(obj_t
		BgL_linkzd2mainzd2modulez00_3)
	{
		{	/* SawJvm/jld.scm 39 */
			{	/* SawJvm/jld.scm 40 */
				obj_t BgL_targetz00_1755;

				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* SawJvm/jld.scm 40 */
						BgL_targetz00_1755 = BGl_za2destza2z00zzengine_paramz00;
					}
				else
					{	/* SawJvm/jld.scm 40 */
						BgL_targetz00_1755 = string_to_bstring(BGL_DEFAULT_A_BAT);
					}
				{	/* SawJvm/jld.scm 40 */
					obj_t BgL_jarnamez00_1756;

					BgL_jarnamez00_1756 =
						string_append(BGl_prefixz00zz__osz00(BgL_targetz00_1755),
						BGl_string2162z00zzsaw_jvm_ldz00);
					{	/* SawJvm/jld.scm 43 */
						obj_t BgL_za7ipsza7_1757;

						{	/* SawJvm/jld.scm 45 */
							obj_t BgL_arg1326z00_1786;

							BgL_arg1326z00_1786 =
								BGl_zc3z04anonymousza31327ze3ze70z60zzsaw_jvm_ldz00
								(BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00);
							BgL_za7ipsza7_1757 =
								BGl_appendzd221011zd2zzsaw_jvm_ldz00(BgL_arg1326z00_1786,
								BGl_za2additionalzd2bigloozd2za7ipsza2za7zzengine_paramz00);
						}
						{	/* SawJvm/jld.scm 44 */

							if (CBOOL(BGl_za2jvmzd2jarzf3za2z21zzengine_paramz00))
								{	/* SawJvm/jld.scm 48 */
									obj_t BgL_manifestz00_1758;

									BgL_manifestz00_1758 =
										BGl_makezd2manifestzd2namez00zzsaw_jvm_ldz00();
									{	/* SawJvm/jld.scm 48 */
										obj_t BgL_ozd2fileszd2_1759;

										{	/* SawJvm/jld.scm 49 */
											obj_t BgL_arg1315z00_1768;

											{	/* SawJvm/jld.scm 49 */
												obj_t BgL_l1256z00_1769;

												BgL_l1256z00_1769 =
													BGl_za2srczd2filesza2zd2zzengine_paramz00;
												if (NULLP(BgL_l1256z00_1769))
													{	/* SawJvm/jld.scm 49 */
														BgL_arg1315z00_1768 = BNIL;
													}
												else
													{	/* SawJvm/jld.scm 49 */
														obj_t BgL_head1258z00_1771;

														{	/* SawJvm/jld.scm 49 */
															obj_t BgL_arg1322z00_1783;

															{	/* SawJvm/jld.scm 49 */
																obj_t BgL_arg1323z00_1784;

																BgL_arg1323z00_1784 =
																	CAR(((obj_t) BgL_l1256z00_1769));
																BgL_arg1322z00_1783 =
																	BGl_sourcezd2ze3jvmzd2classze3zzsaw_jvm_ldz00
																	(BgL_arg1323z00_1784);
															}
															BgL_head1258z00_1771 =
																MAKE_YOUNG_PAIR(BgL_arg1322z00_1783, BNIL);
														}
														{	/* SawJvm/jld.scm 49 */
															obj_t BgL_g1261z00_1772;

															BgL_g1261z00_1772 =
																CDR(((obj_t) BgL_l1256z00_1769));
															{
																obj_t BgL_l1256z00_1774;
																obj_t BgL_tail1259z00_1775;

																BgL_l1256z00_1774 = BgL_g1261z00_1772;
																BgL_tail1259z00_1775 = BgL_head1258z00_1771;
															BgL_zc3z04anonymousza31317ze3z87_1776:
																if (NULLP(BgL_l1256z00_1774))
																	{	/* SawJvm/jld.scm 49 */
																		BgL_arg1315z00_1768 = BgL_head1258z00_1771;
																	}
																else
																	{	/* SawJvm/jld.scm 49 */
																		obj_t BgL_newtail1260z00_1778;

																		{	/* SawJvm/jld.scm 49 */
																			obj_t BgL_arg1320z00_1780;

																			{	/* SawJvm/jld.scm 49 */
																				obj_t BgL_arg1321z00_1781;

																				BgL_arg1321z00_1781 =
																					CAR(((obj_t) BgL_l1256z00_1774));
																				BgL_arg1320z00_1780 =
																					BGl_sourcezd2ze3jvmzd2classze3zzsaw_jvm_ldz00
																					(BgL_arg1321z00_1781);
																			}
																			BgL_newtail1260z00_1778 =
																				MAKE_YOUNG_PAIR(BgL_arg1320z00_1780,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1259z00_1775,
																			BgL_newtail1260z00_1778);
																		{	/* SawJvm/jld.scm 49 */
																			obj_t BgL_arg1319z00_1779;

																			BgL_arg1319z00_1779 =
																				CDR(((obj_t) BgL_l1256z00_1774));
																			{
																				obj_t BgL_tail1259z00_3341;
																				obj_t BgL_l1256z00_3340;

																				BgL_l1256z00_3340 = BgL_arg1319z00_1779;
																				BgL_tail1259z00_3341 =
																					BgL_newtail1260z00_1778;
																				BgL_tail1259z00_1775 =
																					BgL_tail1259z00_3341;
																				BgL_l1256z00_1774 = BgL_l1256z00_3340;
																				goto
																					BgL_zc3z04anonymousza31317ze3z87_1776;
																			}
																		}
																	}
															}
														}
													}
											}
											BgL_ozd2fileszd2_1759 =
												BGl_appendzd221011zd2zzsaw_jvm_ldz00
												(BgL_arg1315z00_1768,
												BGl_za2ozd2filesza2zd2zzengine_paramz00);
										}
										{	/* SawJvm/jld.scm 49 */
											obj_t BgL_allzd2objectszd2_1760;

											BgL_allzd2objectszd2_1760 =
												BGl_uniquez00zzsaw_jvm_ldz00
												(BGl_objectszd2ze3classesz31zzsaw_jvm_ldz00
												(BgL_ozd2fileszd2_1759));
											{	/* SawJvm/jld.scm 51 */

												{	/* SawJvm/jld.scm 52 */
													obj_t BgL_list1306z00_1761;

													BgL_list1306z00_1761 =
														MAKE_YOUNG_PAIR(BGl_string2163z00zzsaw_jvm_ldz00,
														BNIL);
													BGl_verbosez00zztools_speekz00(BINT(1L),
														BgL_list1306z00_1761);
												}
												{	/* SawJvm/jld.scm 53 */
													obj_t BgL_list1307z00_1762;

													{	/* SawJvm/jld.scm 53 */
														obj_t BgL_arg1308z00_1763;

														{	/* SawJvm/jld.scm 53 */
															obj_t BgL_arg1310z00_1764;

															BgL_arg1310z00_1764 =
																MAKE_YOUNG_PAIR
																(BGl_string2164z00zzsaw_jvm_ldz00, BNIL);
															BgL_arg1308z00_1763 =
																MAKE_YOUNG_PAIR(BgL_jarnamez00_1756,
																BgL_arg1310z00_1764);
														}
														BgL_list1307z00_1762 =
															MAKE_YOUNG_PAIR(BGl_string2165z00zzsaw_jvm_ldz00,
															BgL_arg1308z00_1763);
													}
													BGl_verbosez00zztools_speekz00(BINT(2L),
														BgL_list1307z00_1762);
												}
												{	/* SawJvm/jld.scm 54 */
													obj_t BgL_list1311z00_1765;

													BgL_list1311z00_1765 =
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
													BGl_verbosez00zztools_speekz00(BINT(1L),
														BgL_list1311z00_1765);
												}
												BGl_generatezd2jvmzd2manifestz00zzsaw_jvm_ldz00
													(BgL_manifestz00_1758,
													BGl_findzd2jvmzd2mainz00zzsaw_jvm_ldz00
													(BgL_ozd2fileszd2_1759), BgL_jarnamez00_1756,
													BgL_za7ipsza7_1757);
												BGl_jvmzd2jarzd2zzsaw_jvm_ldz00(BgL_jarnamez00_1756,
													BgL_manifestz00_1758, BgL_allzd2objectszd2_1760);
								}}}}
							else
								{	/* SawJvm/jld.scm 47 */
									BFALSE;
								}
							{	/* SawJvm/jld.scm 61 */
								obj_t BgL_arg1325z00_1785;

								BgL_arg1325z00_1785 =
									BGl_findzd2jvmzd2mainclassz00zzsaw_jvm_ldz00
									(BgL_linkzd2mainzd2modulez00_3);
								return
									BBOOL(BGl_generatezd2jvmzd2scriptz00zzsaw_jvm_ldz00
									(BgL_targetz00_1755, BgL_arg1325z00_1785, BgL_jarnamez00_1756,
										BgL_za7ipsza7_1757));
							}
						}
					}
				}
			}
		}

	}



/* <@anonymous:1327>~0 */
	obj_t BGl_zc3z04anonymousza31327ze3ze70z60zzsaw_jvm_ldz00(obj_t
		BgL_l1255z00_1788)
	{
		{	/* SawJvm/jld.scm 45 */
			if (NULLP(BgL_l1255z00_1788))
				{	/* SawJvm/jld.scm 45 */
					return BNIL;
				}
			else
				{	/* SawJvm/jld.scm 45 */
					obj_t BgL_arg1329z00_1791;
					obj_t BgL_arg1331z00_1792;

					{	/* SawJvm/jld.scm 45 */
						obj_t BgL_arg1332z00_1793;

						BgL_arg1332z00_1793 = CAR(((obj_t) BgL_l1255z00_1788));
						BgL_arg1329z00_1791 =
							BGl_libraryzd2ze3za7ipsz96zzsaw_jvm_ldz00(BgL_arg1332z00_1793);
					}
					{	/* SawJvm/jld.scm 45 */
						obj_t BgL_arg1333z00_1794;

						BgL_arg1333z00_1794 = CDR(((obj_t) BgL_l1255z00_1788));
						BgL_arg1331z00_1792 =
							BGl_zc3z04anonymousza31327ze3ze70z60zzsaw_jvm_ldz00
							(BgL_arg1333z00_1794);
					}
					return bgl_append2(BgL_arg1329z00_1791, BgL_arg1331z00_1792);
				}
		}

	}



/* &jvm-ld */
	obj_t BGl_z62jvmzd2ldzb0zzsaw_jvm_ldz00(obj_t BgL_envz00_3091,
		obj_t BgL_linkzd2mainzd2modulez00_3092)
	{
		{	/* SawJvm/jld.scm 39 */
			return BGl_jvmzd2ldzd2zzsaw_jvm_ldz00(BgL_linkzd2mainzd2modulez00_3092);
		}

	}



/* source->jvm-class */
	obj_t BGl_sourcezd2ze3jvmzd2classze3zzsaw_jvm_ldz00(obj_t BgL_sz00_4)
	{
		{	/* SawJvm/jld.scm 68 */
			{	/* SawJvm/jld.scm 69 */
				obj_t BgL_qz00_1798;

				BgL_qz00_1798 =
					BGl_sourcezd2ze3qualifiedzd2typeze3zzread_jvmz00(BgL_sz00_4);
				{	/* SawJvm/jld.scm 71 */
					obj_t BgL_arg1339z00_1799;

					if (STRINGP(BgL_qz00_1798))
						{	/* SawJvm/jld.scm 71 */
							BgL_arg1339z00_1799 =
								string_append(BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00
								(BgL_qz00_1798, (char) (((unsigned char) '.')),
									(char) (((unsigned char) '/'))),
								BGl_string2166z00zzsaw_jvm_ldz00);
						}
					else
						{	/* SawJvm/jld.scm 71 */
							BgL_arg1339z00_1799 =
								string_append(BGl_prefixz00zz__osz00(BgL_sz00_4),
								BGl_string2166z00zzsaw_jvm_ldz00);
						}
					return
						BGl_jvmzd2classzd2withzd2directoryzd2zzread_jvmz00
						(BgL_arg1339z00_1799);
				}
			}
		}

	}



/* find-jvm-mainclass */
	obj_t BGl_findzd2jvmzd2mainclassz00zzsaw_jvm_ldz00(obj_t
		BgL_linkzd2mainzd2modulez00_5)
	{
		{	/* SawJvm/jld.scm 78 */
			if (STRINGP(BGl_za2jvmzd2mainclassza2zd2zzengine_paramz00))
				{	/* SawJvm/jld.scm 80 */
					return BGl_za2jvmzd2mainclassza2zd2zzengine_paramz00;
				}
			else
				{	/* SawJvm/jld.scm 80 */
					if (CBOOL(BGl_za2mainza2z00zzmodule_modulez00))
						{	/* SawJvm/jld.scm 82 */
							return
								BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
								(BGl_za2moduleza2z00zzmodule_modulez00);
						}
					else
						{	/* SawJvm/jld.scm 82 */
							if (SYMBOLP(BgL_linkzd2mainzd2modulez00_5))
								{	/* SawJvm/jld.scm 84 */
									return
										BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
										(BgL_linkzd2mainzd2modulez00_5);
								}
							else
								{	/* SawJvm/jld.scm 84 */
									return
										BGl_errorz00zz__errorz00(BGl_string2167z00zzsaw_jvm_ldz00,
										BGl_string2168z00zzsaw_jvm_ldz00,
										BGl_string2169z00zzsaw_jvm_ldz00);
								}
						}
				}
		}

	}



/* library->zips */
	obj_t BGl_libraryzd2ze3za7ipsz96zzsaw_jvm_ldz00(obj_t BgL_libz00_6)
	{
		{	/* SawJvm/jld.scm 94 */
			{	/* SawJvm/jld.scm 95 */
				obj_t BgL_dirz00_1805;

				{	/* SawJvm/jld.scm 95 */
					bool_t BgL_test2332z00_3392;

					{	/* SawJvm/jld.scm 95 */
						obj_t BgL_string1z00_2630;

						BgL_string1z00_2630 = BGl_za2jvmzd2shellza2zd2zzengine_paramz00;
						{	/* SawJvm/jld.scm 95 */
							long BgL_l1z00_2632;

							BgL_l1z00_2632 = STRING_LENGTH(BgL_string1z00_2630);
							if ((BgL_l1z00_2632 == 5L))
								{	/* SawJvm/jld.scm 95 */
									int BgL_arg1282z00_2635;

									{	/* SawJvm/jld.scm 95 */
										char *BgL_auxz00_3398;
										char *BgL_tmpz00_3396;

										BgL_auxz00_3398 =
											BSTRING_TO_STRING(BGl_string2170z00zzsaw_jvm_ldz00);
										BgL_tmpz00_3396 = BSTRING_TO_STRING(BgL_string1z00_2630);
										BgL_arg1282z00_2635 =
											memcmp(BgL_tmpz00_3396, BgL_auxz00_3398, BgL_l1z00_2632);
									}
									BgL_test2332z00_3392 = ((long) (BgL_arg1282z00_2635) == 0L);
								}
							else
								{	/* SawJvm/jld.scm 95 */
									BgL_test2332z00_3392 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2332z00_3392)
						{	/* SawJvm/jld.scm 96 */
							obj_t BgL_arg1485z00_1839;

							BgL_arg1485z00_1839 =
								BGl_jvmzd2bigloozd2classpathz00zzsaw_jvm_ldz00();
							BgL_dirz00_1805 =
								MAKE_YOUNG_PAIR(BgL_arg1485z00_1839,
								BGl_za2libzd2dirza2zd2zzengine_paramz00);
						}
					else
						{	/* SawJvm/jld.scm 95 */
							BgL_dirz00_1805 = BGl_za2libzd2dirza2zd2zzengine_paramz00;
						}
				}
				{	/* SawJvm/jld.scm 95 */
					obj_t BgL_bez00_1806;

					{	/* SawJvm/jld.scm 98 */
						obj_t BgL_arg1472z00_1837;

						BgL_arg1472z00_1837 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_bez00_1806 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1472z00_1837)))->BgL_srfi0z00);
					}
					{	/* SawJvm/jld.scm 98 */
						obj_t BgL_nz00_1807;

						{	/* SawJvm/jld.scm 100 */
							obj_t BgL_arg1453z00_1835;

							{	/* SawJvm/jld.scm 100 */
								bool_t BgL_test2334z00_3408;

								if (CBOOL(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
									{	/* SawJvm/jld.scm 100 */
										if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
											{	/* SawJvm/jld.scm 100 */
												BgL_test2334z00_3408 = ((bool_t) 0);
											}
										else
											{	/* SawJvm/jld.scm 100 */
												BgL_test2334z00_3408 = ((bool_t) 1);
											}
									}
								else
									{	/* SawJvm/jld.scm 100 */
										BgL_test2334z00_3408 = ((bool_t) 0);
									}
								if (BgL_test2334z00_3408)
									{	/* SawJvm/jld.scm 100 */
										BgL_arg1453z00_1835 = BGl_string2171z00zzsaw_jvm_ldz00;
									}
								else
									{	/* SawJvm/jld.scm 100 */
										BgL_arg1453z00_1835 = BGl_string2172z00zzsaw_jvm_ldz00;
									}
							}
							BgL_nz00_1807 =
								BGl_libraryzd2filezd2namez00zz__libraryz00(BgL_libz00_6,
								BgL_arg1453z00_1835, BgL_bez00_1806);
						}
						{	/* SawJvm/jld.scm 99 */
							obj_t BgL_fz00_1808;

							BgL_fz00_1808 =
								BGl_findzd2filezf2pathz20zz__osz00
								(BGl_makezd2sharedzd2libzd2namezd2zz__osz00(BgL_nz00_1807,
									BgL_bez00_1806), BgL_dirz00_1805);
							{	/* SawJvm/jld.scm 103 */
								obj_t BgL_nez00_1809;

								{	/* SawJvm/jld.scm 106 */
									obj_t BgL_arg1434z00_1831;

									{	/* SawJvm/jld.scm 106 */
										obj_t BgL_arg1437z00_1832;

										{	/* SawJvm/jld.scm 106 */
											bool_t BgL_test2337z00_3416;

											if (CBOOL(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
												{	/* SawJvm/jld.scm 106 */
													if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
														{	/* SawJvm/jld.scm 106 */
															BgL_test2337z00_3416 = ((bool_t) 0);
														}
													else
														{	/* SawJvm/jld.scm 106 */
															BgL_test2337z00_3416 = ((bool_t) 1);
														}
												}
											else
												{	/* SawJvm/jld.scm 106 */
													BgL_test2337z00_3416 = ((bool_t) 0);
												}
											if (BgL_test2337z00_3416)
												{	/* SawJvm/jld.scm 106 */
													BgL_arg1437z00_1832 =
														BGl_string2173z00zzsaw_jvm_ldz00;
												}
											else
												{	/* SawJvm/jld.scm 106 */
													BgL_arg1437z00_1832 =
														BGl_string2174z00zzsaw_jvm_ldz00;
												}
										}
										BgL_arg1434z00_1831 =
											BGl_libraryzd2filezd2namez00zz__libraryz00(BgL_libz00_6,
											BgL_arg1437z00_1832, BgL_bez00_1806);
									}
									BgL_nez00_1809 =
										BGl_makezd2sharedzd2libzd2namezd2zz__osz00
										(BgL_arg1434z00_1831, BgL_bez00_1806);
								}
								{	/* SawJvm/jld.scm 104 */
									obj_t BgL_fez00_1810;

									BgL_fez00_1810 =
										BGl_findzd2filezf2pathz20zz__osz00(BgL_nez00_1809,
										BgL_dirz00_1805);
									{	/* SawJvm/jld.scm 108 */

										{	/* SawJvm/jld.scm 110 */
											bool_t BgL_test2340z00_3424;

											if (STRINGP(BgL_fz00_1808))
												{	/* SawJvm/jld.scm 110 */
													BgL_test2340z00_3424 = STRINGP(BgL_fez00_1810);
												}
											else
												{	/* SawJvm/jld.scm 110 */
													BgL_test2340z00_3424 = ((bool_t) 0);
												}
											if (BgL_test2340z00_3424)
												{	/* SawJvm/jld.scm 111 */
													obj_t BgL_list1349z00_1813;

													{	/* SawJvm/jld.scm 111 */
														obj_t BgL_arg1351z00_1814;

														BgL_arg1351z00_1814 =
															MAKE_YOUNG_PAIR(BgL_fez00_1810, BNIL);
														BgL_list1349z00_1813 =
															MAKE_YOUNG_PAIR(BgL_fz00_1808,
															BgL_arg1351z00_1814);
													}
													return BgL_list1349z00_1813;
												}
											else
												{	/* SawJvm/jld.scm 110 */
													if (STRINGP(BgL_fz00_1808))
														{	/* SawJvm/jld.scm 112 */
															{	/* SawJvm/jld.scm 115 */
																obj_t BgL_arg1361z00_1816;

																{	/* SawJvm/jld.scm 115 */
																	obj_t BgL_arg1370z00_1820;

																	{	/* SawJvm/jld.scm 115 */
																		bool_t BgL_test2343z00_3432;

																		if (CBOOL
																			(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
																			{	/* SawJvm/jld.scm 115 */
																				if (CBOOL
																					(BGl_za2purifyza2z00zzengine_paramz00))
																					{	/* SawJvm/jld.scm 115 */
																						BgL_test2343z00_3432 = ((bool_t) 0);
																					}
																				else
																					{	/* SawJvm/jld.scm 115 */
																						BgL_test2343z00_3432 = ((bool_t) 1);
																					}
																			}
																		else
																			{	/* SawJvm/jld.scm 115 */
																				BgL_test2343z00_3432 = ((bool_t) 0);
																			}
																		if (BgL_test2343z00_3432)
																			{	/* SawJvm/jld.scm 115 */
																				BgL_arg1370z00_1820 =
																					BGl_string2173z00zzsaw_jvm_ldz00;
																			}
																		else
																			{	/* SawJvm/jld.scm 115 */
																				BgL_arg1370z00_1820 =
																					BGl_string2174z00zzsaw_jvm_ldz00;
																			}
																	}
																	{	/* SawJvm/jld.scm 114 */
																		obj_t BgL_list1371z00_1821;

																		{	/* SawJvm/jld.scm 114 */
																			obj_t BgL_arg1375z00_1822;

																			BgL_arg1375z00_1822 =
																				MAKE_YOUNG_PAIR(BgL_nez00_1809, BNIL);
																			BgL_list1371z00_1821 =
																				MAKE_YOUNG_PAIR(BgL_arg1370z00_1820,
																				BgL_arg1375z00_1822);
																		}
																		BgL_arg1361z00_1816 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string2175z00zzsaw_jvm_ldz00,
																			BgL_list1371z00_1821);
																	}
																}
																{	/* SawJvm/jld.scm 113 */
																	obj_t BgL_list1362z00_1817;

																	{	/* SawJvm/jld.scm 113 */
																		obj_t BgL_arg1364z00_1818;

																		{	/* SawJvm/jld.scm 113 */
																			obj_t BgL_arg1367z00_1819;

																			BgL_arg1367z00_1819 =
																				MAKE_YOUNG_PAIR(BgL_libz00_6, BNIL);
																			BgL_arg1364z00_1818 =
																				MAKE_YOUNG_PAIR(BgL_arg1361z00_1816,
																				BgL_arg1367z00_1819);
																		}
																		BgL_list1362z00_1817 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2176z00zzsaw_jvm_ldz00,
																			BgL_arg1364z00_1818);
																	}
																	BGl_warningz00zz__errorz00
																		(BgL_list1362z00_1817);
																}
															}
															{	/* SawJvm/jld.scm 118 */
																obj_t BgL_list1383z00_1824;

																BgL_list1383z00_1824 =
																	MAKE_YOUNG_PAIR(BgL_fz00_1808, BNIL);
																return BgL_list1383z00_1824;
															}
														}
													else
														{	/* SawJvm/jld.scm 112 */
															{	/* SawJvm/jld.scm 121 */
																obj_t BgL_arg1408z00_1825;

																{	/* SawJvm/jld.scm 121 */
																	obj_t BgL_list1422z00_1829;

																	BgL_list1422z00_1829 =
																		MAKE_YOUNG_PAIR(BgL_nz00_1807, BNIL);
																	BgL_arg1408z00_1825 =
																		BGl_formatz00zz__r4_output_6_10_3z00
																		(BGl_string2177z00zzsaw_jvm_ldz00,
																		BgL_list1422z00_1829);
																}
																{	/* SawJvm/jld.scm 120 */
																	obj_t BgL_list1409z00_1826;

																	{	/* SawJvm/jld.scm 120 */
																		obj_t BgL_arg1410z00_1827;

																		{	/* SawJvm/jld.scm 120 */
																			obj_t BgL_arg1421z00_1828;

																			BgL_arg1421z00_1828 =
																				MAKE_YOUNG_PAIR(BgL_libz00_6, BNIL);
																			BgL_arg1410z00_1827 =
																				MAKE_YOUNG_PAIR(BgL_arg1408z00_1825,
																				BgL_arg1421z00_1828);
																		}
																		BgL_list1409z00_1826 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2176z00zzsaw_jvm_ldz00,
																			BgL_arg1410z00_1827);
																	}
																	BGl_warningz00zz__errorz00
																		(BgL_list1409z00_1826);
																}
															}
															return BNIL;
														}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* find-jvm-main */
	obj_t BGl_findzd2jvmzd2mainz00zzsaw_jvm_ldz00(obj_t BgL_ozd2fileszd2_7)
	{
		{	/* SawJvm/jld.scm 128 */
			{	/* SawJvm/jld.scm 129 */
				bool_t BgL_test2346z00_3451;

				{	/* SawJvm/jld.scm 129 */
					obj_t BgL_objz00_2644;

					BgL_objz00_2644 = BGl_za2mainza2z00zzmodule_modulez00;
					{	/* SawJvm/jld.scm 129 */
						obj_t BgL_classz00_2645;

						BgL_classz00_2645 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_objz00_2644))
							{	/* SawJvm/jld.scm 129 */
								BgL_objectz00_bglt BgL_arg1807z00_2647;

								BgL_arg1807z00_2647 = (BgL_objectz00_bglt) (BgL_objz00_2644);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawJvm/jld.scm 129 */
										long BgL_idxz00_2653;

										BgL_idxz00_2653 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2647);
										BgL_test2346z00_3451 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2653 + 2L)) == BgL_classz00_2645);
									}
								else
									{	/* SawJvm/jld.scm 129 */
										bool_t BgL_res2159z00_2678;

										{	/* SawJvm/jld.scm 129 */
											obj_t BgL_oclassz00_2661;

											{	/* SawJvm/jld.scm 129 */
												obj_t BgL_arg1815z00_2669;
												long BgL_arg1816z00_2670;

												BgL_arg1815z00_2669 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawJvm/jld.scm 129 */
													long BgL_arg1817z00_2671;

													BgL_arg1817z00_2671 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2647);
													BgL_arg1816z00_2670 =
														(BgL_arg1817z00_2671 - OBJECT_TYPE);
												}
												BgL_oclassz00_2661 =
													VECTOR_REF(BgL_arg1815z00_2669, BgL_arg1816z00_2670);
											}
											{	/* SawJvm/jld.scm 129 */
												bool_t BgL__ortest_1115z00_2662;

												BgL__ortest_1115z00_2662 =
													(BgL_classz00_2645 == BgL_oclassz00_2661);
												if (BgL__ortest_1115z00_2662)
													{	/* SawJvm/jld.scm 129 */
														BgL_res2159z00_2678 = BgL__ortest_1115z00_2662;
													}
												else
													{	/* SawJvm/jld.scm 129 */
														long BgL_odepthz00_2663;

														{	/* SawJvm/jld.scm 129 */
															obj_t BgL_arg1804z00_2664;

															BgL_arg1804z00_2664 = (BgL_oclassz00_2661);
															BgL_odepthz00_2663 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2664);
														}
														if ((2L < BgL_odepthz00_2663))
															{	/* SawJvm/jld.scm 129 */
																obj_t BgL_arg1802z00_2666;

																{	/* SawJvm/jld.scm 129 */
																	obj_t BgL_arg1803z00_2667;

																	BgL_arg1803z00_2667 = (BgL_oclassz00_2661);
																	BgL_arg1802z00_2666 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2667,
																		2L);
																}
																BgL_res2159z00_2678 =
																	(BgL_arg1802z00_2666 == BgL_classz00_2645);
															}
														else
															{	/* SawJvm/jld.scm 129 */
																BgL_res2159z00_2678 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2346z00_3451 = BgL_res2159z00_2678;
									}
							}
						else
							{	/* SawJvm/jld.scm 129 */
								BgL_test2346z00_3451 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2346z00_3451)
					{	/* SawJvm/jld.scm 129 */
						return
							BGl_prefixz00zz__osz00(CAR
							(BGl_za2srczd2filesza2zd2zzengine_paramz00));
					}
				else
					{
						obj_t BgL_ozd2fileszd2_1843;

						BgL_ozd2fileszd2_1843 = BgL_ozd2fileszd2_7;
					BgL_zc3z04anonymousza31490ze3z87_1844:
						if (NULLP(BgL_ozd2fileszd2_1843))
							{	/* SawJvm/jld.scm 132 */
								return
									BGl_errorz00zz__errorz00(BGl_string2178z00zzsaw_jvm_ldz00,
									BGl_string2179z00zzsaw_jvm_ldz00, BgL_ozd2fileszd2_1843);
							}
						else
							{	/* SawJvm/jld.scm 134 */
								obj_t BgL_prefz00_1846;

								{	/* SawJvm/jld.scm 134 */
									obj_t BgL_arg1575z00_1892;

									{	/* SawJvm/jld.scm 134 */
										obj_t BgL_arg1576z00_1893;

										BgL_arg1576z00_1893 = CAR(((obj_t) BgL_ozd2fileszd2_1843));
										BgL_arg1575z00_1892 =
											BGl_prefixz00zz__osz00(BgL_arg1576z00_1893);
									}
									BgL_prefz00_1846 =
										BGl_unprofzd2srczd2namez00zzengine_linkz00
										(BgL_arg1575z00_1892);
								}
								{	/* SawJvm/jld.scm 134 */
									obj_t BgL_bprefz00_1847;

									BgL_bprefz00_1847 =
										BGl_basenamez00zz__osz00(BgL_prefz00_1846);
									{	/* SawJvm/jld.scm 135 */
										obj_t BgL_scmzd2filezd2_1848;

										BgL_scmzd2filezd2_1848 =
											BGl_findzd2srczd2filez00zzengine_linkz00(BgL_prefz00_1846,
											BgL_bprefz00_1847);
										{	/* SawJvm/jld.scm 136 */

											{	/* SawJvm/jld.scm 137 */
												bool_t BgL_test2352z00_3485;

												if (STRINGP(BgL_scmzd2filezd2_1848))
													{	/* SawJvm/jld.scm 137 */
														if (fexists(BSTRING_TO_STRING
																(BgL_scmzd2filezd2_1848)))
															{	/* SawJvm/jld.scm 138 */
																BgL_test2352z00_3485 = ((bool_t) 0);
															}
														else
															{	/* SawJvm/jld.scm 138 */
																BgL_test2352z00_3485 = ((bool_t) 1);
															}
													}
												else
													{	/* SawJvm/jld.scm 137 */
														BgL_test2352z00_3485 = ((bool_t) 1);
													}
												if (BgL_test2352z00_3485)
													{	/* SawJvm/jld.scm 139 */
														obj_t BgL_arg1502z00_1852;

														BgL_arg1502z00_1852 =
															CDR(((obj_t) BgL_ozd2fileszd2_1843));
														{
															obj_t BgL_ozd2fileszd2_3493;

															BgL_ozd2fileszd2_3493 = BgL_arg1502z00_1852;
															BgL_ozd2fileszd2_1843 = BgL_ozd2fileszd2_3493;
															goto BgL_zc3z04anonymousza31490ze3z87_1844;
														}
													}
												else
													{	/* SawJvm/jld.scm 140 */
														obj_t BgL__ortest_1108z00_1853;

														BgL__ortest_1108z00_1853 =
															BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
															(BgL_scmzd2filezd2_1848,
															BGl_proc2180z00zzsaw_jvm_ldz00);
														if (CBOOL(BgL__ortest_1108z00_1853))
															{	/* SawJvm/jld.scm 140 */
																return BgL__ortest_1108z00_1853;
															}
														else
															{	/* SawJvm/jld.scm 146 */
																obj_t BgL_arg1509z00_1854;

																BgL_arg1509z00_1854 =
																	CDR(((obj_t) BgL_ozd2fileszd2_1843));
																{
																	obj_t BgL_ozd2fileszd2_3499;

																	BgL_ozd2fileszd2_3499 = BgL_arg1509z00_1854;
																	BgL_ozd2fileszd2_1843 = BgL_ozd2fileszd2_3499;
																	goto BgL_zc3z04anonymousza31490ze3z87_1844;
																}
															}
													}
											}
										}
									}
								}
							}
					}
			}
		}

	}



/* &<@anonymous:1514> */
	obj_t BGl_z62zc3z04anonymousza31514ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3094)
	{
		{	/* SawJvm/jld.scm 141 */
			{	/* SawJvm/jld.scm 142 */
				obj_t BgL_ezd2105zd2_3143;

				BgL_ezd2105zd2_3143 = BGl_compilerzd2readzd2zzread_readerz00(BNIL);
				if (PAIRP(BgL_ezd2105zd2_3143))
					{	/* SawJvm/jld.scm 141 */
						obj_t BgL_cdrzd2109zd2_3144;

						BgL_cdrzd2109zd2_3144 = CDR(BgL_ezd2105zd2_3143);
						if ((CAR(BgL_ezd2105zd2_3143) == CNST_TABLE_REF(0)))
							{	/* SawJvm/jld.scm 141 */
								if (PAIRP(BgL_cdrzd2109zd2_3144))
									{
										obj_t BgL_gzd2114zd2_3146;

										BgL_gzd2114zd2_3146 = CDR(BgL_cdrzd2109zd2_3144);
									BgL_zc3z04anonymousza31536ze3z87_3145:
										if (PAIRP(BgL_gzd2114zd2_3146))
											{	/* SawJvm/jld.scm 141 */
												obj_t BgL_carzd2116zd2_3147;

												BgL_carzd2116zd2_3147 = CAR(BgL_gzd2114zd2_3146);
												if (PAIRP(BgL_carzd2116zd2_3147))
													{	/* SawJvm/jld.scm 141 */
														obj_t BgL_cdrzd2119zd2_3148;

														BgL_cdrzd2119zd2_3148 = CDR(BgL_carzd2116zd2_3147);
														if (
															(CAR(BgL_carzd2116zd2_3147) == CNST_TABLE_REF(1)))
															{	/* SawJvm/jld.scm 141 */
																if (PAIRP(BgL_cdrzd2119zd2_3148))
																	{	/* SawJvm/jld.scm 141 */
																		if (NULLP(CDR(BgL_cdrzd2119zd2_3148)))
																			{	/* SawJvm/jld.scm 141 */
																				obj_t BgL_arg1552z00_3149;

																				BgL_arg1552z00_3149 =
																					CAR(((obj_t) BgL_cdrzd2109zd2_3144));
																				return
																					BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
																					(BgL_arg1552z00_3149);
																			}
																		else
																			{
																				obj_t BgL_gzd2114zd2_3528;

																				BgL_gzd2114zd2_3528 =
																					CDR(BgL_gzd2114zd2_3146);
																				BgL_gzd2114zd2_3146 =
																					BgL_gzd2114zd2_3528;
																				goto
																					BgL_zc3z04anonymousza31536ze3z87_3145;
																			}
																	}
																else
																	{
																		obj_t BgL_gzd2114zd2_3530;

																		BgL_gzd2114zd2_3530 =
																			CDR(BgL_gzd2114zd2_3146);
																		BgL_gzd2114zd2_3146 = BgL_gzd2114zd2_3530;
																		goto BgL_zc3z04anonymousza31536ze3z87_3145;
																	}
															}
														else
															{
																obj_t BgL_gzd2114zd2_3532;

																BgL_gzd2114zd2_3532 = CDR(BgL_gzd2114zd2_3146);
																BgL_gzd2114zd2_3146 = BgL_gzd2114zd2_3532;
																goto BgL_zc3z04anonymousza31536ze3z87_3145;
															}
													}
												else
													{
														obj_t BgL_gzd2114zd2_3534;

														BgL_gzd2114zd2_3534 = CDR(BgL_gzd2114zd2_3146);
														BgL_gzd2114zd2_3146 = BgL_gzd2114zd2_3534;
														goto BgL_zc3z04anonymousza31536ze3z87_3145;
													}
											}
										else
											{	/* SawJvm/jld.scm 141 */
												return BFALSE;
											}
									}
								else
									{	/* SawJvm/jld.scm 141 */
										return BFALSE;
									}
							}
						else
							{	/* SawJvm/jld.scm 141 */
								return BFALSE;
							}
					}
				else
					{	/* SawJvm/jld.scm 141 */
						return BFALSE;
					}
			}
		}

	}



/* objects->classes */
	obj_t BGl_objectszd2ze3classesz31zzsaw_jvm_ldz00(obj_t BgL_objectsz00_8)
	{
		{	/* SawJvm/jld.scm 151 */
			{
				obj_t BgL_sourcez00_2001;

				{
					obj_t BgL_objectsz00_1901;
					obj_t BgL_classesz00_1902;

					BgL_objectsz00_1901 = BgL_objectsz00_8;
					BgL_classesz00_1902 = BNIL;
				BgL_zc3z04anonymousza31577ze3z87_1903:
					if (NULLP(BgL_objectsz00_1901))
						{	/* SawJvm/jld.scm 220 */
							return BgL_classesz00_1902;
						}
					else
						{	/* SawJvm/jld.scm 222 */
							obj_t BgL_objectz00_1905;

							BgL_objectz00_1905 = CAR(((obj_t) BgL_objectsz00_1901));
							{	/* SawJvm/jld.scm 222 */
								obj_t BgL_prefz00_1906;

								BgL_prefz00_1906 =
									BGl_unprofzd2srczd2namez00zzengine_linkz00
									(BGl_prefixz00zz__osz00(BgL_objectz00_1905));
								{	/* SawJvm/jld.scm 223 */
									obj_t BgL_bprefz00_1907;

									BgL_bprefz00_1907 =
										BGl_basenamez00zz__osz00(BgL_prefz00_1906);
									{	/* SawJvm/jld.scm 224 */
										obj_t BgL_scmzd2filezd2_1908;

										BgL_scmzd2filezd2_1908 =
											BGl_findzd2srczd2filez00zzengine_linkz00(BgL_prefz00_1906,
											BgL_bprefz00_1907);
										{	/* SawJvm/jld.scm 225 */

											{	/* SawJvm/jld.scm 226 */
												bool_t BgL_test2365z00_3545;

												if (STRINGP(BgL_scmzd2filezd2_1908))
													{	/* SawJvm/jld.scm 226 */
														BgL_test2365z00_3545 =
															fexists(BSTRING_TO_STRING
															(BgL_scmzd2filezd2_1908));
													}
												else
													{	/* SawJvm/jld.scm 226 */
														BgL_test2365z00_3545 = ((bool_t) 0);
													}
												if (BgL_test2365z00_3545)
													{	/* SawJvm/jld.scm 227 */
														obj_t BgL_arg1584z00_1911;
														obj_t BgL_arg1585z00_1912;

														BgL_arg1584z00_1911 =
															CDR(((obj_t) BgL_objectsz00_1901));
														{	/* SawJvm/jld.scm 228 */
															obj_t BgL_tmpz00_3552;

															{	/* SawJvm/jld.scm 229 */
																obj_t BgL_auxz00_3553;

																BgL_sourcez00_2001 = BgL_scmzd2filezd2_1908;
																{	/* SawJvm/jld.scm 201 */
																	bool_t BgL_test2367z00_3554;

																	{	/* SawJvm/jld.scm 201 */
																		bool_t BgL_test2368z00_3555;

																		{	/* SawJvm/jld.scm 201 */
																			obj_t BgL_arg1738z00_2044;

																			BgL_arg1738z00_2044 =
																				BGl_suffixz00zz__osz00
																				(BgL_sourcez00_2001);
																			{	/* SawJvm/jld.scm 201 */
																				long BgL_l1z00_2745;

																				BgL_l1z00_2745 =
																					STRING_LENGTH(BgL_arg1738z00_2044);
																				if ((BgL_l1z00_2745 == 3L))
																					{	/* SawJvm/jld.scm 201 */
																						int BgL_arg1282z00_2748;

																						{	/* SawJvm/jld.scm 201 */
																							char *BgL_auxz00_3562;
																							char *BgL_tmpz00_3560;

																							BgL_auxz00_3562 =
																								BSTRING_TO_STRING
																								(BGl_string2181z00zzsaw_jvm_ldz00);
																							BgL_tmpz00_3560 =
																								BSTRING_TO_STRING
																								(BgL_arg1738z00_2044);
																							BgL_arg1282z00_2748 =
																								memcmp(BgL_tmpz00_3560,
																								BgL_auxz00_3562,
																								BgL_l1z00_2745);
																						}
																						BgL_test2368z00_3555 =
																							(
																							(long) (BgL_arg1282z00_2748) ==
																							0L);
																					}
																				else
																					{	/* SawJvm/jld.scm 201 */
																						BgL_test2368z00_3555 = ((bool_t) 0);
																					}
																			}
																		}
																		if (BgL_test2368z00_3555)
																			{	/* SawJvm/jld.scm 201 */
																				BgL_test2367z00_3554 = ((bool_t) 0);
																			}
																		else
																			{	/* SawJvm/jld.scm 201 */
																				BgL_test2367z00_3554 =
																					fexists(BSTRING_TO_STRING
																					(BgL_sourcez00_2001));
																			}
																	}
																	if (BgL_test2367z00_3554)
																		{	/* SawJvm/jld.scm 201 */
																			BgL_auxz00_3553 =
																				BGl_withzd2inputzd2fromzd2filezd2zz__r4_ports_6_10_1z00
																				(BgL_sourcez00_2001,
																				BGl_proc2182z00zzsaw_jvm_ldz00);
																		}
																	else
																		{	/* SawJvm/jld.scm 201 */
																			BgL_auxz00_3553 = BNIL;
																		}
																}
																BgL_tmpz00_3552 =
																	BGl_appendzd221011zd2zzsaw_jvm_ldz00
																	(BgL_auxz00_3553, BgL_classesz00_1902);
															}
															BgL_arg1585z00_1912 =
																MAKE_YOUNG_PAIR(BgL_objectz00_1905,
																BgL_tmpz00_3552);
														}
														{
															obj_t BgL_classesz00_3573;
															obj_t BgL_objectsz00_3572;

															BgL_objectsz00_3572 = BgL_arg1584z00_1911;
															BgL_classesz00_3573 = BgL_arg1585z00_1912;
															BgL_classesz00_1902 = BgL_classesz00_3573;
															BgL_objectsz00_1901 = BgL_objectsz00_3572;
															goto BgL_zc3z04anonymousza31577ze3z87_1903;
														}
													}
												else
													{	/* SawJvm/jld.scm 230 */
														obj_t BgL_arg1593z00_1915;
														obj_t BgL_arg1594z00_1916;

														BgL_arg1593z00_1915 =
															CDR(((obj_t) BgL_objectsz00_1901));
														BgL_arg1594z00_1916 =
															MAKE_YOUNG_PAIR(BgL_objectz00_1905,
															BgL_classesz00_1902);
														{
															obj_t BgL_classesz00_3578;
															obj_t BgL_objectsz00_3577;

															BgL_objectsz00_3577 = BgL_arg1593z00_1915;
															BgL_classesz00_3578 = BgL_arg1594z00_1916;
															BgL_classesz00_1902 = BgL_classesz00_3578;
															BgL_objectsz00_1901 = BgL_objectsz00_3577;
															goto BgL_zc3z04anonymousza31577ze3z87_1903;
														}
													}
											}
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* &<@anonymous:1709> */
	obj_t BGl_z62zc3z04anonymousza31709ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3098)
	{
		{	/* SawJvm/jld.scm 204 */
			return BGl_zc3z04exitza31710ze3ze70z60zzsaw_jvm_ldz00();
		}

	}



/* <@exit:1710>~0 */
	obj_t BGl_zc3z04exitza31710ze3ze70z60zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 205 */
			{
				obj_t BgL_idz00_1920;
				obj_t BgL_modz00_1994;
				obj_t BgL_modz00_1946;
				obj_t BgL_basez00_1947;
				obj_t BgL_clausesz00_1948;
				jmp_buf_t jmpbuf;
				void *BgL_an_exit1110z00_2010;

				if (SET_EXIT(BgL_an_exit1110z00_2010))
					{
						return BGL_EXIT_VALUE();
					}
				else
					{
#if( SIGSETJMP_SAVESIGS == 0 )
					 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
					 // bgl_restore_signal_handlers();
#endif

						BgL_an_exit1110z00_2010 = (void *) jmpbuf;
						{	/* SawJvm/jld.scm 205 */
							obj_t BgL_env1114z00_2011;

							BgL_env1114z00_2011 = BGL_CURRENT_DYNAMIC_ENV();
							PUSH_ENV_EXIT(BgL_env1114z00_2011, BgL_an_exit1110z00_2010, 1L);
							{	/* SawJvm/jld.scm 205 */
								obj_t BgL_an_exitd1111z00_2012;

								BgL_an_exitd1111z00_2012 =
									BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1114z00_2011);
								{	/* SawJvm/jld.scm 206 */
									obj_t BgL_res1113z00_2015;

									{	/* SawJvm/jld.scm 206 */
										obj_t BgL_mz00_2016;

										{	/* SawJvm/jld.scm 207 */
											obj_t BgL_zc3z04anonymousza31735ze3z87_3095;

											BgL_zc3z04anonymousza31735ze3z87_3095 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31735ze3ze5zzsaw_jvm_ldz00,
												(int) (1L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31735ze3z87_3095,
												(int) (0L), BgL_an_exitd1111z00_2012);
											BgL_mz00_2016 =
												BGl_withzd2exceptionzd2handlerz00zz__errorz00
												(BgL_zc3z04anonymousza31735ze3z87_3095,
												BGl_proc2185z00zzsaw_jvm_ldz00);
										}
										if (PAIRP(BgL_mz00_2016))
											{	/* SawJvm/jld.scm 209 */
												obj_t BgL_cdrzd2161zd2_2023;

												BgL_cdrzd2161zd2_2023 = CDR(((obj_t) BgL_mz00_2016));
												if ((CAR(((obj_t) BgL_mz00_2016)) == CNST_TABLE_REF(0)))
													{	/* SawJvm/jld.scm 209 */
														if (PAIRP(BgL_cdrzd2161zd2_2023))
															{	/* SawJvm/jld.scm 209 */
																obj_t BgL_arg1717z00_2027;
																obj_t BgL_arg1718z00_2028;

																BgL_arg1717z00_2027 =
																	CAR(BgL_cdrzd2161zd2_2023);
																BgL_arg1718z00_2028 =
																	CDR(BgL_cdrzd2161zd2_2023);
																BgL_modz00_1946 = BgL_arg1717z00_2027;
																{	/* SawJvm/jld.scm 212 */
																	obj_t BgL_auxz00_3680;

																	BgL_modz00_1994 = BgL_arg1717z00_2027;
																	{	/* SawJvm/jld.scm 193 */
																		obj_t BgL_dz00_1996;

																		BgL_dz00_1996 =
																			BGl_dirnamez00zz__osz00
																			(BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00
																			(BGl_modulezd2ze3qualifiedzd2typeze3zzread_jvmz00
																				(BgL_modz00_1994),
																				(char) (((unsigned char) '.')),
																				FILE_SEPARATOR));
																		{	/* SawJvm/jld.scm 197 */
																			bool_t BgL_test2388z00_3685;

																			{	/* SawJvm/jld.scm 197 */
																				long BgL_l1z00_2734;

																				BgL_l1z00_2734 =
																					STRING_LENGTH(BgL_dz00_1996);
																				if ((BgL_l1z00_2734 == 1L))
																					{	/* SawJvm/jld.scm 197 */
																						int BgL_arg1282z00_2737;

																						{	/* SawJvm/jld.scm 197 */
																							char *BgL_auxz00_3691;
																							char *BgL_tmpz00_3689;

																							BgL_auxz00_3691 =
																								BSTRING_TO_STRING
																								(BGl_string2183z00zzsaw_jvm_ldz00);
																							BgL_tmpz00_3689 =
																								BSTRING_TO_STRING
																								(BgL_dz00_1996);
																							BgL_arg1282z00_2737 =
																								memcmp(BgL_tmpz00_3689,
																								BgL_auxz00_3691,
																								BgL_l1z00_2734);
																						}
																						BgL_test2388z00_3685 =
																							(
																							(long) (BgL_arg1282z00_2737) ==
																							0L);
																					}
																				else
																					{	/* SawJvm/jld.scm 197 */
																						BgL_test2388z00_3685 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2388z00_3685)
																				{	/* SawJvm/jld.scm 197 */
																					BgL_auxz00_3680 =
																						BGl_string2184z00zzsaw_jvm_ldz00;
																				}
																			else
																				{	/* SawJvm/jld.scm 197 */
																					BgL_auxz00_3680 = BgL_dz00_1996;
																				}
																		}
																	}
																	BgL_basez00_1947 =
																		BGl_jvmzd2classzd2withzd2directoryzd2zzread_jvmz00
																		(BgL_auxz00_3680);
																}
																BgL_clausesz00_1948 = BgL_arg1718z00_2028;
																{
																	obj_t BgL_clausesz00_1952;
																	obj_t BgL_classesz00_1953;

																	BgL_clausesz00_1952 = BgL_clausesz00_1948;
																	BgL_classesz00_1953 = BNIL;
																BgL_zc3z04anonymousza31644ze3z87_1954:
																	if (NULLP(BgL_clausesz00_1952))
																		{	/* SawJvm/jld.scm 168 */
																			BgL_res1113z00_2015 = BgL_classesz00_1953;
																		}
																	else
																		{
																			obj_t BgL_statexpz00_1956;

																			{	/* SawJvm/jld.scm 170 */
																				obj_t BgL_ezd2132zd2_1959;

																				BgL_ezd2132zd2_1959 =
																					CAR(((obj_t) BgL_clausesz00_1952));
																				if (PAIRP(BgL_ezd2132zd2_1959))
																					{	/* SawJvm/jld.scm 170 */
																						obj_t BgL_carzd2135zd2_1961;
																						obj_t BgL_cdrzd2136zd2_1962;

																						BgL_carzd2135zd2_1961 =
																							CAR(BgL_ezd2132zd2_1959);
																						BgL_cdrzd2136zd2_1962 =
																							CDR(BgL_ezd2132zd2_1959);
																						if (
																							(BgL_carzd2135zd2_1961 ==
																								CNST_TABLE_REF(6)))
																							{	/* SawJvm/jld.scm 170 */
																								BgL_statexpz00_1956 =
																									BgL_cdrzd2136zd2_1962;
																							BgL_tagzd2130zd2_1957:
																								{
																									obj_t BgL_statexpz00_1964;
																									obj_t BgL_classesz00_1965;

																									BgL_statexpz00_1964 =
																										BgL_statexpz00_1956;
																									BgL_classesz00_1965 =
																										BgL_classesz00_1953;
																								BgL_zc3z04anonymousza31647ze3z87_1966:
																									if (NULLP
																										(BgL_statexpz00_1964))
																										{	/* SawJvm/jld.scm 175 */
																											obj_t BgL_arg1650z00_1968;

																											BgL_arg1650z00_1968 =
																												CDR(
																												((obj_t)
																													BgL_clausesz00_1952));
																											{
																												obj_t
																													BgL_classesz00_3620;
																												obj_t
																													BgL_clausesz00_3619;
																												BgL_clausesz00_3619 =
																													BgL_arg1650z00_1968;
																												BgL_classesz00_3620 =
																													BgL_classesz00_1965;
																												BgL_classesz00_1953 =
																													BgL_classesz00_3620;
																												BgL_clausesz00_1952 =
																													BgL_clausesz00_3619;
																												goto
																													BgL_zc3z04anonymousza31644ze3z87_1954;
																											}
																										}
																									else
																										{
																											obj_t BgL_identz00_1969;

																											{	/* SawJvm/jld.scm 177 */
																												obj_t
																													BgL_ezd2141zd2_1972;
																												BgL_ezd2141zd2_1972 =
																													CAR(((obj_t)
																														BgL_statexpz00_1964));
																												if (PAIRP
																													(BgL_ezd2141zd2_1972))
																													{	/* SawJvm/jld.scm 177 */
																														obj_t
																															BgL_carzd2144zd2_1974;
																														obj_t
																															BgL_cdrzd2145zd2_1975;
																														BgL_carzd2144zd2_1974
																															=
																															CAR
																															(BgL_ezd2141zd2_1972);
																														BgL_cdrzd2145zd2_1975
																															=
																															CDR
																															(BgL_ezd2141zd2_1972);
																														{

																															if (
																																(BgL_carzd2144zd2_1974
																																	==
																																	CNST_TABLE_REF
																																	(2)))
																																{	/* SawJvm/jld.scm 177 */
																																BgL_kapzd2146zd2_1976:
																																	if (PAIRP
																																		(BgL_cdrzd2145zd2_1975))
																																		{	/* SawJvm/jld.scm 177 */
																																			BgL_identz00_1969
																																				=
																																				CAR
																																				(BgL_cdrzd2145zd2_1975);
																																			{	/* SawJvm/jld.scm 180 */
																																				obj_t
																																					BgL_idz00_1984;
																																				BgL_idz00_1920
																																					=
																																					BgL_identz00_1969;
																																				{	/* SawJvm/jld.scm 153 */
																																					obj_t
																																						BgL_stringz00_1922;
																																					{	/* SawJvm/jld.scm 153 */
																																						obj_t
																																							BgL_arg1455z00_2697;
																																						BgL_arg1455z00_2697
																																							=
																																							SYMBOL_TO_STRING
																																							(((obj_t) BgL_idz00_1920));
																																						BgL_stringz00_1922
																																							=
																																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																							(BgL_arg1455z00_2697);
																																					}
																																					{	/* SawJvm/jld.scm 153 */
																																						long
																																							BgL_lenz00_1923;
																																						BgL_lenz00_1923
																																							=
																																							STRING_LENGTH
																																							(BgL_stringz00_1922);
																																						{	/* SawJvm/jld.scm 154 */

																																							{
																																								long
																																									BgL_walkerz00_1925;
																																								BgL_walkerz00_1925
																																									=
																																									0L;
																																							BgL_zc3z04anonymousza31597ze3z87_1926:
																																								if ((BgL_walkerz00_1925 == BgL_lenz00_1923))
																																									{	/* SawJvm/jld.scm 157 */
																																										BgL_idz00_1984
																																											=
																																											BgL_idz00_1920;
																																									}
																																								else
																																									{	/* SawJvm/jld.scm 159 */
																																										bool_t
																																											BgL_test2381z00_3638;
																																										if ((STRING_REF(BgL_stringz00_1922, BgL_walkerz00_1925) == ((unsigned char) ':')))
																																											{	/* SawJvm/jld.scm 159 */
																																												if ((BgL_walkerz00_1925 < (BgL_lenz00_1923 - 1L)))
																																													{	/* SawJvm/jld.scm 160 */
																																														BgL_test2381z00_3638
																																															=
																																															(STRING_REF
																																															(BgL_stringz00_1922,
																																																(BgL_walkerz00_1925
																																																	+
																																																	1L))
																																															==
																																															((unsigned char) ':'));
																																													}
																																												else
																																													{	/* SawJvm/jld.scm 160 */
																																														BgL_test2381z00_3638
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																										else
																																											{	/* SawJvm/jld.scm 159 */
																																												BgL_test2381z00_3638
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																										if (BgL_test2381z00_3638)
																																											{	/* SawJvm/jld.scm 159 */
																																												BgL_idz00_1984
																																													=
																																													bstring_to_symbol
																																													(c_substring
																																													(BgL_stringz00_1922,
																																														0L,
																																														BgL_walkerz00_1925));
																																											}
																																										else
																																											{
																																												long
																																													BgL_walkerz00_3650;
																																												BgL_walkerz00_3650
																																													=
																																													(BgL_walkerz00_1925
																																													+
																																													1L);
																																												BgL_walkerz00_1925
																																													=
																																													BgL_walkerz00_3650;
																																												goto
																																													BgL_zc3z04anonymousza31597ze3z87_1926;
																																											}
																																									}
																																							}
																																						}
																																					}
																																				}
																																				{	/* SawJvm/jld.scm 180 */
																																					obj_t
																																						BgL_mglz00_1985;
																																					BgL_mglz00_1985
																																						=
																																						BGl_classzd2idzd2ze3typezd2namez31zzbackend_cplibz00
																																						(BgL_idz00_1984,
																																						BgL_modz00_1946);
																																					{	/* SawJvm/jld.scm 181 */

																																						{	/* SawJvm/jld.scm 182 */
																																							obj_t
																																								BgL_arg1663z00_1986;
																																							obj_t
																																								BgL_arg1675z00_1987;
																																							BgL_arg1663z00_1986
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_statexpz00_1964));
																																							BgL_arg1675z00_1987
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BGl_makezd2filezd2namez00zz__osz00
																																								(BgL_basez00_1947,
																																									string_append
																																									(BgL_mglz00_1985,
																																										BGl_string2166z00zzsaw_jvm_ldz00)),
																																								BgL_classesz00_1965);
																																							{
																																								obj_t
																																									BgL_classesz00_3659;
																																								obj_t
																																									BgL_statexpz00_3658;
																																								BgL_statexpz00_3658
																																									=
																																									BgL_arg1663z00_1986;
																																								BgL_classesz00_3659
																																									=
																																									BgL_arg1675z00_1987;
																																								BgL_classesz00_1965
																																									=
																																									BgL_classesz00_3659;
																																								BgL_statexpz00_1964
																																									=
																																									BgL_statexpz00_3658;
																																								goto
																																									BgL_zc3z04anonymousza31647ze3z87_1966;
																																							}
																																						}
																																					}
																																				}
																																			}
																																		}
																																	else
																																		{	/* SawJvm/jld.scm 177 */
																																		BgL_tagzd2140zd2_1971:
																																			{	/* SawJvm/jld.scm 188 */
																																				obj_t
																																					BgL_arg1688z00_1990;
																																				BgL_arg1688z00_1990
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_statexpz00_1964));
																																				{
																																					obj_t
																																						BgL_statexpz00_3663;
																																					BgL_statexpz00_3663
																																						=
																																						BgL_arg1688z00_1990;
																																					BgL_statexpz00_1964
																																						=
																																						BgL_statexpz00_3663;
																																					goto
																																						BgL_zc3z04anonymousza31647ze3z87_1966;
																																				}
																																			}
																																		}
																																}
																															else
																																{	/* SawJvm/jld.scm 177 */
																																	if (
																																		(BgL_carzd2144zd2_1974
																																			==
																																			CNST_TABLE_REF
																																			(3)))
																																		{	/* SawJvm/jld.scm 177 */
																																			goto
																																				BgL_kapzd2146zd2_1976;
																																		}
																																	else
																																		{	/* SawJvm/jld.scm 177 */
																																			if (
																																				(BgL_carzd2144zd2_1974
																																					==
																																					CNST_TABLE_REF
																																					(4)))
																																				{	/* SawJvm/jld.scm 177 */
																																					goto
																																						BgL_kapzd2146zd2_1976;
																																				}
																																			else
																																				{	/* SawJvm/jld.scm 177 */
																																					if (
																																						(BgL_carzd2144zd2_1974
																																							==
																																							CNST_TABLE_REF
																																							(5)))
																																						{	/* SawJvm/jld.scm 177 */
																																							goto
																																								BgL_kapzd2146zd2_1976;
																																						}
																																					else
																																						{	/* SawJvm/jld.scm 177 */
																																							goto
																																								BgL_tagzd2140zd2_1971;
																																						}
																																				}
																																		}
																																}
																														}
																													}
																												else
																													{	/* SawJvm/jld.scm 177 */
																														goto
																															BgL_tagzd2140zd2_1971;
																													}
																											}
																										}
																								}
																							}
																						else
																							{	/* SawJvm/jld.scm 170 */
																								if (
																									(BgL_carzd2135zd2_1961 ==
																										CNST_TABLE_REF(7)))
																									{
																										obj_t BgL_statexpz00_3676;

																										BgL_statexpz00_3676 =
																											BgL_cdrzd2136zd2_1962;
																										BgL_statexpz00_1956 =
																											BgL_statexpz00_3676;
																										goto BgL_tagzd2130zd2_1957;
																									}
																								else
																									{	/* SawJvm/jld.scm 170 */
																									BgL_tagzd2131zd2_1958:
																										{	/* SawJvm/jld.scm 191 */
																											obj_t BgL_arg1689z00_1992;

																											BgL_arg1689z00_1992 =
																												CDR(
																												((obj_t)
																													BgL_clausesz00_1952));
																											{
																												obj_t
																													BgL_clausesz00_3679;
																												BgL_clausesz00_3679 =
																													BgL_arg1689z00_1992;
																												BgL_clausesz00_1952 =
																													BgL_clausesz00_3679;
																												goto
																													BgL_zc3z04anonymousza31644ze3z87_1954;
																											}
																										}
																									}
																							}
																					}
																				else
																					{	/* SawJvm/jld.scm 170 */
																						goto BgL_tagzd2131zd2_1958;
																					}
																			}
																		}
																}
															}
														else
															{	/* SawJvm/jld.scm 209 */
																BgL_res1113z00_2015 = BNIL;
															}
													}
												else
													{	/* SawJvm/jld.scm 209 */
														BgL_res1113z00_2015 = BNIL;
													}
											}
										else
											{	/* SawJvm/jld.scm 209 */
												BgL_res1113z00_2015 = BNIL;
											}
									}
									POP_ENV_EXIT(BgL_env1114z00_2011);
									return BgL_res1113z00_2015;
								}
							}
						}
					}
			}
		}

	}



/* &<@anonymous:1736> */
	obj_t BGl_z62zc3z04anonymousza31736ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3099)
	{
		{	/* SawJvm/jld.scm 208 */
			{	/* SawJvm/jld.scm 208 */
				obj_t BgL_arg1737z00_3150;

				{	/* SawJvm/jld.scm 208 */
					obj_t BgL_tmpz00_3698;

					BgL_tmpz00_3698 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1737z00_3150 = BGL_ENV_CURRENT_INPUT_PORT(BgL_tmpz00_3698);
				}
				{	/* SawJvm/jld.scm 208 */

					{	/* SawJvm/jld.scm 208 */

						return BGl_readz00zz__readerz00(BgL_arg1737z00_3150, BFALSE);
					}
				}
			}
		}

	}



/* &<@anonymous:1735> */
	obj_t BGl_z62zc3z04anonymousza31735ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3100, obj_t BgL__z00_3102)
	{
		{	/* SawJvm/jld.scm 207 */
			return
				unwind_stack_until(PROCEDURE_REF(BgL_envz00_3100,
					(int) (0L)), BFALSE, BNIL, BFALSE, BFALSE);
		}

	}



/* unique */
	obj_t BGl_uniquez00zzsaw_jvm_ldz00(obj_t BgL_lstz00_9)
	{
		{	/* SawJvm/jld.scm 236 */
			{	/* SawJvm/jld.scm 237 */
				obj_t BgL_tz00_2049;

				BgL_tz00_2049 = BGl_makezd2hashtablezd2zz__hashz00(BNIL);
				{
					obj_t BgL_l1262z00_2051;

					BgL_l1262z00_2051 = BgL_lstz00_9;
				BgL_zc3z04anonymousza31739ze3z87_2052:
					if (PAIRP(BgL_l1262z00_2051))
						{	/* SawJvm/jld.scm 238 */
							{	/* SawJvm/jld.scm 238 */
								obj_t BgL_fz00_2054;

								BgL_fz00_2054 = CAR(BgL_l1262z00_2051);
								BGl_hashtablezd2putz12zc0zz__hashz00(BgL_tz00_2049,
									BgL_fz00_2054, BgL_fz00_2054);
							}
							{
								obj_t BgL_l1262z00_3710;

								BgL_l1262z00_3710 = CDR(BgL_l1262z00_2051);
								BgL_l1262z00_2051 = BgL_l1262z00_3710;
								goto BgL_zc3z04anonymousza31739ze3z87_2052;
							}
						}
					else
						{	/* SawJvm/jld.scm 238 */
							((bool_t) 1);
						}
				}
				return BGl_hashtablezd2ze3listz31zz__hashz00(BgL_tz00_2049);
			}
		}

	}



/* jvm-jar */
	obj_t BGl_jvmzd2jarzd2zzsaw_jvm_ldz00(obj_t BgL_targetz00_10,
		obj_t BgL_manifestz00_11, obj_t BgL_objectsz00_12)
	{
		{	/* SawJvm/jld.scm 244 */
			{	/* SawJvm/jld.scm 245 */
				obj_t BgL_cmdz00_2058;

				if (STRINGP(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00))
					{
						obj_t BgL_objectsz00_2075;
						obj_t BgL_cmdz00_2076;

						BgL_objectsz00_2075 = BgL_objectsz00_12;
						BgL_cmdz00_2076 = BGl_string2184z00zzsaw_jvm_ldz00;
					BgL_zc3z04anonymousza31759ze3z87_2077:
						if (NULLP(BgL_objectsz00_2075))
							{	/* SawJvm/jld.scm 249 */
								obj_t BgL_arg1761z00_2079;

								BgL_arg1761z00_2079 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(8));
								{	/* SawJvm/jld.scm 249 */
									obj_t BgL_list1762z00_2080;

									{	/* SawJvm/jld.scm 249 */
										obj_t BgL_arg1765z00_2081;

										{	/* SawJvm/jld.scm 249 */
											obj_t BgL_arg1767z00_2082;

											{	/* SawJvm/jld.scm 249 */
												obj_t BgL_arg1770z00_2083;

												{	/* SawJvm/jld.scm 249 */
													obj_t BgL_arg1771z00_2084;

													{	/* SawJvm/jld.scm 249 */
														obj_t BgL_arg1773z00_2085;

														{	/* SawJvm/jld.scm 249 */
															obj_t BgL_arg1775z00_2086;

															BgL_arg1775z00_2086 =
																MAKE_YOUNG_PAIR(BgL_cmdz00_2076, BNIL);
															BgL_arg1773z00_2085 =
																MAKE_YOUNG_PAIR
																(BGl_string2186z00zzsaw_jvm_ldz00,
																BgL_arg1775z00_2086);
														}
														BgL_arg1771z00_2084 =
															MAKE_YOUNG_PAIR(BgL_targetz00_10,
															BgL_arg1773z00_2085);
													}
													BgL_arg1770z00_2083 =
														MAKE_YOUNG_PAIR(BGl_string2186z00zzsaw_jvm_ldz00,
														BgL_arg1771z00_2084);
												}
												BgL_arg1767z00_2082 =
													MAKE_YOUNG_PAIR(BgL_manifestz00_11,
													BgL_arg1770z00_2083);
											}
											BgL_arg1765z00_2081 =
												MAKE_YOUNG_PAIR(BGl_string2186z00zzsaw_jvm_ldz00,
												BgL_arg1767z00_2082);
										}
										BgL_list1762z00_2080 =
											MAKE_YOUNG_PAIR(BgL_arg1761z00_2079, BgL_arg1765z00_2081);
									}
									BgL_cmdz00_2058 =
										BGl_stringzd2appendzd2zz__r4_strings_6_7z00
										(BgL_list1762z00_2080);
								}
							}
						else
							{	/* SawJvm/jld.scm 253 */
								obj_t BgL_arg1798z00_2087;
								obj_t BgL_arg1799z00_2088;

								BgL_arg1798z00_2087 = CDR(((obj_t) BgL_objectsz00_2075));
								{	/* SawJvm/jld.scm 256 */
									obj_t BgL_arg1805z00_2089;

									{	/* SawJvm/jld.scm 256 */
										obj_t BgL_arg1831z00_2096;

										BgL_arg1831z00_2096 = CAR(((obj_t) BgL_objectsz00_2075));
										BgL_arg1805z00_2089 =
											BGl_jvmzd2classzd2sanszd2directoryzd2zzread_jvmz00
											(BgL_arg1831z00_2096);
									}
									{	/* SawJvm/jld.scm 254 */
										obj_t BgL_list1806z00_2090;

										{	/* SawJvm/jld.scm 254 */
											obj_t BgL_arg1808z00_2091;

											{	/* SawJvm/jld.scm 254 */
												obj_t BgL_arg1812z00_2092;

												{	/* SawJvm/jld.scm 254 */
													obj_t BgL_arg1820z00_2093;

													{	/* SawJvm/jld.scm 254 */
														obj_t BgL_arg1822z00_2094;

														{	/* SawJvm/jld.scm 254 */
															obj_t BgL_arg1823z00_2095;

															BgL_arg1823z00_2095 =
																MAKE_YOUNG_PAIR(BgL_cmdz00_2076, BNIL);
															BgL_arg1822z00_2094 =
																MAKE_YOUNG_PAIR
																(BGl_string2186z00zzsaw_jvm_ldz00,
																BgL_arg1823z00_2095);
														}
														BgL_arg1820z00_2093 =
															MAKE_YOUNG_PAIR(BgL_arg1805z00_2089,
															BgL_arg1822z00_2094);
													}
													BgL_arg1812z00_2092 =
														MAKE_YOUNG_PAIR(BGl_string2186z00zzsaw_jvm_ldz00,
														BgL_arg1820z00_2093);
												}
												BgL_arg1808z00_2091 =
													MAKE_YOUNG_PAIR
													(BGl_za2jvmzd2directoryza2zd2zzengine_paramz00,
													BgL_arg1812z00_2092);
											}
											BgL_list1806z00_2090 =
												MAKE_YOUNG_PAIR(BGl_string2187z00zzsaw_jvm_ldz00,
												BgL_arg1808z00_2091);
										}
										BgL_arg1799z00_2088 =
											BGl_stringzd2appendzd2zz__r4_strings_6_7z00
											(BgL_list1806z00_2090);
									}
								}
								{
									obj_t BgL_cmdz00_3740;
									obj_t BgL_objectsz00_3739;

									BgL_objectsz00_3739 = BgL_arg1798z00_2087;
									BgL_cmdz00_3740 = BgL_arg1799z00_2088;
									BgL_cmdz00_2076 = BgL_cmdz00_3740;
									BgL_objectsz00_2075 = BgL_objectsz00_3739;
									goto BgL_zc3z04anonymousza31759ze3z87_2077;
								}
							}
					}
				else
					{
						obj_t BgL_objectsz00_2099;
						obj_t BgL_cmdz00_2100;

						BgL_objectsz00_2099 = BgL_objectsz00_12;
						BgL_cmdz00_2100 = BGl_string2184z00zzsaw_jvm_ldz00;
					BgL_zc3z04anonymousza31832ze3z87_2101:
						if (NULLP(BgL_objectsz00_2099))
							{	/* SawJvm/jld.scm 260 */
								obj_t BgL_arg1834z00_2103;

								BgL_arg1834z00_2103 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(8));
								{	/* SawJvm/jld.scm 260 */
									obj_t BgL_list1835z00_2104;

									{	/* SawJvm/jld.scm 260 */
										obj_t BgL_arg1836z00_2105;

										{	/* SawJvm/jld.scm 260 */
											obj_t BgL_arg1837z00_2106;

											{	/* SawJvm/jld.scm 260 */
												obj_t BgL_arg1838z00_2107;

												{	/* SawJvm/jld.scm 260 */
													obj_t BgL_arg1839z00_2108;

													{	/* SawJvm/jld.scm 260 */
														obj_t BgL_arg1840z00_2109;

														{	/* SawJvm/jld.scm 260 */
															obj_t BgL_arg1842z00_2110;

															BgL_arg1842z00_2110 =
																MAKE_YOUNG_PAIR(BgL_cmdz00_2100, BNIL);
															BgL_arg1840z00_2109 =
																MAKE_YOUNG_PAIR
																(BGl_string2186z00zzsaw_jvm_ldz00,
																BgL_arg1842z00_2110);
														}
														BgL_arg1839z00_2108 =
															MAKE_YOUNG_PAIR(BgL_targetz00_10,
															BgL_arg1840z00_2109);
													}
													BgL_arg1838z00_2107 =
														MAKE_YOUNG_PAIR(BGl_string2186z00zzsaw_jvm_ldz00,
														BgL_arg1839z00_2108);
												}
												BgL_arg1837z00_2106 =
													MAKE_YOUNG_PAIR(BgL_manifestz00_11,
													BgL_arg1838z00_2107);
											}
											BgL_arg1836z00_2105 =
												MAKE_YOUNG_PAIR(BGl_string2186z00zzsaw_jvm_ldz00,
												BgL_arg1837z00_2106);
										}
										BgL_list1835z00_2104 =
											MAKE_YOUNG_PAIR(BgL_arg1834z00_2103, BgL_arg1836z00_2105);
									}
									BgL_cmdz00_2058 =
										BGl_stringzd2appendzd2zz__r4_strings_6_7z00
										(BgL_list1835z00_2104);
								}
							}
						else
							{	/* SawJvm/jld.scm 264 */
								obj_t BgL_arg1843z00_2111;
								obj_t BgL_arg1844z00_2112;

								BgL_arg1843z00_2111 = CDR(((obj_t) BgL_objectsz00_2099));
								{	/* SawJvm/jld.scm 265 */
									obj_t BgL_arg1845z00_2113;

									BgL_arg1845z00_2113 = CAR(((obj_t) BgL_objectsz00_2099));
									BgL_arg1844z00_2112 =
										string_append_3(BgL_arg1845z00_2113,
										BGl_string2186z00zzsaw_jvm_ldz00, BgL_cmdz00_2100);
								}
								{
									obj_t BgL_cmdz00_3759;
									obj_t BgL_objectsz00_3758;

									BgL_objectsz00_3758 = BgL_arg1843z00_2111;
									BgL_cmdz00_3759 = BgL_arg1844z00_2112;
									BgL_cmdz00_2100 = BgL_cmdz00_3759;
									BgL_objectsz00_2099 = BgL_objectsz00_3758;
									goto BgL_zc3z04anonymousza31832ze3z87_2101;
								}
							}
					}
				{	/* SawJvm/jld.scm 267 */
					obj_t BgL_list1748z00_2059;

					{	/* SawJvm/jld.scm 267 */
						obj_t BgL_arg1749z00_2060;

						{	/* SawJvm/jld.scm 267 */
							obj_t BgL_arg1750z00_2061;

							BgL_arg1750z00_2061 =
								MAKE_YOUNG_PAIR(BGl_string2188z00zzsaw_jvm_ldz00, BNIL);
							BgL_arg1749z00_2060 =
								MAKE_YOUNG_PAIR(BgL_cmdz00_2058, BgL_arg1750z00_2061);
						}
						BgL_list1748z00_2059 =
							MAKE_YOUNG_PAIR(BGl_string2189z00zzsaw_jvm_ldz00,
							BgL_arg1749z00_2060);
					}
					BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1748z00_2059);
				}
				{	/* SawJvm/jld.scm 268 */
					obj_t BgL_exitd1117z00_2062;

					BgL_exitd1117z00_2062 = BGL_EXITD_TOP_AS_OBJ();
					{	/* SawJvm/jld.scm 272 */
						obj_t BgL_zc3z04anonymousza31757ze3z87_3103;

						BgL_zc3z04anonymousza31757ze3z87_3103 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31757ze3ze5zzsaw_jvm_ldz00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31757ze3z87_3103, (int) (0L),
							BgL_manifestz00_11);
						{	/* SawJvm/jld.scm 268 */
							obj_t BgL_arg1828z00_2777;

							{	/* SawJvm/jld.scm 268 */
								obj_t BgL_arg1829z00_2778;

								BgL_arg1829z00_2778 = BGL_EXITD_PROTECT(BgL_exitd1117z00_2062);
								BgL_arg1828z00_2777 =
									MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31757ze3z87_3103,
									BgL_arg1829z00_2778);
							}
							BGL_EXITD_PROTECT_SET(BgL_exitd1117z00_2062, BgL_arg1828z00_2777);
							BUNSPEC;
						}
						{	/* SawJvm/jld.scm 269 */
							obj_t BgL_tmp1119z00_2064;

							{	/* SawJvm/jld.scm 269 */
								bool_t BgL_test2394z00_3774;

								{	/* SawJvm/jld.scm 269 */
									obj_t BgL_arg1755z00_2069;

									{	/* SawJvm/jld.scm 269 */
										obj_t BgL_list1756z00_2070;

										BgL_list1756z00_2070 =
											MAKE_YOUNG_PAIR(BgL_cmdz00_2058, BNIL);
										BgL_arg1755z00_2069 =
											BGl_systemz00zz__osz00(BgL_list1756z00_2070);
									}
									BgL_test2394z00_3774 =
										((long) CINT(BgL_arg1755z00_2069) == 0L);
								}
								if (BgL_test2394z00_3774)
									{	/* SawJvm/jld.scm 269 */
										BgL_tmp1119z00_2064 = BTRUE;
									}
								else
									{	/* SawJvm/jld.scm 269 */
										BgL_tmp1119z00_2064 =
											BGl_errorz00zz__errorz00
											(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(8)), BGl_string2190z00zzsaw_jvm_ldz00,
											BgL_targetz00_10);
									}
							}
							{	/* SawJvm/jld.scm 268 */
								bool_t BgL_test2395z00_3782;

								{	/* SawJvm/jld.scm 268 */
									obj_t BgL_arg1827z00_2781;

									BgL_arg1827z00_2781 =
										BGL_EXITD_PROTECT(BgL_exitd1117z00_2062);
									BgL_test2395z00_3782 = PAIRP(BgL_arg1827z00_2781);
								}
								if (BgL_test2395z00_3782)
									{	/* SawJvm/jld.scm 268 */
										obj_t BgL_arg1825z00_2782;

										{	/* SawJvm/jld.scm 268 */
											obj_t BgL_arg1826z00_2783;

											BgL_arg1826z00_2783 =
												BGL_EXITD_PROTECT(BgL_exitd1117z00_2062);
											BgL_arg1825z00_2782 = CDR(((obj_t) BgL_arg1826z00_2783));
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1117z00_2062,
											BgL_arg1825z00_2782);
										BUNSPEC;
									}
								else
									{	/* SawJvm/jld.scm 268 */
										BFALSE;
									}
							}
							BGl_z62zc3z04anonymousza31757ze3ze5zzsaw_jvm_ldz00
								(BgL_zc3z04anonymousza31757ze3z87_3103);
							return BgL_tmp1119z00_2064;
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1757> */
	obj_t BGl_z62zc3z04anonymousza31757ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3104)
	{
		{	/* SawJvm/jld.scm 268 */
			{	/* SawJvm/jld.scm 272 */
				obj_t BgL_manifestz00_3105;

				BgL_manifestz00_3105 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3104, (int) (0L)));
				{	/* SawJvm/jld.scm 272 */
					bool_t BgL_tmpz00_3793;

					if (CBOOL(BGl_za2rmzd2tmpzd2filesza2z00zzengine_paramz00))
						{	/* SawJvm/jld.scm 273 */
							char *BgL_stringz00_3151;

							BgL_stringz00_3151 = BSTRING_TO_STRING(BgL_manifestz00_3105);
							if (unlink(BgL_stringz00_3151))
								{	/* SawJvm/jld.scm 273 */
									BgL_tmpz00_3793 = ((bool_t) 0);
								}
							else
								{	/* SawJvm/jld.scm 273 */
									BgL_tmpz00_3793 = ((bool_t) 1);
								}
						}
					else
						{	/* SawJvm/jld.scm 272 */
							BgL_tmpz00_3793 = ((bool_t) 0);
						}
					return BBOOL(BgL_tmpz00_3793);
				}
			}
		}

	}



/* make-manifest-name */
	obj_t BGl_makezd2manifestzd2namez00zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 278 */
			{
				obj_t BgL_namez00_2787;

				BgL_namez00_2787 = BGl_string2192z00zzsaw_jvm_ldz00;
			BgL_loopz00_2786:
				if (fexists(BSTRING_TO_STRING(BgL_namez00_2787)))
					{
						obj_t BgL_namez00_3803;

						BgL_namez00_3803 =
							string_append(BgL_namez00_2787, BGl_string2191z00zzsaw_jvm_ldz00);
						BgL_namez00_2787 = BgL_namez00_3803;
						goto BgL_loopz00_2786;
					}
				else
					{	/* SawJvm/jld.scm 280 */
						return BgL_namez00_2787;
					}
			}
		}

	}



/* jvm-bigloo-classpath */
	obj_t BGl_jvmzd2bigloozd2classpathz00zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 287 */
			{	/* SawJvm/jld.scm 289 */
				obj_t BgL_arg1849z00_2121;

				if (STRINGP(BGl_za2jvmzd2bigloozd2classpathza2z00zzengine_paramz00))
					{	/* SawJvm/jld.scm 289 */
						BgL_arg1849z00_2121 =
							BGl_za2jvmzd2bigloozd2classpathza2z00zzengine_paramz00;
					}
				else
					{	/* SawJvm/jld.scm 289 */
						BgL_arg1849z00_2121 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(9));
					}
				return BGl_uncygdrivez00zztools_miscz00(BgL_arg1849z00_2121);
			}
		}

	}



/* generate-jvm-manifest */
	obj_t BGl_generatezd2jvmzd2manifestz00zzsaw_jvm_ldz00(obj_t BgL_fnamez00_14,
		obj_t BgL_mainz00_15, obj_t BgL_jarnamez00_16, obj_t BgL_za7ipsza7_17)
	{
		{	/* SawJvm/jld.scm 304 */
			{	/* SawJvm/jld.scm 306 */
				bool_t BgL_test2400z00_3810;

				{	/* SawJvm/jld.scm 306 */
					obj_t BgL_string1z00_2794;

					BgL_string1z00_2794 = BGl_za2jvmzd2shellza2zd2zzengine_paramz00;
					{	/* SawJvm/jld.scm 306 */
						long BgL_l1z00_2796;

						BgL_l1z00_2796 = STRING_LENGTH(BgL_string1z00_2794);
						if ((BgL_l1z00_2796 == 2L))
							{	/* SawJvm/jld.scm 306 */
								int BgL_arg1282z00_2799;

								{	/* SawJvm/jld.scm 306 */
									char *BgL_auxz00_3816;
									char *BgL_tmpz00_3814;

									BgL_auxz00_3816 =
										BSTRING_TO_STRING(BGl_string2193z00zzsaw_jvm_ldz00);
									BgL_tmpz00_3814 = BSTRING_TO_STRING(BgL_string1z00_2794);
									BgL_arg1282z00_2799 =
										memcmp(BgL_tmpz00_3814, BgL_auxz00_3816, BgL_l1z00_2796);
								}
								BgL_test2400z00_3810 = ((long) (BgL_arg1282z00_2799) == 0L);
							}
						else
							{	/* SawJvm/jld.scm 306 */
								BgL_test2400z00_3810 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2400z00_3810)
					{	/* SawJvm/jld.scm 306 */
						return
							BGl_generatezd2shzd2jvmzd2manifestzd2zzsaw_jvm_ldz00
							(BgL_fnamez00_14, BgL_mainz00_15, BgL_za7ipsza7_17);
					}
				else
					{	/* SawJvm/jld.scm 308 */
						bool_t BgL_test2402z00_3822;

						{	/* SawJvm/jld.scm 308 */
							obj_t BgL_string1z00_2805;

							BgL_string1z00_2805 = BGl_za2jvmzd2shellza2zd2zzengine_paramz00;
							{	/* SawJvm/jld.scm 308 */
								long BgL_l1z00_2807;

								BgL_l1z00_2807 = STRING_LENGTH(BgL_string1z00_2805);
								if ((BgL_l1z00_2807 == 5L))
									{	/* SawJvm/jld.scm 308 */
										int BgL_arg1282z00_2810;

										{	/* SawJvm/jld.scm 308 */
											char *BgL_auxz00_3828;
											char *BgL_tmpz00_3826;

											BgL_auxz00_3828 =
												BSTRING_TO_STRING(BGl_string2170z00zzsaw_jvm_ldz00);
											BgL_tmpz00_3826 = BSTRING_TO_STRING(BgL_string1z00_2805);
											BgL_arg1282z00_2810 =
												memcmp(BgL_tmpz00_3826, BgL_auxz00_3828,
												BgL_l1z00_2807);
										}
										BgL_test2402z00_3822 = ((long) (BgL_arg1282z00_2810) == 0L);
									}
								else
									{	/* SawJvm/jld.scm 308 */
										BgL_test2402z00_3822 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2402z00_3822)
							{	/* SawJvm/jld.scm 308 */
								return
									BGl_generatezd2msdoszd2jvmzd2manifestzd2zzsaw_jvm_ldz00
									(BgL_fnamez00_14, BgL_mainz00_15, BgL_jarnamez00_16);
							}
						else
							{	/* SawJvm/jld.scm 308 */
								{	/* SawJvm/jld.scm 311 */
									obj_t BgL_list1854z00_2126;

									{	/* SawJvm/jld.scm 311 */
										obj_t BgL_arg1856z00_2127;

										{	/* SawJvm/jld.scm 311 */
											obj_t BgL_arg1857z00_2128;

											{	/* SawJvm/jld.scm 311 */
												obj_t BgL_arg1858z00_2129;

												BgL_arg1858z00_2129 =
													MAKE_YOUNG_PAIR(BGl_string2194z00zzsaw_jvm_ldz00,
													BNIL);
												BgL_arg1857z00_2128 =
													MAKE_YOUNG_PAIR
													(BGl_za2jvmzd2shellza2zd2zzengine_paramz00,
													BgL_arg1858z00_2129);
											}
											BgL_arg1856z00_2127 =
												MAKE_YOUNG_PAIR(BGl_string2195z00zzsaw_jvm_ldz00,
												BgL_arg1857z00_2128);
										}
										BgL_list1854z00_2126 =
											MAKE_YOUNG_PAIR(BGl_string2196z00zzsaw_jvm_ldz00,
											BgL_arg1856z00_2127);
									}
									BGl_warningz00zz__errorz00(BgL_list1854z00_2126);
								}
								return
									BGl_generatezd2shzd2jvmzd2manifestzd2zzsaw_jvm_ldz00
									(BgL_fnamez00_14, BgL_mainz00_15, BgL_za7ipsza7_17);
							}
					}
			}
		}

	}



/* split-72 */
	obj_t BGl_splitzd272zd2zzsaw_jvm_ldz00(obj_t BgL_strz00_18)
	{
		{	/* SawJvm/jld.scm 318 */
			return BGl_loopze70ze7zzsaw_jvm_ldz00(BgL_strz00_18, 0L);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzsaw_jvm_ldz00(obj_t BgL_strz00_3131,
		long BgL_oz00_2131)
	{
		{	/* SawJvm/jld.scm 319 */
			if (((STRING_LENGTH(((obj_t) BgL_strz00_3131)) - BgL_oz00_2131) <= 72L))
				{	/* SawJvm/jld.scm 320 */
					if ((BgL_oz00_2131 == 0L))
						{	/* SawJvm/jld.scm 321 */
							return BgL_strz00_3131;
						}
					else
						{	/* SawJvm/jld.scm 323 */
							long BgL_arg1866z00_2137;

							BgL_arg1866z00_2137 = STRING_LENGTH(((obj_t) BgL_strz00_3131));
							return
								c_substring(
								((obj_t) BgL_strz00_3131), BgL_oz00_2131, BgL_arg1866z00_2137);
						}
				}
			else
				{	/* SawJvm/jld.scm 324 */
					obj_t BgL_arg1868z00_2138;
					obj_t BgL_arg1869z00_2139;

					{	/* SawJvm/jld.scm 324 */
						long BgL_arg1870z00_2140;

						BgL_arg1870z00_2140 = (BgL_oz00_2131 + 71L);
						BgL_arg1868z00_2138 =
							c_substring(
							((obj_t) BgL_strz00_3131), BgL_oz00_2131, BgL_arg1870z00_2140);
					}
					BgL_arg1869z00_2139 =
						BGl_loopze70ze7zzsaw_jvm_ldz00(BgL_strz00_3131,
						(BgL_oz00_2131 + 72L));
					return
						string_append_3(BgL_arg1868z00_2138,
						BGl_string2197z00zzsaw_jvm_ldz00, BgL_arg1869z00_2139);
				}
		}

	}



/* manifest-classpath-format */
	obj_t BGl_manifestzd2classpathzd2formatz00zzsaw_jvm_ldz00(obj_t
		BgL_classpathz00_19)
	{
		{	/* SawJvm/jld.scm 332 */
			{	/* SawJvm/jld.scm 333 */
				bool_t BgL_test2406z00_3858;

				{	/* SawJvm/jld.scm 333 */
					obj_t BgL_string1z00_2830;

					BgL_string1z00_2830 = BGl_za2jvmzd2shellza2zd2zzengine_paramz00;
					{	/* SawJvm/jld.scm 333 */
						long BgL_l1z00_2832;

						BgL_l1z00_2832 = STRING_LENGTH(BgL_string1z00_2830);
						if ((BgL_l1z00_2832 == 2L))
							{	/* SawJvm/jld.scm 333 */
								int BgL_arg1282z00_2835;

								{	/* SawJvm/jld.scm 333 */
									char *BgL_auxz00_3864;
									char *BgL_tmpz00_3862;

									BgL_auxz00_3864 =
										BSTRING_TO_STRING(BGl_string2193z00zzsaw_jvm_ldz00);
									BgL_tmpz00_3862 = BSTRING_TO_STRING(BgL_string1z00_2830);
									BgL_arg1282z00_2835 =
										memcmp(BgL_tmpz00_3862, BgL_auxz00_3864, BgL_l1z00_2832);
								}
								BgL_test2406z00_3858 = ((long) (BgL_arg1282z00_2835) == 0L);
							}
						else
							{	/* SawJvm/jld.scm 333 */
								BgL_test2406z00_3858 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2406z00_3858)
					{	/* SawJvm/jld.scm 333 */
						return
							BGl_stringzd2replacezd2zz__r4_strings_6_7z00(BgL_classpathz00_19,
							(char) (((unsigned char) ':')), (char) (((unsigned char) ' ')));
					}
				else
					{	/* SawJvm/jld.scm 335 */
						bool_t BgL_test2408z00_3872;

						{	/* SawJvm/jld.scm 335 */
							obj_t BgL_string1z00_2841;

							BgL_string1z00_2841 = BGl_za2jvmzd2shellza2zd2zzengine_paramz00;
							{	/* SawJvm/jld.scm 335 */
								long BgL_l1z00_2843;

								BgL_l1z00_2843 = STRING_LENGTH(BgL_string1z00_2841);
								if ((BgL_l1z00_2843 == 5L))
									{	/* SawJvm/jld.scm 335 */
										int BgL_arg1282z00_2846;

										{	/* SawJvm/jld.scm 335 */
											char *BgL_auxz00_3878;
											char *BgL_tmpz00_3876;

											BgL_auxz00_3878 =
												BSTRING_TO_STRING(BGl_string2170z00zzsaw_jvm_ldz00);
											BgL_tmpz00_3876 = BSTRING_TO_STRING(BgL_string1z00_2841);
											BgL_arg1282z00_2846 =
												memcmp(BgL_tmpz00_3876, BgL_auxz00_3878,
												BgL_l1z00_2843);
										}
										BgL_test2408z00_3872 = ((long) (BgL_arg1282z00_2846) == 0L);
									}
								else
									{	/* SawJvm/jld.scm 335 */
										BgL_test2408z00_3872 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2408z00_3872)
							{	/* SawJvm/jld.scm 335 */
								return
									BGl_stringzd2replacezd2zz__r4_strings_6_7z00
									(BgL_classpathz00_19, (char) (((unsigned char) ';')),
									(char) (((unsigned char) ' ')));
							}
						else
							{	/* SawJvm/jld.scm 335 */
								{	/* SawJvm/jld.scm 338 */
									obj_t BgL_list1877z00_2147;

									{	/* SawJvm/jld.scm 338 */
										obj_t BgL_arg1878z00_2148;

										{	/* SawJvm/jld.scm 338 */
											obj_t BgL_arg1879z00_2149;

											{	/* SawJvm/jld.scm 338 */
												obj_t BgL_arg1880z00_2150;

												BgL_arg1880z00_2150 =
													MAKE_YOUNG_PAIR(BGl_string2198z00zzsaw_jvm_ldz00,
													BNIL);
												BgL_arg1879z00_2149 =
													MAKE_YOUNG_PAIR
													(BGl_za2jvmzd2shellza2zd2zzengine_paramz00,
													BgL_arg1880z00_2150);
											}
											BgL_arg1878z00_2148 =
												MAKE_YOUNG_PAIR(BGl_string2199z00zzsaw_jvm_ldz00,
												BgL_arg1879z00_2149);
										}
										BgL_list1877z00_2147 =
											MAKE_YOUNG_PAIR(BGl_string2200z00zzsaw_jvm_ldz00,
											BgL_arg1878z00_2148);
									}
									BGl_warningz00zz__errorz00(BgL_list1877z00_2147);
								}
								return
									BGl_stringzd2replacezd2zz__r4_strings_6_7z00
									(BgL_classpathz00_19, (char) (((unsigned char) ':')),
									(char) (((unsigned char) ' ')));
		}}}}

	}



/* generate-sh-jvm-manifest */
	obj_t BGl_generatezd2shzd2jvmzd2manifestzd2zzsaw_jvm_ldz00(obj_t
		BgL_fnamez00_20, obj_t BgL_mainz00_21, obj_t BgL_za7ipsza7_22)
	{
		{	/* SawJvm/jld.scm 345 */
			{	/* SawJvm/jld.scm 348 */
				obj_t BgL_zc3z04anonymousza31883ze3z87_3106;

				BgL_zc3z04anonymousza31883ze3z87_3106 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_jvm_ldz00,
					(int) (0L), (int) (2L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31883ze3z87_3106,
					(int) (0L), BgL_mainz00_21);
				PROCEDURE_SET(BgL_zc3z04anonymousza31883ze3z87_3106,
					(int) (1L), BgL_za7ipsza7_22);
				return
					BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
					(BgL_fnamez00_20, BgL_zc3z04anonymousza31883ze3z87_3106);
			}
		}

	}



/* &<@anonymous:1883> */
	obj_t BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3107)
	{
		{	/* SawJvm/jld.scm 347 */
			{	/* SawJvm/jld.scm 348 */
				obj_t BgL_mainz00_3108;
				obj_t BgL_za7ipsza7_3109;

				BgL_mainz00_3108 = PROCEDURE_REF(BgL_envz00_3107, (int) (0L));
				BgL_za7ipsza7_3109 = PROCEDURE_REF(BgL_envz00_3107, (int) (1L));
				{	/* SawJvm/jld.scm 348 */
					obj_t BgL_port1264z00_3152;

					{	/* SawJvm/jld.scm 348 */
						obj_t BgL_tmpz00_3906;

						BgL_tmpz00_3906 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1264z00_3152 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3906);
					}
					bgl_display_string(BGl_string2201z00zzsaw_jvm_ldz00,
						BgL_port1264z00_3152);
					bgl_display_char(((unsigned char) 10), BgL_port1264z00_3152);
				}
				{	/* SawJvm/jld.scm 349 */
					obj_t BgL_port1265z00_3153;

					{	/* SawJvm/jld.scm 349 */
						obj_t BgL_tmpz00_3911;

						BgL_tmpz00_3911 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1265z00_3153 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3911);
					}
					bgl_display_string(BGl_string2202z00zzsaw_jvm_ldz00,
						BgL_port1265z00_3153);
					bgl_display_obj(BgL_mainz00_3108, BgL_port1265z00_3153);
					bgl_display_char(((unsigned char) 10), BgL_port1265z00_3153);
				}
				{	/* SawJvm/jld.scm 351 */
					obj_t BgL_arg1884z00_3154;

					BgL_arg1884z00_3154 =
						BGl_splitzd272zd2zzsaw_jvm_ldz00
						(BGl_manifestzd2classpathzd2formatz00zzsaw_jvm_ldz00
						(BGl_za2jvmzd2classpathza2zd2zzengine_paramz00));
					{	/* SawJvm/jld.scm 350 */
						obj_t BgL_list1885z00_3155;

						{	/* SawJvm/jld.scm 350 */
							obj_t BgL_arg1887z00_3156;

							BgL_arg1887z00_3156 = MAKE_YOUNG_PAIR(BgL_arg1884z00_3154, BNIL);
							BgL_list1885z00_3155 =
								MAKE_YOUNG_PAIR(BGl_string2203z00zzsaw_jvm_ldz00,
								BgL_arg1887z00_3156);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1885z00_3155);
				}}
				{	/* SawJvm/jld.scm 354 */
					obj_t BgL_arg1889z00_3157;

					{	/* SawJvm/jld.scm 354 */
						obj_t BgL_tmpz00_3922;

						BgL_tmpz00_3922 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1889z00_3157 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3922);
					}
					bgl_display_string(BGl_string2204z00zzsaw_jvm_ldz00,
						BgL_arg1889z00_3157);
				}
				{	/* SawJvm/jld.scm 357 */
					obj_t BgL_arg1890z00_3158;
					obj_t BgL_arg1891z00_3159;

					{	/* SawJvm/jld.scm 357 */
						obj_t BgL_arg1892z00_3160;

						{	/* SawJvm/jld.scm 357 */
							obj_t BgL_arg1893z00_3161;
							obj_t BgL_arg1894z00_3162;

							BgL_arg1893z00_3161 =
								BGl_jvmzd2bigloozd2classpathz00zzsaw_jvm_ldz00();
							{	/* SawJvm/jld.scm 358 */
								bool_t BgL_test2410z00_3927;

								if (CBOOL(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
									{	/* SawJvm/jld.scm 358 */
										if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
											{	/* SawJvm/jld.scm 358 */
												BgL_test2410z00_3927 = ((bool_t) 0);
											}
										else
											{	/* SawJvm/jld.scm 358 */
												BgL_test2410z00_3927 = ((bool_t) 1);
											}
									}
								else
									{	/* SawJvm/jld.scm 358 */
										BgL_test2410z00_3927 = ((bool_t) 0);
									}
								if (BgL_test2410z00_3927)
									{	/* SawJvm/jld.scm 358 */
										BgL_arg1894z00_3162 = BGl_string2205z00zzsaw_jvm_ldz00;
									}
								else
									{	/* SawJvm/jld.scm 358 */
										BgL_arg1894z00_3162 = BGl_string2206z00zzsaw_jvm_ldz00;
									}
							}
							BgL_arg1892z00_3160 =
								BGl_makezd2filezd2namez00zz__osz00(BgL_arg1893z00_3161,
								BgL_arg1894z00_3162);
						}
						BgL_arg1890z00_3158 =
							BGl_splitzd272zd2zzsaw_jvm_ldz00(BgL_arg1892z00_3160);
					}
					{	/* SawJvm/jld.scm 355 */
						obj_t BgL_tmpz00_3934;

						BgL_tmpz00_3934 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1891z00_3159 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3934);
					}
					bgl_display_obj(BgL_arg1890z00_3158, BgL_arg1891z00_3159);
				}
				{
					obj_t BgL_l1266z00_3164;

					BgL_l1266z00_3164 = BgL_za7ipsza7_3109;
				BgL_zc3z04anonymousza31896ze3z87_3163:
					if (PAIRP(BgL_l1266z00_3164))
						{	/* SawJvm/jld.scm 361 */
							{	/* SawJvm/jld.scm 365 */
								obj_t BgL_lz00_3165;

								BgL_lz00_3165 = CAR(BgL_l1266z00_3164);
								{	/* SawJvm/jld.scm 365 */
									obj_t BgL_arg1898z00_3166;

									{	/* SawJvm/jld.scm 365 */
										obj_t BgL_tmpz00_3941;

										BgL_tmpz00_3941 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1898z00_3166 =
											BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3941);
									}
									bgl_display_string(BGl_string2204z00zzsaw_jvm_ldz00,
										BgL_arg1898z00_3166);
								}
								{	/* SawJvm/jld.scm 366 */
									obj_t BgL_arg1899z00_3167;
									obj_t BgL_arg1901z00_3168;

									BgL_arg1899z00_3167 =
										BGl_splitzd272zd2zzsaw_jvm_ldz00
										(BGl_userzd2libraryzd2zzsaw_jvm_ldz00(BgL_lz00_3165));
									{	/* SawJvm/jld.scm 366 */
										obj_t BgL_tmpz00_3947;

										BgL_tmpz00_3947 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1901z00_3168 =
											BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3947);
									}
									bgl_display_obj(BgL_arg1899z00_3167, BgL_arg1901z00_3168);
								}
							}
							{
								obj_t BgL_l1266z00_3951;

								BgL_l1266z00_3951 = CDR(BgL_l1266z00_3164);
								BgL_l1266z00_3164 = BgL_l1266z00_3951;
								goto BgL_zc3z04anonymousza31896ze3z87_3163;
							}
						}
					else
						{	/* SawJvm/jld.scm 361 */
							((bool_t) 1);
						}
				}
				{	/* SawJvm/jld.scm 368 */
					obj_t BgL_arg1904z00_3169;

					{	/* SawJvm/jld.scm 368 */
						obj_t BgL_tmpz00_3953;

						BgL_tmpz00_3953 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1904z00_3169 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3953);
					}
					bgl_display_char(((unsigned char) 10), BgL_arg1904z00_3169);
				}
				{	/* SawJvm/jld.scm 369 */
					obj_t BgL_port1268z00_3170;

					{	/* SawJvm/jld.scm 369 */
						obj_t BgL_tmpz00_3957;

						BgL_tmpz00_3957 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1268z00_3170 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3957);
					}
					bgl_display_string(BGl_string2207z00zzsaw_jvm_ldz00,
						BgL_port1268z00_3170);
					bgl_display_obj(BGl_za2bigloozd2nameza2zd2zzengine_paramz00,
						BgL_port1268z00_3170);
					bgl_display_char(((unsigned char) 10), BgL_port1268z00_3170);
				}
				{	/* SawJvm/jld.scm 370 */
					obj_t BgL_arg1906z00_3171;

					{	/* SawJvm/jld.scm 370 */
						obj_t BgL_tmpz00_3963;

						BgL_tmpz00_3963 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1906z00_3171 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3963);
					}
					return bgl_display_char(((unsigned char) 10), BgL_arg1906z00_3171);
		}}}

	}



/* generate-msdos-jvm-manifest */
	obj_t BGl_generatezd2msdoszd2jvmzd2manifestzd2zzsaw_jvm_ldz00(obj_t
		BgL_fnamez00_23, obj_t BgL_mainz00_24, obj_t BgL_jarnamez00_25)
	{
		{	/* SawJvm/jld.scm 375 */
			{	/* SawJvm/jld.scm 378 */
				obj_t BgL_zc3z04anonymousza31911ze3z87_3110;

				BgL_zc3z04anonymousza31911ze3z87_3110 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31911ze3ze5zzsaw_jvm_ldz00,
					(int) (0L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31911ze3z87_3110,
					(int) (0L), BgL_mainz00_24);
				return
					BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
					(BgL_fnamez00_23, BgL_zc3z04anonymousza31911ze3z87_3110);
			}
		}

	}



/* &<@anonymous:1911> */
	obj_t BGl_z62zc3z04anonymousza31911ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3111)
	{
		{	/* SawJvm/jld.scm 377 */
			{	/* SawJvm/jld.scm 378 */
				obj_t BgL_mainz00_3112;

				BgL_mainz00_3112 = PROCEDURE_REF(BgL_envz00_3111, (int) (0L));
				{	/* SawJvm/jld.scm 378 */
					obj_t BgL_port1269z00_3172;

					{	/* SawJvm/jld.scm 378 */
						obj_t BgL_tmpz00_3975;

						BgL_tmpz00_3975 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1269z00_3172 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3975);
					}
					bgl_display_string(BGl_string2201z00zzsaw_jvm_ldz00,
						BgL_port1269z00_3172);
					bgl_display_char(((unsigned char) 10), BgL_port1269z00_3172);
				}
				{	/* SawJvm/jld.scm 379 */
					obj_t BgL_port1270z00_3173;

					{	/* SawJvm/jld.scm 379 */
						obj_t BgL_tmpz00_3980;

						BgL_tmpz00_3980 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1270z00_3173 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3980);
					}
					bgl_display_string(BGl_string2202z00zzsaw_jvm_ldz00,
						BgL_port1270z00_3173);
					bgl_display_obj(BgL_mainz00_3112, BgL_port1270z00_3173);
					bgl_display_char(((unsigned char) 10), BgL_port1270z00_3173);
				}
				{	/* SawJvm/jld.scm 380 */
					obj_t BgL_port1271z00_3174;

					{	/* SawJvm/jld.scm 380 */
						obj_t BgL_tmpz00_3986;

						BgL_tmpz00_3986 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1271z00_3174 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3986);
					}
					bgl_display_string(BGl_string2207z00zzsaw_jvm_ldz00,
						BgL_port1271z00_3174);
					bgl_display_obj(BGl_za2bigloozd2nameza2zd2zzengine_paramz00,
						BgL_port1271z00_3174);
					bgl_display_char(((unsigned char) 10), BgL_port1271z00_3174);
				}
				{	/* SawJvm/jld.scm 381 */
					obj_t BgL_arg1912z00_3175;

					{	/* SawJvm/jld.scm 381 */
						obj_t BgL_tmpz00_3992;

						BgL_tmpz00_3992 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1912z00_3175 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_3992);
					}
					return bgl_display_char(((unsigned char) 10), BgL_arg1912z00_3175);
		}}}

	}



/* generate-jvm-sh-script */
	bool_t BGl_generatezd2jvmzd2shzd2scriptzd2zzsaw_jvm_ldz00(obj_t
		BgL_targetz00_26, obj_t BgL_mainzd2classzd2_27, obj_t BgL_za7ipsza7_28)
	{
		{	/* SawJvm/jld.scm 386 */
			{

				if (CBOOL(BGl_za2jvmzd2jarzf3za2z21zzengine_paramz00))
					{	/* SawJvm/jld.scm 451 */
						{	/* SawJvm/jld.scm 400 */
							obj_t BgL_zc3z04anonymousza31930ze3z87_3115;

							BgL_zc3z04anonymousza31930ze3z87_3115 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31930ze3ze5zzsaw_jvm_ldz00, (int) (0L),
								(int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31930ze3z87_3115, (int) (0L),
								BgL_targetz00_26);
							BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
								(BgL_targetz00_26, BgL_zc3z04anonymousza31930ze3z87_3115);
						}
						{	/* SawJvm/jld.scm 421 */
							obj_t BgL_list1947z00_2235;

							{	/* SawJvm/jld.scm 421 */
								obj_t BgL_arg1948z00_2236;

								{	/* SawJvm/jld.scm 421 */
									obj_t BgL_arg1949z00_2237;

									BgL_arg1949z00_2237 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BNIL);
									BgL_arg1948z00_2236 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg1949z00_2237);
								}
								BgL_list1947z00_2235 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_arg1948z00_2236);
							}
							return
								BGl_chmodz00zz__osz00(BgL_targetz00_26, BgL_list1947z00_2235);
						}
					}
				else
					{	/* SawJvm/jld.scm 451 */
						{	/* SawJvm/jld.scm 425 */
							obj_t BgL_zc3z04anonymousza31952ze3z87_3114;

							BgL_zc3z04anonymousza31952ze3z87_3114 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31952ze3ze5zzsaw_jvm_ldz00, (int) (0L),
								(int) (2L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31952ze3z87_3114, (int) (0L),
								BgL_za7ipsza7_28);
							PROCEDURE_SET(BgL_zc3z04anonymousza31952ze3z87_3114, (int) (1L),
								BgL_mainzd2classzd2_27);
							BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
								(BgL_targetz00_26, BgL_zc3z04anonymousza31952ze3z87_3114);
						}
						{	/* SawJvm/jld.scm 450 */
							obj_t BgL_list1978z00_2279;

							{	/* SawJvm/jld.scm 450 */
								obj_t BgL_arg1979z00_2280;

								{	/* SawJvm/jld.scm 450 */
									obj_t BgL_arg1980z00_2281;

									BgL_arg1980z00_2281 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BNIL);
									BgL_arg1979z00_2280 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg1980z00_2281);
								}
								BgL_list1978z00_2279 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_arg1979z00_2280);
							}
							return
								BGl_chmodz00zz__osz00(BgL_targetz00_26, BgL_list1978z00_2279);
						}
					}
			}
		}

	}



/* &generate-jvm-env */
	obj_t BGl_z62generatezd2jvmzd2envz62zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 396 */
			{
				obj_t BgL_envz00_2196;
				obj_t BgL_resz00_2197;

				BgL_envz00_2196 = BGl_za2jvmzd2envza2zd2zzengine_paramz00;
				BgL_resz00_2197 = BGl_string2184z00zzsaw_jvm_ldz00;
			BgL_zc3z04anonymousza31915ze3z87_2198:
				if (NULLP(BgL_envz00_2196))
					{	/* SawJvm/jld.scm 392 */
						return BgL_resz00_2197;
					}
				else
					{	/* SawJvm/jld.scm 394 */
						obj_t BgL_arg1917z00_2200;
						obj_t BgL_arg1918z00_2201;

						BgL_arg1917z00_2200 = CDR(((obj_t) BgL_envz00_2196));
						{	/* SawJvm/jld.scm 395 */
							obj_t BgL_arg1919z00_2202;
							obj_t BgL_arg1920z00_2203;

							BgL_arg1919z00_2202 = CAR(((obj_t) BgL_envz00_2196));
							BgL_arg1920z00_2203 = CAR(((obj_t) BgL_envz00_2196));
							{	/* SawJvm/jld.scm 395 */
								obj_t BgL_list1921z00_2204;

								{	/* SawJvm/jld.scm 395 */
									obj_t BgL_arg1923z00_2205;

									{	/* SawJvm/jld.scm 395 */
										obj_t BgL_arg1924z00_2206;

										{	/* SawJvm/jld.scm 395 */
											obj_t BgL_arg1925z00_2207;

											{	/* SawJvm/jld.scm 395 */
												obj_t BgL_arg1926z00_2208;

												{	/* SawJvm/jld.scm 395 */
													obj_t BgL_arg1927z00_2209;

													BgL_arg1927z00_2209 =
														MAKE_YOUNG_PAIR(BgL_resz00_2197, BNIL);
													BgL_arg1926z00_2208 =
														MAKE_YOUNG_PAIR(BGl_string2186z00zzsaw_jvm_ldz00,
														BgL_arg1927z00_2209);
												}
												BgL_arg1925z00_2207 =
													MAKE_YOUNG_PAIR(BgL_arg1920z00_2203,
													BgL_arg1926z00_2208);
											}
											BgL_arg1924z00_2206 =
												MAKE_YOUNG_PAIR(BGl_string2208z00zzsaw_jvm_ldz00,
												BgL_arg1925z00_2207);
										}
										BgL_arg1923z00_2205 =
											MAKE_YOUNG_PAIR(BgL_arg1919z00_2202, BgL_arg1924z00_2206);
									}
									BgL_list1921z00_2204 =
										MAKE_YOUNG_PAIR(BGl_string2209z00zzsaw_jvm_ldz00,
										BgL_arg1923z00_2205);
								}
								BgL_arg1918z00_2201 =
									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
									(BgL_list1921z00_2204);
							}
						}
						{
							obj_t BgL_resz00_4042;
							obj_t BgL_envz00_4041;

							BgL_envz00_4041 = BgL_arg1917z00_2200;
							BgL_resz00_4042 = BgL_arg1918z00_2201;
							BgL_resz00_2197 = BgL_resz00_4042;
							BgL_envz00_2196 = BgL_envz00_4041;
							goto BgL_zc3z04anonymousza31915ze3z87_2198;
						}
					}
			}
		}

	}



/* &<@anonymous:1952> */
	obj_t BGl_z62zc3z04anonymousza31952ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3116)
	{
		{	/* SawJvm/jld.scm 424 */
			{	/* SawJvm/jld.scm 425 */
				obj_t BgL_za7ipsza7_3117;
				obj_t BgL_mainzd2classzd2_3118;

				BgL_za7ipsza7_3117 = PROCEDURE_REF(BgL_envz00_3116, (int) (0L));
				BgL_mainzd2classzd2_3118 = PROCEDURE_REF(BgL_envz00_3116, (int) (1L));
				{	/* SawJvm/jld.scm 425 */
					obj_t BgL_port1276z00_3176;

					{	/* SawJvm/jld.scm 425 */
						obj_t BgL_tmpz00_4047;

						BgL_tmpz00_4047 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1276z00_3176 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4047);
					}
					bgl_display_string(BGl_string2210z00zzsaw_jvm_ldz00,
						BgL_port1276z00_3176);
					bgl_display_char(((unsigned char) 10), BgL_port1276z00_3176);
				}
				{	/* SawJvm/jld.scm 426 */
					obj_t BgL_arg1953z00_3177;

					{	/* SawJvm/jld.scm 426 */
						obj_t BgL_tmpz00_4052;

						BgL_tmpz00_4052 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1953z00_3177 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4052);
					}
					bgl_display_char(((unsigned char) 10), BgL_arg1953z00_3177);
				}
				{	/* SawJvm/jld.scm 427 */
					obj_t BgL_port1282z00_3178;

					{	/* SawJvm/jld.scm 427 */
						obj_t BgL_tmpz00_4056;

						BgL_tmpz00_4056 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1282z00_3178 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4056);
					}
					bgl_display_string(BGl_string2211z00zzsaw_jvm_ldz00,
						BgL_port1282z00_3178);
					{	/* SawJvm/jld.scm 433 */
						obj_t BgL_arg1954z00_3179;

						{	/* SawJvm/jld.scm 433 */
							obj_t BgL_arg1955z00_3180;

							{	/* SawJvm/jld.scm 433 */
								obj_t BgL_arg1956z00_3181;

								{	/* SawJvm/jld.scm 433 */
									obj_t BgL_arg1957z00_3182;

									{	/* SawJvm/jld.scm 433 */
										obj_t BgL_arg1958z00_3183;

										{	/* SawJvm/jld.scm 433 */
											obj_t BgL_arg1959z00_3184;
											obj_t BgL_arg1960z00_3185;

											{	/* SawJvm/jld.scm 433 */
												obj_t BgL_arg1961z00_3186;

												{	/* SawJvm/jld.scm 433 */
													obj_t BgL_arg1962z00_3187;
													obj_t BgL_arg1963z00_3188;

													BgL_arg1962z00_3187 =
														BGl_jvmzd2bigloozd2classpathz00zzsaw_jvm_ldz00();
													{	/* SawJvm/jld.scm 434 */
														bool_t BgL_test2416z00_4061;

														if (CBOOL
															(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
															{	/* SawJvm/jld.scm 434 */
																if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
																	{	/* SawJvm/jld.scm 435 */
																		BgL_test2416z00_4061 = ((bool_t) 0);
																	}
																else
																	{	/* SawJvm/jld.scm 435 */
																		BgL_test2416z00_4061 = ((bool_t) 1);
																	}
															}
														else
															{	/* SawJvm/jld.scm 434 */
																BgL_test2416z00_4061 = ((bool_t) 0);
															}
														if (BgL_test2416z00_4061)
															{	/* SawJvm/jld.scm 434 */
																BgL_arg1963z00_3188 =
																	BGl_string2205z00zzsaw_jvm_ldz00;
															}
														else
															{	/* SawJvm/jld.scm 434 */
																BgL_arg1963z00_3188 =
																	BGl_string2206z00zzsaw_jvm_ldz00;
															}
													}
													BgL_arg1961z00_3186 =
														BGl_makezd2filezd2namez00zz__osz00
														(BgL_arg1962z00_3187, BgL_arg1963z00_3188);
												}
												BgL_arg1959z00_3184 =
													string_append_3(BGl_string2212z00zzsaw_jvm_ldz00,
													BgL_arg1961z00_3186,
													BGl_string2212z00zzsaw_jvm_ldz00);
											}
											{	/* SawJvm/jld.scm 438 */
												obj_t BgL_arg1965z00_3189;

												if (NULLP(BgL_za7ipsza7_3117))
													{	/* SawJvm/jld.scm 438 */
														BgL_arg1965z00_3189 = BNIL;
													}
												else
													{	/* SawJvm/jld.scm 438 */
														obj_t BgL_head1279z00_3190;

														BgL_head1279z00_3190 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1277z00_3192;
															obj_t BgL_tail1280z00_3193;

															BgL_l1277z00_3192 = BgL_za7ipsza7_3117;
															BgL_tail1280z00_3193 = BgL_head1279z00_3190;
														BgL_zc3z04anonymousza31967ze3z87_3191:
															if (NULLP(BgL_l1277z00_3192))
																{	/* SawJvm/jld.scm 438 */
																	BgL_arg1965z00_3189 =
																		CDR(BgL_head1279z00_3190);
																}
															else
																{	/* SawJvm/jld.scm 438 */
																	obj_t BgL_newtail1281z00_3194;

																	{	/* SawJvm/jld.scm 438 */
																		obj_t BgL_arg1970z00_3195;

																		{	/* SawJvm/jld.scm 438 */
																			obj_t BgL_fz00_3196;

																			BgL_fz00_3196 =
																				CAR(((obj_t) BgL_l1277z00_3192));
																			BgL_arg1970z00_3195 =
																				string_append_3
																				(BGl_string2212z00zzsaw_jvm_ldz00,
																				BGl_userzd2libraryzd2zzsaw_jvm_ldz00
																				(BgL_fz00_3196),
																				BGl_string2212z00zzsaw_jvm_ldz00);
																		}
																		BgL_newtail1281z00_3194 =
																			MAKE_YOUNG_PAIR(BgL_arg1970z00_3195,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1280z00_3193,
																		BgL_newtail1281z00_3194);
																	{	/* SawJvm/jld.scm 438 */
																		obj_t BgL_arg1969z00_3197;

																		BgL_arg1969z00_3197 =
																			CDR(((obj_t) BgL_l1277z00_3192));
																		{
																			obj_t BgL_tail1280z00_4083;
																			obj_t BgL_l1277z00_4082;

																			BgL_l1277z00_4082 = BgL_arg1969z00_3197;
																			BgL_tail1280z00_4083 =
																				BgL_newtail1281z00_3194;
																			BgL_tail1280z00_3193 =
																				BgL_tail1280z00_4083;
																			BgL_l1277z00_3192 = BgL_l1277z00_4082;
																			goto
																				BgL_zc3z04anonymousza31967ze3z87_3191;
																		}
																	}
																}
														}
													}
												BgL_arg1960z00_3185 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1965z00_3189, BNIL);
											}
											BgL_arg1958z00_3183 =
												MAKE_YOUNG_PAIR(BgL_arg1959z00_3184,
												BgL_arg1960z00_3185);
										}
										BgL_arg1957z00_3182 =
											MAKE_YOUNG_PAIR(BGl_string2213z00zzsaw_jvm_ldz00,
											BgL_arg1958z00_3183);
									}
									BgL_arg1956z00_3181 =
										MAKE_YOUNG_PAIR(BGl_string2214z00zzsaw_jvm_ldz00,
										BgL_arg1957z00_3182);
								}
								BgL_arg1955z00_3180 =
									MAKE_YOUNG_PAIR(BGl_za2jvmzd2classpathza2zd2zzengine_paramz00,
									BgL_arg1956z00_3181);
							}
							BgL_arg1954z00_3179 =
								BGl_listzd2ze3shzd2pathzd2stringz31zzsaw_jvm_ldz00
								(BgL_arg1955z00_3180);
						}
						bgl_display_obj(BgL_arg1954z00_3179, BgL_port1282z00_3178);
					}
					bgl_display_char(((unsigned char) 10), BgL_port1282z00_3178);
				}
				{	/* SawJvm/jld.scm 439 */
					obj_t BgL_port1283z00_3198;

					{	/* SawJvm/jld.scm 439 */
						obj_t BgL_tmpz00_4092;

						BgL_tmpz00_4092 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1283z00_3198 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4092);
					}
					bgl_display_string(BGl_string2215z00zzsaw_jvm_ldz00,
						BgL_port1283z00_3198);
					bgl_display_char(((unsigned char) 10), BgL_port1283z00_3198);
				}
				{	/* SawJvm/jld.scm 440 */
					obj_t BgL_arg1972z00_3199;

					{	/* SawJvm/jld.scm 440 */
						obj_t BgL_tmpz00_4097;

						BgL_tmpz00_4097 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1972z00_3199 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4097);
					}
					bgl_display_char(((unsigned char) 10), BgL_arg1972z00_3199);
				}
				{	/* SawJvm/jld.scm 441 */
					obj_t BgL_port1284z00_3200;

					{	/* SawJvm/jld.scm 441 */
						obj_t BgL_tmpz00_4101;

						BgL_tmpz00_4101 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1284z00_3200 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4101);
					}
					bgl_display_string(BGl_string2216z00zzsaw_jvm_ldz00,
						BgL_port1284z00_3200);
					bgl_display_obj(BGl_za2jvmzd2javaza2zd2zzengine_paramz00,
						BgL_port1284z00_3200);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1284z00_3200);
					{	/* SawJvm/jld.scm 442 */
						obj_t BgL_arg1973z00_3201;

						if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
							{	/* SawJvm/jld.scm 442 */
								BgL_arg1973z00_3201 = BGl_string2184z00zzsaw_jvm_ldz00;
							}
						else
							{	/* SawJvm/jld.scm 442 */
								BgL_arg1973z00_3201 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(13));
							}
						bgl_display_obj(BgL_arg1973z00_3201, BgL_port1284z00_3200);
					}
					bgl_display_string(BGl_string2217z00zzsaw_jvm_ldz00,
						BgL_port1284z00_3200);
					bgl_display_obj(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
							(14)), BgL_port1284z00_3200);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1284z00_3200);
					bgl_display_obj(BGl_za2jvmzd2optionsza2zd2zzengine_paramz00,
						BgL_port1284z00_3200);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1284z00_3200);
					bgl_display_obj(BGl_z62generatezd2jvmzd2envz62zzsaw_jvm_ldz00(),
						BgL_port1284z00_3200);
					bgl_display_obj(BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00
						(BgL_mainzd2classzd2_3118, FILE_SEPARATOR,
							(char) (((unsigned char) '.'))), BgL_port1284z00_3200);
					bgl_display_string(BGl_string2218z00zzsaw_jvm_ldz00,
						BgL_port1284z00_3200);
					return bgl_display_char(((unsigned char) 10), BgL_port1284z00_3200);
		}}}

	}



/* &<@anonymous:1930> */
	obj_t BGl_z62zc3z04anonymousza31930ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3119)
	{
		{	/* SawJvm/jld.scm 399 */
			{	/* SawJvm/jld.scm 400 */
				obj_t BgL_targetz00_3120;

				BgL_targetz00_3120 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3119, (int) (0L)));
				{	/* SawJvm/jld.scm 400 */
					obj_t BgL_port1272z00_3202;

					{	/* SawJvm/jld.scm 400 */
						obj_t BgL_tmpz00_4129;

						BgL_tmpz00_4129 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1272z00_3202 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4129);
					}
					bgl_display_string(BGl_string2210z00zzsaw_jvm_ldz00,
						BgL_port1272z00_3202);
					bgl_display_char(((unsigned char) 10), BgL_port1272z00_3202);
				}
				{	/* SawJvm/jld.scm 401 */
					obj_t BgL_arg1931z00_3203;

					{	/* SawJvm/jld.scm 401 */
						obj_t BgL_tmpz00_4134;

						BgL_tmpz00_4134 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1931z00_3203 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4134);
					}
					bgl_display_char(((unsigned char) 10), BgL_arg1931z00_3203);
				}
				{	/* SawJvm/jld.scm 402 */
					obj_t BgL_port1273z00_3204;

					{	/* SawJvm/jld.scm 402 */
						obj_t BgL_tmpz00_4138;

						BgL_tmpz00_4138 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1273z00_3204 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4138);
					}
					bgl_display_string(BGl_string2219z00zzsaw_jvm_ldz00,
						BgL_port1273z00_3204);
					{	/* SawJvm/jld.scm 404 */
						obj_t BgL_arg1932z00_3205;

						{	/* SawJvm/jld.scm 404 */
							obj_t BgL_arg1933z00_3206;

							{	/* SawJvm/jld.scm 404 */
								obj_t BgL_arg1934z00_3207;

								BgL_arg1934z00_3207 =
									MAKE_YOUNG_PAIR(BGl_string2213z00zzsaw_jvm_ldz00, BNIL);
								BgL_arg1933z00_3206 =
									MAKE_YOUNG_PAIR(BGl_za2jvmzd2classpathza2zd2zzengine_paramz00,
									BgL_arg1934z00_3207);
							}
							BgL_arg1932z00_3205 =
								BGl_listzd2ze3shzd2pathzd2stringz31zzsaw_jvm_ldz00
								(BgL_arg1933z00_3206);
						}
						bgl_display_obj(BgL_arg1932z00_3205, BgL_port1273z00_3204);
					}
					bgl_display_string(BGl_string2220z00zzsaw_jvm_ldz00,
						BgL_port1273z00_3204);
					bgl_display_char(((unsigned char) 10), BgL_port1273z00_3204);
				}
				{	/* SawJvm/jld.scm 406 */
					obj_t BgL_port1274z00_3208;

					{	/* SawJvm/jld.scm 406 */
						obj_t BgL_tmpz00_4148;

						BgL_tmpz00_4148 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1274z00_3208 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4148);
					}
					bgl_display_string(BGl_string2215z00zzsaw_jvm_ldz00,
						BgL_port1274z00_3208);
					bgl_display_char(((unsigned char) 10), BgL_port1274z00_3208);
				}
				{	/* SawJvm/jld.scm 407 */
					obj_t BgL_arg1935z00_3209;

					{	/* SawJvm/jld.scm 407 */
						obj_t BgL_tmpz00_4153;

						BgL_tmpz00_4153 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1935z00_3209 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4153);
					}
					bgl_display_char(((unsigned char) 10), BgL_arg1935z00_3209);
				}
				{	/* SawJvm/jld.scm 408 */
					obj_t BgL_port1275z00_3210;

					{	/* SawJvm/jld.scm 408 */
						obj_t BgL_tmpz00_4157;

						BgL_tmpz00_4157 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1275z00_3210 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4157);
					}
					bgl_display_string(BGl_string2216z00zzsaw_jvm_ldz00,
						BgL_port1275z00_3210);
					bgl_display_obj(BGl_za2jvmzd2javaza2zd2zzengine_paramz00,
						BgL_port1275z00_3210);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1275z00_3210);
					{	/* SawJvm/jld.scm 409 */
						obj_t BgL_arg1936z00_3211;

						if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
							{	/* SawJvm/jld.scm 409 */
								BgL_arg1936z00_3211 = BGl_string2184z00zzsaw_jvm_ldz00;
							}
						else
							{	/* SawJvm/jld.scm 409 */
								BgL_arg1936z00_3211 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(13));
							}
						bgl_display_obj(BgL_arg1936z00_3211, BgL_port1275z00_3210);
					}
					bgl_display_string(BGl_string2217z00zzsaw_jvm_ldz00,
						BgL_port1275z00_3210);
					bgl_display_obj(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
							(14)), BgL_port1275z00_3210);
					bgl_display_string(BGl_string2221z00zzsaw_jvm_ldz00,
						BgL_port1275z00_3210);
					bgl_display_obj(BGl_za2jvmzd2optionsza2zd2zzengine_paramz00,
						BgL_port1275z00_3210);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1275z00_3210);
					bgl_display_obj(BGl_z62generatezd2jvmzd2envz62zzsaw_jvm_ldz00(),
						BgL_port1275z00_3210);
					{	/* SawJvm/jld.scm 414 */
						obj_t BgL_arg1939z00_3212;

						{	/* SawJvm/jld.scm 414 */
							bool_t BgL_test2423z00_4177;

							{	/* SawJvm/jld.scm 414 */
								obj_t BgL_arg1946z00_3213;

								BgL_arg1946z00_3213 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(15));
								{	/* SawJvm/jld.scm 414 */
									long BgL_l1z00_3214;

									BgL_l1z00_3214 = STRING_LENGTH(((obj_t) BgL_arg1946z00_3213));
									if ((BgL_l1z00_3214 == 0L))
										{	/* SawJvm/jld.scm 414 */
											int BgL_arg1282z00_3215;

											{	/* SawJvm/jld.scm 414 */
												char *BgL_auxz00_4187;
												char *BgL_tmpz00_4184;

												BgL_auxz00_4187 =
													BSTRING_TO_STRING(BGl_string2184z00zzsaw_jvm_ldz00);
												BgL_tmpz00_4184 =
													BSTRING_TO_STRING(((obj_t) BgL_arg1946z00_3213));
												BgL_arg1282z00_3215 =
													memcmp(BgL_tmpz00_4184, BgL_auxz00_4187,
													BgL_l1z00_3214);
											}
											BgL_test2423z00_4177 =
												((long) (BgL_arg1282z00_3215) == 0L);
										}
									else
										{	/* SawJvm/jld.scm 414 */
											BgL_test2423z00_4177 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2423z00_4177)
								{	/* SawJvm/jld.scm 414 */
									BgL_arg1939z00_3212 =
										BGl_prefixz00zz__osz00(BgL_targetz00_3120);
								}
							else
								{	/* SawJvm/jld.scm 414 */
									BgL_arg1939z00_3212 =
										BGl_makezd2filezd2namez00zz__osz00(string_append_3
										(BGl_string2222z00zzsaw_jvm_ldz00,
											BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(15)), BGl_string2223z00zzsaw_jvm_ldz00),
										BGl_prefixz00zz__osz00(BGl_basenamez00zz__osz00
											(BgL_targetz00_3120)));
								}
						}
						bgl_display_obj(BgL_arg1939z00_3212, BgL_port1275z00_3210);
					}
					bgl_display_string(BGl_string2224z00zzsaw_jvm_ldz00,
						BgL_port1275z00_3210);
					return bgl_display_char(((unsigned char) 10), BgL_port1275z00_3210);
		}}}

	}



/* generate-jvm-msdos-script */
	bool_t BGl_generatezd2jvmzd2msdoszd2scriptzd2zzsaw_jvm_ldz00(obj_t
		BgL_targetz00_29, obj_t BgL_mainzd2classzd2_30, obj_t BgL_jarnamez00_31,
		obj_t BgL_za7ipsza7_32)
	{
		{	/* SawJvm/jld.scm 458 */
			{

				if (CBOOL(BGl_za2jvmzd2jarzf3za2z21zzengine_paramz00))
					{	/* SawJvm/jld.scm 523 */
						{	/* SawJvm/jld.scm 470 */
							obj_t BgL_zc3z04anonymousza31996ze3z87_3123;

							BgL_zc3z04anonymousza31996ze3z87_3123 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31996ze3ze5zzsaw_jvm_ldz00, (int) (0L),
								(int) (3L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31996ze3z87_3123, (int) (0L),
								BgL_jarnamez00_31);
							PROCEDURE_SET(BgL_zc3z04anonymousza31996ze3z87_3123, (int) (1L),
								BgL_za7ipsza7_32);
							PROCEDURE_SET(BgL_zc3z04anonymousza31996ze3z87_3123, (int) (2L),
								BgL_mainzd2classzd2_30);
							BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
								(BgL_targetz00_29, BgL_zc3z04anonymousza31996ze3z87_3123);
						}
						{	/* SawJvm/jld.scm 495 */
							obj_t BgL_list2025z00_2345;

							{	/* SawJvm/jld.scm 495 */
								obj_t BgL_arg2026z00_2346;

								{	/* SawJvm/jld.scm 495 */
									obj_t BgL_arg2027z00_2347;

									BgL_arg2027z00_2347 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BNIL);
									BgL_arg2026z00_2346 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg2027z00_2347);
								}
								BgL_list2025z00_2345 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_arg2026z00_2346);
							}
							return
								BGl_chmodz00zz__osz00(BgL_targetz00_29, BgL_list2025z00_2345);
						}
					}
				else
					{	/* SawJvm/jld.scm 523 */
						{	/* SawJvm/jld.scm 499 */
							obj_t BgL_zc3z04anonymousza32030ze3z87_3122;

							BgL_zc3z04anonymousza32030ze3z87_3122 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza32030ze3ze5zzsaw_jvm_ldz00, (int) (0L),
								(int) (2L));
							PROCEDURE_SET(BgL_zc3z04anonymousza32030ze3z87_3122, (int) (0L),
								BgL_za7ipsza7_32);
							PROCEDURE_SET(BgL_zc3z04anonymousza32030ze3z87_3122, (int) (1L),
								BgL_mainzd2classzd2_30);
							BGl_withzd2outputzd2tozd2filezd2zz__r4_ports_6_10_1z00
								(BgL_targetz00_29, BgL_zc3z04anonymousza32030ze3z87_3122);
						}
						{	/* SawJvm/jld.scm 522 */
							obj_t BgL_list2057z00_2382;

							{	/* SawJvm/jld.scm 522 */
								obj_t BgL_arg2058z00_2383;

								{	/* SawJvm/jld.scm 522 */
									obj_t BgL_arg2059z00_2384;

									BgL_arg2059z00_2384 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BNIL);
									BgL_arg2058z00_2383 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BgL_arg2059z00_2384);
								}
								BgL_list2057z00_2382 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_arg2058z00_2383);
							}
							return
								BGl_chmodz00zz__osz00(BgL_targetz00_29, BgL_list2057z00_2382);
						}
					}
			}
		}

	}



/* &generate-jvm-env2161 */
	obj_t BGl_z62generatezd2jvmzd2env2161z62zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 466 */
			{
				obj_t BgL_envz00_2291;
				obj_t BgL_resz00_2292;

				BgL_envz00_2291 = BGl_za2jvmzd2envza2zd2zzengine_paramz00;
				BgL_resz00_2292 = BGl_string2184z00zzsaw_jvm_ldz00;
			BgL_zc3z04anonymousza31982ze3z87_2293:
				if (NULLP(BgL_envz00_2291))
					{	/* SawJvm/jld.scm 462 */
						return BgL_resz00_2292;
					}
				else
					{	/* SawJvm/jld.scm 464 */
						obj_t BgL_arg1984z00_2295;
						obj_t BgL_arg1985z00_2296;

						BgL_arg1984z00_2295 = CDR(((obj_t) BgL_envz00_2291));
						{	/* SawJvm/jld.scm 465 */
							obj_t BgL_arg1986z00_2297;
							obj_t BgL_arg1987z00_2298;

							BgL_arg1986z00_2297 = CAR(((obj_t) BgL_envz00_2291));
							BgL_arg1987z00_2298 = CAR(((obj_t) BgL_envz00_2291));
							{	/* SawJvm/jld.scm 465 */
								obj_t BgL_list1988z00_2299;

								{	/* SawJvm/jld.scm 465 */
									obj_t BgL_arg1989z00_2300;

									{	/* SawJvm/jld.scm 465 */
										obj_t BgL_arg1990z00_2301;

										{	/* SawJvm/jld.scm 465 */
											obj_t BgL_arg1991z00_2302;

											{	/* SawJvm/jld.scm 465 */
												obj_t BgL_arg1992z00_2303;

												{	/* SawJvm/jld.scm 465 */
													obj_t BgL_arg1993z00_2304;

													BgL_arg1993z00_2304 =
														MAKE_YOUNG_PAIR(BgL_resz00_2292, BNIL);
													BgL_arg1992z00_2303 =
														MAKE_YOUNG_PAIR(BGl_string2186z00zzsaw_jvm_ldz00,
														BgL_arg1993z00_2304);
												}
												BgL_arg1991z00_2302 =
													MAKE_YOUNG_PAIR(BgL_arg1987z00_2298,
													BgL_arg1992z00_2303);
											}
											BgL_arg1990z00_2301 =
												MAKE_YOUNG_PAIR(BGl_string2208z00zzsaw_jvm_ldz00,
												BgL_arg1991z00_2302);
										}
										BgL_arg1989z00_2300 =
											MAKE_YOUNG_PAIR(BgL_arg1986z00_2297, BgL_arg1990z00_2301);
									}
									BgL_list1988z00_2299 =
										MAKE_YOUNG_PAIR(BGl_string2209z00zzsaw_jvm_ldz00,
										BgL_arg1989z00_2300);
								}
								BgL_arg1985z00_2296 =
									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
									(BgL_list1988z00_2299);
							}
						}
						{
							obj_t BgL_resz00_4252;
							obj_t BgL_envz00_4251;

							BgL_envz00_4251 = BgL_arg1984z00_2295;
							BgL_resz00_4252 = BgL_arg1985z00_2296;
							BgL_resz00_2292 = BgL_resz00_4252;
							BgL_envz00_2291 = BgL_envz00_4251;
							goto BgL_zc3z04anonymousza31982ze3z87_2293;
						}
					}
			}
		}

	}



/* &<@anonymous:2030> */
	obj_t BGl_z62zc3z04anonymousza32030ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3124)
	{
		{	/* SawJvm/jld.scm 498 */
			{	/* SawJvm/jld.scm 499 */
				obj_t BgL_za7ipsza7_3125;
				obj_t BgL_mainzd2classzd2_3126;

				BgL_za7ipsza7_3125 = PROCEDURE_REF(BgL_envz00_3124, (int) (0L));
				BgL_mainzd2classzd2_3126 = PROCEDURE_REF(BgL_envz00_3124, (int) (1L));
				{	/* SawJvm/jld.scm 499 */
					obj_t BgL_port1296z00_3216;

					{	/* SawJvm/jld.scm 499 */
						obj_t BgL_tmpz00_4257;

						BgL_tmpz00_4257 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1296z00_3216 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4257);
					}
					bgl_display_string(BGl_string2225z00zzsaw_jvm_ldz00,
						BgL_port1296z00_3216);
					bgl_display_obj(BGl_za2jvmzd2javaza2zd2zzengine_paramz00,
						BgL_port1296z00_3216);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1296z00_3216);
					bgl_display_obj(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
							(14)), BgL_port1296z00_3216);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1296z00_3216);
					{	/* SawJvm/jld.scm 501 */
						obj_t BgL_arg2033z00_3217;

						if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
							{	/* SawJvm/jld.scm 501 */
								BgL_arg2033z00_3217 = BGl_string2184z00zzsaw_jvm_ldz00;
							}
						else
							{	/* SawJvm/jld.scm 501 */
								BgL_arg2033z00_3217 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(13));
							}
						bgl_display_obj(BgL_arg2033z00_3217, BgL_port1296z00_3216);
					}
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1296z00_3216);
					bgl_display_obj(BGl_za2jvmzd2optionsza2zd2zzengine_paramz00,
						BgL_port1296z00_3216);
					bgl_display_string(BGl_string2226z00zzsaw_jvm_ldz00,
						BgL_port1296z00_3216);
					{	/* SawJvm/jld.scm 509 */
						obj_t BgL_arg2034z00_3218;

						{	/* SawJvm/jld.scm 509 */
							obj_t BgL_arg2036z00_3219;

							{	/* SawJvm/jld.scm 509 */
								obj_t BgL_arg2037z00_3220;

								{	/* SawJvm/jld.scm 509 */
									obj_t BgL_arg2038z00_3221;

									{	/* SawJvm/jld.scm 509 */
										obj_t BgL_arg2039z00_3222;
										obj_t BgL_arg2040z00_3223;

										{	/* SawJvm/jld.scm 509 */
											obj_t BgL_arg2041z00_3224;
											obj_t BgL_arg2042z00_3225;

											BgL_arg2041z00_3224 =
												BGl_stringzd2replacezd2zz__r4_strings_6_7z00
												(BGl_jvmzd2bigloozd2classpathz00zzsaw_jvm_ldz00(),
												(char) (((unsigned char) '/')),
												(char) (((unsigned char) '\\')));
											if (CBOOL(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
												{	/* SawJvm/jld.scm 511 */
													BgL_arg2042z00_3225 =
														BGl_string2227z00zzsaw_jvm_ldz00;
												}
											else
												{	/* SawJvm/jld.scm 511 */
													BgL_arg2042z00_3225 =
														BGl_string2228z00zzsaw_jvm_ldz00;
												}
											BgL_arg2039z00_3222 =
												string_append(BgL_arg2041z00_3224, BgL_arg2042z00_3225);
										}
										{	/* SawJvm/jld.scm 514 */
											obj_t BgL_arg2045z00_3226;

											if (NULLP(BgL_za7ipsza7_3125))
												{	/* SawJvm/jld.scm 514 */
													BgL_arg2045z00_3226 = BNIL;
												}
											else
												{	/* SawJvm/jld.scm 514 */
													obj_t BgL_head1293z00_3227;

													BgL_head1293z00_3227 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													{
														obj_t BgL_l1291z00_3229;
														obj_t BgL_tail1294z00_3230;

														BgL_l1291z00_3229 = BgL_za7ipsza7_3125;
														BgL_tail1294z00_3230 = BgL_head1293z00_3227;
													BgL_zc3z04anonymousza32047ze3z87_3228:
														if (NULLP(BgL_l1291z00_3229))
															{	/* SawJvm/jld.scm 514 */
																BgL_arg2045z00_3226 = CDR(BgL_head1293z00_3227);
															}
														else
															{	/* SawJvm/jld.scm 514 */
																obj_t BgL_newtail1295z00_3231;

																{	/* SawJvm/jld.scm 514 */
																	obj_t BgL_arg2050z00_3232;

																	{	/* SawJvm/jld.scm 514 */
																		obj_t BgL_xz00_3233;

																		BgL_xz00_3233 =
																			CAR(((obj_t) BgL_l1291z00_3229));
																		BgL_arg2050z00_3232 =
																			BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00
																			(BGl_userzd2libraryzd2zzsaw_jvm_ldz00
																			(BgL_xz00_3233),
																			(char) (((unsigned char) '/')),
																			(char) (((unsigned char) '\\')));
																	}
																	BgL_newtail1295z00_3231 =
																		MAKE_YOUNG_PAIR(BgL_arg2050z00_3232, BNIL);
																}
																SET_CDR(BgL_tail1294z00_3230,
																	BgL_newtail1295z00_3231);
																{	/* SawJvm/jld.scm 514 */
																	obj_t BgL_arg2049z00_3234;

																	BgL_arg2049z00_3234 =
																		CDR(((obj_t) BgL_l1291z00_3229));
																	{
																		obj_t BgL_tail1294z00_4299;
																		obj_t BgL_l1291z00_4298;

																		BgL_l1291z00_4298 = BgL_arg2049z00_3234;
																		BgL_tail1294z00_4299 =
																			BgL_newtail1295z00_3231;
																		BgL_tail1294z00_3230 = BgL_tail1294z00_4299;
																		BgL_l1291z00_3229 = BgL_l1291z00_4298;
																		goto BgL_zc3z04anonymousza32047ze3z87_3228;
																	}
																}
															}
													}
												}
											BgL_arg2040z00_3223 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg2045z00_3226, BNIL);
										}
										BgL_arg2038z00_3221 =
											MAKE_YOUNG_PAIR(BgL_arg2039z00_3222, BgL_arg2040z00_3223);
									}
									BgL_arg2037z00_3220 =
										MAKE_YOUNG_PAIR(BGl_string2229z00zzsaw_jvm_ldz00,
										BgL_arg2038z00_3221);
								}
								BgL_arg2036z00_3219 =
									MAKE_YOUNG_PAIR(BGl_za2jvmzd2classpathza2zd2zzengine_paramz00,
									BgL_arg2037z00_3220);
							}
							BgL_arg2034z00_3218 =
								BGl_listzd2ze3msdoszd2pathzd2stringz31zzsaw_jvm_ldz00
								(BgL_arg2036z00_3219);
						}
						bgl_display_obj(BgL_arg2034z00_3218, BgL_port1296z00_3216);
					}
					bgl_display_string(BGl_string2230z00zzsaw_jvm_ldz00,
						BgL_port1296z00_3216);
					bgl_display_obj(BGl_z62generatezd2jvmzd2env2161z62zzsaw_jvm_ldz00(),
						BgL_port1296z00_3216);
					bgl_display_string(BGl_string2231z00zzsaw_jvm_ldz00,
						BgL_port1296z00_3216);
					bgl_display_obj(BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00
						(BgL_mainzd2classzd2_3126, FILE_SEPARATOR,
							(char) (((unsigned char) '.'))), BgL_port1296z00_3216);
					bgl_display_string(BGl_string2232z00zzsaw_jvm_ldz00,
						BgL_port1296z00_3216);
					return bgl_display_char(((unsigned char) 10), BgL_port1296z00_3216);
		}}}

	}



/* &<@anonymous:1996> */
	obj_t BGl_z62zc3z04anonymousza31996ze3ze5zzsaw_jvm_ldz00(obj_t
		BgL_envz00_3127)
	{
		{	/* SawJvm/jld.scm 469 */
			{	/* SawJvm/jld.scm 470 */
				obj_t BgL_jarnamez00_3128;
				obj_t BgL_za7ipsza7_3129;
				obj_t BgL_mainzd2classzd2_3130;

				BgL_jarnamez00_3128 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3127, (int) (0L)));
				BgL_za7ipsza7_3129 = PROCEDURE_REF(BgL_envz00_3127, (int) (1L));
				BgL_mainzd2classzd2_3130 = PROCEDURE_REF(BgL_envz00_3127, (int) (2L));
				{	/* SawJvm/jld.scm 470 */
					obj_t BgL_port1290z00_3235;

					{	/* SawJvm/jld.scm 470 */
						obj_t BgL_tmpz00_4322;

						BgL_tmpz00_4322 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_port1290z00_3235 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4322);
					}
					bgl_display_string(BGl_string2225z00zzsaw_jvm_ldz00,
						BgL_port1290z00_3235);
					bgl_display_obj(BGl_za2jvmzd2javaza2zd2zzengine_paramz00,
						BgL_port1290z00_3235);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1290z00_3235);
					bgl_display_obj(BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
							(14)), BgL_port1290z00_3235);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1290z00_3235);
					{	/* SawJvm/jld.scm 472 */
						obj_t BgL_arg1998z00_3236;

						if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
							{	/* SawJvm/jld.scm 472 */
								BgL_arg1998z00_3236 = BGl_string2184z00zzsaw_jvm_ldz00;
							}
						else
							{	/* SawJvm/jld.scm 472 */
								BgL_arg1998z00_3236 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(13));
							}
						bgl_display_obj(BgL_arg1998z00_3236, BgL_port1290z00_3235);
					}
					bgl_display_string(BGl_string2226z00zzsaw_jvm_ldz00,
						BgL_port1290z00_3235);
					{	/* SawJvm/jld.scm 477 */
						obj_t BgL_arg1999z00_3237;

						{	/* SawJvm/jld.scm 477 */
							obj_t BgL_arg2000z00_3238;

							{	/* SawJvm/jld.scm 477 */
								obj_t BgL_arg2001z00_3239;

								{	/* SawJvm/jld.scm 477 */
									obj_t BgL_arg2002z00_3240;

									{	/* SawJvm/jld.scm 477 */
										obj_t BgL_arg2003z00_3241;
										obj_t BgL_arg2004z00_3242;

										{	/* SawJvm/jld.scm 477 */
											obj_t BgL_arg2006z00_3243;
											obj_t BgL_arg2007z00_3244;

											{	/* SawJvm/jld.scm 477 */
												obj_t BgL_arg2008z00_3245;

												BgL_arg2008z00_3245 =
													BGl_dirnamez00zz__osz00(BgL_jarnamez00_3128);
												if (STRINGP
													(BGl_za2jvmzd2jarpathza2zd2zzengine_paramz00))
													{	/* SawJvm/jld.scm 297 */
														BgL_arg2006z00_3243 =
															BGl_za2jvmzd2jarpathza2zd2zzengine_paramz00;
													}
												else
													{	/* SawJvm/jld.scm 297 */
														BgL_arg2006z00_3243 = BgL_arg2008z00_3245;
													}
											}
											BgL_arg2007z00_3244 =
												BGl_basenamez00zz__osz00(BgL_jarnamez00_3128);
											BgL_arg2003z00_3241 =
												string_append_3(BgL_arg2006z00_3243,
												BGl_string2233z00zzsaw_jvm_ldz00, BgL_arg2007z00_3244);
										}
										{	/* SawJvm/jld.scm 481 */
											obj_t BgL_arg2009z00_3246;
											obj_t BgL_arg2010z00_3247;

											{	/* SawJvm/jld.scm 481 */
												obj_t BgL_arg2011z00_3248;
												obj_t BgL_arg2012z00_3249;

												BgL_arg2011z00_3248 =
													BGl_stringzd2replacezd2zz__r4_strings_6_7z00
													(BGl_jvmzd2bigloozd2classpathz00zzsaw_jvm_ldz00(),
													(char) (((unsigned char) '/')),
													(char) (((unsigned char) '\\')));
												if (CBOOL
													(BGl_za2unsafezd2libraryza2zd2zzengine_paramz00))
													{	/* SawJvm/jld.scm 483 */
														BgL_arg2012z00_3249 =
															BGl_string2227z00zzsaw_jvm_ldz00;
													}
												else
													{	/* SawJvm/jld.scm 483 */
														BgL_arg2012z00_3249 =
															BGl_string2228z00zzsaw_jvm_ldz00;
													}
												BgL_arg2009z00_3246 =
													string_append(BgL_arg2011z00_3248,
													BgL_arg2012z00_3249);
											}
											{	/* SawJvm/jld.scm 486 */
												obj_t BgL_arg2014z00_3250;

												if (NULLP(BgL_za7ipsza7_3129))
													{	/* SawJvm/jld.scm 486 */
														BgL_arg2014z00_3250 = BNIL;
													}
												else
													{	/* SawJvm/jld.scm 486 */
														obj_t BgL_head1287z00_3251;

														BgL_head1287z00_3251 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1285z00_3253;
															obj_t BgL_tail1288z00_3254;

															BgL_l1285z00_3253 = BgL_za7ipsza7_3129;
															BgL_tail1288z00_3254 = BgL_head1287z00_3251;
														BgL_zc3z04anonymousza32016ze3z87_3252:
															if (NULLP(BgL_l1285z00_3253))
																{	/* SawJvm/jld.scm 486 */
																	BgL_arg2014z00_3250 =
																		CDR(BgL_head1287z00_3251);
																}
															else
																{	/* SawJvm/jld.scm 486 */
																	obj_t BgL_newtail1289z00_3255;

																	{	/* SawJvm/jld.scm 486 */
																		obj_t BgL_arg2019z00_3256;

																		{	/* SawJvm/jld.scm 486 */
																			obj_t BgL_xz00_3257;

																			BgL_xz00_3257 =
																				CAR(((obj_t) BgL_l1285z00_3253));
																			BgL_arg2019z00_3256 =
																				BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00
																				(BGl_userzd2libraryzd2zzsaw_jvm_ldz00
																				(BgL_xz00_3257),
																				(char) (((unsigned char) '/')),
																				(char) (((unsigned char) '\\')));
																		}
																		BgL_newtail1289z00_3255 =
																			MAKE_YOUNG_PAIR(BgL_arg2019z00_3256,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1288z00_3254,
																		BgL_newtail1289z00_3255);
																	{	/* SawJvm/jld.scm 486 */
																		obj_t BgL_arg2018z00_3258;

																		BgL_arg2018z00_3258 =
																			CDR(((obj_t) BgL_l1285z00_3253));
																		{
																			obj_t BgL_tail1288z00_4367;
																			obj_t BgL_l1285z00_4366;

																			BgL_l1285z00_4366 = BgL_arg2018z00_3258;
																			BgL_tail1288z00_4367 =
																				BgL_newtail1289z00_3255;
																			BgL_tail1288z00_3254 =
																				BgL_tail1288z00_4367;
																			BgL_l1285z00_3253 = BgL_l1285z00_4366;
																			goto
																				BgL_zc3z04anonymousza32016ze3z87_3252;
																		}
																	}
																}
														}
													}
												BgL_arg2010z00_3247 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg2014z00_3250, BNIL);
											}
											BgL_arg2004z00_3242 =
												MAKE_YOUNG_PAIR(BgL_arg2009z00_3246,
												BgL_arg2010z00_3247);
										}
										BgL_arg2002z00_3240 =
											MAKE_YOUNG_PAIR(BgL_arg2003z00_3241, BgL_arg2004z00_3242);
									}
									BgL_arg2001z00_3239 =
										MAKE_YOUNG_PAIR(BGl_string2229z00zzsaw_jvm_ldz00,
										BgL_arg2002z00_3240);
								}
								BgL_arg2000z00_3238 =
									MAKE_YOUNG_PAIR(BGl_za2jvmzd2classpathza2zd2zzengine_paramz00,
									BgL_arg2001z00_3239);
							}
							BgL_arg1999z00_3237 =
								BGl_listzd2ze3msdoszd2pathzd2stringz31zzsaw_jvm_ldz00
								(BgL_arg2000z00_3238);
						}
						bgl_display_obj(BgL_arg1999z00_3237, BgL_port1290z00_3235);
					}
					bgl_display_string(BGl_string2230z00zzsaw_jvm_ldz00,
						BgL_port1290z00_3235);
					bgl_display_obj(BGl_za2jvmzd2optionsza2zd2zzengine_paramz00,
						BgL_port1290z00_3235);
					bgl_display_string(BGl_string2186z00zzsaw_jvm_ldz00,
						BgL_port1290z00_3235);
					bgl_display_obj(BGl_z62generatezd2jvmzd2env2161z62zzsaw_jvm_ldz00(),
						BgL_port1290z00_3235);
					bgl_display_string(BGl_string2231z00zzsaw_jvm_ldz00,
						BgL_port1290z00_3235);
					bgl_display_obj(BGl_stringzd2replacez12zc0zz__r4_strings_6_7z00
						(BgL_mainzd2classzd2_3130, FILE_SEPARATOR,
							(char) (((unsigned char) '.'))), BgL_port1290z00_3235);
					bgl_display_string(BGl_string2232z00zzsaw_jvm_ldz00,
						BgL_port1290z00_3235);
					return bgl_display_char(((unsigned char) 10), BgL_port1290z00_3235);
		}}}

	}



/* generate-jvm-script */
	bool_t BGl_generatezd2jvmzd2scriptz00zzsaw_jvm_ldz00(obj_t BgL_targetz00_33,
		obj_t BgL_mainzd2classzd2_34, obj_t BgL_jarnamez00_35,
		obj_t BgL_za7ipsza7_36)
	{
		{	/* SawJvm/jld.scm 530 */
			{	/* SawJvm/jld.scm 531 */
				obj_t BgL_list2060z00_2388;

				{	/* SawJvm/jld.scm 531 */
					obj_t BgL_arg2061z00_2389;

					BgL_arg2061z00_2389 =
						MAKE_YOUNG_PAIR(BGl_za2jvmzd2shellza2zd2zzengine_paramz00, BNIL);
					BgL_list2060z00_2388 =
						MAKE_YOUNG_PAIR(BGl_string2234z00zzsaw_jvm_ldz00,
						BgL_arg2061z00_2389);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list2060z00_2388);
			}
			{	/* SawJvm/jld.scm 532 */
				obj_t BgL_list2062z00_2390;

				{	/* SawJvm/jld.scm 532 */
					obj_t BgL_arg2063z00_2391;

					{	/* SawJvm/jld.scm 532 */
						obj_t BgL_arg2064z00_2392;

						BgL_arg2064z00_2392 =
							MAKE_YOUNG_PAIR(BGl_string2164z00zzsaw_jvm_ldz00, BNIL);
						BgL_arg2063z00_2391 =
							MAKE_YOUNG_PAIR(BgL_targetz00_33, BgL_arg2064z00_2392);
					}
					BgL_list2062z00_2390 =
						MAKE_YOUNG_PAIR(BGl_string2165z00zzsaw_jvm_ldz00,
						BgL_arg2063z00_2391);
				}
				BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list2062z00_2390);
			}
			{	/* SawJvm/jld.scm 533 */
				obj_t BgL_list2065z00_2393;

				BgL_list2065z00_2393 =
					MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list2065z00_2393);
			}
			{	/* SawJvm/jld.scm 535 */
				bool_t BgL_test2436z00_4399;

				{	/* SawJvm/jld.scm 535 */
					obj_t BgL_string1z00_3021;

					BgL_string1z00_3021 = BGl_za2jvmzd2shellza2zd2zzengine_paramz00;
					{	/* SawJvm/jld.scm 535 */
						long BgL_l1z00_3023;

						BgL_l1z00_3023 = STRING_LENGTH(BgL_string1z00_3021);
						if ((BgL_l1z00_3023 == 2L))
							{	/* SawJvm/jld.scm 535 */
								int BgL_arg1282z00_3026;

								{	/* SawJvm/jld.scm 535 */
									char *BgL_auxz00_4405;
									char *BgL_tmpz00_4403;

									BgL_auxz00_4405 =
										BSTRING_TO_STRING(BGl_string2193z00zzsaw_jvm_ldz00);
									BgL_tmpz00_4403 = BSTRING_TO_STRING(BgL_string1z00_3021);
									BgL_arg1282z00_3026 =
										memcmp(BgL_tmpz00_4403, BgL_auxz00_4405, BgL_l1z00_3023);
								}
								BgL_test2436z00_4399 = ((long) (BgL_arg1282z00_3026) == 0L);
							}
						else
							{	/* SawJvm/jld.scm 535 */
								BgL_test2436z00_4399 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2436z00_4399)
					{	/* SawJvm/jld.scm 535 */
						return
							BGl_generatezd2jvmzd2shzd2scriptzd2zzsaw_jvm_ldz00
							(BgL_targetz00_33, BgL_mainzd2classzd2_34, BgL_za7ipsza7_36);
					}
				else
					{	/* SawJvm/jld.scm 537 */
						bool_t BgL_test2438z00_4411;

						{	/* SawJvm/jld.scm 537 */
							obj_t BgL_string1z00_3032;

							BgL_string1z00_3032 = BGl_za2jvmzd2shellza2zd2zzengine_paramz00;
							{	/* SawJvm/jld.scm 537 */
								long BgL_l1z00_3034;

								BgL_l1z00_3034 = STRING_LENGTH(BgL_string1z00_3032);
								if ((BgL_l1z00_3034 == 5L))
									{	/* SawJvm/jld.scm 537 */
										int BgL_arg1282z00_3037;

										{	/* SawJvm/jld.scm 537 */
											char *BgL_auxz00_4417;
											char *BgL_tmpz00_4415;

											BgL_auxz00_4417 =
												BSTRING_TO_STRING(BGl_string2170z00zzsaw_jvm_ldz00);
											BgL_tmpz00_4415 = BSTRING_TO_STRING(BgL_string1z00_3032);
											BgL_arg1282z00_3037 =
												memcmp(BgL_tmpz00_4415, BgL_auxz00_4417,
												BgL_l1z00_3034);
										}
										BgL_test2438z00_4411 = ((long) (BgL_arg1282z00_3037) == 0L);
									}
								else
									{	/* SawJvm/jld.scm 537 */
										BgL_test2438z00_4411 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2438z00_4411)
							{	/* SawJvm/jld.scm 537 */
								BGL_TAIL return
									BGl_generatezd2jvmzd2msdoszd2scriptzd2zzsaw_jvm_ldz00
									(BgL_targetz00_33, BgL_mainzd2classzd2_34, BgL_jarnamez00_35,
									BgL_za7ipsza7_36);
							}
						else
							{	/* SawJvm/jld.scm 537 */
								{	/* SawJvm/jld.scm 540 */
									obj_t BgL_list2068z00_2396;

									{	/* SawJvm/jld.scm 540 */
										obj_t BgL_arg2069z00_2397;

										{	/* SawJvm/jld.scm 540 */
											obj_t BgL_arg2070z00_2398;

											{	/* SawJvm/jld.scm 540 */
												obj_t BgL_arg2072z00_2399;

												BgL_arg2072z00_2399 =
													MAKE_YOUNG_PAIR(BGl_string2194z00zzsaw_jvm_ldz00,
													BNIL);
												BgL_arg2070z00_2398 =
													MAKE_YOUNG_PAIR
													(BGl_za2jvmzd2shellza2zd2zzengine_paramz00,
													BgL_arg2072z00_2399);
											}
											BgL_arg2069z00_2397 =
												MAKE_YOUNG_PAIR(BGl_string2195z00zzsaw_jvm_ldz00,
												BgL_arg2070z00_2398);
										}
										BgL_list2068z00_2396 =
											MAKE_YOUNG_PAIR(BGl_string2235z00zzsaw_jvm_ldz00,
											BgL_arg2069z00_2397);
									}
									BGl_warningz00zz__errorz00(BgL_list2068z00_2396);
								}
								return
									BGl_generatezd2jvmzd2shzd2scriptzd2zzsaw_jvm_ldz00
									(BgL_targetz00_33, BgL_mainzd2classzd2_34, BgL_za7ipsza7_36);
							}
					}
			}
		}

	}



/* user-library */
	obj_t BGl_userzd2libraryzd2zzsaw_jvm_ldz00(obj_t BgL_libz00_37)
	{
		{	/* SawJvm/jld.scm 547 */
			{
				obj_t BgL_libz00_2403;

				{	/* SawJvm/jld.scm 558 */
					bool_t BgL_test2440z00_4429;

					BgL_libz00_2403 = BgL_libz00_37;
					{	/* SawJvm/jld.scm 549 */
						long BgL_lenz00_2405;

						BgL_lenz00_2405 = STRING_LENGTH(BgL_libz00_2403);
						{
							long BgL_iz00_2407;

							BgL_iz00_2407 = 0L;
						BgL_zc3z04anonymousza32076ze3z87_2408:
							if ((BgL_iz00_2407 == BgL_lenz00_2405))
								{	/* SawJvm/jld.scm 552 */
									BgL_test2440z00_4429 = ((bool_t) 0);
								}
							else
								{	/* SawJvm/jld.scm 552 */
									if (
										(STRING_REF(BgL_libz00_2403, BgL_iz00_2407) ==
											(unsigned char) (FILE_SEPARATOR)))
										{	/* SawJvm/jld.scm 554 */
											BgL_test2440z00_4429 = ((bool_t) 1);
										}
									else
										{
											long BgL_iz00_4437;

											BgL_iz00_4437 = (BgL_iz00_2407 + 1L);
											BgL_iz00_2407 = BgL_iz00_4437;
											goto BgL_zc3z04anonymousza32076ze3z87_2408;
										}
								}
						}
					}
					if (BgL_test2440z00_4429)
						{	/* SawJvm/jld.scm 558 */
							return BgL_libz00_37;
						}
					else
						{	/* SawJvm/jld.scm 558 */
							return
								BGl_makezd2filezd2namez00zz__osz00
								(BGl_jvmzd2bigloozd2classpathz00zzsaw_jvm_ldz00(),
								BgL_libz00_37);
						}
				}
			}
		}

	}



/* list->path-string */
	obj_t BGl_listzd2ze3pathzd2stringze3zzsaw_jvm_ldz00(obj_t BgL_pathz00_38,
		obj_t BgL_separatorz00_39)
	{
		{	/* SawJvm/jld.scm 565 */
			{	/* SawJvm/jld.scm 566 */
				obj_t BgL_rpathz00_2418;

				BgL_rpathz00_2418 = bgl_reverse(BgL_pathz00_38);
				{	/* SawJvm/jld.scm 567 */
					obj_t BgL_g1121z00_2419;
					obj_t BgL_g1122z00_2420;

					BgL_g1121z00_2419 = CDR(((obj_t) BgL_rpathz00_2418));
					BgL_g1122z00_2420 = CAR(((obj_t) BgL_rpathz00_2418));
					{
						obj_t BgL_pathz00_2422;
						obj_t BgL_resz00_2423;

						BgL_pathz00_2422 = BgL_g1121z00_2419;
						BgL_resz00_2423 = BgL_g1122z00_2420;
					BgL_zc3z04anonymousza32084ze3z87_2424:
						if (NULLP(BgL_pathz00_2422))
							{	/* SawJvm/jld.scm 569 */
								return BgL_resz00_2423;
							}
						else
							{	/* SawJvm/jld.scm 571 */
								obj_t BgL_arg2086z00_2426;
								obj_t BgL_arg2087z00_2427;

								BgL_arg2086z00_2426 = CDR(((obj_t) BgL_pathz00_2422));
								{	/* SawJvm/jld.scm 571 */
									obj_t BgL_arg2088z00_2428;

									BgL_arg2088z00_2428 = CAR(((obj_t) BgL_pathz00_2422));
									BgL_arg2087z00_2427 =
										string_append_3(BgL_arg2088z00_2428, BgL_separatorz00_39,
										BgL_resz00_2423);
								}
								{
									obj_t BgL_resz00_4454;
									obj_t BgL_pathz00_4453;

									BgL_pathz00_4453 = BgL_arg2086z00_2426;
									BgL_resz00_4454 = BgL_arg2087z00_2427;
									BgL_resz00_2423 = BgL_resz00_4454;
									BgL_pathz00_2422 = BgL_pathz00_4453;
									goto BgL_zc3z04anonymousza32084ze3z87_2424;
								}
							}
					}
				}
			}
		}

	}



/* list->sh-path-string */
	obj_t BGl_listzd2ze3shzd2pathzd2stringz31zzsaw_jvm_ldz00(obj_t BgL_pathz00_40)
	{
		{	/* SawJvm/jld.scm 576 */
			{	/* SawJvm/jld.scm 578 */
				obj_t BgL_arg2089z00_2430;

				if (STRINGP(BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00))
					{	/* SawJvm/jld.scm 578 */
						BgL_arg2089z00_2430 =
							BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00;
					}
				else
					{	/* SawJvm/jld.scm 578 */
						BgL_arg2089z00_2430 = BGl_string2236z00zzsaw_jvm_ldz00;
					}
				return
					BGl_listzd2ze3pathzd2stringze3zzsaw_jvm_ldz00(BgL_pathz00_40,
					BgL_arg2089z00_2430);
			}
		}

	}



/* list->msdos-path-string */
	obj_t BGl_listzd2ze3msdoszd2pathzd2stringz31zzsaw_jvm_ldz00(obj_t
		BgL_pathz00_41)
	{
		{	/* SawJvm/jld.scm 585 */
			{	/* SawJvm/jld.scm 587 */
				obj_t BgL_arg2091z00_2432;

				if (STRINGP(BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00))
					{	/* SawJvm/jld.scm 587 */
						BgL_arg2091z00_2432 =
							BGl_za2jvmzd2pathzd2separatorza2z00zzengine_paramz00;
					}
				else
					{	/* SawJvm/jld.scm 587 */
						BgL_arg2091z00_2432 = BGl_string2237z00zzsaw_jvm_ldz00;
					}
				return
					BGl_listzd2ze3pathzd2stringze3zzsaw_jvm_ldz00(BgL_pathz00_41,
					BgL_arg2091z00_2432);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 16 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 16 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_ldz00(void)
	{
		{	/* SawJvm/jld.scm 16 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzengine_configurez00(272817175L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzengine_linkz00(117219619L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzread_readerz00(95801752L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2238z00zzsaw_jvm_ldz00));
		}

	}

#ifdef __cplusplus
}
#endif
