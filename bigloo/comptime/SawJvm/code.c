/*===========================================================================*/
/*   (SawJvm/code.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawJvm/code.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_JVM_CODE_TYPE_DEFINITIONS
#define BGL_SAW_JVM_CODE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_jvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
		obj_t BgL_qnamez00;
		obj_t BgL_classesz00;
		obj_t BgL_currentzd2classzd2;
		obj_t BgL_declarationsz00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_currentzd2methodzd2;
		obj_t BgL_codez00;
		obj_t BgL_lightzd2funcallszd2;
		obj_t BgL_inlinez00;
	}             *BgL_jvmz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_selectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
	}                    *BgL_rtl_selectz00_bglt;

	typedef struct BgL_rtl_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
		obj_t BgL_labelsz00;
	}                    *BgL_rtl_switchz00_bglt;

	typedef struct BgL_rtl_ifeqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifeqz00_bglt;

	typedef struct BgL_rtl_ifnez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifnez00_bglt;

	typedef struct BgL_rtl_goz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                *BgL_rtl_goz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_loadgz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                   *BgL_rtl_loadgz00_bglt;

	typedef struct BgL_rtl_loadfunz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                     *BgL_rtl_loadfunz00_bglt;

	typedef struct BgL_rtl_globalrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                       *BgL_rtl_globalrefz00_bglt;

	typedef struct BgL_rtl_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_getfieldz00_bglt;

	typedef struct BgL_rtl_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                    *BgL_rtl_vallocz00_bglt;

	typedef struct BgL_rtl_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vrefz00_bglt;

	typedef struct BgL_rtl_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                        *BgL_rtl_instanceofz00_bglt;

	typedef struct BgL_rtl_makeboxz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                     *BgL_rtl_makeboxz00_bglt;

	typedef struct BgL_rtl_storegz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                    *BgL_rtl_storegz00_bglt;

	typedef struct BgL_rtl_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_setfieldz00_bglt;

	typedef struct BgL_rtl_vsetz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vsetz00_bglt;

	typedef struct BgL_rtl_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_constrz00;
	}                 *BgL_rtl_newz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_lightfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_funsz00;
		obj_t BgL_rettypez00;
	}                          *BgL_rtl_lightfuncallz00_bglt;

	typedef struct BgL_rtl_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_totypez00;
		struct BgL_typez00_bgl *BgL_fromtypez00;
	}                  *BgL_rtl_castz00_bglt;

	typedef struct BgL_rtl_cast_nullz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                       *BgL_rtl_cast_nullz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_indexedz00_bgl
	{
		int BgL_indexz00;
	}                 *BgL_indexedz00_bglt;

	typedef struct BgL_lregz00_bgl
	{
		obj_t BgL_idz00;
	}              *BgL_lregz00_bglt;

	typedef struct BgL_liveblockz00_bgl
	{
		obj_t BgL_inz00;
		obj_t BgL_outz00;
	}                   *BgL_liveblockz00_bglt;


#endif													// BGL_SAW_JVM_CODE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static bool_t BGl_zc3z04anonymousza31935ze3ze70z60zzsaw_jvm_codez00(obj_t,
		obj_t);
	static obj_t BGl_za2lastzd2lineza2zd2zzsaw_jvm_codez00 = BUNSPEC;
	static bool_t BGl_fixpointze70ze7zzsaw_jvm_codez00(obj_t, obj_t);
	extern obj_t BGl_rtl_funcallz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2funzd2rtl_boxset1763z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_funz00zzsaw_defsz00;
	extern obj_t BGl_rtl_newz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_genzd2exprzd2zzsaw_jvm_codez00(BgL_jvmz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static BgL_blockz00_bglt BGl_livezd2initzd2zzsaw_jvm_codez00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_jvm_codez00 = BUNSPEC;
	static obj_t
		BGl_genzd2funzd2withzd2argszd2zzsaw_jvm_codez00(BgL_rtl_funz00_bglt, obj_t,
		obj_t);
	static obj_t
		BGl_z62genzd2argszd2genzd2funzd2rtl1716z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62genzd2argszd2genzd2funzd2rtl1718z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_boxref1761z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_genzd2inszd2zzsaw_jvm_codez00(BgL_jvmz00_bglt, obj_t, obj_t);
	static obj_t BGl_z62genzd2argszd2genzd2funzb0zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_boxrefz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2funzd2rtl_instance1753z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t
		BGl_z62genzd2argszd2genzd2funzd2rtl1724z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_pushzd2stringzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	static obj_t BGl_genzd2funzd2zzsaw_jvm_codez00(BgL_rtl_funz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_cast1755z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_storeg1712z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_vrefz00zzsaw_defsz00;
	static obj_t BGl_toplevelzd2initzd2zzsaw_jvm_codez00(void);
	static obj_t BGl_exprze70ze7zzsaw_jvm_codez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2withzd2args1698zb0zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_protectz00zzsaw_defsz00;
	extern obj_t BGl_rtl_ifeqz00zzsaw_defsz00;
	extern obj_t BGl_rtl_boxsetz00zzsaw_defsz00;
	static obj_t
		BGl_z62genzd2argszd2genzd2funzd2rtl1747z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_vlengthz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2funzd2rtl_protecte1771z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32104ze3ze5zzsaw_jvm_codez00(obj_t,
		obj_t);
	extern obj_t BGl_loadzd2fieldzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_typez00_bglt, BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62modulezd2codezb0zzsaw_jvm_codez00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_rtl_makeboxz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_jvm_codez00(void);
	static obj_t BGl_regzd2ze3lregze70zd6zzsaw_jvm_codez00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62genzd2argszd2genzd2funzd2rtl1759z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_vsetz00zzsaw_defsz00;
	static obj_t BGl_objectzd2initzd2zzsaw_jvm_codez00(void);
	static obj_t BGl_z62genzd2funzd2rtl_cast_nul1757z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2zf2zf2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_fail1767z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static bool_t BGl_za2hasprotectza2z00zzsaw_jvm_codez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_rtl_lightfuncallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	extern obj_t BGl_rtl_vallocz00zzsaw_defsz00;
	extern obj_t BGl_rtl_cast_nullz00zzsaw_defsz00;
	extern obj_t BGl_compilezd2typezd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_rtl_loadfunz00zzsaw_defsz00;
	static obj_t BGl_livezd2resetzd2zzsaw_jvm_codez00(BgL_jvmz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	static obj_t BGl_getzd2localszd2zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_mov1705z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	static obj_t BGl_methodzd2initzd2zzsaw_jvm_codez00(void);
	static obj_t BGl_livezd2argze70z35zzsaw_jvm_codez00(obj_t, obj_t);
	extern obj_t BGl_rtl_jumpexitz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2funzd2rtl_apply1737z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_outzd2linezd2zzsaw_jvm_codez00(obj_t, BgL_rtl_funz00_bglt);
	extern obj_t BGl_labelz00zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	extern obj_t BGl_rtl_switchz00zzsaw_defsz00;
	extern obj_t BGl_rtl_goz00zzsaw_defsz00;
	extern obj_t BGl_branchz00zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t, obj_t);
	extern obj_t BGl_rtl_ifnez00zzsaw_defsz00;
	static obj_t BGl_storezd2regzd2zzsaw_jvm_codez00(BgL_jvmz00_bglt, obj_t);
	static obj_t BGl_z62genzd2funzd2withzd2argszb0zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_vlength1743z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_instanceofz00zzsaw_defsz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_rtl_setfieldz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2funzd2rtl_vset1741z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_callzd2globalzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	static obj_t BGl_z62genzd2funzd2rtl_vref1739z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_pushzd2numzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_callz00zzsaw_defsz00;
	static obj_t
		BGl_genzd2argszd2genzd2predicatezd2zzsaw_jvm_codez00(BgL_rtl_funz00_bglt,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_loadg1710z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	extern obj_t
		BGl_inlinezd2callzd2withzd2argszf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2102z62zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32082ze3ze5zzsaw_jvm_codez00(obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2105z62zzsaw_jvm_codez00(obj_t, obj_t);
	static bool_t BGl_za2protectresultza2z00zzsaw_jvm_codez00;
	static obj_t BGl_livezd2argsze70z35zzsaw_jvm_codez00(obj_t, obj_t);
	static bool_t BGl_livezd2fixzd2zzsaw_jvm_codez00(obj_t);
	extern obj_t BGl_rtl_storegz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2argszd2genzd2fun1696zb0zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2112z62zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2113z62zzsaw_jvm_codez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_declarezd2globalzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	static bool_t BGl_includeze70ze7zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2117z62zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2118z62zzsaw_jvm_codez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_jumpexit1765z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_storezd2fieldzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt,
		BgL_typez00_bglt, BgL_typez00_bglt, obj_t);
	extern obj_t BGl_rtl_lastz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2funzd2rtl_valloc1745z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_livezd2instrsze70z35zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32181ze3ze5zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_returnz00zzsaw_defsz00;
	static obj_t BGl_check_funze70ze7zzsaw_jvm_codez00(obj_t);
	extern obj_t BGl_rtl_applyz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_codez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzjas_asz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_inlinez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_funcallz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_outz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_namesz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__ucs2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_protect1769z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_intifyz00zzsaw_jvm_codez00(obj_t);
	extern obj_t BGl_za2jvmzd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_z62genzd2funzb0zzsaw_jvm_codez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2argszd2genzd2predica1772zb0zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2argszd2genzd2predica1775zb0zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_flatz00zzsaw_jvm_codez00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_loadfun1735z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	extern obj_t BGl_rtl_loadgz00zzsaw_defsz00;
	static obj_t BGl_Lze70ze7zzsaw_jvm_codez00(obj_t);
	extern obj_t BGl_rtl_loadiz00zzsaw_defsz00;
	extern obj_t BGl_codez12z12zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	extern obj_t BGl_rtl_notseqz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2funzd2rtl_getfield1749z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_failz00zzsaw_defsz00;
	static BgL_rtl_regz00_bglt BGl_z62lambda2077z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_jvm_codez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_codez00(void);
	extern obj_t BGl_inlinezd2predicatezf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt, obj_t, obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_lightfun1728z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_lregz00zzsaw_jvm_codez00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_codez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_codez00(void);
	static BgL_rtl_regz00_bglt BGl_z62lambda2080z62zzsaw_jvm_codez00(obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda2083z62zzsaw_jvm_codez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_remq(obj_t, obj_t);
	extern obj_t BGl_rtl_castz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2fun1700zb0zzsaw_jvm_codez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_nop1703z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62genzd2exprzb0zzsaw_jvm_codez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_loadi1708z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2091z62zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32267ze3ze5zzsaw_jvm_codez00(obj_t);
	static obj_t BGl_z62lambda2092z62zzsaw_jvm_codez00(obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2099z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_globalrefz00zzsaw_defsz00;
	static obj_t BGl_za2bexitregza2z00zzsaw_jvm_codez00 = BUNSPEC;
	static obj_t BGl_z62genzd2funzd2rtl_switch1722z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_newobjz00zzsaw_jvm_outz00(BgL_jvmz00_bglt, BgL_typez00_bglt,
		obj_t, obj_t);
	extern obj_t BGl_rtl_getfieldz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2argszd2genzd2predicatezb0zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_setfield1751z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t
		BGl_z62genzd2funzd2withzd2argszd2rt1730z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	extern int BGl_siza7ezd2destz75zzsaw_jvm_typez00(obj_t);
	static obj_t
		BGl_z62genzd2funzd2withzd2argszd2rt1732z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_effectz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_loadzd2regzd2zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_genzd2funcallzd2zzsaw_jvm_codez00(obj_t, obj_t);
	static obj_t BGl_liveblockz00zzsaw_jvm_codez00 = BUNSPEC;
	extern obj_t BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00;
	static obj_t BGl_doubleze70ze7zzsaw_jvm_codez00(obj_t);
	static obj_t
		BGl_genzd2argszd2genzd2funzd2zzsaw_jvm_codez00(BgL_rtl_funz00_bglt, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_go1720z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_localvarz00zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2rtl_call1726z62zzsaw_jvm_codez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2codezd2zzsaw_jvm_codez00(BgL_jvmz00_bglt,
		obj_t, obj_t);
	extern obj_t BGl_pushzd2intzd2zzsaw_jvm_outz00(BgL_jvmz00_bglt, obj_t);
	extern obj_t BGl_rtl_protectedz00zzsaw_defsz00;
	extern obj_t BGl_blockz00zzsaw_defsz00;
	extern obj_t BGl_za2purifyza2z00zzengine_paramz00;
	extern obj_t BGl_inlinezd2callzf3z21zzsaw_jvm_inlinez00(BgL_jvmz00_bglt,
		BgL_globalz00_bglt);
	static obj_t BGl_z62genzd2funzd2rtl_globalre1714z62zzsaw_jvm_codez00(obj_t,
		obj_t, obj_t);
	static obj_t __cnst[113];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7b0za72547z00,
		BGl_z62genzd2funzb0zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2500z00zzsaw_jvm_codez00,
		BgL_bgl_string2500za700za7za7s2548za7, "gen-fun", 7);
	      DEFINE_STRING(BGl_string2507z00zzsaw_jvm_codez00,
		BgL_bgl_string2507za700za7za7s2549za7, "gen-args-gen-fun", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2501z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2550za7,
		BGl_z62genzd2funzd2rtl_mov1705z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2502z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2551za7,
		BGl_z62genzd2funzd2rtl_loadi1708z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2503z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2552za7,
		BGl_z62genzd2funzd2rtl_loadg1710z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2504z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2553za7,
		BGl_z62genzd2funzd2rtl_storeg1712z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2505z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2554za7,
		BGl_z62genzd2funzd2rtl_globalre1714z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2506z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22555za7,
		BGl_z62genzd2argszd2genzd2funzd2rtl1716z62zzsaw_jvm_codez00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2508z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22556za7,
		BGl_z62genzd2argszd2genzd2funzd2rtl1718z62zzsaw_jvm_codez00, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string2515z00zzsaw_jvm_codez00,
		BgL_bgl_string2515za700za7za7s2557za7, "gen-fun-with-args", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2509z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2558za7,
		BGl_z62genzd2funzd2rtl_go1720z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2510z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2559za7,
		BGl_z62genzd2funzd2rtl_switch1722z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2511z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22560za7,
		BGl_z62genzd2argszd2genzd2funzd2rtl1724z62zzsaw_jvm_codez00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2512z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2561za7,
		BGl_z62genzd2funzd2rtl_call1726z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2513z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2562za7,
		BGl_z62genzd2funzd2rtl_lightfun1728z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2514z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2w2563za7,
		BGl_z62genzd2funzd2withzd2argszd2rt1730z62zzsaw_jvm_codez00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2516z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2w2564za7,
		BGl_z62genzd2funzd2withzd2argszd2rt1732z62zzsaw_jvm_codez00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2517z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2565za7,
		BGl_z62genzd2funzd2rtl_loadfun1735z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2518z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2566za7,
		BGl_z62genzd2funzd2rtl_apply1737z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2519z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2567za7,
		BGl_z62genzd2funzd2rtl_vref1739z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_REAL(BGl_real2540z00zzsaw_jvm_codez00,
		BgL_bgl_real2540za700za7za7saw2568za7, 0.75);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2520z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2569za7,
		BGl_z62genzd2funzd2rtl_vset1741z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2521z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2570za7,
		BGl_z62genzd2funzd2rtl_vlength1743z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2522z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2571za7,
		BGl_z62genzd2funzd2rtl_valloc1745z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2523z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22572za7,
		BGl_z62genzd2argszd2genzd2funzd2rtl1747z62zzsaw_jvm_codez00, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2524z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2573za7,
		BGl_z62genzd2funzd2rtl_getfield1749z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2525z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2574za7,
		BGl_z62genzd2funzd2rtl_setfield1751z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2526z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2575za7,
		BGl_z62genzd2funzd2rtl_instance1753z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2527z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2576za7,
		BGl_z62genzd2funzd2rtl_cast1755z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2528z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2577za7,
		BGl_z62genzd2funzd2rtl_cast_nul1757z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2529z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22578za7,
		BGl_z62genzd2argszd2genzd2funzd2rtl1759z62zzsaw_jvm_codez00, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string2537z00zzsaw_jvm_codez00,
		BgL_bgl_string2537za700za7za7s2579za7, "gen-args-gen-predicate", 22);
	      DEFINE_STRING(BGl_string2538z00zzsaw_jvm_codez00,
		BgL_bgl_string2538za700za7za7s2580za7, "I", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2530z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2581za7,
		BGl_z62genzd2funzd2rtl_boxref1761z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2531z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2582za7,
		BGl_z62genzd2funzd2rtl_boxset1763z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2532z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2583za7,
		BGl_z62genzd2funzd2rtl_jumpexit1765z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2533z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2584za7,
		BGl_z62genzd2funzd2rtl_fail1767z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2534z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2585za7,
		BGl_z62genzd2funzd2rtl_protect1769z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2541z00zzsaw_jvm_codez00,
		BgL_bgl_string2541za700za7za7s2586za7, "L", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2535z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2587za7,
		BGl_z62genzd2funzd2rtl_protecte1771z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2542z00zzsaw_jvm_codez00,
		BgL_bgl_string2542za700za7za7s2588za7, "loadi", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2536z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22589za7,
		BGl_z62genzd2argszd2genzd2predica1775zb0zzsaw_jvm_codez00, 0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string2543z00zzsaw_jvm_codez00,
		BgL_bgl_string2543za700za7za7s2590za7, "bad atom value", 14);
	      DEFINE_STRING(BGl_string2544z00zzsaw_jvm_codez00,
		BgL_bgl_string2544za700za7za7s2591za7, "saw_jvm_code", 12);
	      DEFINE_STRING(BGl_string2545z00zzsaw_jvm_codez00,
		BgL_bgl_string2545za700za7za7s2592za7,
		"(invokespecial init_bucs2) (new bucs2) (getstatic *key*) (getstatic *rest*) (getstatic *optional*) (getstatic *eof*) (invokestatic getbytes) getstatic putstatic (putfield procindex) (invokespecial init) (new me) tableswitch lookupswitch if_icmpne else invokestatic (dreturn) (freturn) (lreturn) (ireturn) (return) void (aaload) (daload) (faload) (laload) (iaload) (saload) (caload) (baload) (aastore) (dastore) (fastore) (lastore) (iastore) (sastore) (castore) (bastore) (arraylength) anewarray newarray goto instanceof checkcast aconst_null fconst_0 (invokespecial init_cell) (dup) (new cell) (getfield ccar) (putfield ccar) (checkcast throwable) (invokestatic jumpexit) (athrow) (invokestatic fail) (invokestatic setexit) ok not-inlined gen-fun1700 ifeq ifne liveblock out in saw_jvm_code lreg obj id (invokevirtual pfuncall4) (invokevirtual pfuncall3) (invokevirtual pfuncall2) (invokevirtual pfuncall1) (invokevirtual pfuncall0) (invokevirtual papply) (invokestatic cons) (getstatic *nil*) line location astore dstore fs"
		"tore lstore istore aload dload fload float lload iload int short char byte boolean from (pop) (pop2) (getstatic *unspecified*) no-value _ long double (invokestatic java_exception_handler) catch2 (areturn) (invokestatic debug_handler) (checkcast exit) catch end begin (handler from catch catch2 throwable) (handler from catch catch bexception) ",
		1367);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2539z00zzsaw_jvm_codez00,
		BgL_bgl_za762za7c3za704anonymo2593za7,
		BGl_z62zc3z04anonymousza32181ze3ze5zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2474z00zzsaw_jvm_codez00,
		BgL_bgl_string2474za700za7za7s2594za7, "P", 1);
	      DEFINE_STRING(BGl_string2475z00zzsaw_jvm_codez00,
		BgL_bgl_string2475za700za7za7s2595za7, "V", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2476z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2092za7622596z00, BGl_z62lambda2092z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2477z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2091za7622597z00, BGl_z62lambda2091z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2478z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2083za7622598z00, BGl_z62lambda2083z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_genzd2funzd2withzd2argszd2envz00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2w2599za7,
		BGl_z62genzd2funzd2withzd2argszb0zzsaw_jvm_codez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2479z00zzsaw_jvm_codez00,
		BgL_bgl_za762za7c3za704anonymo2600za7,
		BGl_z62zc3z04anonymousza32082ze3ze5zzsaw_jvm_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2480z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2080za7622601z00, BGl_z62lambda2080z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2481z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2077za7622602z00, BGl_z62lambda2077z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2482z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2113za7622603z00, BGl_z62lambda2113z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2483z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2112za7622604z00, BGl_z62lambda2112z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2484z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2118za7622605z00, BGl_z62lambda2118z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2491z00zzsaw_jvm_codez00,
		BgL_bgl_string2491za700za7za7s2606za7, "gen-args-gen-fun1696", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2485z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2117za7622607z00, BGl_z62lambda2117z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2486z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2105za7622608z00, BGl_z62lambda2105z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2493z00zzsaw_jvm_codez00,
		BgL_bgl_string2493za700za7za7s2609za7, "gen-fun-with-args1698", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2487z00zzsaw_jvm_codez00,
		BgL_bgl_za762za7c3za704anonymo2610za7,
		BGl_z62zc3z04anonymousza32104ze3ze5zzsaw_jvm_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2488z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2102za7622611z00, BGl_z62lambda2102z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2495z00zzsaw_jvm_codez00,
		BgL_bgl_string2495za700za7za7s2612za7, "gen-fun1700", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2489z00zzsaw_jvm_codez00,
		BgL_bgl_za762lambda2099za7622613z00, BGl_z62lambda2099z62zzsaw_jvm_codez00,
		0L, BUNSPEC, 6);
	      DEFINE_STRING(BGl_string2497z00zzsaw_jvm_codez00,
		BgL_bgl_string2497za700za7za7s2614za7, "gen-args-gen-predica1772", 24);
	      DEFINE_STRING(BGl_string2498z00zzsaw_jvm_codez00,
		BgL_bgl_string2498za700za7za7s2615za7, "No method for this object", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2490z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22616za7,
		BGl_z62genzd2argszd2genzd2fun1696zb0zzsaw_jvm_codez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2492z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2w2617za7,
		BGl_z62genzd2funzd2withzd2args1698zb0zzsaw_jvm_codez00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2494z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2fun17002618z00,
		BGl_z62genzd2fun1700zb0zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2496z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22619za7,
		BGl_z62genzd2argszd2genzd2predica1772zb0zzsaw_jvm_codez00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2499z00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2funza7d2r2620za7,
		BGl_z62genzd2funzd2rtl_nop1703z62zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_genzd2argszd2genzd2predicatezd2envz00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22621za7,
		BGl_z62genzd2argszd2genzd2predicatezb0zzsaw_jvm_codez00, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_modulezd2codezd2envz00zzsaw_jvm_codez00,
		BgL_bgl_za762moduleza7d2code2622z00,
		BGl_z62modulezd2codezb0zzsaw_jvm_codez00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_genzd2exprzd2envz00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2exprza7b02623za7,
		BGl_z62genzd2exprzb0zzsaw_jvm_codez00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_genzd2argszd2genzd2funzd2envz00zzsaw_jvm_codez00,
		BgL_bgl_za762genza7d2argsza7d22624za7,
		BGl_z62genzd2argszd2genzd2funzb0zzsaw_jvm_codez00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2lastzd2lineza2zd2zzsaw_jvm_codez00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_jvm_codez00));
		     ADD_ROOT((void *) (&BGl_lregz00zzsaw_jvm_codez00));
		     ADD_ROOT((void *) (&BGl_za2bexitregza2z00zzsaw_jvm_codez00));
		     ADD_ROOT((void *) (&BGl_liveblockz00zzsaw_jvm_codez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_codez00(long
		BgL_checksumz00_5960, char *BgL_fromz00_5961)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_jvm_codez00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_jvm_codez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_jvm_codez00();
					BGl_libraryzd2moduleszd2initz00zzsaw_jvm_codez00();
					BGl_cnstzd2initzd2zzsaw_jvm_codez00();
					BGl_importedzd2moduleszd2initz00zzsaw_jvm_codez00();
					BGl_objectzd2initzd2zzsaw_jvm_codez00();
					BGl_genericzd2initzd2zzsaw_jvm_codez00();
					BGl_methodzd2initzd2zzsaw_jvm_codez00();
					return BGl_toplevelzd2initzd2zzsaw_jvm_codez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_codez00(void)
	{
		{	/* SawJvm/code.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__ucs2z00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L,
				"saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_booleans_6_1z00(0L,
				"saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "saw_jvm_code");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_jvm_code");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_jvm_codez00(void)
	{
		{	/* SawJvm/code.scm 1 */
			{	/* SawJvm/code.scm 1 */
				obj_t BgL_cportz00_5661;

				{	/* SawJvm/code.scm 1 */
					obj_t BgL_stringz00_5668;

					BgL_stringz00_5668 = BGl_string2545z00zzsaw_jvm_codez00;
					{	/* SawJvm/code.scm 1 */
						obj_t BgL_startz00_5669;

						BgL_startz00_5669 = BINT(0L);
						{	/* SawJvm/code.scm 1 */
							obj_t BgL_endz00_5670;

							BgL_endz00_5670 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_5668)));
							{	/* SawJvm/code.scm 1 */

								BgL_cportz00_5661 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_5668, BgL_startz00_5669, BgL_endz00_5670);
				}}}}
				{
					long BgL_iz00_5662;

					BgL_iz00_5662 = 112L;
				BgL_loopz00_5663:
					if ((BgL_iz00_5662 == -1L))
						{	/* SawJvm/code.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawJvm/code.scm 1 */
							{	/* SawJvm/code.scm 1 */
								obj_t BgL_arg2546z00_5664;

								{	/* SawJvm/code.scm 1 */

									{	/* SawJvm/code.scm 1 */
										obj_t BgL_locationz00_5666;

										BgL_locationz00_5666 = BBOOL(((bool_t) 0));
										{	/* SawJvm/code.scm 1 */

											BgL_arg2546z00_5664 =
												BGl_readz00zz__readerz00(BgL_cportz00_5661,
												BgL_locationz00_5666);
										}
									}
								}
								{	/* SawJvm/code.scm 1 */
									int BgL_tmpz00_6001;

									BgL_tmpz00_6001 = (int) (BgL_iz00_5662);
									CNST_TABLE_SET(BgL_tmpz00_6001, BgL_arg2546z00_5664);
							}}
							{	/* SawJvm/code.scm 1 */
								int BgL_auxz00_5667;

								BgL_auxz00_5667 = (int) ((BgL_iz00_5662 - 1L));
								{
									long BgL_iz00_6006;

									BgL_iz00_6006 = (long) (BgL_auxz00_5667);
									BgL_iz00_5662 = BgL_iz00_6006;
									goto BgL_loopz00_5663;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_codez00(void)
	{
		{	/* SawJvm/code.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_jvm_codez00(void)
	{
		{	/* SawJvm/code.scm 1 */
			BGl_za2hasprotectza2z00zzsaw_jvm_codez00 = ((bool_t) 0);
			BGl_za2protectresultza2z00zzsaw_jvm_codez00 = ((bool_t) 0);
			BGl_za2bexitregza2z00zzsaw_jvm_codez00 = BFALSE;
			BGl_za2lastzd2lineza2zd2zzsaw_jvm_codez00 = BINT(-1L);
			return BUNSPEC;
		}

	}



/* module-code */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2codezd2zzsaw_jvm_codez00(BgL_jvmz00_bglt
		BgL_mez00_71, obj_t BgL_paramsz00_72, obj_t BgL_lz00_73)
	{
		{	/* SawJvm/code.scm 46 */
			{	/* SawJvm/code.scm 47 */
				obj_t BgL_locsz00_2758;

				BgL_locsz00_2758 =
					BGl_getzd2localszd2zzsaw_jvm_codez00(BgL_paramsz00_72, BgL_lz00_73);
				BGl_declarezd2localszd2zzsaw_jvm_outz00(BgL_mez00_71,
					BGl_doubleze70ze7zzsaw_jvm_codez00(BgL_paramsz00_72),
					BGl_doubleze70ze7zzsaw_jvm_codez00(BgL_locsz00_2758));
			}
			if (BGl_za2hasprotectza2z00zzsaw_jvm_codez00)
				{	/* SawJvm/code.scm 57 */
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(0));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(1));
				}
			else
				{	/* SawJvm/code.scm 57 */
					BFALSE;
				}
			{	/* SawJvm/code.scm 62 */
				obj_t BgL_pendingz00_2775;

				BgL_pendingz00_2775 = BUNSPEC;
				if (CBOOL(BGl_za2jvmzd2debugza2zd2zzengine_paramz00))
					{	/* SawJvm/code.scm 63 */
						BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(2));
						{
							obj_t BgL_l1624z00_2777;

							BgL_l1624z00_2777 = BgL_lz00_73;
						BgL_zc3z04anonymousza31845ze3z87_2778:
							if (PAIRP(BgL_l1624z00_2777))
								{	/* SawJvm/code.scm 65 */
									BGl_livezd2initzd2zzsaw_jvm_codez00(CAR(BgL_l1624z00_2777));
									{
										obj_t BgL_l1624z00_6027;

										BgL_l1624z00_6027 = CDR(BgL_l1624z00_2777);
										BgL_l1624z00_2777 = BgL_l1624z00_6027;
										goto BgL_zc3z04anonymousza31845ze3z87_2778;
									}
								}
							else
								{	/* SawJvm/code.scm 65 */
									((bool_t) 1);
								}
						}
						{
							obj_t BgL_l1626z00_2784;

							BgL_l1626z00_2784 = BgL_lz00_73;
						BgL_zc3z04anonymousza31849ze3z87_2785:
							if (PAIRP(BgL_l1626z00_2784))
								{	/* SawJvm/code.scm 66 */
									BGl_livezd2fixzd2zzsaw_jvm_codez00(CAR(BgL_l1626z00_2784));
									{
										obj_t BgL_l1626z00_6033;

										BgL_l1626z00_6033 = CDR(BgL_l1626z00_2784);
										BgL_l1626z00_2784 = BgL_l1626z00_6033;
										goto BgL_zc3z04anonymousza31849ze3z87_2785;
									}
								}
							else
								{	/* SawJvm/code.scm 66 */
									((bool_t) 1);
								}
						}
						if (NULLP(BgL_paramsz00_72))
							{	/* SawJvm/code.scm 67 */
								BgL_pendingz00_2775 = BNIL;
							}
						else
							{	/* SawJvm/code.scm 67 */
								obj_t BgL_head1630z00_2792;

								BgL_head1630z00_2792 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1628z00_2794;
									obj_t BgL_tail1631z00_2795;

									BgL_l1628z00_2794 = BgL_paramsz00_72;
									BgL_tail1631z00_2795 = BgL_head1630z00_2792;
								BgL_zc3z04anonymousza31854ze3z87_2796:
									if (NULLP(BgL_l1628z00_2794))
										{	/* SawJvm/code.scm 67 */
											BgL_pendingz00_2775 = CDR(BgL_head1630z00_2792);
										}
									else
										{	/* SawJvm/code.scm 67 */
											obj_t BgL_newtail1632z00_2798;

											{	/* SawJvm/code.scm 67 */
												obj_t BgL_arg1857z00_2800;

												{	/* SawJvm/code.scm 67 */
													obj_t BgL_pz00_2801;

													BgL_pz00_2801 = CAR(((obj_t) BgL_l1628z00_2794));
													BgL_arg1857z00_2800 =
														MAKE_YOUNG_PAIR(BgL_pz00_2801, CNST_TABLE_REF(2));
												}
												BgL_newtail1632z00_2798 =
													MAKE_YOUNG_PAIR(BgL_arg1857z00_2800, BNIL);
											}
											SET_CDR(BgL_tail1631z00_2795, BgL_newtail1632z00_2798);
											{	/* SawJvm/code.scm 67 */
												obj_t BgL_arg1856z00_2799;

												BgL_arg1856z00_2799 = CDR(((obj_t) BgL_l1628z00_2794));
												{
													obj_t BgL_tail1631z00_6050;
													obj_t BgL_l1628z00_6049;

													BgL_l1628z00_6049 = BgL_arg1856z00_2799;
													BgL_tail1631z00_6050 = BgL_newtail1632z00_2798;
													BgL_tail1631z00_2795 = BgL_tail1631z00_6050;
													BgL_l1628z00_2794 = BgL_l1628z00_6049;
													goto BgL_zc3z04anonymousza31854ze3z87_2796;
												}
											}
										}
								}
							}
					}
				else
					{	/* SawJvm/code.scm 63 */
						BFALSE;
					}
				{
					obj_t BgL_l1636z00_2804;

					BgL_l1636z00_2804 = BgL_lz00_73;
				BgL_zc3z04anonymousza31858ze3z87_2805:
					if (PAIRP(BgL_l1636z00_2804))
						{	/* SawJvm/code.scm 68 */
							{	/* SawJvm/code.scm 69 */
								obj_t BgL_bz00_2807;

								BgL_bz00_2807 = CAR(BgL_l1636z00_2804);
								{	/* SawJvm/code.scm 69 */
									int BgL_arg1860z00_2808;

									BgL_arg1860z00_2808 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_2807)))->BgL_labelz00);
									BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_71,
										BINT(BgL_arg1860z00_2808));
								}
								if (CBOOL(BGl_za2jvmzd2debugza2zd2zzengine_paramz00))
									{	/* SawJvm/code.scm 70 */
										BgL_pendingz00_2775 =
											BGl_livezd2resetzd2zzsaw_jvm_codez00(BgL_mez00_71,
											BgL_bz00_2807, BgL_pendingz00_2775);
									}
								else
									{	/* SawJvm/code.scm 70 */
										BFALSE;
									}
								{	/* SawJvm/code.scm 71 */
									obj_t BgL_g1635z00_2809;

									BgL_g1635z00_2809 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_2807)))->BgL_firstz00);
									{
										obj_t BgL_l1633z00_2811;

										BgL_l1633z00_2811 = BgL_g1635z00_2809;
									BgL_zc3z04anonymousza31861ze3z87_2812:
										if (PAIRP(BgL_l1633z00_2811))
											{	/* SawJvm/code.scm 72 */
												{	/* SawJvm/code.scm 71 */
													obj_t BgL_iz00_2814;

													BgL_iz00_2814 = CAR(BgL_l1633z00_2811);
													BgL_pendingz00_2775 =
														BGl_genzd2inszd2zzsaw_jvm_codez00(BgL_mez00_71,
														BgL_iz00_2814, BgL_pendingz00_2775);
												}
												{
													obj_t BgL_l1633z00_6067;

													BgL_l1633z00_6067 = CDR(BgL_l1633z00_2811);
													BgL_l1633z00_2811 = BgL_l1633z00_6067;
													goto BgL_zc3z04anonymousza31861ze3z87_2812;
												}
											}
										else
											{	/* SawJvm/code.scm 72 */
												((bool_t) 1);
											}
									}
								}
							}
							{
								obj_t BgL_l1636z00_6069;

								BgL_l1636z00_6069 = CDR(BgL_l1636z00_2804);
								BgL_l1636z00_2804 = BgL_l1636z00_6069;
								goto BgL_zc3z04anonymousza31858ze3z87_2805;
							}
						}
					else
						{	/* SawJvm/code.scm 68 */
							((bool_t) 1);
						}
				}
				if (CBOOL(BGl_za2jvmzd2debugza2zd2zzengine_paramz00))
					{	/* SawJvm/code.scm 74 */
						BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(3));
						{
							obj_t BgL_l1638z00_2820;

							BgL_l1638z00_2820 = BgL_pendingz00_2775;
						BgL_zc3z04anonymousza31865ze3z87_2821:
							if (PAIRP(BgL_l1638z00_2820))
								{	/* SawJvm/code.scm 76 */
									{	/* SawJvm/code.scm 76 */
										obj_t BgL_pz00_2823;

										BgL_pz00_2823 = CAR(BgL_l1638z00_2820);
										{	/* SawJvm/code.scm 76 */
											obj_t BgL_arg1868z00_2824;
											obj_t BgL_arg1869z00_2825;

											BgL_arg1868z00_2824 = CAR(((obj_t) BgL_pz00_2823));
											BgL_arg1869z00_2825 = CDR(((obj_t) BgL_pz00_2823));
											{	/* SawJvm/code.scm 76 */
												obj_t BgL_ez00_4308;

												BgL_ez00_4308 = CNST_TABLE_REF(3);
												{	/* SawJvm/code.scm 205 */
													obj_t BgL_arg1973z00_4310;

													{
														BgL_lregz00_bglt BgL_auxz00_6083;

														{
															obj_t BgL_auxz00_6084;

															{	/* SawJvm/code.scm 205 */
																BgL_objectz00_bglt BgL_tmpz00_6085;

																BgL_tmpz00_6085 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_regz00_bglt) BgL_arg1868z00_2824));
																BgL_auxz00_6084 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6085);
															}
															BgL_auxz00_6083 =
																((BgL_lregz00_bglt) BgL_auxz00_6084);
														}
														BgL_arg1973z00_4310 =
															(((BgL_lregz00_bglt) COBJECT(BgL_auxz00_6083))->
															BgL_idz00);
													}
													BGl_localvarz00zzsaw_jvm_outz00(BgL_mez00_71,
														BgL_arg1868z00_2824, BgL_arg1869z00_2825,
														BgL_ez00_4308, BgL_arg1973z00_4310);
												}
											}
										}
									}
									{
										obj_t BgL_l1638z00_6092;

										BgL_l1638z00_6092 = CDR(BgL_l1638z00_2820);
										BgL_l1638z00_2820 = BgL_l1638z00_6092;
										goto BgL_zc3z04anonymousza31865ze3z87_2821;
									}
								}
							else
								{	/* SawJvm/code.scm 76 */
									((bool_t) 1);
								}
						}
					}
				else
					{	/* SawJvm/code.scm 74 */
						((bool_t) 0);
					}
			}
			if (BGl_za2hasprotectza2z00zzsaw_jvm_codez00)
				{	/* SawJvm/code.scm 79 */
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(4));
					BGl_loadzd2regzd2zzsaw_jvm_codez00(
						((obj_t) BgL_mez00_71), BGl_za2bexitregza2z00zzsaw_jvm_codez00);
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(5));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(6));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(7));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(8));
					BGl_loadzd2regzd2zzsaw_jvm_codez00(
						((obj_t) BgL_mez00_71), BGl_za2bexitregza2z00zzsaw_jvm_codez00);
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(5));
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(9));
					return
						BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_71, CNST_TABLE_REF(7));
				}
			else
				{	/* SawJvm/code.scm 79 */
					return BFALSE;
				}
		}

	}



/* double~0 */
	obj_t BGl_doubleze70ze7zzsaw_jvm_codez00(obj_t BgL_lz00_2762)
	{
		{	/* SawJvm/code.scm 55 */
			if (NULLP(BgL_lz00_2762))
				{	/* SawJvm/code.scm 49 */
					return BgL_lz00_2762;
				}
			else
				{	/* SawJvm/code.scm 51 */
					obj_t BgL_regz00_2765;
					obj_t BgL_rz00_2766;

					BgL_regz00_2765 = CAR(((obj_t) BgL_lz00_2762));
					{	/* SawJvm/code.scm 51 */
						obj_t BgL_arg1844z00_2773;

						BgL_arg1844z00_2773 = CDR(((obj_t) BgL_lz00_2762));
						BgL_rz00_2766 =
							BGl_doubleze70ze7zzsaw_jvm_codez00(BgL_arg1844z00_2773);
					}
					{	/* SawJvm/code.scm 52 */
						obj_t BgL_typez00_2767;

						BgL_typez00_2767 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_rtl_regz00_bglt) COBJECT(
												((BgL_rtl_regz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_regz00_2765))))->
										BgL_typez00)))->BgL_namez00);
						{	/* SawJvm/code.scm 53 */
							bool_t BgL_test2640z00_6126;

							if ((BgL_typez00_2767 == CNST_TABLE_REF(10)))
								{	/* SawJvm/code.scm 53 */
									BgL_test2640z00_6126 = ((bool_t) 1);
								}
							else
								{	/* SawJvm/code.scm 53 */
									BgL_test2640z00_6126 =
										(BgL_typez00_2767 == CNST_TABLE_REF(11));
								}
							if (BgL_test2640z00_6126)
								{	/* SawJvm/code.scm 54 */
									obj_t BgL_arg1839z00_2769;
									obj_t BgL_arg1840z00_2770;

									{
										BgL_lregz00_bglt BgL_auxz00_6132;

										{
											obj_t BgL_auxz00_6133;

											{	/* SawJvm/code.scm 54 */
												BgL_objectz00_bglt BgL_tmpz00_6134;

												BgL_tmpz00_6134 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_regz00_2765));
												BgL_auxz00_6133 = BGL_OBJECT_WIDENING(BgL_tmpz00_6134);
											}
											BgL_auxz00_6132 = ((BgL_lregz00_bglt) BgL_auxz00_6133);
										}
										BgL_arg1839z00_2769 =
											(((BgL_lregz00_bglt) COBJECT(BgL_auxz00_6132))->
											BgL_idz00);
									}
									BgL_arg1840z00_2770 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_rz00_2766);
									return
										MAKE_YOUNG_PAIR(BgL_arg1839z00_2769, BgL_arg1840z00_2770);
								}
							else
								{	/* SawJvm/code.scm 55 */
									obj_t BgL_arg1842z00_2771;

									{
										BgL_lregz00_bglt BgL_auxz00_6143;

										{
											obj_t BgL_auxz00_6144;

											{	/* SawJvm/code.scm 55 */
												BgL_objectz00_bglt BgL_tmpz00_6145;

												BgL_tmpz00_6145 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_regz00_2765));
												BgL_auxz00_6144 = BGL_OBJECT_WIDENING(BgL_tmpz00_6145);
											}
											BgL_auxz00_6143 = ((BgL_lregz00_bglt) BgL_auxz00_6144);
										}
										BgL_arg1842z00_2771 =
											(((BgL_lregz00_bglt) COBJECT(BgL_auxz00_6143))->
											BgL_idz00);
									}
									return MAKE_YOUNG_PAIR(BgL_arg1842z00_2771, BgL_rz00_2766);
								}
						}
					}
				}
		}

	}



/* &module-code */
	obj_t BGl_z62modulezd2codezb0zzsaw_jvm_codez00(obj_t BgL_envz00_5386,
		obj_t BgL_mez00_5387, obj_t BgL_paramsz00_5388, obj_t BgL_lz00_5389)
	{
		{	/* SawJvm/code.scm 46 */
			return
				BGl_modulezd2codezd2zzsaw_jvm_codez00(
				((BgL_jvmz00_bglt) BgL_mez00_5387), BgL_paramsz00_5388, BgL_lz00_5389);
		}

	}



/* get-locals */
	obj_t BGl_getzd2localszd2zzsaw_jvm_codez00(obj_t BgL_paramsz00_74,
		obj_t BgL_lz00_75)
	{
		{	/* SawJvm/code.scm 96 */
			BGl_za2hasprotectza2z00zzsaw_jvm_codez00 = ((bool_t) 0);
			{	/* SawJvm/code.scm 98 */
				struct bgl_cell BgL_box2642_5655z00;
				struct bgl_cell BgL_box2643_5656z00;
				obj_t BgL_nz00_5655;
				obj_t BgL_regsz00_5656;

				BgL_nz00_5655 = MAKE_CELL_STACK(BINT(0L), BgL_box2642_5655z00);
				BgL_regsz00_5656 = MAKE_CELL_STACK(BNIL, BgL_box2643_5656z00);
				{
					obj_t BgL_ez00_2902;

					{
						obj_t BgL_lz00_2837;

						BgL_lz00_2837 = BgL_paramsz00_74;
					BgL_zc3z04anonymousza31871ze3z87_2838:
						if (NULLP(BgL_lz00_2837))
							{	/* SawJvm/code.scm 134 */
								((bool_t) 0);
							}
						else
							{	/* SawJvm/code.scm 134 */
								{	/* SawJvm/code.scm 135 */
									BgL_rtl_regz00_bglt BgL_tmp1200z00_2840;

									BgL_tmp1200z00_2840 =
										((BgL_rtl_regz00_bglt) CAR(((obj_t) BgL_lz00_2837)));
									{	/* SawJvm/code.scm 135 */
										BgL_lregz00_bglt BgL_wide1202z00_2842;

										BgL_wide1202z00_2842 =
											((BgL_lregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_lregz00_bgl))));
										{	/* SawJvm/code.scm 135 */
											obj_t BgL_auxz00_6164;
											BgL_objectz00_bglt BgL_tmpz00_6161;

											BgL_auxz00_6164 = ((obj_t) BgL_wide1202z00_2842);
											BgL_tmpz00_6161 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_tmp1200z00_2840));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6161, BgL_auxz00_6164);
										}
										((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_tmp1200z00_2840));
										{	/* SawJvm/code.scm 135 */
											long BgL_arg1873z00_2843;

											BgL_arg1873z00_2843 =
												BGL_CLASS_NUM(BGl_lregz00zzsaw_jvm_codez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_tmp1200z00_2840)),
												BgL_arg1873z00_2843);
										}
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_tmp1200z00_2840));
									}
									{
										obj_t BgL_auxz00_6182;
										BgL_lregz00_bglt BgL_auxz00_6175;

										{	/* SawJvm/code.scm 135 */
											long BgL_nz00_4459;

											BgL_nz00_4459 =
												(long) CINT(
												((obj_t) ((obj_t) CELL_REF(BgL_nz00_5655))));
											{	/* SawJvm/code.scm 100 */
												obj_t BgL_arg1884z00_4460;

												{	/* SawJvm/code.scm 100 */
													obj_t BgL_arg1885z00_4461;

													{	/* SawJvm/code.scm 100 */

														BgL_arg1885z00_4461 =
															BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
															(BgL_nz00_4459, 10L);
													}
													BgL_arg1884z00_4460 =
														string_append(BGl_string2474z00zzsaw_jvm_codez00,
														BgL_arg1885z00_4461);
												}
												BgL_auxz00_6182 =
													bstring_to_symbol(BgL_arg1884z00_4460);
										}}
										{
											obj_t BgL_auxz00_6176;

											{	/* SawJvm/code.scm 135 */
												BgL_objectz00_bglt BgL_tmpz00_6177;

												BgL_tmpz00_6177 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_tmp1200z00_2840));
												BgL_auxz00_6176 = BGL_OBJECT_WIDENING(BgL_tmpz00_6177);
											}
											BgL_auxz00_6175 = ((BgL_lregz00_bglt) BgL_auxz00_6176);
										}
										((((BgL_lregz00_bglt) COBJECT(BgL_auxz00_6175))->
												BgL_idz00) = ((obj_t) BgL_auxz00_6182), BUNSPEC);
									}
									((BgL_rtl_regz00_bglt) BgL_tmp1200z00_2840);
								}
								{	/* SawJvm/code.scm 136 */
									obj_t BgL_auxz00_5657;

									BgL_auxz00_5657 =
										ADDFX(
										((obj_t) ((obj_t) CELL_REF(BgL_nz00_5655))), BINT(1L));
									CELL_SET(BgL_nz00_5655, BgL_auxz00_5657);
								}
								{	/* SawJvm/code.scm 137 */
									obj_t BgL_arg1874z00_2845;

									BgL_arg1874z00_2845 = CDR(((obj_t) BgL_lz00_2837));
									{
										obj_t BgL_lz00_6195;

										BgL_lz00_6195 = BgL_arg1874z00_2845;
										BgL_lz00_2837 = BgL_lz00_6195;
										goto BgL_zc3z04anonymousza31871ze3z87_2838;
									}
								}
							}
					}
					{
						obj_t BgL_l1649z00_2848;

						BgL_l1649z00_2848 = BgL_lz00_75;
					BgL_zc3z04anonymousza31875ze3z87_2849:
						if (PAIRP(BgL_l1649z00_2848))
							{	/* SawJvm/code.scm 138 */
								{	/* SawJvm/code.scm 138 */
									obj_t BgL_bz00_2851;

									BgL_bz00_2851 = CAR(BgL_l1649z00_2848);
									{	/* SawJvm/code.scm 138 */
										obj_t BgL_g1648z00_2852;

										BgL_g1648z00_2852 =
											(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_2851)))->BgL_firstz00);
										{
											obj_t BgL_l1646z00_2854;

											BgL_l1646z00_2854 = BgL_g1648z00_2852;
										BgL_zc3z04anonymousza31877ze3z87_2855:
											if (PAIRP(BgL_l1646z00_2854))
												{	/* SawJvm/code.scm 138 */
													BgL_ez00_2902 = CAR(BgL_l1646z00_2854);
													{	/* SawJvm/code.scm 129 */
														obj_t BgL_destz00_2904;

														BgL_destz00_2904 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_ez00_2902)))->
															BgL_destz00);
														if (CBOOL(BgL_destz00_2904))
															{	/* SawJvm/code.scm 130 */
																BGl_regzd2ze3lregze70zd6zzsaw_jvm_codez00
																	(BgL_regsz00_5656, BgL_nz00_5655,
																	BgL_destz00_2904);
															}
														else
															{	/* SawJvm/code.scm 130 */
																BFALSE;
															}
														BGl_check_funze70ze7zzsaw_jvm_codez00
															(BgL_ez00_2902);
														{	/* SawJvm/code.scm 132 */
															obj_t BgL_g1645z00_2905;

															BgL_g1645z00_2905 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_ez00_2902)))->
																BgL_argsz00);
															{
																obj_t BgL_l1643z00_2907;

																BgL_l1643z00_2907 = BgL_g1645z00_2905;
															BgL_zc3z04anonymousza31903ze3z87_2908:
																if (PAIRP(BgL_l1643z00_2907))
																	{	/* SawJvm/code.scm 132 */
																		BGl_exprze70ze7zzsaw_jvm_codez00
																			(BgL_regsz00_5656, BgL_nz00_5655,
																			CAR(BgL_l1643z00_2907));
																		{
																			obj_t BgL_l1643z00_6215;

																			BgL_l1643z00_6215 =
																				CDR(BgL_l1643z00_2907);
																			BgL_l1643z00_2907 = BgL_l1643z00_6215;
																			goto
																				BgL_zc3z04anonymousza31903ze3z87_2908;
																		}
																	}
																else
																	{	/* SawJvm/code.scm 132 */
																		((bool_t) 1);
																	}
															}
														}
													}
													{
														obj_t BgL_l1646z00_6218;

														BgL_l1646z00_6218 = CDR(BgL_l1646z00_2854);
														BgL_l1646z00_2854 = BgL_l1646z00_6218;
														goto BgL_zc3z04anonymousza31877ze3z87_2855;
													}
												}
											else
												{	/* SawJvm/code.scm 138 */
													((bool_t) 1);
												}
										}
									}
								}
								{
									obj_t BgL_l1649z00_6220;

									BgL_l1649z00_6220 = CDR(BgL_l1649z00_2848);
									BgL_l1649z00_2848 = BgL_l1649z00_6220;
									goto BgL_zc3z04anonymousza31875ze3z87_2849;
								}
							}
						else
							{	/* SawJvm/code.scm 138 */
								((bool_t) 1);
							}
					}
					return
						bgl_reverse_bang(((obj_t) ((obj_t) CELL_REF(BgL_regsz00_5656))));
				}
			}
		}

	}



/* reg->lreg~0 */
	obj_t BGl_regzd2ze3lregze70zd6zzsaw_jvm_codez00(obj_t BgL_regsz00_5650,
		obj_t BgL_nz00_5649, obj_t BgL_rz00_2869)
	{
		{	/* SawJvm/code.scm 106 */
			{	/* SawJvm/code.scm 103 */
				bool_t BgL_test2649z00_6224;

				{	/* SawJvm/code.scm 103 */
					obj_t BgL_classz00_4318;

					BgL_classz00_4318 = BGl_lregz00zzsaw_jvm_codez00;
					if (BGL_OBJECTP(BgL_rz00_2869))
						{	/* SawJvm/code.scm 103 */
							obj_t BgL_oclassz00_4320;

							{	/* SawJvm/code.scm 103 */
								obj_t BgL_arg1815z00_4322;
								long BgL_arg1816z00_4323;

								BgL_arg1815z00_4322 = (BGl_za2classesza2z00zz__objectz00);
								{	/* SawJvm/code.scm 103 */
									long BgL_arg1817z00_4324;

									BgL_arg1817z00_4324 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_rz00_2869));
									BgL_arg1816z00_4323 = (BgL_arg1817z00_4324 - OBJECT_TYPE);
								}
								BgL_oclassz00_4320 =
									VECTOR_REF(BgL_arg1815z00_4322, BgL_arg1816z00_4323);
							}
							BgL_test2649z00_6224 = (BgL_oclassz00_4320 == BgL_classz00_4318);
						}
					else
						{	/* SawJvm/code.scm 103 */
							BgL_test2649z00_6224 = ((bool_t) 0);
						}
				}
				if (BgL_test2649z00_6224)
					{	/* SawJvm/code.scm 103 */
						return BFALSE;
					}
				else
					{	/* SawJvm/code.scm 103 */
						{	/* SawJvm/code.scm 104 */
							BgL_lregz00_bglt BgL_wide1195z00_2874;

							BgL_wide1195z00_2874 =
								((BgL_lregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_lregz00_bgl))));
							{	/* SawJvm/code.scm 104 */
								obj_t BgL_auxz00_6238;
								BgL_objectz00_bglt BgL_tmpz00_6234;

								BgL_auxz00_6238 = ((obj_t) BgL_wide1195z00_2874);
								BgL_tmpz00_6234 =
									((BgL_objectz00_bglt)
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_rz00_2869)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6234, BgL_auxz00_6238);
							}
							((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2869)));
							{	/* SawJvm/code.scm 104 */
								long BgL_arg1888z00_2875;

								BgL_arg1888z00_2875 =
									BGL_CLASS_NUM(BGl_lregz00zzsaw_jvm_codez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2869))),
									BgL_arg1888z00_2875);
							}
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2869)));
						}
						{
							obj_t BgL_auxz00_6260;
							BgL_lregz00_bglt BgL_auxz00_6252;

							{	/* SawJvm/code.scm 104 */
								long BgL_nz00_4335;

								BgL_nz00_4335 =
									(long) CINT(((obj_t) ((obj_t) CELL_REF(BgL_nz00_5649))));
								{	/* SawJvm/code.scm 100 */
									obj_t BgL_arg1884z00_4336;

									{	/* SawJvm/code.scm 100 */
										obj_t BgL_arg1885z00_4337;

										{	/* SawJvm/code.scm 100 */

											BgL_arg1885z00_4337 =
												BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
												(BgL_nz00_4335, 10L);
										}
										BgL_arg1884z00_4336 =
											string_append(BGl_string2475z00zzsaw_jvm_codez00,
											BgL_arg1885z00_4337);
									}
									BgL_auxz00_6260 = bstring_to_symbol(BgL_arg1884z00_4336);
							}}
							{
								obj_t BgL_auxz00_6253;

								{	/* SawJvm/code.scm 104 */
									BgL_objectz00_bglt BgL_tmpz00_6254;

									BgL_tmpz00_6254 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2869)));
									BgL_auxz00_6253 = BGL_OBJECT_WIDENING(BgL_tmpz00_6254);
								}
								BgL_auxz00_6252 = ((BgL_lregz00_bglt) BgL_auxz00_6253);
							}
							((((BgL_lregz00_bglt) COBJECT(BgL_auxz00_6252))->BgL_idz00) =
								((obj_t) BgL_auxz00_6260), BUNSPEC);
						}
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2869));
						{	/* SawJvm/code.scm 105 */
							obj_t BgL_auxz00_5651;

							BgL_auxz00_5651 =
								ADDFX(((obj_t) ((obj_t) CELL_REF(BgL_nz00_5649))), BINT(1L));
							CELL_SET(BgL_nz00_5649, BgL_auxz00_5651);
						}
						{	/* SawJvm/code.scm 106 */
							obj_t BgL_auxz00_5652;

							BgL_auxz00_5652 =
								MAKE_YOUNG_PAIR(BgL_rz00_2869,
								((obj_t) ((obj_t) CELL_REF(BgL_regsz00_5650))));
							return CELL_SET(BgL_regsz00_5650, BgL_auxz00_5652);
						}
					}
			}
		}

	}



/* check_fun~0 */
	obj_t BGl_check_funze70ze7zzsaw_jvm_codez00(obj_t BgL_ez00_2886)
	{
		{	/* SawJvm/code.scm 122 */
			{	/* SawJvm/code.scm 117 */
				BgL_rtl_funz00_bglt BgL_funz00_2888;

				BgL_funz00_2888 =
					(((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_ez00_2886)))->BgL_funz00);
				{	/* SawJvm/code.scm 119 */
					bool_t BgL_test2651z00_6276;

					{	/* SawJvm/code.scm 119 */
						obj_t BgL_classz00_4343;

						BgL_classz00_4343 = BGl_rtl_protectz00zzsaw_defsz00;
						{	/* SawJvm/code.scm 119 */
							BgL_objectz00_bglt BgL_arg1807z00_4345;

							{	/* SawJvm/code.scm 119 */
								obj_t BgL_tmpz00_6277;

								BgL_tmpz00_6277 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2888));
								BgL_arg1807z00_4345 = (BgL_objectz00_bglt) (BgL_tmpz00_6277);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/code.scm 119 */
									long BgL_idxz00_4351;

									BgL_idxz00_4351 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4345);
									BgL_test2651z00_6276 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4351 + 2L)) == BgL_classz00_4343);
								}
							else
								{	/* SawJvm/code.scm 119 */
									bool_t BgL_res2435z00_4376;

									{	/* SawJvm/code.scm 119 */
										obj_t BgL_oclassz00_4359;

										{	/* SawJvm/code.scm 119 */
											obj_t BgL_arg1815z00_4367;
											long BgL_arg1816z00_4368;

											BgL_arg1815z00_4367 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/code.scm 119 */
												long BgL_arg1817z00_4369;

												BgL_arg1817z00_4369 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4345);
												BgL_arg1816z00_4368 =
													(BgL_arg1817z00_4369 - OBJECT_TYPE);
											}
											BgL_oclassz00_4359 =
												VECTOR_REF(BgL_arg1815z00_4367, BgL_arg1816z00_4368);
										}
										{	/* SawJvm/code.scm 119 */
											bool_t BgL__ortest_1115z00_4360;

											BgL__ortest_1115z00_4360 =
												(BgL_classz00_4343 == BgL_oclassz00_4359);
											if (BgL__ortest_1115z00_4360)
												{	/* SawJvm/code.scm 119 */
													BgL_res2435z00_4376 = BgL__ortest_1115z00_4360;
												}
											else
												{	/* SawJvm/code.scm 119 */
													long BgL_odepthz00_4361;

													{	/* SawJvm/code.scm 119 */
														obj_t BgL_arg1804z00_4362;

														BgL_arg1804z00_4362 = (BgL_oclassz00_4359);
														BgL_odepthz00_4361 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4362);
													}
													if ((2L < BgL_odepthz00_4361))
														{	/* SawJvm/code.scm 119 */
															obj_t BgL_arg1802z00_4364;

															{	/* SawJvm/code.scm 119 */
																obj_t BgL_arg1803z00_4365;

																BgL_arg1803z00_4365 = (BgL_oclassz00_4359);
																BgL_arg1802z00_4364 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4365,
																	2L);
															}
															BgL_res2435z00_4376 =
																(BgL_arg1802z00_4364 == BgL_classz00_4343);
														}
													else
														{	/* SawJvm/code.scm 119 */
															BgL_res2435z00_4376 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2651z00_6276 = BgL_res2435z00_4376;
								}
						}
					}
					if (BgL_test2651z00_6276)
						{	/* SawJvm/code.scm 119 */
							BGl_za2hasprotectza2z00zzsaw_jvm_codez00 = ((bool_t) 1);
							return (BGl_za2bexitregza2z00zzsaw_jvm_codez00 =
								(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_ez00_2886)))->BgL_destz00),
								BUNSPEC);
						}
					else
						{	/* SawJvm/code.scm 122 */
							bool_t BgL__ortest_1199z00_2890;

							{	/* SawJvm/code.scm 122 */
								obj_t BgL_classz00_4378;

								BgL_classz00_4378 = BGl_rtl_switchz00zzsaw_defsz00;
								{	/* SawJvm/code.scm 122 */
									BgL_objectz00_bglt BgL_arg1807z00_4380;

									{	/* SawJvm/code.scm 122 */
										obj_t BgL_tmpz00_6302;

										BgL_tmpz00_6302 =
											((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2888));
										BgL_arg1807z00_4380 =
											(BgL_objectz00_bglt) (BgL_tmpz00_6302);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawJvm/code.scm 122 */
											long BgL_idxz00_4386;

											BgL_idxz00_4386 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4380);
											BgL__ortest_1199z00_2890 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4386 + 4L)) == BgL_classz00_4378);
										}
									else
										{	/* SawJvm/code.scm 122 */
											bool_t BgL_res2436z00_4411;

											{	/* SawJvm/code.scm 122 */
												obj_t BgL_oclassz00_4394;

												{	/* SawJvm/code.scm 122 */
													obj_t BgL_arg1815z00_4402;
													long BgL_arg1816z00_4403;

													BgL_arg1815z00_4402 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawJvm/code.scm 122 */
														long BgL_arg1817z00_4404;

														BgL_arg1817z00_4404 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4380);
														BgL_arg1816z00_4403 =
															(BgL_arg1817z00_4404 - OBJECT_TYPE);
													}
													BgL_oclassz00_4394 =
														VECTOR_REF(BgL_arg1815z00_4402,
														BgL_arg1816z00_4403);
												}
												{	/* SawJvm/code.scm 122 */
													bool_t BgL__ortest_1115z00_4395;

													BgL__ortest_1115z00_4395 =
														(BgL_classz00_4378 == BgL_oclassz00_4394);
													if (BgL__ortest_1115z00_4395)
														{	/* SawJvm/code.scm 122 */
															BgL_res2436z00_4411 = BgL__ortest_1115z00_4395;
														}
													else
														{	/* SawJvm/code.scm 122 */
															long BgL_odepthz00_4396;

															{	/* SawJvm/code.scm 122 */
																obj_t BgL_arg1804z00_4397;

																BgL_arg1804z00_4397 = (BgL_oclassz00_4394);
																BgL_odepthz00_4396 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4397);
															}
															if ((4L < BgL_odepthz00_4396))
																{	/* SawJvm/code.scm 122 */
																	obj_t BgL_arg1802z00_4399;

																	{	/* SawJvm/code.scm 122 */
																		obj_t BgL_arg1803z00_4400;

																		BgL_arg1803z00_4400 = (BgL_oclassz00_4394);
																		BgL_arg1802z00_4399 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4400, 4L);
																	}
																	BgL_res2436z00_4411 =
																		(BgL_arg1802z00_4399 == BgL_classz00_4378);
																}
															else
																{	/* SawJvm/code.scm 122 */
																	BgL_res2436z00_4411 = ((bool_t) 0);
																}
														}
												}
											}
											BgL__ortest_1199z00_2890 = BgL_res2436z00_4411;
										}
								}
							}
							if (BgL__ortest_1199z00_2890)
								{	/* SawJvm/code.scm 122 */
									return BBOOL(BgL__ortest_1199z00_2890);
								}
							else
								{	/* SawJvm/code.scm 122 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* expr~0 */
	obj_t BGl_exprze70ze7zzsaw_jvm_codez00(obj_t BgL_regsz00_5654,
		obj_t BgL_nz00_5653, obj_t BgL_ez00_2891)
	{
		{	/* SawJvm/code.scm 127 */
			{	/* SawJvm/code.scm 124 */
				bool_t BgL_test2659z00_6327;

				{	/* SawJvm/code.scm 124 */
					obj_t BgL_classz00_4412;

					BgL_classz00_4412 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_ez00_2891))
						{	/* SawJvm/code.scm 124 */
							BgL_objectz00_bglt BgL_arg1807z00_4414;

							BgL_arg1807z00_4414 = (BgL_objectz00_bglt) (BgL_ez00_2891);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/code.scm 124 */
									long BgL_idxz00_4420;

									BgL_idxz00_4420 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4414);
									BgL_test2659z00_6327 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4420 + 1L)) == BgL_classz00_4412);
								}
							else
								{	/* SawJvm/code.scm 124 */
									bool_t BgL_res2437z00_4445;

									{	/* SawJvm/code.scm 124 */
										obj_t BgL_oclassz00_4428;

										{	/* SawJvm/code.scm 124 */
											obj_t BgL_arg1815z00_4436;
											long BgL_arg1816z00_4437;

											BgL_arg1815z00_4436 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/code.scm 124 */
												long BgL_arg1817z00_4438;

												BgL_arg1817z00_4438 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4414);
												BgL_arg1816z00_4437 =
													(BgL_arg1817z00_4438 - OBJECT_TYPE);
											}
											BgL_oclassz00_4428 =
												VECTOR_REF(BgL_arg1815z00_4436, BgL_arg1816z00_4437);
										}
										{	/* SawJvm/code.scm 124 */
											bool_t BgL__ortest_1115z00_4429;

											BgL__ortest_1115z00_4429 =
												(BgL_classz00_4412 == BgL_oclassz00_4428);
											if (BgL__ortest_1115z00_4429)
												{	/* SawJvm/code.scm 124 */
													BgL_res2437z00_4445 = BgL__ortest_1115z00_4429;
												}
											else
												{	/* SawJvm/code.scm 124 */
													long BgL_odepthz00_4430;

													{	/* SawJvm/code.scm 124 */
														obj_t BgL_arg1804z00_4431;

														BgL_arg1804z00_4431 = (BgL_oclassz00_4428);
														BgL_odepthz00_4430 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4431);
													}
													if ((1L < BgL_odepthz00_4430))
														{	/* SawJvm/code.scm 124 */
															obj_t BgL_arg1802z00_4433;

															{	/* SawJvm/code.scm 124 */
																obj_t BgL_arg1803z00_4434;

																BgL_arg1803z00_4434 = (BgL_oclassz00_4428);
																BgL_arg1802z00_4433 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4434,
																	1L);
															}
															BgL_res2437z00_4445 =
																(BgL_arg1802z00_4433 == BgL_classz00_4412);
														}
													else
														{	/* SawJvm/code.scm 124 */
															BgL_res2437z00_4445 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2659z00_6327 = BgL_res2437z00_4445;
								}
						}
					else
						{	/* SawJvm/code.scm 124 */
							BgL_test2659z00_6327 = ((bool_t) 0);
						}
				}
				if (BgL_test2659z00_6327)
					{	/* SawJvm/code.scm 124 */
						BGL_TAIL return
							BGl_regzd2ze3lregze70zd6zzsaw_jvm_codez00(BgL_regsz00_5654,
							BgL_nz00_5653, BgL_ez00_2891);
					}
				else
					{	/* SawJvm/code.scm 124 */
						BGl_check_funze70ze7zzsaw_jvm_codez00(BgL_ez00_2891);
						{	/* SawJvm/code.scm 127 */
							obj_t BgL_g1642z00_2894;

							BgL_g1642z00_2894 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_ez00_2891)))->BgL_argsz00);
							{
								obj_t BgL_l1640z00_2896;

								{	/* SawJvm/code.scm 127 */
									bool_t BgL_tmpz00_6354;

									BgL_l1640z00_2896 = BgL_g1642z00_2894;
								BgL_zc3z04anonymousza31897ze3z87_2897:
									if (PAIRP(BgL_l1640z00_2896))
										{	/* SawJvm/code.scm 127 */
											{	/* SawJvm/code.scm 127 */
												obj_t BgL_arg1899z00_2899;

												BgL_arg1899z00_2899 = CAR(BgL_l1640z00_2896);
												BGl_exprze70ze7zzsaw_jvm_codez00(BgL_regsz00_5654,
													BgL_nz00_5653, BgL_arg1899z00_2899);
											}
											{
												obj_t BgL_l1640z00_6359;

												BgL_l1640z00_6359 = CDR(BgL_l1640z00_2896);
												BgL_l1640z00_2896 = BgL_l1640z00_6359;
												goto BgL_zc3z04anonymousza31897ze3z87_2897;
											}
										}
									else
										{	/* SawJvm/code.scm 127 */
											BgL_tmpz00_6354 = ((bool_t) 1);
										}
									return BBOOL(BgL_tmpz00_6354);
								}
							}
						}
					}
			}
		}

	}



/* live-init */
	BgL_blockz00_bglt BGl_livezd2initzd2zzsaw_jvm_codez00(obj_t BgL_bz00_76)
	{
		{	/* SawJvm/code.scm 145 */
			{	/* SawJvm/code.scm 146 */
				BgL_liveblockz00_bglt BgL_wide1207z00_2921;

				BgL_wide1207z00_2921 =
					((BgL_liveblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_liveblockz00_bgl))));
				{	/* SawJvm/code.scm 146 */
					obj_t BgL_auxz00_6367;
					BgL_objectz00_bglt BgL_tmpz00_6363;

					BgL_auxz00_6367 = ((obj_t) BgL_wide1207z00_2921);
					BgL_tmpz00_6363 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_76)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6363, BgL_auxz00_6367);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_76)));
				{	/* SawJvm/code.scm 146 */
					long BgL_arg1911z00_2922;

					BgL_arg1911z00_2922 =
						BGL_CLASS_NUM(BGl_liveblockz00zzsaw_jvm_codez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_76))), BgL_arg1911z00_2922);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_76)));
			}
			{
				BgL_liveblockz00_bglt BgL_auxz00_6381;

				{
					obj_t BgL_auxz00_6382;

					{	/* SawJvm/code.scm 146 */
						BgL_objectz00_bglt BgL_tmpz00_6383;

						BgL_tmpz00_6383 =
							((BgL_objectz00_bglt)
							((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_76)));
						BgL_auxz00_6382 = BGL_OBJECT_WIDENING(BgL_tmpz00_6383);
					}
					BgL_auxz00_6381 = ((BgL_liveblockz00_bglt) BgL_auxz00_6382);
				}
				((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_6381))->BgL_inz00) =
					((obj_t) BNIL), BUNSPEC);
			}
			{
				BgL_liveblockz00_bglt BgL_auxz00_6390;

				{
					obj_t BgL_auxz00_6391;

					{	/* SawJvm/code.scm 146 */
						BgL_objectz00_bglt BgL_tmpz00_6392;

						BgL_tmpz00_6392 =
							((BgL_objectz00_bglt)
							((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_76)));
						BgL_auxz00_6391 = BGL_OBJECT_WIDENING(BgL_tmpz00_6392);
					}
					BgL_auxz00_6390 = ((BgL_liveblockz00_bglt) BgL_auxz00_6391);
				}
				((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_6390))->BgL_outz00) =
					((obj_t) BNIL), BUNSPEC);
			}
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_76));
		}

	}



/* live-fix */
	bool_t BGl_livezd2fixzd2zzsaw_jvm_codez00(obj_t BgL_bz00_77)
	{
		{	/* SawJvm/code.scm 148 */
			{	/* SawJvm/code.scm 185 */
				obj_t BgL_arg1912z00_2931;

				{
					BgL_liveblockz00_bglt BgL_auxz00_6401;

					{
						obj_t BgL_auxz00_6402;

						{	/* SawJvm/code.scm 185 */
							BgL_objectz00_bglt BgL_tmpz00_6403;

							BgL_tmpz00_6403 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_77));
							BgL_auxz00_6402 = BGL_OBJECT_WIDENING(BgL_tmpz00_6403);
						}
						BgL_auxz00_6401 = ((BgL_liveblockz00_bglt) BgL_auxz00_6402);
					}
					BgL_arg1912z00_2931 =
						(((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_6401))->BgL_outz00);
				}
				return
					BGl_fixpointze70ze7zzsaw_jvm_codez00(BgL_bz00_77,
					BgL_arg1912z00_2931);
			}
		}

	}



/* <@anonymous:1935>~0 */
	bool_t BGl_zc3z04anonymousza31935ze3ze70z60zzsaw_jvm_codez00(obj_t
		BgL_regsz00_5646, obj_t BgL_l1651z00_2961)
	{
		{	/* SawJvm/code.scm 165 */
		BGl_zc3z04anonymousza31935ze3ze70z60zzsaw_jvm_codez00:
			if (PAIRP(BgL_l1651z00_2961))
				{	/* SawJvm/code.scm 165 */
					{	/* SawJvm/code.scm 165 */
						obj_t BgL_az00_2964;

						BgL_az00_2964 = CAR(BgL_l1651z00_2961);
						{	/* SawJvm/code.scm 165 */
							obj_t BgL_auxz00_5647;

							BgL_auxz00_5647 =
								BGl_livezd2argze70z35zzsaw_jvm_codez00(BgL_az00_2964,
								CELL_REF(BgL_regsz00_5646));
							CELL_SET(BgL_regsz00_5646, BgL_auxz00_5647);
						}
					}
					{	/* SawJvm/code.scm 165 */
						obj_t BgL_arg1937z00_2965;

						BgL_arg1937z00_2965 = CDR(BgL_l1651z00_2961);
						{
							obj_t BgL_l1651z00_6415;

							BgL_l1651z00_6415 = BgL_arg1937z00_2965;
							BgL_l1651z00_2961 = BgL_l1651z00_6415;
							goto BGl_zc3z04anonymousza31935ze3ze70z60zzsaw_jvm_codez00;
						}
					}
				}
			else
				{	/* SawJvm/code.scm 165 */
					return ((bool_t) 1);
				}
		}

	}



/* live-args~0 */
	obj_t BGl_livezd2argsze70z35zzsaw_jvm_codez00(obj_t BgL_argsz00_2957,
		obj_t BgL_regsz00_2958)
	{
		{	/* SawJvm/code.scm 165 */
			{	/* SawJvm/code.scm 165 */
				struct bgl_cell BgL_box2666_5648z00;
				obj_t BgL_regsz00_5648;

				BgL_regsz00_5648 =
					MAKE_CELL_STACK(BgL_regsz00_2958, BgL_box2666_5648z00);
				BGl_zc3z04anonymousza31935ze3ze70z60zzsaw_jvm_codez00(BgL_regsz00_5648,
					BgL_argsz00_2957);
				return CELL_REF(BgL_regsz00_5648);
			}
		}

	}



/* live-arg~0 */
	obj_t BGl_livezd2argze70z35zzsaw_jvm_codez00(obj_t BgL_az00_2951,
		obj_t BgL_regsz00_2952)
	{
		{	/* SawJvm/code.scm 163 */
			{	/* SawJvm/code.scm 159 */
				bool_t BgL_test2667z00_6417;

				{	/* SawJvm/code.scm 159 */
					obj_t BgL_classz00_4484;

					BgL_classz00_4484 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_az00_2951))
						{	/* SawJvm/code.scm 159 */
							BgL_objectz00_bglt BgL_arg1807z00_4486;

							BgL_arg1807z00_4486 = (BgL_objectz00_bglt) (BgL_az00_2951);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/code.scm 159 */
									long BgL_idxz00_4492;

									BgL_idxz00_4492 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4486);
									BgL_test2667z00_6417 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4492 + 1L)) == BgL_classz00_4484);
								}
							else
								{	/* SawJvm/code.scm 159 */
									bool_t BgL_res2438z00_4517;

									{	/* SawJvm/code.scm 159 */
										obj_t BgL_oclassz00_4500;

										{	/* SawJvm/code.scm 159 */
											obj_t BgL_arg1815z00_4508;
											long BgL_arg1816z00_4509;

											BgL_arg1815z00_4508 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/code.scm 159 */
												long BgL_arg1817z00_4510;

												BgL_arg1817z00_4510 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4486);
												BgL_arg1816z00_4509 =
													(BgL_arg1817z00_4510 - OBJECT_TYPE);
											}
											BgL_oclassz00_4500 =
												VECTOR_REF(BgL_arg1815z00_4508, BgL_arg1816z00_4509);
										}
										{	/* SawJvm/code.scm 159 */
											bool_t BgL__ortest_1115z00_4501;

											BgL__ortest_1115z00_4501 =
												(BgL_classz00_4484 == BgL_oclassz00_4500);
											if (BgL__ortest_1115z00_4501)
												{	/* SawJvm/code.scm 159 */
													BgL_res2438z00_4517 = BgL__ortest_1115z00_4501;
												}
											else
												{	/* SawJvm/code.scm 159 */
													long BgL_odepthz00_4502;

													{	/* SawJvm/code.scm 159 */
														obj_t BgL_arg1804z00_4503;

														BgL_arg1804z00_4503 = (BgL_oclassz00_4500);
														BgL_odepthz00_4502 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4503);
													}
													if ((1L < BgL_odepthz00_4502))
														{	/* SawJvm/code.scm 159 */
															obj_t BgL_arg1802z00_4505;

															{	/* SawJvm/code.scm 159 */
																obj_t BgL_arg1803z00_4506;

																BgL_arg1803z00_4506 = (BgL_oclassz00_4500);
																BgL_arg1802z00_4505 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4506,
																	1L);
															}
															BgL_res2438z00_4517 =
																(BgL_arg1802z00_4505 == BgL_classz00_4484);
														}
													else
														{	/* SawJvm/code.scm 159 */
															BgL_res2438z00_4517 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2667z00_6417 = BgL_res2438z00_4517;
								}
						}
					else
						{	/* SawJvm/code.scm 159 */
							BgL_test2667z00_6417 = ((bool_t) 0);
						}
				}
				if (BgL_test2667z00_6417)
					{	/* SawJvm/code.scm 159 */
						if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_az00_2951,
									BgL_regsz00_2952)))
							{	/* SawJvm/code.scm 161 */
								return BgL_regsz00_2952;
							}
						else
							{	/* SawJvm/code.scm 161 */
								return MAKE_YOUNG_PAIR(BgL_az00_2951, BgL_regsz00_2952);
							}
					}
				else
					{	/* SawJvm/code.scm 160 */
						obj_t BgL_arg1933z00_2956;

						BgL_arg1933z00_2956 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_az00_2951)))->BgL_argsz00);
						return
							BGl_livezd2argsze70z35zzsaw_jvm_codez00(BgL_arg1933z00_2956,
							BgL_regsz00_2952);
					}
			}
		}

	}



/* live-instrs~0 */
	obj_t BGl_livezd2instrsze70z35zzsaw_jvm_codez00(obj_t BgL_lz00_2975,
		obj_t BgL_regsz00_2976)
	{
		{	/* SawJvm/code.scm 173 */
			{
				obj_t BgL_insz00_2967;
				obj_t BgL_regsz00_2968;

				if (NULLP(BgL_lz00_2975))
					{	/* SawJvm/code.scm 171 */
						return BgL_regsz00_2976;
					}
				else
					{	/* SawJvm/code.scm 173 */
						obj_t BgL_arg1945z00_2979;
						obj_t BgL_arg1946z00_2980;

						BgL_arg1945z00_2979 = CAR(((obj_t) BgL_lz00_2975));
						{	/* SawJvm/code.scm 173 */
							obj_t BgL_arg1947z00_2981;

							BgL_arg1947z00_2981 = CDR(((obj_t) BgL_lz00_2975));
							BgL_arg1946z00_2980 =
								BGl_livezd2instrsze70z35zzsaw_jvm_codez00(BgL_arg1947z00_2981,
								BgL_regsz00_2976);
						}
						BgL_insz00_2967 = BgL_arg1945z00_2979;
						BgL_regsz00_2968 = BgL_arg1946z00_2980;
						{	/* SawJvm/code.scm 169 */
							obj_t BgL_arg1939z00_2971;
							obj_t BgL_arg1940z00_2972;

							BgL_arg1939z00_2971 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_2967)))->BgL_argsz00);
							if (CBOOL(
									(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_insz00_2967)))->
										BgL_destz00)))
								{	/* SawJvm/code.scm 169 */
									BgL_arg1940z00_2972 =
										bgl_remq(
										(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_2967)))->
											BgL_destz00), BgL_regsz00_2968);
								}
							else
								{	/* SawJvm/code.scm 169 */
									BgL_arg1940z00_2972 = BgL_regsz00_2968;
								}
							return
								BGl_livezd2argsze70z35zzsaw_jvm_codez00(BgL_arg1939z00_2971,
								BgL_arg1940z00_2972);
						}
					}
			}
		}

	}



/* include~0 */
	bool_t BGl_includeze70ze7zzsaw_jvm_codez00(obj_t BgL_l1z00_2932,
		obj_t BgL_l2z00_2933)
	{
		{	/* SawJvm/code.scm 152 */
		BGl_includeze70ze7zzsaw_jvm_codez00:
			if (NULLP(BgL_l1z00_2932))
				{	/* SawJvm/code.scm 150 */
					return ((bool_t) 1);
				}
			else
				{	/* SawJvm/code.scm 150 */
					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
									((obj_t) BgL_l1z00_2932)), BgL_l2z00_2933)))
						{
							obj_t BgL_l1z00_6471;

							BgL_l1z00_6471 = CDR(((obj_t) BgL_l1z00_2932));
							BgL_l1z00_2932 = BgL_l1z00_6471;
							goto BGl_includeze70ze7zzsaw_jvm_codez00;
						}
					else
						{	/* SawJvm/code.scm 151 */
							return ((bool_t) 0);
						}
				}
		}

	}



/* fixpoint~0 */
	bool_t BGl_fixpointze70ze7zzsaw_jvm_codez00(obj_t BgL_bz00_2982,
		obj_t BgL_livez00_2983)
	{
		{	/* SawJvm/code.scm 184 */
			{
				obj_t BgL_l1z00_2940;
				obj_t BgL_l2z00_2941;

				{
					BgL_liveblockz00_bglt BgL_auxz00_6474;

					{
						obj_t BgL_auxz00_6475;

						{	/* SawJvm/code.scm 176 */
							BgL_objectz00_bglt BgL_tmpz00_6476;

							BgL_tmpz00_6476 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2982));
							BgL_auxz00_6475 = BGL_OBJECT_WIDENING(BgL_tmpz00_6476);
						}
						BgL_auxz00_6474 = ((BgL_liveblockz00_bglt) BgL_auxz00_6475);
					}
					((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_6474))->BgL_outz00) =
						((obj_t) BgL_livez00_2983), BUNSPEC);
				}
				{	/* SawJvm/code.scm 177 */
					obj_t BgL_nlivez00_2986;

					{	/* SawJvm/code.scm 177 */
						obj_t BgL_arg1957z00_3001;

						BgL_arg1957z00_3001 =
							(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt)
										((BgL_blockz00_bglt) BgL_bz00_2982))))->BgL_firstz00);
						BgL_nlivez00_2986 =
							BGl_livezd2instrsze70z35zzsaw_jvm_codez00(BgL_arg1957z00_3001,
							BgL_livez00_2983);
					}
					{	/* SawJvm/code.scm 178 */
						bool_t BgL_test2677z00_6486;

						{	/* SawJvm/code.scm 178 */
							obj_t BgL_arg1956z00_3000;

							{
								BgL_liveblockz00_bglt BgL_auxz00_6487;

								{
									obj_t BgL_auxz00_6488;

									{	/* SawJvm/code.scm 178 */
										BgL_objectz00_bglt BgL_tmpz00_6489;

										BgL_tmpz00_6489 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt) BgL_bz00_2982));
										BgL_auxz00_6488 = BGL_OBJECT_WIDENING(BgL_tmpz00_6489);
									}
									BgL_auxz00_6487 = ((BgL_liveblockz00_bglt) BgL_auxz00_6488);
								}
								BgL_arg1956z00_3000 =
									(((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_6487))->
									BgL_inz00);
							}
							BgL_test2677z00_6486 =
								BGl_includeze70ze7zzsaw_jvm_codez00(BgL_nlivez00_2986,
								BgL_arg1956z00_3000);
						}
						if (BgL_test2677z00_6486)
							{	/* SawJvm/code.scm 178 */
								return ((bool_t) 0);
							}
						else
							{	/* SawJvm/code.scm 178 */
								{
									BgL_liveblockz00_bglt BgL_auxz00_6496;

									{
										obj_t BgL_auxz00_6497;

										{	/* SawJvm/code.scm 179 */
											BgL_objectz00_bglt BgL_tmpz00_6498;

											BgL_tmpz00_6498 =
												((BgL_objectz00_bglt)
												((BgL_blockz00_bglt) BgL_bz00_2982));
											BgL_auxz00_6497 = BGL_OBJECT_WIDENING(BgL_tmpz00_6498);
										}
										BgL_auxz00_6496 = ((BgL_liveblockz00_bglt) BgL_auxz00_6497);
									}
									((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_6496))->
											BgL_inz00) = ((obj_t) BgL_nlivez00_2986), BUNSPEC);
								}
								{	/* SawJvm/code.scm 180 */
									obj_t BgL_g1655z00_2989;

									BgL_g1655z00_2989 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt)
													((BgL_blockz00_bglt) BgL_bz00_2982))))->BgL_predsz00);
									{
										obj_t BgL_l1653z00_2991;

										BgL_l1653z00_2991 = BgL_g1655z00_2989;
									BgL_zc3z04anonymousza31951ze3z87_2992:
										if (PAIRP(BgL_l1653z00_2991))
											{	/* SawJvm/code.scm 180 */
												{	/* SawJvm/code.scm 181 */
													obj_t BgL_pz00_2994;

													BgL_pz00_2994 = CAR(BgL_l1653z00_2991);
													{	/* SawJvm/code.scm 181 */
														obj_t BgL_oz00_2995;

														{
															BgL_liveblockz00_bglt BgL_auxz00_6510;

															{
																obj_t BgL_auxz00_6511;

																{	/* SawJvm/code.scm 181 */
																	BgL_objectz00_bglt BgL_tmpz00_6512;

																	BgL_tmpz00_6512 =
																		((BgL_objectz00_bglt)
																		((BgL_blockz00_bglt) BgL_pz00_2994));
																	BgL_auxz00_6511 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_6512);
																}
																BgL_auxz00_6510 =
																	((BgL_liveblockz00_bglt) BgL_auxz00_6511);
															}
															BgL_oz00_2995 =
																(((BgL_liveblockz00_bglt)
																	COBJECT(BgL_auxz00_6510))->BgL_outz00);
														}
														if (BGl_includeze70ze7zzsaw_jvm_codez00
															(BgL_nlivez00_2986, BgL_oz00_2995))
															{	/* SawJvm/code.scm 182 */
																((bool_t) 0);
															}
														else
															{	/* SawJvm/code.scm 183 */
																obj_t BgL_auxz00_6520;

																BgL_l1z00_2940 = BgL_nlivez00_2986;
																BgL_l2z00_2941 = BgL_oz00_2995;
															BgL_zc3z04anonymousza31920ze3z87_2942:
																if (NULLP(BgL_l1z00_2940))
																	{	/* SawJvm/code.scm 154 */
																		BgL_auxz00_6520 = BgL_l2z00_2941;
																	}
																else
																	{	/* SawJvm/code.scm 154 */
																		if (CBOOL
																			(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(CAR(((obj_t) BgL_l1z00_2940)),
																					BgL_l2z00_2941)))
																			{	/* SawJvm/code.scm 155 */
																				obj_t BgL_arg1925z00_2946;

																				BgL_arg1925z00_2946 =
																					CDR(((obj_t) BgL_l1z00_2940));
																				{
																					obj_t BgL_l1z00_6530;

																					BgL_l1z00_6530 = BgL_arg1925z00_2946;
																					BgL_l1z00_2940 = BgL_l1z00_6530;
																					goto
																						BgL_zc3z04anonymousza31920ze3z87_2942;
																				}
																			}
																		else
																			{	/* SawJvm/code.scm 156 */
																				obj_t BgL_arg1926z00_2947;
																				obj_t BgL_arg1927z00_2948;

																				BgL_arg1926z00_2947 =
																					CDR(((obj_t) BgL_l1z00_2940));
																				{	/* SawJvm/code.scm 156 */
																					obj_t BgL_arg1928z00_2949;

																					BgL_arg1928z00_2949 =
																						CAR(((obj_t) BgL_l1z00_2940));
																					BgL_arg1927z00_2948 =
																						MAKE_YOUNG_PAIR(BgL_arg1928z00_2949,
																						BgL_l2z00_2941);
																				}
																				{
																					obj_t BgL_l2z00_6537;
																					obj_t BgL_l1z00_6536;

																					BgL_l1z00_6536 = BgL_arg1926z00_2947;
																					BgL_l2z00_6537 = BgL_arg1927z00_2948;
																					BgL_l2z00_2941 = BgL_l2z00_6537;
																					BgL_l1z00_2940 = BgL_l1z00_6536;
																					goto
																						BgL_zc3z04anonymousza31920ze3z87_2942;
																				}
																			}
																	}
																BGl_fixpointze70ze7zzsaw_jvm_codez00
																	(BgL_pz00_2994, BgL_auxz00_6520);
															}
													}
												}
												{
													obj_t BgL_l1653z00_6539;

													BgL_l1653z00_6539 = CDR(BgL_l1653z00_2991);
													BgL_l1653z00_2991 = BgL_l1653z00_6539;
													goto BgL_zc3z04anonymousza31951ze3z87_2992;
												}
											}
										else
											{	/* SawJvm/code.scm 180 */
												return ((bool_t) 1);
											}
									}
								}
							}
					}
				}
			}
		}

	}



/* live-reset */
	obj_t BGl_livezd2resetzd2zzsaw_jvm_codez00(BgL_jvmz00_bglt BgL_mez00_78,
		obj_t BgL_bz00_79, obj_t BgL_pendingz00_80)
	{
		{	/* SawJvm/code.scm 187 */
			{	/* SawJvm/code.scm 188 */
				obj_t BgL_labz00_3009;
				obj_t BgL_npendingz00_3010;

				{	/* SawJvm/code.scm 188 */

					{	/* SawJvm/code.scm 188 */

						BgL_labz00_3009 = BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
					}
				}
				BgL_npendingz00_3010 = BNIL;
				BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_78, BgL_labz00_3009);
				{
					obj_t BgL_l1656z00_3013;

					BgL_l1656z00_3013 = BgL_pendingz00_80;
				BgL_zc3z04anonymousza31958ze3z87_3014:
					if (PAIRP(BgL_l1656z00_3013))
						{	/* SawJvm/code.scm 191 */
							{	/* SawJvm/code.scm 192 */
								obj_t BgL_pz00_3016;

								BgL_pz00_3016 = CAR(BgL_l1656z00_3013);
								{	/* SawJvm/code.scm 192 */
									bool_t BgL_test2683z00_6546;

									{	/* SawJvm/code.scm 192 */
										obj_t BgL_arg1965z00_3022;
										obj_t BgL_arg1966z00_3023;

										BgL_arg1965z00_3022 = CAR(((obj_t) BgL_pz00_3016));
										{
											BgL_liveblockz00_bglt BgL_auxz00_6549;

											{
												obj_t BgL_auxz00_6550;

												{	/* SawJvm/code.scm 192 */
													BgL_objectz00_bglt BgL_tmpz00_6551;

													BgL_tmpz00_6551 =
														((BgL_objectz00_bglt)
														((BgL_blockz00_bglt) BgL_bz00_79));
													BgL_auxz00_6550 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_6551);
												}
												BgL_auxz00_6549 =
													((BgL_liveblockz00_bglt) BgL_auxz00_6550);
											}
											BgL_arg1966z00_3023 =
												(((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_6549))->
												BgL_inz00);
										}
										BgL_test2683z00_6546 =
											CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1965z00_3022, BgL_arg1966z00_3023));
									}
									if (BgL_test2683z00_6546)
										{	/* SawJvm/code.scm 192 */
											BgL_npendingz00_3010 =
												MAKE_YOUNG_PAIR(BgL_pz00_3016, BgL_npendingz00_3010);
										}
									else
										{	/* SawJvm/code.scm 194 */
											obj_t BgL_arg1963z00_3020;
											obj_t BgL_arg1964z00_3021;

											BgL_arg1963z00_3020 = CAR(((obj_t) BgL_pz00_3016));
											BgL_arg1964z00_3021 = CDR(((obj_t) BgL_pz00_3016));
											{	/* SawJvm/code.scm 205 */
												obj_t BgL_arg1973z00_4538;

												{
													BgL_lregz00_bglt BgL_auxz00_6564;

													{
														obj_t BgL_auxz00_6565;

														{	/* SawJvm/code.scm 205 */
															BgL_objectz00_bglt BgL_tmpz00_6566;

															BgL_tmpz00_6566 =
																((BgL_objectz00_bglt)
																((BgL_rtl_regz00_bglt) BgL_arg1963z00_3020));
															BgL_auxz00_6565 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_6566);
														}
														BgL_auxz00_6564 =
															((BgL_lregz00_bglt) BgL_auxz00_6565);
													}
													BgL_arg1973z00_4538 =
														(((BgL_lregz00_bglt) COBJECT(BgL_auxz00_6564))->
														BgL_idz00);
												}
												BGl_localvarz00zzsaw_jvm_outz00(BgL_mez00_78,
													BgL_arg1963z00_3020, BgL_arg1964z00_3021,
													BgL_labz00_3009, BgL_arg1973z00_4538);
											}
										}
								}
							}
							{
								obj_t BgL_l1656z00_6573;

								BgL_l1656z00_6573 = CDR(BgL_l1656z00_3013);
								BgL_l1656z00_3013 = BgL_l1656z00_6573;
								goto BgL_zc3z04anonymousza31958ze3z87_3014;
							}
						}
					else
						{	/* SawJvm/code.scm 191 */
							((bool_t) 1);
						}
				}
				{	/* SawJvm/code.scm 196 */
					obj_t BgL_g1660z00_3026;

					{
						BgL_liveblockz00_bglt BgL_auxz00_6575;

						{
							obj_t BgL_auxz00_6576;

							{	/* SawJvm/code.scm 196 */
								BgL_objectz00_bglt BgL_tmpz00_6577;

								BgL_tmpz00_6577 =
									((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_79));
								BgL_auxz00_6576 = BGL_OBJECT_WIDENING(BgL_tmpz00_6577);
							}
							BgL_auxz00_6575 = ((BgL_liveblockz00_bglt) BgL_auxz00_6576);
						}
						BgL_g1660z00_3026 =
							(((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_6575))->BgL_inz00);
					}
					{
						obj_t BgL_l1658z00_3028;

						BgL_l1658z00_3028 = BgL_g1660z00_3026;
					BgL_zc3z04anonymousza31968ze3z87_3029:
						if (PAIRP(BgL_l1658z00_3028))
							{	/* SawJvm/code.scm 196 */
								{	/* SawJvm/code.scm 197 */
									obj_t BgL_iz00_3031;

									BgL_iz00_3031 = CAR(BgL_l1658z00_3028);
									if (CBOOL(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_iz00_3031, BgL_npendingz00_3010)))
										{	/* SawJvm/code.scm 197 */
											BFALSE;
										}
									else
										{	/* SawJvm/code.scm 198 */
											obj_t BgL_arg1971z00_3033;

											BgL_arg1971z00_3033 =
												MAKE_YOUNG_PAIR(BgL_iz00_3031, BgL_labz00_3009);
											BgL_npendingz00_3010 =
												MAKE_YOUNG_PAIR(BgL_arg1971z00_3033,
												BgL_npendingz00_3010);
										}
								}
								{
									obj_t BgL_l1658z00_6591;

									BgL_l1658z00_6591 = CDR(BgL_l1658z00_3028);
									BgL_l1658z00_3028 = BgL_l1658z00_6591;
									goto BgL_zc3z04anonymousza31968ze3z87_3029;
								}
							}
						else
							{	/* SawJvm/code.scm 196 */
								((bool_t) 1);
							}
					}
				}
				return BgL_npendingz00_3010;
			}
		}

	}



/* gen-ins */
	obj_t BGl_genzd2inszd2zzsaw_jvm_codez00(BgL_jvmz00_bglt BgL_mez00_85,
		obj_t BgL_insz00_86, obj_t BgL_pz00_87)
	{
		{	/* SawJvm/code.scm 210 */
			{	/* SawJvm/code.scm 213 */
				bool_t BgL_test2686z00_6593;

				{	/* SawJvm/code.scm 213 */
					obj_t BgL_arg1995z00_3064;

					{	/* SawJvm/code.scm 213 */
						BgL_rtl_funz00_bglt BgL_arg1996z00_3065;
						obj_t BgL_arg1997z00_3066;

						BgL_arg1996z00_3065 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_funz00);
						BgL_arg1997z00_3066 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_argsz00);
						BgL_arg1995z00_3064 =
							BGl_genzd2argszd2genzd2funzd2zzsaw_jvm_codez00
							(BgL_arg1996z00_3065, ((obj_t) BgL_mez00_85),
							BgL_arg1997z00_3066);
					}
					BgL_test2686z00_6593 = (BgL_arg1995z00_3064 == CNST_TABLE_REF(13));
				}
				if (BgL_test2686z00_6593)
					{	/* SawJvm/code.scm 213 */
						if (CBOOL(
								(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_destz00)))
							{	/* SawJvm/code.scm 214 */
								BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_85,
									CNST_TABLE_REF(14));
								{	/* SawJvm/code.scm 216 */
									obj_t BgL_arg1979z00_3045;

									BgL_arg1979z00_3045 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_destz00);
									BGl_storezd2regzd2zzsaw_jvm_codez00(BgL_mez00_85,
										BgL_arg1979z00_3045);
								}
							}
						else
							{	/* SawJvm/code.scm 214 */
								BFALSE;
							}
					}
				else
					{	/* SawJvm/code.scm 213 */
						if (CBOOL(
								(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_destz00)))
							{	/* SawJvm/code.scm 218 */
								obj_t BgL_arg1981z00_3047;

								BgL_arg1981z00_3047 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_destz00);
								BGl_storezd2regzd2zzsaw_jvm_codez00(BgL_mez00_85,
									BgL_arg1981z00_3047);
							}
						else
							{	/* SawJvm/code.scm 220 */
								bool_t BgL_test2689z00_6618;

								{	/* SawJvm/code.scm 220 */
									bool_t BgL_test2690z00_6619;

									{	/* SawJvm/code.scm 220 */
										BgL_rtl_funz00_bglt BgL_arg1994z00_3063;

										BgL_arg1994z00_3063 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_funz00);
										{	/* SawJvm/code.scm 220 */
											obj_t BgL_classz00_4547;

											BgL_classz00_4547 = BGl_rtl_lastz00zzsaw_defsz00;
											{	/* SawJvm/code.scm 220 */
												BgL_objectz00_bglt BgL_arg1807z00_4549;

												{	/* SawJvm/code.scm 220 */
													obj_t BgL_tmpz00_6622;

													BgL_tmpz00_6622 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1994z00_3063));
													BgL_arg1807z00_4549 =
														(BgL_objectz00_bglt) (BgL_tmpz00_6622);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawJvm/code.scm 220 */
														long BgL_idxz00_4555;

														BgL_idxz00_4555 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4549);
														BgL_test2690z00_6619 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_4555 + 2L)) == BgL_classz00_4547);
													}
												else
													{	/* SawJvm/code.scm 220 */
														bool_t BgL_res2439z00_4580;

														{	/* SawJvm/code.scm 220 */
															obj_t BgL_oclassz00_4563;

															{	/* SawJvm/code.scm 220 */
																obj_t BgL_arg1815z00_4571;
																long BgL_arg1816z00_4572;

																BgL_arg1815z00_4571 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawJvm/code.scm 220 */
																	long BgL_arg1817z00_4573;

																	BgL_arg1817z00_4573 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4549);
																	BgL_arg1816z00_4572 =
																		(BgL_arg1817z00_4573 - OBJECT_TYPE);
																}
																BgL_oclassz00_4563 =
																	VECTOR_REF(BgL_arg1815z00_4571,
																	BgL_arg1816z00_4572);
															}
															{	/* SawJvm/code.scm 220 */
																bool_t BgL__ortest_1115z00_4564;

																BgL__ortest_1115z00_4564 =
																	(BgL_classz00_4547 == BgL_oclassz00_4563);
																if (BgL__ortest_1115z00_4564)
																	{	/* SawJvm/code.scm 220 */
																		BgL_res2439z00_4580 =
																			BgL__ortest_1115z00_4564;
																	}
																else
																	{	/* SawJvm/code.scm 220 */
																		long BgL_odepthz00_4565;

																		{	/* SawJvm/code.scm 220 */
																			obj_t BgL_arg1804z00_4566;

																			BgL_arg1804z00_4566 =
																				(BgL_oclassz00_4563);
																			BgL_odepthz00_4565 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_4566);
																		}
																		if ((2L < BgL_odepthz00_4565))
																			{	/* SawJvm/code.scm 220 */
																				obj_t BgL_arg1802z00_4568;

																				{	/* SawJvm/code.scm 220 */
																					obj_t BgL_arg1803z00_4569;

																					BgL_arg1803z00_4569 =
																						(BgL_oclassz00_4563);
																					BgL_arg1802z00_4568 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_4569, 2L);
																				}
																				BgL_res2439z00_4580 =
																					(BgL_arg1802z00_4568 ==
																					BgL_classz00_4547);
																			}
																		else
																			{	/* SawJvm/code.scm 220 */
																				BgL_res2439z00_4580 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2690z00_6619 = BgL_res2439z00_4580;
													}
											}
										}
									}
									if (BgL_test2690z00_6619)
										{	/* SawJvm/code.scm 220 */
											BgL_test2689z00_6618 = ((bool_t) 1);
										}
									else
										{	/* SawJvm/code.scm 221 */
											bool_t BgL_test2694z00_6645;

											{	/* SawJvm/code.scm 221 */
												BgL_rtl_funz00_bglt BgL_arg1993z00_3062;

												BgL_arg1993z00_3062 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_insz00_86)))->
													BgL_funz00);
												{	/* SawJvm/code.scm 221 */
													obj_t BgL_classz00_4581;

													BgL_classz00_4581 = BGl_rtl_notseqz00zzsaw_defsz00;
													{	/* SawJvm/code.scm 221 */
														BgL_objectz00_bglt BgL_arg1807z00_4583;

														{	/* SawJvm/code.scm 221 */
															obj_t BgL_tmpz00_6648;

															BgL_tmpz00_6648 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg1993z00_3062));
															BgL_arg1807z00_4583 =
																(BgL_objectz00_bglt) (BgL_tmpz00_6648);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawJvm/code.scm 221 */
																long BgL_idxz00_4589;

																BgL_idxz00_4589 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4583);
																BgL_test2694z00_6645 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4589 + 2L)) ==
																	BgL_classz00_4581);
															}
														else
															{	/* SawJvm/code.scm 221 */
																bool_t BgL_res2440z00_4614;

																{	/* SawJvm/code.scm 221 */
																	obj_t BgL_oclassz00_4597;

																	{	/* SawJvm/code.scm 221 */
																		obj_t BgL_arg1815z00_4605;
																		long BgL_arg1816z00_4606;

																		BgL_arg1815z00_4605 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawJvm/code.scm 221 */
																			long BgL_arg1817z00_4607;

																			BgL_arg1817z00_4607 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4583);
																			BgL_arg1816z00_4606 =
																				(BgL_arg1817z00_4607 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4597 =
																			VECTOR_REF(BgL_arg1815z00_4605,
																			BgL_arg1816z00_4606);
																	}
																	{	/* SawJvm/code.scm 221 */
																		bool_t BgL__ortest_1115z00_4598;

																		BgL__ortest_1115z00_4598 =
																			(BgL_classz00_4581 == BgL_oclassz00_4597);
																		if (BgL__ortest_1115z00_4598)
																			{	/* SawJvm/code.scm 221 */
																				BgL_res2440z00_4614 =
																					BgL__ortest_1115z00_4598;
																			}
																		else
																			{	/* SawJvm/code.scm 221 */
																				long BgL_odepthz00_4599;

																				{	/* SawJvm/code.scm 221 */
																					obj_t BgL_arg1804z00_4600;

																					BgL_arg1804z00_4600 =
																						(BgL_oclassz00_4597);
																					BgL_odepthz00_4599 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4600);
																				}
																				if ((2L < BgL_odepthz00_4599))
																					{	/* SawJvm/code.scm 221 */
																						obj_t BgL_arg1802z00_4602;

																						{	/* SawJvm/code.scm 221 */
																							obj_t BgL_arg1803z00_4603;

																							BgL_arg1803z00_4603 =
																								(BgL_oclassz00_4597);
																							BgL_arg1802z00_4602 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4603, 2L);
																						}
																						BgL_res2440z00_4614 =
																							(BgL_arg1802z00_4602 ==
																							BgL_classz00_4581);
																					}
																				else
																					{	/* SawJvm/code.scm 221 */
																						BgL_res2440z00_4614 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2694z00_6645 = BgL_res2440z00_4614;
															}
													}
												}
											}
											if (BgL_test2694z00_6645)
												{	/* SawJvm/code.scm 221 */
													BgL_test2689z00_6618 = ((bool_t) 1);
												}
											else
												{	/* SawJvm/code.scm 222 */
													BgL_rtl_funz00_bglt BgL_arg1992z00_3061;

													BgL_arg1992z00_3061 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_insz00_86)))->
														BgL_funz00);
													{	/* SawJvm/code.scm 222 */
														obj_t BgL_classz00_4615;

														BgL_classz00_4615 = BGl_rtl_effectz00zzsaw_defsz00;
														{	/* SawJvm/code.scm 222 */
															BgL_objectz00_bglt BgL_arg1807z00_4617;

															{	/* SawJvm/code.scm 222 */
																obj_t BgL_tmpz00_6673;

																BgL_tmpz00_6673 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_arg1992z00_3061));
																BgL_arg1807z00_4617 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_6673);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* SawJvm/code.scm 222 */
																	long BgL_idxz00_4623;

																	BgL_idxz00_4623 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4617);
																	BgL_test2689z00_6618 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4623 + 2L)) ==
																		BgL_classz00_4615);
																}
															else
																{	/* SawJvm/code.scm 222 */
																	bool_t BgL_res2441z00_4648;

																	{	/* SawJvm/code.scm 222 */
																		obj_t BgL_oclassz00_4631;

																		{	/* SawJvm/code.scm 222 */
																			obj_t BgL_arg1815z00_4639;
																			long BgL_arg1816z00_4640;

																			BgL_arg1815z00_4639 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawJvm/code.scm 222 */
																				long BgL_arg1817z00_4641;

																				BgL_arg1817z00_4641 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4617);
																				BgL_arg1816z00_4640 =
																					(BgL_arg1817z00_4641 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4631 =
																				VECTOR_REF(BgL_arg1815z00_4639,
																				BgL_arg1816z00_4640);
																		}
																		{	/* SawJvm/code.scm 222 */
																			bool_t BgL__ortest_1115z00_4632;

																			BgL__ortest_1115z00_4632 =
																				(BgL_classz00_4615 ==
																				BgL_oclassz00_4631);
																			if (BgL__ortest_1115z00_4632)
																				{	/* SawJvm/code.scm 222 */
																					BgL_res2441z00_4648 =
																						BgL__ortest_1115z00_4632;
																				}
																			else
																				{	/* SawJvm/code.scm 222 */
																					long BgL_odepthz00_4633;

																					{	/* SawJvm/code.scm 222 */
																						obj_t BgL_arg1804z00_4634;

																						BgL_arg1804z00_4634 =
																							(BgL_oclassz00_4631);
																						BgL_odepthz00_4633 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4634);
																					}
																					if ((2L < BgL_odepthz00_4633))
																						{	/* SawJvm/code.scm 222 */
																							obj_t BgL_arg1802z00_4636;

																							{	/* SawJvm/code.scm 222 */
																								obj_t BgL_arg1803z00_4637;

																								BgL_arg1803z00_4637 =
																									(BgL_oclassz00_4631);
																								BgL_arg1802z00_4636 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4637, 2L);
																							}
																							BgL_res2441z00_4648 =
																								(BgL_arg1802z00_4636 ==
																								BgL_classz00_4615);
																						}
																					else
																						{	/* SawJvm/code.scm 222 */
																							BgL_res2441z00_4648 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2689z00_6618 = BgL_res2441z00_4648;
																}
														}
													}
												}
										}
								}
								if (BgL_test2689z00_6618)
									{	/* SawJvm/code.scm 220 */
										BFALSE;
									}
								else
									{	/* SawJvm/code.scm 223 */
										int BgL_nz00_3056;

										BgL_nz00_3056 =
											BGl_siza7ezd2destz75zzsaw_jvm_typez00(BgL_insz00_86);
										if (((long) (BgL_nz00_3056) > 0L))
											{	/* SawJvm/code.scm 224 */
												if (((long) (BgL_nz00_3056) > 1L))
													{	/* SawJvm/code.scm 225 */
														BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_85,
															CNST_TABLE_REF(15));
													}
												else
													{	/* SawJvm/code.scm 225 */
														BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_85,
															CNST_TABLE_REF(16));
													}
											}
										else
											{	/* SawJvm/code.scm 224 */
												BFALSE;
											}
									}
							}
					}
			}
			{	/* SawJvm/code.scm 228 */
				bool_t BgL_test2703z00_6707;

				{	/* SawJvm/code.scm 228 */
					BgL_rtl_funz00_bglt BgL_arg2000z00_3069;

					BgL_arg2000z00_3069 =
						(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_funz00);
					{	/* SawJvm/code.scm 228 */
						obj_t BgL_classz00_4651;

						BgL_classz00_4651 = BGl_rtl_protectz00zzsaw_defsz00;
						{	/* SawJvm/code.scm 228 */
							BgL_objectz00_bglt BgL_arg1807z00_4653;

							{	/* SawJvm/code.scm 228 */
								obj_t BgL_tmpz00_6710;

								BgL_tmpz00_6710 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg2000z00_3069));
								BgL_arg1807z00_4653 = (BgL_objectz00_bglt) (BgL_tmpz00_6710);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/code.scm 228 */
									long BgL_idxz00_4659;

									BgL_idxz00_4659 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4653);
									BgL_test2703z00_6707 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4659 + 2L)) == BgL_classz00_4651);
								}
							else
								{	/* SawJvm/code.scm 228 */
									bool_t BgL_res2442z00_4684;

									{	/* SawJvm/code.scm 228 */
										obj_t BgL_oclassz00_4667;

										{	/* SawJvm/code.scm 228 */
											obj_t BgL_arg1815z00_4675;
											long BgL_arg1816z00_4676;

											BgL_arg1815z00_4675 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/code.scm 228 */
												long BgL_arg1817z00_4677;

												BgL_arg1817z00_4677 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4653);
												BgL_arg1816z00_4676 =
													(BgL_arg1817z00_4677 - OBJECT_TYPE);
											}
											BgL_oclassz00_4667 =
												VECTOR_REF(BgL_arg1815z00_4675, BgL_arg1816z00_4676);
										}
										{	/* SawJvm/code.scm 228 */
											bool_t BgL__ortest_1115z00_4668;

											BgL__ortest_1115z00_4668 =
												(BgL_classz00_4651 == BgL_oclassz00_4667);
											if (BgL__ortest_1115z00_4668)
												{	/* SawJvm/code.scm 228 */
													BgL_res2442z00_4684 = BgL__ortest_1115z00_4668;
												}
											else
												{	/* SawJvm/code.scm 228 */
													long BgL_odepthz00_4669;

													{	/* SawJvm/code.scm 228 */
														obj_t BgL_arg1804z00_4670;

														BgL_arg1804z00_4670 = (BgL_oclassz00_4667);
														BgL_odepthz00_4669 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4670);
													}
													if ((2L < BgL_odepthz00_4669))
														{	/* SawJvm/code.scm 228 */
															obj_t BgL_arg1802z00_4672;

															{	/* SawJvm/code.scm 228 */
																obj_t BgL_arg1803z00_4673;

																BgL_arg1803z00_4673 = (BgL_oclassz00_4667);
																BgL_arg1802z00_4672 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4673,
																	2L);
															}
															BgL_res2442z00_4684 =
																(BgL_arg1802z00_4672 == BgL_classz00_4651);
														}
													else
														{	/* SawJvm/code.scm 228 */
															BgL_res2442z00_4684 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2703z00_6707 = BgL_res2442z00_4684;
								}
						}
					}
				}
				if (BgL_test2703z00_6707)
					{	/* SawJvm/code.scm 228 */
						BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_85, CNST_TABLE_REF(17));
					}
				else
					{	/* SawJvm/code.scm 228 */
						BFALSE;
					}
			}
			{	/* SawJvm/code.scm 229 */
				bool_t BgL_test2707z00_6735;

				if (CBOOL(BGl_za2jvmzd2debugza2zd2zzengine_paramz00))
					{	/* SawJvm/code.scm 229 */
						BgL_test2707z00_6735 =
							CBOOL(
							(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_destz00));
					}
				else
					{	/* SawJvm/code.scm 229 */
						BgL_test2707z00_6735 = ((bool_t) 0);
					}
				if (BgL_test2707z00_6735)
					{	/* SawJvm/code.scm 230 */
						bool_t BgL_test2709z00_6741;

						{	/* SawJvm/code.scm 230 */
							obj_t BgL_arg2007z00_3077;

							BgL_arg2007z00_3077 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_destz00);
							BgL_test2709z00_6741 =
								CBOOL(BGl_assqz00zz__r4_pairs_and_lists_6_3z00
								(BgL_arg2007z00_3077, BgL_pz00_87));
						}
						if (BgL_test2709z00_6741)
							{	/* SawJvm/code.scm 230 */
								BFALSE;
							}
						else
							{	/* SawJvm/code.scm 231 */
								obj_t BgL_labz00_3073;

								{	/* SawJvm/code.scm 231 */

									{	/* SawJvm/code.scm 231 */

										BgL_labz00_3073 =
											BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
									}
								}
								BGl_labelz00zzsaw_jvm_outz00(BgL_mez00_85, BgL_labz00_3073);
								{	/* SawJvm/code.scm 233 */
									obj_t BgL_arg2004z00_3074;

									{	/* SawJvm/code.scm 233 */
										obj_t BgL_arg2006z00_3075;

										BgL_arg2006z00_3075 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_86)))->BgL_destz00);
										BgL_arg2004z00_3074 =
											MAKE_YOUNG_PAIR(BgL_arg2006z00_3075, BgL_labz00_3073);
									}
									BgL_pz00_87 =
										MAKE_YOUNG_PAIR(BgL_arg2004z00_3074, BgL_pz00_87);
								}
							}
					}
				else
					{	/* SawJvm/code.scm 229 */
						BFALSE;
					}
			}
			return BgL_pz00_87;
		}

	}



/* gen-expr */
	BGL_EXPORTED_DEF obj_t BGl_genzd2exprzd2zzsaw_jvm_codez00(BgL_jvmz00_bglt
		BgL_mez00_88, obj_t BgL_insz00_89)
	{
		{	/* SawJvm/code.scm 236 */
			{	/* SawJvm/code.scm 237 */
				bool_t BgL_test2710z00_6752;

				{	/* SawJvm/code.scm 237 */
					obj_t BgL_classz00_4685;

					BgL_classz00_4685 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_insz00_89))
						{	/* SawJvm/code.scm 237 */
							BgL_objectz00_bglt BgL_arg1807z00_4687;

							BgL_arg1807z00_4687 = (BgL_objectz00_bglt) (BgL_insz00_89);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/code.scm 237 */
									long BgL_idxz00_4693;

									BgL_idxz00_4693 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4687);
									BgL_test2710z00_6752 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4693 + 1L)) == BgL_classz00_4685);
								}
							else
								{	/* SawJvm/code.scm 237 */
									bool_t BgL_res2443z00_4718;

									{	/* SawJvm/code.scm 237 */
										obj_t BgL_oclassz00_4701;

										{	/* SawJvm/code.scm 237 */
											obj_t BgL_arg1815z00_4709;
											long BgL_arg1816z00_4710;

											BgL_arg1815z00_4709 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/code.scm 237 */
												long BgL_arg1817z00_4711;

												BgL_arg1817z00_4711 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4687);
												BgL_arg1816z00_4710 =
													(BgL_arg1817z00_4711 - OBJECT_TYPE);
											}
											BgL_oclassz00_4701 =
												VECTOR_REF(BgL_arg1815z00_4709, BgL_arg1816z00_4710);
										}
										{	/* SawJvm/code.scm 237 */
											bool_t BgL__ortest_1115z00_4702;

											BgL__ortest_1115z00_4702 =
												(BgL_classz00_4685 == BgL_oclassz00_4701);
											if (BgL__ortest_1115z00_4702)
												{	/* SawJvm/code.scm 237 */
													BgL_res2443z00_4718 = BgL__ortest_1115z00_4702;
												}
											else
												{	/* SawJvm/code.scm 237 */
													long BgL_odepthz00_4703;

													{	/* SawJvm/code.scm 237 */
														obj_t BgL_arg1804z00_4704;

														BgL_arg1804z00_4704 = (BgL_oclassz00_4701);
														BgL_odepthz00_4703 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4704);
													}
													if ((1L < BgL_odepthz00_4703))
														{	/* SawJvm/code.scm 237 */
															obj_t BgL_arg1802z00_4706;

															{	/* SawJvm/code.scm 237 */
																obj_t BgL_arg1803z00_4707;

																BgL_arg1803z00_4707 = (BgL_oclassz00_4701);
																BgL_arg1802z00_4706 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4707,
																	1L);
															}
															BgL_res2443z00_4718 =
																(BgL_arg1802z00_4706 == BgL_classz00_4685);
														}
													else
														{	/* SawJvm/code.scm 237 */
															BgL_res2443z00_4718 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2710z00_6752 = BgL_res2443z00_4718;
								}
						}
					else
						{	/* SawJvm/code.scm 237 */
							BgL_test2710z00_6752 = ((bool_t) 0);
						}
				}
				if (BgL_test2710z00_6752)
					{	/* SawJvm/code.scm 237 */
						return
							BGl_loadzd2regzd2zzsaw_jvm_codez00(
							((obj_t) BgL_mez00_88), BgL_insz00_89);
					}
				else
					{	/* SawJvm/code.scm 240 */
						bool_t BgL_test2715z00_6777;

						{	/* SawJvm/code.scm 240 */
							obj_t BgL_arg2013z00_3084;

							{	/* SawJvm/code.scm 240 */
								BgL_rtl_funz00_bglt BgL_arg2014z00_3085;
								obj_t BgL_arg2015z00_3086;

								BgL_arg2014z00_3085 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_89)))->BgL_funz00);
								BgL_arg2015z00_3086 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_89)))->BgL_argsz00);
								BgL_arg2013z00_3084 =
									BGl_genzd2argszd2genzd2funzd2zzsaw_jvm_codez00
									(BgL_arg2014z00_3085, ((obj_t) BgL_mez00_88),
									BgL_arg2015z00_3086);
							}
							BgL_test2715z00_6777 =
								(BgL_arg2013z00_3084 == CNST_TABLE_REF(13));
						}
						if (BgL_test2715z00_6777)
							{	/* SawJvm/code.scm 240 */
								BGL_TAIL return
									BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_88,
									CNST_TABLE_REF(14));
							}
						else
							{	/* SawJvm/code.scm 240 */
								return BFALSE;
							}
					}
			}
		}

	}



/* &gen-expr */
	obj_t BGl_z62genzd2exprzb0zzsaw_jvm_codez00(obj_t BgL_envz00_5390,
		obj_t BgL_mez00_5391, obj_t BgL_insz00_5392)
	{
		{	/* SawJvm/code.scm 236 */
			return
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
				((BgL_jvmz00_bglt) BgL_mez00_5391), BgL_insz00_5392);
		}

	}



/* load-reg */
	obj_t BGl_loadzd2regzd2zzsaw_jvm_codez00(obj_t BgL_mez00_90,
		obj_t BgL_rz00_91)
	{
		{	/* SawJvm/code.scm 243 */
			{	/* SawJvm/code.scm 245 */
				obj_t BgL_arg2016z00_3087;

				{	/* SawJvm/code.scm 245 */
					obj_t BgL_arg2017z00_3088;
					obj_t BgL_arg2018z00_3089;

					{	/* SawJvm/code.scm 245 */
						obj_t BgL_casezd2valuezd2_3090;

						BgL_casezd2valuezd2_3090 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_rtl_regz00_bglt) COBJECT(
												((BgL_rtl_regz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_rz00_91))))->
										BgL_typez00)))->BgL_namez00);
						{	/* SawJvm/code.scm 245 */
							bool_t BgL_test2716z00_6794;

							{	/* SawJvm/code.scm 245 */
								bool_t BgL__ortest_1215z00_3100;

								BgL__ortest_1215z00_3100 =
									(BgL_casezd2valuezd2_3090 == CNST_TABLE_REF(18));
								if (BgL__ortest_1215z00_3100)
									{	/* SawJvm/code.scm 245 */
										BgL_test2716z00_6794 = BgL__ortest_1215z00_3100;
									}
								else
									{	/* SawJvm/code.scm 245 */
										bool_t BgL__ortest_1216z00_3101;

										BgL__ortest_1216z00_3101 =
											(BgL_casezd2valuezd2_3090 == CNST_TABLE_REF(19));
										if (BgL__ortest_1216z00_3101)
											{	/* SawJvm/code.scm 245 */
												BgL_test2716z00_6794 = BgL__ortest_1216z00_3101;
											}
										else
											{	/* SawJvm/code.scm 245 */
												bool_t BgL__ortest_1217z00_3102;

												BgL__ortest_1217z00_3102 =
													(BgL_casezd2valuezd2_3090 == CNST_TABLE_REF(20));
												if (BgL__ortest_1217z00_3102)
													{	/* SawJvm/code.scm 245 */
														BgL_test2716z00_6794 = BgL__ortest_1217z00_3102;
													}
												else
													{	/* SawJvm/code.scm 245 */
														bool_t BgL__ortest_1218z00_3103;

														BgL__ortest_1218z00_3103 =
															(BgL_casezd2valuezd2_3090 == CNST_TABLE_REF(21));
														if (BgL__ortest_1218z00_3103)
															{	/* SawJvm/code.scm 245 */
																BgL_test2716z00_6794 = BgL__ortest_1218z00_3103;
															}
														else
															{	/* SawJvm/code.scm 245 */
																BgL_test2716z00_6794 =
																	(BgL_casezd2valuezd2_3090 ==
																	CNST_TABLE_REF(22));
															}
													}
											}
									}
							}
							if (BgL_test2716z00_6794)
								{	/* SawJvm/code.scm 245 */
									BgL_arg2017z00_3088 = CNST_TABLE_REF(23);
								}
							else
								{	/* SawJvm/code.scm 245 */
									if ((BgL_casezd2valuezd2_3090 == CNST_TABLE_REF(11)))
										{	/* SawJvm/code.scm 245 */
											BgL_arg2017z00_3088 = CNST_TABLE_REF(24);
										}
									else
										{	/* SawJvm/code.scm 245 */
											if ((BgL_casezd2valuezd2_3090 == CNST_TABLE_REF(25)))
												{	/* SawJvm/code.scm 245 */
													BgL_arg2017z00_3088 = CNST_TABLE_REF(26);
												}
											else
												{	/* SawJvm/code.scm 245 */
													if ((BgL_casezd2valuezd2_3090 == CNST_TABLE_REF(10)))
														{	/* SawJvm/code.scm 245 */
															BgL_arg2017z00_3088 = CNST_TABLE_REF(27);
														}
													else
														{	/* SawJvm/code.scm 245 */
															BgL_arg2017z00_3088 = CNST_TABLE_REF(28);
														}
												}
										}
								}
						}
					}
					{	/* SawJvm/code.scm 251 */
						obj_t BgL_arg2025z00_3105;

						{
							BgL_lregz00_bglt BgL_auxz00_6823;

							{
								obj_t BgL_auxz00_6824;

								{	/* SawJvm/code.scm 251 */
									BgL_objectz00_bglt BgL_tmpz00_6825;

									BgL_tmpz00_6825 =
										((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_91));
									BgL_auxz00_6824 = BGL_OBJECT_WIDENING(BgL_tmpz00_6825);
								}
								BgL_auxz00_6823 = ((BgL_lregz00_bglt) BgL_auxz00_6824);
							}
							BgL_arg2025z00_3105 =
								(((BgL_lregz00_bglt) COBJECT(BgL_auxz00_6823))->BgL_idz00);
						}
						BgL_arg2018z00_3089 = MAKE_YOUNG_PAIR(BgL_arg2025z00_3105, BNIL);
					}
					BgL_arg2016z00_3087 =
						MAKE_YOUNG_PAIR(BgL_arg2017z00_3088, BgL_arg2018z00_3089);
				}
				return
					BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_90), BgL_arg2016z00_3087);
			}
		}

	}



/* store-reg */
	obj_t BGl_storezd2regzd2zzsaw_jvm_codez00(BgL_jvmz00_bglt BgL_mez00_92,
		obj_t BgL_rz00_93)
	{
		{	/* SawJvm/code.scm 253 */
			{	/* SawJvm/code.scm 255 */
				obj_t BgL_arg2026z00_3106;

				{	/* SawJvm/code.scm 255 */
					obj_t BgL_arg2027z00_3107;
					obj_t BgL_arg2029z00_3108;

					{	/* SawJvm/code.scm 255 */
						obj_t BgL_casezd2valuezd2_3109;

						BgL_casezd2valuezd2_3109 =
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_rtl_regz00_bglt) COBJECT(
												((BgL_rtl_regz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_rz00_93))))->
										BgL_typez00)))->BgL_namez00);
						{	/* SawJvm/code.scm 255 */
							bool_t BgL_test2724z00_6839;

							{	/* SawJvm/code.scm 255 */
								bool_t BgL__ortest_1219z00_3119;

								BgL__ortest_1219z00_3119 =
									(BgL_casezd2valuezd2_3109 == CNST_TABLE_REF(18));
								if (BgL__ortest_1219z00_3119)
									{	/* SawJvm/code.scm 255 */
										BgL_test2724z00_6839 = BgL__ortest_1219z00_3119;
									}
								else
									{	/* SawJvm/code.scm 255 */
										bool_t BgL__ortest_1220z00_3120;

										BgL__ortest_1220z00_3120 =
											(BgL_casezd2valuezd2_3109 == CNST_TABLE_REF(19));
										if (BgL__ortest_1220z00_3120)
											{	/* SawJvm/code.scm 255 */
												BgL_test2724z00_6839 = BgL__ortest_1220z00_3120;
											}
										else
											{	/* SawJvm/code.scm 255 */
												bool_t BgL__ortest_1221z00_3121;

												BgL__ortest_1221z00_3121 =
													(BgL_casezd2valuezd2_3109 == CNST_TABLE_REF(20));
												if (BgL__ortest_1221z00_3121)
													{	/* SawJvm/code.scm 255 */
														BgL_test2724z00_6839 = BgL__ortest_1221z00_3121;
													}
												else
													{	/* SawJvm/code.scm 255 */
														bool_t BgL__ortest_1222z00_3122;

														BgL__ortest_1222z00_3122 =
															(BgL_casezd2valuezd2_3109 == CNST_TABLE_REF(21));
														if (BgL__ortest_1222z00_3122)
															{	/* SawJvm/code.scm 255 */
																BgL_test2724z00_6839 = BgL__ortest_1222z00_3122;
															}
														else
															{	/* SawJvm/code.scm 255 */
																BgL_test2724z00_6839 =
																	(BgL_casezd2valuezd2_3109 ==
																	CNST_TABLE_REF(22));
															}
													}
											}
									}
							}
							if (BgL_test2724z00_6839)
								{	/* SawJvm/code.scm 255 */
									BgL_arg2027z00_3107 = CNST_TABLE_REF(29);
								}
							else
								{	/* SawJvm/code.scm 255 */
									if ((BgL_casezd2valuezd2_3109 == CNST_TABLE_REF(11)))
										{	/* SawJvm/code.scm 255 */
											BgL_arg2027z00_3107 = CNST_TABLE_REF(30);
										}
									else
										{	/* SawJvm/code.scm 255 */
											if ((BgL_casezd2valuezd2_3109 == CNST_TABLE_REF(25)))
												{	/* SawJvm/code.scm 255 */
													BgL_arg2027z00_3107 = CNST_TABLE_REF(31);
												}
											else
												{	/* SawJvm/code.scm 255 */
													if ((BgL_casezd2valuezd2_3109 == CNST_TABLE_REF(10)))
														{	/* SawJvm/code.scm 255 */
															BgL_arg2027z00_3107 = CNST_TABLE_REF(32);
														}
													else
														{	/* SawJvm/code.scm 255 */
															BgL_arg2027z00_3107 = CNST_TABLE_REF(33);
														}
												}
										}
								}
						}
					}
					{	/* SawJvm/code.scm 261 */
						obj_t BgL_arg2036z00_3124;

						{
							BgL_lregz00_bglt BgL_auxz00_6868;

							{
								obj_t BgL_auxz00_6869;

								{	/* SawJvm/code.scm 261 */
									BgL_objectz00_bglt BgL_tmpz00_6870;

									BgL_tmpz00_6870 =
										((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_93));
									BgL_auxz00_6869 = BGL_OBJECT_WIDENING(BgL_tmpz00_6870);
								}
								BgL_auxz00_6868 = ((BgL_lregz00_bglt) BgL_auxz00_6869);
							}
							BgL_arg2036z00_3124 =
								(((BgL_lregz00_bglt) COBJECT(BgL_auxz00_6868))->BgL_idz00);
						}
						BgL_arg2029z00_3108 = MAKE_YOUNG_PAIR(BgL_arg2036z00_3124, BNIL);
					}
					BgL_arg2026z00_3106 =
						MAKE_YOUNG_PAIR(BgL_arg2027z00_3107, BgL_arg2029z00_3108);
				}
				BGL_TAIL return
					BGl_codez12z12zzsaw_jvm_outz00(BgL_mez00_92, BgL_arg2026z00_3106);
			}
		}

	}



/* out-line */
	obj_t BGl_outzd2linezd2zzsaw_jvm_codez00(obj_t BgL_mez00_94,
		BgL_rtl_funz00_bglt BgL_funz00_95)
	{
		{	/* SawJvm/code.scm 267 */
			{	/* SawJvm/code.scm 268 */
				obj_t BgL_locz00_3125;

				BgL_locz00_3125 =
					(((BgL_rtl_funz00_bglt) COBJECT(BgL_funz00_95))->BgL_locz00);
				{	/* SawJvm/code.scm 269 */
					bool_t BgL_test2732z00_6880;

					if (CBOOL(BGl_za2jvmzd2debugza2zd2zzengine_paramz00))
						{	/* SawJvm/code.scm 269 */
							if (STRUCTP(BgL_locz00_3125))
								{	/* SawJvm/code.scm 269 */
									BgL_test2732z00_6880 =
										(STRUCT_KEY(BgL_locz00_3125) == CNST_TABLE_REF(34));
								}
							else
								{	/* SawJvm/code.scm 269 */
									BgL_test2732z00_6880 = ((bool_t) 0);
								}
						}
					else
						{	/* SawJvm/code.scm 269 */
							BgL_test2732z00_6880 = ((bool_t) 0);
						}
					if (BgL_test2732z00_6880)
						{	/* SawJvm/code.scm 270 */
							obj_t BgL_nz00_3127;

							BgL_nz00_3127 = STRUCT_REF(BgL_locz00_3125, (int) (2L));
							{	/* SawJvm/code.scm 270 */
								long BgL_pz00_3128;

								BgL_pz00_3128 =
									((long) CINT(STRUCT_REF(BgL_locz00_3125, (int) (1L))) + 1L);
								{	/* SawJvm/code.scm 271 */
									obj_t BgL_lastz00_3129;

									if (CBOOL(BGl_za2jvmzd2charzd2infoza2z00zzjas_asz00))
										{	/* SawJvm/code.scm 272 */
											BgL_lastz00_3129 = BINT(BgL_pz00_3128);
										}
									else
										{	/* SawJvm/code.scm 272 */
											BgL_lastz00_3129 = BgL_nz00_3127;
										}
									{	/* SawJvm/code.scm 272 */

										if (
											((long) CINT(BgL_lastz00_3129) ==
												(long) CINT(BGl_za2lastzd2lineza2zd2zzsaw_jvm_codez00)))
											{	/* SawJvm/code.scm 275 */
												return BFALSE;
											}
										else
											{	/* SawJvm/code.scm 275 */
												BGl_za2lastzd2lineza2zd2zzsaw_jvm_codez00 =
													BgL_lastz00_3129;
												{	/* SawJvm/code.scm 277 */
													obj_t BgL_arg2039z00_3131;

													{	/* SawJvm/code.scm 277 */
														obj_t BgL_arg2040z00_3132;

														{	/* SawJvm/code.scm 277 */
															obj_t BgL_arg2041z00_3133;

															BgL_arg2041z00_3133 =
																MAKE_YOUNG_PAIR(BINT(BgL_pz00_3128), BNIL);
															BgL_arg2040z00_3132 =
																MAKE_YOUNG_PAIR(BgL_nz00_3127,
																BgL_arg2041z00_3133);
														}
														BgL_arg2039z00_3131 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(35),
															BgL_arg2040z00_3132);
													}
													return
														BGl_codez12z12zzsaw_jvm_outz00(
														((BgL_jvmz00_bglt) BgL_mez00_94),
														BgL_arg2039z00_3131);
												}
											}
									}
								}
							}
						}
					else
						{	/* SawJvm/code.scm 269 */
							return BFALSE;
						}
				}
			}
		}

	}



/* intify */
	obj_t BGl_intifyz00zzsaw_jvm_codez00(obj_t BgL_xz00_124)
	{
		{	/* SawJvm/code.scm 400 */
			if (INTEGERP(BgL_xz00_124))
				{	/* SawJvm/code.scm 402 */
					return BgL_xz00_124;
				}
			else
				{	/* SawJvm/code.scm 402 */
					if (BGL_UINT32P(BgL_xz00_124))
						{	/* SawJvm/code.scm 403 */
							uint32_t BgL_xz00_4743;

							BgL_xz00_4743 = BGL_BUINT32_TO_UINT32(BgL_xz00_124);
							return BINT((long) (BgL_xz00_4743));
						}
					else
						{	/* SawJvm/code.scm 403 */
							if (BGL_INT32P(BgL_xz00_124))
								{	/* SawJvm/code.scm 404 */
									int32_t BgL_xz00_4744;

									BgL_xz00_4744 = BGL_BINT32_TO_INT32(BgL_xz00_124);
									{	/* SawJvm/code.scm 404 */
										long BgL_arg1446z00_4745;

										BgL_arg1446z00_4745 = (long) (BgL_xz00_4744);
										return BINT((long) (BgL_arg1446z00_4745));
								}}
							else
								{	/* SawJvm/code.scm 404 */
									return BgL_xz00_124;
								}
						}
				}
		}

	}



/* flat */
	obj_t BGl_flatz00zzsaw_jvm_codez00(obj_t BgL_alz00_127, obj_t BgL_ldefz00_128)
	{
		{	/* SawJvm/code.scm 438 */
			{
				obj_t BgL_alz00_3140;
				obj_t BgL_iz00_3141;
				obj_t BgL_rz00_3142;

				{	/* SawJvm/code.scm 445 */
					obj_t BgL_arg2046z00_3139;

					{	/* SawJvm/code.scm 445 */
						obj_t BgL_pairz00_4770;

						BgL_pairz00_4770 = CAR(((obj_t) BgL_alz00_127));
						BgL_arg2046z00_3139 = CAR(BgL_pairz00_4770);
					}
					BgL_alz00_3140 = BgL_alz00_127;
					BgL_iz00_3141 = BgL_arg2046z00_3139;
					BgL_rz00_3142 = BNIL;
				BgL_zc3z04anonymousza32047ze3z87_3143:
					if (NULLP(BgL_alz00_3140))
						{	/* SawJvm/code.scm 441 */
							return bgl_reverse_bang(BgL_rz00_3142);
						}
					else
						{	/* SawJvm/code.scm 442 */
							bool_t BgL_test2741z00_6927;

							{	/* SawJvm/code.scm 442 */
								long BgL_tmpz00_6928;

								{	/* SawJvm/code.scm 442 */
									obj_t BgL_pairz00_4750;

									BgL_pairz00_4750 = CAR(((obj_t) BgL_alz00_3140));
									BgL_tmpz00_6928 = (long) CINT(CAR(BgL_pairz00_4750));
								}
								BgL_test2741z00_6927 =
									((long) CINT(BgL_iz00_3141) == BgL_tmpz00_6928);
							}
							if (BgL_test2741z00_6927)
								{	/* SawJvm/code.scm 442 */
									obj_t BgL_arg2051z00_3147;
									long BgL_arg2052z00_3148;
									obj_t BgL_arg2055z00_3149;

									BgL_arg2051z00_3147 = CDR(((obj_t) BgL_alz00_3140));
									BgL_arg2052z00_3148 = ((long) CINT(BgL_iz00_3141) + 1L);
									{	/* SawJvm/code.scm 442 */
										obj_t BgL_arg2056z00_3150;

										{	/* SawJvm/code.scm 442 */
											obj_t BgL_pairz00_4758;

											BgL_pairz00_4758 = CAR(((obj_t) BgL_alz00_3140));
											BgL_arg2056z00_3150 = CDR(BgL_pairz00_4758);
										}
										BgL_arg2055z00_3149 =
											MAKE_YOUNG_PAIR(BgL_arg2056z00_3150, BgL_rz00_3142);
									}
									{
										obj_t BgL_rz00_6946;
										obj_t BgL_iz00_6944;
										obj_t BgL_alz00_6943;

										BgL_alz00_6943 = BgL_arg2051z00_3147;
										BgL_iz00_6944 = BINT(BgL_arg2052z00_3148);
										BgL_rz00_6946 = BgL_arg2055z00_3149;
										BgL_rz00_3142 = BgL_rz00_6946;
										BgL_iz00_3141 = BgL_iz00_6944;
										BgL_alz00_3140 = BgL_alz00_6943;
										goto BgL_zc3z04anonymousza32047ze3z87_3143;
									}
								}
							else
								{	/* SawJvm/code.scm 443 */
									bool_t BgL_test2742z00_6947;

									{	/* SawJvm/code.scm 443 */
										long BgL_tmpz00_6948;

										{	/* SawJvm/code.scm 443 */
											obj_t BgL_pairz00_4762;

											BgL_pairz00_4762 = CAR(((obj_t) BgL_alz00_3140));
											BgL_tmpz00_6948 = (long) CINT(CAR(BgL_pairz00_4762));
										}
										BgL_test2742z00_6947 =
											((long) CINT(BgL_iz00_3141) > BgL_tmpz00_6948);
									}
									if (BgL_test2742z00_6947)
										{	/* SawJvm/code.scm 443 */
											obj_t BgL_arg2059z00_3153;

											BgL_arg2059z00_3153 = CDR(((obj_t) BgL_alz00_3140));
											{
												obj_t BgL_alz00_6957;

												BgL_alz00_6957 = BgL_arg2059z00_3153;
												BgL_alz00_3140 = BgL_alz00_6957;
												goto BgL_zc3z04anonymousza32047ze3z87_3143;
											}
										}
									else
										{	/* SawJvm/code.scm 444 */
											long BgL_arg2060z00_3154;
											obj_t BgL_arg2061z00_3155;

											BgL_arg2060z00_3154 = ((long) CINT(BgL_iz00_3141) + 1L);
											BgL_arg2061z00_3155 =
												MAKE_YOUNG_PAIR(BgL_ldefz00_128, BgL_rz00_3142);
											{
												obj_t BgL_rz00_6963;
												obj_t BgL_iz00_6961;

												BgL_iz00_6961 = BINT(BgL_arg2060z00_3154);
												BgL_rz00_6963 = BgL_arg2061z00_3155;
												BgL_rz00_3142 = BgL_rz00_6963;
												BgL_iz00_3141 = BgL_iz00_6961;
												goto BgL_zc3z04anonymousza32047ze3z87_3143;
											}
										}
								}
						}
				}
			}
		}

	}



/* gen-funcall */
	obj_t BGl_genzd2funcallzd2zzsaw_jvm_codez00(obj_t BgL_mez00_139,
		obj_t BgL_argsz00_140)
	{
		{	/* SawJvm/code.scm 482 */
			{	/* SawJvm/code.scm 483 */
				long BgL_nz00_3159;

				BgL_nz00_3159 = (bgl_list_length(BgL_argsz00_140) - 1L);
				{

					switch (BgL_nz00_3159)
						{
						case 0L:

							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_139), CNST_TABLE_REF(39));
							break;
						case 1L:

							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_139), CNST_TABLE_REF(40));
							break;
						case 2L:

							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_139), CNST_TABLE_REF(41));
							break;
						case 3L:

							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_139), CNST_TABLE_REF(42));
							break;
						case 4L:

							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_139), CNST_TABLE_REF(43));
							break;
						default:
							BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_139), CNST_TABLE_REF(36));
							{	/* SawJvm/code.scm 492 */
								obj_t BgL_g1687z00_3162;

								BgL_g1687z00_3162 = CDR(((obj_t) BgL_argsz00_140));
								{
									obj_t BgL_l1685z00_3164;

									BgL_l1685z00_3164 = BgL_g1687z00_3162;
								BgL_zc3z04anonymousza32064ze3z87_3165:
									if (PAIRP(BgL_l1685z00_3164))
										{	/* SawJvm/code.scm 492 */
											BGl_codez12z12zzsaw_jvm_outz00(
												((BgL_jvmz00_bglt) BgL_mez00_139), CNST_TABLE_REF(37));
											{
												obj_t BgL_l1685z00_6991;

												BgL_l1685z00_6991 = CDR(BgL_l1685z00_3164);
												BgL_l1685z00_3164 = BgL_l1685z00_6991;
												goto BgL_zc3z04anonymousza32064ze3z87_3165;
											}
										}
									else
										{	/* SawJvm/code.scm 492 */
											((bool_t) 1);
										}
								}
							}
							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_139), CNST_TABLE_REF(38));
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_jvm_codez00(void)
	{
		{	/* SawJvm/code.scm 1 */
			{	/* SawJvm/code.scm 27 */
				obj_t BgL_arg2075z00_3175;
				obj_t BgL_arg2076z00_3176;

				{	/* SawJvm/code.scm 27 */
					obj_t BgL_v1692z00_3205;

					BgL_v1692z00_3205 = create_vector(1L);
					{	/* SawJvm/code.scm 27 */
						obj_t BgL_arg2088z00_3206;

						BgL_arg2088z00_3206 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(44),
							BGl_proc2477z00zzsaw_jvm_codez00,
							BGl_proc2476z00zzsaw_jvm_codez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(45));
						VECTOR_SET(BgL_v1692z00_3205, 0L, BgL_arg2088z00_3206);
					}
					BgL_arg2075z00_3175 = BgL_v1692z00_3205;
				}
				{	/* SawJvm/code.scm 27 */
					obj_t BgL_v1693z00_3216;

					BgL_v1693z00_3216 = create_vector(0L);
					BgL_arg2076z00_3176 = BgL_v1693z00_3216;
				}
				BGl_lregz00zzsaw_jvm_codez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(46),
					CNST_TABLE_REF(47), BGl_rtl_regz00zzsaw_defsz00, 43957L,
					BGl_proc2481z00zzsaw_jvm_codez00, BGl_proc2480z00zzsaw_jvm_codez00,
					BFALSE, BGl_proc2479z00zzsaw_jvm_codez00,
					BGl_proc2478z00zzsaw_jvm_codez00, BgL_arg2075z00_3175,
					BgL_arg2076z00_3176);
			}
			{	/* SawJvm/code.scm 28 */
				obj_t BgL_arg2097z00_3225;
				obj_t BgL_arg2098z00_3226;

				{	/* SawJvm/code.scm 28 */
					obj_t BgL_v1694z00_3253;

					BgL_v1694z00_3253 = create_vector(2L);
					{	/* SawJvm/code.scm 28 */
						obj_t BgL_arg2109z00_3254;

						BgL_arg2109z00_3254 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(48),
							BGl_proc2483z00zzsaw_jvm_codez00,
							BGl_proc2482z00zzsaw_jvm_codez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(45));
						VECTOR_SET(BgL_v1694z00_3253, 0L, BgL_arg2109z00_3254);
					}
					{	/* SawJvm/code.scm 28 */
						obj_t BgL_arg2114z00_3264;

						BgL_arg2114z00_3264 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(49),
							BGl_proc2485z00zzsaw_jvm_codez00,
							BGl_proc2484z00zzsaw_jvm_codez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(45));
						VECTOR_SET(BgL_v1694z00_3253, 1L, BgL_arg2114z00_3264);
					}
					BgL_arg2097z00_3225 = BgL_v1694z00_3253;
				}
				{	/* SawJvm/code.scm 28 */
					obj_t BgL_v1695z00_3274;

					BgL_v1695z00_3274 = create_vector(0L);
					BgL_arg2098z00_3226 = BgL_v1695z00_3274;
				}
				return (BGl_liveblockz00zzsaw_jvm_codez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(50),
						CNST_TABLE_REF(47), BGl_blockz00zzsaw_defsz00, 41336L,
						BGl_proc2489z00zzsaw_jvm_codez00, BGl_proc2488z00zzsaw_jvm_codez00,
						BFALSE, BGl_proc2487z00zzsaw_jvm_codez00,
						BGl_proc2486z00zzsaw_jvm_codez00, BgL_arg2097z00_3225,
						BgL_arg2098z00_3226), BUNSPEC);
			}
		}

	}



/* &lambda2105 */
	BgL_blockz00_bglt BGl_z62lambda2105z62zzsaw_jvm_codez00(obj_t BgL_envz00_5407,
		obj_t BgL_o1183z00_5408)
	{
		{	/* SawJvm/code.scm 28 */
			{	/* SawJvm/code.scm 28 */
				long BgL_arg2106z00_5673;

				{	/* SawJvm/code.scm 28 */
					obj_t BgL_arg2107z00_5674;

					{	/* SawJvm/code.scm 28 */
						obj_t BgL_arg2108z00_5675;

						{	/* SawJvm/code.scm 28 */
							obj_t BgL_arg1815z00_5676;
							long BgL_arg1816z00_5677;

							BgL_arg1815z00_5676 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawJvm/code.scm 28 */
								long BgL_arg1817z00_5678;

								BgL_arg1817z00_5678 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1183z00_5408)));
								BgL_arg1816z00_5677 = (BgL_arg1817z00_5678 - OBJECT_TYPE);
							}
							BgL_arg2108z00_5675 =
								VECTOR_REF(BgL_arg1815z00_5676, BgL_arg1816z00_5677);
						}
						BgL_arg2107z00_5674 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2108z00_5675);
					}
					{	/* SawJvm/code.scm 28 */
						obj_t BgL_tmpz00_7026;

						BgL_tmpz00_7026 = ((obj_t) BgL_arg2107z00_5674);
						BgL_arg2106z00_5673 = BGL_CLASS_NUM(BgL_tmpz00_7026);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1183z00_5408)), BgL_arg2106z00_5673);
			}
			{	/* SawJvm/code.scm 28 */
				BgL_objectz00_bglt BgL_tmpz00_7032;

				BgL_tmpz00_7032 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1183z00_5408));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7032, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1183z00_5408));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1183z00_5408));
		}

	}



/* &<@anonymous:2104> */
	obj_t BGl_z62zc3z04anonymousza32104ze3ze5zzsaw_jvm_codez00(obj_t
		BgL_envz00_5409, obj_t BgL_new1182z00_5410)
	{
		{	/* SawJvm/code.scm 28 */
			{
				BgL_blockz00_bglt BgL_auxz00_7040;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1182z00_5410))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1182z00_5410))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1182z00_5410))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1182z00_5410))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_liveblockz00_bglt BgL_auxz00_7054;

					{
						obj_t BgL_auxz00_7055;

						{	/* SawJvm/code.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_7056;

							BgL_tmpz00_7056 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1182z00_5410));
							BgL_auxz00_7055 = BGL_OBJECT_WIDENING(BgL_tmpz00_7056);
						}
						BgL_auxz00_7054 = ((BgL_liveblockz00_bglt) BgL_auxz00_7055);
					}
					((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_7054))->BgL_inz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_liveblockz00_bglt BgL_auxz00_7062;

					{
						obj_t BgL_auxz00_7063;

						{	/* SawJvm/code.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_7064;

							BgL_tmpz00_7064 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1182z00_5410));
							BgL_auxz00_7063 = BGL_OBJECT_WIDENING(BgL_tmpz00_7064);
						}
						BgL_auxz00_7062 = ((BgL_liveblockz00_bglt) BgL_auxz00_7063);
					}
					((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_7062))->BgL_outz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_7040 = ((BgL_blockz00_bglt) BgL_new1182z00_5410);
				return ((obj_t) BgL_auxz00_7040);
			}
		}

	}



/* &lambda2102 */
	BgL_blockz00_bglt BGl_z62lambda2102z62zzsaw_jvm_codez00(obj_t BgL_envz00_5411,
		obj_t BgL_o1179z00_5412)
	{
		{	/* SawJvm/code.scm 28 */
			{	/* SawJvm/code.scm 28 */
				BgL_liveblockz00_bglt BgL_wide1181z00_5681;

				BgL_wide1181z00_5681 =
					((BgL_liveblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_liveblockz00_bgl))));
				{	/* SawJvm/code.scm 28 */
					obj_t BgL_auxz00_7077;
					BgL_objectz00_bglt BgL_tmpz00_7073;

					BgL_auxz00_7077 = ((obj_t) BgL_wide1181z00_5681);
					BgL_tmpz00_7073 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1179z00_5412)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7073, BgL_auxz00_7077);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1179z00_5412)));
				{	/* SawJvm/code.scm 28 */
					long BgL_arg2103z00_5682;

					BgL_arg2103z00_5682 =
						BGL_CLASS_NUM(BGl_liveblockz00zzsaw_jvm_codez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1179z00_5412))), BgL_arg2103z00_5682);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1179z00_5412)));
			}
		}

	}



/* &lambda2099 */
	BgL_blockz00_bglt BGl_z62lambda2099z62zzsaw_jvm_codez00(obj_t BgL_envz00_5413,
		obj_t BgL_label1173z00_5414, obj_t BgL_preds1174z00_5415,
		obj_t BgL_succs1175z00_5416, obj_t BgL_first1176z00_5417,
		obj_t BgL_in1177z00_5418, obj_t BgL_out1178z00_5419)
	{
		{	/* SawJvm/code.scm 28 */
			{	/* SawJvm/code.scm 28 */
				int BgL_label1173z00_5683;

				BgL_label1173z00_5683 = CINT(BgL_label1173z00_5414);
				{	/* SawJvm/code.scm 28 */
					BgL_blockz00_bglt BgL_new1254z00_5687;

					{	/* SawJvm/code.scm 28 */
						BgL_blockz00_bglt BgL_tmp1252z00_5688;
						BgL_liveblockz00_bglt BgL_wide1253z00_5689;

						{
							BgL_blockz00_bglt BgL_auxz00_7092;

							{	/* SawJvm/code.scm 28 */
								BgL_blockz00_bglt BgL_new1251z00_5690;

								BgL_new1251z00_5690 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawJvm/code.scm 28 */
									long BgL_arg2101z00_5691;

									BgL_arg2101z00_5691 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1251z00_5690),
										BgL_arg2101z00_5691);
								}
								{	/* SawJvm/code.scm 28 */
									BgL_objectz00_bglt BgL_tmpz00_7097;

									BgL_tmpz00_7097 = ((BgL_objectz00_bglt) BgL_new1251z00_5690);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7097, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1251z00_5690);
								BgL_auxz00_7092 = BgL_new1251z00_5690;
							}
							BgL_tmp1252z00_5688 = ((BgL_blockz00_bglt) BgL_auxz00_7092);
						}
						BgL_wide1253z00_5689 =
							((BgL_liveblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_liveblockz00_bgl))));
						{	/* SawJvm/code.scm 28 */
							obj_t BgL_auxz00_7105;
							BgL_objectz00_bglt BgL_tmpz00_7103;

							BgL_auxz00_7105 = ((obj_t) BgL_wide1253z00_5689);
							BgL_tmpz00_7103 = ((BgL_objectz00_bglt) BgL_tmp1252z00_5688);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7103, BgL_auxz00_7105);
						}
						((BgL_objectz00_bglt) BgL_tmp1252z00_5688);
						{	/* SawJvm/code.scm 28 */
							long BgL_arg2100z00_5692;

							BgL_arg2100z00_5692 =
								BGL_CLASS_NUM(BGl_liveblockz00zzsaw_jvm_codez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1252z00_5688),
								BgL_arg2100z00_5692);
						}
						BgL_new1254z00_5687 = ((BgL_blockz00_bglt) BgL_tmp1252z00_5688);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1254z00_5687)))->BgL_labelz00) =
						((int) BgL_label1173z00_5683), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1254z00_5687)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1174z00_5415)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1254z00_5687)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1175z00_5416)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1254z00_5687)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1176z00_5417)), BUNSPEC);
					{
						BgL_liveblockz00_bglt BgL_auxz00_7124;

						{
							obj_t BgL_auxz00_7125;

							{	/* SawJvm/code.scm 28 */
								BgL_objectz00_bglt BgL_tmpz00_7126;

								BgL_tmpz00_7126 = ((BgL_objectz00_bglt) BgL_new1254z00_5687);
								BgL_auxz00_7125 = BGL_OBJECT_WIDENING(BgL_tmpz00_7126);
							}
							BgL_auxz00_7124 = ((BgL_liveblockz00_bglt) BgL_auxz00_7125);
						}
						((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_7124))->BgL_inz00) =
							((obj_t) BgL_in1177z00_5418), BUNSPEC);
					}
					{
						BgL_liveblockz00_bglt BgL_auxz00_7131;

						{
							obj_t BgL_auxz00_7132;

							{	/* SawJvm/code.scm 28 */
								BgL_objectz00_bglt BgL_tmpz00_7133;

								BgL_tmpz00_7133 = ((BgL_objectz00_bglt) BgL_new1254z00_5687);
								BgL_auxz00_7132 = BGL_OBJECT_WIDENING(BgL_tmpz00_7133);
							}
							BgL_auxz00_7131 = ((BgL_liveblockz00_bglt) BgL_auxz00_7132);
						}
						((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_7131))->BgL_outz00) =
							((obj_t) BgL_out1178z00_5419), BUNSPEC);
					}
					return BgL_new1254z00_5687;
				}
			}
		}

	}



/* &lambda2118 */
	obj_t BGl_z62lambda2118z62zzsaw_jvm_codez00(obj_t BgL_envz00_5420,
		obj_t BgL_oz00_5421, obj_t BgL_vz00_5422)
	{
		{	/* SawJvm/code.scm 28 */
			{
				BgL_liveblockz00_bglt BgL_auxz00_7138;

				{
					obj_t BgL_auxz00_7139;

					{	/* SawJvm/code.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_7140;

						BgL_tmpz00_7140 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_5421));
						BgL_auxz00_7139 = BGL_OBJECT_WIDENING(BgL_tmpz00_7140);
					}
					BgL_auxz00_7138 = ((BgL_liveblockz00_bglt) BgL_auxz00_7139);
				}
				return
					((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_7138))->BgL_outz00) =
					((obj_t) BgL_vz00_5422), BUNSPEC);
			}
		}

	}



/* &lambda2117 */
	obj_t BGl_z62lambda2117z62zzsaw_jvm_codez00(obj_t BgL_envz00_5423,
		obj_t BgL_oz00_5424)
	{
		{	/* SawJvm/code.scm 28 */
			{
				BgL_liveblockz00_bglt BgL_auxz00_7146;

				{
					obj_t BgL_auxz00_7147;

					{	/* SawJvm/code.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_7148;

						BgL_tmpz00_7148 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_5424));
						BgL_auxz00_7147 = BGL_OBJECT_WIDENING(BgL_tmpz00_7148);
					}
					BgL_auxz00_7146 = ((BgL_liveblockz00_bglt) BgL_auxz00_7147);
				}
				return (((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_7146))->BgL_outz00);
			}
		}

	}



/* &lambda2113 */
	obj_t BGl_z62lambda2113z62zzsaw_jvm_codez00(obj_t BgL_envz00_5425,
		obj_t BgL_oz00_5426, obj_t BgL_vz00_5427)
	{
		{	/* SawJvm/code.scm 28 */
			{
				BgL_liveblockz00_bglt BgL_auxz00_7154;

				{
					obj_t BgL_auxz00_7155;

					{	/* SawJvm/code.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_7156;

						BgL_tmpz00_7156 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_5426));
						BgL_auxz00_7155 = BGL_OBJECT_WIDENING(BgL_tmpz00_7156);
					}
					BgL_auxz00_7154 = ((BgL_liveblockz00_bglt) BgL_auxz00_7155);
				}
				return
					((((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_7154))->BgL_inz00) =
					((obj_t) BgL_vz00_5427), BUNSPEC);
			}
		}

	}



/* &lambda2112 */
	obj_t BGl_z62lambda2112z62zzsaw_jvm_codez00(obj_t BgL_envz00_5428,
		obj_t BgL_oz00_5429)
	{
		{	/* SawJvm/code.scm 28 */
			{
				BgL_liveblockz00_bglt BgL_auxz00_7162;

				{
					obj_t BgL_auxz00_7163;

					{	/* SawJvm/code.scm 28 */
						BgL_objectz00_bglt BgL_tmpz00_7164;

						BgL_tmpz00_7164 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_5429));
						BgL_auxz00_7163 = BGL_OBJECT_WIDENING(BgL_tmpz00_7164);
					}
					BgL_auxz00_7162 = ((BgL_liveblockz00_bglt) BgL_auxz00_7163);
				}
				return (((BgL_liveblockz00_bglt) COBJECT(BgL_auxz00_7162))->BgL_inz00);
			}
		}

	}



/* &lambda2083 */
	BgL_rtl_regz00_bglt BGl_z62lambda2083z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5430, obj_t BgL_o1171z00_5431)
	{
		{	/* SawJvm/code.scm 27 */
			{	/* SawJvm/code.scm 27 */
				long BgL_arg2084z00_5698;

				{	/* SawJvm/code.scm 27 */
					obj_t BgL_arg2086z00_5699;

					{	/* SawJvm/code.scm 27 */
						obj_t BgL_arg2087z00_5700;

						{	/* SawJvm/code.scm 27 */
							obj_t BgL_arg1815z00_5701;
							long BgL_arg1816z00_5702;

							BgL_arg1815z00_5701 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawJvm/code.scm 27 */
								long BgL_arg1817z00_5703;

								BgL_arg1817z00_5703 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_o1171z00_5431)));
								BgL_arg1816z00_5702 = (BgL_arg1817z00_5703 - OBJECT_TYPE);
							}
							BgL_arg2087z00_5700 =
								VECTOR_REF(BgL_arg1815z00_5701, BgL_arg1816z00_5702);
						}
						BgL_arg2086z00_5699 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2087z00_5700);
					}
					{	/* SawJvm/code.scm 27 */
						obj_t BgL_tmpz00_7177;

						BgL_tmpz00_7177 = ((obj_t) BgL_arg2086z00_5699);
						BgL_arg2084z00_5698 = BGL_CLASS_NUM(BgL_tmpz00_7177);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) BgL_o1171z00_5431)), BgL_arg2084z00_5698);
			}
			{	/* SawJvm/code.scm 27 */
				BgL_objectz00_bglt BgL_tmpz00_7183;

				BgL_tmpz00_7183 =
					((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1171z00_5431));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7183, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1171z00_5431));
			return ((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1171z00_5431));
		}

	}



/* &<@anonymous:2082> */
	obj_t BGl_z62zc3z04anonymousza32082ze3ze5zzsaw_jvm_codez00(obj_t
		BgL_envz00_5432, obj_t BgL_new1170z00_5433)
	{
		{	/* SawJvm/code.scm 27 */
			{
				BgL_rtl_regz00_bglt BgL_auxz00_7191;

				{
					BgL_typez00_bglt BgL_auxz00_7192;

					{	/* SawJvm/code.scm 27 */
						obj_t BgL_classz00_5705;

						BgL_classz00_5705 = BGl_typez00zztype_typez00;
						{	/* SawJvm/code.scm 27 */
							obj_t BgL__ortest_1117z00_5706;

							BgL__ortest_1117z00_5706 = BGL_CLASS_NIL(BgL_classz00_5705);
							if (CBOOL(BgL__ortest_1117z00_5706))
								{	/* SawJvm/code.scm 27 */
									BgL_auxz00_7192 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_5706);
								}
							else
								{	/* SawJvm/code.scm 27 */
									BgL_auxz00_7192 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_5705));
								}
						}
					}
					((((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_new1170z00_5433))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_7192), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt)
									((BgL_rtl_regz00_bglt) BgL_new1170z00_5433))))->BgL_varz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1170z00_5433))))->BgL_onexprzf3zf3) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1170z00_5433))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1170z00_5433))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1170z00_5433))))->BgL_debugnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1170z00_5433))))->BgL_hardwarez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_lregz00_bglt BgL_auxz00_7220;

					{
						obj_t BgL_auxz00_7221;

						{	/* SawJvm/code.scm 27 */
							BgL_objectz00_bglt BgL_tmpz00_7222;

							BgL_tmpz00_7222 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1170z00_5433));
							BgL_auxz00_7221 = BGL_OBJECT_WIDENING(BgL_tmpz00_7222);
						}
						BgL_auxz00_7220 = ((BgL_lregz00_bglt) BgL_auxz00_7221);
					}
					((((BgL_lregz00_bglt) COBJECT(BgL_auxz00_7220))->BgL_idz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_7191 = ((BgL_rtl_regz00_bglt) BgL_new1170z00_5433);
				return ((obj_t) BgL_auxz00_7191);
			}
		}

	}



/* &lambda2080 */
	BgL_rtl_regz00_bglt BGl_z62lambda2080z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5434, obj_t BgL_o1167z00_5435)
	{
		{	/* SawJvm/code.scm 27 */
			{	/* SawJvm/code.scm 27 */
				BgL_lregz00_bglt BgL_wide1169z00_5708;

				BgL_wide1169z00_5708 =
					((BgL_lregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_lregz00_bgl))));
				{	/* SawJvm/code.scm 27 */
					obj_t BgL_auxz00_7235;
					BgL_objectz00_bglt BgL_tmpz00_7231;

					BgL_auxz00_7235 = ((obj_t) BgL_wide1169z00_5708);
					BgL_tmpz00_7231 =
						((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1167z00_5435)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7231, BgL_auxz00_7235);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1167z00_5435)));
				{	/* SawJvm/code.scm 27 */
					long BgL_arg2081z00_5709;

					BgL_arg2081z00_5709 = BGL_CLASS_NUM(BGl_lregz00zzsaw_jvm_codez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_o1167z00_5435))),
						BgL_arg2081z00_5709);
				}
				return
					((BgL_rtl_regz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1167z00_5435)));
			}
		}

	}



/* &lambda2077 */
	BgL_rtl_regz00_bglt BGl_z62lambda2077z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5436, obj_t BgL_type1159z00_5437, obj_t BgL_var1160z00_5438,
		obj_t BgL_onexprzf31161zf3_5439, obj_t BgL_name1162z00_5440,
		obj_t BgL_key1163z00_5441, obj_t BgL_debugname1164z00_5442,
		obj_t BgL_hardware1165z00_5443, obj_t BgL_id1166z00_5444)
	{
		{	/* SawJvm/code.scm 27 */
			{	/* SawJvm/code.scm 27 */
				BgL_rtl_regz00_bglt BgL_new1249z00_5711;

				{	/* SawJvm/code.scm 27 */
					BgL_rtl_regz00_bglt BgL_tmp1247z00_5712;
					BgL_lregz00_bglt BgL_wide1248z00_5713;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_7249;

						{	/* SawJvm/code.scm 27 */
							BgL_rtl_regz00_bglt BgL_new1246z00_5714;

							BgL_new1246z00_5714 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawJvm/code.scm 27 */
								long BgL_arg2079z00_5715;

								BgL_arg2079z00_5715 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1246z00_5714),
									BgL_arg2079z00_5715);
							}
							{	/* SawJvm/code.scm 27 */
								BgL_objectz00_bglt BgL_tmpz00_7254;

								BgL_tmpz00_7254 = ((BgL_objectz00_bglt) BgL_new1246z00_5714);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7254, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1246z00_5714);
							BgL_auxz00_7249 = BgL_new1246z00_5714;
						}
						BgL_tmp1247z00_5712 = ((BgL_rtl_regz00_bglt) BgL_auxz00_7249);
					}
					BgL_wide1248z00_5713 =
						((BgL_lregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_lregz00_bgl))));
					{	/* SawJvm/code.scm 27 */
						obj_t BgL_auxz00_7262;
						BgL_objectz00_bglt BgL_tmpz00_7260;

						BgL_auxz00_7262 = ((obj_t) BgL_wide1248z00_5713);
						BgL_tmpz00_7260 = ((BgL_objectz00_bglt) BgL_tmp1247z00_5712);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7260, BgL_auxz00_7262);
					}
					((BgL_objectz00_bglt) BgL_tmp1247z00_5712);
					{	/* SawJvm/code.scm 27 */
						long BgL_arg2078z00_5716;

						BgL_arg2078z00_5716 = BGL_CLASS_NUM(BGl_lregz00zzsaw_jvm_codez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1247z00_5712), BgL_arg2078z00_5716);
					}
					BgL_new1249z00_5711 = ((BgL_rtl_regz00_bglt) BgL_tmp1247z00_5712);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1249z00_5711)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1159z00_5437)),
					BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1249z00_5711)))->BgL_varz00) =
					((obj_t) BgL_var1160z00_5438), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1249z00_5711)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31161zf3_5439), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1249z00_5711)))->BgL_namez00) =
					((obj_t) BgL_name1162z00_5440), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1249z00_5711)))->BgL_keyz00) =
					((obj_t) BgL_key1163z00_5441), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1249z00_5711)))->BgL_debugnamez00) =
					((obj_t) BgL_debugname1164z00_5442), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1249z00_5711)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1165z00_5443), BUNSPEC);
				{
					BgL_lregz00_bglt BgL_auxz00_7285;

					{
						obj_t BgL_auxz00_7286;

						{	/* SawJvm/code.scm 27 */
							BgL_objectz00_bglt BgL_tmpz00_7287;

							BgL_tmpz00_7287 = ((BgL_objectz00_bglt) BgL_new1249z00_5711);
							BgL_auxz00_7286 = BGL_OBJECT_WIDENING(BgL_tmpz00_7287);
						}
						BgL_auxz00_7285 = ((BgL_lregz00_bglt) BgL_auxz00_7286);
					}
					((((BgL_lregz00_bglt) COBJECT(BgL_auxz00_7285))->BgL_idz00) =
						((obj_t) BgL_id1166z00_5444), BUNSPEC);
				}
				return BgL_new1249z00_5711;
			}
		}

	}



/* &lambda2092 */
	obj_t BGl_z62lambda2092z62zzsaw_jvm_codez00(obj_t BgL_envz00_5445,
		obj_t BgL_oz00_5446, obj_t BgL_vz00_5447)
	{
		{	/* SawJvm/code.scm 27 */
			{
				BgL_lregz00_bglt BgL_auxz00_7292;

				{
					obj_t BgL_auxz00_7293;

					{	/* SawJvm/code.scm 27 */
						BgL_objectz00_bglt BgL_tmpz00_7294;

						BgL_tmpz00_7294 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_5446));
						BgL_auxz00_7293 = BGL_OBJECT_WIDENING(BgL_tmpz00_7294);
					}
					BgL_auxz00_7292 = ((BgL_lregz00_bglt) BgL_auxz00_7293);
				}
				return
					((((BgL_lregz00_bglt) COBJECT(BgL_auxz00_7292))->BgL_idz00) =
					((obj_t) BgL_vz00_5447), BUNSPEC);
			}
		}

	}



/* &lambda2091 */
	obj_t BGl_z62lambda2091z62zzsaw_jvm_codez00(obj_t BgL_envz00_5448,
		obj_t BgL_oz00_5449)
	{
		{	/* SawJvm/code.scm 27 */
			{
				BgL_lregz00_bglt BgL_auxz00_7300;

				{
					obj_t BgL_auxz00_7301;

					{	/* SawJvm/code.scm 27 */
						BgL_objectz00_bglt BgL_tmpz00_7302;

						BgL_tmpz00_7302 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_5449));
						BgL_auxz00_7301 = BGL_OBJECT_WIDENING(BgL_tmpz00_7302);
					}
					BgL_auxz00_7300 = ((BgL_lregz00_bglt) BgL_auxz00_7301);
				}
				return (((BgL_lregz00_bglt) COBJECT(BgL_auxz00_7300))->BgL_idz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_jvm_codez00(void)
	{
		{	/* SawJvm/code.scm 1 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_genzd2argszd2genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_proc2490z00zzsaw_jvm_codez00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_string2491z00zzsaw_jvm_codez00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_genzd2funzd2withzd2argszd2envz00zzsaw_jvm_codez00,
				BGl_proc2492z00zzsaw_jvm_codez00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_string2493z00zzsaw_jvm_codez00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_proc2494z00zzsaw_jvm_codez00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_string2495z00zzsaw_jvm_codez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_genzd2argszd2genzd2predicatezd2envz00zzsaw_jvm_codez00,
				BGl_proc2496z00zzsaw_jvm_codez00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_string2497z00zzsaw_jvm_codez00);
		}

	}



/* &gen-args-gen-predica1772 */
	obj_t BGl_z62genzd2argszd2genzd2predica1772zb0zzsaw_jvm_codez00(obj_t
		BgL_envz00_5454, obj_t BgL_funz00_5455, obj_t BgL_mez00_5456,
		obj_t BgL_argsz00_5457, obj_t BgL_onzf3zf3_5458, obj_t BgL_labz00_5459)
	{
		{	/* SawJvm/code.scm 638 */
			BGl_genzd2argszd2genzd2funzd2zzsaw_jvm_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5455), BgL_mez00_5456,
				BgL_argsz00_5457);
			BGl_outzd2linezd2zzsaw_jvm_codez00(BgL_mez00_5456,
				((BgL_rtl_funz00_bglt) BgL_funz00_5455));
			{	/* SawJvm/code.scm 642 */
				obj_t BgL_arg2126z00_5720;

				if (CBOOL(BgL_onzf3zf3_5458))
					{	/* SawJvm/code.scm 642 */
						BgL_arg2126z00_5720 = CNST_TABLE_REF(51);
					}
				else
					{	/* SawJvm/code.scm 642 */
						BgL_arg2126z00_5720 = CNST_TABLE_REF(52);
					}
				return
					BGl_branchz00zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5456), BgL_arg2126z00_5720,
					BgL_labz00_5459);
			}
		}

	}



/* &gen-fun1700 */
	obj_t BGl_z62genzd2fun1700zb0zzsaw_jvm_codez00(obj_t BgL_envz00_5460,
		obj_t BgL_funz00_5461, obj_t BgL_mez00_5462)
	{
		{	/* SawJvm/code.scm 308 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(53),
				BGl_string2498z00zzsaw_jvm_codez00,
				((obj_t) ((BgL_rtl_funz00_bglt) BgL_funz00_5461)));
		}

	}



/* &gen-fun-with-args1698 */
	obj_t BGl_z62genzd2funzd2withzd2args1698zb0zzsaw_jvm_codez00(obj_t
		BgL_envz00_5463, obj_t BgL_funz00_5464, obj_t BgL_mez00_5465,
		obj_t BgL_argsz00_5466)
	{
		{	/* SawJvm/code.scm 294 */
			return
				BGl_genzd2funzd2zzsaw_jvm_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5464), BgL_mez00_5465);
		}

	}



/* &gen-args-gen-fun1696 */
	obj_t BGl_z62genzd2argszd2genzd2fun1696zb0zzsaw_jvm_codez00(obj_t
		BgL_envz00_5467, obj_t BgL_funz00_5468, obj_t BgL_mez00_5469,
		obj_t BgL_argsz00_5470)
	{
		{	/* SawJvm/code.scm 284 */
			{
				obj_t BgL_l1661z00_5725;

				BgL_l1661z00_5725 = BgL_argsz00_5470;
			BgL_zc3z04anonymousza32120ze3z87_5724:
				if (PAIRP(BgL_l1661z00_5725))
					{	/* SawJvm/code.scm 286 */
						{	/* SawJvm/code.scm 286 */
							obj_t BgL_az00_5726;

							BgL_az00_5726 = CAR(BgL_l1661z00_5725);
							BGl_genzd2exprzd2zzsaw_jvm_codez00(
								((BgL_jvmz00_bglt) BgL_mez00_5469), BgL_az00_5726);
						}
						{
							obj_t BgL_l1661z00_7333;

							BgL_l1661z00_7333 = CDR(BgL_l1661z00_5725);
							BgL_l1661z00_5725 = BgL_l1661z00_7333;
							goto BgL_zc3z04anonymousza32120ze3z87_5724;
						}
					}
				else
					{	/* SawJvm/code.scm 286 */
						((bool_t) 1);
					}
			}
			BGl_outzd2linezd2zzsaw_jvm_codez00(BgL_mez00_5469,
				((BgL_rtl_funz00_bglt) BgL_funz00_5468));
			return
				BGl_genzd2funzd2withzd2argszd2zzsaw_jvm_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5468), BgL_mez00_5469,
				BgL_argsz00_5470);
		}

	}



/* gen-args-gen-fun */
	obj_t BGl_genzd2argszd2genzd2funzd2zzsaw_jvm_codez00(BgL_rtl_funz00_bglt
		BgL_funz00_96, obj_t BgL_mez00_97, obj_t BgL_argsz00_98)
	{
		{	/* SawJvm/code.scm 284 */
			{	/* SawJvm/code.scm 284 */
				obj_t BgL_method1697z00_3312;

				{	/* SawJvm/code.scm 284 */
					obj_t BgL_res2449z00_4875;

					{	/* SawJvm/code.scm 284 */
						long BgL_objzd2classzd2numz00_4846;

						BgL_objzd2classzd2numz00_4846 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_96));
						{	/* SawJvm/code.scm 284 */
							obj_t BgL_arg1811z00_4847;

							BgL_arg1811z00_4847 =
								PROCEDURE_REF
								(BGl_genzd2argszd2genzd2funzd2envz00zzsaw_jvm_codez00,
								(int) (1L));
							{	/* SawJvm/code.scm 284 */
								int BgL_offsetz00_4850;

								BgL_offsetz00_4850 = (int) (BgL_objzd2classzd2numz00_4846);
								{	/* SawJvm/code.scm 284 */
									long BgL_offsetz00_4851;

									BgL_offsetz00_4851 =
										((long) (BgL_offsetz00_4850) - OBJECT_TYPE);
									{	/* SawJvm/code.scm 284 */
										long BgL_modz00_4852;

										BgL_modz00_4852 =
											(BgL_offsetz00_4851 >> (int) ((long) ((int) (4L))));
										{	/* SawJvm/code.scm 284 */
											long BgL_restz00_4854;

											BgL_restz00_4854 =
												(BgL_offsetz00_4851 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawJvm/code.scm 284 */

												{	/* SawJvm/code.scm 284 */
													obj_t BgL_bucketz00_4856;

													BgL_bucketz00_4856 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4847), BgL_modz00_4852);
													BgL_res2449z00_4875 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4856), BgL_restz00_4854);
					}}}}}}}}
					BgL_method1697z00_3312 = BgL_res2449z00_4875;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1697z00_3312,
					((obj_t) BgL_funz00_96), BgL_mez00_97, BgL_argsz00_98);
			}
		}

	}



/* &gen-args-gen-fun */
	obj_t BGl_z62genzd2argszd2genzd2funzb0zzsaw_jvm_codez00(obj_t BgL_envz00_5471,
		obj_t BgL_funz00_5472, obj_t BgL_mez00_5473, obj_t BgL_argsz00_5474)
	{
		{	/* SawJvm/code.scm 284 */
			return
				BGl_genzd2argszd2genzd2funzd2zzsaw_jvm_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5472), BgL_mez00_5473,
				BgL_argsz00_5474);
		}

	}



/* gen-fun-with-args */
	obj_t BGl_genzd2funzd2withzd2argszd2zzsaw_jvm_codez00(BgL_rtl_funz00_bglt
		BgL_funz00_99, obj_t BgL_mez00_100, obj_t BgL_argsz00_101)
	{
		{	/* SawJvm/code.scm 294 */
			{	/* SawJvm/code.scm 294 */
				obj_t BgL_method1699z00_3313;

				{	/* SawJvm/code.scm 294 */
					obj_t BgL_res2454z00_4906;

					{	/* SawJvm/code.scm 294 */
						long BgL_objzd2classzd2numz00_4877;

						BgL_objzd2classzd2numz00_4877 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_99));
						{	/* SawJvm/code.scm 294 */
							obj_t BgL_arg1811z00_4878;

							BgL_arg1811z00_4878 =
								PROCEDURE_REF
								(BGl_genzd2funzd2withzd2argszd2envz00zzsaw_jvm_codez00,
								(int) (1L));
							{	/* SawJvm/code.scm 294 */
								int BgL_offsetz00_4881;

								BgL_offsetz00_4881 = (int) (BgL_objzd2classzd2numz00_4877);
								{	/* SawJvm/code.scm 294 */
									long BgL_offsetz00_4882;

									BgL_offsetz00_4882 =
										((long) (BgL_offsetz00_4881) - OBJECT_TYPE);
									{	/* SawJvm/code.scm 294 */
										long BgL_modz00_4883;

										BgL_modz00_4883 =
											(BgL_offsetz00_4882 >> (int) ((long) ((int) (4L))));
										{	/* SawJvm/code.scm 294 */
											long BgL_restz00_4885;

											BgL_restz00_4885 =
												(BgL_offsetz00_4882 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawJvm/code.scm 294 */

												{	/* SawJvm/code.scm 294 */
													obj_t BgL_bucketz00_4887;

													BgL_bucketz00_4887 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4878), BgL_modz00_4883);
													BgL_res2454z00_4906 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4887), BgL_restz00_4885);
					}}}}}}}}
					BgL_method1699z00_3313 = BgL_res2454z00_4906;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1699z00_3313,
					((obj_t) BgL_funz00_99), BgL_mez00_100, BgL_argsz00_101);
			}
		}

	}



/* &gen-fun-with-args */
	obj_t BGl_z62genzd2funzd2withzd2argszb0zzsaw_jvm_codez00(obj_t
		BgL_envz00_5475, obj_t BgL_funz00_5476, obj_t BgL_mez00_5477,
		obj_t BgL_argsz00_5478)
	{
		{	/* SawJvm/code.scm 294 */
			return
				BGl_genzd2funzd2withzd2argszd2zzsaw_jvm_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5476), BgL_mez00_5477,
				BgL_argsz00_5478);
		}

	}



/* gen-fun */
	obj_t BGl_genzd2funzd2zzsaw_jvm_codez00(BgL_rtl_funz00_bglt BgL_funz00_102,
		obj_t BgL_mez00_103)
	{
		{	/* SawJvm/code.scm 308 */
			{	/* SawJvm/code.scm 308 */
				obj_t BgL_method1701z00_3314;

				{	/* SawJvm/code.scm 308 */
					obj_t BgL_res2459z00_4937;

					{	/* SawJvm/code.scm 308 */
						long BgL_objzd2classzd2numz00_4908;

						BgL_objzd2classzd2numz00_4908 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_102));
						{	/* SawJvm/code.scm 308 */
							obj_t BgL_arg1811z00_4909;

							BgL_arg1811z00_4909 =
								PROCEDURE_REF(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
								(int) (1L));
							{	/* SawJvm/code.scm 308 */
								int BgL_offsetz00_4912;

								BgL_offsetz00_4912 = (int) (BgL_objzd2classzd2numz00_4908);
								{	/* SawJvm/code.scm 308 */
									long BgL_offsetz00_4913;

									BgL_offsetz00_4913 =
										((long) (BgL_offsetz00_4912) - OBJECT_TYPE);
									{	/* SawJvm/code.scm 308 */
										long BgL_modz00_4914;

										BgL_modz00_4914 =
											(BgL_offsetz00_4913 >> (int) ((long) ((int) (4L))));
										{	/* SawJvm/code.scm 308 */
											long BgL_restz00_4916;

											BgL_restz00_4916 =
												(BgL_offsetz00_4913 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawJvm/code.scm 308 */

												{	/* SawJvm/code.scm 308 */
													obj_t BgL_bucketz00_4918;

													BgL_bucketz00_4918 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4909), BgL_modz00_4914);
													BgL_res2459z00_4937 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4918), BgL_restz00_4916);
					}}}}}}}}
					BgL_method1701z00_3314 = BgL_res2459z00_4937;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1701z00_3314,
					((obj_t) BgL_funz00_102), BgL_mez00_103);
			}
		}

	}



/* &gen-fun */
	obj_t BGl_z62genzd2funzb0zzsaw_jvm_codez00(obj_t BgL_envz00_5479,
		obj_t BgL_funz00_5480, obj_t BgL_mez00_5481)
	{
		{	/* SawJvm/code.scm 308 */
			return
				BGl_genzd2funzd2zzsaw_jvm_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5480), BgL_mez00_5481);
		}

	}



/* gen-args-gen-predicate */
	obj_t BGl_genzd2argszd2genzd2predicatezd2zzsaw_jvm_codez00(BgL_rtl_funz00_bglt
		BgL_funz00_184, obj_t BgL_mez00_185, obj_t BgL_argsz00_186,
		obj_t BgL_onzf3zf3_187, obj_t BgL_labz00_188)
	{
		{	/* SawJvm/code.scm 638 */
			{	/* SawJvm/code.scm 638 */
				obj_t BgL_method1773z00_3315;

				{	/* SawJvm/code.scm 638 */
					obj_t BgL_res2464z00_4968;

					{	/* SawJvm/code.scm 638 */
						long BgL_objzd2classzd2numz00_4939;

						BgL_objzd2classzd2numz00_4939 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_184));
						{	/* SawJvm/code.scm 638 */
							obj_t BgL_arg1811z00_4940;

							BgL_arg1811z00_4940 =
								PROCEDURE_REF
								(BGl_genzd2argszd2genzd2predicatezd2envz00zzsaw_jvm_codez00,
								(int) (1L));
							{	/* SawJvm/code.scm 638 */
								int BgL_offsetz00_4943;

								BgL_offsetz00_4943 = (int) (BgL_objzd2classzd2numz00_4939);
								{	/* SawJvm/code.scm 638 */
									long BgL_offsetz00_4944;

									BgL_offsetz00_4944 =
										((long) (BgL_offsetz00_4943) - OBJECT_TYPE);
									{	/* SawJvm/code.scm 638 */
										long BgL_modz00_4945;

										BgL_modz00_4945 =
											(BgL_offsetz00_4944 >> (int) ((long) ((int) (4L))));
										{	/* SawJvm/code.scm 638 */
											long BgL_restz00_4947;

											BgL_restz00_4947 =
												(BgL_offsetz00_4944 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawJvm/code.scm 638 */

												{	/* SawJvm/code.scm 638 */
													obj_t BgL_bucketz00_4949;

													BgL_bucketz00_4949 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4940), BgL_modz00_4945);
													BgL_res2464z00_4968 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4949), BgL_restz00_4947);
					}}}}}}}}
					BgL_method1773z00_3315 = BgL_res2464z00_4968;
				}
				return
					BGL_PROCEDURE_CALL5(BgL_method1773z00_3315,
					((obj_t) BgL_funz00_184), BgL_mez00_185, BgL_argsz00_186,
					BgL_onzf3zf3_187, BgL_labz00_188);
			}
		}

	}



/* &gen-args-gen-predicate */
	obj_t BGl_z62genzd2argszd2genzd2predicatezb0zzsaw_jvm_codez00(obj_t
		BgL_envz00_5482, obj_t BgL_funz00_5483, obj_t BgL_mez00_5484,
		obj_t BgL_argsz00_5485, obj_t BgL_onzf3zf3_5486, obj_t BgL_labz00_5487)
	{
		{	/* SawJvm/code.scm 638 */
			return
				BGl_genzd2argszd2genzd2predicatezd2zzsaw_jvm_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5483), BgL_mez00_5484,
				BgL_argsz00_5485, BgL_onzf3zf3_5486, BgL_labz00_5487);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_jvm_codez00(void)
	{
		{	/* SawJvm/code.scm 1 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_nopz00zzsaw_defsz00,
				BGl_proc2499z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_movz00zzsaw_defsz00,
				BGl_proc2501z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_loadiz00zzsaw_defsz00,
				BGl_proc2502z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_loadgz00zzsaw_defsz00,
				BGl_proc2503z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_storegz00zzsaw_defsz00, BGl_proc2504z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_globalrefz00zzsaw_defsz00, BGl_proc2505z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2argszd2genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_ifeqz00zzsaw_defsz00, BGl_proc2506z00zzsaw_jvm_codez00,
				BGl_string2507z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2argszd2genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_ifnez00zzsaw_defsz00, BGl_proc2508z00zzsaw_jvm_codez00,
				BGl_string2507z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_goz00zzsaw_defsz00,
				BGl_proc2509z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_switchz00zzsaw_defsz00, BGl_proc2510z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2argszd2genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_callz00zzsaw_defsz00, BGl_proc2511z00zzsaw_jvm_codez00,
				BGl_string2507z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_callz00zzsaw_defsz00,
				BGl_proc2512z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_lightfuncallz00zzsaw_defsz00, BGl_proc2513z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2withzd2argszd2envz00zzsaw_jvm_codez00,
				BGl_rtl_funcallz00zzsaw_defsz00, BGl_proc2514z00zzsaw_jvm_codez00,
				BGl_string2515z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2withzd2argszd2envz00zzsaw_jvm_codez00,
				BGl_rtl_returnz00zzsaw_defsz00, BGl_proc2516z00zzsaw_jvm_codez00,
				BGl_string2515z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_loadfunz00zzsaw_defsz00, BGl_proc2517z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_applyz00zzsaw_defsz00,
				BGl_proc2518z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_vrefz00zzsaw_defsz00,
				BGl_proc2519z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_vsetz00zzsaw_defsz00,
				BGl_proc2520z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_vlengthz00zzsaw_defsz00, BGl_proc2521z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_vallocz00zzsaw_defsz00, BGl_proc2522z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2argszd2genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_newz00zzsaw_defsz00, BGl_proc2523z00zzsaw_jvm_codez00,
				BGl_string2507z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_getfieldz00zzsaw_defsz00, BGl_proc2524z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_setfieldz00zzsaw_defsz00, BGl_proc2525z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_instanceofz00zzsaw_defsz00, BGl_proc2526z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_castz00zzsaw_defsz00,
				BGl_proc2527z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_cast_nullz00zzsaw_defsz00, BGl_proc2528z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2argszd2genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_makeboxz00zzsaw_defsz00, BGl_proc2529z00zzsaw_jvm_codez00,
				BGl_string2507z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_boxrefz00zzsaw_defsz00, BGl_proc2530z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_boxsetz00zzsaw_defsz00, BGl_proc2531z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_jumpexitz00zzsaw_defsz00, BGl_proc2532z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00, BGl_rtl_failz00zzsaw_defsz00,
				BGl_proc2533z00zzsaw_jvm_codez00, BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_protectz00zzsaw_defsz00, BGl_proc2534z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2envz00zzsaw_jvm_codez00,
				BGl_rtl_protectedz00zzsaw_defsz00, BGl_proc2535z00zzsaw_jvm_codez00,
				BGl_string2500z00zzsaw_jvm_codez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2argszd2genzd2predicatezd2envz00zzsaw_jvm_codez00,
				BGl_rtl_callz00zzsaw_defsz00, BGl_proc2536z00zzsaw_jvm_codez00,
				BGl_string2537z00zzsaw_jvm_codez00);
		}

	}



/* &gen-args-gen-predica1775 */
	obj_t BGl_z62genzd2argszd2genzd2predica1775zb0zzsaw_jvm_codez00(obj_t
		BgL_envz00_5525, obj_t BgL_funz00_5526, obj_t BgL_mez00_5527,
		obj_t BgL_argsz00_5528, obj_t BgL_onzf3zf3_5529, obj_t BgL_labz00_5530)
	{
		{	/* SawJvm/code.scm 645 */
			{
				obj_t BgL_l1690z00_5729;

				BgL_l1690z00_5729 = BgL_argsz00_5528;
			BgL_zc3z04anonymousza32295ze3z87_5728:
				if (PAIRP(BgL_l1690z00_5729))
					{	/* SawJvm/code.scm 646 */
						{	/* SawJvm/code.scm 646 */
							obj_t BgL_az00_5730;

							BgL_az00_5730 = CAR(BgL_l1690z00_5729);
							BGl_genzd2exprzd2zzsaw_jvm_codez00(
								((BgL_jvmz00_bglt) BgL_mez00_5527), BgL_az00_5730);
						}
						{
							obj_t BgL_l1690z00_7516;

							BgL_l1690z00_7516 = CDR(BgL_l1690z00_5729);
							BgL_l1690z00_5729 = BgL_l1690z00_7516;
							goto BgL_zc3z04anonymousza32295ze3z87_5728;
						}
					}
				else
					{	/* SawJvm/code.scm 646 */
						((bool_t) 1);
					}
			}
			BGl_outzd2linezd2zzsaw_jvm_codez00(BgL_mez00_5527,
				((BgL_rtl_funz00_bglt) ((BgL_rtl_callz00_bglt) BgL_funz00_5526)));
			{	/* SawJvm/code.scm 648 */
				obj_t BgL_rz00_5731;

				{	/* SawJvm/code.scm 648 */
					BgL_globalz00_bglt BgL_arg2299z00_5732;

					BgL_arg2299z00_5732 =
						(((BgL_rtl_callz00_bglt) COBJECT(
								((BgL_rtl_callz00_bglt) BgL_funz00_5526)))->BgL_varz00);
					BgL_rz00_5731 =
						BGl_inlinezd2predicatezf3z21zzsaw_jvm_inlinez00(
						((BgL_jvmz00_bglt) BgL_mez00_5527), BgL_arg2299z00_5732,
						BgL_onzf3zf3_5529, BgL_labz00_5530);
				}
				if ((BgL_rz00_5731 == CNST_TABLE_REF(54)))
					{	/* SawJvm/code.scm 649 */
						BGl_genzd2funzd2withzd2argszd2zzsaw_jvm_codez00(
							((BgL_rtl_funz00_bglt)
								((BgL_rtl_callz00_bglt) BgL_funz00_5526)), BgL_mez00_5527,
							BgL_argsz00_5528);
						{	/* SawJvm/code.scm 651 */
							obj_t BgL_arg2298z00_5733;

							if (CBOOL(BgL_onzf3zf3_5529))
								{	/* SawJvm/code.scm 651 */
									BgL_arg2298z00_5733 = CNST_TABLE_REF(51);
								}
							else
								{	/* SawJvm/code.scm 651 */
									BgL_arg2298z00_5733 = CNST_TABLE_REF(52);
								}
							return
								BGl_branchz00zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_5527), BgL_arg2298z00_5733,
								BgL_labz00_5530);
						}
					}
				else
					{	/* SawJvm/code.scm 649 */
						return BgL_rz00_5731;
					}
			}
		}

	}



/* &gen-fun-rtl_protecte1771 */
	obj_t BGl_z62genzd2funzd2rtl_protecte1771z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5531, obj_t BgL_funz00_5532, obj_t BgL_mez00_5533)
	{
		{	/* SawJvm/code.scm 629 */
			return CNST_TABLE_REF(55);
		}

	}



/* &gen-fun-rtl_protect1769 */
	obj_t BGl_z62genzd2funzd2rtl_protect1769z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5534, obj_t BgL_funz00_5535, obj_t BgL_mez00_5536)
	{
		{	/* SawJvm/code.scm 626 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5536), CNST_TABLE_REF(56));
		}

	}



/* &gen-fun-rtl_fail1767 */
	obj_t BGl_z62genzd2funzd2rtl_fail1767z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5537, obj_t BgL_funz00_5538, obj_t BgL_mez00_5539)
	{
		{	/* SawJvm/code.scm 621 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5539), CNST_TABLE_REF(57));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5539), CNST_TABLE_REF(58));
			return CNST_TABLE_REF(13);
		}

	}



/* &gen-fun-rtl_jumpexit1765 */
	obj_t BGl_z62genzd2funzd2rtl_jumpexit1765z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5540, obj_t BgL_funz00_5541, obj_t BgL_mez00_5542)
	{
		{	/* SawJvm/code.scm 614 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5542), CNST_TABLE_REF(59));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5542), CNST_TABLE_REF(60));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5542), CNST_TABLE_REF(58));
			return CNST_TABLE_REF(13);
		}

	}



/* &gen-fun-rtl_boxset1763 */
	obj_t BGl_z62genzd2funzd2rtl_boxset1763z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5543, obj_t BgL_funz00_5544, obj_t BgL_mez00_5545)
	{
		{	/* SawJvm/code.scm 607 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5545), CNST_TABLE_REF(61));
			return CNST_TABLE_REF(13);
		}

	}



/* &gen-fun-rtl_boxref1761 */
	obj_t BGl_z62genzd2funzd2rtl_boxref1761z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5546, obj_t BgL_funz00_5547, obj_t BgL_mez00_5548)
	{
		{	/* SawJvm/code.scm 604 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5548), CNST_TABLE_REF(62));
		}

	}



/* &gen-args-gen-fun-rtl1759 */
	obj_t BGl_z62genzd2argszd2genzd2funzd2rtl1759z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5549, obj_t BgL_funz00_5550, obj_t BgL_mez00_5551,
		obj_t BgL_argsz00_5552)
	{
		{	/* SawJvm/code.scm 597 */
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5551), CNST_TABLE_REF(63));
			BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5551), CNST_TABLE_REF(64));
			{	/* SawJvm/code.scm 600 */
				obj_t BgL_arg2294z00_5741;

				BgL_arg2294z00_5741 = CAR(((obj_t) BgL_argsz00_5552));
				BGl_genzd2exprzd2zzsaw_jvm_codez00(
					((BgL_jvmz00_bglt) BgL_mez00_5551), BgL_arg2294z00_5741);
			}
			BGl_outzd2linezd2zzsaw_jvm_codez00(BgL_mez00_5551,
				((BgL_rtl_funz00_bglt) ((BgL_rtl_makeboxz00_bglt) BgL_funz00_5550)));
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5551), CNST_TABLE_REF(65));
		}

	}



/* &gen-fun-rtl_cast_nul1757 */
	obj_t BGl_z62genzd2funzd2rtl_cast_nul1757z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5553, obj_t BgL_funz00_5554, obj_t BgL_mez00_5555)
	{
		{	/* SawJvm/code.scm 588 */
			{	/* SawJvm/code.scm 589 */
				BgL_typez00_bglt BgL_typez00_5743;

				BgL_typez00_5743 =
					(((BgL_rtl_cast_nullz00_bglt) COBJECT(
							((BgL_rtl_cast_nullz00_bglt) BgL_funz00_5554)))->BgL_typez00);
				if (
					((((BgL_typez00_bglt) COBJECT(BgL_typez00_5743))->BgL_namez00) ==
						CNST_TABLE_REF(25)))
					{	/* SawJvm/code.scm 591 */
						obj_t BgL_arg2291z00_5744;

						BgL_arg2291z00_5744 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(66), BNIL);
						return
							BGl_codez12z12zzsaw_jvm_outz00(
							((BgL_jvmz00_bglt) BgL_mez00_5555), BgL_arg2291z00_5744);
					}
				else
					{	/* SawJvm/code.scm 592 */
						obj_t BgL_arg2292z00_5745;

						BgL_arg2292z00_5745 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(67), BNIL);
						return
							BGl_codez12z12zzsaw_jvm_outz00(
							((BgL_jvmz00_bglt) BgL_mez00_5555), BgL_arg2292z00_5745);
					}
			}
		}

	}



/* &gen-fun-rtl_cast1755 */
	obj_t BGl_z62genzd2funzd2rtl_cast1755z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5556, obj_t BgL_funz00_5557, obj_t BgL_mez00_5558)
	{
		{	/* SawJvm/code.scm 582 */
			if (CBOOL(BGl_za2purifyza2z00zzengine_paramz00))
				{	/* SawJvm/code.scm 584 */
					BgL_typez00_bglt BgL_typez00_5747;

					BgL_typez00_5747 =
						(((BgL_rtl_castz00_bglt) COBJECT(
								((BgL_rtl_castz00_bglt) BgL_funz00_5557)))->BgL_totypez00);
					if (
						((((BgL_typez00_bglt) COBJECT(BgL_typez00_5747))->BgL_namez00) ==
							CNST_TABLE_REF(45)))
						{	/* SawJvm/code.scm 585 */
							return BFALSE;
						}
					else
						{	/* SawJvm/code.scm 586 */
							obj_t BgL_arg2284z00_5748;

							{	/* SawJvm/code.scm 586 */
								obj_t BgL_arg2286z00_5749;

								{	/* SawJvm/code.scm 586 */
									obj_t BgL_arg2287z00_5750;

									BgL_arg2287z00_5750 =
										BGl_compilezd2typezd2zzsaw_jvm_outz00(
										((BgL_jvmz00_bglt) BgL_mez00_5558), BgL_typez00_5747);
									BgL_arg2286z00_5749 =
										MAKE_YOUNG_PAIR(BgL_arg2287z00_5750, BNIL);
								}
								BgL_arg2284z00_5748 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(68), BgL_arg2286z00_5749);
							}
							return
								BGl_codez12z12zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_5558), BgL_arg2284z00_5748);
						}
				}
			else
				{	/* SawJvm/code.scm 583 */
					return BFALSE;
				}
		}

	}



/* &gen-fun-rtl_instance1753 */
	obj_t BGl_z62genzd2funzd2rtl_instance1753z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5559, obj_t BgL_funz00_5560, obj_t BgL_mez00_5561)
	{
		{	/* SawJvm/code.scm 572 */
			{	/* SawJvm/code.scm 573 */
				obj_t BgL_arg2277z00_5752;

				{	/* SawJvm/code.scm 573 */
					obj_t BgL_arg2279z00_5753;

					{	/* SawJvm/code.scm 573 */
						obj_t BgL_arg2280z00_5754;

						{	/* SawJvm/code.scm 573 */
							BgL_typez00_bglt BgL_arg2281z00_5755;

							BgL_arg2281z00_5755 =
								(((BgL_rtl_instanceofz00_bglt) COBJECT(
										((BgL_rtl_instanceofz00_bglt) BgL_funz00_5560)))->
								BgL_typez00);
							BgL_arg2280z00_5754 =
								BGl_compilezd2typezd2zzsaw_jvm_outz00(((BgL_jvmz00_bglt)
									BgL_mez00_5561), BgL_arg2281z00_5755);
						}
						BgL_arg2279z00_5753 = MAKE_YOUNG_PAIR(BgL_arg2280z00_5754, BNIL);
					}
					BgL_arg2277z00_5752 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(69), BgL_arg2279z00_5753);
				}
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5561), BgL_arg2277z00_5752);
			}
			{	/* SawJvm/code.scm 574 */
				obj_t BgL_l1z00_5756;
				obj_t BgL_l2z00_5757;

				BgL_l1z00_5756 =
					BGl_gensymz00zz__r4_symbols_6_4z00
					(BGl_string2538z00zzsaw_jvm_codez00);
				BgL_l2z00_5757 =
					BGl_gensymz00zz__r4_symbols_6_4z00
					(BGl_string2538z00zzsaw_jvm_codez00);
				BGl_branchz00zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5561),
					CNST_TABLE_REF(52), BgL_l1z00_5756);
				BGl_pushzd2intzd2zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5561),
					BINT(1L));
				BGl_branchz00zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5561),
					CNST_TABLE_REF(70), BgL_l2z00_5757);
				BGl_labelz00zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5561),
					BgL_l1z00_5756);
				BGl_pushzd2intzd2zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5561),
					BINT(0L));
				return BGl_labelz00zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5561),
					BgL_l2z00_5757);
			}
		}

	}



/* &gen-fun-rtl_setfield1751 */
	obj_t BGl_z62genzd2funzd2rtl_setfield1751z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5562, obj_t BgL_funz00_5563, obj_t BgL_mez00_5564)
	{
		{	/* SawJvm/code.scm 567 */
			{	/* SawJvm/code.scm 569 */
				BgL_typez00_bglt BgL_arg2274z00_5759;
				BgL_typez00_bglt BgL_arg2275z00_5760;
				obj_t BgL_arg2276z00_5761;

				BgL_arg2274z00_5759 =
					(((BgL_rtl_setfieldz00_bglt) COBJECT(
							((BgL_rtl_setfieldz00_bglt) BgL_funz00_5563)))->BgL_typez00);
				BgL_arg2275z00_5760 =
					(((BgL_rtl_setfieldz00_bglt) COBJECT(
							((BgL_rtl_setfieldz00_bglt) BgL_funz00_5563)))->BgL_objtypez00);
				BgL_arg2276z00_5761 =
					(((BgL_rtl_setfieldz00_bglt) COBJECT(
							((BgL_rtl_setfieldz00_bglt) BgL_funz00_5563)))->BgL_namez00);
				BGl_storezd2fieldzd2zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5564), BgL_arg2274z00_5759,
					BgL_arg2275z00_5760, BgL_arg2276z00_5761);
			}
			return CNST_TABLE_REF(13);
		}

	}



/* &gen-fun-rtl_getfield1749 */
	obj_t BGl_z62genzd2funzd2rtl_getfield1749z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5565, obj_t BgL_funz00_5566, obj_t BgL_mez00_5567)
	{
		{	/* SawJvm/code.scm 563 */
			{	/* SawJvm/code.scm 565 */
				BgL_typez00_bglt BgL_arg2271z00_5763;
				BgL_typez00_bglt BgL_arg2272z00_5764;
				obj_t BgL_arg2273z00_5765;

				BgL_arg2271z00_5763 =
					(((BgL_rtl_getfieldz00_bglt) COBJECT(
							((BgL_rtl_getfieldz00_bglt) BgL_funz00_5566)))->BgL_typez00);
				BgL_arg2272z00_5764 =
					(((BgL_rtl_getfieldz00_bglt) COBJECT(
							((BgL_rtl_getfieldz00_bglt) BgL_funz00_5566)))->BgL_objtypez00);
				BgL_arg2273z00_5765 =
					(((BgL_rtl_getfieldz00_bglt) COBJECT(
							((BgL_rtl_getfieldz00_bglt) BgL_funz00_5566)))->BgL_namez00);
				return
					BGl_loadzd2fieldzd2zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5567), BgL_arg2271z00_5763,
					BgL_arg2272z00_5764, BgL_arg2273z00_5765);
			}
		}

	}



/* &gen-args-gen-fun-rtl1747 */
	obj_t BGl_z62genzd2argszd2genzd2funzd2rtl1747z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5568, obj_t BgL_funz00_5569, obj_t BgL_mez00_5570,
		obj_t BgL_argsz00_5571)
	{
		{	/* SawJvm/code.scm 557 */
			BGl_outzd2linezd2zzsaw_jvm_codez00(BgL_mez00_5570,
				((BgL_rtl_funz00_bglt) ((BgL_rtl_newz00_bglt) BgL_funz00_5569)));
			{	/* SawJvm/code.scm 559 */
				BgL_typez00_bglt BgL_arg2264z00_5767;
				obj_t BgL_arg2266z00_5768;

				BgL_arg2264z00_5767 =
					(((BgL_rtl_newz00_bglt) COBJECT(
							((BgL_rtl_newz00_bglt) BgL_funz00_5569)))->BgL_typez00);
				BgL_arg2266z00_5768 =
					(((BgL_rtl_newz00_bglt) COBJECT(
							((BgL_rtl_newz00_bglt) BgL_funz00_5569)))->BgL_constrz00);
				{	/* SawJvm/code.scm 560 */
					obj_t BgL_zc3z04anonymousza32267ze3z87_5769;

					BgL_zc3z04anonymousza32267ze3z87_5769 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza32267ze3ze5zzsaw_jvm_codez00, (int) (0L),
						(int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32267ze3z87_5769, (int) (0L),
						BgL_mez00_5570);
					PROCEDURE_SET(BgL_zc3z04anonymousza32267ze3z87_5769, (int) (1L),
						BgL_argsz00_5571);
					return BGl_newobjz00zzsaw_jvm_outz00(((BgL_jvmz00_bglt)
							BgL_mez00_5570), BgL_arg2264z00_5767,
						BgL_zc3z04anonymousza32267ze3z87_5769, BgL_arg2266z00_5768);
				}
			}
		}

	}



/* &<@anonymous:2267> */
	obj_t BGl_z62zc3z04anonymousza32267ze3ze5zzsaw_jvm_codez00(obj_t
		BgL_envz00_5572)
	{
		{	/* SawJvm/code.scm 560 */
			{	/* SawJvm/code.scm 560 */
				obj_t BgL_mez00_5573;
				obj_t BgL_argsz00_5574;

				BgL_mez00_5573 = PROCEDURE_REF(BgL_envz00_5572, (int) (0L));
				BgL_argsz00_5574 = PROCEDURE_REF(BgL_envz00_5572, (int) (1L));
				{	/* SawJvm/code.scm 560 */
					bool_t BgL_tmpz00_7674;

					{
						obj_t BgL_l1688z00_5771;

						BgL_l1688z00_5771 = BgL_argsz00_5574;
					BgL_zc3z04anonymousza32268ze3z87_5770:
						if (PAIRP(BgL_l1688z00_5771))
							{	/* SawJvm/code.scm 560 */
								{	/* SawJvm/code.scm 560 */
									obj_t BgL_az00_5772;

									BgL_az00_5772 = CAR(BgL_l1688z00_5771);
									BGl_genzd2exprzd2zzsaw_jvm_codez00(
										((BgL_jvmz00_bglt) BgL_mez00_5573), BgL_az00_5772);
								}
								{
									obj_t BgL_l1688z00_7680;

									BgL_l1688z00_7680 = CDR(BgL_l1688z00_5771);
									BgL_l1688z00_5771 = BgL_l1688z00_7680;
									goto BgL_zc3z04anonymousza32268ze3z87_5770;
								}
							}
						else
							{	/* SawJvm/code.scm 560 */
								BgL_tmpz00_7674 = ((bool_t) 1);
							}
					}
					return BBOOL(BgL_tmpz00_7674);
				}
			}
		}

	}



/* &gen-fun-rtl_valloc1745 */
	obj_t BGl_z62genzd2funzd2rtl_valloc1745z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5575, obj_t BgL_funz00_5576, obj_t BgL_mez00_5577)
	{
		{	/* SawJvm/code.scm 546 */
			{	/* SawJvm/code.scm 547 */
				obj_t BgL_typez00_5774;

				{	/* SawJvm/code.scm 547 */
					BgL_typez00_bglt BgL_arg2263z00_5775;

					BgL_arg2263z00_5775 =
						(((BgL_rtl_vallocz00_bglt) COBJECT(
								((BgL_rtl_vallocz00_bglt) BgL_funz00_5576)))->BgL_typez00);
					BgL_typez00_5774 =
						BGl_compilezd2typezd2zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_5577), BgL_arg2263z00_5775);
				}
				{	/* SawJvm/code.scm 548 */
					obj_t BgL_arg2259z00_5776;

					{	/* SawJvm/code.scm 548 */
						obj_t BgL_arg2260z00_5777;
						obj_t BgL_arg2261z00_5778;

						{	/* SawJvm/code.scm 548 */
							bool_t BgL_test2754z00_7687;

							{	/* SawJvm/code.scm 548 */
								bool_t BgL__ortest_1237z00_5779;

								BgL__ortest_1237z00_5779 =
									(BgL_typez00_5774 == CNST_TABLE_REF(18));
								if (BgL__ortest_1237z00_5779)
									{	/* SawJvm/code.scm 548 */
										BgL_test2754z00_7687 = BgL__ortest_1237z00_5779;
									}
								else
									{	/* SawJvm/code.scm 548 */
										bool_t BgL__ortest_1238z00_5780;

										BgL__ortest_1238z00_5780 =
											(BgL_typez00_5774 == CNST_TABLE_REF(19));
										if (BgL__ortest_1238z00_5780)
											{	/* SawJvm/code.scm 548 */
												BgL_test2754z00_7687 = BgL__ortest_1238z00_5780;
											}
										else
											{	/* SawJvm/code.scm 548 */
												bool_t BgL__ortest_1239z00_5781;

												BgL__ortest_1239z00_5781 =
													(BgL_typez00_5774 == CNST_TABLE_REF(20));
												if (BgL__ortest_1239z00_5781)
													{	/* SawJvm/code.scm 548 */
														BgL_test2754z00_7687 = BgL__ortest_1239z00_5781;
													}
												else
													{	/* SawJvm/code.scm 548 */
														bool_t BgL__ortest_1240z00_5782;

														BgL__ortest_1240z00_5782 =
															(BgL_typez00_5774 == CNST_TABLE_REF(21));
														if (BgL__ortest_1240z00_5782)
															{	/* SawJvm/code.scm 548 */
																BgL_test2754z00_7687 = BgL__ortest_1240z00_5782;
															}
														else
															{	/* SawJvm/code.scm 548 */
																bool_t BgL__ortest_1241z00_5783;

																BgL__ortest_1241z00_5783 =
																	(BgL_typez00_5774 == CNST_TABLE_REF(22));
																if (BgL__ortest_1241z00_5783)
																	{	/* SawJvm/code.scm 548 */
																		BgL_test2754z00_7687 =
																			BgL__ortest_1241z00_5783;
																	}
																else
																	{	/* SawJvm/code.scm 548 */
																		bool_t BgL__ortest_1242z00_5784;

																		BgL__ortest_1242z00_5784 =
																			(BgL_typez00_5774 == CNST_TABLE_REF(11));
																		if (BgL__ortest_1242z00_5784)
																			{	/* SawJvm/code.scm 548 */
																				BgL_test2754z00_7687 =
																					BgL__ortest_1242z00_5784;
																			}
																		else
																			{	/* SawJvm/code.scm 548 */
																				bool_t BgL__ortest_1243z00_5785;

																				BgL__ortest_1243z00_5785 =
																					(BgL_typez00_5774 ==
																					CNST_TABLE_REF(25));
																				if (BgL__ortest_1243z00_5785)
																					{	/* SawJvm/code.scm 548 */
																						BgL_test2754z00_7687 =
																							BgL__ortest_1243z00_5785;
																					}
																				else
																					{	/* SawJvm/code.scm 548 */
																						BgL_test2754z00_7687 =
																							(BgL_typez00_5774 ==
																							CNST_TABLE_REF(10));
																					}
																			}
																	}
															}
													}
											}
									}
							}
							if (BgL_test2754z00_7687)
								{	/* SawJvm/code.scm 548 */
									BgL_arg2260z00_5777 = CNST_TABLE_REF(71);
								}
							else
								{	/* SawJvm/code.scm 548 */
									BgL_arg2260z00_5777 = CNST_TABLE_REF(72);
								}
						}
						BgL_arg2261z00_5778 = MAKE_YOUNG_PAIR(BgL_typez00_5774, BNIL);
						BgL_arg2259z00_5776 =
							MAKE_YOUNG_PAIR(BgL_arg2260z00_5777, BgL_arg2261z00_5778);
					}
					return
						BGl_codez12z12zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_5577), BgL_arg2259z00_5776);
				}
			}
		}

	}



/* &gen-fun-rtl_vlength1743 */
	obj_t BGl_z62genzd2funzd2rtl_vlength1743z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5578, obj_t BgL_funz00_5579, obj_t BgL_mez00_5580)
	{
		{	/* SawJvm/code.scm 543 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5580), CNST_TABLE_REF(73));
		}

	}



/* &gen-fun-rtl_vset1741 */
	obj_t BGl_z62genzd2funzd2rtl_vset1741z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5581, obj_t BgL_funz00_5582, obj_t BgL_mez00_5583)
	{
		{	/* SawJvm/code.scm 530 */
			{	/* SawJvm/code.scm 531 */
				obj_t BgL_typez00_5788;

				{	/* SawJvm/code.scm 531 */
					BgL_typez00_bglt BgL_arg2258z00_5789;

					BgL_arg2258z00_5789 =
						(((BgL_rtl_vsetz00_bglt) COBJECT(
								((BgL_rtl_vsetz00_bglt) BgL_funz00_5582)))->BgL_typez00);
					BgL_typez00_5788 =
						BGl_compilezd2typezd2zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_5583), BgL_arg2258z00_5789);
				}
				{	/* SawJvm/code.scm 532 */
					obj_t BgL_arg2250z00_5790;

					{	/* SawJvm/code.scm 532 */
						bool_t BgL_test2762z00_7724;

						{	/* SawJvm/code.scm 532 */
							bool_t BgL__ortest_1236z00_5791;

							BgL__ortest_1236z00_5791 =
								(BgL_typez00_5788 == CNST_TABLE_REF(18));
							if (BgL__ortest_1236z00_5791)
								{	/* SawJvm/code.scm 532 */
									BgL_test2762z00_7724 = BgL__ortest_1236z00_5791;
								}
							else
								{	/* SawJvm/code.scm 532 */
									BgL_test2762z00_7724 =
										(BgL_typez00_5788 == CNST_TABLE_REF(19));
								}
						}
						if (BgL_test2762z00_7724)
							{	/* SawJvm/code.scm 532 */
								BgL_arg2250z00_5790 = CNST_TABLE_REF(74);
							}
						else
							{	/* SawJvm/code.scm 532 */
								if ((BgL_typez00_5788 == CNST_TABLE_REF(20)))
									{	/* SawJvm/code.scm 532 */
										BgL_arg2250z00_5790 = CNST_TABLE_REF(75);
									}
								else
									{	/* SawJvm/code.scm 532 */
										if ((BgL_typez00_5788 == CNST_TABLE_REF(21)))
											{	/* SawJvm/code.scm 532 */
												BgL_arg2250z00_5790 = CNST_TABLE_REF(76);
											}
										else
											{	/* SawJvm/code.scm 532 */
												if ((BgL_typez00_5788 == CNST_TABLE_REF(22)))
													{	/* SawJvm/code.scm 532 */
														BgL_arg2250z00_5790 = CNST_TABLE_REF(77);
													}
												else
													{	/* SawJvm/code.scm 532 */
														if ((BgL_typez00_5788 == CNST_TABLE_REF(11)))
															{	/* SawJvm/code.scm 532 */
																BgL_arg2250z00_5790 = CNST_TABLE_REF(78);
															}
														else
															{	/* SawJvm/code.scm 532 */
																if ((BgL_typez00_5788 == CNST_TABLE_REF(25)))
																	{	/* SawJvm/code.scm 532 */
																		BgL_arg2250z00_5790 = CNST_TABLE_REF(79);
																	}
																else
																	{	/* SawJvm/code.scm 532 */
																		if (
																			(BgL_typez00_5788 == CNST_TABLE_REF(10)))
																			{	/* SawJvm/code.scm 532 */
																				BgL_arg2250z00_5790 =
																					CNST_TABLE_REF(80);
																			}
																		else
																			{	/* SawJvm/code.scm 532 */
																				BgL_arg2250z00_5790 =
																					CNST_TABLE_REF(81);
																			}
																	}
															}
													}
											}
									}
							}
					}
					BGl_codez12z12zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_5583), BgL_arg2250z00_5790);
				}
				return CNST_TABLE_REF(13);
			}
		}

	}



/* &gen-fun-rtl_vref1739 */
	obj_t BGl_z62genzd2funzd2rtl_vref1739z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5584, obj_t BgL_funz00_5585, obj_t BgL_mez00_5586)
	{
		{	/* SawJvm/code.scm 518 */
			{	/* SawJvm/code.scm 519 */
				obj_t BgL_typez00_5793;

				{	/* SawJvm/code.scm 519 */
					BgL_typez00_bglt BgL_arg2249z00_5794;

					BgL_arg2249z00_5794 =
						(((BgL_rtl_vrefz00_bglt) COBJECT(
								((BgL_rtl_vrefz00_bglt) BgL_funz00_5585)))->BgL_typez00);
					BgL_typez00_5793 =
						BGl_compilezd2typezd2zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_5586), BgL_arg2249z00_5794);
				}
				{	/* SawJvm/code.scm 520 */
					obj_t BgL_arg2241z00_5795;

					{	/* SawJvm/code.scm 520 */
						bool_t BgL_test2770z00_7763;

						{	/* SawJvm/code.scm 520 */
							bool_t BgL__ortest_1235z00_5796;

							BgL__ortest_1235z00_5796 =
								(BgL_typez00_5793 == CNST_TABLE_REF(18));
							if (BgL__ortest_1235z00_5796)
								{	/* SawJvm/code.scm 520 */
									BgL_test2770z00_7763 = BgL__ortest_1235z00_5796;
								}
							else
								{	/* SawJvm/code.scm 520 */
									BgL_test2770z00_7763 =
										(BgL_typez00_5793 == CNST_TABLE_REF(19));
								}
						}
						if (BgL_test2770z00_7763)
							{	/* SawJvm/code.scm 520 */
								BgL_arg2241z00_5795 = CNST_TABLE_REF(82);
							}
						else
							{	/* SawJvm/code.scm 520 */
								if ((BgL_typez00_5793 == CNST_TABLE_REF(20)))
									{	/* SawJvm/code.scm 520 */
										BgL_arg2241z00_5795 = CNST_TABLE_REF(83);
									}
								else
									{	/* SawJvm/code.scm 520 */
										if ((BgL_typez00_5793 == CNST_TABLE_REF(21)))
											{	/* SawJvm/code.scm 520 */
												BgL_arg2241z00_5795 = CNST_TABLE_REF(84);
											}
										else
											{	/* SawJvm/code.scm 520 */
												if ((BgL_typez00_5793 == CNST_TABLE_REF(22)))
													{	/* SawJvm/code.scm 520 */
														BgL_arg2241z00_5795 = CNST_TABLE_REF(85);
													}
												else
													{	/* SawJvm/code.scm 520 */
														if ((BgL_typez00_5793 == CNST_TABLE_REF(11)))
															{	/* SawJvm/code.scm 520 */
																BgL_arg2241z00_5795 = CNST_TABLE_REF(86);
															}
														else
															{	/* SawJvm/code.scm 520 */
																if ((BgL_typez00_5793 == CNST_TABLE_REF(25)))
																	{	/* SawJvm/code.scm 520 */
																		BgL_arg2241z00_5795 = CNST_TABLE_REF(87);
																	}
																else
																	{	/* SawJvm/code.scm 520 */
																		if (
																			(BgL_typez00_5793 == CNST_TABLE_REF(10)))
																			{	/* SawJvm/code.scm 520 */
																				BgL_arg2241z00_5795 =
																					CNST_TABLE_REF(88);
																			}
																		else
																			{	/* SawJvm/code.scm 520 */
																				BgL_arg2241z00_5795 =
																					CNST_TABLE_REF(89);
																			}
																	}
															}
													}
											}
									}
							}
					}
					return
						BGl_codez12z12zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_5586), BgL_arg2241z00_5795);
				}
			}
		}

	}



/* &gen-fun-rtl_apply1737 */
	obj_t BGl_z62genzd2funzd2rtl_apply1737z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5587, obj_t BgL_funz00_5588, obj_t BgL_mez00_5589)
	{
		{	/* SawJvm/code.scm 512 */
			return
				BGl_codez12z12zzsaw_jvm_outz00(
				((BgL_jvmz00_bglt) BgL_mez00_5589), CNST_TABLE_REF(38));
		}

	}



/* &gen-fun-rtl_loadfun1735 */
	obj_t BGl_z62genzd2funzd2rtl_loadfun1735z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5590, obj_t BgL_funz00_5591, obj_t BgL_mez00_5592)
	{
		{	/* SawJvm/code.scm 508 */
			{	/* SawJvm/code.scm 509 */
				BgL_globalz00_bglt BgL_varz00_5799;

				BgL_varz00_5799 =
					(((BgL_rtl_loadfunz00_bglt) COBJECT(
							((BgL_rtl_loadfunz00_bglt) BgL_funz00_5591)))->BgL_varz00);
				{	/* SawJvm/code.scm 510 */
					int BgL_arg2240z00_5800;

					{
						BgL_indexedz00_bglt BgL_auxz00_7802;

						{
							obj_t BgL_auxz00_7803;

							{	/* SawJvm/code.scm 510 */
								BgL_objectz00_bglt BgL_tmpz00_7804;

								BgL_tmpz00_7804 =
									((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_varz00_5799));
								BgL_auxz00_7803 = BGL_OBJECT_WIDENING(BgL_tmpz00_7804);
							}
							BgL_auxz00_7802 = ((BgL_indexedz00_bglt) BgL_auxz00_7803);
						}
						BgL_arg2240z00_5800 =
							(((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_7802))->BgL_indexz00);
					}
					return
						BGl_pushzd2intzd2zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_5592), BINT(BgL_arg2240z00_5800));
				}
			}
		}

	}



/* &gen-fun-with-args-rt1732 */
	obj_t BGl_z62genzd2funzd2withzd2argszd2rt1732z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5593, obj_t BgL_funz00_5594, obj_t BgL_mez00_5595,
		obj_t BgL_argsz00_5596)
	{
		{	/* SawJvm/code.scm 495 */
			{	/* SawJvm/code.scm 496 */
				obj_t BgL_az00_5802;

				BgL_az00_5802 = CAR(((obj_t) BgL_argsz00_5596));
				{	/* SawJvm/code.scm 497 */
					obj_t BgL_regz00_5803;

					{	/* SawJvm/code.scm 497 */
						bool_t BgL_test2778z00_7815;

						{	/* SawJvm/code.scm 497 */
							obj_t BgL_classz00_5804;

							BgL_classz00_5804 = BGl_rtl_regz00zzsaw_defsz00;
							if (BGL_OBJECTP(BgL_az00_5802))
								{	/* SawJvm/code.scm 497 */
									BgL_objectz00_bglt BgL_arg1807z00_5805;

									BgL_arg1807z00_5805 = (BgL_objectz00_bglt) (BgL_az00_5802);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawJvm/code.scm 497 */
											long BgL_idxz00_5806;

											BgL_idxz00_5806 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5805);
											BgL_test2778z00_7815 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5806 + 1L)) == BgL_classz00_5804);
										}
									else
										{	/* SawJvm/code.scm 497 */
											bool_t BgL_res2467z00_5809;

											{	/* SawJvm/code.scm 497 */
												obj_t BgL_oclassz00_5810;

												{	/* SawJvm/code.scm 497 */
													obj_t BgL_arg1815z00_5811;
													long BgL_arg1816z00_5812;

													BgL_arg1815z00_5811 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawJvm/code.scm 497 */
														long BgL_arg1817z00_5813;

														BgL_arg1817z00_5813 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5805);
														BgL_arg1816z00_5812 =
															(BgL_arg1817z00_5813 - OBJECT_TYPE);
													}
													BgL_oclassz00_5810 =
														VECTOR_REF(BgL_arg1815z00_5811,
														BgL_arg1816z00_5812);
												}
												{	/* SawJvm/code.scm 497 */
													bool_t BgL__ortest_1115z00_5814;

													BgL__ortest_1115z00_5814 =
														(BgL_classz00_5804 == BgL_oclassz00_5810);
													if (BgL__ortest_1115z00_5814)
														{	/* SawJvm/code.scm 497 */
															BgL_res2467z00_5809 = BgL__ortest_1115z00_5814;
														}
													else
														{	/* SawJvm/code.scm 497 */
															long BgL_odepthz00_5815;

															{	/* SawJvm/code.scm 497 */
																obj_t BgL_arg1804z00_5816;

																BgL_arg1804z00_5816 = (BgL_oclassz00_5810);
																BgL_odepthz00_5815 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5816);
															}
															if ((1L < BgL_odepthz00_5815))
																{	/* SawJvm/code.scm 497 */
																	obj_t BgL_arg1802z00_5817;

																	{	/* SawJvm/code.scm 497 */
																		obj_t BgL_arg1803z00_5818;

																		BgL_arg1803z00_5818 = (BgL_oclassz00_5810);
																		BgL_arg1802z00_5817 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5818, 1L);
																	}
																	BgL_res2467z00_5809 =
																		(BgL_arg1802z00_5817 == BgL_classz00_5804);
																}
															else
																{	/* SawJvm/code.scm 497 */
																	BgL_res2467z00_5809 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2778z00_7815 = BgL_res2467z00_5809;
										}
								}
							else
								{	/* SawJvm/code.scm 497 */
									BgL_test2778z00_7815 = ((bool_t) 0);
								}
						}
						if (BgL_test2778z00_7815)
							{	/* SawJvm/code.scm 497 */
								BgL_regz00_5803 = BgL_az00_5802;
							}
						else
							{	/* SawJvm/code.scm 497 */
								BgL_regz00_5803 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_az00_5802)))->BgL_destz00);
							}
					}
					{	/* SawJvm/code.scm 499 */
						obj_t BgL_arg2232z00_5819;

						{	/* SawJvm/code.scm 499 */
							obj_t BgL_casezd2valuezd2_5820;

							BgL_casezd2valuezd2_5820 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_rtl_regz00_bglt) COBJECT(
													((BgL_rtl_regz00_bglt) BgL_regz00_5803)))->
											BgL_typez00)))->BgL_namez00);
							if ((BgL_casezd2valuezd2_5820 == CNST_TABLE_REF(90)))
								{	/* SawJvm/code.scm 499 */
									BgL_arg2232z00_5819 = CNST_TABLE_REF(91);
								}
							else
								{	/* SawJvm/code.scm 499 */
									bool_t BgL_test2784z00_7847;

									{	/* SawJvm/code.scm 499 */
										bool_t BgL__ortest_1231z00_5821;

										BgL__ortest_1231z00_5821 =
											(BgL_casezd2valuezd2_5820 == CNST_TABLE_REF(18));
										if (BgL__ortest_1231z00_5821)
											{	/* SawJvm/code.scm 499 */
												BgL_test2784z00_7847 = BgL__ortest_1231z00_5821;
											}
										else
											{	/* SawJvm/code.scm 499 */
												bool_t BgL__ortest_1232z00_5822;

												BgL__ortest_1232z00_5822 =
													(BgL_casezd2valuezd2_5820 == CNST_TABLE_REF(19));
												if (BgL__ortest_1232z00_5822)
													{	/* SawJvm/code.scm 499 */
														BgL_test2784z00_7847 = BgL__ortest_1232z00_5822;
													}
												else
													{	/* SawJvm/code.scm 499 */
														bool_t BgL__ortest_1233z00_5823;

														BgL__ortest_1233z00_5823 =
															(BgL_casezd2valuezd2_5820 == CNST_TABLE_REF(20));
														if (BgL__ortest_1233z00_5823)
															{	/* SawJvm/code.scm 499 */
																BgL_test2784z00_7847 = BgL__ortest_1233z00_5823;
															}
														else
															{	/* SawJvm/code.scm 499 */
																bool_t BgL__ortest_1234z00_5824;

																BgL__ortest_1234z00_5824 =
																	(BgL_casezd2valuezd2_5820 ==
																	CNST_TABLE_REF(21));
																if (BgL__ortest_1234z00_5824)
																	{	/* SawJvm/code.scm 499 */
																		BgL_test2784z00_7847 =
																			BgL__ortest_1234z00_5824;
																	}
																else
																	{	/* SawJvm/code.scm 499 */
																		BgL_test2784z00_7847 =
																			(BgL_casezd2valuezd2_5820 ==
																			CNST_TABLE_REF(22));
																	}
															}
													}
											}
									}
									if (BgL_test2784z00_7847)
										{	/* SawJvm/code.scm 499 */
											BgL_arg2232z00_5819 = CNST_TABLE_REF(92);
										}
									else
										{	/* SawJvm/code.scm 499 */
											if ((BgL_casezd2valuezd2_5820 == CNST_TABLE_REF(11)))
												{	/* SawJvm/code.scm 499 */
													BgL_arg2232z00_5819 = CNST_TABLE_REF(93);
												}
											else
												{	/* SawJvm/code.scm 499 */
													if ((BgL_casezd2valuezd2_5820 == CNST_TABLE_REF(25)))
														{	/* SawJvm/code.scm 499 */
															BgL_arg2232z00_5819 = CNST_TABLE_REF(94);
														}
													else
														{	/* SawJvm/code.scm 499 */
															if (
																(BgL_casezd2valuezd2_5820 ==
																	CNST_TABLE_REF(10)))
																{	/* SawJvm/code.scm 499 */
																	BgL_arg2232z00_5819 = CNST_TABLE_REF(95);
																}
															else
																{	/* SawJvm/code.scm 499 */
																	BgL_arg2232z00_5819 = CNST_TABLE_REF(7);
																}
														}
												}
										}
								}
						}
						BGl_codez12z12zzsaw_jvm_outz00(
							((BgL_jvmz00_bglt) BgL_mez00_5595), BgL_arg2232z00_5819);
					}
					return CNST_TABLE_REF(13);
				}
			}
		}

	}



/* &gen-fun-with-args-rt1730 */
	obj_t BGl_z62genzd2funzd2withzd2argszd2rt1730z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5597, obj_t BgL_funz00_5598, obj_t BgL_mez00_5599,
		obj_t BgL_argsz00_5600)
	{
		{	/* SawJvm/code.scm 479 */
			return
				BGl_genzd2funcallzd2zzsaw_jvm_codez00(BgL_mez00_5599, BgL_argsz00_5600);
		}

	}



/* &gen-fun-rtl_lightfun1728 */
	obj_t BGl_z62genzd2funzd2rtl_lightfun1728z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5601, obj_t BgL_funz00_5602, obj_t BgL_mez00_5603)
	{
		{	/* SawJvm/code.scm 471 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						((obj_t)
							((BgL_rtl_lightfuncallz00_bglt) BgL_funz00_5602)),
						(((BgL_jvmz00_bglt) COBJECT(
									((BgL_jvmz00_bglt) BgL_mez00_5603)))->
							BgL_lightzd2funcallszd2))))
				{	/* SawJvm/code.scm 474 */
					BFALSE;
				}
			else
				{
					obj_t BgL_auxz00_7887;

					{	/* SawJvm/code.scm 475 */
						obj_t BgL_arg2227z00_5827;

						BgL_arg2227z00_5827 =
							(((BgL_jvmz00_bglt) COBJECT(
									((BgL_jvmz00_bglt) BgL_mez00_5603)))->
							BgL_lightzd2funcallszd2);
						BgL_auxz00_7887 =
							MAKE_YOUNG_PAIR(((obj_t) ((BgL_rtl_lightfuncallz00_bglt)
									BgL_funz00_5602)), BgL_arg2227z00_5827);
					}
					((((BgL_jvmz00_bglt) COBJECT(
									((BgL_jvmz00_bglt) BgL_mez00_5603)))->
							BgL_lightzd2funcallszd2) = ((obj_t) BgL_auxz00_7887), BUNSPEC);
				}
			{	/* SawJvm/code.scm 476 */
				obj_t BgL_arg2229z00_5828;

				{	/* SawJvm/code.scm 476 */
					obj_t BgL_arg2230z00_5829;

					BgL_arg2230z00_5829 =
						MAKE_YOUNG_PAIR(
						(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
									((BgL_rtl_lightfuncallz00_bglt) BgL_funz00_5602)))->
							BgL_namez00), BNIL);
					BgL_arg2229z00_5828 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(96), BgL_arg2230z00_5829);
				}
				return
					BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5603), BgL_arg2229z00_5828);
			}
		}

	}



/* &gen-fun-rtl_call1726 */
	obj_t BGl_z62genzd2funzd2rtl_call1726z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5604, obj_t BgL_funz00_5605, obj_t BgL_mez00_5606)
	{
		{	/* SawJvm/code.scm 457 */
			{	/* SawJvm/code.scm 458 */
				BgL_globalz00_bglt BgL_varz00_5831;

				BgL_varz00_5831 =
					(((BgL_rtl_callz00_bglt) COBJECT(
							((BgL_rtl_callz00_bglt) BgL_funz00_5605)))->BgL_varz00);
				{	/* SawJvm/code.scm 459 */
					obj_t BgL_rz00_5832;

					BgL_rz00_5832 =
						BGl_inlinezd2callzf3z21zzsaw_jvm_inlinez00(
						((BgL_jvmz00_bglt) BgL_mez00_5606), BgL_varz00_5831);
					if ((BgL_rz00_5832 == CNST_TABLE_REF(54)))
						{	/* SawJvm/code.scm 460 */
							BGl_callzd2globalzd2zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_5606), BgL_varz00_5831);
							if (
								((((BgL_typez00_bglt) COBJECT(
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_varz00_5831)))->
													BgL_typez00)))->BgL_namez00) == CNST_TABLE_REF(90)))
								{	/* SawJvm/code.scm 463 */
									return CNST_TABLE_REF(13);
								}
							else
								{	/* SawJvm/code.scm 463 */
									return CNST_TABLE_REF(55);
								}
						}
					else
						{	/* SawJvm/code.scm 460 */
							return BgL_rz00_5832;
						}
				}
			}
		}

	}



/* &gen-args-gen-fun-rtl1724 */
	obj_t BGl_z62genzd2argszd2genzd2funzd2rtl1724z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5607, obj_t BgL_funz00_5608, obj_t BgL_mez00_5609,
		obj_t BgL_argsz00_5610)
	{
		{	/* SawJvm/code.scm 450 */
			{	/* SawJvm/code.scm 451 */
				obj_t BgL_rz00_5834;

				{	/* SawJvm/code.scm 451 */
					BgL_globalz00_bglt BgL_arg2219z00_5835;

					BgL_arg2219z00_5835 =
						(((BgL_rtl_callz00_bglt) COBJECT(
								((BgL_rtl_callz00_bglt) BgL_funz00_5608)))->BgL_varz00);
					BgL_rz00_5834 =
						BGl_inlinezd2callzd2withzd2argszf3z21zzsaw_jvm_inlinez00(
						((BgL_jvmz00_bglt) BgL_mez00_5609), BgL_arg2219z00_5835,
						BgL_argsz00_5610);
				}
				if ((BgL_rz00_5834 == CNST_TABLE_REF(54)))
					{	/* SawJvm/code.scm 452 */
						{
							obj_t BgL_l1683z00_5837;

							BgL_l1683z00_5837 = BgL_argsz00_5610;
						BgL_zc3z04anonymousza32216ze3z87_5836:
							if (PAIRP(BgL_l1683z00_5837))
								{	/* SawJvm/code.scm 453 */
									{	/* SawJvm/code.scm 453 */
										obj_t BgL_az00_5838;

										BgL_az00_5838 = CAR(BgL_l1683z00_5837);
										BGl_genzd2exprzd2zzsaw_jvm_codez00(
											((BgL_jvmz00_bglt) BgL_mez00_5609), BgL_az00_5838);
									}
									{
										obj_t BgL_l1683z00_7931;

										BgL_l1683z00_7931 = CDR(BgL_l1683z00_5837);
										BgL_l1683z00_5837 = BgL_l1683z00_7931;
										goto BgL_zc3z04anonymousza32216ze3z87_5836;
									}
								}
							else
								{	/* SawJvm/code.scm 453 */
									((bool_t) 1);
								}
						}
						return
							BGl_genzd2funzd2zzsaw_jvm_codez00(
							((BgL_rtl_funz00_bglt)
								((BgL_rtl_callz00_bglt) BgL_funz00_5608)), BgL_mez00_5609);
					}
				else
					{	/* SawJvm/code.scm 452 */
						return BgL_rz00_5834;
					}
			}
		}

	}



/* &gen-fun-rtl_switch1722 */
	obj_t BGl_z62genzd2funzd2rtl_switch1722z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5611, obj_t BgL_funz00_5612, obj_t BgL_mez00_5613)
	{
		{	/* SawJvm/code.scm 407 */
			{	/* SawJvm/code.scm 410 */
				obj_t BgL_ldefz00_5840;
				obj_t BgL_num2labz00_5841;

				BgL_ldefz00_5840 = BUNSPEC;
				BgL_num2labz00_5841 = BNIL;
				{
					obj_t BgL_nz00_5843;
					obj_t BgL_labz00_5844;

					{	/* SawJvm/code.scm 415 */
						obj_t BgL_g1674z00_5846;
						obj_t BgL_g1675z00_5847;

						BgL_g1674z00_5846 =
							(((BgL_rtl_selectz00_bglt) COBJECT(
									((BgL_rtl_selectz00_bglt)
										((BgL_rtl_switchz00_bglt) BgL_funz00_5612))))->
							BgL_patternsz00);
						{	/* SawJvm/code.scm 420 */
							obj_t BgL_l1665z00_5848;

							BgL_l1665z00_5848 =
								(((BgL_rtl_switchz00_bglt) COBJECT(
										((BgL_rtl_switchz00_bglt) BgL_funz00_5612)))->
								BgL_labelsz00);
							if (NULLP(BgL_l1665z00_5848))
								{	/* SawJvm/code.scm 420 */
									BgL_g1675z00_5847 = BNIL;
								}
							else
								{	/* SawJvm/code.scm 420 */
									obj_t BgL_head1667z00_5849;

									{	/* SawJvm/code.scm 420 */
										int BgL_arg2174z00_5850;

										BgL_arg2174z00_5850 =
											(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt)
														CAR(((obj_t) BgL_l1665z00_5848)))))->BgL_labelz00);
										BgL_head1667z00_5849 =
											MAKE_YOUNG_PAIR(BINT(BgL_arg2174z00_5850), BNIL);
									}
									{	/* SawJvm/code.scm 420 */
										obj_t BgL_g1670z00_5851;

										BgL_g1670z00_5851 = CDR(((obj_t) BgL_l1665z00_5848));
										{
											obj_t BgL_l1665z00_5853;
											obj_t BgL_tail1668z00_5854;

											BgL_l1665z00_5853 = BgL_g1670z00_5851;
											BgL_tail1668z00_5854 = BgL_head1667z00_5849;
										BgL_zc3z04anonymousza32169ze3z87_5852:
											if (NULLP(BgL_l1665z00_5853))
												{	/* SawJvm/code.scm 420 */
													BgL_g1675z00_5847 = BgL_head1667z00_5849;
												}
											else
												{	/* SawJvm/code.scm 420 */
													obj_t BgL_newtail1669z00_5855;

													{	/* SawJvm/code.scm 420 */
														int BgL_arg2172z00_5856;

														BgL_arg2172z00_5856 =
															(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt)
																		CAR(
																			((obj_t) BgL_l1665z00_5853)))))->
															BgL_labelz00);
														BgL_newtail1669z00_5855 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg2172z00_5856), BNIL);
													}
													SET_CDR(BgL_tail1668z00_5854,
														BgL_newtail1669z00_5855);
													{	/* SawJvm/code.scm 420 */
														obj_t BgL_arg2171z00_5857;

														BgL_arg2171z00_5857 =
															CDR(((obj_t) BgL_l1665z00_5853));
														{
															obj_t BgL_tail1668z00_7963;
															obj_t BgL_l1665z00_7962;

															BgL_l1665z00_7962 = BgL_arg2171z00_5857;
															BgL_tail1668z00_7963 = BgL_newtail1669z00_5855;
															BgL_tail1668z00_5854 = BgL_tail1668z00_7963;
															BgL_l1665z00_5853 = BgL_l1665z00_7962;
															goto BgL_zc3z04anonymousza32169ze3z87_5852;
														}
													}
												}
										}
									}
								}
						}
						{
							obj_t BgL_ll1671z00_5859;
							obj_t BgL_ll1672z00_5860;

							BgL_ll1671z00_5859 = BgL_g1674z00_5846;
							BgL_ll1672z00_5860 = BgL_g1675z00_5847;
						BgL_zc3z04anonymousza32161ze3z87_5858:
							if (NULLP(BgL_ll1671z00_5859))
								{	/* SawJvm/code.scm 415 */
									((bool_t) 1);
								}
							else
								{	/* SawJvm/code.scm 415 */
									{	/* SawJvm/code.scm 416 */
										obj_t BgL_patz00_5861;
										obj_t BgL_labz00_5862;

										BgL_patz00_5861 = CAR(((obj_t) BgL_ll1671z00_5859));
										BgL_labz00_5862 = CAR(((obj_t) BgL_ll1672z00_5860));
										if ((BgL_patz00_5861 == CNST_TABLE_REF(97)))
											{	/* SawJvm/code.scm 416 */
												BgL_ldefz00_5840 =
													BGl_Lze70ze7zzsaw_jvm_codez00(BgL_labz00_5862);
											}
										else
											{
												obj_t BgL_l1663z00_5864;

												{	/* SawJvm/code.scm 418 */
													bool_t BgL_tmpz00_7974;

													BgL_l1663z00_5864 = BgL_patz00_5861;
												BgL_zc3z04anonymousza32163ze3z87_5863:
													if (PAIRP(BgL_l1663z00_5864))
														{	/* SawJvm/code.scm 418 */
															BgL_nz00_5843 = CAR(BgL_l1663z00_5864);
															BgL_labz00_5844 = BgL_labz00_5862;
															{	/* SawJvm/code.scm 414 */
																obj_t BgL_arg2213z00_5845;

																BgL_arg2213z00_5845 =
																	MAKE_YOUNG_PAIR(BGl_intifyz00zzsaw_jvm_codez00
																	(BgL_nz00_5843),
																	BGl_Lze70ze7zzsaw_jvm_codez00
																	(BgL_labz00_5844));
																BgL_num2labz00_5841 =
																	MAKE_YOUNG_PAIR(BgL_arg2213z00_5845,
																	BgL_num2labz00_5841);
															}
															{
																obj_t BgL_l1663z00_7982;

																BgL_l1663z00_7982 = CDR(BgL_l1663z00_5864);
																BgL_l1663z00_5864 = BgL_l1663z00_7982;
																goto BgL_zc3z04anonymousza32163ze3z87_5863;
															}
														}
													else
														{	/* SawJvm/code.scm 418 */
															BgL_tmpz00_7974 = ((bool_t) 1);
														}
													BBOOL(BgL_tmpz00_7974);
												}
											}
									}
									{	/* SawJvm/code.scm 415 */
										obj_t BgL_arg2166z00_5865;
										obj_t BgL_arg2167z00_5866;

										BgL_arg2166z00_5865 = CDR(((obj_t) BgL_ll1671z00_5859));
										BgL_arg2167z00_5866 = CDR(((obj_t) BgL_ll1672z00_5860));
										{
											obj_t BgL_ll1672z00_7990;
											obj_t BgL_ll1671z00_7989;

											BgL_ll1671z00_7989 = BgL_arg2166z00_5865;
											BgL_ll1672z00_7990 = BgL_arg2167z00_5866;
											BgL_ll1672z00_5860 = BgL_ll1672z00_7990;
											BgL_ll1671z00_5859 = BgL_ll1671z00_7989;
											goto BgL_zc3z04anonymousza32161ze3z87_5858;
										}
									}
								}
						}
					}
					if (NULLP(CDR(BgL_num2labz00_5841)))
						{	/* SawJvm/code.scm 422 */
							{	/* SawJvm/code.scm 423 */
								obj_t BgL_arg2178z00_5867;

								{	/* SawJvm/code.scm 423 */
									obj_t BgL_pairz00_5868;

									BgL_pairz00_5868 = BgL_num2labz00_5841;
									BgL_arg2178z00_5867 = CAR(CAR(BgL_pairz00_5868));
								}
								BGl_pushzd2intzd2zzsaw_jvm_outz00(
									((BgL_jvmz00_bglt) BgL_mez00_5613), BgL_arg2178z00_5867);
							}
							BGl_branchz00zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_5613),
								CNST_TABLE_REF(98), BgL_ldefz00_5840);
							{	/* SawJvm/code.scm 425 */
								obj_t BgL_arg2179z00_5869;

								{	/* SawJvm/code.scm 425 */
									obj_t BgL_pairz00_5870;

									BgL_pairz00_5870 = BgL_num2labz00_5841;
									BgL_arg2179z00_5869 = CDR(CAR(BgL_pairz00_5870));
								}
								BGl_branchz00zzsaw_jvm_outz00(
									((BgL_jvmz00_bglt) BgL_mez00_5613),
									CNST_TABLE_REF(70), BgL_arg2179z00_5869);
							}
						}
					else
						{	/* SawJvm/code.scm 422 */
							BgL_num2labz00_5841 =
								BGl_sortz00zz__r4_vectors_6_8z00(BgL_num2labz00_5841,
								BGl_proc2539z00zzsaw_jvm_codez00);
							{	/* SawJvm/code.scm 428 */
								obj_t BgL_numsz00_5872;

								{	/* SawJvm/code.scm 428 */
									obj_t BgL_l1676z00_5873;

									BgL_l1676z00_5873 = BgL_num2labz00_5841;
									if (NULLP(BgL_l1676z00_5873))
										{	/* SawJvm/code.scm 428 */
											BgL_numsz00_5872 = BNIL;
										}
									else
										{	/* SawJvm/code.scm 428 */
											obj_t BgL_head1678z00_5874;

											{	/* SawJvm/code.scm 428 */
												obj_t BgL_arg2206z00_5875;

												{	/* SawJvm/code.scm 428 */
													obj_t BgL_pairz00_5876;

													BgL_pairz00_5876 = CAR(((obj_t) BgL_l1676z00_5873));
													BgL_arg2206z00_5875 = CAR(BgL_pairz00_5876);
												}
												BgL_head1678z00_5874 =
													MAKE_YOUNG_PAIR(BgL_arg2206z00_5875, BNIL);
											}
											{	/* SawJvm/code.scm 428 */
												obj_t BgL_g1681z00_5877;

												BgL_g1681z00_5877 = CDR(((obj_t) BgL_l1676z00_5873));
												{
													obj_t BgL_l1676z00_5879;
													obj_t BgL_tail1679z00_5880;

													BgL_l1676z00_5879 = BgL_g1681z00_5877;
													BgL_tail1679z00_5880 = BgL_head1678z00_5874;
												BgL_zc3z04anonymousza32201ze3z87_5878:
													if (NULLP(BgL_l1676z00_5879))
														{	/* SawJvm/code.scm 428 */
															BgL_numsz00_5872 = BgL_head1678z00_5874;
														}
													else
														{	/* SawJvm/code.scm 428 */
															obj_t BgL_newtail1680z00_5881;

															{	/* SawJvm/code.scm 428 */
																obj_t BgL_arg2204z00_5882;

																{	/* SawJvm/code.scm 428 */
																	obj_t BgL_pairz00_5883;

																	BgL_pairz00_5883 =
																		CAR(((obj_t) BgL_l1676z00_5879));
																	BgL_arg2204z00_5882 = CAR(BgL_pairz00_5883);
																}
																BgL_newtail1680z00_5881 =
																	MAKE_YOUNG_PAIR(BgL_arg2204z00_5882, BNIL);
															}
															SET_CDR(BgL_tail1679z00_5880,
																BgL_newtail1680z00_5881);
															{	/* SawJvm/code.scm 428 */
																obj_t BgL_arg2203z00_5884;

																BgL_arg2203z00_5884 =
																	CDR(((obj_t) BgL_l1676z00_5879));
																{
																	obj_t BgL_tail1679z00_8025;
																	obj_t BgL_l1676z00_8024;

																	BgL_l1676z00_8024 = BgL_arg2203z00_5884;
																	BgL_tail1679z00_8025 =
																		BgL_newtail1680z00_5881;
																	BgL_tail1679z00_5880 = BgL_tail1679z00_8025;
																	BgL_l1676z00_5879 = BgL_l1676z00_8024;
																	goto BgL_zc3z04anonymousza32201ze3z87_5878;
																}
															}
														}
												}
											}
										}
								}
								{	/* SawJvm/code.scm 428 */
									obj_t BgL_minz00_5885;

									BgL_minz00_5885 = CAR(((obj_t) BgL_numsz00_5872));
									{	/* SawJvm/code.scm 429 */
										obj_t BgL_maxz00_5886;

										BgL_maxz00_5886 =
											CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
											(BgL_numsz00_5872));
										{	/* SawJvm/code.scm 430 */
											long BgL_nz00_5887;

											BgL_nz00_5887 = bgl_list_length(BgL_numsz00_5872);
											{	/* SawJvm/code.scm 431 */

												{	/* SawJvm/code.scm 432 */
													bool_t BgL_test2805z00_8031;

													{	/* SawJvm/code.scm 432 */
														obj_t BgL_a1682z00_5888;

														BgL_a1682z00_5888 =
															BGl_2zf2zf2zz__r4_numbers_6_5z00(BINT
															(BgL_nz00_5887),
															BINT((1L + ((long) CINT(BgL_maxz00_5886) -
																		(long) CINT(BgL_minz00_5885)))));
														if (REALP(BgL_a1682z00_5888))
															{	/* SawJvm/code.scm 432 */
																BgL_test2805z00_8031 =
																	(REAL_TO_DOUBLE(BgL_a1682z00_5888) <
																	((double) 0.75));
															}
														else
															{	/* SawJvm/code.scm 432 */
																BgL_test2805z00_8031 =
																	BGl_2zc3zc3zz__r4_numbers_6_5z00
																	(BgL_a1682z00_5888,
																	BGL_REAL_CNST
																	(BGl_real2540z00zzsaw_jvm_codez00));
															}
													}
													if (BgL_test2805z00_8031)
														{	/* SawJvm/code.scm 433 */
															obj_t BgL_arg2188z00_5889;

															{	/* SawJvm/code.scm 433 */
																obj_t BgL_arg2189z00_5890;

																{	/* SawJvm/code.scm 433 */
																	obj_t BgL_arg2190z00_5891;

																	BgL_arg2190z00_5891 =
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BgL_num2labz00_5841, BNIL);
																	BgL_arg2189z00_5890 =
																		MAKE_YOUNG_PAIR(BgL_ldefz00_5840,
																		BgL_arg2190z00_5891);
																}
																BgL_arg2188z00_5889 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(99),
																	BgL_arg2189z00_5890);
															}
															BGl_codez12z12zzsaw_jvm_outz00(
																((BgL_jvmz00_bglt) BgL_mez00_5613),
																BgL_arg2188z00_5889);
														}
													else
														{	/* SawJvm/code.scm 435 */
															obj_t BgL_arg2191z00_5892;

															{	/* SawJvm/code.scm 435 */
																obj_t BgL_arg2192z00_5893;

																{	/* SawJvm/code.scm 435 */
																	obj_t BgL_arg2193z00_5894;

																	BgL_arg2193z00_5894 =
																		MAKE_YOUNG_PAIR(BgL_minz00_5885,
																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																		(BGl_flatz00zzsaw_jvm_codez00
																			(BgL_num2labz00_5841, BgL_ldefz00_5840),
																			BNIL));
																	BgL_arg2192z00_5893 =
																		MAKE_YOUNG_PAIR(BgL_ldefz00_5840,
																		BgL_arg2193z00_5894);
																}
																BgL_arg2191z00_5892 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(100),
																	BgL_arg2192z00_5893);
															}
															BGl_codez12z12zzsaw_jvm_outz00(
																((BgL_jvmz00_bglt) BgL_mez00_5613),
																BgL_arg2191z00_5892);
														}
												}
											}
										}
									}
								}
							}
						}
					return CNST_TABLE_REF(13);
				}
			}
		}

	}



/* L~0 */
	obj_t BGl_Lze70ze7zzsaw_jvm_codez00(obj_t BgL_nz00_3497)
	{
		{	/* SawJvm/code.scm 412 */
			{	/* SawJvm/code.scm 412 */
				obj_t BgL_arg2210z00_3499;

				{	/* SawJvm/code.scm 412 */
					obj_t BgL_arg2211z00_3500;

					{	/* SawJvm/code.scm 412 */

						BgL_arg2211z00_3500 =
							BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
							(long) CINT(BgL_nz00_3497), 10L);
					}
					BgL_arg2210z00_3499 =
						string_append(BGl_string2541z00zzsaw_jvm_codez00,
						BgL_arg2211z00_3500);
				}
				return bstring_to_symbol(BgL_arg2210z00_3499);
			}
		}

	}



/* &<@anonymous:2181> */
	obj_t BGl_z62zc3z04anonymousza32181ze3ze5zzsaw_jvm_codez00(obj_t
		BgL_envz00_5614, obj_t BgL_xz00_5615, obj_t BgL_yz00_5616)
	{
		{	/* SawJvm/code.scm 427 */
			return
				BBOOL(
				((long) CINT(CAR(
							((obj_t) BgL_xz00_5615))) <
					(long) CINT(CAR(((obj_t) BgL_yz00_5616)))));
		}

	}



/* &gen-fun-rtl_go1720 */
	obj_t BGl_z62genzd2funzd2rtl_go1720z62zzsaw_jvm_codez00(obj_t BgL_envz00_5617,
		obj_t BgL_funz00_5618, obj_t BgL_mez00_5619)
	{
		{	/* SawJvm/code.scm 396 */
			{	/* SawJvm/code.scm 397 */
				int BgL_arg2159z00_5896;

				BgL_arg2159z00_5896 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_goz00_bglt) COBJECT(
										((BgL_rtl_goz00_bglt) BgL_funz00_5618)))->BgL_toz00)))->
					BgL_labelz00);
				BGl_branchz00zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5619),
					CNST_TABLE_REF(70), BINT(BgL_arg2159z00_5896));
			}
			return CNST_TABLE_REF(13);
		}

	}



/* &gen-args-gen-fun-rtl1718 */
	obj_t BGl_z62genzd2argszd2genzd2funzd2rtl1718z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5620, obj_t BgL_funz00_5621, obj_t BgL_mez00_5622,
		obj_t BgL_argsz00_5623)
	{
		{	/* SawJvm/code.scm 387 */
			{	/* SawJvm/code.scm 388 */
				obj_t BgL_argz00_5898;
				int BgL_labz00_5899;

				BgL_argz00_5898 = CAR(((obj_t) BgL_argsz00_5623));
				BgL_labz00_5899 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_ifnez00_bglt) COBJECT(
										((BgL_rtl_ifnez00_bglt) BgL_funz00_5621)))->BgL_thenz00)))->
					BgL_labelz00);
				{	/* SawJvm/code.scm 389 */
					bool_t BgL_test2807z00_8084;

					{	/* SawJvm/code.scm 389 */
						obj_t BgL_classz00_5900;

						BgL_classz00_5900 = BGl_rtl_regz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_argz00_5898))
							{	/* SawJvm/code.scm 389 */
								BgL_objectz00_bglt BgL_arg1807z00_5901;

								BgL_arg1807z00_5901 = (BgL_objectz00_bglt) (BgL_argz00_5898);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawJvm/code.scm 389 */
										long BgL_idxz00_5902;

										BgL_idxz00_5902 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5901);
										BgL_test2807z00_8084 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5902 + 1L)) == BgL_classz00_5900);
									}
								else
									{	/* SawJvm/code.scm 389 */
										bool_t BgL_res2466z00_5905;

										{	/* SawJvm/code.scm 389 */
											obj_t BgL_oclassz00_5906;

											{	/* SawJvm/code.scm 389 */
												obj_t BgL_arg1815z00_5907;
												long BgL_arg1816z00_5908;

												BgL_arg1815z00_5907 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawJvm/code.scm 389 */
													long BgL_arg1817z00_5909;

													BgL_arg1817z00_5909 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5901);
													BgL_arg1816z00_5908 =
														(BgL_arg1817z00_5909 - OBJECT_TYPE);
												}
												BgL_oclassz00_5906 =
													VECTOR_REF(BgL_arg1815z00_5907, BgL_arg1816z00_5908);
											}
											{	/* SawJvm/code.scm 389 */
												bool_t BgL__ortest_1115z00_5910;

												BgL__ortest_1115z00_5910 =
													(BgL_classz00_5900 == BgL_oclassz00_5906);
												if (BgL__ortest_1115z00_5910)
													{	/* SawJvm/code.scm 389 */
														BgL_res2466z00_5905 = BgL__ortest_1115z00_5910;
													}
												else
													{	/* SawJvm/code.scm 389 */
														long BgL_odepthz00_5911;

														{	/* SawJvm/code.scm 389 */
															obj_t BgL_arg1804z00_5912;

															BgL_arg1804z00_5912 = (BgL_oclassz00_5906);
															BgL_odepthz00_5911 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5912);
														}
														if ((1L < BgL_odepthz00_5911))
															{	/* SawJvm/code.scm 389 */
																obj_t BgL_arg1802z00_5913;

																{	/* SawJvm/code.scm 389 */
																	obj_t BgL_arg1803z00_5914;

																	BgL_arg1803z00_5914 = (BgL_oclassz00_5906);
																	BgL_arg1802z00_5913 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5914,
																		1L);
																}
																BgL_res2466z00_5905 =
																	(BgL_arg1802z00_5913 == BgL_classz00_5900);
															}
														else
															{	/* SawJvm/code.scm 389 */
																BgL_res2466z00_5905 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2807z00_8084 = BgL_res2466z00_5905;
									}
							}
						else
							{	/* SawJvm/code.scm 389 */
								BgL_test2807z00_8084 = ((bool_t) 0);
							}
					}
					if (BgL_test2807z00_8084)
						{	/* SawJvm/code.scm 389 */
							BGl_loadzd2regzd2zzsaw_jvm_codez00(BgL_mez00_5622,
								BgL_argz00_5898);
							BGl_branchz00zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5622),
								CNST_TABLE_REF(51), BINT(BgL_labz00_5899));
						}
					else
						{	/* SawJvm/code.scm 393 */
							BgL_rtl_funz00_bglt BgL_arg2156z00_5915;
							obj_t BgL_arg2157z00_5916;

							BgL_arg2156z00_5915 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_argz00_5898)))->BgL_funz00);
							BgL_arg2157z00_5916 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_argz00_5898)))->BgL_argsz00);
							BGl_genzd2argszd2genzd2predicatezd2zzsaw_jvm_codez00
								(BgL_arg2156z00_5915, BgL_mez00_5622, BgL_arg2157z00_5916,
								BTRUE, BINT(BgL_labz00_5899));
						}
				}
				return CNST_TABLE_REF(13);
			}
		}

	}



/* &gen-args-gen-fun-rtl1716 */
	obj_t BGl_z62genzd2argszd2genzd2funzd2rtl1716z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5624, obj_t BgL_funz00_5625, obj_t BgL_mez00_5626,
		obj_t BgL_argsz00_5627)
	{
		{	/* SawJvm/code.scm 378 */
			{	/* SawJvm/code.scm 379 */
				obj_t BgL_argz00_5918;
				int BgL_labz00_5919;

				BgL_argz00_5918 = CAR(((obj_t) BgL_argsz00_5627));
				BgL_labz00_5919 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_ifeqz00_bglt) COBJECT(
										((BgL_rtl_ifeqz00_bglt) BgL_funz00_5625)))->BgL_thenz00)))->
					BgL_labelz00);
				{	/* SawJvm/code.scm 380 */
					bool_t BgL_test2812z00_8124;

					{	/* SawJvm/code.scm 380 */
						obj_t BgL_classz00_5920;

						BgL_classz00_5920 = BGl_rtl_regz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_argz00_5918))
							{	/* SawJvm/code.scm 380 */
								BgL_objectz00_bglt BgL_arg1807z00_5921;

								BgL_arg1807z00_5921 = (BgL_objectz00_bglt) (BgL_argz00_5918);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawJvm/code.scm 380 */
										long BgL_idxz00_5922;

										BgL_idxz00_5922 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5921);
										BgL_test2812z00_8124 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5922 + 1L)) == BgL_classz00_5920);
									}
								else
									{	/* SawJvm/code.scm 380 */
										bool_t BgL_res2465z00_5925;

										{	/* SawJvm/code.scm 380 */
											obj_t BgL_oclassz00_5926;

											{	/* SawJvm/code.scm 380 */
												obj_t BgL_arg1815z00_5927;
												long BgL_arg1816z00_5928;

												BgL_arg1815z00_5927 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawJvm/code.scm 380 */
													long BgL_arg1817z00_5929;

													BgL_arg1817z00_5929 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5921);
													BgL_arg1816z00_5928 =
														(BgL_arg1817z00_5929 - OBJECT_TYPE);
												}
												BgL_oclassz00_5926 =
													VECTOR_REF(BgL_arg1815z00_5927, BgL_arg1816z00_5928);
											}
											{	/* SawJvm/code.scm 380 */
												bool_t BgL__ortest_1115z00_5930;

												BgL__ortest_1115z00_5930 =
													(BgL_classz00_5920 == BgL_oclassz00_5926);
												if (BgL__ortest_1115z00_5930)
													{	/* SawJvm/code.scm 380 */
														BgL_res2465z00_5925 = BgL__ortest_1115z00_5930;
													}
												else
													{	/* SawJvm/code.scm 380 */
														long BgL_odepthz00_5931;

														{	/* SawJvm/code.scm 380 */
															obj_t BgL_arg1804z00_5932;

															BgL_arg1804z00_5932 = (BgL_oclassz00_5926);
															BgL_odepthz00_5931 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5932);
														}
														if ((1L < BgL_odepthz00_5931))
															{	/* SawJvm/code.scm 380 */
																obj_t BgL_arg1802z00_5933;

																{	/* SawJvm/code.scm 380 */
																	obj_t BgL_arg1803z00_5934;

																	BgL_arg1803z00_5934 = (BgL_oclassz00_5926);
																	BgL_arg1802z00_5933 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5934,
																		1L);
																}
																BgL_res2465z00_5925 =
																	(BgL_arg1802z00_5933 == BgL_classz00_5920);
															}
														else
															{	/* SawJvm/code.scm 380 */
																BgL_res2465z00_5925 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2812z00_8124 = BgL_res2465z00_5925;
									}
							}
						else
							{	/* SawJvm/code.scm 380 */
								BgL_test2812z00_8124 = ((bool_t) 0);
							}
					}
					if (BgL_test2812z00_8124)
						{	/* SawJvm/code.scm 380 */
							BGl_loadzd2regzd2zzsaw_jvm_codez00(BgL_mez00_5626,
								BgL_argz00_5918);
							BGl_branchz00zzsaw_jvm_outz00(((BgL_jvmz00_bglt) BgL_mez00_5626),
								CNST_TABLE_REF(52), BINT(BgL_labz00_5919));
						}
					else
						{	/* SawJvm/code.scm 384 */
							BgL_rtl_funz00_bglt BgL_arg2151z00_5935;
							obj_t BgL_arg2152z00_5936;

							BgL_arg2151z00_5935 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_argz00_5918)))->BgL_funz00);
							BgL_arg2152z00_5936 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_argz00_5918)))->BgL_argsz00);
							BGl_genzd2argszd2genzd2predicatezd2zzsaw_jvm_codez00
								(BgL_arg2151z00_5935, BgL_mez00_5626, BgL_arg2152z00_5936,
								BFALSE, BINT(BgL_labz00_5919));
						}
				}
				return CNST_TABLE_REF(13);
			}
		}

	}



/* &gen-fun-rtl_globalre1714 */
	obj_t BGl_z62genzd2funzd2rtl_globalre1714z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5628, obj_t BgL_funz00_5629, obj_t BgL_mez00_5630)
	{
		{	/* SawJvm/code.scm 366 */
			{	/* SawJvm/code.scm 367 */
				BgL_globalz00_bglt BgL_varz00_5938;

				BgL_varz00_5938 =
					(((BgL_rtl_globalrefz00_bglt) COBJECT(
							((BgL_rtl_globalrefz00_bglt) BgL_funz00_5629)))->BgL_varz00);
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5630), CNST_TABLE_REF(101));
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5630), CNST_TABLE_REF(64));
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5630), CNST_TABLE_REF(102));
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5630), CNST_TABLE_REF(64));
				{	/* SawJvm/code.scm 372 */
					int BgL_arg2149z00_5939;

					{
						BgL_indexedz00_bglt BgL_auxz00_8173;

						{
							obj_t BgL_auxz00_8174;

							{	/* SawJvm/code.scm 372 */
								BgL_objectz00_bglt BgL_tmpz00_8175;

								BgL_tmpz00_8175 =
									((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_varz00_5938));
								BgL_auxz00_8174 = BGL_OBJECT_WIDENING(BgL_tmpz00_8175);
							}
							BgL_auxz00_8173 = ((BgL_indexedz00_bglt) BgL_auxz00_8174);
						}
						BgL_arg2149z00_5939 =
							(((BgL_indexedz00_bglt) COBJECT(BgL_auxz00_8173))->BgL_indexz00);
					}
					BGl_pushzd2intzd2zzsaw_jvm_outz00(
						((BgL_jvmz00_bglt) BgL_mez00_5630), BINT(BgL_arg2149z00_5939));
				}
				return
					BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5630), CNST_TABLE_REF(103));
			}
		}

	}



/* &gen-fun-rtl_storeg1712 */
	obj_t BGl_z62genzd2funzd2rtl_storeg1712z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5631, obj_t BgL_funz00_5632, obj_t BgL_mez00_5633)
	{
		{	/* SawJvm/code.scm 362 */
			{	/* SawJvm/code.scm 363 */
				obj_t BgL_arg2145z00_5941;

				{	/* SawJvm/code.scm 363 */
					obj_t BgL_arg2146z00_5942;

					{	/* SawJvm/code.scm 363 */
						obj_t BgL_arg2147z00_5943;

						{	/* SawJvm/code.scm 363 */
							BgL_globalz00_bglt BgL_arg2148z00_5944;

							BgL_arg2148z00_5944 =
								(((BgL_rtl_storegz00_bglt) COBJECT(
										((BgL_rtl_storegz00_bglt) BgL_funz00_5632)))->BgL_varz00);
							BgL_arg2147z00_5943 =
								BGl_declarezd2globalzd2zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_5633), BgL_arg2148z00_5944);
						}
						BgL_arg2146z00_5942 = MAKE_YOUNG_PAIR(BgL_arg2147z00_5943, BNIL);
					}
					BgL_arg2145z00_5941 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(104), BgL_arg2146z00_5942);
				}
				BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5633), BgL_arg2145z00_5941);
			}
			return CNST_TABLE_REF(13);
		}

	}



/* &gen-fun-rtl_loadg1710 */
	obj_t BGl_z62genzd2funzd2rtl_loadg1710z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5634, obj_t BgL_funz00_5635, obj_t BgL_mez00_5636)
	{
		{	/* SawJvm/code.scm 359 */
			{	/* SawJvm/code.scm 360 */
				obj_t BgL_arg2141z00_5946;

				{	/* SawJvm/code.scm 360 */
					obj_t BgL_arg2142z00_5947;

					{	/* SawJvm/code.scm 360 */
						obj_t BgL_arg2143z00_5948;

						{	/* SawJvm/code.scm 360 */
							BgL_globalz00_bglt BgL_arg2144z00_5949;

							BgL_arg2144z00_5949 =
								(((BgL_rtl_loadgz00_bglt) COBJECT(
										((BgL_rtl_loadgz00_bglt) BgL_funz00_5635)))->BgL_varz00);
							BgL_arg2143z00_5948 =
								BGl_declarezd2globalzd2zzsaw_jvm_outz00(
								((BgL_jvmz00_bglt) BgL_mez00_5636), BgL_arg2144z00_5949);
						}
						BgL_arg2142z00_5947 = MAKE_YOUNG_PAIR(BgL_arg2143z00_5948, BNIL);
					}
					BgL_arg2141z00_5946 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(105), BgL_arg2142z00_5947);
				}
				return
					BGl_codez12z12zzsaw_jvm_outz00(
					((BgL_jvmz00_bglt) BgL_mez00_5636), BgL_arg2141z00_5946);
			}
		}

	}



/* &gen-fun-rtl_loadi1708 */
	obj_t BGl_z62genzd2funzd2rtl_loadi1708z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5637, obj_t BgL_funz00_5638, obj_t BgL_mez00_5639)
	{
		{	/* SawJvm/code.scm 322 */
			{	/* SawJvm/code.scm 323 */
				BgL_atomz00_bglt BgL_constantz00_5951;

				BgL_constantz00_5951 =
					(((BgL_rtl_loadiz00_bglt) COBJECT(
							((BgL_rtl_loadiz00_bglt) BgL_funz00_5638)))->BgL_constantz00);
				{	/* SawJvm/code.scm 324 */
					obj_t BgL_valuez00_5952;

					BgL_valuez00_5952 =
						(((BgL_atomz00_bglt) COBJECT(BgL_constantz00_5951))->BgL_valuez00);
					if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_valuez00_5952))
						{	/* SawJvm/code.scm 327 */
							obj_t BgL_arg2129z00_5953;

							BgL_arg2129z00_5953 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_constantz00_5951)))->
											BgL_typez00)))->BgL_namez00);
							return BGl_pushzd2numzd2zzsaw_jvm_outz00(((BgL_jvmz00_bglt)
									BgL_mez00_5639), BgL_valuez00_5952, BgL_arg2129z00_5953);
						}
					else
						{	/* SawJvm/code.scm 326 */
							if (NULLP(BgL_valuez00_5952))
								{	/* SawJvm/code.scm 328 */
									return
										BGl_codez12z12zzsaw_jvm_outz00(
										((BgL_jvmz00_bglt) BgL_mez00_5639), CNST_TABLE_REF(36));
								}
							else
								{	/* SawJvm/code.scm 328 */
									if (BOOLEANP(BgL_valuez00_5952))
										{	/* SawJvm/code.scm 331 */
											long BgL_arg2133z00_5954;

											if (CBOOL(BgL_valuez00_5952))
												{	/* SawJvm/code.scm 331 */
													BgL_arg2133z00_5954 = 1L;
												}
											else
												{	/* SawJvm/code.scm 331 */
													BgL_arg2133z00_5954 = 0L;
												}
											return
												BGl_pushzd2intzd2zzsaw_jvm_outz00(
												((BgL_jvmz00_bglt) BgL_mez00_5639),
												BINT(BgL_arg2133z00_5954));
										}
									else
										{	/* SawJvm/code.scm 330 */
											if (CHARP(BgL_valuez00_5952))
												{	/* SawJvm/code.scm 333 */
													long BgL_arg2135z00_5955;

													BgL_arg2135z00_5955 = (CCHAR(BgL_valuez00_5952));
													return
														BGl_pushzd2intzd2zzsaw_jvm_outz00(
														((BgL_jvmz00_bglt) BgL_mez00_5639),
														BINT(BgL_arg2135z00_5955));
												}
											else
												{	/* SawJvm/code.scm 332 */
													if (STRINGP(BgL_valuez00_5952))
														{	/* SawJvm/code.scm 334 */
															BGl_pushzd2stringzd2zzsaw_jvm_outz00(
																((BgL_jvmz00_bglt) BgL_mez00_5639),
																BgL_valuez00_5952);
															return
																BGl_codez12z12zzsaw_jvm_outz00((
																	(BgL_jvmz00_bglt) BgL_mez00_5639),
																CNST_TABLE_REF(106));
														}
													else
														{	/* SawJvm/code.scm 334 */
															if (EOF_OBJECTP(BgL_valuez00_5952))
																{	/* SawJvm/code.scm 338 */
																	return
																		BGl_codez12z12zzsaw_jvm_outz00(
																		((BgL_jvmz00_bglt) BgL_mez00_5639),
																		CNST_TABLE_REF(107));
																}
															else
																{	/* SawJvm/code.scm 338 */
																	if ((BgL_valuez00_5952 == (BOPTIONAL)))
																		{	/* SawJvm/code.scm 340 */
																			return
																				BGl_codez12z12zzsaw_jvm_outz00(
																				((BgL_jvmz00_bglt) BgL_mez00_5639),
																				CNST_TABLE_REF(108));
																		}
																	else
																		{	/* SawJvm/code.scm 340 */
																			if ((BgL_valuez00_5952 == (BREST)))
																				{	/* SawJvm/code.scm 342 */
																					return
																						BGl_codez12z12zzsaw_jvm_outz00(
																						((BgL_jvmz00_bglt) BgL_mez00_5639),
																						CNST_TABLE_REF(109));
																				}
																			else
																				{	/* SawJvm/code.scm 342 */
																					if ((BgL_valuez00_5952 == (BKEY)))
																						{	/* SawJvm/code.scm 344 */
																							return
																								BGl_codez12z12zzsaw_jvm_outz00(
																								((BgL_jvmz00_bglt)
																									BgL_mez00_5639),
																								CNST_TABLE_REF(110));
																						}
																					else
																						{	/* SawJvm/code.scm 344 */
																							if (
																								(BgL_valuez00_5952 == BUNSPEC))
																								{	/* SawJvm/code.scm 346 */
																									return
																										BGl_codez12z12zzsaw_jvm_outz00
																										(((BgL_jvmz00_bglt)
																											BgL_mez00_5639),
																										CNST_TABLE_REF(14));
																								}
																							else
																								{	/* SawJvm/code.scm 346 */
																									if (UCS2P(BgL_valuez00_5952))
																										{	/* SawJvm/code.scm 348 */
																											BGl_codez12z12zzsaw_jvm_outz00
																												(((BgL_jvmz00_bglt)
																													BgL_mez00_5639),
																												CNST_TABLE_REF(111));
																											BGl_codez12z12zzsaw_jvm_outz00
																												(((BgL_jvmz00_bglt)
																													BgL_mez00_5639),
																												CNST_TABLE_REF(64));
																											{	/* SawJvm/code.scm 352 */
																												int BgL_arg2139z00_5956;

																												{	/* SawJvm/code.scm 352 */
																													ucs2_t
																														BgL_ucs2z00_5957;
																													BgL_ucs2z00_5957 =
																														CUCS2
																														(BgL_valuez00_5952);
																													{	/* SawJvm/code.scm 352 */
																														obj_t
																															BgL_tmpz00_8276;
																														BgL_tmpz00_8276 =
																															BUCS2
																															(BgL_ucs2z00_5957);
																														BgL_arg2139z00_5956
																															=
																															CUCS2
																															(BgL_tmpz00_8276);
																												}}
																												BGl_pushzd2intzd2zzsaw_jvm_outz00
																													(((BgL_jvmz00_bglt)
																														BgL_mez00_5639),
																													BINT
																													(BgL_arg2139z00_5956));
																											}
																											return
																												BGl_codez12z12zzsaw_jvm_outz00
																												(((BgL_jvmz00_bglt)
																													BgL_mez00_5639),
																												CNST_TABLE_REF(112));
																										}
																									else
																										{	/* SawJvm/code.scm 348 */
																											return
																												BGl_errorz00zz__errorz00
																												(BGl_string2542z00zzsaw_jvm_codez00,
																												BGl_string2543z00zzsaw_jvm_codez00,
																												BgL_valuez00_5952);
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
			}
		}

	}



/* &gen-fun-rtl_mov1705 */
	obj_t BGl_z62genzd2funzd2rtl_mov1705z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5640, obj_t BgL_funz00_5641, obj_t BgL_mez00_5642)
	{
		{	/* SawJvm/code.scm 316 */
			return CNST_TABLE_REF(55);
		}

	}



/* &gen-fun-rtl_nop1703 */
	obj_t BGl_z62genzd2funzd2rtl_nop1703z62zzsaw_jvm_codez00(obj_t
		BgL_envz00_5643, obj_t BgL_funz00_5644, obj_t BgL_mez00_5645)
	{
		{	/* SawJvm/code.scm 313 */
			return CNST_TABLE_REF(13);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_codez00(void)
	{
		{	/* SawJvm/code.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzbackend_bvmz00(336068337L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(0L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzbackend_libz00(321177283L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_namesz00(179871433L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_typez00(437600037L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_outz00(441641707L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_funcallz00(282450876L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_jvm_inlinez00(252555369L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
			return
				BGl_modulezd2initializa7ationz75zzjas_asz00(476839586L,
				BSTRING_TO_STRING(BGl_string2544z00zzsaw_jvm_codez00));
		}

	}

#ifdef __cplusplus
}
#endif
