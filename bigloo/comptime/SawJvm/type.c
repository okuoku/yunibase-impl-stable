/*===========================================================================*/
/*   (SawJvm/type.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawJvm/type.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_JVM_TYPE_TYPE_DEFINITIONS
#define BGL_SAW_JVM_TYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_loadgz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                   *BgL_rtl_loadgz00_bglt;

	typedef struct BgL_rtl_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_getfieldz00_bglt;

	typedef struct BgL_rtl_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vrefz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;


#endif													// BGL_SAW_JVM_TYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_ne1523z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_rtl_funcallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_funz00zzsaw_defsz00;
	extern obj_t BGl_rtl_newz00zzsaw_defsz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_jvm_typez00 = BUNSPEC;
	static int BGl_siza7ezd2destzd2funza7zzsaw_jvm_typez00(BgL_rtl_funz00_bglt,
		BgL_rtl_insz00_bglt);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_mo1513z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_vrefz00zzsaw_defsz00;
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_la1505z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_jvm_typez00(void);
	static obj_t BGl_z62typezd2destzd2fun1532z62zzsaw_jvm_typez00(obj_t, obj_t);
	extern obj_t BGl_rtl_purez00zzsaw_defsz00;
	static obj_t BGl_genericzd2initzd2zzsaw_jvm_typez00(void);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_li1527z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_jvm_typez00(void);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_no1507z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_rtl_lightfuncallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_pu1511z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_jvm_typez00(void);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_ap1525z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62typeSiza7ezc5zzsaw_jvm_typez00(obj_t, obj_t);
	static obj_t BGl_za2za2longzd2typesza2za2zd2zzsaw_jvm_typez00 = BUNSPEC;
	static obj_t BGl_z62siza7ezd2destzd2fun1502zc5zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62typezd2destzd2funz62zzsaw_jvm_typez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_callz00zzsaw_defsz00;
	extern obj_t BGl_rtl_lastz00zzsaw_defsz00;
	extern obj_t BGl_rtl_applyz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_jvm_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_namesz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_bvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	extern obj_t BGl_rtl_loadgz00zzsaw_defsz00;
	extern obj_t BGl_rtl_loadiz00zzsaw_defsz00;
	extern obj_t BGl_rtl_notseqz00zzsaw_defsz00;
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_ef1509z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_jvm_typez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_typez00(void);
	BGL_EXPORTED_DECL int BGl_typeSiza7eza7zzsaw_jvm_typez00(BgL_typez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_typez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_typez00(void);
	static obj_t BGl_z62siza7ezd2destzd2funzc5zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_rtl_castz00zzsaw_defsz00;
	static obj_t BGl_z62siza7ezd2destz17zzsaw_jvm_typez00(obj_t, obj_t);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_fu1529z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	extern obj_t BGl_rtl_getfieldz00zzsaw_defsz00;
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_ca1531z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt
		BGl_typezd2destzd2funz00zzsaw_jvm_typez00(BgL_rtl_funz00_bglt);
	BGL_EXPORTED_DECL int BGl_siza7ezd2destz75zzsaw_jvm_typez00(obj_t);
	extern obj_t BGl_rtl_effectz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_ge1519z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_vr1521z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_lo1515z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62siza7ezd2destzd2funzd2rtl_lo1517z17zzsaw_jvm_typez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2funzd2rtl_ca1535zb0zzsaw_jvm_typez00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typeSiza7ezd2envz75zzsaw_jvm_typez00,
		BgL_bgl_za762typesiza7a7eza7c51897za7,
		BGl_z62typeSiza7ezc5zzsaw_jvm_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_typezd2destzd2funzd2envzd2zzsaw_jvm_typez00,
		BgL_bgl_za762typeza7d2destza7d1898za7,
		BGl_z62typezd2destzd2funz62zzsaw_jvm_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1872z00zzsaw_jvm_typez00,
		BgL_bgl_string1872za700za7za7s1899za7, "size-dest-fun1502", 17);
	      DEFINE_STRING(BGl_string1874z00zzsaw_jvm_typez00,
		BgL_bgl_string1874za700za7za7s1900za7, "type-dest-fun1532", 17);
	      DEFINE_STRING(BGl_string1875z00zzsaw_jvm_typez00,
		BgL_bgl_string1875za700za7za7s1901za7, "no method for", 13);
	      DEFINE_STRING(BGl_string1877z00zzsaw_jvm_typez00,
		BgL_bgl_string1877za700za7za7s1902za7, "size-dest-fun::int", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1903za7,
		BGl_z62siza7ezd2destzd2fun1502zc5zzsaw_jvm_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzsaw_jvm_typez00,
		BgL_bgl_za762typeza7d2destza7d1904za7,
		BGl_z62typezd2destzd2fun1532z62zzsaw_jvm_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1905za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_la1505z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1906za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_no1507z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1907za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_ef1509z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1887z00zzsaw_jvm_typez00,
		BgL_bgl_string1887za700za7za7s1908za7, "size-dest-fun::type", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1909za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_pu1511z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1910za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_mo1513z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1911za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_lo1515z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1912za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_lo1517z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1884z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1913za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_ge1519z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1885z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1914za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_vr1521z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1915za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_ne1523z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1893z00zzsaw_jvm_typez00,
		BgL_bgl_string1893za700za7za7s1916za7, "type-dest-fun::type", 19);
	      DEFINE_STRING(BGl_string1894z00zzsaw_jvm_typez00,
		BgL_bgl_string1894za700za7za7s1917za7, "saw_jvm_type", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1918za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_ap1525z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1895z00zzsaw_jvm_typez00,
		BgL_bgl_string1895za700za7za7s1919za7,
		"size-dest-fun void (elong llong uelong ullong double int64 uint64) ", 67);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1889z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1920za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_li1527z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1890z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1921za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_fu1529z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1922za7,
		BGl_z62siza7ezd2destzd2funzd2rtl_ca1531z17zzsaw_jvm_typez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1892z00zzsaw_jvm_typez00,
		BgL_bgl_za762typeza7d2destza7d1923za7,
		BGl_z62typezd2destzd2funzd2rtl_ca1535zb0zzsaw_jvm_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1924za7,
		BGl_z62siza7ezd2destzd2funzc5zzsaw_jvm_typez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_siza7ezd2destzd2envza7zzsaw_jvm_typez00,
		BgL_bgl_za762siza7a7eza7d2dest1925za7,
		BGl_z62siza7ezd2destz17zzsaw_jvm_typez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_jvm_typez00));
		     ADD_ROOT((void *) (&BGl_za2za2longzd2typesza2za2zd2zzsaw_jvm_typez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_jvm_typez00(long
		BgL_checksumz00_3054, char *BgL_fromz00_3055)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_jvm_typez00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_jvm_typez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_jvm_typez00();
					BGl_libraryzd2moduleszd2initz00zzsaw_jvm_typez00();
					BGl_cnstzd2initzd2zzsaw_jvm_typez00();
					BGl_importedzd2moduleszd2initz00zzsaw_jvm_typez00();
					BGl_genericzd2initzd2zzsaw_jvm_typez00();
					BGl_methodzd2initzd2zzsaw_jvm_typez00();
					return BGl_toplevelzd2initzd2zzsaw_jvm_typez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_jvm_typez00(void)
	{
		{	/* SawJvm/type.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_jvm_type");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_jvm_type");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_jvm_typez00(void)
	{
		{	/* SawJvm/type.scm 1 */
			{	/* SawJvm/type.scm 1 */
				obj_t BgL_cportz00_3014;

				{	/* SawJvm/type.scm 1 */
					obj_t BgL_stringz00_3021;

					BgL_stringz00_3021 = BGl_string1895z00zzsaw_jvm_typez00;
					{	/* SawJvm/type.scm 1 */
						obj_t BgL_startz00_3022;

						BgL_startz00_3022 = BINT(0L);
						{	/* SawJvm/type.scm 1 */
							obj_t BgL_endz00_3023;

							BgL_endz00_3023 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3021)));
							{	/* SawJvm/type.scm 1 */

								BgL_cportz00_3014 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3021, BgL_startz00_3022, BgL_endz00_3023);
				}}}}
				{
					long BgL_iz00_3015;

					BgL_iz00_3015 = 2L;
				BgL_loopz00_3016:
					if ((BgL_iz00_3015 == -1L))
						{	/* SawJvm/type.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawJvm/type.scm 1 */
							{	/* SawJvm/type.scm 1 */
								obj_t BgL_arg1896z00_3017;

								{	/* SawJvm/type.scm 1 */

									{	/* SawJvm/type.scm 1 */
										obj_t BgL_locationz00_3019;

										BgL_locationz00_3019 = BBOOL(((bool_t) 0));
										{	/* SawJvm/type.scm 1 */

											BgL_arg1896z00_3017 =
												BGl_readz00zz__readerz00(BgL_cportz00_3014,
												BgL_locationz00_3019);
										}
									}
								}
								{	/* SawJvm/type.scm 1 */
									int BgL_tmpz00_3086;

									BgL_tmpz00_3086 = (int) (BgL_iz00_3015);
									CNST_TABLE_SET(BgL_tmpz00_3086, BgL_arg1896z00_3017);
							}}
							{	/* SawJvm/type.scm 1 */
								int BgL_auxz00_3020;

								BgL_auxz00_3020 = (int) ((BgL_iz00_3015 - 1L));
								{
									long BgL_iz00_3091;

									BgL_iz00_3091 = (long) (BgL_auxz00_3020);
									BgL_iz00_3015 = BgL_iz00_3091;
									goto BgL_loopz00_3016;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_jvm_typez00(void)
	{
		{	/* SawJvm/type.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_jvm_typez00(void)
	{
		{	/* SawJvm/type.scm 1 */
			BGl_za2za2longzd2typesza2za2zd2zzsaw_jvm_typez00 = CNST_TABLE_REF(0);
			return BUNSPEC;
		}

	}



/* typeSize */
	BGL_EXPORTED_DEF int BGl_typeSiza7eza7zzsaw_jvm_typez00(BgL_typez00_bglt
		BgL_typez00_3)
	{
		{	/* SawJvm/type.scm 18 */
			{	/* SawJvm/type.scm 19 */
				obj_t BgL_idz00_2227;

				BgL_idz00_2227 =
					(((BgL_typez00_bglt) COBJECT(BgL_typez00_3))->BgL_idz00);
				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_2227,
							CNST_TABLE_REF(0))))
					{	/* SawJvm/type.scm 20 */
						return (int) (2L);
					}
				else
					{	/* SawJvm/type.scm 20 */
						if ((BgL_idz00_2227 == CNST_TABLE_REF(1)))
							{	/* SawJvm/type.scm 21 */
								return (int) (0L);
							}
						else
							{	/* SawJvm/type.scm 21 */
								return (int) (1L);
		}}}}

	}



/* &typeSize */
	obj_t BGl_z62typeSiza7ezc5zzsaw_jvm_typez00(obj_t BgL_envz00_2939,
		obj_t BgL_typez00_2940)
	{
		{	/* SawJvm/type.scm 18 */
			return
				BINT(BGl_typeSiza7eza7zzsaw_jvm_typez00(
					((BgL_typez00_bglt) BgL_typez00_2940)));
		}

	}



/* size-dest */
	BGL_EXPORTED_DEF int BGl_siza7ezd2destz75zzsaw_jvm_typez00(obj_t BgL_insz00_4)
	{
		{	/* SawJvm/type.scm 24 */
			{	/* SawJvm/type.scm 25 */
				bool_t BgL_test1930z00_3109;

				{	/* SawJvm/type.scm 25 */
					obj_t BgL_classz00_2779;

					BgL_classz00_2779 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_insz00_4))
						{	/* SawJvm/type.scm 25 */
							BgL_objectz00_bglt BgL_arg1807z00_2781;

							BgL_arg1807z00_2781 = (BgL_objectz00_bglt) (BgL_insz00_4);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawJvm/type.scm 25 */
									long BgL_idxz00_2787;

									BgL_idxz00_2787 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2781);
									BgL_test1930z00_3109 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2787 + 1L)) == BgL_classz00_2779);
								}
							else
								{	/* SawJvm/type.scm 25 */
									bool_t BgL_res1859z00_2812;

									{	/* SawJvm/type.scm 25 */
										obj_t BgL_oclassz00_2795;

										{	/* SawJvm/type.scm 25 */
											obj_t BgL_arg1815z00_2803;
											long BgL_arg1816z00_2804;

											BgL_arg1815z00_2803 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawJvm/type.scm 25 */
												long BgL_arg1817z00_2805;

												BgL_arg1817z00_2805 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2781);
												BgL_arg1816z00_2804 =
													(BgL_arg1817z00_2805 - OBJECT_TYPE);
											}
											BgL_oclassz00_2795 =
												VECTOR_REF(BgL_arg1815z00_2803, BgL_arg1816z00_2804);
										}
										{	/* SawJvm/type.scm 25 */
											bool_t BgL__ortest_1115z00_2796;

											BgL__ortest_1115z00_2796 =
												(BgL_classz00_2779 == BgL_oclassz00_2795);
											if (BgL__ortest_1115z00_2796)
												{	/* SawJvm/type.scm 25 */
													BgL_res1859z00_2812 = BgL__ortest_1115z00_2796;
												}
											else
												{	/* SawJvm/type.scm 25 */
													long BgL_odepthz00_2797;

													{	/* SawJvm/type.scm 25 */
														obj_t BgL_arg1804z00_2798;

														BgL_arg1804z00_2798 = (BgL_oclassz00_2795);
														BgL_odepthz00_2797 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2798);
													}
													if ((1L < BgL_odepthz00_2797))
														{	/* SawJvm/type.scm 25 */
															obj_t BgL_arg1802z00_2800;

															{	/* SawJvm/type.scm 25 */
																obj_t BgL_arg1803z00_2801;

																BgL_arg1803z00_2801 = (BgL_oclassz00_2795);
																BgL_arg1802z00_2800 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2801,
																	1L);
															}
															BgL_res1859z00_2812 =
																(BgL_arg1802z00_2800 == BgL_classz00_2779);
														}
													else
														{	/* SawJvm/type.scm 25 */
															BgL_res1859z00_2812 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1930z00_3109 = BgL_res1859z00_2812;
								}
						}
					else
						{	/* SawJvm/type.scm 25 */
							BgL_test1930z00_3109 = ((bool_t) 0);
						}
				}
				if (BgL_test1930z00_3109)
					{	/* SawJvm/type.scm 26 */
						BgL_typez00_bglt BgL_arg1552z00_2230;

						BgL_arg1552z00_2230 =
							(((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt) BgL_insz00_4)))->BgL_typez00);
						return BGl_typeSiza7eza7zzsaw_jvm_typez00(BgL_arg1552z00_2230);
					}
				else
					{	/* SawJvm/type.scm 25 */
						if (CBOOL(
								(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_4)))->BgL_destz00)))
							{	/* SawJvm/type.scm 29 */
								BgL_typez00_bglt BgL_arg1559z00_2233;

								BgL_arg1559z00_2233 =
									(((BgL_rtl_regz00_bglt) COBJECT(
											((BgL_rtl_regz00_bglt)
												(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_insz00_4)))->
													BgL_destz00))))->BgL_typez00);
								return BGl_typeSiza7eza7zzsaw_jvm_typez00(BgL_arg1559z00_2233);
							}
						else
							{	/* SawJvm/type.scm 30 */
								BgL_rtl_funz00_bglt BgL_arg1564z00_2235;

								BgL_arg1564z00_2235 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_4)))->BgL_funz00);
								return
									BGl_siza7ezd2destzd2funza7zzsaw_jvm_typez00
									(BgL_arg1564z00_2235, ((BgL_rtl_insz00_bglt) BgL_insz00_4));
							}
					}
			}
		}

	}



/* &size-dest */
	obj_t BGl_z62siza7ezd2destz17zzsaw_jvm_typez00(obj_t BgL_envz00_2941,
		obj_t BgL_insz00_2942)
	{
		{	/* SawJvm/type.scm 24 */
			return BINT(BGl_siza7ezd2destz75zzsaw_jvm_typez00(BgL_insz00_2942));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_jvm_typez00(void)
	{
		{	/* SawJvm/type.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_jvm_typez00(void)
	{
		{	/* SawJvm/type.scm 1 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_proc1871z00zzsaw_jvm_typez00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_string1872z00zzsaw_jvm_typez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2destzd2funzd2envzd2zzsaw_jvm_typez00,
				BGl_proc1873z00zzsaw_jvm_typez00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_string1874z00zzsaw_jvm_typez00);
		}

	}



/* &type-dest-fun1532 */
	obj_t BGl_z62typezd2destzd2fun1532z62zzsaw_jvm_typez00(obj_t BgL_envz00_2945,
		obj_t BgL_funz00_2946)
	{
		{	/* SawJvm/type.scm 74 */
			{	/* SawJvm/type.scm 76 */
				obj_t BgL_arg1575z00_3026;

				{	/* SawJvm/type.scm 76 */
					obj_t BgL_arg1576z00_3027;

					{	/* SawJvm/type.scm 76 */
						obj_t BgL_arg1815z00_3028;
						long BgL_arg1816z00_3029;

						BgL_arg1815z00_3028 = (BGl_za2classesza2z00zz__objectz00);
						{	/* SawJvm/type.scm 76 */
							long BgL_arg1817z00_3030;

							BgL_arg1817z00_3030 =
								BGL_OBJECT_CLASS_NUM(
								((BgL_objectz00_bglt) ((BgL_rtl_funz00_bglt) BgL_funz00_2946)));
							BgL_arg1816z00_3029 = (BgL_arg1817z00_3030 - OBJECT_TYPE);
						}
						BgL_arg1576z00_3027 =
							VECTOR_REF(BgL_arg1815z00_3028, BgL_arg1816z00_3029);
					}
					BgL_arg1575z00_3026 =
						BGl_classzd2namezd2zz__objectz00(BgL_arg1576z00_3027);
				}
				return
					BGl_errorz00zz__errorz00(CNST_TABLE_REF(2),
					BGl_string1875z00zzsaw_jvm_typez00, BgL_arg1575z00_3026);
			}
		}

	}



/* &size-dest-fun1502 */
	obj_t BGl_z62siza7ezd2destzd2fun1502zc5zzsaw_jvm_typez00(obj_t
		BgL_envz00_2947, obj_t BgL_funz00_2948, obj_t BgL_insz00_2949)
	{
		{	/* SawJvm/type.scm 35 */
			return
				BINT(BGl_typeSiza7eza7zzsaw_jvm_typez00
				(BGl_typezd2destzd2funz00zzsaw_jvm_typez00(((BgL_rtl_funz00_bglt)
							BgL_funz00_2948))));
		}

	}



/* size-dest-fun */
	int BGl_siza7ezd2destzd2funza7zzsaw_jvm_typez00(BgL_rtl_funz00_bglt
		BgL_funz00_5, BgL_rtl_insz00_bglt BgL_insz00_6)
	{
		{	/* SawJvm/type.scm 35 */
			{	/* SawJvm/type.scm 35 */
				obj_t BgL_method1503z00_2248;

				{	/* SawJvm/type.scm 35 */
					obj_t BgL_res1864z00_2855;

					{	/* SawJvm/type.scm 35 */
						long BgL_objzd2classzd2numz00_2826;

						BgL_objzd2classzd2numz00_2826 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_5));
						{	/* SawJvm/type.scm 35 */
							obj_t BgL_arg1811z00_2827;

							BgL_arg1811z00_2827 =
								PROCEDURE_REF(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
								(int) (1L));
							{	/* SawJvm/type.scm 35 */
								int BgL_offsetz00_2830;

								BgL_offsetz00_2830 = (int) (BgL_objzd2classzd2numz00_2826);
								{	/* SawJvm/type.scm 35 */
									long BgL_offsetz00_2831;

									BgL_offsetz00_2831 =
										((long) (BgL_offsetz00_2830) - OBJECT_TYPE);
									{	/* SawJvm/type.scm 35 */
										long BgL_modz00_2832;

										BgL_modz00_2832 =
											(BgL_offsetz00_2831 >> (int) ((long) ((int) (4L))));
										{	/* SawJvm/type.scm 35 */
											long BgL_restz00_2834;

											BgL_restz00_2834 =
												(BgL_offsetz00_2831 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawJvm/type.scm 35 */

												{	/* SawJvm/type.scm 35 */
													obj_t BgL_bucketz00_2836;

													BgL_bucketz00_2836 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2827), BgL_modz00_2832);
													BgL_res1864z00_2855 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2836), BgL_restz00_2834);
					}}}}}}}}
					BgL_method1503z00_2248 = BgL_res1864z00_2855;
				}
				return
					CINT(BGL_PROCEDURE_CALL2(BgL_method1503z00_2248,
						((obj_t) BgL_funz00_5), ((obj_t) BgL_insz00_6)));
			}
		}

	}



/* &size-dest-fun */
	obj_t BGl_z62siza7ezd2destzd2funzc5zzsaw_jvm_typez00(obj_t BgL_envz00_2950,
		obj_t BgL_funz00_2951, obj_t BgL_insz00_2952)
	{
		{	/* SawJvm/type.scm 35 */
			return
				BINT(BGl_siza7ezd2destzd2funza7zzsaw_jvm_typez00(
					((BgL_rtl_funz00_bglt) BgL_funz00_2951),
					((BgL_rtl_insz00_bglt) BgL_insz00_2952)));
		}

	}



/* type-dest-fun */
	BgL_typez00_bglt BGl_typezd2destzd2funz00zzsaw_jvm_typez00(BgL_rtl_funz00_bglt
		BgL_funz00_35)
	{
		{	/* SawJvm/type.scm 74 */
			{	/* SawJvm/type.scm 74 */
				obj_t BgL_method1533z00_2249;

				{	/* SawJvm/type.scm 74 */
					obj_t BgL_res1869z00_2886;

					{	/* SawJvm/type.scm 74 */
						long BgL_objzd2classzd2numz00_2857;

						BgL_objzd2classzd2numz00_2857 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_35));
						{	/* SawJvm/type.scm 74 */
							obj_t BgL_arg1811z00_2858;

							BgL_arg1811z00_2858 =
								PROCEDURE_REF(BGl_typezd2destzd2funzd2envzd2zzsaw_jvm_typez00,
								(int) (1L));
							{	/* SawJvm/type.scm 74 */
								int BgL_offsetz00_2861;

								BgL_offsetz00_2861 = (int) (BgL_objzd2classzd2numz00_2857);
								{	/* SawJvm/type.scm 74 */
									long BgL_offsetz00_2862;

									BgL_offsetz00_2862 =
										((long) (BgL_offsetz00_2861) - OBJECT_TYPE);
									{	/* SawJvm/type.scm 74 */
										long BgL_modz00_2863;

										BgL_modz00_2863 =
											(BgL_offsetz00_2862 >> (int) ((long) ((int) (4L))));
										{	/* SawJvm/type.scm 74 */
											long BgL_restz00_2865;

											BgL_restz00_2865 =
												(BgL_offsetz00_2862 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawJvm/type.scm 74 */

												{	/* SawJvm/type.scm 74 */
													obj_t BgL_bucketz00_2867;

													BgL_bucketz00_2867 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2858), BgL_modz00_2863);
													BgL_res1869z00_2886 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2867), BgL_restz00_2865);
					}}}}}}}}
					BgL_method1533z00_2249 = BgL_res1869z00_2886;
				}
				return
					((BgL_typez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1533z00_2249, ((obj_t) BgL_funz00_35)));
			}
		}

	}



/* &type-dest-fun */
	BgL_typez00_bglt BGl_z62typezd2destzd2funz62zzsaw_jvm_typez00(obj_t
		BgL_envz00_2953, obj_t BgL_funz00_2954)
	{
		{	/* SawJvm/type.scm 74 */
			return
				BGl_typezd2destzd2funz00zzsaw_jvm_typez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_2954));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_jvm_typez00(void)
	{
		{	/* SawJvm/type.scm 1 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_lastz00zzsaw_defsz00, BGl_proc1876z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_notseqz00zzsaw_defsz00, BGl_proc1878z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_effectz00zzsaw_defsz00, BGl_proc1879z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_purez00zzsaw_defsz00, BGl_proc1880z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_movz00zzsaw_defsz00, BGl_proc1881z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_loadiz00zzsaw_defsz00, BGl_proc1882z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_loadgz00zzsaw_defsz00, BGl_proc1883z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_getfieldz00zzsaw_defsz00, BGl_proc1884z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_vrefz00zzsaw_defsz00, BGl_proc1885z00zzsaw_jvm_typez00,
				BGl_string1877z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_newz00zzsaw_defsz00, BGl_proc1886z00zzsaw_jvm_typez00,
				BGl_string1887z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_applyz00zzsaw_defsz00, BGl_proc1888z00zzsaw_jvm_typez00,
				BGl_string1887z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_lightfuncallz00zzsaw_defsz00, BGl_proc1889z00zzsaw_jvm_typez00,
				BGl_string1887z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_funcallz00zzsaw_defsz00, BGl_proc1890z00zzsaw_jvm_typez00,
				BGl_string1887z00zzsaw_jvm_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_siza7ezd2destzd2funzd2envz75zzsaw_jvm_typez00,
				BGl_rtl_castz00zzsaw_defsz00, BGl_proc1891z00zzsaw_jvm_typez00,
				BGl_string1887z00zzsaw_jvm_typez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2funzd2envzd2zzsaw_jvm_typez00,
				BGl_rtl_callz00zzsaw_defsz00, BGl_proc1892z00zzsaw_jvm_typez00,
				BGl_string1893z00zzsaw_jvm_typez00);
		}

	}



/* &type-dest-fun-rtl_ca1535 */
	BgL_typez00_bglt
		BGl_z62typezd2destzd2funzd2rtl_ca1535zb0zzsaw_jvm_typez00(obj_t
		BgL_envz00_2970, obj_t BgL_funz00_2971)
	{
		{	/* SawJvm/type.scm 78 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt)
							(((BgL_rtl_callz00_bglt) COBJECT(
										((BgL_rtl_callz00_bglt) BgL_funz00_2971)))->BgL_varz00))))->
				BgL_typez00);
		}

	}



/* &size-dest-fun-rtl_ca1531 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_ca1531z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2972, obj_t BgL_funz00_2973, obj_t BgL_insz00_2974)
	{
		{	/* SawJvm/type.scm 70 */
			return BINT(1L);
		}

	}



/* &size-dest-fun-rtl_fu1529 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_fu1529z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2975, obj_t BgL_funz00_2976, obj_t BgL_insz00_2977)
	{
		{	/* SawJvm/type.scm 69 */
			return BINT(1L);
		}

	}



/* &size-dest-fun-rtl_li1527 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_li1527z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2978, obj_t BgL_funz00_2979, obj_t BgL_insz00_2980)
	{
		{	/* SawJvm/type.scm 68 */
			return BINT(1L);
		}

	}



/* &size-dest-fun-rtl_ap1525 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_ap1525z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2981, obj_t BgL_funz00_2982, obj_t BgL_insz00_2983)
	{
		{	/* SawJvm/type.scm 67 */
			return BINT(1L);
		}

	}



/* &size-dest-fun-rtl_ne1523 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_ne1523z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2984, obj_t BgL_funz00_2985, obj_t BgL_insz00_2986)
	{
		{	/* SawJvm/type.scm 66 */
			return BINT(1L);
		}

	}



/* &size-dest-fun-rtl_vr1521 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_vr1521z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2987, obj_t BgL_funz00_2988, obj_t BgL_insz00_2989)
	{
		{	/* SawJvm/type.scm 62 */
			return
				BINT(BGl_typeSiza7eza7zzsaw_jvm_typez00(
					(((BgL_rtl_vrefz00_bglt) COBJECT(
								((BgL_rtl_vrefz00_bglt) BgL_funz00_2988)))->BgL_typez00)));
		}

	}



/* &size-dest-fun-rtl_ge1519 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_ge1519z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2990, obj_t BgL_funz00_2991, obj_t BgL_insz00_2992)
	{
		{	/* SawJvm/type.scm 59 */
			return
				BINT(BGl_typeSiza7eza7zzsaw_jvm_typez00(
					(((BgL_rtl_getfieldz00_bglt) COBJECT(
								((BgL_rtl_getfieldz00_bglt) BgL_funz00_2991)))->BgL_typez00)));
		}

	}



/* &size-dest-fun-rtl_lo1517 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_lo1517z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2993, obj_t BgL_funz00_2994, obj_t BgL_insz00_2995)
	{
		{	/* SawJvm/type.scm 56 */
			{	/* SawJvm/type.scm 57 */
				int BgL_tmpz00_3267;

				{	/* SawJvm/type.scm 57 */
					BgL_typez00_bglt BgL_arg1591z00_3042;

					BgL_arg1591z00_3042 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									(((BgL_rtl_loadgz00_bglt) COBJECT(
												((BgL_rtl_loadgz00_bglt) BgL_funz00_2994)))->
										BgL_varz00))))->BgL_typez00);
					BgL_tmpz00_3267 =
						BGl_typeSiza7eza7zzsaw_jvm_typez00(BgL_arg1591z00_3042);
				}
				return BINT(BgL_tmpz00_3267);
			}
		}

	}



/* &size-dest-fun-rtl_lo1515 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_lo1515z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2996, obj_t BgL_funz00_2997, obj_t BgL_insz00_2998)
	{
		{	/* SawJvm/type.scm 49 */
			{	/* SawJvm/type.scm 50 */
				int BgL_tmpz00_3274;

				{	/* SawJvm/type.scm 50 */
					BgL_atomz00_bglt BgL_constantz00_3044;

					BgL_constantz00_3044 =
						(((BgL_rtl_loadiz00_bglt) COBJECT(
								((BgL_rtl_loadiz00_bglt) BgL_funz00_2997)))->BgL_constantz00);
					{	/* SawJvm/type.scm 51 */
						obj_t BgL_valuez00_3045;

						BgL_valuez00_3045 =
							(((BgL_atomz00_bglt) COBJECT(BgL_constantz00_3044))->
							BgL_valuez00);
						if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_valuez00_3045))
							{	/* SawJvm/type.scm 53 */
								BgL_typez00_bglt BgL_arg1589z00_3046;

								BgL_arg1589z00_3046 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_constantz00_3044)))->BgL_typez00);
								BgL_tmpz00_3274 =
									BGl_typeSiza7eza7zzsaw_jvm_typez00(BgL_arg1589z00_3046);
							}
						else
							{	/* SawJvm/type.scm 52 */
								BgL_tmpz00_3274 = (int) (1L);
				}}}
				return BINT(BgL_tmpz00_3274);
			}
		}

	}



/* &size-dest-fun-rtl_mo1513 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_mo1513z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_2999, obj_t BgL_funz00_3000, obj_t BgL_insz00_3001)
	{
		{	/* SawJvm/type.scm 46 */
			{	/* SawJvm/type.scm 47 */
				int BgL_tmpz00_3285;

				{	/* SawJvm/type.scm 47 */
					obj_t BgL_arg1584z00_3048;

					{	/* SawJvm/type.scm 47 */
						obj_t BgL_pairz00_3049;

						BgL_pairz00_3049 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_3001)))->BgL_argsz00);
						BgL_arg1584z00_3048 = CAR(BgL_pairz00_3049);
					}
					BgL_tmpz00_3285 =
						BGl_siza7ezd2destz75zzsaw_jvm_typez00(BgL_arg1584z00_3048);
				}
				return BINT(BgL_tmpz00_3285);
			}
		}

	}



/* &size-dest-fun-rtl_pu1511 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_pu1511z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_3002, obj_t BgL_funz00_3003, obj_t BgL_insz00_3004)
	{
		{	/* SawJvm/type.scm 44 */
			return BINT(1L);
		}

	}



/* &size-dest-fun-rtl_ef1509 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_ef1509z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_3005, obj_t BgL_funz00_3006, obj_t BgL_insz00_3007)
	{
		{	/* SawJvm/type.scm 41 */
			return BINT(0L);
		}

	}



/* &size-dest-fun-rtl_no1507 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_no1507z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_3008, obj_t BgL_funz00_3009, obj_t BgL_insz00_3010)
	{
		{	/* SawJvm/type.scm 40 */
			return BINT(0L);
		}

	}



/* &size-dest-fun-rtl_la1505 */
	obj_t BGl_z62siza7ezd2destzd2funzd2rtl_la1505z17zzsaw_jvm_typez00(obj_t
		BgL_envz00_3011, obj_t BgL_funz00_3012, obj_t BgL_insz00_3013)
	{
		{	/* SawJvm/type.scm 39 */
			return BINT(0L);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_jvm_typez00(void)
	{
		{	/* SawJvm/type.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_jvm_typez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_jvm_typez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_jvm_typez00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_jvm_typez00));
			BGl_modulezd2initializa7ationz75zzbackend_bvmz00(336068337L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_jvm_typez00));
			BGl_modulezd2initializa7ationz75zzbackend_jvm_classz00(0L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_jvm_typez00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_jvm_typez00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_jvm_namesz00(179871433L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_jvm_typez00));
		}

	}

#ifdef __cplusplus
}
#endif
