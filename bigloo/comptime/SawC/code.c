/*===========================================================================*/
/*   (SawC/code.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawC/code.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_C_CODE_TYPE_DEFINITIONS
#define BGL_SAW_C_CODE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_cvmz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}             *BgL_cvmz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_selectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
	}                    *BgL_rtl_selectz00_bglt;

	typedef struct BgL_rtl_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
		obj_t BgL_labelsz00;
	}                    *BgL_rtl_switchz00_bglt;

	typedef struct BgL_rtl_ifeqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifeqz00_bglt;

	typedef struct BgL_rtl_ifnez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifnez00_bglt;

	typedef struct BgL_rtl_goz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                *BgL_rtl_goz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_loadgz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                   *BgL_rtl_loadgz00_bglt;

	typedef struct BgL_rtl_loadfunz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                     *BgL_rtl_loadfunz00_bglt;

	typedef struct BgL_rtl_globalrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                       *BgL_rtl_globalrefz00_bglt;

	typedef struct BgL_rtl_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_getfieldz00_bglt;

	typedef struct BgL_rtl_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                    *BgL_rtl_vallocz00_bglt;

	typedef struct BgL_rtl_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vrefz00_bglt;

	typedef struct BgL_rtl_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                     *BgL_rtl_vlengthz00_bglt;

	typedef struct BgL_rtl_storegz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                    *BgL_rtl_storegz00_bglt;

	typedef struct BgL_rtl_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_setfieldz00_bglt;

	typedef struct BgL_rtl_vsetz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vsetz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_lightfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_funsz00;
		obj_t BgL_rettypez00;
	}                          *BgL_rtl_lightfuncallz00_bglt;

	typedef struct BgL_rtl_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                    *BgL_rtl_pragmaz00_bglt;

	typedef struct BgL_rtl_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_totypez00;
		struct BgL_typez00_bgl *BgL_fromtypez00;
	}                  *BgL_rtl_castz00_bglt;

	typedef struct BgL_rtl_cast_nullz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                       *BgL_rtl_cast_nullz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_sawciregz00_bgl
	{
		obj_t BgL_indexz00;
	}                  *BgL_sawciregz00_bglt;


#endif													// BGL_SAW_C_CODE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_genzd2typezd2regsz00zzsaw_c_codez00(obj_t);
	static obj_t BGl_z62acceptzd2foldingzf3zd2cvm1735z91zzsaw_c_codez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_emitzd2pragmazd2zzsaw_c_codez00(obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2namezd2rtl_loa1698zb0zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31846ze3ze5zzsaw_c_codez00(obj_t);
	extern obj_t BGl_rtl_funcallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_funz00zzsaw_defsz00;
	static bool_t BGl_za2inlinezd2simplezd2macrosza2z00zzsaw_c_codez00;
	static obj_t BGl_z62genzd2funzd2namezd2rtl_vse1692zb0zzsaw_c_codez00(obj_t,
		obj_t);
	extern obj_t
		BGl_makezd2typedzd2declarationz00zztype_toolsz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_genzd2exprzd2zzsaw_c_codez00(BgL_rtl_funz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	extern obj_t BGl_rtl_pragmaz00zzsaw_defsz00;
	static obj_t BGl_z62sawzd2cheaderzb0zzsaw_c_codez00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_c_codez00 = BUNSPEC;
	static obj_t BGl_z62genzd2funzd2namezd2rtl_val1688zb0zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_z62genzd2exprzd2rtl_funcall1676z62zzsaw_c_codez00(obj_t,
		obj_t, obj_t);
	static bool_t BGl_za2commentza2z00zzsaw_c_codez00;
	static obj_t BGl_genzd2inszd2zzsaw_c_codez00(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t BGl_z62genzd2exprzd2rtl_call1684z62zzsaw_c_codez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2czd2portza2zd2zzbackend_c_emitz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_genzd2typezd2regz00zzsaw_c_codez00(obj_t);
	extern obj_t BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31776ze3ze5zzsaw_c_codez00(obj_t);
	BGL_IMPORT obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	extern obj_t BGl_rtl_vrefz00zzsaw_defsz00;
	static obj_t BGl_toplevelzd2initzd2zzsaw_c_codez00(void);
	extern obj_t BGl_rtl_protectz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t BGl_sawzd2cepiloguezd2zzsaw_c_codez00(void);
	extern obj_t BGl_rtl_ifeqz00zzsaw_defsz00;
	static bool_t BGl_genbodyz00zzsaw_c_codez00(obj_t, obj_t);
	static obj_t BGl_headerz00zzsaw_c_codez00(void);
	extern obj_t BGl_rtl_vlengthz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	static bool_t BGl_za2haspushexitza2z00zzsaw_c_codez00;
	static obj_t BGl_z62genzd2prefixzd2rtl_cast_1732z62zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_c_codez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62genzd2prefix1703zb0zzsaw_c_codez00(obj_t, obj_t);
	static obj_t BGl_z62genzd2exprzd2rtl_setfiel1674z62zzsaw_c_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62genzd2prefixzd2rtl_globa1714z62zzsaw_c_codez00(obj_t,
		obj_t);
	static bool_t BGl_printzd2locationzf3z21zzsaw_c_codez00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_vsetz00zzsaw_defsz00;
	static obj_t BGl_objectzd2initzd2zzsaw_c_codez00(void);
	extern obj_t BGl_bbvz00zzsaw_bbvz00(BgL_backendz00_bglt, BgL_globalz00_bglt,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_buildzd2treezd2zzsaw_exprz00(BgL_backendz00_bglt, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static bool_t BGl_za2hasprotectza2z00zzsaw_c_codez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_za2counterza2z00zzsaw_c_codez00 = BUNSPEC;
	static obj_t BGl_walkze70ze7zzsaw_c_codez00(obj_t);
	static obj_t BGl_genzd2funzd2namez00zzsaw_c_codez00(BgL_rtl_funz00_bglt);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62genzd2prefixzd2rtl_vset1726z62zzsaw_c_codez00(obj_t,
		obj_t);
	extern obj_t BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00;
	extern obj_t BGl_rtl_lightfuncallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	extern obj_t BGl_rtl_vallocz00zzsaw_defsz00;
	extern obj_t BGl_rtl_cast_nullz00zzsaw_defsz00;
	extern obj_t BGl_rtl_loadfunz00zzsaw_defsz00;
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	static obj_t BGl_getzd2localszd2zzsaw_c_codez00(obj_t, obj_t);
	static bool_t BGl_declarezd2regszd2zzsaw_c_codez00(obj_t);
	extern BgL_rtl_regz00_bglt
		BGl_localzd2ze3regz31zzsaw_node2rtlz00(BgL_localz00_bglt);
	static obj_t BGl_z62genzd2prefixzd2rtl_vref1724z62zzsaw_c_codez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_typenamez00zzsaw_c_codez00(obj_t);
	static obj_t BGl_z62genzd2exprzd2rtl_switch1682z62zzsaw_c_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_inlzd2opzd2zzsaw_c_codez00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_c_codez00(void);
	static obj_t BGl_z62genzd2prefixzd2rtl_vallo1722z62zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_genzd2regzd2zzsaw_c_codez00(obj_t);
	static obj_t BGl_genzd2prefixzd2zzsaw_c_codez00(BgL_rtl_funz00_bglt);
	extern obj_t BGl_globalzd2ze3blocksz31zzsaw_woodcutterz00(BgL_backendz00_bglt,
		BgL_globalz00_bglt);
	static obj_t BGl_z62sawzd2cgenzb0zzsaw_c_codez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_switchz00zzsaw_defsz00;
	BGL_IMPORT obj_t make_string(long, unsigned char);
	extern obj_t BGl_rtl_goz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2prefixzd2rtl_loadf1710z62zzsaw_c_codez00(obj_t,
		obj_t);
	static bool_t BGl_multiplezd2evaluationzd2zzsaw_c_codez00(obj_t, obj_t);
	extern obj_t BGl_rtl_ifnez00zzsaw_defsz00;
	extern obj_t
		BGl_registerzd2allocationzd2zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt, BgL_globalz00_bglt, obj_t, obj_t);
	static obj_t BGl_z62genzd2exprzd2rtl_getfiel1672z62zzsaw_c_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62genzd2prefixzd2rtl_go1716z62zzsaw_c_codez00(obj_t, obj_t);
	static obj_t BGl_exprzd2ze3iregze70zd6zzsaw_c_codez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_rtl_setfieldz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	BGL_IMPORT obj_t rgc_buffer_substring(obj_t, long, long);
	extern bool_t BGl_globalzd2argszd2safezf3zf3zzast_varz00(BgL_globalz00_bglt);
	static obj_t BGl_z62genzd2prefixzb0zzsaw_c_codez00(obj_t, obj_t);
	static obj_t BGl_z62genzd2prefixzd2rtl_ifne1720z62zzsaw_c_codez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_callz00zzsaw_defsz00;
	static bool_t BGl_deepzd2movzf3z21zzsaw_c_codez00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62genzd2prefixzd2rtl_loadg1708z62zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_genzd2regzf2destz20zzsaw_c_codez00(obj_t);
	static bool_t BGl_outzd2labelzd2zzsaw_c_codez00(int, obj_t);
	extern obj_t BGl_rtl_storegz00zzsaw_defsz00;
	static obj_t BGl_za2badzd2macrosza2zd2zzsaw_c_codez00 = BUNSPEC;
	static obj_t BGl_z62genzd2prefixzd2rtl_ifeq1718z62zzsaw_c_codez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static bool_t BGl_za2haspushbeforeza2z00zzsaw_c_codez00;
	static obj_t BGl_genzd2Xfuncallzd2zzsaw_c_codez00(obj_t, obj_t, obj_t,
		bool_t);
	extern obj_t BGl_rtl_returnz00zzsaw_defsz00;
	static obj_t BGl_check_funze70ze7zzsaw_c_codez00(BgL_rtl_funz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_c_codez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzsaw_registerzd2allocationzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_exprz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_woodcutterz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_findzd2locationzd2zzsaw_c_codez00(BgL_rtl_insz00_bglt);
	extern obj_t BGl_cfunz00zzast_varz00;
	static bool_t BGl_za2traceza2z00zzsaw_c_codez00;
	static long BGl_za2pushtraceemmitedza2z00zzsaw_c_codez00 = 0L;
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_z62zc3z04anonymousza32264ze3ze5zzsaw_c_codez00(obj_t, obj_t);
	extern obj_t BGl_rtl_loadgz00zzsaw_defsz00;
	extern obj_t BGl_rtl_loadiz00zzsaw_defsz00;
	static bool_t BGl_za2countza2z00zzsaw_c_codez00;
	static obj_t BGl_z62genzd2exprzd2rtl_lightfu1670z62zzsaw_c_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_SawCIregz00zzsaw_c_codez00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzsaw_c_codez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_c_codez00(void);
	static obj_t BGl_z62genzd2exprzd2rtl_protect1678z62zzsaw_c_codez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2namez62zzsaw_c_codez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_c_codez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_c_codez00(void);
	static obj_t BGl_z62genzd2exprzd2rtl_pragma1680z62zzsaw_c_codez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sawzd2cgenzd2zzsaw_c_codez00(BgL_cvmz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_rtl_castz00zzsaw_defsz00;
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	static obj_t BGl_z62genzd2expr1667zb0zzsaw_c_codez00(obj_t, obj_t, obj_t);
	static obj_t BGl_genzd2upcasezd2zzsaw_c_codez00(BgL_rtl_funz00_bglt);
	static obj_t BGl_z62genzd2exprzb0zzsaw_c_codez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2namezd2rtl_ret1702zb0zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_z62genzd2funzd2name1685z62zzsaw_c_codez00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda2259z62zzsaw_c_codez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_globalrefz00zzsaw_defsz00;
	static obj_t BGl_z62genzd2prefixzd2rtl_vleng1728z62zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_z62genzd2prefixzd2rtl_loadi1706z62zzsaw_c_codez00(obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda2262z62zzsaw_c_codez00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda2265z62zzsaw_c_codez00(obj_t, obj_t);
	static obj_t BGl_z62sawzd2cepiloguezb0zzsaw_c_codez00(obj_t);
	BGL_IMPORT obj_t BGl_displayza2za2zz__r4_output_6_10_3z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sawzd2cheaderzd2zzsaw_c_codez00(void);
	BGL_IMPORT obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	static obj_t BGl_z62genzd2funzd2namezd2rtl_loa1700zb0zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2272z62zzsaw_c_codez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2273z62zzsaw_c_codez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_getfieldz00zzsaw_defsz00;
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	static obj_t BGl_cepiloguez00zzsaw_c_codez00(void);
	extern obj_t BGl_za2stdcza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62genzd2funzd2namezd2rtl_mov1696zb0zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_thezd2stringze70z35zzsaw_c_codez00(obj_t);
	static obj_t BGl_z62genzd2funzd2namezd2rtl_vre1690zb0zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_genfunz00zzsaw_c_codez00(BgL_cvmz00_bglt, BgL_globalz00_bglt,
		obj_t);
	static obj_t BGl_vfunzd2namezd2zzsaw_c_codez00(obj_t, BgL_typez00_bglt);
	extern obj_t
		BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00;
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_IMPORT bool_t rgc_fill_buffer(obj_t);
	extern obj_t BGl_cvmz00zzbackend_cvmz00;
	static obj_t BGl_z62genzd2prefixzd2rtl_cast1730z62zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzsaw_c_codez00(obj_t);
	static obj_t BGl_z62genzd2funzd2namezd2rtl_vle1694zb0zzsaw_c_codez00(obj_t,
		obj_t);
	static obj_t BGl_reg_namez00zzsaw_c_codez00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_vextraz00zzsaw_c_codez00(BgL_typez00_bglt);
	static obj_t BGl_z62genzd2prefixzd2rtl_store1712z62zzsaw_c_codez00(obj_t,
		obj_t);
	static bool_t BGl_genzd2argszd2zzsaw_c_codez00(obj_t);
	static obj_t __cnst[12];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawzd2cheaderzd2envz00zzsaw_c_codez00,
		BgL_bgl_za762sawza7d2cheader2847z00, BGl_z62sawzd2cheaderzb0zzsaw_c_codez00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawzd2cgenzd2envz00zzsaw_c_codez00,
		BgL_bgl_za762sawza7d2cgenza7b02848za7, BGl_z62sawzd2cgenzb0zzsaw_c_codez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2700z00zzsaw_c_codez00,
		BgL_bgl_string2700za700za7za7s2849za7, " \"", 2);
	      DEFINE_STRING(BGl_string2701z00zzsaw_c_codez00,
		BgL_bgl_string2701za700za7za7s2850za7, "\"", 1);
	      DEFINE_STRING(BGl_string2702z00zzsaw_c_codez00,
		BgL_bgl_string2702za700za7za7s2851za7, "\t", 1);
	      DEFINE_STRING(BGl_string2703z00zzsaw_c_codez00,
		BgL_bgl_string2703za700za7za7s2852za7, " = ", 3);
	      DEFINE_STRING(BGl_string2704z00zzsaw_c_codez00,
		BgL_bgl_string2704za700za7za7s2853za7, "obj_t", 5);
	      DEFINE_STRING(BGl_string2705z00zzsaw_c_codez00,
		BgL_bgl_string2705za700za7za7s2854za7, "((~a (*)(obj_t, ...))", 21);
	      DEFINE_STRING(BGl_string2706z00zzsaw_c_codez00,
		BgL_bgl_string2706za700za7za7s2855za7, "((~a (*)(~(, )))", 16);
	      DEFINE_STRING(BGl_string2707z00zzsaw_c_codez00,
		BgL_bgl_string2707za700za7za7s2856za7, " (*)())", 7);
	      DEFINE_STRING(BGl_string2708z00zzsaw_c_codez00,
		BgL_bgl_string2708za700za7za7s2857za7, "((", 2);
	      DEFINE_STRING(BGl_string2709z00zzsaw_c_codez00,
		BgL_bgl_string2709za700za7za7s2858za7, "ENTRY(", 6);
	      DEFINE_STRING(BGl_string2710z00zzsaw_c_codez00,
		BgL_bgl_string2710za700za7za7s2859za7, "PROCEDURE_", 10);
	      DEFINE_STRING(BGl_string2711z00zzsaw_c_codez00,
		BgL_bgl_string2711za700za7za7s2860za7, "))(", 3);
	      DEFINE_STRING(BGl_string2712z00zzsaw_c_codez00,
		BgL_bgl_string2712za700za7za7s2861za7, ", BEOA", 6);
	      DEFINE_STRING(BGl_string2713z00zzsaw_c_codez00,
		BgL_bgl_string2713za700za7za7s2862za7, "BGL_", 4);
	      DEFINE_STRING(BGl_string2714z00zzsaw_c_codez00,
		BgL_bgl_string2714za700za7za7s2863za7, "T", 1);
	      DEFINE_STRING(BGl_string2715z00zzsaw_c_codez00,
		BgL_bgl_string2715za700za7za7s2864za7, "BGL_RTL_", 8);
	      DEFINE_STRING(BGl_string2716z00zzsaw_c_codez00,
		BgL_bgl_string2716za700za7za7s2865za7, "==", 2);
	      DEFINE_STRING(BGl_string2717z00zzsaw_c_codez00,
		BgL_bgl_string2717za700za7za7s2866za7, "BGL_RTL_EQ", 10);
	      DEFINE_STRING(BGl_string2718z00zzsaw_c_codez00,
		BgL_bgl_string2718za700za7za7s2867za7, ">=", 2);
	      DEFINE_STRING(BGl_string2719z00zzsaw_c_codez00,
		BgL_bgl_string2719za700za7za7s2868za7, "BGL_RTL_GE", 10);
	      DEFINE_STRING(BGl_string2720z00zzsaw_c_codez00,
		BgL_bgl_string2720za700za7za7s2869za7, "<=", 2);
	      DEFINE_STRING(BGl_string2721z00zzsaw_c_codez00,
		BgL_bgl_string2721za700za7za7s2870za7, "BGL_RTL_LE", 10);
	      DEFINE_STRING(BGl_string2722z00zzsaw_c_codez00,
		BgL_bgl_string2722za700za7za7s2871za7, ">", 1);
	      DEFINE_STRING(BGl_string2723z00zzsaw_c_codez00,
		BgL_bgl_string2723za700za7za7s2872za7, "BGL_RTL_GT", 10);
	      DEFINE_STRING(BGl_string2724z00zzsaw_c_codez00,
		BgL_bgl_string2724za700za7za7s2873za7, "<", 1);
	      DEFINE_STRING(BGl_string2725z00zzsaw_c_codez00,
		BgL_bgl_string2725za700za7za7s2874za7, "BGL_RTL_LT", 10);
	      DEFINE_STRING(BGl_string2726z00zzsaw_c_codez00,
		BgL_bgl_string2726za700za7za7s2875za7, "|", 1);
	      DEFINE_STRING(BGl_string2727z00zzsaw_c_codez00,
		BgL_bgl_string2727za700za7za7s2876za7, "BGL_RTL_OR", 10);
	      DEFINE_STRING(BGl_string2809z00zzsaw_c_codez00,
		BgL_bgl_string2809za700za7za7s2877za7, "accept-folding?", 15);
	      DEFINE_STRING(BGl_string2728z00zzsaw_c_codez00,
		BgL_bgl_string2728za700za7za7s2878za7, "&", 1);
	      DEFINE_STRING(BGl_string2729z00zzsaw_c_codez00,
		BgL_bgl_string2729za700za7za7s2879za7, "BGL_RTL_AND", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2800z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza72880za7,
		BGl_z62genzd2prefixzd2rtl_ifeq1718z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2801z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza72881za7,
		BGl_z62genzd2prefixzd2rtl_ifne1720z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2802z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza72882za7,
		BGl_z62genzd2prefixzd2rtl_vallo1722z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2803z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza72883za7,
		BGl_z62genzd2prefixzd2rtl_vref1724z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2810z00zzsaw_c_codez00,
		BgL_bgl_string2810za700za7za7s2884za7, "(obj_t) ", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2804z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza72885za7,
		BGl_z62genzd2prefixzd2rtl_vset1726z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2811z00zzsaw_c_codez00,
		BgL_bgl_string2811za700za7za7s2886za7, "return", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2805z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza72887za7,
		BGl_z62genzd2prefixzd2rtl_vleng1728z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2730z00zzsaw_c_codez00,
		BgL_bgl_string2730za700za7za7s2888za7, "+", 1);
	      DEFINE_STRING(BGl_string2812z00zzsaw_c_codez00,
		BgL_bgl_string2812za700za7za7s2889za7, "VLENGTH", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2806z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza72890za7,
		BGl_z62genzd2prefixzd2rtl_cast1730z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2731z00zzsaw_c_codez00,
		BgL_bgl_string2731za700za7za7s2891za7, "BGL_RTL_ADD", 11);
	      DEFINE_STRING(BGl_string2813z00zzsaw_c_codez00,
		BgL_bgl_string2813za700za7za7s2892za7, "VSET", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2807z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza72893za7,
		BGl_z62genzd2prefixzd2rtl_cast_1732z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2732z00zzsaw_c_codez00,
		BgL_bgl_string2732za700za7za7s2894za7, "-", 1);
	      DEFINE_STRING(BGl_string2814z00zzsaw_c_codez00,
		BgL_bgl_string2814za700za7za7s2895za7, "VREF", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2808z00zzsaw_c_codez00,
		BgL_bgl_za762acceptza7d2fold2896z00,
		BGl_z62acceptzd2foldingzf3zd2cvm1735z91zzsaw_c_codez00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2733z00zzsaw_c_codez00,
		BgL_bgl_string2733za700za7za7s2897za7, "BGL_RTL_SUB", 11);
	      DEFINE_STRING(BGl_string2652z00zzsaw_c_codez00,
		BgL_bgl_string2652za700za7za7s2898za7, "BIGLOOSAWPROFILE", 16);
	      DEFINE_STRING(BGl_string2815z00zzsaw_c_codez00,
		BgL_bgl_string2815za700za7za7s2899za7, "VALLOC", 6);
	      DEFINE_STRING(BGl_string2734z00zzsaw_c_codez00,
		BgL_bgl_string2734za700za7za7s2900za7, "*", 1);
	      DEFINE_STRING(BGl_string2653z00zzsaw_c_codez00,
		BgL_bgl_string2653za700za7za7s2901za7, "true", 4);
	      DEFINE_STRING(BGl_string2816z00zzsaw_c_codez00,
		BgL_bgl_string2816za700za7za7s2902za7, "make_fx_procedure", 17);
	      DEFINE_STRING(BGl_string2735z00zzsaw_c_codez00,
		BgL_bgl_string2735za700za7za7s2903za7, "BGL_RTL_MUL", 11);
	      DEFINE_STRING(BGl_string2817z00zzsaw_c_codez00,
		BgL_bgl_string2817za700za7za7s2904za7, "MAKE_FX_PROCEDURE", 17);
	      DEFINE_STRING(BGl_string2736z00zzsaw_c_codez00,
		BgL_bgl_string2736za700za7za7s2905za7, "/", 1);
	      DEFINE_STRING(BGl_string2655z00zzsaw_c_codez00,
		BgL_bgl_string2655za700za7za7s2906za7, "extern int bbcount[];\n", 22);
	      DEFINE_STRING(BGl_string2818z00zzsaw_c_codez00,
		BgL_bgl_string2818za700za7za7s2907za7, "make_va_procedure", 17);
	      DEFINE_STRING(BGl_string2737z00zzsaw_c_codez00,
		BgL_bgl_string2737za700za7za7s2908za7, "BGL_RTL_DIV", 11);
	      DEFINE_STRING(BGl_string2656z00zzsaw_c_codez00,
		BgL_bgl_string2656za700za7za7s2909za7, "extern char *bbname[];\n", 23);
	      DEFINE_STRING(BGl_string2819z00zzsaw_c_codez00,
		BgL_bgl_string2819za700za7za7s2910za7, "MAKE_VA_PROCEDURE", 17);
	      DEFINE_STRING(BGl_string2738z00zzsaw_c_codez00,
		BgL_bgl_string2738za700za7za7s2911za7, "%", 1);
	      DEFINE_STRING(BGl_string2657z00zzsaw_c_codez00,
		BgL_bgl_string2657za700za7za7s2912za7, "extern char *bbfun[];\n", 22);
	      DEFINE_STRING(BGl_string2739z00zzsaw_c_codez00,
		BgL_bgl_string2739za700za7za7s2913za7, "BGL_RTL_REM", 11);
	      DEFINE_STRING(BGl_string2658z00zzsaw_c_codez00,
		BgL_bgl_string2658za700za7za7s2914za7,
		"extern obj_t PRINT_TRACE(obj_t);\n\n", 34);
	      DEFINE_STRING(BGl_string2659z00zzsaw_c_codez00,
		BgL_bgl_string2659za700za7za7s2915za7, "\n", 1);
	      DEFINE_STRING(BGl_string2820z00zzsaw_c_codez00,
		BgL_bgl_string2820za700za7za7s2916za7, "MAKE_L_PROCEDURE", 16);
	      DEFINE_STRING(BGl_string2821z00zzsaw_c_codez00,
		BgL_bgl_string2821za700za7za7s2917za7, "(function_t) ", 13);
	      DEFINE_STRING(BGl_string2740z00zzsaw_c_codez00,
		BgL_bgl_string2740za700za7za7s2918za7, " | ", 3);
	      DEFINE_STRING(BGl_string2822z00zzsaw_c_codez00,
		BgL_bgl_string2822za700za7za7s2919za7, "switch(", 7);
	      DEFINE_STRING(BGl_string2741z00zzsaw_c_codez00,
		BgL_bgl_string2741za700za7za7s2920za7, " & ", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2654z00zzsaw_c_codez00,
		BgL_bgl_za762za7c3za704anonymo2921za7,
		BGl_z62zc3z04anonymousza31762ze3ze5zzsaw_c_codez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2823z00zzsaw_c_codez00,
		BgL_bgl_string2823za700za7za7s2922za7, ") {", 3);
	      DEFINE_STRING(BGl_string2742z00zzsaw_c_codez00,
		BgL_bgl_string2742za700za7za7s2923za7, " ^ ", 3);
	      DEFINE_STRING(BGl_string2661z00zzsaw_c_codez00,
		BgL_bgl_string2661za700za7za7s2924za7, "];\n", 3);
	      DEFINE_STRING(BGl_string2824z00zzsaw_c_codez00,
		BgL_bgl_string2824za700za7za7s2925za7, "\n\t default: ", 12);
	      DEFINE_STRING(BGl_string2743z00zzsaw_c_codez00,
		BgL_bgl_string2743za700za7za7s2926za7, "BGL_RTL_XOR", 11);
	      DEFINE_STRING(BGl_string2662z00zzsaw_c_codez00,
		BgL_bgl_string2662za700za7za7s2927za7, "int bbcount[", 12);
	      DEFINE_STRING(BGl_string2825z00zzsaw_c_codez00,
		BgL_bgl_string2825za700za7za7s2928za7, "\n\t case ", 8);
	      DEFINE_STRING(BGl_string2744z00zzsaw_c_codez00,
		BgL_bgl_string2744za700za7za7s2929za7, " >> ", 4);
	      DEFINE_STRING(BGl_string2663z00zzsaw_c_codez00,
		BgL_bgl_string2663za700za7za7s2930za7, "char *bbname[", 13);
	      DEFINE_STRING(BGl_string2826z00zzsaw_c_codez00,
		BgL_bgl_string2826za700za7za7s2931za7, ";", 1);
	      DEFINE_STRING(BGl_string2745z00zzsaw_c_codez00,
		BgL_bgl_string2745za700za7za7s2932za7, "BGL_RTL_RSH", 11);
	      DEFINE_STRING(BGl_string2664z00zzsaw_c_codez00,
		BgL_bgl_string2664za700za7za7s2933za7, "char *bbfun[", 12);
	      DEFINE_STRING(BGl_string2827z00zzsaw_c_codez00,
		BgL_bgl_string2827za700za7za7s2934za7, " goto L", 7);
	      DEFINE_STRING(BGl_string2746z00zzsaw_c_codez00,
		BgL_bgl_string2746za700za7za7s2935za7, " << ", 4);
	      DEFINE_STRING(BGl_string2665z00zzsaw_c_codez00,
		BgL_bgl_string2665za700za7za7s2936za7, "obj_t PRINT_TRACE(obj_t r) {\n",
		29);
	      DEFINE_STRING(BGl_string2828z00zzsaw_c_codez00,
		BgL_bgl_string2828za700za7za7s2937za7, "\n\t}", 3);
	      DEFINE_STRING(BGl_string2747z00zzsaw_c_codez00,
		BgL_bgl_string2747za700za7za7s2938za7, "BGL_RTL_LSH", 11);
	      DEFINE_STRING(BGl_string2666z00zzsaw_c_codez00,
		BgL_bgl_string2666za700za7za7s2939za7, "\tint i;\n", 8);
	      DEFINE_STRING(BGl_string2829z00zzsaw_c_codez00,
		BgL_bgl_string2829za700za7za7s2940za7, "BUNSPEC", 7);
	      DEFINE_STRING(BGl_string2748z00zzsaw_c_codez00,
		BgL_bgl_string2748za700za7za7s2941za7, "PUSH_EXIT", 9);
	      DEFINE_STRING(BGl_string2667z00zzsaw_c_codez00,
		BgL_bgl_string2667za700za7za7s2942za7, "\tputs(\"[\");\n", 12);
	      DEFINE_STRING(BGl_string2749z00zzsaw_c_codez00,
		BgL_bgl_string2749za700za7za7s2943za7, "BGL_RTL_PUSH_EXIT", 17);
	      DEFINE_STRING(BGl_string2668z00zzsaw_c_codez00,
		BgL_bgl_string2668za700za7za7s2944za7, ";i++) {\n", 8);
	      DEFINE_STRING(BGl_string2669z00zzsaw_c_codez00,
		BgL_bgl_string2669za700za7za7s2945za7, "\tfor(i=0;i<", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2660z00zzsaw_c_codez00,
		BgL_bgl_za762za7c3za704anonymo2946za7,
		BGl_z62zc3z04anonymousza31776ze3ze5zzsaw_c_codez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2830z00zzsaw_c_codez00,
		BgL_bgl_string2830za700za7za7s2947za7, "(obj_t) jmpbuf;\n\t", 17);
	      DEFINE_STRING(BGl_string2831z00zzsaw_c_codez00,
		BgL_bgl_string2831za700za7za7s2948za7, "{BGL_STORE_TRACE();", 19);
	      DEFINE_STRING(BGl_string2750z00zzsaw_c_codez00,
		BgL_bgl_string2750za700za7za7s2949za7, "PUSH_BEFORE", 11);
	      DEFINE_STRING(BGl_string2832z00zzsaw_c_codez00,
		BgL_bgl_string2832za700za7za7s2950za7,
		"if(SETJMP(jmpbuf)) {BGL_RESTORE_TRACE(); return(BGL_EXIT_VALUE());}}\n",
		69);
	      DEFINE_STRING(BGl_string2751z00zzsaw_c_codez00,
		BgL_bgl_string2751za700za7za7s2951za7, "BGL_RTL_PUSH_BEFORE", 19);
	      DEFINE_STRING(BGl_string2670z00zzsaw_c_codez00,
		BgL_bgl_string2670za700za7za7s2952za7, "\t\tif (bbname[i]) {\n", 19);
	      DEFINE_STRING(BGl_string2833z00zzsaw_c_codez00,
		BgL_bgl_string2833za700za7za7s2953za7, "#if( SIGSETJMP_SAVESIGS == 0 )\n",
		31);
	      DEFINE_STRING(BGl_string2752z00zzsaw_c_codez00,
		BgL_bgl_string2752za700za7za7s2954za7, "PUSH_TRACE", 10);
	      DEFINE_STRING(BGl_string2671z00zzsaw_c_codez00,
		BgL_bgl_string2671za700za7za7s2955za7,
		"\t\t\tprintf(\"\t{\\\"fun\\\": \\\"%s\\\", \\\"id\\\": %s, \\\"count\\\": %d},\\n\", bbfun[i], bbname[i],bbcount[i]);\n",
		95);
	      DEFINE_STRING(BGl_string2834z00zzsaw_c_codez00,
		BgL_bgl_string2834za700za7za7s2956za7,
		"  // bgl_restore_signal_handlers();\n", 36);
	      DEFINE_STRING(BGl_string2753z00zzsaw_c_codez00,
		BgL_bgl_string2753za700za7za7s2957za7, "{PUSH_TRACE", 11);
	      DEFINE_STRING(BGl_string2672z00zzsaw_c_codez00,
		BgL_bgl_string2672za700za7za7s2958za7, "\t\t}\n", 4);
	      DEFINE_STRING(BGl_string2835z00zzsaw_c_codez00,
		BgL_bgl_string2835za700za7za7s2959za7, "#endif\n", 7);
	      DEFINE_STRING(BGl_string2754z00zzsaw_c_codez00,
		BgL_bgl_string2754za700za7za7s2960za7, "BGL_ENV_PUSH_TRACE", 18);
	      DEFINE_STRING(BGl_string2673z00zzsaw_c_codez00,
		BgL_bgl_string2673za700za7za7s2961za7, "\t}\n", 3);
	      DEFINE_STRING(BGl_string2836z00zzsaw_c_codez00,
		BgL_bgl_string2836za700za7za7s2962za7, "(VA_PROCEDUREP(", 15);
	      DEFINE_STRING(BGl_string2755z00zzsaw_c_codez00,
		BgL_bgl_string2755za700za7za7s2963za7, "{BGL_ENV_PUSH_TRACE", 19);
	      DEFINE_STRING(BGl_string2674z00zzsaw_c_codez00,
		BgL_bgl_string2674za700za7za7s2964za7, "\tprintf(\"\tnull\\n\");\n", 20);
	      DEFINE_STRING(BGl_string2837z00zzsaw_c_codez00,
		BgL_bgl_string2837za700za7za7s2965za7, ") ? ", 4);
	      DEFINE_STRING(BGl_string2756z00zzsaw_c_codez00,
		BgL_bgl_string2756za700za7za7s2966za7, "BIGLOO_EXIT", 11);
	      DEFINE_STRING(BGl_string2675z00zzsaw_c_codez00,
		BgL_bgl_string2675za700za7za7s2967za7, "\tputs(\"]\");\n", 12);
	      DEFINE_STRING(BGl_string2838z00zzsaw_c_codez00,
		BgL_bgl_string2838za700za7za7s2968za7, " : ", 3);
	      DEFINE_STRING(BGl_string2757z00zzsaw_c_codez00,
		BgL_bgl_string2757za700za7za7s2969za7, "PRINT_TRACE", 11);
	      DEFINE_STRING(BGl_string2676z00zzsaw_c_codez00,
		BgL_bgl_string2676za700za7za7s2970za7, "\treturn(BIGLOO_EXIT(r));\n", 25);
	      DEFINE_STRING(BGl_string2839z00zzsaw_c_codez00,
		BgL_bgl_string2839za700za7za7s2971za7, "BGL_SAW_SETFIELD(", 17);
	      DEFINE_STRING(BGl_string2758z00zzsaw_c_codez00,
		BgL_bgl_string2758za700za7za7s2972za7, "STRING_REF", 10);
	      DEFINE_STRING(BGl_string2677z00zzsaw_c_codez00,
		BgL_bgl_string2677za700za7za7s2973za7, "}\n", 2);
	      DEFINE_STRING(BGl_string2759z00zzsaw_c_codez00,
		BgL_bgl_string2759za700za7za7s2974za7, "BGL_RTL_STRING_REF", 18);
	      DEFINE_STRING(BGl_string2678z00zzsaw_c_codez00,
		BgL_bgl_string2678za700za7za7s2975za7, " ", 1);
	      DEFINE_STRING(BGl_string2679z00zzsaw_c_codez00,
		BgL_bgl_string2679za700za7za7s2976za7, " {\n", 3);
	      DEFINE_STRING(BGl_string2840z00zzsaw_c_codez00,
		BgL_bgl_string2840za700za7za7s2977za7, ",", 1);
	      DEFINE_STRING(BGl_string2841z00zzsaw_c_codez00,
		BgL_bgl_string2841za700za7za7s2978za7, "BGL_SAW_GETFIELD(", 17);
	      DEFINE_STRING(BGl_string2760z00zzsaw_c_codez00,
		BgL_bgl_string2760za700za7za7s2979za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string2842z00zzsaw_c_codez00,
		BgL_bgl_string2842za700za7za7s2980za7, "L_", 2);
	      DEFINE_STRING(BGl_string2761z00zzsaw_c_codez00,
		BgL_bgl_string2761za700za7za7s2981za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string2680z00zzsaw_c_codez00,
		BgL_bgl_string2680za700za7za7s2982za7, " jmp_buf_t jmpbuf;\n", 19);
	      DEFINE_STRING(BGl_string2843z00zzsaw_c_codez00,
		BgL_bgl_string2843za700za7za7s2983za7, "saw_c_code", 10);
	      DEFINE_STRING(BGl_string2762z00zzsaw_c_codez00,
		BgL_bgl_string2762za700za7za7s2984za7, "$", 1);
	      DEFINE_STRING(BGl_string2681z00zzsaw_c_codez00,
		BgL_bgl_string2681za700za7za7s2985za7, " struct exitd exitd;\n", 21);
	      DEFINE_STRING(BGl_string2844z00zzsaw_c_codez00,
		BgL_bgl_string2844za700za7za7s2986za7,
		"bigloo-c else saw_c_code SawCIreg obj index (double llong ullong elong uelong long ulong int uint char uchar byte ubyte bool string int8 uint8 int16 uint16 int32 uint32 int64 uint64) #z1 location push-before! push-exit! (\"WRITE_CHAR\" \"BOOLEANP\" \"INTEGERP\" \"NULLP\" \"PAIRP\" \"SYMBOLP\" \"VECTORP\") ",
		293);
	      DEFINE_STRING(BGl_string2682z00zzsaw_c_codez00,
		BgL_bgl_string2682za700za7za7s2987za7, " struct befored befored;\n", 25);
	      DEFINE_STRING(BGl_string2683z00zzsaw_c_codez00,
		BgL_bgl_string2683za700za7za7s2988za7, "\n}\n\n", 4);
	      DEFINE_STRING(BGl_string2684z00zzsaw_c_codez00,
		BgL_bgl_string2684za700za7za7s2989za7, "(", 1);
	      DEFINE_STRING(BGl_string2685z00zzsaw_c_codez00,
		BgL_bgl_string2685za700za7za7s2990za7, ", ", 2);
	      DEFINE_STRING(BGl_string2686z00zzsaw_c_codez00,
		BgL_bgl_string2686za700za7za7s2991za7, ")", 1);
	      DEFINE_STRING(BGl_string2687z00zzsaw_c_codez00,
		BgL_bgl_string2687za700za7za7s2992za7, "V", 1);
	      DEFINE_STRING(BGl_string2688z00zzsaw_c_codez00,
		BgL_bgl_string2688za700za7za7s2993za7, "R", 1);
	      DEFINE_STRING(BGl_string2689z00zzsaw_c_codez00,
		BgL_bgl_string2689za700za7za7s2994za7, ";\n", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2763z00zzsaw_c_codez00,
		BgL_bgl_za762lambda2273za7622995z00, BGl_z62lambda2273z62zzsaw_c_codez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2770z00zzsaw_c_codez00,
		BgL_bgl_string2770za700za7za7s2996za7, "gen-expr1667", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2764z00zzsaw_c_codez00,
		BgL_bgl_za762lambda2272za7622997z00, BGl_z62lambda2272z62zzsaw_c_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2765z00zzsaw_c_codez00,
		BgL_bgl_za762lambda2265za7622998z00, BGl_z62lambda2265z62zzsaw_c_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2690z00zzsaw_c_codez00,
		BgL_bgl_string2690za700za7za7s2999za7, "", 0);
	      DEFINE_STRING(BGl_string2772z00zzsaw_c_codez00,
		BgL_bgl_string2772za700za7za7s3000za7, "gen-fun-name1685", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2766z00zzsaw_c_codez00,
		BgL_bgl_za762za7c3za704anonymo3001za7,
		BGl_z62zc3z04anonymousza32264ze3ze5zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2691z00zzsaw_c_codez00,
		BgL_bgl_string2691za700za7za7s3002za7, ":", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2767z00zzsaw_c_codez00,
		BgL_bgl_za762lambda2262za7623003z00, BGl_z62lambda2262z62zzsaw_c_codez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2692z00zzsaw_c_codez00,
		BgL_bgl_string2692za700za7za7s3004za7, "L", 1);
	      DEFINE_STRING(BGl_string2774z00zzsaw_c_codez00,
		BgL_bgl_string2774za700za7za7s3005za7, "gen-prefix1703", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2768z00zzsaw_c_codez00,
		BgL_bgl_za762lambda2259za7623006z00, BGl_z62lambda2259z62zzsaw_c_codez00,
		0L, BUNSPEC, 8);
	      DEFINE_STRING(BGl_string2693z00zzsaw_c_codez00,
		BgL_bgl_string2693za700za7za7s3007za7, "]++;\n", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2769z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2expr1663008z00,
		BGl_z62genzd2expr1667zb0zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2694z00zzsaw_c_codez00,
		BgL_bgl_string2694za700za7za7s3009za7, "\tbbcount[", 9);
	      DEFINE_STRING(BGl_string2776z00zzsaw_c_codez00,
		BgL_bgl_string2776za700za7za7s3010za7, "gen-expr", 8);
	      DEFINE_STRING(BGl_string2695z00zzsaw_c_codez00,
		BgL_bgl_string2695za700za7za7s3011za7, "\";\n", 3);
	      DEFINE_STRING(BGl_string2696z00zzsaw_c_codez00,
		BgL_bgl_string2696za700za7za7s3012za7, "] = \"", 5);
	      DEFINE_STRING(BGl_string2697z00zzsaw_c_codez00,
		BgL_bgl_string2697za700za7za7s3013za7, "\tbbname[", 8);
	      DEFINE_STRING(BGl_string2698z00zzsaw_c_codez00,
		BgL_bgl_string2698za700za7za7s3014za7, "\tbbfun[", 7);
	      DEFINE_STRING(BGl_string2699z00zzsaw_c_codez00,
		BgL_bgl_string2699za700za7za7s3015za7, "#line ", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2771z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3016za7,
		BGl_z62genzd2funzd2name1685z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2773z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefix13017z00,
		BGl_z62genzd2prefix1703zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2775z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7d23018za7,
		BGl_z62genzd2exprzd2rtl_lightfu1670z62zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2777z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7d23019za7,
		BGl_z62genzd2exprzd2rtl_getfiel1672z62zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2778z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7d23020za7,
		BGl_z62genzd2exprzd2rtl_setfiel1674z62zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2785z00zzsaw_c_codez00,
		BgL_bgl_string2785za700za7za7s3021za7, "gen-fun-name", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2779z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7d23022za7,
		BGl_z62genzd2exprzd2rtl_funcall1676z62zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2780z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7d23023za7,
		BGl_z62genzd2exprzd2rtl_protect1678z62zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2781z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7d23024za7,
		BGl_z62genzd2exprzd2rtl_pragma1680z62zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2782z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7d23025za7,
		BGl_z62genzd2exprzd2rtl_switch1682z62zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2783z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7d23026za7,
		BGl_z62genzd2exprzd2rtl_call1684z62zzsaw_c_codez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2784z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3027za7,
		BGl_z62genzd2funzd2namezd2rtl_val1688zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2786z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3028za7,
		BGl_z62genzd2funzd2namezd2rtl_vre1690zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2787z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3029za7,
		BGl_z62genzd2funzd2namezd2rtl_vse1692zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2794z00zzsaw_c_codez00,
		BgL_bgl_string2794za700za7za7s3030za7, "gen-prefix", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2788z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3031za7,
		BGl_z62genzd2funzd2namezd2rtl_vle1694zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2789z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3032za7,
		BGl_z62genzd2funzd2namezd2rtl_mov1696zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2790z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3033za7,
		BGl_z62genzd2funzd2namezd2rtl_loa1698zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2791z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3034za7,
		BGl_z62genzd2funzd2namezd2rtl_loa1700zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2792z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3035za7,
		BGl_z62genzd2funzd2namezd2rtl_ret1702zb0zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2793z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza73036za7,
		BGl_z62genzd2prefixzd2rtl_loadi1706z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2795z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza73037za7,
		BGl_z62genzd2prefixzd2rtl_loadg1708z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2796z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza73038za7,
		BGl_z62genzd2prefixzd2rtl_loadf1710z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2797z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza73039za7,
		BGl_z62genzd2prefixzd2rtl_store1712z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2798z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza73040za7,
		BGl_z62genzd2prefixzd2rtl_globa1714z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2799z00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza73041za7,
		BGl_z62genzd2prefixzd2rtl_go1716z62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	extern obj_t BGl_acceptzd2foldingzf3zd2envzf3zzsaw_exprz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sawzd2cepiloguezd2envz00zzsaw_c_codez00,
		BgL_bgl_za762sawza7d2cepilog3042z00,
		BGl_z62sawzd2cepiloguezb0zzsaw_c_codez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_GENERIC(BGl_genzd2exprzd2envz00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2exprza7b03043za7, BGl_z62genzd2exprzb0zzsaw_c_codez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
		BgL_bgl_za762genza7d2funza7d2n3044za7,
		BGl_z62genzd2funzd2namez62zzsaw_c_codez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
		BgL_bgl_za762genza7d2prefixza73045za7,
		BGl_z62genzd2prefixzb0zzsaw_c_codez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_c_codez00));
		     ADD_ROOT((void *) (&BGl_za2counterza2z00zzsaw_c_codez00));
		     ADD_ROOT((void *) (&BGl_za2badzd2macrosza2zd2zzsaw_c_codez00));
		     ADD_ROOT((void *) (&BGl_SawCIregz00zzsaw_c_codez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_c_codez00(long
		BgL_checksumz00_6220, char *BgL_fromz00_6221)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_c_codez00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_c_codez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_c_codez00();
					BGl_libraryzd2moduleszd2initz00zzsaw_c_codez00();
					BGl_cnstzd2initzd2zzsaw_c_codez00();
					BGl_importedzd2moduleszd2initz00zzsaw_c_codez00();
					BGl_objectzd2initzd2zzsaw_c_codez00();
					BGl_genericzd2initzd2zzsaw_c_codez00();
					BGl_methodzd2initzd2zzsaw_c_codez00();
					return BGl_toplevelzd2initzd2zzsaw_c_codez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_c_code");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_c_code");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__rgcz00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "saw_c_code");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "saw_c_code");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 1 */
			{	/* SawC/code.scm 1 */
				obj_t BgL_cportz00_5992;

				{	/* SawC/code.scm 1 */
					obj_t BgL_stringz00_5999;

					BgL_stringz00_5999 = BGl_string2844z00zzsaw_c_codez00;
					{	/* SawC/code.scm 1 */
						obj_t BgL_startz00_6000;

						BgL_startz00_6000 = BINT(0L);
						{	/* SawC/code.scm 1 */
							obj_t BgL_endz00_6001;

							BgL_endz00_6001 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_5999)));
							{	/* SawC/code.scm 1 */

								BgL_cportz00_5992 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_5999, BgL_startz00_6000, BgL_endz00_6001);
				}}}}
				{
					long BgL_iz00_5993;

					BgL_iz00_5993 = 11L;
				BgL_loopz00_5994:
					if ((BgL_iz00_5993 == -1L))
						{	/* SawC/code.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawC/code.scm 1 */
							{	/* SawC/code.scm 1 */
								obj_t BgL_arg2846z00_5995;

								{	/* SawC/code.scm 1 */

									{	/* SawC/code.scm 1 */
										obj_t BgL_locationz00_5997;

										BgL_locationz00_5997 = BBOOL(((bool_t) 0));
										{	/* SawC/code.scm 1 */

											BgL_arg2846z00_5995 =
												BGl_readz00zz__readerz00(BgL_cportz00_5992,
												BgL_locationz00_5997);
										}
									}
								}
								{	/* SawC/code.scm 1 */
									int BgL_tmpz00_6260;

									BgL_tmpz00_6260 = (int) (BgL_iz00_5993);
									CNST_TABLE_SET(BgL_tmpz00_6260, BgL_arg2846z00_5995);
							}}
							{	/* SawC/code.scm 1 */
								int BgL_auxz00_5998;

								BgL_auxz00_5998 = (int) ((BgL_iz00_5993 - 1L));
								{
									long BgL_iz00_6265;

									BgL_iz00_6265 = (long) (BgL_auxz00_5998);
									BgL_iz00_5993 = BgL_iz00_6265;
									goto BgL_loopz00_5994;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 1 */
			BGl_za2commentza2z00zzsaw_c_codez00 = ((bool_t) 0);
			BGl_za2traceza2z00zzsaw_c_codez00 = ((bool_t) 0);
			BGl_za2countza2z00zzsaw_c_codez00 =
				BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BGl_getenvz00zz__osz00
				(BGl_string2652z00zzsaw_c_codez00), BGl_string2653z00zzsaw_c_codez00);
			BGl_za2inlinezd2simplezd2macrosza2z00zzsaw_c_codez00 = ((bool_t) 1);
			BGl_za2counterza2z00zzsaw_c_codez00 = BINT(0L);
			BGl_za2hasprotectza2z00zzsaw_c_codez00 = ((bool_t) 0);
			BGl_za2haspushexitza2z00zzsaw_c_codez00 = ((bool_t) 0);
			BGl_za2haspushbeforeza2z00zzsaw_c_codez00 = ((bool_t) 0);
			BGl_za2pushtraceemmitedza2z00zzsaw_c_codez00 = 0L;
			return (BGl_za2badzd2macrosza2zd2zzsaw_c_codez00 =
				CNST_TABLE_REF(0), BUNSPEC);
		}

	}



/* saw-cheader */
	BGL_EXPORTED_DEF obj_t BGl_sawzd2cheaderzd2zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 43 */
			return
				BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
				(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
				BGl_proc2654z00zzsaw_c_codez00);
		}

	}



/* &saw-cheader */
	obj_t BGl_z62sawzd2cheaderzb0zzsaw_c_codez00(obj_t BgL_envz00_5818)
	{
		{	/* SawC/code.scm 43 */
			return BGl_sawzd2cheaderzd2zzsaw_c_codez00();
		}

	}



/* &<@anonymous:1762> */
	obj_t BGl_z62zc3z04anonymousza31762ze3ze5zzsaw_c_codez00(obj_t
		BgL_envz00_5819)
	{
		{	/* SawC/code.scm 45 */
			return BGl_headerz00zzsaw_c_codez00();
		}

	}



/* header */
	obj_t BGl_headerz00zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 47 */
			if (BGl_za2countza2z00zzsaw_c_codez00)
				{	/* SawC/code.scm 48 */
					{	/* SawC/code.scm 49 */
						obj_t BgL_arg1765z00_2431;

						{	/* SawC/code.scm 49 */
							obj_t BgL_tmpz00_6276;

							BgL_tmpz00_6276 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1765z00_2431 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6276);
						}
						bgl_display_string(BGl_string2655z00zzsaw_c_codez00,
							BgL_arg1765z00_2431);
					}
					{	/* SawC/code.scm 50 */
						obj_t BgL_arg1767z00_2432;

						{	/* SawC/code.scm 50 */
							obj_t BgL_tmpz00_6280;

							BgL_tmpz00_6280 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1767z00_2432 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6280);
						}
						bgl_display_string(BGl_string2656z00zzsaw_c_codez00,
							BgL_arg1767z00_2432);
					}
					{	/* SawC/code.scm 51 */
						obj_t BgL_arg1770z00_2433;

						{	/* SawC/code.scm 51 */
							obj_t BgL_tmpz00_6284;

							BgL_tmpz00_6284 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1770z00_2433 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6284);
						}
						bgl_display_string(BGl_string2657z00zzsaw_c_codez00,
							BgL_arg1770z00_2433);
					}
					{	/* SawC/code.scm 52 */
						obj_t BgL_arg1771z00_2434;

						{	/* SawC/code.scm 52 */
							obj_t BgL_tmpz00_6288;

							BgL_tmpz00_6288 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1771z00_2434 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6288);
						}
						bgl_display_string(BGl_string2658z00zzsaw_c_codez00,
							BgL_arg1771z00_2434);
					}
				}
			else
				{	/* SawC/code.scm 48 */
					BFALSE;
				}
			{	/* SawC/code.scm 54 */
				obj_t BgL_arg1773z00_2435;

				{	/* SawC/code.scm 54 */
					obj_t BgL_tmpz00_6292;

					BgL_tmpz00_6292 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1773z00_2435 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6292);
				}
				return
					bgl_display_string(BGl_string2659z00zzsaw_c_codez00,
					BgL_arg1773z00_2435);
			}
		}

	}



/* saw-cepilogue */
	BGL_EXPORTED_DEF obj_t BGl_sawzd2cepiloguezd2zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 56 */
			return
				BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
				(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
				BGl_proc2660z00zzsaw_c_codez00);
		}

	}



/* &saw-cepilogue */
	obj_t BGl_z62sawzd2cepiloguezb0zzsaw_c_codez00(obj_t BgL_envz00_5821)
	{
		{	/* SawC/code.scm 56 */
			return BGl_sawzd2cepiloguezd2zzsaw_c_codez00();
		}

	}



/* &<@anonymous:1776> */
	obj_t BGl_z62zc3z04anonymousza31776ze3ze5zzsaw_c_codez00(obj_t
		BgL_envz00_5822)
	{
		{	/* SawC/code.scm 58 */
			return BGl_cepiloguez00zzsaw_c_codez00();
		}

	}



/* cepilogue */
	obj_t BGl_cepiloguez00zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 60 */
			{	/* SawC/code.scm 61 */
				obj_t BgL_arg1798z00_2439;

				{	/* SawC/code.scm 61 */
					obj_t BgL_tmpz00_6299;

					BgL_tmpz00_6299 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1798z00_2439 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6299);
				}
				bgl_display_string(BGl_string2659z00zzsaw_c_codez00,
					BgL_arg1798z00_2439);
			}
			if (BGl_za2countza2z00zzsaw_c_codez00)
				{	/* SawC/code.scm 62 */
					{	/* SawC/code.scm 63 */
						obj_t BgL_list1799z00_2440;

						{	/* SawC/code.scm 63 */
							obj_t BgL_arg1805z00_2441;

							{	/* SawC/code.scm 63 */
								obj_t BgL_arg1806z00_2442;

								BgL_arg1806z00_2442 =
									MAKE_YOUNG_PAIR(BGl_string2661z00zzsaw_c_codez00, BNIL);
								BgL_arg1805z00_2441 =
									MAKE_YOUNG_PAIR(BGl_za2counterza2z00zzsaw_c_codez00,
									BgL_arg1806z00_2442);
							}
							BgL_list1799z00_2440 =
								MAKE_YOUNG_PAIR(BGl_string2662z00zzsaw_c_codez00,
								BgL_arg1805z00_2441);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1799z00_2440);
					}
					{	/* SawC/code.scm 64 */
						obj_t BgL_list1807z00_2443;

						{	/* SawC/code.scm 64 */
							obj_t BgL_arg1808z00_2444;

							{	/* SawC/code.scm 64 */
								obj_t BgL_arg1812z00_2445;

								BgL_arg1812z00_2445 =
									MAKE_YOUNG_PAIR(BGl_string2661z00zzsaw_c_codez00, BNIL);
								BgL_arg1808z00_2444 =
									MAKE_YOUNG_PAIR(BGl_za2counterza2z00zzsaw_c_codez00,
									BgL_arg1812z00_2445);
							}
							BgL_list1807z00_2443 =
								MAKE_YOUNG_PAIR(BGl_string2663z00zzsaw_c_codez00,
								BgL_arg1808z00_2444);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1807z00_2443);
					}
					{	/* SawC/code.scm 65 */
						obj_t BgL_list1813z00_2446;

						{	/* SawC/code.scm 65 */
							obj_t BgL_arg1820z00_2447;

							{	/* SawC/code.scm 65 */
								obj_t BgL_arg1822z00_2448;

								BgL_arg1822z00_2448 =
									MAKE_YOUNG_PAIR(BGl_string2661z00zzsaw_c_codez00, BNIL);
								BgL_arg1820z00_2447 =
									MAKE_YOUNG_PAIR(BGl_za2counterza2z00zzsaw_c_codez00,
									BgL_arg1822z00_2448);
							}
							BgL_list1813z00_2446 =
								MAKE_YOUNG_PAIR(BGl_string2664z00zzsaw_c_codez00,
								BgL_arg1820z00_2447);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1813z00_2446);
					}
					{	/* SawC/code.scm 66 */
						obj_t BgL_arg1823z00_2449;

						{	/* SawC/code.scm 66 */
							obj_t BgL_tmpz00_6316;

							BgL_tmpz00_6316 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1823z00_2449 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6316);
						}
						bgl_display_string(BGl_string2665z00zzsaw_c_codez00,
							BgL_arg1823z00_2449);
					}
					{	/* SawC/code.scm 67 */
						obj_t BgL_arg1831z00_2450;

						{	/* SawC/code.scm 67 */
							obj_t BgL_tmpz00_6320;

							BgL_tmpz00_6320 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1831z00_2450 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6320);
						}
						bgl_display_string(BGl_string2666z00zzsaw_c_codez00,
							BgL_arg1831z00_2450);
					}
					{	/* SawC/code.scm 68 */
						obj_t BgL_arg1832z00_2451;

						{	/* SawC/code.scm 68 */
							obj_t BgL_tmpz00_6324;

							BgL_tmpz00_6324 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1832z00_2451 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6324);
						}
						bgl_display_string(BGl_string2667z00zzsaw_c_codez00,
							BgL_arg1832z00_2451);
					}
					{	/* SawC/code.scm 69 */
						obj_t BgL_list1833z00_2452;

						{	/* SawC/code.scm 69 */
							obj_t BgL_arg1834z00_2453;

							{	/* SawC/code.scm 69 */
								obj_t BgL_arg1835z00_2454;

								BgL_arg1835z00_2454 =
									MAKE_YOUNG_PAIR(BGl_string2668z00zzsaw_c_codez00, BNIL);
								BgL_arg1834z00_2453 =
									MAKE_YOUNG_PAIR(BGl_za2counterza2z00zzsaw_c_codez00,
									BgL_arg1835z00_2454);
							}
							BgL_list1833z00_2452 =
								MAKE_YOUNG_PAIR(BGl_string2669z00zzsaw_c_codez00,
								BgL_arg1834z00_2453);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1833z00_2452);
					}
					{	/* SawC/code.scm 70 */
						obj_t BgL_arg1836z00_2455;

						{	/* SawC/code.scm 70 */
							obj_t BgL_tmpz00_6332;

							BgL_tmpz00_6332 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1836z00_2455 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6332);
						}
						bgl_display_string(BGl_string2670z00zzsaw_c_codez00,
							BgL_arg1836z00_2455);
					}
					{	/* SawC/code.scm 71 */
						obj_t BgL_arg1837z00_2456;

						{	/* SawC/code.scm 71 */
							obj_t BgL_tmpz00_6336;

							BgL_tmpz00_6336 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1837z00_2456 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6336);
						}
						bgl_display_string(BGl_string2671z00zzsaw_c_codez00,
							BgL_arg1837z00_2456);
					}
					{	/* SawC/code.scm 72 */
						obj_t BgL_arg1838z00_2457;

						{	/* SawC/code.scm 72 */
							obj_t BgL_tmpz00_6340;

							BgL_tmpz00_6340 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1838z00_2457 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6340);
						}
						bgl_display_string(BGl_string2672z00zzsaw_c_codez00,
							BgL_arg1838z00_2457);
					}
					{	/* SawC/code.scm 73 */
						obj_t BgL_arg1839z00_2458;

						{	/* SawC/code.scm 73 */
							obj_t BgL_tmpz00_6344;

							BgL_tmpz00_6344 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1839z00_2458 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6344);
						}
						bgl_display_string(BGl_string2673z00zzsaw_c_codez00,
							BgL_arg1839z00_2458);
					}
					{	/* SawC/code.scm 74 */
						obj_t BgL_arg1840z00_2459;

						{	/* SawC/code.scm 74 */
							obj_t BgL_tmpz00_6348;

							BgL_tmpz00_6348 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1840z00_2459 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6348);
						}
						bgl_display_string(BGl_string2674z00zzsaw_c_codez00,
							BgL_arg1840z00_2459);
					}
					{	/* SawC/code.scm 75 */
						obj_t BgL_arg1842z00_2460;

						{	/* SawC/code.scm 75 */
							obj_t BgL_tmpz00_6352;

							BgL_tmpz00_6352 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1842z00_2460 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6352);
						}
						bgl_display_string(BGl_string2675z00zzsaw_c_codez00,
							BgL_arg1842z00_2460);
					}
					{	/* SawC/code.scm 76 */
						obj_t BgL_arg1843z00_2461;

						{	/* SawC/code.scm 76 */
							obj_t BgL_tmpz00_6356;

							BgL_tmpz00_6356 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1843z00_2461 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6356);
						}
						bgl_display_string(BGl_string2676z00zzsaw_c_codez00,
							BgL_arg1843z00_2461);
					}
					{	/* SawC/code.scm 77 */
						obj_t BgL_arg1844z00_2462;

						{	/* SawC/code.scm 77 */
							obj_t BgL_tmpz00_6360;

							BgL_tmpz00_6360 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg1844z00_2462 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6360);
						}
						return
							bgl_display_string(BGl_string2677z00zzsaw_c_codez00,
							BgL_arg1844z00_2462);
					}
				}
			else
				{	/* SawC/code.scm 62 */
					return BFALSE;
				}
		}

	}



/* saw-cgen */
	BGL_EXPORTED_DEF obj_t BGl_sawzd2cgenzd2zzsaw_c_codez00(BgL_cvmz00_bglt
		BgL_bz00_46, BgL_globalz00_bglt BgL_vz00_47)
	{
		{	/* SawC/code.scm 79 */
			{	/* SawC/code.scm 80 */
				obj_t BgL_lz00_2463;

				BgL_lz00_2463 =
					BGl_globalzd2ze3blocksz31zzsaw_woodcutterz00(
					((BgL_backendz00_bglt) BgL_bz00_46), BgL_vz00_47);
				BGl_za2hasprotectza2z00zzsaw_c_codez00 = ((bool_t) 0);
				BGl_za2haspushexitza2z00zzsaw_c_codez00 = ((bool_t) 0);
				BGl_za2haspushbeforeza2z00zzsaw_c_codez00 = ((bool_t) 0);
				BGl_za2pushtraceemmitedza2z00zzsaw_c_codez00 = 0L;
				{	/* SawC/code.scm 86 */
					obj_t BgL_zc3z04anonymousza31846ze3z87_5823;

					BgL_zc3z04anonymousza31846ze3z87_5823 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31846ze3ze5zzsaw_c_codez00, (int) (0L),
						(int) (3L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31846ze3z87_5823, (int) (0L),
						((obj_t) BgL_bz00_46));
					PROCEDURE_SET(BgL_zc3z04anonymousza31846ze3z87_5823, (int) (1L),
						((obj_t) BgL_vz00_47));
					PROCEDURE_SET(BgL_zc3z04anonymousza31846ze3z87_5823, (int) (2L),
						BgL_lz00_2463);
					return
						BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
						(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
						BgL_zc3z04anonymousza31846ze3z87_5823);
				}
			}
		}

	}



/* &saw-cgen */
	obj_t BGl_z62sawzd2cgenzb0zzsaw_c_codez00(obj_t BgL_envz00_5824,
		obj_t BgL_bz00_5825, obj_t BgL_vz00_5826)
	{
		{	/* SawC/code.scm 79 */
			return
				BGl_sawzd2cgenzd2zzsaw_c_codez00(
				((BgL_cvmz00_bglt) BgL_bz00_5825),
				((BgL_globalz00_bglt) BgL_vz00_5826));
		}

	}



/* &<@anonymous:1846> */
	obj_t BGl_z62zc3z04anonymousza31846ze3ze5zzsaw_c_codez00(obj_t
		BgL_envz00_5827)
	{
		{	/* SawC/code.scm 86 */
			{	/* SawC/code.scm 86 */
				BgL_cvmz00_bglt BgL_bz00_5828;
				BgL_globalz00_bglt BgL_vz00_5829;
				obj_t BgL_lz00_5830;

				BgL_bz00_5828 =
					((BgL_cvmz00_bglt) PROCEDURE_REF(BgL_envz00_5827, (int) (0L)));
				BgL_vz00_5829 =
					((BgL_globalz00_bglt) PROCEDURE_REF(BgL_envz00_5827, (int) (1L)));
				BgL_lz00_5830 = ((obj_t) PROCEDURE_REF(BgL_envz00_5827, (int) (2L)));
				return
					BGl_genfunz00zzsaw_c_codez00(BgL_bz00_5828, BgL_vz00_5829,
					BgL_lz00_5830);
			}
		}

	}



/* genfun */
	obj_t BGl_genfunz00zzsaw_c_codez00(BgL_cvmz00_bglt BgL_bz00_48,
		BgL_globalz00_bglt BgL_vz00_49, obj_t BgL_lz00_50)
	{
		{	/* SawC/code.scm 88 */
			{	/* SawC/code.scm 90 */
				BgL_sfunz00_bglt BgL_i1179z00_2468;

				BgL_i1179z00_2468 =
					((BgL_sfunz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_vz00_49)))->BgL_valuez00));
				{	/* SawC/code.scm 91 */
					obj_t BgL_paramsz00_2469;

					{	/* SawC/code.scm 91 */
						obj_t BgL_l1617z00_2497;

						BgL_l1617z00_2497 =
							(((BgL_sfunz00_bglt) COBJECT(BgL_i1179z00_2468))->BgL_argsz00);
						if (NULLP(BgL_l1617z00_2497))
							{	/* SawC/code.scm 91 */
								BgL_paramsz00_2469 = BNIL;
							}
						else
							{	/* SawC/code.scm 91 */
								obj_t BgL_head1619z00_2499;

								{	/* SawC/code.scm 91 */
									BgL_rtl_regz00_bglt BgL_arg1883z00_2511;

									{	/* SawC/code.scm 91 */
										obj_t BgL_arg1884z00_2512;

										BgL_arg1884z00_2512 = CAR(((obj_t) BgL_l1617z00_2497));
										BgL_arg1883z00_2511 =
											BGl_localzd2ze3regz31zzsaw_node2rtlz00(
											((BgL_localz00_bglt) BgL_arg1884z00_2512));
									}
									BgL_head1619z00_2499 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_arg1883z00_2511), BNIL);
								}
								{	/* SawC/code.scm 91 */
									obj_t BgL_g1622z00_2500;

									BgL_g1622z00_2500 = CDR(((obj_t) BgL_l1617z00_2497));
									{
										obj_t BgL_l1617z00_2502;
										obj_t BgL_tail1620z00_2503;

										BgL_l1617z00_2502 = BgL_g1622z00_2500;
										BgL_tail1620z00_2503 = BgL_head1619z00_2499;
									BgL_zc3z04anonymousza31877ze3z87_2504:
										if (NULLP(BgL_l1617z00_2502))
											{	/* SawC/code.scm 91 */
												BgL_paramsz00_2469 = BgL_head1619z00_2499;
											}
										else
											{	/* SawC/code.scm 91 */
												obj_t BgL_newtail1621z00_2506;

												{	/* SawC/code.scm 91 */
													BgL_rtl_regz00_bglt BgL_arg1880z00_2508;

													{	/* SawC/code.scm 91 */
														obj_t BgL_arg1882z00_2509;

														BgL_arg1882z00_2509 =
															CAR(((obj_t) BgL_l1617z00_2502));
														BgL_arg1880z00_2508 =
															BGl_localzd2ze3regz31zzsaw_node2rtlz00(
															((BgL_localz00_bglt) BgL_arg1882z00_2509));
													}
													BgL_newtail1621z00_2506 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1880z00_2508), BNIL);
												}
												SET_CDR(BgL_tail1620z00_2503, BgL_newtail1621z00_2506);
												{	/* SawC/code.scm 91 */
													obj_t BgL_arg1879z00_2507;

													BgL_arg1879z00_2507 =
														CDR(((obj_t) BgL_l1617z00_2502));
													{
														obj_t BgL_tail1620z00_6417;
														obj_t BgL_l1617z00_6416;

														BgL_l1617z00_6416 = BgL_arg1879z00_2507;
														BgL_tail1620z00_6417 = BgL_newtail1621z00_2506;
														BgL_tail1620z00_2503 = BgL_tail1620z00_6417;
														BgL_l1617z00_2502 = BgL_l1617z00_6416;
														goto BgL_zc3z04anonymousza31877ze3z87_2504;
													}
												}
											}
									}
								}
							}
					}
					BGl_buildzd2treezd2zzsaw_exprz00(
						((BgL_backendz00_bglt) BgL_bz00_48), BgL_paramsz00_2469,
						BgL_lz00_50);
					BgL_lz00_50 =
						BGl_registerzd2allocationzd2zzsaw_registerzd2allocationzd2((
							(BgL_backendz00_bglt) BgL_bz00_48), BgL_vz00_49,
						BgL_paramsz00_2469, BgL_lz00_50);
					BgL_lz00_50 =
						BGl_bbvz00zzsaw_bbvz00(((BgL_backendz00_bglt) BgL_bz00_48),
						BgL_vz00_49, BgL_paramsz00_2469, BgL_lz00_50);
					{	/* SawC/code.scm 96 */
						obj_t BgL_localsz00_2470;

						BgL_localsz00_2470 =
							BGl_getzd2localszd2zzsaw_c_codez00(BgL_paramsz00_2469,
							BgL_lz00_50);
						{	/* SawC/code.scm 98 */
							obj_t BgL_insz00_2475;

							{	/* SawC/code.scm 98 */
								obj_t BgL_arg1854z00_2480;

								{	/* SawC/code.scm 98 */
									BgL_blockz00_bglt BgL_oz00_4080;

									{	/* SawC/code.scm 98 */
										obj_t BgL_pairz00_4079;

										BgL_pairz00_4079 = BgL_lz00_50;
										BgL_oz00_4080 = ((BgL_blockz00_bglt) CAR(BgL_pairz00_4079));
									}
									BgL_arg1854z00_2480 =
										(((BgL_blockz00_bglt) COBJECT(BgL_oz00_4080))->
										BgL_firstz00);
								}
								BgL_insz00_2475 = CAR(((obj_t) BgL_arg1854z00_2480));
							}
							{	/* SawC/code.scm 99 */
								bool_t BgL_test3052z00_6430;

								if (CBOOL
									(BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00))
									{	/* SawC/code.scm 99 */
										if (CBOOL
											(BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00))
											{	/* SawC/code.scm 100 */
												BgL_test3052z00_6430 = ((bool_t) 0);
											}
										else
											{	/* SawC/code.scm 100 */
												BgL_test3052z00_6430 = ((bool_t) 1);
											}
									}
								else
									{	/* SawC/code.scm 99 */
										BgL_test3052z00_6430 = ((bool_t) 0);
									}
								if (BgL_test3052z00_6430)
									{	/* SawC/code.scm 101 */
										bool_t BgL__ortest_1180z00_2477;

										BgL__ortest_1180z00_2477 =
											BGl_printzd2locationzf3z21zzsaw_c_codez00(
											(((BgL_sfunz00_bglt) COBJECT(BgL_i1179z00_2468))->
												BgL_locz00));
										if (BgL__ortest_1180z00_2477)
											{	/* SawC/code.scm 101 */
												BgL__ortest_1180z00_2477;
											}
										else
											{	/* SawC/code.scm 102 */
												obj_t BgL_arg1852z00_2478;

												BgL_arg1852z00_2478 =
													BGl_findzd2locationzd2zzsaw_c_codez00(
													((BgL_rtl_insz00_bglt) BgL_insz00_2475));
												BGl_printzd2locationzf3z21zzsaw_c_codez00
													(BgL_arg1852z00_2478);
											}
									}
								else
									{	/* SawC/code.scm 99 */
										((bool_t) 0);
									}
							}
						}
						{	/* SawC/code.scm 103 */
							BgL_typez00_bglt BgL_arg1857z00_2482;

							BgL_arg1857z00_2482 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_vz00_49)))->BgL_typez00);
							{	/* SawC/code.scm 496 */
								obj_t BgL_arg2077z00_4083;

								BgL_arg2077z00_4083 =
									(((BgL_typez00_bglt) COBJECT(BgL_arg1857z00_2482))->
									BgL_namez00);
								{	/* SawC/code.scm 496 */
									obj_t BgL_list2078z00_4084;

									BgL_list2078z00_4084 =
										MAKE_YOUNG_PAIR(BgL_arg2077z00_4083, BNIL);
									BGl_displayza2za2zz__r4_output_6_10_3z00
										(BgL_list2078z00_4084);
								}
							}
						}
						{	/* SawC/code.scm 104 */
							obj_t BgL_arg1858z00_2483;

							{	/* SawC/code.scm 515 */
								obj_t BgL_arg2080z00_4087;

								BgL_arg2080z00_4087 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_vz00_49)))->BgL_namez00);
								BgL_arg1858z00_2483 =
									BGl_inlzd2opzd2zzsaw_c_codez00(BgL_arg2080z00_4087);
							}
							{	/* SawC/code.scm 104 */
								obj_t BgL_list1859z00_2484;

								{	/* SawC/code.scm 104 */
									obj_t BgL_arg1860z00_2485;

									BgL_arg1860z00_2485 =
										MAKE_YOUNG_PAIR(BgL_arg1858z00_2483, BNIL);
									BgL_list1859z00_2484 =
										MAKE_YOUNG_PAIR(BGl_string2678z00zzsaw_c_codez00,
										BgL_arg1860z00_2485);
								}
								BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1859z00_2484);
							}
						}
						BGl_genzd2typezd2regsz00zzsaw_c_codez00(BgL_paramsz00_2469);
						{	/* SawC/code.scm 106 */
							obj_t BgL_arg1862z00_2486;

							{	/* SawC/code.scm 106 */
								obj_t BgL_tmpz00_6453;

								BgL_tmpz00_6453 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1862z00_2486 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6453);
							}
							bgl_display_string(BGl_string2679z00zzsaw_c_codez00,
								BgL_arg1862z00_2486);
						}
						if (BGl_za2hasprotectza2z00zzsaw_c_codez00)
							{	/* SawC/code.scm 107 */
								obj_t BgL_arg1863z00_2487;

								{	/* SawC/code.scm 107 */
									obj_t BgL_tmpz00_6458;

									BgL_tmpz00_6458 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1863z00_2487 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6458);
								}
								bgl_display_string(BGl_string2680z00zzsaw_c_codez00,
									BgL_arg1863z00_2487);
							}
						else
							{	/* SawC/code.scm 107 */
								BFALSE;
							}
						if (BGl_za2haspushexitza2z00zzsaw_c_codez00)
							{	/* SawC/code.scm 108 */
								obj_t BgL_arg1864z00_2488;

								{	/* SawC/code.scm 108 */
									obj_t BgL_tmpz00_6463;

									BgL_tmpz00_6463 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1864z00_2488 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6463);
								}
								bgl_display_string(BGl_string2681z00zzsaw_c_codez00,
									BgL_arg1864z00_2488);
							}
						else
							{	/* SawC/code.scm 108 */
								BFALSE;
							}
						if (BGl_za2haspushbeforeza2z00zzsaw_c_codez00)
							{	/* SawC/code.scm 109 */
								obj_t BgL_arg1866z00_2489;

								{	/* SawC/code.scm 109 */
									obj_t BgL_tmpz00_6468;

									BgL_tmpz00_6468 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1866z00_2489 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6468);
								}
								bgl_display_string(BGl_string2682z00zzsaw_c_codez00,
									BgL_arg1866z00_2489);
							}
						else
							{	/* SawC/code.scm 109 */
								BFALSE;
							}
						BGl_declarezd2regszd2zzsaw_c_codez00(BgL_localsz00_2470);
						((bool_t) 0);
					}
				}
			}
			{	/* SawC/code.scm 113 */
				obj_t BgL_arg1885z00_2514;

				BgL_arg1885z00_2514 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_vz00_49)))->BgL_idz00);
				BGl_genbodyz00zzsaw_c_codez00(BgL_lz00_50, BgL_arg1885z00_2514);
			}
			{	/* SawC/code.scm 114 */
				obj_t BgL_arg1887z00_2515;
				obj_t BgL_arg1888z00_2516;

				BgL_arg1887z00_2515 =
					make_string(BGl_za2pushtraceemmitedza2z00zzsaw_c_codez00,
					((unsigned char) '}'));
				{	/* SawC/code.scm 114 */
					obj_t BgL_tmpz00_6477;

					BgL_tmpz00_6477 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1888z00_2516 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6477);
				}
				bgl_display_obj(BgL_arg1887z00_2515, BgL_arg1888z00_2516);
			}
			{	/* SawC/code.scm 115 */
				obj_t BgL_arg1889z00_2517;

				{	/* SawC/code.scm 115 */
					obj_t BgL_tmpz00_6481;

					BgL_tmpz00_6481 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1889z00_2517 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6481);
				}
				return
					bgl_display_string(BGl_string2683z00zzsaw_c_codez00,
					BgL_arg1889z00_2517);
			}
		}

	}



/* get-locals */
	obj_t BGl_getzd2localszd2zzsaw_c_codez00(obj_t BgL_paramsz00_51,
		obj_t BgL_lz00_52)
	{
		{	/* SawC/code.scm 117 */
			{	/* SawC/code.scm 119 */
				struct bgl_cell BgL_box3059_5986z00;
				struct bgl_cell BgL_box3060_5987z00;
				obj_t BgL_nz00_5986;
				obj_t BgL_regsz00_5987;

				BgL_nz00_5986 = MAKE_CELL_STACK(BINT(0L), BgL_box3059_5986z00);
				BgL_regsz00_5987 = MAKE_CELL_STACK(BNIL, BgL_box3060_5987z00);
				{
					BgL_blockz00_bglt BgL_bz00_2569;

					{
						obj_t BgL_l1635z00_2524;

						BgL_l1635z00_2524 = BgL_paramsz00_51;
					BgL_zc3z04anonymousza31890ze3z87_2525:
						if (PAIRP(BgL_l1635z00_2524))
							{	/* SawC/code.scm 142 */
								BGl_exprzd2ze3iregze70zd6zzsaw_c_codez00(BgL_regsz00_5987,
									BgL_nz00_5986, CAR(BgL_l1635z00_2524));
								{
									obj_t BgL_l1635z00_6490;

									BgL_l1635z00_6490 = CDR(BgL_l1635z00_2524);
									BgL_l1635z00_2524 = BgL_l1635z00_6490;
									goto BgL_zc3z04anonymousza31890ze3z87_2525;
								}
							}
						else
							{	/* SawC/code.scm 142 */
								((bool_t) 1);
							}
					}
					{	/* SawC/code.scm 143 */
						obj_t BgL_auxz00_5988;

						BgL_auxz00_5988 = BNIL;
						CELL_SET(BgL_regsz00_5987, BgL_auxz00_5988);
					}
					{
						obj_t BgL_l1637z00_2531;

						BgL_l1637z00_2531 = BgL_lz00_52;
					BgL_zc3z04anonymousza31894ze3z87_2532:
						if (PAIRP(BgL_l1637z00_2531))
							{	/* SawC/code.scm 144 */
								{	/* SawC/code.scm 144 */
									obj_t BgL_arg1896z00_2534;

									BgL_arg1896z00_2534 = CAR(BgL_l1637z00_2531);
									BgL_bz00_2569 = ((BgL_blockz00_bglt) BgL_arg1896z00_2534);
									{	/* SawC/code.scm 135 */
										obj_t BgL_g1634z00_2571;

										BgL_g1634z00_2571 =
											(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2569))->
											BgL_firstz00);
										{
											obj_t BgL_l1632z00_2573;

											BgL_l1632z00_2573 = BgL_g1634z00_2571;
										BgL_zc3z04anonymousza31916ze3z87_2574:
											if (PAIRP(BgL_l1632z00_2573))
												{	/* SawC/code.scm 141 */
													{	/* SawC/code.scm 137 */
														obj_t BgL_insz00_2576;

														BgL_insz00_2576 = CAR(BgL_l1632z00_2573);
														{	/* SawC/code.scm 138 */
															BgL_rtl_funz00_bglt BgL_arg1918z00_2578;

															BgL_arg1918z00_2578 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_insz00_2576)))->
																BgL_funz00);
															BGl_check_funze70ze7zzsaw_c_codez00
																(BgL_arg1918z00_2578);
														}
														if (CBOOL(
																(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				BgL_insz00_2576)))->BgL_destz00)))
															{	/* SawC/code.scm 139 */
																obj_t BgL_arg1920z00_2580;

																BgL_arg1920z00_2580 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				BgL_insz00_2576)))->BgL_destz00);
																BGl_exprzd2ze3iregze70zd6zzsaw_c_codez00
																	(BgL_regsz00_5987, BgL_nz00_5986,
																	BgL_arg1920z00_2580);
															}
														else
															{	/* SawC/code.scm 139 */
																BFALSE;
															}
														{	/* SawC/code.scm 140 */
															obj_t BgL_g1631z00_2581;

															BgL_g1631z00_2581 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_insz00_2576)))->
																BgL_argsz00);
															{
																obj_t BgL_l1629z00_2583;

																BgL_l1629z00_2583 = BgL_g1631z00_2581;
															BgL_zc3z04anonymousza31921ze3z87_2584:
																if (PAIRP(BgL_l1629z00_2583))
																	{	/* SawC/code.scm 140 */
																		BGl_exprzd2ze3iregze70zd6zzsaw_c_codez00
																			(BgL_regsz00_5987, BgL_nz00_5986,
																			CAR(BgL_l1629z00_2583));
																		{
																			obj_t BgL_l1629z00_6515;

																			BgL_l1629z00_6515 =
																				CDR(BgL_l1629z00_2583);
																			BgL_l1629z00_2583 = BgL_l1629z00_6515;
																			goto
																				BgL_zc3z04anonymousza31921ze3z87_2584;
																		}
																	}
																else
																	{	/* SawC/code.scm 140 */
																		((bool_t) 1);
																	}
															}
														}
													}
													{
														obj_t BgL_l1632z00_6517;

														BgL_l1632z00_6517 = CDR(BgL_l1632z00_2573);
														BgL_l1632z00_2573 = BgL_l1632z00_6517;
														goto BgL_zc3z04anonymousza31916ze3z87_2574;
													}
												}
											else
												{	/* SawC/code.scm 141 */
													((bool_t) 1);
												}
										}
									}
								}
								{
									obj_t BgL_l1637z00_6520;

									BgL_l1637z00_6520 = CDR(BgL_l1637z00_2531);
									BgL_l1637z00_2531 = BgL_l1637z00_6520;
									goto BgL_zc3z04anonymousza31894ze3z87_2532;
								}
							}
						else
							{	/* SawC/code.scm 144 */
								((bool_t) 1);
							}
					}
					return ((obj_t) ((obj_t) CELL_REF(BgL_regsz00_5987)));
				}
			}
		}

	}



/* check_fun~0 */
	obj_t BGl_check_funze70ze7zzsaw_c_codez00(BgL_rtl_funz00_bglt BgL_funz00_2537)
	{
		{	/* SawC/code.scm 121 */
			{	/* SawC/code.scm 121 */
				bool_t BgL_test3066z00_6523;

				{	/* SawC/code.scm 121 */
					obj_t BgL_classz00_4106;

					BgL_classz00_4106 = BGl_rtl_protectz00zzsaw_defsz00;
					{	/* SawC/code.scm 121 */
						BgL_objectz00_bglt BgL_arg1807z00_4108;

						{	/* SawC/code.scm 121 */
							obj_t BgL_tmpz00_6524;

							BgL_tmpz00_6524 =
								((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2537));
							BgL_arg1807z00_4108 = (BgL_objectz00_bglt) (BgL_tmpz00_6524);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawC/code.scm 121 */
								long BgL_idxz00_4114;

								BgL_idxz00_4114 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4108);
								BgL_test3066z00_6523 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4114 + 2L)) == BgL_classz00_4106);
							}
						else
							{	/* SawC/code.scm 121 */
								bool_t BgL_res2608z00_4139;

								{	/* SawC/code.scm 121 */
									obj_t BgL_oclassz00_4122;

									{	/* SawC/code.scm 121 */
										obj_t BgL_arg1815z00_4130;
										long BgL_arg1816z00_4131;

										BgL_arg1815z00_4130 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawC/code.scm 121 */
											long BgL_arg1817z00_4132;

											BgL_arg1817z00_4132 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4108);
											BgL_arg1816z00_4131 = (BgL_arg1817z00_4132 - OBJECT_TYPE);
										}
										BgL_oclassz00_4122 =
											VECTOR_REF(BgL_arg1815z00_4130, BgL_arg1816z00_4131);
									}
									{	/* SawC/code.scm 121 */
										bool_t BgL__ortest_1115z00_4123;

										BgL__ortest_1115z00_4123 =
											(BgL_classz00_4106 == BgL_oclassz00_4122);
										if (BgL__ortest_1115z00_4123)
											{	/* SawC/code.scm 121 */
												BgL_res2608z00_4139 = BgL__ortest_1115z00_4123;
											}
										else
											{	/* SawC/code.scm 121 */
												long BgL_odepthz00_4124;

												{	/* SawC/code.scm 121 */
													obj_t BgL_arg1804z00_4125;

													BgL_arg1804z00_4125 = (BgL_oclassz00_4122);
													BgL_odepthz00_4124 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4125);
												}
												if ((2L < BgL_odepthz00_4124))
													{	/* SawC/code.scm 121 */
														obj_t BgL_arg1802z00_4127;

														{	/* SawC/code.scm 121 */
															obj_t BgL_arg1803z00_4128;

															BgL_arg1803z00_4128 = (BgL_oclassz00_4122);
															BgL_arg1802z00_4127 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4128,
																2L);
														}
														BgL_res2608z00_4139 =
															(BgL_arg1802z00_4127 == BgL_classz00_4106);
													}
												else
													{	/* SawC/code.scm 121 */
														BgL_res2608z00_4139 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test3066z00_6523 = BgL_res2608z00_4139;
							}
					}
				}
				if (BgL_test3066z00_6523)
					{	/* SawC/code.scm 121 */
						BGl_za2hasprotectza2z00zzsaw_c_codez00 = ((bool_t) 1);
					}
				else
					{	/* SawC/code.scm 121 */
						BFALSE;
					}
			}
			{	/* SawC/code.scm 122 */
				bool_t BgL_test3070z00_6547;

				{	/* SawC/code.scm 122 */
					obj_t BgL_classz00_4140;

					BgL_classz00_4140 = BGl_rtl_callz00zzsaw_defsz00;
					{	/* SawC/code.scm 122 */
						BgL_objectz00_bglt BgL_arg1807z00_4142;

						{	/* SawC/code.scm 122 */
							obj_t BgL_tmpz00_6548;

							BgL_tmpz00_6548 =
								((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2537));
							BgL_arg1807z00_4142 = (BgL_objectz00_bglt) (BgL_tmpz00_6548);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawC/code.scm 122 */
								long BgL_idxz00_4148;

								BgL_idxz00_4148 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4142);
								BgL_test3070z00_6547 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4148 + 2L)) == BgL_classz00_4140);
							}
						else
							{	/* SawC/code.scm 122 */
								bool_t BgL_res2609z00_4173;

								{	/* SawC/code.scm 122 */
									obj_t BgL_oclassz00_4156;

									{	/* SawC/code.scm 122 */
										obj_t BgL_arg1815z00_4164;
										long BgL_arg1816z00_4165;

										BgL_arg1815z00_4164 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawC/code.scm 122 */
											long BgL_arg1817z00_4166;

											BgL_arg1817z00_4166 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4142);
											BgL_arg1816z00_4165 = (BgL_arg1817z00_4166 - OBJECT_TYPE);
										}
										BgL_oclassz00_4156 =
											VECTOR_REF(BgL_arg1815z00_4164, BgL_arg1816z00_4165);
									}
									{	/* SawC/code.scm 122 */
										bool_t BgL__ortest_1115z00_4157;

										BgL__ortest_1115z00_4157 =
											(BgL_classz00_4140 == BgL_oclassz00_4156);
										if (BgL__ortest_1115z00_4157)
											{	/* SawC/code.scm 122 */
												BgL_res2609z00_4173 = BgL__ortest_1115z00_4157;
											}
										else
											{	/* SawC/code.scm 122 */
												long BgL_odepthz00_4158;

												{	/* SawC/code.scm 122 */
													obj_t BgL_arg1804z00_4159;

													BgL_arg1804z00_4159 = (BgL_oclassz00_4156);
													BgL_odepthz00_4158 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4159);
												}
												if ((2L < BgL_odepthz00_4158))
													{	/* SawC/code.scm 122 */
														obj_t BgL_arg1802z00_4161;

														{	/* SawC/code.scm 122 */
															obj_t BgL_arg1803z00_4162;

															BgL_arg1803z00_4162 = (BgL_oclassz00_4156);
															BgL_arg1802z00_4161 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4162,
																2L);
														}
														BgL_res2609z00_4173 =
															(BgL_arg1802z00_4161 == BgL_classz00_4140);
													}
												else
													{	/* SawC/code.scm 122 */
														BgL_res2609z00_4173 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test3070z00_6547 = BgL_res2609z00_4173;
							}
					}
				}
				if (BgL_test3070z00_6547)
					{	/* SawC/code.scm 123 */
						obj_t BgL_idz00_2541;

						BgL_idz00_2541 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										(((BgL_rtl_callz00_bglt) COBJECT(
													((BgL_rtl_callz00_bglt) BgL_funz00_2537)))->
											BgL_varz00))))->BgL_idz00);
						if ((BgL_idz00_2541 == CNST_TABLE_REF(1)))
							{	/* SawC/code.scm 124 */
								BGl_za2haspushexitza2z00zzsaw_c_codez00 = ((bool_t) 1);
							}
						else
							{	/* SawC/code.scm 124 */
								BFALSE;
							}
						if ((BgL_idz00_2541 == CNST_TABLE_REF(2)))
							{	/* SawC/code.scm 125 */
								return (BGl_za2haspushbeforeza2z00zzsaw_c_codez00 =
									((bool_t) 1), BUNSPEC);
							}
						else
							{	/* SawC/code.scm 125 */
								return BFALSE;
							}
					}
				else
					{	/* SawC/code.scm 122 */
						return BFALSE;
					}
			}
		}

	}



/* expr->ireg~0 */
	obj_t BGl_exprzd2ze3iregze70zd6zzsaw_c_codez00(obj_t BgL_regsz00_5983,
		obj_t BgL_nz00_5982, obj_t BgL_ez00_2543)
	{
		{	/* SawC/code.scm 133 */
			{	/* SawC/code.scm 128 */
				bool_t BgL__ortest_1182z00_2545;

				{	/* SawC/code.scm 128 */
					obj_t BgL_classz00_4176;

					BgL_classz00_4176 = BGl_SawCIregz00zzsaw_c_codez00;
					if (BGL_OBJECTP(BgL_ez00_2543))
						{	/* SawC/code.scm 128 */
							obj_t BgL_oclassz00_4178;

							{	/* SawC/code.scm 128 */
								obj_t BgL_arg1815z00_4180;
								long BgL_arg1816z00_4181;

								BgL_arg1815z00_4180 = (BGl_za2classesza2z00zz__objectz00);
								{	/* SawC/code.scm 128 */
									long BgL_arg1817z00_4182;

									BgL_arg1817z00_4182 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_2543));
									BgL_arg1816z00_4181 = (BgL_arg1817z00_4182 - OBJECT_TYPE);
								}
								BgL_oclassz00_4178 =
									VECTOR_REF(BgL_arg1815z00_4180, BgL_arg1816z00_4181);
							}
							BgL__ortest_1182z00_2545 =
								(BgL_oclassz00_4178 == BgL_classz00_4176);
						}
					else
						{	/* SawC/code.scm 128 */
							BgL__ortest_1182z00_2545 = ((bool_t) 0);
						}
				}
				if (BgL__ortest_1182z00_2545)
					{	/* SawC/code.scm 128 */
						return BBOOL(BgL__ortest_1182z00_2545);
					}
				else
					{	/* SawC/code.scm 129 */
						bool_t BgL_test3078z00_6591;

						{	/* SawC/code.scm 129 */
							obj_t BgL_classz00_4188;

							BgL_classz00_4188 = BGl_rtl_regz00zzsaw_defsz00;
							if (BGL_OBJECTP(BgL_ez00_2543))
								{	/* SawC/code.scm 129 */
									BgL_objectz00_bglt BgL_arg1807z00_4190;

									BgL_arg1807z00_4190 = (BgL_objectz00_bglt) (BgL_ez00_2543);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawC/code.scm 129 */
											long BgL_idxz00_4196;

											BgL_idxz00_4196 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4190);
											BgL_test3078z00_6591 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4196 + 1L)) == BgL_classz00_4188);
										}
									else
										{	/* SawC/code.scm 129 */
											bool_t BgL_res2610z00_4221;

											{	/* SawC/code.scm 129 */
												obj_t BgL_oclassz00_4204;

												{	/* SawC/code.scm 129 */
													obj_t BgL_arg1815z00_4212;
													long BgL_arg1816z00_4213;

													BgL_arg1815z00_4212 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawC/code.scm 129 */
														long BgL_arg1817z00_4214;

														BgL_arg1817z00_4214 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4190);
														BgL_arg1816z00_4213 =
															(BgL_arg1817z00_4214 - OBJECT_TYPE);
													}
													BgL_oclassz00_4204 =
														VECTOR_REF(BgL_arg1815z00_4212,
														BgL_arg1816z00_4213);
												}
												{	/* SawC/code.scm 129 */
													bool_t BgL__ortest_1115z00_4205;

													BgL__ortest_1115z00_4205 =
														(BgL_classz00_4188 == BgL_oclassz00_4204);
													if (BgL__ortest_1115z00_4205)
														{	/* SawC/code.scm 129 */
															BgL_res2610z00_4221 = BgL__ortest_1115z00_4205;
														}
													else
														{	/* SawC/code.scm 129 */
															long BgL_odepthz00_4206;

															{	/* SawC/code.scm 129 */
																obj_t BgL_arg1804z00_4207;

																BgL_arg1804z00_4207 = (BgL_oclassz00_4204);
																BgL_odepthz00_4206 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4207);
															}
															if ((1L < BgL_odepthz00_4206))
																{	/* SawC/code.scm 129 */
																	obj_t BgL_arg1802z00_4209;

																	{	/* SawC/code.scm 129 */
																		obj_t BgL_arg1803z00_4210;

																		BgL_arg1803z00_4210 = (BgL_oclassz00_4204);
																		BgL_arg1802z00_4209 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4210, 1L);
																	}
																	BgL_res2610z00_4221 =
																		(BgL_arg1802z00_4209 == BgL_classz00_4188);
																}
															else
																{	/* SawC/code.scm 129 */
																	BgL_res2610z00_4221 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3078z00_6591 = BgL_res2610z00_4221;
										}
								}
							else
								{	/* SawC/code.scm 129 */
									BgL_test3078z00_6591 = ((bool_t) 0);
								}
						}
						if (BgL_test3078z00_6591)
							{	/* SawC/code.scm 129 */
								{	/* SawC/code.scm 129 */
									BgL_sawciregz00_bglt BgL_wide1185z00_2549;

									BgL_wide1185z00_2549 =
										((BgL_sawciregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_sawciregz00_bgl))));
									{	/* SawC/code.scm 129 */
										obj_t BgL_auxz00_6619;
										BgL_objectz00_bglt BgL_tmpz00_6615;

										BgL_auxz00_6619 = ((obj_t) BgL_wide1185z00_2549);
										BgL_tmpz00_6615 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_ez00_2543)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6615, BgL_auxz00_6619);
									}
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_ez00_2543)));
									{	/* SawC/code.scm 129 */
										long BgL_arg1904z00_2550;

										BgL_arg1904z00_2550 =
											BGL_CLASS_NUM(BGl_SawCIregz00zzsaw_c_codez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_ez00_2543))),
											BgL_arg1904z00_2550);
									}
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_ez00_2543)));
								}
								{
									BgL_sawciregz00_bglt BgL_auxz00_6633;

									{
										obj_t BgL_auxz00_6634;

										{	/* SawC/code.scm 129 */
											BgL_objectz00_bglt BgL_tmpz00_6635;

											BgL_tmpz00_6635 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_ez00_2543)));
											BgL_auxz00_6634 = BGL_OBJECT_WIDENING(BgL_tmpz00_6635);
										}
										BgL_auxz00_6633 = ((BgL_sawciregz00_bglt) BgL_auxz00_6634);
									}
									((((BgL_sawciregz00_bglt) COBJECT(BgL_auxz00_6633))->
											BgL_indexz00) =
										((obj_t) ((obj_t) ((obj_t) CELL_REF(BgL_nz00_5982)))),
										BUNSPEC);
								}
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_ez00_2543));
								{	/* SawC/code.scm 130 */
									obj_t BgL_auxz00_5984;

									BgL_auxz00_5984 =
										ADDFX(
										((obj_t) ((obj_t) CELL_REF(BgL_nz00_5982))), BINT(1L));
									CELL_SET(BgL_nz00_5982, BgL_auxz00_5984);
								}
								{	/* SawC/code.scm 131 */
									obj_t BgL_auxz00_5985;

									BgL_auxz00_5985 =
										MAKE_YOUNG_PAIR(BgL_ez00_2543,
										((obj_t) ((obj_t) CELL_REF(BgL_regsz00_5983))));
									return CELL_SET(BgL_regsz00_5983, BgL_auxz00_5985);
								}
							}
						else
							{	/* SawC/code.scm 129 */
								{	/* SawC/code.scm 132 */
									BgL_rtl_funz00_bglt BgL_arg1906z00_2552;

									BgL_arg1906z00_2552 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_ez00_2543)))->BgL_funz00);
									BGl_check_funze70ze7zzsaw_c_codez00(BgL_arg1906z00_2552);
								}
								{	/* SawC/code.scm 133 */
									obj_t BgL_l1623z00_2553;

									BgL_l1623z00_2553 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_ez00_2543)))->BgL_argsz00);
									if (NULLP(BgL_l1623z00_2553))
										{	/* SawC/code.scm 133 */
											return BNIL;
										}
									else
										{	/* SawC/code.scm 133 */
											obj_t BgL_head1625z00_2555;

											{	/* SawC/code.scm 133 */
												obj_t BgL_arg1913z00_2567;

												{	/* SawC/code.scm 133 */
													obj_t BgL_arg1914z00_2568;

													BgL_arg1914z00_2568 = CAR(BgL_l1623z00_2553);
													BgL_arg1913z00_2567 =
														BGl_exprzd2ze3iregze70zd6zzsaw_c_codez00
														(BgL_regsz00_5983, BgL_nz00_5982,
														BgL_arg1914z00_2568);
												}
												BgL_head1625z00_2555 =
													MAKE_YOUNG_PAIR(BgL_arg1913z00_2567, BNIL);
											}
											{	/* SawC/code.scm 133 */
												obj_t BgL_g1628z00_2556;

												BgL_g1628z00_2556 = CDR(BgL_l1623z00_2553);
												{
													obj_t BgL_l1623z00_2558;
													obj_t BgL_tail1626z00_2559;

													BgL_l1623z00_2558 = BgL_g1628z00_2556;
													BgL_tail1626z00_2559 = BgL_head1625z00_2555;
												BgL_zc3z04anonymousza31908ze3z87_2560:
													if (NULLP(BgL_l1623z00_2558))
														{	/* SawC/code.scm 133 */
															return BgL_head1625z00_2555;
														}
													else
														{	/* SawC/code.scm 133 */
															obj_t BgL_newtail1627z00_2562;

															{	/* SawC/code.scm 133 */
																obj_t BgL_arg1911z00_2564;

																{	/* SawC/code.scm 133 */
																	obj_t BgL_arg1912z00_2565;

																	BgL_arg1912z00_2565 =
																		CAR(((obj_t) BgL_l1623z00_2558));
																	BgL_arg1911z00_2564 =
																		BGl_exprzd2ze3iregze70zd6zzsaw_c_codez00
																		(BgL_regsz00_5983, BgL_nz00_5982,
																		BgL_arg1912z00_2565);
																}
																BgL_newtail1627z00_2562 =
																	MAKE_YOUNG_PAIR(BgL_arg1911z00_2564, BNIL);
															}
															SET_CDR(BgL_tail1626z00_2559,
																BgL_newtail1627z00_2562);
															{	/* SawC/code.scm 133 */
																obj_t BgL_arg1910z00_2563;

																BgL_arg1910z00_2563 =
																	CDR(((obj_t) BgL_l1623z00_2558));
																{
																	obj_t BgL_tail1626z00_6671;
																	obj_t BgL_l1623z00_6670;

																	BgL_l1623z00_6670 = BgL_arg1910z00_2563;
																	BgL_tail1626z00_6671 =
																		BgL_newtail1627z00_2562;
																	BgL_tail1626z00_2559 = BgL_tail1626z00_6671;
																	BgL_l1623z00_2558 = BgL_l1623z00_6670;
																	goto BgL_zc3z04anonymousza31908ze3z87_2560;
																}
															}
														}
												}
											}
										}
								}
							}
					}
			}
		}

	}



/* gen-type-regs */
	obj_t BGl_genzd2typezd2regsz00zzsaw_c_codez00(obj_t BgL_lz00_53)
	{
		{	/* SawC/code.scm 147 */
			{	/* SawC/code.scm 148 */
				obj_t BgL_arg1926z00_2594;

				{	/* SawC/code.scm 148 */
					obj_t BgL_tmpz00_6672;

					BgL_tmpz00_6672 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1926z00_2594 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6672);
				}
				bgl_display_string(BGl_string2684z00zzsaw_c_codez00,
					BgL_arg1926z00_2594);
			}
			if (NULLP(BgL_lz00_53))
				{	/* SawC/code.scm 149 */
					((bool_t) 0);
				}
			else
				{	/* SawC/code.scm 149 */
					BGl_genzd2typezd2regz00zzsaw_c_codez00(CAR(BgL_lz00_53));
					{	/* SawC/code.scm 152 */
						obj_t BgL_g1641z00_2597;

						BgL_g1641z00_2597 = CDR(BgL_lz00_53);
						{
							obj_t BgL_l1639z00_2599;

							BgL_l1639z00_2599 = BgL_g1641z00_2597;
						BgL_zc3z04anonymousza31929ze3z87_2600:
							if (PAIRP(BgL_l1639z00_2599))
								{	/* SawC/code.scm 153 */
									{	/* SawC/code.scm 152 */
										obj_t BgL_argz00_2602;

										BgL_argz00_2602 = CAR(BgL_l1639z00_2599);
										{	/* SawC/code.scm 152 */
											obj_t BgL_arg1931z00_2603;

											{	/* SawC/code.scm 152 */
												obj_t BgL_tmpz00_6684;

												BgL_tmpz00_6684 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg1931z00_2603 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6684);
											}
											bgl_display_string(BGl_string2685z00zzsaw_c_codez00,
												BgL_arg1931z00_2603);
										}
										BGl_genzd2typezd2regz00zzsaw_c_codez00(BgL_argz00_2602);
									}
									{
										obj_t BgL_l1639z00_6689;

										BgL_l1639z00_6689 = CDR(BgL_l1639z00_2599);
										BgL_l1639z00_2599 = BgL_l1639z00_6689;
										goto BgL_zc3z04anonymousza31929ze3z87_2600;
									}
								}
							else
								{	/* SawC/code.scm 153 */
									((bool_t) 1);
								}
						}
					}
				}
			{	/* SawC/code.scm 154 */
				obj_t BgL_arg1933z00_2606;

				{	/* SawC/code.scm 154 */
					obj_t BgL_tmpz00_6691;

					BgL_tmpz00_6691 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1933z00_2606 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6691);
				}
				return
					bgl_display_string(BGl_string2686z00zzsaw_c_codez00,
					BgL_arg1933z00_2606);
			}
		}

	}



/* gen-type-reg */
	obj_t BGl_genzd2typezd2regz00zzsaw_c_codez00(obj_t BgL_regz00_54)
	{
		{	/* SawC/code.scm 156 */
			{	/* SawC/code.scm 157 */
				obj_t BgL_arg1934z00_2607;
				obj_t BgL_arg1935z00_2608;

				{	/* SawC/code.scm 157 */
					BgL_typez00_bglt BgL_arg1936z00_2609;
					obj_t BgL_arg1937z00_2610;

					BgL_arg1936z00_2609 =
						(((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_regz00_54)))->BgL_typez00);
					BgL_arg1937z00_2610 = BGl_reg_namez00zzsaw_c_codez00(BgL_regz00_54);
					BgL_arg1934z00_2607 =
						BGl_makezd2typedzd2declarationz00zztype_toolsz00
						(BgL_arg1936z00_2609, BgL_arg1937z00_2610);
				}
				{	/* SawC/code.scm 157 */
					obj_t BgL_tmpz00_6699;

					BgL_tmpz00_6699 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1935z00_2608 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6699);
				}
				return bgl_display_obj(BgL_arg1934z00_2607, BgL_arg1935z00_2608);
			}
		}

	}



/* reg_name */
	obj_t BGl_reg_namez00zzsaw_c_codez00(obj_t BgL_regz00_55)
	{
		{	/* SawC/code.scm 159 */
			{	/* SawC/code.scm 160 */
				obj_t BgL__ortest_1188z00_2611;

				BgL__ortest_1188z00_2611 =
					(((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_regz00_55)))->BgL_debugnamez00);
				if (CBOOL(BgL__ortest_1188z00_2611))
					{	/* SawC/code.scm 160 */
						return BgL__ortest_1188z00_2611;
					}
				else
					{	/* SawC/code.scm 161 */
						obj_t BgL_arg1938z00_2612;
						obj_t BgL_arg1939z00_2613;

						if (CBOOL(
								(((BgL_rtl_regz00_bglt) COBJECT(
											((BgL_rtl_regz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_regz00_55))))->BgL_varz00)))
							{	/* SawC/code.scm 161 */
								BgL_arg1938z00_2612 = BGl_string2687z00zzsaw_c_codez00;
							}
						else
							{	/* SawC/code.scm 161 */
								BgL_arg1938z00_2612 = BGl_string2688z00zzsaw_c_codez00;
							}
						{	/* SawC/code.scm 162 */
							obj_t BgL_arg1941z00_2615;

							{
								BgL_sawciregz00_bglt BgL_auxz00_6712;

								{
									obj_t BgL_auxz00_6713;

									{	/* SawC/code.scm 162 */
										BgL_objectz00_bglt BgL_tmpz00_6714;

										BgL_tmpz00_6714 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_regz00_55));
										BgL_auxz00_6713 = BGL_OBJECT_WIDENING(BgL_tmpz00_6714);
									}
									BgL_auxz00_6712 = ((BgL_sawciregz00_bglt) BgL_auxz00_6713);
								}
								BgL_arg1941z00_2615 =
									(((BgL_sawciregz00_bglt) COBJECT(BgL_auxz00_6712))->
									BgL_indexz00);
							}
							{	/* SawC/code.scm 162 */

								BgL_arg1939z00_2613 =
									BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
									(long) CINT(BgL_arg1941z00_2615), 10L);
						}}
						return string_append(BgL_arg1938z00_2612, BgL_arg1939z00_2613);
					}
			}
		}

	}



/* declare-regs */
	bool_t BGl_declarezd2regszd2zzsaw_c_codez00(obj_t BgL_lz00_56)
	{
		{	/* SawC/code.scm 164 */
			{
				obj_t BgL_l1642z00_2619;

				BgL_l1642z00_2619 = BgL_lz00_56;
			BgL_zc3z04anonymousza31942ze3z87_2620:
				if (PAIRP(BgL_l1642z00_2619))
					{	/* SawC/code.scm 165 */
						{	/* SawC/code.scm 166 */
							obj_t BgL_rz00_2622;

							BgL_rz00_2622 = CAR(BgL_l1642z00_2619);
							{	/* SawC/code.scm 166 */
								obj_t BgL_arg1944z00_2623;

								{	/* SawC/code.scm 166 */
									obj_t BgL_tmpz00_6726;

									BgL_tmpz00_6726 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1944z00_2623 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6726);
								}
								bgl_display_string(BGl_string2678z00zzsaw_c_codez00,
									BgL_arg1944z00_2623);
							}
							BGl_genzd2typezd2regz00zzsaw_c_codez00(BgL_rz00_2622);
							{	/* SawC/code.scm 172 */
								obj_t BgL_arg1952z00_2633;

								{	/* SawC/code.scm 172 */
									obj_t BgL_tmpz00_6731;

									BgL_tmpz00_6731 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1952z00_2633 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6731);
								}
								bgl_display_string(BGl_string2689z00zzsaw_c_codez00,
									BgL_arg1952z00_2633);
							}
						}
						{
							obj_t BgL_l1642z00_6735;

							BgL_l1642z00_6735 = CDR(BgL_l1642z00_2619);
							BgL_l1642z00_2619 = BgL_l1642z00_6735;
							goto BgL_zc3z04anonymousza31942ze3z87_2620;
						}
					}
				else
					{	/* SawC/code.scm 165 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* genbody */
	bool_t BGl_genbodyz00zzsaw_c_codez00(obj_t BgL_lz00_57, obj_t BgL_funidz00_58)
	{
		{	/* SawC/code.scm 175 */
			{
				obj_t BgL_l1647z00_2637;

				BgL_l1647z00_2637 = BgL_lz00_57;
			BgL_zc3z04anonymousza31954ze3z87_2638:
				if (PAIRP(BgL_l1647z00_2637))
					{	/* SawC/code.scm 176 */
						{	/* SawC/code.scm 177 */
							obj_t BgL_bz00_2640;

							BgL_bz00_2640 = CAR(BgL_l1647z00_2637);
							{	/* SawC/code.scm 177 */
								int BgL_arg1956z00_2641;

								BgL_arg1956z00_2641 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_bz00_2640)))->BgL_labelz00);
								BGl_outzd2labelzd2zzsaw_c_codez00(BgL_arg1956z00_2641,
									BgL_funidz00_58);
							}
							{	/* SawC/code.scm 178 */
								obj_t BgL_lz00_2642;

								BgL_lz00_2642 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_bz00_2640)))->BgL_firstz00);
								{	/* SawC/code.scm 179 */
									bool_t BgL_test3091z00_6745;

									{	/* SawC/code.scm 179 */
										obj_t BgL_arg1960z00_2647;

										{	/* SawC/code.scm 179 */
											obj_t BgL_arg1961z00_2648;

											BgL_arg1961z00_2648 = CAR(((obj_t) BgL_lz00_2642));
											BgL_arg1960z00_2647 =
												BGl_findzd2locationzd2zzsaw_c_codez00(
												((BgL_rtl_insz00_bglt) BgL_arg1961z00_2648));
										}
										if (STRUCTP(BgL_arg1960z00_2647))
											{	/* SawC/code.scm 179 */
												BgL_test3091z00_6745 =
													(STRUCT_KEY(BgL_arg1960z00_2647) ==
													CNST_TABLE_REF(3));
											}
										else
											{	/* SawC/code.scm 179 */
												BgL_test3091z00_6745 = ((bool_t) 0);
											}
									}
									if (BgL_test3091z00_6745)
										{	/* SawC/code.scm 179 */
											obj_t BgL_port1644z00_2646;

											{	/* SawC/code.scm 179 */
												obj_t BgL_tmpz00_6755;

												BgL_tmpz00_6755 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1644z00_2646 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6755);
											}
											bgl_display_string(BGl_string2690z00zzsaw_c_codez00,
												BgL_port1644z00_2646);
											bgl_display_char(((unsigned char) 10),
												BgL_port1644z00_2646);
										}
									else
										{	/* SawC/code.scm 179 */
											BFALSE;
										}
								}
								{
									obj_t BgL_l1645z00_2650;

									BgL_l1645z00_2650 = BgL_lz00_2642;
								BgL_zc3z04anonymousza31962ze3z87_2651:
									if (PAIRP(BgL_l1645z00_2650))
										{	/* SawC/code.scm 180 */
											{	/* SawC/code.scm 180 */
												obj_t BgL_arg1964z00_2653;

												BgL_arg1964z00_2653 = CAR(BgL_l1645z00_2650);
												BGl_genzd2inszd2zzsaw_c_codez00(
													((BgL_rtl_insz00_bglt) BgL_arg1964z00_2653));
											}
											{
												obj_t BgL_l1645z00_6765;

												BgL_l1645z00_6765 = CDR(BgL_l1645z00_2650);
												BgL_l1645z00_2650 = BgL_l1645z00_6765;
												goto BgL_zc3z04anonymousza31962ze3z87_2651;
											}
										}
									else
										{	/* SawC/code.scm 180 */
											((bool_t) 1);
										}
								}
							}
						}
						{
							obj_t BgL_l1647z00_6767;

							BgL_l1647z00_6767 = CDR(BgL_l1647z00_2637);
							BgL_l1647z00_2637 = BgL_l1647z00_6767;
							goto BgL_zc3z04anonymousza31954ze3z87_2638;
						}
					}
				else
					{	/* SawC/code.scm 176 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* out-label */
	bool_t BGl_outzd2labelzd2zzsaw_c_codez00(int BgL_labelz00_59,
		obj_t BgL_funidz00_60)
	{
		{	/* SawC/code.scm 183 */
			{	/* SawC/code.scm 184 */
				obj_t BgL_list1967z00_2658;

				{	/* SawC/code.scm 184 */
					obj_t BgL_arg1968z00_2659;

					{	/* SawC/code.scm 184 */
						obj_t BgL_arg1969z00_2660;

						BgL_arg1969z00_2660 =
							MAKE_YOUNG_PAIR(BGl_string2691z00zzsaw_c_codez00, BNIL);
						BgL_arg1968z00_2659 =
							MAKE_YOUNG_PAIR(BINT(BgL_labelz00_59), BgL_arg1969z00_2660);
					}
					BgL_list1967z00_2658 =
						MAKE_YOUNG_PAIR(BGl_string2692z00zzsaw_c_codez00,
						BgL_arg1968z00_2659);
				}
				BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1967z00_2658);
			}
			if (BGl_za2countza2z00zzsaw_c_codez00)
				{	/* SawC/code.scm 185 */
					{	/* SawC/code.scm 186 */
						obj_t BgL_list1970z00_2661;

						{	/* SawC/code.scm 186 */
							obj_t BgL_arg1971z00_2662;

							{	/* SawC/code.scm 186 */
								obj_t BgL_arg1972z00_2663;

								BgL_arg1972z00_2663 =
									MAKE_YOUNG_PAIR(BGl_string2693z00zzsaw_c_codez00, BNIL);
								BgL_arg1971z00_2662 =
									MAKE_YOUNG_PAIR(BGl_za2counterza2z00zzsaw_c_codez00,
									BgL_arg1972z00_2663);
							}
							BgL_list1970z00_2661 =
								MAKE_YOUNG_PAIR(BGl_string2694z00zzsaw_c_codez00,
								BgL_arg1971z00_2662);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1970z00_2661);
					}
					{	/* SawC/code.scm 187 */
						obj_t BgL_list1973z00_2664;

						{	/* SawC/code.scm 187 */
							obj_t BgL_arg1974z00_2665;

							{	/* SawC/code.scm 187 */
								obj_t BgL_arg1975z00_2666;

								{	/* SawC/code.scm 187 */
									obj_t BgL_arg1976z00_2667;

									{	/* SawC/code.scm 187 */
										obj_t BgL_arg1977z00_2668;

										BgL_arg1977z00_2668 =
											MAKE_YOUNG_PAIR(BGl_string2695z00zzsaw_c_codez00, BNIL);
										BgL_arg1976z00_2667 =
											MAKE_YOUNG_PAIR(BINT(BgL_labelz00_59),
											BgL_arg1977z00_2668);
									}
									BgL_arg1975z00_2666 =
										MAKE_YOUNG_PAIR(BGl_string2696z00zzsaw_c_codez00,
										BgL_arg1976z00_2667);
								}
								BgL_arg1974z00_2665 =
									MAKE_YOUNG_PAIR(BGl_za2counterza2z00zzsaw_c_codez00,
									BgL_arg1975z00_2666);
							}
							BgL_list1973z00_2664 =
								MAKE_YOUNG_PAIR(BGl_string2697z00zzsaw_c_codez00,
								BgL_arg1974z00_2665);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1973z00_2664);
					}
					{	/* SawC/code.scm 188 */
						obj_t BgL_list1978z00_2669;

						{	/* SawC/code.scm 188 */
							obj_t BgL_arg1979z00_2670;

							{	/* SawC/code.scm 188 */
								obj_t BgL_arg1980z00_2671;

								{	/* SawC/code.scm 188 */
									obj_t BgL_arg1981z00_2672;

									{	/* SawC/code.scm 188 */
										obj_t BgL_arg1982z00_2673;

										BgL_arg1982z00_2673 =
											MAKE_YOUNG_PAIR(BGl_string2695z00zzsaw_c_codez00, BNIL);
										BgL_arg1981z00_2672 =
											MAKE_YOUNG_PAIR(BgL_funidz00_60, BgL_arg1982z00_2673);
									}
									BgL_arg1980z00_2671 =
										MAKE_YOUNG_PAIR(BGl_string2696z00zzsaw_c_codez00,
										BgL_arg1981z00_2672);
								}
								BgL_arg1979z00_2670 =
									MAKE_YOUNG_PAIR(BGl_za2counterza2z00zzsaw_c_codez00,
									BgL_arg1980z00_2671);
							}
							BgL_list1978z00_2669 =
								MAKE_YOUNG_PAIR(BGl_string2698z00zzsaw_c_codez00,
								BgL_arg1979z00_2670);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list1978z00_2669);
					}
					if (INTEGERP(BGl_za2counterza2z00zzsaw_c_codez00))
						{	/* SawC/code.scm 189 */
							obj_t BgL_za72za7_4288;

							BgL_za72za7_4288 = BGl_za2counterza2z00zzsaw_c_codez00;
							{	/* SawC/code.scm 189 */
								obj_t BgL_tmpz00_4289;

								BgL_tmpz00_4289 = BINT(0L);
								{	/* SawC/code.scm 189 */
									bool_t BgL_test3096z00_6795;

									{	/* SawC/code.scm 189 */
										obj_t BgL_tmpz00_6796;

										BgL_tmpz00_6796 = BINT(1L);
										BgL_test3096z00_6795 =
											BGL_ADDFX_OV(BgL_tmpz00_6796, BgL_za72za7_4288,
											BgL_tmpz00_4289);
									}
									if (BgL_test3096z00_6795)
										{	/* SawC/code.scm 189 */
											BGl_za2counterza2z00zzsaw_c_codez00 =
												bgl_bignum_add(CNST_TABLE_REF(4),
												bgl_long_to_bignum((long) CINT(BgL_za72za7_4288)));
										}
									else
										{	/* SawC/code.scm 189 */
											BGl_za2counterza2z00zzsaw_c_codez00 = BgL_tmpz00_4289;
										}
								}
							}
						}
					else
						{	/* SawC/code.scm 189 */
							BGl_za2counterza2z00zzsaw_c_codez00 =
								BGl_2zb2zb2zz__r4_numbers_6_5z00(BINT(1L),
								BGl_za2counterza2z00zzsaw_c_codez00);
						}
				}
			else
				{	/* SawC/code.scm 185 */
					BFALSE;
				}
			return ((bool_t) 0);
		}

	}



/* print-location? */
	bool_t BGl_printzd2locationzf3z21zzsaw_c_codez00(obj_t BgL_locz00_61)
	{
		{	/* SawC/code.scm 192 */
			{	/* SawC/code.scm 193 */
				bool_t BgL_test3097z00_6805;

				if (STRUCTP(BgL_locz00_61))
					{	/* SawC/code.scm 193 */
						BgL_test3097z00_6805 =
							(STRUCT_KEY(BgL_locz00_61) == CNST_TABLE_REF(3));
					}
				else
					{	/* SawC/code.scm 193 */
						BgL_test3097z00_6805 = ((bool_t) 0);
					}
				if (BgL_test3097z00_6805)
					{	/* SawC/code.scm 193 */
						{	/* SawC/code.scm 195 */
							obj_t BgL_port1649z00_2678;

							{	/* SawC/code.scm 195 */
								obj_t BgL_tmpz00_6811;

								BgL_tmpz00_6811 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1649z00_2678 =
									BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6811);
							}
							bgl_display_string(BGl_string2699z00zzsaw_c_codez00,
								BgL_port1649z00_2678);
							bgl_display_obj(STRUCT_REF(BgL_locz00_61, (int) (2L)),
								BgL_port1649z00_2678);
							bgl_display_string(BGl_string2700z00zzsaw_c_codez00,
								BgL_port1649z00_2678);
							bgl_display_obj(STRUCT_REF(BgL_locz00_61, (int) (0L)),
								BgL_port1649z00_2678);
							bgl_display_string(BGl_string2701z00zzsaw_c_codez00,
								BgL_port1649z00_2678);
							bgl_display_char(((unsigned char) 10), BgL_port1649z00_2678);
						}
						return ((bool_t) 1);
					}
				else
					{	/* SawC/code.scm 193 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* find-location */
	obj_t BGl_findzd2locationzd2zzsaw_c_codez00(BgL_rtl_insz00_bglt BgL_insz00_62)
	{
		{	/* SawC/code.scm 199 */
			{	/* SawC/code.scm 211 */
				obj_t BgL__andtest_1192z00_2683;

				BgL__andtest_1192z00_2683 =
					BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00;
				if (CBOOL(BgL__andtest_1192z00_2683))
					{	/* SawC/code.scm 212 */
						obj_t BgL__andtest_1193z00_2684;

						BgL__andtest_1193z00_2684 =
							BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00;
						if (CBOOL(BgL__andtest_1193z00_2684))
							{	/* SawC/code.scm 212 */
								return BFALSE;
							}
						else
							{	/* SawC/code.scm 212 */
								return BGl_walkze70ze7zzsaw_c_codez00(((obj_t) BgL_insz00_62));
							}
					}
				else
					{	/* SawC/code.scm 211 */
						return BFALSE;
					}
			}
		}

	}



/* walk~0 */
	obj_t BGl_walkze70ze7zzsaw_c_codez00(obj_t BgL_insz00_2691)
	{
		{	/* SawC/code.scm 210 */
			{
				obj_t BgL_lz00_2685;

				{	/* SawC/code.scm 205 */
					bool_t BgL_test3101z00_6830;

					{	/* SawC/code.scm 205 */
						obj_t BgL_classz00_4313;

						BgL_classz00_4313 = BGl_rtl_insz00zzsaw_defsz00;
						if (BGL_OBJECTP(BgL_insz00_2691))
							{	/* SawC/code.scm 205 */
								BgL_objectz00_bglt BgL_arg1807z00_4315;

								BgL_arg1807z00_4315 = (BgL_objectz00_bglt) (BgL_insz00_2691);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawC/code.scm 205 */
										long BgL_idxz00_4321;

										BgL_idxz00_4321 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4315);
										BgL_test3101z00_6830 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4321 + 1L)) == BgL_classz00_4313);
									}
								else
									{	/* SawC/code.scm 205 */
										bool_t BgL_res2613z00_4346;

										{	/* SawC/code.scm 205 */
											obj_t BgL_oclassz00_4329;

											{	/* SawC/code.scm 205 */
												obj_t BgL_arg1815z00_4337;
												long BgL_arg1816z00_4338;

												BgL_arg1815z00_4337 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawC/code.scm 205 */
													long BgL_arg1817z00_4339;

													BgL_arg1817z00_4339 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4315);
													BgL_arg1816z00_4338 =
														(BgL_arg1817z00_4339 - OBJECT_TYPE);
												}
												BgL_oclassz00_4329 =
													VECTOR_REF(BgL_arg1815z00_4337, BgL_arg1816z00_4338);
											}
											{	/* SawC/code.scm 205 */
												bool_t BgL__ortest_1115z00_4330;

												BgL__ortest_1115z00_4330 =
													(BgL_classz00_4313 == BgL_oclassz00_4329);
												if (BgL__ortest_1115z00_4330)
													{	/* SawC/code.scm 205 */
														BgL_res2613z00_4346 = BgL__ortest_1115z00_4330;
													}
												else
													{	/* SawC/code.scm 205 */
														long BgL_odepthz00_4331;

														{	/* SawC/code.scm 205 */
															obj_t BgL_arg1804z00_4332;

															BgL_arg1804z00_4332 = (BgL_oclassz00_4329);
															BgL_odepthz00_4331 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4332);
														}
														if ((1L < BgL_odepthz00_4331))
															{	/* SawC/code.scm 205 */
																obj_t BgL_arg1802z00_4334;

																{	/* SawC/code.scm 205 */
																	obj_t BgL_arg1803z00_4335;

																	BgL_arg1803z00_4335 = (BgL_oclassz00_4329);
																	BgL_arg1802z00_4334 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4335,
																		1L);
																}
																BgL_res2613z00_4346 =
																	(BgL_arg1802z00_4334 == BgL_classz00_4313);
															}
														else
															{	/* SawC/code.scm 205 */
																BgL_res2613z00_4346 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3101z00_6830 = BgL_res2613z00_4346;
									}
							}
						else
							{	/* SawC/code.scm 205 */
								BgL_test3101z00_6830 = ((bool_t) 0);
							}
					}
					if (BgL_test3101z00_6830)
						{	/* SawC/code.scm 207 */
							obj_t BgL_locz00_2694;

							BgL_locz00_2694 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_2691)))->BgL_locz00);
							{	/* SawC/code.scm 208 */
								bool_t BgL_test3106z00_6855;

								if (STRUCTP(BgL_locz00_2694))
									{	/* SawC/code.scm 208 */
										BgL_test3106z00_6855 =
											(STRUCT_KEY(BgL_locz00_2694) == CNST_TABLE_REF(3));
									}
								else
									{	/* SawC/code.scm 208 */
										BgL_test3106z00_6855 = ((bool_t) 0);
									}
								if (BgL_test3106z00_6855)
									{	/* SawC/code.scm 208 */
										return BgL_locz00_2694;
									}
								else
									{	/* SawC/code.scm 210 */
										obj_t BgL_arg1995z00_2696;

										BgL_arg1995z00_2696 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_2691)))->
											BgL_argsz00);
										BgL_lz00_2685 = BgL_arg1995z00_2696;
									BgL_zc3z04anonymousza31989ze3z87_2686:
										if (NULLP(BgL_lz00_2685))
											{	/* SawC/code.scm 201 */
												return BFALSE;
											}
										else
											{	/* SawC/code.scm 202 */
												obj_t BgL__ortest_1191z00_2688;

												{	/* SawC/code.scm 202 */
													obj_t BgL_arg1991z00_2690;

													BgL_arg1991z00_2690 = CAR(((obj_t) BgL_lz00_2685));
													BgL__ortest_1191z00_2688 =
														BGl_walkze70ze7zzsaw_c_codez00(BgL_arg1991z00_2690);
												}
												if (CBOOL(BgL__ortest_1191z00_2688))
													{	/* SawC/code.scm 202 */
														return BgL__ortest_1191z00_2688;
													}
												else
													{	/* SawC/code.scm 203 */
														obj_t BgL_arg1990z00_2689;

														BgL_arg1990z00_2689 = CDR(((obj_t) BgL_lz00_2685));
														{
															obj_t BgL_lz00_6872;

															BgL_lz00_6872 = BgL_arg1990z00_2689;
															BgL_lz00_2685 = BgL_lz00_6872;
															goto BgL_zc3z04anonymousza31989ze3z87_2686;
														}
													}
											}
									}
							}
						}
					else
						{	/* SawC/code.scm 205 */
							return BFALSE;
						}
				}
			}
		}

	}



/* gen-ins */
	obj_t BGl_genzd2inszd2zzsaw_c_codez00(BgL_rtl_insz00_bglt BgL_insz00_63)
	{
		{	/* SawC/code.scm 218 */
			BGl_printzd2locationzf3z21zzsaw_c_codez00
				(BGl_findzd2locationzd2zzsaw_c_codez00(BgL_insz00_63));
			{	/* SawC/code.scm 221 */
				obj_t BgL_arg1997z00_2701;

				{	/* SawC/code.scm 221 */
					obj_t BgL_tmpz00_6875;

					BgL_tmpz00_6875 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1997z00_2701 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6875);
				}
				bgl_display_string(BGl_string2702z00zzsaw_c_codez00,
					BgL_arg1997z00_2701);
			}
			if (CBOOL((((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_63))->BgL_destz00)))
				{	/* SawC/code.scm 222 */
					BGl_genzd2regzf2destz20zzsaw_c_codez00(
						(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_63))->BgL_destz00));
					{	/* SawC/code.scm 224 */
						obj_t BgL_arg2000z00_2704;

						{	/* SawC/code.scm 224 */
							obj_t BgL_tmpz00_6884;

							BgL_tmpz00_6884 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2000z00_2704 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6884);
						}
						bgl_display_string(BGl_string2703z00zzsaw_c_codez00,
							BgL_arg2000z00_2704);
					}
				}
			else
				{	/* SawC/code.scm 222 */
					BFALSE;
				}
			BGl_genzd2exprzd2zzsaw_c_codez00(
				(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_63))->BgL_funz00),
				(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_63))->BgL_argsz00));
			{	/* SawC/code.scm 226 */
				obj_t BgL_arg2003z00_2707;

				{	/* SawC/code.scm 226 */
					obj_t BgL_tmpz00_6891;

					BgL_tmpz00_6891 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2003z00_2707 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6891);
				}
				return
					bgl_display_string(BGl_string2689z00zzsaw_c_codez00,
					BgL_arg2003z00_2707);
			}
		}

	}



/* gen-args */
	bool_t BGl_genzd2argszd2zzsaw_c_codez00(obj_t BgL_argsz00_66)
	{
		{	/* SawC/code.scm 236 */
			if (NULLP(BgL_argsz00_66))
				{	/* SawC/code.scm 237 */
					return ((bool_t) 0);
				}
			else
				{	/* SawC/code.scm 237 */
					{	/* SawC/code.scm 238 */
						obj_t BgL_arg2006z00_2709;

						BgL_arg2006z00_2709 = CAR(((obj_t) BgL_argsz00_66));
						BGl_genzd2regzd2zzsaw_c_codez00(BgL_arg2006z00_2709);
					}
					{	/* SawC/code.scm 239 */
						obj_t BgL_g1652z00_2710;

						BgL_g1652z00_2710 = CDR(((obj_t) BgL_argsz00_66));
						{
							obj_t BgL_l1650z00_2712;

							BgL_l1650z00_2712 = BgL_g1652z00_2710;
						BgL_zc3z04anonymousza32007ze3z87_2713:
							if (PAIRP(BgL_l1650z00_2712))
								{	/* SawC/code.scm 240 */
									{	/* SawC/code.scm 239 */
										obj_t BgL_rz00_2715;

										BgL_rz00_2715 = CAR(BgL_l1650z00_2712);
										{	/* SawC/code.scm 239 */
											obj_t BgL_arg2009z00_2716;

											{	/* SawC/code.scm 239 */
												obj_t BgL_tmpz00_6905;

												BgL_tmpz00_6905 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg2009z00_2716 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6905);
											}
											bgl_display_string(BGl_string2685z00zzsaw_c_codez00,
												BgL_arg2009z00_2716);
										}
										BGl_genzd2regzd2zzsaw_c_codez00(BgL_rz00_2715);
									}
									{
										obj_t BgL_l1650z00_6910;

										BgL_l1650z00_6910 = CDR(BgL_l1650z00_2712);
										BgL_l1650z00_2712 = BgL_l1650z00_6910;
										goto BgL_zc3z04anonymousza32007ze3z87_2713;
									}
								}
							else
								{	/* SawC/code.scm 240 */
									return ((bool_t) 1);
								}
						}
					}
				}
		}

	}



/* gen-reg */
	obj_t BGl_genzd2regzd2zzsaw_c_codez00(obj_t BgL_regz00_67)
	{
		{	/* SawC/code.scm 242 */
			{	/* SawC/code.scm 243 */
				bool_t BgL_test3113z00_6912;

				{	/* SawC/code.scm 243 */
					obj_t BgL_classz00_4369;

					BgL_classz00_4369 = BGl_SawCIregz00zzsaw_c_codez00;
					if (BGL_OBJECTP(BgL_regz00_67))
						{	/* SawC/code.scm 243 */
							obj_t BgL_oclassz00_4371;

							{	/* SawC/code.scm 243 */
								obj_t BgL_arg1815z00_4373;
								long BgL_arg1816z00_4374;

								BgL_arg1815z00_4373 = (BGl_za2classesza2z00zz__objectz00);
								{	/* SawC/code.scm 243 */
									long BgL_arg1817z00_4375;

									BgL_arg1817z00_4375 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_regz00_67));
									BgL_arg1816z00_4374 = (BgL_arg1817z00_4375 - OBJECT_TYPE);
								}
								BgL_oclassz00_4371 =
									VECTOR_REF(BgL_arg1815z00_4373, BgL_arg1816z00_4374);
							}
							BgL_test3113z00_6912 = (BgL_oclassz00_4371 == BgL_classz00_4369);
						}
					else
						{	/* SawC/code.scm 243 */
							BgL_test3113z00_6912 = ((bool_t) 0);
						}
				}
				if (BgL_test3113z00_6912)
					{	/* SawC/code.scm 243 */
						BGL_TAIL return
							BGl_genzd2regzf2destz20zzsaw_c_codez00(BgL_regz00_67);
					}
				else
					{	/* SawC/code.scm 245 */
						BgL_rtl_funz00_bglt BgL_arg2012z00_2720;
						obj_t BgL_arg2013z00_2721;

						BgL_arg2012z00_2720 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_regz00_67)))->BgL_funz00);
						BgL_arg2013z00_2721 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_regz00_67)))->BgL_argsz00);
						return
							BGl_genzd2exprzd2zzsaw_c_codez00(BgL_arg2012z00_2720,
							BgL_arg2013z00_2721);
					}
			}
		}

	}



/* gen-reg/dest */
	obj_t BGl_genzd2regzf2destz20zzsaw_c_codez00(obj_t BgL_regz00_68)
	{
		{	/* SawC/code.scm 247 */
			if (CBOOL(
					(((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_regz00_68)))->BgL_debugnamez00)))
				{	/* SawC/code.scm 249 */
					obj_t BgL_arg2015z00_2723;
					obj_t BgL_arg2016z00_2724;

					BgL_arg2015z00_2723 =
						(((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_regz00_68)))->BgL_debugnamez00);
					{	/* SawC/code.scm 249 */
						obj_t BgL_tmpz00_6933;

						BgL_tmpz00_6933 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg2016z00_2724 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_6933);
					}
					return bgl_display_obj(BgL_arg2015z00_2723, BgL_arg2016z00_2724);
				}
			else
				{	/* SawC/code.scm 250 */
					obj_t BgL_arg2017z00_2725;
					obj_t BgL_arg2018z00_2726;

					if (CBOOL(
							(((BgL_rtl_regz00_bglt) COBJECT(
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_regz00_68))))->BgL_varz00)))
						{	/* SawC/code.scm 250 */
							BgL_arg2017z00_2725 = BGl_string2687z00zzsaw_c_codez00;
						}
					else
						{	/* SawC/code.scm 250 */
							BgL_arg2017z00_2725 = BGl_string2688z00zzsaw_c_codez00;
						}
					{
						BgL_sawciregz00_bglt BgL_auxz00_6942;

						{
							obj_t BgL_auxz00_6943;

							{	/* SawC/code.scm 250 */
								BgL_objectz00_bglt BgL_tmpz00_6944;

								BgL_tmpz00_6944 =
									((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_regz00_68));
								BgL_auxz00_6943 = BGL_OBJECT_WIDENING(BgL_tmpz00_6944);
							}
							BgL_auxz00_6942 = ((BgL_sawciregz00_bglt) BgL_auxz00_6943);
						}
						BgL_arg2018z00_2726 =
							(((BgL_sawciregz00_bglt) COBJECT(BgL_auxz00_6942))->BgL_indexz00);
					}
					{	/* SawC/code.scm 250 */
						obj_t BgL_list2019z00_2727;

						{	/* SawC/code.scm 250 */
							obj_t BgL_arg2020z00_2728;

							BgL_arg2020z00_2728 = MAKE_YOUNG_PAIR(BgL_arg2018z00_2726, BNIL);
							BgL_list2019z00_2727 =
								MAKE_YOUNG_PAIR(BgL_arg2017z00_2725, BgL_arg2020z00_2728);
						}
						BGL_TAIL return
							BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2019z00_2727);
					}
				}
		}

	}



/* typename */
	obj_t BGl_typenamez00zzsaw_c_codez00(obj_t BgL_oz00_77)
	{
		{	/* SawC/code.scm 293 */
		BGl_typenamez00zzsaw_c_codez00:
			{	/* SawC/code.scm 295 */
				bool_t BgL_test3117z00_6953;

				{	/* SawC/code.scm 295 */
					obj_t BgL_classz00_4389;

					BgL_classz00_4389 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_oz00_77))
						{	/* SawC/code.scm 295 */
							BgL_objectz00_bglt BgL_arg1807z00_4391;

							BgL_arg1807z00_4391 = (BgL_objectz00_bglt) (BgL_oz00_77);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawC/code.scm 295 */
									long BgL_idxz00_4397;

									BgL_idxz00_4397 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4391);
									BgL_test3117z00_6953 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4397 + 1L)) == BgL_classz00_4389);
								}
							else
								{	/* SawC/code.scm 295 */
									bool_t BgL_res2615z00_4422;

									{	/* SawC/code.scm 295 */
										obj_t BgL_oclassz00_4405;

										{	/* SawC/code.scm 295 */
											obj_t BgL_arg1815z00_4413;
											long BgL_arg1816z00_4414;

											BgL_arg1815z00_4413 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawC/code.scm 295 */
												long BgL_arg1817z00_4415;

												BgL_arg1817z00_4415 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4391);
												BgL_arg1816z00_4414 =
													(BgL_arg1817z00_4415 - OBJECT_TYPE);
											}
											BgL_oclassz00_4405 =
												VECTOR_REF(BgL_arg1815z00_4413, BgL_arg1816z00_4414);
										}
										{	/* SawC/code.scm 295 */
											bool_t BgL__ortest_1115z00_4406;

											BgL__ortest_1115z00_4406 =
												(BgL_classz00_4389 == BgL_oclassz00_4405);
											if (BgL__ortest_1115z00_4406)
												{	/* SawC/code.scm 295 */
													BgL_res2615z00_4422 = BgL__ortest_1115z00_4406;
												}
											else
												{	/* SawC/code.scm 295 */
													long BgL_odepthz00_4407;

													{	/* SawC/code.scm 295 */
														obj_t BgL_arg1804z00_4408;

														BgL_arg1804z00_4408 = (BgL_oclassz00_4405);
														BgL_odepthz00_4407 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4408);
													}
													if ((1L < BgL_odepthz00_4407))
														{	/* SawC/code.scm 295 */
															obj_t BgL_arg1802z00_4410;

															{	/* SawC/code.scm 295 */
																obj_t BgL_arg1803z00_4411;

																BgL_arg1803z00_4411 = (BgL_oclassz00_4405);
																BgL_arg1802z00_4410 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4411,
																	1L);
															}
															BgL_res2615z00_4422 =
																(BgL_arg1802z00_4410 == BgL_classz00_4389);
														}
													else
														{	/* SawC/code.scm 295 */
															BgL_res2615z00_4422 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3117z00_6953 = BgL_res2615z00_4422;
								}
						}
					else
						{	/* SawC/code.scm 295 */
							BgL_test3117z00_6953 = ((bool_t) 0);
						}
				}
				if (BgL_test3117z00_6953)
					{	/* SawC/code.scm 295 */
						return
							(((BgL_typez00_bglt) COBJECT(
									(((BgL_rtl_regz00_bglt) COBJECT(
												((BgL_rtl_regz00_bglt) BgL_oz00_77)))->BgL_typez00)))->
							BgL_namez00);
					}
				else
					{	/* SawC/code.scm 297 */
						bool_t BgL_test3122z00_6979;

						{	/* SawC/code.scm 297 */
							obj_t BgL_classz00_4425;

							BgL_classz00_4425 = BGl_rtl_insz00zzsaw_defsz00;
							if (BGL_OBJECTP(BgL_oz00_77))
								{	/* SawC/code.scm 297 */
									BgL_objectz00_bglt BgL_arg1807z00_4427;

									BgL_arg1807z00_4427 = (BgL_objectz00_bglt) (BgL_oz00_77);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawC/code.scm 297 */
											long BgL_idxz00_4433;

											BgL_idxz00_4433 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4427);
											BgL_test3122z00_6979 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4433 + 1L)) == BgL_classz00_4425);
										}
									else
										{	/* SawC/code.scm 297 */
											bool_t BgL_res2616z00_4458;

											{	/* SawC/code.scm 297 */
												obj_t BgL_oclassz00_4441;

												{	/* SawC/code.scm 297 */
													obj_t BgL_arg1815z00_4449;
													long BgL_arg1816z00_4450;

													BgL_arg1815z00_4449 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawC/code.scm 297 */
														long BgL_arg1817z00_4451;

														BgL_arg1817z00_4451 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4427);
														BgL_arg1816z00_4450 =
															(BgL_arg1817z00_4451 - OBJECT_TYPE);
													}
													BgL_oclassz00_4441 =
														VECTOR_REF(BgL_arg1815z00_4449,
														BgL_arg1816z00_4450);
												}
												{	/* SawC/code.scm 297 */
													bool_t BgL__ortest_1115z00_4442;

													BgL__ortest_1115z00_4442 =
														(BgL_classz00_4425 == BgL_oclassz00_4441);
													if (BgL__ortest_1115z00_4442)
														{	/* SawC/code.scm 297 */
															BgL_res2616z00_4458 = BgL__ortest_1115z00_4442;
														}
													else
														{	/* SawC/code.scm 297 */
															long BgL_odepthz00_4443;

															{	/* SawC/code.scm 297 */
																obj_t BgL_arg1804z00_4444;

																BgL_arg1804z00_4444 = (BgL_oclassz00_4441);
																BgL_odepthz00_4443 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4444);
															}
															if ((1L < BgL_odepthz00_4443))
																{	/* SawC/code.scm 297 */
																	obj_t BgL_arg1802z00_4446;

																	{	/* SawC/code.scm 297 */
																		obj_t BgL_arg1803z00_4447;

																		BgL_arg1803z00_4447 = (BgL_oclassz00_4441);
																		BgL_arg1802z00_4446 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4447, 1L);
																	}
																	BgL_res2616z00_4458 =
																		(BgL_arg1802z00_4446 == BgL_classz00_4425);
																}
															else
																{	/* SawC/code.scm 297 */
																	BgL_res2616z00_4458 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3122z00_6979 = BgL_res2616z00_4458;
										}
								}
							else
								{	/* SawC/code.scm 297 */
									BgL_test3122z00_6979 = ((bool_t) 0);
								}
						}
						if (BgL_test3122z00_6979)
							{	/* SawC/code.scm 297 */
								if (CBOOL(
										(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_oz00_77)))->BgL_destz00)))
									{	/* SawC/code.scm 299 */
										obj_t BgL_arg2027z00_2734;

										BgL_arg2027z00_2734 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_oz00_77)))->BgL_destz00);
										{
											obj_t BgL_oz00_7008;

											BgL_oz00_7008 = BgL_arg2027z00_2734;
											BgL_oz00_77 = BgL_oz00_7008;
											goto BGl_typenamez00zzsaw_c_codez00;
										}
									}
								else
									{	/* SawC/code.scm 298 */
										return BGl_string2704z00zzsaw_c_codez00;
									}
							}
						else
							{	/* SawC/code.scm 297 */
								return BGl_string2704z00zzsaw_c_codez00;
							}
					}
			}
		}

	}



/* gen-Xfuncall */
	obj_t BGl_genzd2Xfuncallzd2zzsaw_c_codez00(obj_t BgL_strengthz00_78,
		obj_t BgL_rettypez00_79, obj_t BgL_argsz00_80, bool_t BgL_eoazf3zf3_81)
	{
		{	/* SawC/code.scm 304 */
			if (CBOOL(BGl_za2stdcza2z00zzengine_paramz00))
				{	/* SawC/code.scm 307 */
					obj_t BgL_arg2029z00_2735;
					obj_t BgL_arg2030z00_2736;

					if (BgL_eoazf3zf3_81)
						{	/* SawC/code.scm 308 */
							obj_t BgL_arg2031z00_2737;

							BgL_arg2031z00_2737 =
								(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_rettypez00_79)))->BgL_namez00);
							{	/* SawC/code.scm 308 */
								obj_t BgL_list2032z00_2738;

								BgL_list2032z00_2738 =
									MAKE_YOUNG_PAIR(BgL_arg2031z00_2737, BNIL);
								BgL_arg2029z00_2735 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string2705z00zzsaw_c_codez00, BgL_list2032z00_2738);
							}
						}
					else
						{	/* SawC/code.scm 310 */
							obj_t BgL_arg2033z00_2739;
							obj_t BgL_arg2034z00_2740;

							BgL_arg2033z00_2739 =
								BGl_typenamez00zzsaw_c_codez00(BgL_rettypez00_79);
							if (NULLP(BgL_argsz00_80))
								{	/* SawC/code.scm 311 */
									BgL_arg2034z00_2740 = BNIL;
								}
							else
								{	/* SawC/code.scm 311 */
									obj_t BgL_head1655z00_2745;

									{	/* SawC/code.scm 311 */
										obj_t BgL_arg2044z00_2757;

										{	/* SawC/code.scm 311 */
											obj_t BgL_arg2045z00_2758;

											BgL_arg2045z00_2758 = CAR(((obj_t) BgL_argsz00_80));
											BgL_arg2044z00_2757 =
												BGl_typenamez00zzsaw_c_codez00(BgL_arg2045z00_2758);
										}
										BgL_head1655z00_2745 =
											MAKE_YOUNG_PAIR(BgL_arg2044z00_2757, BNIL);
									}
									{	/* SawC/code.scm 311 */
										obj_t BgL_g1658z00_2746;

										BgL_g1658z00_2746 = CDR(((obj_t) BgL_argsz00_80));
										{
											obj_t BgL_l1653z00_2748;
											obj_t BgL_tail1656z00_2749;

											BgL_l1653z00_2748 = BgL_g1658z00_2746;
											BgL_tail1656z00_2749 = BgL_head1655z00_2745;
										BgL_zc3z04anonymousza32038ze3z87_2750:
											if (NULLP(BgL_l1653z00_2748))
												{	/* SawC/code.scm 311 */
													BgL_arg2034z00_2740 = BgL_head1655z00_2745;
												}
											else
												{	/* SawC/code.scm 311 */
													obj_t BgL_newtail1657z00_2752;

													{	/* SawC/code.scm 311 */
														obj_t BgL_arg2041z00_2754;

														{	/* SawC/code.scm 311 */
															obj_t BgL_arg2042z00_2755;

															BgL_arg2042z00_2755 =
																CAR(((obj_t) BgL_l1653z00_2748));
															BgL_arg2041z00_2754 =
																BGl_typenamez00zzsaw_c_codez00
																(BgL_arg2042z00_2755);
														}
														BgL_newtail1657z00_2752 =
															MAKE_YOUNG_PAIR(BgL_arg2041z00_2754, BNIL);
													}
													SET_CDR(BgL_tail1656z00_2749,
														BgL_newtail1657z00_2752);
													{	/* SawC/code.scm 311 */
														obj_t BgL_arg2040z00_2753;

														BgL_arg2040z00_2753 =
															CDR(((obj_t) BgL_l1653z00_2748));
														{
															obj_t BgL_tail1656z00_7035;
															obj_t BgL_l1653z00_7034;

															BgL_l1653z00_7034 = BgL_arg2040z00_2753;
															BgL_tail1656z00_7035 = BgL_newtail1657z00_2752;
															BgL_tail1656z00_2749 = BgL_tail1656z00_7035;
															BgL_l1653z00_2748 = BgL_l1653z00_7034;
															goto BgL_zc3z04anonymousza32038ze3z87_2750;
														}
													}
												}
										}
									}
								}
							{	/* SawC/code.scm 309 */
								obj_t BgL_list2035z00_2741;

								{	/* SawC/code.scm 309 */
									obj_t BgL_arg2036z00_2742;

									BgL_arg2036z00_2742 =
										MAKE_YOUNG_PAIR(BgL_arg2034z00_2740, BNIL);
									BgL_list2035z00_2741 =
										MAKE_YOUNG_PAIR(BgL_arg2033z00_2739, BgL_arg2036z00_2742);
								}
								BgL_arg2029z00_2735 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string2706z00zzsaw_c_codez00, BgL_list2035z00_2741);
							}
						}
					{	/* SawC/code.scm 306 */
						obj_t BgL_tmpz00_7039;

						BgL_tmpz00_7039 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg2030z00_2736 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7039);
					}
					bgl_display_obj(BgL_arg2029z00_2735, BgL_arg2030z00_2736);
				}
			else
				{	/* SawC/code.scm 312 */
					obj_t BgL_arg2046z00_2759;

					BgL_arg2046z00_2759 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_rettypez00_79)))->BgL_namez00);
					{	/* SawC/code.scm 312 */
						obj_t BgL_list2047z00_2760;

						{	/* SawC/code.scm 312 */
							obj_t BgL_arg2048z00_2761;

							{	/* SawC/code.scm 312 */
								obj_t BgL_arg2049z00_2762;

								BgL_arg2049z00_2762 =
									MAKE_YOUNG_PAIR(BGl_string2707z00zzsaw_c_codez00, BNIL);
								BgL_arg2048z00_2761 =
									MAKE_YOUNG_PAIR(BgL_arg2046z00_2759, BgL_arg2049z00_2762);
							}
							BgL_list2047z00_2760 =
								MAKE_YOUNG_PAIR(BGl_string2708z00zzsaw_c_codez00,
								BgL_arg2048z00_2761);
						}
						BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2047z00_2760);
					}
				}
			{	/* SawC/code.scm 313 */
				obj_t BgL_list2050z00_2763;

				{	/* SawC/code.scm 313 */
					obj_t BgL_arg2051z00_2764;

					{	/* SawC/code.scm 313 */
						obj_t BgL_arg2052z00_2765;

						BgL_arg2052z00_2765 =
							MAKE_YOUNG_PAIR(BGl_string2709z00zzsaw_c_codez00, BNIL);
						BgL_arg2051z00_2764 =
							MAKE_YOUNG_PAIR(BgL_strengthz00_78, BgL_arg2052z00_2765);
					}
					BgL_list2050z00_2763 =
						MAKE_YOUNG_PAIR(BGl_string2710z00zzsaw_c_codez00,
						BgL_arg2051z00_2764);
				}
				BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2050z00_2763);
			}
			{	/* SawC/code.scm 314 */
				obj_t BgL_arg2055z00_2766;

				BgL_arg2055z00_2766 = CAR(((obj_t) BgL_argsz00_80));
				BGl_genzd2regzd2zzsaw_c_codez00(BgL_arg2055z00_2766);
			}
			{	/* SawC/code.scm 315 */
				obj_t BgL_arg2056z00_2767;

				{	/* SawC/code.scm 315 */
					obj_t BgL_tmpz00_7056;

					BgL_tmpz00_7056 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2056z00_2767 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7056);
				}
				bgl_display_string(BGl_string2711z00zzsaw_c_codez00,
					BgL_arg2056z00_2767);
			}
			BGl_genzd2argszd2zzsaw_c_codez00(BgL_argsz00_80);
			if (BgL_eoazf3zf3_81)
				{	/* SawC/code.scm 317 */
					obj_t BgL_arg2057z00_2768;

					{	/* SawC/code.scm 317 */
						obj_t BgL_tmpz00_7062;

						BgL_tmpz00_7062 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg2057z00_2768 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7062);
					}
					bgl_display_string(BGl_string2712z00zzsaw_c_codez00,
						BgL_arg2057z00_2768);
				}
			else
				{	/* SawC/code.scm 317 */
					BFALSE;
				}
			{	/* SawC/code.scm 318 */
				obj_t BgL_arg2058z00_2769;

				{	/* SawC/code.scm 318 */
					obj_t BgL_tmpz00_7066;

					BgL_tmpz00_7066 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2058z00_2769 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7066);
				}
				return
					bgl_display_string(BGl_string2686z00zzsaw_c_codez00,
					BgL_arg2058z00_2769);
			}
		}

	}



/* gen-upcase */
	obj_t BGl_genzd2upcasezd2zzsaw_c_codez00(BgL_rtl_funz00_bglt BgL_funz00_91)
	{
		{	/* SawC/code.scm 407 */
			{	/* SawC/code.scm 409 */
				obj_t BgL_arg2059z00_2770;

				{	/* SawC/code.scm 409 */
					obj_t BgL_arg2062z00_2773;

					{	/* SawC/code.scm 409 */
						obj_t BgL_arg2063z00_2774;

						{	/* SawC/code.scm 409 */
							obj_t BgL_arg2064z00_2775;

							{	/* SawC/code.scm 409 */
								obj_t BgL_arg1815z00_4480;
								long BgL_arg1816z00_4481;

								BgL_arg1815z00_4480 = (BGl_za2classesza2z00zz__objectz00);
								{	/* SawC/code.scm 409 */
									long BgL_arg1817z00_4482;

									BgL_arg1817z00_4482 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_91));
									BgL_arg1816z00_4481 = (BgL_arg1817z00_4482 - OBJECT_TYPE);
								}
								BgL_arg2064z00_2775 =
									VECTOR_REF(BgL_arg1815z00_4480, BgL_arg1816z00_4481);
							}
							BgL_arg2063z00_2774 =
								BGl_classzd2namezd2zz__objectz00(BgL_arg2064z00_2775);
						}
						{	/* SawC/code.scm 409 */
							obj_t BgL_arg1455z00_4489;

							BgL_arg1455z00_4489 = SYMBOL_TO_STRING(BgL_arg2063z00_2774);
							BgL_arg2062z00_2773 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_4489);
					}}
					BgL_arg2059z00_2770 =
						BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(BgL_arg2062z00_2773);
				}
				{	/* SawC/code.scm 408 */
					obj_t BgL_list2060z00_2771;

					{	/* SawC/code.scm 408 */
						obj_t BgL_arg2061z00_2772;

						BgL_arg2061z00_2772 = MAKE_YOUNG_PAIR(BgL_arg2059z00_2770, BNIL);
						BgL_list2060z00_2771 =
							MAKE_YOUNG_PAIR(BGl_string2713z00zzsaw_c_codez00,
							BgL_arg2061z00_2772);
					}
					return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2060z00_2771);
				}
			}
		}

	}



/* vfun-name */
	obj_t BGl_vfunzd2namezd2zzsaw_c_codez00(obj_t BgL_fz00_92,
		BgL_typez00_bglt BgL_typez00_93)
	{
		{	/* SawC/code.scm 411 */
			{	/* SawC/code.scm 412 */
				obj_t BgL_arg2065z00_2776;

				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							(((BgL_typez00_bglt) COBJECT(BgL_typez00_93))->BgL_idz00),
							CNST_TABLE_REF(5))))
					{	/* SawC/code.scm 412 */
						BgL_arg2065z00_2776 = BGl_string2714z00zzsaw_c_codez00;
					}
				else
					{	/* SawC/code.scm 412 */
						BgL_arg2065z00_2776 = BGl_string2690z00zzsaw_c_codez00;
					}
				{	/* SawC/code.scm 412 */
					obj_t BgL_list2066z00_2777;

					{	/* SawC/code.scm 412 */
						obj_t BgL_arg2067z00_2778;

						{	/* SawC/code.scm 412 */
							obj_t BgL_arg2068z00_2779;

							BgL_arg2068z00_2779 = MAKE_YOUNG_PAIR(BgL_fz00_92, BNIL);
							BgL_arg2067z00_2778 =
								MAKE_YOUNG_PAIR(BgL_arg2065z00_2776, BgL_arg2068z00_2779);
						}
						BgL_list2066z00_2777 =
							MAKE_YOUNG_PAIR(BGl_string2715z00zzsaw_c_codez00,
							BgL_arg2067z00_2778);
					}
					return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2066z00_2777);
				}
			}
		}

	}



/* vextra */
	obj_t BGl_vextraz00zzsaw_c_codez00(BgL_typez00_bglt BgL_typez00_112)
	{
		{	/* SawC/code.scm 468 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						(((BgL_typez00_bglt) COBJECT(BgL_typez00_112))->BgL_idz00),
						CNST_TABLE_REF(5))))
				{	/* SawC/code.scm 470 */
					obj_t BgL_tnz00_2782;

					BgL_tnz00_2782 =
						(((BgL_typez00_bglt) COBJECT(BgL_typez00_112))->BgL_namez00);
					{	/* SawC/code.scm 471 */
						obj_t BgL_arg2072z00_2783;

						BgL_arg2072z00_2783 = bigloo_mangle(BgL_tnz00_2782);
						{	/* SawC/code.scm 471 */
							obj_t BgL_list2073z00_2784;

							{	/* SawC/code.scm 471 */
								obj_t BgL_arg2074z00_2785;

								{	/* SawC/code.scm 471 */
									obj_t BgL_arg2075z00_2786;

									{	/* SawC/code.scm 471 */
										obj_t BgL_arg2076z00_2787;

										BgL_arg2076z00_2787 =
											MAKE_YOUNG_PAIR(BGl_string2685z00zzsaw_c_codez00, BNIL);
										BgL_arg2075z00_2786 =
											MAKE_YOUNG_PAIR(BgL_tnz00_2782, BgL_arg2076z00_2787);
									}
									BgL_arg2074z00_2785 =
										MAKE_YOUNG_PAIR(BGl_string2685z00zzsaw_c_codez00,
										BgL_arg2075z00_2786);
								}
								BgL_list2073z00_2784 =
									MAKE_YOUNG_PAIR(BgL_arg2072z00_2783, BgL_arg2074z00_2785);
							}
							return
								BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2073z00_2784);
						}
					}
				}
			else
				{	/* SawC/code.scm 469 */
					return BFALSE;
				}
		}

	}



/* inl-op */
	obj_t BGl_inlzd2opzd2zzsaw_c_codez00(obj_t BgL_sz00_122)
	{
		{	/* SawC/code.scm 517 */
			{	/* SawC/code.scm 519 */
				bool_t BgL_test3135z00_7103;

				{	/* SawC/code.scm 519 */
					long BgL_l1z00_4506;

					BgL_l1z00_4506 = STRING_LENGTH(((obj_t) BgL_sz00_122));
					if ((BgL_l1z00_4506 == 2L))
						{	/* SawC/code.scm 519 */
							int BgL_arg1282z00_4509;

							{	/* SawC/code.scm 519 */
								char *BgL_auxz00_7111;
								char *BgL_tmpz00_7108;

								BgL_auxz00_7111 =
									BSTRING_TO_STRING(BGl_string2716z00zzsaw_c_codez00);
								BgL_tmpz00_7108 = BSTRING_TO_STRING(((obj_t) BgL_sz00_122));
								BgL_arg1282z00_4509 =
									memcmp(BgL_tmpz00_7108, BgL_auxz00_7111, BgL_l1z00_4506);
							}
							BgL_test3135z00_7103 = ((long) (BgL_arg1282z00_4509) == 0L);
						}
					else
						{	/* SawC/code.scm 519 */
							BgL_test3135z00_7103 = ((bool_t) 0);
						}
				}
				if (BgL_test3135z00_7103)
					{	/* SawC/code.scm 519 */
						return BGl_string2717z00zzsaw_c_codez00;
					}
				else
					{	/* SawC/code.scm 520 */
						bool_t BgL_test3137z00_7116;

						{	/* SawC/code.scm 520 */
							long BgL_l1z00_4517;

							BgL_l1z00_4517 = STRING_LENGTH(((obj_t) BgL_sz00_122));
							if ((BgL_l1z00_4517 == 2L))
								{	/* SawC/code.scm 520 */
									int BgL_arg1282z00_4520;

									{	/* SawC/code.scm 520 */
										char *BgL_auxz00_7124;
										char *BgL_tmpz00_7121;

										BgL_auxz00_7124 =
											BSTRING_TO_STRING(BGl_string2718z00zzsaw_c_codez00);
										BgL_tmpz00_7121 = BSTRING_TO_STRING(((obj_t) BgL_sz00_122));
										BgL_arg1282z00_4520 =
											memcmp(BgL_tmpz00_7121, BgL_auxz00_7124, BgL_l1z00_4517);
									}
									BgL_test3137z00_7116 = ((long) (BgL_arg1282z00_4520) == 0L);
								}
							else
								{	/* SawC/code.scm 520 */
									BgL_test3137z00_7116 = ((bool_t) 0);
								}
						}
						if (BgL_test3137z00_7116)
							{	/* SawC/code.scm 520 */
								return BGl_string2719z00zzsaw_c_codez00;
							}
						else
							{	/* SawC/code.scm 521 */
								bool_t BgL_test3139z00_7129;

								{	/* SawC/code.scm 521 */
									long BgL_l1z00_4528;

									BgL_l1z00_4528 = STRING_LENGTH(((obj_t) BgL_sz00_122));
									if ((BgL_l1z00_4528 == 2L))
										{	/* SawC/code.scm 521 */
											int BgL_arg1282z00_4531;

											{	/* SawC/code.scm 521 */
												char *BgL_auxz00_7137;
												char *BgL_tmpz00_7134;

												BgL_auxz00_7137 =
													BSTRING_TO_STRING(BGl_string2720z00zzsaw_c_codez00);
												BgL_tmpz00_7134 =
													BSTRING_TO_STRING(((obj_t) BgL_sz00_122));
												BgL_arg1282z00_4531 =
													memcmp(BgL_tmpz00_7134, BgL_auxz00_7137,
													BgL_l1z00_4528);
											}
											BgL_test3139z00_7129 =
												((long) (BgL_arg1282z00_4531) == 0L);
										}
									else
										{	/* SawC/code.scm 521 */
											BgL_test3139z00_7129 = ((bool_t) 0);
										}
								}
								if (BgL_test3139z00_7129)
									{	/* SawC/code.scm 521 */
										return BGl_string2721z00zzsaw_c_codez00;
									}
								else
									{	/* SawC/code.scm 522 */
										bool_t BgL_test3141z00_7142;

										{	/* SawC/code.scm 522 */
											long BgL_l1z00_4539;

											BgL_l1z00_4539 = STRING_LENGTH(((obj_t) BgL_sz00_122));
											if ((BgL_l1z00_4539 == 1L))
												{	/* SawC/code.scm 522 */
													int BgL_arg1282z00_4542;

													{	/* SawC/code.scm 522 */
														char *BgL_auxz00_7150;
														char *BgL_tmpz00_7147;

														BgL_auxz00_7150 =
															BSTRING_TO_STRING
															(BGl_string2722z00zzsaw_c_codez00);
														BgL_tmpz00_7147 =
															BSTRING_TO_STRING(((obj_t) BgL_sz00_122));
														BgL_arg1282z00_4542 =
															memcmp(BgL_tmpz00_7147, BgL_auxz00_7150,
															BgL_l1z00_4539);
													}
													BgL_test3141z00_7142 =
														((long) (BgL_arg1282z00_4542) == 0L);
												}
											else
												{	/* SawC/code.scm 522 */
													BgL_test3141z00_7142 = ((bool_t) 0);
												}
										}
										if (BgL_test3141z00_7142)
											{	/* SawC/code.scm 522 */
												return BGl_string2723z00zzsaw_c_codez00;
											}
										else
											{	/* SawC/code.scm 523 */
												bool_t BgL_test3143z00_7155;

												{	/* SawC/code.scm 523 */
													long BgL_l1z00_4550;

													BgL_l1z00_4550 =
														STRING_LENGTH(((obj_t) BgL_sz00_122));
													if ((BgL_l1z00_4550 == 1L))
														{	/* SawC/code.scm 523 */
															int BgL_arg1282z00_4553;

															{	/* SawC/code.scm 523 */
																char *BgL_auxz00_7163;
																char *BgL_tmpz00_7160;

																BgL_auxz00_7163 =
																	BSTRING_TO_STRING
																	(BGl_string2724z00zzsaw_c_codez00);
																BgL_tmpz00_7160 =
																	BSTRING_TO_STRING(((obj_t) BgL_sz00_122));
																BgL_arg1282z00_4553 =
																	memcmp(BgL_tmpz00_7160, BgL_auxz00_7163,
																	BgL_l1z00_4550);
															}
															BgL_test3143z00_7155 =
																((long) (BgL_arg1282z00_4553) == 0L);
														}
													else
														{	/* SawC/code.scm 523 */
															BgL_test3143z00_7155 = ((bool_t) 0);
														}
												}
												if (BgL_test3143z00_7155)
													{	/* SawC/code.scm 523 */
														return BGl_string2725z00zzsaw_c_codez00;
													}
												else
													{	/* SawC/code.scm 524 */
														bool_t BgL_test3145z00_7168;

														{	/* SawC/code.scm 524 */
															long BgL_l1z00_4561;

															BgL_l1z00_4561 =
																STRING_LENGTH(((obj_t) BgL_sz00_122));
															if ((BgL_l1z00_4561 == 1L))
																{	/* SawC/code.scm 524 */
																	int BgL_arg1282z00_4564;

																	{	/* SawC/code.scm 524 */
																		char *BgL_auxz00_7176;
																		char *BgL_tmpz00_7173;

																		BgL_auxz00_7176 =
																			BSTRING_TO_STRING
																			(BGl_string2726z00zzsaw_c_codez00);
																		BgL_tmpz00_7173 =
																			BSTRING_TO_STRING(((obj_t) BgL_sz00_122));
																		BgL_arg1282z00_4564 =
																			memcmp(BgL_tmpz00_7173, BgL_auxz00_7176,
																			BgL_l1z00_4561);
																	}
																	BgL_test3145z00_7168 =
																		((long) (BgL_arg1282z00_4564) == 0L);
																}
															else
																{	/* SawC/code.scm 524 */
																	BgL_test3145z00_7168 = ((bool_t) 0);
																}
														}
														if (BgL_test3145z00_7168)
															{	/* SawC/code.scm 524 */
																return BGl_string2727z00zzsaw_c_codez00;
															}
														else
															{	/* SawC/code.scm 525 */
																bool_t BgL_test3147z00_7181;

																{	/* SawC/code.scm 525 */
																	long BgL_l1z00_4572;

																	BgL_l1z00_4572 =
																		STRING_LENGTH(((obj_t) BgL_sz00_122));
																	if ((BgL_l1z00_4572 == 1L))
																		{	/* SawC/code.scm 525 */
																			int BgL_arg1282z00_4575;

																			{	/* SawC/code.scm 525 */
																				char *BgL_auxz00_7189;
																				char *BgL_tmpz00_7186;

																				BgL_auxz00_7189 =
																					BSTRING_TO_STRING
																					(BGl_string2728z00zzsaw_c_codez00);
																				BgL_tmpz00_7186 =
																					BSTRING_TO_STRING(((obj_t)
																						BgL_sz00_122));
																				BgL_arg1282z00_4575 =
																					memcmp(BgL_tmpz00_7186,
																					BgL_auxz00_7189, BgL_l1z00_4572);
																			}
																			BgL_test3147z00_7181 =
																				((long) (BgL_arg1282z00_4575) == 0L);
																		}
																	else
																		{	/* SawC/code.scm 525 */
																			BgL_test3147z00_7181 = ((bool_t) 0);
																		}
																}
																if (BgL_test3147z00_7181)
																	{	/* SawC/code.scm 525 */
																		return BGl_string2729z00zzsaw_c_codez00;
																	}
																else
																	{	/* SawC/code.scm 526 */
																		bool_t BgL_test3149z00_7194;

																		{	/* SawC/code.scm 526 */
																			long BgL_l1z00_4583;

																			BgL_l1z00_4583 =
																				STRING_LENGTH(((obj_t) BgL_sz00_122));
																			if ((BgL_l1z00_4583 == 1L))
																				{	/* SawC/code.scm 526 */
																					int BgL_arg1282z00_4586;

																					{	/* SawC/code.scm 526 */
																						char *BgL_auxz00_7202;
																						char *BgL_tmpz00_7199;

																						BgL_auxz00_7202 =
																							BSTRING_TO_STRING
																							(BGl_string2730z00zzsaw_c_codez00);
																						BgL_tmpz00_7199 =
																							BSTRING_TO_STRING(((obj_t)
																								BgL_sz00_122));
																						BgL_arg1282z00_4586 =
																							memcmp(BgL_tmpz00_7199,
																							BgL_auxz00_7202, BgL_l1z00_4583);
																					}
																					BgL_test3149z00_7194 =
																						(
																						(long) (BgL_arg1282z00_4586) == 0L);
																				}
																			else
																				{	/* SawC/code.scm 526 */
																					BgL_test3149z00_7194 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test3149z00_7194)
																			{	/* SawC/code.scm 526 */
																				return BGl_string2731z00zzsaw_c_codez00;
																			}
																		else
																			{	/* SawC/code.scm 527 */
																				bool_t BgL_test3151z00_7207;

																				{	/* SawC/code.scm 527 */
																					long BgL_l1z00_4594;

																					BgL_l1z00_4594 =
																						STRING_LENGTH(
																						((obj_t) BgL_sz00_122));
																					if ((BgL_l1z00_4594 == 1L))
																						{	/* SawC/code.scm 527 */
																							int BgL_arg1282z00_4597;

																							{	/* SawC/code.scm 527 */
																								char *BgL_auxz00_7215;
																								char *BgL_tmpz00_7212;

																								BgL_auxz00_7215 =
																									BSTRING_TO_STRING
																									(BGl_string2732z00zzsaw_c_codez00);
																								BgL_tmpz00_7212 =
																									BSTRING_TO_STRING(((obj_t)
																										BgL_sz00_122));
																								BgL_arg1282z00_4597 =
																									memcmp(BgL_tmpz00_7212,
																									BgL_auxz00_7215,
																									BgL_l1z00_4594);
																							}
																							BgL_test3151z00_7207 =
																								(
																								(long) (BgL_arg1282z00_4597) ==
																								0L);
																						}
																					else
																						{	/* SawC/code.scm 527 */
																							BgL_test3151z00_7207 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test3151z00_7207)
																					{	/* SawC/code.scm 527 */
																						return
																							BGl_string2733z00zzsaw_c_codez00;
																					}
																				else
																					{	/* SawC/code.scm 528 */
																						bool_t BgL_test3153z00_7220;

																						{	/* SawC/code.scm 528 */
																							long BgL_l1z00_4605;

																							BgL_l1z00_4605 =
																								STRING_LENGTH(
																								((obj_t) BgL_sz00_122));
																							if ((BgL_l1z00_4605 == 1L))
																								{	/* SawC/code.scm 528 */
																									int BgL_arg1282z00_4608;

																									{	/* SawC/code.scm 528 */
																										char *BgL_auxz00_7228;
																										char *BgL_tmpz00_7225;

																										BgL_auxz00_7228 =
																											BSTRING_TO_STRING
																											(BGl_string2734z00zzsaw_c_codez00);
																										BgL_tmpz00_7225 =
																											BSTRING_TO_STRING(((obj_t)
																												BgL_sz00_122));
																										BgL_arg1282z00_4608 =
																											memcmp(BgL_tmpz00_7225,
																											BgL_auxz00_7228,
																											BgL_l1z00_4605);
																									}
																									BgL_test3153z00_7220 =
																										(
																										(long) (BgL_arg1282z00_4608)
																										== 0L);
																								}
																							else
																								{	/* SawC/code.scm 528 */
																									BgL_test3153z00_7220 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test3153z00_7220)
																							{	/* SawC/code.scm 528 */
																								return
																									BGl_string2735z00zzsaw_c_codez00;
																							}
																						else
																							{	/* SawC/code.scm 529 */
																								bool_t BgL_test3155z00_7233;

																								{	/* SawC/code.scm 529 */
																									long BgL_l1z00_4616;

																									BgL_l1z00_4616 =
																										STRING_LENGTH(
																										((obj_t) BgL_sz00_122));
																									if ((BgL_l1z00_4616 == 1L))
																										{	/* SawC/code.scm 529 */
																											int BgL_arg1282z00_4619;

																											{	/* SawC/code.scm 529 */
																												char *BgL_auxz00_7241;
																												char *BgL_tmpz00_7238;

																												BgL_auxz00_7241 =
																													BSTRING_TO_STRING
																													(BGl_string2736z00zzsaw_c_codez00);
																												BgL_tmpz00_7238 =
																													BSTRING_TO_STRING((
																														(obj_t)
																														BgL_sz00_122));
																												BgL_arg1282z00_4619 =
																													memcmp
																													(BgL_tmpz00_7238,
																													BgL_auxz00_7241,
																													BgL_l1z00_4616);
																											}
																											BgL_test3155z00_7233 =
																												(
																												(long)
																												(BgL_arg1282z00_4619) ==
																												0L);
																										}
																									else
																										{	/* SawC/code.scm 529 */
																											BgL_test3155z00_7233 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test3155z00_7233)
																									{	/* SawC/code.scm 529 */
																										return
																											BGl_string2737z00zzsaw_c_codez00;
																									}
																								else
																									{	/* SawC/code.scm 530 */
																										bool_t BgL_test3157z00_7246;

																										{	/* SawC/code.scm 530 */
																											long BgL_l1z00_4627;

																											BgL_l1z00_4627 =
																												STRING_LENGTH(
																												((obj_t) BgL_sz00_122));
																											if (
																												(BgL_l1z00_4627 == 1L))
																												{	/* SawC/code.scm 530 */
																													int
																														BgL_arg1282z00_4630;
																													{	/* SawC/code.scm 530 */
																														char
																															*BgL_auxz00_7254;
																														char
																															*BgL_tmpz00_7251;
																														BgL_auxz00_7254 =
																															BSTRING_TO_STRING
																															(BGl_string2738z00zzsaw_c_codez00);
																														BgL_tmpz00_7251 =
																															BSTRING_TO_STRING(
																															((obj_t)
																																BgL_sz00_122));
																														BgL_arg1282z00_4630
																															=
																															memcmp
																															(BgL_tmpz00_7251,
																															BgL_auxz00_7254,
																															BgL_l1z00_4627);
																													}
																													BgL_test3157z00_7246 =
																														(
																														(long)
																														(BgL_arg1282z00_4630)
																														== 0L);
																												}
																											else
																												{	/* SawC/code.scm 530 */
																													BgL_test3157z00_7246 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test3157z00_7246)
																											{	/* SawC/code.scm 530 */
																												return
																													BGl_string2739z00zzsaw_c_codez00;
																											}
																										else
																											{	/* SawC/code.scm 531 */
																												bool_t
																													BgL_test3159z00_7259;
																												{	/* SawC/code.scm 531 */
																													long BgL_l1z00_4638;

																													BgL_l1z00_4638 =
																														STRING_LENGTH(
																														((obj_t)
																															BgL_sz00_122));
																													if ((BgL_l1z00_4638 ==
																															3L))
																														{	/* SawC/code.scm 531 */
																															int
																																BgL_arg1282z00_4641;
																															{	/* SawC/code.scm 531 */
																																char
																																	*BgL_auxz00_7267;
																																char
																																	*BgL_tmpz00_7264;
																																BgL_auxz00_7267
																																	=
																																	BSTRING_TO_STRING
																																	(BGl_string2740z00zzsaw_c_codez00);
																																BgL_tmpz00_7264
																																	=
																																	BSTRING_TO_STRING
																																	(((obj_t)
																																		BgL_sz00_122));
																																BgL_arg1282z00_4641
																																	=
																																	memcmp
																																	(BgL_tmpz00_7264,
																																	BgL_auxz00_7267,
																																	BgL_l1z00_4638);
																															}
																															BgL_test3159z00_7259
																																=
																																((long)
																																(BgL_arg1282z00_4641)
																																== 0L);
																														}
																													else
																														{	/* SawC/code.scm 531 */
																															BgL_test3159z00_7259
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test3159z00_7259)
																													{	/* SawC/code.scm 531 */
																														return
																															BGl_string2727z00zzsaw_c_codez00;
																													}
																												else
																													{	/* SawC/code.scm 532 */
																														bool_t
																															BgL_test3161z00_7272;
																														{	/* SawC/code.scm 532 */
																															long
																																BgL_l1z00_4649;
																															BgL_l1z00_4649 =
																																STRING_LENGTH((
																																	(obj_t)
																																	BgL_sz00_122));
																															if (
																																(BgL_l1z00_4649
																																	== 3L))
																																{	/* SawC/code.scm 532 */
																																	int
																																		BgL_arg1282z00_4652;
																																	{	/* SawC/code.scm 532 */
																																		char
																																			*BgL_auxz00_7280;
																																		char
																																			*BgL_tmpz00_7277;
																																		BgL_auxz00_7280
																																			=
																																			BSTRING_TO_STRING
																																			(BGl_string2741z00zzsaw_c_codez00);
																																		BgL_tmpz00_7277
																																			=
																																			BSTRING_TO_STRING
																																			(((obj_t)
																																				BgL_sz00_122));
																																		BgL_arg1282z00_4652
																																			=
																																			memcmp
																																			(BgL_tmpz00_7277,
																																			BgL_auxz00_7280,
																																			BgL_l1z00_4649);
																																	}
																																	BgL_test3161z00_7272
																																		=
																																		((long)
																																		(BgL_arg1282z00_4652)
																																		== 0L);
																																}
																															else
																																{	/* SawC/code.scm 532 */
																																	BgL_test3161z00_7272
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																														if (BgL_test3161z00_7272)
																															{	/* SawC/code.scm 532 */
																																return
																																	BGl_string2729z00zzsaw_c_codez00;
																															}
																														else
																															{	/* SawC/code.scm 533 */
																																bool_t
																																	BgL_test3163z00_7285;
																																{	/* SawC/code.scm 533 */
																																	long
																																		BgL_l1z00_4660;
																																	BgL_l1z00_4660
																																		=
																																		STRING_LENGTH
																																		(((obj_t)
																																			BgL_sz00_122));
																																	if (
																																		(BgL_l1z00_4660
																																			== 3L))
																																		{	/* SawC/code.scm 533 */
																																			int
																																				BgL_arg1282z00_4663;
																																			{	/* SawC/code.scm 533 */
																																				char
																																					*BgL_auxz00_7293;
																																				char
																																					*BgL_tmpz00_7290;
																																				BgL_auxz00_7293
																																					=
																																					BSTRING_TO_STRING
																																					(BGl_string2742z00zzsaw_c_codez00);
																																				BgL_tmpz00_7290
																																					=
																																					BSTRING_TO_STRING
																																					(((obj_t) BgL_sz00_122));
																																				BgL_arg1282z00_4663
																																					=
																																					memcmp
																																					(BgL_tmpz00_7290,
																																					BgL_auxz00_7293,
																																					BgL_l1z00_4660);
																																			}
																																			BgL_test3163z00_7285
																																				=
																																				((long)
																																				(BgL_arg1282z00_4663)
																																				== 0L);
																																		}
																																	else
																																		{	/* SawC/code.scm 533 */
																																			BgL_test3163z00_7285
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test3163z00_7285)
																																	{	/* SawC/code.scm 533 */
																																		return
																																			BGl_string2743z00zzsaw_c_codez00;
																																	}
																																else
																																	{	/* SawC/code.scm 534 */
																																		bool_t
																																			BgL_test3165z00_7298;
																																		{	/* SawC/code.scm 534 */
																																			long
																																				BgL_l1z00_4671;
																																			BgL_l1z00_4671
																																				=
																																				STRING_LENGTH
																																				(((obj_t) BgL_sz00_122));
																																			if (
																																				(BgL_l1z00_4671
																																					==
																																					4L))
																																				{	/* SawC/code.scm 534 */
																																					int
																																						BgL_arg1282z00_4674;
																																					{	/* SawC/code.scm 534 */
																																						char
																																							*BgL_auxz00_7306;
																																						char
																																							*BgL_tmpz00_7303;
																																						BgL_auxz00_7306
																																							=
																																							BSTRING_TO_STRING
																																							(BGl_string2744z00zzsaw_c_codez00);
																																						BgL_tmpz00_7303
																																							=
																																							BSTRING_TO_STRING
																																							(((obj_t) BgL_sz00_122));
																																						BgL_arg1282z00_4674
																																							=
																																							memcmp
																																							(BgL_tmpz00_7303,
																																							BgL_auxz00_7306,
																																							BgL_l1z00_4671);
																																					}
																																					BgL_test3165z00_7298
																																						=
																																						(
																																						(long)
																																						(BgL_arg1282z00_4674)
																																						==
																																						0L);
																																				}
																																			else
																																				{	/* SawC/code.scm 534 */
																																					BgL_test3165z00_7298
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																		if (BgL_test3165z00_7298)
																																			{	/* SawC/code.scm 534 */
																																				return
																																					BGl_string2745z00zzsaw_c_codez00;
																																			}
																																		else
																																			{	/* SawC/code.scm 535 */
																																				bool_t
																																					BgL_test3167z00_7311;
																																				{	/* SawC/code.scm 535 */
																																					long
																																						BgL_l1z00_4682;
																																					BgL_l1z00_4682
																																						=
																																						STRING_LENGTH
																																						(((obj_t) BgL_sz00_122));
																																					if (
																																						(BgL_l1z00_4682
																																							==
																																							4L))
																																						{	/* SawC/code.scm 535 */
																																							int
																																								BgL_arg1282z00_4685;
																																							{	/* SawC/code.scm 535 */
																																								char
																																									*BgL_auxz00_7319;
																																								char
																																									*BgL_tmpz00_7316;
																																								BgL_auxz00_7319
																																									=
																																									BSTRING_TO_STRING
																																									(BGl_string2746z00zzsaw_c_codez00);
																																								BgL_tmpz00_7316
																																									=
																																									BSTRING_TO_STRING
																																									(
																																									((obj_t) BgL_sz00_122));
																																								BgL_arg1282z00_4685
																																									=
																																									memcmp
																																									(BgL_tmpz00_7316,
																																									BgL_auxz00_7319,
																																									BgL_l1z00_4682);
																																							}
																																							BgL_test3167z00_7311
																																								=
																																								(
																																								(long)
																																								(BgL_arg1282z00_4685)
																																								==
																																								0L);
																																						}
																																					else
																																						{	/* SawC/code.scm 535 */
																																							BgL_test3167z00_7311
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																				if (BgL_test3167z00_7311)
																																					{	/* SawC/code.scm 535 */
																																						return
																																							BGl_string2747z00zzsaw_c_codez00;
																																					}
																																				else
																																					{	/* SawC/code.scm 536 */
																																						bool_t
																																							BgL_test3169z00_7324;
																																						{	/* SawC/code.scm 536 */
																																							long
																																								BgL_l1z00_4693;
																																							BgL_l1z00_4693
																																								=
																																								STRING_LENGTH
																																								(
																																								((obj_t) BgL_sz00_122));
																																							if ((BgL_l1z00_4693 == 9L))
																																								{	/* SawC/code.scm 536 */
																																									int
																																										BgL_arg1282z00_4696;
																																									{	/* SawC/code.scm 536 */
																																										char
																																											*BgL_auxz00_7332;
																																										char
																																											*BgL_tmpz00_7329;
																																										BgL_auxz00_7332
																																											=
																																											BSTRING_TO_STRING
																																											(BGl_string2748z00zzsaw_c_codez00);
																																										BgL_tmpz00_7329
																																											=
																																											BSTRING_TO_STRING
																																											(
																																											((obj_t) BgL_sz00_122));
																																										BgL_arg1282z00_4696
																																											=
																																											memcmp
																																											(BgL_tmpz00_7329,
																																											BgL_auxz00_7332,
																																											BgL_l1z00_4693);
																																									}
																																									BgL_test3169z00_7324
																																										=
																																										(
																																										(long)
																																										(BgL_arg1282z00_4696)
																																										==
																																										0L);
																																								}
																																							else
																																								{	/* SawC/code.scm 536 */
																																									BgL_test3169z00_7324
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																						if (BgL_test3169z00_7324)
																																							{	/* SawC/code.scm 536 */
																																								return
																																									BGl_string2749z00zzsaw_c_codez00;
																																							}
																																						else
																																							{	/* SawC/code.scm 537 */
																																								bool_t
																																									BgL_test3171z00_7337;
																																								{	/* SawC/code.scm 537 */
																																									long
																																										BgL_l1z00_4704;
																																									BgL_l1z00_4704
																																										=
																																										STRING_LENGTH
																																										(
																																										((obj_t) BgL_sz00_122));
																																									if ((BgL_l1z00_4704 == 11L))
																																										{	/* SawC/code.scm 537 */
																																											int
																																												BgL_arg1282z00_4707;
																																											{	/* SawC/code.scm 537 */
																																												char
																																													*BgL_auxz00_7345;
																																												char
																																													*BgL_tmpz00_7342;
																																												BgL_auxz00_7345
																																													=
																																													BSTRING_TO_STRING
																																													(BGl_string2750z00zzsaw_c_codez00);
																																												BgL_tmpz00_7342
																																													=
																																													BSTRING_TO_STRING
																																													(
																																													((obj_t) BgL_sz00_122));
																																												BgL_arg1282z00_4707
																																													=
																																													memcmp
																																													(BgL_tmpz00_7342,
																																													BgL_auxz00_7345,
																																													BgL_l1z00_4704);
																																											}
																																											BgL_test3171z00_7337
																																												=
																																												(
																																												(long)
																																												(BgL_arg1282z00_4707)
																																												==
																																												0L);
																																										}
																																									else
																																										{	/* SawC/code.scm 537 */
																																											BgL_test3171z00_7337
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																								if (BgL_test3171z00_7337)
																																									{	/* SawC/code.scm 537 */
																																										return
																																											BGl_string2751z00zzsaw_c_codez00;
																																									}
																																								else
																																									{	/* SawC/code.scm 538 */
																																										bool_t
																																											BgL_test3173z00_7350;
																																										{	/* SawC/code.scm 538 */
																																											long
																																												BgL_l1z00_4715;
																																											BgL_l1z00_4715
																																												=
																																												STRING_LENGTH
																																												(
																																												((obj_t) BgL_sz00_122));
																																											if ((BgL_l1z00_4715 == 10L))
																																												{	/* SawC/code.scm 538 */
																																													int
																																														BgL_arg1282z00_4718;
																																													{	/* SawC/code.scm 538 */
																																														char
																																															*BgL_auxz00_7358;
																																														char
																																															*BgL_tmpz00_7355;
																																														BgL_auxz00_7358
																																															=
																																															BSTRING_TO_STRING
																																															(BGl_string2752z00zzsaw_c_codez00);
																																														BgL_tmpz00_7355
																																															=
																																															BSTRING_TO_STRING
																																															(
																																															((obj_t) BgL_sz00_122));
																																														BgL_arg1282z00_4718
																																															=
																																															memcmp
																																															(BgL_tmpz00_7355,
																																															BgL_auxz00_7358,
																																															BgL_l1z00_4715);
																																													}
																																													BgL_test3173z00_7350
																																														=
																																														(
																																														(long)
																																														(BgL_arg1282z00_4718)
																																														==
																																														0L);
																																												}
																																											else
																																												{	/* SawC/code.scm 538 */
																																													BgL_test3173z00_7350
																																														=
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																										if (BgL_test3173z00_7350)
																																											{	/* SawC/code.scm 538 */
																																												BGl_za2pushtraceemmitedza2z00zzsaw_c_codez00
																																													=
																																													(1L
																																													+
																																													BGl_za2pushtraceemmitedza2z00zzsaw_c_codez00);
																																												return
																																													BGl_string2753z00zzsaw_c_codez00;
																																											}
																																										else
																																											{	/* SawC/code.scm 541 */
																																												bool_t
																																													BgL_test3175z00_7364;
																																												{	/* SawC/code.scm 541 */
																																													long
																																														BgL_l1z00_4727;
																																													BgL_l1z00_4727
																																														=
																																														STRING_LENGTH
																																														(
																																														((obj_t) BgL_sz00_122));
																																													if ((BgL_l1z00_4727 == 18L))
																																														{	/* SawC/code.scm 541 */
																																															int
																																																BgL_arg1282z00_4730;
																																															{	/* SawC/code.scm 541 */
																																																char
																																																	*BgL_auxz00_7372;
																																																char
																																																	*BgL_tmpz00_7369;
																																																BgL_auxz00_7372
																																																	=
																																																	BSTRING_TO_STRING
																																																	(BGl_string2754z00zzsaw_c_codez00);
																																																BgL_tmpz00_7369
																																																	=
																																																	BSTRING_TO_STRING
																																																	(
																																																	((obj_t) BgL_sz00_122));
																																																BgL_arg1282z00_4730
																																																	=
																																																	memcmp
																																																	(BgL_tmpz00_7369,
																																																	BgL_auxz00_7372,
																																																	BgL_l1z00_4727);
																																															}
																																															BgL_test3175z00_7364
																																																=
																																																(
																																																(long)
																																																(BgL_arg1282z00_4730)
																																																==
																																																0L);
																																														}
																																													else
																																														{	/* SawC/code.scm 541 */
																																															BgL_test3175z00_7364
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																												if (BgL_test3175z00_7364)
																																													{	/* SawC/code.scm 541 */
																																														BGl_za2pushtraceemmitedza2z00zzsaw_c_codez00
																																															=
																																															(1L
																																															+
																																															BGl_za2pushtraceemmitedza2z00zzsaw_c_codez00);
																																														return
																																															BGl_string2755z00zzsaw_c_codez00;
																																													}
																																												else
																																													{	/* SawC/code.scm 544 */
																																														bool_t
																																															BgL_test3177z00_7378;
																																														{	/* SawC/code.scm 544 */
																																															long
																																																BgL_l1z00_4739;
																																															BgL_l1z00_4739
																																																=
																																																STRING_LENGTH
																																																(
																																																((obj_t) BgL_sz00_122));
																																															if ((BgL_l1z00_4739 == 11L))
																																																{	/* SawC/code.scm 544 */
																																																	int
																																																		BgL_arg1282z00_4742;
																																																	{	/* SawC/code.scm 544 */
																																																		char
																																																			*BgL_auxz00_7386;
																																																		char
																																																			*BgL_tmpz00_7383;
																																																		BgL_auxz00_7386
																																																			=
																																																			BSTRING_TO_STRING
																																																			(BGl_string2756z00zzsaw_c_codez00);
																																																		BgL_tmpz00_7383
																																																			=
																																																			BSTRING_TO_STRING
																																																			(
																																																			((obj_t) BgL_sz00_122));
																																																		BgL_arg1282z00_4742
																																																			=
																																																			memcmp
																																																			(BgL_tmpz00_7383,
																																																			BgL_auxz00_7386,
																																																			BgL_l1z00_4739);
																																																	}
																																																	BgL_test3177z00_7378
																																																		=
																																																		(
																																																		(long)
																																																		(BgL_arg1282z00_4742)
																																																		==
																																																		0L);
																																																}
																																															else
																																																{	/* SawC/code.scm 544 */
																																																	BgL_test3177z00_7378
																																																		=
																																																		(
																																																		(bool_t)
																																																		0);
																																																}
																																														}
																																														if (BgL_test3177z00_7378)
																																															{	/* SawC/code.scm 544 */
																																																if (BGl_za2countza2z00zzsaw_c_codez00)
																																																	{	/* SawC/code.scm 545 */
																																																		return
																																																			BGl_string2757z00zzsaw_c_codez00;
																																																	}
																																																else
																																																	{	/* SawC/code.scm 545 */
																																																		return
																																																			BgL_sz00_122;
																																																	}
																																															}
																																														else
																																															{	/* SawC/code.scm 547 */
																																																bool_t
																																																	BgL_test3180z00_7392;
																																																{	/* SawC/code.scm 547 */
																																																	long
																																																		BgL_l1z00_4750;
																																																	BgL_l1z00_4750
																																																		=
																																																		STRING_LENGTH
																																																		(
																																																		((obj_t) BgL_sz00_122));
																																																	if ((BgL_l1z00_4750 == 10L))
																																																		{	/* SawC/code.scm 547 */
																																																			int
																																																				BgL_arg1282z00_4753;
																																																			{	/* SawC/code.scm 547 */
																																																				char
																																																					*BgL_auxz00_7400;
																																																				char
																																																					*BgL_tmpz00_7397;
																																																				BgL_auxz00_7400
																																																					=
																																																					BSTRING_TO_STRING
																																																					(BGl_string2758z00zzsaw_c_codez00);
																																																				BgL_tmpz00_7397
																																																					=
																																																					BSTRING_TO_STRING
																																																					(
																																																					((obj_t) BgL_sz00_122));
																																																				BgL_arg1282z00_4753
																																																					=
																																																					memcmp
																																																					(BgL_tmpz00_7397,
																																																					BgL_auxz00_7400,
																																																					BgL_l1z00_4750);
																																																			}
																																																			BgL_test3180z00_7392
																																																				=
																																																				(
																																																				(long)
																																																				(BgL_arg1282z00_4753)
																																																				==
																																																				0L);
																																																		}
																																																	else
																																																		{	/* SawC/code.scm 547 */
																																																			BgL_test3180z00_7392
																																																				=
																																																				(
																																																				(bool_t)
																																																				0);
																																																		}
																																																}
																																																if (BgL_test3180z00_7392)
																																																	{	/* SawC/code.scm 547 */
																																																		return
																																																			BGl_string2759z00zzsaw_c_codez00;
																																																	}
																																																else
																																																	{	/* SawC/code.scm 547 */
																																																		return
																																																			BgL_sz00_122;
																																																	}
																																															}
																																													}
																																											}
																																									}
																																							}
																																					}
																																			}
																																	}
																															}
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* deep-mov? */
	bool_t BGl_deepzd2movzf3z21zzsaw_c_codez00(obj_t BgL_insz00_126)
	{
		{	/* SawC/code.scm 557 */
		BGl_deepzd2movzf3z21zzsaw_c_codez00:
			{	/* SawC/code.scm 558 */
				bool_t BgL__ortest_1200z00_2816;

				{	/* SawC/code.scm 558 */
					obj_t BgL_classz00_4759;

					BgL_classz00_4759 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_insz00_126))
						{	/* SawC/code.scm 558 */
							BgL_objectz00_bglt BgL_arg1807z00_4761;

							BgL_arg1807z00_4761 = (BgL_objectz00_bglt) (BgL_insz00_126);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawC/code.scm 558 */
									long BgL_idxz00_4767;

									BgL_idxz00_4767 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4761);
									BgL__ortest_1200z00_2816 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4767 + 1L)) == BgL_classz00_4759);
								}
							else
								{	/* SawC/code.scm 558 */
									bool_t BgL_res2617z00_4792;

									{	/* SawC/code.scm 558 */
										obj_t BgL_oclassz00_4775;

										{	/* SawC/code.scm 558 */
											obj_t BgL_arg1815z00_4783;
											long BgL_arg1816z00_4784;

											BgL_arg1815z00_4783 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawC/code.scm 558 */
												long BgL_arg1817z00_4785;

												BgL_arg1817z00_4785 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4761);
												BgL_arg1816z00_4784 =
													(BgL_arg1817z00_4785 - OBJECT_TYPE);
											}
											BgL_oclassz00_4775 =
												VECTOR_REF(BgL_arg1815z00_4783, BgL_arg1816z00_4784);
										}
										{	/* SawC/code.scm 558 */
											bool_t BgL__ortest_1115z00_4776;

											BgL__ortest_1115z00_4776 =
												(BgL_classz00_4759 == BgL_oclassz00_4775);
											if (BgL__ortest_1115z00_4776)
												{	/* SawC/code.scm 558 */
													BgL_res2617z00_4792 = BgL__ortest_1115z00_4776;
												}
											else
												{	/* SawC/code.scm 558 */
													long BgL_odepthz00_4777;

													{	/* SawC/code.scm 558 */
														obj_t BgL_arg1804z00_4778;

														BgL_arg1804z00_4778 = (BgL_oclassz00_4775);
														BgL_odepthz00_4777 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4778);
													}
													if ((1L < BgL_odepthz00_4777))
														{	/* SawC/code.scm 558 */
															obj_t BgL_arg1802z00_4780;

															{	/* SawC/code.scm 558 */
																obj_t BgL_arg1803z00_4781;

																BgL_arg1803z00_4781 = (BgL_oclassz00_4775);
																BgL_arg1802z00_4780 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4781,
																	1L);
															}
															BgL_res2617z00_4792 =
																(BgL_arg1802z00_4780 == BgL_classz00_4759);
														}
													else
														{	/* SawC/code.scm 558 */
															BgL_res2617z00_4792 = ((bool_t) 0);
														}
												}
										}
									}
									BgL__ortest_1200z00_2816 = BgL_res2617z00_4792;
								}
						}
					else
						{	/* SawC/code.scm 558 */
							BgL__ortest_1200z00_2816 = ((bool_t) 0);
						}
				}
				if (BgL__ortest_1200z00_2816)
					{	/* SawC/code.scm 558 */
						return BgL__ortest_1200z00_2816;
					}
				else
					{	/* SawC/code.scm 559 */
						bool_t BgL_test3187z00_7428;

						{	/* SawC/code.scm 559 */
							BgL_rtl_funz00_bglt BgL_arg2106z00_2820;

							BgL_arg2106z00_2820 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_126)))->BgL_funz00);
							{	/* SawC/code.scm 559 */
								obj_t BgL_classz00_4794;

								BgL_classz00_4794 = BGl_rtl_movz00zzsaw_defsz00;
								{	/* SawC/code.scm 559 */
									BgL_objectz00_bglt BgL_arg1807z00_4796;

									{	/* SawC/code.scm 559 */
										obj_t BgL_tmpz00_7431;

										BgL_tmpz00_7431 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2106z00_2820));
										BgL_arg1807z00_4796 =
											(BgL_objectz00_bglt) (BgL_tmpz00_7431);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawC/code.scm 559 */
											long BgL_idxz00_4802;

											BgL_idxz00_4802 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4796);
											BgL_test3187z00_7428 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4802 + 3L)) == BgL_classz00_4794);
										}
									else
										{	/* SawC/code.scm 559 */
											bool_t BgL_res2618z00_4827;

											{	/* SawC/code.scm 559 */
												obj_t BgL_oclassz00_4810;

												{	/* SawC/code.scm 559 */
													obj_t BgL_arg1815z00_4818;
													long BgL_arg1816z00_4819;

													BgL_arg1815z00_4818 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawC/code.scm 559 */
														long BgL_arg1817z00_4820;

														BgL_arg1817z00_4820 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4796);
														BgL_arg1816z00_4819 =
															(BgL_arg1817z00_4820 - OBJECT_TYPE);
													}
													BgL_oclassz00_4810 =
														VECTOR_REF(BgL_arg1815z00_4818,
														BgL_arg1816z00_4819);
												}
												{	/* SawC/code.scm 559 */
													bool_t BgL__ortest_1115z00_4811;

													BgL__ortest_1115z00_4811 =
														(BgL_classz00_4794 == BgL_oclassz00_4810);
													if (BgL__ortest_1115z00_4811)
														{	/* SawC/code.scm 559 */
															BgL_res2618z00_4827 = BgL__ortest_1115z00_4811;
														}
													else
														{	/* SawC/code.scm 559 */
															long BgL_odepthz00_4812;

															{	/* SawC/code.scm 559 */
																obj_t BgL_arg1804z00_4813;

																BgL_arg1804z00_4813 = (BgL_oclassz00_4810);
																BgL_odepthz00_4812 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4813);
															}
															if ((3L < BgL_odepthz00_4812))
																{	/* SawC/code.scm 559 */
																	obj_t BgL_arg1802z00_4815;

																	{	/* SawC/code.scm 559 */
																		obj_t BgL_arg1803z00_4816;

																		BgL_arg1803z00_4816 = (BgL_oclassz00_4810);
																		BgL_arg1802z00_4815 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4816, 3L);
																	}
																	BgL_res2618z00_4827 =
																		(BgL_arg1802z00_4815 == BgL_classz00_4794);
																}
															else
																{	/* SawC/code.scm 559 */
																	BgL_res2618z00_4827 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3187z00_7428 = BgL_res2618z00_4827;
										}
								}
							}
						}
						if (BgL_test3187z00_7428)
							{	/* SawC/code.scm 560 */
								obj_t BgL_arg2104z00_2818;

								{	/* SawC/code.scm 560 */
									obj_t BgL_pairz00_4829;

									BgL_pairz00_4829 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_insz00_126)))->BgL_argsz00);
									BgL_arg2104z00_2818 = CAR(BgL_pairz00_4829);
								}
								{
									obj_t BgL_insz00_7457;

									BgL_insz00_7457 = BgL_arg2104z00_2818;
									BgL_insz00_126 = BgL_insz00_7457;
									goto BGl_deepzd2movzf3z21zzsaw_c_codez00;
								}
							}
						else
							{	/* SawC/code.scm 559 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* multiple-evaluation */
	bool_t BGl_multiplezd2evaluationzd2zzsaw_c_codez00(obj_t BgL_insz00_127,
		obj_t BgL_treez00_128)
	{
		{	/* SawC/code.scm 562 */
			{	/* SawC/code.scm 563 */
				BgL_rtl_funz00_bglt BgL_funz00_2821;

				BgL_funz00_2821 =
					(((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_insz00_127)))->BgL_funz00);
				{	/* SawC/code.scm 565 */
					bool_t BgL__ortest_1202z00_2822;

					{	/* SawC/code.scm 565 */
						obj_t BgL_classz00_4831;

						BgL_classz00_4831 = BGl_rtl_vallocz00zzsaw_defsz00;
						{	/* SawC/code.scm 565 */
							BgL_objectz00_bglt BgL_arg1807z00_4833;

							{	/* SawC/code.scm 565 */
								obj_t BgL_tmpz00_7460;

								BgL_tmpz00_7460 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2821));
								BgL_arg1807z00_4833 = (BgL_objectz00_bglt) (BgL_tmpz00_7460);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawC/code.scm 565 */
									long BgL_idxz00_4839;

									BgL_idxz00_4839 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4833);
									BgL__ortest_1202z00_2822 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4839 + 3L)) == BgL_classz00_4831);
								}
							else
								{	/* SawC/code.scm 565 */
									bool_t BgL_res2619z00_4864;

									{	/* SawC/code.scm 565 */
										obj_t BgL_oclassz00_4847;

										{	/* SawC/code.scm 565 */
											obj_t BgL_arg1815z00_4855;
											long BgL_arg1816z00_4856;

											BgL_arg1815z00_4855 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawC/code.scm 565 */
												long BgL_arg1817z00_4857;

												BgL_arg1817z00_4857 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4833);
												BgL_arg1816z00_4856 =
													(BgL_arg1817z00_4857 - OBJECT_TYPE);
											}
											BgL_oclassz00_4847 =
												VECTOR_REF(BgL_arg1815z00_4855, BgL_arg1816z00_4856);
										}
										{	/* SawC/code.scm 565 */
											bool_t BgL__ortest_1115z00_4848;

											BgL__ortest_1115z00_4848 =
												(BgL_classz00_4831 == BgL_oclassz00_4847);
											if (BgL__ortest_1115z00_4848)
												{	/* SawC/code.scm 565 */
													BgL_res2619z00_4864 = BgL__ortest_1115z00_4848;
												}
											else
												{	/* SawC/code.scm 565 */
													long BgL_odepthz00_4849;

													{	/* SawC/code.scm 565 */
														obj_t BgL_arg1804z00_4850;

														BgL_arg1804z00_4850 = (BgL_oclassz00_4847);
														BgL_odepthz00_4849 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4850);
													}
													if ((3L < BgL_odepthz00_4849))
														{	/* SawC/code.scm 565 */
															obj_t BgL_arg1802z00_4852;

															{	/* SawC/code.scm 565 */
																obj_t BgL_arg1803z00_4853;

																BgL_arg1803z00_4853 = (BgL_oclassz00_4847);
																BgL_arg1802z00_4852 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4853,
																	3L);
															}
															BgL_res2619z00_4864 =
																(BgL_arg1802z00_4852 == BgL_classz00_4831);
														}
													else
														{	/* SawC/code.scm 565 */
															BgL_res2619z00_4864 = ((bool_t) 0);
														}
												}
										}
									}
									BgL__ortest_1202z00_2822 = BgL_res2619z00_4864;
								}
						}
					}
					if (BgL__ortest_1202z00_2822)
						{	/* SawC/code.scm 565 */
							return BgL__ortest_1202z00_2822;
						}
					else
						{	/* SawC/code.scm 566 */
							bool_t BgL__ortest_1203z00_2823;

							{	/* SawC/code.scm 566 */
								bool_t BgL_test3195z00_7484;

								{	/* SawC/code.scm 566 */
									obj_t BgL_classz00_4865;

									BgL_classz00_4865 = BGl_rtl_lightfuncallz00zzsaw_defsz00;
									{	/* SawC/code.scm 566 */
										BgL_objectz00_bglt BgL_arg1807z00_4867;

										{	/* SawC/code.scm 566 */
											obj_t BgL_tmpz00_7485;

											BgL_tmpz00_7485 =
												((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2821));
											BgL_arg1807z00_4867 =
												(BgL_objectz00_bglt) (BgL_tmpz00_7485);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawC/code.scm 566 */
												long BgL_idxz00_4873;

												BgL_idxz00_4873 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4867);
												BgL_test3195z00_7484 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4873 + 2L)) == BgL_classz00_4865);
											}
										else
											{	/* SawC/code.scm 566 */
												bool_t BgL_res2620z00_4898;

												{	/* SawC/code.scm 566 */
													obj_t BgL_oclassz00_4881;

													{	/* SawC/code.scm 566 */
														obj_t BgL_arg1815z00_4889;
														long BgL_arg1816z00_4890;

														BgL_arg1815z00_4889 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawC/code.scm 566 */
															long BgL_arg1817z00_4891;

															BgL_arg1817z00_4891 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4867);
															BgL_arg1816z00_4890 =
																(BgL_arg1817z00_4891 - OBJECT_TYPE);
														}
														BgL_oclassz00_4881 =
															VECTOR_REF(BgL_arg1815z00_4889,
															BgL_arg1816z00_4890);
													}
													{	/* SawC/code.scm 566 */
														bool_t BgL__ortest_1115z00_4882;

														BgL__ortest_1115z00_4882 =
															(BgL_classz00_4865 == BgL_oclassz00_4881);
														if (BgL__ortest_1115z00_4882)
															{	/* SawC/code.scm 566 */
																BgL_res2620z00_4898 = BgL__ortest_1115z00_4882;
															}
														else
															{	/* SawC/code.scm 566 */
																long BgL_odepthz00_4883;

																{	/* SawC/code.scm 566 */
																	obj_t BgL_arg1804z00_4884;

																	BgL_arg1804z00_4884 = (BgL_oclassz00_4881);
																	BgL_odepthz00_4883 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4884);
																}
																if ((2L < BgL_odepthz00_4883))
																	{	/* SawC/code.scm 566 */
																		obj_t BgL_arg1802z00_4886;

																		{	/* SawC/code.scm 566 */
																			obj_t BgL_arg1803z00_4887;

																			BgL_arg1803z00_4887 =
																				(BgL_oclassz00_4881);
																			BgL_arg1802z00_4886 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4887, 2L);
																		}
																		BgL_res2620z00_4898 =
																			(BgL_arg1802z00_4886 ==
																			BgL_classz00_4865);
																	}
																else
																	{	/* SawC/code.scm 566 */
																		BgL_res2620z00_4898 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3195z00_7484 = BgL_res2620z00_4898;
											}
									}
								}
								if (BgL_test3195z00_7484)
									{	/* SawC/code.scm 567 */
										obj_t BgL_tmpz00_7508;

										{	/* SawC/code.scm 567 */
											obj_t BgL_pairz00_4900;

											BgL_pairz00_4900 =
												(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_insz00_127)))->
												BgL_argsz00);
											BgL_tmpz00_7508 = CAR(BgL_pairz00_4900);
										}
										BgL__ortest_1203z00_2823 =
											(BgL_tmpz00_7508 ==
											(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_treez00_128)))->
												BgL_destz00));
									}
								else
									{	/* SawC/code.scm 566 */
										BgL__ortest_1203z00_2823 = ((bool_t) 0);
									}
							}
							if (BgL__ortest_1203z00_2823)
								{	/* SawC/code.scm 566 */
									return BgL__ortest_1203z00_2823;
								}
							else
								{	/* SawC/code.scm 569 */
									bool_t BgL__ortest_1204z00_2824;

									{	/* SawC/code.scm 569 */
										bool_t BgL_test3200z00_7516;

										{	/* SawC/code.scm 569 */
											obj_t BgL_classz00_4902;

											BgL_classz00_4902 = BGl_rtl_funcallz00zzsaw_defsz00;
											{	/* SawC/code.scm 569 */
												BgL_objectz00_bglt BgL_arg1807z00_4904;

												{	/* SawC/code.scm 569 */
													obj_t BgL_tmpz00_7517;

													BgL_tmpz00_7517 =
														((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2821));
													BgL_arg1807z00_4904 =
														(BgL_objectz00_bglt) (BgL_tmpz00_7517);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawC/code.scm 569 */
														long BgL_idxz00_4910;

														BgL_idxz00_4910 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4904);
														BgL_test3200z00_7516 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_4910 + 2L)) == BgL_classz00_4902);
													}
												else
													{	/* SawC/code.scm 569 */
														bool_t BgL_res2621z00_4935;

														{	/* SawC/code.scm 569 */
															obj_t BgL_oclassz00_4918;

															{	/* SawC/code.scm 569 */
																obj_t BgL_arg1815z00_4926;
																long BgL_arg1816z00_4927;

																BgL_arg1815z00_4926 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawC/code.scm 569 */
																	long BgL_arg1817z00_4928;

																	BgL_arg1817z00_4928 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4904);
																	BgL_arg1816z00_4927 =
																		(BgL_arg1817z00_4928 - OBJECT_TYPE);
																}
																BgL_oclassz00_4918 =
																	VECTOR_REF(BgL_arg1815z00_4926,
																	BgL_arg1816z00_4927);
															}
															{	/* SawC/code.scm 569 */
																bool_t BgL__ortest_1115z00_4919;

																BgL__ortest_1115z00_4919 =
																	(BgL_classz00_4902 == BgL_oclassz00_4918);
																if (BgL__ortest_1115z00_4919)
																	{	/* SawC/code.scm 569 */
																		BgL_res2621z00_4935 =
																			BgL__ortest_1115z00_4919;
																	}
																else
																	{	/* SawC/code.scm 569 */
																		long BgL_odepthz00_4920;

																		{	/* SawC/code.scm 569 */
																			obj_t BgL_arg1804z00_4921;

																			BgL_arg1804z00_4921 =
																				(BgL_oclassz00_4918);
																			BgL_odepthz00_4920 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_4921);
																		}
																		if ((2L < BgL_odepthz00_4920))
																			{	/* SawC/code.scm 569 */
																				obj_t BgL_arg1802z00_4923;

																				{	/* SawC/code.scm 569 */
																					obj_t BgL_arg1803z00_4924;

																					BgL_arg1803z00_4924 =
																						(BgL_oclassz00_4918);
																					BgL_arg1802z00_4923 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_4924, 2L);
																				}
																				BgL_res2621z00_4935 =
																					(BgL_arg1802z00_4923 ==
																					BgL_classz00_4902);
																			}
																		else
																			{	/* SawC/code.scm 569 */
																				BgL_res2621z00_4935 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3200z00_7516 = BgL_res2621z00_4935;
													}
											}
										}
										if (BgL_test3200z00_7516)
											{	/* SawC/code.scm 570 */
												obj_t BgL_tmpz00_7540;

												{	/* SawC/code.scm 570 */
													obj_t BgL_pairz00_4937;

													BgL_pairz00_4937 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_insz00_127)))->
														BgL_argsz00);
													BgL_tmpz00_7540 = CAR(BgL_pairz00_4937);
												}
												BgL__ortest_1204z00_2824 =
													(BgL_tmpz00_7540 ==
													(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_treez00_128)))->
														BgL_destz00));
											}
										else
											{	/* SawC/code.scm 569 */
												BgL__ortest_1204z00_2824 = ((bool_t) 0);
											}
									}
									if (BgL__ortest_1204z00_2824)
										{	/* SawC/code.scm 569 */
											return BgL__ortest_1204z00_2824;
										}
									else
										{	/* SawC/code.scm 572 */
											bool_t BgL_test3205z00_7548;

											{	/* SawC/code.scm 572 */
												obj_t BgL_classz00_4939;

												BgL_classz00_4939 = BGl_rtl_callz00zzsaw_defsz00;
												{	/* SawC/code.scm 572 */
													BgL_objectz00_bglt BgL_arg1807z00_4941;

													{	/* SawC/code.scm 572 */
														obj_t BgL_tmpz00_7549;

														BgL_tmpz00_7549 =
															((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2821));
														BgL_arg1807z00_4941 =
															(BgL_objectz00_bglt) (BgL_tmpz00_7549);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawC/code.scm 572 */
															long BgL_idxz00_4947;

															BgL_idxz00_4947 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4941);
															BgL_test3205z00_7548 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4947 + 2L)) == BgL_classz00_4939);
														}
													else
														{	/* SawC/code.scm 572 */
															bool_t BgL_res2622z00_4972;

															{	/* SawC/code.scm 572 */
																obj_t BgL_oclassz00_4955;

																{	/* SawC/code.scm 572 */
																	obj_t BgL_arg1815z00_4963;
																	long BgL_arg1816z00_4964;

																	BgL_arg1815z00_4963 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawC/code.scm 572 */
																		long BgL_arg1817z00_4965;

																		BgL_arg1817z00_4965 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4941);
																		BgL_arg1816z00_4964 =
																			(BgL_arg1817z00_4965 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4955 =
																		VECTOR_REF(BgL_arg1815z00_4963,
																		BgL_arg1816z00_4964);
																}
																{	/* SawC/code.scm 572 */
																	bool_t BgL__ortest_1115z00_4956;

																	BgL__ortest_1115z00_4956 =
																		(BgL_classz00_4939 == BgL_oclassz00_4955);
																	if (BgL__ortest_1115z00_4956)
																		{	/* SawC/code.scm 572 */
																			BgL_res2622z00_4972 =
																				BgL__ortest_1115z00_4956;
																		}
																	else
																		{	/* SawC/code.scm 572 */
																			long BgL_odepthz00_4957;

																			{	/* SawC/code.scm 572 */
																				obj_t BgL_arg1804z00_4958;

																				BgL_arg1804z00_4958 =
																					(BgL_oclassz00_4955);
																				BgL_odepthz00_4957 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4958);
																			}
																			if ((2L < BgL_odepthz00_4957))
																				{	/* SawC/code.scm 572 */
																					obj_t BgL_arg1802z00_4960;

																					{	/* SawC/code.scm 572 */
																						obj_t BgL_arg1803z00_4961;

																						BgL_arg1803z00_4961 =
																							(BgL_oclassz00_4955);
																						BgL_arg1802z00_4960 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4961, 2L);
																					}
																					BgL_res2622z00_4972 =
																						(BgL_arg1802z00_4960 ==
																						BgL_classz00_4939);
																				}
																			else
																				{	/* SawC/code.scm 572 */
																					BgL_res2622z00_4972 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3205z00_7548 = BgL_res2622z00_4972;
														}
												}
											}
											if (BgL_test3205z00_7548)
												{	/* SawC/code.scm 572 */
													if (BGl_globalzd2argszd2safezf3zf3zzast_varz00(
															(((BgL_rtl_callz00_bglt) COBJECT(
																		((BgL_rtl_callz00_bglt) BgL_funz00_2821)))->
																BgL_varz00)))
														{	/* SawC/code.scm 584 */
															return ((bool_t) 0);
														}
													else
														{	/* SawC/code.scm 584 */
															return ((bool_t) 1);
														}
												}
											else
												{	/* SawC/code.scm 572 */
													return ((bool_t) 0);
												}
										}
								}
						}
				}
			}
		}

	}



/* emit-pragma */
	obj_t BGl_emitzd2pragmazd2zzsaw_c_codez00(obj_t BgL_formatz00_130,
		obj_t BgL_argsz00_131)
	{
		{	/* SawC/code.scm 589 */
			if (NULLP(BgL_argsz00_131))
				{	/* SawC/code.scm 590 */
					return
						bgl_display_obj(BgL_formatz00_130,
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
			else
				{	/* SawC/code.scm 592 */
					obj_t BgL_sportz00_2837;

					{	/* SawC/code.scm 592 */
						long BgL_endz00_3122;

						BgL_endz00_3122 = STRING_LENGTH(BgL_formatz00_130);
						{	/* SawC/code.scm 592 */

							BgL_sportz00_2837 =
								BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
								(BgL_formatz00_130, BINT(0L), BINT(BgL_endz00_3122));
					}}
					{	/* SawC/code.scm 592 */
						obj_t BgL_argsz00_2838;

						BgL_argsz00_2838 =
							BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(BgL_argsz00_131);
						{
							obj_t BgL_iportz00_2841;

							{	/* SawC/code.scm 594 */

								BgL_iportz00_2841 = BgL_sportz00_2837;
								{
									obj_t BgL_iportz00_2876;
									long BgL_lastzd2matchzd2_2877;
									long BgL_forwardz00_2878;
									long BgL_bufposz00_2879;
									obj_t BgL_iportz00_2889;
									long BgL_lastzd2matchzd2_2890;
									long BgL_forwardz00_2891;
									long BgL_bufposz00_2892;
									obj_t BgL_iportz00_2912;
									long BgL_lastzd2matchzd2_2913;
									long BgL_forwardz00_2914;
									long BgL_bufposz00_2915;
									obj_t BgL_iportz00_2925;
									long BgL_lastzd2matchzd2_2926;
									long BgL_forwardz00_2927;
									long BgL_bufposz00_2928;
									obj_t BgL_iportz00_2938;
									long BgL_lastzd2matchzd2_2939;
									long BgL_forwardz00_2940;
									long BgL_bufposz00_2941;

								BgL_zc3z04anonymousza32246ze3z87_3071:
									RGC_START_MATCH(BgL_iportz00_2841);
									{	/* SawC/code.scm 594 */
										long BgL_matchz00_3072;

										{	/* SawC/code.scm 594 */
											long BgL_arg2251z00_3084;
											long BgL_arg2252z00_3085;

											BgL_arg2251z00_3084 =
												RGC_BUFFER_FORWARD(BgL_iportz00_2841);
											BgL_arg2252z00_3085 =
												RGC_BUFFER_BUFPOS(BgL_iportz00_2841);
											BgL_iportz00_2925 = BgL_iportz00_2841;
											BgL_lastzd2matchzd2_2926 = 3L;
											BgL_forwardz00_2927 = BgL_arg2251z00_3084;
											BgL_bufposz00_2928 = BgL_arg2252z00_3085;
										BgL_zc3z04anonymousza32144ze3z87_2929:
											if ((BgL_forwardz00_2927 == BgL_bufposz00_2928))
												{	/* SawC/code.scm 594 */
													if (rgc_fill_buffer(BgL_iportz00_2925))
														{	/* SawC/code.scm 594 */
															long BgL_arg2147z00_2932;
															long BgL_arg2148z00_2933;

															BgL_arg2147z00_2932 =
																RGC_BUFFER_FORWARD(BgL_iportz00_2925);
															BgL_arg2148z00_2933 =
																RGC_BUFFER_BUFPOS(BgL_iportz00_2925);
															{
																long BgL_bufposz00_7594;
																long BgL_forwardz00_7593;

																BgL_forwardz00_7593 = BgL_arg2147z00_2932;
																BgL_bufposz00_7594 = BgL_arg2148z00_2933;
																BgL_bufposz00_2928 = BgL_bufposz00_7594;
																BgL_forwardz00_2927 = BgL_forwardz00_7593;
																goto BgL_zc3z04anonymousza32144ze3z87_2929;
															}
														}
													else
														{	/* SawC/code.scm 594 */
															BgL_matchz00_3072 = BgL_lastzd2matchzd2_2926;
														}
												}
											else
												{	/* SawC/code.scm 594 */
													int BgL_curz00_2934;

													BgL_curz00_2934 =
														RGC_BUFFER_GET_CHAR(BgL_iportz00_2925,
														BgL_forwardz00_2927);
													{	/* SawC/code.scm 594 */

														if (((long) (BgL_curz00_2934) == 36L))
															{	/* SawC/code.scm 594 */
																BgL_iportz00_2889 = BgL_iportz00_2925;
																BgL_lastzd2matchzd2_2890 =
																	BgL_lastzd2matchzd2_2926;
																BgL_forwardz00_2891 =
																	(1L + BgL_forwardz00_2927);
																BgL_bufposz00_2892 = BgL_bufposz00_2928;
															BgL_zc3z04anonymousza32124ze3z87_2893:
																{	/* SawC/code.scm 594 */
																	long BgL_newzd2matchzd2_2894;

																	RGC_STOP_MATCH(BgL_iportz00_2889,
																		BgL_forwardz00_2891);
																	BgL_newzd2matchzd2_2894 = 3L;
																	if (
																		(BgL_forwardz00_2891 == BgL_bufposz00_2892))
																		{	/* SawC/code.scm 594 */
																			if (rgc_fill_buffer(BgL_iportz00_2889))
																				{	/* SawC/code.scm 594 */
																					long BgL_arg2127z00_2897;
																					long BgL_arg2129z00_2898;

																					BgL_arg2127z00_2897 =
																						RGC_BUFFER_FORWARD
																						(BgL_iportz00_2889);
																					BgL_arg2129z00_2898 =
																						RGC_BUFFER_BUFPOS
																						(BgL_iportz00_2889);
																					{
																						long BgL_bufposz00_7607;
																						long BgL_forwardz00_7606;

																						BgL_forwardz00_7606 =
																							BgL_arg2127z00_2897;
																						BgL_bufposz00_7607 =
																							BgL_arg2129z00_2898;
																						BgL_bufposz00_2892 =
																							BgL_bufposz00_7607;
																						BgL_forwardz00_2891 =
																							BgL_forwardz00_7606;
																						goto
																							BgL_zc3z04anonymousza32124ze3z87_2893;
																					}
																				}
																			else
																				{	/* SawC/code.scm 594 */
																					BgL_matchz00_3072 =
																						BgL_newzd2matchzd2_2894;
																				}
																		}
																	else
																		{	/* SawC/code.scm 594 */
																			int BgL_curz00_2899;

																			BgL_curz00_2899 =
																				RGC_BUFFER_GET_CHAR(BgL_iportz00_2889,
																				BgL_forwardz00_2891);
																			{	/* SawC/code.scm 594 */

																				{	/* SawC/code.scm 594 */
																					bool_t BgL_test3216z00_7609;

																					if (((long) (BgL_curz00_2899) >= 48L))
																						{	/* SawC/code.scm 594 */
																							BgL_test3216z00_7609 =
																								(
																								(long) (BgL_curz00_2899) < 58L);
																						}
																					else
																						{	/* SawC/code.scm 594 */
																							BgL_test3216z00_7609 =
																								((bool_t) 0);
																						}
																					if (BgL_test3216z00_7609)
																						{	/* SawC/code.scm 594 */
																							BgL_iportz00_2938 =
																								BgL_iportz00_2889;
																							BgL_lastzd2matchzd2_2939 =
																								BgL_newzd2matchzd2_2894;
																							BgL_forwardz00_2940 =
																								(1L + BgL_forwardz00_2891);
																							BgL_bufposz00_2941 =
																								BgL_bufposz00_2892;
																						BgL_zc3z04anonymousza32152ze3z87_2942:
																							{	/* SawC/code.scm 594 */
																								long BgL_newzd2matchzd2_2943;

																								RGC_STOP_MATCH
																									(BgL_iportz00_2938,
																									BgL_forwardz00_2940);
																								BgL_newzd2matchzd2_2943 = 0L;
																								if (
																									(BgL_forwardz00_2940 ==
																										BgL_bufposz00_2941))
																									{	/* SawC/code.scm 594 */
																										if (rgc_fill_buffer
																											(BgL_iportz00_2938))
																											{	/* SawC/code.scm 594 */
																												long
																													BgL_arg2155z00_2946;
																												long
																													BgL_arg2156z00_2947;
																												BgL_arg2155z00_2946 =
																													RGC_BUFFER_FORWARD
																													(BgL_iportz00_2938);
																												BgL_arg2156z00_2947 =
																													RGC_BUFFER_BUFPOS
																													(BgL_iportz00_2938);
																												{
																													long
																														BgL_bufposz00_7623;
																													long
																														BgL_forwardz00_7622;
																													BgL_forwardz00_7622 =
																														BgL_arg2155z00_2946;
																													BgL_bufposz00_7623 =
																														BgL_arg2156z00_2947;
																													BgL_bufposz00_2941 =
																														BgL_bufposz00_7623;
																													BgL_forwardz00_2940 =
																														BgL_forwardz00_7622;
																													goto
																														BgL_zc3z04anonymousza32152ze3z87_2942;
																												}
																											}
																										else
																											{	/* SawC/code.scm 594 */
																												BgL_matchz00_3072 =
																													BgL_newzd2matchzd2_2943;
																											}
																									}
																								else
																									{	/* SawC/code.scm 594 */
																										int BgL_curz00_2948;

																										BgL_curz00_2948 =
																											RGC_BUFFER_GET_CHAR
																											(BgL_iportz00_2938,
																											BgL_forwardz00_2940);
																										{	/* SawC/code.scm 594 */

																											{	/* SawC/code.scm 594 */
																												bool_t
																													BgL_test3220z00_7625;
																												if (((long)
																														(BgL_curz00_2948) >=
																														48L))
																													{	/* SawC/code.scm 594 */
																														BgL_test3220z00_7625
																															=
																															((long)
																															(BgL_curz00_2948)
																															< 58L);
																													}
																												else
																													{	/* SawC/code.scm 594 */
																														BgL_test3220z00_7625
																															= ((bool_t) 0);
																													}
																												if (BgL_test3220z00_7625)
																													{
																														long
																															BgL_forwardz00_7632;
																														long
																															BgL_lastzd2matchzd2_7631;
																														BgL_lastzd2matchzd2_7631
																															=
																															BgL_newzd2matchzd2_2943;
																														BgL_forwardz00_7632
																															=
																															(1L +
																															BgL_forwardz00_2940);
																														BgL_forwardz00_2940
																															=
																															BgL_forwardz00_7632;
																														BgL_lastzd2matchzd2_2939
																															=
																															BgL_lastzd2matchzd2_7631;
																														goto
																															BgL_zc3z04anonymousza32152ze3z87_2942;
																													}
																												else
																													{	/* SawC/code.scm 594 */
																														BgL_matchz00_3072 =
																															BgL_newzd2matchzd2_2943;
																													}
																											}
																										}
																									}
																							}
																						}
																					else
																						{	/* SawC/code.scm 594 */
																							if (
																								((long) (BgL_curz00_2899) ==
																									36L))
																								{	/* SawC/code.scm 594 */
																									long BgL_arg2134z00_2904;

																									BgL_arg2134z00_2904 =
																										(1L + BgL_forwardz00_2891);
																									{	/* SawC/code.scm 594 */
																										long
																											BgL_newzd2matchzd2_5002;
																										RGC_STOP_MATCH
																											(BgL_iportz00_2889,
																											BgL_arg2134z00_2904);
																										BgL_newzd2matchzd2_5002 =
																											2L;
																										BgL_matchz00_3072 =
																											BgL_newzd2matchzd2_5002;
																								}}
																							else
																								{	/* SawC/code.scm 594 */
																									BgL_matchz00_3072 =
																										BgL_newzd2matchzd2_2894;
																								}
																						}
																				}
																			}
																		}
																}
															}
														else
															{	/* SawC/code.scm 594 */
																BgL_iportz00_2912 = BgL_iportz00_2925;
																BgL_lastzd2matchzd2_2913 =
																	BgL_lastzd2matchzd2_2926;
																BgL_forwardz00_2914 =
																	(1L + BgL_forwardz00_2927);
																BgL_bufposz00_2915 = BgL_bufposz00_2928;
															BgL_zc3z04anonymousza32136ze3z87_2916:
																{	/* SawC/code.scm 594 */
																	long BgL_newzd2matchzd2_2917;

																	RGC_STOP_MATCH(BgL_iportz00_2912,
																		BgL_forwardz00_2914);
																	BgL_newzd2matchzd2_2917 = 1L;
																	if (
																		(BgL_forwardz00_2914 == BgL_bufposz00_2915))
																		{	/* SawC/code.scm 594 */
																			if (rgc_fill_buffer(BgL_iportz00_2912))
																				{	/* SawC/code.scm 594 */
																					long BgL_arg2139z00_2920;
																					long BgL_arg2141z00_2921;

																					BgL_arg2139z00_2920 =
																						RGC_BUFFER_FORWARD
																						(BgL_iportz00_2912);
																					BgL_arg2141z00_2921 =
																						RGC_BUFFER_BUFPOS
																						(BgL_iportz00_2912);
																					{
																						long BgL_bufposz00_7649;
																						long BgL_forwardz00_7648;

																						BgL_forwardz00_7648 =
																							BgL_arg2139z00_2920;
																						BgL_bufposz00_7649 =
																							BgL_arg2141z00_2921;
																						BgL_bufposz00_2915 =
																							BgL_bufposz00_7649;
																						BgL_forwardz00_2914 =
																							BgL_forwardz00_7648;
																						goto
																							BgL_zc3z04anonymousza32136ze3z87_2916;
																					}
																				}
																			else
																				{	/* SawC/code.scm 594 */
																					BgL_matchz00_3072 =
																						BgL_newzd2matchzd2_2917;
																				}
																		}
																	else
																		{	/* SawC/code.scm 594 */
																			int BgL_curz00_2922;

																			BgL_curz00_2922 =
																				RGC_BUFFER_GET_CHAR(BgL_iportz00_2912,
																				BgL_forwardz00_2914);
																			{	/* SawC/code.scm 594 */

																				if (((long) (BgL_curz00_2922) == 36L))
																					{	/* SawC/code.scm 594 */
																						BgL_matchz00_3072 =
																							BgL_newzd2matchzd2_2917;
																					}
																				else
																					{	/* SawC/code.scm 594 */
																						BgL_iportz00_2876 =
																							BgL_iportz00_2912;
																						BgL_lastzd2matchzd2_2877 =
																							BgL_newzd2matchzd2_2917;
																						BgL_forwardz00_2878 =
																							(1L + BgL_forwardz00_2914);
																						BgL_bufposz00_2879 =
																							BgL_bufposz00_2915;
																					BgL_zc3z04anonymousza32117ze3z87_2880:
																						{	/* SawC/code.scm 594 */
																							long BgL_newzd2matchzd2_2881;

																							RGC_STOP_MATCH(BgL_iportz00_2876,
																								BgL_forwardz00_2878);
																							BgL_newzd2matchzd2_2881 = 1L;
																							if (
																								(BgL_forwardz00_2878 ==
																									BgL_bufposz00_2879))
																								{	/* SawC/code.scm 594 */
																									if (rgc_fill_buffer
																										(BgL_iportz00_2876))
																										{	/* SawC/code.scm 594 */
																											long BgL_arg2120z00_2884;
																											long BgL_arg2121z00_2885;

																											BgL_arg2120z00_2884 =
																												RGC_BUFFER_FORWARD
																												(BgL_iportz00_2876);
																											BgL_arg2121z00_2885 =
																												RGC_BUFFER_BUFPOS
																												(BgL_iportz00_2876);
																											{
																												long BgL_bufposz00_7662;
																												long
																													BgL_forwardz00_7661;
																												BgL_forwardz00_7661 =
																													BgL_arg2120z00_2884;
																												BgL_bufposz00_7662 =
																													BgL_arg2121z00_2885;
																												BgL_bufposz00_2879 =
																													BgL_bufposz00_7662;
																												BgL_forwardz00_2878 =
																													BgL_forwardz00_7661;
																												goto
																													BgL_zc3z04anonymousza32117ze3z87_2880;
																											}
																										}
																									else
																										{	/* SawC/code.scm 594 */
																											BgL_matchz00_3072 =
																												BgL_newzd2matchzd2_2881;
																										}
																								}
																							else
																								{	/* SawC/code.scm 594 */
																									int BgL_curz00_2886;

																									BgL_curz00_2886 =
																										RGC_BUFFER_GET_CHAR
																										(BgL_iportz00_2876,
																										BgL_forwardz00_2878);
																									{	/* SawC/code.scm 594 */

																										if (
																											((long) (BgL_curz00_2886)
																												== 36L))
																											{	/* SawC/code.scm 594 */
																												BgL_matchz00_3072 =
																													BgL_newzd2matchzd2_2881;
																											}
																										else
																											{
																												long
																													BgL_forwardz00_7668;
																												long
																													BgL_lastzd2matchzd2_7667;
																												BgL_lastzd2matchzd2_7667
																													=
																													BgL_newzd2matchzd2_2881;
																												BgL_forwardz00_7668 =
																													(1L +
																													BgL_forwardz00_2878);
																												BgL_forwardz00_2878 =
																													BgL_forwardz00_7668;
																												BgL_lastzd2matchzd2_2877
																													=
																													BgL_lastzd2matchzd2_7667;
																												goto
																													BgL_zc3z04anonymousza32117ze3z87_2880;
																											}
																									}
																								}
																						}
																					}
																			}
																		}
																}
															}
													}
												}
										}
										RGC_SET_FILEPOS(BgL_iportz00_2841);
										{

											switch (BgL_matchz00_3072)
												{
												case 3L:

													{	/* SawC/code.scm 594 */
														bool_t BgL_test3229z00_7673;

														{	/* SawC/code.scm 594 */
															long BgL_arg2240z00_3062;

															BgL_arg2240z00_3062 =
																RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_2841);
															BgL_test3229z00_7673 =
																(BgL_arg2240z00_3062 == 0L);
														}
														if (BgL_test3229z00_7673)
															{	/* SawC/code.scm 594 */
																BEOF;
															}
														else
															{	/* SawC/code.scm 594 */
																BCHAR(RGC_BUFFER_CHARACTER(BgL_iportz00_2841));
															}
													}
													break;
												case 2L:

													bgl_display_string(BGl_string2762z00zzsaw_c_codez00,
														BGl_za2czd2portza2zd2zzbackend_c_emitz00);
													goto BgL_zc3z04anonymousza32246ze3z87_3071;
													break;
												case 1L:

													{	/* SawC/code.scm 603 */
														obj_t BgL_arg2247z00_3075;

														BgL_arg2247z00_3075 =
															BGl_thezd2stringze70z35zzsaw_c_codez00
															(BgL_iportz00_2841);
														bgl_display_obj(BgL_arg2247z00_3075,
															BGl_za2czd2portza2zd2zzbackend_c_emitz00);
													}
													goto BgL_zc3z04anonymousza32246ze3z87_3071;
													break;
												case 0L:

													{	/* SawC/code.scm 596 */
														obj_t BgL_strz00_3076;

														BgL_strz00_3076 =
															BGl_thezd2stringze70z35zzsaw_c_codez00
															(BgL_iportz00_2841);
														{	/* SawC/code.scm 596 */
															long BgL_lenz00_3077;

															BgL_lenz00_3077 =
																RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_2841);
															{	/* SawC/code.scm 597 */
																obj_t BgL_indexz00_3078;

																{	/* SawC/code.scm 599 */
																	obj_t BgL_arg2250z00_3081;

																	BgL_arg2250z00_3081 =
																		c_substring(BgL_strz00_3076, 1L,
																		BgL_lenz00_3077);
																	{	/* SawC/code.scm 598 */

																		BgL_indexz00_3078 =
																			BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
																			(BgL_arg2250z00_3081, BINT(10L));
																}}
																{	/* SawC/code.scm 598 */

																	BGl_genzd2regzd2zzsaw_c_codez00(VECTOR_REF
																		(BgL_argsz00_2838,
																			((long) CINT(BgL_indexz00_3078) - 1L)));
																	goto BgL_zc3z04anonymousza32246ze3z87_3071;
																}
															}
														}
													}
													break;
												default:
													BGl_errorz00zz__errorz00
														(BGl_string2760z00zzsaw_c_codez00,
														BGl_string2761z00zzsaw_c_codez00,
														BINT(BgL_matchz00_3072));
												}
										}
									}
								}
								return BTRUE;
							}
						}
					}
				}
		}

	}



/* the-string~0 */
	obj_t BGl_thezd2stringze70z35zzsaw_c_codez00(obj_t BgL_iportz00_5981)
	{
		{	/* SawC/code.scm 594 */
			{	/* SawC/code.scm 594 */
				long BgL_arg2166z00_2964;

				BgL_arg2166z00_2964 = RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_5981);
				return rgc_buffer_substring(BgL_iportz00_5981, 0L, BgL_arg2166z00_2964);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 1 */
			{	/* SawC/code.scm 30 */
				obj_t BgL_arg2257z00_3127;
				obj_t BgL_arg2258z00_3128;

				{	/* SawC/code.scm 30 */
					obj_t BgL_v1665z00_3157;

					BgL_v1665z00_3157 = create_vector(1L);
					{	/* SawC/code.scm 30 */
						obj_t BgL_arg2269z00_3158;

						BgL_arg2269z00_3158 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc2764z00zzsaw_c_codez00, BGl_proc2763z00zzsaw_c_codez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(7));
						VECTOR_SET(BgL_v1665z00_3157, 0L, BgL_arg2269z00_3158);
					}
					BgL_arg2257z00_3127 = BgL_v1665z00_3157;
				}
				{	/* SawC/code.scm 30 */
					obj_t BgL_v1666z00_3168;

					BgL_v1666z00_3168 = create_vector(0L);
					BgL_arg2258z00_3128 = BgL_v1666z00_3168;
				}
				return (BGl_SawCIregz00zzsaw_c_codez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(8),
						CNST_TABLE_REF(9), BGl_rtl_regz00zzsaw_defsz00, 48725L,
						BGl_proc2768z00zzsaw_c_codez00, BGl_proc2767z00zzsaw_c_codez00,
						BFALSE, BGl_proc2766z00zzsaw_c_codez00,
						BGl_proc2765z00zzsaw_c_codez00, BgL_arg2257z00_3127,
						BgL_arg2258z00_3128), BUNSPEC);
			}
		}

	}



/* &lambda2265 */
	BgL_rtl_regz00_bglt BGl_z62lambda2265z62zzsaw_c_codez00(obj_t BgL_envz00_5837,
		obj_t BgL_o1168z00_5838)
	{
		{	/* SawC/code.scm 30 */
			{	/* SawC/code.scm 30 */
				long BgL_arg2266z00_6004;

				{	/* SawC/code.scm 30 */
					obj_t BgL_arg2267z00_6005;

					{	/* SawC/code.scm 30 */
						obj_t BgL_arg2268z00_6006;

						{	/* SawC/code.scm 30 */
							obj_t BgL_arg1815z00_6007;
							long BgL_arg1816z00_6008;

							BgL_arg1815z00_6007 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawC/code.scm 30 */
								long BgL_arg1817z00_6009;

								BgL_arg1817z00_6009 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_o1168z00_5838)));
								BgL_arg1816z00_6008 = (BgL_arg1817z00_6009 - OBJECT_TYPE);
							}
							BgL_arg2268z00_6006 =
								VECTOR_REF(BgL_arg1815z00_6007, BgL_arg1816z00_6008);
						}
						BgL_arg2267z00_6005 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2268z00_6006);
					}
					{	/* SawC/code.scm 30 */
						obj_t BgL_tmpz00_7711;

						BgL_tmpz00_7711 = ((obj_t) BgL_arg2267z00_6005);
						BgL_arg2266z00_6004 = BGL_CLASS_NUM(BgL_tmpz00_7711);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) BgL_o1168z00_5838)), BgL_arg2266z00_6004);
			}
			{	/* SawC/code.scm 30 */
				BgL_objectz00_bglt BgL_tmpz00_7717;

				BgL_tmpz00_7717 =
					((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1168z00_5838));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7717, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1168z00_5838));
			return ((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1168z00_5838));
		}

	}



/* &<@anonymous:2264> */
	obj_t BGl_z62zc3z04anonymousza32264ze3ze5zzsaw_c_codez00(obj_t
		BgL_envz00_5839, obj_t BgL_new1167z00_5840)
	{
		{	/* SawC/code.scm 30 */
			{
				BgL_rtl_regz00_bglt BgL_auxz00_7725;

				{
					BgL_typez00_bglt BgL_auxz00_7726;

					{	/* SawC/code.scm 30 */
						obj_t BgL_classz00_6011;

						BgL_classz00_6011 = BGl_typez00zztype_typez00;
						{	/* SawC/code.scm 30 */
							obj_t BgL__ortest_1117z00_6012;

							BgL__ortest_1117z00_6012 = BGL_CLASS_NIL(BgL_classz00_6011);
							if (CBOOL(BgL__ortest_1117z00_6012))
								{	/* SawC/code.scm 30 */
									BgL_auxz00_7726 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6012);
								}
							else
								{	/* SawC/code.scm 30 */
									BgL_auxz00_7726 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6011));
								}
						}
					}
					((((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_new1167z00_5840))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_7726), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt)
									((BgL_rtl_regz00_bglt) BgL_new1167z00_5840))))->BgL_varz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1167z00_5840))))->BgL_onexprzf3zf3) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1167z00_5840))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1167z00_5840))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1167z00_5840))))->BgL_debugnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1167z00_5840))))->BgL_hardwarez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_sawciregz00_bglt BgL_auxz00_7754;

					{
						obj_t BgL_auxz00_7755;

						{	/* SawC/code.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_7756;

							BgL_tmpz00_7756 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1167z00_5840));
							BgL_auxz00_7755 = BGL_OBJECT_WIDENING(BgL_tmpz00_7756);
						}
						BgL_auxz00_7754 = ((BgL_sawciregz00_bglt) BgL_auxz00_7755);
					}
					((((BgL_sawciregz00_bglt) COBJECT(BgL_auxz00_7754))->BgL_indexz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_7725 = ((BgL_rtl_regz00_bglt) BgL_new1167z00_5840);
				return ((obj_t) BgL_auxz00_7725);
			}
		}

	}



/* &lambda2262 */
	BgL_rtl_regz00_bglt BGl_z62lambda2262z62zzsaw_c_codez00(obj_t BgL_envz00_5841,
		obj_t BgL_o1164z00_5842)
	{
		{	/* SawC/code.scm 30 */
			{	/* SawC/code.scm 30 */
				BgL_sawciregz00_bglt BgL_wide1166z00_6014;

				BgL_wide1166z00_6014 =
					((BgL_sawciregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawciregz00_bgl))));
				{	/* SawC/code.scm 30 */
					obj_t BgL_auxz00_7769;
					BgL_objectz00_bglt BgL_tmpz00_7765;

					BgL_auxz00_7769 = ((obj_t) BgL_wide1166z00_6014);
					BgL_tmpz00_7765 =
						((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1164z00_5842)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7765, BgL_auxz00_7769);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1164z00_5842)));
				{	/* SawC/code.scm 30 */
					long BgL_arg2263z00_6015;

					BgL_arg2263z00_6015 = BGL_CLASS_NUM(BGl_SawCIregz00zzsaw_c_codez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_o1164z00_5842))),
						BgL_arg2263z00_6015);
				}
				return
					((BgL_rtl_regz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1164z00_5842)));
			}
		}

	}



/* &lambda2259 */
	BgL_rtl_regz00_bglt BGl_z62lambda2259z62zzsaw_c_codez00(obj_t BgL_envz00_5843,
		obj_t BgL_type1156z00_5844, obj_t BgL_var1157z00_5845,
		obj_t BgL_onexprzf31158zf3_5846, obj_t BgL_name1159z00_5847,
		obj_t BgL_key1160z00_5848, obj_t BgL_debugname1161z00_5849,
		obj_t BgL_hardware1162z00_5850, obj_t BgL_index1163z00_5851)
	{
		{	/* SawC/code.scm 30 */
			{	/* SawC/code.scm 30 */
				BgL_rtl_regz00_bglt BgL_new1220z00_6017;

				{	/* SawC/code.scm 30 */
					BgL_rtl_regz00_bglt BgL_tmp1218z00_6018;
					BgL_sawciregz00_bglt BgL_wide1219z00_6019;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_7783;

						{	/* SawC/code.scm 30 */
							BgL_rtl_regz00_bglt BgL_new1217z00_6020;

							BgL_new1217z00_6020 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawC/code.scm 30 */
								long BgL_arg2261z00_6021;

								BgL_arg2261z00_6021 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1217z00_6020),
									BgL_arg2261z00_6021);
							}
							{	/* SawC/code.scm 30 */
								BgL_objectz00_bglt BgL_tmpz00_7788;

								BgL_tmpz00_7788 = ((BgL_objectz00_bglt) BgL_new1217z00_6020);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7788, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1217z00_6020);
							BgL_auxz00_7783 = BgL_new1217z00_6020;
						}
						BgL_tmp1218z00_6018 = ((BgL_rtl_regz00_bglt) BgL_auxz00_7783);
					}
					BgL_wide1219z00_6019 =
						((BgL_sawciregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sawciregz00_bgl))));
					{	/* SawC/code.scm 30 */
						obj_t BgL_auxz00_7796;
						BgL_objectz00_bglt BgL_tmpz00_7794;

						BgL_auxz00_7796 = ((obj_t) BgL_wide1219z00_6019);
						BgL_tmpz00_7794 = ((BgL_objectz00_bglt) BgL_tmp1218z00_6018);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7794, BgL_auxz00_7796);
					}
					((BgL_objectz00_bglt) BgL_tmp1218z00_6018);
					{	/* SawC/code.scm 30 */
						long BgL_arg2260z00_6022;

						BgL_arg2260z00_6022 = BGL_CLASS_NUM(BGl_SawCIregz00zzsaw_c_codez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1218z00_6018), BgL_arg2260z00_6022);
					}
					BgL_new1220z00_6017 = ((BgL_rtl_regz00_bglt) BgL_tmp1218z00_6018);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1220z00_6017)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1156z00_5844)),
					BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1220z00_6017)))->BgL_varz00) =
					((obj_t) BgL_var1157z00_5845), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1220z00_6017)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31158zf3_5846), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1220z00_6017)))->BgL_namez00) =
					((obj_t) BgL_name1159z00_5847), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1220z00_6017)))->BgL_keyz00) =
					((obj_t) BgL_key1160z00_5848), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1220z00_6017)))->BgL_debugnamez00) =
					((obj_t) BgL_debugname1161z00_5849), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1220z00_6017)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1162z00_5850), BUNSPEC);
				{
					BgL_sawciregz00_bglt BgL_auxz00_7819;

					{
						obj_t BgL_auxz00_7820;

						{	/* SawC/code.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_7821;

							BgL_tmpz00_7821 = ((BgL_objectz00_bglt) BgL_new1220z00_6017);
							BgL_auxz00_7820 = BGL_OBJECT_WIDENING(BgL_tmpz00_7821);
						}
						BgL_auxz00_7819 = ((BgL_sawciregz00_bglt) BgL_auxz00_7820);
					}
					((((BgL_sawciregz00_bglt) COBJECT(BgL_auxz00_7819))->BgL_indexz00) =
						((obj_t) BgL_index1163z00_5851), BUNSPEC);
				}
				return BgL_new1220z00_6017;
			}
		}

	}



/* &lambda2273 */
	obj_t BGl_z62lambda2273z62zzsaw_c_codez00(obj_t BgL_envz00_5852,
		obj_t BgL_oz00_5853, obj_t BgL_vz00_5854)
	{
		{	/* SawC/code.scm 30 */
			{
				BgL_sawciregz00_bglt BgL_auxz00_7826;

				{
					obj_t BgL_auxz00_7827;

					{	/* SawC/code.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_7828;

						BgL_tmpz00_7828 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_5853));
						BgL_auxz00_7827 = BGL_OBJECT_WIDENING(BgL_tmpz00_7828);
					}
					BgL_auxz00_7826 = ((BgL_sawciregz00_bglt) BgL_auxz00_7827);
				}
				return
					((((BgL_sawciregz00_bglt) COBJECT(BgL_auxz00_7826))->BgL_indexz00) =
					((obj_t) BgL_vz00_5854), BUNSPEC);
			}
		}

	}



/* &lambda2272 */
	obj_t BGl_z62lambda2272z62zzsaw_c_codez00(obj_t BgL_envz00_5855,
		obj_t BgL_oz00_5856)
	{
		{	/* SawC/code.scm 30 */
			{
				BgL_sawciregz00_bglt BgL_auxz00_7834;

				{
					obj_t BgL_auxz00_7835;

					{	/* SawC/code.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_7836;

						BgL_tmpz00_7836 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_5856));
						BgL_auxz00_7835 = BGL_OBJECT_WIDENING(BgL_tmpz00_7836);
					}
					BgL_auxz00_7834 = ((BgL_sawciregz00_bglt) BgL_auxz00_7835);
				}
				return
					(((BgL_sawciregz00_bglt) COBJECT(BgL_auxz00_7834))->BgL_indexz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 1 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00, BGl_proc2769z00zzsaw_c_codez00,
				BGl_rtl_funz00zzsaw_defsz00, BGl_string2770z00zzsaw_c_codez00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_proc2771z00zzsaw_c_codez00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_string2772z00zzsaw_c_codez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_proc2773z00zzsaw_c_codez00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_string2774z00zzsaw_c_codez00);
		}

	}



/* &gen-prefix1703 */
	obj_t BGl_z62genzd2prefix1703zb0zzsaw_c_codez00(obj_t BgL_envz00_5860,
		obj_t BgL_funz00_5861)
	{
		{	/* SawC/code.scm 440 */
			return BGl_string2690z00zzsaw_c_codez00;
		}

	}



/* &gen-fun-name1685 */
	obj_t BGl_z62genzd2funzd2name1685z62zzsaw_c_codez00(obj_t BgL_envz00_5862,
		obj_t BgL_funz00_5863)
	{
		{	/* SawC/code.scm 403 */
			return
				BGl_genzd2upcasezd2zzsaw_c_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5863));
		}

	}



/* &gen-expr1667 */
	obj_t BGl_z62genzd2expr1667zb0zzsaw_c_codez00(obj_t BgL_envz00_5864,
		obj_t BgL_funz00_5865, obj_t BgL_argsz00_5866)
	{
		{	/* SawC/code.scm 228 */
			BGl_genzd2funzd2namez00zzsaw_c_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5865));
			{	/* SawC/code.scm 231 */
				obj_t BgL_arg2275z00_6028;

				{	/* SawC/code.scm 231 */
					obj_t BgL_tmpz00_7849;

					BgL_tmpz00_7849 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2275z00_6028 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7849);
				}
				bgl_display_string(BGl_string2684z00zzsaw_c_codez00,
					BgL_arg2275z00_6028);
			}
			BGl_genzd2prefixzd2zzsaw_c_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5865));
			BGl_genzd2argszd2zzsaw_c_codez00(BgL_argsz00_5866);
			{	/* SawC/code.scm 234 */
				obj_t BgL_arg2276z00_6029;

				{	/* SawC/code.scm 234 */
					obj_t BgL_tmpz00_7856;

					BgL_tmpz00_7856 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2276z00_6029 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7856);
				}
				return
					bgl_display_string(BGl_string2686z00zzsaw_c_codez00,
					BgL_arg2276z00_6029);
			}
		}

	}



/* gen-expr */
	obj_t BGl_genzd2exprzd2zzsaw_c_codez00(BgL_rtl_funz00_bglt BgL_funz00_64,
		obj_t BgL_argsz00_65)
	{
		{	/* SawC/code.scm 228 */
			{	/* SawC/code.scm 228 */
				obj_t BgL_method1668z00_3188;

				{	/* SawC/code.scm 228 */
					obj_t BgL_res2627z00_5128;

					{	/* SawC/code.scm 228 */
						long BgL_objzd2classzd2numz00_5099;

						BgL_objzd2classzd2numz00_5099 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_64));
						{	/* SawC/code.scm 228 */
							obj_t BgL_arg1811z00_5100;

							BgL_arg1811z00_5100 =
								PROCEDURE_REF(BGl_genzd2exprzd2envz00zzsaw_c_codez00,
								(int) (1L));
							{	/* SawC/code.scm 228 */
								int BgL_offsetz00_5103;

								BgL_offsetz00_5103 = (int) (BgL_objzd2classzd2numz00_5099);
								{	/* SawC/code.scm 228 */
									long BgL_offsetz00_5104;

									BgL_offsetz00_5104 =
										((long) (BgL_offsetz00_5103) - OBJECT_TYPE);
									{	/* SawC/code.scm 228 */
										long BgL_modz00_5105;

										BgL_modz00_5105 =
											(BgL_offsetz00_5104 >> (int) ((long) ((int) (4L))));
										{	/* SawC/code.scm 228 */
											long BgL_restz00_5107;

											BgL_restz00_5107 =
												(BgL_offsetz00_5104 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawC/code.scm 228 */

												{	/* SawC/code.scm 228 */
													obj_t BgL_bucketz00_5109;

													BgL_bucketz00_5109 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5100), BgL_modz00_5105);
													BgL_res2627z00_5128 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5109), BgL_restz00_5107);
					}}}}}}}}
					BgL_method1668z00_3188 = BgL_res2627z00_5128;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1668z00_3188,
					((obj_t) BgL_funz00_64), BgL_argsz00_65);
			}
		}

	}



/* &gen-expr */
	obj_t BGl_z62genzd2exprzb0zzsaw_c_codez00(obj_t BgL_envz00_5867,
		obj_t BgL_funz00_5868, obj_t BgL_argsz00_5869)
	{
		{	/* SawC/code.scm 228 */
			return
				BGl_genzd2exprzd2zzsaw_c_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5868), BgL_argsz00_5869);
		}

	}



/* gen-fun-name */
	obj_t BGl_genzd2funzd2namez00zzsaw_c_codez00(BgL_rtl_funz00_bglt
		BgL_funz00_90)
	{
		{	/* SawC/code.scm 403 */
			{	/* SawC/code.scm 403 */
				obj_t BgL_method1686z00_3189;

				{	/* SawC/code.scm 403 */
					obj_t BgL_res2632z00_5159;

					{	/* SawC/code.scm 403 */
						long BgL_objzd2classzd2numz00_5130;

						BgL_objzd2classzd2numz00_5130 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_90));
						{	/* SawC/code.scm 403 */
							obj_t BgL_arg1811z00_5131;

							BgL_arg1811z00_5131 =
								PROCEDURE_REF(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
								(int) (1L));
							{	/* SawC/code.scm 403 */
								int BgL_offsetz00_5134;

								BgL_offsetz00_5134 = (int) (BgL_objzd2classzd2numz00_5130);
								{	/* SawC/code.scm 403 */
									long BgL_offsetz00_5135;

									BgL_offsetz00_5135 =
										((long) (BgL_offsetz00_5134) - OBJECT_TYPE);
									{	/* SawC/code.scm 403 */
										long BgL_modz00_5136;

										BgL_modz00_5136 =
											(BgL_offsetz00_5135 >> (int) ((long) ((int) (4L))));
										{	/* SawC/code.scm 403 */
											long BgL_restz00_5138;

											BgL_restz00_5138 =
												(BgL_offsetz00_5135 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawC/code.scm 403 */

												{	/* SawC/code.scm 403 */
													obj_t BgL_bucketz00_5140;

													BgL_bucketz00_5140 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5131), BgL_modz00_5136);
													BgL_res2632z00_5159 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5140), BgL_restz00_5138);
					}}}}}}}}
					BgL_method1686z00_3189 = BgL_res2632z00_5159;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1686z00_3189, ((obj_t) BgL_funz00_90));
			}
		}

	}



/* &gen-fun-name */
	obj_t BGl_z62genzd2funzd2namez62zzsaw_c_codez00(obj_t BgL_envz00_5870,
		obj_t BgL_funz00_5871)
	{
		{	/* SawC/code.scm 403 */
			return
				BGl_genzd2funzd2namez00zzsaw_c_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5871));
		}

	}



/* gen-prefix */
	obj_t BGl_genzd2prefixzd2zzsaw_c_codez00(BgL_rtl_funz00_bglt BgL_funz00_103)
	{
		{	/* SawC/code.scm 440 */
			{	/* SawC/code.scm 440 */
				obj_t BgL_method1704z00_3190;

				{	/* SawC/code.scm 440 */
					obj_t BgL_res2637z00_5190;

					{	/* SawC/code.scm 440 */
						long BgL_objzd2classzd2numz00_5161;

						BgL_objzd2classzd2numz00_5161 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_103));
						{	/* SawC/code.scm 440 */
							obj_t BgL_arg1811z00_5162;

							BgL_arg1811z00_5162 =
								PROCEDURE_REF(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
								(int) (1L));
							{	/* SawC/code.scm 440 */
								int BgL_offsetz00_5165;

								BgL_offsetz00_5165 = (int) (BgL_objzd2classzd2numz00_5161);
								{	/* SawC/code.scm 440 */
									long BgL_offsetz00_5166;

									BgL_offsetz00_5166 =
										((long) (BgL_offsetz00_5165) - OBJECT_TYPE);
									{	/* SawC/code.scm 440 */
										long BgL_modz00_5167;

										BgL_modz00_5167 =
											(BgL_offsetz00_5166 >> (int) ((long) ((int) (4L))));
										{	/* SawC/code.scm 440 */
											long BgL_restz00_5169;

											BgL_restz00_5169 =
												(BgL_offsetz00_5166 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawC/code.scm 440 */

												{	/* SawC/code.scm 440 */
													obj_t BgL_bucketz00_5171;

													BgL_bucketz00_5171 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5162), BgL_modz00_5167);
													BgL_res2637z00_5190 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5171), BgL_restz00_5169);
					}}}}}}}}
					BgL_method1704z00_3190 = BgL_res2637z00_5190;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1704z00_3190, ((obj_t) BgL_funz00_103));
			}
		}

	}



/* &gen-prefix */
	obj_t BGl_z62genzd2prefixzb0zzsaw_c_codez00(obj_t BgL_envz00_5872,
		obj_t BgL_funz00_5873)
	{
		{	/* SawC/code.scm 440 */
			return
				BGl_genzd2prefixzd2zzsaw_c_codez00(
				((BgL_rtl_funz00_bglt) BgL_funz00_5873));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 1 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00,
				BGl_rtl_lightfuncallz00zzsaw_defsz00, BGl_proc2775z00zzsaw_c_codez00,
				BGl_string2776z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00,
				BGl_rtl_getfieldz00zzsaw_defsz00, BGl_proc2777z00zzsaw_c_codez00,
				BGl_string2776z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00,
				BGl_rtl_setfieldz00zzsaw_defsz00, BGl_proc2778z00zzsaw_c_codez00,
				BGl_string2776z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00,
				BGl_rtl_funcallz00zzsaw_defsz00, BGl_proc2779z00zzsaw_c_codez00,
				BGl_string2776z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00,
				BGl_rtl_protectz00zzsaw_defsz00, BGl_proc2780z00zzsaw_c_codez00,
				BGl_string2776z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00, BGl_rtl_pragmaz00zzsaw_defsz00,
				BGl_proc2781z00zzsaw_c_codez00, BGl_string2776z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00, BGl_rtl_switchz00zzsaw_defsz00,
				BGl_proc2782z00zzsaw_c_codez00, BGl_string2776z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2exprzd2envz00zzsaw_c_codez00, BGl_rtl_callz00zzsaw_defsz00,
				BGl_proc2783z00zzsaw_c_codez00, BGl_string2776z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_rtl_vallocz00zzsaw_defsz00, BGl_proc2784z00zzsaw_c_codez00,
				BGl_string2785z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_rtl_vrefz00zzsaw_defsz00, BGl_proc2786z00zzsaw_c_codez00,
				BGl_string2785z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_rtl_vsetz00zzsaw_defsz00, BGl_proc2787z00zzsaw_c_codez00,
				BGl_string2785z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_rtl_vlengthz00zzsaw_defsz00, BGl_proc2788z00zzsaw_c_codez00,
				BGl_string2785z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_rtl_movz00zzsaw_defsz00, BGl_proc2789z00zzsaw_c_codez00,
				BGl_string2785z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_rtl_loadiz00zzsaw_defsz00, BGl_proc2790z00zzsaw_c_codez00,
				BGl_string2785z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_rtl_loadgz00zzsaw_defsz00, BGl_proc2791z00zzsaw_c_codez00,
				BGl_string2785z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2funzd2namezd2envzd2zzsaw_c_codez00,
				BGl_rtl_returnz00zzsaw_defsz00, BGl_proc2792z00zzsaw_c_codez00,
				BGl_string2785z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_rtl_loadiz00zzsaw_defsz00, BGl_proc2793z00zzsaw_c_codez00,
				BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_rtl_loadgz00zzsaw_defsz00, BGl_proc2795z00zzsaw_c_codez00,
				BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_rtl_loadfunz00zzsaw_defsz00, BGl_proc2796z00zzsaw_c_codez00,
				BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_rtl_storegz00zzsaw_defsz00, BGl_proc2797z00zzsaw_c_codez00,
				BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_rtl_globalrefz00zzsaw_defsz00, BGl_proc2798z00zzsaw_c_codez00,
				BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00, BGl_rtl_goz00zzsaw_defsz00,
				BGl_proc2799z00zzsaw_c_codez00, BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00, BGl_rtl_ifeqz00zzsaw_defsz00,
				BGl_proc2800z00zzsaw_c_codez00, BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00, BGl_rtl_ifnez00zzsaw_defsz00,
				BGl_proc2801z00zzsaw_c_codez00, BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_rtl_vallocz00zzsaw_defsz00, BGl_proc2802z00zzsaw_c_codez00,
				BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00, BGl_rtl_vrefz00zzsaw_defsz00,
				BGl_proc2803z00zzsaw_c_codez00, BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00, BGl_rtl_vsetz00zzsaw_defsz00,
				BGl_proc2804z00zzsaw_c_codez00, BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_rtl_vlengthz00zzsaw_defsz00, BGl_proc2805z00zzsaw_c_codez00,
				BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00, BGl_rtl_castz00zzsaw_defsz00,
				BGl_proc2806z00zzsaw_c_codez00, BGl_string2794z00zzsaw_c_codez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_genzd2prefixzd2envz00zzsaw_c_codez00,
				BGl_rtl_cast_nullz00zzsaw_defsz00, BGl_proc2807z00zzsaw_c_codez00,
				BGl_string2794z00zzsaw_c_codez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_acceptzd2foldingzf3zd2envzf3zzsaw_exprz00,
				BGl_cvmz00zzbackend_cvmz00, BGl_proc2808z00zzsaw_c_codez00,
				BGl_string2809z00zzsaw_c_codez00);
		}

	}



/* &accept-folding?-cvm1735 */
	obj_t BGl_z62acceptzd2foldingzf3zd2cvm1735z91zzsaw_c_codez00(obj_t
		BgL_envz00_5905, obj_t BgL_bz00_5906, obj_t BgL_insz00_5907,
		obj_t BgL_treez00_5908)
	{
		{	/* SawC/code.scm 553 */
			{	/* SawC/code.scm 554 */
				bool_t BgL__ortest_1199z00_6031;

				BgL__ortest_1199z00_6031 =
					BGl_deepzd2movzf3z21zzsaw_c_codez00(BgL_treez00_5908);
				if (BgL__ortest_1199z00_6031)
					{	/* SawC/code.scm 554 */
						return BBOOL(BgL__ortest_1199z00_6031);
					}
				else
					{	/* SawC/code.scm 554 */
						if (BGl_multiplezd2evaluationzd2zzsaw_c_codez00(BgL_insz00_5907,
								BgL_treez00_5908))
							{	/* SawC/code.scm 555 */
								return BFALSE;
							}
						else
							{	/* SawC/code.scm 555 */
								return BTRUE;
							}
					}
			}
		}

	}



/* &gen-prefix-rtl_cast_1732 */
	obj_t BGl_z62genzd2prefixzd2rtl_cast_1732z62zzsaw_c_codez00(obj_t
		BgL_envz00_5909, obj_t BgL_funz00_5910)
	{
		{	/* SawC/code.scm 489 */
			{	/* SawC/code.scm 490 */
				obj_t BgL_arg2432z00_6033;
				obj_t BgL_arg2434z00_6034;

				BgL_arg2432z00_6033 =
					BGl_makezd2typedzd2declarationz00zztype_toolsz00(
					(((BgL_rtl_cast_nullz00_bglt) COBJECT(
								((BgL_rtl_cast_nullz00_bglt) BgL_funz00_5910)))->BgL_typez00),
					BGl_string2690z00zzsaw_c_codez00);
				{	/* SawC/code.scm 490 */
					obj_t BgL_tmpz00_7996;

					BgL_tmpz00_7996 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2434z00_6034 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7996);
				}
				return bgl_display_obj(BgL_arg2432z00_6033, BgL_arg2434z00_6034);
			}
		}

	}



/* &gen-prefix-rtl_cast1730 */
	obj_t BGl_z62genzd2prefixzd2rtl_cast1730z62zzsaw_c_codez00(obj_t
		BgL_envz00_5911, obj_t BgL_funz00_5912)
	{
		{	/* SawC/code.scm 485 */
			{	/* SawC/code.scm 486 */
				obj_t BgL_arg2428z00_6036;
				obj_t BgL_arg2429z00_6037;

				BgL_arg2428z00_6036 =
					BGl_makezd2typedzd2declarationz00zztype_toolsz00(
					(((BgL_rtl_castz00_bglt) COBJECT(
								((BgL_rtl_castz00_bglt) BgL_funz00_5912)))->BgL_totypez00),
					BGl_string2690z00zzsaw_c_codez00);
				{	/* SawC/code.scm 486 */
					obj_t BgL_tmpz00_8003;

					BgL_tmpz00_8003 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2429z00_6037 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8003);
				}
				bgl_display_obj(BgL_arg2428z00_6036, BgL_arg2429z00_6037);
			}
			{	/* SawC/code.scm 487 */
				obj_t BgL_arg2431z00_6038;

				{	/* SawC/code.scm 487 */
					obj_t BgL_tmpz00_8007;

					BgL_tmpz00_8007 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2431z00_6038 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8007);
				}
				return
					bgl_display_string(BGl_string2685z00zzsaw_c_codez00,
					BgL_arg2431z00_6038);
			}
		}

	}



/* &gen-prefix-rtl_vleng1728 */
	obj_t BGl_z62genzd2prefixzd2rtl_vleng1728z62zzsaw_c_codez00(obj_t
		BgL_envz00_5913, obj_t BgL_funz00_5914)
	{
		{	/* SawC/code.scm 482 */
			return
				BGl_vextraz00zzsaw_c_codez00(
				(((BgL_rtl_vlengthz00_bglt) COBJECT(
							((BgL_rtl_vlengthz00_bglt) BgL_funz00_5914)))->BgL_typez00));
		}

	}



/* &gen-prefix-rtl_vset1726 */
	obj_t BGl_z62genzd2prefixzd2rtl_vset1726z62zzsaw_c_codez00(obj_t
		BgL_envz00_5915, obj_t BgL_funz00_5916)
	{
		{	/* SawC/code.scm 479 */
			return
				BGl_vextraz00zzsaw_c_codez00(
				(((BgL_rtl_vsetz00_bglt) COBJECT(
							((BgL_rtl_vsetz00_bglt) BgL_funz00_5916)))->BgL_typez00));
		}

	}



/* &gen-prefix-rtl_vref1724 */
	obj_t BGl_z62genzd2prefixzd2rtl_vref1724z62zzsaw_c_codez00(obj_t
		BgL_envz00_5917, obj_t BgL_funz00_5918)
	{
		{	/* SawC/code.scm 476 */
			return
				BGl_vextraz00zzsaw_c_codez00(
				(((BgL_rtl_vrefz00_bglt) COBJECT(
							((BgL_rtl_vrefz00_bglt) BgL_funz00_5918)))->BgL_typez00));
		}

	}



/* &gen-prefix-rtl_vallo1722 */
	obj_t BGl_z62genzd2prefixzd2rtl_vallo1722z62zzsaw_c_codez00(obj_t
		BgL_envz00_5919, obj_t BgL_funz00_5920)
	{
		{	/* SawC/code.scm 473 */
			return
				BGl_vextraz00zzsaw_c_codez00(
				(((BgL_rtl_vallocz00_bglt) COBJECT(
							((BgL_rtl_vallocz00_bglt) BgL_funz00_5920)))->BgL_typez00));
		}

	}



/* &gen-prefix-rtl_ifne1720 */
	obj_t BGl_z62genzd2prefixzd2rtl_ifne1720z62zzsaw_c_codez00(obj_t
		BgL_envz00_5921, obj_t BgL_funz00_5922)
	{
		{	/* SawC/code.scm 465 */
			{	/* SawC/code.scm 466 */
				int BgL_arg2417z00_6044;

				BgL_arg2417z00_6044 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_ifnez00_bglt) COBJECT(
										((BgL_rtl_ifnez00_bglt) BgL_funz00_5922)))->BgL_thenz00)))->
					BgL_labelz00);
				{	/* SawC/code.scm 466 */
					obj_t BgL_list2418z00_6045;

					{	/* SawC/code.scm 466 */
						obj_t BgL_arg2419z00_6046;

						{	/* SawC/code.scm 466 */
							obj_t BgL_arg2420z00_6047;

							BgL_arg2420z00_6047 =
								MAKE_YOUNG_PAIR(BGl_string2685z00zzsaw_c_codez00, BNIL);
							BgL_arg2419z00_6046 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg2417z00_6044), BgL_arg2420z00_6047);
						}
						BgL_list2418z00_6045 =
							MAKE_YOUNG_PAIR(BGl_string2692z00zzsaw_c_codez00,
							BgL_arg2419z00_6046);
					}
					return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2418z00_6045);
				}
			}
		}

	}



/* &gen-prefix-rtl_ifeq1718 */
	obj_t BGl_z62genzd2prefixzd2rtl_ifeq1718z62zzsaw_c_codez00(obj_t
		BgL_envz00_5923, obj_t BgL_funz00_5924)
	{
		{	/* SawC/code.scm 462 */
			{	/* SawC/code.scm 463 */
				int BgL_arg2410z00_6049;

				BgL_arg2410z00_6049 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_ifeqz00_bglt) COBJECT(
										((BgL_rtl_ifeqz00_bglt) BgL_funz00_5924)))->BgL_thenz00)))->
					BgL_labelz00);
				{	/* SawC/code.scm 463 */
					obj_t BgL_list2411z00_6050;

					{	/* SawC/code.scm 463 */
						obj_t BgL_arg2412z00_6051;

						{	/* SawC/code.scm 463 */
							obj_t BgL_arg2414z00_6052;

							BgL_arg2414z00_6052 =
								MAKE_YOUNG_PAIR(BGl_string2685z00zzsaw_c_codez00, BNIL);
							BgL_arg2412z00_6051 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg2410z00_6049), BgL_arg2414z00_6052);
						}
						BgL_list2411z00_6050 =
							MAKE_YOUNG_PAIR(BGl_string2692z00zzsaw_c_codez00,
							BgL_arg2412z00_6051);
					}
					return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2411z00_6050);
				}
			}
		}

	}



/* &gen-prefix-rtl_go1716 */
	obj_t BGl_z62genzd2prefixzd2rtl_go1716z62zzsaw_c_codez00(obj_t
		BgL_envz00_5925, obj_t BgL_funz00_5926)
	{
		{	/* SawC/code.scm 459 */
			{	/* SawC/code.scm 460 */
				int BgL_arg2404z00_6054;

				BgL_arg2404z00_6054 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_goz00_bglt) COBJECT(
										((BgL_rtl_goz00_bglt) BgL_funz00_5926)))->BgL_toz00)))->
					BgL_labelz00);
				{	/* SawC/code.scm 460 */
					obj_t BgL_list2405z00_6055;

					{	/* SawC/code.scm 460 */
						obj_t BgL_arg2407z00_6056;

						BgL_arg2407z00_6056 =
							MAKE_YOUNG_PAIR(BINT(BgL_arg2404z00_6054), BNIL);
						BgL_list2405z00_6055 =
							MAKE_YOUNG_PAIR(BGl_string2692z00zzsaw_c_codez00,
							BgL_arg2407z00_6056);
					}
					return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2405z00_6055);
				}
			}
		}

	}



/* &gen-prefix-rtl_globa1714 */
	obj_t BGl_z62genzd2prefixzd2rtl_globa1714z62zzsaw_c_codez00(obj_t
		BgL_envz00_5927, obj_t BgL_funz00_5928)
	{
		{	/* SawC/code.scm 456 */
			{	/* SawC/code.scm 457 */
				obj_t BgL_arg2401z00_6058;

				{	/* SawC/code.scm 457 */
					BgL_globalz00_bglt BgL_arg2403z00_6059;

					BgL_arg2403z00_6059 =
						(((BgL_rtl_globalrefz00_bglt) COBJECT(
								((BgL_rtl_globalrefz00_bglt) BgL_funz00_5928)))->BgL_varz00);
					{	/* SawC/code.scm 515 */
						obj_t BgL_arg2080z00_6060;

						BgL_arg2080z00_6060 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_arg2403z00_6059)))->BgL_namez00);
						BgL_arg2401z00_6058 =
							BGl_inlzd2opzd2zzsaw_c_codez00(BgL_arg2080z00_6060);
					}
				}
				{	/* SawC/code.scm 457 */
					obj_t BgL_list2402z00_6061;

					BgL_list2402z00_6061 = MAKE_YOUNG_PAIR(BgL_arg2401z00_6058, BNIL);
					return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2402z00_6061);
				}
			}
		}

	}



/* &gen-prefix-rtl_store1712 */
	obj_t BGl_z62genzd2prefixzd2rtl_store1712z62zzsaw_c_codez00(obj_t
		BgL_envz00_5929, obj_t BgL_funz00_5930)
	{
		{	/* SawC/code.scm 453 */
			{	/* SawC/code.scm 454 */
				obj_t BgL_arg2396z00_6063;

				{	/* SawC/code.scm 454 */
					BgL_globalz00_bglt BgL_arg2399z00_6064;

					BgL_arg2399z00_6064 =
						(((BgL_rtl_storegz00_bglt) COBJECT(
								((BgL_rtl_storegz00_bglt) BgL_funz00_5930)))->BgL_varz00);
					{	/* SawC/code.scm 515 */
						obj_t BgL_arg2080z00_6065;

						BgL_arg2080z00_6065 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_arg2399z00_6064)))->BgL_namez00);
						BgL_arg2396z00_6063 =
							BGl_inlzd2opzd2zzsaw_c_codez00(BgL_arg2080z00_6065);
					}
				}
				{	/* SawC/code.scm 454 */
					obj_t BgL_list2397z00_6066;

					{	/* SawC/code.scm 454 */
						obj_t BgL_arg2398z00_6067;

						BgL_arg2398z00_6067 =
							MAKE_YOUNG_PAIR(BGl_string2685z00zzsaw_c_codez00, BNIL);
						BgL_list2397z00_6066 =
							MAKE_YOUNG_PAIR(BgL_arg2396z00_6063, BgL_arg2398z00_6067);
					}
					return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2397z00_6066);
				}
			}
		}

	}



/* &gen-prefix-rtl_loadf1710 */
	obj_t BGl_z62genzd2prefixzd2rtl_loadf1710z62zzsaw_c_codez00(obj_t
		BgL_envz00_5931, obj_t BgL_funz00_5932)
	{
		{	/* SawC/code.scm 450 */
			{	/* SawC/code.scm 451 */
				obj_t BgL_arg2391z00_6069;

				{	/* SawC/code.scm 451 */
					BgL_globalz00_bglt BgL_arg2395z00_6070;

					BgL_arg2395z00_6070 =
						(((BgL_rtl_loadfunz00_bglt) COBJECT(
								((BgL_rtl_loadfunz00_bglt) BgL_funz00_5932)))->BgL_varz00);
					{	/* SawC/code.scm 515 */
						obj_t BgL_arg2080z00_6071;

						BgL_arg2080z00_6071 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_arg2395z00_6070)))->BgL_namez00);
						BgL_arg2391z00_6069 =
							BGl_inlzd2opzd2zzsaw_c_codez00(BgL_arg2080z00_6071);
					}
				}
				{	/* SawC/code.scm 451 */
					obj_t BgL_list2392z00_6072;

					{	/* SawC/code.scm 451 */
						obj_t BgL_arg2393z00_6073;

						BgL_arg2393z00_6073 = MAKE_YOUNG_PAIR(BgL_arg2391z00_6069, BNIL);
						BgL_list2392z00_6072 =
							MAKE_YOUNG_PAIR(BGl_string2810z00zzsaw_c_codez00,
							BgL_arg2393z00_6073);
					}
					return BGl_displayza2za2zz__r4_output_6_10_3z00(BgL_list2392z00_6072);
				}
			}
		}

	}



/* &gen-prefix-rtl_loadg1708 */
	obj_t BGl_z62genzd2prefixzd2rtl_loadg1708z62zzsaw_c_codez00(obj_t
		BgL_envz00_5933, obj_t BgL_funz00_5934)
	{
		{	/* SawC/code.scm 447 */
			{	/* SawC/code.scm 448 */
				obj_t BgL_arg2388z00_6075;
				obj_t BgL_arg2389z00_6076;

				{	/* SawC/code.scm 448 */
					BgL_globalz00_bglt BgL_arg2390z00_6077;

					BgL_arg2390z00_6077 =
						(((BgL_rtl_loadgz00_bglt) COBJECT(
								((BgL_rtl_loadgz00_bglt) BgL_funz00_5934)))->BgL_varz00);
					{	/* SawC/code.scm 515 */
						obj_t BgL_arg2080z00_6078;

						BgL_arg2080z00_6078 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_arg2390z00_6077)))->BgL_namez00);
						BgL_arg2388z00_6075 =
							BGl_inlzd2opzd2zzsaw_c_codez00(BgL_arg2080z00_6078);
					}
				}
				{	/* SawC/code.scm 448 */
					obj_t BgL_tmpz00_8074;

					BgL_tmpz00_8074 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2389z00_6076 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8074);
				}
				return bgl_display_obj(BgL_arg2388z00_6075, BgL_arg2389z00_6076);
			}
		}

	}



/* &gen-prefix-rtl_loadi1706 */
	obj_t BGl_z62genzd2prefixzd2rtl_loadi1706z62zzsaw_c_codez00(obj_t
		BgL_envz00_5935, obj_t BgL_funz00_5936)
	{
		{	/* SawC/code.scm 443 */
			{	/* SawC/code.scm 444 */
				BgL_atomz00_bglt BgL_iz00_6080;

				BgL_iz00_6080 =
					(((BgL_rtl_loadiz00_bglt) COBJECT(
							((BgL_rtl_loadiz00_bglt) BgL_funz00_5936)))->BgL_constantz00);
				{	/* SawC/code.scm 445 */
					obj_t BgL_arg2386z00_6081;
					BgL_typez00_bglt BgL_arg2387z00_6082;

					BgL_arg2386z00_6081 =
						(((BgL_atomz00_bglt) COBJECT(BgL_iz00_6080))->BgL_valuez00);
					BgL_arg2387z00_6082 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_iz00_6080)))->BgL_typez00);
					return
						BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00(BgL_arg2386z00_6081,
						BgL_arg2387z00_6082);
				}
			}
		}

	}



/* &gen-fun-name-rtl_ret1702 */
	obj_t BGl_z62genzd2funzd2namezd2rtl_ret1702zb0zzsaw_c_codez00(obj_t
		BgL_envz00_5937, obj_t BgL_funz00_5938)
	{
		{	/* SawC/code.scm 434 */
			{	/* SawC/code.scm 435 */
				obj_t BgL_arg2385z00_6084;

				{	/* SawC/code.scm 435 */
					obj_t BgL_tmpz00_8084;

					BgL_tmpz00_8084 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2385z00_6084 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8084);
				}
				return
					bgl_display_string(BGl_string2811z00zzsaw_c_codez00,
					BgL_arg2385z00_6084);
			}
		}

	}



/* &gen-fun-name-rtl_loa1700 */
	obj_t BGl_z62genzd2funzd2namezd2rtl_loa1700zb0zzsaw_c_codez00(obj_t
		BgL_envz00_5939, obj_t BgL_funz00_5940)
	{
		{	/* SawC/code.scm 432 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &gen-fun-name-rtl_loa1698 */
	obj_t BGl_z62genzd2funzd2namezd2rtl_loa1698zb0zzsaw_c_codez00(obj_t
		BgL_envz00_5941, obj_t BgL_funz00_5942)
	{
		{	/* SawC/code.scm 431 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &gen-fun-name-rtl_mov1696 */
	obj_t BGl_z62genzd2funzd2namezd2rtl_mov1696zb0zzsaw_c_codez00(obj_t
		BgL_envz00_5943, obj_t BgL_funz00_5944)
	{
		{	/* SawC/code.scm 430 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &gen-fun-name-rtl_vle1694 */
	obj_t BGl_z62genzd2funzd2namezd2rtl_vle1694zb0zzsaw_c_codez00(obj_t
		BgL_envz00_5945, obj_t BgL_funz00_5946)
	{
		{	/* SawC/code.scm 423 */
			return
				BGl_vfunzd2namezd2zzsaw_c_codez00(BGl_string2812z00zzsaw_c_codez00,
				(((BgL_rtl_vlengthz00_bglt) COBJECT(
							((BgL_rtl_vlengthz00_bglt) BgL_funz00_5946)))->BgL_typez00));
		}

	}



/* &gen-fun-name-rtl_vse1692 */
	obj_t BGl_z62genzd2funzd2namezd2rtl_vse1692zb0zzsaw_c_codez00(obj_t
		BgL_envz00_5947, obj_t BgL_funz00_5948)
	{
		{	/* SawC/code.scm 420 */
			return
				BGl_vfunzd2namezd2zzsaw_c_codez00(BGl_string2813z00zzsaw_c_codez00,
				(((BgL_rtl_vsetz00_bglt) COBJECT(
							((BgL_rtl_vsetz00_bglt) BgL_funz00_5948)))->BgL_typez00));
		}

	}



/* &gen-fun-name-rtl_vre1690 */
	obj_t BGl_z62genzd2funzd2namezd2rtl_vre1690zb0zzsaw_c_codez00(obj_t
		BgL_envz00_5949, obj_t BgL_funz00_5950)
	{
		{	/* SawC/code.scm 417 */
			return
				BGl_vfunzd2namezd2zzsaw_c_codez00(BGl_string2814z00zzsaw_c_codez00,
				(((BgL_rtl_vrefz00_bglt) COBJECT(
							((BgL_rtl_vrefz00_bglt) BgL_funz00_5950)))->BgL_typez00));
		}

	}



/* &gen-fun-name-rtl_val1688 */
	obj_t BGl_z62genzd2funzd2namezd2rtl_val1688zb0zzsaw_c_codez00(obj_t
		BgL_envz00_5951, obj_t BgL_funz00_5952)
	{
		{	/* SawC/code.scm 414 */
			return
				BGl_vfunzd2namezd2zzsaw_c_codez00(BGl_string2815z00zzsaw_c_codez00,
				(((BgL_rtl_vallocz00_bglt) COBJECT(
							((BgL_rtl_vallocz00_bglt) BgL_funz00_5952)))->BgL_typez00));
		}

	}



/* &gen-expr-rtl_call1684 */
	obj_t BGl_z62genzd2exprzd2rtl_call1684z62zzsaw_c_codez00(obj_t
		BgL_envz00_5953, obj_t BgL_funz00_5954, obj_t BgL_argsz00_5955)
	{
		{	/* SawC/code.scm 361 */
			{
				obj_t BgL_namez00_6111;
				obj_t BgL_argsz00_6112;
				obj_t BgL_namez00_6095;
				obj_t BgL_argsz00_6096;

				{	/* SawC/code.scm 391 */
					BgL_globalz00_bglt BgL_varz00_6124;

					BgL_varz00_6124 =
						(((BgL_rtl_callz00_bglt) COBJECT(
								((BgL_rtl_callz00_bglt) BgL_funz00_5954)))->BgL_varz00);
					{	/* SawC/code.scm 391 */
						obj_t BgL_namez00_6125;

						{	/* SawC/code.scm 515 */
							obj_t BgL_arg2080z00_6126;

							BgL_arg2080z00_6126 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_varz00_6124)))->BgL_namez00);
							BgL_namez00_6125 =
								BGl_inlzd2opzd2zzsaw_c_codez00(BgL_arg2080z00_6126);
						}
						{	/* SawC/code.scm 392 */

							{	/* SawC/code.scm 393 */
								bool_t BgL_test3233z00_8108;

								{	/* SawC/code.scm 393 */
									bool_t BgL_test3234z00_8109;

									{	/* SawC/code.scm 393 */
										obj_t BgL_classz00_6127;

										BgL_classz00_6127 = BGl_globalz00zzast_varz00;
										{	/* SawC/code.scm 393 */
											BgL_objectz00_bglt BgL_arg1807z00_6128;

											{	/* SawC/code.scm 393 */
												obj_t BgL_tmpz00_8110;

												BgL_tmpz00_8110 =
													((obj_t) ((BgL_objectz00_bglt) BgL_varz00_6124));
												BgL_arg1807z00_6128 =
													(BgL_objectz00_bglt) (BgL_tmpz00_8110);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawC/code.scm 393 */
													long BgL_idxz00_6129;

													BgL_idxz00_6129 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6128);
													BgL_test3234z00_8109 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_6129 + 2L)) == BgL_classz00_6127);
												}
											else
												{	/* SawC/code.scm 393 */
													bool_t BgL_res2638z00_6132;

													{	/* SawC/code.scm 393 */
														obj_t BgL_oclassz00_6133;

														{	/* SawC/code.scm 393 */
															obj_t BgL_arg1815z00_6134;
															long BgL_arg1816z00_6135;

															BgL_arg1815z00_6134 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawC/code.scm 393 */
																long BgL_arg1817z00_6136;

																BgL_arg1817z00_6136 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6128);
																BgL_arg1816z00_6135 =
																	(BgL_arg1817z00_6136 - OBJECT_TYPE);
															}
															BgL_oclassz00_6133 =
																VECTOR_REF(BgL_arg1815z00_6134,
																BgL_arg1816z00_6135);
														}
														{	/* SawC/code.scm 393 */
															bool_t BgL__ortest_1115z00_6137;

															BgL__ortest_1115z00_6137 =
																(BgL_classz00_6127 == BgL_oclassz00_6133);
															if (BgL__ortest_1115z00_6137)
																{	/* SawC/code.scm 393 */
																	BgL_res2638z00_6132 =
																		BgL__ortest_1115z00_6137;
																}
															else
																{	/* SawC/code.scm 393 */
																	long BgL_odepthz00_6138;

																	{	/* SawC/code.scm 393 */
																		obj_t BgL_arg1804z00_6139;

																		BgL_arg1804z00_6139 = (BgL_oclassz00_6133);
																		BgL_odepthz00_6138 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_6139);
																	}
																	if ((2L < BgL_odepthz00_6138))
																		{	/* SawC/code.scm 393 */
																			obj_t BgL_arg1802z00_6140;

																			{	/* SawC/code.scm 393 */
																				obj_t BgL_arg1803z00_6141;

																				BgL_arg1803z00_6141 =
																					(BgL_oclassz00_6133);
																				BgL_arg1802z00_6140 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_6141, 2L);
																			}
																			BgL_res2638z00_6132 =
																				(BgL_arg1802z00_6140 ==
																				BgL_classz00_6127);
																		}
																	else
																		{	/* SawC/code.scm 393 */
																			BgL_res2638z00_6132 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3234z00_8109 = BgL_res2638z00_6132;
												}
										}
									}
									if (BgL_test3234z00_8109)
										{	/* SawC/code.scm 394 */
											bool_t BgL_test3238z00_8133;

											{	/* SawC/code.scm 394 */
												BgL_valuez00_bglt BgL_arg2349z00_6142;

												BgL_arg2349z00_6142 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_varz00_6124)))->
													BgL_valuez00);
												{	/* SawC/code.scm 394 */
													obj_t BgL_classz00_6143;

													BgL_classz00_6143 = BGl_cfunz00zzast_varz00;
													{	/* SawC/code.scm 394 */
														BgL_objectz00_bglt BgL_arg1807z00_6144;

														{	/* SawC/code.scm 394 */
															obj_t BgL_tmpz00_8136;

															BgL_tmpz00_8136 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg2349z00_6142));
															BgL_arg1807z00_6144 =
																(BgL_objectz00_bglt) (BgL_tmpz00_8136);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawC/code.scm 394 */
																long BgL_idxz00_6145;

																BgL_idxz00_6145 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_6144);
																BgL_test3238z00_8133 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_6145 + 3L)) ==
																	BgL_classz00_6143);
															}
														else
															{	/* SawC/code.scm 394 */
																bool_t BgL_res2639z00_6148;

																{	/* SawC/code.scm 394 */
																	obj_t BgL_oclassz00_6149;

																	{	/* SawC/code.scm 394 */
																		obj_t BgL_arg1815z00_6150;
																		long BgL_arg1816z00_6151;

																		BgL_arg1815z00_6150 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawC/code.scm 394 */
																			long BgL_arg1817z00_6152;

																			BgL_arg1817z00_6152 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_6144);
																			BgL_arg1816z00_6151 =
																				(BgL_arg1817z00_6152 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_6149 =
																			VECTOR_REF(BgL_arg1815z00_6150,
																			BgL_arg1816z00_6151);
																	}
																	{	/* SawC/code.scm 394 */
																		bool_t BgL__ortest_1115z00_6153;

																		BgL__ortest_1115z00_6153 =
																			(BgL_classz00_6143 == BgL_oclassz00_6149);
																		if (BgL__ortest_1115z00_6153)
																			{	/* SawC/code.scm 394 */
																				BgL_res2639z00_6148 =
																					BgL__ortest_1115z00_6153;
																			}
																		else
																			{	/* SawC/code.scm 394 */
																				long BgL_odepthz00_6154;

																				{	/* SawC/code.scm 394 */
																					obj_t BgL_arg1804z00_6155;

																					BgL_arg1804z00_6155 =
																						(BgL_oclassz00_6149);
																					BgL_odepthz00_6154 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_6155);
																				}
																				if ((3L < BgL_odepthz00_6154))
																					{	/* SawC/code.scm 394 */
																						obj_t BgL_arg1802z00_6156;

																						{	/* SawC/code.scm 394 */
																							obj_t BgL_arg1803z00_6157;

																							BgL_arg1803z00_6157 =
																								(BgL_oclassz00_6149);
																							BgL_arg1802z00_6156 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_6157, 3L);
																						}
																						BgL_res2639z00_6148 =
																							(BgL_arg1802z00_6156 ==
																							BgL_classz00_6143);
																					}
																				else
																					{	/* SawC/code.scm 394 */
																						BgL_res2639z00_6148 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3238z00_8133 = BgL_res2639z00_6148;
															}
													}
												}
											}
											if (BgL_test3238z00_8133)
												{	/* SawC/code.scm 394 */
													if (
														(((BgL_cfunz00_bglt) COBJECT(
																	((BgL_cfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						BgL_varz00_6124)))->
																			BgL_valuez00))))->BgL_infixzf3zf3))
														{	/* SawC/code.scm 396 */
															bool_t BgL_test3243z00_8164;

															if (
																((((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_varz00_6124)))->BgL_namez00) ==
																	BgL_namez00_6125))
																{	/* SawC/code.scm 364 */
																	BgL_test3243z00_8164 = ((bool_t) 0);
																}
															else
																{	/* SawC/code.scm 364 */
																	BgL_test3243z00_8164 = ((bool_t) 1);
																}
															if (BgL_test3243z00_8164)
																{	/* SawC/code.scm 396 */
																	BgL_test3233z00_8108 = ((bool_t) 0);
																}
															else
																{	/* SawC/code.scm 396 */
																	BgL_test3233z00_8108 = ((bool_t) 1);
																}
														}
													else
														{	/* SawC/code.scm 395 */
															BgL_test3233z00_8108 = ((bool_t) 0);
														}
												}
											else
												{	/* SawC/code.scm 394 */
													BgL_test3233z00_8108 = ((bool_t) 0);
												}
										}
									else
										{	/* SawC/code.scm 393 */
											BgL_test3233z00_8108 = ((bool_t) 0);
										}
								}
								if (BgL_test3233z00_8108)
									{	/* SawC/code.scm 393 */
										BgL_namez00_6111 = BgL_namez00_6125;
										BgL_argsz00_6112 = BgL_argsz00_5955;
										if (NULLP(BgL_argsz00_6112))
											{	/* SawC/code.scm 368 */
												obj_t BgL_list2356z00_6113;

												{	/* SawC/code.scm 368 */
													obj_t BgL_arg2357z00_6114;

													{	/* SawC/code.scm 368 */
														obj_t BgL_arg2358z00_6115;

														BgL_arg2358z00_6115 =
															MAKE_YOUNG_PAIR(BGl_string2686z00zzsaw_c_codez00,
															BNIL);
														BgL_arg2357z00_6114 =
															MAKE_YOUNG_PAIR(BgL_namez00_6111,
															BgL_arg2358z00_6115);
													}
													BgL_list2356z00_6113 =
														MAKE_YOUNG_PAIR(BGl_string2684z00zzsaw_c_codez00,
														BgL_arg2357z00_6114);
												}
												return
													BGl_displayza2za2zz__r4_output_6_10_3z00
													(BgL_list2356z00_6113);
											}
										else
											{	/* SawC/code.scm 367 */
												{	/* SawC/code.scm 370 */
													obj_t BgL_arg2361z00_6116;

													{	/* SawC/code.scm 370 */
														obj_t BgL_tmpz00_8175;

														BgL_tmpz00_8175 = BGL_CURRENT_DYNAMIC_ENV();
														BgL_arg2361z00_6116 =
															BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8175);
													}
													bgl_display_string(BGl_string2684z00zzsaw_c_codez00,
														BgL_arg2361z00_6116);
												}
												{	/* SawC/code.scm 371 */
													obj_t BgL_arg2363z00_6117;

													BgL_arg2363z00_6117 = CAR(((obj_t) BgL_argsz00_6112));
													BGl_genzd2regzd2zzsaw_c_codez00(BgL_arg2363z00_6117);
												}
												{	/* SawC/code.scm 372 */
													obj_t BgL_arg2364z00_6118;

													{	/* SawC/code.scm 372 */
														obj_t BgL_tmpz00_8182;

														BgL_tmpz00_8182 = BGL_CURRENT_DYNAMIC_ENV();
														BgL_arg2364z00_6118 =
															BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8182);
													}
													bgl_display_obj(BgL_namez00_6111,
														BgL_arg2364z00_6118);
												}
												{	/* SawC/code.scm 373 */
													obj_t BgL_g1197z00_6119;

													BgL_g1197z00_6119 = CDR(((obj_t) BgL_argsz00_6112));
													{
														obj_t BgL_argsz00_6121;

														BgL_argsz00_6121 = BgL_g1197z00_6119;
													BgL_loopz00_6120:
														if (PAIRP(BgL_argsz00_6121))
															{	/* SawC/code.scm 374 */
																BGl_genzd2regzd2zzsaw_c_codez00(CAR
																	(BgL_argsz00_6121));
																{	/* SawC/code.scm 376 */
																	obj_t BgL_arg2368z00_6122;

																	{	/* SawC/code.scm 376 */
																		obj_t BgL_tmpz00_8192;

																		BgL_tmpz00_8192 = BGL_CURRENT_DYNAMIC_ENV();
																		BgL_arg2368z00_6122 =
																			BGL_ENV_CURRENT_OUTPUT_PORT
																			(BgL_tmpz00_8192);
																	}
																	bgl_display_obj(BgL_namez00_6111,
																		BgL_arg2368z00_6122);
																}
																{
																	obj_t BgL_argsz00_8196;

																	BgL_argsz00_8196 = CDR(BgL_argsz00_6121);
																	BgL_argsz00_6121 = BgL_argsz00_8196;
																	goto BgL_loopz00_6120;
																}
															}
														else
															{	/* SawC/code.scm 374 */
																((bool_t) 0);
															}
													}
												}
												{	/* SawC/code.scm 378 */
													obj_t BgL_arg2370z00_6123;

													{	/* SawC/code.scm 378 */
														obj_t BgL_tmpz00_8198;

														BgL_tmpz00_8198 = BGL_CURRENT_DYNAMIC_ENV();
														BgL_arg2370z00_6123 =
															BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8198);
													}
													return
														bgl_display_string(BGl_string2686z00zzsaw_c_codez00,
														BgL_arg2370z00_6123);
												}
											}
									}
								else
									{	/* SawC/code.scm 393 */
										BgL_namez00_6095 = BgL_namez00_6125;
										BgL_argsz00_6096 = BgL_argsz00_5955;
										{	/* SawC/code.scm 381 */
											obj_t BgL_list2372z00_6097;

											{	/* SawC/code.scm 381 */
												obj_t BgL_arg2373z00_6098;

												BgL_arg2373z00_6098 =
													MAKE_YOUNG_PAIR(BGl_string2684z00zzsaw_c_codez00,
													BNIL);
												BgL_list2372z00_6097 =
													MAKE_YOUNG_PAIR(BgL_namez00_6095,
													BgL_arg2373z00_6098);
											}
											BGl_displayza2za2zz__r4_output_6_10_3z00
												(BgL_list2372z00_6097);
										}
										{	/* SawC/code.scm 382 */
											bool_t BgL_test3247z00_8205;

											{	/* SawC/code.scm 382 */
												bool_t BgL_test3248z00_8206;

												{	/* SawC/code.scm 382 */
													long BgL_l1z00_6099;

													BgL_l1z00_6099 =
														STRING_LENGTH(((obj_t) BgL_namez00_6095));
													if ((BgL_l1z00_6099 == 17L))
														{	/* SawC/code.scm 382 */
															int BgL_arg1282z00_6100;

															{	/* SawC/code.scm 382 */
																char *BgL_auxz00_8214;
																char *BgL_tmpz00_8211;

																BgL_auxz00_8214 =
																	BSTRING_TO_STRING
																	(BGl_string2816z00zzsaw_c_codez00);
																BgL_tmpz00_8211 =
																	BSTRING_TO_STRING(((obj_t) BgL_namez00_6095));
																BgL_arg1282z00_6100 =
																	memcmp(BgL_tmpz00_8211, BgL_auxz00_8214,
																	BgL_l1z00_6099);
															}
															BgL_test3248z00_8206 =
																((long) (BgL_arg1282z00_6100) == 0L);
														}
													else
														{	/* SawC/code.scm 382 */
															BgL_test3248z00_8206 = ((bool_t) 0);
														}
												}
												if (BgL_test3248z00_8206)
													{	/* SawC/code.scm 382 */
														BgL_test3247z00_8205 = ((bool_t) 1);
													}
												else
													{	/* SawC/code.scm 383 */
														bool_t BgL_test3250z00_8219;

														{	/* SawC/code.scm 383 */
															long BgL_l1z00_6101;

															BgL_l1z00_6101 =
																STRING_LENGTH(((obj_t) BgL_namez00_6095));
															if ((BgL_l1z00_6101 == 17L))
																{	/* SawC/code.scm 383 */
																	int BgL_arg1282z00_6102;

																	{	/* SawC/code.scm 383 */
																		char *BgL_auxz00_8227;
																		char *BgL_tmpz00_8224;

																		BgL_auxz00_8227 =
																			BSTRING_TO_STRING
																			(BGl_string2817z00zzsaw_c_codez00);
																		BgL_tmpz00_8224 =
																			BSTRING_TO_STRING(((obj_t)
																				BgL_namez00_6095));
																		BgL_arg1282z00_6102 =
																			memcmp(BgL_tmpz00_8224, BgL_auxz00_8227,
																			BgL_l1z00_6101);
																	}
																	BgL_test3250z00_8219 =
																		((long) (BgL_arg1282z00_6102) == 0L);
																}
															else
																{	/* SawC/code.scm 383 */
																	BgL_test3250z00_8219 = ((bool_t) 0);
																}
														}
														if (BgL_test3250z00_8219)
															{	/* SawC/code.scm 383 */
																BgL_test3247z00_8205 = ((bool_t) 1);
															}
														else
															{	/* SawC/code.scm 384 */
																bool_t BgL_test3252z00_8232;

																{	/* SawC/code.scm 384 */
																	long BgL_l1z00_6103;

																	BgL_l1z00_6103 =
																		STRING_LENGTH(((obj_t) BgL_namez00_6095));
																	if ((BgL_l1z00_6103 == 17L))
																		{	/* SawC/code.scm 384 */
																			int BgL_arg1282z00_6104;

																			{	/* SawC/code.scm 384 */
																				char *BgL_auxz00_8240;
																				char *BgL_tmpz00_8237;

																				BgL_auxz00_8240 =
																					BSTRING_TO_STRING
																					(BGl_string2818z00zzsaw_c_codez00);
																				BgL_tmpz00_8237 =
																					BSTRING_TO_STRING(((obj_t)
																						BgL_namez00_6095));
																				BgL_arg1282z00_6104 =
																					memcmp(BgL_tmpz00_8237,
																					BgL_auxz00_8240, BgL_l1z00_6103);
																			}
																			BgL_test3252z00_8232 =
																				((long) (BgL_arg1282z00_6104) == 0L);
																		}
																	else
																		{	/* SawC/code.scm 384 */
																			BgL_test3252z00_8232 = ((bool_t) 0);
																		}
																}
																if (BgL_test3252z00_8232)
																	{	/* SawC/code.scm 384 */
																		BgL_test3247z00_8205 = ((bool_t) 1);
																	}
																else
																	{	/* SawC/code.scm 385 */
																		bool_t BgL_test3254z00_8245;

																		{	/* SawC/code.scm 385 */
																			long BgL_l1z00_6105;

																			BgL_l1z00_6105 =
																				STRING_LENGTH(
																				((obj_t) BgL_namez00_6095));
																			if ((BgL_l1z00_6105 == 17L))
																				{	/* SawC/code.scm 385 */
																					int BgL_arg1282z00_6106;

																					{	/* SawC/code.scm 385 */
																						char *BgL_auxz00_8253;
																						char *BgL_tmpz00_8250;

																						BgL_auxz00_8253 =
																							BSTRING_TO_STRING
																							(BGl_string2819z00zzsaw_c_codez00);
																						BgL_tmpz00_8250 =
																							BSTRING_TO_STRING(((obj_t)
																								BgL_namez00_6095));
																						BgL_arg1282z00_6106 =
																							memcmp(BgL_tmpz00_8250,
																							BgL_auxz00_8253, BgL_l1z00_6105);
																					}
																					BgL_test3254z00_8245 =
																						(
																						(long) (BgL_arg1282z00_6106) == 0L);
																				}
																			else
																				{	/* SawC/code.scm 385 */
																					BgL_test3254z00_8245 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test3254z00_8245)
																			{	/* SawC/code.scm 385 */
																				BgL_test3247z00_8205 = ((bool_t) 1);
																			}
																		else
																			{	/* SawC/code.scm 386 */
																				long BgL_l1z00_6107;

																				BgL_l1z00_6107 =
																					STRING_LENGTH(
																					((obj_t) BgL_namez00_6095));
																				if ((BgL_l1z00_6107 == 16L))
																					{	/* SawC/code.scm 386 */
																						int BgL_arg1282z00_6108;

																						{	/* SawC/code.scm 386 */
																							char *BgL_auxz00_8265;
																							char *BgL_tmpz00_8262;

																							BgL_auxz00_8265 =
																								BSTRING_TO_STRING
																								(BGl_string2820z00zzsaw_c_codez00);
																							BgL_tmpz00_8262 =
																								BSTRING_TO_STRING(((obj_t)
																									BgL_namez00_6095));
																							BgL_arg1282z00_6108 =
																								memcmp(BgL_tmpz00_8262,
																								BgL_auxz00_8265,
																								BgL_l1z00_6107);
																						}
																						BgL_test3247z00_8205 =
																							(
																							(long) (BgL_arg1282z00_6108) ==
																							0L);
																					}
																				else
																					{	/* SawC/code.scm 386 */
																						BgL_test3247z00_8205 = ((bool_t) 0);
																					}
																			}
																	}
															}
													}
											}
											if (BgL_test3247z00_8205)
												{	/* SawC/code.scm 387 */
													obj_t BgL_arg2379z00_6109;

													{	/* SawC/code.scm 387 */
														obj_t BgL_tmpz00_8270;

														BgL_tmpz00_8270 = BGL_CURRENT_DYNAMIC_ENV();
														BgL_arg2379z00_6109 =
															BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8270);
													}
													bgl_display_string(BGl_string2821z00zzsaw_c_codez00,
														BgL_arg2379z00_6109);
												}
											else
												{	/* SawC/code.scm 382 */
													BFALSE;
												}
										}
										BGl_genzd2argszd2zzsaw_c_codez00(BgL_argsz00_6096);
										{	/* SawC/code.scm 389 */
											obj_t BgL_arg2380z00_6110;

											{	/* SawC/code.scm 389 */
												obj_t BgL_tmpz00_8275;

												BgL_tmpz00_8275 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg2380z00_6110 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8275);
											}
											return
												bgl_display_string(BGl_string2686z00zzsaw_c_codez00,
												BgL_arg2380z00_6110);
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* &gen-expr-rtl_switch1682 */
	obj_t BGl_z62genzd2exprzd2rtl_switch1682z62zzsaw_c_codez00(obj_t
		BgL_envz00_5956, obj_t BgL_funz00_5957, obj_t BgL_argsz00_5958)
	{
		{	/* SawC/code.scm 340 */
			{	/* SawC/code.scm 341 */
				obj_t BgL_patsz00_6159;

				BgL_patsz00_6159 =
					(((BgL_rtl_selectz00_bglt) COBJECT(
							((BgL_rtl_selectz00_bglt)
								((BgL_rtl_switchz00_bglt) BgL_funz00_5957))))->BgL_patternsz00);
				{	/* SawC/code.scm 341 */
					obj_t BgL_regz00_6160;

					BgL_regz00_6160 = CAR(((obj_t) BgL_argsz00_5958));
					{	/* SawC/code.scm 342 */
						BgL_typez00_bglt BgL_tregz00_6161;

						BgL_tregz00_6161 =
							(((BgL_rtl_selectz00_bglt) COBJECT(
									((BgL_rtl_selectz00_bglt)
										((BgL_rtl_switchz00_bglt) BgL_funz00_5957))))->BgL_typez00);
						{	/* SawC/code.scm 343 */

							{	/* SawC/code.scm 344 */
								obj_t BgL_arg2320z00_6162;

								{	/* SawC/code.scm 344 */
									obj_t BgL_tmpz00_8287;

									BgL_tmpz00_8287 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg2320z00_6162 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8287);
								}
								bgl_display_string(BGl_string2822z00zzsaw_c_codez00,
									BgL_arg2320z00_6162);
							}
							BGl_genzd2regzd2zzsaw_c_codez00(BgL_regz00_6160);
							{	/* SawC/code.scm 346 */
								obj_t BgL_arg2321z00_6163;

								{	/* SawC/code.scm 346 */
									obj_t BgL_tmpz00_8292;

									BgL_tmpz00_8292 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg2321z00_6163 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8292);
								}
								bgl_display_string(BGl_string2823z00zzsaw_c_codez00,
									BgL_arg2321z00_6163);
							}
							{	/* SawC/code.scm 347 */
								obj_t BgL_g1664z00_6164;

								BgL_g1664z00_6164 =
									(((BgL_rtl_switchz00_bglt) COBJECT(
											((BgL_rtl_switchz00_bglt) BgL_funz00_5957)))->
									BgL_labelsz00);
								{
									obj_t BgL_ll1661z00_6166;
									obj_t BgL_ll1662z00_6167;

									BgL_ll1661z00_6166 = BgL_patsz00_6159;
									BgL_ll1662z00_6167 = BgL_g1664z00_6164;
								BgL_zc3z04anonymousza32322ze3z87_6165:
									if (NULLP(BgL_ll1661z00_6166))
										{	/* SawC/code.scm 347 */
											((bool_t) 1);
										}
									else
										{	/* SawC/code.scm 347 */
											{	/* SawC/code.scm 354 */
												obj_t BgL_patz00_6168;
												obj_t BgL_labz00_6169;

												BgL_patz00_6168 = CAR(((obj_t) BgL_ll1661z00_6166));
												BgL_labz00_6169 = CAR(((obj_t) BgL_ll1662z00_6167));
												if ((BgL_patz00_6168 == CNST_TABLE_REF(10)))
													{	/* SawC/code.scm 349 */
														obj_t BgL_arg2324z00_6170;

														{	/* SawC/code.scm 349 */
															obj_t BgL_tmpz00_8307;

															BgL_tmpz00_8307 = BGL_CURRENT_DYNAMIC_ENV();
															BgL_arg2324z00_6170 =
																BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8307);
														}
														bgl_display_string(BGl_string2824z00zzsaw_c_codez00,
															BgL_arg2324z00_6170);
													}
												else
													{
														obj_t BgL_l1659z00_6172;

														{	/* SawC/code.scm 350 */
															bool_t BgL_tmpz00_8311;

															BgL_l1659z00_6172 = BgL_patz00_6168;
														BgL_zc3z04anonymousza32325ze3z87_6171:
															if (PAIRP(BgL_l1659z00_6172))
																{	/* SawC/code.scm 350 */
																	{	/* SawC/code.scm 351 */
																		obj_t BgL_nz00_6173;

																		BgL_nz00_6173 = CAR(BgL_l1659z00_6172);
																		{	/* SawC/code.scm 351 */
																			obj_t BgL_arg2327z00_6174;

																			{	/* SawC/code.scm 351 */
																				obj_t BgL_tmpz00_8315;

																				BgL_tmpz00_8315 =
																					BGL_CURRENT_DYNAMIC_ENV();
																				BgL_arg2327z00_6174 =
																					BGL_ENV_CURRENT_OUTPUT_PORT
																					(BgL_tmpz00_8315);
																			}
																			bgl_display_string
																				(BGl_string2825z00zzsaw_c_codez00,
																				BgL_arg2327z00_6174);
																		}
																		BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00
																			(BgL_nz00_6173, BgL_tregz00_6161);
																		{	/* SawC/code.scm 353 */
																			obj_t BgL_arg2328z00_6175;

																			{	/* SawC/code.scm 353 */
																				obj_t BgL_tmpz00_8320;

																				BgL_tmpz00_8320 =
																					BGL_CURRENT_DYNAMIC_ENV();
																				BgL_arg2328z00_6175 =
																					BGL_ENV_CURRENT_OUTPUT_PORT
																					(BgL_tmpz00_8320);
																			}
																			bgl_display_string
																				(BGl_string2691z00zzsaw_c_codez00,
																				BgL_arg2328z00_6175);
																		}
																	}
																	{
																		obj_t BgL_l1659z00_8324;

																		BgL_l1659z00_8324 = CDR(BgL_l1659z00_6172);
																		BgL_l1659z00_6172 = BgL_l1659z00_8324;
																		goto BgL_zc3z04anonymousza32325ze3z87_6171;
																	}
																}
															else
																{	/* SawC/code.scm 350 */
																	BgL_tmpz00_8311 = ((bool_t) 1);
																}
															BBOOL(BgL_tmpz00_8311);
														}
													}
												{	/* SawC/code.scm 355 */
													int BgL_arg2331z00_6176;

													BgL_arg2331z00_6176 =
														(((BgL_blockz00_bglt) COBJECT(
																((BgL_blockz00_bglt) BgL_labz00_6169)))->
														BgL_labelz00);
													{	/* SawC/code.scm 355 */
														obj_t BgL_list2332z00_6177;

														{	/* SawC/code.scm 355 */
															obj_t BgL_arg2333z00_6178;

															{	/* SawC/code.scm 355 */
																obj_t BgL_arg2335z00_6179;

																BgL_arg2335z00_6179 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2826z00zzsaw_c_codez00, BNIL);
																BgL_arg2333z00_6178 =
																	MAKE_YOUNG_PAIR(BINT(BgL_arg2331z00_6176),
																	BgL_arg2335z00_6179);
															}
															BgL_list2332z00_6177 =
																MAKE_YOUNG_PAIR
																(BGl_string2827z00zzsaw_c_codez00,
																BgL_arg2333z00_6178);
														}
														BGl_displayza2za2zz__r4_output_6_10_3z00
															(BgL_list2332z00_6177);
											}}}
											{	/* SawC/code.scm 347 */
												obj_t BgL_arg2336z00_6180;
												obj_t BgL_arg2337z00_6181;

												BgL_arg2336z00_6180 = CDR(((obj_t) BgL_ll1661z00_6166));
												BgL_arg2337z00_6181 = CDR(((obj_t) BgL_ll1662z00_6167));
												{
													obj_t BgL_ll1662z00_8339;
													obj_t BgL_ll1661z00_8338;

													BgL_ll1661z00_8338 = BgL_arg2336z00_6180;
													BgL_ll1662z00_8339 = BgL_arg2337z00_6181;
													BgL_ll1662z00_6167 = BgL_ll1662z00_8339;
													BgL_ll1661z00_6166 = BgL_ll1661z00_8338;
													goto BgL_zc3z04anonymousza32322ze3z87_6165;
												}
											}
										}
								}
							}
							{	/* SawC/code.scm 358 */
								obj_t BgL_arg2338z00_6182;

								{	/* SawC/code.scm 358 */
									obj_t BgL_tmpz00_8340;

									BgL_tmpz00_8340 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg2338z00_6182 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8340);
								}
								return
									bgl_display_string(BGl_string2828z00zzsaw_c_codez00,
									BgL_arg2338z00_6182);
							}
						}
					}
				}
			}
		}

	}



/* &gen-expr-rtl_pragma1680 */
	obj_t BGl_z62genzd2exprzd2rtl_pragma1680z62zzsaw_c_codez00(obj_t
		BgL_envz00_5959, obj_t BgL_funz00_5960, obj_t BgL_argsz00_5961)
	{
		{	/* SawC/code.scm 331 */
			if (
				((((BgL_rtl_pragmaz00_bglt) COBJECT(
								((BgL_rtl_pragmaz00_bglt) BgL_funz00_5960)))->BgL_srfi0z00) ==
					CNST_TABLE_REF(11)))
				{	/* SawC/code.scm 333 */
					BGl_emitzd2pragmazd2zzsaw_c_codez00(
						(((BgL_rtl_pragmaz00_bglt) COBJECT(
									((BgL_rtl_pragmaz00_bglt) BgL_funz00_5960)))->BgL_formatz00),
						BgL_argsz00_5961);
					{	/* SawC/code.scm 336 */
						obj_t BgL_arg2317z00_6184;

						{	/* SawC/code.scm 336 */
							obj_t BgL_tmpz00_8352;

							BgL_tmpz00_8352 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2317z00_6184 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8352);
						}
						return
							bgl_display_string(BGl_string2690z00zzsaw_c_codez00,
							BgL_arg2317z00_6184);
					}
				}
			else
				{	/* SawC/code.scm 337 */
					obj_t BgL_arg2318z00_6185;

					{	/* SawC/code.scm 337 */
						obj_t BgL_tmpz00_8356;

						BgL_tmpz00_8356 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg2318z00_6185 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8356);
					}
					return
						bgl_display_string(BGl_string2829z00zzsaw_c_codez00,
						BgL_arg2318z00_6185);
				}
		}

	}



/* &gen-expr-rtl_protect1678 */
	obj_t BGl_z62genzd2exprzd2rtl_protect1678z62zzsaw_c_codez00(obj_t
		BgL_envz00_5962, obj_t BgL_funz00_5963, obj_t BgL_argsz00_5964)
	{
		{	/* SawC/code.scm 321 */
			{	/* SawC/code.scm 323 */
				obj_t BgL_arg2311z00_6187;

				{	/* SawC/code.scm 323 */
					obj_t BgL_tmpz00_8360;

					BgL_tmpz00_8360 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2311z00_6187 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8360);
				}
				bgl_display_string(BGl_string2830z00zzsaw_c_codez00,
					BgL_arg2311z00_6187);
			}
			{	/* SawC/code.scm 324 */
				obj_t BgL_arg2312z00_6188;

				{	/* SawC/code.scm 324 */
					obj_t BgL_tmpz00_8364;

					BgL_tmpz00_8364 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2312z00_6188 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8364);
				}
				bgl_display_string(BGl_string2831z00zzsaw_c_codez00,
					BgL_arg2312z00_6188);
			}
			{	/* SawC/code.scm 325 */
				obj_t BgL_arg2313z00_6189;

				{	/* SawC/code.scm 325 */
					obj_t BgL_tmpz00_8368;

					BgL_tmpz00_8368 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2313z00_6189 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8368);
				}
				bgl_display_string(BGl_string2832z00zzsaw_c_codez00,
					BgL_arg2313z00_6189);
			}
			bgl_display_string(BGl_string2833z00zzsaw_c_codez00,
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			bgl_display_string(BGl_string2834z00zzsaw_c_codez00,
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			return bgl_display_string(BGl_string2835z00zzsaw_c_codez00,
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
		}

	}



/* &gen-expr-rtl_funcall1676 */
	obj_t BGl_z62genzd2exprzd2rtl_funcall1676z62zzsaw_c_codez00(obj_t
		BgL_envz00_5965, obj_t BgL_funz00_5966, obj_t BgL_argsz00_5967)
	{
		{	/* SawC/code.scm 282 */
			if (CBOOL(BGl_za2stdcza2z00zzengine_paramz00))
				{	/* SawC/code.scm 283 */
					{	/* SawC/code.scm 284 */
						obj_t BgL_arg2306z00_6191;

						{	/* SawC/code.scm 284 */
							obj_t BgL_tmpz00_8377;

							BgL_tmpz00_8377 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2306z00_6191 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8377);
						}
						bgl_display_string(BGl_string2836z00zzsaw_c_codez00,
							BgL_arg2306z00_6191);
					}
					{	/* SawC/code.scm 285 */
						obj_t BgL_arg2307z00_6192;

						BgL_arg2307z00_6192 = CAR(((obj_t) BgL_argsz00_5967));
						BGl_genzd2regzd2zzsaw_c_codez00(BgL_arg2307z00_6192);
					}
					{	/* SawC/code.scm 286 */
						obj_t BgL_arg2308z00_6193;

						{	/* SawC/code.scm 286 */
							obj_t BgL_tmpz00_8384;

							BgL_tmpz00_8384 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2308z00_6193 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8384);
						}
						bgl_display_string(BGl_string2837z00zzsaw_c_codez00,
							BgL_arg2308z00_6193);
					}
					BGl_genzd2Xfuncallzd2zzsaw_c_codez00(BGl_string2690z00zzsaw_c_codez00,
						BGl_za2objza2z00zztype_cachez00, BgL_argsz00_5967, ((bool_t) 1));
					{	/* SawC/code.scm 288 */
						obj_t BgL_arg2309z00_6194;

						{	/* SawC/code.scm 288 */
							obj_t BgL_tmpz00_8389;

							BgL_tmpz00_8389 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2309z00_6194 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8389);
						}
						bgl_display_string(BGl_string2838z00zzsaw_c_codez00,
							BgL_arg2309z00_6194);
					}
					BGl_genzd2Xfuncallzd2zzsaw_c_codez00(BGl_string2690z00zzsaw_c_codez00,
						BGl_za2objza2z00zztype_cachez00, BgL_argsz00_5967, ((bool_t) 0));
					{	/* SawC/code.scm 290 */
						obj_t BgL_arg2310z00_6195;

						{	/* SawC/code.scm 290 */
							obj_t BgL_tmpz00_8394;

							BgL_tmpz00_8394 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2310z00_6195 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8394);
						}
						return
							bgl_display_string(BGl_string2686z00zzsaw_c_codez00,
							BgL_arg2310z00_6195);
					}
				}
			else
				{	/* SawC/code.scm 283 */
					return
						BGl_genzd2Xfuncallzd2zzsaw_c_codez00
						(BGl_string2690z00zzsaw_c_codez00, BGl_za2objza2z00zztype_cachez00,
						BgL_argsz00_5967, ((bool_t) 1));
				}
		}

	}



/* &gen-expr-rtl_setfiel1674 */
	obj_t BGl_z62genzd2exprzd2rtl_setfiel1674z62zzsaw_c_codez00(obj_t
		BgL_envz00_5968, obj_t BgL_funz00_5969, obj_t BgL_argsz00_5970)
	{
		{	/* SawC/code.scm 269 */
			{	/* SawC/code.scm 270 */
				obj_t BgL_arg2292z00_6197;

				{	/* SawC/code.scm 270 */
					obj_t BgL_tmpz00_8399;

					BgL_tmpz00_8399 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2292z00_6197 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8399);
				}
				bgl_display_string(BGl_string2839z00zzsaw_c_codez00,
					BgL_arg2292z00_6197);
			}
			{	/* SawC/code.scm 271 */
				obj_t BgL_arg2293z00_6198;
				obj_t BgL_arg2294z00_6199;

				BgL_arg2293z00_6198 =
					(((BgL_typez00_bglt) COBJECT(
							(((BgL_rtl_setfieldz00_bglt) COBJECT(
										((BgL_rtl_setfieldz00_bglt) BgL_funz00_5969)))->
								BgL_objtypez00)))->BgL_namez00);
				{	/* SawC/code.scm 271 */
					obj_t BgL_tmpz00_8406;

					BgL_tmpz00_8406 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2294z00_6199 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8406);
				}
				bgl_display_obj(BgL_arg2293z00_6198, BgL_arg2294z00_6199);
			}
			{	/* SawC/code.scm 272 */
				obj_t BgL_arg2296z00_6200;

				{	/* SawC/code.scm 272 */
					obj_t BgL_tmpz00_8410;

					BgL_tmpz00_8410 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2296z00_6200 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8410);
				}
				bgl_display_string(BGl_string2685z00zzsaw_c_codez00,
					BgL_arg2296z00_6200);
			}
			BGl_genzd2prefixzd2zzsaw_c_codez00(
				((BgL_rtl_funz00_bglt) ((BgL_rtl_setfieldz00_bglt) BgL_funz00_5969)));
			{	/* SawC/code.scm 274 */
				obj_t BgL_arg2297z00_6201;

				BgL_arg2297z00_6201 = CAR(((obj_t) BgL_argsz00_5970));
				BGl_genzd2regzd2zzsaw_c_codez00(BgL_arg2297z00_6201);
			}
			{	/* SawC/code.scm 275 */
				obj_t BgL_arg2298z00_6202;

				{	/* SawC/code.scm 275 */
					obj_t BgL_tmpz00_8420;

					BgL_tmpz00_8420 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2298z00_6202 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8420);
				}
				bgl_display_string(BGl_string2840z00zzsaw_c_codez00,
					BgL_arg2298z00_6202);
			}
			{	/* SawC/code.scm 276 */
				obj_t BgL_arg2299z00_6203;
				obj_t BgL_arg2301z00_6204;

				BgL_arg2299z00_6203 =
					(((BgL_rtl_setfieldz00_bglt) COBJECT(
							((BgL_rtl_setfieldz00_bglt) BgL_funz00_5969)))->BgL_namez00);
				{	/* SawC/code.scm 276 */
					obj_t BgL_tmpz00_8426;

					BgL_tmpz00_8426 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2301z00_6204 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8426);
				}
				bgl_display_obj(BgL_arg2299z00_6203, BgL_arg2301z00_6204);
			}
			{	/* SawC/code.scm 277 */
				obj_t BgL_arg2302z00_6205;

				{	/* SawC/code.scm 277 */
					obj_t BgL_tmpz00_8430;

					BgL_tmpz00_8430 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2302z00_6205 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8430);
				}
				bgl_display_string(BGl_string2840z00zzsaw_c_codez00,
					BgL_arg2302z00_6205);
			}
			{	/* SawC/code.scm 278 */
				obj_t BgL_arg2304z00_6206;

				{	/* SawC/code.scm 278 */
					obj_t BgL_pairz00_6207;

					BgL_pairz00_6207 = CDR(((obj_t) BgL_argsz00_5970));
					BgL_arg2304z00_6206 = CAR(BgL_pairz00_6207);
				}
				BGl_genzd2regzd2zzsaw_c_codez00(BgL_arg2304z00_6206);
			}
			{	/* SawC/code.scm 279 */
				obj_t BgL_arg2305z00_6208;

				{	/* SawC/code.scm 279 */
					obj_t BgL_tmpz00_8438;

					BgL_tmpz00_8438 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2305z00_6208 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8438);
				}
				return
					bgl_display_string(BGl_string2686z00zzsaw_c_codez00,
					BgL_arg2305z00_6208);
			}
		}

	}



/* &gen-expr-rtl_getfiel1672 */
	obj_t BGl_z62genzd2exprzd2rtl_getfiel1672z62zzsaw_c_codez00(obj_t
		BgL_envz00_5971, obj_t BgL_funz00_5972, obj_t BgL_argsz00_5973)
	{
		{	/* SawC/code.scm 259 */
			{	/* SawC/code.scm 260 */
				obj_t BgL_arg2281z00_6210;

				{	/* SawC/code.scm 260 */
					obj_t BgL_tmpz00_8442;

					BgL_tmpz00_8442 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2281z00_6210 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8442);
				}
				bgl_display_string(BGl_string2841z00zzsaw_c_codez00,
					BgL_arg2281z00_6210);
			}
			{	/* SawC/code.scm 261 */
				obj_t BgL_arg2282z00_6211;
				obj_t BgL_arg2283z00_6212;

				BgL_arg2282z00_6211 =
					(((BgL_typez00_bglt) COBJECT(
							(((BgL_rtl_getfieldz00_bglt) COBJECT(
										((BgL_rtl_getfieldz00_bglt) BgL_funz00_5972)))->
								BgL_objtypez00)))->BgL_namez00);
				{	/* SawC/code.scm 261 */
					obj_t BgL_tmpz00_8449;

					BgL_tmpz00_8449 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2283z00_6212 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8449);
				}
				bgl_display_obj(BgL_arg2282z00_6211, BgL_arg2283z00_6212);
			}
			{	/* SawC/code.scm 262 */
				obj_t BgL_arg2286z00_6213;

				{	/* SawC/code.scm 262 */
					obj_t BgL_tmpz00_8453;

					BgL_tmpz00_8453 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2286z00_6213 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8453);
				}
				bgl_display_string(BGl_string2685z00zzsaw_c_codez00,
					BgL_arg2286z00_6213);
			}
			BGl_genzd2prefixzd2zzsaw_c_codez00(
				((BgL_rtl_funz00_bglt) ((BgL_rtl_getfieldz00_bglt) BgL_funz00_5972)));
			{	/* SawC/code.scm 264 */
				obj_t BgL_arg2287z00_6214;

				BgL_arg2287z00_6214 = CAR(((obj_t) BgL_argsz00_5973));
				BGl_genzd2regzd2zzsaw_c_codez00(BgL_arg2287z00_6214);
			}
			{	/* SawC/code.scm 265 */
				obj_t BgL_arg2288z00_6215;

				{	/* SawC/code.scm 265 */
					obj_t BgL_tmpz00_8463;

					BgL_tmpz00_8463 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2288z00_6215 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8463);
				}
				bgl_display_string(BGl_string2685z00zzsaw_c_codez00,
					BgL_arg2288z00_6215);
			}
			{	/* SawC/code.scm 266 */
				obj_t BgL_arg2289z00_6216;
				obj_t BgL_arg2290z00_6217;

				BgL_arg2289z00_6216 =
					(((BgL_rtl_getfieldz00_bglt) COBJECT(
							((BgL_rtl_getfieldz00_bglt) BgL_funz00_5972)))->BgL_namez00);
				{	/* SawC/code.scm 266 */
					obj_t BgL_tmpz00_8469;

					BgL_tmpz00_8469 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2290z00_6217 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8469);
				}
				bgl_display_obj(BgL_arg2289z00_6216, BgL_arg2290z00_6217);
			}
			{	/* SawC/code.scm 267 */
				obj_t BgL_arg2291z00_6218;

				{	/* SawC/code.scm 267 */
					obj_t BgL_tmpz00_8473;

					BgL_tmpz00_8473 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg2291z00_6218 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_8473);
				}
				return
					bgl_display_string(BGl_string2686z00zzsaw_c_codez00,
					BgL_arg2291z00_6218);
			}
		}

	}



/* &gen-expr-rtl_lightfu1670 */
	obj_t BGl_z62genzd2exprzd2rtl_lightfu1670z62zzsaw_c_codez00(obj_t
		BgL_envz00_5974, obj_t BgL_funz00_5975, obj_t BgL_argsz00_5976)
	{
		{	/* SawC/code.scm 255 */
			return
				BGl_genzd2Xfuncallzd2zzsaw_c_codez00(BGl_string2842z00zzsaw_c_codez00,
				(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
							((BgL_rtl_lightfuncallz00_bglt) BgL_funz00_5975)))->
					BgL_rettypez00), BgL_argsz00_5976, ((bool_t) 0));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_c_codez00(void)
	{
		{	/* SawC/code.scm 1 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzcnst_allocz00(192699986L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzbackend_cvmz00(18449009L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(474089076L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_woodcutterz00(352965511L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(20930040L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_exprz00(142709001L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			BGl_modulezd2initializa7ationz75zzsaw_registerzd2allocationzd2(250697396L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvz00(263563890L,
				BSTRING_TO_STRING(BGl_string2843z00zzsaw_c_codez00));
		}

	}

#ifdef __cplusplus
}
#endif
