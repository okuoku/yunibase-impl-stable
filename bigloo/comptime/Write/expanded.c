/*===========================================================================*/
/*   (Write/expanded.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Write/expanded.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_WRITE_EXPANDED_TYPE_DEFINITIONS
#define BGL_WRITE_EXPANDED_TYPE_DEFINITIONS
#endif													// BGL_WRITE_EXPANDED_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzwrite_expandedz00 = BUNSPEC;
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzwrite_expandedz00(void);
	static obj_t BGl_genericzd2initzd2zzwrite_expandedz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzwrite_expandedz00(void);
	BGL_IMPORT obj_t BGl_ppz00zz__ppz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzwrite_expandedz00(void);
	extern obj_t BGl_writezd2schemezd2commentz00zzwrite_schemez00(obj_t, obj_t);
	extern obj_t BGl_writezd2schemezd2filezd2headerzd2zzwrite_schemez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	extern obj_t BGl_za2modulezd2clauseza2zd2zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzwrite_expandedz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzwrite_schemez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_z62zc3z04anonymousza31188ze3ze5zzwrite_expandedz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzwrite_expandedz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzwrite_expandedz00(void);
	static obj_t BGl_z62writezd2expandedzb0zzwrite_expandedz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzwrite_expandedz00(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzwrite_expandedz00(void);
	BGL_EXPORTED_DECL obj_t BGl_writezd2expandedzd2zzwrite_expandedz00(obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string1210z00zzwrite_expandedz00,
		BgL_bgl_string1210za700za7za7w1228za7, ".escm", 5);
	      DEFINE_STRING(BGl_string1211z00zzwrite_expandedz00,
		BgL_bgl_string1211za700za7za7w1229za7, "The expanded module", 19);
	      DEFINE_STRING(BGl_string1212z00zzwrite_expandedz00,
		BgL_bgl_string1212za700za7za7w1230za7,
		"---------------------------------------------------------", 57);
	      DEFINE_STRING(BGl_string1213z00zzwrite_expandedz00,
		BgL_bgl_string1213za700za7za7w1231za7,
		"!!! WARNING !!!      !!! WARNING !!!      !!! WARNING !!!", 57);
	      DEFINE_STRING(BGl_string1214z00zzwrite_expandedz00,
		BgL_bgl_string1214za700za7za7w1232za7,
		"This expanded file cannot be compiled \"as is\". In order to", 58);
	      DEFINE_STRING(BGl_string1215z00zzwrite_expandedz00,
		BgL_bgl_string1215za700za7za7w1233za7, "compile it:", 11);
	      DEFINE_STRING(BGl_string1216z00zzwrite_expandedz00,
		BgL_bgl_string1216za700za7za7w1234za7,
		"   - the explicit call to the MODULE-INITIALIZATION ", 52);
	      DEFINE_STRING(BGl_string1217z00zzwrite_expandedz00,
		BgL_bgl_string1217za700za7za7w1235za7, "     must be removed.", 21);
	      DEFINE_STRING(BGl_string1218z00zzwrite_expandedz00,
		BgL_bgl_string1218za700za7za7w1236za7,
		"   - If the source module was INCLUDING files,", 46);
	      DEFINE_STRING(BGl_string1219z00zzwrite_expandedz00,
		BgL_bgl_string1219za700za7za7w1237za7,
		"     you must select manually which files still have to", 55);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_writezd2expandedzd2envz00zzwrite_expandedz00,
		BgL_bgl_za762writeza7d2expan1238z00,
		BGl_z62writezd2expandedzb0zzwrite_expandedz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1220z00zzwrite_expandedz00,
		BgL_bgl_string1220za700za7za7w1239za7,
		"     be included in the expanded forms.", 39);
	      DEFINE_STRING(BGl_string1221z00zzwrite_expandedz00,
		BgL_bgl_string1221za700za7za7w1240za7, "The module clause", 17);
	      DEFINE_STRING(BGl_string1222z00zzwrite_expandedz00,
		BgL_bgl_string1222za700za7za7w1241za7, "unit: ", 6);
	      DEFINE_STRING(BGl_string1223z00zzwrite_expandedz00,
		BgL_bgl_string1223za700za7za7w1242za7, "write-expanded", 14);
	      DEFINE_STRING(BGl_string1224z00zzwrite_expandedz00,
		BgL_bgl_string1224za700za7za7w1243za7, "Can't open output file", 22);
	      DEFINE_STRING(BGl_string1225z00zzwrite_expandedz00,
		BgL_bgl_string1225za700za7za7w1244za7, "write_expanded", 14);
	      DEFINE_STRING(BGl_string1226z00zzwrite_expandedz00,
		BgL_bgl_string1226za700za7za7w1245za7, "define-inline define --to-stdout ",
		33);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzwrite_expandedz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzwrite_expandedz00(long
		BgL_checksumz00_261, char *BgL_fromz00_262)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzwrite_expandedz00))
				{
					BGl_requirezd2initializa7ationz75zzwrite_expandedz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzwrite_expandedz00();
					BGl_libraryzd2moduleszd2initz00zzwrite_expandedz00();
					BGl_cnstzd2initzd2zzwrite_expandedz00();
					BGl_importedzd2moduleszd2initz00zzwrite_expandedz00();
					return BGl_toplevelzd2initzd2zzwrite_expandedz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzwrite_expandedz00(void)
	{
		{	/* Write/expanded.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"write_expanded");
			BGl_modulezd2initializa7ationz75zz__ppz00(0L, "write_expanded");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "write_expanded");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"write_expanded");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "write_expanded");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "write_expanded");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"write_expanded");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"write_expanded");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"write_expanded");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "write_expanded");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"write_expanded");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "write_expanded");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "write_expanded");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzwrite_expandedz00(void)
	{
		{	/* Write/expanded.scm 15 */
			{	/* Write/expanded.scm 15 */
				obj_t BgL_cportz00_249;

				{	/* Write/expanded.scm 15 */
					obj_t BgL_stringz00_256;

					BgL_stringz00_256 = BGl_string1226z00zzwrite_expandedz00;
					{	/* Write/expanded.scm 15 */
						obj_t BgL_startz00_257;

						BgL_startz00_257 = BINT(0L);
						{	/* Write/expanded.scm 15 */
							obj_t BgL_endz00_258;

							BgL_endz00_258 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_256)));
							{	/* Write/expanded.scm 15 */

								BgL_cportz00_249 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_256, BgL_startz00_257, BgL_endz00_258);
				}}}}
				{
					long BgL_iz00_250;

					BgL_iz00_250 = 2L;
				BgL_loopz00_251:
					if ((BgL_iz00_250 == -1L))
						{	/* Write/expanded.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Write/expanded.scm 15 */
							{	/* Write/expanded.scm 15 */
								obj_t BgL_arg1227z00_252;

								{	/* Write/expanded.scm 15 */

									{	/* Write/expanded.scm 15 */
										obj_t BgL_locationz00_254;

										BgL_locationz00_254 = BBOOL(((bool_t) 0));
										{	/* Write/expanded.scm 15 */

											BgL_arg1227z00_252 =
												BGl_readz00zz__readerz00(BgL_cportz00_249,
												BgL_locationz00_254);
										}
									}
								}
								{	/* Write/expanded.scm 15 */
									int BgL_tmpz00_293;

									BgL_tmpz00_293 = (int) (BgL_iz00_250);
									CNST_TABLE_SET(BgL_tmpz00_293, BgL_arg1227z00_252);
							}}
							{	/* Write/expanded.scm 15 */
								int BgL_auxz00_255;

								BgL_auxz00_255 = (int) ((BgL_iz00_250 - 1L));
								{
									long BgL_iz00_298;

									BgL_iz00_298 = (long) (BgL_auxz00_255);
									BgL_iz00_250 = BgL_iz00_298;
									goto BgL_loopz00_251;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzwrite_expandedz00(void)
	{
		{	/* Write/expanded.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzwrite_expandedz00(void)
	{
		{	/* Write/expanded.scm 15 */
			return BUNSPEC;
		}

	}



/* write-expanded */
	BGL_EXPORTED_DEF obj_t BGl_writezd2expandedzd2zzwrite_expandedz00(obj_t
		BgL_unitsz00_25)
	{
		{	/* Write/expanded.scm 27 */
			{	/* Write/expanded.scm 28 */
				obj_t BgL_outputzd2namezd2_57;

				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* Write/expanded.scm 29 */
						BgL_outputzd2namezd2_57 = BGl_za2destza2z00zzengine_paramz00;
					}
				else
					{	/* Write/expanded.scm 29 */
						if ((BGl_za2destza2z00zzengine_paramz00 == CNST_TABLE_REF(0)))
							{	/* Write/expanded.scm 31 */
								BgL_outputzd2namezd2_57 = BFALSE;
							}
						else
							{	/* Write/expanded.scm 33 */
								bool_t BgL_test1250z00_306;

								if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
									{	/* Write/expanded.scm 34 */
										obj_t BgL_tmpz00_309;

										BgL_tmpz00_309 =
											CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
										BgL_test1250z00_306 = STRINGP(BgL_tmpz00_309);
									}
								else
									{	/* Write/expanded.scm 33 */
										BgL_test1250z00_306 = ((bool_t) 0);
									}
								if (BgL_test1250z00_306)
									{	/* Write/expanded.scm 33 */
										BgL_outputzd2namezd2_57 =
											string_append(BGl_prefixz00zz__osz00(CAR
												(BGl_za2srczd2filesza2zd2zzengine_paramz00)),
											BGl_string1210z00zzwrite_expandedz00);
									}
								else
									{	/* Write/expanded.scm 33 */
										BgL_outputzd2namezd2_57 = BFALSE;
									}
							}
					}
				{	/* Write/expanded.scm 28 */
					obj_t BgL_portz00_58;

					if (STRINGP(BgL_outputzd2namezd2_57))
						{	/* Write/expanded.scm 40 */

							BgL_portz00_58 =
								BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_outputzd2namezd2_57, BTRUE);
						}
					else
						{	/* Write/expanded.scm 41 */
							obj_t BgL_tmpz00_318;

							BgL_tmpz00_318 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_portz00_58 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_318);
						}
					{	/* Write/expanded.scm 39 */

						if (OUTPUT_PORTP(BgL_portz00_58))
							{	/* Write/expanded.scm 44 */
								obj_t BgL_exitd1048z00_60;

								BgL_exitd1048z00_60 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Write/expanded.scm 115 */
									obj_t BgL_zc3z04anonymousza31188ze3z87_243;

									BgL_zc3z04anonymousza31188ze3z87_243 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31188ze3ze5zzwrite_expandedz00,
										(int) (0L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31188ze3z87_243,
										(int) (0L), BgL_portz00_58);
									{	/* Write/expanded.scm 44 */
										obj_t BgL_arg1828z00_195;

										{	/* Write/expanded.scm 44 */
											obj_t BgL_arg1829z00_196;

											BgL_arg1829z00_196 =
												BGL_EXITD_PROTECT(BgL_exitd1048z00_60);
											BgL_arg1828z00_195 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31188ze3z87_243,
												BgL_arg1829z00_196);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1048z00_60,
											BgL_arg1828z00_195);
										BUNSPEC;
									}
									{	/* Write/expanded.scm 46 */
										bool_t BgL_tmp1050z00_62;

										BGl_writezd2schemezd2filezd2headerzd2zzwrite_schemez00
											(BgL_portz00_58, BGl_string1211z00zzwrite_expandedz00);
										{	/* Write/expanded.scm 47 */
											obj_t BgL_list1080z00_63;

											BgL_list1080z00_63 =
												MAKE_YOUNG_PAIR(BGl_string1212z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1080z00_63);
										}
										{	/* Write/expanded.scm 50 */
											obj_t BgL_list1081z00_64;

											BgL_list1081z00_64 =
												MAKE_YOUNG_PAIR(BGl_string1213z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1081z00_64);
										}
										{	/* Write/expanded.scm 53 */
											obj_t BgL_list1082z00_65;

											BgL_list1082z00_65 =
												MAKE_YOUNG_PAIR(BGl_string1212z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1082z00_65);
										}
										{	/* Write/expanded.scm 56 */
											obj_t BgL_list1083z00_66;

											BgL_list1083z00_66 =
												MAKE_YOUNG_PAIR(BGl_string1214z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1083z00_66);
										}
										{	/* Write/expanded.scm 59 */
											obj_t BgL_list1084z00_67;

											BgL_list1084z00_67 =
												MAKE_YOUNG_PAIR(BGl_string1215z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1084z00_67);
										}
										{	/* Write/expanded.scm 62 */
											obj_t BgL_list1085z00_68;

											BgL_list1085z00_68 =
												MAKE_YOUNG_PAIR(BGl_string1216z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1085z00_68);
										}
										{	/* Write/expanded.scm 65 */
											obj_t BgL_list1086z00_69;

											BgL_list1086z00_69 =
												MAKE_YOUNG_PAIR(BGl_string1217z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1086z00_69);
										}
										{	/* Write/expanded.scm 68 */
											obj_t BgL_list1087z00_70;

											BgL_list1087z00_70 =
												MAKE_YOUNG_PAIR(BGl_string1218z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1087z00_70);
										}
										{	/* Write/expanded.scm 71 */
											obj_t BgL_list1088z00_71;

											BgL_list1088z00_71 =
												MAKE_YOUNG_PAIR(BGl_string1219z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1088z00_71);
										}
										{	/* Write/expanded.scm 74 */
											obj_t BgL_list1089z00_72;

											BgL_list1089z00_72 =
												MAKE_YOUNG_PAIR(BGl_string1220z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1089z00_72);
										}
										{	/* Write/expanded.scm 77 */
											obj_t BgL_list1090z00_73;

											BgL_list1090z00_73 =
												MAKE_YOUNG_PAIR(BGl_string1212z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1090z00_73);
										}
										{	/* Write/expanded.scm 80 */
											obj_t BgL_list1091z00_74;

											BgL_list1091z00_74 =
												MAKE_YOUNG_PAIR(BGl_string1221z00zzwrite_expandedz00,
												BNIL);
											BGl_writezd2schemezd2commentz00zzwrite_schemez00
												(BgL_portz00_58, BgL_list1091z00_74);
										}
										{	/* Write/expanded.scm 81 */
											obj_t BgL_list1092z00_75;

											BgL_list1092z00_75 =
												MAKE_YOUNG_PAIR(BgL_portz00_58, BNIL);
											BGl_ppz00zz__ppz00
												(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00,
												BgL_list1092z00_75);
										}
										bgl_display_char(((unsigned char) 10), BgL_portz00_58);
										{
											obj_t BgL_l1057z00_77;

											BgL_l1057z00_77 = BgL_unitsz00_25;
										BgL_zc3z04anonymousza31093ze3z87_78:
											if (PAIRP(BgL_l1057z00_77))
												{	/* Write/expanded.scm 83 */
													{	/* Write/expanded.scm 85 */
														obj_t BgL_uz00_80;

														BgL_uz00_80 = CAR(BgL_l1057z00_77);
														if (CBOOL(STRUCT_REF(
																	((obj_t) BgL_uz00_80), (int) (3L))))
															{	/* Write/expanded.scm 85 */
																{	/* Write/expanded.scm 87 */
																	bool_t BgL_test1258z00_368;

																	{	/* Write/expanded.scm 87 */
																		obj_t BgL_tmpz00_369;

																		BgL_tmpz00_369 =
																			STRUCT_REF(
																			((obj_t) BgL_uz00_80), (int) (2L));
																		BgL_test1258z00_368 = PAIRP(BgL_tmpz00_369);
																	}
																	if (BgL_test1258z00_368)
																		{	/* Write/expanded.scm 92 */
																			obj_t BgL_arg1102z00_84;

																			{	/* Write/expanded.scm 92 */
																				obj_t BgL_arg1104z00_86;

																				{	/* Write/expanded.scm 92 */
																					obj_t BgL_arg1114z00_87;

																					BgL_arg1114z00_87 =
																						STRUCT_REF(
																						((obj_t) BgL_uz00_80), (int) (0L));
																					{	/* Write/expanded.scm 91 */
																						obj_t BgL_arg1455z00_203;

																						BgL_arg1455z00_203 =
																							SYMBOL_TO_STRING(
																							((obj_t) BgL_arg1114z00_87));
																						BgL_arg1104z00_86 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_203);
																				}}
																				BgL_arg1102z00_84 =
																					string_append
																					(BGl_string1222z00zzwrite_expandedz00,
																					BgL_arg1104z00_86);
																			}
																			{	/* Write/expanded.scm 88 */
																				obj_t BgL_list1103z00_85;

																				BgL_list1103z00_85 =
																					MAKE_YOUNG_PAIR(BgL_arg1102z00_84,
																					BNIL);
																				BGl_writezd2schemezd2commentz00zzwrite_schemez00
																					(BgL_portz00_58, BgL_list1103z00_85);
																		}}
																	else
																		{	/* Write/expanded.scm 87 */
																			BFALSE;
																		}
																}
																{	/* Write/expanded.scm 94 */
																	obj_t BgL_g1056z00_89;

																	{	/* Write/expanded.scm 109 */
																		obj_t BgL_codez00_156;

																		BgL_codez00_156 =
																			STRUCT_REF(
																			((obj_t) BgL_uz00_80), (int) (2L));
																		if (PROCEDUREP(BgL_codez00_156))
																			{	/* Write/expanded.scm 110 */
																				BgL_g1056z00_89 =
																					BGL_PROCEDURE_CALL0(BgL_codez00_156);
																			}
																		else
																			{	/* Write/expanded.scm 110 */
																				BgL_g1056z00_89 = BgL_codez00_156;
																			}
																	}
																	{
																		obj_t BgL_l1054z00_91;

																		BgL_l1054z00_91 = BgL_g1056z00_89;
																	BgL_zc3z04anonymousza31116ze3z87_92:
																		if (PAIRP(BgL_l1054z00_91))
																			{	/* Write/expanded.scm 109 */
																				{	/* Write/expanded.scm 97 */
																					obj_t BgL_codez00_94;

																					BgL_codez00_94 = CAR(BgL_l1054z00_91);
																					{
																						obj_t BgL_namez00_99;
																						obj_t BgL_valuez00_100;

																						if (PAIRP(BgL_codez00_94))
																							{	/* Write/expanded.scm 97 */
																								obj_t BgL_cdrzd2113zd2_109;

																								BgL_cdrzd2113zd2_109 =
																									CDR(((obj_t) BgL_codez00_94));
																								if (
																									(CAR(
																											((obj_t) BgL_codez00_94))
																										== CNST_TABLE_REF(1)))
																									{	/* Write/expanded.scm 97 */
																										if (PAIRP
																											(BgL_cdrzd2113zd2_109))
																											{	/* Write/expanded.scm 97 */
																												obj_t
																													BgL_carzd2117zd2_113;
																												obj_t
																													BgL_cdrzd2118zd2_114;
																												BgL_carzd2117zd2_113 =
																													CAR
																													(BgL_cdrzd2113zd2_109);
																												BgL_cdrzd2118zd2_114 =
																													CDR
																													(BgL_cdrzd2113zd2_109);
																												if (PAIRP
																													(BgL_carzd2117zd2_113))
																													{	/* Write/expanded.scm 97 */
																														if (PAIRP
																															(BgL_cdrzd2118zd2_114))
																															{	/* Write/expanded.scm 97 */
																																if (NULLP(CDR
																																		(BgL_cdrzd2118zd2_114)))
																																	{	/* Write/expanded.scm 97 */
																																		obj_t
																																			BgL_arg1129z00_119;
																																		BgL_arg1129z00_119
																																			=
																																			CAR
																																			(BgL_carzd2117zd2_113);
																																		{	/* Write/expanded.scm 99 */
																																			obj_t
																																				BgL_list1172z00_218;
																																			BgL_list1172z00_218
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1129z00_119,
																																				BNIL);
																																			BGl_writezd2schemezd2commentz00zzwrite_schemez00
																																				(BgL_portz00_58,
																																				BgL_list1172z00_218);
																																		}
																																		{	/* Write/expanded.scm 100 */
																																			obj_t
																																				BgL_list1173z00_219;
																																			BgL_list1173z00_219
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_portz00_58,
																																				BNIL);
																																			BGl_ppz00zz__ppz00
																																				(BgL_codez00_94,
																																				BgL_list1173z00_219);
																																		}
																																	}
																																else
																																	{	/* Write/expanded.scm 97 */
																																	BgL_tagzd2104zd2_106:
																																		{	/* Write/expanded.scm 108 */
																																			obj_t
																																				BgL_list1178z00_153;
																																			BgL_list1178z00_153
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_portz00_58,
																																				BNIL);
																																			BGl_ppz00zz__ppz00
																																				(BgL_codez00_94,
																																				BgL_list1178z00_153);
																																		}
																																	}
																															}
																														else
																															{	/* Write/expanded.scm 97 */
																																goto
																																	BgL_tagzd2104zd2_106;
																															}
																													}
																												else
																													{	/* Write/expanded.scm 97 */
																														obj_t
																															BgL_cdrzd2162zd2_124;
																														BgL_cdrzd2162zd2_124
																															=
																															CDR(((obj_t)
																																BgL_cdrzd2113zd2_109));
																														if (PAIRP
																															(BgL_cdrzd2162zd2_124))
																															{	/* Write/expanded.scm 97 */
																																if (NULLP(CDR
																																		(BgL_cdrzd2162zd2_124)))
																																	{	/* Write/expanded.scm 97 */
																																		obj_t
																																			BgL_arg1142z00_128;
																																		obj_t
																																			BgL_arg1143z00_129;
																																		BgL_arg1142z00_128
																																			=
																																			CAR((
																																				(obj_t)
																																				BgL_cdrzd2113zd2_109));
																																		BgL_arg1143z00_129
																																			=
																																			CAR
																																			(BgL_cdrzd2162zd2_124);
																																		BgL_namez00_99
																																			=
																																			BgL_arg1142z00_128;
																																		BgL_valuez00_100
																																			=
																																			BgL_arg1143z00_129;
																																		{	/* Write/expanded.scm 102 */
																																			obj_t
																																				BgL_list1174z00_149;
																																			BgL_list1174z00_149
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_namez00_99,
																																				BNIL);
																																			BGl_writezd2schemezd2commentz00zzwrite_schemez00
																																				(BgL_portz00_58,
																																				BgL_list1174z00_149);
																																		}
																																		{	/* Write/expanded.scm 103 */
																																			obj_t
																																				BgL_list1175z00_150;
																																			BgL_list1175z00_150
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_portz00_58,
																																				BNIL);
																																			BGl_ppz00zz__ppz00
																																				(BgL_codez00_94,
																																				BgL_list1175z00_150);
																																		}
																																	}
																																else
																																	{	/* Write/expanded.scm 97 */
																																		goto
																																			BgL_tagzd2104zd2_106;
																																	}
																															}
																														else
																															{	/* Write/expanded.scm 97 */
																																goto
																																	BgL_tagzd2104zd2_106;
																															}
																													}
																											}
																										else
																											{	/* Write/expanded.scm 97 */
																												goto
																													BgL_tagzd2104zd2_106;
																											}
																									}
																								else
																									{	/* Write/expanded.scm 97 */
																										if (
																											(CAR(
																													((obj_t)
																														BgL_codez00_94)) ==
																												CNST_TABLE_REF(2)))
																											{	/* Write/expanded.scm 97 */
																												if (PAIRP
																													(BgL_cdrzd2113zd2_109))
																													{	/* Write/expanded.scm 97 */
																														obj_t
																															BgL_carzd2207zd2_135;
																														obj_t
																															BgL_cdrzd2208zd2_136;
																														BgL_carzd2207zd2_135
																															=
																															CAR
																															(BgL_cdrzd2113zd2_109);
																														BgL_cdrzd2208zd2_136
																															=
																															CDR
																															(BgL_cdrzd2113zd2_109);
																														if (PAIRP
																															(BgL_carzd2207zd2_135))
																															{	/* Write/expanded.scm 97 */
																																if (PAIRP
																																	(BgL_cdrzd2208zd2_136))
																																	{	/* Write/expanded.scm 97 */
																																		if (NULLP
																																			(CDR
																																				(BgL_cdrzd2208zd2_136)))
																																			{	/* Write/expanded.scm 97 */
																																				obj_t
																																					BgL_arg1157z00_141;
																																				BgL_arg1157z00_141
																																					=
																																					CAR
																																					(BgL_carzd2207zd2_135);
																																				{	/* Write/expanded.scm 105 */
																																					obj_t
																																						BgL_list1176z00_233;
																																					BgL_list1176z00_233
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1157z00_141,
																																						BNIL);
																																					BGl_writezd2schemezd2commentz00zzwrite_schemez00
																																						(BgL_portz00_58,
																																						BgL_list1176z00_233);
																																				}
																																				{	/* Write/expanded.scm 106 */
																																					obj_t
																																						BgL_list1177z00_234;
																																					BgL_list1177z00_234
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_portz00_58,
																																						BNIL);
																																					BGl_ppz00zz__ppz00
																																						(BgL_codez00_94,
																																						BgL_list1177z00_234);
																																				}
																																			}
																																		else
																																			{	/* Write/expanded.scm 97 */
																																				goto
																																					BgL_tagzd2104zd2_106;
																																			}
																																	}
																																else
																																	{	/* Write/expanded.scm 97 */
																																		goto
																																			BgL_tagzd2104zd2_106;
																																	}
																															}
																														else
																															{	/* Write/expanded.scm 97 */
																																goto
																																	BgL_tagzd2104zd2_106;
																															}
																													}
																												else
																													{	/* Write/expanded.scm 97 */
																														goto
																															BgL_tagzd2104zd2_106;
																													}
																											}
																										else
																											{	/* Write/expanded.scm 97 */
																												goto
																													BgL_tagzd2104zd2_106;
																											}
																									}
																							}
																						else
																							{	/* Write/expanded.scm 97 */
																								goto BgL_tagzd2104zd2_106;
																							}
																					}
																				}
																				{
																					obj_t BgL_l1054z00_456;

																					BgL_l1054z00_456 =
																						CDR(BgL_l1054z00_91);
																					BgL_l1054z00_91 = BgL_l1054z00_456;
																					goto
																						BgL_zc3z04anonymousza31116ze3z87_92;
																				}
																			}
																		else
																			{	/* Write/expanded.scm 109 */
																				((bool_t) 1);
																			}
																	}
																}
																{	/* Write/expanded.scm 113 */
																	obj_t BgL_tmpz00_458;

																	BgL_tmpz00_458 = ((obj_t) BgL_portz00_58);
																	bgl_display_char(((unsigned char) 10),
																		BgL_tmpz00_458);
															}}
														else
															{	/* Write/expanded.scm 85 */
																BFALSE;
															}
													}
													{
														obj_t BgL_l1057z00_461;

														BgL_l1057z00_461 = CDR(BgL_l1057z00_77);
														BgL_l1057z00_77 = BgL_l1057z00_461;
														goto BgL_zc3z04anonymousza31093ze3z87_78;
													}
												}
											else
												{	/* Write/expanded.scm 83 */
													BgL_tmp1050z00_62 = ((bool_t) 1);
												}
										}
										{	/* Write/expanded.scm 44 */
											bool_t BgL_test1283z00_463;

											{	/* Write/expanded.scm 44 */
												obj_t BgL_arg1827z00_239;

												BgL_arg1827z00_239 =
													BGL_EXITD_PROTECT(BgL_exitd1048z00_60);
												BgL_test1283z00_463 = PAIRP(BgL_arg1827z00_239);
											}
											if (BgL_test1283z00_463)
												{	/* Write/expanded.scm 44 */
													obj_t BgL_arg1825z00_240;

													{	/* Write/expanded.scm 44 */
														obj_t BgL_arg1826z00_241;

														BgL_arg1826z00_241 =
															BGL_EXITD_PROTECT(BgL_exitd1048z00_60);
														BgL_arg1825z00_240 =
															CDR(((obj_t) BgL_arg1826z00_241));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1048z00_60,
														BgL_arg1825z00_240);
													BUNSPEC;
												}
											else
												{	/* Write/expanded.scm 44 */
													BFALSE;
												}
										}
										BGl_z62zc3z04anonymousza31188ze3ze5zzwrite_expandedz00
											(BgL_zc3z04anonymousza31188ze3z87_243);
										return BBOOL(BgL_tmp1050z00_62);
									}
								}
							}
						else
							{	/* Write/expanded.scm 42 */
								return
									BGl_errorz00zz__errorz00(BGl_string1223z00zzwrite_expandedz00,
									BGl_string1224z00zzwrite_expandedz00,
									BgL_outputzd2namezd2_57);
							}
					}
				}
			}
		}

	}



/* &write-expanded */
	obj_t BGl_z62writezd2expandedzb0zzwrite_expandedz00(obj_t BgL_envz00_244,
		obj_t BgL_unitsz00_245)
	{
		{	/* Write/expanded.scm 27 */
			return BGl_writezd2expandedzd2zzwrite_expandedz00(BgL_unitsz00_245);
		}

	}



/* &<@anonymous:1188> */
	obj_t BGl_z62zc3z04anonymousza31188ze3ze5zzwrite_expandedz00(obj_t
		BgL_envz00_246)
	{
		{	/* Write/expanded.scm 44 */
			{	/* Write/expanded.scm 115 */
				obj_t BgL_portz00_247;

				BgL_portz00_247 = PROCEDURE_REF(BgL_envz00_246, (int) (0L));
				{	/* Write/expanded.scm 115 */
					bool_t BgL_test1284z00_476;

					if (OUTPUT_PORTP(BgL_portz00_247))
						{	/* Write/expanded.scm 116 */
							bool_t BgL_test1286z00_479;

							{	/* Write/expanded.scm 116 */
								obj_t BgL_arg1196z00_260;

								{	/* Write/expanded.scm 116 */
									obj_t BgL_tmpz00_480;

									BgL_tmpz00_480 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1196z00_260 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_480);
								}
								BgL_test1286z00_479 = (BgL_portz00_247 == BgL_arg1196z00_260);
							}
							if (BgL_test1286z00_479)
								{	/* Write/expanded.scm 116 */
									BgL_test1284z00_476 = ((bool_t) 0);
								}
							else
								{	/* Write/expanded.scm 116 */
									BgL_test1284z00_476 = ((bool_t) 1);
								}
						}
					else
						{	/* Write/expanded.scm 115 */
							BgL_test1284z00_476 = ((bool_t) 0);
						}
					if (BgL_test1284z00_476)
						{	/* Write/expanded.scm 115 */
							return bgl_close_output_port(BgL_portz00_247);
						}
					else
						{	/* Write/expanded.scm 115 */
							return BFALSE;
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzwrite_expandedz00(void)
	{
		{	/* Write/expanded.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzwrite_expandedz00(void)
	{
		{	/* Write/expanded.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzwrite_expandedz00(void)
	{
		{	/* Write/expanded.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzwrite_expandedz00(void)
	{
		{	/* Write/expanded.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1225z00zzwrite_expandedz00));
			BGl_modulezd2initializa7ationz75zzwrite_schemez00(305499406L,
				BSTRING_TO_STRING(BGl_string1225z00zzwrite_expandedz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1225z00zzwrite_expandedz00));
			BGl_modulezd2initializa7ationz75zzast_unitz00(234044111L,
				BSTRING_TO_STRING(BGl_string1225z00zzwrite_expandedz00));
			return
				BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1225z00zzwrite_expandedz00));
		}

	}

#ifdef __cplusplus
}
#endif
