/*===========================================================================*/
/*   (Write/ast.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Write/ast.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_WRITE_AST_TYPE_DEFINITIONS
#define BGL_WRITE_AST_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;


#endif													// BGL_WRITE_AST_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	extern obj_t BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzwrite_astz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_genericzd2initzd2zzwrite_astz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzwrite_astz00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_za2ppzd2caseza2zd2zz__ppz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_IMPORT obj_t BGl_ppz00zz__ppz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzwrite_astz00(void);
	extern obj_t BGl_writezd2schemezd2commentz00zzwrite_schemez00(obj_t, obj_t);
	extern obj_t BGl_writezd2schemezd2filezd2headerzd2zzwrite_schemez00(obj_t,
		obj_t);
	extern obj_t BGl_argszd2listzd2ze3argsza2z41zztools_argsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	static obj_t BGl_makezd2sfunzd2sinfoz00zzwrite_astz00(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62writezd2astzb0zzwrite_astz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31329ze3ze5zzwrite_astz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_writezd2astzd2zzwrite_astz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31541ze3ze5zzwrite_astz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzwrite_astz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_pptypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzwrite_schemez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__ppz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_atomzd2ze3stringze70zd6zzwrite_astz00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_cnstzd2initzd2zzwrite_astz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzwrite_astz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzwrite_astz00(void);
	BGL_IMPORT obj_t
		BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzwrite_astz00(void);
	extern obj_t
		BGl_functionzd2typezd2ze3stringze3zztype_pptypez00(BgL_variablez00_bglt);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	BGL_IMPORT obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	static obj_t __cnst[8];


	   
		 
		DEFINE_STRING(BGl_string1717z00zzwrite_astz00,
		BgL_bgl_string1717za700za7za7w1740za7, ".ast", 4);
	      DEFINE_STRING(BGl_string1718z00zzwrite_astz00,
		BgL_bgl_string1718za700za7za7w1741za7, ".", 1);
	      DEFINE_STRING(BGl_string1719z00zzwrite_astz00,
		BgL_bgl_string1719za700za7za7w1742za7, "The AST (", 9);
	      DEFINE_STRING(BGl_string1720z00zzwrite_astz00,
		BgL_bgl_string1720za700za7za7w1743za7, ")", 1);
	      DEFINE_STRING(BGl_string1721z00zzwrite_astz00,
		BgL_bgl_string1721za700za7za7w1744za7, "write-ast", 9);
	      DEFINE_STRING(BGl_string1722z00zzwrite_astz00,
		BgL_bgl_string1722za700za7za7w1745za7, "Can't open output file", 22);
	      DEFINE_STRING(BGl_string1723z00zzwrite_astz00,
		BgL_bgl_string1723za700za7za7w1746za7, "  predicate-of: ", 16);
	      DEFINE_STRING(BGl_string1724z00zzwrite_astz00,
		BgL_bgl_string1724za700za7za7w1747za7, "", 0);
	      DEFINE_STRING(BGl_string1725z00zzwrite_astz00,
		BgL_bgl_string1725za700za7za7w1748za7, "  user?: #t", 11);
	      DEFINE_STRING(BGl_string1726z00zzwrite_astz00,
		BgL_bgl_string1726za700za7za7w1749za7, "  user?: #f", 11);
	      DEFINE_STRING(BGl_string1727z00zzwrite_astz00,
		BgL_bgl_string1727za700za7za7w1750za7, "]", 1);
	      DEFINE_STRING(BGl_string1728z00zzwrite_astz00,
		BgL_bgl_string1728za700za7za7w1751za7, " removable: ", 12);
	      DEFINE_STRING(BGl_string1729z00zzwrite_astz00,
		BgL_bgl_string1729za700za7za7w1752za7, "  loc: ", 7);
	      DEFINE_STRING(BGl_string1730z00zzwrite_astz00,
		BgL_bgl_string1730za700za7za7w1753za7, "  rm: ", 6);
	      DEFINE_STRING(BGl_string1731z00zzwrite_astz00,
		BgL_bgl_string1731za700za7za7w1754za7, "  occ: ", 7);
	      DEFINE_STRING(BGl_string1732z00zzwrite_astz00,
		BgL_bgl_string1732za700za7za7w1755za7, "  side-effect: ", 15);
	      DEFINE_STRING(BGl_string1733z00zzwrite_astz00,
		BgL_bgl_string1733za700za7za7w1756za7, "[", 1);
	      DEFINE_STRING(BGl_string1734z00zzwrite_astz00,
		BgL_bgl_string1734za700za7za7w1757za7, "#t", 2);
	      DEFINE_STRING(BGl_string1735z00zzwrite_astz00,
		BgL_bgl_string1735za700za7za7w1758za7, "#f", 2);
	      DEFINE_STRING(BGl_string1736z00zzwrite_astz00,
		BgL_bgl_string1736za700za7za7w1759za7, "#unspecified", 12);
	      DEFINE_STRING(BGl_string1737z00zzwrite_astz00,
		BgL_bgl_string1737za700za7za7w1760za7, "write_ast", 9);
	      DEFINE_STRING(BGl_string1738z00zzwrite_astz00,
		BgL_bgl_string1738za700za7za7w1761za7,
		"define define-method smfun define-inline sifun define-generic sgfun lower ",
		74);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_writezd2astzd2envz00zzwrite_astz00,
		BgL_bgl_za762writeza7d2astza7b1762za7, BGl_z62writezd2astzb0zzwrite_astz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzwrite_astz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzwrite_astz00(long
		BgL_checksumz00_1788, char *BgL_fromz00_1789)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzwrite_astz00))
				{
					BGl_requirezd2initializa7ationz75zzwrite_astz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzwrite_astz00();
					BGl_libraryzd2moduleszd2initz00zzwrite_astz00();
					BGl_cnstzd2initzd2zzwrite_astz00();
					BGl_importedzd2moduleszd2initz00zzwrite_astz00();
					return BGl_methodzd2initzd2zzwrite_astz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzwrite_astz00(void)
	{
		{	/* Write/ast.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__ppz00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"write_ast");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"write_ast");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "write_ast");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"write_ast");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzwrite_astz00(void)
	{
		{	/* Write/ast.scm 15 */
			{	/* Write/ast.scm 15 */
				obj_t BgL_cportz00_1775;

				{	/* Write/ast.scm 15 */
					obj_t BgL_stringz00_1782;

					BgL_stringz00_1782 = BGl_string1738z00zzwrite_astz00;
					{	/* Write/ast.scm 15 */
						obj_t BgL_startz00_1783;

						BgL_startz00_1783 = BINT(0L);
						{	/* Write/ast.scm 15 */
							obj_t BgL_endz00_1784;

							BgL_endz00_1784 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1782)));
							{	/* Write/ast.scm 15 */

								BgL_cportz00_1775 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1782, BgL_startz00_1783, BgL_endz00_1784);
				}}}}
				{
					long BgL_iz00_1776;

					BgL_iz00_1776 = 7L;
				BgL_loopz00_1777:
					if ((BgL_iz00_1776 == -1L))
						{	/* Write/ast.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Write/ast.scm 15 */
							{	/* Write/ast.scm 15 */
								obj_t BgL_arg1739z00_1778;

								{	/* Write/ast.scm 15 */

									{	/* Write/ast.scm 15 */
										obj_t BgL_locationz00_1780;

										BgL_locationz00_1780 = BBOOL(((bool_t) 0));
										{	/* Write/ast.scm 15 */

											BgL_arg1739z00_1778 =
												BGl_readz00zz__readerz00(BgL_cportz00_1775,
												BgL_locationz00_1780);
										}
									}
								}
								{	/* Write/ast.scm 15 */
									int BgL_tmpz00_1823;

									BgL_tmpz00_1823 = (int) (BgL_iz00_1776);
									CNST_TABLE_SET(BgL_tmpz00_1823, BgL_arg1739z00_1778);
							}}
							{	/* Write/ast.scm 15 */
								int BgL_auxz00_1781;

								BgL_auxz00_1781 = (int) ((BgL_iz00_1776 - 1L));
								{
									long BgL_iz00_1828;

									BgL_iz00_1828 = (long) (BgL_auxz00_1781);
									BgL_iz00_1776 = BgL_iz00_1828;
									goto BgL_loopz00_1777;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzwrite_astz00(void)
	{
		{	/* Write/ast.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* write-ast */
	BGL_EXPORTED_DEF obj_t BGl_writezd2astzd2zzwrite_astz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Write/ast.scm 29 */
			{	/* Write/ast.scm 31 */
				obj_t BgL_outputzd2namezd2_1354;

				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* Write/ast.scm 31 */
						BgL_outputzd2namezd2_1354 = BGl_za2destza2z00zzengine_paramz00;
					}
				else
					{	/* Write/ast.scm 33 */
						bool_t BgL_test1766z00_1833;

						if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
							{	/* Write/ast.scm 34 */
								obj_t BgL_tmpz00_1836;

								BgL_tmpz00_1836 =
									CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
								BgL_test1766z00_1833 = STRINGP(BgL_tmpz00_1836);
							}
						else
							{	/* Write/ast.scm 33 */
								BgL_test1766z00_1833 = ((bool_t) 0);
							}
						if (BgL_test1766z00_1833)
							{	/* Write/ast.scm 35 */
								obj_t BgL_arg1335z00_1418;
								obj_t BgL_arg1339z00_1419;

								BgL_arg1335z00_1418 =
									BGl_prefixz00zz__osz00(CAR
									(BGl_za2srczd2filesza2zd2zzengine_paramz00));
								{	/* Write/ast.scm 36 */
									obj_t BgL_symbolz00_1651;

									BgL_symbolz00_1651 = BGl_za2passza2z00zzengine_paramz00;
									{	/* Write/ast.scm 36 */
										obj_t BgL_arg1455z00_1652;

										BgL_arg1455z00_1652 = SYMBOL_TO_STRING(BgL_symbolz00_1651);
										BgL_arg1339z00_1419 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1652);
									}
								}
								{	/* Write/ast.scm 35 */
									obj_t BgL_list1340z00_1420;

									{	/* Write/ast.scm 35 */
										obj_t BgL_arg1342z00_1421;

										{	/* Write/ast.scm 35 */
											obj_t BgL_arg1343z00_1422;

											{	/* Write/ast.scm 35 */
												obj_t BgL_arg1346z00_1423;

												BgL_arg1346z00_1423 =
													MAKE_YOUNG_PAIR(BGl_string1717z00zzwrite_astz00,
													BNIL);
												BgL_arg1343z00_1422 =
													MAKE_YOUNG_PAIR(BgL_arg1339z00_1419,
													BgL_arg1346z00_1423);
											}
											BgL_arg1342z00_1421 =
												MAKE_YOUNG_PAIR(BGl_string1718z00zzwrite_astz00,
												BgL_arg1343z00_1422);
										}
										BgL_list1340z00_1420 =
											MAKE_YOUNG_PAIR(BgL_arg1335z00_1418, BgL_arg1342z00_1421);
									}
									BgL_outputzd2namezd2_1354 =
										BGl_stringzd2appendzd2zz__r4_strings_6_7z00
										(BgL_list1340z00_1420);
								}
							}
						else
							{	/* Write/ast.scm 33 */
								BgL_outputzd2namezd2_1354 = BFALSE;
							}
					}
				{	/* Write/ast.scm 31 */
					obj_t BgL_portz00_1355;

					if (STRINGP(BgL_outputzd2namezd2_1354))
						{	/* Write/ast.scm 40 */

							BgL_portz00_1355 =
								BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_outputzd2namezd2_1354, BTRUE);
						}
					else
						{	/* Write/ast.scm 41 */
							obj_t BgL_tmpz00_1851;

							BgL_tmpz00_1851 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_portz00_1355 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1851);
						}
					{	/* Write/ast.scm 39 */

						if (OUTPUT_PORTP(BgL_portz00_1355))
							{	/* Write/ast.scm 44 */
								obj_t BgL_exitd1106z00_1357;

								BgL_exitd1106z00_1357 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Write/ast.scm 75 */
									obj_t BgL_zc3z04anonymousza31329ze3z87_1767;

									BgL_zc3z04anonymousza31329ze3z87_1767 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31329ze3ze5zzwrite_astz00,
										(int) (0L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31329ze3z87_1767,
										(int) (0L), BgL_portz00_1355);
									{	/* Write/ast.scm 44 */
										obj_t BgL_arg1828z00_1655;

										{	/* Write/ast.scm 44 */
											obj_t BgL_arg1829z00_1656;

											BgL_arg1829z00_1656 =
												BGL_EXITD_PROTECT(BgL_exitd1106z00_1357);
											BgL_arg1828z00_1655 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31329ze3z87_1767,
												BgL_arg1829z00_1656);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1106z00_1357,
											BgL_arg1828z00_1655);
										BUNSPEC;
									}
									{	/* Write/ast.scm 47 */
										bool_t BgL_tmp1108z00_1359;

										if (CBOOL
											(BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00))
											{	/* Write/ast.scm 46 */
												BFALSE;
											}
										else
											{	/* Write/ast.scm 46 */
												BGl_za2ppzd2caseza2zd2zz__ppz00 = CNST_TABLE_REF(0);
											}
										BGl_writezd2schemezd2filezd2headerzd2zzwrite_schemez00
											(BgL_portz00_1355,
											string_append_3(BGl_string1719z00zzwrite_astz00,
												BGl_za2currentzd2passza2zd2zzengine_passz00,
												BGl_string1720z00zzwrite_astz00));
										{
											obj_t BgL_l1236z00_1362;

											BgL_l1236z00_1362 = BgL_globalsz00_3;
										BgL_zc3z04anonymousza31249ze3z87_1363:
											if (PAIRP(BgL_l1236z00_1362))
												{	/* Write/ast.scm 52 */
													{	/* Write/ast.scm 54 */
														obj_t BgL_gz00_1365;

														BgL_gz00_1365 = CAR(BgL_l1236z00_1362);
														{	/* Write/ast.scm 54 */
															BgL_valuez00_bglt BgL_funz00_1366;

															BgL_funz00_1366 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_globalz00_bglt) BgL_gz00_1365))))->
																BgL_valuez00);
															{	/* Write/ast.scm 55 */
																obj_t BgL_arg1252z00_1367;

																BgL_arg1252z00_1367 =
																	BGl_shapez00zztools_shapez00(BgL_gz00_1365);
																{	/* Write/ast.scm 55 */
																	obj_t BgL_list1253z00_1368;

																	BgL_list1253z00_1368 =
																		MAKE_YOUNG_PAIR(BgL_arg1252z00_1367, BNIL);
																	BGl_writezd2schemezd2commentz00zzwrite_schemez00
																		(BgL_portz00_1355, BgL_list1253z00_1368);
																}
															}
															{	/* Write/ast.scm 57 */
																obj_t BgL_arg1268z00_1369;

																BgL_arg1268z00_1369 =
																	BGl_functionzd2typezd2ze3stringze3zztype_pptypez00
																	(((BgL_variablez00_bglt) BgL_gz00_1365));
																{	/* Write/ast.scm 56 */
																	obj_t BgL_list1269z00_1370;

																	BgL_list1269z00_1370 =
																		MAKE_YOUNG_PAIR(BgL_arg1268z00_1369, BNIL);
																	BGl_writezd2schemezd2commentz00zzwrite_schemez00
																		(BgL_portz00_1355, BgL_list1269z00_1370);
																}
															}
															{	/* Write/ast.scm 58 */
																obj_t BgL_arg1272z00_1371;

																BgL_arg1272z00_1371 =
																	BGl_makezd2sfunzd2sinfoz00zzwrite_astz00(
																	((BgL_globalz00_bglt) BgL_gz00_1365));
																{	/* Write/ast.scm 58 */
																	obj_t BgL_list1273z00_1372;

																	BgL_list1273z00_1372 =
																		MAKE_YOUNG_PAIR(BgL_arg1272z00_1371, BNIL);
																	BGl_writezd2schemezd2commentz00zzwrite_schemez00
																		(BgL_portz00_1355, BgL_list1273z00_1372);
																}
															}
															{	/* Write/ast.scm 59 */
																obj_t BgL_arg1284z00_1373;

																{	/* Write/ast.scm 59 */
																	obj_t BgL_arg1304z00_1375;
																	obj_t BgL_arg1305z00_1376;

																	{	/* Write/ast.scm 59 */
																		obj_t BgL_casezd2valuezd2_1377;

																		BgL_casezd2valuezd2_1377 =
																			(((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						BgL_funz00_1366)))->BgL_classz00);
																		if ((BgL_casezd2valuezd2_1377 ==
																				CNST_TABLE_REF(1)))
																			{	/* Write/ast.scm 59 */
																				BgL_arg1304z00_1375 = CNST_TABLE_REF(2);
																			}
																		else
																			{	/* Write/ast.scm 59 */
																				if (
																					(BgL_casezd2valuezd2_1377 ==
																						CNST_TABLE_REF(3)))
																					{	/* Write/ast.scm 59 */
																						BgL_arg1304z00_1375 =
																							CNST_TABLE_REF(4);
																					}
																				else
																					{	/* Write/ast.scm 59 */
																						if (
																							(BgL_casezd2valuezd2_1377 ==
																								CNST_TABLE_REF(5)))
																							{	/* Write/ast.scm 59 */
																								BgL_arg1304z00_1375 =
																									CNST_TABLE_REF(6);
																							}
																						else
																							{	/* Write/ast.scm 59 */
																								BgL_arg1304z00_1375 =
																									CNST_TABLE_REF(7);
																							}
																					}
																			}
																	}
																	{	/* Write/ast.scm 68 */
																		obj_t BgL_arg1310z00_1383;
																		obj_t BgL_arg1311z00_1384;

																		{	/* Write/ast.scm 68 */
																			obj_t BgL_arg1312z00_1385;
																			obj_t BgL_arg1314z00_1386;

																			BgL_arg1312z00_1385 =
																				BGl_shapez00zztools_shapez00
																				(BgL_gz00_1365);
																			{	/* Write/ast.scm 70 */
																				obj_t BgL_arg1315z00_1387;
																				long BgL_arg1316z00_1388;

																				{	/* Write/ast.scm 70 */
																					obj_t BgL_l1230z00_1389;

																					BgL_l1230z00_1389 =
																						(((BgL_sfunz00_bglt) COBJECT(
																								((BgL_sfunz00_bglt)
																									BgL_funz00_1366)))->
																						BgL_argsz00);
																					if (NULLP(BgL_l1230z00_1389))
																						{	/* Write/ast.scm 70 */
																							BgL_arg1315z00_1387 = BNIL;
																						}
																					else
																						{	/* Write/ast.scm 70 */
																							obj_t BgL_head1232z00_1391;

																							{	/* Write/ast.scm 70 */
																								obj_t BgL_arg1323z00_1403;

																								{	/* Write/ast.scm 70 */
																									obj_t BgL_arg1325z00_1404;

																									BgL_arg1325z00_1404 =
																										CAR(
																										((obj_t)
																											BgL_l1230z00_1389));
																									BgL_arg1323z00_1403 =
																										BGl_shapez00zztools_shapez00
																										(BgL_arg1325z00_1404);
																								}
																								BgL_head1232z00_1391 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1323z00_1403, BNIL);
																							}
																							{	/* Write/ast.scm 70 */
																								obj_t BgL_g1235z00_1392;

																								BgL_g1235z00_1392 =
																									CDR(
																									((obj_t) BgL_l1230z00_1389));
																								{
																									obj_t BgL_l1230z00_1394;
																									obj_t BgL_tail1233z00_1395;

																									BgL_l1230z00_1394 =
																										BgL_g1235z00_1392;
																									BgL_tail1233z00_1395 =
																										BgL_head1232z00_1391;
																								BgL_zc3z04anonymousza31318ze3z87_1396:
																									if (NULLP
																										(BgL_l1230z00_1394))
																										{	/* Write/ast.scm 70 */
																											BgL_arg1315z00_1387 =
																												BgL_head1232z00_1391;
																										}
																									else
																										{	/* Write/ast.scm 70 */
																											obj_t
																												BgL_newtail1234z00_1398;
																											{	/* Write/ast.scm 70 */
																												obj_t
																													BgL_arg1321z00_1400;
																												{	/* Write/ast.scm 70 */
																													obj_t
																														BgL_arg1322z00_1401;
																													BgL_arg1322z00_1401 =
																														CAR(((obj_t)
																															BgL_l1230z00_1394));
																													BgL_arg1321z00_1400 =
																														BGl_shapez00zztools_shapez00
																														(BgL_arg1322z00_1401);
																												}
																												BgL_newtail1234z00_1398
																													=
																													MAKE_YOUNG_PAIR
																													(BgL_arg1321z00_1400,
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1233z00_1395,
																												BgL_newtail1234z00_1398);
																											{	/* Write/ast.scm 70 */
																												obj_t
																													BgL_arg1320z00_1399;
																												BgL_arg1320z00_1399 =
																													CDR(((obj_t)
																														BgL_l1230z00_1394));
																												{
																													obj_t
																														BgL_tail1233z00_1923;
																													obj_t
																														BgL_l1230z00_1922;
																													BgL_l1230z00_1922 =
																														BgL_arg1320z00_1399;
																													BgL_tail1233z00_1923 =
																														BgL_newtail1234z00_1398;
																													BgL_tail1233z00_1395 =
																														BgL_tail1233z00_1923;
																													BgL_l1230z00_1394 =
																														BgL_l1230z00_1922;
																													goto
																														BgL_zc3z04anonymousza31318ze3z87_1396;
																												}
																											}
																										}
																								}
																							}
																						}
																				}
																				BgL_arg1316z00_1388 =
																					(((BgL_funz00_bglt) COBJECT(
																							((BgL_funz00_bglt)
																								((BgL_sfunz00_bglt)
																									BgL_funz00_1366))))->
																					BgL_arityz00);
																				BgL_arg1314z00_1386 =
																					BGl_argszd2listzd2ze3argsza2z41zztools_argsz00
																					(BgL_arg1315z00_1387,
																					BINT(BgL_arg1316z00_1388));
																			}
																			BgL_arg1310z00_1383 =
																				MAKE_YOUNG_PAIR(BgL_arg1312z00_1385,
																				BgL_arg1314z00_1386);
																		}
																		{	/* Write/ast.scm 72 */
																			obj_t BgL_arg1326z00_1405;

																			{	/* Write/ast.scm 72 */
																				obj_t BgL_arg1327z00_1406;

																				BgL_arg1327z00_1406 =
																					(((BgL_sfunz00_bglt) COBJECT(
																							((BgL_sfunz00_bglt)
																								BgL_funz00_1366)))->
																					BgL_bodyz00);
																				BgL_arg1326z00_1405 =
																					BGl_shapez00zztools_shapez00
																					(BgL_arg1327z00_1406);
																			}
																			BgL_arg1311z00_1384 =
																				MAKE_YOUNG_PAIR(BgL_arg1326z00_1405,
																				BNIL);
																		}
																		BgL_arg1305z00_1376 =
																			MAKE_YOUNG_PAIR(BgL_arg1310z00_1383,
																			BgL_arg1311z00_1384);
																	}
																	BgL_arg1284z00_1373 =
																		MAKE_YOUNG_PAIR(BgL_arg1304z00_1375,
																		BgL_arg1305z00_1376);
																}
																{	/* Write/ast.scm 59 */
																	obj_t BgL_list1285z00_1374;

																	BgL_list1285z00_1374 =
																		MAKE_YOUNG_PAIR(BgL_portz00_1355, BNIL);
																	BGl_ppz00zz__ppz00(BgL_arg1284z00_1373,
																		BgL_list1285z00_1374);
																}
															}
														}
													}
													{
														obj_t BgL_l1236z00_1938;

														BgL_l1236z00_1938 = CDR(BgL_l1236z00_1362);
														BgL_l1236z00_1362 = BgL_l1236z00_1938;
														goto BgL_zc3z04anonymousza31249ze3z87_1363;
													}
												}
											else
												{	/* Write/ast.scm 52 */
													BgL_tmp1108z00_1359 = ((bool_t) 1);
												}
										}
										{	/* Write/ast.scm 44 */
											bool_t BgL_test1778z00_1940;

											{	/* Write/ast.scm 44 */
												obj_t BgL_arg1827z00_1673;

												BgL_arg1827z00_1673 =
													BGL_EXITD_PROTECT(BgL_exitd1106z00_1357);
												BgL_test1778z00_1940 = PAIRP(BgL_arg1827z00_1673);
											}
											if (BgL_test1778z00_1940)
												{	/* Write/ast.scm 44 */
													obj_t BgL_arg1825z00_1674;

													{	/* Write/ast.scm 44 */
														obj_t BgL_arg1826z00_1675;

														BgL_arg1826z00_1675 =
															BGL_EXITD_PROTECT(BgL_exitd1106z00_1357);
														BgL_arg1825z00_1674 =
															CDR(((obj_t) BgL_arg1826z00_1675));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1106z00_1357,
														BgL_arg1825z00_1674);
													BUNSPEC;
												}
											else
												{	/* Write/ast.scm 44 */
													BFALSE;
												}
										}
										bgl_close_output_port(BgL_portz00_1355);
										return BBOOL(BgL_tmp1108z00_1359);
									}
								}
							}
						else
							{	/* Write/ast.scm 42 */
								return
									BGl_errorz00zz__errorz00(BGl_string1721z00zzwrite_astz00,
									BGl_string1722z00zzwrite_astz00, BgL_outputzd2namezd2_1354);
							}
					}
				}
			}
		}

	}



/* &write-ast */
	obj_t BGl_z62writezd2astzb0zzwrite_astz00(obj_t BgL_envz00_1768,
		obj_t BgL_globalsz00_1769)
	{
		{	/* Write/ast.scm 29 */
			return BGl_writezd2astzd2zzwrite_astz00(BgL_globalsz00_1769);
		}

	}



/* &<@anonymous:1329> */
	obj_t BGl_z62zc3z04anonymousza31329ze3ze5zzwrite_astz00(obj_t BgL_envz00_1770)
	{
		{	/* Write/ast.scm 44 */
			{	/* Write/ast.scm 75 */
				obj_t BgL_portz00_1771;

				BgL_portz00_1771 = PROCEDURE_REF(BgL_envz00_1770, (int) (0L));
				return bgl_close_output_port(((obj_t) BgL_portz00_1771));
			}
		}

	}



/* make-sfun-sinfo */
	obj_t BGl_makezd2sfunzd2sinfoz00zzwrite_astz00(BgL_globalz00_bglt BgL_gz00_4)
	{
		{	/* Write/ast.scm 80 */
			{	/* Write/ast.scm 103 */
				BgL_valuez00_bglt BgL_sfunz00_1428;

				BgL_sfunz00_1428 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_gz00_4)))->BgL_valuez00);
				{	/* Write/ast.scm 105 */
					obj_t BgL_arg1351z00_1429;
					obj_t BgL_arg1352z00_1430;
					obj_t BgL_arg1361z00_1431;
					obj_t BgL_arg1364z00_1432;
					obj_t BgL_arg1367z00_1433;
					obj_t BgL_arg1370z00_1434;
					obj_t BgL_arg1371z00_1435;
					obj_t BgL_arg1375z00_1436;

					if (CBOOL(BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00))
						{	/* Write/ast.scm 106 */
							obj_t BgL_arg1473z00_1452;

							BgL_arg1473z00_1452 =
								(((BgL_globalz00_bglt) COBJECT(BgL_gz00_4))->BgL_importz00);
							{	/* Write/ast.scm 106 */
								obj_t BgL_arg1455z00_1686;

								BgL_arg1455z00_1686 =
									SYMBOL_TO_STRING(((obj_t) BgL_arg1473z00_1452));
								BgL_arg1351z00_1429 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_1686);
							}
						}
					else
						{	/* Write/ast.scm 107 */
							obj_t BgL_arg1485z00_1453;

							{	/* Write/ast.scm 107 */
								obj_t BgL_arg1489z00_1454;

								BgL_arg1489z00_1454 =
									(((BgL_globalz00_bglt) COBJECT(BgL_gz00_4))->BgL_importz00);
								{	/* Write/ast.scm 107 */
									obj_t BgL_arg1455z00_1689;

									BgL_arg1455z00_1689 =
										SYMBOL_TO_STRING(((obj_t) BgL_arg1489z00_1454));
									BgL_arg1485z00_1453 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_1689);
								}
							}
							BgL_arg1351z00_1429 =
								BGl_stringzd2downcasezd2zz__r4_strings_6_7z00
								(BgL_arg1485z00_1453);
						}
					{	/* Write/ast.scm 108 */
						obj_t BgL_arg1502z00_1455;

						BgL_arg1502z00_1455 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt)
										((BgL_sfunz00_bglt) BgL_sfunz00_1428))))->
							BgL_sidezd2effectzd2);
						BgL_arg1352z00_1430 =
							BGl_atomzd2ze3stringze70zd6zzwrite_astz00(BgL_arg1502z00_1455);
					}
					{	/* Write/ast.scm 109 */
						obj_t BgL_tz00_1456;

						BgL_tz00_1456 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt)
										((BgL_sfunz00_bglt) BgL_sfunz00_1428))))->
							BgL_predicatezd2ofzd2);
						{	/* Write/ast.scm 110 */
							bool_t BgL_test1780z00_1975;

							{	/* Write/ast.scm 110 */
								obj_t BgL_classz00_1692;

								BgL_classz00_1692 = BGl_typez00zztype_typez00;
								if (BGL_OBJECTP(BgL_tz00_1456))
									{	/* Write/ast.scm 110 */
										BgL_objectz00_bglt BgL_arg1807z00_1694;

										BgL_arg1807z00_1694 = (BgL_objectz00_bglt) (BgL_tz00_1456);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Write/ast.scm 110 */
												long BgL_idxz00_1700;

												BgL_idxz00_1700 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1694);
												BgL_test1780z00_1975 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_1700 + 1L)) == BgL_classz00_1692);
											}
										else
											{	/* Write/ast.scm 110 */
												bool_t BgL_res1715z00_1725;

												{	/* Write/ast.scm 110 */
													obj_t BgL_oclassz00_1708;

													{	/* Write/ast.scm 110 */
														obj_t BgL_arg1815z00_1716;
														long BgL_arg1816z00_1717;

														BgL_arg1815z00_1716 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Write/ast.scm 110 */
															long BgL_arg1817z00_1718;

															BgL_arg1817z00_1718 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1694);
															BgL_arg1816z00_1717 =
																(BgL_arg1817z00_1718 - OBJECT_TYPE);
														}
														BgL_oclassz00_1708 =
															VECTOR_REF(BgL_arg1815z00_1716,
															BgL_arg1816z00_1717);
													}
													{	/* Write/ast.scm 110 */
														bool_t BgL__ortest_1115z00_1709;

														BgL__ortest_1115z00_1709 =
															(BgL_classz00_1692 == BgL_oclassz00_1708);
														if (BgL__ortest_1115z00_1709)
															{	/* Write/ast.scm 110 */
																BgL_res1715z00_1725 = BgL__ortest_1115z00_1709;
															}
														else
															{	/* Write/ast.scm 110 */
																long BgL_odepthz00_1710;

																{	/* Write/ast.scm 110 */
																	obj_t BgL_arg1804z00_1711;

																	BgL_arg1804z00_1711 = (BgL_oclassz00_1708);
																	BgL_odepthz00_1710 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_1711);
																}
																if ((1L < BgL_odepthz00_1710))
																	{	/* Write/ast.scm 110 */
																		obj_t BgL_arg1802z00_1713;

																		{	/* Write/ast.scm 110 */
																			obj_t BgL_arg1803z00_1714;

																			BgL_arg1803z00_1714 =
																				(BgL_oclassz00_1708);
																			BgL_arg1802z00_1713 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_1714, 1L);
																		}
																		BgL_res1715z00_1725 =
																			(BgL_arg1802z00_1713 ==
																			BgL_classz00_1692);
																	}
																else
																	{	/* Write/ast.scm 110 */
																		BgL_res1715z00_1725 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1780z00_1975 = BgL_res1715z00_1725;
											}
									}
								else
									{	/* Write/ast.scm 110 */
										BgL_test1780z00_1975 = ((bool_t) 0);
									}
							}
							if (BgL_test1780z00_1975)
								{	/* Write/ast.scm 110 */
									BgL_arg1361z00_1431 =
										string_append(BGl_string1723z00zzwrite_astz00,
										BGl_atomzd2ze3stringze70zd6zzwrite_astz00
										(BGl_shapez00zztools_shapez00(BgL_tz00_1456)));
								}
							else
								{	/* Write/ast.scm 110 */
									BgL_arg1361z00_1431 = BGl_string1724z00zzwrite_astz00;
								}
						}
					}
					{	/* Write/ast.scm 114 */

						BgL_arg1364z00_1432 =
							BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_gz00_4)))->BgL_occurrencez00),
							10L);
					}
					{	/* Write/ast.scm 115 */
						obj_t BgL_arg1516z00_1463;

						BgL_arg1516z00_1463 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_gz00_4)))->BgL_removablez00);
						BgL_arg1367z00_1433 =
							BGl_atomzd2ze3stringze70zd6zzwrite_astz00(BgL_arg1516z00_1463);
					}
					{	/* Write/ast.scm 116 */
						obj_t BgL_pz00_1464;

						{	/* Write/ast.scm 116 */

							{	/* Write/ast.scm 116 */

								BgL_pz00_1464 =
									BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
							}
						}
						{	/* Write/ast.scm 117 */
							obj_t BgL_arg1535z00_1465;

							BgL_arg1535z00_1465 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_sfunz00_1428)))->BgL_locz00);
							bgl_display_obj(BgL_arg1535z00_1465, BgL_pz00_1464);
						}
						BgL_arg1370z00_1434 = bgl_close_output_port(BgL_pz00_1464);
					}
					if (
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_gz00_4)))->BgL_userzf3zf3))
						{	/* Write/ast.scm 119 */
							BgL_arg1371z00_1435 = BGl_string1725z00zzwrite_astz00;
						}
					else
						{	/* Write/ast.scm 119 */
							BgL_arg1371z00_1435 = BGl_string1726z00zzwrite_astz00;
						}
					{	/* Write/ast.scm 124 */
						obj_t BgL_zc3z04anonymousza31541ze3z87_1772;

						BgL_zc3z04anonymousza31541ze3z87_1772 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31541ze3ze5zzwrite_astz00, (int) (0L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31541ze3z87_1772, (int) (0L),
							((obj_t) BgL_gz00_4));
						BgL_arg1375z00_1436 =
							BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
							(BgL_zc3z04anonymousza31541ze3z87_1772);
					}
					{	/* Write/ast.scm 104 */
						obj_t BgL_list1376z00_1437;

						{	/* Write/ast.scm 104 */
							obj_t BgL_arg1377z00_1438;

							{	/* Write/ast.scm 104 */
								obj_t BgL_arg1378z00_1439;

								{	/* Write/ast.scm 104 */
									obj_t BgL_arg1379z00_1440;

									{	/* Write/ast.scm 104 */
										obj_t BgL_arg1380z00_1441;

										{	/* Write/ast.scm 104 */
											obj_t BgL_arg1408z00_1442;

											{	/* Write/ast.scm 104 */
												obj_t BgL_arg1410z00_1443;

												{	/* Write/ast.scm 104 */
													obj_t BgL_arg1421z00_1444;

													{	/* Write/ast.scm 104 */
														obj_t BgL_arg1422z00_1445;

														{	/* Write/ast.scm 104 */
															obj_t BgL_arg1434z00_1446;

															{	/* Write/ast.scm 104 */
																obj_t BgL_arg1437z00_1447;

																{	/* Write/ast.scm 104 */
																	obj_t BgL_arg1448z00_1448;

																	{	/* Write/ast.scm 104 */
																		obj_t BgL_arg1453z00_1449;

																		{	/* Write/ast.scm 104 */
																			obj_t BgL_arg1454z00_1450;

																			{	/* Write/ast.scm 104 */
																				obj_t BgL_arg1472z00_1451;

																				BgL_arg1472z00_1451 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1727z00zzwrite_astz00,
																					BNIL);
																				BgL_arg1454z00_1450 =
																					MAKE_YOUNG_PAIR(BgL_arg1375z00_1436,
																					BgL_arg1472z00_1451);
																			}
																			BgL_arg1453z00_1449 =
																				MAKE_YOUNG_PAIR
																				(BGl_string1728z00zzwrite_astz00,
																				BgL_arg1454z00_1450);
																		}
																		BgL_arg1448z00_1448 =
																			MAKE_YOUNG_PAIR(BgL_arg1371z00_1435,
																			BgL_arg1453z00_1449);
																	}
																	BgL_arg1437z00_1447 =
																		MAKE_YOUNG_PAIR(BgL_arg1370z00_1434,
																		BgL_arg1448z00_1448);
																}
																BgL_arg1434z00_1446 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1729z00zzwrite_astz00,
																	BgL_arg1437z00_1447);
															}
															BgL_arg1422z00_1445 =
																MAKE_YOUNG_PAIR(BgL_arg1367z00_1433,
																BgL_arg1434z00_1446);
														}
														BgL_arg1421z00_1444 =
															MAKE_YOUNG_PAIR(BGl_string1730z00zzwrite_astz00,
															BgL_arg1422z00_1445);
													}
													BgL_arg1410z00_1443 =
														MAKE_YOUNG_PAIR(BgL_arg1364z00_1432,
														BgL_arg1421z00_1444);
												}
												BgL_arg1408z00_1442 =
													MAKE_YOUNG_PAIR(BGl_string1731z00zzwrite_astz00,
													BgL_arg1410z00_1443);
											}
											BgL_arg1380z00_1441 =
												MAKE_YOUNG_PAIR(BgL_arg1361z00_1431,
												BgL_arg1408z00_1442);
										}
										BgL_arg1379z00_1440 =
											MAKE_YOUNG_PAIR(BgL_arg1352z00_1430, BgL_arg1380z00_1441);
									}
									BgL_arg1378z00_1439 =
										MAKE_YOUNG_PAIR(BGl_string1732z00zzwrite_astz00,
										BgL_arg1379z00_1440);
								}
								BgL_arg1377z00_1438 =
									MAKE_YOUNG_PAIR(BgL_arg1351z00_1429, BgL_arg1378z00_1439);
							}
							BgL_list1376z00_1437 =
								MAKE_YOUNG_PAIR(BGl_string1733z00zzwrite_astz00,
								BgL_arg1377z00_1438);
						}
						return
							BGl_stringzd2appendzd2zz__r4_strings_6_7z00(BgL_list1376z00_1437);
					}
				}
			}
		}

	}



/* atom->string~0 */
	obj_t BGl_atomzd2ze3stringze70zd6zzwrite_astz00(obj_t BgL_atomz00_1473)
	{
		{	/* Write/ast.scm 102 */
			{

				{	/* Write/ast.scm 82 */
					long BgL_aux1112z00_1476;

					if (CNSTP(BgL_atomz00_1473))
						{	/* Write/ast.scm 82 */
							BgL_aux1112z00_1476 = CCNST(BgL_atomz00_1473);
						}
					else
						{	/* Write/ast.scm 82 */
							BgL_aux1112z00_1476 = -1L;
						}
					switch (BgL_aux1112z00_1476)
						{
						case 4L:

							return BGl_string1734z00zzwrite_astz00;
							break;
						case 2L:

							return BGl_string1735z00zzwrite_astz00;
							break;
						case 3L:

							return BGl_string1736z00zzwrite_astz00;
							break;
						default:
							if (SYMBOLP(BgL_atomz00_1473))
								{	/* Write/ast.scm 91 */
									if (CBOOL
										(BGl_za2astzd2casezd2sensitiveza2z00zzengine_paramz00))
										{	/* Write/ast.scm 93 */
											obj_t BgL_arg1455z00_1679;

											BgL_arg1455z00_1679 = SYMBOL_TO_STRING(BgL_atomz00_1473);
											return
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_1679);
										}
									else
										{	/* Write/ast.scm 94 */
											obj_t BgL_arg1552z00_1480;

											{	/* Write/ast.scm 94 */
												obj_t BgL_arg1455z00_1681;

												BgL_arg1455z00_1681 =
													SYMBOL_TO_STRING(BgL_atomz00_1473);
												BgL_arg1552z00_1480 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1681);
											}
											return
												BGl_stringzd2downcasezd2zz__r4_strings_6_7z00
												(BgL_arg1552z00_1480);
										}
								}
							else
								{	/* Write/ast.scm 91 */
									if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_atomz00_1473))
										{	/* Write/ast.scm 96 */

											return
												BGl_numberzd2ze3stringz31zz__r4_numbers_6_5z00
												(BgL_atomz00_1473, BINT(10L));
										}
									else
										{	/* Write/ast.scm 95 */
											if (STRINGP(BgL_atomz00_1473))
												{	/* Write/ast.scm 97 */
													return BgL_atomz00_1473;
												}
											else
												{	/* Write/ast.scm 100 */
													obj_t BgL_pz00_1485;

													{	/* Write/ast.scm 100 */

														{	/* Write/ast.scm 100 */

															BgL_pz00_1485 =
																BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00
																(BTRUE);
														}
													}
													bgl_display_obj(BgL_atomz00_1473, BgL_pz00_1485);
													return bgl_close_output_port(BgL_pz00_1485);
												}
										}
								}
						}
				}
			}
		}

	}



/* &<@anonymous:1541> */
	obj_t BGl_z62zc3z04anonymousza31541ze3ze5zzwrite_astz00(obj_t BgL_envz00_1773)
	{
		{	/* Write/ast.scm 123 */
			{	/* Write/ast.scm 124 */
				BgL_globalz00_bglt BgL_gz00_1774;

				BgL_gz00_1774 =
					((BgL_globalz00_bglt) PROCEDURE_REF(BgL_envz00_1773, (int) (0L)));
				{	/* Write/ast.scm 124 */
					obj_t BgL_arg1544z00_1786;
					obj_t BgL_arg1546z00_1787;

					BgL_arg1544z00_1786 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_gz00_1774)))->BgL_removablez00);
					{	/* Write/ast.scm 124 */
						obj_t BgL_tmpz00_2065;

						BgL_tmpz00_2065 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg1546z00_1787 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_2065);
					}
					return bgl_display_obj(BgL_arg1544z00_1786, BgL_arg1546z00_1787);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzwrite_astz00(void)
	{
		{	/* Write/ast.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzwrite_astz00(void)
	{
		{	/* Write/ast.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzwrite_astz00(void)
	{
		{	/* Write/ast.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzwrite_astz00(void)
	{
		{	/* Write/ast.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zzinit_mainz00(288050968L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zzwrite_schemez00(305499406L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zztype_pptypez00(220178216L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
			return
				BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1737z00zzwrite_astz00));
		}

	}

#ifdef __cplusplus
}
#endif
