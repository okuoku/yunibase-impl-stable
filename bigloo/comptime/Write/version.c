/*===========================================================================*/
/*   (Write/version.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Write/version.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_WRITE_VERSION_TYPE_DEFINITIONS
#define BGL_WRITE_VERSION_TYPE_DEFINITIONS
#endif													// BGL_WRITE_VERSION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzwrite_versionz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_shortzd2versionzd2zzwrite_versionz00(void);
	extern obj_t BGl_za2bigloozd2emailza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_versionz00zzwrite_versionz00(void);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzwrite_versionz00(void);
	static obj_t BGl_objectzd2initzd2zzwrite_versionz00(void);
	static obj_t BGl_z62revisionz62zzwrite_versionz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzwrite_versionz00(void);
	extern obj_t BGl_za2bigloozd2urlza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_za2bigloozd2versionza2zd2zzengine_paramz00;
	static obj_t BGl_z62versionz62zzwrite_versionz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzwrite_versionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	extern obj_t BGl_za2bigloozd2nameza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_revisionz00zzwrite_versionz00(void);
	static obj_t BGl_cnstzd2initzd2zzwrite_versionz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzwrite_versionz00(void);
	static obj_t BGl_horsez00zzwrite_versionz00(obj_t);
	static obj_t BGl_z62shortzd2versionzb0zzwrite_versionz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzwrite_versionz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzwrite_versionz00(void);
	extern obj_t BGl_za2bigloozd2authorza2zd2zzengine_paramz00;
	extern obj_t BGl_za2bigloozd2dateza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	static obj_t BGl_displayzd2tozd2columnz00zzwrite_versionz00(obj_t, long,
		unsigned char);
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_shortzd2versionzd2envz00zzwrite_versionz00,
		BgL_bgl_za762shortza7d2versi1066z00,
		BGl_z62shortzd2versionzb0zzwrite_versionz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_revisionzd2envzd2zzwrite_versionz00,
		BgL_bgl_za762revisionza762za7za71067z00,
		BGl_z62revisionz62zzwrite_versionz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_versionzd2envzd2zzwrite_versionz00,
		BgL_bgl_za762versionza762za7za7w1068z00,
		BGl_z62versionz62zzwrite_versionz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1059z00zzwrite_versionz00,
		BgL_bgl_string1059za700za7za7w1069za7, "", 0);
	      DEFINE_STRING(BGl_string1060z00zzwrite_versionz00,
		BgL_bgl_string1060za700za7za7w1070za7, "email: ", 7);
	      DEFINE_STRING(BGl_string1061z00zzwrite_versionz00,
		BgL_bgl_string1061za700za7za7w1071za7, "url: ", 5);
	      DEFINE_STRING(BGl_string1062z00zzwrite_versionz00,
		BgL_bgl_string1062za700za7za7w1072za7, "`a practical Scheme compiler'", 29);
	      DEFINE_STRING(BGl_string1063z00zzwrite_versionz00,
		BgL_bgl_string1063za700za7za7w1073za7, "write_version", 13);
	      DEFINE_STRING(BGl_string1064z00zzwrite_versionz00,
		BgL_bgl_string1064za700za7za7w1074za7,
		"done (\"            ,--^, \" \"      _ ___/ /|/  \" \"  ,;'( )__, ) '   \" \" ;;  //   L__.    \" \" '   \\\\    /  '    \" \"      ^   ^       \") ",
		134);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzwrite_versionz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzwrite_versionz00(long
		BgL_checksumz00_116, char *BgL_fromz00_117)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzwrite_versionz00))
				{
					BGl_requirezd2initializa7ationz75zzwrite_versionz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzwrite_versionz00();
					BGl_libraryzd2moduleszd2initz00zzwrite_versionz00();
					BGl_cnstzd2initzd2zzwrite_versionz00();
					BGl_importedzd2moduleszd2initz00zzwrite_versionz00();
					return BGl_methodzd2initzd2zzwrite_versionz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"write_version");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "write_version");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"write_version");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"write_version");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"write_version");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"write_version");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"write_version");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 15 */
			{	/* Write/version.scm 15 */
				obj_t BgL_cportz00_105;

				{	/* Write/version.scm 15 */
					obj_t BgL_stringz00_112;

					BgL_stringz00_112 = BGl_string1064z00zzwrite_versionz00;
					{	/* Write/version.scm 15 */
						obj_t BgL_startz00_113;

						BgL_startz00_113 = BINT(0L);
						{	/* Write/version.scm 15 */
							obj_t BgL_endz00_114;

							BgL_endz00_114 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_112)));
							{	/* Write/version.scm 15 */

								BgL_cportz00_105 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_112, BgL_startz00_113, BgL_endz00_114);
				}}}}
				{
					long BgL_iz00_106;

					BgL_iz00_106 = 1L;
				BgL_loopz00_107:
					if ((BgL_iz00_106 == -1L))
						{	/* Write/version.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Write/version.scm 15 */
							{	/* Write/version.scm 15 */
								obj_t BgL_arg1065z00_108;

								{	/* Write/version.scm 15 */

									{	/* Write/version.scm 15 */
										obj_t BgL_locationz00_110;

										BgL_locationz00_110 = BBOOL(((bool_t) 0));
										{	/* Write/version.scm 15 */

											BgL_arg1065z00_108 =
												BGl_readz00zz__readerz00(BgL_cportz00_105,
												BgL_locationz00_110);
										}
									}
								}
								{	/* Write/version.scm 15 */
									int BgL_tmpz00_142;

									BgL_tmpz00_142 = (int) (BgL_iz00_106);
									CNST_TABLE_SET(BgL_tmpz00_142, BgL_arg1065z00_108);
							}}
							{	/* Write/version.scm 15 */
								int BgL_auxz00_111;

								BgL_auxz00_111 = (int) ((BgL_iz00_106 - 1L));
								{
									long BgL_iz00_147;

									BgL_iz00_147 = (long) (BgL_auxz00_111);
									BgL_iz00_106 = BgL_iz00_147;
									goto BgL_loopz00_107;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* revision */
	BGL_EXPORTED_DEF obj_t BGl_revisionz00zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 25 */
			{	/* Write/version.scm 26 */
				obj_t BgL_port1014z00_15;

				{	/* Write/version.scm 26 */
					obj_t BgL_tmpz00_150;

					BgL_tmpz00_150 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1014z00_15 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_150);
				}
				bgl_display_obj(BGl_za2bigloozd2versionza2zd2zzengine_paramz00,
					BgL_port1014z00_15);
				return bgl_display_char(((unsigned char) 10), BgL_port1014z00_15);
		}}

	}



/* &revision */
	obj_t BGl_z62revisionz62zzwrite_versionz00(obj_t BgL_envz00_102)
	{
		{	/* Write/version.scm 25 */
			return BGl_revisionz00zzwrite_versionz00();
		}

	}



/* short-version */
	BGL_EXPORTED_DEF obj_t BGl_shortzd2versionzd2zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 31 */
			{	/* Write/version.scm 32 */
				obj_t BgL_port1015z00_16;

				{	/* Write/version.scm 32 */
					obj_t BgL_tmpz00_156;

					BgL_tmpz00_156 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1015z00_16 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_156);
				}
				bgl_display_obj(BGl_za2bigloozd2nameza2zd2zzengine_paramz00,
					BgL_port1015z00_16);
				return bgl_display_char(((unsigned char) 10), BgL_port1015z00_16);
		}}

	}



/* &short-version */
	obj_t BGl_z62shortzd2versionzb0zzwrite_versionz00(obj_t BgL_envz00_103)
	{
		{	/* Write/version.scm 31 */
			return BGl_shortzd2versionzd2zzwrite_versionz00();
		}

	}



/* version */
	BGL_EXPORTED_DEF obj_t BGl_versionz00zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 37 */
			BGl_displayzd2tozd2columnz00zzwrite_versionz00
				(BGl_string1059z00zzwrite_versionz00, 79L, ((unsigned char) '-'));
			{	/* Write/version.scm 39 */
				obj_t BgL_arg1022z00_17;

				{	/* Write/version.scm 39 */
					obj_t BgL_tmpz00_163;

					BgL_tmpz00_163 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1022z00_17 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_163);
				}
				bgl_display_char(((unsigned char) 10), BgL_arg1022z00_17);
			}
			{	/* Write/version.scm 42 */
				obj_t BgL_arg1023z00_18;
				obj_t BgL_arg1024z00_19;
				obj_t BgL_arg1025z00_20;

				if (
					(STRING_REF(BGl_za2bigloozd2dateza2zd2zzengine_paramz00,
							0L) == ((unsigned char) ' ')))
					{	/* Write/version.scm 43 */
						long BgL_arg1037z00_29;

						BgL_arg1037z00_29 =
							STRING_LENGTH(BGl_za2bigloozd2dateza2zd2zzengine_paramz00);
						BgL_arg1023z00_18 =
							c_substring(BGl_za2bigloozd2dateza2zd2zzengine_paramz00, 1L,
							BgL_arg1037z00_29);
					}
				else
					{	/* Write/version.scm 42 */
						BgL_arg1023z00_18 = BGl_za2bigloozd2dateza2zd2zzengine_paramz00;
					}
				BgL_arg1024z00_19 =
					string_append(BGl_string1060z00zzwrite_versionz00,
					BGl_za2bigloozd2emailza2zd2zzengine_paramz00);
				BgL_arg1025z00_20 =
					string_append(BGl_string1061z00zzwrite_versionz00,
					BGl_za2bigloozd2urlza2zd2zzengine_paramz00);
				{	/* Write/version.scm 40 */
					obj_t BgL_list1026z00_21;

					{	/* Write/version.scm 40 */
						obj_t BgL_arg1027z00_22;

						{	/* Write/version.scm 40 */
							obj_t BgL_arg1028z00_23;

							{	/* Write/version.scm 40 */
								obj_t BgL_arg1029z00_24;

								{	/* Write/version.scm 40 */
									obj_t BgL_arg1030z00_25;

									{	/* Write/version.scm 40 */
										obj_t BgL_arg1033z00_26;

										BgL_arg1033z00_26 =
											MAKE_YOUNG_PAIR(BgL_arg1025z00_20, BNIL);
										BgL_arg1030z00_25 =
											MAKE_YOUNG_PAIR(BgL_arg1024z00_19, BgL_arg1033z00_26);
									}
									BgL_arg1029z00_24 =
										MAKE_YOUNG_PAIR
										(BGl_za2bigloozd2authorza2zd2zzengine_paramz00,
										BgL_arg1030z00_25);
								}
								BgL_arg1028z00_23 =
									MAKE_YOUNG_PAIR(BgL_arg1023z00_18, BgL_arg1029z00_24);
							}
							BgL_arg1027z00_22 =
								MAKE_YOUNG_PAIR(BGl_string1062z00zzwrite_versionz00,
								BgL_arg1028z00_23);
						}
						BgL_list1026z00_21 =
							MAKE_YOUNG_PAIR(BGl_za2bigloozd2nameza2zd2zzengine_paramz00,
							BgL_arg1027z00_22);
					}
					BGl_horsez00zzwrite_versionz00(BgL_list1026z00_21);
				}
			}
			BGl_displayzd2tozd2columnz00zzwrite_versionz00
				(BGl_string1059z00zzwrite_versionz00, 79L, ((unsigned char) '-'));
			{	/* Write/version.scm 49 */
				obj_t BgL_arg1039z00_31;

				{	/* Write/version.scm 49 */
					obj_t BgL_tmpz00_182;

					BgL_tmpz00_182 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1039z00_31 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_182);
				}
				bgl_display_char(((unsigned char) 10), BgL_arg1039z00_31);
			}
			{	/* Write/version.scm 50 */
				obj_t BgL_arg1040z00_32;

				{	/* Write/version.scm 50 */
					obj_t BgL_tmpz00_186;

					BgL_tmpz00_186 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1040z00_32 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_186);
				}
				return bgl_display_char(((unsigned char) 10), BgL_arg1040z00_32);
		}}

	}



/* &version */
	obj_t BGl_z62versionz62zzwrite_versionz00(obj_t BgL_envz00_104)
	{
		{	/* Write/version.scm 37 */
			return BGl_versionz00zzwrite_versionz00();
		}

	}



/* horse */
	obj_t BGl_horsez00zzwrite_versionz00(obj_t BgL_lz00_3)
	{
		{	/* Write/version.scm 55 */
			{	/* Write/version.scm 56 */
				obj_t BgL_g1012z00_33;

				BgL_g1012z00_33 = CNST_TABLE_REF(0);
				{
					obj_t BgL_lz00_35;
					obj_t BgL_horsez00_36;

					BgL_lz00_35 = BgL_lz00_3;
					BgL_horsez00_36 = BgL_g1012z00_33;
				BgL_zc3z04anonymousza31041ze3z87_37:
					if (NULLP(BgL_lz00_35))
						{	/* Write/version.scm 64 */
							if (NULLP(BgL_horsez00_36))
								{	/* Write/version.scm 65 */
									return CNST_TABLE_REF(1);
								}
							else
								{	/* Write/version.scm 65 */
									BGl_displayzd2tozd2columnz00zzwrite_versionz00
										(BGl_string1059z00zzwrite_versionz00, 62L,
										((unsigned char) ' '));
									{	/* Write/version.scm 69 */
										obj_t BgL_port1016z00_40;

										{	/* Write/version.scm 69 */
											obj_t BgL_tmpz00_198;

											BgL_tmpz00_198 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_port1016z00_40 =
												BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_198);
										}
										{	/* Write/version.scm 69 */
											obj_t BgL_arg1044z00_41;

											BgL_arg1044z00_41 = CAR(((obj_t) BgL_horsez00_36));
											bgl_display_obj(BgL_arg1044z00_41, BgL_port1016z00_40);
										}
										bgl_display_char(((unsigned char) 10), BgL_port1016z00_40);
									}
									{	/* Write/version.scm 70 */
										obj_t BgL_arg1045z00_42;

										BgL_arg1045z00_42 = CDR(((obj_t) BgL_horsez00_36));
										{
											obj_t BgL_horsez00_208;
											obj_t BgL_lz00_207;

											BgL_lz00_207 = BNIL;
											BgL_horsez00_208 = BgL_arg1045z00_42;
											BgL_horsez00_36 = BgL_horsez00_208;
											BgL_lz00_35 = BgL_lz00_207;
											goto BgL_zc3z04anonymousza31041ze3z87_37;
										}
									}
								}
						}
					else
						{	/* Write/version.scm 64 */
							if (NULLP(BgL_horsez00_36))
								{	/* Write/version.scm 71 */
									{	/* Write/version.scm 72 */
										obj_t BgL_port1017z00_44;

										{	/* Write/version.scm 72 */
											obj_t BgL_tmpz00_211;

											BgL_tmpz00_211 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_port1017z00_44 =
												BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_211);
										}
										{	/* Write/version.scm 72 */
											obj_t BgL_arg1047z00_45;

											BgL_arg1047z00_45 = CAR(((obj_t) BgL_lz00_35));
											bgl_display_obj(BgL_arg1047z00_45, BgL_port1017z00_44);
										}
										bgl_display_char(((unsigned char) 10), BgL_port1017z00_44);
									}
									{	/* Write/version.scm 73 */
										obj_t BgL_arg1048z00_46;

										BgL_arg1048z00_46 = CDR(((obj_t) BgL_lz00_35));
										{
											obj_t BgL_horsez00_221;
											obj_t BgL_lz00_220;

											BgL_lz00_220 = BgL_arg1048z00_46;
											BgL_horsez00_221 = BNIL;
											BgL_horsez00_36 = BgL_horsez00_221;
											BgL_lz00_35 = BgL_lz00_220;
											goto BgL_zc3z04anonymousza31041ze3z87_37;
										}
									}
								}
							else
								{	/* Write/version.scm 71 */
									{	/* Write/version.scm 75 */
										obj_t BgL_arg1049z00_47;

										BgL_arg1049z00_47 = CAR(((obj_t) BgL_lz00_35));
										BGl_displayzd2tozd2columnz00zzwrite_versionz00
											(BgL_arg1049z00_47, 62L, ((unsigned char) ' '));
									}
									{	/* Write/version.scm 76 */
										obj_t BgL_port1018z00_48;

										{	/* Write/version.scm 76 */
											obj_t BgL_tmpz00_225;

											BgL_tmpz00_225 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_port1018z00_48 =
												BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_225);
										}
										{	/* Write/version.scm 76 */
											obj_t BgL_arg1050z00_49;

											BgL_arg1050z00_49 = CAR(((obj_t) BgL_horsez00_36));
											bgl_display_obj(BgL_arg1050z00_49, BgL_port1018z00_48);
										}
										bgl_display_char(((unsigned char) 10), BgL_port1018z00_48);
									}
									{	/* Write/version.scm 77 */
										obj_t BgL_arg1051z00_50;
										obj_t BgL_arg1052z00_51;

										BgL_arg1051z00_50 = CDR(((obj_t) BgL_lz00_35));
										BgL_arg1052z00_51 = CDR(((obj_t) BgL_horsez00_36));
										{
											obj_t BgL_horsez00_237;
											obj_t BgL_lz00_236;

											BgL_lz00_236 = BgL_arg1051z00_50;
											BgL_horsez00_237 = BgL_arg1052z00_51;
											BgL_horsez00_36 = BgL_horsez00_237;
											BgL_lz00_35 = BgL_lz00_236;
											goto BgL_zc3z04anonymousza31041ze3z87_37;
										}
									}
								}
						}
				}
			}
		}

	}



/* display-to-column */
	obj_t BGl_displayzd2tozd2columnz00zzwrite_versionz00(obj_t BgL_stringz00_4,
		long BgL_columnz00_5, unsigned char BgL_charz00_6)
	{
		{	/* Write/version.scm 82 */
			{	/* Write/version.scm 83 */
				obj_t BgL_arg1053z00_53;

				{	/* Write/version.scm 83 */
					obj_t BgL_tmpz00_238;

					BgL_tmpz00_238 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_arg1053z00_53 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_238);
				}
				bgl_display_obj(BgL_stringz00_4, BgL_arg1053z00_53);
			}
			{	/* Write/version.scm 84 */
				long BgL_g1013z00_54;

				BgL_g1013z00_54 = (1L + STRING_LENGTH(((obj_t) BgL_stringz00_4)));
				{
					long BgL_lz00_56;

					BgL_lz00_56 = BgL_g1013z00_54;
				BgL_zc3z04anonymousza31054ze3z87_57:
					if ((BgL_lz00_56 == BgL_columnz00_5))
						{	/* Write/version.scm 85 */
							return CNST_TABLE_REF(1);
						}
					else
						{	/* Write/version.scm 85 */
							{	/* Write/version.scm 88 */
								obj_t BgL_arg1056z00_59;

								{	/* Write/version.scm 88 */
									obj_t BgL_tmpz00_248;

									BgL_tmpz00_248 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_arg1056z00_59 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_248);
								}
								bgl_display_char(BgL_charz00_6, BgL_arg1056z00_59);
							}
							{
								long BgL_lz00_252;

								BgL_lz00_252 = (BgL_lz00_56 + 1L);
								BgL_lz00_56 = BgL_lz00_252;
								goto BgL_zc3z04anonymousza31054ze3z87_57;
							}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzwrite_versionz00(void)
	{
		{	/* Write/version.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1063z00zzwrite_versionz00));
			return
				BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1063z00zzwrite_versionz00));
		}

	}

#ifdef __cplusplus
}
#endif
