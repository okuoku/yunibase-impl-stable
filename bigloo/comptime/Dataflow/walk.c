/*===========================================================================*/
/*   (Dataflow/walk.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Dataflow/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_DATAFLOW_WALK_TYPE_DEFINITIONS
#define BGL_DATAFLOW_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_localzf2valuezf2_bgl
	{
		int BgL_stampz00;
		struct BgL_nodez00_bgl *BgL_nodez00;
	}                       *BgL_localzf2valuezf2_bglt;


#endif													// BGL_DATAFLOW_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62dataflowzd2testzd2envzd2va1430zb0zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62abortzf3zd2sync1448z43zzdataflow_walkz00(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzdataflow_walkz00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62dataflowzd2testzd2envz62zzdataflow_walkz00(obj_t, obj_t);
	static obj_t
		BGl_z62dataflowzd2nodez12zd2appzd2l1388za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzdataflow_walkz00(void);
	static obj_t BGl_z62dataflowzd2walkz12za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzdataflow_walkz00(void);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzdataflow_walkz00(void);
	static obj_t BGl_z62zc3z04anonymousza31868ze3ze5zzdataflow_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	static obj_t BGl_za2z42epairzf3za2zb1zzdataflow_walkz00 = BUNSPEC;
	static obj_t BGl_z62dataflowzd2nodez12zd2exter1392z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62dataflowzd2nodez12zd2funca1390z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62dataflowzd2nodez12zd2setq1394z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62dataflowzd2nodez12zd2boxzd2r1416za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_dataflowzd2testzd2falsezd2envzd2zzdataflow_walkz00(BgL_nodez00_bglt);
	static obj_t
		BGl_z62dataflowzd2nodez12zd2letzd2f1403za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62dataflowzd2nodez12zd2sync1384z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62dataflowzd2nodez12zd2fail1399z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62dataflowzd2nodez12zd2letzd2v1406za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dataflowzd2walkz12zc0zzdataflow_walkz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62dataflowzd2nodez12zd2var1378z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_abortzf3zf3zzdataflow_walkz00(BgL_nodez00_bglt);
	static obj_t BGl_z62dataflowzd2nodez12zd2condi1396z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzdataflow_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzdataflow_walkz00(void);
	static obj_t
		BGl_z62dataflowzd2nodez12zd2jumpzd21412za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t
		BGl_z62dataflowzd2nodez12zd2boxzd2s1418za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_removezd2variablezd2fromzd2envze70z35zzdataflow_walkz00
		(BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza31610ze3ze5zzdataflow_walkz00(obj_t,
		obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	static obj_t BGl_z62abortzf3zd2letzd2fun1452z91zzdataflow_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62dataflowzd2nodez121375za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	static obj_t BGl_dataflowzd2nodeza2z12z62zzdataflow_walkz00(obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static obj_t BGl_z62dataflowzd2nodez12zd2app1386z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	static long BGl_letzd2varzd2stampz00zzdataflow_walkz00 = 0L;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	static obj_t BGl_z62dataflowzd2testzd2envzd2ap1423zb0zzdataflow_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62abortzf31441z91zzdataflow_walkz00(obj_t, obj_t);
	static obj_t BGl_z62dataflowzd2testzd2envzd2in1426zb0zzdataflow_walkz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzdataflow_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_feffectz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_spreadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_cgraphz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_za2z42pairzf3za2zb1zzdataflow_walkz00 = BUNSPEC;
	static obj_t
		BGl_z62dataflowzd2nodez12zd2makezd21414za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62dataflowzd2nodez12za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62abortzf3z91zzdataflow_walkz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzdataflow_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzdataflow_walkz00(void);
	static obj_t BGl_z62dataflowzd2nodez12zd2switc1401z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62dataflowzd2testzd2env1419z62zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62dataflowzd2nodez12zd2closu1380z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzdataflow_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzdataflow_walkz00(void);
	static obj_t BGl_za2z42nullzf3za2zb1zzdataflow_walkz00 = BUNSPEC;
	static BgL_localz00_bglt BGl_z62lambda1606z62zzdataflow_walkz00(obj_t, obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static obj_t BGl_dataflowzd2testzd2envz00zzdataflow_walkz00(BgL_nodez00_bglt);
	extern obj_t BGl_za2bnilza2z00zztype_cachez00;
	static obj_t BGl_z62abortzf3zd2app1456z43zzdataflow_walkz00(obj_t, obj_t);
	static obj_t BGl_z62dataflowzd2testzd2envzd2co1428zb0zzdataflow_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	static BgL_localz00_bglt BGl_z62lambda1611z62zzdataflow_walkz00(obj_t, obj_t);
	static obj_t BGl_z62dataflowzd2testzd2falsezd2envzb0zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62dataflowzd2testzd2falsezd21433zb0zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1628z62zzdataflow_walkz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1629z62zzdataflow_walkz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_valuez00zzast_varz00;
	static obj_t BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(BgL_nodez00_bglt,
		obj_t);
	static obj_t BGl_z62abortzf3zd2letzd2var1450z91zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62abortzf3zd2sequence1446z43zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62dataflowzd2testzd2falsezd21440zb0zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_localzf2valuezf2zzdataflow_walkz00 = BUNSPEC;
	static obj_t BGl_z62abortzf3zd2funcall1458z43zzdataflow_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62dataflowzd2nodez12zd2seque1382z70zzdataflow_walkz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62lambda1647z62zzdataflow_walkz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1648z62zzdataflow_walkz00(obj_t, obj_t, obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_isazd2ofzd2zztype_miscz00(BgL_nodez00_bglt);
	static obj_t
		BGl_z62dataflowzd2nodez12zd2setzd2e1408za2zzdataflow_walkz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62abortzf3zd2conditional1454z43zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62dataflowzd2testzd2envzd2le1432zb0zzdataflow_walkz00(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1596z62zzdataflow_walkz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62abortzf3zd2appzd2ly1460z91zzdataflow_walkz00(obj_t,
		obj_t);
	static obj_t BGl_z62abortzf3zd2fail1444z43zzdataflow_walkz00(obj_t, obj_t);
	static obj_t __cnst[12];


	   
		 
		DEFINE_STRING(BGl_string2303z00zzdataflow_walkz00,
		BgL_bgl_string2303za700za7za7d2310za7, "dataflow_walk", 13);
	      DEFINE_STRING(BGl_string2304z00zzdataflow_walkz00,
		BgL_bgl_string2304za700za7za7d2311za7,
		"read _ dataflow_walk local/value node int stamp $epair? $pair? foreign $null? pass-started ",
		91);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2300z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2ap2312za7,
		BGl_z62abortzf3zd2app1456z43zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2301z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2fu2313za7,
		BGl_z62abortzf3zd2funcall1458z43zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2302z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2ap2314za7,
		BGl_z62abortzf3zd2appzd2ly1460z91zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2242z00zzdataflow_walkz00,
		BgL_bgl_string2242za700za7za7d2315za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2243z00zzdataflow_walkz00,
		BgL_bgl_string2243za700za7za7d2316za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2244z00zzdataflow_walkz00,
		BgL_bgl_string2244za700za7za7d2317za7, " error", 6);
	      DEFINE_STRING(BGl_string2245z00zzdataflow_walkz00,
		BgL_bgl_string2245za700za7za7d2318za7, "s", 1);
	      DEFINE_STRING(BGl_string2246z00zzdataflow_walkz00,
		BgL_bgl_string2246za700za7za7d2319za7, "", 0);
	      DEFINE_STRING(BGl_string2247z00zzdataflow_walkz00,
		BgL_bgl_string2247za700za7za7d2320za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2248z00zzdataflow_walkz00,
		BgL_bgl_string2248za700za7za7d2321za7, "failure during postlude hook", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2249z00zzdataflow_walkz00,
		BgL_bgl_za762lambda1629za7622322z00, BGl_z62lambda1629z62zzdataflow_walkz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dataflowzd2walkz12zd2envz12zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2wa2323z00,
		BGl_z62dataflowzd2walkz12za2zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2258z00zzdataflow_walkz00,
		BgL_bgl_string2258za700za7za7d2324za7, "dataflow-node!1375", 18);
	      DEFINE_STATIC_BGL_GENERIC(BGl_abortzf3zd2envz21zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za791za7za72325za7,
		BGl_z62abortzf3z91zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2250z00zzdataflow_walkz00,
		BgL_bgl_za762lambda1628za7622326z00, BGl_z62lambda1628z62zzdataflow_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2251z00zzdataflow_walkz00,
		BgL_bgl_za762lambda1648za7622327z00, BGl_z62lambda1648z62zzdataflow_walkz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2252z00zzdataflow_walkz00,
		BgL_bgl_za762lambda1647za7622328z00, BGl_z62lambda1647z62zzdataflow_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2253z00zzdataflow_walkz00,
		BgL_bgl_za762lambda1611za7622329z00, BGl_z62lambda1611z62zzdataflow_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2260z00zzdataflow_walkz00,
		BgL_bgl_string2260za700za7za7d2330za7, "dataflow-test-env1419", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2254z00zzdataflow_walkz00,
		BgL_bgl_za762za7c3za704anonymo2331za7,
		BGl_z62zc3z04anonymousza31610ze3ze5zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2255z00zzdataflow_walkz00,
		BgL_bgl_za762lambda1606za7622332z00, BGl_z62lambda1606z62zzdataflow_walkz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2262z00zzdataflow_walkz00,
		BgL_bgl_string2262za700za7za7d2333za7, "dataflow-test-false-1433", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2256z00zzdataflow_walkz00,
		BgL_bgl_za762lambda1596za7622334z00, BGl_z62lambda1596z62zzdataflow_walkz00,
		0L, BUNSPEC, 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2257z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2335z00,
		BGl_z62dataflowzd2nodez121375za2zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2264z00zzdataflow_walkz00,
		BgL_bgl_string2264za700za7za7d2336za7, "abort?1441", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2259z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2337z00,
		BGl_z62dataflowzd2testzd2env1419z62zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2266z00zzdataflow_walkz00,
		BgL_bgl_string2266za700za7za7d2338za7, "dataflow-node!", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2261z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2339z00,
		BGl_z62dataflowzd2testzd2falsezd21433zb0zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2263z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f31441za72340za7,
		BGl_z62abortzf31441z91zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2265z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2341z00,
		BGl_z62dataflowzd2nodez12zd2var1378z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2267z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2342z00,
		BGl_z62dataflowzd2nodez12zd2closu1380z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2268z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2343z00,
		BGl_z62dataflowzd2nodez12zd2seque1382z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2269z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2344z00,
		BGl_z62dataflowzd2nodez12zd2sync1384z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2270z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2345z00,
		BGl_z62dataflowzd2nodez12zd2app1386z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2271z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2346z00,
		BGl_z62dataflowzd2nodez12zd2appzd2l1388za2zzdataflow_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2272z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2347z00,
		BGl_z62dataflowzd2nodez12zd2funca1390z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2273z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2348z00,
		BGl_z62dataflowzd2nodez12zd2exter1392z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2274z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2349z00,
		BGl_z62dataflowzd2nodez12zd2setq1394z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2275z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2350z00,
		BGl_z62dataflowzd2nodez12zd2condi1396z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2276z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2351z00,
		BGl_z62dataflowzd2nodez12zd2fail1399z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2277z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2352z00,
		BGl_z62dataflowzd2nodez12zd2switc1401z70zzdataflow_walkz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2278z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2353z00,
		BGl_z62dataflowzd2nodez12zd2letzd2f1403za2zzdataflow_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2279z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2354z00,
		BGl_z62dataflowzd2nodez12zd2letzd2v1406za2zzdataflow_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string2286z00zzdataflow_walkz00,
		BgL_bgl_string2286za700za7za7d2355za7, "dataflow-test-env", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2280z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2356z00,
		BGl_z62dataflowzd2nodez12zd2setzd2e1408za2zzdataflow_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2281z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2357z00,
		BGl_z62dataflowzd2nodez12zd2jumpzd21412za2zzdataflow_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2282z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2358z00,
		BGl_z62dataflowzd2nodez12zd2makezd21414za2zzdataflow_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2283z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2359z00,
		BGl_z62dataflowzd2nodez12zd2boxzd2r1416za2zzdataflow_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2284z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2360z00,
		BGl_z62dataflowzd2nodez12zd2boxzd2s1418za2zzdataflow_walkz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2285z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2361z00,
		BGl_z62dataflowzd2testzd2envzd2ap1423zb0zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2292z00zzdataflow_walkz00,
		BgL_bgl_string2292za700za7za7d2362za7, "dataflow-test-false-env", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2287z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2363z00,
		BGl_z62dataflowzd2testzd2envzd2in1426zb0zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2294z00zzdataflow_walkz00,
		BgL_bgl_string2294za700za7za7d2364za7, "abort?", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2288z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2365z00,
		BGl_z62dataflowzd2testzd2envzd2co1428zb0zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2289z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2366z00,
		BGl_z62dataflowzd2testzd2envzd2va1430zb0zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2290z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2367z00,
		BGl_z62dataflowzd2testzd2envzd2le1432zb0zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2291z00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2368z00,
		BGl_z62dataflowzd2testzd2falsezd21440zb0zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2293z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2fa2369za7,
		BGl_z62abortzf3zd2fail1444z43zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2295z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2se2370za7,
		BGl_z62abortzf3zd2sequence1446z43zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2296z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2sy2371za7,
		BGl_z62abortzf3zd2sync1448z43zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2297z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2le2372za7,
		BGl_z62abortzf3zd2letzd2var1450z91zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2298z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2le2373za7,
		BGl_z62abortzf3zd2letzd2fun1452z91zzdataflow_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2299z00zzdataflow_walkz00,
		BgL_bgl_za762abortza7f3za7d2co2374za7,
		BGl_z62abortzf3zd2conditional1454z43zzdataflow_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_dataflowzd2testzd2envzd2envzd2zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2375z00,
		BGl_z62dataflowzd2testzd2envz62zzdataflow_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2no2376z00,
		BGl_z62dataflowzd2nodez12za2zzdataflow_walkz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_dataflowzd2testzd2falsezd2envzd2envz00zzdataflow_walkz00,
		BgL_bgl_za762dataflowza7d2te2377z00,
		BGl_z62dataflowzd2testzd2falsezd2envzb0zzdataflow_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzdataflow_walkz00));
		     ADD_ROOT((void *) (&BGl_za2z42epairzf3za2zb1zzdataflow_walkz00));
		     ADD_ROOT((void *) (&BGl_za2z42pairzf3za2zb1zzdataflow_walkz00));
		     ADD_ROOT((void *) (&BGl_za2z42nullzf3za2zb1zzdataflow_walkz00));
		     ADD_ROOT((void *) (&BGl_localzf2valuezf2zzdataflow_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzdataflow_walkz00(long
		BgL_checksumz00_4604, char *BgL_fromz00_4605)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzdataflow_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzdataflow_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzdataflow_walkz00();
					BGl_libraryzd2moduleszd2initz00zzdataflow_walkz00();
					BGl_cnstzd2initzd2zzdataflow_walkz00();
					BGl_importedzd2moduleszd2initz00zzdataflow_walkz00();
					BGl_objectzd2initzd2zzdataflow_walkz00();
					BGl_genericzd2initzd2zzdataflow_walkz00();
					BGl_methodzd2initzd2zzdataflow_walkz00();
					return BGl_toplevelzd2initzd2zzdataflow_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzdataflow_walkz00(void)
	{
		{	/* Dataflow/walk.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"dataflow_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"dataflow_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzdataflow_walkz00(void)
	{
		{	/* Dataflow/walk.scm 17 */
			{	/* Dataflow/walk.scm 17 */
				obj_t BgL_cportz00_4113;

				{	/* Dataflow/walk.scm 17 */
					obj_t BgL_stringz00_4120;

					BgL_stringz00_4120 = BGl_string2304z00zzdataflow_walkz00;
					{	/* Dataflow/walk.scm 17 */
						obj_t BgL_startz00_4121;

						BgL_startz00_4121 = BINT(0L);
						{	/* Dataflow/walk.scm 17 */
							obj_t BgL_endz00_4122;

							BgL_endz00_4122 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4120)));
							{	/* Dataflow/walk.scm 17 */

								BgL_cportz00_4113 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4120, BgL_startz00_4121, BgL_endz00_4122);
				}}}}
				{
					long BgL_iz00_4114;

					BgL_iz00_4114 = 11L;
				BgL_loopz00_4115:
					if ((BgL_iz00_4114 == -1L))
						{	/* Dataflow/walk.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Dataflow/walk.scm 17 */
							{	/* Dataflow/walk.scm 17 */
								obj_t BgL_arg2305z00_4116;

								{	/* Dataflow/walk.scm 17 */

									{	/* Dataflow/walk.scm 17 */
										obj_t BgL_locationz00_4118;

										BgL_locationz00_4118 = BBOOL(((bool_t) 0));
										{	/* Dataflow/walk.scm 17 */

											BgL_arg2305z00_4116 =
												BGl_readz00zz__readerz00(BgL_cportz00_4113,
												BgL_locationz00_4118);
										}
									}
								}
								{	/* Dataflow/walk.scm 17 */
									int BgL_tmpz00_4639;

									BgL_tmpz00_4639 = (int) (BgL_iz00_4114);
									CNST_TABLE_SET(BgL_tmpz00_4639, BgL_arg2305z00_4116);
							}}
							{	/* Dataflow/walk.scm 17 */
								int BgL_auxz00_4119;

								BgL_auxz00_4119 = (int) ((BgL_iz00_4114 - 1L));
								{
									long BgL_iz00_4644;

									BgL_iz00_4644 = (long) (BgL_auxz00_4119);
									BgL_iz00_4114 = BgL_iz00_4644;
									goto BgL_loopz00_4115;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzdataflow_walkz00(void)
	{
		{	/* Dataflow/walk.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzdataflow_walkz00(void)
	{
		{	/* Dataflow/walk.scm 17 */
			BGl_za2z42nullzf3za2zb1zzdataflow_walkz00 = BFALSE;
			BGl_za2z42pairzf3za2zb1zzdataflow_walkz00 = BFALSE;
			BGl_za2z42epairzf3za2zb1zzdataflow_walkz00 = BFALSE;
			BGl_letzd2varzd2stampz00zzdataflow_walkz00 = 0L;
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzdataflow_walkz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1813;

				BgL_headz00_1813 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1814;
					obj_t BgL_tailz00_1815;

					BgL_prevz00_1814 = BgL_headz00_1813;
					BgL_tailz00_1815 = BgL_l1z00_1;
				BgL_loopz00_1816:
					if (PAIRP(BgL_tailz00_1815))
						{
							obj_t BgL_newzd2prevzd2_1818;

							BgL_newzd2prevzd2_1818 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1815), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1814, BgL_newzd2prevzd2_1818);
							{
								obj_t BgL_tailz00_4654;
								obj_t BgL_prevz00_4653;

								BgL_prevz00_4653 = BgL_newzd2prevzd2_1818;
								BgL_tailz00_4654 = CDR(BgL_tailz00_1815);
								BgL_tailz00_1815 = BgL_tailz00_4654;
								BgL_prevz00_1814 = BgL_prevz00_4653;
								goto BgL_loopz00_1816;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1813);
				}
			}
		}

	}



/* dataflow-walk! */
	BGL_EXPORTED_DEF obj_t BGl_dataflowzd2walkz12zc0zzdataflow_walkz00(obj_t
		BgL_globalsz00_56, obj_t BgL_namez00_57)
	{
		{	/* Dataflow/walk.scm 45 */
			{	/* Dataflow/walk.scm 46 */
				obj_t BgL_list1490z00_1827;

				{	/* Dataflow/walk.scm 46 */
					obj_t BgL_arg1502z00_1828;

					{	/* Dataflow/walk.scm 46 */
						obj_t BgL_arg1509z00_1829;

						BgL_arg1509z00_1829 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1502z00_1828 =
							MAKE_YOUNG_PAIR(BgL_namez00_57, BgL_arg1509z00_1829);
					}
					BgL_list1490z00_1827 =
						MAKE_YOUNG_PAIR(BGl_string2242z00zzdataflow_walkz00,
						BgL_arg1502z00_1828);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1490z00_1827);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 = BgL_namez00_57;
			{	/* Dataflow/walk.scm 46 */
				obj_t BgL_g1141z00_1830;

				BgL_g1141z00_1830 = BNIL;
				{
					obj_t BgL_hooksz00_1833;
					obj_t BgL_hnamesz00_1834;

					BgL_hooksz00_1833 = BgL_g1141z00_1830;
					BgL_hnamesz00_1834 = BNIL;
				BgL_zc3z04anonymousza31510ze3z87_1835:
					if (NULLP(BgL_hooksz00_1833))
						{	/* Dataflow/walk.scm 46 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Dataflow/walk.scm 46 */
							bool_t BgL_test2382z00_4667;

							{	/* Dataflow/walk.scm 46 */
								obj_t BgL_fun1536z00_1842;

								BgL_fun1536z00_1842 = CAR(((obj_t) BgL_hooksz00_1833));
								BgL_test2382z00_4667 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1536z00_1842));
							}
							if (BgL_test2382z00_4667)
								{	/* Dataflow/walk.scm 46 */
									obj_t BgL_arg1514z00_1839;
									obj_t BgL_arg1516z00_1840;

									BgL_arg1514z00_1839 = CDR(((obj_t) BgL_hooksz00_1833));
									BgL_arg1516z00_1840 = CDR(((obj_t) BgL_hnamesz00_1834));
									{
										obj_t BgL_hnamesz00_4679;
										obj_t BgL_hooksz00_4678;

										BgL_hooksz00_4678 = BgL_arg1514z00_1839;
										BgL_hnamesz00_4679 = BgL_arg1516z00_1840;
										BgL_hnamesz00_1834 = BgL_hnamesz00_4679;
										BgL_hooksz00_1833 = BgL_hooksz00_4678;
										goto BgL_zc3z04anonymousza31510ze3z87_1835;
									}
								}
							else
								{	/* Dataflow/walk.scm 46 */
									obj_t BgL_arg1535z00_1841;

									BgL_arg1535z00_1841 = CAR(((obj_t) BgL_hnamesz00_1834));
									BGl_internalzd2errorzd2zztools_errorz00(BgL_namez00_57,
										BGl_string2243z00zzdataflow_walkz00, BgL_arg1535z00_1841);
								}
						}
				}
			}
			BGl_za2z42nullzf3za2zb1zzdataflow_walkz00 =
				BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(1),
				CNST_TABLE_REF(2));
			BGl_za2z42pairzf3za2zb1zzdataflow_walkz00 =
				BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(3),
				CNST_TABLE_REF(2));
			BGl_za2z42epairzf3za2zb1zzdataflow_walkz00 =
				BGl_findzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(4),
				CNST_TABLE_REF(2));
			{
				obj_t BgL_l1331z00_1846;

				BgL_l1331z00_1846 = BgL_globalsz00_56;
			BgL_zc3z04anonymousza31538ze3z87_1847:
				if (PAIRP(BgL_l1331z00_1846))
					{	/* Dataflow/walk.scm 50 */
						{	/* Dataflow/walk.scm 50 */
							obj_t BgL_arg1540z00_1849;

							BgL_arg1540z00_1849 = CAR(BgL_l1331z00_1846);
							{	/* Dataflow/walk.scm 64 */
								obj_t BgL_arg1575z00_2853;

								BgL_arg1575z00_2853 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_arg1540z00_1849))))->
													BgL_valuez00))))->BgL_bodyz00);
								BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(((BgL_nodez00_bglt)
										BgL_arg1575z00_2853), BNIL);
							}
						}
						{
							obj_t BgL_l1331z00_4702;

							BgL_l1331z00_4702 = CDR(BgL_l1331z00_1846);
							BgL_l1331z00_1846 = BgL_l1331z00_4702;
							goto BgL_zc3z04anonymousza31538ze3z87_1847;
						}
					}
				else
					{	/* Dataflow/walk.scm 50 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Dataflow/walk.scm 51 */
					{	/* Dataflow/walk.scm 51 */
						obj_t BgL_port1333z00_1854;

						{	/* Dataflow/walk.scm 51 */
							obj_t BgL_tmpz00_4707;

							BgL_tmpz00_4707 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1333z00_1854 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4707);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1333z00_1854);
						bgl_display_string(BGl_string2244z00zzdataflow_walkz00,
							BgL_port1333z00_1854);
						{	/* Dataflow/walk.scm 51 */
							obj_t BgL_arg1552z00_1855;

							{	/* Dataflow/walk.scm 51 */
								bool_t BgL_test2385z00_4712;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Dataflow/walk.scm 51 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Dataflow/walk.scm 51 */
												BgL_test2385z00_4712 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Dataflow/walk.scm 51 */
												BgL_test2385z00_4712 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Dataflow/walk.scm 51 */
										BgL_test2385z00_4712 = ((bool_t) 0);
									}
								if (BgL_test2385z00_4712)
									{	/* Dataflow/walk.scm 51 */
										BgL_arg1552z00_1855 = BGl_string2245z00zzdataflow_walkz00;
									}
								else
									{	/* Dataflow/walk.scm 51 */
										BgL_arg1552z00_1855 = BGl_string2246z00zzdataflow_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1552z00_1855, BgL_port1333z00_1854);
						}
						bgl_display_string(BGl_string2247z00zzdataflow_walkz00,
							BgL_port1333z00_1854);
						bgl_display_char(((unsigned char) 10), BgL_port1333z00_1854);
					}
					{	/* Dataflow/walk.scm 51 */
						obj_t BgL_list1556z00_1859;

						BgL_list1556z00_1859 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						return BGl_exitz00zz__errorz00(BgL_list1556z00_1859);
					}
				}
			else
				{	/* Dataflow/walk.scm 51 */
					obj_t BgL_g1143z00_1860;

					BgL_g1143z00_1860 = BNIL;
					{
						obj_t BgL_hooksz00_1863;
						obj_t BgL_hnamesz00_1864;

						BgL_hooksz00_1863 = BgL_g1143z00_1860;
						BgL_hnamesz00_1864 = BNIL;
					BgL_zc3z04anonymousza31557ze3z87_1865:
						if (NULLP(BgL_hooksz00_1863))
							{	/* Dataflow/walk.scm 51 */
								return BgL_globalsz00_56;
							}
						else
							{	/* Dataflow/walk.scm 51 */
								bool_t BgL_test2389z00_4729;

								{	/* Dataflow/walk.scm 51 */
									obj_t BgL_fun1573z00_1872;

									BgL_fun1573z00_1872 = CAR(((obj_t) BgL_hooksz00_1863));
									BgL_test2389z00_4729 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1573z00_1872));
								}
								if (BgL_test2389z00_4729)
									{	/* Dataflow/walk.scm 51 */
										obj_t BgL_arg1564z00_1869;
										obj_t BgL_arg1565z00_1870;

										BgL_arg1564z00_1869 = CDR(((obj_t) BgL_hooksz00_1863));
										BgL_arg1565z00_1870 = CDR(((obj_t) BgL_hnamesz00_1864));
										{
											obj_t BgL_hnamesz00_4741;
											obj_t BgL_hooksz00_4740;

											BgL_hooksz00_4740 = BgL_arg1564z00_1869;
											BgL_hnamesz00_4741 = BgL_arg1565z00_1870;
											BgL_hnamesz00_1864 = BgL_hnamesz00_4741;
											BgL_hooksz00_1863 = BgL_hooksz00_4740;
											goto BgL_zc3z04anonymousza31557ze3z87_1865;
										}
									}
								else
									{	/* Dataflow/walk.scm 51 */
										obj_t BgL_arg1571z00_1871;

										BgL_arg1571z00_1871 = CAR(((obj_t) BgL_hnamesz00_1864));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string2248z00zzdataflow_walkz00, BgL_arg1571z00_1871);
									}
							}
					}
				}
		}

	}



/* &dataflow-walk! */
	obj_t BGl_z62dataflowzd2walkz12za2zzdataflow_walkz00(obj_t BgL_envz00_3923,
		obj_t BgL_globalsz00_3924, obj_t BgL_namez00_3925)
	{
		{	/* Dataflow/walk.scm 45 */
			return
				BGl_dataflowzd2walkz12zc0zzdataflow_walkz00(BgL_globalsz00_3924,
				BgL_namez00_3925);
		}

	}



/* dataflow-node*! */
	obj_t BGl_dataflowzd2nodeza2z12z62zzdataflow_walkz00(obj_t BgL_nodeza2za2_99,
		obj_t BgL_envz00_100)
	{
		{	/* Dataflow/walk.scm 309 */
			{
				obj_t BgL_l1355z00_1878;

				BgL_l1355z00_1878 = BgL_nodeza2za2_99;
			BgL_zc3z04anonymousza31577ze3z87_1879:
				if (PAIRP(BgL_l1355z00_1878))
					{	/* Dataflow/walk.scm 310 */
						{	/* Dataflow/walk.scm 310 */
							obj_t BgL_nz00_1881;

							BgL_nz00_1881 = CAR(BgL_l1355z00_1878);
							BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
								((BgL_nodez00_bglt) BgL_nz00_1881), BgL_envz00_100);
						}
						{
							obj_t BgL_l1355z00_4751;

							BgL_l1355z00_4751 = CDR(BgL_l1355z00_1878);
							BgL_l1355z00_1878 = BgL_l1355z00_4751;
							goto BgL_zc3z04anonymousza31577ze3z87_1879;
						}
					}
				else
					{	/* Dataflow/walk.scm 310 */
						((bool_t) 1);
					}
			}
			return BgL_envz00_100;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzdataflow_walkz00(void)
	{
		{	/* Dataflow/walk.scm 17 */
			{	/* Dataflow/walk.scm 37 */
				obj_t BgL_arg1594z00_1888;
				obj_t BgL_arg1595z00_1889;

				{	/* Dataflow/walk.scm 37 */
					obj_t BgL_v1373z00_1925;

					BgL_v1373z00_1925 = create_vector(2L);
					{	/* Dataflow/walk.scm 37 */
						obj_t BgL_arg1625z00_1926;

						BgL_arg1625z00_1926 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc2250z00zzdataflow_walkz00,
							BGl_proc2249z00zzdataflow_walkz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1373z00_1925, 0L, BgL_arg1625z00_1926);
					}
					{	/* Dataflow/walk.scm 37 */
						obj_t BgL_arg1630z00_1936;

						BgL_arg1630z00_1936 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc2252z00zzdataflow_walkz00,
							BGl_proc2251z00zzdataflow_walkz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_nodez00zzast_nodez00);
						VECTOR_SET(BgL_v1373z00_1925, 1L, BgL_arg1630z00_1936);
					}
					BgL_arg1594z00_1888 = BgL_v1373z00_1925;
				}
				{	/* Dataflow/walk.scm 37 */
					obj_t BgL_v1374z00_1946;

					BgL_v1374z00_1946 = create_vector(0L);
					BgL_arg1595z00_1889 = BgL_v1374z00_1946;
				}
				return (BGl_localzf2valuezf2zzdataflow_walkz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(8),
						CNST_TABLE_REF(9), BGl_localz00zzast_varz00, 51723L,
						BGl_proc2256z00zzdataflow_walkz00,
						BGl_proc2255z00zzdataflow_walkz00, BFALSE,
						BGl_proc2254z00zzdataflow_walkz00,
						BGl_proc2253z00zzdataflow_walkz00, BgL_arg1594z00_1888,
						BgL_arg1595z00_1889), BUNSPEC);
			}
		}

	}



/* &lambda1611 */
	BgL_localz00_bglt BGl_z62lambda1611z62zzdataflow_walkz00(obj_t
		BgL_envz00_3934, obj_t BgL_o1128z00_3935)
	{
		{	/* Dataflow/walk.scm 37 */
			{	/* Dataflow/walk.scm 37 */
				long BgL_arg1613z00_4125;

				{	/* Dataflow/walk.scm 37 */
					obj_t BgL_arg1615z00_4126;

					{	/* Dataflow/walk.scm 37 */
						obj_t BgL_arg1616z00_4127;

						{	/* Dataflow/walk.scm 37 */
							obj_t BgL_arg1815z00_4128;
							long BgL_arg1816z00_4129;

							BgL_arg1815z00_4128 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Dataflow/walk.scm 37 */
								long BgL_arg1817z00_4130;

								BgL_arg1817z00_4130 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1128z00_3935)));
								BgL_arg1816z00_4129 = (BgL_arg1817z00_4130 - OBJECT_TYPE);
							}
							BgL_arg1616z00_4127 =
								VECTOR_REF(BgL_arg1815z00_4128, BgL_arg1816z00_4129);
						}
						BgL_arg1615z00_4126 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1616z00_4127);
					}
					{	/* Dataflow/walk.scm 37 */
						obj_t BgL_tmpz00_4772;

						BgL_tmpz00_4772 = ((obj_t) BgL_arg1615z00_4126);
						BgL_arg1613z00_4125 = BGL_CLASS_NUM(BgL_tmpz00_4772);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1128z00_3935)), BgL_arg1613z00_4125);
			}
			{	/* Dataflow/walk.scm 37 */
				BgL_objectz00_bglt BgL_tmpz00_4778;

				BgL_tmpz00_4778 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1128z00_3935));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4778, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1128z00_3935));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1128z00_3935));
		}

	}



/* &<@anonymous:1610> */
	obj_t BGl_z62zc3z04anonymousza31610ze3ze5zzdataflow_walkz00(obj_t
		BgL_envz00_3936, obj_t BgL_new1127z00_3937)
	{
		{	/* Dataflow/walk.scm 37 */
			{
				BgL_localz00_bglt BgL_auxz00_4786;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1127z00_3937))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(10)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_4794;

					{	/* Dataflow/walk.scm 37 */
						obj_t BgL_classz00_4132;

						BgL_classz00_4132 = BGl_typez00zztype_typez00;
						{	/* Dataflow/walk.scm 37 */
							obj_t BgL__ortest_1117z00_4133;

							BgL__ortest_1117z00_4133 = BGL_CLASS_NIL(BgL_classz00_4132);
							if (CBOOL(BgL__ortest_1117z00_4133))
								{	/* Dataflow/walk.scm 37 */
									BgL_auxz00_4794 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4133);
								}
							else
								{	/* Dataflow/walk.scm 37 */
									BgL_auxz00_4794 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4132));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1127z00_3937))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_4794), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_4804;

					{	/* Dataflow/walk.scm 37 */
						obj_t BgL_classz00_4134;

						BgL_classz00_4134 = BGl_valuez00zzast_varz00;
						{	/* Dataflow/walk.scm 37 */
							obj_t BgL__ortest_1117z00_4135;

							BgL__ortest_1117z00_4135 = BGL_CLASS_NIL(BgL_classz00_4134);
							if (CBOOL(BgL__ortest_1117z00_4135))
								{	/* Dataflow/walk.scm 37 */
									BgL_auxz00_4804 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_4135);
								}
							else
								{	/* Dataflow/walk.scm 37 */
									BgL_auxz00_4804 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4134));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1127z00_3937))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_4804), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1127z00_3937))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1127z00_3937))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2valuezf2_bglt BgL_auxz00_4841;

					{
						obj_t BgL_auxz00_4842;

						{	/* Dataflow/walk.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_4843;

							BgL_tmpz00_4843 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1127z00_3937));
							BgL_auxz00_4842 = BGL_OBJECT_WIDENING(BgL_tmpz00_4843);
						}
						BgL_auxz00_4841 = ((BgL_localzf2valuezf2_bglt) BgL_auxz00_4842);
					}
					((((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_4841))->
							BgL_stampz00) = ((int) (int) (0L)), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4857;
					BgL_localzf2valuezf2_bglt BgL_auxz00_4850;

					{	/* Dataflow/walk.scm 37 */
						obj_t BgL_classz00_4136;

						BgL_classz00_4136 = BGl_nodez00zzast_nodez00;
						{	/* Dataflow/walk.scm 37 */
							obj_t BgL__ortest_1117z00_4137;

							BgL__ortest_1117z00_4137 = BGL_CLASS_NIL(BgL_classz00_4136);
							if (CBOOL(BgL__ortest_1117z00_4137))
								{	/* Dataflow/walk.scm 37 */
									BgL_auxz00_4857 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_4137);
								}
							else
								{	/* Dataflow/walk.scm 37 */
									BgL_auxz00_4857 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4136));
								}
						}
					}
					{
						obj_t BgL_auxz00_4851;

						{	/* Dataflow/walk.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_4852;

							BgL_tmpz00_4852 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1127z00_3937));
							BgL_auxz00_4851 = BGL_OBJECT_WIDENING(BgL_tmpz00_4852);
						}
						BgL_auxz00_4850 = ((BgL_localzf2valuezf2_bglt) BgL_auxz00_4851);
					}
					((((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_4850))->
							BgL_nodez00) = ((BgL_nodez00_bglt) BgL_auxz00_4857), BUNSPEC);
				}
				BgL_auxz00_4786 = ((BgL_localz00_bglt) BgL_new1127z00_3937);
				return ((obj_t) BgL_auxz00_4786);
			}
		}

	}



/* &lambda1606 */
	BgL_localz00_bglt BGl_z62lambda1606z62zzdataflow_walkz00(obj_t
		BgL_envz00_3938, obj_t BgL_o1123z00_3939)
	{
		{	/* Dataflow/walk.scm 37 */
			{	/* Dataflow/walk.scm 37 */
				BgL_localzf2valuezf2_bglt BgL_wide1125z00_4139;

				BgL_wide1125z00_4139 =
					((BgL_localzf2valuezf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2valuezf2_bgl))));
				{	/* Dataflow/walk.scm 37 */
					obj_t BgL_auxz00_4872;
					BgL_objectz00_bglt BgL_tmpz00_4868;

					BgL_auxz00_4872 = ((obj_t) BgL_wide1125z00_4139);
					BgL_tmpz00_4868 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_3939)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4868, BgL_auxz00_4872);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_3939)));
				{	/* Dataflow/walk.scm 37 */
					long BgL_arg1609z00_4140;

					BgL_arg1609z00_4140 =
						BGL_CLASS_NUM(BGl_localzf2valuezf2zzdataflow_walkz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1123z00_3939))), BgL_arg1609z00_4140);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1123z00_3939)));
			}
		}

	}



/* &lambda1596 */
	BgL_localz00_bglt BGl_z62lambda1596z62zzdataflow_walkz00(obj_t
		BgL_envz00_3940, obj_t BgL_id1108z00_3941, obj_t BgL_name1109z00_3942,
		obj_t BgL_type1110z00_3943, obj_t BgL_value1111z00_3944,
		obj_t BgL_access1112z00_3945, obj_t BgL_fastzd2alpha1113zd2_3946,
		obj_t BgL_removable1114z00_3947, obj_t BgL_occurrence1115z00_3948,
		obj_t BgL_occurrencew1116z00_3949, obj_t BgL_userzf31117zf3_3950,
		obj_t BgL_key1118z00_3951, obj_t BgL_valzd2noescape1119zd2_3952,
		obj_t BgL_volatile1120z00_3953, obj_t BgL_stamp1121z00_3954,
		obj_t BgL_node1122z00_3955)
	{
		{	/* Dataflow/walk.scm 37 */
			{	/* Dataflow/walk.scm 37 */
				long BgL_occurrence1115z00_4144;
				long BgL_occurrencew1116z00_4145;
				bool_t BgL_userzf31117zf3_4146;
				long BgL_key1118z00_4147;
				bool_t BgL_volatile1120z00_4148;
				int BgL_stamp1121z00_4149;

				BgL_occurrence1115z00_4144 = (long) CINT(BgL_occurrence1115z00_3948);
				BgL_occurrencew1116z00_4145 = (long) CINT(BgL_occurrencew1116z00_3949);
				BgL_userzf31117zf3_4146 = CBOOL(BgL_userzf31117zf3_3950);
				BgL_key1118z00_4147 = (long) CINT(BgL_key1118z00_3951);
				BgL_volatile1120z00_4148 = CBOOL(BgL_volatile1120z00_3953);
				BgL_stamp1121z00_4149 = CINT(BgL_stamp1121z00_3954);
				{	/* Dataflow/walk.scm 37 */
					BgL_localz00_bglt BgL_new1195z00_4151;

					{	/* Dataflow/walk.scm 37 */
						BgL_localz00_bglt BgL_tmp1193z00_4152;
						BgL_localzf2valuezf2_bglt BgL_wide1194z00_4153;

						{
							BgL_localz00_bglt BgL_auxz00_4892;

							{	/* Dataflow/walk.scm 37 */
								BgL_localz00_bglt BgL_new1192z00_4154;

								BgL_new1192z00_4154 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Dataflow/walk.scm 37 */
									long BgL_arg1605z00_4155;

									BgL_arg1605z00_4155 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1192z00_4154),
										BgL_arg1605z00_4155);
								}
								{	/* Dataflow/walk.scm 37 */
									BgL_objectz00_bglt BgL_tmpz00_4897;

									BgL_tmpz00_4897 = ((BgL_objectz00_bglt) BgL_new1192z00_4154);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4897, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1192z00_4154);
								BgL_auxz00_4892 = BgL_new1192z00_4154;
							}
							BgL_tmp1193z00_4152 = ((BgL_localz00_bglt) BgL_auxz00_4892);
						}
						BgL_wide1194z00_4153 =
							((BgL_localzf2valuezf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2valuezf2_bgl))));
						{	/* Dataflow/walk.scm 37 */
							obj_t BgL_auxz00_4905;
							BgL_objectz00_bglt BgL_tmpz00_4903;

							BgL_auxz00_4905 = ((obj_t) BgL_wide1194z00_4153);
							BgL_tmpz00_4903 = ((BgL_objectz00_bglt) BgL_tmp1193z00_4152);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4903, BgL_auxz00_4905);
						}
						((BgL_objectz00_bglt) BgL_tmp1193z00_4152);
						{	/* Dataflow/walk.scm 37 */
							long BgL_arg1602z00_4156;

							BgL_arg1602z00_4156 =
								BGL_CLASS_NUM(BGl_localzf2valuezf2zzdataflow_walkz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1193z00_4152),
								BgL_arg1602z00_4156);
						}
						BgL_new1195z00_4151 = ((BgL_localz00_bglt) BgL_tmp1193z00_4152);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1195z00_4151)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1108z00_3941)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_namez00) =
						((obj_t) BgL_name1109z00_3942), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1110z00_3943)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1111z00_3944)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_accessz00) =
						((obj_t) BgL_access1112z00_3945), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1113zd2_3946), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_removablez00) =
						((obj_t) BgL_removable1114z00_3947), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_occurrencez00) =
						((long) BgL_occurrence1115z00_4144), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1116z00_4145), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1195z00_4151)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31117zf3_4146), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1195z00_4151)))->BgL_keyz00) =
						((long) BgL_key1118z00_4147), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1195z00_4151)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1119zd2_3952), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1195z00_4151)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1120z00_4148), BUNSPEC);
					{
						BgL_localzf2valuezf2_bglt BgL_auxz00_4942;

						{
							obj_t BgL_auxz00_4943;

							{	/* Dataflow/walk.scm 37 */
								BgL_objectz00_bglt BgL_tmpz00_4944;

								BgL_tmpz00_4944 = ((BgL_objectz00_bglt) BgL_new1195z00_4151);
								BgL_auxz00_4943 = BGL_OBJECT_WIDENING(BgL_tmpz00_4944);
							}
							BgL_auxz00_4942 = ((BgL_localzf2valuezf2_bglt) BgL_auxz00_4943);
						}
						((((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_4942))->
								BgL_stampz00) = ((int) BgL_stamp1121z00_4149), BUNSPEC);
					}
					{
						BgL_localzf2valuezf2_bglt BgL_auxz00_4949;

						{
							obj_t BgL_auxz00_4950;

							{	/* Dataflow/walk.scm 37 */
								BgL_objectz00_bglt BgL_tmpz00_4951;

								BgL_tmpz00_4951 = ((BgL_objectz00_bglt) BgL_new1195z00_4151);
								BgL_auxz00_4950 = BGL_OBJECT_WIDENING(BgL_tmpz00_4951);
							}
							BgL_auxz00_4949 = ((BgL_localzf2valuezf2_bglt) BgL_auxz00_4950);
						}
						((((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_4949))->
								BgL_nodez00) =
							((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_node1122z00_3955)),
							BUNSPEC);
					}
					return BgL_new1195z00_4151;
				}
			}
		}

	}



/* &lambda1648 */
	obj_t BGl_z62lambda1648z62zzdataflow_walkz00(obj_t BgL_envz00_3956,
		obj_t BgL_oz00_3957, obj_t BgL_vz00_3958)
	{
		{	/* Dataflow/walk.scm 37 */
			{
				BgL_localzf2valuezf2_bglt BgL_auxz00_4957;

				{
					obj_t BgL_auxz00_4958;

					{	/* Dataflow/walk.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_4959;

						BgL_tmpz00_4959 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_3957));
						BgL_auxz00_4958 = BGL_OBJECT_WIDENING(BgL_tmpz00_4959);
					}
					BgL_auxz00_4957 = ((BgL_localzf2valuezf2_bglt) BgL_auxz00_4958);
				}
				return
					((((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_4957))->
						BgL_nodez00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_vz00_3958)), BUNSPEC);
			}
		}

	}



/* &lambda1647 */
	BgL_nodez00_bglt BGl_z62lambda1647z62zzdataflow_walkz00(obj_t BgL_envz00_3959,
		obj_t BgL_oz00_3960)
	{
		{	/* Dataflow/walk.scm 37 */
			{
				BgL_localzf2valuezf2_bglt BgL_auxz00_4966;

				{
					obj_t BgL_auxz00_4967;

					{	/* Dataflow/walk.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_4968;

						BgL_tmpz00_4968 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_3960));
						BgL_auxz00_4967 = BGL_OBJECT_WIDENING(BgL_tmpz00_4968);
					}
					BgL_auxz00_4966 = ((BgL_localzf2valuezf2_bglt) BgL_auxz00_4967);
				}
				return
					(((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_4966))->BgL_nodez00);
			}
		}

	}



/* &lambda1629 */
	obj_t BGl_z62lambda1629z62zzdataflow_walkz00(obj_t BgL_envz00_3961,
		obj_t BgL_oz00_3962, obj_t BgL_vz00_3963)
	{
		{	/* Dataflow/walk.scm 37 */
			{	/* Dataflow/walk.scm 37 */
				int BgL_vz00_4161;

				BgL_vz00_4161 = CINT(BgL_vz00_3963);
				{
					BgL_localzf2valuezf2_bglt BgL_auxz00_4975;

					{
						obj_t BgL_auxz00_4976;

						{	/* Dataflow/walk.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_4977;

							BgL_tmpz00_4977 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_3962));
							BgL_auxz00_4976 = BGL_OBJECT_WIDENING(BgL_tmpz00_4977);
						}
						BgL_auxz00_4975 = ((BgL_localzf2valuezf2_bglt) BgL_auxz00_4976);
					}
					return
						((((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_4975))->
							BgL_stampz00) = ((int) BgL_vz00_4161), BUNSPEC);
		}}}

	}



/* &lambda1628 */
	obj_t BGl_z62lambda1628z62zzdataflow_walkz00(obj_t BgL_envz00_3964,
		obj_t BgL_oz00_3965)
	{
		{	/* Dataflow/walk.scm 37 */
			{	/* Dataflow/walk.scm 37 */
				int BgL_tmpz00_4983;

				{
					BgL_localzf2valuezf2_bglt BgL_auxz00_4984;

					{
						obj_t BgL_auxz00_4985;

						{	/* Dataflow/walk.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_4986;

							BgL_tmpz00_4986 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_3965));
							BgL_auxz00_4985 = BGL_OBJECT_WIDENING(BgL_tmpz00_4986);
						}
						BgL_auxz00_4984 = ((BgL_localzf2valuezf2_bglt) BgL_auxz00_4985);
					}
					BgL_tmpz00_4983 =
						(((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_4984))->
						BgL_stampz00);
				}
				return BINT(BgL_tmpz00_4983);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzdataflow_walkz00(void)
	{
		{	/* Dataflow/walk.scm 17 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_proc2257z00zzdataflow_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2258z00zzdataflow_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_dataflowzd2testzd2envzd2envzd2zzdataflow_walkz00,
				BGl_proc2259z00zzdataflow_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2260z00zzdataflow_walkz00);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_dataflowzd2testzd2falsezd2envzd2envz00zzdataflow_walkz00,
				BGl_proc2261z00zzdataflow_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2262z00zzdataflow_walkz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00,
				BGl_proc2263z00zzdataflow_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2264z00zzdataflow_walkz00);
		}

	}



/* &abort?1441 */
	obj_t BGl_z62abortzf31441z91zzdataflow_walkz00(obj_t BgL_envz00_3970,
		obj_t BgL_nodez00_3971)
	{
		{	/* Dataflow/walk.scm 430 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &dataflow-test-false-1433 */
	obj_t BGl_z62dataflowzd2testzd2falsezd21433zb0zzdataflow_walkz00(obj_t
		BgL_envz00_3972, obj_t BgL_nodez00_3973)
	{
		{	/* Dataflow/walk.scm 402 */
			return BNIL;
		}

	}



/* &dataflow-test-env1419 */
	obj_t BGl_z62dataflowzd2testzd2env1419z62zzdataflow_walkz00(obj_t
		BgL_envz00_3974, obj_t BgL_nodez00_3975)
	{
		{	/* Dataflow/walk.scm 316 */
			return BNIL;
		}

	}



/* &dataflow-node!1375 */
	obj_t BGl_z62dataflowzd2nodez121375za2zzdataflow_walkz00(obj_t
		BgL_envz00_3976, obj_t BgL_nodez00_3977, obj_t BgL_envz00_3978)
	{
		{	/* Dataflow/walk.scm 69 */
			return ((obj_t) BgL_envz00_3978);
		}

	}



/* dataflow-node! */
	obj_t BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(BgL_nodez00_bglt
		BgL_nodez00_59, obj_t BgL_envz00_60)
	{
		{	/* Dataflow/walk.scm 69 */
			{	/* Dataflow/walk.scm 69 */
				obj_t BgL_method1376z00_1968;

				{	/* Dataflow/walk.scm 69 */
					obj_t BgL_res2200z00_2948;

					{	/* Dataflow/walk.scm 69 */
						long BgL_objzd2classzd2numz00_2919;

						BgL_objzd2classzd2numz00_2919 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_59));
						{	/* Dataflow/walk.scm 69 */
							obj_t BgL_arg1811z00_2920;

							BgL_arg1811z00_2920 =
								PROCEDURE_REF(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
								(int) (1L));
							{	/* Dataflow/walk.scm 69 */
								int BgL_offsetz00_2923;

								BgL_offsetz00_2923 = (int) (BgL_objzd2classzd2numz00_2919);
								{	/* Dataflow/walk.scm 69 */
									long BgL_offsetz00_2924;

									BgL_offsetz00_2924 =
										((long) (BgL_offsetz00_2923) - OBJECT_TYPE);
									{	/* Dataflow/walk.scm 69 */
										long BgL_modz00_2925;

										BgL_modz00_2925 =
											(BgL_offsetz00_2924 >> (int) ((long) ((int) (4L))));
										{	/* Dataflow/walk.scm 69 */
											long BgL_restz00_2927;

											BgL_restz00_2927 =
												(BgL_offsetz00_2924 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Dataflow/walk.scm 69 */

												{	/* Dataflow/walk.scm 69 */
													obj_t BgL_bucketz00_2929;

													BgL_bucketz00_2929 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2920), BgL_modz00_2925);
													BgL_res2200z00_2948 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2929), BgL_restz00_2927);
					}}}}}}}}
					BgL_method1376z00_1968 = BgL_res2200z00_2948;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1376z00_1968,
					((obj_t) BgL_nodez00_59), BgL_envz00_60);
			}
		}

	}



/* &dataflow-node! */
	obj_t BGl_z62dataflowzd2nodez12za2zzdataflow_walkz00(obj_t BgL_envz00_3979,
		obj_t BgL_nodez00_3980, obj_t BgL_envz00_3981)
	{
		{	/* Dataflow/walk.scm 69 */
			return
				BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3980), BgL_envz00_3981);
		}

	}



/* dataflow-test-env */
	obj_t BGl_dataflowzd2testzd2envz00zzdataflow_walkz00(BgL_nodez00_bglt
		BgL_nodez00_101)
	{
		{	/* Dataflow/walk.scm 316 */
			{	/* Dataflow/walk.scm 316 */
				obj_t BgL_method1420z00_1969;

				{	/* Dataflow/walk.scm 316 */
					obj_t BgL_res2205z00_2979;

					{	/* Dataflow/walk.scm 316 */
						long BgL_objzd2classzd2numz00_2950;

						BgL_objzd2classzd2numz00_2950 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_101));
						{	/* Dataflow/walk.scm 316 */
							obj_t BgL_arg1811z00_2951;

							BgL_arg1811z00_2951 =
								PROCEDURE_REF
								(BGl_dataflowzd2testzd2envzd2envzd2zzdataflow_walkz00,
								(int) (1L));
							{	/* Dataflow/walk.scm 316 */
								int BgL_offsetz00_2954;

								BgL_offsetz00_2954 = (int) (BgL_objzd2classzd2numz00_2950);
								{	/* Dataflow/walk.scm 316 */
									long BgL_offsetz00_2955;

									BgL_offsetz00_2955 =
										((long) (BgL_offsetz00_2954) - OBJECT_TYPE);
									{	/* Dataflow/walk.scm 316 */
										long BgL_modz00_2956;

										BgL_modz00_2956 =
											(BgL_offsetz00_2955 >> (int) ((long) ((int) (4L))));
										{	/* Dataflow/walk.scm 316 */
											long BgL_restz00_2958;

											BgL_restz00_2958 =
												(BgL_offsetz00_2955 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Dataflow/walk.scm 316 */

												{	/* Dataflow/walk.scm 316 */
													obj_t BgL_bucketz00_2960;

													BgL_bucketz00_2960 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2951), BgL_modz00_2956);
													BgL_res2205z00_2979 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2960), BgL_restz00_2958);
					}}}}}}}}
					BgL_method1420z00_1969 = BgL_res2205z00_2979;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1420z00_1969,
					((obj_t) BgL_nodez00_101));
			}
		}

	}



/* &dataflow-test-env */
	obj_t BGl_z62dataflowzd2testzd2envz62zzdataflow_walkz00(obj_t BgL_envz00_3982,
		obj_t BgL_nodez00_3983)
	{
		{	/* Dataflow/walk.scm 316 */
			return
				BGl_dataflowzd2testzd2envz00zzdataflow_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3983));
		}

	}



/* dataflow-test-false-env */
	obj_t BGl_dataflowzd2testzd2falsezd2envzd2zzdataflow_walkz00(BgL_nodez00_bglt
		BgL_nodez00_107)
	{
		{	/* Dataflow/walk.scm 402 */
			{	/* Dataflow/walk.scm 402 */
				obj_t BgL_method1437z00_1970;

				{	/* Dataflow/walk.scm 402 */
					obj_t BgL_res2210z00_3010;

					{	/* Dataflow/walk.scm 402 */
						long BgL_objzd2classzd2numz00_2981;

						BgL_objzd2classzd2numz00_2981 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_107));
						{	/* Dataflow/walk.scm 402 */
							obj_t BgL_arg1811z00_2982;

							BgL_arg1811z00_2982 =
								PROCEDURE_REF
								(BGl_dataflowzd2testzd2falsezd2envzd2envz00zzdataflow_walkz00,
								(int) (1L));
							{	/* Dataflow/walk.scm 402 */
								int BgL_offsetz00_2985;

								BgL_offsetz00_2985 = (int) (BgL_objzd2classzd2numz00_2981);
								{	/* Dataflow/walk.scm 402 */
									long BgL_offsetz00_2986;

									BgL_offsetz00_2986 =
										((long) (BgL_offsetz00_2985) - OBJECT_TYPE);
									{	/* Dataflow/walk.scm 402 */
										long BgL_modz00_2987;

										BgL_modz00_2987 =
											(BgL_offsetz00_2986 >> (int) ((long) ((int) (4L))));
										{	/* Dataflow/walk.scm 402 */
											long BgL_restz00_2989;

											BgL_restz00_2989 =
												(BgL_offsetz00_2986 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Dataflow/walk.scm 402 */

												{	/* Dataflow/walk.scm 402 */
													obj_t BgL_bucketz00_2991;

													BgL_bucketz00_2991 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2982), BgL_modz00_2987);
													BgL_res2210z00_3010 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2991), BgL_restz00_2989);
					}}}}}}}}
					BgL_method1437z00_1970 = BgL_res2210z00_3010;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1437z00_1970,
					((obj_t) BgL_nodez00_107));
			}
		}

	}



/* &dataflow-test-false-env */
	obj_t BGl_z62dataflowzd2testzd2falsezd2envzb0zzdataflow_walkz00(obj_t
		BgL_envz00_3984, obj_t BgL_nodez00_3985)
	{
		{	/* Dataflow/walk.scm 402 */
			return
				BGl_dataflowzd2testzd2falsezd2envzd2zzdataflow_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3985));
		}

	}



/* abort? */
	obj_t BGl_abortzf3zf3zzdataflow_walkz00(BgL_nodez00_bglt BgL_nodez00_109)
	{
		{	/* Dataflow/walk.scm 430 */
			{	/* Dataflow/walk.scm 430 */
				obj_t BgL_method1442z00_1971;

				{	/* Dataflow/walk.scm 430 */
					obj_t BgL_res2215z00_3041;

					{	/* Dataflow/walk.scm 430 */
						long BgL_objzd2classzd2numz00_3012;

						BgL_objzd2classzd2numz00_3012 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_109));
						{	/* Dataflow/walk.scm 430 */
							obj_t BgL_arg1811z00_3013;

							BgL_arg1811z00_3013 =
								PROCEDURE_REF(BGl_abortzf3zd2envz21zzdataflow_walkz00,
								(int) (1L));
							{	/* Dataflow/walk.scm 430 */
								int BgL_offsetz00_3016;

								BgL_offsetz00_3016 = (int) (BgL_objzd2classzd2numz00_3012);
								{	/* Dataflow/walk.scm 430 */
									long BgL_offsetz00_3017;

									BgL_offsetz00_3017 =
										((long) (BgL_offsetz00_3016) - OBJECT_TYPE);
									{	/* Dataflow/walk.scm 430 */
										long BgL_modz00_3018;

										BgL_modz00_3018 =
											(BgL_offsetz00_3017 >> (int) ((long) ((int) (4L))));
										{	/* Dataflow/walk.scm 430 */
											long BgL_restz00_3020;

											BgL_restz00_3020 =
												(BgL_offsetz00_3017 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Dataflow/walk.scm 430 */

												{	/* Dataflow/walk.scm 430 */
													obj_t BgL_bucketz00_3022;

													BgL_bucketz00_3022 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3013), BgL_modz00_3018);
													BgL_res2215z00_3041 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3022), BgL_restz00_3020);
					}}}}}}}}
					BgL_method1442z00_1971 = BgL_res2215z00_3041;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1442z00_1971,
					((obj_t) BgL_nodez00_109));
			}
		}

	}



/* &abort? */
	obj_t BGl_z62abortzf3z91zzdataflow_walkz00(obj_t BgL_envz00_3986,
		obj_t BgL_nodez00_3987)
	{
		{	/* Dataflow/walk.scm 430 */
			return
				BGl_abortzf3zf3zzdataflow_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_3987));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzdataflow_walkz00(void)
	{
		{	/* Dataflow/walk.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_varz00zzast_nodez00, BGl_proc2265z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_closurez00zzast_nodez00, BGl_proc2267z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2268z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_syncz00zzast_nodez00, BGl_proc2269z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_appz00zzast_nodez00, BGl_proc2270z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2271z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2272z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_externz00zzast_nodez00, BGl_proc2273z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_setqz00zzast_nodez00, BGl_proc2274z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2275z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_failz00zzast_nodez00, BGl_proc2276z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_switchz00zzast_nodez00, BGl_proc2277z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2278z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2279z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2280z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2281z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2282z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2283z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2nodez12zd2envz12zzdataflow_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2284z00zzdataflow_walkz00,
				BGl_string2266z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2testzd2envzd2envzd2zzdataflow_walkz00,
				BGl_appz00zzast_nodez00, BGl_proc2285z00zzdataflow_walkz00,
				BGl_string2286z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2testzd2envzd2envzd2zzdataflow_walkz00,
				BGl_instanceofz00zzast_nodez00, BGl_proc2287z00zzdataflow_walkz00,
				BGl_string2286z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2testzd2envzd2envzd2zzdataflow_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2288z00zzdataflow_walkz00,
				BGl_string2286z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2testzd2envzd2envzd2zzdataflow_walkz00,
				BGl_varz00zzast_nodez00, BGl_proc2289z00zzdataflow_walkz00,
				BGl_string2286z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2testzd2envzd2envzd2zzdataflow_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2290z00zzdataflow_walkz00,
				BGl_string2286z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dataflowzd2testzd2falsezd2envzd2envz00zzdataflow_walkz00,
				BGl_appz00zzast_nodez00, BGl_proc2291z00zzdataflow_walkz00,
				BGl_string2292z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc2293z00zzdataflow_walkz00, BGl_string2294z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00, BGl_sequencez00zzast_nodez00,
				BGl_proc2295z00zzdataflow_walkz00, BGl_string2294z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc2296z00zzdataflow_walkz00, BGl_string2294z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc2297z00zzdataflow_walkz00, BGl_string2294z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc2298z00zzdataflow_walkz00, BGl_string2294z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2299z00zzdataflow_walkz00,
				BGl_string2294z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2300z00zzdataflow_walkz00, BGl_string2294z00zzdataflow_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00, BGl_funcallz00zzast_nodez00,
				BGl_proc2301z00zzdataflow_walkz00, BGl_string2294z00zzdataflow_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_abortzf3zd2envz21zzdataflow_walkz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2302z00zzdataflow_walkz00, BGl_string2294z00zzdataflow_walkz00);
		}

	}



/* &abort?-app-ly1460 */
	obj_t BGl_z62abortzf3zd2appzd2ly1460z91zzdataflow_walkz00(obj_t
		BgL_envz00_4023, obj_t BgL_nodez00_4024)
	{
		{	/* Dataflow/walk.scm 491 */
			{	/* Dataflow/walk.scm 493 */
				obj_t BgL__ortest_1191z00_4169;

				BgL__ortest_1191z00_4169 =
					BGl_abortzf3zf3zzdataflow_walkz00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_4024)))->BgL_funz00));
				if (CBOOL(BgL__ortest_1191z00_4169))
					{	/* Dataflow/walk.scm 493 */
						return BgL__ortest_1191z00_4169;
					}
				else
					{	/* Dataflow/walk.scm 493 */
						return
							BGl_abortzf3zf3zzdataflow_walkz00(
							(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_4024)))->BgL_argz00));
					}
			}
		}

	}



/* &abort?-funcall1458 */
	obj_t BGl_z62abortzf3zd2funcall1458z43zzdataflow_walkz00(obj_t
		BgL_envz00_4025, obj_t BgL_nodez00_4026)
	{
		{	/* Dataflow/walk.scm 484 */
			{	/* Dataflow/walk.scm 486 */
				obj_t BgL__ortest_1189z00_4171;

				BgL__ortest_1189z00_4171 =
					BGl_abortzf3zf3zzdataflow_walkz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_4026)))->BgL_funz00));
				if (CBOOL(BgL__ortest_1189z00_4171))
					{	/* Dataflow/walk.scm 486 */
						return BgL__ortest_1189z00_4171;
					}
				else
					{	/* Dataflow/walk.scm 486 */
						obj_t BgL_g1371z00_4172;

						BgL_g1371z00_4172 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_4026)))->BgL_argsz00);
						{
							obj_t BgL_l1369z00_4174;

							BgL_l1369z00_4174 = BgL_g1371z00_4172;
						BgL_zc3z04anonymousza32121ze3z87_4173:
							if (NULLP(BgL_l1369z00_4174))
								{	/* Dataflow/walk.scm 486 */
									return BFALSE;
								}
							else
								{	/* Dataflow/walk.scm 486 */
									obj_t BgL__ortest_1372z00_4175;

									{	/* Dataflow/walk.scm 486 */
										obj_t BgL_arg2124z00_4176;

										BgL_arg2124z00_4176 = CAR(((obj_t) BgL_l1369z00_4174));
										BgL__ortest_1372z00_4175 =
											BGl_abortzf3zf3zzdataflow_walkz00(
											((BgL_nodez00_bglt) BgL_arg2124z00_4176));
									}
									if (CBOOL(BgL__ortest_1372z00_4175))
										{	/* Dataflow/walk.scm 486 */
											return BgL__ortest_1372z00_4175;
										}
									else
										{	/* Dataflow/walk.scm 486 */
											obj_t BgL_arg2123z00_4177;

											BgL_arg2123z00_4177 = CDR(((obj_t) BgL_l1369z00_4174));
											{
												obj_t BgL_l1369z00_5187;

												BgL_l1369z00_5187 = BgL_arg2123z00_4177;
												BgL_l1369z00_4174 = BgL_l1369z00_5187;
												goto BgL_zc3z04anonymousza32121ze3z87_4173;
											}
										}
								}
						}
					}
			}
		}

	}



/* &abort?-app1456 */
	obj_t BGl_z62abortzf3zd2app1456z43zzdataflow_walkz00(obj_t BgL_envz00_4027,
		obj_t BgL_nodez00_4028)
	{
		{	/* Dataflow/walk.scm 477 */
			{	/* Dataflow/walk.scm 479 */
				obj_t BgL__ortest_1187z00_4179;

				{	/* Dataflow/walk.scm 479 */
					BgL_varz00_bglt BgL_arg2120z00_4180;

					BgL_arg2120z00_4180 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_4028)))->BgL_funz00);
					BgL__ortest_1187z00_4179 =
						BGl_abortzf3zf3zzdataflow_walkz00(
						((BgL_nodez00_bglt) BgL_arg2120z00_4180));
				}
				if (CBOOL(BgL__ortest_1187z00_4179))
					{	/* Dataflow/walk.scm 479 */
						return BgL__ortest_1187z00_4179;
					}
				else
					{	/* Dataflow/walk.scm 479 */
						obj_t BgL_g1367z00_4181;

						BgL_g1367z00_4181 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_4028)))->BgL_argsz00);
						{
							obj_t BgL_l1365z00_4183;

							BgL_l1365z00_4183 = BgL_g1367z00_4181;
						BgL_zc3z04anonymousza32116ze3z87_4182:
							if (NULLP(BgL_l1365z00_4183))
								{	/* Dataflow/walk.scm 479 */
									return BFALSE;
								}
							else
								{	/* Dataflow/walk.scm 479 */
									obj_t BgL__ortest_1368z00_4184;

									{	/* Dataflow/walk.scm 479 */
										obj_t BgL_arg2119z00_4185;

										BgL_arg2119z00_4185 = CAR(((obj_t) BgL_l1365z00_4183));
										BgL__ortest_1368z00_4184 =
											BGl_abortzf3zf3zzdataflow_walkz00(
											((BgL_nodez00_bglt) BgL_arg2119z00_4185));
									}
									if (CBOOL(BgL__ortest_1368z00_4184))
										{	/* Dataflow/walk.scm 479 */
											return BgL__ortest_1368z00_4184;
										}
									else
										{	/* Dataflow/walk.scm 479 */
											obj_t BgL_arg2118z00_4186;

											BgL_arg2118z00_4186 = CDR(((obj_t) BgL_l1365z00_4183));
											{
												obj_t BgL_l1365z00_5206;

												BgL_l1365z00_5206 = BgL_arg2118z00_4186;
												BgL_l1365z00_4183 = BgL_l1365z00_5206;
												goto BgL_zc3z04anonymousza32116ze3z87_4182;
											}
										}
								}
						}
					}
			}
		}

	}



/* &abort?-conditional1454 */
	obj_t BGl_z62abortzf3zd2conditional1454z43zzdataflow_walkz00(obj_t
		BgL_envz00_4029, obj_t BgL_nodez00_4030)
	{
		{	/* Dataflow/walk.scm 470 */
			{	/* Dataflow/walk.scm 472 */
				obj_t BgL__ortest_1184z00_4188;

				BgL__ortest_1184z00_4188 =
					BGl_abortzf3zf3zzdataflow_walkz00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_4030)))->BgL_testz00));
				if (CBOOL(BgL__ortest_1184z00_4188))
					{	/* Dataflow/walk.scm 472 */
						return BgL__ortest_1184z00_4188;
					}
				else
					{	/* Dataflow/walk.scm 472 */
						obj_t BgL__andtest_1185z00_4189;

						BgL__andtest_1185z00_4189 =
							BGl_abortzf3zf3zzdataflow_walkz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_4030)))->
								BgL_truez00));
						if (CBOOL(BgL__andtest_1185z00_4189))
							{	/* Dataflow/walk.scm 472 */
								return
									BGl_abortzf3zf3zzdataflow_walkz00(
									(((BgL_conditionalz00_bglt) COBJECT(
												((BgL_conditionalz00_bglt) BgL_nodez00_4030)))->
										BgL_falsez00));
							}
						else
							{	/* Dataflow/walk.scm 472 */
								return BFALSE;
							}
					}
			}
		}

	}



/* &abort?-let-fun1452 */
	obj_t BGl_z62abortzf3zd2letzd2fun1452z91zzdataflow_walkz00(obj_t
		BgL_envz00_4031, obj_t BgL_nodez00_4032)
	{
		{	/* Dataflow/walk.scm 463 */
			return
				BGl_abortzf3zf3zzdataflow_walkz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4032)))->BgL_bodyz00));
		}

	}



/* &abort?-let-var1450 */
	obj_t BGl_z62abortzf3zd2letzd2var1450z91zzdataflow_walkz00(obj_t
		BgL_envz00_4033, obj_t BgL_nodez00_4034)
	{
		{	/* Dataflow/walk.scm 456 */
			{	/* Dataflow/walk.scm 458 */
				obj_t BgL__ortest_1181z00_4192;

				{	/* Dataflow/walk.scm 458 */
					obj_t BgL_g1363z00_4193;

					BgL_g1363z00_4193 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4034)))->BgL_bindingsz00);
					{
						obj_t BgL_l1361z00_4195;

						BgL_l1361z00_4195 = BgL_g1363z00_4193;
					BgL_zc3z04anonymousza32108ze3z87_4194:
						if (NULLP(BgL_l1361z00_4195))
							{	/* Dataflow/walk.scm 458 */
								BgL__ortest_1181z00_4192 = BFALSE;
							}
						else
							{	/* Dataflow/walk.scm 458 */
								obj_t BgL__ortest_1364z00_4196;

								{	/* Dataflow/walk.scm 458 */
									obj_t BgL_bz00_4197;

									BgL_bz00_4197 = CAR(((obj_t) BgL_l1361z00_4195));
									{	/* Dataflow/walk.scm 458 */
										obj_t BgL_arg2111z00_4198;

										BgL_arg2111z00_4198 = CDR(((obj_t) BgL_bz00_4197));
										BgL__ortest_1364z00_4196 =
											BGl_abortzf3zf3zzdataflow_walkz00(
											((BgL_nodez00_bglt) BgL_arg2111z00_4198));
									}
								}
								if (CBOOL(BgL__ortest_1364z00_4196))
									{	/* Dataflow/walk.scm 458 */
										BgL__ortest_1181z00_4192 = BgL__ortest_1364z00_4196;
									}
								else
									{	/* Dataflow/walk.scm 458 */
										obj_t BgL_arg2110z00_4199;

										BgL_arg2110z00_4199 = CDR(((obj_t) BgL_l1361z00_4195));
										{
											obj_t BgL_l1361z00_5237;

											BgL_l1361z00_5237 = BgL_arg2110z00_4199;
											BgL_l1361z00_4195 = BgL_l1361z00_5237;
											goto BgL_zc3z04anonymousza32108ze3z87_4194;
										}
									}
							}
					}
				}
				if (CBOOL(BgL__ortest_1181z00_4192))
					{	/* Dataflow/walk.scm 458 */
						return BgL__ortest_1181z00_4192;
					}
				else
					{	/* Dataflow/walk.scm 458 */
						return
							BGl_abortzf3zf3zzdataflow_walkz00(
							(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_4034)))->BgL_bodyz00));
					}
			}
		}

	}



/* &abort?-sync1448 */
	obj_t BGl_z62abortzf3zd2sync1448z43zzdataflow_walkz00(obj_t BgL_envz00_4035,
		obj_t BgL_nodez00_4036)
	{
		{	/* Dataflow/walk.scm 448 */
			{	/* Dataflow/walk.scm 449 */
				obj_t BgL__ortest_1178z00_4201;

				BgL__ortest_1178z00_4201 =
					BGl_abortzf3zf3zzdataflow_walkz00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_4036)))->BgL_mutexz00));
				if (CBOOL(BgL__ortest_1178z00_4201))
					{	/* Dataflow/walk.scm 449 */
						return BgL__ortest_1178z00_4201;
					}
				else
					{	/* Dataflow/walk.scm 450 */
						obj_t BgL__ortest_1179z00_4202;

						BgL__ortest_1179z00_4202 =
							BGl_abortzf3zf3zzdataflow_walkz00(
							(((BgL_syncz00_bglt) COBJECT(
										((BgL_syncz00_bglt) BgL_nodez00_4036)))->BgL_prelockz00));
						if (CBOOL(BgL__ortest_1179z00_4202))
							{	/* Dataflow/walk.scm 450 */
								return BgL__ortest_1179z00_4202;
							}
						else
							{	/* Dataflow/walk.scm 450 */
								return
									BGl_abortzf3zf3zzdataflow_walkz00(
									(((BgL_syncz00_bglt) COBJECT(
												((BgL_syncz00_bglt) BgL_nodez00_4036)))->BgL_bodyz00));
							}
					}
			}
		}

	}



/* &abort?-sequence1446 */
	obj_t BGl_z62abortzf3zd2sequence1446z43zzdataflow_walkz00(obj_t
		BgL_envz00_4037, obj_t BgL_nodez00_4038)
	{
		{	/* Dataflow/walk.scm 442 */
			{	/* Dataflow/walk.scm 443 */
				obj_t BgL_g1359z00_4204;

				BgL_g1359z00_4204 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_4038)))->BgL_nodesz00);
				{
					obj_t BgL_l1357z00_4206;

					BgL_l1357z00_4206 = BgL_g1359z00_4204;
				BgL_zc3z04anonymousza32100ze3z87_4205:
					if (NULLP(BgL_l1357z00_4206))
						{	/* Dataflow/walk.scm 443 */
							return BFALSE;
						}
					else
						{	/* Dataflow/walk.scm 443 */
							obj_t BgL__ortest_1360z00_4207;

							{	/* Dataflow/walk.scm 443 */
								obj_t BgL_arg2103z00_4208;

								BgL_arg2103z00_4208 = CAR(((obj_t) BgL_l1357z00_4206));
								BgL__ortest_1360z00_4207 =
									BGl_abortzf3zf3zzdataflow_walkz00(
									((BgL_nodez00_bglt) BgL_arg2103z00_4208));
							}
							if (CBOOL(BgL__ortest_1360z00_4207))
								{	/* Dataflow/walk.scm 443 */
									return BgL__ortest_1360z00_4207;
								}
							else
								{	/* Dataflow/walk.scm 443 */
									obj_t BgL_arg2102z00_4209;

									BgL_arg2102z00_4209 = CDR(((obj_t) BgL_l1357z00_4206));
									{
										obj_t BgL_l1357z00_5268;

										BgL_l1357z00_5268 = BgL_arg2102z00_4209;
										BgL_l1357z00_4206 = BgL_l1357z00_5268;
										goto BgL_zc3z04anonymousza32100ze3z87_4205;
									}
								}
						}
				}
			}
		}

	}



/* &abort?-fail1444 */
	obj_t BGl_z62abortzf3zd2fail1444z43zzdataflow_walkz00(obj_t BgL_envz00_4039,
		obj_t BgL_nodez00_4040)
	{
		{	/* Dataflow/walk.scm 436 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &dataflow-test-false-1440 */
	obj_t BGl_z62dataflowzd2testzd2falsezd21440zb0zzdataflow_walkz00(obj_t
		BgL_envz00_4041, obj_t BgL_nodez00_4042)
	{
		{	/* Dataflow/walk.scm 408 */
			{	/* Dataflow/walk.scm 410 */
				BgL_variablez00_bglt BgL_fz00_4212;

				BgL_fz00_4212 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_4042)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Dataflow/walk.scm 411 */

					{	/* Dataflow/walk.scm 412 */
						bool_t BgL_test2410z00_5273;

						{	/* Dataflow/walk.scm 412 */
							bool_t BgL_test2411z00_5274;

							{	/* Dataflow/walk.scm 412 */
								obj_t BgL_tmpz00_5275;

								BgL_tmpz00_5275 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_4042)))->BgL_argsz00);
								BgL_test2411z00_5274 = PAIRP(BgL_tmpz00_5275);
							}
							if (BgL_test2411z00_5274)
								{	/* Dataflow/walk.scm 412 */
									if (NULLP(CDR(
												(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_4042)))->
													BgL_argsz00))))
										{	/* Dataflow/walk.scm 413 */
											bool_t BgL_test2413z00_5284;

											{	/* Dataflow/walk.scm 413 */
												obj_t BgL_arg2094z00_4213;

												BgL_arg2094z00_4213 =
													CAR(
													(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_4042)))->
														BgL_argsz00));
												{	/* Dataflow/walk.scm 413 */
													obj_t BgL_classz00_4214;

													BgL_classz00_4214 = BGl_varz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_arg2094z00_4213))
														{	/* Dataflow/walk.scm 413 */
															BgL_objectz00_bglt BgL_arg1807z00_4215;

															BgL_arg1807z00_4215 =
																(BgL_objectz00_bglt) (BgL_arg2094z00_4213);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Dataflow/walk.scm 413 */
																	long BgL_idxz00_4216;

																	BgL_idxz00_4216 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4215);
																	BgL_test2413z00_5284 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4216 + 2L)) ==
																		BgL_classz00_4214);
																}
															else
																{	/* Dataflow/walk.scm 413 */
																	bool_t BgL_res2233z00_4219;

																	{	/* Dataflow/walk.scm 413 */
																		obj_t BgL_oclassz00_4220;

																		{	/* Dataflow/walk.scm 413 */
																			obj_t BgL_arg1815z00_4221;
																			long BgL_arg1816z00_4222;

																			BgL_arg1815z00_4221 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Dataflow/walk.scm 413 */
																				long BgL_arg1817z00_4223;

																				BgL_arg1817z00_4223 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4215);
																				BgL_arg1816z00_4222 =
																					(BgL_arg1817z00_4223 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4220 =
																				VECTOR_REF(BgL_arg1815z00_4221,
																				BgL_arg1816z00_4222);
																		}
																		{	/* Dataflow/walk.scm 413 */
																			bool_t BgL__ortest_1115z00_4224;

																			BgL__ortest_1115z00_4224 =
																				(BgL_classz00_4214 ==
																				BgL_oclassz00_4220);
																			if (BgL__ortest_1115z00_4224)
																				{	/* Dataflow/walk.scm 413 */
																					BgL_res2233z00_4219 =
																						BgL__ortest_1115z00_4224;
																				}
																			else
																				{	/* Dataflow/walk.scm 413 */
																					long BgL_odepthz00_4225;

																					{	/* Dataflow/walk.scm 413 */
																						obj_t BgL_arg1804z00_4226;

																						BgL_arg1804z00_4226 =
																							(BgL_oclassz00_4220);
																						BgL_odepthz00_4225 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4226);
																					}
																					if ((2L < BgL_odepthz00_4225))
																						{	/* Dataflow/walk.scm 413 */
																							obj_t BgL_arg1802z00_4227;

																							{	/* Dataflow/walk.scm 413 */
																								obj_t BgL_arg1803z00_4228;

																								BgL_arg1803z00_4228 =
																									(BgL_oclassz00_4220);
																								BgL_arg1802z00_4227 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4228, 2L);
																							}
																							BgL_res2233z00_4219 =
																								(BgL_arg1802z00_4227 ==
																								BgL_classz00_4214);
																						}
																					else
																						{	/* Dataflow/walk.scm 413 */
																							BgL_res2233z00_4219 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2413z00_5284 = BgL_res2233z00_4219;
																}
														}
													else
														{	/* Dataflow/walk.scm 413 */
															BgL_test2413z00_5284 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2413z00_5284)
												{	/* Dataflow/walk.scm 414 */
													BgL_typez00_bglt BgL_arg2089z00_4229;

													{
														BgL_variablez00_bglt BgL_auxz00_5310;

														{
															BgL_varz00_bglt BgL_auxz00_5311;

															{	/* Dataflow/walk.scm 414 */
																obj_t BgL_pairz00_4230;

																BgL_pairz00_4230 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_nodez00_4042)))->
																	BgL_argsz00);
																BgL_auxz00_5311 =
																	((BgL_varz00_bglt) CAR(BgL_pairz00_4230));
															}
															BgL_auxz00_5310 =
																(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5311))->
																BgL_variablez00);
														}
														BgL_arg2089z00_4229 =
															(((BgL_variablez00_bglt)
																COBJECT(BgL_auxz00_5310))->BgL_typez00);
													}
													BgL_test2410z00_5273 =
														(
														((obj_t) BgL_arg2089z00_4229) ==
														BGl_za2pairzd2nilza2zd2zztype_cachez00);
												}
											else
												{	/* Dataflow/walk.scm 413 */
													BgL_test2410z00_5273 = ((bool_t) 0);
												}
										}
									else
										{	/* Dataflow/walk.scm 412 */
											BgL_test2410z00_5273 = ((bool_t) 0);
										}
								}
							else
								{	/* Dataflow/walk.scm 412 */
									BgL_test2410z00_5273 = ((bool_t) 0);
								}
						}
						if (BgL_test2410z00_5273)
							{	/* Dataflow/walk.scm 412 */
								if (
									(((obj_t) BgL_fz00_4212) ==
										BGl_za2z42nullzf3za2zb1zzdataflow_walkz00))
									{	/* Dataflow/walk.scm 417 */
										obj_t BgL_arg2078z00_4231;

										{	/* Dataflow/walk.scm 417 */
											BgL_variablez00_bglt BgL_arg2080z00_4232;

											{
												BgL_varz00_bglt BgL_auxz00_5323;

												{	/* Dataflow/walk.scm 417 */
													obj_t BgL_pairz00_4233;

													BgL_pairz00_4233 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_4042)))->
														BgL_argsz00);
													BgL_auxz00_5323 =
														((BgL_varz00_bglt) CAR(BgL_pairz00_4233));
												}
												BgL_arg2080z00_4232 =
													(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5323))->
													BgL_variablez00);
											}
											BgL_arg2078z00_4231 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg2080z00_4232),
												BGl_za2pairza2z00zztype_cachez00);
										}
										{	/* Dataflow/walk.scm 417 */
											obj_t BgL_list2079z00_4234;

											BgL_list2079z00_4234 =
												MAKE_YOUNG_PAIR(BgL_arg2078z00_4231, BNIL);
											return BgL_list2079z00_4234;
										}
									}
								else
									{	/* Dataflow/walk.scm 418 */
										bool_t BgL_test2419z00_5332;

										if (
											(((obj_t) BgL_fz00_4212) ==
												BGl_za2z42pairzf3za2zb1zzdataflow_walkz00))
											{	/* Dataflow/walk.scm 418 */
												BgL_test2419z00_5332 = ((bool_t) 1);
											}
										else
											{	/* Dataflow/walk.scm 418 */
												BgL_test2419z00_5332 =
													(
													((obj_t) BgL_fz00_4212) ==
													BGl_za2z42epairzf3za2zb1zzdataflow_walkz00);
											}
										if (BgL_test2419z00_5332)
											{	/* Dataflow/walk.scm 419 */
												obj_t BgL_arg2084z00_4235;

												{	/* Dataflow/walk.scm 419 */
													BgL_variablez00_bglt BgL_arg2086z00_4236;

													{
														BgL_varz00_bglt BgL_auxz00_5338;

														{	/* Dataflow/walk.scm 419 */
															obj_t BgL_pairz00_4237;

															BgL_pairz00_4237 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_nodez00_4042)))->
																BgL_argsz00);
															BgL_auxz00_5338 =
																((BgL_varz00_bglt) CAR(BgL_pairz00_4237));
														}
														BgL_arg2086z00_4236 =
															(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5338))->
															BgL_variablez00);
													}
													BgL_arg2084z00_4235 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg2086z00_4236),
														BGl_za2bnilza2z00zztype_cachez00);
												}
												{	/* Dataflow/walk.scm 419 */
													obj_t BgL_list2085z00_4238;

													BgL_list2085z00_4238 =
														MAKE_YOUNG_PAIR(BgL_arg2084z00_4235, BNIL);
													return BgL_list2085z00_4238;
												}
											}
										else
											{	/* Dataflow/walk.scm 418 */
												return BNIL;
											}
									}
							}
						else
							{	/* Dataflow/walk.scm 412 */
								return BNIL;
							}
					}
				}
			}
		}

	}



/* &dataflow-test-env-le1432 */
	obj_t BGl_z62dataflowzd2testzd2envzd2le1432zb0zzdataflow_walkz00(obj_t
		BgL_envz00_4043, obj_t BgL_nodez00_4044)
	{
		{	/* Dataflow/walk.scm 380 */
			{	/* Dataflow/walk.scm 382 */
				bool_t BgL_test2421z00_5347;

				{	/* Dataflow/walk.scm 382 */
					bool_t BgL_test2422z00_5348;

					{	/* Dataflow/walk.scm 382 */
						obj_t BgL_tmpz00_5349;

						BgL_tmpz00_5349 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_4044)))->
							BgL_bindingsz00);
						BgL_test2422z00_5348 = PAIRP(BgL_tmpz00_5349);
					}
					if (BgL_test2422z00_5348)
						{	/* Dataflow/walk.scm 382 */
							BgL_test2421z00_5347 =
								NULLP(CDR(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_4044)))->
										BgL_bindingsz00)));
						}
					else
						{	/* Dataflow/walk.scm 382 */
							BgL_test2421z00_5347 = ((bool_t) 0);
						}
				}
				if (BgL_test2421z00_5347)
					{	/* Dataflow/walk.scm 384 */
						bool_t BgL_test2423z00_5357;

						{	/* Dataflow/walk.scm 384 */
							obj_t BgL_arg2050z00_4240;

							{	/* Dataflow/walk.scm 384 */
								obj_t BgL_pairz00_4241;

								BgL_pairz00_4241 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_4044)))->
									BgL_bindingsz00);
								BgL_arg2050z00_4240 = CDR(CAR(BgL_pairz00_4241));
							}
							{	/* Dataflow/walk.scm 384 */
								obj_t BgL_classz00_4242;

								BgL_classz00_4242 = BGl_varz00zzast_nodez00;
								if (BGL_OBJECTP(BgL_arg2050z00_4240))
									{	/* Dataflow/walk.scm 384 */
										BgL_objectz00_bglt BgL_arg1807z00_4243;

										BgL_arg1807z00_4243 =
											(BgL_objectz00_bglt) (BgL_arg2050z00_4240);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Dataflow/walk.scm 384 */
												long BgL_idxz00_4244;

												BgL_idxz00_4244 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4243);
												BgL_test2423z00_5357 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4244 + 2L)) == BgL_classz00_4242);
											}
										else
											{	/* Dataflow/walk.scm 384 */
												bool_t BgL_res2230z00_4247;

												{	/* Dataflow/walk.scm 384 */
													obj_t BgL_oclassz00_4248;

													{	/* Dataflow/walk.scm 384 */
														obj_t BgL_arg1815z00_4249;
														long BgL_arg1816z00_4250;

														BgL_arg1815z00_4249 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Dataflow/walk.scm 384 */
															long BgL_arg1817z00_4251;

															BgL_arg1817z00_4251 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4243);
															BgL_arg1816z00_4250 =
																(BgL_arg1817z00_4251 - OBJECT_TYPE);
														}
														BgL_oclassz00_4248 =
															VECTOR_REF(BgL_arg1815z00_4249,
															BgL_arg1816z00_4250);
													}
													{	/* Dataflow/walk.scm 384 */
														bool_t BgL__ortest_1115z00_4252;

														BgL__ortest_1115z00_4252 =
															(BgL_classz00_4242 == BgL_oclassz00_4248);
														if (BgL__ortest_1115z00_4252)
															{	/* Dataflow/walk.scm 384 */
																BgL_res2230z00_4247 = BgL__ortest_1115z00_4252;
															}
														else
															{	/* Dataflow/walk.scm 384 */
																long BgL_odepthz00_4253;

																{	/* Dataflow/walk.scm 384 */
																	obj_t BgL_arg1804z00_4254;

																	BgL_arg1804z00_4254 = (BgL_oclassz00_4248);
																	BgL_odepthz00_4253 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4254);
																}
																if ((2L < BgL_odepthz00_4253))
																	{	/* Dataflow/walk.scm 384 */
																		obj_t BgL_arg1802z00_4255;

																		{	/* Dataflow/walk.scm 384 */
																			obj_t BgL_arg1803z00_4256;

																			BgL_arg1803z00_4256 =
																				(BgL_oclassz00_4248);
																			BgL_arg1802z00_4255 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4256, 2L);
																		}
																		BgL_res2230z00_4247 =
																			(BgL_arg1802z00_4255 ==
																			BgL_classz00_4242);
																	}
																else
																	{	/* Dataflow/walk.scm 384 */
																		BgL_res2230z00_4247 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2423z00_5357 = BgL_res2230z00_4247;
											}
									}
								else
									{	/* Dataflow/walk.scm 384 */
										BgL_test2423z00_5357 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2423z00_5357)
							{	/* Dataflow/walk.scm 385 */
								obj_t BgL_envz00_4257;

								BgL_envz00_4257 =
									BGl_dataflowzd2testzd2envz00zzdataflow_walkz00(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_nodez00_4044)))->
										BgL_bodyz00));
								{	/* Dataflow/walk.scm 386 */
									bool_t BgL_test2428z00_5387;

									if (NULLP(BgL_envz00_4257))
										{	/* Dataflow/walk.scm 386 */
											BgL_test2428z00_5387 = ((bool_t) 0);
										}
									else
										{	/* Dataflow/walk.scm 386 */
											if (NULLP(CDR(BgL_envz00_4257)))
												{	/* Dataflow/walk.scm 388 */
													obj_t BgL_tmpz00_5393;

													{	/* Dataflow/walk.scm 388 */
														obj_t BgL_pairz00_4258;

														BgL_pairz00_4258 =
															(((BgL_letzd2varzd2_bglt) COBJECT(
																	((BgL_letzd2varzd2_bglt) BgL_nodez00_4044)))->
															BgL_bindingsz00);
														BgL_tmpz00_5393 = CAR(CAR(BgL_pairz00_4258));
													}
													BgL_test2428z00_5387 =
														(CAR(CAR(BgL_envz00_4257)) == BgL_tmpz00_5393);
												}
											else
												{	/* Dataflow/walk.scm 387 */
													BgL_test2428z00_5387 = ((bool_t) 0);
												}
										}
									if (BgL_test2428z00_5387)
										{	/* Dataflow/walk.scm 389 */
											obj_t BgL_arg2008z00_4259;

											{	/* Dataflow/walk.scm 389 */
												BgL_variablez00_bglt BgL_arg2010z00_4260;
												obj_t BgL_arg2011z00_4261;

												{
													BgL_varz00_bglt BgL_auxz00_5401;

													{
														obj_t BgL_auxz00_5402;

														{	/* Dataflow/walk.scm 389 */
															obj_t BgL_pairz00_4262;

															BgL_pairz00_4262 =
																(((BgL_letzd2varzd2_bglt) COBJECT(
																		((BgL_letzd2varzd2_bglt)
																			BgL_nodez00_4044)))->BgL_bindingsz00);
															{	/* Dataflow/walk.scm 389 */
																obj_t BgL_pairz00_4263;

																BgL_pairz00_4263 = CAR(BgL_pairz00_4262);
																BgL_auxz00_5402 = CDR(BgL_pairz00_4263);
															}
														}
														BgL_auxz00_5401 =
															((BgL_varz00_bglt) BgL_auxz00_5402);
													}
													BgL_arg2010z00_4260 =
														(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5401))->
														BgL_variablez00);
												}
												BgL_arg2011z00_4261 = CDR(CAR(BgL_envz00_4257));
												BgL_arg2008z00_4259 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_arg2010z00_4260), BgL_arg2011z00_4261);
											}
											{	/* Dataflow/walk.scm 389 */
												obj_t BgL_list2009z00_4264;

												BgL_list2009z00_4264 =
													MAKE_YOUNG_PAIR(BgL_arg2008z00_4259, BNIL);
												return BgL_list2009z00_4264;
											}
										}
									else
										{	/* Dataflow/walk.scm 386 */
											return BNIL;
										}
								}
							}
						else
							{	/* Dataflow/walk.scm 391 */
								bool_t BgL_test2431z00_5414;

								{	/* Dataflow/walk.scm 391 */
									bool_t BgL_test2432z00_5415;

									{	/* Dataflow/walk.scm 391 */
										BgL_nodez00_bglt BgL_arg2049z00_4265;

										BgL_arg2049z00_4265 =
											(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_nodez00_4044)))->
											BgL_bodyz00);
										{	/* Dataflow/walk.scm 391 */
											obj_t BgL_classz00_4266;

											BgL_classz00_4266 = BGl_varz00zzast_nodez00;
											{	/* Dataflow/walk.scm 391 */
												BgL_objectz00_bglt BgL_arg1807z00_4267;

												{	/* Dataflow/walk.scm 391 */
													obj_t BgL_tmpz00_5418;

													BgL_tmpz00_5418 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg2049z00_4265));
													BgL_arg1807z00_4267 =
														(BgL_objectz00_bglt) (BgL_tmpz00_5418);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Dataflow/walk.scm 391 */
														long BgL_idxz00_4268;

														BgL_idxz00_4268 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4267);
														BgL_test2432z00_5415 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_4268 + 2L)) == BgL_classz00_4266);
													}
												else
													{	/* Dataflow/walk.scm 391 */
														bool_t BgL_res2232z00_4271;

														{	/* Dataflow/walk.scm 391 */
															obj_t BgL_oclassz00_4272;

															{	/* Dataflow/walk.scm 391 */
																obj_t BgL_arg1815z00_4273;
																long BgL_arg1816z00_4274;

																BgL_arg1815z00_4273 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Dataflow/walk.scm 391 */
																	long BgL_arg1817z00_4275;

																	BgL_arg1817z00_4275 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4267);
																	BgL_arg1816z00_4274 =
																		(BgL_arg1817z00_4275 - OBJECT_TYPE);
																}
																BgL_oclassz00_4272 =
																	VECTOR_REF(BgL_arg1815z00_4273,
																	BgL_arg1816z00_4274);
															}
															{	/* Dataflow/walk.scm 391 */
																bool_t BgL__ortest_1115z00_4276;

																BgL__ortest_1115z00_4276 =
																	(BgL_classz00_4266 == BgL_oclassz00_4272);
																if (BgL__ortest_1115z00_4276)
																	{	/* Dataflow/walk.scm 391 */
																		BgL_res2232z00_4271 =
																			BgL__ortest_1115z00_4276;
																	}
																else
																	{	/* Dataflow/walk.scm 391 */
																		long BgL_odepthz00_4277;

																		{	/* Dataflow/walk.scm 391 */
																			obj_t BgL_arg1804z00_4278;

																			BgL_arg1804z00_4278 =
																				(BgL_oclassz00_4272);
																			BgL_odepthz00_4277 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_4278);
																		}
																		if ((2L < BgL_odepthz00_4277))
																			{	/* Dataflow/walk.scm 391 */
																				obj_t BgL_arg1802z00_4279;

																				{	/* Dataflow/walk.scm 391 */
																					obj_t BgL_arg1803z00_4280;

																					BgL_arg1803z00_4280 =
																						(BgL_oclassz00_4272);
																					BgL_arg1802z00_4279 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_4280, 2L);
																				}
																				BgL_res2232z00_4271 =
																					(BgL_arg1802z00_4279 ==
																					BgL_classz00_4266);
																			}
																		else
																			{	/* Dataflow/walk.scm 391 */
																				BgL_res2232z00_4271 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2432z00_5415 = BgL_res2232z00_4271;
													}
											}
										}
									}
									if (BgL_test2432z00_5415)
										{	/* Dataflow/walk.scm 392 */
											bool_t BgL_test2436z00_5441;

											{	/* Dataflow/walk.scm 392 */
												obj_t BgL_tmpz00_5442;

												{	/* Dataflow/walk.scm 392 */
													obj_t BgL_pairz00_4281;

													BgL_pairz00_4281 =
														(((BgL_letzd2varzd2_bglt) COBJECT(
																((BgL_letzd2varzd2_bglt) BgL_nodez00_4044)))->
														BgL_bindingsz00);
													BgL_tmpz00_5442 = CAR(CAR(BgL_pairz00_4281));
												}
												BgL_test2436z00_5441 =
													(
													((obj_t)
														(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt)
																		(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_nodez00_4044)))->
																			BgL_bodyz00))))->BgL_variablez00)) ==
													BgL_tmpz00_5442);
											}
											if (BgL_test2436z00_5441)
												{	/* Dataflow/walk.scm 393 */
													BgL_typez00_bglt BgL_arg2041z00_4282;

													{
														BgL_variablez00_bglt BgL_auxz00_5453;

														{
															obj_t BgL_auxz00_5454;

															{	/* Dataflow/walk.scm 393 */
																obj_t BgL_pairz00_4283;

																BgL_pairz00_4283 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_nodez00_4044)))->BgL_bindingsz00);
																{	/* Dataflow/walk.scm 393 */
																	obj_t BgL_pairz00_4284;

																	BgL_pairz00_4284 = CAR(BgL_pairz00_4283);
																	BgL_auxz00_5454 = CAR(BgL_pairz00_4284);
																}
															}
															BgL_auxz00_5453 =
																((BgL_variablez00_bglt) BgL_auxz00_5454);
														}
														BgL_arg2041z00_4282 =
															(((BgL_variablez00_bglt)
																COBJECT(BgL_auxz00_5453))->BgL_typez00);
													}
													BgL_test2431z00_5414 =
														(
														((obj_t) BgL_arg2041z00_4282) ==
														BGl_za2boolza2z00zztype_cachez00);
												}
											else
												{	/* Dataflow/walk.scm 392 */
													BgL_test2431z00_5414 = ((bool_t) 0);
												}
										}
									else
										{	/* Dataflow/walk.scm 391 */
											BgL_test2431z00_5414 = ((bool_t) 0);
										}
								}
								if (BgL_test2431z00_5414)
									{	/* Dataflow/walk.scm 394 */
										obj_t BgL_arg2039z00_4285;

										{	/* Dataflow/walk.scm 394 */
											obj_t BgL_pairz00_4286;

											BgL_pairz00_4286 =
												(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_nodez00_4044)))->
												BgL_bindingsz00);
											BgL_arg2039z00_4285 = CDR(CAR(BgL_pairz00_4286));
										}
										return
											BGl_dataflowzd2testzd2envz00zzdataflow_walkz00(
											((BgL_nodez00_bglt) BgL_arg2039z00_4285));
									}
								else
									{	/* Dataflow/walk.scm 391 */
										return BNIL;
									}
							}
					}
				else
					{	/* Dataflow/walk.scm 382 */
						return BNIL;
					}
			}
		}

	}



/* &dataflow-test-env-va1430 */
	obj_t BGl_z62dataflowzd2testzd2envzd2va1430zb0zzdataflow_walkz00(obj_t
		BgL_envz00_4045, obj_t BgL_nodez00_4046)
	{
		{	/* Dataflow/walk.scm 363 */
			{	/* Dataflow/walk.scm 365 */
				bool_t BgL_test2437z00_5469;

				{	/* Dataflow/walk.scm 365 */
					BgL_variablez00_bglt BgL_arg1989z00_4288;

					BgL_arg1989z00_4288 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_nodez00_4046)))->BgL_variablez00);
					{	/* Dataflow/walk.scm 365 */
						obj_t BgL_classz00_4289;

						BgL_classz00_4289 = BGl_localzf2valuezf2zzdataflow_walkz00;
						{	/* Dataflow/walk.scm 365 */
							obj_t BgL_oclassz00_4290;

							{	/* Dataflow/walk.scm 365 */
								obj_t BgL_arg1815z00_4291;
								long BgL_arg1816z00_4292;

								BgL_arg1815z00_4291 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Dataflow/walk.scm 365 */
									long BgL_arg1817z00_4293;

									BgL_arg1817z00_4293 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt) BgL_arg1989z00_4288));
									BgL_arg1816z00_4292 = (BgL_arg1817z00_4293 - OBJECT_TYPE);
								}
								BgL_oclassz00_4290 =
									VECTOR_REF(BgL_arg1815z00_4291, BgL_arg1816z00_4292);
							}
							BgL_test2437z00_5469 = (BgL_oclassz00_4290 == BgL_classz00_4289);
				}}}
				if (BgL_test2437z00_5469)
					{	/* Dataflow/walk.scm 366 */
						BgL_nodez00_bglt BgL_arg1987z00_4294;

						{	/* Dataflow/walk.scm 366 */
							BgL_localz00_bglt BgL_oz00_4295;

							BgL_oz00_4295 =
								((BgL_localz00_bglt)
								(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_4046)))->BgL_variablez00));
							{
								BgL_localzf2valuezf2_bglt BgL_auxz00_5481;

								{
									obj_t BgL_auxz00_5482;

									{	/* Dataflow/walk.scm 366 */
										BgL_objectz00_bglt BgL_tmpz00_5483;

										BgL_tmpz00_5483 = ((BgL_objectz00_bglt) BgL_oz00_4295);
										BgL_auxz00_5482 = BGL_OBJECT_WIDENING(BgL_tmpz00_5483);
									}
									BgL_auxz00_5481 =
										((BgL_localzf2valuezf2_bglt) BgL_auxz00_5482);
								}
								BgL_arg1987z00_4294 =
									(((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_5481))->
									BgL_nodez00);
							}
						}
						return
							BGl_dataflowzd2testzd2envz00zzdataflow_walkz00
							(BgL_arg1987z00_4294);
					}
				else
					{	/* Dataflow/walk.scm 365 */
						return BNIL;
					}
			}
		}

	}



/* &dataflow-test-env-co1428 */
	obj_t BGl_z62dataflowzd2testzd2envzd2co1428zb0zzdataflow_walkz00(obj_t
		BgL_envz00_4047, obj_t BgL_nodez00_4048)
	{
		{	/* Dataflow/walk.scm 354 */
			{	/* Dataflow/walk.scm 356 */
				bool_t BgL_test2438z00_5489;

				{	/* Dataflow/walk.scm 356 */
					bool_t BgL_test2439z00_5490;

					{	/* Dataflow/walk.scm 356 */
						BgL_nodez00_bglt BgL_arg1984z00_4297;

						BgL_arg1984z00_4297 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_4048)))->BgL_falsez00);
						{	/* Dataflow/walk.scm 356 */
							obj_t BgL_classz00_4298;

							BgL_classz00_4298 = BGl_atomz00zzast_nodez00;
							{	/* Dataflow/walk.scm 356 */
								BgL_objectz00_bglt BgL_arg1807z00_4299;

								{	/* Dataflow/walk.scm 356 */
									obj_t BgL_tmpz00_5493;

									BgL_tmpz00_5493 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1984z00_4297));
									BgL_arg1807z00_4299 = (BgL_objectz00_bglt) (BgL_tmpz00_5493);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Dataflow/walk.scm 356 */
										long BgL_idxz00_4300;

										BgL_idxz00_4300 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4299);
										BgL_test2439z00_5490 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4300 + 2L)) == BgL_classz00_4298);
									}
								else
									{	/* Dataflow/walk.scm 356 */
										bool_t BgL_res2229z00_4303;

										{	/* Dataflow/walk.scm 356 */
											obj_t BgL_oclassz00_4304;

											{	/* Dataflow/walk.scm 356 */
												obj_t BgL_arg1815z00_4305;
												long BgL_arg1816z00_4306;

												BgL_arg1815z00_4305 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Dataflow/walk.scm 356 */
													long BgL_arg1817z00_4307;

													BgL_arg1817z00_4307 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4299);
													BgL_arg1816z00_4306 =
														(BgL_arg1817z00_4307 - OBJECT_TYPE);
												}
												BgL_oclassz00_4304 =
													VECTOR_REF(BgL_arg1815z00_4305, BgL_arg1816z00_4306);
											}
											{	/* Dataflow/walk.scm 356 */
												bool_t BgL__ortest_1115z00_4308;

												BgL__ortest_1115z00_4308 =
													(BgL_classz00_4298 == BgL_oclassz00_4304);
												if (BgL__ortest_1115z00_4308)
													{	/* Dataflow/walk.scm 356 */
														BgL_res2229z00_4303 = BgL__ortest_1115z00_4308;
													}
												else
													{	/* Dataflow/walk.scm 356 */
														long BgL_odepthz00_4309;

														{	/* Dataflow/walk.scm 356 */
															obj_t BgL_arg1804z00_4310;

															BgL_arg1804z00_4310 = (BgL_oclassz00_4304);
															BgL_odepthz00_4309 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4310);
														}
														if ((2L < BgL_odepthz00_4309))
															{	/* Dataflow/walk.scm 356 */
																obj_t BgL_arg1802z00_4311;

																{	/* Dataflow/walk.scm 356 */
																	obj_t BgL_arg1803z00_4312;

																	BgL_arg1803z00_4312 = (BgL_oclassz00_4304);
																	BgL_arg1802z00_4311 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4312,
																		2L);
																}
																BgL_res2229z00_4303 =
																	(BgL_arg1802z00_4311 == BgL_classz00_4298);
															}
														else
															{	/* Dataflow/walk.scm 356 */
																BgL_res2229z00_4303 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2439z00_5490 = BgL_res2229z00_4303;
									}
							}
						}
					}
					if (BgL_test2439z00_5490)
						{	/* Dataflow/walk.scm 356 */
							BgL_test2438z00_5489 =
								(
								(((BgL_atomz00_bglt) COBJECT(
											((BgL_atomz00_bglt)
												(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt) BgL_nodez00_4048)))->
													BgL_falsez00))))->BgL_valuez00) == BFALSE);
						}
					else
						{	/* Dataflow/walk.scm 356 */
							BgL_test2438z00_5489 = ((bool_t) 0);
						}
				}
				if (BgL_test2438z00_5489)
					{	/* Dataflow/walk.scm 356 */
						return
							BGl_appendzd221011zd2zzdataflow_walkz00
							(BGl_dataflowzd2testzd2envz00zzdataflow_walkz00(((
										(BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
												BgL_nodez00_4048)))->BgL_testz00)),
							BGl_dataflowzd2testzd2envz00zzdataflow_walkz00(((
										(BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
												BgL_nodez00_4048)))->BgL_truez00)));
					}
				else
					{	/* Dataflow/walk.scm 356 */
						return BNIL;
					}
			}
		}

	}



/* &dataflow-test-env-in1426 */
	obj_t BGl_z62dataflowzd2testzd2envzd2in1426zb0zzdataflow_walkz00(obj_t
		BgL_envz00_4049, obj_t BgL_nodez00_4050)
	{
		{	/* Dataflow/walk.scm 345 */
			{	/* Dataflow/walk.scm 347 */
				bool_t BgL_test2443z00_5528;

				{	/* Dataflow/walk.scm 347 */
					obj_t BgL_arg1970z00_4314;

					{	/* Dataflow/walk.scm 347 */
						obj_t BgL_pairz00_4315;

						BgL_pairz00_4315 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_instanceofz00_bglt) BgL_nodez00_4050))))->
							BgL_exprza2za2);
						BgL_arg1970z00_4314 = CAR(BgL_pairz00_4315);
					}
					{	/* Dataflow/walk.scm 347 */
						obj_t BgL_classz00_4316;

						BgL_classz00_4316 = BGl_varz00zzast_nodez00;
						if (BGL_OBJECTP(BgL_arg1970z00_4314))
							{	/* Dataflow/walk.scm 347 */
								BgL_objectz00_bglt BgL_arg1807z00_4317;

								BgL_arg1807z00_4317 =
									(BgL_objectz00_bglt) (BgL_arg1970z00_4314);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Dataflow/walk.scm 347 */
										long BgL_idxz00_4318;

										BgL_idxz00_4318 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4317);
										BgL_test2443z00_5528 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4318 + 2L)) == BgL_classz00_4316);
									}
								else
									{	/* Dataflow/walk.scm 347 */
										bool_t BgL_res2227z00_4321;

										{	/* Dataflow/walk.scm 347 */
											obj_t BgL_oclassz00_4322;

											{	/* Dataflow/walk.scm 347 */
												obj_t BgL_arg1815z00_4323;
												long BgL_arg1816z00_4324;

												BgL_arg1815z00_4323 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Dataflow/walk.scm 347 */
													long BgL_arg1817z00_4325;

													BgL_arg1817z00_4325 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4317);
													BgL_arg1816z00_4324 =
														(BgL_arg1817z00_4325 - OBJECT_TYPE);
												}
												BgL_oclassz00_4322 =
													VECTOR_REF(BgL_arg1815z00_4323, BgL_arg1816z00_4324);
											}
											{	/* Dataflow/walk.scm 347 */
												bool_t BgL__ortest_1115z00_4326;

												BgL__ortest_1115z00_4326 =
													(BgL_classz00_4316 == BgL_oclassz00_4322);
												if (BgL__ortest_1115z00_4326)
													{	/* Dataflow/walk.scm 347 */
														BgL_res2227z00_4321 = BgL__ortest_1115z00_4326;
													}
												else
													{	/* Dataflow/walk.scm 347 */
														long BgL_odepthz00_4327;

														{	/* Dataflow/walk.scm 347 */
															obj_t BgL_arg1804z00_4328;

															BgL_arg1804z00_4328 = (BgL_oclassz00_4322);
															BgL_odepthz00_4327 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4328);
														}
														if ((2L < BgL_odepthz00_4327))
															{	/* Dataflow/walk.scm 347 */
																obj_t BgL_arg1802z00_4329;

																{	/* Dataflow/walk.scm 347 */
																	obj_t BgL_arg1803z00_4330;

																	BgL_arg1803z00_4330 = (BgL_oclassz00_4322);
																	BgL_arg1802z00_4329 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4330,
																		2L);
																}
																BgL_res2227z00_4321 =
																	(BgL_arg1802z00_4329 == BgL_classz00_4316);
															}
														else
															{	/* Dataflow/walk.scm 347 */
																BgL_res2227z00_4321 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2443z00_5528 = BgL_res2227z00_4321;
									}
							}
						else
							{	/* Dataflow/walk.scm 347 */
								BgL_test2443z00_5528 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2443z00_5528)
					{	/* Dataflow/walk.scm 348 */
						obj_t BgL_arg1964z00_4331;

						{	/* Dataflow/walk.scm 348 */
							BgL_variablez00_bglt BgL_arg1966z00_4332;
							BgL_typez00_bglt BgL_arg1967z00_4333;

							{
								BgL_varz00_bglt BgL_auxz00_5555;

								{	/* Dataflow/walk.scm 348 */
									obj_t BgL_pairz00_4334;

									BgL_pairz00_4334 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_instanceofz00_bglt) BgL_nodez00_4050))))->
										BgL_exprza2za2);
									BgL_auxz00_5555 = ((BgL_varz00_bglt) CAR(BgL_pairz00_4334));
								}
								BgL_arg1966z00_4332 =
									(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5555))->
									BgL_variablez00);
							}
							BgL_arg1967z00_4333 =
								(((BgL_instanceofz00_bglt) COBJECT(
										((BgL_instanceofz00_bglt) BgL_nodez00_4050)))->
								BgL_classz00);
							BgL_arg1964z00_4331 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1966z00_4332),
								((obj_t) BgL_arg1967z00_4333));
						}
						{	/* Dataflow/walk.scm 348 */
							obj_t BgL_list1965z00_4335;

							BgL_list1965z00_4335 = MAKE_YOUNG_PAIR(BgL_arg1964z00_4331, BNIL);
							return BgL_list1965z00_4335;
						}
					}
				else
					{	/* Dataflow/walk.scm 347 */
						return BNIL;
					}
			}
		}

	}



/* &dataflow-test-env-ap1423 */
	obj_t BGl_z62dataflowzd2testzd2envzd2ap1423zb0zzdataflow_walkz00(obj_t
		BgL_envz00_4051, obj_t BgL_nodez00_4052)
	{
		{	/* Dataflow/walk.scm 322 */
			{	/* Dataflow/walk.scm 324 */
				BgL_variablez00_bglt BgL_fz00_4337;

				BgL_fz00_4337 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_4052)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Dataflow/walk.scm 324 */
					BgL_valuez00_bglt BgL_funvz00_4338;

					BgL_funvz00_4338 =
						(((BgL_variablez00_bglt) COBJECT(BgL_fz00_4337))->BgL_valuez00);
					{	/* Dataflow/walk.scm 325 */

						{	/* Dataflow/walk.scm 327 */
							obj_t BgL_g1171z00_4339;

							BgL_g1171z00_4339 =
								BGl_isazd2ofzd2zztype_miscz00(
								((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4052)));
							if (CBOOL(BgL_g1171z00_4339))
								{	/* Dataflow/walk.scm 330 */
									bool_t BgL_test2449z00_5577;

									{	/* Dataflow/walk.scm 330 */
										obj_t BgL_arg1923z00_4340;

										BgL_arg1923z00_4340 =
											CAR(
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_4052)))->
												BgL_argsz00));
										{	/* Dataflow/walk.scm 330 */
											obj_t BgL_classz00_4341;

											BgL_classz00_4341 = BGl_varz00zzast_nodez00;
											if (BGL_OBJECTP(BgL_arg1923z00_4340))
												{	/* Dataflow/walk.scm 330 */
													BgL_objectz00_bglt BgL_arg1807z00_4342;

													BgL_arg1807z00_4342 =
														(BgL_objectz00_bglt) (BgL_arg1923z00_4340);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Dataflow/walk.scm 330 */
															long BgL_idxz00_4343;

															BgL_idxz00_4343 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4342);
															BgL_test2449z00_5577 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4343 + 2L)) == BgL_classz00_4341);
														}
													else
														{	/* Dataflow/walk.scm 330 */
															bool_t BgL_res2222z00_4346;

															{	/* Dataflow/walk.scm 330 */
																obj_t BgL_oclassz00_4347;

																{	/* Dataflow/walk.scm 330 */
																	obj_t BgL_arg1815z00_4348;
																	long BgL_arg1816z00_4349;

																	BgL_arg1815z00_4348 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Dataflow/walk.scm 330 */
																		long BgL_arg1817z00_4350;

																		BgL_arg1817z00_4350 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4342);
																		BgL_arg1816z00_4349 =
																			(BgL_arg1817z00_4350 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4347 =
																		VECTOR_REF(BgL_arg1815z00_4348,
																		BgL_arg1816z00_4349);
																}
																{	/* Dataflow/walk.scm 330 */
																	bool_t BgL__ortest_1115z00_4351;

																	BgL__ortest_1115z00_4351 =
																		(BgL_classz00_4341 == BgL_oclassz00_4347);
																	if (BgL__ortest_1115z00_4351)
																		{	/* Dataflow/walk.scm 330 */
																			BgL_res2222z00_4346 =
																				BgL__ortest_1115z00_4351;
																		}
																	else
																		{	/* Dataflow/walk.scm 330 */
																			long BgL_odepthz00_4352;

																			{	/* Dataflow/walk.scm 330 */
																				obj_t BgL_arg1804z00_4353;

																				BgL_arg1804z00_4353 =
																					(BgL_oclassz00_4347);
																				BgL_odepthz00_4352 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4353);
																			}
																			if ((2L < BgL_odepthz00_4352))
																				{	/* Dataflow/walk.scm 330 */
																					obj_t BgL_arg1802z00_4354;

																					{	/* Dataflow/walk.scm 330 */
																						obj_t BgL_arg1803z00_4355;

																						BgL_arg1803z00_4355 =
																							(BgL_oclassz00_4347);
																						BgL_arg1802z00_4354 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4355, 2L);
																					}
																					BgL_res2222z00_4346 =
																						(BgL_arg1802z00_4354 ==
																						BgL_classz00_4341);
																				}
																			else
																				{	/* Dataflow/walk.scm 330 */
																					BgL_res2222z00_4346 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2449z00_5577 = BgL_res2222z00_4346;
														}
												}
											else
												{	/* Dataflow/walk.scm 330 */
													BgL_test2449z00_5577 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2449z00_5577)
										{	/* Dataflow/walk.scm 331 */
											obj_t BgL_arg1916z00_4356;

											{	/* Dataflow/walk.scm 331 */
												BgL_variablez00_bglt BgL_arg1918z00_4357;

												{
													BgL_varz00_bglt BgL_auxz00_5603;

													{	/* Dataflow/walk.scm 331 */
														obj_t BgL_pairz00_4358;

														BgL_pairz00_4358 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_4052)))->
															BgL_argsz00);
														BgL_auxz00_5603 =
															((BgL_varz00_bglt) CAR(BgL_pairz00_4358));
													}
													BgL_arg1918z00_4357 =
														(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5603))->
														BgL_variablez00);
												}
												BgL_arg1916z00_4356 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_arg1918z00_4357), BgL_g1171z00_4339);
											}
											{	/* Dataflow/walk.scm 331 */
												obj_t BgL_list1917z00_4359;

												BgL_list1917z00_4359 =
													MAKE_YOUNG_PAIR(BgL_arg1916z00_4356, BNIL);
												return BgL_list1917z00_4359;
											}
										}
									else
										{	/* Dataflow/walk.scm 330 */
											return BNIL;
										}
								}
							else
								{	/* Dataflow/walk.scm 333 */
									bool_t BgL_test2454z00_5612;

									{	/* Dataflow/walk.scm 333 */
										bool_t BgL_test2455z00_5613;

										{	/* Dataflow/walk.scm 333 */
											obj_t BgL_classz00_4360;

											BgL_classz00_4360 = BGl_funz00zzast_varz00;
											{	/* Dataflow/walk.scm 333 */
												BgL_objectz00_bglt BgL_arg1807z00_4361;

												{	/* Dataflow/walk.scm 333 */
													obj_t BgL_tmpz00_5614;

													BgL_tmpz00_5614 =
														((obj_t) ((BgL_objectz00_bglt) BgL_funvz00_4338));
													BgL_arg1807z00_4361 =
														(BgL_objectz00_bglt) (BgL_tmpz00_5614);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Dataflow/walk.scm 333 */
														long BgL_idxz00_4362;

														BgL_idxz00_4362 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4361);
														BgL_test2455z00_5613 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_4362 + 2L)) == BgL_classz00_4360);
													}
												else
													{	/* Dataflow/walk.scm 333 */
														bool_t BgL_res2224z00_4365;

														{	/* Dataflow/walk.scm 333 */
															obj_t BgL_oclassz00_4366;

															{	/* Dataflow/walk.scm 333 */
																obj_t BgL_arg1815z00_4367;
																long BgL_arg1816z00_4368;

																BgL_arg1815z00_4367 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Dataflow/walk.scm 333 */
																	long BgL_arg1817z00_4369;

																	BgL_arg1817z00_4369 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4361);
																	BgL_arg1816z00_4368 =
																		(BgL_arg1817z00_4369 - OBJECT_TYPE);
																}
																BgL_oclassz00_4366 =
																	VECTOR_REF(BgL_arg1815z00_4367,
																	BgL_arg1816z00_4368);
															}
															{	/* Dataflow/walk.scm 333 */
																bool_t BgL__ortest_1115z00_4370;

																BgL__ortest_1115z00_4370 =
																	(BgL_classz00_4360 == BgL_oclassz00_4366);
																if (BgL__ortest_1115z00_4370)
																	{	/* Dataflow/walk.scm 333 */
																		BgL_res2224z00_4365 =
																			BgL__ortest_1115z00_4370;
																	}
																else
																	{	/* Dataflow/walk.scm 333 */
																		long BgL_odepthz00_4371;

																		{	/* Dataflow/walk.scm 333 */
																			obj_t BgL_arg1804z00_4372;

																			BgL_arg1804z00_4372 =
																				(BgL_oclassz00_4366);
																			BgL_odepthz00_4371 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_4372);
																		}
																		if ((2L < BgL_odepthz00_4371))
																			{	/* Dataflow/walk.scm 333 */
																				obj_t BgL_arg1802z00_4373;

																				{	/* Dataflow/walk.scm 333 */
																					obj_t BgL_arg1803z00_4374;

																					BgL_arg1803z00_4374 =
																						(BgL_oclassz00_4366);
																					BgL_arg1802z00_4373 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_4374, 2L);
																				}
																				BgL_res2224z00_4365 =
																					(BgL_arg1802z00_4373 ==
																					BgL_classz00_4360);
																			}
																		else
																			{	/* Dataflow/walk.scm 333 */
																				BgL_res2224z00_4365 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2455z00_5613 = BgL_res2224z00_4365;
													}
											}
										}
										if (BgL_test2455z00_5613)
											{	/* Dataflow/walk.scm 333 */
												if (CBOOL(
														(((BgL_funz00_bglt) COBJECT(
																	((BgL_funz00_bglt) BgL_funvz00_4338)))->
															BgL_predicatezd2ofzd2)))
													{	/* Dataflow/walk.scm 335 */
														bool_t BgL_test2460z00_5641;

														{	/* Dataflow/walk.scm 335 */
															obj_t BgL_tmpz00_5642;

															BgL_tmpz00_5642 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_nodez00_4052)))->
																BgL_argsz00);
															BgL_test2460z00_5641 = PAIRP(BgL_tmpz00_5642);
														}
														if (BgL_test2460z00_5641)
															{	/* Dataflow/walk.scm 335 */
																if (NULLP(CDR(
																			(((BgL_appz00_bglt) COBJECT(
																						((BgL_appz00_bglt)
																							BgL_nodez00_4052)))->
																				BgL_argsz00))))
																	{	/* Dataflow/walk.scm 336 */
																		bool_t BgL_test2462z00_5651;

																		{	/* Dataflow/walk.scm 336 */
																			obj_t BgL_arg1955z00_4375;

																			BgL_arg1955z00_4375 =
																				CAR(
																				(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								BgL_nodez00_4052)))->
																					BgL_argsz00));
																			{	/* Dataflow/walk.scm 336 */
																				obj_t BgL_classz00_4376;

																				BgL_classz00_4376 =
																					BGl_varz00zzast_nodez00;
																				if (BGL_OBJECTP(BgL_arg1955z00_4375))
																					{	/* Dataflow/walk.scm 336 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_4377;
																						BgL_arg1807z00_4377 =
																							(BgL_objectz00_bglt)
																							(BgL_arg1955z00_4375);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Dataflow/walk.scm 336 */
																								long BgL_idxz00_4378;

																								BgL_idxz00_4378 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_4377);
																								BgL_test2462z00_5651 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_4378 + 2L)) ==
																									BgL_classz00_4376);
																							}
																						else
																							{	/* Dataflow/walk.scm 336 */
																								bool_t BgL_res2225z00_4381;

																								{	/* Dataflow/walk.scm 336 */
																									obj_t BgL_oclassz00_4382;

																									{	/* Dataflow/walk.scm 336 */
																										obj_t BgL_arg1815z00_4383;
																										long BgL_arg1816z00_4384;

																										BgL_arg1815z00_4383 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Dataflow/walk.scm 336 */
																											long BgL_arg1817z00_4385;

																											BgL_arg1817z00_4385 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_4377);
																											BgL_arg1816z00_4384 =
																												(BgL_arg1817z00_4385 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_4382 =
																											VECTOR_REF
																											(BgL_arg1815z00_4383,
																											BgL_arg1816z00_4384);
																									}
																									{	/* Dataflow/walk.scm 336 */
																										bool_t
																											BgL__ortest_1115z00_4386;
																										BgL__ortest_1115z00_4386 =
																											(BgL_classz00_4376 ==
																											BgL_oclassz00_4382);
																										if (BgL__ortest_1115z00_4386)
																											{	/* Dataflow/walk.scm 336 */
																												BgL_res2225z00_4381 =
																													BgL__ortest_1115z00_4386;
																											}
																										else
																											{	/* Dataflow/walk.scm 336 */
																												long BgL_odepthz00_4387;

																												{	/* Dataflow/walk.scm 336 */
																													obj_t
																														BgL_arg1804z00_4388;
																													BgL_arg1804z00_4388 =
																														(BgL_oclassz00_4382);
																													BgL_odepthz00_4387 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_4388);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_4387))
																													{	/* Dataflow/walk.scm 336 */
																														obj_t
																															BgL_arg1802z00_4389;
																														{	/* Dataflow/walk.scm 336 */
																															obj_t
																																BgL_arg1803z00_4390;
																															BgL_arg1803z00_4390
																																=
																																(BgL_oclassz00_4382);
																															BgL_arg1802z00_4389
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_4390,
																																2L);
																														}
																														BgL_res2225z00_4381
																															=
																															(BgL_arg1802z00_4389
																															==
																															BgL_classz00_4376);
																													}
																												else
																													{	/* Dataflow/walk.scm 336 */
																														BgL_res2225z00_4381
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2462z00_5651 =
																									BgL_res2225z00_4381;
																							}
																					}
																				else
																					{	/* Dataflow/walk.scm 336 */
																						BgL_test2462z00_5651 = ((bool_t) 0);
																					}
																			}
																		}
																		if (BgL_test2462z00_5651)
																			{	/* Dataflow/walk.scm 337 */
																				BgL_typez00_bglt BgL_arg1951z00_4391;

																				{
																					BgL_variablez00_bglt BgL_auxz00_5677;

																					{
																						BgL_varz00_bglt BgL_auxz00_5678;

																						{	/* Dataflow/walk.scm 337 */
																							obj_t BgL_pairz00_4392;

																							BgL_pairz00_4392 =
																								(((BgL_appz00_bglt) COBJECT(
																										((BgL_appz00_bglt)
																											BgL_nodez00_4052)))->
																								BgL_argsz00);
																							BgL_auxz00_5678 =
																								((BgL_varz00_bglt)
																								CAR(BgL_pairz00_4392));
																						}
																						BgL_auxz00_5677 =
																							(((BgL_varz00_bglt)
																								COBJECT(BgL_auxz00_5678))->
																							BgL_variablez00);
																					}
																					BgL_arg1951z00_4391 =
																						(((BgL_variablez00_bglt)
																							COBJECT(BgL_auxz00_5677))->
																						BgL_typez00);
																				}
																				BgL_test2454z00_5612 =
																					BGl_bigloozd2typezf3z21zztype_typez00
																					(BgL_arg1951z00_4391);
																			}
																		else
																			{	/* Dataflow/walk.scm 336 */
																				BgL_test2454z00_5612 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Dataflow/walk.scm 335 */
																		BgL_test2454z00_5612 = ((bool_t) 0);
																	}
															}
														else
															{	/* Dataflow/walk.scm 335 */
																BgL_test2454z00_5612 = ((bool_t) 0);
															}
													}
												else
													{	/* Dataflow/walk.scm 334 */
														BgL_test2454z00_5612 = ((bool_t) 0);
													}
											}
										else
											{	/* Dataflow/walk.scm 333 */
												BgL_test2454z00_5612 = ((bool_t) 0);
											}
									}
									if (BgL_test2454z00_5612)
										{	/* Dataflow/walk.scm 338 */
											obj_t BgL_arg1945z00_4393;

											{	/* Dataflow/walk.scm 338 */
												BgL_variablez00_bglt BgL_arg1947z00_4394;
												obj_t BgL_arg1948z00_4395;

												{
													BgL_varz00_bglt BgL_auxz00_5686;

													{	/* Dataflow/walk.scm 338 */
														obj_t BgL_pairz00_4396;

														BgL_pairz00_4396 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_4052)))->
															BgL_argsz00);
														BgL_auxz00_5686 =
															((BgL_varz00_bglt) CAR(BgL_pairz00_4396));
													}
													BgL_arg1947z00_4394 =
														(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5686))->
														BgL_variablez00);
												}
												BgL_arg1948z00_4395 =
													(((BgL_funz00_bglt) COBJECT(
															((BgL_funz00_bglt) BgL_funvz00_4338)))->
													BgL_predicatezd2ofzd2);
												BgL_arg1945z00_4393 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1947z00_4394),
													BgL_arg1948z00_4395);
											}
											{	/* Dataflow/walk.scm 338 */
												obj_t BgL_list1946z00_4397;

												BgL_list1946z00_4397 =
													MAKE_YOUNG_PAIR(BgL_arg1945z00_4393, BNIL);
												return BgL_list1946z00_4397;
											}
										}
									else
										{	/* Dataflow/walk.scm 333 */
											return BNIL;
										}
								}
						}
					}
				}
			}
		}

	}



/* &dataflow-node!-box-s1418 */
	obj_t BGl_z62dataflowzd2nodez12zd2boxzd2s1418za2zzdataflow_walkz00(obj_t
		BgL_envz00_4053, obj_t BgL_nodez00_4054, obj_t BgL_envz00_4055)
	{
		{	/* Dataflow/walk.scm 302 */
			return
				BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4054)))->BgL_valuez00),
				BgL_envz00_4055);
		}

	}



/* &dataflow-node!-box-r1416 */
	obj_t BGl_z62dataflowzd2nodez12zd2boxzd2r1416za2zzdataflow_walkz00(obj_t
		BgL_envz00_4056, obj_t BgL_nodez00_4057, obj_t BgL_envz00_4058)
	{
		{	/* Dataflow/walk.scm 295 */
			return ((obj_t) BgL_envz00_4058);
		}

	}



/* &dataflow-node!-make-1414 */
	obj_t BGl_z62dataflowzd2nodez12zd2makezd21414za2zzdataflow_walkz00(obj_t
		BgL_envz00_4059, obj_t BgL_nodez00_4060, obj_t BgL_envz00_4061)
	{
		{	/* Dataflow/walk.scm 289 */
			return
				BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_4060)))->BgL_valuez00),
				BgL_envz00_4061);
		}

	}



/* &dataflow-node!-jump-1412 */
	obj_t BGl_z62dataflowzd2nodez12zd2jumpzd21412za2zzdataflow_walkz00(obj_t
		BgL_envz00_4062, obj_t BgL_nodez00_4063, obj_t BgL_envz00_4064)
	{
		{	/* Dataflow/walk.scm 280 */
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4063)))->BgL_exitz00),
				BgL_envz00_4064);
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00((((BgL_jumpzd2exzd2itz00_bglt)
						COBJECT(((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4063)))->
					BgL_valuez00), BgL_envz00_4064);
			return BNIL;
		}

	}



/* &dataflow-node!-set-e1408 */
	obj_t BGl_z62dataflowzd2nodez12zd2setzd2e1408za2zzdataflow_walkz00(obj_t
		BgL_envz00_4065, obj_t BgL_nodez00_4066, obj_t BgL_envz00_4067)
	{
		{	/* Dataflow/walk.scm 271 */
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4066)))->BgL_bodyz00),
				BgL_envz00_4067);
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00((((BgL_setzd2exzd2itz00_bglt)
						COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4066)))->
					BgL_onexitz00), BgL_envz00_4067);
			return BNIL;
		}

	}



/* &dataflow-node!-let-v1406 */
	obj_t BGl_z62dataflowzd2nodez12zd2letzd2v1406za2zzdataflow_walkz00(obj_t
		BgL_envz00_4068, obj_t BgL_nodez00_4069, obj_t BgL_envz00_4070)
	{
		{	/* Dataflow/walk.scm 239 */
			BGl_letzd2varzd2stampz00zzdataflow_walkz00 =
				(1L + BGl_letzd2varzd2stampz00zzdataflow_walkz00);
			{	/* Dataflow/walk.scm 242 */
				long BgL_stampz00_4405;

				BgL_stampz00_4405 = BGl_letzd2varzd2stampz00zzdataflow_walkz00;
				{	/* Dataflow/walk.scm 242 */
					obj_t BgL_envz00_4406;

					{
						obj_t BgL_bindingsz00_4408;
						obj_t BgL_envz00_4409;

						BgL_bindingsz00_4408 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_4069)))->
							BgL_bindingsz00);
						BgL_envz00_4409 = BgL_envz00_4070;
					BgL_loopz00_4407:
						if (NULLP(BgL_bindingsz00_4408))
							{	/* Dataflow/walk.scm 245 */
								BgL_envz00_4406 = BgL_envz00_4409;
							}
						else
							{	/* Dataflow/walk.scm 247 */
								obj_t BgL_nodez00_4410;

								{	/* Dataflow/walk.scm 247 */
									obj_t BgL_pairz00_4411;

									BgL_pairz00_4411 = CAR(((obj_t) BgL_bindingsz00_4408));
									BgL_nodez00_4410 = CDR(BgL_pairz00_4411);
								}
								{	/* Dataflow/walk.scm 247 */
									obj_t BgL_envz00_4412;

									BgL_envz00_4412 =
										BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
										((BgL_nodez00_bglt) BgL_nodez00_4410), BgL_envz00_4409);
									{	/* Dataflow/walk.scm 248 */

										{	/* Dataflow/walk.scm 249 */
											obj_t BgL_lz00_4413;

											{	/* Dataflow/walk.scm 249 */
												obj_t BgL_pairz00_4414;

												BgL_pairz00_4414 = CAR(((obj_t) BgL_bindingsz00_4408));
												BgL_lz00_4413 = CAR(BgL_pairz00_4414);
											}
											if (
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_lz00_4413)))->
														BgL_accessz00) == CNST_TABLE_REF(11)))
												{	/* Dataflow/walk.scm 250 */
													{	/* Dataflow/walk.scm 251 */
														BgL_localzf2valuezf2_bglt BgL_wide1163z00_4415;

														BgL_wide1163z00_4415 =
															((BgL_localzf2valuezf2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_localzf2valuezf2_bgl))));
														{	/* Dataflow/walk.scm 251 */
															obj_t BgL_auxz00_5737;
															BgL_objectz00_bglt BgL_tmpz00_5733;

															BgL_auxz00_5737 = ((obj_t) BgL_wide1163z00_4415);
															BgL_tmpz00_5733 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_lz00_4413)));
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5733,
																BgL_auxz00_5737);
														}
														((BgL_objectz00_bglt)
															((BgL_localz00_bglt)
																((BgL_localz00_bglt) BgL_lz00_4413)));
														{	/* Dataflow/walk.scm 251 */
															long BgL_arg1898z00_4416;

															BgL_arg1898z00_4416 =
																BGL_CLASS_NUM
																(BGl_localzf2valuezf2zzdataflow_walkz00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																		(BgL_localz00_bglt) ((BgL_localz00_bglt)
																			BgL_lz00_4413))), BgL_arg1898z00_4416);
														}
														((BgL_localz00_bglt)
															((BgL_localz00_bglt)
																((BgL_localz00_bglt) BgL_lz00_4413)));
													}
													{
														BgL_localzf2valuezf2_bglt BgL_auxz00_5751;

														{
															obj_t BgL_auxz00_5752;

															{	/* Dataflow/walk.scm 252 */
																BgL_objectz00_bglt BgL_tmpz00_5753;

																BgL_tmpz00_5753 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt)
																		((BgL_localz00_bglt) BgL_lz00_4413)));
																BgL_auxz00_5752 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5753);
															}
															BgL_auxz00_5751 =
																((BgL_localzf2valuezf2_bglt) BgL_auxz00_5752);
														}
														((((BgL_localzf2valuezf2_bglt)
																	COBJECT(BgL_auxz00_5751))->BgL_stampz00) =
															((int) (int) (BgL_stampz00_4405)), BUNSPEC);
													}
													{
														BgL_localzf2valuezf2_bglt BgL_auxz00_5761;

														{
															obj_t BgL_auxz00_5762;

															{	/* Dataflow/walk.scm 253 */
																BgL_objectz00_bglt BgL_tmpz00_5763;

																BgL_tmpz00_5763 =
																	((BgL_objectz00_bglt)
																	((BgL_localz00_bglt)
																		((BgL_localz00_bglt) BgL_lz00_4413)));
																BgL_auxz00_5762 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5763);
															}
															BgL_auxz00_5761 =
																((BgL_localzf2valuezf2_bglt) BgL_auxz00_5762);
														}
														((((BgL_localzf2valuezf2_bglt)
																	COBJECT(BgL_auxz00_5761))->BgL_nodez00) =
															((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																	BgL_nodez00_4410)), BUNSPEC);
													}
													((obj_t)
														((BgL_localz00_bglt)
															((BgL_localz00_bglt) BgL_lz00_4413)));
												}
											else
												{	/* Dataflow/walk.scm 250 */
													BFALSE;
												}
										}
										{	/* Dataflow/walk.scm 254 */
											obj_t BgL_arg1901z00_4417;

											BgL_arg1901z00_4417 = CDR(((obj_t) BgL_bindingsz00_4408));
											{
												obj_t BgL_envz00_5777;
												obj_t BgL_bindingsz00_5776;

												BgL_bindingsz00_5776 = BgL_arg1901z00_4417;
												BgL_envz00_5777 = BgL_envz00_4412;
												BgL_envz00_4409 = BgL_envz00_5777;
												BgL_bindingsz00_4408 = BgL_bindingsz00_5776;
												goto BgL_loopz00_4407;
											}
										}
									}
								}
							}
					}
					{	/* Dataflow/walk.scm 243 */

						{	/* Dataflow/walk.scm 255 */
							obj_t BgL_arg1866z00_4418;

							BgL_arg1866z00_4418 =
								BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
								(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_4069)))->
									BgL_bodyz00), BgL_envz00_4406);
							{	/* Dataflow/walk.scm 257 */
								obj_t BgL_zc3z04anonymousza31868ze3z87_4419;

								BgL_zc3z04anonymousza31868ze3z87_4419 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31868ze3ze5zzdataflow_walkz00,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31868ze3z87_4419, (int) (0L),
									BINT(BgL_stampz00_4405));
								{	/* Dataflow/walk.scm 255 */
									obj_t BgL_list1867z00_4420;

									BgL_list1867z00_4420 =
										MAKE_YOUNG_PAIR(BgL_arg1866z00_4418, BNIL);
									return
										BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
										(BgL_zc3z04anonymousza31868ze3z87_4419,
										BgL_list1867z00_4420);
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1868> */
	obj_t BGl_z62zc3z04anonymousza31868ze3ze5zzdataflow_walkz00(obj_t
		BgL_envz00_4071, obj_t BgL_bz00_4073)
	{
		{	/* Dataflow/walk.scm 255 */
			{	/* Dataflow/walk.scm 257 */
				long BgL_stampz00_4072;

				BgL_stampz00_4072 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_4071, (int) (0L)));
				{	/* Dataflow/walk.scm 257 */
					bool_t BgL_test2469z00_5794;

					{	/* Dataflow/walk.scm 257 */
						bool_t BgL_test2470z00_5795;

						{	/* Dataflow/walk.scm 257 */
							obj_t BgL_arg1891z00_4421;

							BgL_arg1891z00_4421 = CAR(((obj_t) BgL_bz00_4073));
							{	/* Dataflow/walk.scm 257 */
								obj_t BgL_classz00_4422;

								BgL_classz00_4422 = BGl_localzf2valuezf2zzdataflow_walkz00;
								if (BGL_OBJECTP(BgL_arg1891z00_4421))
									{	/* Dataflow/walk.scm 257 */
										obj_t BgL_oclassz00_4423;

										{	/* Dataflow/walk.scm 257 */
											obj_t BgL_arg1815z00_4424;
											long BgL_arg1816z00_4425;

											BgL_arg1815z00_4424 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Dataflow/walk.scm 257 */
												long BgL_arg1817z00_4426;

												BgL_arg1817z00_4426 =
													BGL_OBJECT_CLASS_NUM(
													((BgL_objectz00_bglt) BgL_arg1891z00_4421));
												BgL_arg1816z00_4425 =
													(BgL_arg1817z00_4426 - OBJECT_TYPE);
											}
											BgL_oclassz00_4423 =
												VECTOR_REF(BgL_arg1815z00_4424, BgL_arg1816z00_4425);
										}
										BgL_test2470z00_5795 =
											(BgL_oclassz00_4423 == BgL_classz00_4422);
									}
								else
									{	/* Dataflow/walk.scm 257 */
										BgL_test2470z00_5795 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2470z00_5795)
							{	/* Dataflow/walk.scm 258 */
								int BgL_arg1889z00_4427;

								{	/* Dataflow/walk.scm 258 */
									BgL_localz00_bglt BgL_oz00_4428;

									BgL_oz00_4428 =
										((BgL_localz00_bglt) CAR(((obj_t) BgL_bz00_4073)));
									{
										BgL_localzf2valuezf2_bglt BgL_auxz00_5809;

										{
											obj_t BgL_auxz00_5810;

											{	/* Dataflow/walk.scm 258 */
												BgL_objectz00_bglt BgL_tmpz00_5811;

												BgL_tmpz00_5811 = ((BgL_objectz00_bglt) BgL_oz00_4428);
												BgL_auxz00_5810 = BGL_OBJECT_WIDENING(BgL_tmpz00_5811);
											}
											BgL_auxz00_5809 =
												((BgL_localzf2valuezf2_bglt) BgL_auxz00_5810);
										}
										BgL_arg1889z00_4427 =
											(((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_5809))->
											BgL_stampz00);
								}}
								BgL_test2469z00_5794 =
									((long) (BgL_arg1889z00_4427) < BgL_stampz00_4072);
							}
						else
							{	/* Dataflow/walk.scm 257 */
								BgL_test2469z00_5794 = ((bool_t) 1);
							}
					}
					if (BgL_test2469z00_5794)
						{	/* Dataflow/walk.scm 257 */
							return BgL_bz00_4073;
						}
					else
						{	/* Dataflow/walk.scm 260 */
							bool_t BgL_test2472z00_5818;

							{	/* Dataflow/walk.scm 260 */
								BgL_nodez00_bglt BgL_arg1887z00_4429;

								{	/* Dataflow/walk.scm 260 */
									BgL_localz00_bglt BgL_oz00_4430;

									BgL_oz00_4430 =
										((BgL_localz00_bglt) CAR(((obj_t) BgL_bz00_4073)));
									{
										BgL_localzf2valuezf2_bglt BgL_auxz00_5822;

										{
											obj_t BgL_auxz00_5823;

											{	/* Dataflow/walk.scm 260 */
												BgL_objectz00_bglt BgL_tmpz00_5824;

												BgL_tmpz00_5824 = ((BgL_objectz00_bglt) BgL_oz00_4430);
												BgL_auxz00_5823 = BGL_OBJECT_WIDENING(BgL_tmpz00_5824);
											}
											BgL_auxz00_5822 =
												((BgL_localzf2valuezf2_bglt) BgL_auxz00_5823);
										}
										BgL_arg1887z00_4429 =
											(((BgL_localzf2valuezf2_bglt) COBJECT(BgL_auxz00_5822))->
											BgL_nodez00);
									}
								}
								{	/* Dataflow/walk.scm 260 */
									obj_t BgL_classz00_4431;

									BgL_classz00_4431 = BGl_varz00zzast_nodez00;
									{	/* Dataflow/walk.scm 260 */
										BgL_objectz00_bglt BgL_arg1807z00_4432;

										{	/* Dataflow/walk.scm 260 */
											obj_t BgL_tmpz00_5829;

											BgL_tmpz00_5829 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1887z00_4429));
											BgL_arg1807z00_4432 =
												(BgL_objectz00_bglt) (BgL_tmpz00_5829);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Dataflow/walk.scm 260 */
												long BgL_idxz00_4433;

												BgL_idxz00_4433 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4432);
												BgL_test2472z00_5818 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4433 + 2L)) == BgL_classz00_4431);
											}
										else
											{	/* Dataflow/walk.scm 260 */
												bool_t BgL_res2221z00_4436;

												{	/* Dataflow/walk.scm 260 */
													obj_t BgL_oclassz00_4437;

													{	/* Dataflow/walk.scm 260 */
														obj_t BgL_arg1815z00_4438;
														long BgL_arg1816z00_4439;

														BgL_arg1815z00_4438 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Dataflow/walk.scm 260 */
															long BgL_arg1817z00_4440;

															BgL_arg1817z00_4440 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4432);
															BgL_arg1816z00_4439 =
																(BgL_arg1817z00_4440 - OBJECT_TYPE);
														}
														BgL_oclassz00_4437 =
															VECTOR_REF(BgL_arg1815z00_4438,
															BgL_arg1816z00_4439);
													}
													{	/* Dataflow/walk.scm 260 */
														bool_t BgL__ortest_1115z00_4441;

														BgL__ortest_1115z00_4441 =
															(BgL_classz00_4431 == BgL_oclassz00_4437);
														if (BgL__ortest_1115z00_4441)
															{	/* Dataflow/walk.scm 260 */
																BgL_res2221z00_4436 = BgL__ortest_1115z00_4441;
															}
														else
															{	/* Dataflow/walk.scm 260 */
																long BgL_odepthz00_4442;

																{	/* Dataflow/walk.scm 260 */
																	obj_t BgL_arg1804z00_4443;

																	BgL_arg1804z00_4443 = (BgL_oclassz00_4437);
																	BgL_odepthz00_4442 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4443);
																}
																if ((2L < BgL_odepthz00_4442))
																	{	/* Dataflow/walk.scm 260 */
																		obj_t BgL_arg1802z00_4444;

																		{	/* Dataflow/walk.scm 260 */
																			obj_t BgL_arg1803z00_4445;

																			BgL_arg1803z00_4445 =
																				(BgL_oclassz00_4437);
																			BgL_arg1802z00_4444 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4445, 2L);
																		}
																		BgL_res2221z00_4436 =
																			(BgL_arg1802z00_4444 ==
																			BgL_classz00_4431);
																	}
																else
																	{	/* Dataflow/walk.scm 260 */
																		BgL_res2221z00_4436 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2472z00_5818 = BgL_res2221z00_4436;
											}
									}
								}
							}
							if (BgL_test2472z00_5818)
								{	/* Dataflow/walk.scm 261 */
									BgL_variablez00_bglt BgL_vz00_4446;

									{	/* Dataflow/walk.scm 261 */
										BgL_nodez00_bglt BgL_arg1884z00_4447;

										{	/* Dataflow/walk.scm 261 */
											BgL_localz00_bglt BgL_oz00_4448;

											BgL_oz00_4448 =
												((BgL_localz00_bglt) CAR(((obj_t) BgL_bz00_4073)));
											{
												BgL_localzf2valuezf2_bglt BgL_auxz00_5855;

												{
													obj_t BgL_auxz00_5856;

													{	/* Dataflow/walk.scm 261 */
														BgL_objectz00_bglt BgL_tmpz00_5857;

														BgL_tmpz00_5857 =
															((BgL_objectz00_bglt) BgL_oz00_4448);
														BgL_auxz00_5856 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5857);
													}
													BgL_auxz00_5855 =
														((BgL_localzf2valuezf2_bglt) BgL_auxz00_5856);
												}
												BgL_arg1884z00_4447 =
													(((BgL_localzf2valuezf2_bglt)
														COBJECT(BgL_auxz00_5855))->BgL_nodez00);
											}
										}
										BgL_vz00_4446 =
											(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_arg1884z00_4447)))->
											BgL_variablez00);
									}
									if (
										((((BgL_variablez00_bglt) COBJECT(BgL_vz00_4446))->
												BgL_accessz00) == CNST_TABLE_REF(11)))
										{	/* Dataflow/walk.scm 263 */
											obj_t BgL_arg1882z00_4449;

											BgL_arg1882z00_4449 = CDR(((obj_t) BgL_bz00_4073));
											return
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_vz00_4446), BgL_arg1882z00_4449);
										}
									else
										{	/* Dataflow/walk.scm 262 */
											return BFALSE;
										}
								}
							else
								{	/* Dataflow/walk.scm 260 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* &dataflow-node!-let-f1403 */
	obj_t BGl_z62dataflowzd2nodez12zd2letzd2f1403za2zzdataflow_walkz00(obj_t
		BgL_envz00_4074, obj_t BgL_nodez00_4075, obj_t BgL_envz00_4076)
	{
		{	/* Dataflow/walk.scm 224 */
			{	/* Dataflow/walk.scm 226 */
				obj_t BgL_g1354z00_4451;

				BgL_g1354z00_4451 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4075)))->BgL_localsz00);
				{
					obj_t BgL_l1352z00_4453;

					BgL_l1352z00_4453 = BgL_g1354z00_4451;
				BgL_zc3z04anonymousza31857ze3z87_4452:
					if (PAIRP(BgL_l1352z00_4453))
						{	/* Dataflow/walk.scm 226 */
							{	/* Dataflow/walk.scm 227 */
								obj_t BgL_localz00_4454;

								BgL_localz00_4454 = CAR(BgL_l1352z00_4453);
								{	/* Dataflow/walk.scm 227 */
									obj_t BgL_arg1859z00_4455;

									BgL_arg1859z00_4455 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_localz00_4454))))->
														BgL_valuez00))))->BgL_bodyz00);
									BGl_dataflowzd2nodez12zc0zzdataflow_walkz00((
											(BgL_nodez00_bglt) BgL_arg1859z00_4455), BNIL);
								}
							}
							{
								obj_t BgL_l1352z00_5884;

								BgL_l1352z00_5884 = CDR(BgL_l1352z00_4453);
								BgL_l1352z00_4453 = BgL_l1352z00_5884;
								goto BgL_zc3z04anonymousza31857ze3z87_4452;
							}
						}
					else
						{	/* Dataflow/walk.scm 226 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4075)))->BgL_bodyz00),
				BgL_envz00_4076);
		}

	}



/* &dataflow-node!-switc1401 */
	obj_t BGl_z62dataflowzd2nodez12zd2switc1401z70zzdataflow_walkz00(obj_t
		BgL_envz00_4077, obj_t BgL_nodez00_4078, obj_t BgL_envz00_4079)
	{
		{	/* Dataflow/walk.scm 213 */
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_4078)))->BgL_testz00),
				BgL_envz00_4079);
			{	/* Dataflow/walk.scm 216 */
				obj_t BgL_g1351z00_4457;

				BgL_g1351z00_4457 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_4078)))->BgL_clausesz00);
				{
					obj_t BgL_l1349z00_4459;

					BgL_l1349z00_4459 = BgL_g1351z00_4457;
				BgL_zc3z04anonymousza31852ze3z87_4458:
					if (PAIRP(BgL_l1349z00_4459))
						{	/* Dataflow/walk.scm 216 */
							{	/* Dataflow/walk.scm 217 */
								obj_t BgL_clausez00_4460;

								BgL_clausez00_4460 = CAR(BgL_l1349z00_4459);
								{	/* Dataflow/walk.scm 217 */
									obj_t BgL_arg1854z00_4461;

									BgL_arg1854z00_4461 = CDR(((obj_t) BgL_clausez00_4460));
									BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
										((BgL_nodez00_bglt) BgL_arg1854z00_4461), BgL_envz00_4079);
								}
							}
							{
								obj_t BgL_l1349z00_5901;

								BgL_l1349z00_5901 = CDR(BgL_l1349z00_4459);
								BgL_l1349z00_4459 = BgL_l1349z00_5901;
								goto BgL_zc3z04anonymousza31852ze3z87_4458;
							}
						}
					else
						{	/* Dataflow/walk.scm 216 */
							((bool_t) 1);
						}
				}
			}
			return BgL_envz00_4079;
		}

	}



/* &dataflow-node!-fail1399 */
	obj_t BGl_z62dataflowzd2nodez12zd2fail1399z70zzdataflow_walkz00(obj_t
		BgL_envz00_4080, obj_t BgL_nodez00_4081, obj_t BgL_envz00_4082)
	{
		{	/* Dataflow/walk.scm 203 */
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_4081)))->BgL_procz00),
				BgL_envz00_4082);
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00((((BgL_failz00_bglt)
						COBJECT(((BgL_failz00_bglt) BgL_nodez00_4081)))->BgL_msgz00),
				BgL_envz00_4082);
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00((((BgL_failz00_bglt)
						COBJECT(((BgL_failz00_bglt) BgL_nodez00_4081)))->BgL_objz00),
				BgL_envz00_4082);
			return BgL_envz00_4082;
		}

	}



/* &dataflow-node!-condi1396 */
	obj_t BGl_z62dataflowzd2nodez12zd2condi1396z70zzdataflow_walkz00(obj_t
		BgL_envz00_4083, obj_t BgL_nodez00_4084, obj_t BgL_envz00_4085)
	{
		{	/* Dataflow/walk.scm 180 */
			{
				obj_t BgL_env1z00_4465;
				obj_t BgL_env2z00_4466;

				{	/* Dataflow/walk.scm 191 */
					obj_t BgL_tenvz00_4477;

					BgL_tenvz00_4477 =
						BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_4084)))->BgL_testz00),
						BgL_envz00_4085);
					{	/* Dataflow/walk.scm 191 */
						obj_t BgL_truezd2envzd2_4478;

						BgL_truezd2envzd2_4478 =
							BGl_appendzd221011zd2zzdataflow_walkz00
							(BGl_dataflowzd2testzd2envz00zzdataflow_walkz00(((
										(BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
												BgL_nodez00_4084)))->BgL_testz00)), BgL_tenvz00_4477);
						{	/* Dataflow/walk.scm 192 */
							obj_t BgL_falsezd2envzd2_4479;

							BgL_falsezd2envzd2_4479 =
								BGl_appendzd221011zd2zzdataflow_walkz00
								(BGl_dataflowzd2testzd2falsezd2envzd2zzdataflow_walkz00(((
											(BgL_conditionalz00_bglt)
											COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_4084)))->
										BgL_testz00)), BgL_tenvz00_4477);
							{	/* Dataflow/walk.scm 193 */

								{	/* Dataflow/walk.scm 194 */
									obj_t BgL_tenvz00_4480;
									obj_t BgL_fenvz00_4481;

									BgL_tenvz00_4480 =
										BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
										(((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_nodez00_4084)))->
											BgL_falsez00), BgL_falsezd2envzd2_4479);
									BgL_fenvz00_4481 =
										BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(((
												(BgL_conditionalz00_bglt)
												COBJECT(((BgL_conditionalz00_bglt) BgL_nodez00_4084)))->
											BgL_truez00), BgL_truezd2envzd2_4478);
									{	/* Dataflow/walk.scm 196 */
										bool_t BgL_test2479z00_5929;

										{	/* Dataflow/walk.scm 196 */
											BgL_nodez00_bglt BgL_arg1831z00_4482;

											BgL_arg1831z00_4482 =
												(((BgL_conditionalz00_bglt) COBJECT(
														((BgL_conditionalz00_bglt) BgL_nodez00_4084)))->
												BgL_falsez00);
											BgL_test2479z00_5929 =
												CBOOL(BGl_abortzf3zf3zzdataflow_walkz00
												(BgL_arg1831z00_4482));
										}
										if (BgL_test2479z00_5929)
											{	/* Dataflow/walk.scm 196 */
												return BgL_tenvz00_4480;
											}
										else
											{	/* Dataflow/walk.scm 196 */
												BgL_env1z00_4465 = BgL_tenvz00_4480;
												BgL_env2z00_4466 = BgL_fenvz00_4481;
												{	/* Dataflow/walk.scm 183 */
													obj_t BgL_hook1348z00_4467;

													BgL_hook1348z00_4467 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
													{
														obj_t BgL_l1345z00_4469;
														obj_t BgL_h1346z00_4470;

														BgL_l1345z00_4469 = BgL_env1z00_4465;
														BgL_h1346z00_4470 = BgL_hook1348z00_4467;
													BgL_zc3z04anonymousza31840ze3z87_4468:
														if (NULLP(BgL_l1345z00_4469))
															{	/* Dataflow/walk.scm 183 */
																return CDR(BgL_hook1348z00_4467);
															}
														else
															{	/* Dataflow/walk.scm 183 */
																bool_t BgL_test2481z00_5938;

																{	/* Dataflow/walk.scm 184 */
																	obj_t BgL_cz00_4471;

																	BgL_cz00_4471 =
																		CAR(((obj_t) BgL_l1345z00_4469));
																	{	/* Dataflow/walk.scm 185 */
																		obj_t BgL_c2z00_4472;

																		BgL_c2z00_4472 =
																			BGl_assqz00zz__r4_pairs_and_lists_6_3z00
																			(CAR(((obj_t) BgL_cz00_4471)),
																			BgL_env2z00_4466);
																		{	/* Dataflow/walk.scm 186 */

																			if (PAIRP(BgL_c2z00_4472))
																				{	/* Dataflow/walk.scm 187 */
																					BgL_test2481z00_5938 =
																						(CDR(BgL_c2z00_4472) ==
																						CDR(((obj_t) BgL_cz00_4471)));
																				}
																			else
																				{	/* Dataflow/walk.scm 187 */
																					BgL_test2481z00_5938 = ((bool_t) 0);
																				}
																		}
																	}
																}
																if (BgL_test2481z00_5938)
																	{	/* Dataflow/walk.scm 183 */
																		obj_t BgL_nh1347z00_4473;

																		{	/* Dataflow/walk.scm 183 */
																			obj_t BgL_arg1845z00_4474;

																			BgL_arg1845z00_4474 =
																				CAR(((obj_t) BgL_l1345z00_4469));
																			BgL_nh1347z00_4473 =
																				MAKE_YOUNG_PAIR(BgL_arg1845z00_4474,
																				BNIL);
																		}
																		SET_CDR(BgL_h1346z00_4470,
																			BgL_nh1347z00_4473);
																		{	/* Dataflow/walk.scm 183 */
																			obj_t BgL_arg1844z00_4475;

																			BgL_arg1844z00_4475 =
																				CDR(((obj_t) BgL_l1345z00_4469));
																			{
																				obj_t BgL_h1346z00_5957;
																				obj_t BgL_l1345z00_5956;

																				BgL_l1345z00_5956 = BgL_arg1844z00_4475;
																				BgL_h1346z00_5957 = BgL_nh1347z00_4473;
																				BgL_h1346z00_4470 = BgL_h1346z00_5957;
																				BgL_l1345z00_4469 = BgL_l1345z00_5956;
																				goto
																					BgL_zc3z04anonymousza31840ze3z87_4468;
																			}
																		}
																	}
																else
																	{	/* Dataflow/walk.scm 183 */
																		obj_t BgL_arg1846z00_4476;

																		BgL_arg1846z00_4476 =
																			CDR(((obj_t) BgL_l1345z00_4469));
																		{
																			obj_t BgL_l1345z00_5960;

																			BgL_l1345z00_5960 = BgL_arg1846z00_4476;
																			BgL_l1345z00_4469 = BgL_l1345z00_5960;
																			goto
																				BgL_zc3z04anonymousza31840ze3z87_4468;
																		}
																	}
															}
													}
												}
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &dataflow-node!-setq1394 */
	obj_t BGl_z62dataflowzd2nodez12zd2setq1394z70zzdataflow_walkz00(obj_t
		BgL_envz00_4086, obj_t BgL_nodez00_4087, obj_t BgL_envz00_4088)
	{
		{	/* Dataflow/walk.scm 159 */
			{	/* Dataflow/walk.scm 167 */
				obj_t BgL_nenvz00_4484;

				BgL_nenvz00_4484 =
					BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_4087)))->BgL_valuez00),
					BgL_envz00_4088);
				{	/* Dataflow/walk.scm 168 */
					BgL_varz00_bglt BgL_i1154z00_4485;

					BgL_i1154z00_4485 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_4087)))->BgL_varz00);
					{	/* Dataflow/walk.scm 169 */
						bool_t BgL_test2483z00_5966;

						{	/* Dataflow/walk.scm 169 */
							BgL_variablez00_bglt BgL_arg1773z00_4486;

							BgL_arg1773z00_4486 =
								(((BgL_varz00_bglt) COBJECT(BgL_i1154z00_4485))->
								BgL_variablez00);
							{	/* Dataflow/walk.scm 169 */
								obj_t BgL_classz00_4487;

								BgL_classz00_4487 = BGl_globalz00zzast_varz00;
								{	/* Dataflow/walk.scm 169 */
									BgL_objectz00_bglt BgL_arg1807z00_4488;

									{	/* Dataflow/walk.scm 169 */
										obj_t BgL_tmpz00_5968;

										BgL_tmpz00_5968 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1773z00_4486));
										BgL_arg1807z00_4488 =
											(BgL_objectz00_bglt) (BgL_tmpz00_5968);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Dataflow/walk.scm 169 */
											long BgL_idxz00_4489;

											BgL_idxz00_4489 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4488);
											BgL_test2483z00_5966 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4489 + 2L)) == BgL_classz00_4487);
										}
									else
										{	/* Dataflow/walk.scm 169 */
											bool_t BgL_res2220z00_4492;

											{	/* Dataflow/walk.scm 169 */
												obj_t BgL_oclassz00_4493;

												{	/* Dataflow/walk.scm 169 */
													obj_t BgL_arg1815z00_4494;
													long BgL_arg1816z00_4495;

													BgL_arg1815z00_4494 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Dataflow/walk.scm 169 */
														long BgL_arg1817z00_4496;

														BgL_arg1817z00_4496 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4488);
														BgL_arg1816z00_4495 =
															(BgL_arg1817z00_4496 - OBJECT_TYPE);
													}
													BgL_oclassz00_4493 =
														VECTOR_REF(BgL_arg1815z00_4494,
														BgL_arg1816z00_4495);
												}
												{	/* Dataflow/walk.scm 169 */
													bool_t BgL__ortest_1115z00_4497;

													BgL__ortest_1115z00_4497 =
														(BgL_classz00_4487 == BgL_oclassz00_4493);
													if (BgL__ortest_1115z00_4497)
														{	/* Dataflow/walk.scm 169 */
															BgL_res2220z00_4492 = BgL__ortest_1115z00_4497;
														}
													else
														{	/* Dataflow/walk.scm 169 */
															long BgL_odepthz00_4498;

															{	/* Dataflow/walk.scm 169 */
																obj_t BgL_arg1804z00_4499;

																BgL_arg1804z00_4499 = (BgL_oclassz00_4493);
																BgL_odepthz00_4498 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4499);
															}
															if ((2L < BgL_odepthz00_4498))
																{	/* Dataflow/walk.scm 169 */
																	obj_t BgL_arg1802z00_4500;

																	{	/* Dataflow/walk.scm 169 */
																		obj_t BgL_arg1803z00_4501;

																		BgL_arg1803z00_4501 = (BgL_oclassz00_4493);
																		BgL_arg1802z00_4500 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4501, 2L);
																	}
																	BgL_res2220z00_4492 =
																		(BgL_arg1802z00_4500 == BgL_classz00_4487);
																}
															else
																{	/* Dataflow/walk.scm 169 */
																	BgL_res2220z00_4492 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2483z00_5966 = BgL_res2220z00_4492;
										}
								}
							}
						}
						if (BgL_test2483z00_5966)
							{	/* Dataflow/walk.scm 169 */
								return
									BGl_removezd2variablezd2fromzd2envze70z35zzdataflow_walkz00(
									(((BgL_varz00_bglt) COBJECT(BgL_i1154z00_4485))->
										BgL_variablez00), BgL_nenvz00_4484);
							}
						else
							{	/* Dataflow/walk.scm 171 */
								BgL_typez00_bglt BgL_typz00_4502;

								BgL_typz00_4502 =
									BGl_getzd2typezd2zztype_typeofz00(
									(((BgL_setqz00_bglt) COBJECT(
												((BgL_setqz00_bglt) BgL_nodez00_4087)))->BgL_valuez00),
									((bool_t) 1));
								{	/* Dataflow/walk.scm 172 */
									bool_t BgL_test2487z00_5996;

									if (
										(((obj_t) BgL_typz00_4502) ==
											BGl_za2_za2z00zztype_cachez00))
										{	/* Dataflow/walk.scm 172 */
											BgL_test2487z00_5996 = ((bool_t) 1);
										}
									else
										{	/* Dataflow/walk.scm 172 */
											BgL_test2487z00_5996 =
												(
												((obj_t) BgL_typz00_4502) ==
												BGl_za2objza2z00zztype_cachez00);
										}
									if (BgL_test2487z00_5996)
										{	/* Dataflow/walk.scm 172 */
											return
												BGl_removezd2variablezd2fromzd2envze70z35zzdataflow_walkz00
												((((BgL_varz00_bglt) COBJECT(BgL_i1154z00_4485))->
													BgL_variablez00), BgL_nenvz00_4484);
										}
									else
										{	/* Dataflow/walk.scm 174 */
											obj_t BgL_arg1762z00_4503;
											obj_t BgL_arg1765z00_4504;

											{	/* Dataflow/walk.scm 174 */
												BgL_variablez00_bglt BgL_arg1767z00_4505;

												BgL_arg1767z00_4505 =
													(((BgL_varz00_bglt) COBJECT(BgL_i1154z00_4485))->
													BgL_variablez00);
												BgL_arg1762z00_4503 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1767z00_4505),
													((obj_t) BgL_typz00_4502));
											}
											BgL_arg1765z00_4504 =
												BGl_removezd2variablezd2fromzd2envze70z35zzdataflow_walkz00
												((((BgL_varz00_bglt) COBJECT(BgL_i1154z00_4485))->
													BgL_variablez00), BgL_nenvz00_4484);
											return MAKE_YOUNG_PAIR(BgL_arg1762z00_4503,
												BgL_arg1765z00_4504);
										}
								}
							}
					}
				}
			}
		}

	}



/* remove-variable-from-env~0 */
	obj_t
		BGl_removezd2variablezd2fromzd2envze70z35zzdataflow_walkz00
		(BgL_variablez00_bglt BgL_variablez00_2109, obj_t BgL_envz00_2110)
	{
		{	/* Dataflow/walk.scm 164 */
			{	/* Dataflow/walk.scm 162 */
				obj_t BgL_hook1343z00_2112;

				BgL_hook1343z00_2112 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
				{
					obj_t BgL_l1340z00_2114;
					obj_t BgL_h1341z00_2115;

					BgL_l1340z00_2114 = BgL_envz00_2110;
					BgL_h1341z00_2115 = BgL_hook1343z00_2112;
				BgL_zc3z04anonymousza31777ze3z87_2116:
					if (NULLP(BgL_l1340z00_2114))
						{	/* Dataflow/walk.scm 162 */
							return CDR(BgL_hook1343z00_2112);
						}
					else
						{	/* Dataflow/walk.scm 162 */
							bool_t BgL_test2490z00_6015;

							{	/* Dataflow/walk.scm 163 */
								bool_t BgL_test2491z00_6016;

								{	/* Dataflow/walk.scm 163 */
									obj_t BgL_tmpz00_6017;

									{	/* Dataflow/walk.scm 163 */
										obj_t BgL_pairz00_3205;

										BgL_pairz00_3205 = CAR(((obj_t) BgL_l1340z00_2114));
										BgL_tmpz00_6017 = CAR(BgL_pairz00_3205);
									}
									BgL_test2491z00_6016 =
										(BgL_tmpz00_6017 == ((obj_t) BgL_variablez00_2109));
								}
								if (BgL_test2491z00_6016)
									{	/* Dataflow/walk.scm 163 */
										BgL_test2490z00_6015 = ((bool_t) 0);
									}
								else
									{	/* Dataflow/walk.scm 163 */
										BgL_test2490z00_6015 = ((bool_t) 1);
									}
							}
							if (BgL_test2490z00_6015)
								{	/* Dataflow/walk.scm 162 */
									obj_t BgL_nh1342z00_2123;

									{	/* Dataflow/walk.scm 162 */
										obj_t BgL_arg1812z00_2125;

										BgL_arg1812z00_2125 = CAR(((obj_t) BgL_l1340z00_2114));
										BgL_nh1342z00_2123 =
											MAKE_YOUNG_PAIR(BgL_arg1812z00_2125, BNIL);
									}
									SET_CDR(BgL_h1341z00_2115, BgL_nh1342z00_2123);
									{	/* Dataflow/walk.scm 162 */
										obj_t BgL_arg1808z00_2124;

										BgL_arg1808z00_2124 = CDR(((obj_t) BgL_l1340z00_2114));
										{
											obj_t BgL_h1341z00_6030;
											obj_t BgL_l1340z00_6029;

											BgL_l1340z00_6029 = BgL_arg1808z00_2124;
											BgL_h1341z00_6030 = BgL_nh1342z00_2123;
											BgL_h1341z00_2115 = BgL_h1341z00_6030;
											BgL_l1340z00_2114 = BgL_l1340z00_6029;
											goto BgL_zc3z04anonymousza31777ze3z87_2116;
										}
									}
								}
							else
								{	/* Dataflow/walk.scm 162 */
									obj_t BgL_arg1820z00_2126;

									BgL_arg1820z00_2126 = CDR(((obj_t) BgL_l1340z00_2114));
									{
										obj_t BgL_l1340z00_6033;

										BgL_l1340z00_6033 = BgL_arg1820z00_2126;
										BgL_l1340z00_2114 = BgL_l1340z00_6033;
										goto BgL_zc3z04anonymousza31777ze3z87_2116;
									}
								}
						}
				}
			}
		}

	}



/* &dataflow-node!-exter1392 */
	obj_t BGl_z62dataflowzd2nodez12zd2exter1392z70zzdataflow_walkz00(obj_t
		BgL_envz00_4089, obj_t BgL_nodez00_4090, obj_t BgL_envz00_4091)
	{
		{	/* Dataflow/walk.scm 152 */
			{	/* Dataflow/walk.scm 153 */
				obj_t BgL_res2307z00_4507;

				BgL_res2307z00_4507 =
					BGl_dataflowzd2nodeza2z12z62zzdataflow_walkz00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_4090)))->BgL_exprza2za2),
					BgL_envz00_4091);
				return BgL_res2307z00_4507;
			}
		}

	}



/* &dataflow-node!-funca1390 */
	obj_t BGl_z62dataflowzd2nodez12zd2funca1390z70zzdataflow_walkz00(obj_t
		BgL_envz00_4092, obj_t BgL_nodez00_4093, obj_t BgL_envz00_4094)
	{
		{	/* Dataflow/walk.scm 144 */
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_4093)))->BgL_funz00),
				BgL_envz00_4094);
			return
				BGl_dataflowzd2nodeza2z12z62zzdataflow_walkz00((((BgL_funcallz00_bglt)
						COBJECT(((BgL_funcallz00_bglt) BgL_nodez00_4093)))->BgL_argsz00),
				BgL_envz00_4094);
		}

	}



/* &dataflow-node!-app-l1388 */
	obj_t BGl_z62dataflowzd2nodez12zd2appzd2l1388za2zzdataflow_walkz00(obj_t
		BgL_envz00_4095, obj_t BgL_nodez00_4096, obj_t BgL_envz00_4097)
	{
		{	/* Dataflow/walk.scm 136 */
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_4096)))->BgL_funz00),
				BgL_envz00_4097);
			return
				BGl_dataflowzd2nodez12zc0zzdataflow_walkz00((((BgL_appzd2lyzd2_bglt)
						COBJECT(((BgL_appzd2lyzd2_bglt) BgL_nodez00_4096)))->BgL_argz00),
				BgL_envz00_4097);
		}

	}



/* &dataflow-node!-app1386 */
	obj_t BGl_z62dataflowzd2nodez12zd2app1386z70zzdataflow_walkz00(obj_t
		BgL_envz00_4098, obj_t BgL_nodez00_4099, obj_t BgL_envz00_4100)
	{
		{	/* Dataflow/walk.scm 120 */
			{	/* Dataflow/walk.scm 122 */
				obj_t BgL_aenvz00_4511;

				BgL_aenvz00_4511 =
					BGl_dataflowzd2nodeza2z12z62zzdataflow_walkz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_4099)))->BgL_argsz00),
					BgL_envz00_4100);
				{	/* Dataflow/walk.scm 123 */
					bool_t BgL_test2492z00_6052;

					{	/* Dataflow/walk.scm 123 */
						bool_t BgL_test2493z00_6053;

						{	/* Dataflow/walk.scm 123 */
							BgL_varz00_bglt BgL_arg1746z00_4512;

							BgL_arg1746z00_4512 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_4099)))->BgL_funz00);
							{	/* Dataflow/walk.scm 123 */
								obj_t BgL_classz00_4513;

								BgL_classz00_4513 = BGl_varz00zzast_nodez00;
								{	/* Dataflow/walk.scm 123 */
									BgL_objectz00_bglt BgL_arg1807z00_4514;

									{	/* Dataflow/walk.scm 123 */
										obj_t BgL_tmpz00_6056;

										BgL_tmpz00_6056 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1746z00_4512));
										BgL_arg1807z00_4514 =
											(BgL_objectz00_bglt) (BgL_tmpz00_6056);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Dataflow/walk.scm 123 */
											long BgL_idxz00_4515;

											BgL_idxz00_4515 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4514);
											BgL_test2493z00_6053 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4515 + 2L)) == BgL_classz00_4513);
										}
									else
										{	/* Dataflow/walk.scm 123 */
											bool_t BgL_res2217z00_4518;

											{	/* Dataflow/walk.scm 123 */
												obj_t BgL_oclassz00_4519;

												{	/* Dataflow/walk.scm 123 */
													obj_t BgL_arg1815z00_4520;
													long BgL_arg1816z00_4521;

													BgL_arg1815z00_4520 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Dataflow/walk.scm 123 */
														long BgL_arg1817z00_4522;

														BgL_arg1817z00_4522 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4514);
														BgL_arg1816z00_4521 =
															(BgL_arg1817z00_4522 - OBJECT_TYPE);
													}
													BgL_oclassz00_4519 =
														VECTOR_REF(BgL_arg1815z00_4520,
														BgL_arg1816z00_4521);
												}
												{	/* Dataflow/walk.scm 123 */
													bool_t BgL__ortest_1115z00_4523;

													BgL__ortest_1115z00_4523 =
														(BgL_classz00_4513 == BgL_oclassz00_4519);
													if (BgL__ortest_1115z00_4523)
														{	/* Dataflow/walk.scm 123 */
															BgL_res2217z00_4518 = BgL__ortest_1115z00_4523;
														}
													else
														{	/* Dataflow/walk.scm 123 */
															long BgL_odepthz00_4524;

															{	/* Dataflow/walk.scm 123 */
																obj_t BgL_arg1804z00_4525;

																BgL_arg1804z00_4525 = (BgL_oclassz00_4519);
																BgL_odepthz00_4524 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4525);
															}
															if ((2L < BgL_odepthz00_4524))
																{	/* Dataflow/walk.scm 123 */
																	obj_t BgL_arg1802z00_4526;

																	{	/* Dataflow/walk.scm 123 */
																		obj_t BgL_arg1803z00_4527;

																		BgL_arg1803z00_4527 = (BgL_oclassz00_4519);
																		BgL_arg1802z00_4526 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4527, 2L);
																	}
																	BgL_res2217z00_4518 =
																		(BgL_arg1802z00_4526 == BgL_classz00_4513);
																}
															else
																{	/* Dataflow/walk.scm 123 */
																	BgL_res2217z00_4518 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2493z00_6053 = BgL_res2217z00_4518;
										}
								}
							}
						}
						if (BgL_test2493z00_6053)
							{	/* Dataflow/walk.scm 123 */
								BgL_variablez00_bglt BgL_arg1739z00_4528;

								BgL_arg1739z00_4528 =
									(((BgL_varz00_bglt) COBJECT(
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_4099)))->
												BgL_funz00)))->BgL_variablez00);
								{	/* Dataflow/walk.scm 123 */
									obj_t BgL_classz00_4529;

									BgL_classz00_4529 = BGl_localz00zzast_varz00;
									{	/* Dataflow/walk.scm 123 */
										BgL_objectz00_bglt BgL_arg1807z00_4530;

										{	/* Dataflow/walk.scm 123 */
											obj_t BgL_tmpz00_6082;

											BgL_tmpz00_6082 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1739z00_4528));
											BgL_arg1807z00_4530 =
												(BgL_objectz00_bglt) (BgL_tmpz00_6082);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Dataflow/walk.scm 123 */
												long BgL_idxz00_4531;

												BgL_idxz00_4531 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4530);
												BgL_test2492z00_6052 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4531 + 2L)) == BgL_classz00_4529);
											}
										else
											{	/* Dataflow/walk.scm 123 */
												bool_t BgL_res2218z00_4534;

												{	/* Dataflow/walk.scm 123 */
													obj_t BgL_oclassz00_4535;

													{	/* Dataflow/walk.scm 123 */
														obj_t BgL_arg1815z00_4536;
														long BgL_arg1816z00_4537;

														BgL_arg1815z00_4536 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Dataflow/walk.scm 123 */
															long BgL_arg1817z00_4538;

															BgL_arg1817z00_4538 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4530);
															BgL_arg1816z00_4537 =
																(BgL_arg1817z00_4538 - OBJECT_TYPE);
														}
														BgL_oclassz00_4535 =
															VECTOR_REF(BgL_arg1815z00_4536,
															BgL_arg1816z00_4537);
													}
													{	/* Dataflow/walk.scm 123 */
														bool_t BgL__ortest_1115z00_4539;

														BgL__ortest_1115z00_4539 =
															(BgL_classz00_4529 == BgL_oclassz00_4535);
														if (BgL__ortest_1115z00_4539)
															{	/* Dataflow/walk.scm 123 */
																BgL_res2218z00_4534 = BgL__ortest_1115z00_4539;
															}
														else
															{	/* Dataflow/walk.scm 123 */
																long BgL_odepthz00_4540;

																{	/* Dataflow/walk.scm 123 */
																	obj_t BgL_arg1804z00_4541;

																	BgL_arg1804z00_4541 = (BgL_oclassz00_4535);
																	BgL_odepthz00_4540 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4541);
																}
																if ((2L < BgL_odepthz00_4540))
																	{	/* Dataflow/walk.scm 123 */
																		obj_t BgL_arg1802z00_4542;

																		{	/* Dataflow/walk.scm 123 */
																			obj_t BgL_arg1803z00_4543;

																			BgL_arg1803z00_4543 =
																				(BgL_oclassz00_4535);
																			BgL_arg1802z00_4542 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4543, 2L);
																		}
																		BgL_res2218z00_4534 =
																			(BgL_arg1802z00_4542 ==
																			BgL_classz00_4529);
																	}
																else
																	{	/* Dataflow/walk.scm 123 */
																		BgL_res2218z00_4534 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2492z00_6052 = BgL_res2218z00_4534;
											}
									}
								}
							}
						else
							{	/* Dataflow/walk.scm 123 */
								BgL_test2492z00_6052 = ((bool_t) 0);
							}
					}
					if (BgL_test2492z00_6052)
						{	/* Dataflow/walk.scm 127 */
							obj_t BgL_hook1338z00_4544;

							BgL_hook1338z00_4544 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
							{
								obj_t BgL_l1335z00_4546;
								obj_t BgL_h1336z00_4547;

								BgL_l1335z00_4546 = BgL_aenvz00_4511;
								BgL_h1336z00_4547 = BgL_hook1338z00_4544;
							BgL_zc3z04anonymousza31726ze3z87_4545:
								if (NULLP(BgL_l1335z00_4546))
									{	/* Dataflow/walk.scm 127 */
										return CDR(BgL_hook1338z00_4544);
									}
								else
									{	/* Dataflow/walk.scm 127 */
										bool_t BgL_test2501z00_6109;

										{	/* Dataflow/walk.scm 128 */
											obj_t BgL_vz00_4548;

											{	/* Dataflow/walk.scm 128 */
												obj_t BgL_pairz00_4549;

												BgL_pairz00_4549 = CAR(((obj_t) BgL_l1335z00_4546));
												BgL_vz00_4548 = CAR(BgL_pairz00_4549);
											}
											{	/* Dataflow/walk.scm 129 */
												bool_t BgL__ortest_1150z00_4550;

												{	/* Dataflow/walk.scm 129 */
													obj_t BgL_classz00_4551;

													BgL_classz00_4551 = BGl_globalz00zzast_varz00;
													if (BGL_OBJECTP(BgL_vz00_4548))
														{	/* Dataflow/walk.scm 129 */
															BgL_objectz00_bglt BgL_arg1807z00_4552;

															BgL_arg1807z00_4552 =
																(BgL_objectz00_bglt) (BgL_vz00_4548);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Dataflow/walk.scm 129 */
																	long BgL_idxz00_4553;

																	BgL_idxz00_4553 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4552);
																	BgL__ortest_1150z00_4550 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4553 + 2L)) ==
																		BgL_classz00_4551);
																}
															else
																{	/* Dataflow/walk.scm 129 */
																	bool_t BgL_res2219z00_4556;

																	{	/* Dataflow/walk.scm 129 */
																		obj_t BgL_oclassz00_4557;

																		{	/* Dataflow/walk.scm 129 */
																			obj_t BgL_arg1815z00_4558;
																			long BgL_arg1816z00_4559;

																			BgL_arg1815z00_4558 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Dataflow/walk.scm 129 */
																				long BgL_arg1817z00_4560;

																				BgL_arg1817z00_4560 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4552);
																				BgL_arg1816z00_4559 =
																					(BgL_arg1817z00_4560 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4557 =
																				VECTOR_REF(BgL_arg1815z00_4558,
																				BgL_arg1816z00_4559);
																		}
																		{	/* Dataflow/walk.scm 129 */
																			bool_t BgL__ortest_1115z00_4561;

																			BgL__ortest_1115z00_4561 =
																				(BgL_classz00_4551 ==
																				BgL_oclassz00_4557);
																			if (BgL__ortest_1115z00_4561)
																				{	/* Dataflow/walk.scm 129 */
																					BgL_res2219z00_4556 =
																						BgL__ortest_1115z00_4561;
																				}
																			else
																				{	/* Dataflow/walk.scm 129 */
																					long BgL_odepthz00_4562;

																					{	/* Dataflow/walk.scm 129 */
																						obj_t BgL_arg1804z00_4563;

																						BgL_arg1804z00_4563 =
																							(BgL_oclassz00_4557);
																						BgL_odepthz00_4562 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4563);
																					}
																					if ((2L < BgL_odepthz00_4562))
																						{	/* Dataflow/walk.scm 129 */
																							obj_t BgL_arg1802z00_4564;

																							{	/* Dataflow/walk.scm 129 */
																								obj_t BgL_arg1803z00_4565;

																								BgL_arg1803z00_4565 =
																									(BgL_oclassz00_4557);
																								BgL_arg1802z00_4564 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4565, 2L);
																							}
																							BgL_res2219z00_4556 =
																								(BgL_arg1802z00_4564 ==
																								BgL_classz00_4551);
																						}
																					else
																						{	/* Dataflow/walk.scm 129 */
																							BgL_res2219z00_4556 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL__ortest_1150z00_4550 =
																		BgL_res2219z00_4556;
																}
														}
													else
														{	/* Dataflow/walk.scm 129 */
															BgL__ortest_1150z00_4550 = ((bool_t) 0);
														}
												}
												if (BgL__ortest_1150z00_4550)
													{	/* Dataflow/walk.scm 129 */
														BgL_test2501z00_6109 = BgL__ortest_1150z00_4550;
													}
												else
													{	/* Dataflow/walk.scm 129 */
														BgL_test2501z00_6109 =
															(
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_vz00_4548)))->
																BgL_accessz00) == CNST_TABLE_REF(11));
													}
											}
										}
										if (BgL_test2501z00_6109)
											{	/* Dataflow/walk.scm 127 */
												obj_t BgL_nh1337z00_4566;

												{	/* Dataflow/walk.scm 127 */
													obj_t BgL_arg1736z00_4567;

													BgL_arg1736z00_4567 =
														CAR(((obj_t) BgL_l1335z00_4546));
													BgL_nh1337z00_4566 =
														MAKE_YOUNG_PAIR(BgL_arg1736z00_4567, BNIL);
												}
												SET_CDR(BgL_h1336z00_4547, BgL_nh1337z00_4566);
												{	/* Dataflow/walk.scm 127 */
													obj_t BgL_arg1735z00_4568;

													BgL_arg1735z00_4568 =
														CDR(((obj_t) BgL_l1335z00_4546));
													{
														obj_t BgL_h1336z00_6147;
														obj_t BgL_l1335z00_6146;

														BgL_l1335z00_6146 = BgL_arg1735z00_4568;
														BgL_h1336z00_6147 = BgL_nh1337z00_4566;
														BgL_h1336z00_4547 = BgL_h1336z00_6147;
														BgL_l1335z00_4546 = BgL_l1335z00_6146;
														goto BgL_zc3z04anonymousza31726ze3z87_4545;
													}
												}
											}
										else
											{	/* Dataflow/walk.scm 127 */
												obj_t BgL_arg1737z00_4569;

												BgL_arg1737z00_4569 = CDR(((obj_t) BgL_l1335z00_4546));
												{
													obj_t BgL_l1335z00_6150;

													BgL_l1335z00_6150 = BgL_arg1737z00_4569;
													BgL_l1335z00_4546 = BgL_l1335z00_6150;
													goto BgL_zc3z04anonymousza31726ze3z87_4545;
												}
											}
									}
							}
						}
					else
						{	/* Dataflow/walk.scm 123 */
							return BgL_aenvz00_4511;
						}
				}
			}
		}

	}



/* &dataflow-node!-sync1384 */
	obj_t BGl_z62dataflowzd2nodez12zd2sync1384z70zzdataflow_walkz00(obj_t
		BgL_envz00_4101, obj_t BgL_nodez00_4102, obj_t BgL_envz00_4103)
	{
		{	/* Dataflow/walk.scm 111 */
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4102)))->BgL_mutexz00),
				BgL_envz00_4103);
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00((((BgL_syncz00_bglt)
						COBJECT(((BgL_syncz00_bglt) BgL_nodez00_4102)))->BgL_prelockz00),
				BgL_envz00_4103);
			BGl_dataflowzd2nodez12zc0zzdataflow_walkz00((((BgL_syncz00_bglt)
						COBJECT(((BgL_syncz00_bglt) BgL_nodez00_4102)))->BgL_bodyz00),
				BgL_envz00_4103);
			return BgL_envz00_4103;
		}

	}



/* &dataflow-node!-seque1382 */
	obj_t BGl_z62dataflowzd2nodez12zd2seque1382z70zzdataflow_walkz00(obj_t
		BgL_envz00_4104, obj_t BgL_nodez00_4105, obj_t BgL_envz00_4106)
	{
		{	/* Dataflow/walk.scm 101 */
			{	/* Dataflow/walk.scm 102 */
				obj_t BgL_res2308z00_4579;

				{	/* Dataflow/walk.scm 102 */
					obj_t BgL_g1148z00_4572;

					BgL_g1148z00_4572 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_4105)))->BgL_nodesz00);
					{
						obj_t BgL_nodeza2za2_4574;
						obj_t BgL_envz00_4575;

						BgL_nodeza2za2_4574 = BgL_g1148z00_4572;
						BgL_envz00_4575 = BgL_envz00_4106;
					BgL_loopz00_4573:
						if (NULLP(BgL_nodeza2za2_4574))
							{	/* Dataflow/walk.scm 104 */
								BgL_res2308z00_4579 = BgL_envz00_4575;
							}
						else
							{	/* Dataflow/walk.scm 106 */
								obj_t BgL_arg1708z00_4576;
								obj_t BgL_arg1709z00_4577;

								BgL_arg1708z00_4576 = CDR(((obj_t) BgL_nodeza2za2_4574));
								{	/* Dataflow/walk.scm 106 */
									obj_t BgL_arg1710z00_4578;

									BgL_arg1710z00_4578 = CAR(((obj_t) BgL_nodeza2za2_4574));
									BgL_arg1709z00_4577 =
										BGl_dataflowzd2nodez12zc0zzdataflow_walkz00(
										((BgL_nodez00_bglt) BgL_arg1710z00_4578), BgL_envz00_4575);
								}
								{
									obj_t BgL_envz00_6171;
									obj_t BgL_nodeza2za2_6170;

									BgL_nodeza2za2_6170 = BgL_arg1708z00_4576;
									BgL_envz00_6171 = BgL_arg1709z00_4577;
									BgL_envz00_4575 = BgL_envz00_6171;
									BgL_nodeza2za2_4574 = BgL_nodeza2za2_6170;
									goto BgL_loopz00_4573;
								}
							}
					}
				}
				return BgL_res2308z00_4579;
			}
		}

	}



/* &dataflow-node!-closu1380 */
	obj_t BGl_z62dataflowzd2nodez12zd2closu1380z70zzdataflow_walkz00(obj_t
		BgL_envz00_4107, obj_t BgL_nodez00_4108, obj_t BgL_envz00_4109)
	{
		{	/* Dataflow/walk.scm 95 */
			return ((obj_t) BgL_envz00_4109);
		}

	}



/* &dataflow-node!-var1378 */
	obj_t BGl_z62dataflowzd2nodez12zd2var1378z70zzdataflow_walkz00(obj_t
		BgL_envz00_4110, obj_t BgL_nodez00_4111, obj_t BgL_envz00_4112)
	{
		{	/* Dataflow/walk.scm 81 */
			{	/* Dataflow/walk.scm 83 */
				BgL_variablez00_bglt BgL_i1146z00_4583;

				BgL_i1146z00_4583 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_4111)))->BgL_variablez00);
				{	/* Dataflow/walk.scm 84 */
					bool_t BgL_test2508z00_6175;

					if (BGl_bigloozd2typezf3z21zztype_typez00(
							(((BgL_variablez00_bglt) COBJECT(BgL_i1146z00_4583))->
								BgL_typez00)))
						{	/* Dataflow/walk.scm 85 */
							bool_t BgL__ortest_1147z00_4584;

							{	/* Dataflow/walk.scm 85 */
								bool_t BgL_test2510z00_6179;

								{	/* Dataflow/walk.scm 85 */
									BgL_typez00_bglt BgL_arg1702z00_4585;

									BgL_arg1702z00_4585 =
										(((BgL_variablez00_bglt) COBJECT(BgL_i1146z00_4583))->
										BgL_typez00);
									{	/* Dataflow/walk.scm 85 */
										obj_t BgL_classz00_4586;

										BgL_classz00_4586 = BGl_tclassz00zzobject_classz00;
										{	/* Dataflow/walk.scm 85 */
											BgL_objectz00_bglt BgL_arg1807z00_4587;

											{	/* Dataflow/walk.scm 85 */
												obj_t BgL_tmpz00_6181;

												BgL_tmpz00_6181 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1702z00_4585));
												BgL_arg1807z00_4587 =
													(BgL_objectz00_bglt) (BgL_tmpz00_6181);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Dataflow/walk.scm 85 */
													long BgL_idxz00_4588;

													BgL_idxz00_4588 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4587);
													BgL_test2510z00_6179 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4588 + 2L)) == BgL_classz00_4586);
												}
											else
												{	/* Dataflow/walk.scm 85 */
													bool_t BgL_res2216z00_4591;

													{	/* Dataflow/walk.scm 85 */
														obj_t BgL_oclassz00_4592;

														{	/* Dataflow/walk.scm 85 */
															obj_t BgL_arg1815z00_4593;
															long BgL_arg1816z00_4594;

															BgL_arg1815z00_4593 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Dataflow/walk.scm 85 */
																long BgL_arg1817z00_4595;

																BgL_arg1817z00_4595 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4587);
																BgL_arg1816z00_4594 =
																	(BgL_arg1817z00_4595 - OBJECT_TYPE);
															}
															BgL_oclassz00_4592 =
																VECTOR_REF(BgL_arg1815z00_4593,
																BgL_arg1816z00_4594);
														}
														{	/* Dataflow/walk.scm 85 */
															bool_t BgL__ortest_1115z00_4596;

															BgL__ortest_1115z00_4596 =
																(BgL_classz00_4586 == BgL_oclassz00_4592);
															if (BgL__ortest_1115z00_4596)
																{	/* Dataflow/walk.scm 85 */
																	BgL_res2216z00_4591 =
																		BgL__ortest_1115z00_4596;
																}
															else
																{	/* Dataflow/walk.scm 85 */
																	long BgL_odepthz00_4597;

																	{	/* Dataflow/walk.scm 85 */
																		obj_t BgL_arg1804z00_4598;

																		BgL_arg1804z00_4598 = (BgL_oclassz00_4592);
																		BgL_odepthz00_4597 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4598);
																	}
																	if ((2L < BgL_odepthz00_4597))
																		{	/* Dataflow/walk.scm 85 */
																			obj_t BgL_arg1802z00_4599;

																			{	/* Dataflow/walk.scm 85 */
																				obj_t BgL_arg1803z00_4600;

																				BgL_arg1803z00_4600 =
																					(BgL_oclassz00_4592);
																				BgL_arg1802z00_4599 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4600, 2L);
																			}
																			BgL_res2216z00_4591 =
																				(BgL_arg1802z00_4599 ==
																				BgL_classz00_4586);
																		}
																	else
																		{	/* Dataflow/walk.scm 85 */
																			BgL_res2216z00_4591 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2510z00_6179 = BgL_res2216z00_4591;
												}
										}
									}
								}
								if (BgL_test2510z00_6179)
									{	/* Dataflow/walk.scm 85 */
										BgL__ortest_1147z00_4584 = ((bool_t) 0);
									}
								else
									{	/* Dataflow/walk.scm 85 */
										BgL__ortest_1147z00_4584 = ((bool_t) 1);
									}
							}
							if (BgL__ortest_1147z00_4584)
								{	/* Dataflow/walk.scm 85 */
									BgL_test2508z00_6175 = BgL__ortest_1147z00_4584;
								}
							else
								{	/* Dataflow/walk.scm 85 */
									BgL_typez00_bglt BgL_arg1700z00_4601;
									BgL_typez00_bglt BgL_arg1701z00_4602;

									BgL_arg1700z00_4601 =
										(((BgL_variablez00_bglt) COBJECT(BgL_i1146z00_4583))->
										BgL_typez00);
									BgL_arg1701z00_4602 =
										(((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
														BgL_nodez00_4111))))->BgL_typez00);
									BgL_test2508z00_6175 =
										BGl_typezd2subclasszf3z21zzobject_classz00
										(BgL_arg1700z00_4601, BgL_arg1701z00_4602);
								}
						}
					else
						{	/* Dataflow/walk.scm 84 */
							BgL_test2508z00_6175 = ((bool_t) 0);
						}
					if (BgL_test2508z00_6175)
						{	/* Dataflow/walk.scm 86 */
							obj_t BgL_bz00_4603;

							BgL_bz00_4603 =
								BGl_assqz00zz__r4_pairs_and_lists_6_3z00(
								((obj_t)
									(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_4111)))->
										BgL_variablez00)), BgL_envz00_4112);
							if (PAIRP(BgL_bz00_4603))
								{	/* Dataflow/walk.scm 87 */
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_4111))))->
											BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												CDR(BgL_bz00_4603))), BUNSPEC);
								}
							else
								{	/* Dataflow/walk.scm 87 */
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_4111))))->
											BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																		BgL_nodez00_4111)))->BgL_variablez00)))->
												BgL_typez00)), BUNSPEC);
								}
						}
					else
						{	/* Dataflow/walk.scm 84 */
							BFALSE;
						}
				}
			}
			return BgL_envz00_4112;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzdataflow_walkz00(void)
	{
		{	/* Dataflow/walk.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_miscz00(49975086L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzeffect_cgraphz00(271385047L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzeffect_spreadz00(348216764L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			BGl_modulezd2initializa7ationz75zzeffect_feffectz00(516374368L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2303z00zzdataflow_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
