/*===========================================================================*/
/*   (Module/load.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/load.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_LOAD_TYPE_DEFINITIONS
#define BGL_MODULE_LOAD_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;


#endif													// BGL_MODULE_LOAD_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62zc3z04anonymousza31104ze3ze5zzmodule_loadz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_loadz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31105ze3ze5zzmodule_loadz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_loadz00(void);
	static obj_t BGl_objectzd2initzd2zzmodule_loadz00(void);
	static obj_t BGl_loadzd2parserzd2zzmodule_loadz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_dirnamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_loadz00(void);
	BGL_EXPORTED_DECL obj_t BGl_makezd2loadzd2compilerz00zzmodule_loadz00(void);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	extern obj_t BGl_loadzd2modulezd2zzread_loadz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_bigloozd2modulezd2resolverz00zz__modulez00(void);
	static obj_t BGl_z62loadzd2producerzb0zzmodule_loadz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzmodule_loadz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_loadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__modulez00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzmodule_loadz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_loadz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_loadz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_loadz00(void);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2accesszd2filesza2zd2zzengine_paramz00;
	static obj_t BGl_z62makezd2loadzd2compilerz62zzmodule_loadz00(obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_loadzd2producerzd2envz00zzmodule_loadz00,
		BgL_bgl_za762loadza7d2produc1207z00,
		BGl_z62loadzd2producerzb0zzmodule_loadz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1197z00zzmodule_loadz00,
		BgL_bgl_string1197za700za7za7m1208za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1198z00zzmodule_loadz00,
		BgL_bgl_string1198za700za7za7m1209za7, "Illegal `load' clause", 21);
	      DEFINE_STRING(BGl_string1199z00zzmodule_loadz00,
		BgL_bgl_string1199za700za7za7m1210za7, "Illegal load clause", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1195z00zzmodule_loadz00,
		BgL_bgl_za762za7c3za704anonymo1211za7,
		BGl_z62zc3z04anonymousza31104ze3ze5zzmodule_loadz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1196z00zzmodule_loadz00,
		BgL_bgl_za762za7c3za704anonymo1212za7,
		BGl_z62zc3z04anonymousza31105ze3ze5zzmodule_loadz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2loadzd2compilerzd2envzd2zzmodule_loadz00,
		BgL_bgl_za762makeza7d2loadza7d1213za7,
		BGl_z62makezd2loadzd2compilerz62zzmodule_loadz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1200z00zzmodule_loadz00,
		BgL_bgl_string1200za700za7za7m1214za7, "load", 4);
	      DEFINE_STRING(BGl_string1201z00zzmodule_loadz00,
		BgL_bgl_string1201za700za7za7m1215za7, "Can't load module", 17);
	      DEFINE_STRING(BGl_string1202z00zzmodule_loadz00,
		BgL_bgl_string1202za700za7za7m1216za7, "module_load", 11);
	      DEFINE_STRING(BGl_string1203z00zzmodule_loadz00,
		BgL_bgl_string1203za700za7za7m1217za7, "void load ", 10);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzmodule_loadz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_loadz00(long
		BgL_checksumz00_613, char *BgL_fromz00_614)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_loadz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_loadz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_loadz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_loadz00();
					BGl_cnstzd2initzd2zzmodule_loadz00();
					BGl_importedzd2moduleszd2initz00zzmodule_loadz00();
					return BGl_methodzd2initzd2zzmodule_loadz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_loadz00(void)
	{
		{	/* Module/load.scm 15 */
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "module_load");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_load");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "module_load");
			BGl_modulezd2initializa7ationz75zz__modulez00(0L, "module_load");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "module_load");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "module_load");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_load");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_load");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_loadz00(void)
	{
		{	/* Module/load.scm 15 */
			{	/* Module/load.scm 15 */
				obj_t BgL_cportz00_595;

				{	/* Module/load.scm 15 */
					obj_t BgL_stringz00_602;

					BgL_stringz00_602 = BGl_string1203z00zzmodule_loadz00;
					{	/* Module/load.scm 15 */
						obj_t BgL_startz00_603;

						BgL_startz00_603 = BINT(0L);
						{	/* Module/load.scm 15 */
							obj_t BgL_endz00_604;

							BgL_endz00_604 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_602)));
							{	/* Module/load.scm 15 */

								BgL_cportz00_595 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_602, BgL_startz00_603, BgL_endz00_604);
				}}}}
				{
					long BgL_iz00_596;

					BgL_iz00_596 = 1L;
				BgL_loopz00_597:
					if ((BgL_iz00_596 == -1L))
						{	/* Module/load.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/load.scm 15 */
							{	/* Module/load.scm 15 */
								obj_t BgL_arg1206z00_598;

								{	/* Module/load.scm 15 */

									{	/* Module/load.scm 15 */
										obj_t BgL_locationz00_600;

										BgL_locationz00_600 = BBOOL(((bool_t) 0));
										{	/* Module/load.scm 15 */

											BgL_arg1206z00_598 =
												BGl_readz00zz__readerz00(BgL_cportz00_595,
												BgL_locationz00_600);
										}
									}
								}
								{	/* Module/load.scm 15 */
									int BgL_tmpz00_640;

									BgL_tmpz00_640 = (int) (BgL_iz00_596);
									CNST_TABLE_SET(BgL_tmpz00_640, BgL_arg1206z00_598);
							}}
							{	/* Module/load.scm 15 */
								int BgL_auxz00_601;

								BgL_auxz00_601 = (int) ((BgL_iz00_596 - 1L));
								{
									long BgL_iz00_645;

									BgL_iz00_645 = (long) (BgL_auxz00_601);
									BgL_iz00_596 = BgL_iz00_645;
									goto BgL_loopz00_597;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_loadz00(void)
	{
		{	/* Module/load.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* make-load-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2loadzd2compilerz00zzmodule_loadz00(void)
	{
		{	/* Module/load.scm 30 */
			{	/* Module/load.scm 31 */
				BgL_ccompz00_bglt BgL_new1062z00_454;

				{	/* Module/load.scm 31 */
					BgL_ccompz00_bglt BgL_new1061z00_459;

					BgL_new1061z00_459 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/load.scm 31 */
						long BgL_arg1114z00_460;

						BgL_arg1114z00_460 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1061z00_459), BgL_arg1114z00_460);
					}
					BgL_new1062z00_454 = BgL_new1061z00_459;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1062z00_454))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1062z00_454))->BgL_producerz00) =
					((obj_t) BGl_loadzd2producerzd2envz00zzmodule_loadz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1062z00_454))->BgL_consumerz00) =
					((obj_t) BGl_proc1195z00zzmodule_loadz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1062z00_454))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_proc1196z00zzmodule_loadz00), BUNSPEC);
				return ((obj_t) BgL_new1062z00_454);
			}
		}

	}



/* &make-load-compiler */
	obj_t BGl_z62makezd2loadzd2compilerz62zzmodule_loadz00(obj_t BgL_envz00_588)
	{
		{	/* Module/load.scm 30 */
			return BGl_makezd2loadzd2compilerz00zzmodule_loadz00();
		}

	}



/* &<@anonymous:1105> */
	obj_t BGl_z62zc3z04anonymousza31105ze3ze5zzmodule_loadz00(obj_t
		BgL_envz00_589)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(1);
		}

	}



/* &<@anonymous:1104> */
	obj_t BGl_z62zc3z04anonymousza31104ze3ze5zzmodule_loadz00(obj_t
		BgL_envz00_590, obj_t BgL_mz00_591, obj_t BgL_cz00_592)
	{
		{	/* Module/module.scm 54 */
			return BNIL;
		}

	}



/* &load-producer */
	obj_t BGl_z62loadzd2producerzb0zzmodule_loadz00(obj_t BgL_envz00_593,
		obj_t BgL_clausez00_594)
	{
		{	/* Module/load.scm 37 */
			{
				obj_t BgL_protosz00_608;

				if (PAIRP(BgL_clausez00_594))
					{	/* Module/load.scm 38 */
						obj_t BgL_arg1122z00_612;

						BgL_arg1122z00_612 = CDR(((obj_t) BgL_clausez00_594));
						{	/* Module/load.scm 38 */
							bool_t BgL_tmpz00_664;

							BgL_protosz00_608 = BgL_arg1122z00_612;
							{
								obj_t BgL_l1091z00_610;

								BgL_l1091z00_610 = BgL_protosz00_608;
							BgL_zc3z04anonymousza31123ze3z87_609:
								if (PAIRP(BgL_l1091z00_610))
									{	/* Module/load.scm 40 */
										BGl_loadzd2parserzd2zzmodule_loadz00(CAR(BgL_l1091z00_610),
											BgL_clausez00_594);
										{
											obj_t BgL_l1091z00_669;

											BgL_l1091z00_669 = CDR(BgL_l1091z00_610);
											BgL_l1091z00_610 = BgL_l1091z00_669;
											goto BgL_zc3z04anonymousza31123ze3z87_609;
										}
									}
								else
									{	/* Module/load.scm 40 */
										BgL_tmpz00_664 = ((bool_t) 1);
									}
							}
							return BBOOL(BgL_tmpz00_664);
						}
					}
				else
					{	/* Module/load.scm 38 */
						{	/* Module/load.scm 42 */
							obj_t BgL_list1126z00_611;

							BgL_list1126z00_611 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							return
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string1197z00zzmodule_loadz00,
								BGl_string1198z00zzmodule_loadz00, BgL_clausez00_594,
								BgL_list1126z00_611);
						}
					}
			}
		}

	}



/* load-parser */
	obj_t BGl_loadzd2parserzd2zzmodule_loadz00(obj_t BgL_protoz00_4,
		obj_t BgL_clausez00_5)
	{
		{	/* Module/load.scm 50 */
			{
				obj_t BgL_modulez00_475;
				obj_t BgL_filez00_476;
				obj_t BgL_filesz00_477;

				if (PAIRP(BgL_protoz00_4))
					{	/* Module/load.scm 51 */
						obj_t BgL_carzd2119zd2_483;
						obj_t BgL_cdrzd2120zd2_484;

						BgL_carzd2119zd2_483 = CAR(((obj_t) BgL_protoz00_4));
						BgL_cdrzd2120zd2_484 = CDR(((obj_t) BgL_protoz00_4));
						if (SYMBOLP(BgL_carzd2119zd2_483))
							{	/* Module/load.scm 51 */
								if (PAIRP(BgL_cdrzd2120zd2_484))
									{	/* Module/load.scm 51 */
										obj_t BgL_carzd2125zd2_487;

										BgL_carzd2125zd2_487 = CAR(BgL_cdrzd2120zd2_484);
										if (STRINGP(BgL_carzd2125zd2_487))
											{	/* Module/load.scm 51 */
												BgL_modulez00_475 = BgL_carzd2119zd2_483;
												BgL_filez00_476 = BgL_carzd2125zd2_487;
												BgL_filesz00_477 = CDR(BgL_cdrzd2120zd2_484);
												{
													obj_t BgL_fz00_495;

													BgL_fz00_495 = BgL_filesz00_477;
												BgL_zc3z04anonymousza31136ze3z87_496:
													if (NULLP(BgL_fz00_495))
														{	/* Module/load.scm 56 */
															obj_t BgL_arg1138z00_498;

															BgL_arg1138z00_498 =
																MAKE_YOUNG_PAIR(BgL_filez00_476,
																BgL_filesz00_477);
															return
																BGl_loadzd2modulezd2zzread_loadz00
																(BgL_modulez00_475, BgL_arg1138z00_498);
														}
													else
														{	/* Module/load.scm 57 */
															bool_t BgL_test1227z00_691;

															{	/* Module/load.scm 57 */
																obj_t BgL_tmpz00_692;

																BgL_tmpz00_692 = CAR(((obj_t) BgL_fz00_495));
																BgL_test1227z00_691 = STRINGP(BgL_tmpz00_692);
															}
															if (BgL_test1227z00_691)
																{	/* Module/load.scm 60 */
																	obj_t BgL_arg1142z00_501;

																	BgL_arg1142z00_501 =
																		CDR(((obj_t) BgL_fz00_495));
																	{
																		obj_t BgL_fz00_698;

																		BgL_fz00_698 = BgL_arg1142z00_501;
																		BgL_fz00_495 = BgL_fz00_698;
																		goto BgL_zc3z04anonymousza31136ze3z87_496;
																	}
																}
															else
																{	/* Module/load.scm 58 */
																	obj_t BgL_list1143z00_502;

																	BgL_list1143z00_502 =
																		MAKE_YOUNG_PAIR(BNIL, BNIL);
																	return
																		BGl_userzd2errorzd2zztools_errorz00
																		(BGl_string1197z00zzmodule_loadz00,
																		BGl_string1199z00zzmodule_loadz00,
																		BgL_clausez00_5, BgL_list1143z00_502);
																}
														}
												}
											}
										else
											{	/* Module/load.scm 51 */
											BgL_tagzd2111zd2_480:
												{	/* Module/load.scm 68 */
													obj_t BgL_list1159z00_526;

													BgL_list1159z00_526 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													return
														BGl_userzd2errorzd2zztools_errorz00
														(BGl_string1197z00zzmodule_loadz00,
														BGl_string1199z00zzmodule_loadz00, BgL_clausez00_5,
														BgL_list1159z00_526);
												}
											}
									}
								else
									{	/* Module/load.scm 51 */
										goto BgL_tagzd2111zd2_480;
									}
							}
						else
							{	/* Module/load.scm 51 */
								goto BgL_tagzd2111zd2_480;
							}
					}
				else
					{	/* Module/load.scm 51 */
						if (SYMBOLP(BgL_protoz00_4))
							{	/* Module/load.scm 51 */
								{	/* Module/load.scm 62 */
									obj_t BgL_abasez00_505;

									{	/* Module/load.scm 62 */
										obj_t BgL_l1093z00_510;

										BgL_l1093z00_510 =
											BGl_za2accesszd2filesza2zd2zzengine_paramz00;
										if (NULLP(BgL_l1093z00_510))
											{	/* Module/load.scm 62 */
												BgL_abasez00_505 = BNIL;
											}
										else
											{	/* Module/load.scm 62 */
												obj_t BgL_head1095z00_512;

												{	/* Module/load.scm 62 */
													obj_t BgL_arg1157z00_524;

													{	/* Module/load.scm 62 */
														obj_t BgL_arg1158z00_525;

														BgL_arg1158z00_525 =
															CAR(((obj_t) BgL_l1093z00_510));
														BgL_arg1157z00_524 =
															BGl_dirnamez00zz__osz00(BgL_arg1158z00_525);
													}
													BgL_head1095z00_512 =
														MAKE_YOUNG_PAIR(BgL_arg1157z00_524, BNIL);
												}
												{	/* Module/load.scm 62 */
													obj_t BgL_g1098z00_513;

													BgL_g1098z00_513 = CDR(((obj_t) BgL_l1093z00_510));
													{
														obj_t BgL_l1093z00_515;
														obj_t BgL_tail1096z00_516;

														BgL_l1093z00_515 = BgL_g1098z00_513;
														BgL_tail1096z00_516 = BgL_head1095z00_512;
													BgL_zc3z04anonymousza31150ze3z87_517:
														if (NULLP(BgL_l1093z00_515))
															{	/* Module/load.scm 62 */
																BgL_abasez00_505 = BgL_head1095z00_512;
															}
														else
															{	/* Module/load.scm 62 */
																obj_t BgL_newtail1097z00_519;

																{	/* Module/load.scm 62 */
																	obj_t BgL_arg1153z00_521;

																	{	/* Module/load.scm 62 */
																		obj_t BgL_arg1154z00_522;

																		BgL_arg1154z00_522 =
																			CAR(((obj_t) BgL_l1093z00_515));
																		BgL_arg1153z00_521 =
																			BGl_dirnamez00zz__osz00
																			(BgL_arg1154z00_522);
																	}
																	BgL_newtail1097z00_519 =
																		MAKE_YOUNG_PAIR(BgL_arg1153z00_521, BNIL);
																}
																SET_CDR(BgL_tail1096z00_516,
																	BgL_newtail1097z00_519);
																{	/* Module/load.scm 62 */
																	obj_t BgL_arg1152z00_520;

																	BgL_arg1152z00_520 =
																		CDR(((obj_t) BgL_l1093z00_515));
																	{
																		obj_t BgL_tail1096z00_724;
																		obj_t BgL_l1093z00_723;

																		BgL_l1093z00_723 = BgL_arg1152z00_520;
																		BgL_tail1096z00_724 =
																			BgL_newtail1097z00_519;
																		BgL_tail1096z00_516 = BgL_tail1096z00_724;
																		BgL_l1093z00_515 = BgL_l1093z00_723;
																		goto BgL_zc3z04anonymousza31150ze3z87_517;
																	}
																}
															}
													}
												}
											}
									}
									{	/* Module/load.scm 62 */
										obj_t BgL_bz00_506;

										{	/* Module/load.scm 63 */
											obj_t BgL_fun1148z00_509;

											BgL_fun1148z00_509 =
												BGl_bigloozd2modulezd2resolverz00zz__modulez00();
											BgL_bz00_506 =
												BGL_PROCEDURE_CALL3(BgL_fun1148z00_509, BgL_protoz00_4,
												BNIL, BgL_abasez00_505);
										}
										{	/* Module/load.scm 63 */

											if (PAIRP(BgL_bz00_506))
												{	/* Module/load.scm 64 */
													return
														BGl_loadzd2modulezd2zzread_loadz00(BgL_protoz00_4,
														BgL_bz00_506);
												}
											else
												{	/* Module/load.scm 64 */
													return
														BGl_userzd2errorzd2zztools_errorz00
														(BGl_string1200z00zzmodule_loadz00,
														BGl_string1201z00zzmodule_loadz00, BgL_protoz00_4,
														BNIL);
												}
										}
									}
								}
							}
						else
							{	/* Module/load.scm 51 */
								goto BgL_tagzd2111zd2_480;
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_loadz00(void)
	{
		{	/* Module/load.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_loadz00(void)
	{
		{	/* Module/load.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_loadz00(void)
	{
		{	/* Module/load.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_loadz00(void)
	{
		{	/* Module/load.scm 15 */
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
			return
				BGl_modulezd2initializa7ationz75zzread_loadz00(52515292L,
				BSTRING_TO_STRING(BGl_string1202z00zzmodule_loadz00));
		}

	}

#ifdef __cplusplus
}
#endif
