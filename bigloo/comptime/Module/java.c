/*===========================================================================*/
/*   (Module/java.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/java.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_JAVA_TYPE_DEFINITIONS
#define BGL_MODULE_JAVA_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_jklassz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_srcz00;
		obj_t BgL_locz00;
		obj_t BgL_idz00;
		obj_t BgL_iddz00;
		obj_t BgL_jnamez00;
		obj_t BgL_packagez00;
		obj_t BgL_fieldsz00;
		obj_t BgL_methodsz00;
		obj_t BgL_constructorsz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_modulez00;
	}                *BgL_jklassz00_bglt;

	typedef struct BgL_jmethodz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_srcz00;
		obj_t BgL_idz00;
		obj_t BgL_argsz00;
		obj_t BgL_jnamez00;
		obj_t BgL_modifiersz00;
	}                 *BgL_jmethodz00_bglt;

	typedef struct BgL_jconstructorz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_srcz00;
		obj_t BgL_idz00;
		obj_t BgL_argsz00;
		obj_t BgL_jnamez00;
		obj_t BgL_modifiersz00;
	}                      *BgL_jconstructorz00_bglt;


#endif													// BGL_MODULE_JAVA_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_heapzd2addzd2jclassz12z12zzmodule_javaz00(obj_t);
	static BgL_jklassz00_bglt BGl_z62lambda1936z62zzmodule_javaz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_jklassz00_bglt BGl_z62lambda1939z62zzmodule_javaz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32011ze3ze5zzmodule_javaz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32003ze3ze5zzmodule_javaz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_javaz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31774ze3ze5zzmodule_javaz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1945z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1946z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1950z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1951z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1955z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1956z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza32021ze3ze5zzmodule_javaz00(obj_t,
		obj_t);
	extern BgL_typez00_bglt
		BGl_declarezd2javazd2classzd2typez12zc0zzobject_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_javazd2producerzd2zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1960z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1961z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1966z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1967z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_declarezd2jklasszd2zzmodule_javaz00(BgL_jklassz00_bglt);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzmodule_javaz00(void);
	static obj_t BGl_z62lambda1973z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2javazd2compilerz62zzmodule_javaz00(obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62lambda1974z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_za2jclassesza2z00zzmodule_javaz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62lambda1980z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1981z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_javaz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62lambda1987z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1988z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62heapzd2addzd2jclassz12z70zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31230ze3ze5zzmodule_javaz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_javaz00(void);
	static obj_t BGl_z62lambda1994z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1995z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31231ze3ze5zzmodule_javaz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_findzd2javazd2classz00zzmodule_javaz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31982ze3ze5zzmodule_javaz00(obj_t);
	static bool_t BGl_everyze70ze7zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62findzd2javazd2classz62zzmodule_javaz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_javazd2parsezd2classz00zzmodule_javaz00(obj_t, obj_t, obj_t,
		bool_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31975ze3ze5zzmodule_javaz00(obj_t);
	static obj_t BGl_javazd2parserzd2zzmodule_javaz00(obj_t, obj_t);
	extern obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32051ze3ze5zzmodule_javaz00(obj_t);
	static obj_t BGl_javazd2declarezd2arrayz00zzmodule_javaz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	static obj_t BGl_z62zc3z04anonymousza31968ze3ze5zzmodule_javaz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_rempropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2jvmzd2foreignzd2classzd2idza2zd2zzengine_paramz00;
	static obj_t BGl_methodzd2initzd2zzmodule_javaz00(void);
	static bool_t BGl_modifierzd2listzf3ze70zc6zzmodule_javaz00(obj_t);
	static obj_t BGl_za2jexportedza2z00zzmodule_javaz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t);
	static obj_t BGl_za2javazd2classesza2zd2zzmodule_javaz00 = BUNSPEC;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32063ze3ze5zzmodule_javaz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31996ze3ze5zzmodule_javaz00(obj_t);
	static obj_t BGl_z62lambda2001z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2002z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_jmethodz00zzmodule_javaz00 = BUNSPEC;
	static obj_t BGl_z62lambda2009z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31989ze3ze5zzmodule_javaz00(obj_t);
	static bool_t BGl_javazd2declarezd2classz00zzmodule_javaz00(obj_t, obj_t,
		obj_t, obj_t, bool_t, obj_t);
	static obj_t BGl_z62lambda2010z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static BgL_jmethodz00_bglt BGl_z62lambda2017z62zzmodule_javaz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_jmethodz00_bglt BGl_z62lambda2019z62zzmodule_javaz00(obj_t);
	static BgL_typez00_bglt
		BGl_declarezd2javazd2classz12z12zzmodule_javaz00(BgL_jklassz00_bglt);
	static obj_t BGl_z62lambda2026z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2027z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_jclassz00zzobject_classz00;
	static obj_t BGl_jklassz00zzmodule_javaz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	static obj_t BGl_z62lambda2032z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2033z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	static obj_t BGl_z62lambda2038z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2039z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t
		BGl_importzd2javazd2classzd2accessorsz12zc0zzobject_javazd2accesszd2(obj_t,
		obj_t, BgL_typez00_bglt, bool_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2043z62zzmodule_javaz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2044z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2javazd2compilerz00zzmodule_javaz00(void);
	static obj_t BGl_z62lambda2049z62zzmodule_javaz00(obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_declarezd2jvmzd2typez12z12zzforeign_jtypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzmodule_javaz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_jtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_jvmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_javazd2accesszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_foreignz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_checksumz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62lambda2050z62zzmodule_javaz00(obj_t, obj_t, obj_t);
	static BgL_jconstructorz00_bglt BGl_z62lambda2059z62zzmodule_javaz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt BGl_typezd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	static BgL_jconstructorz00_bglt BGl_z62lambda2061z62zzmodule_javaz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzmodule_javaz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_javaz00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_javaz00(void);
	BGL_IMPORT obj_t
		BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_javaz00(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	extern bool_t BGl_typezd2identzf3z21zzast_identz00(obj_t);
	static bool_t BGl_argzd2listzf3ze70zc6zzmodule_javaz00(obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31903ze3ze5zzmodule_javaz00(obj_t);
	extern obj_t BGl_foreignzd2accesseszd2addz12z12zzmodule_foreignz00(obj_t);
	static obj_t BGl_javazd2declarezd2componentz00zzmodule_javaz00(obj_t,
		BgL_jklassz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	static bool_t BGl_javazd2refinezd2classz00zzmodule_javaz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_javazd2errorzd2zzmodule_javaz00(obj_t, obj_t);
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	extern obj_t
		BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00(BgL_typez00_bglt,
		BgL_typez00_bglt, obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	extern obj_t BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00;
	static obj_t BGl_jconstructorz00zzmodule_javaz00 = BUNSPEC;
	extern obj_t BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(obj_t, obj_t,
		obj_t);
	extern BgL_typez00_bglt
		BGl_delayzd2classzd2accessorsz12z12zzmodule_classz00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31753ze3ze5zzmodule_javaz00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_za2jklassesza2z00zzmodule_javaz00 = BUNSPEC;
	static obj_t BGl_z62bindzd2jklassz12za2zzmodule_javaz00(obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza31941ze3ze5zzmodule_javaz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzmodule_javaz00(obj_t,
		obj_t);
	static obj_t BGl_z62javazd2finaliza7erz17zzmodule_javaz00(obj_t);
	static obj_t __cnst[41];


	   
		 
		DEFINE_STRING(BGl_string2120z00zzmodule_javaz00,
		BgL_bgl_string2120za700za7za7m2178za7, "Illegal Java field", 18);
	      DEFINE_STRING(BGl_string2121z00zzmodule_javaz00,
		BgL_bgl_string2121za700za7za7m2179za7,
		"Illegal Java field (wrong modifiers)", 36);
	      DEFINE_STRING(BGl_string2124z00zzmodule_javaz00,
		BgL_bgl_string2124za700za7za7m2180za7,
		"Illegal first argument of virtual method", 40);
	      DEFINE_STRING(BGl_string2125z00zzmodule_javaz00,
		BgL_bgl_string2125za700za7za7m2181za7, "Illegal array item type", 23);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2122z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2182za7,
		BGl_z62zc3z04anonymousza31774ze3ze5zzmodule_javaz00);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2123z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2183za7,
		BGl_z62zc3z04anonymousza31771ze3ze5zzmodule_javaz00);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzmodule_javaz00,
		BgL_bgl_za762lambda1946za7622184z00, BGl_z62lambda1946z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzmodule_javaz00,
		BgL_bgl_za762lambda1945za7622185z00, BGl_z62lambda1945z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zzmodule_javaz00,
		BgL_bgl_za762lambda1951za7622186z00, BGl_z62lambda1951z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zzmodule_javaz00,
		BgL_bgl_za762lambda1950za7622187z00, BGl_z62lambda1950z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zzmodule_javaz00,
		BgL_bgl_za762lambda1956za7622188z00, BGl_z62lambda1956z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzmodule_javaz00,
		BgL_bgl_za762lambda1955za7622189z00, BGl_z62lambda1955z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzmodule_javaz00,
		BgL_bgl_za762lambda1961za7622190z00, BGl_z62lambda1961z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzmodule_javaz00,
		BgL_bgl_za762lambda1960za7622191z00, BGl_z62lambda1960z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2134z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2192za7,
		BGl_z62zc3z04anonymousza31968ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zzmodule_javaz00,
		BgL_bgl_za762lambda1967za7622193z00, BGl_z62lambda1967z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2136z00zzmodule_javaz00,
		BgL_bgl_za762lambda1966za7622194z00, BGl_z62lambda1966z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2137z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2195za7,
		BGl_z62zc3z04anonymousza31975ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2138z00zzmodule_javaz00,
		BgL_bgl_za762lambda1974za7622196z00, BGl_z62lambda1974z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2139z00zzmodule_javaz00,
		BgL_bgl_za762lambda1973za7622197z00, BGl_z62lambda1973z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2140z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2198za7,
		BGl_z62zc3z04anonymousza31982ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2141z00zzmodule_javaz00,
		BgL_bgl_za762lambda1981za7622199z00, BGl_z62lambda1981z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2142z00zzmodule_javaz00,
		BgL_bgl_za762lambda1980za7622200z00, BGl_z62lambda1980z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2143z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2201za7,
		BGl_z62zc3z04anonymousza31989ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzmodule_javaz00,
		BgL_bgl_za762lambda1988za7622202z00, BGl_z62lambda1988z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2145z00zzmodule_javaz00,
		BgL_bgl_za762lambda1987za7622203z00, BGl_z62lambda1987z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2204za7,
		BGl_z62zc3z04anonymousza31996ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzmodule_javaz00,
		BgL_bgl_za762lambda1995za7622205z00, BGl_z62lambda1995z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzmodule_javaz00,
		BgL_bgl_za762lambda1994za7622206z00, BGl_z62lambda1994z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2207za7,
		BGl_z62zc3z04anonymousza32003ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzmodule_javaz00,
		BgL_bgl_za762lambda2002za7622208z00, BGl_z62lambda2002z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzmodule_javaz00,
		BgL_bgl_za762lambda2001za7622209z00, BGl_z62lambda2001z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2210za7,
		BGl_z62zc3z04anonymousza32011ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2153z00zzmodule_javaz00,
		BgL_bgl_za762lambda2010za7622211z00, BGl_z62lambda2010z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzmodule_javaz00,
		BgL_bgl_za762lambda2009za7622212z00, BGl_z62lambda2009z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2213za7,
		BGl_z62zc3z04anonymousza31941ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzmodule_javaz00,
		BgL_bgl_za762lambda1939za7622214z00, BGl_z62lambda1939z62zzmodule_javaz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzmodule_javaz00,
		BgL_bgl_za762lambda1936za7622215z00, BGl_z62lambda1936z62zzmodule_javaz00,
		0L, BUNSPEC, 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzmodule_javaz00,
		BgL_bgl_za762lambda2027za7622216z00, BGl_z62lambda2027z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzmodule_javaz00,
		BgL_bgl_za762lambda2026za7622217z00, BGl_z62lambda2026z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzmodule_javaz00,
		BgL_bgl_za762lambda2033za7622218z00, BGl_z62lambda2033z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzmodule_javaz00,
		BgL_bgl_za762lambda2032za7622219z00, BGl_z62lambda2032z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2162z00zzmodule_javaz00,
		BgL_bgl_za762lambda2039za7622220z00, BGl_z62lambda2039z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzmodule_javaz00,
		BgL_bgl_za762lambda2038za7622221z00, BGl_z62lambda2038z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzmodule_javaz00,
		BgL_bgl_za762lambda2044za7622222z00, BGl_z62lambda2044z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2165z00zzmodule_javaz00,
		BgL_bgl_za762lambda2043za7622223z00, BGl_z62lambda2043z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2224za7,
		BGl_z62zc3z04anonymousza32051ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzmodule_javaz00,
		BgL_bgl_za762lambda2050za7622225z00, BGl_z62lambda2050z62zzmodule_javaz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzmodule_javaz00,
		BgL_bgl_za762lambda2049za7622226z00, BGl_z62lambda2049z62zzmodule_javaz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2175z00zzmodule_javaz00,
		BgL_bgl_string2175za700za7za7m2227za7, "module_java", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2228za7,
		BGl_z62zc3z04anonymousza32021ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2176z00zzmodule_javaz00,
		BgL_bgl_string2176za700za7za7m2229za7,
		"_ jconstructor jmethod modifiers bstring args module_java module bool abstract? constructors methods pair-nil fields package jname idd symbol id obj loc pair src static (public private protected static final synchronized abstract) public constructor method field foreign abstract %% - make-heap make-add-heap jklass array abstract-class class export java ",
		355);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2javazd2compilerzd2envzd2zzmodule_javaz00,
		BgL_bgl_za762makeza7d2javaza7d2230za7,
		BGl_z62makezd2javazd2compilerz62zzmodule_javaz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzmodule_javaz00,
		BgL_bgl_za762lambda2019za7622231z00, BGl_z62lambda2019z62zzmodule_javaz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzmodule_javaz00,
		BgL_bgl_za762lambda2017za7622232z00, BGl_z62lambda2017z62zzmodule_javaz00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2172z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2233za7,
		BGl_z62zc3z04anonymousza32063ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2173z00zzmodule_javaz00,
		BgL_bgl_za762lambda2061za7622234z00, BGl_z62lambda2061z62zzmodule_javaz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2174z00zzmodule_javaz00,
		BgL_bgl_za762lambda2059za7622235z00, BGl_z62lambda2059z62zzmodule_javaz00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2099z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2236za7,
		BGl_z62zc3z04anonymousza31230ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_heapzd2addzd2jclassz12zd2envzc0zzmodule_javaz00,
		BgL_bgl_za762heapza7d2addza7d22237za7,
		BGl_z62heapzd2addzd2jclassz12z70zzmodule_javaz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_javazd2finaliza7erzd2envza7zzmodule_javaz00,
		BgL_bgl_za762javaza7d2finali2238z00,
		BGl_z62javazd2finaliza7erz17zzmodule_javaz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2javazd2classzd2envzd2zzmodule_javaz00,
		BgL_bgl_za762findza7d2javaza7d2239za7,
		BGl_z62findzd2javazd2classz62zzmodule_javaz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_bindzd2jklassz12zd2envz12zzmodule_javaz00,
		BgL_bgl_za762bindza7d2jklass2240z00,
		BGl_z62bindzd2jklassz12za2zzmodule_javaz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2101z00zzmodule_javaz00,
		BgL_bgl_string2101za700za7za7m2241za7, "Illegal java variable", 21);
	      DEFINE_STRING(BGl_string2102z00zzmodule_javaz00,
		BgL_bgl_string2102za700za7za7m2242za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string2103z00zzmodule_javaz00,
		BgL_bgl_string2103za700za7za7m2243za7, "Illegal `java' clause", 21);
	      DEFINE_STRING(BGl_string2104z00zzmodule_javaz00,
		BgL_bgl_string2104za700za7za7m2244za7, "Illegal java export form", 24);
	      DEFINE_STRING(BGl_string2105z00zzmodule_javaz00,
		BgL_bgl_string2105za700za7za7m2245za7, "Illegal foreign class definition",
		32);
	      DEFINE_STRING(BGl_string2106z00zzmodule_javaz00,
		BgL_bgl_string2106za700za7za7m2246za7, "", 0);
	      DEFINE_STRING(BGl_string2107z00zzmodule_javaz00,
		BgL_bgl_string2107za700za7za7m2247za7, "Can't find main class declaration",
		33);
	      DEFINE_STRING(BGl_string2108z00zzmodule_javaz00,
		BgL_bgl_string2108za700za7za7m2248za7, "Super class is not a Java class",
		31);
	      DEFINE_STRING(BGl_string2109z00zzmodule_javaz00,
		BgL_bgl_string2109za700za7za7m2249za7, "Java", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2100z00zzmodule_javaz00,
		BgL_bgl_za762za7c3za704anonymo2250za7,
		BGl_z62zc3z04anonymousza31231ze3ze5zzmodule_javaz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2110z00zzmodule_javaz00,
		BgL_bgl_string2110za700za7za7m2251za7,
		"Re-exportation of global variable (ignored)", 43);
	      DEFINE_STRING(BGl_string2111z00zzmodule_javaz00,
		BgL_bgl_string2111za700za7za7m2252za7,
		"Unbound (or static) global variable", 35);
	      DEFINE_STRING(BGl_string2112z00zzmodule_javaz00,
		BgL_bgl_string2112za700za7za7m2253za7, "Illegal java class", 18);
	      DEFINE_STRING(BGl_string2113z00zzmodule_javaz00,
		BgL_bgl_string2113za700za7za7m2254za7, "Illegal Java class redefinition",
		31);
	      DEFINE_STRING(BGl_string2114z00zzmodule_javaz00,
		BgL_bgl_string2114za700za7za7m2255za7, "Illegal class field", 19);
	      DEFINE_STRING(BGl_string2115z00zzmodule_javaz00,
		BgL_bgl_string2115za700za7za7m2256za7, "Illegal class field `", 21);
	      DEFINE_STRING(BGl_string2116z00zzmodule_javaz00,
		BgL_bgl_string2116za700za7za7m2257za7, "'", 1);
	      DEFINE_STRING(BGl_string2117z00zzmodule_javaz00,
		BgL_bgl_string2117za700za7za7m2258za7, "<init>", 6);
	      DEFINE_STRING(BGl_string2118z00zzmodule_javaz00,
		BgL_bgl_string2118za700za7za7m2259za7, "Illegal Java method", 19);
	      DEFINE_STRING(BGl_string2119z00zzmodule_javaz00,
		BgL_bgl_string2119za700za7za7m2260za7,
		"Illegal Java method (wrong modifiers)", 37);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzmodule_javaz00));
		     ADD_ROOT((void *) (&BGl_za2jclassesza2z00zzmodule_javaz00));
		     ADD_ROOT((void *) (&BGl_za2jexportedza2z00zzmodule_javaz00));
		     ADD_ROOT((void *) (&BGl_za2javazd2classesza2zd2zzmodule_javaz00));
		     ADD_ROOT((void *) (&BGl_jmethodz00zzmodule_javaz00));
		     ADD_ROOT((void *) (&BGl_jklassz00zzmodule_javaz00));
		     ADD_ROOT((void *) (&BGl_jconstructorz00zzmodule_javaz00));
		     ADD_ROOT((void *) (&BGl_za2jklassesza2z00zzmodule_javaz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_javaz00(long
		BgL_checksumz00_2824, char *BgL_fromz00_2825)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_javaz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_javaz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_javaz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_javaz00();
					BGl_cnstzd2initzd2zzmodule_javaz00();
					BGl_importedzd2moduleszd2initz00zzmodule_javaz00();
					BGl_objectzd2initzd2zzmodule_javaz00();
					return BGl_toplevelzd2initzd2zzmodule_javaz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "module_java");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_java");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_java");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "module_java");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_java");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"module_java");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "module_java");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"module_java");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "module_java");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_java");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "module_java");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_java");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_java");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 15 */
			{	/* Module/java.scm 15 */
				obj_t BgL_cportz00_2650;

				{	/* Module/java.scm 15 */
					obj_t BgL_stringz00_2657;

					BgL_stringz00_2657 = BGl_string2176z00zzmodule_javaz00;
					{	/* Module/java.scm 15 */
						obj_t BgL_startz00_2658;

						BgL_startz00_2658 = BINT(0L);
						{	/* Module/java.scm 15 */
							obj_t BgL_endz00_2659;

							BgL_endz00_2659 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2657)));
							{	/* Module/java.scm 15 */

								BgL_cportz00_2650 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2657, BgL_startz00_2658, BgL_endz00_2659);
				}}}}
				{
					long BgL_iz00_2651;

					BgL_iz00_2651 = 40L;
				BgL_loopz00_2652:
					if ((BgL_iz00_2651 == -1L))
						{	/* Module/java.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/java.scm 15 */
							{	/* Module/java.scm 15 */
								obj_t BgL_arg2177z00_2653;

								{	/* Module/java.scm 15 */

									{	/* Module/java.scm 15 */
										obj_t BgL_locationz00_2655;

										BgL_locationz00_2655 = BBOOL(((bool_t) 0));
										{	/* Module/java.scm 15 */

											BgL_arg2177z00_2653 =
												BGl_readz00zz__readerz00(BgL_cportz00_2650,
												BgL_locationz00_2655);
										}
									}
								}
								{	/* Module/java.scm 15 */
									int BgL_tmpz00_2857;

									BgL_tmpz00_2857 = (int) (BgL_iz00_2651);
									CNST_TABLE_SET(BgL_tmpz00_2857, BgL_arg2177z00_2653);
							}}
							{	/* Module/java.scm 15 */
								int BgL_auxz00_2656;

								BgL_auxz00_2656 = (int) ((BgL_iz00_2651 - 1L));
								{
									long BgL_iz00_2862;

									BgL_iz00_2862 = (long) (BgL_auxz00_2656);
									BgL_iz00_2651 = BgL_iz00_2862;
									goto BgL_loopz00_2652;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 15 */
			BGl_za2jklassesza2z00zzmodule_javaz00 = BNIL;
			BGl_za2jclassesza2z00zzmodule_javaz00 = BNIL;
			BGl_za2jexportedza2z00zzmodule_javaz00 = BNIL;
			return (BGl_za2javazd2classesza2zd2zzmodule_javaz00 = BNIL, BUNSPEC);
		}

	}



/* make-java-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2javazd2compilerz00zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 72 */
			{	/* Module/java.scm 73 */
				BgL_ccompz00_bglt BgL_new1107z00_1009;

				{	/* Module/java.scm 74 */
					BgL_ccompz00_bglt BgL_new1106z00_1015;

					BgL_new1106z00_1015 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/java.scm 74 */
						long BgL_arg1232z00_1016;

						BgL_arg1232z00_1016 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1106z00_1015), BgL_arg1232z00_1016);
					}
					BgL_new1107z00_1009 = BgL_new1106z00_1015;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1107z00_1009))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1107z00_1009))->BgL_producerz00) =
					((obj_t) BGl_proc2099z00zzmodule_javaz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1107z00_1009))->BgL_consumerz00) =
					((obj_t) BGl_proc2100z00zzmodule_javaz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1107z00_1009))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_javazd2finaliza7erzd2envza7zzmodule_javaz00), BUNSPEC);
				return ((obj_t) BgL_new1107z00_1009);
			}
		}

	}



/* &make-java-compiler */
	obj_t BGl_z62makezd2javazd2compilerz62zzmodule_javaz00(obj_t BgL_envz00_2453)
	{
		{	/* Module/java.scm 72 */
			return BGl_makezd2javazd2compilerz00zzmodule_javaz00();
		}

	}



/* &<@anonymous:1231> */
	obj_t BGl_z62zc3z04anonymousza31231ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2454, obj_t BgL_mz00_2455, obj_t BgL_cz00_2456)
	{
		{	/* Module/java.scm 76 */
			return
				BGl_javazd2producerzd2zzmodule_javaz00(BgL_mz00_2455, BgL_cz00_2456);
		}

	}



/* &<@anonymous:1230> */
	obj_t BGl_z62zc3z04anonymousza31230ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2457, obj_t BgL_cz00_2458)
	{
		{	/* Module/java.scm 75 */
			return
				BGl_javazd2producerzd2zzmodule_javaz00
				(BGl_za2moduleza2z00zzmodule_modulez00, BgL_cz00_2458);
		}

	}



/* java-error */
	obj_t BGl_javazd2errorzd2zzmodule_javaz00(obj_t BgL_javaz00_112,
		obj_t BgL_msgz00_113)
	{
		{	/* Module/java.scm 82 */
			{	/* Module/java.scm 84 */
				obj_t BgL_arg1233z00_1017;

				if (PAIRP(BgL_msgz00_113))
					{	/* Module/java.scm 84 */
						BgL_arg1233z00_1017 = CAR(BgL_msgz00_113);
					}
				else
					{	/* Module/java.scm 84 */
						BgL_arg1233z00_1017 = BGl_string2101z00zzmodule_javaz00;
					}
				{	/* Module/java.scm 83 */
					obj_t BgL_list1234z00_1018;

					BgL_list1234z00_1018 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					return
						BGl_userzd2errorzd2zztools_errorz00
						(BGl_string2102z00zzmodule_javaz00, BgL_arg1233z00_1017,
						BgL_javaz00_112, BgL_list1234z00_1018);
				}
			}
		}

	}



/* java-producer */
	obj_t BGl_javazd2producerzd2zzmodule_javaz00(obj_t BgL_modulez00_114,
		obj_t BgL_clausez00_115)
	{
		{	/* Module/java.scm 91 */
			{	/* Module/java.scm 92 */
				bool_t BgL_test2264z00_2883;

				{	/* Module/java.scm 92 */
					obj_t BgL_arg1252z00_1037;

					{	/* Module/java.scm 92 */
						obj_t BgL_arg1268z00_1038;

						BgL_arg1268z00_1038 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_arg1252z00_1037 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1268z00_1038)))->
							BgL_foreignzd2clausezd2supportz00);
					}
					BgL_test2264z00_2883 =
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(0),
							BgL_arg1252z00_1037));
				}
				if (BgL_test2264z00_2883)
					{
						obj_t BgL_protosz00_1023;

						if (PAIRP(BgL_clausez00_115))
							{	/* Module/java.scm 93 */
								obj_t BgL_arg1242z00_1028;

								BgL_arg1242z00_1028 = CDR(((obj_t) BgL_clausez00_115));
								BgL_protosz00_1023 = BgL_arg1242z00_1028;
								{
									obj_t BgL_l1185z00_1030;

									BgL_l1185z00_1030 = BgL_protosz00_1023;
								BgL_zc3z04anonymousza31243ze3z87_1031:
									if (PAIRP(BgL_l1185z00_1030))
										{	/* Module/java.scm 95 */
											BGl_javazd2parserzd2zzmodule_javaz00(CAR
												(BgL_l1185z00_1030), BgL_modulez00_114);
											{
												obj_t BgL_l1185z00_2898;

												BgL_l1185z00_2898 = CDR(BgL_l1185z00_1030);
												BgL_l1185z00_1030 = BgL_l1185z00_2898;
												goto BgL_zc3z04anonymousza31243ze3z87_1031;
											}
										}
									else
										{	/* Module/java.scm 95 */
											((bool_t) 1);
										}
								}
								return BNIL;
							}
						else
							{	/* Module/java.scm 93 */
								{	/* Module/java.scm 98 */
									obj_t BgL_list1249z00_1036;

									BgL_list1249z00_1036 =
										MAKE_YOUNG_PAIR(BGl_string2103z00zzmodule_javaz00, BNIL);
									BGL_TAIL return
										BGl_javazd2errorzd2zzmodule_javaz00(BgL_clausez00_115,
										BgL_list1249z00_1036);
								}
							}
					}
				else
					{	/* Module/java.scm 92 */
						return BNIL;
					}
			}
		}

	}



/* java-parser */
	obj_t BGl_javazd2parserzd2zzmodule_javaz00(obj_t BgL_javaz00_116,
		obj_t BgL_modulez00_117)
	{
		{	/* Module/java.scm 104 */
			{

				if (PAIRP(BgL_javaz00_116))
					{	/* Module/java.scm 106 */
						obj_t BgL_cdrzd2382zd2_1055;

						BgL_cdrzd2382zd2_1055 = CDR(((obj_t) BgL_javaz00_116));
						if ((CAR(((obj_t) BgL_javaz00_116)) == CNST_TABLE_REF(1)))
							{	/* Module/java.scm 106 */
								if (PAIRP(BgL_cdrzd2382zd2_1055))
									{	/* Module/java.scm 106 */
										obj_t BgL_carzd2385zd2_1059;
										obj_t BgL_cdrzd2386zd2_1060;

										BgL_carzd2385zd2_1059 = CAR(BgL_cdrzd2382zd2_1055);
										BgL_cdrzd2386zd2_1060 = CDR(BgL_cdrzd2382zd2_1055);
										if (SYMBOLP(BgL_carzd2385zd2_1059))
											{	/* Module/java.scm 106 */
												if (PAIRP(BgL_cdrzd2386zd2_1060))
													{	/* Module/java.scm 106 */
														obj_t BgL_carzd2391zd2_1063;

														BgL_carzd2391zd2_1063 = CAR(BgL_cdrzd2386zd2_1060);
														if (STRINGP(BgL_carzd2391zd2_1063))
															{	/* Module/java.scm 106 */
																if (NULLP(CDR(BgL_cdrzd2386zd2_1060)))
																	{	/* Module/java.scm 109 */
																		obj_t BgL_arg1331z00_1909;

																		BgL_arg1331z00_1909 =
																			MAKE_YOUNG_PAIR(BgL_javaz00_116,
																			BgL_modulez00_117);
																		return
																			(BGl_za2jexportedza2z00zzmodule_javaz00 =
																			MAKE_YOUNG_PAIR(BgL_arg1331z00_1909,
																				BGl_za2jexportedza2z00zzmodule_javaz00),
																			BUNSPEC);
																	}
																else
																	{	/* Module/java.scm 106 */
																	BgL_tagzd2368zd2_1042:
																		{	/* Module/java.scm 111 */
																			obj_t BgL_list1332z00_1098;

																			BgL_list1332z00_1098 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2104z00zzmodule_javaz00,
																				BNIL);
																			BGL_TAIL return
																				BGl_javazd2errorzd2zzmodule_javaz00
																				(BgL_javaz00_116, BgL_list1332z00_1098);
																		}
																	}
															}
														else
															{	/* Module/java.scm 106 */
																goto BgL_tagzd2368zd2_1042;
															}
													}
												else
													{	/* Module/java.scm 106 */
														goto BgL_tagzd2368zd2_1042;
													}
											}
										else
											{	/* Module/java.scm 106 */
												goto BgL_tagzd2368zd2_1042;
											}
									}
								else
									{	/* Module/java.scm 106 */
										goto BgL_tagzd2368zd2_1042;
									}
							}
						else
							{	/* Module/java.scm 106 */
								if ((CAR(((obj_t) BgL_javaz00_116)) == CNST_TABLE_REF(2)))
									{	/* Module/java.scm 106 */
										if (PAIRP(BgL_cdrzd2382zd2_1055))
											{	/* Module/java.scm 106 */
												return
													BGl_javazd2parsezd2classz00zzmodule_javaz00
													(BgL_javaz00_116, CAR(BgL_cdrzd2382zd2_1055),
													CDR(BgL_cdrzd2382zd2_1055), ((bool_t) 0),
													BgL_modulez00_117);
											}
										else
											{	/* Module/java.scm 106 */
											BgL_tagzd2372zd2_1052:
												BGL_TAIL return
													BGl_javazd2errorzd2zzmodule_javaz00(BgL_javaz00_116,
													BNIL);
											}
									}
								else
									{	/* Module/java.scm 106 */
										obj_t BgL_cdrzd2442zd2_1074;

										BgL_cdrzd2442zd2_1074 = CDR(((obj_t) BgL_javaz00_116));
										if ((CAR(((obj_t) BgL_javaz00_116)) == CNST_TABLE_REF(3)))
											{	/* Module/java.scm 106 */
												if (PAIRP(BgL_cdrzd2442zd2_1074))
													{	/* Module/java.scm 106 */
														return
															BGl_javazd2parsezd2classz00zzmodule_javaz00
															(BgL_javaz00_116, CAR(BgL_cdrzd2442zd2_1074),
															CDR(BgL_cdrzd2442zd2_1074), ((bool_t) 1),
															BgL_modulez00_117);
													}
												else
													{	/* Module/java.scm 106 */
														goto BgL_tagzd2372zd2_1052;
													}
											}
										else
											{	/* Module/java.scm 106 */
												if (
													(CAR(((obj_t) BgL_javaz00_116)) == CNST_TABLE_REF(4)))
													{	/* Module/java.scm 106 */
														if (PAIRP(BgL_cdrzd2442zd2_1074))
															{	/* Module/java.scm 106 */
																obj_t BgL_carzd2461zd2_1084;
																obj_t BgL_cdrzd2462zd2_1085;

																BgL_carzd2461zd2_1084 =
																	CAR(BgL_cdrzd2442zd2_1074);
																BgL_cdrzd2462zd2_1085 =
																	CDR(BgL_cdrzd2442zd2_1074);
																if (SYMBOLP(BgL_carzd2461zd2_1084))
																	{	/* Module/java.scm 106 */
																		if (PAIRP(BgL_cdrzd2462zd2_1085))
																			{	/* Module/java.scm 106 */
																				obj_t BgL_carzd2467zd2_1088;

																				BgL_carzd2467zd2_1088 =
																					CAR(BgL_cdrzd2462zd2_1085);
																				if (SYMBOLP(BgL_carzd2467zd2_1088))
																					{	/* Module/java.scm 106 */
																						if (NULLP(CDR
																								(BgL_cdrzd2462zd2_1085)))
																							{	/* Module/java.scm 106 */
																								return
																									BGl_javazd2declarezd2arrayz00zzmodule_javaz00
																									(BgL_javaz00_116,
																									BgL_carzd2461zd2_1084,
																									BgL_carzd2467zd2_1088);
																							}
																						else
																							{	/* Module/java.scm 106 */
																								goto BgL_tagzd2372zd2_1052;
																							}
																					}
																				else
																					{	/* Module/java.scm 106 */
																						goto BgL_tagzd2372zd2_1052;
																					}
																			}
																		else
																			{	/* Module/java.scm 106 */
																				goto BgL_tagzd2372zd2_1052;
																			}
																	}
																else
																	{	/* Module/java.scm 106 */
																		goto BgL_tagzd2372zd2_1052;
																	}
															}
														else
															{	/* Module/java.scm 106 */
																goto BgL_tagzd2372zd2_1052;
															}
													}
												else
													{	/* Module/java.scm 106 */
														goto BgL_tagzd2372zd2_1052;
													}
											}
									}
							}
					}
				else
					{	/* Module/java.scm 106 */
						goto BgL_tagzd2372zd2_1052;
					}
			}
		}

	}



/* &bind-jklass! */
	obj_t BGl_z62bindzd2jklassz12za2zzmodule_javaz00(obj_t BgL_envz00_2460,
		obj_t BgL_jklassz00_2461)
	{
		{	/* Module/java.scm 157 */
			{	/* Module/java.scm 158 */
				obj_t BgL_arg1335z00_2662;

				BgL_arg1335z00_2662 =
					(((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_jklassz00_2461)))->BgL_idz00);
				BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_arg1335z00_2662,
					CNST_TABLE_REF(5),
					((obj_t) ((BgL_jklassz00_bglt) BgL_jklassz00_2461)));
			}
			return (BGl_za2jklassesza2z00zzmodule_javaz00 =
				MAKE_YOUNG_PAIR(
					((obj_t)
						((BgL_jklassz00_bglt) BgL_jklassz00_2461)),
					BGl_za2jklassesza2z00zzmodule_javaz00), BUNSPEC);
		}

	}



/* &java-finalizer */
	obj_t BGl_z62javazd2finaliza7erz17zzmodule_javaz00(obj_t BgL_envz00_2459)
	{
		{	/* Module/java.scm 167 */
			{	/* Module/java.scm 171 */
				obj_t BgL_jklassz00_2663;

				{	/* Module/java.scm 171 */
					obj_t BgL_identz00_2664;

					BgL_identz00_2664 =
						BGl_za2jvmzd2foreignzd2classzd2idza2zd2zzengine_paramz00;
					BgL_jklassz00_2663 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_identz00_2664,
						CNST_TABLE_REF(5));
				}
				{	/* Module/java.scm 172 */
					bool_t BgL_test2284z00_2983;

					{	/* Module/java.scm 172 */
						obj_t BgL_classz00_2665;

						BgL_classz00_2665 = BGl_jklassz00zzmodule_javaz00;
						if (BGL_OBJECTP(BgL_jklassz00_2663))
							{	/* Module/java.scm 172 */
								obj_t BgL_oclassz00_2666;

								{	/* Module/java.scm 172 */
									obj_t BgL_arg1815z00_2667;
									long BgL_arg1816z00_2668;

									BgL_arg1815z00_2667 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Module/java.scm 172 */
										long BgL_arg1817z00_2669;

										BgL_arg1817z00_2669 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt) BgL_jklassz00_2663));
										BgL_arg1816z00_2668 = (BgL_arg1817z00_2669 - OBJECT_TYPE);
									}
									BgL_oclassz00_2666 =
										VECTOR_REF(BgL_arg1815z00_2667, BgL_arg1816z00_2668);
								}
								BgL_test2284z00_2983 =
									(BgL_oclassz00_2666 == BgL_classz00_2665);
							}
						else
							{	/* Module/java.scm 172 */
								BgL_test2284z00_2983 = ((bool_t) 0);
							}
					}
					if (BgL_test2284z00_2983)
						{	/* Module/java.scm 173 */
							bool_t BgL_test2286z00_2992;

							{	/* Module/java.scm 173 */
								obj_t BgL_tmpz00_2993;

								BgL_tmpz00_2993 =
									(((BgL_jklassz00_bglt) COBJECT(
											((BgL_jklassz00_bglt) BgL_jklassz00_2663)))->
									BgL_jnamez00);
								BgL_test2286z00_2992 = STRINGP(BgL_tmpz00_2993);
							}
							if (BgL_test2286z00_2992)
								{	/* Module/java.scm 177 */
									bool_t BgL_test2287z00_2997;

									{	/* Module/java.scm 177 */
										obj_t BgL_arg1348z00_2670;

										BgL_arg1348z00_2670 =
											(((BgL_jklassz00_bglt) COBJECT(
													((BgL_jklassz00_bglt) BgL_jklassz00_2663)))->
											BgL_jnamez00);
										BgL_test2287z00_2997 =
											(BgL_arg1348z00_2670 ==
											BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00);
									}
									if (BgL_test2287z00_2997)
										{	/* Module/java.scm 177 */
											BFALSE;
										}
									else
										{	/* Module/java.scm 178 */
											obj_t BgL_arg1346z00_2671;

											BgL_arg1346z00_2671 =
												(((BgL_jklassz00_bglt) COBJECT(
														((BgL_jklassz00_bglt) BgL_jklassz00_2663)))->
												BgL_srcz00);
											{	/* Module/java.scm 178 */
												obj_t BgL_list1347z00_2672;

												BgL_list1347z00_2672 =
													MAKE_YOUNG_PAIR(BGl_string2105z00zzmodule_javaz00,
													BNIL);
												BGl_javazd2errorzd2zzmodule_javaz00(BgL_arg1346z00_2671,
													BgL_list1347z00_2672);
											}
										}
								}
							else
								{	/* Module/java.scm 173 */
									((((BgL_jklassz00_bglt) COBJECT(
													((BgL_jklassz00_bglt) BgL_jklassz00_2663)))->
											BgL_abstractzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
									{	/* Module/java.scm 176 */
										obj_t BgL_vz00_2673;

										BgL_vz00_2673 =
											BGl_za2jvmzd2foreignzd2classzd2nameza2zd2zzengine_paramz00;
										((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
															BgL_jklassz00_2663)))->BgL_jnamez00) =
											((obj_t) BgL_vz00_2673), BUNSPEC);
									}
								}
						}
					else
						{	/* Module/java.scm 172 */
							BFALSE;
						}
				}
			}
			{
				obj_t BgL_l1187z00_2675;

				BgL_l1187z00_2675 = BGl_za2jklassesza2z00zzmodule_javaz00;
			BgL_zc3z04anonymousza31350ze3z87_2674:
				if (PAIRP(BgL_l1187z00_2675))
					{	/* Module/java.scm 181 */
						{	/* Module/java.scm 182 */
							obj_t BgL_jklassz00_2676;

							BgL_jklassz00_2676 = CAR(BgL_l1187z00_2675);
							{	/* Module/java.scm 183 */
								obj_t BgL_arg1361z00_2677;

								BgL_arg1361z00_2677 =
									(((BgL_jklassz00_bglt) COBJECT(
											((BgL_jklassz00_bglt) BgL_jklassz00_2676)))->BgL_idz00);
								BGl_rempropz12z12zz__r4_symbols_6_4z00(BgL_arg1361z00_2677,
									CNST_TABLE_REF(5));
							}
							{	/* Module/java.scm 184 */
								bool_t BgL_test2289z00_3016;

								{	/* Module/java.scm 184 */
									obj_t BgL_tmpz00_3017;

									BgL_tmpz00_3017 =
										(((BgL_jklassz00_bglt) COBJECT(
												((BgL_jklassz00_bglt) BgL_jklassz00_2676)))->
										BgL_jnamez00);
									BgL_test2289z00_3016 = STRINGP(BgL_tmpz00_3017);
								}
								if (BgL_test2289z00_3016)
									{	/* Module/java.scm 188 */
										obj_t BgL_prefixz00_2678;

										{	/* Module/java.scm 188 */
											obj_t BgL_arg1375z00_2679;

											BgL_arg1375z00_2679 =
												(((BgL_jklassz00_bglt) COBJECT(
														((BgL_jklassz00_bglt) BgL_jklassz00_2676)))->
												BgL_jnamez00);
											BgL_prefixz00_2678 =
												BGl_prefixz00zz__osz00(BgL_arg1375z00_2679);
										}
										{	/* Module/java.scm 189 */
											bool_t BgL_test2290z00_3024;

											{	/* Module/java.scm 189 */
												obj_t BgL_arg1370z00_2680;

												BgL_arg1370z00_2680 =
													(((BgL_jklassz00_bglt) COBJECT(
															((BgL_jklassz00_bglt) BgL_jklassz00_2676)))->
													BgL_jnamez00);
												{	/* Module/java.scm 189 */
													long BgL_l1z00_2681;

													BgL_l1z00_2681 = STRING_LENGTH(BgL_prefixz00_2678);
													if (
														(BgL_l1z00_2681 ==
															STRING_LENGTH(((obj_t) BgL_arg1370z00_2680))))
														{	/* Module/java.scm 189 */
															int BgL_arg1282z00_2682;

															{	/* Module/java.scm 189 */
																char *BgL_auxz00_3034;
																char *BgL_tmpz00_3032;

																BgL_auxz00_3034 =
																	BSTRING_TO_STRING(
																	((obj_t) BgL_arg1370z00_2680));
																BgL_tmpz00_3032 =
																	BSTRING_TO_STRING(BgL_prefixz00_2678);
																BgL_arg1282z00_2682 =
																	memcmp(BgL_tmpz00_3032, BgL_auxz00_3034,
																	BgL_l1z00_2681);
															}
															BgL_test2290z00_3024 =
																((long) (BgL_arg1282z00_2682) == 0L);
														}
													else
														{	/* Module/java.scm 189 */
															BgL_test2290z00_3024 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2290z00_3024)
												{	/* Module/java.scm 189 */
													((((BgL_jklassz00_bglt) COBJECT(
																	((BgL_jklassz00_bglt) BgL_jklassz00_2676)))->
															BgL_packagez00) =
														((obj_t) BGl_string2106z00zzmodule_javaz00),
														BUNSPEC);
												}
											else
												{	/* Module/java.scm 189 */
													((((BgL_jklassz00_bglt) COBJECT(
																	((BgL_jklassz00_bglt) BgL_jklassz00_2676)))->
															BgL_packagez00) =
														((obj_t) BgL_prefixz00_2678), BUNSPEC);
												}
										}
										{	/* Module/java.scm 192 */
											BgL_typez00_bglt BgL_arg1371z00_2683;

											BgL_arg1371z00_2683 =
												BGl_declarezd2jklasszd2zzmodule_javaz00(
												((BgL_jklassz00_bglt) BgL_jklassz00_2676));
											BGl_za2jclassesza2z00zzmodule_javaz00 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg1371z00_2683),
												BGl_za2jclassesza2z00zzmodule_javaz00);
										}
									}
								else
									{	/* Module/java.scm 184 */
										BGl_za2jclassesza2z00zzmodule_javaz00 = BNIL;
										{	/* Module/java.scm 187 */
											obj_t BgL_arg1376z00_2684;

											BgL_arg1376z00_2684 =
												(((BgL_jklassz00_bglt) COBJECT(
														((BgL_jklassz00_bglt) BgL_jklassz00_2676)))->
												BgL_srcz00);
											{	/* Module/java.scm 187 */
												obj_t BgL_list1377z00_2685;

												BgL_list1377z00_2685 =
													MAKE_YOUNG_PAIR(BGl_string2107z00zzmodule_javaz00,
													BNIL);
												BGl_javazd2errorzd2zzmodule_javaz00(BgL_arg1376z00_2684,
													BgL_list1377z00_2685);
											}
										}
									}
							}
						}
						{
							obj_t BgL_l1187z00_3052;

							BgL_l1187z00_3052 = CDR(BgL_l1187z00_2675);
							BgL_l1187z00_2675 = BgL_l1187z00_3052;
							goto BgL_zc3z04anonymousza31350ze3z87_2674;
						}
					}
				else
					{	/* Module/java.scm 181 */
						((bool_t) 1);
					}
			}
			if (NULLP(BGl_za2jclassesza2z00zzmodule_javaz00))
				{	/* Module/java.scm 196 */
					((bool_t) 0);
				}
			else
				{	/* Module/java.scm 197 */
					obj_t BgL_g1192z00_2686;

					BgL_g1192z00_2686 =
						bgl_reverse_bang(BGl_za2jclassesza2z00zzmodule_javaz00);
					{
						obj_t BgL_ll1189z00_2688;
						obj_t BgL_ll1190z00_2689;

						BgL_ll1189z00_2688 = BGl_za2jklassesza2z00zzmodule_javaz00;
						BgL_ll1190z00_2689 = BgL_g1192z00_2686;
					BgL_zc3z04anonymousza31383ze3z87_2687:
						if (NULLP(BgL_ll1189z00_2688))
							{	/* Module/java.scm 197 */
								((bool_t) 1);
							}
						else
							{	/* Module/java.scm 197 */
								{	/* Module/java.scm 198 */
									obj_t BgL_jklassz00_2690;
									obj_t BgL_jclassz00_2691;

									BgL_jklassz00_2690 = CAR(((obj_t) BgL_ll1189z00_2688));
									BgL_jclassz00_2691 = CAR(((obj_t) BgL_ll1190z00_2689));
									{	/* Module/java.scm 199 */
										bool_t BgL_test2294z00_3063;

										{	/* Module/java.scm 199 */
											bool_t BgL_test2295z00_3064;

											{	/* Module/java.scm 199 */
												obj_t BgL_tmpz00_3065;

												{
													BgL_jclassz00_bglt BgL_auxz00_3066;

													{
														obj_t BgL_auxz00_3067;

														{	/* Module/java.scm 199 */
															BgL_objectz00_bglt BgL_tmpz00_3068;

															BgL_tmpz00_3068 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_jclassz00_2691));
															BgL_auxz00_3067 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3068);
														}
														BgL_auxz00_3066 =
															((BgL_jclassz00_bglt) BgL_auxz00_3067);
													}
													BgL_tmpz00_3065 =
														(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3066))->
														BgL_itszd2superzd2);
												}
												BgL_test2295z00_3064 = CBOOL(BgL_tmpz00_3065);
											}
											if (BgL_test2295z00_3064)
												{	/* Module/java.scm 199 */
													bool_t BgL_test2296z00_3075;

													{	/* Module/java.scm 199 */
														obj_t BgL_arg1437z00_2692;

														{
															BgL_jclassz00_bglt BgL_auxz00_3076;

															{
																obj_t BgL_auxz00_3077;

																{	/* Module/java.scm 199 */
																	BgL_objectz00_bglt BgL_tmpz00_3078;

																	BgL_tmpz00_3078 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_jclassz00_2691));
																	BgL_auxz00_3077 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_3078);
																}
																BgL_auxz00_3076 =
																	((BgL_jclassz00_bglt) BgL_auxz00_3077);
															}
															BgL_arg1437z00_2692 =
																(((BgL_jclassz00_bglt)
																	COBJECT(BgL_auxz00_3076))->
																BgL_itszd2superzd2);
														}
														{	/* Module/java.scm 199 */
															obj_t BgL_classz00_2693;

															BgL_classz00_2693 =
																BGl_jclassz00zzobject_classz00;
															if (BGL_OBJECTP(BgL_arg1437z00_2692))
																{	/* Module/java.scm 199 */
																	BgL_objectz00_bglt BgL_arg1807z00_2694;

																	BgL_arg1807z00_2694 =
																		(BgL_objectz00_bglt) (BgL_arg1437z00_2692);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Module/java.scm 199 */
																			long BgL_idxz00_2695;

																			BgL_idxz00_2695 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2694);
																			BgL_test2296z00_3075 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2695 + 2L)) ==
																				BgL_classz00_2693);
																		}
																	else
																		{	/* Module/java.scm 199 */
																			bool_t BgL_res2091z00_2698;

																			{	/* Module/java.scm 199 */
																				obj_t BgL_oclassz00_2699;

																				{	/* Module/java.scm 199 */
																					obj_t BgL_arg1815z00_2700;
																					long BgL_arg1816z00_2701;

																					BgL_arg1815z00_2700 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Module/java.scm 199 */
																						long BgL_arg1817z00_2702;

																						BgL_arg1817z00_2702 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2694);
																						BgL_arg1816z00_2701 =
																							(BgL_arg1817z00_2702 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2699 =
																						VECTOR_REF(BgL_arg1815z00_2700,
																						BgL_arg1816z00_2701);
																				}
																				{	/* Module/java.scm 199 */
																					bool_t BgL__ortest_1115z00_2703;

																					BgL__ortest_1115z00_2703 =
																						(BgL_classz00_2693 ==
																						BgL_oclassz00_2699);
																					if (BgL__ortest_1115z00_2703)
																						{	/* Module/java.scm 199 */
																							BgL_res2091z00_2698 =
																								BgL__ortest_1115z00_2703;
																						}
																					else
																						{	/* Module/java.scm 199 */
																							long BgL_odepthz00_2704;

																							{	/* Module/java.scm 199 */
																								obj_t BgL_arg1804z00_2705;

																								BgL_arg1804z00_2705 =
																									(BgL_oclassz00_2699);
																								BgL_odepthz00_2704 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2705);
																							}
																							if ((2L < BgL_odepthz00_2704))
																								{	/* Module/java.scm 199 */
																									obj_t BgL_arg1802z00_2706;

																									{	/* Module/java.scm 199 */
																										obj_t BgL_arg1803z00_2707;

																										BgL_arg1803z00_2707 =
																											(BgL_oclassz00_2699);
																										BgL_arg1802z00_2706 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2707, 2L);
																									}
																									BgL_res2091z00_2698 =
																										(BgL_arg1802z00_2706 ==
																										BgL_classz00_2693);
																								}
																							else
																								{	/* Module/java.scm 199 */
																									BgL_res2091z00_2698 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2296z00_3075 =
																				BgL_res2091z00_2698;
																		}
																}
															else
																{	/* Module/java.scm 199 */
																	BgL_test2296z00_3075 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2296z00_3075)
														{	/* Module/java.scm 199 */
															BgL_test2294z00_3063 = ((bool_t) 0);
														}
													else
														{	/* Module/java.scm 199 */
															BgL_test2294z00_3063 = ((bool_t) 1);
														}
												}
											else
												{	/* Module/java.scm 199 */
													BgL_test2294z00_3063 = ((bool_t) 0);
												}
										}
										if (BgL_test2294z00_3063)
											{	/* Module/java.scm 201 */
												obj_t BgL_arg1434z00_2708;

												BgL_arg1434z00_2708 =
													(((BgL_jklassz00_bglt) COBJECT(
															((BgL_jklassz00_bglt) BgL_jklassz00_2690)))->
													BgL_srcz00);
												{	/* Module/java.scm 200 */
													obj_t BgL_list1435z00_2709;

													BgL_list1435z00_2709 =
														MAKE_YOUNG_PAIR(BGl_string2108z00zzmodule_javaz00,
														BNIL);
													BGl_javazd2errorzd2zzmodule_javaz00
														(BgL_arg1434z00_2708, BgL_list1435z00_2709);
												}
											}
										else
											{	/* Module/java.scm 199 */
												BFALSE;
											}
									}
								}
								{	/* Module/java.scm 197 */
									obj_t BgL_arg1448z00_2710;
									obj_t BgL_arg1453z00_2711;

									BgL_arg1448z00_2710 = CDR(((obj_t) BgL_ll1189z00_2688));
									BgL_arg1453z00_2711 = CDR(((obj_t) BgL_ll1190z00_2689));
									{
										obj_t BgL_ll1190z00_3115;
										obj_t BgL_ll1189z00_3114;

										BgL_ll1189z00_3114 = BgL_arg1448z00_2710;
										BgL_ll1190z00_3115 = BgL_arg1453z00_2711;
										BgL_ll1190z00_2689 = BgL_ll1190z00_3115;
										BgL_ll1189z00_2688 = BgL_ll1189z00_3114;
										goto BgL_zc3z04anonymousza31383ze3z87_2687;
									}
								}
							}
					}
				}
			BGl_za2jclassesza2z00zzmodule_javaz00 = BNIL;
			{
				obj_t BgL_l1193z00_2713;

				BgL_l1193z00_2713 = BGl_za2jexportedza2z00zzmodule_javaz00;
			BgL_zc3z04anonymousza31454ze3z87_2712:
				if (PAIRP(BgL_l1193z00_2713))
					{	/* Module/java.scm 206 */
						{	/* Module/java.scm 207 */
							obj_t BgL_jmodz00_2714;

							BgL_jmodz00_2714 = CAR(BgL_l1193z00_2713);
							{	/* Module/java.scm 207 */
								obj_t BgL_javaz00_2715;

								BgL_javaz00_2715 = CAR(((obj_t) BgL_jmodz00_2714));
								{	/* Module/java.scm 207 */
									obj_t BgL_modulez00_2716;

									BgL_modulez00_2716 = CDR(((obj_t) BgL_jmodz00_2714));
									{	/* Module/java.scm 208 */
										obj_t BgL_globalz00_2717;

										{	/* Module/java.scm 209 */
											obj_t BgL_arg1489z00_2718;

											{	/* Module/java.scm 209 */
												obj_t BgL_pairz00_2719;

												BgL_pairz00_2719 = CDR(((obj_t) BgL_javaz00_2715));
												BgL_arg1489z00_2718 = CAR(BgL_pairz00_2719);
											}
											BgL_globalz00_2717 =
												BGl_findzd2globalzd2zzast_envz00(BgL_arg1489z00_2718,
												BNIL);
										}
										{	/* Module/java.scm 209 */
											obj_t BgL_namez00_2720;

											{	/* Module/java.scm 210 */
												obj_t BgL_pairz00_2721;

												{	/* Module/java.scm 210 */
													obj_t BgL_pairz00_2722;

													BgL_pairz00_2722 = CDR(((obj_t) BgL_javaz00_2715));
													BgL_pairz00_2721 = CDR(BgL_pairz00_2722);
												}
												BgL_namez00_2720 = CAR(BgL_pairz00_2721);
											}
											{	/* Module/java.scm 210 */

												{	/* Module/java.scm 212 */
													bool_t BgL_test2302z00_3131;

													{	/* Module/java.scm 212 */
														obj_t BgL_classz00_2723;

														BgL_classz00_2723 = BGl_globalz00zzast_varz00;
														if (BGL_OBJECTP(BgL_globalz00_2717))
															{	/* Module/java.scm 212 */
																BgL_objectz00_bglt BgL_arg1807z00_2724;

																BgL_arg1807z00_2724 =
																	(BgL_objectz00_bglt) (BgL_globalz00_2717);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Module/java.scm 212 */
																		long BgL_idxz00_2725;

																		BgL_idxz00_2725 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2724);
																		BgL_test2302z00_3131 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2725 + 2L)) ==
																			BgL_classz00_2723);
																	}
																else
																	{	/* Module/java.scm 212 */
																		bool_t BgL_res2092z00_2728;

																		{	/* Module/java.scm 212 */
																			obj_t BgL_oclassz00_2729;

																			{	/* Module/java.scm 212 */
																				obj_t BgL_arg1815z00_2730;
																				long BgL_arg1816z00_2731;

																				BgL_arg1815z00_2730 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Module/java.scm 212 */
																					long BgL_arg1817z00_2732;

																					BgL_arg1817z00_2732 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2724);
																					BgL_arg1816z00_2731 =
																						(BgL_arg1817z00_2732 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2729 =
																					VECTOR_REF(BgL_arg1815z00_2730,
																					BgL_arg1816z00_2731);
																			}
																			{	/* Module/java.scm 212 */
																				bool_t BgL__ortest_1115z00_2733;

																				BgL__ortest_1115z00_2733 =
																					(BgL_classz00_2723 ==
																					BgL_oclassz00_2729);
																				if (BgL__ortest_1115z00_2733)
																					{	/* Module/java.scm 212 */
																						BgL_res2092z00_2728 =
																							BgL__ortest_1115z00_2733;
																					}
																				else
																					{	/* Module/java.scm 212 */
																						long BgL_odepthz00_2734;

																						{	/* Module/java.scm 212 */
																							obj_t BgL_arg1804z00_2735;

																							BgL_arg1804z00_2735 =
																								(BgL_oclassz00_2729);
																							BgL_odepthz00_2734 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2735);
																						}
																						if ((2L < BgL_odepthz00_2734))
																							{	/* Module/java.scm 212 */
																								obj_t BgL_arg1802z00_2736;

																								{	/* Module/java.scm 212 */
																									obj_t BgL_arg1803z00_2737;

																									BgL_arg1803z00_2737 =
																										(BgL_oclassz00_2729);
																									BgL_arg1802z00_2736 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2737, 2L);
																								}
																								BgL_res2092z00_2728 =
																									(BgL_arg1802z00_2736 ==
																									BgL_classz00_2723);
																							}
																						else
																							{	/* Module/java.scm 212 */
																								BgL_res2092z00_2728 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2302z00_3131 = BgL_res2092z00_2728;
																	}
															}
														else
															{	/* Module/java.scm 212 */
																BgL_test2302z00_3131 = ((bool_t) 0);
															}
													}
													if (BgL_test2302z00_3131)
														{	/* Module/java.scm 218 */
															bool_t BgL_test2307z00_3154;

															{	/* Module/java.scm 218 */
																obj_t BgL_tmpz00_3155;

																BgL_tmpz00_3155 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_globalz00_2717))))->BgL_namez00);
																BgL_test2307z00_3154 = STRINGP(BgL_tmpz00_3155);
															}
															if (BgL_test2307z00_3154)
																{	/* Module/java.scm 218 */
																	BGl_userzd2warningzd2zztools_errorz00
																		(BGl_string2109z00zzmodule_javaz00,
																		BGl_string2110z00zzmodule_javaz00,
																		BgL_javaz00_2715);
																}
															else
																{	/* Module/java.scm 218 */
																	((((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_globalz00_2717))))->
																			BgL_namez00) =
																		((obj_t) BgL_namez00_2720), BUNSPEC);
																}
														}
													else
														{	/* Module/java.scm 213 */
															bool_t BgL_test2308z00_3164;

															{	/* Module/java.scm 213 */
																bool_t BgL_test2309z00_3165;

																if (
																	(BGl_za2passza2z00zzengine_paramz00 ==
																		CNST_TABLE_REF(6)))
																	{	/* Module/java.scm 213 */
																		BgL_test2309z00_3165 = ((bool_t) 1);
																	}
																else
																	{	/* Module/java.scm 213 */
																		BgL_test2309z00_3165 =
																			(BGl_za2passza2z00zzengine_paramz00 ==
																			CNST_TABLE_REF(7));
																	}
																if (BgL_test2309z00_3165)
																	{	/* Module/java.scm 213 */
																		BgL_test2308z00_3164 = ((bool_t) 0);
																	}
																else
																	{	/* Module/java.scm 213 */
																		BgL_test2308z00_3164 =
																			(BgL_modulez00_2716 ==
																			BGl_za2moduleza2z00zzmodule_modulez00);
																	}
															}
															if (BgL_test2308z00_3164)
																{	/* Module/java.scm 216 */
																	obj_t BgL_list1488z00_2738;

																	BgL_list1488z00_2738 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2111z00zzmodule_javaz00, BNIL);
																	BGl_javazd2errorzd2zzmodule_javaz00
																		(BgL_javaz00_2715, BgL_list1488z00_2738);
																}
															else
																{	/* Module/java.scm 213 */
																	BFALSE;
																}
														}
												}
											}
										}
									}
								}
							}
						}
						{
							obj_t BgL_l1193z00_3174;

							BgL_l1193z00_3174 = CDR(BgL_l1193z00_2713);
							BgL_l1193z00_2713 = BgL_l1193z00_3174;
							goto BgL_zc3z04anonymousza31454ze3z87_2712;
						}
					}
				else
					{	/* Module/java.scm 206 */
						((bool_t) 1);
					}
			}
			BGl_za2jexportedza2z00zzmodule_javaz00 = BNIL;
			return (BGl_za2jklassesza2z00zzmodule_javaz00 = BNIL, BUNSPEC);
		}

	}



/* java-parse-class */
	obj_t BGl_javazd2parsezd2classz00zzmodule_javaz00(obj_t BgL_javaz00_120,
		obj_t BgL_identz00_121, obj_t BgL_restz00_122,
		bool_t BgL_abstractzf3zf3_123, obj_t BgL_modulez00_124)
	{
		{	/* Module/java.scm 233 */
			{	/* Module/java.scm 234 */
				obj_t BgL_tserz00_1175;

				BgL_tserz00_1175 = bgl_reverse(BgL_restz00_122);
				{	/* Module/java.scm 234 */
					obj_t BgL_jnamez00_1176;

					if (NULLP(BgL_tserz00_1175))
						{	/* Module/java.scm 235 */
							BgL_jnamez00_1176 = BFALSE;
						}
					else
						{	/* Module/java.scm 235 */
							BgL_jnamez00_1176 = CAR(BgL_tserz00_1175);
						}
					{	/* Module/java.scm 235 */

						if (SYMBOLP(BgL_identz00_121))
							{	/* Module/java.scm 237 */
								if (STRINGP(BgL_jnamez00_1176))
									{	/* Module/java.scm 240 */
										obj_t BgL_arg1509z00_1179;

										BgL_arg1509z00_1179 = CDR(((obj_t) BgL_tserz00_1175));
										return
											BBOOL(BGl_javazd2declarezd2classz00zzmodule_javaz00
											(BgL_javaz00_120, BgL_identz00_121, BgL_jnamez00_1176,
												BgL_arg1509z00_1179, BgL_abstractzf3zf3_123,
												BgL_modulez00_124));
									}
								else
									{	/* Module/java.scm 239 */
										return
											BBOOL(BGl_javazd2refinezd2classz00zzmodule_javaz00
											(BgL_javaz00_120, BgL_identz00_121, BgL_restz00_122,
												BgL_modulez00_124));
									}
							}
						else
							{	/* Module/java.scm 238 */
								obj_t BgL_list1510z00_1180;

								BgL_list1510z00_1180 =
									MAKE_YOUNG_PAIR(BGl_string2112z00zzmodule_javaz00, BNIL);
								return
									BGl_javazd2errorzd2zzmodule_javaz00(BgL_javaz00_120,
									BgL_list1510z00_1180);
							}
					}
				}
			}
		}

	}



/* java-declare-class */
	bool_t BGl_javazd2declarezd2classz00zzmodule_javaz00(obj_t BgL_jz00_125,
		obj_t BgL_idz00_126, obj_t BgL_jnamez00_127, obj_t BgL_compz00_128,
		bool_t BgL_az00_129, obj_t BgL_modulez00_130)
	{
		{	/* Module/java.scm 247 */
			{	/* Module/java.scm 249 */
				obj_t BgL_locz00_1182;

				BgL_locz00_1182 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_jz00_125);
				{	/* Module/java.scm 249 */
					obj_t BgL_jklassz00_1183;

					{	/* Module/java.scm 250 */
						obj_t BgL_jklassz00_1191;

						BgL_jklassz00_1191 =
							BGl_getpropz00zz__r4_symbols_6_4z00(BgL_idz00_126,
							CNST_TABLE_REF(5));
						{	/* Module/java.scm 252 */
							bool_t BgL_test2314z00_3195;

							{	/* Module/java.scm 252 */
								obj_t BgL_classz00_2053;

								BgL_classz00_2053 = BGl_jklassz00zzmodule_javaz00;
								if (BGL_OBJECTP(BgL_jklassz00_1191))
									{	/* Module/java.scm 252 */
										obj_t BgL_oclassz00_2055;

										{	/* Module/java.scm 252 */
											obj_t BgL_arg1815z00_2057;
											long BgL_arg1816z00_2058;

											BgL_arg1815z00_2057 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Module/java.scm 252 */
												long BgL_arg1817z00_2059;

												BgL_arg1817z00_2059 =
													BGL_OBJECT_CLASS_NUM(
													((BgL_objectz00_bglt) BgL_jklassz00_1191));
												BgL_arg1816z00_2058 =
													(BgL_arg1817z00_2059 - OBJECT_TYPE);
											}
											BgL_oclassz00_2055 =
												VECTOR_REF(BgL_arg1815z00_2057, BgL_arg1816z00_2058);
										}
										BgL_test2314z00_3195 =
											(BgL_oclassz00_2055 == BgL_classz00_2053);
									}
								else
									{	/* Module/java.scm 252 */
										BgL_test2314z00_3195 = ((bool_t) 0);
									}
							}
							if (BgL_test2314z00_3195)
								{	/* Module/java.scm 252 */
									if (
										(BBOOL(
												(((BgL_jklassz00_bglt) COBJECT(
															((BgL_jklassz00_bglt) BgL_jklassz00_1191)))->
													BgL_abstractzf3zf3)) == BBOOL(BgL_az00_129)))
										{	/* Module/java.scm 263 */
											bool_t BgL_test2317z00_3210;

											{	/* Module/java.scm 263 */
												obj_t BgL_tmpz00_3211;

												BgL_tmpz00_3211 =
													(((BgL_jklassz00_bglt) COBJECT(
															((BgL_jklassz00_bglt) BgL_jklassz00_1191)))->
													BgL_jnamez00);
												BgL_test2317z00_3210 = STRINGP(BgL_tmpz00_3211);
											}
											if (BgL_test2317z00_3210)
												{	/* Module/java.scm 265 */
													bool_t BgL_test2318z00_3215;

													{	/* Module/java.scm 265 */
														obj_t BgL_arg1544z00_1200;

														BgL_arg1544z00_1200 =
															(((BgL_jklassz00_bglt) COBJECT(
																	((BgL_jklassz00_bglt) BgL_jklassz00_1191)))->
															BgL_jnamez00);
														{	/* Module/java.scm 265 */
															long BgL_l1z00_2070;

															BgL_l1z00_2070 =
																STRING_LENGTH(((obj_t) BgL_arg1544z00_1200));
															if (
																(BgL_l1z00_2070 ==
																	STRING_LENGTH(BgL_jnamez00_127)))
																{	/* Module/java.scm 265 */
																	int BgL_arg1282z00_2073;

																	{	/* Module/java.scm 265 */
																		char *BgL_auxz00_3226;
																		char *BgL_tmpz00_3223;

																		BgL_auxz00_3226 =
																			BSTRING_TO_STRING(BgL_jnamez00_127);
																		BgL_tmpz00_3223 =
																			BSTRING_TO_STRING(
																			((obj_t) BgL_arg1544z00_1200));
																		BgL_arg1282z00_2073 =
																			memcmp(BgL_tmpz00_3223, BgL_auxz00_3226,
																			BgL_l1z00_2070);
																	}
																	BgL_test2318z00_3215 =
																		((long) (BgL_arg1282z00_2073) == 0L);
																}
															else
																{	/* Module/java.scm 265 */
																	BgL_test2318z00_3215 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2318z00_3215)
														{	/* Module/java.scm 265 */
															BgL_jklassz00_1183 = BgL_jklassz00_1191;
														}
													else
														{	/* Module/java.scm 268 */
															obj_t BgL_list1542z00_1199;

															BgL_list1542z00_1199 =
																MAKE_YOUNG_PAIR
																(BGl_string2113z00zzmodule_javaz00, BNIL);
															BgL_jklassz00_1183 =
																BGl_javazd2errorzd2zzmodule_javaz00
																(BgL_jz00_125, BgL_list1542z00_1199);
														}
												}
											else
												{	/* Module/java.scm 263 */
													BgL_jklassz00_1183 =
														((((BgL_jklassz00_bglt) COBJECT(
																	((BgL_jklassz00_bglt) BgL_jklassz00_1191)))->
															BgL_jnamez00) =
														((obj_t) BgL_jnamez00_127), BUNSPEC);
												}
										}
									else
										{	/* Module/java.scm 262 */
											obj_t BgL_list1547z00_1202;

											BgL_list1547z00_1202 =
												MAKE_YOUNG_PAIR(BGl_string2113z00zzmodule_javaz00,
												BNIL);
											BgL_jklassz00_1183 =
												BGl_javazd2errorzd2zzmodule_javaz00(BgL_jz00_125,
												BgL_list1547z00_1202);
										}
								}
							else
								{	/* Module/java.scm 253 */
									BgL_jklassz00_bglt BgL_new1111z00_1204;

									{	/* Module/java.scm 254 */
										BgL_jklassz00_bglt BgL_new1110z00_1206;

										BgL_new1110z00_1206 =
											((BgL_jklassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_jklassz00_bgl))));
										{	/* Module/java.scm 254 */
											long BgL_arg1559z00_1207;

											BgL_arg1559z00_1207 =
												BGL_CLASS_NUM(BGl_jklassz00zzmodule_javaz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1110z00_1206),
												BgL_arg1559z00_1207);
										}
										BgL_new1111z00_1204 = BgL_new1110z00_1206;
									}
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_srcz00) = ((obj_t) BgL_jz00_125), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_locz00) = ((obj_t) BgL_locz00_1182), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_idz00) = ((obj_t) BgL_idz00_126), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_iddz00) =
										((obj_t)
											BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_idz00_126,
												BgL_locz00_1182)), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_jnamez00) = ((obj_t) BgL_jnamez00_127), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_packagez00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_fieldsz00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_methodsz00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_constructorsz00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_abstractzf3zf3) = ((bool_t) BgL_az00_129), BUNSPEC);
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1111z00_1204))->
											BgL_modulez00) = ((obj_t) BgL_modulez00_130), BUNSPEC);
									{	/* Module/java.scm 253 */
										obj_t BgL_fun1553z00_1205;

										BgL_fun1553z00_1205 =
											BGl_classzd2constructorzd2zz__objectz00
											(BGl_jklassz00zzmodule_javaz00);
										BGL_PROCEDURE_CALL1(BgL_fun1553z00_1205,
											((obj_t) BgL_new1111z00_1204));
									}
									BgL_jklassz00_1183 = ((obj_t) BgL_new1111z00_1204);
					}}}
					{	/* Module/java.scm 250 */

						{
							obj_t BgL_l1195z00_1185;

							BgL_l1195z00_1185 = BgL_compz00_128;
						BgL_zc3z04anonymousza31512ze3z87_1186:
							if (PAIRP(BgL_l1195z00_1185))
								{	/* Module/java.scm 269 */
									{	/* Module/java.scm 269 */
										obj_t BgL_cz00_1188;

										BgL_cz00_1188 = CAR(BgL_l1195z00_1185);
										BGl_javazd2declarezd2componentz00zzmodule_javaz00
											(BgL_jz00_125, ((BgL_jklassz00_bglt) BgL_jklassz00_1183),
											BgL_cz00_1188);
									}
									{
										obj_t BgL_l1195z00_3265;

										BgL_l1195z00_3265 = CDR(BgL_l1195z00_1185);
										BgL_l1195z00_1185 = BgL_l1195z00_3265;
										goto BgL_zc3z04anonymousza31512ze3z87_1186;
									}
								}
							else
								{	/* Module/java.scm 269 */
									return ((bool_t) 1);
								}
						}
					}
				}
			}
		}

	}



/* java-refine-class */
	bool_t BGl_javazd2refinezd2classz00zzmodule_javaz00(obj_t BgL_jz00_131,
		obj_t BgL_identz00_132, obj_t BgL_compz00_133, obj_t BgL_modulez00_134)
	{
		{	/* Module/java.scm 277 */
			{	/* Module/java.scm 278 */
				obj_t BgL_jklassz00_1208;

				{	/* Module/java.scm 278 */
					obj_t BgL_jklassz00_1216;

					BgL_jklassz00_1216 =
						BGl_getpropz00zz__r4_symbols_6_4z00(BgL_identz00_132,
						CNST_TABLE_REF(5));
					{	/* Module/java.scm 279 */
						bool_t BgL_test2321z00_3269;

						{	/* Module/java.scm 279 */
							obj_t BgL_classz00_2085;

							BgL_classz00_2085 = BGl_jklassz00zzmodule_javaz00;
							if (BGL_OBJECTP(BgL_jklassz00_1216))
								{	/* Module/java.scm 279 */
									obj_t BgL_oclassz00_2087;

									{	/* Module/java.scm 279 */
										obj_t BgL_arg1815z00_2089;
										long BgL_arg1816z00_2090;

										BgL_arg1815z00_2089 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Module/java.scm 279 */
											long BgL_arg1817z00_2091;

											BgL_arg1817z00_2091 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt) BgL_jklassz00_1216));
											BgL_arg1816z00_2090 = (BgL_arg1817z00_2091 - OBJECT_TYPE);
										}
										BgL_oclassz00_2087 =
											VECTOR_REF(BgL_arg1815z00_2089, BgL_arg1816z00_2090);
									}
									BgL_test2321z00_3269 =
										(BgL_oclassz00_2087 == BgL_classz00_2085);
								}
							else
								{	/* Module/java.scm 279 */
									BgL_test2321z00_3269 = ((bool_t) 0);
								}
						}
						if (BgL_test2321z00_3269)
							{	/* Module/java.scm 279 */
								BgL_jklassz00_1208 = BgL_jklassz00_1216;
							}
						else
							{	/* Module/java.scm 281 */
								BgL_jklassz00_bglt BgL_new1114z00_1218;

								{	/* Module/java.scm 282 */
									BgL_jklassz00_bglt BgL_new1113z00_1221;

									BgL_new1113z00_1221 =
										((BgL_jklassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_jklassz00_bgl))));
									{	/* Module/java.scm 282 */
										long BgL_arg1575z00_1222;

										BgL_arg1575z00_1222 =
											BGL_CLASS_NUM(BGl_jklassz00zzmodule_javaz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1113z00_1221),
											BgL_arg1575z00_1222);
									}
									BgL_new1114z00_1218 = BgL_new1113z00_1221;
								}
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_srcz00) = ((obj_t) BgL_jz00_131), BUNSPEC);
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_locz00) =
									((obj_t)
										BGl_findzd2locationzd2zztools_locationz00(BgL_jz00_131)),
									BUNSPEC);
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_idz00) = ((obj_t) BgL_identz00_132), BUNSPEC);
								{
									obj_t BgL_auxz00_3286;

									{	/* Module/java.scm 284 */
										obj_t BgL_arg1571z00_1219;

										BgL_arg1571z00_1219 =
											BGl_findzd2locationzd2zztools_locationz00(BgL_jz00_131);
										BgL_auxz00_3286 =
											BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(BgL_identz00_132,
											BgL_arg1571z00_1219);
									}
									((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
											BgL_iddz00) = ((obj_t) BgL_auxz00_3286), BUNSPEC);
								}
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_jnamez00) = ((obj_t) BUNSPEC), BUNSPEC);
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_packagez00) = ((obj_t) BUNSPEC), BUNSPEC);
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_fieldsz00) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_methodsz00) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_constructorsz00) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_abstractzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
								((((BgL_jklassz00_bglt) COBJECT(BgL_new1114z00_1218))->
										BgL_modulez00) = ((obj_t) BgL_modulez00_134), BUNSPEC);
								{	/* Module/java.scm 281 */
									obj_t BgL_fun1573z00_1220;

									BgL_fun1573z00_1220 =
										BGl_classzd2constructorzd2zz__objectz00
										(BGl_jklassz00zzmodule_javaz00);
									BGL_PROCEDURE_CALL1(BgL_fun1573z00_1220,
										((obj_t) BgL_new1114z00_1218));
								}
								BgL_jklassz00_1208 = ((obj_t) BgL_new1114z00_1218);
				}}}
				{
					obj_t BgL_l1197z00_1210;

					BgL_l1197z00_1210 = BgL_compz00_133;
				BgL_zc3z04anonymousza31560ze3z87_1211:
					if (PAIRP(BgL_l1197z00_1210))
						{	/* Module/java.scm 287 */
							{	/* Module/java.scm 287 */
								obj_t BgL_cz00_1213;

								BgL_cz00_1213 = CAR(BgL_l1197z00_1210);
								BGl_javazd2declarezd2componentz00zzmodule_javaz00(BgL_jz00_131,
									((BgL_jklassz00_bglt) BgL_jklassz00_1208), BgL_cz00_1213);
							}
							{
								obj_t BgL_l1197z00_3309;

								BgL_l1197z00_3309 = CDR(BgL_l1197z00_1210);
								BgL_l1197z00_1210 = BgL_l1197z00_3309;
								goto BgL_zc3z04anonymousza31560ze3z87_1211;
							}
						}
					else
						{	/* Module/java.scm 287 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* java-declare-component */
	obj_t BGl_javazd2declarezd2componentz00zzmodule_javaz00(obj_t BgL_jz00_135,
		BgL_jklassz00_bglt BgL_jklassz00_136, obj_t BgL_componentz00_137)
	{
		{	/* Module/java.scm 292 */
			{
				obj_t BgL_restz00_1226;
				obj_t BgL_restz00_1228;
				obj_t BgL_idz00_1230;
				obj_t BgL_argsz00_1231;

				if (PAIRP(BgL_componentz00_137))
					{	/* Module/java.scm 313 */
						if ((CAR(((obj_t) BgL_componentz00_137)) == CNST_TABLE_REF(12)))
							{	/* Module/java.scm 313 */
								obj_t BgL_arg1589z00_1238;

								BgL_arg1589z00_1238 = CDR(((obj_t) BgL_componentz00_137));
								BgL_restz00_1226 = BgL_arg1589z00_1238;
								{
									obj_t BgL_jnamez00_1291;
									obj_t BgL_idz00_1292;
									obj_t BgL_modz00_1293;

									{	/* Module/java.scm 315 */
										obj_t BgL_ezd2617zd2_1296;

										BgL_ezd2617zd2_1296 = bgl_reverse(BgL_restz00_1226);
										if (NULLP(BgL_ezd2617zd2_1296))
											{	/* Module/java.scm 313 */
											BgL_tagzd2616zd2_1295:
												{	/* Module/java.scm 322 */
													obj_t BgL_list1693z00_1314;

													BgL_list1693z00_1314 =
														MAKE_YOUNG_PAIR(BGl_string2120z00zzmodule_javaz00,
														BNIL);
													return
														BGl_javazd2errorzd2zzmodule_javaz00
														(BgL_componentz00_137, BgL_list1693z00_1314);
												}
											}
										else
											{	/* Module/java.scm 313 */
												obj_t BgL_carzd2624zd2_1298;
												obj_t BgL_cdrzd2625zd2_1299;

												BgL_carzd2624zd2_1298 = CAR(BgL_ezd2617zd2_1296);
												BgL_cdrzd2625zd2_1299 = CDR(BgL_ezd2617zd2_1296);
												if (STRINGP(BgL_carzd2624zd2_1298))
													{	/* Module/java.scm 313 */
														if (PAIRP(BgL_cdrzd2625zd2_1299))
															{	/* Module/java.scm 313 */
																obj_t BgL_carzd2631zd2_1302;

																BgL_carzd2631zd2_1302 =
																	CAR(BgL_cdrzd2625zd2_1299);
																if (SYMBOLP(BgL_carzd2631zd2_1302))
																	{	/* Module/java.scm 313 */
																		BgL_jnamez00_1291 = BgL_carzd2624zd2_1298;
																		BgL_idz00_1292 = BgL_carzd2631zd2_1302;
																		BgL_modz00_1293 =
																			CDR(BgL_cdrzd2625zd2_1299);
																		if (BGl_modifierzd2listzf3ze70zc6zzmodule_javaz00(BgL_modz00_1293))
																			{
																				obj_t BgL_auxz00_3336;

																				{	/* Module/java.scm 320 */
																					obj_t BgL_arg1678z00_1307;
																					obj_t BgL_arg1681z00_1308;

																					{	/* Module/java.scm 320 */
																						obj_t BgL_list1682z00_1309;

																						{	/* Module/java.scm 320 */
																							obj_t BgL_arg1688z00_1310;

																							{	/* Module/java.scm 320 */
																								obj_t BgL_arg1689z00_1311;

																								{	/* Module/java.scm 320 */
																									obj_t BgL_arg1691z00_1312;

																									BgL_arg1691z00_1312 =
																										MAKE_YOUNG_PAIR
																										(BgL_modz00_1293, BNIL);
																									BgL_arg1689z00_1311 =
																										MAKE_YOUNG_PAIR
																										(BgL_jnamez00_1291,
																										BgL_arg1691z00_1312);
																								}
																								BgL_arg1688z00_1310 =
																									MAKE_YOUNG_PAIR
																									(BgL_idz00_1292,
																									BgL_arg1689z00_1311);
																							}
																							BgL_list1682z00_1309 =
																								MAKE_YOUNG_PAIR
																								(BgL_componentz00_137,
																								BgL_arg1688z00_1310);
																						}
																						BgL_arg1678z00_1307 =
																							BgL_list1682z00_1309;
																					}
																					BgL_arg1681z00_1308 =
																						(((BgL_jklassz00_bglt)
																							COBJECT(BgL_jklassz00_136))->
																						BgL_fieldsz00);
																					BgL_auxz00_3336 =
																						MAKE_YOUNG_PAIR(BgL_arg1678z00_1307,
																						BgL_arg1681z00_1308);
																				}
																				return
																					((((BgL_jklassz00_bglt)
																							COBJECT(BgL_jklassz00_136))->
																						BgL_fieldsz00) =
																					((obj_t) BgL_auxz00_3336), BUNSPEC);
																			}
																		else
																			{	/* Module/java.scm 318 */
																				obj_t BgL_list1692z00_1313;

																				BgL_list1692z00_1313 =
																					MAKE_YOUNG_PAIR
																					(BGl_string2121z00zzmodule_javaz00,
																					BNIL);
																				return
																					BGl_javazd2errorzd2zzmodule_javaz00
																					(BgL_componentz00_137,
																					BgL_list1692z00_1313);
																			}
																	}
																else
																	{	/* Module/java.scm 313 */
																		goto BgL_tagzd2616zd2_1295;
																	}
															}
														else
															{	/* Module/java.scm 313 */
																goto BgL_tagzd2616zd2_1295;
															}
													}
												else
													{	/* Module/java.scm 313 */
														goto BgL_tagzd2616zd2_1295;
													}
											}
									}
								}
							}
						else
							{	/* Module/java.scm 313 */
								if ((CAR(((obj_t) BgL_componentz00_137)) == CNST_TABLE_REF(13)))
									{	/* Module/java.scm 313 */
										obj_t BgL_arg1593z00_1241;

										BgL_arg1593z00_1241 = CDR(((obj_t) BgL_componentz00_137));
										BgL_restz00_1228 = BgL_arg1593z00_1241;
										{
											obj_t BgL_jnamez00_1315;
											obj_t BgL_argsz00_1316;
											obj_t BgL_idz00_1317;
											obj_t BgL_modz00_1318;

											{	/* Module/java.scm 324 */
												obj_t BgL_ezd2639zd2_1321;

												BgL_ezd2639zd2_1321 = bgl_reverse(BgL_restz00_1228);
												if (NULLP(BgL_ezd2639zd2_1321))
													{	/* Module/java.scm 313 */
													BgL_tagzd2638zd2_1320:
														{	/* Module/java.scm 343 */
															obj_t BgL_list1720z00_1351;

															BgL_list1720z00_1351 =
																MAKE_YOUNG_PAIR
																(BGl_string2118z00zzmodule_javaz00, BNIL);
															return
																BGl_javazd2errorzd2zzmodule_javaz00
																(BgL_componentz00_137, BgL_list1720z00_1351);
														}
													}
												else
													{	/* Module/java.scm 313 */
														obj_t BgL_carzd2648zd2_1323;
														obj_t BgL_cdrzd2649zd2_1324;

														BgL_carzd2648zd2_1323 = CAR(BgL_ezd2639zd2_1321);
														BgL_cdrzd2649zd2_1324 = CDR(BgL_ezd2639zd2_1321);
														if (STRINGP(BgL_carzd2648zd2_1323))
															{	/* Module/java.scm 313 */
																if (PAIRP(BgL_cdrzd2649zd2_1324))
																	{	/* Module/java.scm 313 */
																		obj_t BgL_carzd2656zd2_1327;
																		obj_t BgL_cdrzd2657zd2_1328;

																		BgL_carzd2656zd2_1327 =
																			CAR(BgL_cdrzd2649zd2_1324);
																		BgL_cdrzd2657zd2_1328 =
																			CDR(BgL_cdrzd2649zd2_1324);
																		if (BGl_argzd2listzf3ze70zc6zzmodule_javaz00
																			(BgL_carzd2656zd2_1327))
																			{	/* Module/java.scm 313 */
																				if (PAIRP(BgL_cdrzd2657zd2_1328))
																					{	/* Module/java.scm 313 */
																						obj_t BgL_carzd2663zd2_1331;

																						BgL_carzd2663zd2_1331 =
																							CAR(BgL_cdrzd2657zd2_1328);
																						if (SYMBOLP(BgL_carzd2663zd2_1331))
																							{	/* Module/java.scm 313 */
																								BgL_jnamez00_1315 =
																									BgL_carzd2648zd2_1323;
																								BgL_argsz00_1316 =
																									BgL_carzd2656zd2_1327;
																								BgL_idz00_1317 =
																									BgL_carzd2663zd2_1331;
																								BgL_modz00_1318 =
																									CDR(BgL_cdrzd2657zd2_1328);
																								if (BGl_modifierzd2listzf3ze70zc6zzmodule_javaz00(BgL_modz00_1318))
																									{	/* Module/java.scm 332 */
																										obj_t BgL_modz00_1336;

																										if (
																											(((BgL_jklassz00_bglt)
																													COBJECT
																													(BgL_jklassz00_136))->
																												BgL_abstractzf3zf3))
																											{	/* Module/java.scm 332 */
																												BgL_modz00_1336 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(10),
																													BgL_modz00_1318);
																											}
																										else
																											{	/* Module/java.scm 332 */
																												BgL_modz00_1336 =
																													BgL_modz00_1318;
																											}
																										{	/* Module/java.scm 332 */
																											BgL_jmethodz00_bglt
																												BgL_jmetz00_1337;
																											{	/* Module/java.scm 333 */
																												BgL_jmethodz00_bglt
																													BgL_new1122z00_1339;
																												{	/* Module/java.scm 334 */
																													BgL_jmethodz00_bglt
																														BgL_new1120z00_1347;
																													BgL_new1120z00_1347 =
																														(
																														(BgL_jmethodz00_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_jmethodz00_bgl))));
																													{	/* Module/java.scm 334 */
																														long
																															BgL_arg1717z00_1348;
																														BgL_arg1717z00_1348
																															=
																															BGL_CLASS_NUM
																															(BGl_jmethodz00zzmodule_javaz00);
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1120z00_1347), BgL_arg1717z00_1348);
																													}
																													BgL_new1122z00_1339 =
																														BgL_new1120z00_1347;
																												}
																												((((BgL_jmethodz00_bglt)
																															COBJECT
																															(BgL_new1122z00_1339))->
																														BgL_srcz00) =
																													((obj_t)
																														BgL_componentz00_137),
																													BUNSPEC);
																												{
																													obj_t BgL_auxz00_3385;

																													if (
																														((((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_iddz00) == CNST_TABLE_REF(11)))
																														{	/* Module/java.scm 335 */
																															BgL_auxz00_3385 =
																																BgL_idz00_1317;
																														}
																													else
																														{	/* Module/java.scm 337 */
																															obj_t
																																BgL_arg1708z00_1342;
																															BgL_arg1708z00_1342
																																=
																																(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_iddz00);
																															{	/* Module/java.scm 337 */
																																obj_t
																																	BgL_list1709z00_1343;
																																{	/* Module/java.scm 337 */
																																	obj_t
																																		BgL_arg1710z00_1344;
																																	{	/* Module/java.scm 337 */
																																		obj_t
																																			BgL_arg1711z00_1345;
																																		BgL_arg1711z00_1345
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_idz00_1317,
																																			BNIL);
																																		BgL_arg1710z00_1344
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(8),
																																			BgL_arg1711z00_1345);
																																	}
																																	BgL_list1709z00_1343
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1708z00_1342,
																																		BgL_arg1710z00_1344);
																																}
																																BgL_auxz00_3385
																																	=
																																	BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																																	(BgL_list1709z00_1343);
																															}
																														}
																													((((BgL_jmethodz00_bglt) COBJECT(BgL_new1122z00_1339))->BgL_idz00) = ((obj_t) BgL_auxz00_3385), BUNSPEC);
																												}
																												((((BgL_jmethodz00_bglt)
																															COBJECT
																															(BgL_new1122z00_1339))->
																														BgL_argsz00) =
																													((obj_t)
																														BgL_argsz00_1316),
																													BUNSPEC);
																												((((BgL_jmethodz00_bglt)
																															COBJECT
																															(BgL_new1122z00_1339))->
																														BgL_jnamez00) =
																													((obj_t)
																														BgL_jnamez00_1315),
																													BUNSPEC);
																												((((BgL_jmethodz00_bglt)
																															COBJECT
																															(BgL_new1122z00_1339))->
																														BgL_modifiersz00) =
																													((obj_t)
																														BgL_modz00_1336),
																													BUNSPEC);
																												BgL_jmetz00_1337 =
																													BgL_new1122z00_1339;
																											}
																											{	/* Module/java.scm 333 */

																												{
																													obj_t BgL_auxz00_3400;

																													{	/* Module/java.scm 341 */
																														obj_t
																															BgL_arg1703z00_1338;
																														BgL_arg1703z00_1338
																															=
																															(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_methodsz00);
																														BgL_auxz00_3400 =
																															MAKE_YOUNG_PAIR((
																																(obj_t)
																																BgL_jmetz00_1337),
																															BgL_arg1703z00_1338);
																													}
																													return
																														((((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_methodsz00) = ((obj_t) BgL_auxz00_3400), BUNSPEC);
																												}
																											}
																										}
																									}
																								else
																									{	/* Module/java.scm 330 */
																										obj_t BgL_list1719z00_1350;

																										BgL_list1719z00_1350 =
																											MAKE_YOUNG_PAIR
																											(BGl_string2119z00zzmodule_javaz00,
																											BNIL);
																										return
																											BGl_javazd2errorzd2zzmodule_javaz00
																											(BgL_componentz00_137,
																											BgL_list1719z00_1350);
																									}
																							}
																						else
																							{	/* Module/java.scm 313 */
																								goto BgL_tagzd2638zd2_1320;
																							}
																					}
																				else
																					{	/* Module/java.scm 313 */
																						goto BgL_tagzd2638zd2_1320;
																					}
																			}
																		else
																			{	/* Module/java.scm 313 */
																				goto BgL_tagzd2638zd2_1320;
																			}
																	}
																else
																	{	/* Module/java.scm 313 */
																		goto BgL_tagzd2638zd2_1320;
																	}
															}
														else
															{	/* Module/java.scm 313 */
																goto BgL_tagzd2638zd2_1320;
															}
													}
											}
										}
									}
								else
									{	/* Module/java.scm 313 */
										obj_t BgL_cdrzd2516zd2_1242;

										BgL_cdrzd2516zd2_1242 = CDR(((obj_t) BgL_componentz00_137));
										if (
											(CAR(
													((obj_t) BgL_componentz00_137)) ==
												CNST_TABLE_REF(14)))
											{	/* Module/java.scm 313 */
												if (PAIRP(BgL_cdrzd2516zd2_1242))
													{	/* Module/java.scm 313 */
														obj_t BgL_cdrzd2520zd2_1246;

														BgL_cdrzd2520zd2_1246 = CDR(BgL_cdrzd2516zd2_1242);
														if (
															(CAR(BgL_cdrzd2516zd2_1242) ==
																CNST_TABLE_REF(15)))
															{	/* Module/java.scm 313 */
																if (PAIRP(BgL_cdrzd2520zd2_1246))
																	{	/* Module/java.scm 313 */
																		obj_t BgL_carzd2523zd2_1250;
																		obj_t BgL_cdrzd2524zd2_1251;

																		BgL_carzd2523zd2_1250 =
																			CAR(BgL_cdrzd2520zd2_1246);
																		BgL_cdrzd2524zd2_1251 =
																			CDR(BgL_cdrzd2520zd2_1246);
																		if (SYMBOLP(BgL_carzd2523zd2_1250))
																			{	/* Module/java.scm 313 */
																				if (PAIRP(BgL_cdrzd2524zd2_1251))
																					{	/* Module/java.scm 313 */
																						obj_t BgL_carzd2529zd2_1254;

																						BgL_carzd2529zd2_1254 =
																							CAR(BgL_cdrzd2524zd2_1251);
																						if (BGl_argzd2listzf3ze70zc6zzmodule_javaz00(BgL_carzd2529zd2_1254))
																							{	/* Module/java.scm 313 */
																								if (NULLP(CDR
																										(BgL_cdrzd2524zd2_1251)))
																									{	/* Module/java.scm 313 */
																										BgL_idz00_1230 =
																											BgL_carzd2523zd2_1250;
																										BgL_argsz00_1231 =
																											BgL_carzd2529zd2_1254;
																									BgL_tagzd2491zd2_1232:
																										{	/* Module/java.scm 347 */
																											BgL_jconstructorz00_bglt
																												BgL_jconstrz00_1353;
																											{	/* Module/java.scm 347 */
																												BgL_jconstructorz00_bglt
																													BgL_new1125z00_1357;
																												{	/* Module/java.scm 348 */
																													BgL_jconstructorz00_bglt
																														BgL_new1124z00_1365;
																													BgL_new1124z00_1365 =
																														(
																														(BgL_jconstructorz00_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_jconstructorz00_bgl))));
																													{	/* Module/java.scm 348 */
																														long
																															BgL_arg1746z00_1366;
																														BgL_arg1746z00_1366
																															=
																															BGL_CLASS_NUM
																															(BGl_jconstructorz00zzmodule_javaz00);
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1124z00_1365), BgL_arg1746z00_1366);
																													}
																													BgL_new1125z00_1357 =
																														BgL_new1124z00_1365;
																												}
																												((((BgL_jmethodz00_bglt)
																															COBJECT((
																																	(BgL_jmethodz00_bglt)
																																	BgL_new1125z00_1357)))->
																														BgL_srcz00) =
																													((obj_t)
																														BgL_componentz00_137),
																													BUNSPEC);
																												{
																													obj_t BgL_auxz00_3442;

																													{	/* Module/java.scm 349 */
																														obj_t
																															BgL_arg1734z00_1358;
																														obj_t
																															BgL_arg1735z00_1359;
																														{	/* Module/java.scm 349 */
																															obj_t
																																BgL_arg1736z00_1360;
																															BgL_arg1736z00_1360
																																=
																																(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_iddz00);
																															{	/* Module/java.scm 349 */
																																obj_t
																																	BgL_list1737z00_1361;
																																{	/* Module/java.scm 349 */
																																	obj_t
																																		BgL_arg1738z00_1362;
																																	{	/* Module/java.scm 349 */
																																		obj_t
																																			BgL_arg1739z00_1363;
																																		{	/* Module/java.scm 349 */
																																			obj_t
																																				BgL_arg1740z00_1364;
																																			BgL_arg1740z00_1364
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_idz00_1230,
																																				BNIL);
																																			BgL_arg1739z00_1363
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(8),
																																				BgL_arg1740z00_1364);
																																		}
																																		BgL_arg1738z00_1362
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1736z00_1360,
																																			BgL_arg1739z00_1363);
																																	}
																																	BgL_list1737z00_1361
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(9),
																																		BgL_arg1738z00_1362);
																																}
																																BgL_arg1734z00_1358
																																	=
																																	BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																																	(BgL_list1737z00_1361);
																														}}
																														BgL_arg1735z00_1359
																															=
																															(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_iddz00);
																														BgL_auxz00_3442 =
																															BGl_makezd2typedzd2identz00zzast_identz00
																															(BgL_arg1734z00_1358,
																															BgL_arg1735z00_1359);
																													}
																													((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt) BgL_new1125z00_1357)))->BgL_idz00) = ((obj_t) BgL_auxz00_3442), BUNSPEC);
																												}
																												((((BgL_jmethodz00_bglt)
																															COBJECT((
																																	(BgL_jmethodz00_bglt)
																																	BgL_new1125z00_1357)))->
																														BgL_argsz00) =
																													((obj_t)
																														BgL_argsz00_1231),
																													BUNSPEC);
																												((((BgL_jmethodz00_bglt)
																															COBJECT((
																																	(BgL_jmethodz00_bglt)
																																	BgL_new1125z00_1357)))->
																														BgL_jnamez00) =
																													((obj_t)
																														BGl_string2117z00zzmodule_javaz00),
																													BUNSPEC);
																												((((BgL_jmethodz00_bglt)
																															COBJECT((
																																	(BgL_jmethodz00_bglt)
																																	BgL_new1125z00_1357)))->
																														BgL_modifiersz00) =
																													((obj_t) BNIL),
																													BUNSPEC);
																												BgL_jconstrz00_1353 =
																													BgL_new1125z00_1357;
																											}
																											{
																												obj_t BgL_auxz00_3461;

																												{	/* Module/java.scm 353 */
																													obj_t
																														BgL_arg1722z00_1354;
																													BgL_arg1722z00_1354 =
																														(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_methodsz00);
																													BgL_auxz00_3461 =
																														MAKE_YOUNG_PAIR((
																															(obj_t)
																															BgL_jconstrz00_1353),
																														BgL_arg1722z00_1354);
																												}
																												((((BgL_jklassz00_bglt)
																															COBJECT
																															(BgL_jklassz00_136))->
																														BgL_methodsz00) =
																													((obj_t)
																														BgL_auxz00_3461),
																													BUNSPEC);
																											}
																											{
																												obj_t BgL_auxz00_3466;

																												{	/* Module/java.scm 354 */
																													obj_t
																														BgL_arg1724z00_1355;
																													obj_t
																														BgL_arg1733z00_1356;
																													BgL_arg1724z00_1355 =
																														MAKE_YOUNG_PAIR
																														(BgL_idz00_1230,
																														BgL_argsz00_1231);
																													BgL_arg1733z00_1356 =
																														(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_constructorsz00);
																													BgL_auxz00_3466 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1724z00_1355,
																														BgL_arg1733z00_1356);
																												}
																												return
																													((((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_136))->BgL_constructorsz00) = ((obj_t) BgL_auxz00_3466), BUNSPEC);
																											}
																										}
																									}
																								else
																									{	/* Module/java.scm 313 */
																									BgL_tagzd2492zd2_1233:
																										if (PAIRP
																											(BgL_componentz00_137))
																											{	/* Module/java.scm 357 */
																												obj_t
																													BgL_list1748z00_1368;
																												BgL_list1748z00_1368 =
																													MAKE_YOUNG_PAIR
																													(BGl_string2114z00zzmodule_javaz00,
																													BNIL);
																												return
																													BGl_javazd2errorzd2zzmodule_javaz00
																													(BgL_componentz00_137,
																													BgL_list1748z00_1368);
																											}
																										else
																											{	/* Module/java.scm 361 */
																												obj_t
																													BgL_arg1749z00_1369;
																												{	/* Module/java.scm 361 */
																													obj_t
																														BgL_arg1751z00_1371;
																													{	/* Module/java.scm 361 */
																														obj_t
																															BgL_zc3z04anonymousza31753ze3z87_2462;
																														BgL_zc3z04anonymousza31753ze3z87_2462
																															=
																															MAKE_FX_PROCEDURE
																															(BGl_z62zc3z04anonymousza31753ze3ze5zzmodule_javaz00,
																															(int) (0L),
																															(int) (1L));
																														PROCEDURE_SET
																															(BgL_zc3z04anonymousza31753ze3z87_2462,
																															(int) (0L),
																															BgL_componentz00_137);
																														BgL_arg1751z00_1371
																															=
																															BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
																															(BgL_zc3z04anonymousza31753ze3z87_2462);
																													}
																													BgL_arg1749z00_1369 =
																														string_append_3
																														(BGl_string2115z00zzmodule_javaz00,
																														BgL_arg1751z00_1371,
																														BGl_string2116z00zzmodule_javaz00);
																												}
																												{	/* Module/java.scm 358 */
																													obj_t
																														BgL_list1750z00_1370;
																													BgL_list1750z00_1370 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1749z00_1369,
																														BNIL);
																													return
																														BGl_javazd2errorzd2zzmodule_javaz00
																														(BgL_jz00_135,
																														BgL_list1750z00_1370);
																												}
																											}
																									}
																							}
																						else
																							{	/* Module/java.scm 313 */
																								goto BgL_tagzd2492zd2_1233;
																							}
																					}
																				else
																					{	/* Module/java.scm 313 */
																						obj_t BgL_carzd2548zd2_1260;
																						obj_t BgL_cdrzd2549zd2_1261;

																						BgL_carzd2548zd2_1260 =
																							CAR(
																							((obj_t) BgL_cdrzd2516zd2_1242));
																						BgL_cdrzd2549zd2_1261 =
																							CDR(
																							((obj_t) BgL_cdrzd2516zd2_1242));
																						if (SYMBOLP(BgL_carzd2548zd2_1260))
																							{	/* Module/java.scm 313 */
																								obj_t BgL_carzd2555zd2_1263;

																								BgL_carzd2555zd2_1263 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2549zd2_1261));
																								if (BGl_argzd2listzf3ze70zc6zzmodule_javaz00(BgL_carzd2555zd2_1263))
																									{	/* Module/java.scm 313 */
																										if (NULLP(CDR(
																													((obj_t)
																														BgL_cdrzd2549zd2_1261))))
																											{
																												obj_t BgL_argsz00_3499;
																												obj_t BgL_idz00_3498;

																												BgL_idz00_3498 =
																													BgL_carzd2548zd2_1260;
																												BgL_argsz00_3499 =
																													BgL_carzd2555zd2_1263;
																												BgL_argsz00_1231 =
																													BgL_argsz00_3499;
																												BgL_idz00_1230 =
																													BgL_idz00_3498;
																												goto
																													BgL_tagzd2491zd2_1232;
																											}
																										else
																											{	/* Module/java.scm 313 */
																												goto
																													BgL_tagzd2492zd2_1233;
																											}
																									}
																								else
																									{	/* Module/java.scm 313 */
																										goto BgL_tagzd2492zd2_1233;
																									}
																							}
																						else
																							{	/* Module/java.scm 313 */
																								goto BgL_tagzd2492zd2_1233;
																							}
																					}
																			}
																		else
																			{	/* Module/java.scm 313 */
																				obj_t BgL_carzd2568zd2_1269;
																				obj_t BgL_cdrzd2569zd2_1270;

																				BgL_carzd2568zd2_1269 =
																					CAR(((obj_t) BgL_cdrzd2516zd2_1242));
																				BgL_cdrzd2569zd2_1270 =
																					CDR(((obj_t) BgL_cdrzd2516zd2_1242));
																				if (SYMBOLP(BgL_carzd2568zd2_1269))
																					{	/* Module/java.scm 313 */
																						obj_t BgL_carzd2574zd2_1272;

																						BgL_carzd2574zd2_1272 =
																							CAR(
																							((obj_t) BgL_cdrzd2569zd2_1270));
																						if (BGl_argzd2listzf3ze70zc6zzmodule_javaz00(BgL_carzd2574zd2_1272))
																							{	/* Module/java.scm 313 */
																								if (NULLP(CDR(
																											((obj_t)
																												BgL_cdrzd2569zd2_1270))))
																									{
																										obj_t BgL_argsz00_3515;
																										obj_t BgL_idz00_3514;

																										BgL_idz00_3514 =
																											BgL_carzd2568zd2_1269;
																										BgL_argsz00_3515 =
																											BgL_carzd2574zd2_1272;
																										BgL_argsz00_1231 =
																											BgL_argsz00_3515;
																										BgL_idz00_1230 =
																											BgL_idz00_3514;
																										goto BgL_tagzd2491zd2_1232;
																									}
																								else
																									{	/* Module/java.scm 313 */
																										goto BgL_tagzd2492zd2_1233;
																									}
																							}
																						else
																							{	/* Module/java.scm 313 */
																								goto BgL_tagzd2492zd2_1233;
																							}
																					}
																				else
																					{	/* Module/java.scm 313 */
																						goto BgL_tagzd2492zd2_1233;
																					}
																			}
																	}
																else
																	{	/* Module/java.scm 313 */
																		goto BgL_tagzd2492zd2_1233;
																	}
															}
														else
															{	/* Module/java.scm 313 */
																obj_t BgL_carzd2587zd2_1278;
																obj_t BgL_cdrzd2588zd2_1279;

																BgL_carzd2587zd2_1278 =
																	CAR(((obj_t) BgL_cdrzd2516zd2_1242));
																BgL_cdrzd2588zd2_1279 =
																	CDR(((obj_t) BgL_cdrzd2516zd2_1242));
																if (SYMBOLP(BgL_carzd2587zd2_1278))
																	{	/* Module/java.scm 313 */
																		if (PAIRP(BgL_cdrzd2588zd2_1279))
																			{	/* Module/java.scm 313 */
																				obj_t BgL_carzd2593zd2_1282;

																				BgL_carzd2593zd2_1282 =
																					CAR(BgL_cdrzd2588zd2_1279);
																				if (BGl_argzd2listzf3ze70zc6zzmodule_javaz00(BgL_carzd2593zd2_1282))
																					{	/* Module/java.scm 313 */
																						if (NULLP(CDR
																								(BgL_cdrzd2588zd2_1279)))
																							{
																								obj_t BgL_argsz00_3531;
																								obj_t BgL_idz00_3530;

																								BgL_idz00_3530 =
																									BgL_carzd2587zd2_1278;
																								BgL_argsz00_3531 =
																									BgL_carzd2593zd2_1282;
																								BgL_argsz00_1231 =
																									BgL_argsz00_3531;
																								BgL_idz00_1230 = BgL_idz00_3530;
																								goto BgL_tagzd2491zd2_1232;
																							}
																						else
																							{	/* Module/java.scm 313 */
																								goto BgL_tagzd2492zd2_1233;
																							}
																					}
																				else
																					{	/* Module/java.scm 313 */
																						goto BgL_tagzd2492zd2_1233;
																					}
																			}
																		else
																			{	/* Module/java.scm 313 */
																				goto BgL_tagzd2492zd2_1233;
																			}
																	}
																else
																	{	/* Module/java.scm 313 */
																		goto BgL_tagzd2492zd2_1233;
																	}
															}
													}
												else
													{	/* Module/java.scm 313 */
														goto BgL_tagzd2492zd2_1233;
													}
											}
										else
											{	/* Module/java.scm 313 */
												goto BgL_tagzd2492zd2_1233;
											}
									}
							}
					}
				else
					{	/* Module/java.scm 313 */
						goto BgL_tagzd2492zd2_1233;
					}
			}
		}

	}



/* every~0 */
	bool_t BGl_everyze70ze7zzmodule_javaz00(obj_t BgL_predzf3zf3_1376,
		obj_t BgL_lstz00_1377)
	{
		{	/* Module/java.scm 303 */
			{
				obj_t BgL_lstz00_1380;

				BgL_lstz00_1380 = BgL_lstz00_1377;
			BgL_zc3z04anonymousza31756ze3z87_1381:
				if (NULLP(BgL_lstz00_1380))
					{	/* Module/java.scm 296 */
						return ((bool_t) 1);
					}
				else
					{	/* Module/java.scm 296 */
						if (PAIRP(BgL_lstz00_1380))
							{	/* Module/java.scm 300 */
								bool_t BgL_test2362z00_3536;

								{	/* Module/java.scm 300 */
									obj_t BgL_arg1767z00_1387;

									BgL_arg1767z00_1387 = CAR(BgL_lstz00_1380);
									BgL_test2362z00_3536 =
										CBOOL(
										((obj_t(*)(obj_t,
													obj_t))
											PROCEDURE_L_ENTRY(BgL_predzf3zf3_1376))
										(BgL_predzf3zf3_1376, BgL_arg1767z00_1387));
								}
								if (BgL_test2362z00_3536)
									{
										obj_t BgL_lstz00_3543;

										BgL_lstz00_3543 = CDR(BgL_lstz00_1380);
										BgL_lstz00_1380 = BgL_lstz00_3543;
										goto BgL_zc3z04anonymousza31756ze3z87_1381;
									}
								else
									{	/* Module/java.scm 300 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Module/java.scm 298 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* modifier-list?~0 */
	bool_t BGl_modifierzd2listzf3ze70zc6zzmodule_javaz00(obj_t BgL_lstz00_1396)
	{
		{	/* Module/java.scm 312 */
			return
				BGl_everyze70ze7zzmodule_javaz00(BGl_proc2122z00zzmodule_javaz00,
				BgL_lstz00_1396);
		}

	}



/* arg-list?~0 */
	bool_t BGl_argzd2listzf3ze70zc6zzmodule_javaz00(obj_t BgL_lstz00_1389)
	{
		{	/* Module/java.scm 305 */
			return
				BGl_everyze70ze7zzmodule_javaz00(BGl_proc2123z00zzmodule_javaz00,
				BgL_lstz00_1389);
		}

	}



/* &<@anonymous:1753> */
	obj_t BGl_z62zc3z04anonymousza31753ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2465)
	{
		{	/* Module/java.scm 360 */
			return
				BGl_writez00zz__r4_output_6_10_3z00(PROCEDURE_REF(BgL_envz00_2465,
					(int) (0L)), BNIL);
		}

	}



/* &<@anonymous:1774> */
	obj_t BGl_z62zc3z04anonymousza31774ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2467, obj_t BgL_sz00_2468)
	{
		{	/* Module/java.scm 307 */
			if (SYMBOLP(BgL_sz00_2468))
				{	/* Module/java.scm 308 */
					return
						BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_sz00_2468,
						CNST_TABLE_REF(16));
				}
			else
				{	/* Module/java.scm 308 */
					return BFALSE;
				}
		}

	}



/* &<@anonymous:1771> */
	obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2469, obj_t BgL_sz00_2470)
	{
		{	/* Module/java.scm 305 */
			{	/* Module/java.scm 305 */
				bool_t BgL_tmpz00_3554;

				if (SYMBOLP(BgL_sz00_2470))
					{	/* Module/java.scm 305 */
						BgL_tmpz00_3554 =
							BGl_typezd2identzf3z21zzast_identz00(BgL_sz00_2470);
					}
				else
					{	/* Module/java.scm 305 */
						BgL_tmpz00_3554 = ((bool_t) 0);
					}
				return BBOOL(BgL_tmpz00_3554);
			}
		}

	}



/* declare-jklass */
	BgL_typez00_bglt BGl_declarezd2jklasszd2zzmodule_javaz00(BgL_jklassz00_bglt
		BgL_jklassz00_138)
	{
		{	/* Module/java.scm 369 */
			{
				BgL_jmethodz00_bglt BgL_jmetz00_1489;
				BgL_jmethodz00_bglt BgL_jmetz00_1447;
				BgL_jmethodz00_bglt BgL_jmetz00_1437;

				BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00(
					(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_138))->BgL_idz00),
					(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_138))->BgL_jnamez00),
					BNIL);
				BGl_addzd2qualifiedzd2typez12z12zzread_jvmz00
					(BGl_fastzd2idzd2ofzd2idzd2zzast_identz00((((BgL_jklassz00_bglt)
								COBJECT(BgL_jklassz00_138))->BgL_idz00),
						(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_138))->BgL_locz00)),
					(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_138))->BgL_jnamez00),
					BNIL);
				{	/* Module/java.scm 397 */
					obj_t BgL_g1201z00_1418;

					BgL_g1201z00_1418 =
						(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_138))->BgL_methodsz00);
					{
						obj_t BgL_l1199z00_1420;

						BgL_l1199z00_1420 = BgL_g1201z00_1418;
					BgL_zc3z04anonymousza31813ze3z87_1421:
						if (PAIRP(BgL_l1199z00_1420))
							{	/* Module/java.scm 397 */
								{	/* Module/java.scm 397 */
									obj_t BgL_arg1820z00_1423;

									BgL_arg1820z00_1423 = CAR(BgL_l1199z00_1420);
									BgL_jmetz00_1489 =
										((BgL_jmethodz00_bglt) BgL_arg1820z00_1423);
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(CNST_TABLE_REF(17),
												(((BgL_jmethodz00_bglt) COBJECT(BgL_jmetz00_1489))->
													BgL_modifiersz00))))
										{
											BgL_globalz00_bglt BgL_auxz00_3576;

											BgL_jmetz00_1437 = BgL_jmetz00_1489;
											{	/* Module/java.scm 373 */
												obj_t BgL_arg1839z00_1440;
												obj_t BgL_arg1840z00_1441;
												obj_t BgL_arg1842z00_1442;
												obj_t BgL_arg1843z00_1443;
												obj_t BgL_arg1844z00_1444;
												obj_t BgL_arg1845z00_1445;
												obj_t BgL_arg1846z00_1446;

												BgL_arg1839z00_1440 =
													(((BgL_jmethodz00_bglt) COBJECT(BgL_jmetz00_1437))->
													BgL_idz00);
												BgL_arg1840z00_1441 =
													(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_138))->
													BgL_idz00);
												BgL_arg1842z00_1442 =
													(((BgL_jmethodz00_bglt) COBJECT(BgL_jmetz00_1437))->
													BgL_jnamez00);
												BgL_arg1843z00_1443 =
													(((BgL_jmethodz00_bglt) COBJECT(BgL_jmetz00_1437))->
													BgL_argsz00);
												BgL_arg1844z00_1444 =
													(((BgL_jmethodz00_bglt) COBJECT(BgL_jmetz00_1437))->
													BgL_modifiersz00);
												BgL_arg1845z00_1445 =
													(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_138))->
													BgL_jnamez00);
												BgL_arg1846z00_1446 =
													(((BgL_jmethodz00_bglt) COBJECT(BgL_jmetz00_1437))->
													BgL_srcz00);
												{	/* Module/java.scm 415 */
													obj_t BgL_pidz00_2151;

													BgL_pidz00_2151 =
														BGl_parsezd2idzd2zzast_identz00(BgL_arg1839z00_1440,
														BGl_findzd2locationzd2zztools_locationz00
														(BgL_arg1846z00_1446));
													{	/* Module/java.scm 415 */
														obj_t BgL_lnz00_2153;

														BgL_lnz00_2153 = CAR(BgL_pidz00_2151);
														{	/* Module/java.scm 416 */
															obj_t BgL_tidz00_2154;

															BgL_tidz00_2154 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt)
																			CDR(BgL_pidz00_2151))))->BgL_idz00);
															{	/* Module/java.scm 417 */

																{	/* Module/java.scm 418 */
																	BgL_globalz00_bglt BgL_gz00_2156;

																	BgL_gz00_2156 =
																		BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2
																		(BgL_lnz00_2153, BFALSE,
																		BgL_arg1840z00_1441, BgL_arg1842z00_1442,
																		BgL_tidz00_2154, BgL_arg1843z00_1443,
																		((bool_t) 0), ((bool_t) 0),
																		BgL_arg1846z00_1446, BFALSE);
																	{	/* Module/java.scm 419 */
																		BgL_valuez00_bglt BgL_arg1894z00_2157;

																		BgL_arg1894z00_2157 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						BgL_gz00_2156)))->BgL_valuez00);
																		((((BgL_cfunz00_bglt)
																					COBJECT(((BgL_cfunz00_bglt)
																							BgL_arg1894z00_2157)))->
																				BgL_methodz00) =
																			((obj_t) BgL_arg1844z00_1444), BUNSPEC);
																	}
																	((((BgL_globalz00_bglt)
																				COBJECT(BgL_gz00_2156))->
																			BgL_jvmzd2typezd2namez00) =
																		((obj_t) ((obj_t) BgL_arg1845z00_1445)),
																		BUNSPEC);
																	BgL_auxz00_3576 = BgL_gz00_2156;
																}
															}
														}
													}
												}
											}
											((obj_t) BgL_auxz00_3576);
										}
									else
										{	/* Module/java.scm 390 */
											BgL_jmetz00_1447 = BgL_jmetz00_1489;
											{	/* Module/java.scm 379 */
												bool_t BgL_test2367z00_3598;

												{	/* Module/java.scm 379 */
													bool_t BgL_test2368z00_3599;

													{	/* Module/java.scm 379 */
														obj_t BgL_classz00_2166;

														BgL_classz00_2166 =
															BGl_jconstructorz00zzmodule_javaz00;
														{	/* Module/java.scm 379 */
															obj_t BgL_oclassz00_2168;

															{	/* Module/java.scm 379 */
																obj_t BgL_arg1815z00_2170;
																long BgL_arg1816z00_2171;

																BgL_arg1815z00_2170 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Module/java.scm 379 */
																	long BgL_arg1817z00_2172;

																	BgL_arg1817z00_2172 =
																		BGL_OBJECT_CLASS_NUM(
																		((BgL_objectz00_bglt) BgL_jmetz00_1447));
																	BgL_arg1816z00_2171 =
																		(BgL_arg1817z00_2172 - OBJECT_TYPE);
																}
																BgL_oclassz00_2168 =
																	VECTOR_REF(BgL_arg1815z00_2170,
																	BgL_arg1816z00_2171);
															}
															BgL_test2368z00_3599 =
																(BgL_oclassz00_2168 == BgL_classz00_2166);
													}}
													if (BgL_test2368z00_3599)
														{	/* Module/java.scm 379 */
															BgL_test2367z00_3598 = ((bool_t) 0);
														}
													else
														{	/* Module/java.scm 380 */
															bool_t BgL_test2369z00_3606;

															if (NULLP(
																	(((BgL_jmethodz00_bglt)
																			COBJECT(BgL_jmetz00_1447))->BgL_argsz00)))
																{	/* Module/java.scm 380 */
																	BgL_test2369z00_3606 = ((bool_t) 0);
																}
															else
																{	/* Module/java.scm 380 */
																	BgL_test2369z00_3606 =
																		(
																		(((BgL_typez00_bglt)
																				COBJECT
																				(BGl_typezd2ofzd2idz00zzast_identz00(CAR
																						((((BgL_jmethodz00_bglt)
																									COBJECT(BgL_jmetz00_1447))->
																								BgL_argsz00)),
																						(((BgL_jklassz00_bglt)
																								COBJECT(BgL_jklassz00_138))->
																							BgL_locz00))))->BgL_idz00) ==
																		(((BgL_jklassz00_bglt)
																				COBJECT(BgL_jklassz00_138))->
																			BgL_iddz00));
																}
															if (BgL_test2369z00_3606)
																{	/* Module/java.scm 380 */
																	BgL_test2367z00_3598 = ((bool_t) 0);
																}
															else
																{	/* Module/java.scm 380 */
																	BgL_test2367z00_3598 = ((bool_t) 1);
																}
														}
												}
												if (BgL_test2367z00_3598)
													{	/* Module/java.scm 383 */
														obj_t BgL_arg1872z00_1470;

														BgL_arg1872z00_1470 =
															(((BgL_jmethodz00_bglt)
																COBJECT(BgL_jmetz00_1447))->BgL_srcz00);
														{	/* Module/java.scm 383 */
															obj_t BgL_list1873z00_1471;

															BgL_list1873z00_1471 =
																MAKE_YOUNG_PAIR
																(BGl_string2124z00zzmodule_javaz00, BNIL);
															BGl_javazd2errorzd2zzmodule_javaz00
																(BgL_arg1872z00_1470, BgL_list1873z00_1471);
														}
													}
												else
													{	/* Module/java.scm 384 */
														obj_t BgL_arg1874z00_1472;
														obj_t BgL_arg1875z00_1473;
														obj_t BgL_arg1876z00_1474;
														obj_t BgL_arg1877z00_1475;
														obj_t BgL_arg1878z00_1476;
														obj_t BgL_arg1879z00_1477;
														obj_t BgL_arg1880z00_1478;

														BgL_arg1874z00_1472 =
															(((BgL_jmethodz00_bglt)
																COBJECT(BgL_jmetz00_1447))->BgL_idz00);
														BgL_arg1875z00_1473 =
															(((BgL_jklassz00_bglt)
																COBJECT(BgL_jklassz00_138))->BgL_idz00);
														BgL_arg1876z00_1474 =
															(((BgL_jmethodz00_bglt)
																COBJECT(BgL_jmetz00_1447))->BgL_jnamez00);
														BgL_arg1877z00_1475 =
															(((BgL_jmethodz00_bglt)
																COBJECT(BgL_jmetz00_1447))->BgL_argsz00);
														BgL_arg1878z00_1476 =
															(((BgL_jmethodz00_bglt)
																COBJECT(BgL_jmetz00_1447))->BgL_modifiersz00);
														BgL_arg1879z00_1477 =
															(((BgL_jklassz00_bglt)
																COBJECT(BgL_jklassz00_138))->BgL_jnamez00);
														BgL_arg1880z00_1478 =
															(((BgL_jmethodz00_bglt)
																COBJECT(BgL_jmetz00_1447))->BgL_srcz00);
														{	/* Module/java.scm 415 */
															obj_t BgL_pidz00_2183;

															BgL_pidz00_2183 =
																BGl_parsezd2idzd2zzast_identz00
																(BgL_arg1874z00_1472,
																BGl_findzd2locationzd2zztools_locationz00
																(BgL_arg1880z00_1478));
															{	/* Module/java.scm 415 */
																obj_t BgL_lnz00_2185;

																BgL_lnz00_2185 = CAR(BgL_pidz00_2183);
																{	/* Module/java.scm 416 */
																	obj_t BgL_tidz00_2186;

																	BgL_tidz00_2186 =
																		(((BgL_typez00_bglt) COBJECT(
																				((BgL_typez00_bglt)
																					CDR(BgL_pidz00_2183))))->BgL_idz00);
																	{	/* Module/java.scm 417 */

																		{	/* Module/java.scm 418 */
																			BgL_globalz00_bglt BgL_gz00_2188;

																			BgL_gz00_2188 =
																				BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2
																				(BgL_lnz00_2185, BFALSE,
																				BgL_arg1875z00_1473,
																				BgL_arg1876z00_1474, BgL_tidz00_2186,
																				BgL_arg1877z00_1475, ((bool_t) 0),
																				((bool_t) 0), BgL_arg1880z00_1478,
																				BFALSE);
																			{	/* Module/java.scm 419 */
																				BgL_valuez00_bglt BgL_arg1894z00_2189;

																				BgL_arg1894z00_2189 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								BgL_gz00_2188)))->BgL_valuez00);
																				((((BgL_cfunz00_bglt)
																							COBJECT(((BgL_cfunz00_bglt)
																									BgL_arg1894z00_2189)))->
																						BgL_methodz00) =
																					((obj_t) BgL_arg1878z00_1476),
																					BUNSPEC);
																			}
																			((((BgL_globalz00_bglt)
																						COBJECT(BgL_gz00_2188))->
																					BgL_jvmzd2typezd2namez00) =
																				((obj_t) ((obj_t) BgL_arg1879z00_1477)),
																				BUNSPEC);
																			((obj_t) BgL_gz00_2188);
																		}
																	}
																}
															}
														}
													}
											}
										}
								}
								{
									obj_t BgL_l1199z00_3642;

									BgL_l1199z00_3642 = CDR(BgL_l1199z00_1420);
									BgL_l1199z00_1420 = BgL_l1199z00_3642;
									goto BgL_zc3z04anonymousza31813ze3z87_1421;
								}
							}
						else
							{	/* Module/java.scm 397 */
								((bool_t) 1);
							}
					}
				}
				{	/* Module/java.scm 398 */
					BgL_typez00_bglt BgL_jclassz00_1426;

					BgL_jclassz00_1426 =
						BGl_declarezd2javazd2classz12z12zzmodule_javaz00(BgL_jklassz00_138);
					{	/* Module/java.scm 400 */
						bool_t BgL_test2371z00_3645;

						{	/* Module/java.scm 400 */
							obj_t BgL_tmpz00_3646;

							{
								BgL_jclassz00_bglt BgL_auxz00_3647;

								{
									obj_t BgL_auxz00_3648;

									{	/* Module/java.scm 400 */
										BgL_objectz00_bglt BgL_tmpz00_3649;

										BgL_tmpz00_3649 = ((BgL_objectz00_bglt) BgL_jclassz00_1426);
										BgL_auxz00_3648 = BGL_OBJECT_WIDENING(BgL_tmpz00_3649);
									}
									BgL_auxz00_3647 = ((BgL_jclassz00_bglt) BgL_auxz00_3648);
								}
								BgL_tmpz00_3646 =
									(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3647))->
									BgL_itszd2superzd2);
							}
							BgL_test2371z00_3645 = CBOOL(BgL_tmpz00_3646);
						}
						if (BgL_test2371z00_3645)
							{	/* Module/java.scm 401 */
								obj_t BgL_typez00_1429;

								{	/* Module/java.scm 402 */
									bool_t BgL_test2372z00_3655;

									{	/* Module/java.scm 402 */
										obj_t BgL_arg1837z00_1436;

										{
											BgL_jclassz00_bglt BgL_auxz00_3656;

											{
												obj_t BgL_auxz00_3657;

												{	/* Module/java.scm 402 */
													BgL_objectz00_bglt BgL_tmpz00_3658;

													BgL_tmpz00_3658 =
														((BgL_objectz00_bglt) BgL_jclassz00_1426);
													BgL_auxz00_3657 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3658);
												}
												BgL_auxz00_3656 =
													((BgL_jclassz00_bglt) BgL_auxz00_3657);
											}
											BgL_arg1837z00_1436 =
												(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3656))->
												BgL_itszd2superzd2);
										}
										{	/* Module/java.scm 402 */
											obj_t BgL_classz00_2202;

											BgL_classz00_2202 = BGl_jclassz00zzobject_classz00;
											if (BGL_OBJECTP(BgL_arg1837z00_1436))
												{	/* Module/java.scm 402 */
													BgL_objectz00_bglt BgL_arg1807z00_2204;

													BgL_arg1807z00_2204 =
														(BgL_objectz00_bglt) (BgL_arg1837z00_1436);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Module/java.scm 402 */
															long BgL_idxz00_2210;

															BgL_idxz00_2210 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2204);
															BgL_test2372z00_3655 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2210 + 2L)) == BgL_classz00_2202);
														}
													else
														{	/* Module/java.scm 402 */
															bool_t BgL_res2094z00_2235;

															{	/* Module/java.scm 402 */
																obj_t BgL_oclassz00_2218;

																{	/* Module/java.scm 402 */
																	obj_t BgL_arg1815z00_2226;
																	long BgL_arg1816z00_2227;

																	BgL_arg1815z00_2226 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Module/java.scm 402 */
																		long BgL_arg1817z00_2228;

																		BgL_arg1817z00_2228 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2204);
																		BgL_arg1816z00_2227 =
																			(BgL_arg1817z00_2228 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2218 =
																		VECTOR_REF(BgL_arg1815z00_2226,
																		BgL_arg1816z00_2227);
																}
																{	/* Module/java.scm 402 */
																	bool_t BgL__ortest_1115z00_2219;

																	BgL__ortest_1115z00_2219 =
																		(BgL_classz00_2202 == BgL_oclassz00_2218);
																	if (BgL__ortest_1115z00_2219)
																		{	/* Module/java.scm 402 */
																			BgL_res2094z00_2235 =
																				BgL__ortest_1115z00_2219;
																		}
																	else
																		{	/* Module/java.scm 402 */
																			long BgL_odepthz00_2220;

																			{	/* Module/java.scm 402 */
																				obj_t BgL_arg1804z00_2221;

																				BgL_arg1804z00_2221 =
																					(BgL_oclassz00_2218);
																				BgL_odepthz00_2220 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2221);
																			}
																			if ((2L < BgL_odepthz00_2220))
																				{	/* Module/java.scm 402 */
																					obj_t BgL_arg1802z00_2223;

																					{	/* Module/java.scm 402 */
																						obj_t BgL_arg1803z00_2224;

																						BgL_arg1803z00_2224 =
																							(BgL_oclassz00_2218);
																						BgL_arg1802z00_2223 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2224, 2L);
																					}
																					BgL_res2094z00_2235 =
																						(BgL_arg1802z00_2223 ==
																						BgL_classz00_2202);
																				}
																			else
																				{	/* Module/java.scm 402 */
																					BgL_res2094z00_2235 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2372z00_3655 = BgL_res2094z00_2235;
														}
												}
											else
												{	/* Module/java.scm 402 */
													BgL_test2372z00_3655 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2372z00_3655)
										{
											BgL_jclassz00_bglt BgL_auxz00_3685;

											{
												obj_t BgL_auxz00_3686;

												{	/* Module/java.scm 403 */
													BgL_objectz00_bglt BgL_tmpz00_3687;

													BgL_tmpz00_3687 =
														((BgL_objectz00_bglt) BgL_jclassz00_1426);
													BgL_auxz00_3686 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3687);
												}
												BgL_auxz00_3685 =
													((BgL_jclassz00_bglt) BgL_auxz00_3686);
											}
											BgL_typez00_1429 =
												(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3685))->
												BgL_itszd2superzd2);
										}
									else
										{	/* Module/java.scm 404 */
											bool_t BgL_test2377z00_3692;

											{	/* Module/java.scm 404 */
												obj_t BgL_arg1836z00_1435;

												{
													BgL_jclassz00_bglt BgL_auxz00_3693;

													{
														obj_t BgL_auxz00_3694;

														{	/* Module/java.scm 404 */
															BgL_objectz00_bglt BgL_tmpz00_3695;

															BgL_tmpz00_3695 =
																((BgL_objectz00_bglt) BgL_jclassz00_1426);
															BgL_auxz00_3694 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3695);
														}
														BgL_auxz00_3693 =
															((BgL_jclassz00_bglt) BgL_auxz00_3694);
													}
													BgL_arg1836z00_1435 =
														(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3693))->
														BgL_itszd2superzd2);
												}
												{	/* Module/java.scm 404 */
													obj_t BgL_classz00_2238;

													BgL_classz00_2238 = BGl_typez00zztype_typez00;
													if (BGL_OBJECTP(BgL_arg1836z00_1435))
														{	/* Module/java.scm 404 */
															BgL_objectz00_bglt BgL_arg1807z00_2240;

															BgL_arg1807z00_2240 =
																(BgL_objectz00_bglt) (BgL_arg1836z00_1435);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Module/java.scm 404 */
																	long BgL_idxz00_2246;

																	BgL_idxz00_2246 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2240);
																	BgL_test2377z00_3692 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2246 + 1L)) ==
																		BgL_classz00_2238);
																}
															else
																{	/* Module/java.scm 404 */
																	bool_t BgL_res2095z00_2271;

																	{	/* Module/java.scm 404 */
																		obj_t BgL_oclassz00_2254;

																		{	/* Module/java.scm 404 */
																			obj_t BgL_arg1815z00_2262;
																			long BgL_arg1816z00_2263;

																			BgL_arg1815z00_2262 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Module/java.scm 404 */
																				long BgL_arg1817z00_2264;

																				BgL_arg1817z00_2264 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2240);
																				BgL_arg1816z00_2263 =
																					(BgL_arg1817z00_2264 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2254 =
																				VECTOR_REF(BgL_arg1815z00_2262,
																				BgL_arg1816z00_2263);
																		}
																		{	/* Module/java.scm 404 */
																			bool_t BgL__ortest_1115z00_2255;

																			BgL__ortest_1115z00_2255 =
																				(BgL_classz00_2238 ==
																				BgL_oclassz00_2254);
																			if (BgL__ortest_1115z00_2255)
																				{	/* Module/java.scm 404 */
																					BgL_res2095z00_2271 =
																						BgL__ortest_1115z00_2255;
																				}
																			else
																				{	/* Module/java.scm 404 */
																					long BgL_odepthz00_2256;

																					{	/* Module/java.scm 404 */
																						obj_t BgL_arg1804z00_2257;

																						BgL_arg1804z00_2257 =
																							(BgL_oclassz00_2254);
																						BgL_odepthz00_2256 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2257);
																					}
																					if ((1L < BgL_odepthz00_2256))
																						{	/* Module/java.scm 404 */
																							obj_t BgL_arg1802z00_2259;

																							{	/* Module/java.scm 404 */
																								obj_t BgL_arg1803z00_2260;

																								BgL_arg1803z00_2260 =
																									(BgL_oclassz00_2254);
																								BgL_arg1802z00_2259 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2260, 1L);
																							}
																							BgL_res2095z00_2271 =
																								(BgL_arg1802z00_2259 ==
																								BgL_classz00_2238);
																						}
																					else
																						{	/* Module/java.scm 404 */
																							BgL_res2095z00_2271 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2377z00_3692 = BgL_res2095z00_2271;
																}
														}
													else
														{	/* Module/java.scm 404 */
															BgL_test2377z00_3692 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2377z00_3692)
												{
													BgL_jclassz00_bglt BgL_auxz00_3722;

													{
														obj_t BgL_auxz00_3723;

														{	/* Module/java.scm 405 */
															BgL_objectz00_bglt BgL_tmpz00_3724;

															BgL_tmpz00_3724 =
																((BgL_objectz00_bglt) BgL_jclassz00_1426);
															BgL_auxz00_3723 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3724);
														}
														BgL_auxz00_3722 =
															((BgL_jclassz00_bglt) BgL_auxz00_3723);
													}
													BgL_typez00_1429 =
														(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3722))->
														BgL_itszd2superzd2);
												}
											else
												{	/* Module/java.scm 407 */
													obj_t BgL_arg1835z00_1434;

													{
														BgL_jclassz00_bglt BgL_auxz00_3729;

														{
															obj_t BgL_auxz00_3730;

															{	/* Module/java.scm 407 */
																BgL_objectz00_bglt BgL_tmpz00_3731;

																BgL_tmpz00_3731 =
																	((BgL_objectz00_bglt) BgL_jclassz00_1426);
																BgL_auxz00_3730 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3731);
															}
															BgL_auxz00_3729 =
																((BgL_jclassz00_bglt) BgL_auxz00_3730);
														}
														BgL_arg1835z00_1434 =
															(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3729))->
															BgL_itszd2superzd2);
													}
													BgL_typez00_1429 =
														((obj_t)
														BGl_findzd2typezd2zztype_envz00
														(BgL_arg1835z00_1434));
												}
										}
								}
								{
									BgL_jclassz00_bglt BgL_auxz00_3738;

									{
										obj_t BgL_auxz00_3739;

										{	/* Module/java.scm 408 */
											BgL_objectz00_bglt BgL_tmpz00_3740;

											BgL_tmpz00_3740 =
												((BgL_objectz00_bglt) BgL_jclassz00_1426);
											BgL_auxz00_3739 = BGL_OBJECT_WIDENING(BgL_tmpz00_3740);
										}
										BgL_auxz00_3738 = ((BgL_jclassz00_bglt) BgL_auxz00_3739);
									}
									((((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_3738))->
											BgL_itszd2superzd2) =
										((obj_t) BgL_typez00_1429), BUNSPEC);
								}
							}
						else
							{	/* Module/java.scm 400 */
								BFALSE;
							}
					}
					return BgL_jclassz00_1426;
				}
			}
		}

	}



/* declare-java-class! */
	BgL_typez00_bglt
		BGl_declarezd2javazd2classz12z12zzmodule_javaz00(BgL_jklassz00_bglt
		BgL_jklassz00_146)
	{
		{	/* Module/java.scm 426 */
			{	/* Module/java.scm 431 */
				obj_t BgL_pidz00_1506;

				BgL_pidz00_1506 =
					BGl_parsezd2idzd2zzast_identz00(
					(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_146))->BgL_idz00),
					(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_146))->BgL_locz00));
				{	/* Module/java.scm 431 */
					obj_t BgL_jidz00_1507;

					BgL_jidz00_1507 = CAR(BgL_pidz00_1506);
					{	/* Module/java.scm 432 */
						obj_t BgL_superz00_1508;

						BgL_superz00_1508 = CDR(BgL_pidz00_1506);
						{	/* Module/java.scm 433 */

							{	/* Module/java.scm 434 */
								obj_t BgL_arg1898z00_1509;

								BgL_arg1898z00_1509 =
									(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_146))->
									BgL_jnamez00);
								{	/* Module/java.scm 465 */
									obj_t BgL_arg1919z00_2294;

									BgL_arg1919z00_2294 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_jidz00_1507), ((obj_t) BgL_arg1898z00_1509));
									BGl_za2javazd2classesza2zd2zzmodule_javaz00 =
										MAKE_YOUNG_PAIR(BgL_arg1919z00_2294,
										BGl_za2javazd2classesza2zd2zzmodule_javaz00);
								}
							}
							{	/* Module/java.scm 437 */
								BgL_typez00_bglt BgL_jclassz00_1510;

								BgL_jclassz00_1510 =
									BGl_declarezd2javazd2classzd2typez12zc0zzobject_classz00
									(BgL_jidz00_1507, BgL_superz00_1508,
									(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_146))->
										BgL_jnamez00),
									(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_146))->
										BgL_packagez00),
									(((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_146))->
										BgL_srcz00));
								((((BgL_typez00_bglt) COBJECT(BgL_jclassz00_1510))->
										BgL_importzd2locationzd2) =
									((obj_t) (((BgL_jklassz00_bglt) COBJECT(BgL_jklassz00_146))->
											BgL_locz00)), BUNSPEC);
								{	/* Module/java.scm 446 */
									obj_t BgL_arg1901z00_1512;

									{	/* Module/java.scm 446 */
										obj_t BgL_zc3z04anonymousza31903ze3z87_2471;

										BgL_zc3z04anonymousza31903ze3z87_2471 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31903ze3ze5zzmodule_javaz00,
											(int) (0L), (int) (2L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31903ze3z87_2471,
											(int) (0L), ((obj_t) BgL_jklassz00_146));
										PROCEDURE_SET(BgL_zc3z04anonymousza31903ze3z87_2471,
											(int) (1L), ((obj_t) BgL_jclassz00_1510));
										BgL_arg1901z00_1512 =
											BGl_makezd2promisezd2zz__r4_control_features_6_9z00
											(BgL_zc3z04anonymousza31903ze3z87_2471);
									}
									BGl_delayzd2classzd2accessorsz12z12zzmodule_classz00
										(BgL_jclassz00_1510, BgL_arg1901z00_1512);
								}
								return ((BgL_typez00_bglt) BgL_jclassz00_1510);
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1903> */
	obj_t BGl_z62zc3z04anonymousza31903ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2472)
	{
		{	/* Module/java.scm 446 */
			{	/* Module/java.scm 446 */
				BgL_jklassz00_bglt BgL_i1131z00_2473;
				BgL_typez00_bglt BgL_jclassz00_2474;

				BgL_i1131z00_2473 =
					((BgL_jklassz00_bglt) PROCEDURE_REF(BgL_envz00_2472, (int) (0L)));
				BgL_jclassz00_2474 =
					((BgL_typez00_bglt) PROCEDURE_REF(BgL_envz00_2472, (int) (1L)));
				{	/* Module/java.scm 446 */
					obj_t BgL_arg1904z00_2739;
					obj_t BgL_arg1906z00_2740;
					bool_t BgL_arg1910z00_2741;
					obj_t BgL_arg1911z00_2742;
					obj_t BgL_arg1912z00_2743;

					BgL_arg1904z00_2739 =
						(((BgL_jklassz00_bglt) COBJECT(BgL_i1131z00_2473))->BgL_fieldsz00);
					BgL_arg1906z00_2740 =
						(((BgL_jklassz00_bglt) COBJECT(BgL_i1131z00_2473))->
						BgL_constructorsz00);
					BgL_arg1910z00_2741 =
						(((BgL_jklassz00_bglt) COBJECT(BgL_i1131z00_2473))->
						BgL_abstractzf3zf3);
					BgL_arg1911z00_2742 =
						(((BgL_jklassz00_bglt) COBJECT(BgL_i1131z00_2473))->BgL_modulez00);
					BgL_arg1912z00_2743 =
						(((BgL_jklassz00_bglt) COBJECT(BgL_i1131z00_2473))->BgL_srcz00);
					return
						BGl_importzd2javazd2classzd2accessorsz12zc0zzobject_javazd2accesszd2
						(BgL_arg1904z00_2739, BgL_arg1906z00_2740,
						((BgL_typez00_bglt) BgL_jclassz00_2474), BgL_arg1910z00_2741,
						BgL_arg1911z00_2742, BgL_arg1912z00_2743);
				}
			}
		}

	}



/* heap-add-jclass! */
	BGL_EXPORTED_DEF obj_t BGl_heapzd2addzd2jclassz12z12zzmodule_javaz00(obj_t
		BgL_newz00_149)
	{
		{	/* Module/java.scm 470 */
			{	/* Module/java.scm 472 */
				obj_t BgL_arg1920z00_2297;
				obj_t BgL_arg1923z00_2298;

				BgL_arg1920z00_2297 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_newz00_149))))->BgL_idz00);
				BgL_arg1923z00_2298 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_newz00_149))))->BgL_namez00);
				{	/* Module/java.scm 465 */
					obj_t BgL_arg1919z00_2303;

					BgL_arg1919z00_2303 =
						MAKE_YOUNG_PAIR(BgL_arg1920z00_2297, ((obj_t) BgL_arg1923z00_2298));
					return (BGl_za2javazd2classesza2zd2zzmodule_javaz00 =
						MAKE_YOUNG_PAIR(BgL_arg1919z00_2303,
							BGl_za2javazd2classesza2zd2zzmodule_javaz00), BUNSPEC);
				}
			}
		}

	}



/* &heap-add-jclass! */
	obj_t BGl_z62heapzd2addzd2jclassz12z70zzmodule_javaz00(obj_t BgL_envz00_2475,
		obj_t BgL_newz00_2476)
	{
		{	/* Module/java.scm 470 */
			return BGl_heapzd2addzd2jclassz12z12zzmodule_javaz00(BgL_newz00_2476);
		}

	}



/* find-java-class */
	BGL_EXPORTED_DEF obj_t BGl_findzd2javazd2classz00zzmodule_javaz00(obj_t
		BgL_classz00_150)
	{
		{	/* Module/java.scm 477 */
			{	/* Module/java.scm 478 */
				obj_t BgL_cellz00_1529;

				BgL_cellz00_1529 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_classz00_150,
					BGl_za2javazd2classesza2zd2zzmodule_javaz00);
				if (PAIRP(BgL_cellz00_1529))
					{	/* Module/java.scm 479 */
						return CDR(BgL_cellz00_1529);
					}
				else
					{	/* Module/java.scm 479 */
						return BFALSE;
					}
			}
		}

	}



/* &find-java-class */
	obj_t BGl_z62findzd2javazd2classz62zzmodule_javaz00(obj_t BgL_envz00_2477,
		obj_t BgL_classz00_2478)
	{
		{	/* Module/java.scm 477 */
			return BGl_findzd2javazd2classz00zzmodule_javaz00(BgL_classz00_2478);
		}

	}



/* java-declare-array */
	obj_t BGl_javazd2declarezd2arrayz00zzmodule_javaz00(obj_t BgL_jz00_151,
		obj_t BgL_idz00_152, obj_t BgL_ofz00_153)
	{
		{	/* Module/java.scm 486 */
			if (BGl_typezd2identzf3z21zzast_identz00(BgL_ofz00_153))
				{	/* Module/java.scm 491 */
					obj_t BgL_sofz00_1532;

					{	/* Module/java.scm 491 */
						obj_t BgL_arg1455z00_2306;

						BgL_arg1455z00_2306 = SYMBOL_TO_STRING(BgL_ofz00_153);
						BgL_sofz00_1532 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2306);
					}
					{	/* Module/java.scm 491 */
						obj_t BgL_tofz00_1533;

						BgL_tofz00_1533 =
							bstring_to_symbol(c_substring(BgL_sofz00_1532, 2L,
								STRING_LENGTH(BgL_sofz00_1532)));
						{	/* Module/java.scm 492 */
							BgL_typez00_bglt BgL_jtypez00_1534;

							BgL_jtypez00_1534 =
								BGl_declarezd2jvmzd2typez12z12zzforeign_jtypez00(BgL_idz00_152,
								BgL_tofz00_1533, BgL_jz00_151);
							{	/* Module/java.scm 493 */

								return
									BGl_foreignzd2accesseszd2addz12z12zzmodule_foreignz00
									(BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00
									(BgL_jtypez00_1534, BgL_jtypez00_1534,
										BGl_findzd2locationzd2zztools_locationz00(BgL_jz00_151)));
							}
						}
					}
				}
			else
				{	/* Module/java.scm 490 */
					obj_t BgL_list1930z00_1539;

					BgL_list1930z00_1539 =
						MAKE_YOUNG_PAIR(BGl_string2125z00zzmodule_javaz00, BNIL);
					return
						BGl_javazd2errorzd2zzmodule_javaz00(BgL_jz00_151,
						BgL_list1930z00_1539);
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 15 */
			{	/* Module/java.scm 48 */
				obj_t BgL_arg1934z00_1543;
				obj_t BgL_arg1935z00_1544;

				{	/* Module/java.scm 48 */
					obj_t BgL_v1202z00_1566;

					BgL_v1202z00_1566 = create_vector(11L);
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1942z00_1567;

						BgL_arg1942z00_1567 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc2127z00zzmodule_javaz00, BGl_proc2126z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1202z00_1566, 0L, BgL_arg1942z00_1567);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1947z00_1577;

						BgL_arg1947z00_1577 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc2129z00zzmodule_javaz00, BGl_proc2128z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(21));
						VECTOR_SET(BgL_v1202z00_1566, 1L, BgL_arg1947z00_1577);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1952z00_1587;

						BgL_arg1952z00_1587 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2131z00zzmodule_javaz00, BGl_proc2130z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(23));
						VECTOR_SET(BgL_v1202z00_1566, 2L, BgL_arg1952z00_1587);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1957z00_1597;

						BgL_arg1957z00_1597 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2133z00zzmodule_javaz00, BGl_proc2132z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(23));
						VECTOR_SET(BgL_v1202z00_1566, 3L, BgL_arg1957z00_1597);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1962z00_1607;

						BgL_arg1962z00_1607 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2136z00zzmodule_javaz00, BGl_proc2135z00zzmodule_javaz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2134z00zzmodule_javaz00, CNST_TABLE_REF(21));
						VECTOR_SET(BgL_v1202z00_1566, 4L, BgL_arg1962z00_1607);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1969z00_1620;

						BgL_arg1969z00_1620 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc2139z00zzmodule_javaz00, BGl_proc2138z00zzmodule_javaz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2137z00zzmodule_javaz00, CNST_TABLE_REF(21));
						VECTOR_SET(BgL_v1202z00_1566, 5L, BgL_arg1969z00_1620);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1976z00_1633;

						BgL_arg1976z00_1633 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2142z00zzmodule_javaz00, BGl_proc2141z00zzmodule_javaz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2140z00zzmodule_javaz00, CNST_TABLE_REF(28));
						VECTOR_SET(BgL_v1202z00_1566, 6L, BgL_arg1976z00_1633);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1983z00_1646;

						BgL_arg1983z00_1646 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(29),
							BGl_proc2145z00zzmodule_javaz00, BGl_proc2144z00zzmodule_javaz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2143z00zzmodule_javaz00, CNST_TABLE_REF(28));
						VECTOR_SET(BgL_v1202z00_1566, 7L, BgL_arg1983z00_1646);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1990z00_1659;

						BgL_arg1990z00_1659 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(30),
							BGl_proc2148z00zzmodule_javaz00, BGl_proc2147z00zzmodule_javaz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2146z00zzmodule_javaz00, CNST_TABLE_REF(28));
						VECTOR_SET(BgL_v1202z00_1566, 8L, BgL_arg1990z00_1659);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg1997z00_1672;

						BgL_arg1997z00_1672 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc2151z00zzmodule_javaz00, BGl_proc2150z00zzmodule_javaz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2149z00zzmodule_javaz00, CNST_TABLE_REF(32));
						VECTOR_SET(BgL_v1202z00_1566, 9L, BgL_arg1997z00_1672);
					}
					{	/* Module/java.scm 48 */
						obj_t BgL_arg2004z00_1685;

						BgL_arg2004z00_1685 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc2154z00zzmodule_javaz00, BGl_proc2153z00zzmodule_javaz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2152z00zzmodule_javaz00, CNST_TABLE_REF(21));
						VECTOR_SET(BgL_v1202z00_1566, 10L, BgL_arg2004z00_1685);
					}
					BgL_arg1934z00_1543 = BgL_v1202z00_1566;
				}
				{	/* Module/java.scm 48 */
					obj_t BgL_v1203z00_1698;

					BgL_v1203z00_1698 = create_vector(0L);
					BgL_arg1935z00_1544 = BgL_v1203z00_1698;
				}
				BGl_jklassz00zzmodule_javaz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(5),
					CNST_TABLE_REF(34), BGl_objectz00zz__objectz00, 1145L,
					BGl_proc2157z00zzmodule_javaz00, BGl_proc2156z00zzmodule_javaz00,
					BGl_bindzd2jklassz12zd2envz12zzmodule_javaz00,
					BGl_proc2155z00zzmodule_javaz00, BFALSE, BgL_arg1934z00_1543,
					BgL_arg1935z00_1544);
			}
			{	/* Module/java.scm 61 */
				obj_t BgL_arg2015z00_1705;
				obj_t BgL_arg2016z00_1706;

				{	/* Module/java.scm 61 */
					obj_t BgL_v1204z00_1721;

					BgL_v1204z00_1721 = create_vector(5L);
					{	/* Module/java.scm 61 */
						obj_t BgL_arg2022z00_1722;

						BgL_arg2022z00_1722 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc2159z00zzmodule_javaz00, BGl_proc2158z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(19));
						VECTOR_SET(BgL_v1204z00_1721, 0L, BgL_arg2022z00_1722);
					}
					{	/* Module/java.scm 61 */
						obj_t BgL_arg2029z00_1732;

						BgL_arg2029z00_1732 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2161z00zzmodule_javaz00, BGl_proc2160z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(23));
						VECTOR_SET(BgL_v1204z00_1721, 1L, BgL_arg2029z00_1732);
					}
					{	/* Module/java.scm 61 */
						obj_t BgL_arg2034z00_1742;

						BgL_arg2034z00_1742 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(35),
							BGl_proc2163z00zzmodule_javaz00, BGl_proc2162z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(28));
						VECTOR_SET(BgL_v1204z00_1721, 2L, BgL_arg2034z00_1742);
					}
					{	/* Module/java.scm 61 */
						obj_t BgL_arg2040z00_1752;

						BgL_arg2040z00_1752 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2165z00zzmodule_javaz00, BGl_proc2164z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(36));
						VECTOR_SET(BgL_v1204z00_1721, 3L, BgL_arg2040z00_1752);
					}
					{	/* Module/java.scm 61 */
						obj_t BgL_arg2045z00_1762;

						BgL_arg2045z00_1762 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(37),
							BGl_proc2168z00zzmodule_javaz00, BGl_proc2167z00zzmodule_javaz00,
							((bool_t) 1), ((bool_t) 0), BFALSE,
							BGl_proc2166z00zzmodule_javaz00, CNST_TABLE_REF(28));
						VECTOR_SET(BgL_v1204z00_1721, 4L, BgL_arg2045z00_1762);
					}
					BgL_arg2015z00_1705 = BgL_v1204z00_1721;
				}
				{	/* Module/java.scm 61 */
					obj_t BgL_v1205z00_1775;

					BgL_v1205z00_1775 = create_vector(0L);
					BgL_arg2016z00_1706 = BgL_v1205z00_1775;
				}
				BGl_jmethodz00zzmodule_javaz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(38),
					CNST_TABLE_REF(34), BGl_objectz00zz__objectz00, 58716L,
					BGl_proc2171z00zzmodule_javaz00, BGl_proc2170z00zzmodule_javaz00,
					BFALSE, BGl_proc2169z00zzmodule_javaz00, BFALSE, BgL_arg2015z00_1705,
					BgL_arg2016z00_1706);
			}
			{	/* Module/java.scm 67 */
				obj_t BgL_arg2057z00_1782;
				obj_t BgL_arg2058z00_1783;

				{	/* Module/java.scm 67 */
					obj_t BgL_v1206z00_1798;

					BgL_v1206z00_1798 = create_vector(0L);
					BgL_arg2057z00_1782 = BgL_v1206z00_1798;
				}
				{	/* Module/java.scm 67 */
					obj_t BgL_v1207z00_1799;

					BgL_v1207z00_1799 = create_vector(0L);
					BgL_arg2058z00_1783 = BgL_v1207z00_1799;
				}
				return (BGl_jconstructorz00zzmodule_javaz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(39),
						CNST_TABLE_REF(34), BGl_jmethodz00zzmodule_javaz00, 37056L,
						BGl_proc2174z00zzmodule_javaz00, BGl_proc2173z00zzmodule_javaz00,
						BFALSE, BGl_proc2172z00zzmodule_javaz00, BFALSE,
						BgL_arg2057z00_1782, BgL_arg2058z00_1783), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2063> */
	obj_t BGl_z62zc3z04anonymousza32063ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2528, obj_t BgL_new1095z00_2529)
	{
		{	/* Module/java.scm 67 */
			{
				BgL_jconstructorz00_bglt BgL_auxz00_3893;

				((((BgL_jmethodz00_bglt) COBJECT(
								((BgL_jmethodz00_bglt)
									((BgL_jconstructorz00_bglt) BgL_new1095z00_2529))))->
						BgL_srcz00) = ((obj_t) MAKE_YOUNG_PAIR(BFALSE, BFALSE)), BUNSPEC);
				((((BgL_jmethodz00_bglt)
							COBJECT(((BgL_jmethodz00_bglt) ((BgL_jconstructorz00_bglt)
										BgL_new1095z00_2529))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(40)), BUNSPEC);
				((((BgL_jmethodz00_bglt)
							COBJECT(((BgL_jmethodz00_bglt) ((BgL_jconstructorz00_bglt)
										BgL_new1095z00_2529))))->BgL_argsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_jmethodz00_bglt)
							COBJECT(((BgL_jmethodz00_bglt) ((BgL_jconstructorz00_bglt)
										BgL_new1095z00_2529))))->BgL_jnamez00) =
					((obj_t) BGl_string2106z00zzmodule_javaz00), BUNSPEC);
				((((BgL_jmethodz00_bglt)
							COBJECT(((BgL_jmethodz00_bglt) ((BgL_jconstructorz00_bglt)
										BgL_new1095z00_2529))))->BgL_modifiersz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_3893 = ((BgL_jconstructorz00_bglt) BgL_new1095z00_2529);
				return ((obj_t) BgL_auxz00_3893);
			}
		}

	}



/* &lambda2061 */
	BgL_jconstructorz00_bglt BGl_z62lambda2061z62zzmodule_javaz00(obj_t
		BgL_envz00_2530)
	{
		{	/* Module/java.scm 67 */
			{	/* Module/java.scm 67 */
				BgL_jconstructorz00_bglt BgL_new1094z00_2745;

				BgL_new1094z00_2745 =
					((BgL_jconstructorz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jconstructorz00_bgl))));
				{	/* Module/java.scm 67 */
					long BgL_arg2062z00_2746;

					BgL_arg2062z00_2746 =
						BGL_CLASS_NUM(BGl_jconstructorz00zzmodule_javaz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1094z00_2745), BgL_arg2062z00_2746);
				}
				return BgL_new1094z00_2745;
			}
		}

	}



/* &lambda2059 */
	BgL_jconstructorz00_bglt BGl_z62lambda2059z62zzmodule_javaz00(obj_t
		BgL_envz00_2531, obj_t BgL_src1089z00_2532, obj_t BgL_id1090z00_2533,
		obj_t BgL_args1091z00_2534, obj_t BgL_jname1092z00_2535,
		obj_t BgL_modifiers1093z00_2536)
	{
		{	/* Module/java.scm 67 */
			{	/* Module/java.scm 67 */
				BgL_jconstructorz00_bglt BgL_new1138z00_2752;

				{	/* Module/java.scm 67 */
					BgL_jconstructorz00_bglt BgL_new1137z00_2753;

					BgL_new1137z00_2753 =
						((BgL_jconstructorz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jconstructorz00_bgl))));
					{	/* Module/java.scm 67 */
						long BgL_arg2060z00_2754;

						BgL_arg2060z00_2754 =
							BGL_CLASS_NUM(BGl_jconstructorz00zzmodule_javaz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1137z00_2753), BgL_arg2060z00_2754);
					}
					BgL_new1138z00_2752 = BgL_new1137z00_2753;
				}
				((((BgL_jmethodz00_bglt) COBJECT(
								((BgL_jmethodz00_bglt) BgL_new1138z00_2752)))->BgL_srcz00) =
					((obj_t) ((obj_t) BgL_src1089z00_2532)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt)
									BgL_new1138z00_2752)))->BgL_idz00) =
					((obj_t) ((obj_t) BgL_id1090z00_2533)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt)
									BgL_new1138z00_2752)))->BgL_argsz00) =
					((obj_t) ((obj_t) BgL_args1091z00_2534)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt)
									BgL_new1138z00_2752)))->BgL_jnamez00) =
					((obj_t) ((obj_t) BgL_jname1092z00_2535)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt)
									BgL_new1138z00_2752)))->BgL_modifiersz00) =
					((obj_t) ((obj_t) BgL_modifiers1093z00_2536)), BUNSPEC);
				return BgL_new1138z00_2752;
			}
		}

	}



/* &<@anonymous:2021> */
	obj_t BGl_z62zc3z04anonymousza32021ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2537, obj_t BgL_new1087z00_2538)
	{
		{	/* Module/java.scm 61 */
			{
				BgL_jmethodz00_bglt BgL_auxz00_3936;

				((((BgL_jmethodz00_bglt) COBJECT(
								((BgL_jmethodz00_bglt) BgL_new1087z00_2538)))->BgL_srcz00) =
					((obj_t) MAKE_YOUNG_PAIR(BFALSE, BFALSE)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt)
									BgL_new1087z00_2538)))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(40)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt)
									BgL_new1087z00_2538)))->BgL_argsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt)
									BgL_new1087z00_2538)))->BgL_jnamez00) =
					((obj_t) BGl_string2106z00zzmodule_javaz00), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(((BgL_jmethodz00_bglt)
									BgL_new1087z00_2538)))->BgL_modifiersz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_3936 = ((BgL_jmethodz00_bglt) BgL_new1087z00_2538);
				return ((obj_t) BgL_auxz00_3936);
			}
		}

	}



/* &lambda2019 */
	BgL_jmethodz00_bglt BGl_z62lambda2019z62zzmodule_javaz00(obj_t
		BgL_envz00_2539)
	{
		{	/* Module/java.scm 61 */
			{	/* Module/java.scm 61 */
				BgL_jmethodz00_bglt BgL_new1086z00_2756;

				BgL_new1086z00_2756 =
					((BgL_jmethodz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jmethodz00_bgl))));
				{	/* Module/java.scm 61 */
					long BgL_arg2020z00_2757;

					BgL_arg2020z00_2757 = BGL_CLASS_NUM(BGl_jmethodz00zzmodule_javaz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1086z00_2756), BgL_arg2020z00_2757);
				}
				return BgL_new1086z00_2756;
			}
		}

	}



/* &lambda2017 */
	BgL_jmethodz00_bglt BGl_z62lambda2017z62zzmodule_javaz00(obj_t
		BgL_envz00_2540, obj_t BgL_src1081z00_2541, obj_t BgL_id1082z00_2542,
		obj_t BgL_args1083z00_2543, obj_t BgL_jname1084z00_2544,
		obj_t BgL_modifiers1085z00_2545)
	{
		{	/* Module/java.scm 61 */
			{	/* Module/java.scm 61 */
				BgL_jmethodz00_bglt BgL_new1136z00_2763;

				{	/* Module/java.scm 61 */
					BgL_jmethodz00_bglt BgL_new1135z00_2764;

					BgL_new1135z00_2764 =
						((BgL_jmethodz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jmethodz00_bgl))));
					{	/* Module/java.scm 61 */
						long BgL_arg2018z00_2765;

						BgL_arg2018z00_2765 = BGL_CLASS_NUM(BGl_jmethodz00zzmodule_javaz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1135z00_2764), BgL_arg2018z00_2765);
					}
					BgL_new1136z00_2763 = BgL_new1135z00_2764;
				}
				((((BgL_jmethodz00_bglt) COBJECT(BgL_new1136z00_2763))->BgL_srcz00) =
					((obj_t) ((obj_t) BgL_src1081z00_2541)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(BgL_new1136z00_2763))->BgL_idz00) =
					((obj_t) ((obj_t) BgL_id1082z00_2542)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(BgL_new1136z00_2763))->BgL_argsz00) =
					((obj_t) ((obj_t) BgL_args1083z00_2543)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(BgL_new1136z00_2763))->BgL_jnamez00) =
					((obj_t) ((obj_t) BgL_jname1084z00_2544)), BUNSPEC);
				((((BgL_jmethodz00_bglt) COBJECT(BgL_new1136z00_2763))->
						BgL_modifiersz00) =
					((obj_t) ((obj_t) BgL_modifiers1085z00_2545)), BUNSPEC);
				return BgL_new1136z00_2763;
			}
		}

	}



/* &<@anonymous:2051> */
	obj_t BGl_z62zc3z04anonymousza32051ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2546)
	{
		{	/* Module/java.scm 61 */
			return BNIL;
		}

	}



/* &lambda2050 */
	obj_t BGl_z62lambda2050z62zzmodule_javaz00(obj_t BgL_envz00_2547,
		obj_t BgL_oz00_2548, obj_t BgL_vz00_2549)
	{
		{	/* Module/java.scm 61 */
			return
				((((BgL_jmethodz00_bglt) COBJECT(
							((BgL_jmethodz00_bglt) BgL_oz00_2548)))->BgL_modifiersz00) =
				((obj_t) ((obj_t) BgL_vz00_2549)), BUNSPEC);
		}

	}



/* &lambda2049 */
	obj_t BGl_z62lambda2049z62zzmodule_javaz00(obj_t BgL_envz00_2550,
		obj_t BgL_oz00_2551)
	{
		{	/* Module/java.scm 61 */
			return
				(((BgL_jmethodz00_bglt) COBJECT(
						((BgL_jmethodz00_bglt) BgL_oz00_2551)))->BgL_modifiersz00);
		}

	}



/* &lambda2044 */
	obj_t BGl_z62lambda2044z62zzmodule_javaz00(obj_t BgL_envz00_2552,
		obj_t BgL_oz00_2553, obj_t BgL_vz00_2554)
	{
		{	/* Module/java.scm 61 */
			return
				((((BgL_jmethodz00_bglt) COBJECT(
							((BgL_jmethodz00_bglt) BgL_oz00_2553)))->BgL_jnamez00) = ((obj_t)
					((obj_t) BgL_vz00_2554)), BUNSPEC);
		}

	}



/* &lambda2043 */
	obj_t BGl_z62lambda2043z62zzmodule_javaz00(obj_t BgL_envz00_2555,
		obj_t BgL_oz00_2556)
	{
		{	/* Module/java.scm 61 */
			return
				(((BgL_jmethodz00_bglt) COBJECT(
						((BgL_jmethodz00_bglt) BgL_oz00_2556)))->BgL_jnamez00);
		}

	}



/* &lambda2039 */
	obj_t BGl_z62lambda2039z62zzmodule_javaz00(obj_t BgL_envz00_2557,
		obj_t BgL_oz00_2558, obj_t BgL_vz00_2559)
	{
		{	/* Module/java.scm 61 */
			return
				((((BgL_jmethodz00_bglt) COBJECT(
							((BgL_jmethodz00_bglt) BgL_oz00_2558)))->BgL_argsz00) = ((obj_t)
					((obj_t) BgL_vz00_2559)), BUNSPEC);
		}

	}



/* &lambda2038 */
	obj_t BGl_z62lambda2038z62zzmodule_javaz00(obj_t BgL_envz00_2560,
		obj_t BgL_oz00_2561)
	{
		{	/* Module/java.scm 61 */
			return
				(((BgL_jmethodz00_bglt) COBJECT(
						((BgL_jmethodz00_bglt) BgL_oz00_2561)))->BgL_argsz00);
		}

	}



/* &lambda2033 */
	obj_t BGl_z62lambda2033z62zzmodule_javaz00(obj_t BgL_envz00_2562,
		obj_t BgL_oz00_2563, obj_t BgL_vz00_2564)
	{
		{	/* Module/java.scm 61 */
			return
				((((BgL_jmethodz00_bglt) COBJECT(
							((BgL_jmethodz00_bglt) BgL_oz00_2563)))->BgL_idz00) = ((obj_t)
					((obj_t) BgL_vz00_2564)), BUNSPEC);
		}

	}



/* &lambda2032 */
	obj_t BGl_z62lambda2032z62zzmodule_javaz00(obj_t BgL_envz00_2565,
		obj_t BgL_oz00_2566)
	{
		{	/* Module/java.scm 61 */
			return
				(((BgL_jmethodz00_bglt) COBJECT(
						((BgL_jmethodz00_bglt) BgL_oz00_2566)))->BgL_idz00);
		}

	}



/* &lambda2027 */
	obj_t BGl_z62lambda2027z62zzmodule_javaz00(obj_t BgL_envz00_2567,
		obj_t BgL_oz00_2568, obj_t BgL_vz00_2569)
	{
		{	/* Module/java.scm 61 */
			return
				((((BgL_jmethodz00_bglt) COBJECT(
							((BgL_jmethodz00_bglt) BgL_oz00_2568)))->BgL_srcz00) = ((obj_t)
					((obj_t) BgL_vz00_2569)), BUNSPEC);
		}

	}



/* &lambda2026 */
	obj_t BGl_z62lambda2026z62zzmodule_javaz00(obj_t BgL_envz00_2570,
		obj_t BgL_oz00_2571)
	{
		{	/* Module/java.scm 61 */
			return
				(((BgL_jmethodz00_bglt) COBJECT(
						((BgL_jmethodz00_bglt) BgL_oz00_2571)))->BgL_srcz00);
		}

	}



/* &<@anonymous:1941> */
	obj_t BGl_z62zc3z04anonymousza31941ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2572, obj_t BgL_new1079z00_2573)
	{
		{	/* Module/java.scm 48 */
			{
				BgL_jklassz00_bglt BgL_auxz00_3994;

				((((BgL_jklassz00_bglt) COBJECT(
								((BgL_jklassz00_bglt) BgL_new1079z00_2573)))->BgL_srcz00) =
					((obj_t) MAKE_YOUNG_PAIR(BFALSE, BFALSE)), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(40)), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_iddz00) =
					((obj_t) CNST_TABLE_REF(40)), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_jnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_packagez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_fieldsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_methodsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_constructorsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_abstractzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_jklassz00_bglt) COBJECT(((BgL_jklassz00_bglt)
									BgL_new1079z00_2573)))->BgL_modulez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_3994 = ((BgL_jklassz00_bglt) BgL_new1079z00_2573);
				return ((obj_t) BgL_auxz00_3994);
			}
		}

	}



/* &lambda1939 */
	BgL_jklassz00_bglt BGl_z62lambda1939z62zzmodule_javaz00(obj_t BgL_envz00_2574)
	{
		{	/* Module/java.scm 48 */
			{	/* Module/java.scm 48 */
				BgL_jklassz00_bglt BgL_new1078z00_2782;

				BgL_new1078z00_2782 =
					((BgL_jklassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jklassz00_bgl))));
				{	/* Module/java.scm 48 */
					long BgL_arg1940z00_2783;

					BgL_arg1940z00_2783 = BGL_CLASS_NUM(BGl_jklassz00zzmodule_javaz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1078z00_2782), BgL_arg1940z00_2783);
				}
				return BgL_new1078z00_2782;
			}
		}

	}



/* &lambda1936 */
	BgL_jklassz00_bglt BGl_z62lambda1936z62zzmodule_javaz00(obj_t BgL_envz00_2575,
		obj_t BgL_src1067z00_2576, obj_t BgL_loc1068z00_2577,
		obj_t BgL_id1069z00_2578, obj_t BgL_idd1070z00_2579,
		obj_t BgL_jname1071z00_2580, obj_t BgL_package1072z00_2581,
		obj_t BgL_fields1073z00_2582, obj_t BgL_methods1074z00_2583,
		obj_t BgL_constructors1075z00_2584, obj_t BgL_abstractzf31076zf3_2585,
		obj_t BgL_module1077z00_2586)
	{
		{	/* Module/java.scm 48 */
			{	/* Module/java.scm 48 */
				bool_t BgL_abstractzf31076zf3_2790;

				BgL_abstractzf31076zf3_2790 = CBOOL(BgL_abstractzf31076zf3_2585);
				{	/* Module/java.scm 48 */
					BgL_jklassz00_bglt BgL_new1134z00_2791;

					{	/* Module/java.scm 48 */
						BgL_jklassz00_bglt BgL_new1133z00_2792;

						BgL_new1133z00_2792 =
							((BgL_jklassz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_jklassz00_bgl))));
						{	/* Module/java.scm 48 */
							long BgL_arg1938z00_2793;

							BgL_arg1938z00_2793 =
								BGL_CLASS_NUM(BGl_jklassz00zzmodule_javaz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1133z00_2792),
								BgL_arg1938z00_2793);
						}
						BgL_new1134z00_2791 = BgL_new1133z00_2792;
					}
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->BgL_srcz00) =
						((obj_t) ((obj_t) BgL_src1067z00_2576)), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->BgL_locz00) =
						((obj_t) BgL_loc1068z00_2577), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1069z00_2578)), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->BgL_iddz00) =
						((obj_t) ((obj_t) BgL_idd1070z00_2579)), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->BgL_jnamez00) =
						((obj_t) BgL_jname1071z00_2580), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->
							BgL_packagez00) = ((obj_t) BgL_package1072z00_2581), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->
							BgL_fieldsz00) =
						((obj_t) ((obj_t) BgL_fields1073z00_2582)), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->
							BgL_methodsz00) =
						((obj_t) ((obj_t) BgL_methods1074z00_2583)), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->
							BgL_constructorsz00) =
						((obj_t) ((obj_t) BgL_constructors1075z00_2584)), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->
							BgL_abstractzf3zf3) =
						((bool_t) BgL_abstractzf31076zf3_2790), BUNSPEC);
					((((BgL_jklassz00_bglt) COBJECT(BgL_new1134z00_2791))->
							BgL_modulez00) = ((obj_t) BgL_module1077z00_2586), BUNSPEC);
					{	/* Module/java.scm 48 */
						obj_t BgL_fun1937z00_2794;

						BgL_fun1937z00_2794 =
							BGl_classzd2constructorzd2zz__objectz00
							(BGl_jklassz00zzmodule_javaz00);
						BGL_PROCEDURE_CALL1(BgL_fun1937z00_2794,
							((obj_t) BgL_new1134z00_2791));
					}
					return BgL_new1134z00_2791;
				}
			}
		}

	}



/* &<@anonymous:2011> */
	obj_t BGl_z62zc3z04anonymousza32011ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2587)
	{
		{	/* Module/java.scm 48 */
			return BUNSPEC;
		}

	}



/* &lambda2010 */
	obj_t BGl_z62lambda2010z62zzmodule_javaz00(obj_t BgL_envz00_2588,
		obj_t BgL_oz00_2589, obj_t BgL_vz00_2590)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2589)))->BgL_modulez00) =
				((obj_t) BgL_vz00_2590), BUNSPEC);
		}

	}



/* &lambda2009 */
	obj_t BGl_z62lambda2009z62zzmodule_javaz00(obj_t BgL_envz00_2591,
		obj_t BgL_oz00_2592)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2592)))->BgL_modulez00);
		}

	}



/* &<@anonymous:2003> */
	obj_t BGl_z62zc3z04anonymousza32003ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2593)
	{
		{	/* Module/java.scm 48 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2002 */
	obj_t BGl_z62lambda2002z62zzmodule_javaz00(obj_t BgL_envz00_2594,
		obj_t BgL_oz00_2595, obj_t BgL_vz00_2596)
	{
		{	/* Module/java.scm 48 */
			{	/* Module/java.scm 48 */
				bool_t BgL_vz00_2798;

				BgL_vz00_2798 = CBOOL(BgL_vz00_2596);
				return
					((((BgL_jklassz00_bglt) COBJECT(
								((BgL_jklassz00_bglt) BgL_oz00_2595)))->BgL_abstractzf3zf3) =
					((bool_t) BgL_vz00_2798), BUNSPEC);
			}
		}

	}



/* &lambda2001 */
	obj_t BGl_z62lambda2001z62zzmodule_javaz00(obj_t BgL_envz00_2597,
		obj_t BgL_oz00_2598)
	{
		{	/* Module/java.scm 48 */
			return
				BBOOL(
				(((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2598)))->BgL_abstractzf3zf3));
		}

	}



/* &<@anonymous:1996> */
	obj_t BGl_z62zc3z04anonymousza31996ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2599)
	{
		{	/* Module/java.scm 48 */
			return BNIL;
		}

	}



/* &lambda1995 */
	obj_t BGl_z62lambda1995z62zzmodule_javaz00(obj_t BgL_envz00_2600,
		obj_t BgL_oz00_2601, obj_t BgL_vz00_2602)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2601)))->BgL_constructorsz00) =
				((obj_t) ((obj_t) BgL_vz00_2602)), BUNSPEC);
		}

	}



/* &lambda1994 */
	obj_t BGl_z62lambda1994z62zzmodule_javaz00(obj_t BgL_envz00_2603,
		obj_t BgL_oz00_2604)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2604)))->BgL_constructorsz00);
		}

	}



/* &<@anonymous:1989> */
	obj_t BGl_z62zc3z04anonymousza31989ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2605)
	{
		{	/* Module/java.scm 48 */
			return BNIL;
		}

	}



/* &lambda1988 */
	obj_t BGl_z62lambda1988z62zzmodule_javaz00(obj_t BgL_envz00_2606,
		obj_t BgL_oz00_2607, obj_t BgL_vz00_2608)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2607)))->BgL_methodsz00) = ((obj_t)
					((obj_t) BgL_vz00_2608)), BUNSPEC);
		}

	}



/* &lambda1987 */
	obj_t BGl_z62lambda1987z62zzmodule_javaz00(obj_t BgL_envz00_2609,
		obj_t BgL_oz00_2610)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2610)))->BgL_methodsz00);
		}

	}



/* &<@anonymous:1982> */
	obj_t BGl_z62zc3z04anonymousza31982ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2611)
	{
		{	/* Module/java.scm 48 */
			return BNIL;
		}

	}



/* &lambda1981 */
	obj_t BGl_z62lambda1981z62zzmodule_javaz00(obj_t BgL_envz00_2612,
		obj_t BgL_oz00_2613, obj_t BgL_vz00_2614)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2613)))->BgL_fieldsz00) = ((obj_t)
					((obj_t) BgL_vz00_2614)), BUNSPEC);
		}

	}



/* &lambda1980 */
	obj_t BGl_z62lambda1980z62zzmodule_javaz00(obj_t BgL_envz00_2615,
		obj_t BgL_oz00_2616)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2616)))->BgL_fieldsz00);
		}

	}



/* &<@anonymous:1975> */
	obj_t BGl_z62zc3z04anonymousza31975ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2617)
	{
		{	/* Module/java.scm 48 */
			return BUNSPEC;
		}

	}



/* &lambda1974 */
	obj_t BGl_z62lambda1974z62zzmodule_javaz00(obj_t BgL_envz00_2618,
		obj_t BgL_oz00_2619, obj_t BgL_vz00_2620)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2619)))->BgL_packagez00) =
				((obj_t) BgL_vz00_2620), BUNSPEC);
		}

	}



/* &lambda1973 */
	obj_t BGl_z62lambda1973z62zzmodule_javaz00(obj_t BgL_envz00_2621,
		obj_t BgL_oz00_2622)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2622)))->BgL_packagez00);
		}

	}



/* &<@anonymous:1968> */
	obj_t BGl_z62zc3z04anonymousza31968ze3ze5zzmodule_javaz00(obj_t
		BgL_envz00_2623)
	{
		{	/* Module/java.scm 48 */
			return BUNSPEC;
		}

	}



/* &lambda1967 */
	obj_t BGl_z62lambda1967z62zzmodule_javaz00(obj_t BgL_envz00_2624,
		obj_t BgL_oz00_2625, obj_t BgL_vz00_2626)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2625)))->BgL_jnamez00) =
				((obj_t) BgL_vz00_2626), BUNSPEC);
		}

	}



/* &lambda1966 */
	obj_t BGl_z62lambda1966z62zzmodule_javaz00(obj_t BgL_envz00_2627,
		obj_t BgL_oz00_2628)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2628)))->BgL_jnamez00);
		}

	}



/* &lambda1961 */
	obj_t BGl_z62lambda1961z62zzmodule_javaz00(obj_t BgL_envz00_2629,
		obj_t BgL_oz00_2630, obj_t BgL_vz00_2631)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2630)))->BgL_iddz00) = ((obj_t)
					((obj_t) BgL_vz00_2631)), BUNSPEC);
		}

	}



/* &lambda1960 */
	obj_t BGl_z62lambda1960z62zzmodule_javaz00(obj_t BgL_envz00_2632,
		obj_t BgL_oz00_2633)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2633)))->BgL_iddz00);
		}

	}



/* &lambda1956 */
	obj_t BGl_z62lambda1956z62zzmodule_javaz00(obj_t BgL_envz00_2634,
		obj_t BgL_oz00_2635, obj_t BgL_vz00_2636)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2635)))->BgL_idz00) = ((obj_t)
					((obj_t) BgL_vz00_2636)), BUNSPEC);
		}

	}



/* &lambda1955 */
	obj_t BGl_z62lambda1955z62zzmodule_javaz00(obj_t BgL_envz00_2637,
		obj_t BgL_oz00_2638)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2638)))->BgL_idz00);
		}

	}



/* &lambda1951 */
	obj_t BGl_z62lambda1951z62zzmodule_javaz00(obj_t BgL_envz00_2639,
		obj_t BgL_oz00_2640, obj_t BgL_vz00_2641)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2640)))->BgL_locz00) =
				((obj_t) BgL_vz00_2641), BUNSPEC);
		}

	}



/* &lambda1950 */
	obj_t BGl_z62lambda1950z62zzmodule_javaz00(obj_t BgL_envz00_2642,
		obj_t BgL_oz00_2643)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2643)))->BgL_locz00);
		}

	}



/* &lambda1946 */
	obj_t BGl_z62lambda1946z62zzmodule_javaz00(obj_t BgL_envz00_2644,
		obj_t BgL_oz00_2645, obj_t BgL_vz00_2646)
	{
		{	/* Module/java.scm 48 */
			return
				((((BgL_jklassz00_bglt) COBJECT(
							((BgL_jklassz00_bglt) BgL_oz00_2645)))->BgL_srcz00) = ((obj_t)
					((obj_t) BgL_vz00_2646)), BUNSPEC);
		}

	}



/* &lambda1945 */
	obj_t BGl_z62lambda1945z62zzmodule_javaz00(obj_t BgL_envz00_2647,
		obj_t BgL_oz00_2648)
	{
		{	/* Module/java.scm 48 */
			return
				(((BgL_jklassz00_bglt) COBJECT(
						((BgL_jklassz00_bglt) BgL_oz00_2648)))->BgL_srcz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_javaz00(void)
	{
		{	/* Module/java.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzmodule_checksumz00(457474182L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzmodule_prototypez00(499400840L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzmodule_foreignz00(42743976L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzobject_javazd2accesszd2(251162345L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzread_jvmz00(261574382L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			BGl_modulezd2initializa7ationz75zzforeign_jtypez00(287572863L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
			return
				BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string2175z00zzmodule_javaz00));
		}

	}

#ifdef __cplusplus
}
#endif
