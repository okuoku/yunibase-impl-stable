/*===========================================================================*/
/*   (Module/alibrary.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/alibrary.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_ALIBRARY_TYPE_DEFINITIONS
#define BGL_MODULE_ALIBRARY_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_MODULE_ALIBRARY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62loadzd2libraryzd2initz62zzmodule_alibraryz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_alibraryz00 = BUNSPEC;
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzmodule_alibraryz00(void);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_alibraryz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_alibraryz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00;
	static obj_t BGl_evalzd2initzd2zzmodule_alibraryz00(void);
	static obj_t BGl_z62zc3z04anonymousza31160ze3ze5zzmodule_alibraryz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31161ze3ze5zzmodule_alibraryz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2alibraryzd2initsz00zzmodule_alibraryz00(void);
	static obj_t BGl_methodzd2initzd2zzmodule_alibraryz00(void);
	BGL_IMPORT obj_t BGl_libraryzd2infozd2zz__libraryz00(obj_t);
	static obj_t BGl_registerzd2libraryzd2initz12z12zzmodule_alibraryz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2alibraryzd2compilerz00zzmodule_alibraryz00(void);
	static obj_t BGl_z62usezd2libraryz12za2zzmodule_alibraryz00(obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	static obj_t BGl_z62zc3z04anonymousza31185ze3ze5zzmodule_alibraryz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__libraryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evenvz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_za2libraryzd2initza2zd2zzmodule_alibraryz00 = BUNSPEC;
	extern obj_t BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_loadqz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00(void);
	static obj_t BGl_cnstzd2initzd2zzmodule_alibraryz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_alibraryz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_alibraryz00(void);
	static obj_t BGl_z62makezd2alibraryzd2compilerz62zzmodule_alibraryz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_alibraryz00(void);
	BGL_IMPORT obj_t BGl_definezd2primopz12zc0zz__evenvz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62getzd2alibraryzd2initsz62zzmodule_alibraryz00(obj_t);
	static obj_t BGl_z62alibraryzd2producerzb0zzmodule_alibraryz00(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_usezd2libraryz12zc0zzmodule_alibraryz00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t __cnst[6];


	   
		 
		DEFINE_STRING(BGl_string1245z00zzmodule_alibraryz00,
		BgL_bgl_string1245za700za7za7m1269za7, "]", 1);
	      DEFINE_STRING(BGl_string1246z00zzmodule_alibraryz00,
		BgL_bgl_string1246za700za7za7m1270za7, "      [reading ", 15);
	      DEFINE_STRING(BGl_string1247z00zzmodule_alibraryz00,
		BgL_bgl_string1247za700za7za7m1271za7, ".init", 5);
	      DEFINE_STRING(BGl_string1250z00zzmodule_alibraryz00,
		BgL_bgl_string1250za700za7za7m1272za7, "library", 7);
	      DEFINE_STRING(BGl_string1251z00zzmodule_alibraryz00,
		BgL_bgl_string1251za700za7za7m1273za7, "Illegal prototype", 17);
	      DEFINE_STRING(BGl_string1252z00zzmodule_alibraryz00,
		BgL_bgl_string1252za700za7za7m1274za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1253z00zzmodule_alibraryz00,
		BgL_bgl_string1253za700za7za7m1275za7, "Illegal `library' clause", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1248z00zzmodule_alibraryz00,
		BgL_bgl_za762za7c3za704anonymo1276za7,
		BGl_z62zc3z04anonymousza31160ze3ze5zzmodule_alibraryz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1255z00zzmodule_alibraryz00,
		BgL_bgl_string1255za700za7za7m1277za7, "~a()", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1249z00zzmodule_alibraryz00,
		BgL_bgl_za762za7c3za704anonymo1278za7,
		BGl_z62zc3z04anonymousza31161ze3ze5zzmodule_alibraryz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1256z00zzmodule_alibraryz00,
		BgL_bgl_string1256za700za7za7m1279za7, "-", 1);
	      DEFINE_STRING(BGl_string1257z00zzmodule_alibraryz00,
		BgL_bgl_string1257za700za7za7m1280za7, "Cannot find library init module",
		31);
	      DEFINE_STRING(BGl_string1258z00zzmodule_alibraryz00,
		BgL_bgl_string1258za700za7za7m1281za7, "module_alibrary", 15);
	      DEFINE_STRING(BGl_string1259z00zzmodule_alibraryz00,
		BgL_bgl_string1259za700za7za7m1282za7,
		"use-library! begin module-initialization pragma void library ", 61);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1254z00zzmodule_alibraryz00,
		BgL_bgl_za762za7c3za704anonymo1283za7,
		BGl_z62zc3z04anonymousza31185ze3ze5zzmodule_alibraryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2alibraryzd2initszd2envzd2zzmodule_alibraryz00,
		BgL_bgl_za762getza7d2alibrar1284z00,
		BGl_z62getzd2alibraryzd2initsz62zzmodule_alibraryz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_alibraryzd2producerzd2envz00zzmodule_alibraryz00,
		BgL_bgl_za762alibraryza7d2pr1285z00,
		BGl_z62alibraryzd2producerzb0zzmodule_alibraryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2alibraryzd2compilerzd2envzd2zzmodule_alibraryz00,
		BgL_bgl_za762makeza7d2alibra1286z00,
		BGl_z62makezd2alibraryzd2compilerz62zzmodule_alibraryz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_usezd2libraryz12zd2envz12zzmodule_alibraryz00,
		BgL_bgl_za762useza7d2library1287z00,
		BGl_z62usezd2libraryz12za2zzmodule_alibraryz00, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_loadzd2libraryzd2initzd2envzd2zzmodule_alibraryz00,
		BgL_bgl_za762loadza7d2librar1288z00,
		BGl_z62loadzd2libraryzd2initz62zzmodule_alibraryz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_alibraryz00));
		     ADD_ROOT((void *) (&BGl_za2libraryzd2initza2zd2zzmodule_alibraryz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(long
		BgL_checksumz00_954, char *BgL_fromz00_955)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_alibraryz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_alibraryz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_alibraryz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_alibraryz00();
					BGl_cnstzd2initzd2zzmodule_alibraryz00();
					BGl_importedzd2moduleszd2initz00zzmodule_alibraryz00();
					BGl_evalzd2initzd2zzmodule_alibraryz00();
					return BGl_toplevelzd2initzd2zzmodule_alibraryz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__libraryz00(0L, "module_alibrary");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "module_alibrary");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "module_alibrary");
			BGl_modulezd2initializa7ationz75zz__evenvz00(0L, "module_alibrary");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"module_alibrary");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "module_alibrary");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_alibrary");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			{	/* Module/alibrary.scm 15 */
				obj_t BgL_cportz00_900;

				{	/* Module/alibrary.scm 15 */
					obj_t BgL_stringz00_907;

					BgL_stringz00_907 = BGl_string1259z00zzmodule_alibraryz00;
					{	/* Module/alibrary.scm 15 */
						obj_t BgL_startz00_908;

						BgL_startz00_908 = BINT(0L);
						{	/* Module/alibrary.scm 15 */
							obj_t BgL_endz00_909;

							BgL_endz00_909 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_907)));
							{	/* Module/alibrary.scm 15 */

								BgL_cportz00_900 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_907, BgL_startz00_908, BgL_endz00_909);
				}}}}
				{
					long BgL_iz00_901;

					BgL_iz00_901 = 5L;
				BgL_loopz00_902:
					if ((BgL_iz00_901 == -1L))
						{	/* Module/alibrary.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/alibrary.scm 15 */
							{	/* Module/alibrary.scm 15 */
								obj_t BgL_arg1268z00_903;

								{	/* Module/alibrary.scm 15 */

									{	/* Module/alibrary.scm 15 */
										obj_t BgL_locationz00_905;

										BgL_locationz00_905 = BBOOL(((bool_t) 0));
										{	/* Module/alibrary.scm 15 */

											BgL_arg1268z00_903 =
												BGl_readz00zz__readerz00(BgL_cportz00_900,
												BgL_locationz00_905);
										}
									}
								}
								{	/* Module/alibrary.scm 15 */
									int BgL_tmpz00_992;

									BgL_tmpz00_992 = (int) (BgL_iz00_901);
									CNST_TABLE_SET(BgL_tmpz00_992, BgL_arg1268z00_903);
							}}
							{	/* Module/alibrary.scm 15 */
								int BgL_auxz00_906;

								BgL_auxz00_906 = (int) ((BgL_iz00_901 - 1L));
								{
									long BgL_iz00_997;

									BgL_iz00_997 = (long) (BgL_auxz00_906);
									BgL_iz00_901 = BgL_iz00_997;
									goto BgL_loopz00_902;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			return (BGl_za2libraryzd2initza2zd2zzmodule_alibraryz00 = BNIL, BUNSPEC);
		}

	}



/* load-library-init */
	BGL_EXPORTED_DEF obj_t BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 47 */
			{

				{	/* Module/alibrary.scm 48 */
					bool_t BgL_tmpz00_1000;

				BgL_zc3z04anonymousza31146ze3z87_659:
					if (NULLP(BGl_za2libraryzd2initza2zd2zzmodule_alibraryz00))
						{	/* Module/alibrary.scm 49 */
							BgL_tmpz00_1000 = ((bool_t) 0);
						}
					else
						{	/* Module/alibrary.scm 49 */
							{	/* Module/alibrary.scm 50 */
								obj_t BgL_lz00_661;

								BgL_lz00_661 = BGl_za2libraryzd2initza2zd2zzmodule_alibraryz00;
								BGl_za2libraryzd2initza2zd2zzmodule_alibraryz00 = BNIL;
								{
									obj_t BgL_l1092z00_663;

									BgL_l1092z00_663 = BgL_lz00_661;
								BgL_zc3z04anonymousza31148ze3z87_664:
									if (PAIRP(BgL_l1092z00_663))
										{	/* Module/alibrary.scm 52 */
											{	/* Module/alibrary.scm 53 */
												obj_t BgL_fnamez00_666;

												BgL_fnamez00_666 = CAR(BgL_l1092z00_663);
												{	/* Module/alibrary.scm 53 */
													obj_t BgL_list1150z00_667;

													{	/* Module/alibrary.scm 53 */
														obj_t BgL_arg1152z00_668;

														{	/* Module/alibrary.scm 53 */
															obj_t BgL_arg1153z00_669;

															{	/* Module/alibrary.scm 53 */
																obj_t BgL_arg1154z00_670;

																BgL_arg1154z00_670 =
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
																	BNIL);
																BgL_arg1153z00_669 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1245z00zzmodule_alibraryz00,
																	BgL_arg1154z00_670);
															}
															BgL_arg1152z00_668 =
																MAKE_YOUNG_PAIR(BgL_fnamez00_666,
																BgL_arg1153z00_669);
														}
														BgL_list1150z00_667 =
															MAKE_YOUNG_PAIR
															(BGl_string1246z00zzmodule_alibraryz00,
															BgL_arg1152z00_668);
													}
													BGl_verbosez00zztools_speekz00(BINT(2L),
														BgL_list1150z00_667);
												}
												{	/* Module/alibrary.scm 54 */
													obj_t BgL_envz00_672;

													BgL_envz00_672 =
														BGl_defaultzd2environmentzd2zz__evalz00();
													{	/* Module/alibrary.scm 54 */

														BGl_loadqz00zz__evalz00(BgL_fnamez00_666,
															BgL_envz00_672);
											}}}
											{
												obj_t BgL_l1092z00_1015;

												BgL_l1092z00_1015 = CDR(BgL_l1092z00_663);
												BgL_l1092z00_663 = BgL_l1092z00_1015;
												goto BgL_zc3z04anonymousza31148ze3z87_664;
											}
										}
									else
										{	/* Module/alibrary.scm 52 */
											((bool_t) 1);
										}
								}
							}
							goto BgL_zc3z04anonymousza31146ze3z87_659;
						}
					return BBOOL(BgL_tmpz00_1000);
				}
			}
		}

	}



/* &load-library-init */
	obj_t BGl_z62loadzd2libraryzd2initz62zzmodule_alibraryz00(obj_t
		BgL_envz00_879)
	{
		{	/* Module/alibrary.scm 47 */
			return BGl_loadzd2libraryzd2initz00zzmodule_alibraryz00();
		}

	}



/* register-library-init! */
	obj_t BGl_registerzd2libraryzd2initz12z12zzmodule_alibraryz00(obj_t
		BgL_libraryz00_75)
	{
		{	/* Module/alibrary.scm 61 */
			{	/* Module/alibrary.scm 62 */
				obj_t BgL_initzd2namezd2_676;

				{	/* Module/alibrary.scm 62 */
					obj_t BgL_arg1158z00_678;

					{	/* Module/alibrary.scm 62 */
						obj_t BgL_arg1455z00_792;

						BgL_arg1455z00_792 = SYMBOL_TO_STRING(BgL_libraryz00_75);
						BgL_arg1158z00_678 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_792);
					}
					BgL_initzd2namezd2_676 =
						string_append(BgL_arg1158z00_678,
						BGl_string1247z00zzmodule_alibraryz00);
				}
				{	/* Module/alibrary.scm 62 */
					obj_t BgL_fnamez00_677;

					BgL_fnamez00_677 =
						BGl_findzd2filezf2pathz20zz__osz00(BgL_initzd2namezd2_676,
						BGl_za2libzd2dirza2zd2zzengine_paramz00);
					{	/* Module/alibrary.scm 63 */

						if (CBOOL(BgL_fnamez00_677))
							{	/* Module/alibrary.scm 64 */
								return (BGl_za2libraryzd2initza2zd2zzmodule_alibraryz00 =
									MAKE_YOUNG_PAIR(BgL_fnamez00_677,
										BGl_za2libraryzd2initza2zd2zzmodule_alibraryz00), BUNSPEC);
							}
						else
							{	/* Module/alibrary.scm 64 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* use-library! */
	BGL_EXPORTED_DEF obj_t BGl_usezd2libraryz12zc0zzmodule_alibraryz00(obj_t
		BgL_libraryz00_76)
	{
		{	/* Module/alibrary.scm 70 */
			if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00(BgL_libraryz00_76,
						BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00)))
				{	/* Module/alibrary.scm 71 */
					return BFALSE;
				}
			else
				{	/* Module/alibrary.scm 71 */
					BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00 =
						MAKE_YOUNG_PAIR(BgL_libraryz00_76,
						BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00);
					{	/* Module/alibrary.scm 74 */
						obj_t BgL_heapzd2namezd2_680;

						{	/* Module/alibrary.scm 74 */
							obj_t BgL_arg1455z00_794;

							BgL_arg1455z00_794 = SYMBOL_TO_STRING(BgL_libraryz00_76);
							BgL_heapzd2namezd2_680 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_794);
						}
						BGl_registerzd2libraryzd2initz12z12zzmodule_alibraryz00
							(BgL_libraryz00_76);
						BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00 =
							MAKE_YOUNG_PAIR(BgL_heapzd2namezd2_680,
							BGl_za2additionalzd2heapzd2namesza2z00zzengine_paramz00);
						return BgL_libraryz00_76;
					}
				}
		}

	}



/* &use-library! */
	obj_t BGl_z62usezd2libraryz12za2zzmodule_alibraryz00(obj_t BgL_envz00_880,
		obj_t BgL_libraryz00_881)
	{
		{	/* Module/alibrary.scm 70 */
			return BGl_usezd2libraryz12zc0zzmodule_alibraryz00(BgL_libraryz00_881);
		}

	}



/* make-alibrary-compiler */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2alibraryzd2compilerz00zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 83 */
			{	/* Module/alibrary.scm 84 */
				BgL_ccompz00_bglt BgL_new1063z00_681;

				{	/* Module/alibrary.scm 85 */
					BgL_ccompz00_bglt BgL_new1062z00_686;

					BgL_new1062z00_686 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/alibrary.scm 85 */
						long BgL_arg1162z00_687;

						BgL_arg1162z00_687 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1062z00_686), BgL_arg1162z00_687);
					}
					BgL_new1063z00_681 = BgL_new1062z00_686;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1063z00_681))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1063z00_681))->BgL_producerz00) =
					((obj_t) BGl_alibraryzd2producerzd2envz00zzmodule_alibraryz00),
					BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1063z00_681))->BgL_consumerz00) =
					((obj_t) BGl_proc1248z00zzmodule_alibraryz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1063z00_681))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_proc1249z00zzmodule_alibraryz00), BUNSPEC);
				return ((obj_t) BgL_new1063z00_681);
			}
		}

	}



/* &make-alibrary-compiler */
	obj_t BGl_z62makezd2alibraryzd2compilerz62zzmodule_alibraryz00(obj_t
		BgL_envz00_884)
	{
		{	/* Module/alibrary.scm 83 */
			return BGl_makezd2alibraryzd2compilerz00zzmodule_alibraryz00();
		}

	}



/* &<@anonymous:1161> */
	obj_t BGl_z62zc3z04anonymousza31161ze3ze5zzmodule_alibraryz00(obj_t
		BgL_envz00_885)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(1);
		}

	}



/* &<@anonymous:1160> */
	obj_t BGl_z62zc3z04anonymousza31160ze3ze5zzmodule_alibraryz00(obj_t
		BgL_envz00_886, obj_t BgL_mz00_887, obj_t BgL_cz00_888)
	{
		{	/* Module/module.scm 54 */
			return BNIL;
		}

	}



/* &alibrary-producer */
	obj_t BGl_z62alibraryzd2producerzb0zzmodule_alibraryz00(obj_t BgL_envz00_889,
		obj_t BgL_clausez00_890)
	{
		{	/* Module/alibrary.scm 91 */
			{
				obj_t BgL_protosz00_913;

				if (PAIRP(BgL_clausez00_890))
					{	/* Module/alibrary.scm 92 */
						obj_t BgL_arg1164z00_918;

						BgL_arg1164z00_918 = CDR(((obj_t) BgL_clausez00_890));
						BgL_protosz00_913 = BgL_arg1164z00_918;
						{
							obj_t BgL_l1094z00_915;

							BgL_l1094z00_915 = BgL_protosz00_913;
						BgL_zc3z04anonymousza31165ze3z87_914:
							if (PAIRP(BgL_l1094z00_915))
								{	/* Module/alibrary.scm 94 */
									{	/* Module/alibrary.scm 95 */
										obj_t BgL_xz00_916;

										BgL_xz00_916 = CAR(BgL_l1094z00_915);
										if (SYMBOLP(BgL_xz00_916))
											{	/* Module/alibrary.scm 95 */
												BGl_usezd2libraryz12zc0zzmodule_alibraryz00
													(BgL_xz00_916);
											}
										else
											{	/* Module/alibrary.scm 95 */
												BGl_errorz00zz__errorz00
													(BGl_string1250z00zzmodule_alibraryz00,
													BGl_string1251z00zzmodule_alibraryz00, BgL_xz00_916);
											}
									}
									{
										obj_t BgL_l1094z00_1058;

										BgL_l1094z00_1058 = CDR(BgL_l1094z00_915);
										BgL_l1094z00_915 = BgL_l1094z00_1058;
										goto BgL_zc3z04anonymousza31165ze3z87_914;
									}
								}
							else
								{	/* Module/alibrary.scm 94 */
									((bool_t) 1);
								}
						}
						return BNIL;
					}
				else
					{	/* Module/alibrary.scm 92 */
						{	/* Module/alibrary.scm 101 */
							obj_t BgL_list1172z00_917;

							BgL_list1172z00_917 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							return
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string1252z00zzmodule_alibraryz00,
								BGl_string1253z00zzmodule_alibraryz00, BgL_clausez00_890,
								BgL_list1172z00_917);
						}
					}
			}
		}

	}



/* get-alibrary-inits */
	BGL_EXPORTED_DEF obj_t BGl_getzd2alibraryzd2initsz00zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 106 */
			{	/* Module/alibrary.scm 107 */
				obj_t BgL_arg1183z00_704;

				{	/* Module/alibrary.scm 127 */
					obj_t BgL_g1945z00_738;

					BgL_g1945z00_738 =
						BGl_za2additionalzd2bigloozd2librariesza2z00zzengine_paramz00;
					{	/* Module/alibrary.scm 127 */

						BgL_arg1183z00_704 =
							BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00
							(BgL_g1945z00_738,
							BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
				}}
				{	/* Module/alibrary.scm 107 */
					obj_t BgL_list1184z00_705;

					BgL_list1184z00_705 = MAKE_YOUNG_PAIR(BgL_arg1183z00_704, BNIL);
					return
						BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
						(BGl_proc1254z00zzmodule_alibraryz00, BgL_list1184z00_705);
				}
			}
		}

	}



/* &get-alibrary-inits */
	obj_t BGl_z62getzd2alibraryzd2initsz62zzmodule_alibraryz00(obj_t
		BgL_envz00_892)
	{
		{	/* Module/alibrary.scm 106 */
			return BGl_getzd2alibraryzd2initsz00zzmodule_alibraryz00();
		}

	}



/* &<@anonymous:1185> */
	obj_t BGl_z62zc3z04anonymousza31185ze3ze5zzmodule_alibraryz00(obj_t
		BgL_envz00_893, obj_t BgL_libz00_894)
	{
		{	/* Module/alibrary.scm 107 */
			{	/* Module/alibrary.scm 108 */
				obj_t BgL_infoz00_919;

				BgL_infoz00_919 = BGl_libraryzd2infozd2zz__libraryz00(BgL_libz00_894);
				if (CBOOL(BgL_infoz00_919))
					{	/* Module/alibrary.scm 111 */
						obj_t BgL_arg1187z00_920;

						{	/* Module/alibrary.scm 111 */
							obj_t BgL_arg1188z00_921;
							obj_t BgL_arg1189z00_922;

							{	/* Module/alibrary.scm 111 */
								bool_t BgL_test1307z00_1069;

								if (CBOOL(STRUCT_REF(((obj_t) BgL_infoz00_919), (int) (3L))))
									{	/* Module/alibrary.scm 112 */
										obj_t BgL_arg1198z00_923;

										BgL_arg1198z00_923 =
											BGl_thezd2backendzd2zzbackend_backendz00();
										BgL_test1307z00_1069 =
											(((BgL_backendz00_bglt) COBJECT(
													((BgL_backendz00_bglt) BgL_arg1198z00_923)))->
											BgL_pragmazd2supportzd2);
									}
								else
									{	/* Module/alibrary.scm 111 */
										BgL_test1307z00_1069 = ((bool_t) 0);
									}
								if (BgL_test1307z00_1069)
									{	/* Module/alibrary.scm 114 */
										obj_t BgL_arg1193z00_924;

										{	/* Module/alibrary.scm 114 */
											obj_t BgL_arg1194z00_925;

											{	/* Module/alibrary.scm 114 */
												obj_t BgL_arg1196z00_926;

												BgL_arg1196z00_926 =
													STRUCT_REF(((obj_t) BgL_infoz00_919), (int) (3L));
												{	/* Module/alibrary.scm 113 */
													obj_t BgL_list1197z00_927;

													BgL_list1197z00_927 =
														MAKE_YOUNG_PAIR(BgL_arg1196z00_926, BNIL);
													BgL_arg1194z00_925 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string1255z00zzmodule_alibraryz00,
														BgL_list1197z00_927);
											}}
											BgL_arg1193z00_924 =
												MAKE_YOUNG_PAIR(BgL_arg1194z00_925, BNIL);
										}
										BgL_arg1188z00_921 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1193z00_924);
									}
								else
									{	/* Module/alibrary.scm 111 */
										BgL_arg1188z00_921 = BFALSE;
									}
							}
							{	/* Module/alibrary.scm 115 */
								obj_t BgL_arg1199z00_928;
								obj_t BgL_arg1200z00_929;

								if (CBOOL(STRUCT_REF(((obj_t) BgL_infoz00_919), (int) (5L))))
									{	/* Module/alibrary.scm 116 */
										obj_t BgL_vz00_930;

										{	/* Module/alibrary.scm 117 */
											obj_t BgL_arg1215z00_931;

											BgL_arg1215z00_931 =
												STRUCT_REF(((obj_t) BgL_infoz00_919), (int) (5L));
											{	/* Module/alibrary.scm 116 */
												obj_t BgL_list1216z00_932;

												BgL_list1216z00_932 =
													MAKE_YOUNG_PAIR(BgL_arg1215z00_931, BNIL);
												BgL_vz00_930 =
													BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(3),
													BgL_list1216z00_932);
										}}
										{	/* Module/alibrary.scm 118 */
											bool_t BgL_test1311z00_1097;

											{	/* Module/alibrary.scm 118 */
												obj_t BgL_classz00_933;

												BgL_classz00_933 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_vz00_930))
													{	/* Module/alibrary.scm 118 */
														BgL_objectz00_bglt BgL_arg1807z00_934;

														BgL_arg1807z00_934 =
															(BgL_objectz00_bglt) (BgL_vz00_930);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Module/alibrary.scm 118 */
																long BgL_idxz00_935;

																BgL_idxz00_935 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_934);
																BgL_test1311z00_1097 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_935 + 2L)) == BgL_classz00_933);
															}
														else
															{	/* Module/alibrary.scm 118 */
																bool_t BgL_res1241z00_938;

																{	/* Module/alibrary.scm 118 */
																	obj_t BgL_oclassz00_939;

																	{	/* Module/alibrary.scm 118 */
																		obj_t BgL_arg1815z00_940;
																		long BgL_arg1816z00_941;

																		BgL_arg1815z00_940 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Module/alibrary.scm 118 */
																			long BgL_arg1817z00_942;

																			BgL_arg1817z00_942 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_934);
																			BgL_arg1816z00_941 =
																				(BgL_arg1817z00_942 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_939 =
																			VECTOR_REF(BgL_arg1815z00_940,
																			BgL_arg1816z00_941);
																	}
																	{	/* Module/alibrary.scm 118 */
																		bool_t BgL__ortest_1115z00_943;

																		BgL__ortest_1115z00_943 =
																			(BgL_classz00_933 == BgL_oclassz00_939);
																		if (BgL__ortest_1115z00_943)
																			{	/* Module/alibrary.scm 118 */
																				BgL_res1241z00_938 =
																					BgL__ortest_1115z00_943;
																			}
																		else
																			{	/* Module/alibrary.scm 118 */
																				long BgL_odepthz00_944;

																				{	/* Module/alibrary.scm 118 */
																					obj_t BgL_arg1804z00_945;

																					BgL_arg1804z00_945 =
																						(BgL_oclassz00_939);
																					BgL_odepthz00_944 =
																						BGL_CLASS_DEPTH(BgL_arg1804z00_945);
																				}
																				if ((2L < BgL_odepthz00_944))
																					{	/* Module/alibrary.scm 118 */
																						obj_t BgL_arg1802z00_946;

																						{	/* Module/alibrary.scm 118 */
																							obj_t BgL_arg1803z00_947;

																							BgL_arg1803z00_947 =
																								(BgL_oclassz00_939);
																							BgL_arg1802z00_946 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_947, 2L);
																						}
																						BgL_res1241z00_938 =
																							(BgL_arg1802z00_946 ==
																							BgL_classz00_933);
																					}
																				else
																					{	/* Module/alibrary.scm 118 */
																						BgL_res1241z00_938 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1311z00_1097 = BgL_res1241z00_938;
															}
													}
												else
													{	/* Module/alibrary.scm 118 */
														BgL_test1311z00_1097 = ((bool_t) 0);
													}
											}
											if (BgL_test1311z00_1097)
												{	/* Module/alibrary.scm 119 */
													obj_t BgL_fz00_948;

													{	/* Module/alibrary.scm 119 */
														bool_t BgL_test1316z00_1120;

														{	/* Module/alibrary.scm 119 */
															obj_t BgL_tmpz00_1121;

															BgL_tmpz00_1121 =
																CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
															BgL_test1316z00_1120 = STRINGP(BgL_tmpz00_1121);
														}
														if (BgL_test1316z00_1120)
															{	/* Module/alibrary.scm 119 */
																BgL_fz00_948 =
																	CAR
																	(BGl_za2srczd2filesza2zd2zzengine_paramz00);
															}
														else
															{	/* Module/alibrary.scm 119 */
																BgL_fz00_948 =
																	BGl_string1256z00zzmodule_alibraryz00;
															}
													}
													{	/* Module/alibrary.scm 122 */
														obj_t BgL_arg1203z00_949;

														{	/* Module/alibrary.scm 122 */
															obj_t BgL_arg1206z00_950;

															BgL_arg1206z00_950 =
																MAKE_YOUNG_PAIR(BgL_fz00_948, BNIL);
															BgL_arg1203z00_949 =
																MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1206z00_950);
														}
														BgL_arg1199z00_928 =
															MAKE_YOUNG_PAIR(BgL_vz00_930, BgL_arg1203z00_949);
													}
												}
											else
												{	/* Module/alibrary.scm 124 */
													obj_t BgL_arg1212z00_951;

													BgL_arg1212z00_951 =
														STRUCT_REF(((obj_t) BgL_infoz00_919), (int) (5L));
													BgL_arg1199z00_928 =
														BGl_errorz00zz__errorz00
														(BGl_string1250z00zzmodule_alibraryz00,
														BGl_string1257z00zzmodule_alibraryz00,
														BgL_arg1212z00_951);
									}}}
								else
									{	/* Module/alibrary.scm 115 */
										BgL_arg1199z00_928 = BFALSE;
									}
								{	/* Module/alibrary.scm 125 */
									obj_t BgL_arg1218z00_952;

									if (CBOOL(STRUCT_REF(((obj_t) BgL_infoz00_919), (int) (9L))))
										{	/* Module/alibrary.scm 126 */
											obj_t BgL_arg1220z00_953;

											BgL_arg1220z00_953 =
												STRUCT_REF(((obj_t) BgL_infoz00_919), (int) (9L));
											BgL_arg1218z00_952 =
												MAKE_YOUNG_PAIR(BgL_arg1220z00_953, BNIL);
										}
									else
										{	/* Module/alibrary.scm 125 */
											BgL_arg1218z00_952 = BFALSE;
										}
									BgL_arg1200z00_929 =
										MAKE_YOUNG_PAIR(BgL_arg1218z00_952, BNIL);
								}
								BgL_arg1189z00_922 =
									MAKE_YOUNG_PAIR(BgL_arg1199z00_928, BgL_arg1200z00_929);
							}
							BgL_arg1187z00_920 =
								MAKE_YOUNG_PAIR(BgL_arg1188z00_921, BgL_arg1189z00_922);
						}
						return MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1187z00_920);
					}
				else
					{	/* Module/alibrary.scm 109 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1258z00zzmodule_alibraryz00));
		}

	}



/* eval-init */
	obj_t BGl_evalzd2initzd2zzmodule_alibraryz00(void)
	{
		{	/* Module/alibrary.scm 15 */
			BGl_definezd2primopz12zc0zz__evenvz00(CNST_TABLE_REF(5),
				BGl_usezd2libraryz12zd2envz12zzmodule_alibraryz00);
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
