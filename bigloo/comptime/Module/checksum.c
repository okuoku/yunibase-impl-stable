/*===========================================================================*/
/*   (Module/checksum.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/checksum.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_CHECKSUM_TYPE_DEFINITIONS
#define BGL_MODULE_CHECKSUM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_CHECKSUM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00(obj_t);
	static long BGl_symbolzd2ze3numberz31zzmodule_checksumz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_checksumz00 = BUNSPEC;
	static obj_t BGl_externzd2clausezd2checksumz00zzmodule_checksumz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_atomzd2ze3numberz31zzmodule_checksumz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31027ze3ze5zzmodule_checksumz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_checksumz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL long BGl_modulezd2checksumzd2zzmodule_checksumz00(obj_t,
		obj_t);
	static obj_t BGl_z62modulezd2checksumzb0zzmodule_checksumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_checksumz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_checksumz00(void);
	static obj_t BGl_listzd2ze3numberz31zzmodule_checksumz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_checksumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT bool_t fexists(char *);
	static obj_t BGl_clausezd2checksumzd2zzmodule_checksumz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzmodule_checksumz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_checksumz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_checksumz00(void);
	static obj_t BGl_includezd2checksumzd2zzmodule_checksumz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2loadzd2pathza2zd2zz__evalz00;
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	BGL_IMPORT long bgl_last_modification_time(char *);
	BGL_IMPORT obj_t bgl_find_runtime_type(obj_t);
	BGL_IMPORT obj_t bgl_real_to_string(double);
	BGL_IMPORT long BGl_getzd2hashnumberzd2zz__hashz00(obj_t);
	static obj_t __cnst[10];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2checksumzd2envz00zzmodule_checksumz00,
		BgL_bgl_za762moduleza7d2chec1222z00,
		BGl_z62modulezd2checksumzb0zzmodule_checksumz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1217z00zzmodule_checksumz00,
		BgL_bgl_string1217za700za7za7m1223za7, "Can't find include file", 23);
	      DEFINE_STRING(BGl_string1218z00zzmodule_checksumz00,
		BgL_bgl_string1218za700za7za7m1224za7, " -- ", 4);
	      DEFINE_STRING(BGl_string1219z00zzmodule_checksumz00,
		BgL_bgl_string1219za700za7za7m1225za7, "module checksum:Unknown clause",
		30);
	      DEFINE_STRING(BGl_string1220z00zzmodule_checksumz00,
		BgL_bgl_string1220za700za7za7m1226za7,
		"info assert default include java extern foreign export include-checksum directives ",
		83);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_checksumz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzmodule_checksumz00(long
		BgL_checksumz00_411, char *BgL_fromz00_412)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_checksumz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_checksumz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_checksumz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_checksumz00();
					BGl_cnstzd2initzd2zzmodule_checksumz00();
					return BGl_methodzd2initzd2zzmodule_checksumz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_checksumz00(void)
	{
		{	/* Module/checksum.scm 18 */
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "module_checksum");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonum_dtoaz00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_checksum");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "module_checksum");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_checksum");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_checksumz00(void)
	{
		{	/* Module/checksum.scm 18 */
			{	/* Module/checksum.scm 18 */
				obj_t BgL_cportz00_400;

				{	/* Module/checksum.scm 18 */
					obj_t BgL_stringz00_407;

					BgL_stringz00_407 = BGl_string1220z00zzmodule_checksumz00;
					{	/* Module/checksum.scm 18 */
						obj_t BgL_startz00_408;

						BgL_startz00_408 = BINT(0L);
						{	/* Module/checksum.scm 18 */
							obj_t BgL_endz00_409;

							BgL_endz00_409 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_407)));
							{	/* Module/checksum.scm 18 */

								BgL_cportz00_400 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_407, BgL_startz00_408, BgL_endz00_409);
				}}}}
				{
					long BgL_iz00_401;

					BgL_iz00_401 = 9L;
				BgL_loopz00_402:
					if ((BgL_iz00_401 == -1L))
						{	/* Module/checksum.scm 18 */
							return BUNSPEC;
						}
					else
						{	/* Module/checksum.scm 18 */
							{	/* Module/checksum.scm 18 */
								obj_t BgL_arg1221z00_403;

								{	/* Module/checksum.scm 18 */

									{	/* Module/checksum.scm 18 */
										obj_t BgL_locationz00_405;

										BgL_locationz00_405 = BBOOL(((bool_t) 0));
										{	/* Module/checksum.scm 18 */

											BgL_arg1221z00_403 =
												BGl_readz00zz__readerz00(BgL_cportz00_400,
												BgL_locationz00_405);
										}
									}
								}
								{	/* Module/checksum.scm 18 */
									int BgL_tmpz00_448;

									BgL_tmpz00_448 = (int) (BgL_iz00_401);
									CNST_TABLE_SET(BgL_tmpz00_448, BgL_arg1221z00_403);
							}}
							{	/* Module/checksum.scm 18 */
								int BgL_auxz00_406;

								BgL_auxz00_406 = (int) ((BgL_iz00_401 - 1L));
								{
									long BgL_iz00_453;

									BgL_iz00_453 = (long) (BgL_auxz00_406);
									BgL_iz00_401 = BgL_iz00_453;
									goto BgL_loopz00_402;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_checksumz00(void)
	{
		{	/* Module/checksum.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* module-checksum */
	BGL_EXPORTED_DEF long BGl_modulezd2checksumzd2zzmodule_checksumz00(obj_t
		BgL_modulez00_3, obj_t BgL_includezd2pathzd2_4)
	{
		{	/* Module/checksum.scm 24 */
			{	/* Module/checksum.scm 25 */
				long BgL_arg1019z00_237;
				obj_t BgL_arg1020z00_238;

				BgL_arg1019z00_237 =
					BGl_symbolzd2ze3numberz31zzmodule_checksumz00(CAR(CDR
						(BgL_modulez00_3)));
				BgL_arg1020z00_238 = CDR(CDR(BgL_modulez00_3));
				return
					(long)
					CINT(BGl_clausezd2checksumzd2zzmodule_checksumz00(BINT
						(BgL_arg1019z00_237), BgL_arg1020z00_238, BgL_includezd2pathzd2_4));
		}}

	}



/* &module-checksum */
	obj_t BGl_z62modulezd2checksumzb0zzmodule_checksumz00(obj_t BgL_envz00_394,
		obj_t BgL_modulez00_395, obj_t BgL_includezd2pathzd2_396)
	{
		{	/* Module/checksum.scm 24 */
			return
				BINT(BGl_modulezd2checksumzd2zzmodule_checksumz00(BgL_modulez00_395,
					BgL_includezd2pathzd2_396));
		}

	}



/* include-checksum */
	obj_t BGl_includezd2checksumzd2zzmodule_checksumz00(obj_t BgL_includez00_5,
		obj_t BgL_checksumz00_6, obj_t BgL_includezd2pathzd2_7)
	{
		{	/* Module/checksum.scm 30 */
			{
				obj_t BgL_expz00_54;

				{	/* Module/checksum.scm 37 */
					obj_t BgL_fiz00_34;

					BgL_fiz00_34 =
						BGl_findzd2filezf2pathz20zz__osz00(BgL_includez00_5,
						BGl_za2loadzd2pathza2zd2zz__evalz00);
					{	/* Module/checksum.scm 38 */
						bool_t BgL_test1229z00_467;

						if (STRINGP(BgL_fiz00_34))
							{	/* Module/checksum.scm 38 */
								BgL_test1229z00_467 = fexists(BSTRING_TO_STRING(BgL_fiz00_34));
							}
						else
							{	/* Module/checksum.scm 38 */
								BgL_test1229z00_467 = ((bool_t) 0);
							}
						if (BgL_test1229z00_467)
							{	/* Module/checksum.scm 39 */
								obj_t BgL_portz00_37;

								{	/* Module/checksum.scm 39 */

									BgL_portz00_37 =
										BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_fiz00_34, BTRUE, BINT(5000000L));
								}
								{	/* Module/checksum.scm 39 */
									obj_t BgL_dirz00_38;

									{	/* Module/checksum.scm 40 */
										obj_t BgL_exitd1012z00_41;

										BgL_exitd1012z00_41 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Module/checksum.scm 44 */
											obj_t BgL_zc3z04anonymousza31027ze3z87_397;

											BgL_zc3z04anonymousza31027ze3z87_397 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31027ze3ze5zzmodule_checksumz00,
												(int) (0L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31027ze3z87_397,
												(int) (0L), BgL_portz00_37);
											{	/* Module/checksum.scm 40 */
												obj_t BgL_arg1828z00_251;

												{	/* Module/checksum.scm 40 */
													obj_t BgL_arg1829z00_252;

													BgL_arg1829z00_252 =
														BGL_EXITD_PROTECT(BgL_exitd1012z00_41);
													BgL_arg1828z00_251 =
														MAKE_YOUNG_PAIR
														(BgL_zc3z04anonymousza31027ze3z87_397,
														BgL_arg1829z00_252);
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1012z00_41,
													BgL_arg1828z00_251);
												BUNSPEC;
											}
											{	/* Module/checksum.scm 40 */
												obj_t BgL_tmp1014z00_43;

												{	/* Module/checksum.scm 40 */
													obj_t BgL_expz00_44;

													{	/* Module/checksum.scm 40 */

														{	/* Module/checksum.scm 40 */

															BgL_expz00_44 =
																BGl_readz00zz__readerz00(
																((obj_t) BgL_portz00_37), BFALSE);
													}}
													{	/* Module/checksum.scm 41 */
														bool_t BgL_test1231z00_485;

														BgL_expz00_54 = BgL_expz00_44;
														if (PAIRP(BgL_expz00_54))
															{	/* Module/checksum.scm 36 */
																BgL_test1231z00_485 =
																	(CAR(BgL_expz00_54) == CNST_TABLE_REF(0));
															}
														else
															{	/* Module/checksum.scm 36 */
																BgL_test1231z00_485 = ((bool_t) 0);
															}
														if (BgL_test1231z00_485)
															{	/* Module/checksum.scm 41 */
																BgL_tmp1014z00_43 = BgL_expz00_44;
															}
														else
															{	/* Module/checksum.scm 41 */
																BgL_tmp1014z00_43 = BFALSE;
															}
													}
												}
												{	/* Module/checksum.scm 40 */
													bool_t BgL_test1233z00_491;

													{	/* Module/checksum.scm 40 */
														obj_t BgL_arg1827z00_254;

														BgL_arg1827z00_254 =
															BGL_EXITD_PROTECT(BgL_exitd1012z00_41);
														BgL_test1233z00_491 = PAIRP(BgL_arg1827z00_254);
													}
													if (BgL_test1233z00_491)
														{	/* Module/checksum.scm 40 */
															obj_t BgL_arg1825z00_255;

															{	/* Module/checksum.scm 40 */
																obj_t BgL_arg1826z00_256;

																BgL_arg1826z00_256 =
																	BGL_EXITD_PROTECT(BgL_exitd1012z00_41);
																BgL_arg1825z00_255 =
																	CDR(((obj_t) BgL_arg1826z00_256));
															}
															BGL_EXITD_PROTECT_SET(BgL_exitd1012z00_41,
																BgL_arg1825z00_255);
															BUNSPEC;
														}
													else
														{	/* Module/checksum.scm 40 */
															BFALSE;
														}
												}
												bgl_close_input_port(((obj_t) BgL_portz00_37));
												BgL_dirz00_38 = BgL_tmp1014z00_43;
											}
										}
									}
									{	/* Module/checksum.scm 40 */

										if (CBOOL(BgL_dirz00_38))
											{	/* Module/checksum.scm 46 */
												obj_t BgL_arg1024z00_39;
												obj_t BgL_arg1025z00_40;

												BgL_arg1024z00_39 =
													BGl_atomzd2ze3numberz31zzmodule_checksumz00
													(BgL_checksumz00_6, BgL_includez00_5);
												BgL_arg1025z00_40 = CDR(((obj_t) BgL_dirz00_38));
												BGL_TAIL return
													BGl_clausezd2checksumzd2zzmodule_checksumz00
													(BgL_arg1024z00_39, BgL_arg1025z00_40,
													BgL_includezd2pathzd2_7);
											}
										else
											{	/* Module/checksum.scm 45 */
												return BgL_checksumz00_6;
											}
									}
								}
							}
						else
							{	/* Module/checksum.scm 38 */
								return
									BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
									BGl_string1217z00zzmodule_checksumz00, BgL_includez00_5);
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1027> */
	obj_t BGl_z62zc3z04anonymousza31027ze3ze5zzmodule_checksumz00(obj_t
		BgL_envz00_398)
	{
		{	/* Module/checksum.scm 40 */
			{	/* Module/checksum.scm 44 */
				obj_t BgL_portz00_399;

				BgL_portz00_399 = PROCEDURE_REF(BgL_envz00_398, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_portz00_399));
			}
		}

	}



/* clause-checksum */
	obj_t BGl_clausezd2checksumzd2zzmodule_checksumz00(obj_t BgL_checksumz00_8,
		obj_t BgL_clausesz00_9, obj_t BgL_includezd2pathzd2_10)
	{
		{	/* Module/checksum.scm 55 */
			{
				obj_t BgL_clausesz00_65;
				obj_t BgL_checksumz00_66;

				BgL_clausesz00_65 = BgL_clausesz00_9;
				BgL_checksumz00_66 = BgL_checksumz00_8;
			BgL_zc3z04anonymousza31034ze3z87_67:
				{
					obj_t BgL_filesz00_75;
					obj_t BgL_restz00_76;

					if (NULLP(BgL_clausesz00_65))
						{	/* Module/checksum.scm 56 */
							return BgL_checksumz00_66;
						}
					else
						{	/* Module/checksum.scm 56 */
							if (PAIRP(BgL_clausesz00_65))
								{	/* Module/checksum.scm 56 */
									obj_t BgL_carzd2120zd2_82;

									BgL_carzd2120zd2_82 = CAR(((obj_t) BgL_clausesz00_65));
									if (PAIRP(BgL_carzd2120zd2_82))
										{	/* Module/checksum.scm 56 */
											if ((CAR(BgL_carzd2120zd2_82) == CNST_TABLE_REF(2)))
												{	/* Module/checksum.scm 56 */
													obj_t BgL_arg1040z00_86;
													obj_t BgL_arg1041z00_87;

													BgL_arg1040z00_86 = CDR(BgL_carzd2120zd2_82);
													BgL_arg1041z00_87 = CDR(((obj_t) BgL_clausesz00_65));
													{
														obj_t BgL_checksumz00_528;
														obj_t BgL_clausesz00_527;

														BgL_clausesz00_527 = BgL_arg1041z00_87;
														BgL_checksumz00_528 =
															BGl_listzd2ze3numberz31zzmodule_checksumz00
															(BgL_checksumz00_66, BgL_arg1040z00_86);
														BgL_checksumz00_66 = BgL_checksumz00_528;
														BgL_clausesz00_65 = BgL_clausesz00_527;
														goto BgL_zc3z04anonymousza31034ze3z87_67;
													}
												}
											else
												{	/* Module/checksum.scm 56 */
													obj_t BgL_cdrzd2133zd2_89;

													BgL_cdrzd2133zd2_89 =
														CDR(((obj_t) BgL_clausesz00_65));
													{	/* Module/checksum.scm 56 */
														obj_t BgL_carzd2135zd2_90;
														obj_t BgL_cdrzd2136zd2_91;

														BgL_carzd2135zd2_90 =
															CAR(((obj_t) BgL_carzd2120zd2_82));
														BgL_cdrzd2136zd2_91 =
															CDR(((obj_t) BgL_carzd2120zd2_82));
														if ((BgL_carzd2135zd2_90 == CNST_TABLE_REF(3)))
															{
																obj_t BgL_checksumz00_540;
																obj_t BgL_clausesz00_539;

																BgL_clausesz00_539 = BgL_cdrzd2133zd2_89;
																BgL_checksumz00_540 =
																	BGl_externzd2clausezd2checksumz00zzmodule_checksumz00
																	(BgL_checksumz00_66, BgL_cdrzd2136zd2_91,
																	BgL_includezd2pathzd2_10);
																BgL_checksumz00_66 = BgL_checksumz00_540;
																BgL_clausesz00_65 = BgL_clausesz00_539;
																goto BgL_zc3z04anonymousza31034ze3z87_67;
															}
														else
															{	/* Module/checksum.scm 56 */
																if ((BgL_carzd2135zd2_90 == CNST_TABLE_REF(4)))
																	{
																		obj_t BgL_checksumz00_546;
																		obj_t BgL_clausesz00_545;

																		BgL_clausesz00_545 = BgL_cdrzd2133zd2_89;
																		BgL_checksumz00_546 =
																			BGl_externzd2clausezd2checksumz00zzmodule_checksumz00
																			(BgL_checksumz00_66, BgL_cdrzd2136zd2_91,
																			BgL_includezd2pathzd2_10);
																		BgL_checksumz00_66 = BgL_checksumz00_546;
																		BgL_clausesz00_65 = BgL_clausesz00_545;
																		goto BgL_zc3z04anonymousza31034ze3z87_67;
																	}
																else
																	{	/* Module/checksum.scm 56 */
																		if (
																			(BgL_carzd2135zd2_90 ==
																				CNST_TABLE_REF(5)))
																			{
																				obj_t BgL_checksumz00_552;
																				obj_t BgL_clausesz00_551;

																				BgL_clausesz00_551 =
																					BgL_cdrzd2133zd2_89;
																				BgL_checksumz00_552 =
																					BGl_externzd2clausezd2checksumz00zzmodule_checksumz00
																					(BgL_checksumz00_66,
																					BgL_cdrzd2136zd2_91,
																					BgL_includezd2pathzd2_10);
																				BgL_checksumz00_66 =
																					BgL_checksumz00_552;
																				BgL_clausesz00_65 = BgL_clausesz00_551;
																				goto
																					BgL_zc3z04anonymousza31034ze3z87_67;
																			}
																		else
																			{	/* Module/checksum.scm 56 */
																				obj_t BgL_carzd2145zd2_92;

																				BgL_carzd2145zd2_92 =
																					CAR(((obj_t) BgL_clausesz00_65));
																				if (
																					(CAR(
																							((obj_t) BgL_carzd2145zd2_92)) ==
																						CNST_TABLE_REF(6)))
																					{	/* Module/checksum.scm 56 */
																						obj_t BgL_arg1044z00_95;
																						obj_t BgL_arg1045z00_96;

																						BgL_arg1044z00_95 =
																							CDR(
																							((obj_t) BgL_carzd2145zd2_92));
																						BgL_arg1045z00_96 =
																							CDR(((obj_t) BgL_clausesz00_65));
																						BgL_filesz00_75 = BgL_arg1044z00_95;
																						BgL_restz00_76 = BgL_arg1045z00_96;
																						{
																							obj_t BgL_filesz00_102;
																							obj_t BgL_checksumz00_103;

																							BgL_filesz00_102 =
																								BgL_filesz00_75;
																							BgL_checksumz00_103 =
																								BgL_checksumz00_66;
																						BgL_zc3z04anonymousza31050ze3z87_104:
																							if (NULLP
																								(BgL_filesz00_102))
																								{
																									obj_t BgL_checksumz00_568;
																									obj_t BgL_clausesz00_567;

																									BgL_clausesz00_567 =
																										BgL_restz00_76;
																									BgL_checksumz00_568 =
																										BgL_checksumz00_103;
																									BgL_checksumz00_66 =
																										BgL_checksumz00_568;
																									BgL_clausesz00_65 =
																										BgL_clausesz00_567;
																									goto
																										BgL_zc3z04anonymousza31034ze3z87_67;
																								}
																							else
																								{	/* Module/checksum.scm 70 */
																									obj_t BgL_arg1052z00_106;
																									obj_t BgL_arg1053z00_107;

																									BgL_arg1052z00_106 =
																										CDR(
																										((obj_t) BgL_filesz00_102));
																									{	/* Module/checksum.scm 70 */
																										obj_t BgL_arg1054z00_108;

																										BgL_arg1054z00_108 =
																											CAR(
																											((obj_t)
																												BgL_filesz00_102));
																										BgL_arg1053z00_107 =
																											BGl_includezd2checksumzd2zzmodule_checksumz00
																											(BgL_arg1054z00_108,
																											BgL_checksumz00_103,
																											BgL_includezd2pathzd2_10);
																									}
																									{
																										obj_t BgL_checksumz00_575;
																										obj_t BgL_filesz00_574;

																										BgL_filesz00_574 =
																											BgL_arg1052z00_106;
																										BgL_checksumz00_575 =
																											BgL_arg1053z00_107;
																										BgL_checksumz00_103 =
																											BgL_checksumz00_575;
																										BgL_filesz00_102 =
																											BgL_filesz00_574;
																										goto
																											BgL_zc3z04anonymousza31050ze3z87_104;
																									}
																								}
																						}
																					}
																				else
																					{	/* Module/checksum.scm 56 */
																					BgL_tagzd2110zd2_78:
																						{	/* Module/checksum.scm 74 */
																							obj_t BgL_arg1055z00_110;

																							BgL_arg1055z00_110 =
																								CDR(
																								((obj_t) BgL_clausesz00_65));
																							{
																								obj_t BgL_clausesz00_578;

																								BgL_clausesz00_578 =
																									BgL_arg1055z00_110;
																								BgL_clausesz00_65 =
																									BgL_clausesz00_578;
																								goto
																									BgL_zc3z04anonymousza31034ze3z87_67;
																							}
																						}
																					}
																			}
																	}
															}
													}
												}
										}
									else
										{	/* Module/checksum.scm 56 */
											goto BgL_tagzd2110zd2_78;
										}
								}
							else
								{	/* Module/checksum.scm 56 */
									goto BgL_tagzd2110zd2_78;
								}
						}
				}
			}
		}

	}



/* symbol->number */
	long BGl_symbolzd2ze3numberz31zzmodule_checksumz00(obj_t BgL_symbolz00_11)
	{
		{	/* Module/checksum.scm 79 */
			{
				char *BgL_strz00_115;

				{	/* Module/checksum.scm 94 */
					char *BgL_arg1056z00_113;

					{	/* Module/checksum.scm 94 */
						obj_t BgL_arg1057z00_114;

						{	/* Module/checksum.scm 94 */
							obj_t BgL_arg1455z00_297;

							BgL_arg1455z00_297 = SYMBOL_TO_STRING(((obj_t) BgL_symbolz00_11));
							BgL_arg1057z00_114 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_297);
						}
						BgL_strz00_115 = BSTRING_TO_STRING(BgL_arg1057z00_114);
						{	/* Module/checksum.scm 81 */
							long BgL_lenz00_117;

							BgL_lenz00_117 =
								(STRING_LENGTH(string_to_bstring(BgL_strz00_115)) - 1L);
							{
								long BgL_iz00_119;
								bool_t BgL_armedzf3zf3_120;

								BgL_iz00_119 = 0L;
								BgL_armedzf3zf3_120 = ((bool_t) 0);
							BgL_zc3z04anonymousza31059ze3z87_121:
								if ((BgL_iz00_119 == BgL_lenz00_117))
									{	/* Module/checksum.scm 86 */
										BgL_arg1056z00_113 = BgL_strz00_115;
									}
								else
									{	/* Module/checksum.scm 86 */
										if (
											(STRING_REF(string_to_bstring(BgL_strz00_115),
													BgL_iz00_119) == ((unsigned char) ':')))
											{	/* Module/checksum.scm 88 */
												if (BgL_armedzf3zf3_120)
													{	/* Module/checksum.scm 90 */
														long BgL_arg1065z00_125;
														long BgL_arg1066z00_126;

														BgL_arg1065z00_125 = (BgL_iz00_119 + 1L);
														BgL_arg1066z00_126 = (BgL_lenz00_117 + 1L);
														{	/* Module/checksum.scm 90 */
															obj_t BgL_stringz00_291;

															BgL_stringz00_291 =
																string_to_bstring(BgL_strz00_115);
															BgL_arg1056z00_113 =
																BSTRING_TO_STRING(c_substring(BgL_stringz00_291,
																	BgL_arg1065z00_125, BgL_arg1066z00_126));
													}}
												else
													{
														bool_t BgL_armedzf3zf3_599;
														long BgL_iz00_597;

														BgL_iz00_597 = (BgL_iz00_119 + 1L);
														BgL_armedzf3zf3_599 = ((bool_t) 1);
														BgL_armedzf3zf3_120 = BgL_armedzf3zf3_599;
														BgL_iz00_119 = BgL_iz00_597;
														goto BgL_zc3z04anonymousza31059ze3z87_121;
													}
											}
										else
											{
												bool_t BgL_armedzf3zf3_602;
												long BgL_iz00_600;

												BgL_iz00_600 = (BgL_iz00_119 + 1L);
												BgL_armedzf3zf3_602 = ((bool_t) 0);
												BgL_armedzf3zf3_120 = BgL_armedzf3zf3_602;
												BgL_iz00_119 = BgL_iz00_600;
												goto BgL_zc3z04anonymousza31059ze3z87_121;
											}
									}
							}
						}
					}
					BGL_TAIL return
						BGl_getzd2hashnumberzd2zz__hashz00(string_to_bstring
						(BgL_arg1056z00_113));
				}
			}
		}

	}



/* atom->number */
	obj_t BGl_atomzd2ze3numberz31zzmodule_checksumz00(obj_t BgL_checksumz00_13,
		obj_t BgL_clausez00_14)
	{
		{	/* Module/checksum.scm 105 */
		BGl_atomzd2ze3numberz31zzmodule_checksumz00:
			if (INTEGERP(BgL_clausez00_14))
				{	/* Module/checksum.scm 107 */
					return
						BINT(
						((long) CINT(BgL_checksumz00_13) ^ (long) CINT(BgL_clausez00_14)));
				}
			else
				{	/* Module/checksum.scm 107 */
					if (REALP(BgL_clausez00_14))
						{	/* Module/checksum.scm 110 */
							obj_t BgL_arg1079z00_135;

							BgL_arg1079z00_135 =
								bgl_real_to_string(REAL_TO_DOUBLE(BgL_clausez00_14));
							{
								obj_t BgL_clausez00_616;

								BgL_clausez00_616 = BgL_arg1079z00_135;
								BgL_clausez00_14 = BgL_clausez00_616;
								goto BGl_atomzd2ze3numberz31zzmodule_checksumz00;
							}
						}
					else
						{	/* Module/checksum.scm 109 */
							if (CHARP(BgL_clausez00_14))
								{	/* Module/checksum.scm 111 */
									return
										BINT(
										((long) CINT(BgL_checksumz00_13) ^
											(23L + (CCHAR(BgL_clausez00_14)))));
								}
							else
								{	/* Module/checksum.scm 111 */
									if (CNSTP(BgL_clausez00_14))
										{	/* Module/checksum.scm 114 */
											long BgL_arg1085z00_140;

											BgL_arg1085z00_140 = (90L + CCNST(BgL_clausez00_14));
											return
												BINT(
												((long) CINT(BgL_checksumz00_13) ^ BgL_arg1085z00_140));
										}
									else
										{	/* Module/checksum.scm 113 */
											if (STRINGP(BgL_clausez00_14))
												{	/* Module/checksum.scm 116 */
													long BgL_arg1090z00_143;

													BgL_arg1090z00_143 =
														(4L +
														BGl_getzd2hashnumberzd2zz__hashz00
														(BgL_clausez00_14));
													return BINT(((long) CINT(BgL_checksumz00_13) ^
															BgL_arg1090z00_143));
												}
											else
												{	/* Module/checksum.scm 115 */
													if (SYMBOLP(BgL_clausez00_14))
														{	/* Module/checksum.scm 118 */
															long BgL_arg1097z00_146;

															BgL_arg1097z00_146 =
																(150L +
																BGl_symbolzd2ze3numberz31zzmodule_checksumz00
																(BgL_clausez00_14));
															return BINT(((long) CINT(BgL_checksumz00_13) ^
																	BgL_arg1097z00_146));
														}
													else
														{	/* Module/checksum.scm 117 */
															if (KEYWORDP(BgL_clausez00_14))
																{	/* Module/checksum.scm 119 */
																	return
																		BINT(
																		((long) CINT(BgL_checksumz00_13) ^
																			(151L + 0L)));
																}
															else
																{	/* Module/checksum.scm 119 */
																	if (PAIRP(BgL_clausez00_14))
																		{	/* Module/checksum.scm 121 */
																			BGL_TAIL return
																				BGl_listzd2ze3numberz31zzmodule_checksumz00
																				(BgL_checksumz00_13, BgL_clausez00_14);
																		}
																	else
																		{	/* Module/checksum.scm 121 */
																			if (VECTORP(BgL_clausez00_14))
																				{	/* Module/checksum.scm 172 */
																					long BgL_arg1188z00_316;
																					obj_t BgL_arg1189z00_317;

																					BgL_arg1188z00_316 =
																						(73854L ^
																						(long) CINT(BgL_checksumz00_13));
																					BgL_arg1189z00_317 =
																						BGl_vectorzd2ze3listz31zz__r4_vectors_6_8z00
																						(BgL_clausez00_14);
																					BGL_TAIL return
																						BGl_listzd2ze3numberz31zzmodule_checksumz00
																						(BINT(BgL_arg1188z00_316),
																						BgL_arg1189z00_317);
																				}
																			else
																				{	/* Module/checksum.scm 123 */
																					if (ELONGP(BgL_clausez00_14))
																						{	/* Module/checksum.scm 126 */
																							long BgL_arg1122z00_154;

																							{	/* Module/checksum.scm 126 */
																								long BgL_xz00_319;

																								BgL_xz00_319 =
																									BELONG_TO_LONG
																									(BgL_clausez00_14);
																								BgL_arg1122z00_154 =
																									(long) (BgL_xz00_319);
																							}
																							return
																								BINT(
																								((long) CINT(BgL_checksumz00_13)
																									^ BgL_arg1122z00_154));
																						}
																					else
																						{	/* Module/checksum.scm 125 */
																							if (LLONGP(BgL_clausez00_14))
																								{	/* Module/checksum.scm 128 */
																									long BgL_xz00_327;
																									long BgL_yz00_328;

																									BgL_xz00_327 =
																										(long)
																										CINT(BgL_checksumz00_13);
																									{	/* Module/checksum.scm 129 */
																										BGL_LONGLONG_T
																											BgL_tmpz00_672;
																										BgL_tmpz00_672 =
																											((BLLONG_TO_LLONG
																												(BgL_clausez00_14) >>
																												(int) (32L)) ^
																											((BLLONG_TO_LLONG
																													(BgL_clausez00_14) <<
																													(int) (32L)) >>
																												(int) (32L)));
																										BgL_yz00_328 =
																											LLONG_TO_LONG
																											(BgL_tmpz00_672);
																									}
																									return
																										BINT(
																										(BgL_xz00_327 ^
																											BgL_yz00_328));
																								}
																							else
																								{	/* Module/checksum.scm 127 */
																									if (BGL_INT8P
																										(BgL_clausez00_14))
																										{	/* Module/checksum.scm 132 */
																											long BgL_arg1131z00_161;

																											{	/* Module/checksum.scm 132 */
																												int8_t BgL_xz00_329;

																												BgL_xz00_329 =
																													BGL_BINT8_TO_INT8
																													(BgL_clausez00_14);
																												{	/* Module/checksum.scm 132 */
																													long
																														BgL_arg1451z00_330;
																													BgL_arg1451z00_330 =
																														(long)
																														(BgL_xz00_329);
																													BgL_arg1131z00_161 =
																														(long)
																														(BgL_arg1451z00_330);
																											}}
																											return
																												BINT(
																												((long)
																													CINT
																													(BgL_checksumz00_13) ^
																													BgL_arg1131z00_161));
																										}
																									else
																										{	/* Module/checksum.scm 131 */
																											if (BGL_UINT8P
																												(BgL_clausez00_14))
																												{	/* Module/checksum.scm 134 */
																													long
																														BgL_arg1137z00_163;
																													{	/* Module/checksum.scm 134 */
																														uint8_t
																															BgL_xz00_334;
																														BgL_xz00_334 =
																															BGL_BUINT8_TO_UINT8
																															(BgL_clausez00_14);
																														{	/* Module/checksum.scm 134 */
																															long
																																BgL_arg1450z00_335;
																															BgL_arg1450z00_335
																																=
																																(long)
																																(BgL_xz00_334);
																															BgL_arg1137z00_163
																																=
																																(long)
																																(BgL_arg1450z00_335);
																													}}
																													return
																														BINT(
																														((long)
																															CINT
																															(BgL_checksumz00_13)
																															^
																															BgL_arg1137z00_163));
																												}
																											else
																												{	/* Module/checksum.scm 133 */
																													if (BGL_INT16P
																														(BgL_clausez00_14))
																														{	/* Module/checksum.scm 136 */
																															long
																																BgL_arg1140z00_165;
																															{	/* Module/checksum.scm 136 */
																																int16_t
																																	BgL_xz00_339;
																																BgL_xz00_339 =
																																	BGL_BINT16_TO_INT16
																																	(BgL_clausez00_14);
																																{	/* Module/checksum.scm 136 */
																																	long
																																		BgL_arg1449z00_340;
																																	BgL_arg1449z00_340
																																		=
																																		(long)
																																		(BgL_xz00_339);
																																	BgL_arg1140z00_165
																																		=
																																		(long)
																																		(BgL_arg1449z00_340);
																															}}
																															return
																																BINT(
																																((long)
																																	CINT
																																	(BgL_checksumz00_13)
																																	^
																																	BgL_arg1140z00_165));
																														}
																													else
																														{	/* Module/checksum.scm 135 */
																															if (BGL_UINT16P
																																(BgL_clausez00_14))
																																{	/* Module/checksum.scm 138 */
																																	long
																																		BgL_arg1142z00_167;
																																	{	/* Module/checksum.scm 138 */
																																		uint16_t
																																			BgL_xz00_344;
																																		BgL_xz00_344
																																			=
																																			BGL_BUINT16_TO_UINT16
																																			(BgL_clausez00_14);
																																		{	/* Module/checksum.scm 138 */
																																			long
																																				BgL_arg1447z00_345;
																																			BgL_arg1447z00_345
																																				=
																																				(long)
																																				(BgL_xz00_344);
																																			BgL_arg1142z00_167
																																				=
																																				(long)
																																				(BgL_arg1447z00_345);
																																	}}
																																	return
																																		BINT(
																																		((long)
																																			CINT
																																			(BgL_checksumz00_13)
																																			^
																																			BgL_arg1142z00_167));
																																}
																															else
																																{	/* Module/checksum.scm 137 */
																																	if (BGL_INT32P
																																		(BgL_clausez00_14))
																																		{	/* Module/checksum.scm 140 */
																																			long
																																				BgL_arg1145z00_169;
																																			{	/* Module/checksum.scm 140 */
																																				int32_t
																																					BgL_xz00_349;
																																				BgL_xz00_349
																																					=
																																					BGL_BINT32_TO_INT32
																																					(BgL_clausez00_14);
																																				{	/* Module/checksum.scm 140 */
																																					long
																																						BgL_arg1446z00_350;
																																					BgL_arg1446z00_350
																																						=
																																						(long)
																																						(BgL_xz00_349);
																																					BgL_arg1145z00_169
																																						=
																																						(long)
																																						(BgL_arg1446z00_350);
																																			}}
																																			return
																																				BINT(
																																				((long)
																																					CINT
																																					(BgL_checksumz00_13)
																																					^
																																					BgL_arg1145z00_169));
																																		}
																																	else
																																		{	/* Module/checksum.scm 139 */
																																			if (BGL_UINT32P(BgL_clausez00_14))
																																				{	/* Module/checksum.scm 142 */
																																					long
																																						BgL_arg1148z00_171;
																																					{	/* Module/checksum.scm 142 */
																																						uint32_t
																																							BgL_xz00_354;
																																						BgL_xz00_354
																																							=
																																							BGL_BUINT32_TO_UINT32
																																							(BgL_clausez00_14);
																																						BgL_arg1148z00_171
																																							=
																																							(long)
																																							(BgL_xz00_354);
																																					}
																																					return
																																						BINT
																																						(((long) CINT(BgL_checksumz00_13) ^ BgL_arg1148z00_171));
																																				}
																																			else
																																				{	/* Module/checksum.scm 141 */
																																					if (BGL_INT64P(BgL_clausez00_14))
																																						{	/* Module/checksum.scm 144 */
																																							long
																																								BgL_arg1152z00_173;
																																							{	/* Module/checksum.scm 144 */
																																								int64_t
																																									BgL_xz00_357;
																																								BgL_xz00_357
																																									=
																																									BGL_BINT64_TO_INT64
																																									(BgL_clausez00_14);
																																								{	/* Module/checksum.scm 144 */
																																									long
																																										BgL_arg1445z00_358;
																																									BgL_arg1445z00_358
																																										=
																																										(long)
																																										(BgL_xz00_357);
																																									BgL_arg1152z00_173
																																										=
																																										(long)
																																										(BgL_arg1445z00_358);
																																							}}
																																							return
																																								BINT
																																								(
																																								((long) CINT(BgL_checksumz00_13) ^ BgL_arg1152z00_173));
																																						}
																																					else
																																						{	/* Module/checksum.scm 143 */
																																							if (BGL_UINT64P(BgL_clausez00_14))
																																								{	/* Module/checksum.scm 146 */
																																									long
																																										BgL_arg1154z00_175;
																																									{	/* Module/checksum.scm 146 */
																																										uint64_t
																																											BgL_xz00_362;
																																										BgL_xz00_362
																																											=
																																											BGL_BINT64_TO_INT64
																																											(BgL_clausez00_14);
																																										{	/* Module/checksum.scm 146 */
																																											long
																																												BgL_arg1444z00_363;
																																											BgL_arg1444z00_363
																																												=
																																												(long)
																																												(BgL_xz00_362);
																																											BgL_arg1154z00_175
																																												=
																																												(long)
																																												(BgL_arg1444z00_363);
																																									}}
																																									return
																																										BINT
																																										(
																																										((long) CINT(BgL_checksumz00_13) ^ BgL_arg1154z00_175));
																																								}
																																							else
																																								{	/* Module/checksum.scm 145 */
																																									if (BGL_INT32P(BgL_clausez00_14))
																																										{	/* Module/checksum.scm 148 */
																																											long
																																												BgL_arg1157z00_177;
																																											{	/* Module/checksum.scm 148 */
																																												int32_t
																																													BgL_xz00_367;
																																												BgL_xz00_367
																																													=
																																													BGL_BINT32_TO_INT32
																																													(BgL_clausez00_14);
																																												{	/* Module/checksum.scm 148 */
																																													long
																																														BgL_arg1446z00_368;
																																													BgL_arg1446z00_368
																																														=
																																														(long)
																																														(BgL_xz00_367);
																																													BgL_arg1157z00_177
																																														=
																																														(long)
																																														(BgL_arg1446z00_368);
																																											}}
																																											return
																																												BINT
																																												(
																																												((long) CINT(BgL_checksumz00_13) ^ BgL_arg1157z00_177));
																																										}
																																									else
																																										{	/* Module/checksum.scm 147 */
																																											{	/* Module/checksum.scm 151 */
																																												obj_t
																																													BgL_arg1158z00_178;
																																												BgL_arg1158z00_178
																																													=
																																													bgl_find_runtime_type
																																													(BgL_clausez00_14);
																																												{	/* Module/checksum.scm 150 */
																																													obj_t
																																														BgL_list1159z00_179;
																																													{	/* Module/checksum.scm 150 */
																																														obj_t
																																															BgL_arg1162z00_180;
																																														{	/* Module/checksum.scm 150 */
																																															obj_t
																																																BgL_arg1164z00_181;
																																															{	/* Module/checksum.scm 150 */
																																																obj_t
																																																	BgL_arg1166z00_182;
																																																BgL_arg1166z00_182
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BgL_arg1158z00_178,
																																																	BNIL);
																																																BgL_arg1164z00_181
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BGl_string1218z00zzmodule_checksumz00,
																																																	BgL_arg1166z00_182);
																																															}
																																															BgL_arg1162z00_180
																																																=
																																																MAKE_YOUNG_PAIR
																																																(BgL_clausez00_14,
																																																BgL_arg1164z00_181);
																																														}
																																														BgL_list1159z00_179
																																															=
																																															MAKE_YOUNG_PAIR
																																															(BGl_string1219z00zzmodule_checksumz00,
																																															BgL_arg1162z00_180);
																																													}
																																													BGl_warningz00zz__errorz00
																																														(BgL_list1159z00_179);
																																												}
																																											}
																																											return
																																												BINT
																																												(0L);
																																										}
																																								}
																																						}
																																				}
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* list->number */
	obj_t BGl_listzd2ze3numberz31zzmodule_checksumz00(obj_t BgL_checksumz00_15,
		obj_t BgL_clausez00_16)
	{
		{	/* Module/checksum.scm 157 */
		BGl_listzd2ze3numberz31zzmodule_checksumz00:
			{

				if (NULLP(BgL_clausez00_16))
					{	/* Module/checksum.scm 158 */
						return BgL_checksumz00_15;
					}
				else
					{	/* Module/checksum.scm 158 */
						if (PAIRP(BgL_clausez00_16))
							{	/* Module/checksum.scm 158 */
								obj_t BgL_carzd2174zd2_189;

								BgL_carzd2174zd2_189 = CAR(((obj_t) BgL_clausez00_16));
								if ((BgL_carzd2174zd2_189 == CNST_TABLE_REF(7)))
									{	/* Module/checksum.scm 158 */
										return BINT(0L);
									}
								else
									{	/* Module/checksum.scm 158 */
										if ((BgL_carzd2174zd2_189 == CNST_TABLE_REF(8)))
											{	/* Module/checksum.scm 158 */
												return BINT(0L);
											}
										else
											{	/* Module/checksum.scm 158 */
												if ((BgL_carzd2174zd2_189 == CNST_TABLE_REF(9)))
													{	/* Module/checksum.scm 158 */
														return BINT(0L);
													}
												else
													{	/* Module/checksum.scm 158 */
													BgL_tagzd2172zd2_185:
														if (PAIRP(BgL_clausez00_16))
															{
																obj_t BgL_clausez00_786;
																obj_t BgL_checksumz00_783;

																BgL_checksumz00_783 =
																	BGl_atomzd2ze3numberz31zzmodule_checksumz00
																	(BgL_checksumz00_15, CAR(BgL_clausez00_16));
																BgL_clausez00_786 = CDR(BgL_clausez00_16);
																BgL_clausez00_16 = BgL_clausez00_786;
																BgL_checksumz00_15 = BgL_checksumz00_783;
																goto
																	BGl_listzd2ze3numberz31zzmodule_checksumz00;
															}
														else
															{	/* Module/checksum.scm 164 */
																BGL_TAIL return
																	BGl_atomzd2ze3numberz31zzmodule_checksumz00
																	(BgL_checksumz00_15, BgL_clausez00_16);
															}
													}
											}
									}
							}
						else
							{	/* Module/checksum.scm 158 */
								goto BgL_tagzd2172zd2_185;
							}
					}
			}
		}

	}



/* extern-clause-checksum */
	obj_t BGl_externzd2clausezd2checksumz00zzmodule_checksumz00(obj_t
		BgL_checksumz00_19, obj_t BgL_clausesz00_20, obj_t BgL_includezd2pathzd2_21)
	{
		{	/* Module/checksum.scm 177 */
			{
				obj_t BgL_clausesz00_201;
				obj_t BgL_checksumz00_202;

				BgL_clausesz00_201 = BgL_clausesz00_20;
				BgL_checksumz00_202 = BgL_checksumz00_19;
			BgL_zc3z04anonymousza31190ze3z87_203:
				{
					obj_t BgL_filez00_205;
					obj_t BgL_restz00_206;

					if (NULLP(BgL_clausesz00_201))
						{	/* Module/checksum.scm 178 */
							return BgL_checksumz00_202;
						}
					else
						{	/* Module/checksum.scm 178 */
							if (PAIRP(BgL_clausesz00_201))
								{	/* Module/checksum.scm 178 */
									obj_t BgL_carzd2188zd2_212;

									BgL_carzd2188zd2_212 = CAR(((obj_t) BgL_clausesz00_201));
									if (PAIRP(BgL_carzd2188zd2_212))
										{	/* Module/checksum.scm 178 */
											obj_t BgL_cdrzd2192zd2_214;

											BgL_cdrzd2192zd2_214 = CDR(BgL_carzd2188zd2_212);
											if ((CAR(BgL_carzd2188zd2_212) == CNST_TABLE_REF(6)))
												{	/* Module/checksum.scm 178 */
													if (PAIRP(BgL_cdrzd2192zd2_214))
														{	/* Module/checksum.scm 178 */
															if (NULLP(CDR(BgL_cdrzd2192zd2_214)))
																{	/* Module/checksum.scm 178 */
																	obj_t BgL_arg1199z00_220;
																	obj_t BgL_arg1200z00_221;

																	BgL_arg1199z00_220 =
																		CAR(BgL_cdrzd2192zd2_214);
																	BgL_arg1200z00_221 =
																		CDR(((obj_t) BgL_clausesz00_201));
																	BgL_filez00_205 = BgL_arg1199z00_220;
																	BgL_restz00_206 = BgL_arg1200z00_221;
																	{	/* Module/checksum.scm 186 */
																		obj_t BgL_fullnamez00_224;

																		BgL_fullnamez00_224 =
																			BGl_findzd2filezf2pathz20zz__osz00
																			(BgL_filez00_205,
																			BgL_includezd2pathzd2_21);
																		{	/* Module/checksum.scm 186 */
																			long BgL_timez00_225;

																			{	/* Module/checksum.scm 187 */
																				bool_t BgL_test1293z00_811;

																				if (STRINGP(BgL_fullnamez00_224))
																					{	/* Module/checksum.scm 187 */
																						BgL_test1293z00_811 =
																							fexists(BSTRING_TO_STRING
																							(BgL_fullnamez00_224));
																					}
																				else
																					{	/* Module/checksum.scm 187 */
																						BgL_test1293z00_811 = ((bool_t) 0);
																					}
																				if (BgL_test1293z00_811)
																					{	/* Module/checksum.scm 190 */
																						double BgL_arg1209z00_230;

																						BgL_arg1209z00_230 =
																							(double)
																							(bgl_last_modification_time
																							(BSTRING_TO_STRING
																								(BgL_fullnamez00_224)));
																						BgL_timez00_225 =
																							(long) (BgL_arg1209z00_230);
																					}
																				else
																					{	/* Module/checksum.scm 187 */
																						BgL_timez00_225 = 0L;
																					}
																			}
																			{	/* Module/checksum.scm 187 */

																				{	/* Module/checksum.scm 192 */
																					obj_t BgL_arg1203z00_226;
																					obj_t BgL_arg1206z00_227;

																					BgL_arg1203z00_226 =
																						CDR(((obj_t) BgL_clausesz00_201));
																					BgL_arg1206z00_227 =
																						BGl_atomzd2ze3numberz31zzmodule_checksumz00
																						(BgL_checksumz00_202,
																						BINT(BgL_timez00_225));
																					{
																						obj_t BgL_checksumz00_825;
																						obj_t BgL_clausesz00_824;

																						BgL_clausesz00_824 =
																							BgL_arg1203z00_226;
																						BgL_checksumz00_825 =
																							BgL_arg1206z00_227;
																						BgL_checksumz00_202 =
																							BgL_checksumz00_825;
																						BgL_clausesz00_201 =
																							BgL_clausesz00_824;
																						goto
																							BgL_zc3z04anonymousza31190ze3z87_203;
																					}
																				}
																			}
																		}
																	}
																}
															else
																{	/* Module/checksum.scm 178 */
																BgL_tagzd2180zd2_208:
																	{	/* Module/checksum.scm 195 */
																		obj_t BgL_arg1212z00_233;
																		obj_t BgL_arg1215z00_234;

																		BgL_arg1212z00_233 =
																			CDR(((obj_t) BgL_clausesz00_201));
																		{	/* Module/checksum.scm 196 */
																			obj_t BgL_arg1216z00_235;

																			BgL_arg1216z00_235 =
																				CAR(((obj_t) BgL_clausesz00_201));
																			BgL_arg1215z00_234 =
																				BGl_atomzd2ze3numberz31zzmodule_checksumz00
																				(BgL_checksumz00_202,
																				BgL_arg1216z00_235);
																		}
																		{
																			obj_t BgL_checksumz00_832;
																			obj_t BgL_clausesz00_831;

																			BgL_clausesz00_831 = BgL_arg1212z00_233;
																			BgL_checksumz00_832 = BgL_arg1215z00_234;
																			BgL_checksumz00_202 = BgL_checksumz00_832;
																			BgL_clausesz00_201 = BgL_clausesz00_831;
																			goto BgL_zc3z04anonymousza31190ze3z87_203;
																		}
																	}
																}
														}
													else
														{	/* Module/checksum.scm 178 */
															goto BgL_tagzd2180zd2_208;
														}
												}
											else
												{	/* Module/checksum.scm 178 */
													goto BgL_tagzd2180zd2_208;
												}
										}
									else
										{	/* Module/checksum.scm 178 */
											goto BgL_tagzd2180zd2_208;
										}
								}
							else
								{	/* Module/checksum.scm 178 */
									goto BgL_tagzd2180zd2_208;
								}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_checksumz00(void)
	{
		{	/* Module/checksum.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_checksumz00(void)
	{
		{	/* Module/checksum.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_checksumz00(void)
	{
		{	/* Module/checksum.scm 18 */
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
