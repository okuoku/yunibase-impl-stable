/*===========================================================================*/
/*   (Module/library.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/library.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_LIBRARY_TYPE_DEFINITIONS
#define BGL_MODULE_LIBRARY_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_MODULE_LIBRARY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_libraryz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzmodule_libraryz00(void);
	static obj_t BGl_z62withzd2libraryzd2modulez12z70zzmodule_libraryz00(obj_t,
		obj_t);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31140ze3ze5zzmodule_libraryz00(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_libraryz00(void);
	static obj_t BGl_za2neededzd2modulesza2zd2zzmodule_libraryz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_withzd2libraryzd2modulez12z12zzmodule_libraryz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_libraryz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2debugzd2moduleza2zd2zzengine_paramz00;
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_getzd2importedzd2modulesz00zzmodule_impusez00(void);
	static obj_t BGl_methodzd2initzd2zzmodule_libraryz00(void);
	static obj_t BGl_za2withzd2libraryzd2modulesza2z00zzmodule_libraryz00 =
		BUNSPEC;
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_libraryzd2finaliza7erz75zzmodule_libraryz00(void);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_libraryz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbdb_settingz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_needzd2libraryzd2modulez12z12zzmodule_libraryz00(obj_t);
	extern obj_t BGl_za2jvmzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zzmodule_libraryz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_libraryz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_libraryz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_libraryz00(void);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	extern obj_t BGl_za2jvmzd2debugzd2moduleza2z00zzbdb_settingz00;
	static obj_t BGl_za2keyza2z00zzmodule_libraryz00 = BUNSPEC;
	static obj_t BGl_z62libraryzd2finaliza7erz17zzmodule_libraryz00(obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_za2bdbzd2moduleza2zd2zzbdb_settingz00;
	extern obj_t BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00(obj_t);
	static obj_t BGl_za2withzd2keyza2zd2zzmodule_libraryz00 = BUNSPEC;
	extern obj_t BGl_cvarz00zzast_varz00;
	static obj_t __cnst[11];


	   
		 
		DEFINE_STRING(BGl_string1314z00zzmodule_libraryz00,
		BgL_bgl_string1314za700za7za7m1319za7, "bgl_init_module_debug_library(\"",
		31);
	      DEFINE_STRING(BGl_string1315z00zzmodule_libraryz00,
		BgL_bgl_string1315za700za7za7m1320za7, "\")", 2);
	      DEFINE_STRING(BGl_string1316z00zzmodule_libraryz00,
		BgL_bgl_string1316za700za7za7m1321za7, "module_library", 14);
	      DEFINE_STRING(BGl_string1317z00zzmodule_libraryz00,
		BgL_bgl_string1317za700za7za7m1322za7,
		"@ unit library-modules begin pragma::void module (#unspecified) jvm bdb foreign with-key ",
		89);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1313z00zzmodule_libraryz00,
		BgL_bgl_za762za7c3za704anonymo1323za7,
		BGl_z62zc3z04anonymousza31140ze3ze5zzmodule_libraryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_libraryzd2finaliza7erzd2envza7zzmodule_libraryz00,
		BgL_bgl_za762libraryza7d2fin1324z00,
		BGl_z62libraryzd2finaliza7erz17zzmodule_libraryz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_withzd2libraryzd2modulez12zd2envzc0zzmodule_libraryz00,
		BgL_bgl_za762withza7d2librar1325z00,
		BGl_z62withzd2libraryzd2modulez12z70zzmodule_libraryz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_libraryz00));
		     ADD_ROOT((void *) (&BGl_za2neededzd2modulesza2zd2zzmodule_libraryz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2withzd2libraryzd2modulesza2z00zzmodule_libraryz00));
		     ADD_ROOT((void *) (&BGl_za2keyza2z00zzmodule_libraryz00));
		     ADD_ROOT((void *) (&BGl_za2withzd2keyza2zd2zzmodule_libraryz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzmodule_libraryz00(long
		BgL_checksumz00_960, char *BgL_fromz00_961)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_libraryz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_libraryz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_libraryz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_libraryz00();
					BGl_cnstzd2initzd2zzmodule_libraryz00();
					BGl_importedzd2moduleszd2initz00zzmodule_libraryz00();
					return BGl_toplevelzd2initzd2zzmodule_libraryz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"module_library");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_library");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_library");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_library");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_library");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_library");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_library");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"module_library");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_library");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "module_library");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_library");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 15 */
			{	/* Module/library.scm 15 */
				obj_t BgL_cportz00_916;

				{	/* Module/library.scm 15 */
					obj_t BgL_stringz00_923;

					BgL_stringz00_923 = BGl_string1317z00zzmodule_libraryz00;
					{	/* Module/library.scm 15 */
						obj_t BgL_startz00_924;

						BgL_startz00_924 = BINT(0L);
						{	/* Module/library.scm 15 */
							obj_t BgL_endz00_925;

							BgL_endz00_925 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_923)));
							{	/* Module/library.scm 15 */

								BgL_cportz00_916 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_923, BgL_startz00_924, BgL_endz00_925);
				}}}}
				{
					long BgL_iz00_917;

					BgL_iz00_917 = 10L;
				BgL_loopz00_918:
					if ((BgL_iz00_917 == -1L))
						{	/* Module/library.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/library.scm 15 */
							{	/* Module/library.scm 15 */
								obj_t BgL_arg1318z00_919;

								{	/* Module/library.scm 15 */

									{	/* Module/library.scm 15 */
										obj_t BgL_locationz00_921;

										BgL_locationz00_921 = BBOOL(((bool_t) 0));
										{	/* Module/library.scm 15 */

											BgL_arg1318z00_919 =
												BGl_readz00zz__readerz00(BgL_cportz00_916,
												BgL_locationz00_921);
										}
									}
								}
								{	/* Module/library.scm 15 */
									int BgL_tmpz00_990;

									BgL_tmpz00_990 = (int) (BgL_iz00_917);
									CNST_TABLE_SET(BgL_tmpz00_990, BgL_arg1318z00_919);
							}}
							{	/* Module/library.scm 15 */
								int BgL_auxz00_922;

								BgL_auxz00_922 = (int) ((BgL_iz00_917 - 1L));
								{
									long BgL_iz00_995;

									BgL_iz00_995 = (long) (BgL_auxz00_922);
									BgL_iz00_917 = BgL_iz00_995;
									goto BgL_loopz00_918;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 15 */
			BGl_za2neededzd2modulesza2zd2zzmodule_libraryz00 = BNIL;
			BGl_za2keyza2z00zzmodule_libraryz00 = BUNSPEC;
			BGl_za2withzd2keyza2zd2zzmodule_libraryz00 =
				BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(0));
			return (BGl_za2withzd2libraryzd2modulesza2z00zzmodule_libraryz00 =
				BNIL, BUNSPEC);
		}

	}



/* library-finalizer */
	BGL_EXPORTED_DEF obj_t BGl_libraryzd2finaliza7erz75zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 31 */
			{	/* Module/library.scm 33 */

				{	/* Module/library.scm 33 */

					BGl_za2keyza2z00zzmodule_libraryz00 =
						BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
			}}
			{	/* Module/library.scm 35 */
				obj_t BgL_g1094z00_587;

				BgL_g1094z00_587 = BGl_getzd2importedzd2modulesz00zzmodule_impusez00();
				{
					obj_t BgL_l1092z00_589;

					BgL_l1092z00_589 = BgL_g1094z00_587;
				BgL_zc3z04anonymousza31132ze3z87_590:
					if (PAIRP(BgL_l1092z00_589))
						{	/* Module/library.scm 36 */
							{	/* Module/library.scm 35 */
								obj_t BgL_modulez00_592;

								BgL_modulez00_592 = CAR(BgL_l1092z00_589);
								BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_modulez00_592,
									BGl_za2keyza2z00zzmodule_libraryz00, BTRUE);
							}
							{
								obj_t BgL_l1092z00_1006;

								BgL_l1092z00_1006 = CDR(BgL_l1092z00_589);
								BgL_l1092z00_589 = BgL_l1092z00_1006;
								goto BgL_zc3z04anonymousza31132ze3z87_590;
							}
						}
					else
						{	/* Module/library.scm 36 */
							((bool_t) 1);
						}
				}
			}
			BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(1),
				BGl_za2keyza2z00zzmodule_libraryz00, BTRUE);
			BGl_putpropz12z12zz__r4_symbols_6_4z00
				(BGl_za2moduleza2z00zzmodule_modulez00,
				BGl_za2keyza2z00zzmodule_libraryz00, BTRUE);
			BGl_forzd2eachzd2globalz12z12zzast_envz00
				(BGl_proc1313z00zzmodule_libraryz00, BNIL);
			{	/* Module/library.scm 50 */
				bool_t BgL_test1329z00_1012;

				if (((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) > 0L))
					{	/* Module/library.scm 51 */
						obj_t BgL_arg1187z00_634;

						{	/* Module/library.scm 51 */
							obj_t BgL_arg1188z00_635;

							BgL_arg1188z00_635 = BGl_thezd2backendzd2zzbackend_backendz00();
							BgL_arg1187z00_634 =
								(((BgL_backendz00_bglt) COBJECT(
										((BgL_backendz00_bglt) BgL_arg1188z00_635)))->
								BgL_debugzd2supportzd2);
						}
						BgL_test1329z00_1012 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(2),
								BgL_arg1187z00_634));
					}
				else
					{	/* Module/library.scm 50 */
						BgL_test1329z00_1012 = ((bool_t) 0);
					}
				if (BgL_test1329z00_1012)
					{
						obj_t BgL_l1095z00_627;

						BgL_l1095z00_627 = BGl_za2bdbzd2moduleza2zd2zzbdb_settingz00;
					BgL_zc3z04anonymousza31173ze3z87_628:
						if (PAIRP(BgL_l1095z00_627))
							{	/* Module/library.scm 52 */
								BGl_needzd2libraryzd2modulez12z12zzmodule_libraryz00(CAR
									(BgL_l1095z00_627));
								{
									obj_t BgL_l1095z00_1026;

									BgL_l1095z00_1026 = CDR(BgL_l1095z00_627);
									BgL_l1095z00_627 = BgL_l1095z00_1026;
									goto BgL_zc3z04anonymousza31173ze3z87_628;
								}
							}
						else
							{	/* Module/library.scm 52 */
								((bool_t) 1);
							}
					}
				else
					{	/* Module/library.scm 50 */
						((bool_t) 0);
					}
			}
			{	/* Module/library.scm 53 */
				bool_t BgL_test1332z00_1028;

				if (CBOOL(BGl_za2jvmzd2debugza2zd2zzengine_paramz00))
					{	/* Module/library.scm 53 */
						if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
							{	/* Module/library.scm 54 */
								BgL_test1332z00_1028 = ((bool_t) 0);
							}
						else
							{	/* Module/library.scm 55 */
								obj_t BgL_arg1197z00_646;

								{	/* Module/library.scm 55 */
									obj_t BgL_arg1198z00_647;

									BgL_arg1198z00_647 =
										BGl_thezd2backendzd2zzbackend_backendz00();
									BgL_arg1197z00_646 =
										(((BgL_backendz00_bglt) COBJECT(
												((BgL_backendz00_bglt) BgL_arg1198z00_647)))->
										BgL_debugzd2supportzd2);
								}
								BgL_test1332z00_1028 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF
										(3), BgL_arg1197z00_646));
							}
					}
				else
					{	/* Module/library.scm 53 */
						BgL_test1332z00_1028 = ((bool_t) 0);
					}
				if (BgL_test1332z00_1028)
					{
						obj_t BgL_l1097z00_640;

						BgL_l1097z00_640 =
							BGl_za2jvmzd2debugzd2moduleza2z00zzbdb_settingz00;
					BgL_zc3z04anonymousza31192ze3z87_641:
						if (PAIRP(BgL_l1097z00_640))
							{	/* Module/library.scm 56 */
								BGl_needzd2libraryzd2modulez12z12zzmodule_libraryz00(CAR
									(BgL_l1097z00_640));
								{
									obj_t BgL_l1097z00_1043;

									BgL_l1097z00_1043 = CDR(BgL_l1097z00_640);
									BgL_l1097z00_640 = BgL_l1097z00_1043;
									goto BgL_zc3z04anonymousza31192ze3z87_641;
								}
							}
						else
							{	/* Module/library.scm 56 */
								((bool_t) 1);
							}
					}
				else
					{	/* Module/library.scm 53 */
						((bool_t) 0);
					}
			}
			{
				obj_t BgL_l1099z00_649;

				BgL_l1099z00_649 =
					BGl_za2withzd2libraryzd2modulesza2z00zzmodule_libraryz00;
			BgL_zc3z04anonymousza31199ze3z87_650:
				if (PAIRP(BgL_l1099z00_649))
					{	/* Module/library.scm 59 */
						BGl_needzd2libraryzd2modulez12z12zzmodule_libraryz00(CAR
							(BgL_l1099z00_649));
						{
							obj_t BgL_l1099z00_1049;

							BgL_l1099z00_1049 = CDR(BgL_l1099z00_649);
							BgL_l1099z00_649 = BgL_l1099z00_1049;
							goto BgL_zc3z04anonymousza31199ze3z87_650;
						}
					}
				else
					{	/* Module/library.scm 59 */
						((bool_t) 1);
					}
			}
			{	/* Module/library.scm 61 */
				obj_t BgL_modulesz00_655;

				BgL_modulesz00_655 = BGl_za2neededzd2modulesza2zd2zzmodule_libraryz00;
				if (NULLP(BgL_modulesz00_655))
					{	/* Module/library.scm 62 */
						return BNIL;
					}
				else
					{	/* Module/library.scm 64 */
						obj_t BgL_g1063z00_657;

						BgL_g1063z00_657 = CNST_TABLE_REF(4);
						{
							obj_t BgL_modulesz00_659;
							obj_t BgL_initzd2callza2z70_660;

							BgL_modulesz00_659 = BgL_modulesz00_655;
							BgL_initzd2callza2z70_660 = BgL_g1063z00_657;
						BgL_zc3z04anonymousza31204ze3z87_661:
							if (NULLP(BgL_modulesz00_659))
								{	/* Module/library.scm 67 */
									obj_t BgL_bodyz00_663;

									{	/* Module/library.scm 67 */
										bool_t BgL_test1341z00_1056;

										if (
											((long) CINT(BGl_za2debugzd2moduleza2zd2zzengine_paramz00)
												> 0L))
											{	/* Module/library.scm 70 */
												obj_t BgL_arg1223z00_676;

												{	/* Module/library.scm 70 */
													obj_t BgL_arg1225z00_677;

													BgL_arg1225z00_677 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_arg1223z00_676 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1225z00_677)))->
														BgL_debugzd2supportzd2);
												}
												BgL_test1341z00_1056 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(CNST_TABLE_REF(5), BgL_arg1223z00_676));
											}
										else
											{	/* Module/library.scm 67 */
												BgL_test1341z00_1056 = ((bool_t) 0);
											}
										if (BgL_test1341z00_1056)
											{	/* Module/library.scm 75 */
												obj_t BgL_arg1212z00_668;

												{	/* Module/library.scm 75 */
													obj_t BgL_arg1215z00_669;

													{	/* Module/library.scm 75 */
														obj_t BgL_arg1216z00_670;
														obj_t BgL_arg1218z00_671;

														{	/* Module/library.scm 75 */
															obj_t BgL_arg1219z00_672;

															{	/* Module/library.scm 75 */
																obj_t BgL_arg1220z00_673;

																{	/* Module/library.scm 75 */
																	obj_t BgL_arg1221z00_674;

																	{	/* Module/library.scm 75 */
																		obj_t BgL_symbolz00_828;

																		BgL_symbolz00_828 =
																			BGl_za2moduleza2z00zzmodule_modulez00;
																		{	/* Module/library.scm 75 */
																			obj_t BgL_arg1455z00_829;

																			BgL_arg1455z00_829 =
																				SYMBOL_TO_STRING(BgL_symbolz00_828);
																			BgL_arg1221z00_674 =
																				BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																				(BgL_arg1455z00_829);
																		}
																	}
																	BgL_arg1220z00_673 =
																		string_append_3
																		(BGl_string1314z00zzmodule_libraryz00,
																		BgL_arg1221z00_674,
																		BGl_string1315z00zzmodule_libraryz00);
																}
																BgL_arg1219z00_672 =
																	MAKE_YOUNG_PAIR(BgL_arg1220z00_673, BNIL);
															}
															BgL_arg1216z00_670 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																BgL_arg1219z00_672);
														}
														BgL_arg1218z00_671 =
															BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
															(BgL_initzd2callza2z70_660, BNIL);
														BgL_arg1215z00_669 =
															MAKE_YOUNG_PAIR(BgL_arg1216z00_670,
															BgL_arg1218z00_671);
													}
													BgL_arg1212z00_668 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
														BgL_arg1215z00_669);
												}
												BgL_bodyz00_663 =
													MAKE_YOUNG_PAIR(BgL_arg1212z00_668, BNIL);
											}
										else
											{	/* Module/library.scm 67 */
												BgL_bodyz00_663 = BgL_initzd2callza2z70_660;
											}
									}
									{	/* Module/library.scm 79 */
										obj_t BgL_idz00_830;

										BgL_idz00_830 = CNST_TABLE_REF(8);
										{	/* Module/library.scm 79 */
											obj_t BgL_newz00_831;

											BgL_newz00_831 =
												create_struct(CNST_TABLE_REF(9), (int) (5L));
											{	/* Module/library.scm 79 */
												int BgL_tmpz00_1081;

												BgL_tmpz00_1081 = (int) (4L);
												STRUCT_SET(BgL_newz00_831, BgL_tmpz00_1081, BFALSE);
											}
											{	/* Module/library.scm 79 */
												int BgL_tmpz00_1084;

												BgL_tmpz00_1084 = (int) (3L);
												STRUCT_SET(BgL_newz00_831, BgL_tmpz00_1084, BTRUE);
											}
											{	/* Module/library.scm 79 */
												int BgL_tmpz00_1087;

												BgL_tmpz00_1087 = (int) (2L);
												STRUCT_SET(BgL_newz00_831, BgL_tmpz00_1087,
													BgL_bodyz00_663);
											}
											{	/* Module/library.scm 79 */
												obj_t BgL_auxz00_1092;
												int BgL_tmpz00_1090;

												BgL_auxz00_1092 = BINT(2L);
												BgL_tmpz00_1090 = (int) (1L);
												STRUCT_SET(BgL_newz00_831, BgL_tmpz00_1090,
													BgL_auxz00_1092);
											}
											{	/* Module/library.scm 79 */
												int BgL_tmpz00_1095;

												BgL_tmpz00_1095 = (int) (0L);
												STRUCT_SET(BgL_newz00_831, BgL_tmpz00_1095,
													BgL_idz00_830);
											}
											return BgL_newz00_831;
										}
									}
								}
							else
								{	/* Module/library.scm 80 */
									obj_t BgL_idz00_678;

									BgL_idz00_678 = CAR(((obj_t) BgL_modulesz00_659));
									{	/* Module/library.scm 80 */
										obj_t BgL_initzd2funzd2idz00_679;

										BgL_initzd2funzd2idz00_679 =
											BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00
											(BgL_idz00_678);
										{	/* Module/library.scm 81 */

											{	/* Module/library.scm 82 */
												obj_t BgL_arg1226z00_680;
												obj_t BgL_arg1227z00_681;

												BgL_arg1226z00_680 = CDR(((obj_t) BgL_modulesz00_659));
												{	/* Module/library.scm 83 */
													obj_t BgL_arg1228z00_682;

													{	/* Module/library.scm 83 */
														obj_t BgL_arg1229z00_683;
														obj_t BgL_arg1230z00_684;

														{	/* Module/library.scm 83 */
															obj_t BgL_arg1231z00_685;

															{	/* Module/library.scm 83 */
																obj_t BgL_arg1232z00_686;

																BgL_arg1232z00_686 =
																	MAKE_YOUNG_PAIR(BgL_idz00_678, BNIL);
																BgL_arg1231z00_685 =
																	MAKE_YOUNG_PAIR(BgL_initzd2funzd2idz00_679,
																	BgL_arg1232z00_686);
															}
															BgL_arg1229z00_683 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																BgL_arg1231z00_685);
														}
														{	/* Module/library.scm 87 */
															obj_t BgL_arg1233z00_687;

															{	/* Module/library.scm 87 */
																obj_t BgL_arg1234z00_688;

																{	/* Module/library.scm 87 */
																	obj_t BgL_symbolz00_839;

																	BgL_symbolz00_839 =
																		BGl_za2moduleza2z00zzmodule_modulez00;
																	{	/* Module/library.scm 87 */
																		obj_t BgL_arg1455z00_840;

																		BgL_arg1455z00_840 =
																			SYMBOL_TO_STRING(BgL_symbolz00_839);
																		BgL_arg1234z00_688 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_840);
																	}
																}
																BgL_arg1233z00_687 =
																	MAKE_YOUNG_PAIR(BgL_arg1234z00_688, BNIL);
															}
															BgL_arg1230z00_684 =
																MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1233z00_687);
														}
														BgL_arg1228z00_682 =
															MAKE_YOUNG_PAIR(BgL_arg1229z00_683,
															BgL_arg1230z00_684);
													}
													BgL_arg1227z00_681 =
														MAKE_YOUNG_PAIR(BgL_arg1228z00_682,
														BgL_initzd2callza2z70_660);
												}
												{
													obj_t BgL_initzd2callza2z70_1115;
													obj_t BgL_modulesz00_1114;

													BgL_modulesz00_1114 = BgL_arg1226z00_680;
													BgL_initzd2callza2z70_1115 = BgL_arg1227z00_681;
													BgL_initzd2callza2z70_660 =
														BgL_initzd2callza2z70_1115;
													BgL_modulesz00_659 = BgL_modulesz00_1114;
													goto BgL_zc3z04anonymousza31204ze3z87_661;
												}
											}
										}
									}
								}
						}
					}
			}
		}

	}



/* &library-finalizer */
	obj_t BGl_z62libraryzd2finaliza7erz17zzmodule_libraryz00(obj_t BgL_envz00_910)
	{
		{	/* Module/library.scm 31 */
			return BGl_libraryzd2finaliza7erz75zzmodule_libraryz00();
		}

	}



/* &<@anonymous:1140> */
	obj_t BGl_z62zc3z04anonymousza31140ze3ze5zzmodule_libraryz00(obj_t
		BgL_envz00_911, obj_t BgL_globalz00_912)
	{
		{	/* Module/library.scm 42 */
			{	/* Module/library.scm 45 */
				bool_t BgL_test1344z00_1117;

				if (
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_globalz00_912))))->
							BgL_occurrencez00) > 0L))
					{	/* Module/library.scm 45 */
						if (CBOOL(
								(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_globalz00_912)))->
									BgL_libraryz00)))
							{	/* Module/library.scm 47 */
								bool_t BgL_test1348z00_1127;

								{	/* Module/library.scm 47 */
									bool_t BgL_test1350z00_1128;

									{	/* Module/library.scm 47 */
										BgL_valuez00_bglt BgL_arg1162z00_927;

										BgL_arg1162z00_927 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_912))))->
											BgL_valuez00);
										{	/* Module/library.scm 47 */
											obj_t BgL_classz00_928;

											BgL_classz00_928 = BGl_cfunz00zzast_varz00;
											{	/* Module/library.scm 47 */
												BgL_objectz00_bglt BgL_arg1807z00_929;

												{	/* Module/library.scm 47 */
													obj_t BgL_tmpz00_1132;

													BgL_tmpz00_1132 =
														((obj_t) ((BgL_objectz00_bglt) BgL_arg1162z00_927));
													BgL_arg1807z00_929 =
														(BgL_objectz00_bglt) (BgL_tmpz00_1132);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Module/library.scm 47 */
														long BgL_idxz00_930;

														BgL_idxz00_930 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_929);
														BgL_test1350z00_1128 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_930 + 3L)) == BgL_classz00_928);
													}
												else
													{	/* Module/library.scm 47 */
														bool_t BgL_res1309z00_933;

														{	/* Module/library.scm 47 */
															obj_t BgL_oclassz00_934;

															{	/* Module/library.scm 47 */
																obj_t BgL_arg1815z00_935;
																long BgL_arg1816z00_936;

																BgL_arg1815z00_935 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Module/library.scm 47 */
																	long BgL_arg1817z00_937;

																	BgL_arg1817z00_937 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_929);
																	BgL_arg1816z00_936 =
																		(BgL_arg1817z00_937 - OBJECT_TYPE);
																}
																BgL_oclassz00_934 =
																	VECTOR_REF(BgL_arg1815z00_935,
																	BgL_arg1816z00_936);
															}
															{	/* Module/library.scm 47 */
																bool_t BgL__ortest_1115z00_938;

																BgL__ortest_1115z00_938 =
																	(BgL_classz00_928 == BgL_oclassz00_934);
																if (BgL__ortest_1115z00_938)
																	{	/* Module/library.scm 47 */
																		BgL_res1309z00_933 =
																			BgL__ortest_1115z00_938;
																	}
																else
																	{	/* Module/library.scm 47 */
																		long BgL_odepthz00_939;

																		{	/* Module/library.scm 47 */
																			obj_t BgL_arg1804z00_940;

																			BgL_arg1804z00_940 = (BgL_oclassz00_934);
																			BgL_odepthz00_939 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_940);
																		}
																		if ((3L < BgL_odepthz00_939))
																			{	/* Module/library.scm 47 */
																				obj_t BgL_arg1802z00_941;

																				{	/* Module/library.scm 47 */
																					obj_t BgL_arg1803z00_942;

																					BgL_arg1803z00_942 =
																						(BgL_oclassz00_934);
																					BgL_arg1802z00_941 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_942, 3L);
																				}
																				BgL_res1309z00_933 =
																					(BgL_arg1802z00_941 ==
																					BgL_classz00_928);
																			}
																		else
																			{	/* Module/library.scm 47 */
																				BgL_res1309z00_933 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1350z00_1128 = BgL_res1309z00_933;
													}
											}
										}
									}
									if (BgL_test1350z00_1128)
										{	/* Module/library.scm 47 */
											BgL_test1348z00_1127 = ((bool_t) 1);
										}
									else
										{	/* Module/library.scm 47 */
											BgL_valuez00_bglt BgL_arg1158z00_943;

											BgL_arg1158z00_943 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_globalz00_912))))->
												BgL_valuez00);
											{	/* Module/library.scm 47 */
												obj_t BgL_classz00_944;

												BgL_classz00_944 = BGl_cvarz00zzast_varz00;
												{	/* Module/library.scm 47 */
													BgL_objectz00_bglt BgL_arg1807z00_945;

													{	/* Module/library.scm 47 */
														obj_t BgL_tmpz00_1158;

														BgL_tmpz00_1158 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg1158z00_943));
														BgL_arg1807z00_945 =
															(BgL_objectz00_bglt) (BgL_tmpz00_1158);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Module/library.scm 47 */
															long BgL_idxz00_946;

															BgL_idxz00_946 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_945);
															BgL_test1348z00_1127 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_946 + 2L)) == BgL_classz00_944);
														}
													else
														{	/* Module/library.scm 47 */
															bool_t BgL_res1310z00_949;

															{	/* Module/library.scm 47 */
																obj_t BgL_oclassz00_950;

																{	/* Module/library.scm 47 */
																	obj_t BgL_arg1815z00_951;
																	long BgL_arg1816z00_952;

																	BgL_arg1815z00_951 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Module/library.scm 47 */
																		long BgL_arg1817z00_953;

																		BgL_arg1817z00_953 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_945);
																		BgL_arg1816z00_952 =
																			(BgL_arg1817z00_953 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_950 =
																		VECTOR_REF(BgL_arg1815z00_951,
																		BgL_arg1816z00_952);
																}
																{	/* Module/library.scm 47 */
																	bool_t BgL__ortest_1115z00_954;

																	BgL__ortest_1115z00_954 =
																		(BgL_classz00_944 == BgL_oclassz00_950);
																	if (BgL__ortest_1115z00_954)
																		{	/* Module/library.scm 47 */
																			BgL_res1310z00_949 =
																				BgL__ortest_1115z00_954;
																		}
																	else
																		{	/* Module/library.scm 47 */
																			long BgL_odepthz00_955;

																			{	/* Module/library.scm 47 */
																				obj_t BgL_arg1804z00_956;

																				BgL_arg1804z00_956 =
																					(BgL_oclassz00_950);
																				BgL_odepthz00_955 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_956);
																			}
																			if ((2L < BgL_odepthz00_955))
																				{	/* Module/library.scm 47 */
																					obj_t BgL_arg1802z00_957;

																					{	/* Module/library.scm 47 */
																						obj_t BgL_arg1803z00_958;

																						BgL_arg1803z00_958 =
																							(BgL_oclassz00_950);
																						BgL_arg1802z00_957 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_958, 2L);
																					}
																					BgL_res1310z00_949 =
																						(BgL_arg1802z00_957 ==
																						BgL_classz00_944);
																				}
																			else
																				{	/* Module/library.scm 47 */
																					BgL_res1310z00_949 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1348z00_1127 = BgL_res1310z00_949;
														}
												}
											}
										}
								}
								if (BgL_test1348z00_1127)
									{	/* Module/library.scm 47 */
										BgL_test1344z00_1117 = ((bool_t) 0);
									}
								else
									{	/* Module/library.scm 47 */
										BgL_test1344z00_1117 = ((bool_t) 1);
									}
							}
						else
							{	/* Module/library.scm 45 */
								BgL_test1344z00_1117 = ((bool_t) 0);
							}
					}
				else
					{	/* Module/library.scm 45 */
						BgL_test1344z00_1117 = ((bool_t) 0);
					}
				if (BgL_test1344z00_1117)
					{	/* Module/library.scm 48 */
						obj_t BgL_arg1157z00_959;

						BgL_arg1157z00_959 =
							(((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_globalz00_912)))->BgL_modulez00);
						return
							BGl_needzd2libraryzd2modulez12z12zzmodule_libraryz00
							(BgL_arg1157z00_959);
					}
				else
					{	/* Module/library.scm 45 */
						return BFALSE;
					}
			}
		}

	}



/* need-library-module! */
	obj_t BGl_needzd2libraryzd2modulez12z12zzmodule_libraryz00(obj_t
		BgL_modulez00_25)
	{
		{	/* Module/library.scm 103 */
			if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00(BgL_modulez00_25,
						BGl_za2keyza2z00zzmodule_libraryz00)))
				{	/* Module/library.scm 104 */
					return BFALSE;
				}
			else
				{	/* Module/library.scm 104 */
					BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_modulez00_25,
						BGl_za2keyza2z00zzmodule_libraryz00, BTRUE);
					return (BGl_za2neededzd2modulesza2zd2zzmodule_libraryz00 =
						MAKE_YOUNG_PAIR(BgL_modulez00_25,
							BGl_za2neededzd2modulesza2zd2zzmodule_libraryz00), BUNSPEC);
				}
		}

	}



/* with-library-module! */
	BGL_EXPORTED_DEF obj_t
		BGl_withzd2libraryzd2modulez12z12zzmodule_libraryz00(obj_t BgL_modulez00_26)
	{
		{	/* Module/library.scm 122 */
			if (CBOOL(BGl_getpropz00zz__r4_symbols_6_4z00(BgL_modulez00_26,
						BGl_za2withzd2keyza2zd2zzmodule_libraryz00)))
				{	/* Module/library.scm 123 */
					return BFALSE;
				}
			else
				{	/* Module/library.scm 123 */
					BGl_putpropz12z12zz__r4_symbols_6_4z00(BgL_modulez00_26,
						BGl_za2withzd2keyza2zd2zzmodule_libraryz00, BTRUE);
					return (BGl_za2withzd2libraryzd2modulesza2z00zzmodule_libraryz00 =
						MAKE_YOUNG_PAIR(BgL_modulez00_26,
							BGl_za2withzd2libraryzd2modulesza2z00zzmodule_libraryz00),
						BUNSPEC);
				}
		}

	}



/* &with-library-module! */
	obj_t BGl_z62withzd2libraryzd2modulez12z70zzmodule_libraryz00(obj_t
		BgL_envz00_913, obj_t BgL_modulez00_914)
	{
		{	/* Module/library.scm 122 */
			return
				BGl_withzd2libraryzd2modulez12z12zzmodule_libraryz00(BgL_modulez00_914);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_libraryz00(void)
	{
		{	/* Module/library.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1316z00zzmodule_libraryz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1316z00zzmodule_libraryz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1316z00zzmodule_libraryz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1316z00zzmodule_libraryz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1316z00zzmodule_libraryz00));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string1316z00zzmodule_libraryz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1316z00zzmodule_libraryz00));
			return
				BGl_modulezd2initializa7ationz75zzbdb_settingz00(444161932L,
				BSTRING_TO_STRING(BGl_string1316z00zzmodule_libraryz00));
		}

	}

#ifdef __cplusplus
}
#endif
