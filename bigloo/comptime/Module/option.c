/*===========================================================================*/
/*   (Module/option.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/option.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_OPTION_TYPE_DEFINITIONS
#define BGL_MODULE_OPTION_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;


#endif													// BGL_MODULE_OPTION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzmodule_optionz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzmodule_optionz00(void);
	BGL_IMPORT obj_t BGl_evalz00zz__evalz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_optionz00(void);
	static obj_t BGl_objectzd2initzd2zzmodule_optionz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_optionz00(void);
	static obj_t BGl_z62zc3z04anonymousza31075ze3ze5zzmodule_optionz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31076ze3ze5zzmodule_optionz00(obj_t);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_optionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzmodule_optionz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_optionz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_optionz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_optionz00(void);
	static obj_t BGl_z62optionzd2producerzb0zzmodule_optionz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2optionzd2compilerz62zzmodule_optionz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2optionzd2compilerz00zzmodule_optionz00(void);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1091z00zzmodule_optionz00,
		BgL_bgl_string1091za700za7za7m1098za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1092z00zzmodule_optionz00,
		BgL_bgl_string1092za700za7za7m1099za7, "Illegal `option' clause", 23);
	      DEFINE_STRING(BGl_string1093z00zzmodule_optionz00,
		BgL_bgl_string1093za700za7za7m1100za7, "module_option", 13);
	      DEFINE_STRING(BGl_string1094z00zzmodule_optionz00,
		BgL_bgl_string1094za700za7za7m1101za7, "void option ", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1089z00zzmodule_optionz00,
		BgL_bgl_za762za7c3za704anonymo1102za7,
		BGl_z62zc3z04anonymousza31075ze3ze5zzmodule_optionz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1090z00zzmodule_optionz00,
		BgL_bgl_za762za7c3za704anonymo1103za7,
		BGl_z62zc3z04anonymousza31076ze3ze5zzmodule_optionz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2optionzd2compilerzd2envzd2zzmodule_optionz00,
		BgL_bgl_za762makeza7d2option1104z00,
		BGl_z62makezd2optionzd2compilerz62zzmodule_optionz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_optionzd2producerzd2envz00zzmodule_optionz00,
		BgL_bgl_za762optionza7d2prod1105z00,
		BGl_z62optionzd2producerzb0zzmodule_optionz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_optionz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_optionz00(long
		BgL_checksumz00_120, char *BgL_fromz00_121)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_optionz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_optionz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_optionz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_optionz00();
					BGl_cnstzd2initzd2zzmodule_optionz00();
					BGl_importedzd2moduleszd2initz00zzmodule_optionz00();
					return BGl_toplevelzd2initzd2zzmodule_optionz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 15 */
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "module_option");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_option");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_option");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_option");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_option");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_option");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_option");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 15 */
			{	/* Module/option.scm 15 */
				obj_t BgL_cportz00_100;

				{	/* Module/option.scm 15 */
					obj_t BgL_stringz00_107;

					BgL_stringz00_107 = BGl_string1094z00zzmodule_optionz00;
					{	/* Module/option.scm 15 */
						obj_t BgL_startz00_108;

						BgL_startz00_108 = BINT(0L);
						{	/* Module/option.scm 15 */
							obj_t BgL_endz00_109;

							BgL_endz00_109 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_107)));
							{	/* Module/option.scm 15 */

								BgL_cportz00_100 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_107, BgL_startz00_108, BgL_endz00_109);
				}}}}
				{
					long BgL_iz00_101;

					BgL_iz00_101 = 1L;
				BgL_loopz00_102:
					if ((BgL_iz00_101 == -1L))
						{	/* Module/option.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/option.scm 15 */
							{	/* Module/option.scm 15 */
								obj_t BgL_arg1097z00_103;

								{	/* Module/option.scm 15 */

									{	/* Module/option.scm 15 */
										obj_t BgL_locationz00_105;

										BgL_locationz00_105 = BBOOL(((bool_t) 0));
										{	/* Module/option.scm 15 */

											BgL_arg1097z00_103 =
												BGl_readz00zz__readerz00(BgL_cportz00_100,
												BgL_locationz00_105);
										}
									}
								}
								{	/* Module/option.scm 15 */
									int BgL_tmpz00_146;

									BgL_tmpz00_146 = (int) (BgL_iz00_101);
									CNST_TABLE_SET(BgL_tmpz00_146, BgL_arg1097z00_103);
							}}
							{	/* Module/option.scm 15 */
								int BgL_auxz00_106;

								BgL_auxz00_106 = (int) ((BgL_iz00_101 - 1L));
								{
									long BgL_iz00_151;

									BgL_iz00_151 = (long) (BgL_auxz00_106);
									BgL_iz00_101 = BgL_iz00_151;
									goto BgL_loopz00_102;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 15 */
			return BUNSPEC;
		}

	}



/* make-option-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2optionzd2compilerz00zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 25 */
			{	/* Module/option.scm 26 */
				BgL_ccompz00_bglt BgL_new1049z00_57;

				{	/* Module/option.scm 27 */
					BgL_ccompz00_bglt BgL_new1048z00_62;

					BgL_new1048z00_62 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/option.scm 27 */
						long BgL_arg1078z00_63;

						BgL_arg1078z00_63 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1048z00_62), BgL_arg1078z00_63);
					}
					BgL_new1049z00_57 = BgL_new1048z00_62;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1049z00_57))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1049z00_57))->BgL_producerz00) =
					((obj_t) BGl_optionzd2producerzd2envz00zzmodule_optionz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1049z00_57))->BgL_consumerz00) =
					((obj_t) BGl_proc1089z00zzmodule_optionz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1049z00_57))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_proc1090z00zzmodule_optionz00), BUNSPEC);
				return ((obj_t) BgL_new1049z00_57);
			}
		}

	}



/* &make-option-compiler */
	obj_t BGl_z62makezd2optionzd2compilerz62zzmodule_optionz00(obj_t
		BgL_envz00_92)
	{
		{	/* Module/option.scm 25 */
			return BGl_makezd2optionzd2compilerz00zzmodule_optionz00();
		}

	}



/* &<@anonymous:1076> */
	obj_t BGl_z62zc3z04anonymousza31076ze3ze5zzmodule_optionz00(obj_t
		BgL_envz00_93)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(1);
		}

	}



/* &<@anonymous:1075> */
	obj_t BGl_z62zc3z04anonymousza31075ze3ze5zzmodule_optionz00(obj_t
		BgL_envz00_94, obj_t BgL_mz00_95, obj_t BgL_cz00_96)
	{
		{	/* Module/module.scm 54 */
			return BNIL;
		}

	}



/* &option-producer */
	obj_t BGl_z62optionzd2producerzb0zzmodule_optionz00(obj_t BgL_envz00_97,
		obj_t BgL_clausez00_98)
	{
		{	/* Module/option.scm 33 */
			{
				obj_t BgL_protosz00_113;

				if (PAIRP(BgL_clausez00_98))
					{	/* Module/option.scm 34 */
						obj_t BgL_arg1080z00_119;

						BgL_arg1080z00_119 = CDR(((obj_t) BgL_clausez00_98));
						BgL_protosz00_113 = BgL_arg1080z00_119;
						{
							obj_t BgL_l1052z00_115;

							BgL_l1052z00_115 = BgL_protosz00_113;
						BgL_zc3z04anonymousza31081ze3z87_114:
							if (PAIRP(BgL_l1052z00_115))
								{	/* Module/option.scm 36 */
									{	/* Module/option.scm 36 */
										obj_t BgL_arg1083z00_116;

										BgL_arg1083z00_116 = CAR(BgL_l1052z00_115);
										{	/* Module/option.scm 36 */
											obj_t BgL_envz00_117;

											BgL_envz00_117 =
												BGl_defaultzd2environmentzd2zz__evalz00();
											{	/* Module/option.scm 36 */

												BGl_evalz00zz__evalz00(BgL_arg1083z00_116,
													BgL_envz00_117);
											}
										}
									}
									{
										obj_t BgL_l1052z00_175;

										BgL_l1052z00_175 = CDR(BgL_l1052z00_115);
										BgL_l1052z00_115 = BgL_l1052z00_175;
										goto BgL_zc3z04anonymousza31081ze3z87_114;
									}
								}
							else
								{	/* Module/option.scm 36 */
									((bool_t) 1);
								}
						}
						return BNIL;
					}
				else
					{	/* Module/option.scm 34 */
						{	/* Module/option.scm 39 */
							obj_t BgL_list1085z00_118;

							BgL_list1085z00_118 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							return
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string1091z00zzmodule_optionz00,
								BGl_string1092z00zzmodule_optionz00, BgL_clausez00_98,
								BgL_list1085z00_118);
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_optionz00(void)
	{
		{	/* Module/option.scm 15 */
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1093z00zzmodule_optionz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1093z00zzmodule_optionz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1093z00zzmodule_optionz00));
		}

	}

#ifdef __cplusplus
}
#endif
