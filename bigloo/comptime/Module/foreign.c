/*===========================================================================*/
/*   (Module/foreign.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/foreign.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_FOREIGN_TYPE_DEFINITIONS
#define BGL_MODULE_FOREIGN_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_MODULE_FOREIGN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_externzd2parserzd2zzmodule_foreignz00(obj_t, bool_t);
	extern obj_t BGl_declarezd2czd2typez12z12zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_foreignz00 = BUNSPEC;
	static obj_t BGl_z62makezd2foreignzd2compilerz62zzmodule_foreignz00(obj_t);
	static obj_t BGl_parsezd2czd2externzd2typezd2zzmodule_foreignz00(obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_foreignzd2parserzd2zzmodule_foreignz00(obj_t, bool_t);
	extern obj_t BGl_caliasz00zzforeign_ctypez00;
	static obj_t BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00 = BUNSPEC;
	static obj_t BGl_z62foreignzd2finaliza7erz17zzmodule_foreignz00(obj_t);
	static obj_t BGl_externzd2producerzd2zzmodule_foreignz00(obj_t, bool_t);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzmodule_foreignz00(void);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static bool_t
		BGl_checkzd2czd2foreignzd2typezd2expzf3zf3zzmodule_foreignz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_foreignz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern BgL_typez00_bglt BGl_declarezd2typez12zc0zztype_envz00(obj_t, obj_t,
		obj_t);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	static obj_t BGl_objectzd2initzd2zzmodule_foreignz00(void);
	extern obj_t BGl_cstructz00zzforeign_ctypez00;
	static obj_t BGl_z62makezd2externzd2compilerz62zzmodule_foreignz00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_za2foreignzd2exportedza2zd2zzmodule_foreignz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2externzd2compilerz00zzmodule_foreignz00(void);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzmodule_foreignz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_foreignz00(void);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_defaultzd2czd2typez00zzmodule_foreignz00(obj_t, obj_t);
	static obj_t BGl_foreignzd2producerzd2zzmodule_foreignz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31165ze3ze5zzmodule_foreignz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31167ze3ze5zzmodule_foreignz00(obj_t,
		obj_t);
	static obj_t
		BGl_czd2externzd2typezd2ze3czd2foreignzd2typez31zzmodule_foreignz00(obj_t);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	static obj_t BGl_z62zc3z04anonymousza31168ze3ze5zzmodule_foreignz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31169ze3ze5zzmodule_foreignz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_foreignz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_checksumz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62foreignzd2producerzb0zzmodule_foreignz00(obj_t, obj_t);
	static obj_t BGl_parsezd2czd2foreignzd2typezd2zzmodule_foreignz00(obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2czd2typezd2zztype_cachez00(void);
	extern obj_t BGl_checkzd2idzd2zzast_identz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzmodule_foreignz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_foreignz00(void);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, bool_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_foreignz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_foreignz00(void);
	extern obj_t BGl_parsezd2idzd2zzast_identz00(obj_t, obj_t);
	static bool_t BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_foreignzd2accesseszd2addz12z12zzmodule_foreignz00(obj_t);
	extern obj_t
		BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00(BgL_typez00_bglt);
	extern obj_t
		BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00(BgL_typez00_bglt,
		BgL_typez00_bglt, obj_t);
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62foreignzd2accesseszd2addz12z70zzmodule_foreignz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2foreignzd2compilerz00zzmodule_foreignz00(void);
	extern obj_t BGl_za2includezd2foreignza2zd2zzengine_paramz00;
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t __cnst[22];


	   
		 
		DEFINE_STRING(BGl_string2300z00zzmodule_foreignz00,
		BgL_bgl_string2300za700za7za7m2309za7, "Illegal `C foreign type'", 24);
	      DEFINE_STRING(BGl_string2301z00zzmodule_foreignz00,
		BgL_bgl_string2301za700za7za7m2310za7, "Illegal `C extern type'", 23);
	      DEFINE_STRING(BGl_string2302z00zzmodule_foreignz00,
		BgL_bgl_string2302za700za7za7m2311za7, "Foreign", 7);
	      DEFINE_STRING(BGl_string2303z00zzmodule_foreignz00,
		BgL_bgl_string2303za700za7za7m2312za7,
		"Re-exportation of global variable (ignored)", 43);
	      DEFINE_STRING(BGl_string2304z00zzmodule_foreignz00,
		BgL_bgl_string2304za700za7za7m2313za7, "Unbound global variable \"~a\"",
		28);
	      DEFINE_STRING(BGl_string2305z00zzmodule_foreignz00,
		BgL_bgl_string2305za700za7za7m2314za7, "Unspecified C type, using type",
		30);
	      DEFINE_STRING(BGl_string2306z00zzmodule_foreignz00,
		BgL_bgl_string2306za700za7za7m2315za7, "module_foreign", 14);
	      DEFINE_STRING(BGl_string2307z00zzmodule_foreignz00,
		BgL_bgl_string2307za700za7za7m2316za7,
		"unit make-heap make-add-heap union* struct* array enum function pointer opaque struct union C * macro export type include infix void extern foreign ",
		148);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_foreignzd2finaliza7erzd2envza7zzmodule_foreignz00,
		BgL_bgl_za762foreignza7d2fin2317z00,
		BGl_z62foreignzd2finaliza7erz17zzmodule_foreignz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2291z00zzmodule_foreignz00,
		BgL_bgl_string2291za700za7za7m2318za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string2292z00zzmodule_foreignz00,
		BgL_bgl_string2292za700za7za7m2319za7, "Illegal `foreign' clause", 24);
	      DEFINE_STRING(BGl_string2293z00zzmodule_foreignz00,
		BgL_bgl_string2293za700za7za7m2320za7, "Illegal `extern' clause", 23);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2287z00zzmodule_foreignz00,
		BgL_bgl_za762za7c3za704anonymo2321za7,
		BGl_z62zc3z04anonymousza31165ze3ze5zzmodule_foreignz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2294z00zzmodule_foreignz00,
		BgL_bgl_string2294za700za7za7m2322za7, "Illegal `include' clause", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2288z00zzmodule_foreignz00,
		BgL_bgl_za762za7c3za704anonymo2323za7,
		BGl_z62zc3z04anonymousza31167ze3ze5zzmodule_foreignz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2295z00zzmodule_foreignz00,
		BgL_bgl_string2295za700za7za7m2324za7, "Illegal foreign form", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2289z00zzmodule_foreignz00,
		BgL_bgl_za762za7c3za704anonymo2325za7,
		BGl_z62zc3z04anonymousza31168ze3ze5zzmodule_foreignz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2296z00zzmodule_foreignz00,
		BgL_bgl_string2296za700za7za7m2326za7, "Illegal `macro' form", 20);
	      DEFINE_STRING(BGl_string2297z00zzmodule_foreignz00,
		BgL_bgl_string2297za700za7za7m2327za7, "Illegal `function' form", 23);
	      DEFINE_STRING(BGl_string2298z00zzmodule_foreignz00,
		BgL_bgl_string2298za700za7za7m2328za7, "Illegal `variable' form", 23);
	      DEFINE_STRING(BGl_string2299z00zzmodule_foreignz00,
		BgL_bgl_string2299za700za7za7m2329za7, "Illegal extern form", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2290z00zzmodule_foreignz00,
		BgL_bgl_za762za7c3za704anonymo2330za7,
		BGl_z62zc3z04anonymousza31169ze3ze5zzmodule_foreignz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2externzd2compilerzd2envzd2zzmodule_foreignz00,
		BgL_bgl_za762makeza7d2extern2331z00,
		BGl_z62makezd2externzd2compilerz62zzmodule_foreignz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2foreignzd2compilerzd2envzd2zzmodule_foreignz00,
		BgL_bgl_za762makeza7d2foreig2332z00,
		BGl_z62makezd2foreignzd2compilerz62zzmodule_foreignz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_foreignzd2accesseszd2addz12zd2envzc0zzmodule_foreignz00,
		BgL_bgl_za762foreignza7d2acc2333z00,
		BGl_z62foreignzd2accesseszd2addz12z70zzmodule_foreignz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_foreignzd2producerzd2envz00zzmodule_foreignz00,
		BgL_bgl_za762foreignza7d2pro2334z00,
		BGl_z62foreignzd2producerzb0zzmodule_foreignz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_foreignz00));
		     ADD_ROOT((void
				*) (&BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00));
		     ADD_ROOT((void
				*) (&BGl_za2foreignzd2exportedza2zd2zzmodule_foreignz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzmodule_foreignz00(long
		BgL_checksumz00_2961, char *BgL_fromz00_2962)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_foreignz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_foreignz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_foreignz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_foreignz00();
					BGl_cnstzd2initzd2zzmodule_foreignz00();
					BGl_importedzd2moduleszd2initz00zzmodule_foreignz00();
					return BGl_toplevelzd2initzd2zzmodule_foreignz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 18 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_foreign");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"module_foreign");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_foreign");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_foreign");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_foreign");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_foreign");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_foreign");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"module_foreign");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_foreign");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"module_foreign");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "module_foreign");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_foreign");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 18 */
			{	/* Module/foreign.scm 18 */
				obj_t BgL_cportz00_2914;

				{	/* Module/foreign.scm 18 */
					obj_t BgL_stringz00_2921;

					BgL_stringz00_2921 = BGl_string2307z00zzmodule_foreignz00;
					{	/* Module/foreign.scm 18 */
						obj_t BgL_startz00_2922;

						BgL_startz00_2922 = BINT(0L);
						{	/* Module/foreign.scm 18 */
							obj_t BgL_endz00_2923;

							BgL_endz00_2923 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2921)));
							{	/* Module/foreign.scm 18 */

								BgL_cportz00_2914 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2921, BgL_startz00_2922, BgL_endz00_2923);
				}}}}
				{
					long BgL_iz00_2915;

					BgL_iz00_2915 = 21L;
				BgL_loopz00_2916:
					if ((BgL_iz00_2915 == -1L))
						{	/* Module/foreign.scm 18 */
							return BUNSPEC;
						}
					else
						{	/* Module/foreign.scm 18 */
							{	/* Module/foreign.scm 18 */
								obj_t BgL_arg2308z00_2917;

								{	/* Module/foreign.scm 18 */

									{	/* Module/foreign.scm 18 */
										obj_t BgL_locationz00_2919;

										BgL_locationz00_2919 = BBOOL(((bool_t) 0));
										{	/* Module/foreign.scm 18 */

											BgL_arg2308z00_2917 =
												BGl_readz00zz__readerz00(BgL_cportz00_2914,
												BgL_locationz00_2919);
										}
									}
								}
								{	/* Module/foreign.scm 18 */
									int BgL_tmpz00_2992;

									BgL_tmpz00_2992 = (int) (BgL_iz00_2915);
									CNST_TABLE_SET(BgL_tmpz00_2992, BgL_arg2308z00_2917);
							}}
							{	/* Module/foreign.scm 18 */
								int BgL_auxz00_2920;

								BgL_auxz00_2920 = (int) ((BgL_iz00_2915 - 1L));
								{
									long BgL_iz00_2997;

									BgL_iz00_2997 = (long) (BgL_auxz00_2920);
									BgL_iz00_2915 = BgL_iz00_2997;
									goto BgL_loopz00_2916;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 18 */
			BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00 = BNIL;
			return (BGl_za2foreignzd2exportedza2zd2zzmodule_foreignz00 =
				BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzmodule_foreignz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1026;

				BgL_headz00_1026 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1027;
					obj_t BgL_tailz00_1028;

					BgL_prevz00_1027 = BgL_headz00_1026;
					BgL_tailz00_1028 = BgL_l1z00_1;
				BgL_loopz00_1029:
					if (PAIRP(BgL_tailz00_1028))
						{
							obj_t BgL_newzd2prevzd2_1031;

							BgL_newzd2prevzd2_1031 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1028), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1027, BgL_newzd2prevzd2_1031);
							{
								obj_t BgL_tailz00_3007;
								obj_t BgL_prevz00_3006;

								BgL_prevz00_3006 = BgL_newzd2prevzd2_1031;
								BgL_tailz00_3007 = CDR(BgL_tailz00_1028);
								BgL_tailz00_1028 = BgL_tailz00_3007;
								BgL_prevz00_1027 = BgL_prevz00_3006;
								goto BgL_loopz00_1029;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1026);
				}
			}
		}

	}



/* make-foreign-compiler */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2foreignzd2compilerz00zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 45 */
			{	/* Module/foreign.scm 46 */
				BgL_ccompz00_bglt BgL_new1074z00_1048;

				{	/* Module/foreign.scm 47 */
					BgL_ccompz00_bglt BgL_new1073z00_1052;

					BgL_new1073z00_1052 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/foreign.scm 47 */
						long BgL_arg1166z00_1053;

						BgL_arg1166z00_1053 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1073z00_1052), BgL_arg1166z00_1053);
					}
					BgL_new1074z00_1048 = BgL_new1073z00_1052;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1074z00_1048))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1074z00_1048))->BgL_producerz00) =
					((obj_t) BGl_foreignzd2producerzd2envz00zzmodule_foreignz00),
					BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1074z00_1048))->BgL_consumerz00) =
					((obj_t) BGl_proc2287z00zzmodule_foreignz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1074z00_1048))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_foreignzd2finaliza7erzd2envza7zzmodule_foreignz00),
					BUNSPEC);
				return ((obj_t) BgL_new1074z00_1048);
			}
		}

	}



/* &make-foreign-compiler */
	obj_t BGl_z62makezd2foreignzd2compilerz62zzmodule_foreignz00(obj_t
		BgL_envz00_2889)
	{
		{	/* Module/foreign.scm 45 */
			return BGl_makezd2foreignzd2compilerz00zzmodule_foreignz00();
		}

	}



/* &<@anonymous:1165> */
	obj_t BGl_z62zc3z04anonymousza31165ze3ze5zzmodule_foreignz00(obj_t
		BgL_envz00_2890, obj_t BgL_mz00_2891, obj_t BgL_cz00_2892)
	{
		{	/* Module/foreign.scm 49 */
			return BGl_foreignzd2producerzd2zzmodule_foreignz00(BgL_cz00_2892);
		}

	}



/* make-extern-compiler */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2externzd2compilerz00zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 55 */
			{	/* Module/foreign.scm 56 */
				BgL_ccompz00_bglt BgL_new1076z00_1054;

				{	/* Module/foreign.scm 57 */
					BgL_ccompz00_bglt BgL_new1075z00_1061;

					BgL_new1075z00_1061 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/foreign.scm 57 */
						long BgL_arg1171z00_1062;

						BgL_arg1171z00_1062 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1075z00_1061), BgL_arg1171z00_1062);
					}
					BgL_new1076z00_1054 = BgL_new1075z00_1061;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1076z00_1054))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1076z00_1054))->BgL_producerz00) =
					((obj_t) BGl_proc2288z00zzmodule_foreignz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1076z00_1054))->BgL_consumerz00) =
					((obj_t) BGl_proc2289z00zzmodule_foreignz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1076z00_1054))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_proc2290z00zzmodule_foreignz00), BUNSPEC);
				return ((obj_t) BgL_new1076z00_1054);
			}
		}

	}



/* &make-extern-compiler */
	obj_t BGl_z62makezd2externzd2compilerz62zzmodule_foreignz00(obj_t
		BgL_envz00_2899)
	{
		{	/* Module/foreign.scm 55 */
			return BGl_makezd2externzd2compilerz00zzmodule_foreignz00();
		}

	}



/* &<@anonymous:1169> */
	obj_t BGl_z62zc3z04anonymousza31169ze3ze5zzmodule_foreignz00(obj_t
		BgL_envz00_2900)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(2);
		}

	}



/* &<@anonymous:1168> */
	obj_t BGl_z62zc3z04anonymousza31168ze3ze5zzmodule_foreignz00(obj_t
		BgL_envz00_2901, obj_t BgL_mz00_2902, obj_t BgL_cz00_2903)
	{
		{	/* Module/foreign.scm 59 */
			return
				BGl_externzd2producerzd2zzmodule_foreignz00(BgL_cz00_2903,
				((bool_t) 0));
		}

	}



/* &<@anonymous:1167> */
	obj_t BGl_z62zc3z04anonymousza31167ze3ze5zzmodule_foreignz00(obj_t
		BgL_envz00_2904, obj_t BgL_cz00_2905)
	{
		{	/* Module/foreign.scm 58 */
			return
				BGl_externzd2producerzd2zzmodule_foreignz00(BgL_cz00_2905,
				((bool_t) 1));
		}

	}



/* foreign-producer */
	obj_t BGl_foreignzd2producerzd2zzmodule_foreignz00(obj_t BgL_clausez00_25)
	{
		{	/* Module/foreign.scm 64 */
			{	/* Module/foreign.scm 65 */
				bool_t BgL_test2338z00_3036;

				{	/* Module/foreign.scm 65 */
					obj_t BgL_arg1193z00_1080;

					{	/* Module/foreign.scm 65 */
						obj_t BgL_arg1194z00_1081;

						BgL_arg1194z00_1081 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_arg1193z00_1080 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1194z00_1081)))->
							BgL_foreignzd2clausezd2supportz00);
					}
					BgL_test2338z00_3036 =
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(0),
							BgL_arg1193z00_1080));
				}
				if (BgL_test2338z00_3036)
					{
						obj_t BgL_protosz00_1066;

						if (PAIRP(BgL_clausez00_25))
							{	/* Module/foreign.scm 66 */
								obj_t BgL_arg1187z00_1071;

								BgL_arg1187z00_1071 = CDR(((obj_t) BgL_clausez00_25));
								BgL_protosz00_1066 = BgL_arg1187z00_1071;
								{
									obj_t BgL_l1139z00_1073;

									BgL_l1139z00_1073 = BgL_protosz00_1066;
								BgL_zc3z04anonymousza31188ze3z87_1074:
									if (PAIRP(BgL_l1139z00_1073))
										{	/* Module/foreign.scm 68 */
											BGl_foreignzd2parserzd2zzmodule_foreignz00(CAR
												(BgL_l1139z00_1073), ((bool_t) 1));
											{
												obj_t BgL_l1139z00_3051;

												BgL_l1139z00_3051 = CDR(BgL_l1139z00_1073);
												BgL_l1139z00_1073 = BgL_l1139z00_3051;
												goto BgL_zc3z04anonymousza31188ze3z87_1074;
											}
										}
									else
										{	/* Module/foreign.scm 68 */
											((bool_t) 1);
										}
								}
								return BNIL;
							}
						else
							{	/* Module/foreign.scm 66 */
								{	/* Module/foreign.scm 71 */
									obj_t BgL_list1191z00_1079;

									BgL_list1191z00_1079 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									return
										BGl_userzd2errorzd2zztools_errorz00
										(BGl_string2291z00zzmodule_foreignz00,
										BGl_string2292z00zzmodule_foreignz00, BgL_clausez00_25,
										BgL_list1191z00_1079);
								}
							}
					}
				else
					{	/* Module/foreign.scm 65 */
						return BNIL;
					}
			}
		}

	}



/* &foreign-producer */
	obj_t BGl_z62foreignzd2producerzb0zzmodule_foreignz00(obj_t BgL_envz00_2893,
		obj_t BgL_clausez00_2894)
	{
		{	/* Module/foreign.scm 64 */
			return BGl_foreignzd2producerzd2zzmodule_foreignz00(BgL_clausez00_2894);
		}

	}



/* extern-producer */
	obj_t BGl_externzd2producerzd2zzmodule_foreignz00(obj_t BgL_clausez00_26,
		bool_t BgL_exportpz00_27)
	{
		{	/* Module/foreign.scm 80 */
			{	/* Module/foreign.scm 81 */
				bool_t BgL_test2341z00_3056;

				{	/* Module/foreign.scm 81 */
					obj_t BgL_arg1206z00_1099;

					{	/* Module/foreign.scm 81 */
						obj_t BgL_arg1208z00_1100;

						BgL_arg1208z00_1100 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_arg1206z00_1099 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1208z00_1100)))->
							BgL_foreignzd2clausezd2supportz00);
					}
					BgL_test2341z00_3056 =
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(1),
							BgL_arg1206z00_1099));
				}
				if (BgL_test2341z00_3056)
					{
						obj_t BgL_protosz00_1085;

						if (PAIRP(BgL_clausez00_26))
							{	/* Module/foreign.scm 82 */
								obj_t BgL_arg1200z00_1090;

								BgL_arg1200z00_1090 = CDR(((obj_t) BgL_clausez00_26));
								BgL_protosz00_1085 = BgL_arg1200z00_1090;
								{
									obj_t BgL_l1141z00_1092;

									BgL_l1141z00_1092 = BgL_protosz00_1085;
								BgL_zc3z04anonymousza31201ze3z87_1093:
									if (PAIRP(BgL_l1141z00_1092))
										{	/* Module/foreign.scm 84 */
											BGl_externzd2parserzd2zzmodule_foreignz00(CAR
												(BgL_l1141z00_1092), BgL_exportpz00_27);
											{
												obj_t BgL_l1141z00_3071;

												BgL_l1141z00_3071 = CDR(BgL_l1141z00_1092);
												BgL_l1141z00_1092 = BgL_l1141z00_3071;
												goto BgL_zc3z04anonymousza31201ze3z87_1093;
											}
										}
									else
										{	/* Module/foreign.scm 84 */
											((bool_t) 1);
										}
								}
								return BNIL;
							}
						else
							{	/* Module/foreign.scm 82 */
								{	/* Module/foreign.scm 87 */
									obj_t BgL_list1204z00_1098;

									BgL_list1204z00_1098 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									return
										BGl_userzd2errorzd2zztools_errorz00
										(BGl_string2291z00zzmodule_foreignz00,
										BGl_string2293z00zzmodule_foreignz00, BgL_clausez00_26,
										BgL_list1204z00_1098);
								}
							}
					}
				else
					{	/* Module/foreign.scm 81 */
						return BNIL;
					}
			}
		}

	}



/* check-c-args? */
	bool_t BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00(obj_t BgL_protoz00_28)
	{
		{	/* Module/foreign.scm 96 */
			{
				obj_t BgL_protoz00_1102;

				BgL_protoz00_1102 = BgL_protoz00_28;
			BgL_zc3z04anonymousza31209ze3z87_1103:
				if (NULLP(BgL_protoz00_1102))
					{	/* Module/foreign.scm 99 */
						return ((bool_t) 1);
					}
				else
					{	/* Module/foreign.scm 99 */
						if (SYMBOLP(BgL_protoz00_1102))
							{	/* Module/foreign.scm 101 */
								return ((bool_t) 1);
							}
						else
							{	/* Module/foreign.scm 101 */
								if (PAIRP(BgL_protoz00_1102))
									{	/* Module/foreign.scm 105 */
										bool_t BgL_test2347z00_3081;

										{	/* Module/foreign.scm 105 */
											obj_t BgL_tmpz00_3082;

											BgL_tmpz00_3082 = CAR(BgL_protoz00_1102);
											BgL_test2347z00_3081 = SYMBOLP(BgL_tmpz00_3082);
										}
										if (BgL_test2347z00_3081)
											{
												obj_t BgL_protoz00_3085;

												BgL_protoz00_3085 = CDR(BgL_protoz00_1102);
												BgL_protoz00_1102 = BgL_protoz00_3085;
												goto BgL_zc3z04anonymousza31209ze3z87_1103;
											}
										else
											{	/* Module/foreign.scm 105 */
												return ((bool_t) 0);
											}
									}
								else
									{	/* Module/foreign.scm 103 */
										return ((bool_t) 0);
									}
							}
					}
			}
		}

	}



/* foreign-parser */
	obj_t BGl_foreignzd2parserzd2zzmodule_foreignz00(obj_t BgL_foreignz00_29,
		bool_t BgL_exportpz00_30)
	{
		{	/* Module/foreign.scm 113 */
			{
				obj_t BgL_typez00_1134;
				obj_t BgL_lzd2namezd2_1135;
				obj_t BgL_czd2namezd2_1136;
				obj_t BgL_typez00_1129;
				obj_t BgL_lzd2namezd2_1130;
				obj_t BgL_protoz00_1131;
				obj_t BgL_czd2namezd2_1132;
				obj_t BgL_typez00_1124;
				obj_t BgL_lzd2namezd2_1125;
				obj_t BgL_czd2namezd2_1126;
				obj_t BgL_typez00_1119;
				obj_t BgL_lzd2namezd2_1120;
				obj_t BgL_protoz00_1121;
				obj_t BgL_czd2namezd2_1122;
				obj_t BgL_stringz00_1112;

				if (PAIRP(BgL_foreignz00_29))
					{	/* Module/foreign.scm 115 */
						obj_t BgL_cdrzd2395zd2_1141;

						BgL_cdrzd2395zd2_1141 = CDR(((obj_t) BgL_foreignz00_29));
						if ((CAR(((obj_t) BgL_foreignz00_29)) == CNST_TABLE_REF(4)))
							{	/* Module/foreign.scm 115 */
								if (PAIRP(BgL_cdrzd2395zd2_1141))
									{	/* Module/foreign.scm 115 */
										if (NULLP(CDR(BgL_cdrzd2395zd2_1141)))
											{	/* Module/foreign.scm 115 */
												BgL_stringz00_1112 = CAR(BgL_cdrzd2395zd2_1141);
												if (STRINGP(BgL_stringz00_1112))
													{	/* Module/foreign.scm 117 */
														if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
																(BgL_stringz00_1112,
																	BGl_za2includezd2foreignza2zd2zzengine_paramz00)))
															{	/* Module/foreign.scm 119 */
																return BFALSE;
															}
														else
															{	/* Module/foreign.scm 119 */
																return
																	(BGl_za2includezd2foreignza2zd2zzengine_paramz00
																	=
																	MAKE_YOUNG_PAIR(BgL_stringz00_1112,
																		BGl_za2includezd2foreignza2zd2zzengine_paramz00),
																	BUNSPEC);
															}
													}
												else
													{	/* Module/foreign.scm 118 */
														obj_t BgL_list1549z00_1278;

														BgL_list1549z00_1278 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														return
															BGl_userzd2errorzd2zztools_errorz00
															(BGl_string2291z00zzmodule_foreignz00,
															BGl_string2294z00zzmodule_foreignz00,
															BgL_foreignz00_29, BgL_list1549z00_1278);
													}
											}
										else
											{	/* Module/foreign.scm 115 */
											BgL_tagzd2378zd2_1118:
												{	/* Module/foreign.scm 128 */
													obj_t BgL_list1553z00_1280;

													BgL_list1553z00_1280 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													return
														BGl_userzd2errorzd2zztools_errorz00
														(BGl_string2291z00zzmodule_foreignz00,
														BGl_string2295z00zzmodule_foreignz00,
														BgL_foreignz00_29, BgL_list1553z00_1280);
												}
											}
									}
								else
									{	/* Module/foreign.scm 115 */
										goto BgL_tagzd2378zd2_1118;
									}
							}
						else
							{	/* Module/foreign.scm 115 */
								if ((CAR(((obj_t) BgL_foreignz00_29)) == CNST_TABLE_REF(5)))
									{	/* Module/foreign.scm 115 */
										return
											BGl_parsezd2czd2foreignzd2typezd2zzmodule_foreignz00
											(BgL_foreignz00_29);
									}
								else
									{	/* Module/foreign.scm 115 */
										if ((CAR(((obj_t) BgL_foreignz00_29)) == CNST_TABLE_REF(6)))
											{	/* Module/foreign.scm 115 */
												if (PAIRP(BgL_cdrzd2395zd2_1141))
													{	/* Module/foreign.scm 115 */
														obj_t BgL_carzd2456zd2_1155;
														obj_t BgL_cdrzd2457zd2_1156;

														BgL_carzd2456zd2_1155 = CAR(BgL_cdrzd2395zd2_1141);
														BgL_cdrzd2457zd2_1156 = CDR(BgL_cdrzd2395zd2_1141);
														if (SYMBOLP(BgL_carzd2456zd2_1155))
															{	/* Module/foreign.scm 115 */
																if (PAIRP(BgL_cdrzd2457zd2_1156))
																	{	/* Module/foreign.scm 115 */
																		obj_t BgL_carzd2462zd2_1159;

																		BgL_carzd2462zd2_1159 =
																			CAR(BgL_cdrzd2457zd2_1156);
																		if (STRINGP(BgL_carzd2462zd2_1159))
																			{	/* Module/foreign.scm 115 */
																				if (NULLP(CDR(BgL_cdrzd2457zd2_1156)))
																					{	/* Module/foreign.scm 125 */
																						obj_t BgL_arg1552z00_2227;

																						BgL_arg1552z00_2227 =
																							MAKE_YOUNG_PAIR(BgL_foreignz00_29,
																							BBOOL(BgL_exportpz00_30));
																						return
																							(BGl_za2foreignzd2exportedza2zd2zzmodule_foreignz00
																							=
																							MAKE_YOUNG_PAIR
																							(BgL_arg1552z00_2227,
																								BGl_za2foreignzd2exportedza2zd2zzmodule_foreignz00),
																							BUNSPEC);
																					}
																				else
																					{	/* Module/foreign.scm 115 */
																						goto BgL_tagzd2378zd2_1118;
																					}
																			}
																		else
																			{	/* Module/foreign.scm 115 */
																				goto BgL_tagzd2378zd2_1118;
																			}
																	}
																else
																	{	/* Module/foreign.scm 115 */
																		goto BgL_tagzd2378zd2_1118;
																	}
															}
														else
															{	/* Module/foreign.scm 115 */
																goto BgL_tagzd2378zd2_1118;
															}
													}
												else
													{	/* Module/foreign.scm 115 */
														goto BgL_tagzd2378zd2_1118;
													}
											}
										else
											{	/* Module/foreign.scm 115 */
												obj_t BgL_cdrzd2509zd2_1164;

												BgL_cdrzd2509zd2_1164 =
													CDR(((obj_t) BgL_foreignz00_29));
												if (
													(CAR(
															((obj_t) BgL_foreignz00_29)) ==
														CNST_TABLE_REF(7)))
													{	/* Module/foreign.scm 115 */
														if (PAIRP(BgL_cdrzd2509zd2_1164))
															{	/* Module/foreign.scm 115 */
																obj_t BgL_cdrzd2515zd2_1168;

																BgL_cdrzd2515zd2_1168 =
																	CDR(BgL_cdrzd2509zd2_1164);
																if (PAIRP(BgL_cdrzd2515zd2_1168))
																	{	/* Module/foreign.scm 115 */
																		obj_t BgL_cdrzd2521zd2_1170;

																		BgL_cdrzd2521zd2_1170 =
																			CDR(BgL_cdrzd2515zd2_1168);
																		if (PAIRP(BgL_cdrzd2521zd2_1170))
																			{	/* Module/foreign.scm 115 */
																				obj_t BgL_cdrzd2526zd2_1172;

																				BgL_cdrzd2526zd2_1172 =
																					CDR(BgL_cdrzd2521zd2_1170);
																				if (PAIRP(BgL_cdrzd2526zd2_1172))
																					{	/* Module/foreign.scm 115 */
																						if (NULLP(CDR
																								(BgL_cdrzd2526zd2_1172)))
																							{	/* Module/foreign.scm 115 */
																								BgL_typez00_1119 =
																									CAR(BgL_cdrzd2509zd2_1164);
																								BgL_lzd2namezd2_1120 =
																									CAR(BgL_cdrzd2515zd2_1168);
																								BgL_protoz00_1121 =
																									CAR(BgL_cdrzd2521zd2_1170);
																								BgL_czd2namezd2_1122 =
																									CAR(BgL_cdrzd2526zd2_1172);
																							BgL_tagzd2379zd2_1123:
																								{	/* Module/foreign.scm 131 */
																									bool_t BgL_test2367z00_3161;

																									if (STRINGP
																										(BgL_czd2namezd2_1122))
																										{	/* Module/foreign.scm 131 */
																											if (SYMBOLP
																												(BgL_typez00_1119))
																												{	/* Module/foreign.scm 132 */
																													if (SYMBOLP
																														(BgL_lzd2namezd2_1120))
																														{	/* Module/foreign.scm 133 */
																															BgL_test2367z00_3161
																																=
																																BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00
																																(BgL_protoz00_1121);
																														}
																													else
																														{	/* Module/foreign.scm 133 */
																															BgL_test2367z00_3161
																																= ((bool_t) 0);
																														}
																												}
																											else
																												{	/* Module/foreign.scm 132 */
																													BgL_test2367z00_3161 =
																														((bool_t) 0);
																												}
																										}
																									else
																										{	/* Module/foreign.scm 131 */
																											BgL_test2367z00_3161 =
																												((bool_t) 0);
																										}
																									if (BgL_test2367z00_3161)
																										{	/* Module/foreign.scm 137 */
																											bool_t
																												BgL_arg1559z00_1285;
																											BgL_arg1559z00_1285 =
																												(CAR(((obj_t)
																														BgL_foreignz00_29))
																												== CNST_TABLE_REF(3));
																											return ((obj_t)
																												BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2
																												(BgL_lzd2namezd2_1120,
																													BFALSE,
																													CNST_TABLE_REF(0),
																													BgL_czd2namezd2_1122,
																													BgL_typez00_1119,
																													BgL_protoz00_1121,
																													BgL_arg1559z00_1285,
																													((bool_t) 1),
																													BgL_foreignz00_29,
																													BgL_foreignz00_29));
																										}
																									else
																										{	/* Module/foreign.scm 135 */
																											obj_t
																												BgL_list1562z00_1287;
																											BgL_list1562z00_1287 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											return
																												BGl_userzd2errorzd2zztools_errorz00
																												(BGl_string2291z00zzmodule_foreignz00,
																												BGl_string2296z00zzmodule_foreignz00,
																												BgL_foreignz00_29,
																												BgL_list1562z00_1287);
																										}
																								}
																							}
																						else
																							{	/* Module/foreign.scm 115 */
																							BgL_tagzd2381zd2_1128:
																								{	/* Module/foreign.scm 145 */
																									obj_t BgL_list1567z00_1297;

																									BgL_list1567z00_1297 =
																										MAKE_YOUNG_PAIR(BNIL, BNIL);
																									return
																										BGl_userzd2errorzd2zztools_errorz00
																										(BGl_string2291z00zzmodule_foreignz00,
																										BGl_string2295z00zzmodule_foreignz00,
																										BgL_foreignz00_29,
																										BgL_list1567z00_1297);
																								}
																							}
																					}
																				else
																					{	/* Module/foreign.scm 115 */
																						obj_t BgL_cdrzd2573zd2_1182;

																						BgL_cdrzd2573zd2_1182 =
																							CDR(
																							((obj_t) BgL_cdrzd2509zd2_1164));
																						{	/* Module/foreign.scm 115 */
																							obj_t BgL_cdrzd2581zd2_1183;

																							BgL_cdrzd2581zd2_1183 =
																								CDR(
																								((obj_t)
																									BgL_cdrzd2573zd2_1182));
																							if (NULLP(CDR(((obj_t)
																											BgL_cdrzd2581zd2_1183))))
																								{	/* Module/foreign.scm 115 */
																									obj_t BgL_arg1308z00_1186;
																									obj_t BgL_arg1310z00_1187;
																									obj_t BgL_arg1311z00_1188;

																									BgL_arg1308z00_1186 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd2509zd2_1164));
																									BgL_arg1310z00_1187 =
																										CAR(((obj_t)
																											BgL_cdrzd2573zd2_1182));
																									BgL_arg1311z00_1188 =
																										CAR(((obj_t)
																											BgL_cdrzd2581zd2_1183));
																									BgL_typez00_1124 =
																										BgL_arg1308z00_1186;
																									BgL_lzd2namezd2_1125 =
																										BgL_arg1310z00_1187;
																									BgL_czd2namezd2_1126 =
																										BgL_arg1311z00_1188;
																									{	/* Module/foreign.scm 139 */
																										bool_t BgL_test2372z00_3198;

																										if (STRINGP
																											(BgL_czd2namezd2_1126))
																											{	/* Module/foreign.scm 139 */
																												if (SYMBOLP
																													(BgL_typez00_1124))
																													{	/* Module/foreign.scm 140 */
																														BgL_test2372z00_3198
																															=
																															SYMBOLP
																															(BgL_lzd2namezd2_1125);
																													}
																												else
																													{	/* Module/foreign.scm 140 */
																														BgL_test2372z00_3198
																															= ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Module/foreign.scm 139 */
																												BgL_test2372z00_3198 =
																													((bool_t) 0);
																											}
																										if (BgL_test2372z00_3198)
																											{	/* Module/foreign.scm 139 */
																												return
																													((obj_t)
																													BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2
																													(BgL_lzd2namezd2_1125,
																														BFALSE,
																														BgL_czd2namezd2_1126,
																														BgL_typez00_1124,
																														((bool_t) 1),
																														BgL_foreignz00_29,
																														BgL_foreignz00_29));
																											}
																										else
																											{	/* Module/foreign.scm 142 */
																												obj_t
																													BgL_list1566z00_1294;
																												BgL_list1566z00_1294 =
																													MAKE_YOUNG_PAIR(BNIL,
																													BNIL);
																												return
																													BGl_userzd2errorzd2zztools_errorz00
																													(BGl_string2291z00zzmodule_foreignz00,
																													BGl_string2296z00zzmodule_foreignz00,
																													BgL_foreignz00_29,
																													BgL_list1566z00_1294);
																											}
																									}
																								}
																							else
																								{	/* Module/foreign.scm 115 */
																									goto BgL_tagzd2381zd2_1128;
																								}
																						}
																					}
																			}
																		else
																			{	/* Module/foreign.scm 115 */
																				goto BgL_tagzd2381zd2_1128;
																			}
																	}
																else
																	{	/* Module/foreign.scm 115 */
																		goto BgL_tagzd2381zd2_1128;
																	}
															}
														else
															{	/* Module/foreign.scm 115 */
																goto BgL_tagzd2381zd2_1128;
															}
													}
												else
													{	/* Module/foreign.scm 115 */
														if (
															(CAR(
																	((obj_t) BgL_foreignz00_29)) ==
																CNST_TABLE_REF(3)))
															{	/* Module/foreign.scm 115 */
																if (PAIRP(BgL_cdrzd2509zd2_1164))
																	{	/* Module/foreign.scm 115 */
																		obj_t BgL_cdrzd2653zd2_1194;

																		BgL_cdrzd2653zd2_1194 =
																			CDR(BgL_cdrzd2509zd2_1164);
																		if (
																			(CAR(BgL_cdrzd2509zd2_1164) ==
																				CNST_TABLE_REF(7)))
																			{	/* Module/foreign.scm 115 */
																				if (PAIRP(BgL_cdrzd2653zd2_1194))
																					{	/* Module/foreign.scm 115 */
																						obj_t BgL_cdrzd2659zd2_1198;

																						BgL_cdrzd2659zd2_1198 =
																							CDR(BgL_cdrzd2653zd2_1194);
																						if (PAIRP(BgL_cdrzd2659zd2_1198))
																							{	/* Module/foreign.scm 115 */
																								obj_t BgL_cdrzd2665zd2_1200;

																								BgL_cdrzd2665zd2_1200 =
																									CDR(BgL_cdrzd2659zd2_1198);
																								if (PAIRP
																									(BgL_cdrzd2665zd2_1200))
																									{	/* Module/foreign.scm 115 */
																										obj_t BgL_cdrzd2670zd2_1202;

																										BgL_cdrzd2670zd2_1202 =
																											CDR
																											(BgL_cdrzd2665zd2_1200);
																										if (PAIRP
																											(BgL_cdrzd2670zd2_1202))
																											{	/* Module/foreign.scm 115 */
																												if (NULLP(CDR
																														(BgL_cdrzd2670zd2_1202)))
																													{
																														obj_t
																															BgL_czd2namezd2_3240;
																														obj_t
																															BgL_protoz00_3238;
																														obj_t
																															BgL_lzd2namezd2_3236;
																														obj_t
																															BgL_typez00_3234;
																														BgL_typez00_3234 =
																															CAR
																															(BgL_cdrzd2653zd2_1194);
																														BgL_lzd2namezd2_3236
																															=
																															CAR
																															(BgL_cdrzd2659zd2_1198);
																														BgL_protoz00_3238 =
																															CAR
																															(BgL_cdrzd2665zd2_1200);
																														BgL_czd2namezd2_3240
																															=
																															CAR
																															(BgL_cdrzd2670zd2_1202);
																														BgL_czd2namezd2_1122
																															=
																															BgL_czd2namezd2_3240;
																														BgL_protoz00_1121 =
																															BgL_protoz00_3238;
																														BgL_lzd2namezd2_1120
																															=
																															BgL_lzd2namezd2_3236;
																														BgL_typez00_1119 =
																															BgL_typez00_3234;
																														goto
																															BgL_tagzd2379zd2_1123;
																													}
																												else
																													{	/* Module/foreign.scm 115 */
																													BgL_tagzd2384zd2_1138:
																														{	/* Module/foreign.scm 161 */
																															obj_t
																																BgL_list1580z00_1312;
																															BgL_list1580z00_1312
																																=
																																MAKE_YOUNG_PAIR
																																(BNIL, BNIL);
																															return
																																BGl_userzd2errorzd2zztools_errorz00
																																(BGl_string2291z00zzmodule_foreignz00,
																																BGl_string2295z00zzmodule_foreignz00,
																																BgL_foreignz00_29,
																																BgL_list1580z00_1312);
																														}
																													}
																											}
																										else
																											{	/* Module/foreign.scm 115 */
																												goto
																													BgL_tagzd2384zd2_1138;
																											}
																									}
																								else
																									{	/* Module/foreign.scm 115 */
																										obj_t BgL_cdrzd2754zd2_1211;

																										BgL_cdrzd2754zd2_1211 =
																											CDR(
																											((obj_t)
																												BgL_foreignz00_29));
																										{	/* Module/foreign.scm 115 */
																											obj_t
																												BgL_cdrzd2762zd2_1212;
																											BgL_cdrzd2762zd2_1212 =
																												CDR(((obj_t)
																													BgL_cdrzd2754zd2_1211));
																											{	/* Module/foreign.scm 115 */
																												obj_t
																													BgL_cdrzd2769zd2_1213;
																												BgL_cdrzd2769zd2_1213 =
																													CDR(((obj_t)
																														BgL_cdrzd2762zd2_1212));
																												if (NULLP(CDR(((obj_t)
																																BgL_cdrzd2769zd2_1213))))
																													{	/* Module/foreign.scm 115 */
																														obj_t
																															BgL_arg1333z00_1216;
																														obj_t
																															BgL_arg1335z00_1217;
																														obj_t
																															BgL_arg1339z00_1218;
																														obj_t
																															BgL_arg1340z00_1219;
																														BgL_arg1333z00_1216
																															=
																															CAR(((obj_t)
																																BgL_foreignz00_29));
																														BgL_arg1335z00_1217
																															=
																															CAR(((obj_t)
																																BgL_cdrzd2754zd2_1211));
																														BgL_arg1339z00_1218
																															=
																															CAR(((obj_t)
																																BgL_cdrzd2762zd2_1212));
																														BgL_arg1340z00_1219
																															=
																															CAR(((obj_t)
																																BgL_cdrzd2769zd2_1213));
																														BgL_typez00_1129 =
																															BgL_arg1333z00_1216;
																														BgL_lzd2namezd2_1130
																															=
																															BgL_arg1335z00_1217;
																														BgL_protoz00_1131 =
																															BgL_arg1339z00_1218;
																														BgL_czd2namezd2_1132
																															=
																															BgL_arg1340z00_1219;
																													BgL_tagzd2382zd2_1133:
																														{	/* Module/foreign.scm 147 */
																															bool_t
																																BgL_test2384z00_3262;
																															if (STRINGP
																																(BgL_czd2namezd2_1132))
																																{	/* Module/foreign.scm 147 */
																																	if (SYMBOLP
																																		(BgL_typez00_1129))
																																		{	/* Module/foreign.scm 148 */
																																			if (SYMBOLP(BgL_lzd2namezd2_1130))
																																				{	/* Module/foreign.scm 149 */
																																					BgL_test2384z00_3262
																																						=
																																						BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00
																																						(BgL_protoz00_1131);
																																				}
																																			else
																																				{	/* Module/foreign.scm 149 */
																																					BgL_test2384z00_3262
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Module/foreign.scm 148 */
																																			BgL_test2384z00_3262
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																															else
																																{	/* Module/foreign.scm 147 */
																																	BgL_test2384z00_3262
																																		=
																																		((bool_t)
																																		0);
																																}
																															if (BgL_test2384z00_3262)
																																{	/* Module/foreign.scm 147 */
																																	return
																																		((obj_t)
																																		BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2
																																		(BgL_lzd2namezd2_1130,
																																			BFALSE,
																																			CNST_TABLE_REF
																																			(0),
																																			BgL_czd2namezd2_1132,
																																			BgL_typez00_1129,
																																			BgL_protoz00_1131,
																																			((bool_t)
																																				0),
																																			((bool_t)
																																				0),
																																			BgL_foreignz00_29,
																																			BgL_foreignz00_29));
																																}
																															else
																																{	/* Module/foreign.scm 151 */
																																	obj_t
																																		BgL_list1575z00_1302;
																																	BgL_list1575z00_1302
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BNIL,
																																		BNIL);
																																	return
																																		BGl_userzd2errorzd2zztools_errorz00
																																		(BGl_string2291z00zzmodule_foreignz00,
																																		BGl_string2297z00zzmodule_foreignz00,
																																		BgL_foreignz00_29,
																																		BgL_list1575z00_1302);
																																}
																														}
																													}
																												else
																													{	/* Module/foreign.scm 115 */
																														goto
																															BgL_tagzd2384zd2_1138;
																													}
																											}
																										}
																									}
																							}
																						else
																							{	/* Module/foreign.scm 115 */
																								obj_t BgL_cdrzd2812zd2_1221;

																								BgL_cdrzd2812zd2_1221 =
																									CDR(
																									((obj_t) BgL_foreignz00_29));
																								{	/* Module/foreign.scm 115 */
																									obj_t BgL_cdrzd2818zd2_1222;

																									BgL_cdrzd2818zd2_1222 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd2812zd2_1221));
																									if (NULLP(CDR(((obj_t)
																													BgL_cdrzd2818zd2_1222))))
																										{	/* Module/foreign.scm 115 */
																											obj_t BgL_arg1346z00_1225;
																											obj_t BgL_arg1348z00_1226;
																											obj_t BgL_arg1349z00_1227;

																											BgL_arg1346z00_1225 =
																												CAR(
																												((obj_t)
																													BgL_foreignz00_29));
																											BgL_arg1348z00_1226 =
																												CAR(((obj_t)
																													BgL_cdrzd2812zd2_1221));
																											BgL_arg1349z00_1227 =
																												CAR(((obj_t)
																													BgL_cdrzd2818zd2_1222));
																											BgL_typez00_1134 =
																												BgL_arg1346z00_1225;
																											BgL_lzd2namezd2_1135 =
																												BgL_arg1348z00_1226;
																											BgL_czd2namezd2_1136 =
																												BgL_arg1349z00_1227;
																										BgL_tagzd2383zd2_1137:
																											{	/* Module/foreign.scm 155 */
																												bool_t
																													BgL_test2389z00_3289;
																												if (STRINGP
																													(BgL_czd2namezd2_1136))
																													{	/* Module/foreign.scm 155 */
																														if (SYMBOLP
																															(BgL_typez00_1134))
																															{	/* Module/foreign.scm 156 */
																																BgL_test2389z00_3289
																																	=
																																	SYMBOLP
																																	(BgL_lzd2namezd2_1135);
																															}
																														else
																															{	/* Module/foreign.scm 156 */
																																BgL_test2389z00_3289
																																	=
																																	((bool_t) 0);
																															}
																													}
																												else
																													{	/* Module/foreign.scm 155 */
																														BgL_test2389z00_3289
																															= ((bool_t) 0);
																													}
																												if (BgL_test2389z00_3289)
																													{	/* Module/foreign.scm 155 */
																														return
																															((obj_t)
																															BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2
																															(BgL_lzd2namezd2_1135,
																																BFALSE,
																																BgL_czd2namezd2_1136,
																																BgL_typez00_1134,
																																((bool_t) 0),
																																BgL_foreignz00_29,
																																BFALSE));
																													}
																												else
																													{	/* Module/foreign.scm 158 */
																														obj_t
																															BgL_list1579z00_1309;
																														BgL_list1579z00_1309
																															=
																															MAKE_YOUNG_PAIR
																															(BNIL, BNIL);
																														return
																															BGl_userzd2errorzd2zztools_errorz00
																															(BGl_string2291z00zzmodule_foreignz00,
																															BGl_string2298z00zzmodule_foreignz00,
																															BgL_foreignz00_29,
																															BgL_list1579z00_1309);
																													}
																											}
																										}
																									else
																										{	/* Module/foreign.scm 115 */
																											goto
																												BgL_tagzd2384zd2_1138;
																										}
																								}
																							}
																					}
																				else
																					{	/* Module/foreign.scm 115 */
																						goto BgL_tagzd2384zd2_1138;
																					}
																			}
																		else
																			{	/* Module/foreign.scm 115 */
																				obj_t BgL_cdrzd2871zd2_1229;

																				BgL_cdrzd2871zd2_1229 =
																					CDR(((obj_t) BgL_foreignz00_29));
																				{	/* Module/foreign.scm 115 */
																					obj_t BgL_cdrzd2877zd2_1230;

																					BgL_cdrzd2877zd2_1230 =
																						CDR(
																						((obj_t) BgL_cdrzd2871zd2_1229));
																					if (PAIRP(BgL_cdrzd2877zd2_1230))
																						{	/* Module/foreign.scm 115 */
																							obj_t BgL_cdrzd2882zd2_1232;

																							BgL_cdrzd2882zd2_1232 =
																								CDR(BgL_cdrzd2877zd2_1230);
																							if (PAIRP(BgL_cdrzd2882zd2_1232))
																								{	/* Module/foreign.scm 115 */
																									if (NULLP(CDR
																											(BgL_cdrzd2882zd2_1232)))
																										{	/* Module/foreign.scm 115 */
																											obj_t BgL_arg1364z00_1236;
																											obj_t BgL_arg1367z00_1237;
																											obj_t BgL_arg1370z00_1238;
																											obj_t BgL_arg1371z00_1239;

																											BgL_arg1364z00_1236 =
																												CAR(
																												((obj_t)
																													BgL_foreignz00_29));
																											BgL_arg1367z00_1237 =
																												CAR(((obj_t)
																													BgL_cdrzd2871zd2_1229));
																											BgL_arg1370z00_1238 =
																												CAR
																												(BgL_cdrzd2877zd2_1230);
																											BgL_arg1371z00_1239 =
																												CAR
																												(BgL_cdrzd2882zd2_1232);
																											{
																												obj_t
																													BgL_czd2namezd2_3320;
																												obj_t BgL_protoz00_3319;
																												obj_t
																													BgL_lzd2namezd2_3318;
																												obj_t BgL_typez00_3317;

																												BgL_typez00_3317 =
																													BgL_arg1364z00_1236;
																												BgL_lzd2namezd2_3318 =
																													BgL_arg1367z00_1237;
																												BgL_protoz00_3319 =
																													BgL_arg1370z00_1238;
																												BgL_czd2namezd2_3320 =
																													BgL_arg1371z00_1239;
																												BgL_czd2namezd2_1132 =
																													BgL_czd2namezd2_3320;
																												BgL_protoz00_1131 =
																													BgL_protoz00_3319;
																												BgL_lzd2namezd2_1130 =
																													BgL_lzd2namezd2_3318;
																												BgL_typez00_1129 =
																													BgL_typez00_3317;
																												goto
																													BgL_tagzd2382zd2_1133;
																											}
																										}
																									else
																										{	/* Module/foreign.scm 115 */
																											goto
																												BgL_tagzd2384zd2_1138;
																										}
																								}
																							else
																								{	/* Module/foreign.scm 115 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd2877zd2_1230))))
																										{	/* Module/foreign.scm 115 */
																											obj_t BgL_arg1408z00_1245;
																											obj_t BgL_arg1410z00_1246;
																											obj_t BgL_arg1421z00_1247;

																											BgL_arg1408z00_1245 =
																												CAR(
																												((obj_t)
																													BgL_foreignz00_29));
																											BgL_arg1410z00_1246 =
																												CAR(((obj_t)
																													BgL_cdrzd2871zd2_1229));
																											BgL_arg1421z00_1247 =
																												CAR(((obj_t)
																													BgL_cdrzd2877zd2_1230));
																											{
																												obj_t
																													BgL_czd2namezd2_3333;
																												obj_t
																													BgL_lzd2namezd2_3332;
																												obj_t BgL_typez00_3331;

																												BgL_typez00_3331 =
																													BgL_arg1408z00_1245;
																												BgL_lzd2namezd2_3332 =
																													BgL_arg1410z00_1246;
																												BgL_czd2namezd2_3333 =
																													BgL_arg1421z00_1247;
																												BgL_czd2namezd2_1136 =
																													BgL_czd2namezd2_3333;
																												BgL_lzd2namezd2_1135 =
																													BgL_lzd2namezd2_3332;
																												BgL_typez00_1134 =
																													BgL_typez00_3331;
																												goto
																													BgL_tagzd2383zd2_1137;
																											}
																										}
																									else
																										{	/* Module/foreign.scm 115 */
																											goto
																												BgL_tagzd2384zd2_1138;
																										}
																								}
																						}
																					else
																						{	/* Module/foreign.scm 115 */
																							goto BgL_tagzd2384zd2_1138;
																						}
																				}
																			}
																	}
																else
																	{	/* Module/foreign.scm 115 */
																		goto BgL_tagzd2384zd2_1138;
																	}
															}
														else
															{	/* Module/foreign.scm 115 */
																obj_t BgL_cdrzd2973zd2_1250;

																BgL_cdrzd2973zd2_1250 =
																	CDR(((obj_t) BgL_foreignz00_29));
																if (PAIRP(BgL_cdrzd2973zd2_1250))
																	{	/* Module/foreign.scm 115 */
																		obj_t BgL_cdrzd2979zd2_1252;

																		BgL_cdrzd2979zd2_1252 =
																			CDR(BgL_cdrzd2973zd2_1250);
																		if (PAIRP(BgL_cdrzd2979zd2_1252))
																			{	/* Module/foreign.scm 115 */
																				obj_t BgL_cdrzd2984zd2_1254;

																				BgL_cdrzd2984zd2_1254 =
																					CDR(BgL_cdrzd2979zd2_1252);
																				if (PAIRP(BgL_cdrzd2984zd2_1254))
																					{	/* Module/foreign.scm 115 */
																						if (NULLP(CDR
																								(BgL_cdrzd2984zd2_1254)))
																							{	/* Module/foreign.scm 115 */
																								obj_t BgL_arg1453z00_1258;
																								obj_t BgL_arg1454z00_1259;
																								obj_t BgL_arg1472z00_1260;
																								obj_t BgL_arg1473z00_1261;

																								BgL_arg1453z00_1258 =
																									CAR(
																									((obj_t) BgL_foreignz00_29));
																								BgL_arg1454z00_1259 =
																									CAR(BgL_cdrzd2973zd2_1250);
																								BgL_arg1472z00_1260 =
																									CAR(BgL_cdrzd2979zd2_1252);
																								BgL_arg1473z00_1261 =
																									CAR(BgL_cdrzd2984zd2_1254);
																								{
																									obj_t BgL_czd2namezd2_3355;
																									obj_t BgL_protoz00_3354;
																									obj_t BgL_lzd2namezd2_3353;
																									obj_t BgL_typez00_3352;

																									BgL_typez00_3352 =
																										BgL_arg1453z00_1258;
																									BgL_lzd2namezd2_3353 =
																										BgL_arg1454z00_1259;
																									BgL_protoz00_3354 =
																										BgL_arg1472z00_1260;
																									BgL_czd2namezd2_3355 =
																										BgL_arg1473z00_1261;
																									BgL_czd2namezd2_1132 =
																										BgL_czd2namezd2_3355;
																									BgL_protoz00_1131 =
																										BgL_protoz00_3354;
																									BgL_lzd2namezd2_1130 =
																										BgL_lzd2namezd2_3353;
																									BgL_typez00_1129 =
																										BgL_typez00_3352;
																									goto BgL_tagzd2382zd2_1133;
																								}
																							}
																						else
																							{	/* Module/foreign.scm 115 */
																								goto BgL_tagzd2384zd2_1138;
																							}
																					}
																				else
																					{	/* Module/foreign.scm 115 */
																						obj_t BgL_cdrzd21017zd2_1264;

																						BgL_cdrzd21017zd2_1264 =
																							CDR(
																							((obj_t) BgL_cdrzd2973zd2_1250));
																						if (NULLP(CDR(
																									((obj_t)
																										BgL_cdrzd21017zd2_1264))))
																							{	/* Module/foreign.scm 115 */
																								obj_t BgL_arg1502z00_1267;
																								obj_t BgL_arg1509z00_1268;
																								obj_t BgL_arg1513z00_1269;

																								BgL_arg1502z00_1267 =
																									CAR(
																									((obj_t) BgL_foreignz00_29));
																								BgL_arg1509z00_1268 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2973zd2_1250));
																								BgL_arg1513z00_1269 =
																									CAR(((obj_t)
																										BgL_cdrzd21017zd2_1264));
																								{
																									obj_t BgL_czd2namezd2_3370;
																									obj_t BgL_lzd2namezd2_3369;
																									obj_t BgL_typez00_3368;

																									BgL_typez00_3368 =
																										BgL_arg1502z00_1267;
																									BgL_lzd2namezd2_3369 =
																										BgL_arg1509z00_1268;
																									BgL_czd2namezd2_3370 =
																										BgL_arg1513z00_1269;
																									BgL_czd2namezd2_1136 =
																										BgL_czd2namezd2_3370;
																									BgL_lzd2namezd2_1135 =
																										BgL_lzd2namezd2_3369;
																									BgL_typez00_1134 =
																										BgL_typez00_3368;
																									goto BgL_tagzd2383zd2_1137;
																								}
																							}
																						else
																							{	/* Module/foreign.scm 115 */
																								goto BgL_tagzd2384zd2_1138;
																							}
																					}
																			}
																		else
																			{	/* Module/foreign.scm 115 */
																				goto BgL_tagzd2384zd2_1138;
																			}
																	}
																else
																	{	/* Module/foreign.scm 115 */
																		goto BgL_tagzd2384zd2_1138;
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Module/foreign.scm 115 */
						goto BgL_tagzd2384zd2_1138;
					}
			}
		}

	}



/* extern-parser */
	obj_t BGl_externzd2parserzd2zzmodule_foreignz00(obj_t BgL_externz00_31,
		bool_t BgL_exportpz00_32)
	{
		{	/* Module/foreign.scm 166 */
			{
				obj_t BgL_idz00_1327;
				obj_t BgL_czd2namezd2_1328;
				obj_t BgL_idz00_1323;
				obj_t BgL_protoz00_1324;
				obj_t BgL_cnz00_1325;
				obj_t BgL_idz00_1319;
				obj_t BgL_czd2namezd2_1320;
				obj_t BgL_idz00_1315;
				obj_t BgL_protoz00_1316;
				obj_t BgL_cnz00_1317;

				if (PAIRP(BgL_externz00_31))
					{	/* Module/foreign.scm 168 */
						if ((CAR(((obj_t) BgL_externz00_31)) == CNST_TABLE_REF(5)))
							{	/* Module/foreign.scm 168 */
								return
									BGl_parsezd2czd2externzd2typezd2zzmodule_foreignz00
									(BgL_externz00_31);
							}
						else
							{	/* Module/foreign.scm 168 */
								obj_t BgL_carzd21120zd2_1335;

								BgL_carzd21120zd2_1335 = CAR(((obj_t) BgL_externz00_31));
								if ((BgL_carzd21120zd2_1335 == CNST_TABLE_REF(6)))
									{	/* Module/foreign.scm 168 */
										BGL_TAIL return
											BGl_foreignzd2parserzd2zzmodule_foreignz00
											(BgL_externz00_31, BgL_exportpz00_32);
									}
								else
									{	/* Module/foreign.scm 168 */
										if ((BgL_carzd21120zd2_1335 == CNST_TABLE_REF(4)))
											{	/* Module/foreign.scm 168 */
												BGL_TAIL return
													BGl_foreignzd2parserzd2zzmodule_foreignz00
													(BgL_externz00_31, BgL_exportpz00_32);
											}
										else
											{	/* Module/foreign.scm 168 */
												obj_t BgL_cdrzd21135zd2_1336;

												BgL_cdrzd21135zd2_1336 =
													CDR(((obj_t) BgL_externz00_31));
												if ((BgL_carzd21120zd2_1335 == CNST_TABLE_REF(7)))
													{	/* Module/foreign.scm 168 */
														if (PAIRP(BgL_cdrzd21135zd2_1336))
															{	/* Module/foreign.scm 168 */
																obj_t BgL_carzd21139zd2_1340;
																obj_t BgL_cdrzd21140zd2_1341;

																BgL_carzd21139zd2_1340 =
																	CAR(BgL_cdrzd21135zd2_1336);
																BgL_cdrzd21140zd2_1341 =
																	CDR(BgL_cdrzd21135zd2_1336);
																if (SYMBOLP(BgL_carzd21139zd2_1340))
																	{	/* Module/foreign.scm 168 */
																		if (PAIRP(BgL_cdrzd21140zd2_1341))
																			{	/* Module/foreign.scm 168 */
																				obj_t BgL_cdrzd21147zd2_1344;

																				BgL_cdrzd21147zd2_1344 =
																					CDR(BgL_cdrzd21140zd2_1341);
																				if (PAIRP(BgL_cdrzd21147zd2_1344))
																					{	/* Module/foreign.scm 168 */
																						obj_t BgL_carzd21150zd2_1346;

																						BgL_carzd21150zd2_1346 =
																							CAR(BgL_cdrzd21147zd2_1344);
																						if (STRINGP(BgL_carzd21150zd2_1346))
																							{	/* Module/foreign.scm 168 */
																								if (NULLP(CDR
																										(BgL_cdrzd21147zd2_1344)))
																									{	/* Module/foreign.scm 168 */
																										BgL_idz00_1315 =
																											BgL_carzd21139zd2_1340;
																										BgL_protoz00_1316 =
																											CAR
																											(BgL_cdrzd21140zd2_1341);
																										BgL_cnz00_1317 =
																											BgL_carzd21150zd2_1346;
																									BgL_tagzd21103zd2_1318:
																										{	/* Module/foreign.scm 177 */
																											obj_t BgL_pidz00_1479;

																											BgL_pidz00_1479 =
																												BGl_parsezd2idzd2zzast_identz00
																												(BgL_idz00_1315,
																												BGl_findzd2locationzd2zztools_locationz00
																												(BgL_externz00_31));
																											{	/* Module/foreign.scm 177 */
																												obj_t BgL_lnz00_1480;

																												BgL_lnz00_1480 =
																													CAR(BgL_pidz00_1479);
																												{	/* Module/foreign.scm 178 */
																													obj_t
																														BgL_typez00_1481;
																													{	/* Module/foreign.scm 179 */
																														obj_t
																															BgL_arg1762z00_1490;
																														BgL_arg1762z00_1490
																															=
																															BGl_defaultzd2czd2typez00zzmodule_foreignz00
																															(CDR
																															(BgL_pidz00_1479),
																															BgL_externz00_31);
																														BgL_typez00_1481 =
																															(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_arg1762z00_1490)))->BgL_idz00);
																													}
																													{	/* Module/foreign.scm 179 */

																														{	/* Module/foreign.scm 180 */
																															bool_t
																																BgL_test2412z00_3418;
																															{	/* Module/foreign.scm 180 */
																																obj_t
																																	BgL_xz00_2909;
																																BgL_xz00_2909 =
																																	BGl_checkzd2idzd2zzast_identz00
																																	(BgL_pidz00_1479,
																																	BgL_externz00_31);
																																((bool_t) 1);
																															}
																															if (BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00(BgL_protoz00_1316))
																																{	/* Module/foreign.scm 181 */
																																	BgL_test2412z00_3418
																																		=
																																		((bool_t)
																																		0);
																																}
																															else
																																{	/* Module/foreign.scm 181 */
																																	BgL_test2412z00_3418
																																		=
																																		((bool_t)
																																		1);
																																}
																															if (BgL_test2412z00_3418)
																																{	/* Module/foreign.scm 182 */
																																	obj_t
																																		BgL_list1755z00_1485;
																																	BgL_list1755z00_1485
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BNIL,
																																		BNIL);
																																	return
																																		BGl_userzd2errorzd2zztools_errorz00
																																		(BGl_string2291z00zzmodule_foreignz00,
																																		BGl_string2299z00zzmodule_foreignz00,
																																		BgL_externz00_31,
																																		BgL_list1755z00_1485);
																																}
																															else
																																{	/* Module/foreign.scm 183 */
																																	bool_t
																																		BgL_infixzf3zf3_1486;
																																	BgL_infixzf3zf3_1486
																																		=
																																		(CAR((
																																				(obj_t)
																																				BgL_externz00_31))
																																		==
																																		CNST_TABLE_REF
																																		(3));
																																	return (
																																		(obj_t)
																																		BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2
																																		(BgL_lnz00_1480,
																																			BFALSE,
																																			CNST_TABLE_REF
																																			(0),
																																			BgL_cnz00_1317,
																																			BgL_typez00_1481,
																																			BgL_protoz00_1316,
																																			BgL_infixzf3zf3_1486,
																																			((bool_t)
																																				1),
																																			BgL_externz00_31,
																																			BFALSE));
																																}
																														}
																													}
																												}
																											}
																										}
																									}
																								else
																									{	/* Module/foreign.scm 168 */
																									BgL_tagzd21105zd2_1322:
																										{	/* Module/foreign.scm 195 */
																											obj_t
																												BgL_list1774z00_1501;
																											BgL_list1774z00_1501 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											return
																												BGl_userzd2errorzd2zztools_errorz00
																												(BGl_string2291z00zzmodule_foreignz00,
																												BGl_string2299z00zzmodule_foreignz00,
																												BgL_externz00_31,
																												BgL_list1774z00_1501);
																										}
																									}
																							}
																						else
																							{	/* Module/foreign.scm 168 */
																								goto BgL_tagzd21105zd2_1322;
																							}
																					}
																				else
																					{	/* Module/foreign.scm 168 */
																						obj_t BgL_cdrzd21197zd2_1352;

																						BgL_cdrzd21197zd2_1352 =
																							CDR(((obj_t) BgL_externz00_31));
																						{	/* Module/foreign.scm 168 */
																							obj_t BgL_carzd21202zd2_1353;
																							obj_t BgL_cdrzd21203zd2_1354;

																							BgL_carzd21202zd2_1353 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd21197zd2_1352));
																							BgL_cdrzd21203zd2_1354 =
																								CDR(((obj_t)
																									BgL_cdrzd21197zd2_1352));
																							if (SYMBOLP
																								(BgL_carzd21202zd2_1353))
																								{	/* Module/foreign.scm 168 */
																									obj_t BgL_carzd21211zd2_1356;

																									BgL_carzd21211zd2_1356 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd21203zd2_1354));
																									if (STRINGP
																										(BgL_carzd21211zd2_1356))
																										{	/* Module/foreign.scm 168 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd21203zd2_1354))))
																												{	/* Module/foreign.scm 168 */
																													BgL_idz00_1319 =
																														BgL_carzd21202zd2_1353;
																													BgL_czd2namezd2_1320 =
																														BgL_carzd21211zd2_1356;
																												BgL_tagzd21104zd2_1321:
																													{	/* Module/foreign.scm 188 */
																														obj_t
																															BgL_pidz00_1493;
																														BgL_pidz00_1493 =
																															BGl_parsezd2idzd2zzast_identz00
																															(BgL_idz00_1319,
																															BGl_findzd2locationzd2zztools_locationz00
																															(BgL_externz00_31));
																														{	/* Module/foreign.scm 188 */
																															obj_t
																																BgL_lzd2namezd2_1494;
																															BgL_lzd2namezd2_1494
																																=
																																CAR
																																(BgL_pidz00_1493);
																															{	/* Module/foreign.scm 189 */
																																obj_t
																																	BgL_typez00_1495;
																																{	/* Module/foreign.scm 190 */
																																	obj_t
																																		BgL_arg1770z00_1498;
																																	BgL_arg1770z00_1498
																																		=
																																		BGl_defaultzd2czd2typez00zzmodule_foreignz00
																																		(CDR
																																		(BgL_pidz00_1493),
																																		BgL_externz00_31);
																																	BgL_typez00_1495
																																		=
																																		(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_arg1770z00_1498)))->BgL_idz00);
																																}
																																{	/* Module/foreign.scm 190 */

																																	{	/* Module/foreign.scm 191 */
																																		obj_t
																																			BgL_xz00_2910;
																																		BgL_xz00_2910
																																			=
																																			BGl_checkzd2idzd2zzast_identz00
																																			(BgL_pidz00_1493,
																																			BgL_externz00_31);
																																		((bool_t)
																																			1);
																																	}
																																	return
																																		((obj_t)
																																		BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2
																																		(BgL_lzd2namezd2_1494,
																																			BFALSE,
																																			BgL_czd2namezd2_1320,
																																			BgL_typez00_1495,
																																			((bool_t)
																																				1),
																																			BgL_externz00_31,
																																			BFALSE));
																																}
																															}
																														}
																													}
																												}
																											else
																												{	/* Module/foreign.scm 168 */
																													goto
																														BgL_tagzd21105zd2_1322;
																												}
																										}
																									else
																										{	/* Module/foreign.scm 168 */
																											goto
																												BgL_tagzd21105zd2_1322;
																										}
																								}
																							else
																								{	/* Module/foreign.scm 168 */
																									goto BgL_tagzd21105zd2_1322;
																								}
																						}
																					}
																			}
																		else
																			{	/* Module/foreign.scm 168 */
																				goto BgL_tagzd21105zd2_1322;
																			}
																	}
																else
																	{	/* Module/foreign.scm 168 */
																		obj_t BgL_cdrzd21254zd2_1361;

																		BgL_cdrzd21254zd2_1361 =
																			CDR(((obj_t) BgL_externz00_31));
																		{	/* Module/foreign.scm 168 */
																			obj_t BgL_carzd21257zd2_1362;
																			obj_t BgL_cdrzd21258zd2_1363;

																			BgL_carzd21257zd2_1362 =
																				CAR(((obj_t) BgL_cdrzd21254zd2_1361));
																			BgL_cdrzd21258zd2_1363 =
																				CDR(((obj_t) BgL_cdrzd21254zd2_1361));
																			if (SYMBOLP(BgL_carzd21257zd2_1362))
																				{	/* Module/foreign.scm 168 */
																					if (PAIRP(BgL_cdrzd21258zd2_1363))
																						{	/* Module/foreign.scm 168 */
																							obj_t BgL_carzd21263zd2_1366;

																							BgL_carzd21263zd2_1366 =
																								CAR(BgL_cdrzd21258zd2_1363);
																							if (STRINGP
																								(BgL_carzd21263zd2_1366))
																								{	/* Module/foreign.scm 168 */
																									if (NULLP(CDR
																											(BgL_cdrzd21258zd2_1363)))
																										{
																											obj_t
																												BgL_czd2namezd2_3477;
																											obj_t BgL_idz00_3476;

																											BgL_idz00_3476 =
																												BgL_carzd21257zd2_1362;
																											BgL_czd2namezd2_3477 =
																												BgL_carzd21263zd2_1366;
																											BgL_czd2namezd2_1320 =
																												BgL_czd2namezd2_3477;
																											BgL_idz00_1319 =
																												BgL_idz00_3476;
																											goto
																												BgL_tagzd21104zd2_1321;
																										}
																									else
																										{	/* Module/foreign.scm 168 */
																											goto
																												BgL_tagzd21105zd2_1322;
																										}
																								}
																							else
																								{	/* Module/foreign.scm 168 */
																									goto BgL_tagzd21105zd2_1322;
																								}
																						}
																					else
																						{	/* Module/foreign.scm 168 */
																							goto BgL_tagzd21105zd2_1322;
																						}
																				}
																			else
																				{	/* Module/foreign.scm 168 */
																					goto BgL_tagzd21105zd2_1322;
																				}
																		}
																	}
															}
														else
															{	/* Module/foreign.scm 168 */
																goto BgL_tagzd21105zd2_1322;
															}
													}
												else
													{	/* Module/foreign.scm 168 */
														obj_t BgL_cdrzd21301zd2_1371;

														BgL_cdrzd21301zd2_1371 =
															CDR(((obj_t) BgL_externz00_31));
														if (
															(CAR(
																	((obj_t) BgL_externz00_31)) ==
																CNST_TABLE_REF(3)))
															{	/* Module/foreign.scm 168 */
																if (PAIRP(BgL_cdrzd21301zd2_1371))
																	{	/* Module/foreign.scm 168 */
																		obj_t BgL_cdrzd21306zd2_1375;

																		BgL_cdrzd21306zd2_1375 =
																			CDR(BgL_cdrzd21301zd2_1371);
																		if (
																			(CAR(BgL_cdrzd21301zd2_1371) ==
																				CNST_TABLE_REF(7)))
																			{	/* Module/foreign.scm 168 */
																				if (PAIRP(BgL_cdrzd21306zd2_1375))
																					{	/* Module/foreign.scm 168 */
																						obj_t BgL_carzd21310zd2_1379;
																						obj_t BgL_cdrzd21311zd2_1380;

																						BgL_carzd21310zd2_1379 =
																							CAR(BgL_cdrzd21306zd2_1375);
																						BgL_cdrzd21311zd2_1380 =
																							CDR(BgL_cdrzd21306zd2_1375);
																						if (SYMBOLP(BgL_carzd21310zd2_1379))
																							{	/* Module/foreign.scm 168 */
																								if (PAIRP
																									(BgL_cdrzd21311zd2_1380))
																									{	/* Module/foreign.scm 168 */
																										obj_t
																											BgL_cdrzd21318zd2_1383;
																										BgL_cdrzd21318zd2_1383 =
																											CDR
																											(BgL_cdrzd21311zd2_1380);
																										if (PAIRP
																											(BgL_cdrzd21318zd2_1383))
																											{	/* Module/foreign.scm 168 */
																												obj_t
																													BgL_carzd21321zd2_1385;
																												BgL_carzd21321zd2_1385 =
																													CAR
																													(BgL_cdrzd21318zd2_1383);
																												if (STRINGP
																													(BgL_carzd21321zd2_1385))
																													{	/* Module/foreign.scm 168 */
																														if (NULLP(CDR
																																(BgL_cdrzd21318zd2_1383)))
																															{
																																obj_t
																																	BgL_cnz00_3512;
																																obj_t
																																	BgL_protoz00_3510;
																																obj_t
																																	BgL_idz00_3509;
																																BgL_idz00_3509 =
																																	BgL_carzd21310zd2_1379;
																																BgL_protoz00_3510
																																	=
																																	CAR
																																	(BgL_cdrzd21311zd2_1380);
																																BgL_cnz00_3512 =
																																	BgL_carzd21321zd2_1385;
																																BgL_cnz00_1317 =
																																	BgL_cnz00_3512;
																																BgL_protoz00_1316
																																	=
																																	BgL_protoz00_3510;
																																BgL_idz00_1315 =
																																	BgL_idz00_3509;
																																goto
																																	BgL_tagzd21103zd2_1318;
																															}
																														else
																															{	/* Module/foreign.scm 168 */
																															BgL_tagzd21108zd2_1330:
																																{	/* Module/foreign.scm 215 */
																																	obj_t
																																		BgL_list1823z00_1522;
																																	BgL_list1823z00_1522
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BNIL,
																																		BNIL);
																																	return
																																		BGl_userzd2errorzd2zztools_errorz00
																																		(BGl_string2291z00zzmodule_foreignz00,
																																		BGl_string2299z00zzmodule_foreignz00,
																																		BgL_externz00_31,
																																		BgL_list1823z00_1522);
																																}
																															}
																													}
																												else
																													{	/* Module/foreign.scm 168 */
																														goto
																															BgL_tagzd21108zd2_1330;
																													}
																											}
																										else
																											{	/* Module/foreign.scm 168 */
																												goto
																													BgL_tagzd21108zd2_1330;
																											}
																									}
																								else
																									{	/* Module/foreign.scm 168 */
																										obj_t
																											BgL_carzd21410zd2_1391;
																										BgL_carzd21410zd2_1391 =
																											CAR(((obj_t)
																												BgL_externz00_31));
																										if (SYMBOLP
																											(BgL_carzd21410zd2_1391))
																											{	/* Module/foreign.scm 168 */
																												obj_t
																													BgL_cdrzd21419zd2_1394;
																												BgL_cdrzd21419zd2_1394 =
																													CDR(((obj_t)
																														BgL_cdrzd21301zd2_1371));
																												{	/* Module/foreign.scm 168 */
																													obj_t
																														BgL_carzd21423zd2_1395;
																													BgL_carzd21423zd2_1395
																														=
																														CAR(((obj_t)
																															BgL_cdrzd21419zd2_1394));
																													if (STRINGP
																														(BgL_carzd21423zd2_1395))
																														{	/* Module/foreign.scm 168 */
																															if (NULLP(CDR(
																																		((obj_t)
																																			BgL_cdrzd21419zd2_1394))))
																																{	/* Module/foreign.scm 168 */
																																	obj_t
																																		BgL_arg1661z00_1399;
																																	BgL_arg1661z00_1399
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21301zd2_1371));
																																	BgL_idz00_1323
																																		=
																																		BgL_carzd21410zd2_1391;
																																	BgL_protoz00_1324
																																		=
																																		BgL_arg1661z00_1399;
																																	BgL_cnz00_1325
																																		=
																																		BgL_carzd21423zd2_1395;
																																BgL_tagzd21106zd2_1326:
																																	{	/* Module/foreign.scm 198 */
																																		obj_t
																																			BgL_pidz00_1502;
																																		BgL_pidz00_1502
																																			=
																																			BGl_parsezd2idzd2zzast_identz00
																																			(BgL_idz00_1323,
																																			BGl_findzd2locationzd2zztools_locationz00
																																			(BgL_externz00_31));
																																		{	/* Module/foreign.scm 198 */
																																			obj_t
																																				BgL_lnz00_1503;
																																			BgL_lnz00_1503
																																				=
																																				CAR
																																				(BgL_pidz00_1502);
																																			{	/* Module/foreign.scm 199 */
																																				obj_t
																																					BgL_typez00_1504;
																																				{	/* Module/foreign.scm 200 */
																																					obj_t
																																						BgL_arg1798z00_1511;
																																					BgL_arg1798z00_1511
																																						=
																																						BGl_defaultzd2czd2typez00zzmodule_foreignz00
																																						(CDR
																																						(BgL_pidz00_1502),
																																						BgL_externz00_31);
																																					BgL_typez00_1504
																																						=
																																						(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_arg1798z00_1511)))->BgL_idz00);
																																				}
																																				{	/* Module/foreign.scm 200 */

																																					{	/* Module/foreign.scm 201 */
																																						bool_t
																																							BgL_test2433z00_3538;
																																						{	/* Module/foreign.scm 201 */
																																							obj_t
																																								BgL_xz00_2911;
																																							BgL_xz00_2911
																																								=
																																								BGl_checkzd2idzd2zzast_identz00
																																								(BgL_pidz00_1502,
																																								BgL_externz00_31);
																																							((bool_t) 1);
																																						}
																																						if (BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00(BgL_protoz00_1324))
																																							{	/* Module/foreign.scm 202 */
																																								BgL_test2433z00_3538
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																						else
																																							{	/* Module/foreign.scm 202 */
																																								BgL_test2433z00_3538
																																									=
																																									(
																																									(bool_t)
																																									1);
																																							}
																																						if (BgL_test2433z00_3538)
																																							{	/* Module/foreign.scm 203 */
																																								obj_t
																																									BgL_list1779z00_1508;
																																								BgL_list1779z00_1508
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BNIL,
																																									BNIL);
																																								return
																																									BGl_userzd2errorzd2zztools_errorz00
																																									(BGl_string2291z00zzmodule_foreignz00,
																																									BGl_string2299z00zzmodule_foreignz00,
																																									BgL_externz00_31,
																																									BgL_list1779z00_1508);
																																							}
																																						else
																																							{	/* Module/foreign.scm 201 */
																																								return
																																									(
																																									(obj_t)
																																									BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2
																																									(BgL_lnz00_1503,
																																										BFALSE,
																																										CNST_TABLE_REF
																																										(0),
																																										BgL_cnz00_1325,
																																										BgL_typez00_1504,
																																										BgL_protoz00_1324,
																																										((bool_t) 0), ((bool_t) 0), BgL_externz00_31, BFALSE));
																																							}
																																					}
																																				}
																																			}
																																		}
																																	}
																																}
																															else
																																{	/* Module/foreign.scm 168 */
																																	goto
																																		BgL_tagzd21108zd2_1330;
																																}
																														}
																													else
																														{	/* Module/foreign.scm 168 */
																															goto
																																BgL_tagzd21108zd2_1330;
																														}
																												}
																											}
																										else
																											{	/* Module/foreign.scm 168 */
																												goto
																													BgL_tagzd21108zd2_1330;
																											}
																									}
																							}
																						else
																							{	/* Module/foreign.scm 168 */
																								obj_t BgL_carzd21459zd2_1401;

																								BgL_carzd21459zd2_1401 =
																									CAR(
																									((obj_t) BgL_externz00_31));
																								if (SYMBOLP
																									(BgL_carzd21459zd2_1401))
																									{	/* Module/foreign.scm 168 */
																										obj_t
																											BgL_cdrzd21467zd2_1404;
																										BgL_cdrzd21467zd2_1404 =
																											CDR(((obj_t)
																												BgL_cdrzd21301zd2_1371));
																										{	/* Module/foreign.scm 168 */
																											obj_t
																												BgL_carzd21470zd2_1405;
																											BgL_carzd21470zd2_1405 =
																												CAR(((obj_t)
																													BgL_cdrzd21467zd2_1404));
																											if (STRINGP
																												(BgL_carzd21470zd2_1405))
																												{	/* Module/foreign.scm 168 */
																													if (NULLP(CDR(
																																((obj_t)
																																	BgL_cdrzd21467zd2_1404))))
																														{	/* Module/foreign.scm 168 */
																															obj_t
																																BgL_arg1678z00_1409;
																															BgL_arg1678z00_1409
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21301zd2_1371));
																															{
																																obj_t
																																	BgL_cnz00_3565;
																																obj_t
																																	BgL_protoz00_3564;
																																obj_t
																																	BgL_idz00_3563;
																																BgL_idz00_3563 =
																																	BgL_carzd21459zd2_1401;
																																BgL_protoz00_3564
																																	=
																																	BgL_arg1678z00_1409;
																																BgL_cnz00_3565 =
																																	BgL_carzd21470zd2_1405;
																																BgL_cnz00_1325 =
																																	BgL_cnz00_3565;
																																BgL_protoz00_1324
																																	=
																																	BgL_protoz00_3564;
																																BgL_idz00_1323 =
																																	BgL_idz00_3563;
																																goto
																																	BgL_tagzd21106zd2_1326;
																															}
																														}
																													else
																														{	/* Module/foreign.scm 168 */
																															goto
																																BgL_tagzd21108zd2_1330;
																														}
																												}
																											else
																												{	/* Module/foreign.scm 168 */
																													goto
																														BgL_tagzd21108zd2_1330;
																												}
																										}
																									}
																								else
																									{	/* Module/foreign.scm 168 */
																										goto BgL_tagzd21108zd2_1330;
																									}
																							}
																					}
																				else
																					{	/* Module/foreign.scm 168 */
																						obj_t BgL_carzd21508zd2_1411;

																						BgL_carzd21508zd2_1411 =
																							CAR(((obj_t) BgL_externz00_31));
																						if (SYMBOLP(BgL_carzd21508zd2_1411))
																							{	/* Module/foreign.scm 168 */
																								obj_t BgL_carzd21514zd2_1414;

																								BgL_carzd21514zd2_1414 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd21301zd2_1371));
																								if (STRINGP
																									(BgL_carzd21514zd2_1414))
																									{	/* Module/foreign.scm 168 */
																										if (NULLP(CDR(
																													((obj_t)
																														BgL_cdrzd21301zd2_1371))))
																											{	/* Module/foreign.scm 168 */
																												BgL_idz00_1327 =
																													BgL_carzd21508zd2_1411;
																												BgL_czd2namezd2_1328 =
																													BgL_carzd21514zd2_1414;
																											BgL_tagzd21107zd2_1329:
																												{	/* Module/foreign.scm 208 */
																													obj_t BgL_pidz00_1514;

																													BgL_pidz00_1514 =
																														BGl_parsezd2idzd2zzast_identz00
																														(BgL_idz00_1327,
																														BGl_findzd2locationzd2zztools_locationz00
																														(BgL_externz00_31));
																													{	/* Module/foreign.scm 208 */
																														obj_t
																															BgL_lzd2namezd2_1515;
																														BgL_lzd2namezd2_1515
																															=
																															CAR
																															(BgL_pidz00_1514);
																														{	/* Module/foreign.scm 209 */
																															obj_t
																																BgL_typez00_1516;
																															{	/* Module/foreign.scm 210 */
																																obj_t
																																	BgL_arg1812z00_1519;
																																BgL_arg1812z00_1519
																																	=
																																	BGl_defaultzd2czd2typez00zzmodule_foreignz00
																																	(CDR
																																	(BgL_pidz00_1514),
																																	BgL_externz00_31);
																																BgL_typez00_1516
																																	=
																																	(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_arg1812z00_1519)))->BgL_idz00);
																															}
																															{	/* Module/foreign.scm 210 */

																																{	/* Module/foreign.scm 211 */
																																	obj_t
																																		BgL_xz00_2912;
																																	BgL_xz00_2912
																																		=
																																		BGl_checkzd2idzd2zzast_identz00
																																		(BgL_pidz00_1514,
																																		BgL_externz00_31);
																																	((bool_t) 1);
																																}
																																return
																																	((obj_t)
																																	BGl_declarezd2globalzd2cvarz12z12zzast_glozd2declzd2
																																	(BgL_lzd2namezd2_1515,
																																		BFALSE,
																																		BgL_czd2namezd2_1328,
																																		BgL_typez00_1516,
																																		((bool_t)
																																			0),
																																		BgL_externz00_31,
																																		BFALSE));
																															}
																														}
																													}
																												}
																											}
																										else
																											{	/* Module/foreign.scm 168 */
																												goto
																													BgL_tagzd21108zd2_1330;
																											}
																									}
																								else
																									{	/* Module/foreign.scm 168 */
																										goto BgL_tagzd21108zd2_1330;
																									}
																							}
																						else
																							{	/* Module/foreign.scm 168 */
																								goto BgL_tagzd21108zd2_1330;
																							}
																					}
																			}
																		else
																			{	/* Module/foreign.scm 168 */
																				obj_t BgL_carzd21536zd2_1419;

																				BgL_carzd21536zd2_1419 =
																					CAR(((obj_t) BgL_externz00_31));
																				if (SYMBOLP(BgL_carzd21536zd2_1419))
																					{	/* Module/foreign.scm 168 */
																						obj_t BgL_cdrzd21544zd2_1422;

																						BgL_cdrzd21544zd2_1422 =
																							CDR(
																							((obj_t) BgL_cdrzd21301zd2_1371));
																						if (PAIRP(BgL_cdrzd21544zd2_1422))
																							{	/* Module/foreign.scm 168 */
																								obj_t BgL_carzd21547zd2_1424;

																								BgL_carzd21547zd2_1424 =
																									CAR(BgL_cdrzd21544zd2_1422);
																								if (STRINGP
																									(BgL_carzd21547zd2_1424))
																									{	/* Module/foreign.scm 168 */
																										if (NULLP(CDR
																												(BgL_cdrzd21544zd2_1422)))
																											{	/* Module/foreign.scm 168 */
																												obj_t
																													BgL_arg1701z00_1428;
																												BgL_arg1701z00_1428 =
																													CAR(((obj_t)
																														BgL_cdrzd21301zd2_1371));
																												{
																													obj_t BgL_cnz00_3606;
																													obj_t
																														BgL_protoz00_3605;
																													obj_t BgL_idz00_3604;

																													BgL_idz00_3604 =
																														BgL_carzd21536zd2_1419;
																													BgL_protoz00_3605 =
																														BgL_arg1701z00_1428;
																													BgL_cnz00_3606 =
																														BgL_carzd21547zd2_1424;
																													BgL_cnz00_1325 =
																														BgL_cnz00_3606;
																													BgL_protoz00_1324 =
																														BgL_protoz00_3605;
																													BgL_idz00_1323 =
																														BgL_idz00_3604;
																													goto
																														BgL_tagzd21106zd2_1326;
																												}
																											}
																										else
																											{	/* Module/foreign.scm 168 */
																												goto
																													BgL_tagzd21108zd2_1330;
																											}
																									}
																								else
																									{	/* Module/foreign.scm 168 */
																										goto BgL_tagzd21108zd2_1330;
																									}
																							}
																						else
																							{	/* Module/foreign.scm 168 */
																								obj_t BgL_cdrzd21568zd2_1431;

																								BgL_cdrzd21568zd2_1431 =
																									CDR(
																									((obj_t) BgL_externz00_31));
																								{	/* Module/foreign.scm 168 */
																									obj_t BgL_carzd21574zd2_1433;

																									BgL_carzd21574zd2_1433 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd21568zd2_1431));
																									if (STRINGP
																										(BgL_carzd21574zd2_1433))
																										{	/* Module/foreign.scm 168 */
																											if (NULLP(CDR(
																														((obj_t)
																															BgL_cdrzd21568zd2_1431))))
																												{
																													obj_t
																														BgL_czd2namezd2_3618;
																													obj_t BgL_idz00_3617;

																													BgL_idz00_3617 =
																														BgL_carzd21536zd2_1419;
																													BgL_czd2namezd2_3618 =
																														BgL_carzd21574zd2_1433;
																													BgL_czd2namezd2_1328 =
																														BgL_czd2namezd2_3618;
																													BgL_idz00_1327 =
																														BgL_idz00_3617;
																													goto
																														BgL_tagzd21107zd2_1329;
																												}
																											else
																												{	/* Module/foreign.scm 168 */
																													goto
																														BgL_tagzd21108zd2_1330;
																												}
																										}
																									else
																										{	/* Module/foreign.scm 168 */
																											goto
																												BgL_tagzd21108zd2_1330;
																										}
																								}
																							}
																					}
																				else
																					{	/* Module/foreign.scm 168 */
																						goto BgL_tagzd21108zd2_1330;
																					}
																			}
																	}
																else
																	{	/* Module/foreign.scm 168 */
																		goto BgL_tagzd21108zd2_1330;
																	}
															}
														else
															{	/* Module/foreign.scm 168 */
																obj_t BgL_carzd21634zd2_1447;

																BgL_carzd21634zd2_1447 =
																	CAR(((obj_t) BgL_externz00_31));
																if (SYMBOLP(BgL_carzd21634zd2_1447))
																	{	/* Module/foreign.scm 168 */
																		if (PAIRP(BgL_cdrzd21301zd2_1371))
																			{	/* Module/foreign.scm 168 */
																				obj_t BgL_cdrzd21642zd2_1451;

																				BgL_cdrzd21642zd2_1451 =
																					CDR(BgL_cdrzd21301zd2_1371);
																				if (PAIRP(BgL_cdrzd21642zd2_1451))
																					{	/* Module/foreign.scm 168 */
																						obj_t BgL_carzd21645zd2_1453;

																						BgL_carzd21645zd2_1453 =
																							CAR(BgL_cdrzd21642zd2_1451);
																						if (STRINGP(BgL_carzd21645zd2_1453))
																							{	/* Module/foreign.scm 168 */
																								if (NULLP(CDR
																										(BgL_cdrzd21642zd2_1451)))
																									{
																										obj_t BgL_cnz00_3637;
																										obj_t BgL_protoz00_3635;
																										obj_t BgL_idz00_3634;

																										BgL_idz00_3634 =
																											BgL_carzd21634zd2_1447;
																										BgL_protoz00_3635 =
																											CAR
																											(BgL_cdrzd21301zd2_1371);
																										BgL_cnz00_3637 =
																											BgL_carzd21645zd2_1453;
																										BgL_cnz00_1325 =
																											BgL_cnz00_3637;
																										BgL_protoz00_1324 =
																											BgL_protoz00_3635;
																										BgL_idz00_1323 =
																											BgL_idz00_3634;
																										goto BgL_tagzd21106zd2_1326;
																									}
																								else
																									{	/* Module/foreign.scm 168 */
																										goto BgL_tagzd21108zd2_1330;
																									}
																							}
																						else
																							{	/* Module/foreign.scm 168 */
																								goto BgL_tagzd21108zd2_1330;
																							}
																					}
																				else
																					{	/* Module/foreign.scm 168 */
																						obj_t BgL_cdrzd21669zd2_1460;

																						BgL_cdrzd21669zd2_1460 =
																							CDR(((obj_t) BgL_externz00_31));
																						{	/* Module/foreign.scm 168 */
																							obj_t BgL_carzd21677zd2_1462;

																							BgL_carzd21677zd2_1462 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd21669zd2_1460));
																							if (STRINGP
																								(BgL_carzd21677zd2_1462))
																								{	/* Module/foreign.scm 168 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd21669zd2_1460))))
																										{
																											obj_t
																												BgL_czd2namezd2_3649;
																											obj_t BgL_idz00_3648;

																											BgL_idz00_3648 =
																												BgL_carzd21634zd2_1447;
																											BgL_czd2namezd2_3649 =
																												BgL_carzd21677zd2_1462;
																											BgL_czd2namezd2_1328 =
																												BgL_czd2namezd2_3649;
																											BgL_idz00_1327 =
																												BgL_idz00_3648;
																											goto
																												BgL_tagzd21107zd2_1329;
																										}
																									else
																										{	/* Module/foreign.scm 168 */
																											goto
																												BgL_tagzd21108zd2_1330;
																										}
																								}
																							else
																								{	/* Module/foreign.scm 168 */
																									goto BgL_tagzd21108zd2_1330;
																								}
																						}
																					}
																			}
																		else
																			{	/* Module/foreign.scm 168 */
																				goto BgL_tagzd21108zd2_1330;
																			}
																	}
																else
																	{	/* Module/foreign.scm 168 */
																		goto BgL_tagzd21108zd2_1330;
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Module/foreign.scm 168 */
						goto BgL_tagzd21108zd2_1330;
					}
			}
		}

	}



/* parse-c-foreign-type */
	obj_t BGl_parsezd2czd2foreignzd2typezd2zzmodule_foreignz00(obj_t
		BgL_typez00_33)
	{
		{	/* Module/foreign.scm 220 */
			{
				obj_t BgL_idz00_1526;
				obj_t BgL_tzd2expzd2_1527;
				obj_t BgL_namez00_1528;

				{	/* Module/foreign.scm 221 */
					obj_t BgL_cdrzd21753zd2_1533;

					BgL_cdrzd21753zd2_1533 = CDR(BgL_typez00_33);
					if ((CAR(BgL_typez00_33) == CNST_TABLE_REF(5)))
						{	/* Module/foreign.scm 221 */
							if (PAIRP(BgL_cdrzd21753zd2_1533))
								{	/* Module/foreign.scm 221 */
									obj_t BgL_carzd21756zd2_1537;
									obj_t BgL_cdrzd21757zd2_1538;

									BgL_carzd21756zd2_1537 = CAR(BgL_cdrzd21753zd2_1533);
									BgL_cdrzd21757zd2_1538 = CDR(BgL_cdrzd21753zd2_1533);
									if (SYMBOLP(BgL_carzd21756zd2_1537))
										{	/* Module/foreign.scm 221 */
											if (PAIRP(BgL_cdrzd21757zd2_1538))
												{	/* Module/foreign.scm 221 */
													obj_t BgL_carzd21762zd2_1541;

													BgL_carzd21762zd2_1541 = CAR(BgL_cdrzd21757zd2_1538);
													if (STRINGP(BgL_carzd21762zd2_1541))
														{	/* Module/foreign.scm 221 */
															if (NULLP(CDR(BgL_cdrzd21757zd2_1538)))
																{	/* Module/foreign.scm 221 */
																	return
																		((obj_t)
																		BGl_declarezd2typez12zc0zztype_envz00
																		(BgL_carzd21756zd2_1537,
																			BgL_carzd21762zd2_1541,
																			CNST_TABLE_REF(9)));
																}
															else
																{	/* Module/foreign.scm 221 */
																	obj_t BgL_carzd21782zd2_1546;
																	obj_t BgL_cdrzd21783zd2_1547;

																	BgL_carzd21782zd2_1546 =
																		CAR(((obj_t) BgL_cdrzd21753zd2_1533));
																	BgL_cdrzd21783zd2_1547 =
																		CDR(((obj_t) BgL_cdrzd21753zd2_1533));
																	if (SYMBOLP(BgL_carzd21782zd2_1546))
																		{	/* Module/foreign.scm 221 */
																			obj_t BgL_cdrzd21793zd2_1549;

																			BgL_cdrzd21793zd2_1549 =
																				CDR(((obj_t) BgL_cdrzd21783zd2_1547));
																			if (PAIRP(BgL_cdrzd21793zd2_1549))
																				{	/* Module/foreign.scm 221 */
																					obj_t BgL_carzd21797zd2_1551;

																					BgL_carzd21797zd2_1551 =
																						CAR(BgL_cdrzd21793zd2_1549);
																					if (STRINGP(BgL_carzd21797zd2_1551))
																						{	/* Module/foreign.scm 221 */
																							if (NULLP(CDR
																									(BgL_cdrzd21793zd2_1549)))
																								{	/* Module/foreign.scm 221 */
																									obj_t BgL_arg1844z00_1555;

																									BgL_arg1844z00_1555 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd21783zd2_1547));
																									BgL_idz00_1526 =
																										BgL_carzd21782zd2_1546;
																									BgL_tzd2expzd2_1527 =
																										BgL_arg1844z00_1555;
																									BgL_namez00_1528 =
																										BgL_carzd21797zd2_1551;
																								BgL_tagzd21744zd2_1529:
																									if (BGl_checkzd2czd2foreignzd2typezd2expzf3zf3zzmodule_foreignz00(BgL_tzd2expzd2_1527))
																										{	/* Module/foreign.scm 226 */
																											obj_t BgL_ctypez00_1585;
																											obj_t BgL_locz00_1586;

																											BgL_ctypez00_1585 =
																												BGl_declarezd2czd2typez12z12zzforeign_ctypez00
																												(BgL_typez00_33,
																												BgL_idz00_1526,
																												BgL_tzd2expzd2_1527,
																												BgL_namez00_1528);
																											BgL_locz00_1586 =
																												BGl_findzd2locationzd2zztools_locationz00
																												(BgL_typez00_33);
																											{	/* Module/foreign.scm 230 */
																												bool_t
																													BgL_test2465z00_3694;
																												{	/* Module/foreign.scm 230 */
																													obj_t
																														BgL_classz00_2386;
																													BgL_classz00_2386 =
																														BGl_typez00zztype_typez00;
																													if (BGL_OBJECTP
																														(BgL_ctypez00_1585))
																														{	/* Module/foreign.scm 230 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_2388;
																															BgL_arg1807z00_2388
																																=
																																(BgL_objectz00_bglt)
																																(BgL_ctypez00_1585);
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Module/foreign.scm 230 */
																																	long
																																		BgL_idxz00_2394;
																																	BgL_idxz00_2394
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_2388);
																																	BgL_test2465z00_3694
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_2394
																																				+
																																				1L)) ==
																																		BgL_classz00_2386);
																																}
																															else
																																{	/* Module/foreign.scm 230 */
																																	bool_t
																																		BgL_res2278z00_2419;
																																	{	/* Module/foreign.scm 230 */
																																		obj_t
																																			BgL_oclassz00_2402;
																																		{	/* Module/foreign.scm 230 */
																																			obj_t
																																				BgL_arg1815z00_2410;
																																			long
																																				BgL_arg1816z00_2411;
																																			BgL_arg1815z00_2410
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Module/foreign.scm 230 */
																																				long
																																					BgL_arg1817z00_2412;
																																				BgL_arg1817z00_2412
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_2388);
																																				BgL_arg1816z00_2411
																																					=
																																					(BgL_arg1817z00_2412
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_2402
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_2410,
																																				BgL_arg1816z00_2411);
																																		}
																																		{	/* Module/foreign.scm 230 */
																																			bool_t
																																				BgL__ortest_1115z00_2403;
																																			BgL__ortest_1115z00_2403
																																				=
																																				(BgL_classz00_2386
																																				==
																																				BgL_oclassz00_2402);
																																			if (BgL__ortest_1115z00_2403)
																																				{	/* Module/foreign.scm 230 */
																																					BgL_res2278z00_2419
																																						=
																																						BgL__ortest_1115z00_2403;
																																				}
																																			else
																																				{	/* Module/foreign.scm 230 */
																																					long
																																						BgL_odepthz00_2404;
																																					{	/* Module/foreign.scm 230 */
																																						obj_t
																																							BgL_arg1804z00_2405;
																																						BgL_arg1804z00_2405
																																							=
																																							(BgL_oclassz00_2402);
																																						BgL_odepthz00_2404
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_2405);
																																					}
																																					if (
																																						(1L
																																							<
																																							BgL_odepthz00_2404))
																																						{	/* Module/foreign.scm 230 */
																																							obj_t
																																								BgL_arg1802z00_2407;
																																							{	/* Module/foreign.scm 230 */
																																								obj_t
																																									BgL_arg1803z00_2408;
																																								BgL_arg1803z00_2408
																																									=
																																									(BgL_oclassz00_2402);
																																								BgL_arg1802z00_2407
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_2408,
																																									1L);
																																							}
																																							BgL_res2278z00_2419
																																								=
																																								(BgL_arg1802z00_2407
																																								==
																																								BgL_classz00_2386);
																																						}
																																					else
																																						{	/* Module/foreign.scm 230 */
																																							BgL_res2278z00_2419
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test2465z00_3694
																																		=
																																		BgL_res2278z00_2419;
																																}
																														}
																													else
																														{	/* Module/foreign.scm 230 */
																															BgL_test2465z00_3694
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test2465z00_3694)
																													{	/* Module/foreign.scm 231 */
																														obj_t
																															BgL_accessesz00_1588;
																														BgL_accessesz00_1588
																															=
																															BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00
																															(((BgL_typez00_bglt) BgL_ctypez00_1585), ((BgL_typez00_bglt) BgL_ctypez00_1585), BgL_locz00_1586);
																														BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00
																															=
																															BGl_appendzd221011zd2zzmodule_foreignz00
																															(((obj_t)
																																BgL_accessesz00_1588),
																															BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00);
																														{	/* Module/foreign.scm 236 */
																															bool_t
																																BgL_test2470z00_3722;
																															{	/* Module/foreign.scm 236 */
																																bool_t
																																	BgL_test2471z00_3723;
																																{	/* Module/foreign.scm 236 */
																																	obj_t
																																		BgL_classz00_2421;
																																	BgL_classz00_2421
																																		=
																																		BGl_caliasz00zzforeign_ctypez00;
																																	if (BGL_OBJECTP(BgL_ctypez00_1585))
																																		{	/* Module/foreign.scm 236 */
																																			BgL_objectz00_bglt
																																				BgL_arg1807z00_2423;
																																			BgL_arg1807z00_2423
																																				=
																																				(BgL_objectz00_bglt)
																																				(BgL_ctypez00_1585);
																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																				{	/* Module/foreign.scm 236 */
																																					long
																																						BgL_idxz00_2429;
																																					BgL_idxz00_2429
																																						=
																																						BGL_OBJECT_INHERITANCE_NUM
																																						(BgL_arg1807z00_2423);
																																					BgL_test2471z00_3723
																																						=
																																						(VECTOR_REF
																																						(BGl_za2inheritancesza2z00zz__objectz00,
																																							(BgL_idxz00_2429
																																								+
																																								2L))
																																						==
																																						BgL_classz00_2421);
																																				}
																																			else
																																				{	/* Module/foreign.scm 236 */
																																					bool_t
																																						BgL_res2279z00_2454;
																																					{	/* Module/foreign.scm 236 */
																																						obj_t
																																							BgL_oclassz00_2437;
																																						{	/* Module/foreign.scm 236 */
																																							obj_t
																																								BgL_arg1815z00_2445;
																																							long
																																								BgL_arg1816z00_2446;
																																							BgL_arg1815z00_2445
																																								=
																																								(BGl_za2classesza2z00zz__objectz00);
																																							{	/* Module/foreign.scm 236 */
																																								long
																																									BgL_arg1817z00_2447;
																																								BgL_arg1817z00_2447
																																									=
																																									BGL_OBJECT_CLASS_NUM
																																									(BgL_arg1807z00_2423);
																																								BgL_arg1816z00_2446
																																									=
																																									(BgL_arg1817z00_2447
																																									-
																																									OBJECT_TYPE);
																																							}
																																							BgL_oclassz00_2437
																																								=
																																								VECTOR_REF
																																								(BgL_arg1815z00_2445,
																																								BgL_arg1816z00_2446);
																																						}
																																						{	/* Module/foreign.scm 236 */
																																							bool_t
																																								BgL__ortest_1115z00_2438;
																																							BgL__ortest_1115z00_2438
																																								=
																																								(BgL_classz00_2421
																																								==
																																								BgL_oclassz00_2437);
																																							if (BgL__ortest_1115z00_2438)
																																								{	/* Module/foreign.scm 236 */
																																									BgL_res2279z00_2454
																																										=
																																										BgL__ortest_1115z00_2438;
																																								}
																																							else
																																								{	/* Module/foreign.scm 236 */
																																									long
																																										BgL_odepthz00_2439;
																																									{	/* Module/foreign.scm 236 */
																																										obj_t
																																											BgL_arg1804z00_2440;
																																										BgL_arg1804z00_2440
																																											=
																																											(BgL_oclassz00_2437);
																																										BgL_odepthz00_2439
																																											=
																																											BGL_CLASS_DEPTH
																																											(BgL_arg1804z00_2440);
																																									}
																																									if ((2L < BgL_odepthz00_2439))
																																										{	/* Module/foreign.scm 236 */
																																											obj_t
																																												BgL_arg1802z00_2442;
																																											{	/* Module/foreign.scm 236 */
																																												obj_t
																																													BgL_arg1803z00_2443;
																																												BgL_arg1803z00_2443
																																													=
																																													(BgL_oclassz00_2437);
																																												BgL_arg1802z00_2442
																																													=
																																													BGL_CLASS_ANCESTORS_REF
																																													(BgL_arg1803z00_2443,
																																													2L);
																																											}
																																											BgL_res2279z00_2454
																																												=
																																												(BgL_arg1802z00_2442
																																												==
																																												BgL_classz00_2421);
																																										}
																																									else
																																										{	/* Module/foreign.scm 236 */
																																											BgL_res2279z00_2454
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																					BgL_test2471z00_3723
																																						=
																																						BgL_res2279z00_2454;
																																				}
																																		}
																																	else
																																		{	/* Module/foreign.scm 236 */
																																			BgL_test2471z00_3723
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																																if (BgL_test2471z00_3723)
																																	{	/* Module/foreign.scm 236 */
																																		obj_t
																																			BgL_arg1885z00_1606;
																																		BgL_arg1885z00_1606
																																			=
																																			(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_ctypez00_1585)))->BgL_aliasz00);
																																		{	/* Module/foreign.scm 236 */
																																			obj_t
																																				BgL_classz00_2456;
																																			BgL_classz00_2456
																																				=
																																				BGl_cstructz00zzforeign_ctypez00;
																																			if (BGL_OBJECTP(BgL_arg1885z00_1606))
																																				{	/* Module/foreign.scm 236 */
																																					BgL_objectz00_bglt
																																						BgL_arg1807z00_2458;
																																					BgL_arg1807z00_2458
																																						=
																																						(BgL_objectz00_bglt)
																																						(BgL_arg1885z00_1606);
																																					if (BGL_CONDEXPAND_ISA_ARCH64())
																																						{	/* Module/foreign.scm 236 */
																																							long
																																								BgL_idxz00_2464;
																																							BgL_idxz00_2464
																																								=
																																								BGL_OBJECT_INHERITANCE_NUM
																																								(BgL_arg1807z00_2458);
																																							BgL_test2470z00_3722
																																								=
																																								(VECTOR_REF
																																								(BGl_za2inheritancesza2z00zz__objectz00,
																																									(BgL_idxz00_2464
																																										+
																																										2L))
																																								==
																																								BgL_classz00_2456);
																																						}
																																					else
																																						{	/* Module/foreign.scm 236 */
																																							bool_t
																																								BgL_res2280z00_2489;
																																							{	/* Module/foreign.scm 236 */
																																								obj_t
																																									BgL_oclassz00_2472;
																																								{	/* Module/foreign.scm 236 */
																																									obj_t
																																										BgL_arg1815z00_2480;
																																									long
																																										BgL_arg1816z00_2481;
																																									BgL_arg1815z00_2480
																																										=
																																										(BGl_za2classesza2z00zz__objectz00);
																																									{	/* Module/foreign.scm 236 */
																																										long
																																											BgL_arg1817z00_2482;
																																										BgL_arg1817z00_2482
																																											=
																																											BGL_OBJECT_CLASS_NUM
																																											(BgL_arg1807z00_2458);
																																										BgL_arg1816z00_2481
																																											=
																																											(BgL_arg1817z00_2482
																																											-
																																											OBJECT_TYPE);
																																									}
																																									BgL_oclassz00_2472
																																										=
																																										VECTOR_REF
																																										(BgL_arg1815z00_2480,
																																										BgL_arg1816z00_2481);
																																								}
																																								{	/* Module/foreign.scm 236 */
																																									bool_t
																																										BgL__ortest_1115z00_2473;
																																									BgL__ortest_1115z00_2473
																																										=
																																										(BgL_classz00_2456
																																										==
																																										BgL_oclassz00_2472);
																																									if (BgL__ortest_1115z00_2473)
																																										{	/* Module/foreign.scm 236 */
																																											BgL_res2280z00_2489
																																												=
																																												BgL__ortest_1115z00_2473;
																																										}
																																									else
																																										{	/* Module/foreign.scm 236 */
																																											long
																																												BgL_odepthz00_2474;
																																											{	/* Module/foreign.scm 236 */
																																												obj_t
																																													BgL_arg1804z00_2475;
																																												BgL_arg1804z00_2475
																																													=
																																													(BgL_oclassz00_2472);
																																												BgL_odepthz00_2474
																																													=
																																													BGL_CLASS_DEPTH
																																													(BgL_arg1804z00_2475);
																																											}
																																											if ((2L < BgL_odepthz00_2474))
																																												{	/* Module/foreign.scm 236 */
																																													obj_t
																																														BgL_arg1802z00_2477;
																																													{	/* Module/foreign.scm 236 */
																																														obj_t
																																															BgL_arg1803z00_2478;
																																														BgL_arg1803z00_2478
																																															=
																																															(BgL_oclassz00_2472);
																																														BgL_arg1802z00_2477
																																															=
																																															BGL_CLASS_ANCESTORS_REF
																																															(BgL_arg1803z00_2478,
																																															2L);
																																													}
																																													BgL_res2280z00_2489
																																														=
																																														(BgL_arg1802z00_2477
																																														==
																																														BgL_classz00_2456);
																																												}
																																											else
																																												{	/* Module/foreign.scm 236 */
																																													BgL_res2280z00_2489
																																														=
																																														(
																																														(bool_t)
																																														0);
																																												}
																																										}
																																								}
																																							}
																																							BgL_test2470z00_3722
																																								=
																																								BgL_res2280z00_2489;
																																						}
																																				}
																																			else
																																				{	/* Module/foreign.scm 236 */
																																					BgL_test2470z00_3722
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	}
																																else
																																	{	/* Module/foreign.scm 236 */
																																		BgL_test2470z00_3722
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																															if (BgL_test2470z00_3722)
																																{	/* Module/foreign.scm 238 */
																																	obj_t
																																		BgL_arg1870z00_1592;
																																	{	/* Module/foreign.scm 238 */
																																		obj_t
																																			BgL_arg1872z00_1593;
																																		{	/* Module/foreign.scm 238 */
																																			obj_t
																																				BgL_arg1873z00_1594;
																																			obj_t
																																				BgL_arg1874z00_1595;
																																			{	/* Module/foreign.scm 238 */
																																				obj_t
																																					BgL_arg1875z00_1596;
																																				{	/* Module/foreign.scm 238 */
																																					obj_t
																																						BgL_arg1876z00_1597;
																																					obj_t
																																						BgL_arg1877z00_1598;
																																					{	/* Module/foreign.scm 238 */
																																						obj_t
																																							BgL_arg1455z00_2491;
																																						BgL_arg1455z00_2491
																																							=
																																							SYMBOL_TO_STRING
																																							(BgL_idz00_1526);
																																						BgL_arg1876z00_1597
																																							=
																																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																							(BgL_arg1455z00_2491);
																																					}
																																					{	/* Module/foreign.scm 238 */
																																						obj_t
																																							BgL_symbolz00_2492;
																																						BgL_symbolz00_2492
																																							=
																																							CNST_TABLE_REF
																																							(8);
																																						{	/* Module/foreign.scm 238 */
																																							obj_t
																																								BgL_arg1455z00_2493;
																																							BgL_arg1455z00_2493
																																								=
																																								SYMBOL_TO_STRING
																																								(BgL_symbolz00_2492);
																																							BgL_arg1877z00_1598
																																								=
																																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																								(BgL_arg1455z00_2493);
																																						}
																																					}
																																					BgL_arg1875z00_1596
																																						=
																																						string_append
																																						(BgL_arg1876z00_1597,
																																						BgL_arg1877z00_1598);
																																				}
																																				BgL_arg1873z00_1594
																																					=
																																					bstring_to_symbol
																																					(BgL_arg1875z00_1596);
																																			}
																																			{	/* Module/foreign.scm 239 */
																																				obj_t
																																					BgL_arg1878z00_1599;
																																				obj_t
																																					BgL_arg1879z00_1600;
																																				{	/* Module/foreign.scm 239 */
																																					obj_t
																																						BgL_arg1880z00_1601;
																																					{	/* Module/foreign.scm 239 */
																																						obj_t
																																							BgL_arg1882z00_1602;
																																						obj_t
																																							BgL_arg1883z00_1603;
																																						{	/* Module/foreign.scm 239 */
																																							obj_t
																																								BgL_arg1455z00_2496;
																																							BgL_arg1455z00_2496
																																								=
																																								SYMBOL_TO_STRING
																																								(
																																								((obj_t) BgL_tzd2expzd2_1527));
																																							BgL_arg1882z00_1602
																																								=
																																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																								(BgL_arg1455z00_2496);
																																						}
																																						{	/* Module/foreign.scm 239 */
																																							obj_t
																																								BgL_symbolz00_2497;
																																							BgL_symbolz00_2497
																																								=
																																								CNST_TABLE_REF
																																								(8);
																																							{	/* Module/foreign.scm 239 */
																																								obj_t
																																									BgL_arg1455z00_2498;
																																								BgL_arg1455z00_2498
																																									=
																																									SYMBOL_TO_STRING
																																									(BgL_symbolz00_2497);
																																								BgL_arg1883z00_1603
																																									=
																																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																									(BgL_arg1455z00_2498);
																																							}
																																						}
																																						BgL_arg1880z00_1601
																																							=
																																							string_append
																																							(BgL_arg1882z00_1602,
																																							BgL_arg1883z00_1603);
																																					}
																																					BgL_arg1878z00_1599
																																						=
																																						bstring_to_symbol
																																						(BgL_arg1880z00_1601);
																																				}
																																				{	/* Module/foreign.scm 240 */
																																					obj_t
																																						BgL_arg1884z00_1604;
																																					BgL_arg1884z00_1604
																																						=
																																						BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00
																																						(((BgL_typez00_bglt) BgL_ctypez00_1585));
																																					BgL_arg1879z00_1600
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_arg1884z00_1604,
																																						BNIL);
																																				}
																																				BgL_arg1874z00_1595
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_arg1878z00_1599,
																																					BgL_arg1879z00_1600);
																																			}
																																			BgL_arg1872z00_1593
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1873z00_1594,
																																				BgL_arg1874z00_1595);
																																		}
																																		BgL_arg1870z00_1592
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(5),
																																			BgL_arg1872z00_1593);
																																	}
																																	BGl_parsezd2czd2foreignzd2typezd2zzmodule_foreignz00
																																		(BgL_arg1870z00_1592);
																																}
																															else
																																{	/* Module/foreign.scm 236 */
																																	BFALSE;
																																}
																														}
																													}
																												else
																													{	/* Module/foreign.scm 230 */
																														BFALSE;
																													}
																											}
																											return BUNSPEC;
																										}
																									else
																										{	/* Module/foreign.scm 242 */
																											obj_t
																												BgL_list1886z00_1607;
																											BgL_list1886z00_1607 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											return
																												BGl_userzd2errorzd2zztools_errorz00
																												(BGl_string2291z00zzmodule_foreignz00,
																												BGl_string2300z00zzmodule_foreignz00,
																												BgL_typez00_33,
																												BgL_list1886z00_1607);
																										}
																								}
																							else
																								{	/* Module/foreign.scm 221 */
																								BgL_tagzd21745zd2_1530:
																									{	/* Module/foreign.scm 244 */
																										obj_t BgL_list1887z00_1608;

																										BgL_list1887z00_1608 =
																											MAKE_YOUNG_PAIR(BNIL,
																											BNIL);
																										return
																											BGl_userzd2errorzd2zztools_errorz00
																											(BGl_string2291z00zzmodule_foreignz00,
																											BGl_string2300z00zzmodule_foreignz00,
																											BgL_typez00_33,
																											BgL_list1887z00_1608);
																									}
																								}
																						}
																					else
																						{	/* Module/foreign.scm 221 */
																							goto BgL_tagzd21745zd2_1530;
																						}
																				}
																			else
																				{	/* Module/foreign.scm 221 */
																					goto BgL_tagzd21745zd2_1530;
																				}
																		}
																	else
																		{	/* Module/foreign.scm 221 */
																			goto BgL_tagzd21745zd2_1530;
																		}
																}
														}
													else
														{	/* Module/foreign.scm 221 */
															obj_t BgL_carzd21815zd2_1559;
															obj_t BgL_cdrzd21816zd2_1560;

															BgL_carzd21815zd2_1559 =
																CAR(((obj_t) BgL_cdrzd21753zd2_1533));
															BgL_cdrzd21816zd2_1560 =
																CDR(((obj_t) BgL_cdrzd21753zd2_1533));
															if (SYMBOLP(BgL_carzd21815zd2_1559))
																{	/* Module/foreign.scm 221 */
																	obj_t BgL_cdrzd21825zd2_1562;

																	BgL_cdrzd21825zd2_1562 =
																		CDR(((obj_t) BgL_cdrzd21816zd2_1560));
																	if (PAIRP(BgL_cdrzd21825zd2_1562))
																		{	/* Module/foreign.scm 221 */
																			obj_t BgL_carzd21828zd2_1564;

																			BgL_carzd21828zd2_1564 =
																				CAR(BgL_cdrzd21825zd2_1562);
																			if (STRINGP(BgL_carzd21828zd2_1564))
																				{	/* Module/foreign.scm 221 */
																					if (NULLP(CDR
																							(BgL_cdrzd21825zd2_1562)))
																						{	/* Module/foreign.scm 221 */
																							obj_t BgL_arg1852z00_1568;

																							BgL_arg1852z00_1568 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd21816zd2_1560));
																							{
																								obj_t BgL_namez00_3817;
																								obj_t BgL_tzd2expzd2_3816;
																								obj_t BgL_idz00_3815;

																								BgL_idz00_3815 =
																									BgL_carzd21815zd2_1559;
																								BgL_tzd2expzd2_3816 =
																									BgL_arg1852z00_1568;
																								BgL_namez00_3817 =
																									BgL_carzd21828zd2_1564;
																								BgL_namez00_1528 =
																									BgL_namez00_3817;
																								BgL_tzd2expzd2_1527 =
																									BgL_tzd2expzd2_3816;
																								BgL_idz00_1526 = BgL_idz00_3815;
																								goto BgL_tagzd21744zd2_1529;
																							}
																						}
																					else
																						{	/* Module/foreign.scm 221 */
																							goto BgL_tagzd21745zd2_1530;
																						}
																				}
																			else
																				{	/* Module/foreign.scm 221 */
																					goto BgL_tagzd21745zd2_1530;
																				}
																		}
																	else
																		{	/* Module/foreign.scm 221 */
																			goto BgL_tagzd21745zd2_1530;
																		}
																}
															else
																{	/* Module/foreign.scm 221 */
																	goto BgL_tagzd21745zd2_1530;
																}
														}
												}
											else
												{	/* Module/foreign.scm 221 */
													goto BgL_tagzd21745zd2_1530;
												}
										}
									else
										{	/* Module/foreign.scm 221 */
											obj_t BgL_carzd21851zd2_1571;
											obj_t BgL_cdrzd21852zd2_1572;

											BgL_carzd21851zd2_1571 =
												CAR(((obj_t) BgL_cdrzd21753zd2_1533));
											BgL_cdrzd21852zd2_1572 =
												CDR(((obj_t) BgL_cdrzd21753zd2_1533));
											if (SYMBOLP(BgL_carzd21851zd2_1571))
												{	/* Module/foreign.scm 221 */
													if (PAIRP(BgL_cdrzd21852zd2_1572))
														{	/* Module/foreign.scm 221 */
															obj_t BgL_cdrzd21859zd2_1575;

															BgL_cdrzd21859zd2_1575 =
																CDR(BgL_cdrzd21852zd2_1572);
															if (PAIRP(BgL_cdrzd21859zd2_1575))
																{	/* Module/foreign.scm 221 */
																	obj_t BgL_carzd21862zd2_1577;

																	BgL_carzd21862zd2_1577 =
																		CAR(BgL_cdrzd21859zd2_1575);
																	if (STRINGP(BgL_carzd21862zd2_1577))
																		{	/* Module/foreign.scm 221 */
																			if (NULLP(CDR(BgL_cdrzd21859zd2_1575)))
																				{
																					obj_t BgL_namez00_3838;
																					obj_t BgL_tzd2expzd2_3836;
																					obj_t BgL_idz00_3835;

																					BgL_idz00_3835 =
																						BgL_carzd21851zd2_1571;
																					BgL_tzd2expzd2_3836 =
																						CAR(BgL_cdrzd21852zd2_1572);
																					BgL_namez00_3838 =
																						BgL_carzd21862zd2_1577;
																					BgL_namez00_1528 = BgL_namez00_3838;
																					BgL_tzd2expzd2_1527 =
																						BgL_tzd2expzd2_3836;
																					BgL_idz00_1526 = BgL_idz00_3835;
																					goto BgL_tagzd21744zd2_1529;
																				}
																			else
																				{	/* Module/foreign.scm 221 */
																					goto BgL_tagzd21745zd2_1530;
																				}
																		}
																	else
																		{	/* Module/foreign.scm 221 */
																			goto BgL_tagzd21745zd2_1530;
																		}
																}
															else
																{	/* Module/foreign.scm 221 */
																	goto BgL_tagzd21745zd2_1530;
																}
														}
													else
														{	/* Module/foreign.scm 221 */
															goto BgL_tagzd21745zd2_1530;
														}
												}
											else
												{	/* Module/foreign.scm 221 */
													goto BgL_tagzd21745zd2_1530;
												}
										}
								}
							else
								{	/* Module/foreign.scm 221 */
									goto BgL_tagzd21745zd2_1530;
								}
						}
					else
						{	/* Module/foreign.scm 221 */
							goto BgL_tagzd21745zd2_1530;
						}
				}
			}
		}

	}



/* check-c-foreign-type-exp? */
	bool_t BGl_checkzd2czd2foreignzd2typezd2expzf3zf3zzmodule_foreignz00(obj_t
		BgL_tzd2expzd2_34)
	{
		{	/* Module/foreign.scm 249 */
			{
				obj_t BgL_slotsz00_1618;
				obj_t BgL_slotsz00_1610;

				if (SYMBOLP(BgL_tzd2expzd2_34))
					{	/* Module/foreign.scm 250 */
						return ((bool_t) 1);
					}
				else
					{	/* Module/foreign.scm 250 */
						if (PAIRP(BgL_tzd2expzd2_34))
							{	/* Module/foreign.scm 250 */
								obj_t BgL_carzd21900zd2_1624;
								obj_t BgL_cdrzd21901zd2_1625;

								BgL_carzd21900zd2_1624 = CAR(((obj_t) BgL_tzd2expzd2_34));
								BgL_cdrzd21901zd2_1625 = CDR(((obj_t) BgL_tzd2expzd2_34));
								if ((BgL_carzd21900zd2_1624 == CNST_TABLE_REF(10)))
									{	/* Module/foreign.scm 250 */
										BgL_slotsz00_1610 = BgL_cdrzd21901zd2_1625;
									BgL_tagzd21886zd2_1611:
										{
											obj_t BgL_slotsz00_1757;

											BgL_slotsz00_1757 = BgL_slotsz00_1610;
										BgL_zc3z04anonymousza32007ze3z87_1758:
											if (NULLP(BgL_slotsz00_1757))
												{	/* Module/foreign.scm 255 */
													return ((bool_t) 1);
												}
											else
												{

													{	/* Module/foreign.scm 257 */
														obj_t BgL_ezd22154zd2_1762;

														BgL_ezd22154zd2_1762 =
															CAR(((obj_t) BgL_slotsz00_1757));
														if (PAIRP(BgL_ezd22154zd2_1762))
															{	/* Module/foreign.scm 257 */
																obj_t BgL_cdrzd22156zd2_1764;

																BgL_cdrzd22156zd2_1764 =
																	CDR(BgL_ezd22154zd2_1762);
																{	/* Module/foreign.scm 257 */
																	bool_t BgL_test2494z00_3857;

																	{	/* Module/foreign.scm 257 */
																		obj_t BgL_tmpz00_3858;

																		BgL_tmpz00_3858 = CAR(BgL_ezd22154zd2_1762);
																		BgL_test2494z00_3857 =
																			SYMBOLP(BgL_tmpz00_3858);
																	}
																	if (BgL_test2494z00_3857)
																		{	/* Module/foreign.scm 257 */
																			if (PAIRP(BgL_cdrzd22156zd2_1764))
																				{	/* Module/foreign.scm 257 */
																					obj_t BgL_cdrzd22158zd2_1768;

																					BgL_cdrzd22158zd2_1768 =
																						CDR(BgL_cdrzd22156zd2_1764);
																					{	/* Module/foreign.scm 257 */
																						bool_t BgL_test2496z00_3864;

																						{	/* Module/foreign.scm 257 */
																							obj_t BgL_tmpz00_3865;

																							BgL_tmpz00_3865 =
																								CAR(BgL_cdrzd22156zd2_1764);
																							BgL_test2496z00_3864 =
																								SYMBOLP(BgL_tmpz00_3865);
																						}
																						if (BgL_test2496z00_3864)
																							{	/* Module/foreign.scm 257 */
																								if (PAIRP
																									(BgL_cdrzd22158zd2_1768))
																									{	/* Module/foreign.scm 257 */
																										bool_t BgL_test2498z00_3870;

																										{	/* Module/foreign.scm 257 */
																											obj_t BgL_tmpz00_3871;

																											BgL_tmpz00_3871 =
																												CAR
																												(BgL_cdrzd22158zd2_1768);
																											BgL_test2498z00_3870 =
																												STRINGP
																												(BgL_tmpz00_3871);
																										}
																										if (BgL_test2498z00_3870)
																											{	/* Module/foreign.scm 257 */
																												if (NULLP(CDR
																														(BgL_cdrzd22158zd2_1768)))
																													{	/* Module/foreign.scm 257 */
																														{
																															obj_t
																																BgL_slotsz00_3877;
																															BgL_slotsz00_3877
																																=
																																CDR(((obj_t)
																																	BgL_slotsz00_1757));
																															BgL_slotsz00_1757
																																=
																																BgL_slotsz00_3877;
																															goto
																																BgL_zc3z04anonymousza32007ze3z87_1758;
																														}
																													}
																												else
																													{	/* Module/foreign.scm 257 */
																														return ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Module/foreign.scm 257 */
																												return ((bool_t) 0);
																											}
																									}
																								else
																									{	/* Module/foreign.scm 257 */
																										return ((bool_t) 0);
																									}
																							}
																						else
																							{	/* Module/foreign.scm 257 */
																								return ((bool_t) 0);
																							}
																					}
																				}
																			else
																				{	/* Module/foreign.scm 257 */
																					return ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Module/foreign.scm 257 */
																			return ((bool_t) 0);
																		}
																}
															}
														else
															{	/* Module/foreign.scm 257 */
																return ((bool_t) 0);
															}
													}
												}
										}
									}
								else
									{	/* Module/foreign.scm 250 */
										if ((BgL_carzd21900zd2_1624 == CNST_TABLE_REF(11)))
											{
												obj_t BgL_slotsz00_3883;

												BgL_slotsz00_3883 = BgL_cdrzd21901zd2_1625;
												BgL_slotsz00_1610 = BgL_slotsz00_3883;
												goto BgL_tagzd21886zd2_1611;
											}
										else
											{	/* Module/foreign.scm 250 */
												if ((BgL_carzd21900zd2_1624 == CNST_TABLE_REF(12)))
													{	/* Module/foreign.scm 250 */
														return NULLP(CDR(((obj_t) BgL_tzd2expzd2_34)));
													}
												else
													{	/* Module/foreign.scm 250 */
														obj_t BgL_cdrzd21948zd2_1640;

														BgL_cdrzd21948zd2_1640 =
															CDR(((obj_t) BgL_tzd2expzd2_34));
														if (
															(CAR(
																	((obj_t) BgL_tzd2expzd2_34)) ==
																CNST_TABLE_REF(13)))
															{	/* Module/foreign.scm 250 */
																if (PAIRP(BgL_cdrzd21948zd2_1640))
																	{	/* Module/foreign.scm 250 */
																		bool_t BgL_test2504z00_3899;

																		{	/* Module/foreign.scm 250 */
																			obj_t BgL_tmpz00_3900;

																			BgL_tmpz00_3900 =
																				CAR(BgL_cdrzd21948zd2_1640);
																			BgL_test2504z00_3899 =
																				SYMBOLP(BgL_tmpz00_3900);
																		}
																		if (BgL_test2504z00_3899)
																			{	/* Module/foreign.scm 250 */
																				return
																					NULLP(CDR(BgL_cdrzd21948zd2_1640));
																			}
																		else
																			{	/* Module/foreign.scm 250 */
																				return ((bool_t) 0);
																			}
																	}
																else
																	{	/* Module/foreign.scm 250 */
																		return ((bool_t) 0);
																	}
															}
														else
															{	/* Module/foreign.scm 250 */
																obj_t BgL_carzd21999zd2_1658;

																BgL_carzd21999zd2_1658 =
																	CAR(((obj_t) BgL_tzd2expzd2_34));
																{

																	if (
																		(BgL_carzd21999zd2_1658 ==
																			CNST_TABLE_REF(17)))
																		{	/* Module/foreign.scm 250 */
																		BgL_kapzd22001zd2_1660:
																			if (PAIRP(BgL_cdrzd21948zd2_1640))
																				{	/* Module/foreign.scm 250 */
																					bool_t BgL_test2507z00_3912;

																					{	/* Module/foreign.scm 250 */
																						obj_t BgL_tmpz00_3913;

																						BgL_tmpz00_3913 =
																							CAR(BgL_cdrzd21948zd2_1640);
																						BgL_test2507z00_3912 =
																							SYMBOLP(BgL_tmpz00_3913);
																					}
																					if (BgL_test2507z00_3912)
																						{	/* Module/foreign.scm 250 */
																							if (NULLP(CDR
																									(BgL_cdrzd21948zd2_1640)))
																								{	/* Module/foreign.scm 250 */
																									return ((bool_t) 1);
																								}
																							else
																								{	/* Module/foreign.scm 250 */
																									obj_t BgL_cdrzd22010zd2_1698;

																									BgL_cdrzd22010zd2_1698 =
																										CDR(
																										((obj_t)
																											BgL_tzd2expzd2_34));
																									if ((CAR(((obj_t)
																													BgL_tzd2expzd2_34)) ==
																											CNST_TABLE_REF(14)))
																										{	/* Module/foreign.scm 250 */
																											obj_t
																												BgL_cdrzd22013zd2_1701;
																											BgL_cdrzd22013zd2_1701 =
																												CDR(((obj_t)
																													BgL_cdrzd22010zd2_1698));
																											{	/* Module/foreign.scm 250 */
																												bool_t
																													BgL_test2510z00_3928;
																												{	/* Module/foreign.scm 250 */
																													obj_t BgL_tmpz00_3929;

																													BgL_tmpz00_3929 =
																														CAR(
																														((obj_t)
																															BgL_cdrzd22010zd2_1698));
																													BgL_test2510z00_3928 =
																														SYMBOLP
																														(BgL_tmpz00_3929);
																												}
																												if (BgL_test2510z00_3928)
																													{	/* Module/foreign.scm 250 */
																														if (PAIRP
																															(BgL_cdrzd22013zd2_1701))
																															{	/* Module/foreign.scm 250 */
																																obj_t
																																	BgL_carzd22015zd2_1705;
																																BgL_carzd22015zd2_1705
																																	=
																																	CAR
																																	(BgL_cdrzd22013zd2_1701);
																																{

																																	if (NULLP
																																		(BgL_carzd22015zd2_1705))
																																		{	/* Module/foreign.scm 250 */
																																		BgL_kapzd22019zd2_1706:
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd22013zd2_1701))))
																																				{	/* Module/foreign.scm 250 */
																																					BGL_TAIL
																																						return
																																						BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00
																																						(BgL_carzd22015zd2_1705);
																																				}
																																			else
																																				{	/* Module/foreign.scm 250 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Module/foreign.scm 250 */
																																			if (PAIRP
																																				(BgL_carzd22015zd2_1705))
																																				{	/* Module/foreign.scm 250 */
																																					goto
																																						BgL_kapzd22019zd2_1706;
																																				}
																																			else
																																				{	/* Module/foreign.scm 250 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																														else
																															{	/* Module/foreign.scm 250 */
																																return ((bool_t)
																																	0);
																															}
																													}
																												else
																													{	/* Module/foreign.scm 250 */
																														return ((bool_t) 0);
																													}
																											}
																										}
																									else
																										{	/* Module/foreign.scm 250 */
																											if (
																												(CAR(
																														((obj_t)
																															BgL_tzd2expzd2_34))
																													==
																													CNST_TABLE_REF(15)))
																												{	/* Module/foreign.scm 250 */
																													BgL_slotsz00_1618 =
																														BgL_cdrzd22010zd2_1698;
																												BgL_tagzd21892zd2_1619:
																													{
																														obj_t
																															BgL_slotsz00_1783;
																														BgL_slotsz00_1783 =
																															BgL_slotsz00_1618;
																													BgL_zc3z04anonymousza32026ze3z87_1784:
																														if (NULLP
																															(BgL_slotsz00_1783))
																															{	/* Module/foreign.scm 274 */
																																return ((bool_t)
																																	1);
																															}
																														else
																															{

																																{	/* Module/foreign.scm 276 */
																																	obj_t
																																		BgL_ezd22163zd2_1788;
																																	BgL_ezd22163zd2_1788
																																		=
																																		CAR(((obj_t)
																																			BgL_slotsz00_1783));
																																	if (PAIRP
																																		(BgL_ezd22163zd2_1788))
																																		{	/* Module/foreign.scm 276 */
																																			obj_t
																																				BgL_cdrzd22165zd2_1790;
																																			BgL_cdrzd22165zd2_1790
																																				=
																																				CDR
																																				(BgL_ezd22163zd2_1788);
																																			{	/* Module/foreign.scm 276 */
																																				bool_t
																																					BgL_test2518z00_3957;
																																				{	/* Module/foreign.scm 276 */
																																					obj_t
																																						BgL_tmpz00_3958;
																																					BgL_tmpz00_3958
																																						=
																																						CAR
																																						(BgL_ezd22163zd2_1788);
																																					BgL_test2518z00_3957
																																						=
																																						SYMBOLP
																																						(BgL_tmpz00_3958);
																																				}
																																				if (BgL_test2518z00_3957)
																																					{	/* Module/foreign.scm 276 */
																																						if (PAIRP(BgL_cdrzd22165zd2_1790))
																																							{	/* Module/foreign.scm 276 */
																																								bool_t
																																									BgL_test2520z00_3963;
																																								{	/* Module/foreign.scm 276 */
																																									obj_t
																																										BgL_tmpz00_3964;
																																									BgL_tmpz00_3964
																																										=
																																										CAR
																																										(BgL_cdrzd22165zd2_1790);
																																									BgL_test2520z00_3963
																																										=
																																										STRINGP
																																										(BgL_tmpz00_3964);
																																								}
																																								if (BgL_test2520z00_3963)
																																									{	/* Module/foreign.scm 276 */
																																										if (NULLP(CDR(BgL_cdrzd22165zd2_1790)))
																																											{	/* Module/foreign.scm 276 */
																																												{
																																													obj_t
																																														BgL_slotsz00_3970;
																																													BgL_slotsz00_3970
																																														=
																																														CDR
																																														(
																																														((obj_t) BgL_slotsz00_1783));
																																													BgL_slotsz00_1783
																																														=
																																														BgL_slotsz00_3970;
																																													goto
																																														BgL_zc3z04anonymousza32026ze3z87_1784;
																																												}
																																											}
																																										else
																																											{	/* Module/foreign.scm 276 */
																																												return
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																								else
																																									{	/* Module/foreign.scm 276 */
																																										return
																																											(
																																											(bool_t)
																																											0);
																																									}
																																							}
																																						else
																																							{	/* Module/foreign.scm 276 */
																																								return
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																				else
																																					{	/* Module/foreign.scm 276 */
																																						return
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																		}
																																	else
																																		{	/* Module/foreign.scm 276 */
																																			return (
																																				(bool_t)
																																				0);
																																		}
																																}
																															}
																													}
																												}
																											else
																												{	/* Module/foreign.scm 250 */
																													return ((bool_t) 0);
																												}
																										}
																								}
																						}
																					else
																						{	/* Module/foreign.scm 250 */
																							obj_t BgL_cdrzd22037zd2_1719;

																							BgL_cdrzd22037zd2_1719 =
																								CDR(
																								((obj_t) BgL_tzd2expzd2_34));
																							if (
																								(CAR(
																										((obj_t) BgL_tzd2expzd2_34))
																									== CNST_TABLE_REF(16)))
																								{	/* Module/foreign.scm 250 */
																									bool_t BgL_test2523z00_3980;

																									{	/* Module/foreign.scm 250 */
																										obj_t BgL_tmpz00_3981;

																										BgL_tmpz00_3981 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd22037zd2_1719));
																										BgL_test2523z00_3980 =
																											SYMBOLP(BgL_tmpz00_3981);
																									}
																									if (BgL_test2523z00_3980)
																										{	/* Module/foreign.scm 250 */
																											return
																												NULLP(CDR(
																													((obj_t)
																														BgL_cdrzd22037zd2_1719)));
																										}
																									else
																										{	/* Module/foreign.scm 250 */
																											return ((bool_t) 0);
																										}
																								}
																							else
																								{	/* Module/foreign.scm 250 */
																									if (
																										(CAR(
																												((obj_t)
																													BgL_tzd2expzd2_34)) ==
																											CNST_TABLE_REF(14)))
																										{	/* Module/foreign.scm 250 */
																											obj_t
																												BgL_cdrzd22057zd2_1731;
																											BgL_cdrzd22057zd2_1731 =
																												CDR(((obj_t)
																													BgL_cdrzd22037zd2_1719));
																											{	/* Module/foreign.scm 250 */
																												bool_t
																													BgL_test2525z00_3995;
																												{	/* Module/foreign.scm 250 */
																													obj_t BgL_tmpz00_3996;

																													BgL_tmpz00_3996 =
																														CAR(
																														((obj_t)
																															BgL_cdrzd22037zd2_1719));
																													BgL_test2525z00_3995 =
																														SYMBOLP
																														(BgL_tmpz00_3996);
																												}
																												if (BgL_test2525z00_3995)
																													{	/* Module/foreign.scm 250 */
																														if (PAIRP
																															(BgL_cdrzd22057zd2_1731))
																															{	/* Module/foreign.scm 250 */
																																obj_t
																																	BgL_carzd22059zd2_1735;
																																BgL_carzd22059zd2_1735
																																	=
																																	CAR
																																	(BgL_cdrzd22057zd2_1731);
																																{

																																	if (NULLP
																																		(BgL_carzd22059zd2_1735))
																																		{	/* Module/foreign.scm 250 */
																																		BgL_kapzd22063zd2_1736:
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd22057zd2_1731))))
																																				{	/* Module/foreign.scm 250 */
																																					BGL_TAIL
																																						return
																																						BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00
																																						(BgL_carzd22059zd2_1735);
																																				}
																																			else
																																				{	/* Module/foreign.scm 250 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Module/foreign.scm 250 */
																																			if (PAIRP
																																				(BgL_carzd22059zd2_1735))
																																				{	/* Module/foreign.scm 250 */
																																					goto
																																						BgL_kapzd22063zd2_1736;
																																				}
																																			else
																																				{	/* Module/foreign.scm 250 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																														else
																															{	/* Module/foreign.scm 250 */
																																return ((bool_t)
																																	0);
																															}
																													}
																												else
																													{	/* Module/foreign.scm 250 */
																														return ((bool_t) 0);
																													}
																											}
																										}
																									else
																										{	/* Module/foreign.scm 250 */
																											if (
																												(CAR(
																														((obj_t)
																															BgL_tzd2expzd2_34))
																													==
																													CNST_TABLE_REF(15)))
																												{
																													obj_t
																														BgL_slotsz00_4017;
																													BgL_slotsz00_4017 =
																														CDR(((obj_t)
																															BgL_tzd2expzd2_34));
																													BgL_slotsz00_1618 =
																														BgL_slotsz00_4017;
																													goto
																														BgL_tagzd21892zd2_1619;
																												}
																											else
																												{	/* Module/foreign.scm 250 */
																													return ((bool_t) 0);
																												}
																										}
																								}
																						}
																				}
																			else
																				{	/* Module/foreign.scm 250 */
																					if (
																						(CAR(
																								((obj_t) BgL_tzd2expzd2_34)) ==
																							CNST_TABLE_REF(15)))
																						{
																							obj_t BgL_slotsz00_4025;

																							BgL_slotsz00_4025 =
																								CDR(
																								((obj_t) BgL_tzd2expzd2_34));
																							BgL_slotsz00_1618 =
																								BgL_slotsz00_4025;
																							goto BgL_tagzd21892zd2_1619;
																						}
																					else
																						{	/* Module/foreign.scm 250 */
																							return ((bool_t) 0);
																						}
																				}
																		}
																	else
																		{	/* Module/foreign.scm 250 */
																			if (
																				(BgL_carzd21999zd2_1658 ==
																					CNST_TABLE_REF(18)))
																				{	/* Module/foreign.scm 250 */
																					goto BgL_kapzd22001zd2_1660;
																				}
																			else
																				{	/* Module/foreign.scm 250 */
																					obj_t BgL_cdrzd22091zd2_1661;

																					BgL_cdrzd22091zd2_1661 =
																						CDR(((obj_t) BgL_tzd2expzd2_34));
																					if (
																						(CAR(
																								((obj_t) BgL_tzd2expzd2_34)) ==
																							CNST_TABLE_REF(16)))
																						{	/* Module/foreign.scm 250 */
																							if (PAIRP(BgL_cdrzd22091zd2_1661))
																								{	/* Module/foreign.scm 250 */
																									bool_t BgL_test2535z00_4040;

																									{	/* Module/foreign.scm 250 */
																										obj_t BgL_tmpz00_4041;

																										BgL_tmpz00_4041 =
																											CAR
																											(BgL_cdrzd22091zd2_1661);
																										BgL_test2535z00_4040 =
																											SYMBOLP(BgL_tmpz00_4041);
																									}
																									if (BgL_test2535z00_4040)
																										{	/* Module/foreign.scm 250 */
																											return
																												NULLP(CDR
																												(BgL_cdrzd22091zd2_1661));
																										}
																									else
																										{	/* Module/foreign.scm 250 */
																											return ((bool_t) 0);
																										}
																								}
																							else
																								{	/* Module/foreign.scm 250 */
																									return ((bool_t) 0);
																								}
																						}
																					else
																						{	/* Module/foreign.scm 250 */
																							if (
																								(CAR(
																										((obj_t) BgL_tzd2expzd2_34))
																									== CNST_TABLE_REF(14)))
																								{	/* Module/foreign.scm 250 */
																									if (PAIRP
																										(BgL_cdrzd22091zd2_1661))
																										{	/* Module/foreign.scm 250 */
																											obj_t
																												BgL_cdrzd22116zd2_1675;
																											BgL_cdrzd22116zd2_1675 =
																												CDR
																												(BgL_cdrzd22091zd2_1661);
																											{	/* Module/foreign.scm 250 */
																												bool_t
																													BgL_test2538z00_4054;
																												{	/* Module/foreign.scm 250 */
																													obj_t BgL_tmpz00_4055;

																													BgL_tmpz00_4055 =
																														CAR
																														(BgL_cdrzd22091zd2_1661);
																													BgL_test2538z00_4054 =
																														SYMBOLP
																														(BgL_tmpz00_4055);
																												}
																												if (BgL_test2538z00_4054)
																													{	/* Module/foreign.scm 250 */
																														if (PAIRP
																															(BgL_cdrzd22116zd2_1675))
																															{	/* Module/foreign.scm 250 */
																																obj_t
																																	BgL_carzd22118zd2_1679;
																																BgL_carzd22118zd2_1679
																																	=
																																	CAR
																																	(BgL_cdrzd22116zd2_1675);
																																{

																																	if (NULLP
																																		(BgL_carzd22118zd2_1679))
																																		{	/* Module/foreign.scm 250 */
																																		BgL_kapzd22122zd2_1680:
																																			if (NULLP
																																				(CDR(((obj_t) BgL_cdrzd22116zd2_1675))))
																																				{	/* Module/foreign.scm 250 */
																																					BGL_TAIL
																																						return
																																						BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00
																																						(BgL_carzd22118zd2_1679);
																																				}
																																			else
																																				{	/* Module/foreign.scm 250 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																	else
																																		{	/* Module/foreign.scm 250 */
																																			if (PAIRP
																																				(BgL_carzd22118zd2_1679))
																																				{	/* Module/foreign.scm 250 */
																																					goto
																																						BgL_kapzd22122zd2_1680;
																																				}
																																			else
																																				{	/* Module/foreign.scm 250 */
																																					return
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																														else
																															{	/* Module/foreign.scm 250 */
																																return ((bool_t)
																																	0);
																															}
																													}
																												else
																													{	/* Module/foreign.scm 250 */
																														return ((bool_t) 0);
																													}
																											}
																										}
																									else
																										{	/* Module/foreign.scm 250 */
																											return ((bool_t) 0);
																										}
																								}
																							else
																								{	/* Module/foreign.scm 250 */
																									if (
																										(CAR(
																												((obj_t)
																													BgL_tzd2expzd2_34)) ==
																											CNST_TABLE_REF(15)))
																										{
																											obj_t BgL_slotsz00_4075;

																											BgL_slotsz00_4075 =
																												CDR(
																												((obj_t)
																													BgL_tzd2expzd2_34));
																											BgL_slotsz00_1618 =
																												BgL_slotsz00_4075;
																											goto
																												BgL_tagzd21892zd2_1619;
																										}
																									else
																										{	/* Module/foreign.scm 250 */
																											return ((bool_t) 0);
																										}
																								}
																						}
																				}
																		}
																}
															}
													}
											}
									}
							}
						else
							{	/* Module/foreign.scm 250 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* parse-c-extern-type */
	obj_t BGl_parsezd2czd2externzd2typezd2zzmodule_foreignz00(obj_t
		BgL_typez00_35)
	{
		{	/* Module/foreign.scm 287 */
			{
				obj_t BgL_idz00_1806;
				obj_t BgL_tzd2expzd2_1807;

				{	/* Module/foreign.scm 288 */
					obj_t BgL_cdrzd22178zd2_1812;

					BgL_cdrzd22178zd2_1812 = CDR(BgL_typez00_35);
					if ((CAR(BgL_typez00_35) == CNST_TABLE_REF(5)))
						{	/* Module/foreign.scm 288 */
							if (PAIRP(BgL_cdrzd22178zd2_1812))
								{	/* Module/foreign.scm 288 */
									obj_t BgL_carzd22181zd2_1816;
									obj_t BgL_cdrzd22182zd2_1817;

									BgL_carzd22181zd2_1816 = CAR(BgL_cdrzd22178zd2_1812);
									BgL_cdrzd22182zd2_1817 = CDR(BgL_cdrzd22178zd2_1812);
									if (SYMBOLP(BgL_carzd22181zd2_1816))
										{	/* Module/foreign.scm 288 */
											if (PAIRP(BgL_cdrzd22182zd2_1817))
												{	/* Module/foreign.scm 288 */
													obj_t BgL_carzd22187zd2_1820;

													BgL_carzd22187zd2_1820 = CAR(BgL_cdrzd22182zd2_1817);
													if (STRINGP(BgL_carzd22187zd2_1820))
														{	/* Module/foreign.scm 288 */
															if (NULLP(CDR(BgL_cdrzd22182zd2_1817)))
																{	/* Module/foreign.scm 288 */
																	BGL_TAIL return
																		BGl_parsezd2czd2foreignzd2typezd2zzmodule_foreignz00
																		(BgL_typez00_35);
																}
															else
																{	/* Module/foreign.scm 288 */
																	obj_t BgL_carzd22204zd2_1825;
																	obj_t BgL_cdrzd22205zd2_1826;

																	BgL_carzd22204zd2_1825 =
																		CAR(((obj_t) BgL_cdrzd22178zd2_1812));
																	BgL_cdrzd22205zd2_1826 =
																		CDR(((obj_t) BgL_cdrzd22178zd2_1812));
																	if (SYMBOLP(BgL_carzd22204zd2_1825))
																		{	/* Module/foreign.scm 288 */
																			obj_t BgL_cdrzd22214zd2_1828;

																			BgL_cdrzd22214zd2_1828 =
																				CDR(((obj_t) BgL_cdrzd22205zd2_1826));
																			if (PAIRP(BgL_cdrzd22214zd2_1828))
																				{	/* Module/foreign.scm 288 */
																					bool_t BgL_test2552z00_4108;

																					{	/* Module/foreign.scm 288 */
																						obj_t BgL_tmpz00_4109;

																						BgL_tmpz00_4109 =
																							CAR(BgL_cdrzd22214zd2_1828);
																						BgL_test2552z00_4108 =
																							STRINGP(BgL_tmpz00_4109);
																					}
																					if (BgL_test2552z00_4108)
																						{	/* Module/foreign.scm 288 */
																							if (NULLP(CDR
																									(BgL_cdrzd22214zd2_1828)))
																								{	/* Module/foreign.scm 288 */
																									obj_t BgL_arg2059z00_1834;

																									BgL_arg2059z00_1834 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd22205zd2_1826));
																									BgL_idz00_1806 =
																										BgL_carzd22204zd2_1825;
																									BgL_tzd2expzd2_1807 =
																										BgL_arg2059z00_1834;
																								BgL_tagzd22169zd2_1808:
																									{	/* Module/foreign.scm 292 */
																										obj_t
																											BgL_foreignzd2typezd2_1866;
																										BgL_foreignzd2typezd2_1866 =
																											BGl_czd2externzd2typezd2ze3czd2foreignzd2typez31zzmodule_foreignz00
																											(BgL_tzd2expzd2_1807);
																										if (CBOOL
																											(BgL_foreignzd2typezd2_1866))
																											{	/* Module/foreign.scm 293 */
																												BGL_TAIL return
																													BGl_parsezd2czd2foreignzd2typezd2zzmodule_foreignz00
																													(BgL_typez00_35);
																											}
																										else
																											{	/* Module/foreign.scm 295 */
																												obj_t
																													BgL_list2084z00_1867;
																												BgL_list2084z00_1867 =
																													MAKE_YOUNG_PAIR(BNIL,
																													BNIL);
																												return
																													BGl_userzd2errorzd2zztools_errorz00
																													(BGl_string2291z00zzmodule_foreignz00,
																													BGl_string2301z00zzmodule_foreignz00,
																													BgL_typez00_35,
																													BgL_list2084z00_1867);
																											}
																									}
																								}
																							else
																								{	/* Module/foreign.scm 288 */
																								BgL_tagzd22170zd2_1809:
																									{	/* Module/foreign.scm 297 */
																										obj_t BgL_list2085z00_1868;

																										BgL_list2085z00_1868 =
																											MAKE_YOUNG_PAIR(BNIL,
																											BNIL);
																										return
																											BGl_userzd2errorzd2zztools_errorz00
																											(BGl_string2291z00zzmodule_foreignz00,
																											BGl_string2301z00zzmodule_foreignz00,
																											BgL_typez00_35,
																											BgL_list2085z00_1868);
																									}
																								}
																						}
																					else
																						{	/* Module/foreign.scm 288 */
																							goto BgL_tagzd22170zd2_1809;
																						}
																				}
																			else
																				{	/* Module/foreign.scm 288 */
																					goto BgL_tagzd22170zd2_1809;
																				}
																		}
																	else
																		{	/* Module/foreign.scm 288 */
																			goto BgL_tagzd22170zd2_1809;
																		}
																}
														}
													else
														{	/* Module/foreign.scm 288 */
															obj_t BgL_carzd22229zd2_1839;
															obj_t BgL_cdrzd22230zd2_1840;

															BgL_carzd22229zd2_1839 =
																CAR(((obj_t) BgL_cdrzd22178zd2_1812));
															BgL_cdrzd22230zd2_1840 =
																CDR(((obj_t) BgL_cdrzd22178zd2_1812));
															if (SYMBOLP(BgL_carzd22229zd2_1839))
																{	/* Module/foreign.scm 288 */
																	obj_t BgL_cdrzd22238zd2_1842;

																	BgL_cdrzd22238zd2_1842 =
																		CDR(((obj_t) BgL_cdrzd22230zd2_1840));
																	if (PAIRP(BgL_cdrzd22238zd2_1842))
																		{	/* Module/foreign.scm 288 */
																			bool_t BgL_test2557z00_4135;

																			{	/* Module/foreign.scm 288 */
																				obj_t BgL_tmpz00_4136;

																				BgL_tmpz00_4136 =
																					CAR(BgL_cdrzd22238zd2_1842);
																				BgL_test2557z00_4135 =
																					STRINGP(BgL_tmpz00_4136);
																			}
																			if (BgL_test2557z00_4135)
																				{	/* Module/foreign.scm 288 */
																					if (NULLP(CDR
																							(BgL_cdrzd22238zd2_1842)))
																						{	/* Module/foreign.scm 288 */
																							obj_t BgL_arg2069z00_1848;

																							BgL_arg2069z00_1848 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd22230zd2_1840));
																							{
																								obj_t BgL_tzd2expzd2_4145;
																								obj_t BgL_idz00_4144;

																								BgL_idz00_4144 =
																									BgL_carzd22229zd2_1839;
																								BgL_tzd2expzd2_4145 =
																									BgL_arg2069z00_1848;
																								BgL_tzd2expzd2_1807 =
																									BgL_tzd2expzd2_4145;
																								BgL_idz00_1806 = BgL_idz00_4144;
																								goto BgL_tagzd22169zd2_1808;
																							}
																						}
																					else
																						{	/* Module/foreign.scm 288 */
																							goto BgL_tagzd22170zd2_1809;
																						}
																				}
																			else
																				{	/* Module/foreign.scm 288 */
																					goto BgL_tagzd22170zd2_1809;
																				}
																		}
																	else
																		{	/* Module/foreign.scm 288 */
																			goto BgL_tagzd22170zd2_1809;
																		}
																}
															else
																{	/* Module/foreign.scm 288 */
																	goto BgL_tagzd22170zd2_1809;
																}
														}
												}
											else
												{	/* Module/foreign.scm 288 */
													goto BgL_tagzd22170zd2_1809;
												}
										}
									else
										{	/* Module/foreign.scm 288 */
											obj_t BgL_carzd22255zd2_1852;
											obj_t BgL_cdrzd22256zd2_1853;

											BgL_carzd22255zd2_1852 =
												CAR(((obj_t) BgL_cdrzd22178zd2_1812));
											BgL_cdrzd22256zd2_1853 =
												CDR(((obj_t) BgL_cdrzd22178zd2_1812));
											if (SYMBOLP(BgL_carzd22255zd2_1852))
												{	/* Module/foreign.scm 288 */
													if (PAIRP(BgL_cdrzd22256zd2_1853))
														{	/* Module/foreign.scm 288 */
															obj_t BgL_cdrzd22262zd2_1856;

															BgL_cdrzd22262zd2_1856 =
																CDR(BgL_cdrzd22256zd2_1853);
															if (PAIRP(BgL_cdrzd22262zd2_1856))
																{	/* Module/foreign.scm 288 */
																	bool_t BgL_test2562z00_4157;

																	{	/* Module/foreign.scm 288 */
																		obj_t BgL_tmpz00_4158;

																		BgL_tmpz00_4158 =
																			CAR(BgL_cdrzd22262zd2_1856);
																		BgL_test2562z00_4157 =
																			STRINGP(BgL_tmpz00_4158);
																	}
																	if (BgL_test2562z00_4157)
																		{	/* Module/foreign.scm 288 */
																			if (NULLP(CDR(BgL_cdrzd22262zd2_1856)))
																				{
																					obj_t BgL_tzd2expzd2_4165;
																					obj_t BgL_idz00_4164;

																					BgL_idz00_4164 =
																						BgL_carzd22255zd2_1852;
																					BgL_tzd2expzd2_4165 =
																						CAR(BgL_cdrzd22256zd2_1853);
																					BgL_tzd2expzd2_1807 =
																						BgL_tzd2expzd2_4165;
																					BgL_idz00_1806 = BgL_idz00_4164;
																					goto BgL_tagzd22169zd2_1808;
																				}
																			else
																				{	/* Module/foreign.scm 288 */
																					goto BgL_tagzd22170zd2_1809;
																				}
																		}
																	else
																		{	/* Module/foreign.scm 288 */
																			goto BgL_tagzd22170zd2_1809;
																		}
																}
															else
																{	/* Module/foreign.scm 288 */
																	goto BgL_tagzd22170zd2_1809;
																}
														}
													else
														{	/* Module/foreign.scm 288 */
															goto BgL_tagzd22170zd2_1809;
														}
												}
											else
												{	/* Module/foreign.scm 288 */
													goto BgL_tagzd22170zd2_1809;
												}
										}
								}
							else
								{	/* Module/foreign.scm 288 */
									goto BgL_tagzd22170zd2_1809;
								}
						}
					else
						{	/* Module/foreign.scm 288 */
							goto BgL_tagzd22170zd2_1809;
						}
				}
			}
		}

	}



/* c-extern-type->c-foreign-type */
	obj_t
		BGl_czd2externzd2typezd2ze3czd2foreignzd2typez31zzmodule_foreignz00(obj_t
		BgL_tzd2expzd2_36)
	{
		{	/* Module/foreign.scm 302 */
			{
				obj_t BgL_slotsz00_1877;
				obj_t BgL_slotsz00_1870;

				if (SYMBOLP(BgL_tzd2expzd2_36))
					{	/* Module/foreign.scm 303 */
						return BgL_tzd2expzd2_36;
					}
				else
					{	/* Module/foreign.scm 303 */
						if (PAIRP(BgL_tzd2expzd2_36))
							{	/* Module/foreign.scm 303 */
								obj_t BgL_carzd22293zd2_1884;
								obj_t BgL_cdrzd22294zd2_1885;

								BgL_carzd22293zd2_1884 = CAR(((obj_t) BgL_tzd2expzd2_36));
								BgL_cdrzd22294zd2_1885 = CDR(((obj_t) BgL_tzd2expzd2_36));
								if ((BgL_carzd22293zd2_1884 == CNST_TABLE_REF(10)))
									{	/* Module/foreign.scm 303 */
										BgL_slotsz00_1870 = BgL_cdrzd22294zd2_1885;
									BgL_tagzd22279zd2_1871:
										{
											obj_t BgL_slotsz00_2014;

											BgL_slotsz00_2014 = BgL_slotsz00_1870;
										BgL_zc3z04anonymousza32195ze3z87_2015:
											if (NULLP(BgL_slotsz00_2014))
												{	/* Module/foreign.scm 308 */
													return BgL_tzd2expzd2_36;
												}
											else
												{
													obj_t BgL_idz00_2017;
													obj_t BgL_namez00_2018;

													{	/* Module/foreign.scm 310 */
														obj_t BgL_ezd22508zd2_2021;

														BgL_ezd22508zd2_2021 =
															CAR(((obj_t) BgL_slotsz00_2014));
														if (PAIRP(BgL_ezd22508zd2_2021))
															{	/* Module/foreign.scm 310 */
																obj_t BgL_carzd22513zd2_2023;
																obj_t BgL_cdrzd22514zd2_2024;

																BgL_carzd22513zd2_2023 =
																	CAR(BgL_ezd22508zd2_2021);
																BgL_cdrzd22514zd2_2024 =
																	CDR(BgL_ezd22508zd2_2021);
																if (SYMBOLP(BgL_carzd22513zd2_2023))
																	{	/* Module/foreign.scm 310 */
																		if (PAIRP(BgL_cdrzd22514zd2_2024))
																			{	/* Module/foreign.scm 310 */
																				obj_t BgL_carzd22518zd2_2027;

																				BgL_carzd22518zd2_2027 =
																					CAR(BgL_cdrzd22514zd2_2024);
																				if (STRINGP(BgL_carzd22518zd2_2027))
																					{	/* Module/foreign.scm 310 */
																						if (NULLP(CDR
																								(BgL_cdrzd22514zd2_2024)))
																							{	/* Module/foreign.scm 310 */
																								BgL_idz00_2017 =
																									BgL_carzd22513zd2_2023;
																								BgL_namez00_2018 =
																									BgL_carzd22518zd2_2027;
																								{	/* Module/foreign.scm 312 */
																									obj_t BgL_pidz00_2032;

																									BgL_pidz00_2032 =
																										BGl_parsezd2idzd2zzast_identz00
																										(BgL_idz00_2017,
																										BGl_findzd2locationzd2zztools_locationz00
																										(BgL_tzd2expzd2_36));
																									{	/* Module/foreign.scm 312 */
																										obj_t BgL_sidz00_2033;

																										BgL_sidz00_2033 =
																											CAR(BgL_pidz00_2032);
																										{	/* Module/foreign.scm 313 */
																											obj_t BgL_typez00_2034;

																											BgL_typez00_2034 =
																												(((BgL_typez00_bglt)
																													COBJECT((
																															(BgL_typez00_bglt)
																															CDR
																															(BgL_pidz00_2032))))->
																												BgL_idz00);
																											{	/* Module/foreign.scm 314 */

																												{	/* Module/foreign.scm 315 */
																													obj_t BgL_xz00_2913;

																													BgL_xz00_2913 =
																														BGl_checkzd2idzd2zzast_identz00
																														(BgL_pidz00_2032,
																														BgL_tzd2expzd2_36);
																													((bool_t) 1);
																												}
																												{	/* Module/foreign.scm 318 */
																													obj_t
																														BgL_arg2205z00_2036;
																													obj_t
																														BgL_arg2206z00_2037;
																													BgL_arg2205z00_2036 =
																														CAR(((obj_t)
																															BgL_slotsz00_2014));
																													{	/* Module/foreign.scm 318 */
																														obj_t
																															BgL_arg2207z00_2038;
																														BgL_arg2207z00_2038
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_namez00_2018,
																															BNIL);
																														BgL_arg2206z00_2037
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_sidz00_2033,
																															BgL_arg2207z00_2038);
																													}
																													{	/* Module/foreign.scm 318 */
																														obj_t
																															BgL_tmpz00_4207;
																														BgL_tmpz00_4207 =
																															((obj_t)
																															BgL_arg2205z00_2036);
																														SET_CDR
																															(BgL_tmpz00_4207,
																															BgL_arg2206z00_2037);
																													}
																												}
																												{	/* Module/foreign.scm 319 */
																													obj_t
																														BgL_arg2208z00_2039;
																													BgL_arg2208z00_2039 =
																														CAR(((obj_t)
																															BgL_slotsz00_2014));
																													{	/* Module/foreign.scm 319 */
																														obj_t
																															BgL_tmpz00_4212;
																														BgL_tmpz00_4212 =
																															((obj_t)
																															BgL_arg2208z00_2039);
																														SET_CAR
																															(BgL_tmpz00_4212,
																															BgL_typez00_2034);
																													}
																												}
																												{	/* Module/foreign.scm 320 */
																													obj_t
																														BgL_arg2209z00_2040;
																													BgL_arg2209z00_2040 =
																														CDR(((obj_t)
																															BgL_slotsz00_2014));
																													{
																														obj_t
																															BgL_slotsz00_4217;
																														BgL_slotsz00_4217 =
																															BgL_arg2209z00_2040;
																														BgL_slotsz00_2014 =
																															BgL_slotsz00_4217;
																														goto
																															BgL_zc3z04anonymousza32195ze3z87_2015;
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						else
																							{	/* Module/foreign.scm 310 */
																								return BFALSE;
																							}
																					}
																				else
																					{	/* Module/foreign.scm 310 */
																						return BFALSE;
																					}
																			}
																		else
																			{	/* Module/foreign.scm 310 */
																				return BFALSE;
																			}
																	}
																else
																	{	/* Module/foreign.scm 310 */
																		return BFALSE;
																	}
															}
														else
															{	/* Module/foreign.scm 310 */
																return BFALSE;
															}
													}
												}
										}
									}
								else
									{	/* Module/foreign.scm 303 */
										if ((BgL_carzd22293zd2_1884 == CNST_TABLE_REF(11)))
											{
												obj_t BgL_slotsz00_4221;

												BgL_slotsz00_4221 = BgL_cdrzd22294zd2_1885;
												BgL_slotsz00_1870 = BgL_slotsz00_4221;
												goto BgL_tagzd22279zd2_1871;
											}
										else
											{	/* Module/foreign.scm 303 */
												if (
													(CAR(
															((obj_t) BgL_tzd2expzd2_36)) ==
														CNST_TABLE_REF(13)))
													{	/* Module/foreign.scm 303 */
														if (PAIRP(BgL_cdrzd22294zd2_1885))
															{	/* Module/foreign.scm 303 */
																bool_t BgL_test2576z00_4229;

																{	/* Module/foreign.scm 303 */
																	obj_t BgL_tmpz00_4230;

																	BgL_tmpz00_4230 = CAR(BgL_cdrzd22294zd2_1885);
																	BgL_test2576z00_4229 =
																		SYMBOLP(BgL_tmpz00_4230);
																}
																if (BgL_test2576z00_4229)
																	{	/* Module/foreign.scm 303 */
																		if (NULLP(CDR(BgL_cdrzd22294zd2_1885)))
																			{	/* Module/foreign.scm 303 */
																				return BgL_tzd2expzd2_36;
																			}
																		else
																			{	/* Module/foreign.scm 303 */
																				return BFALSE;
																			}
																	}
																else
																	{	/* Module/foreign.scm 303 */
																		return BFALSE;
																	}
															}
														else
															{	/* Module/foreign.scm 303 */
																return BFALSE;
															}
													}
												else
													{	/* Module/foreign.scm 303 */
														obj_t BgL_carzd22351zd2_1904;
														obj_t BgL_cdrzd22352zd2_1905;

														BgL_carzd22351zd2_1904 =
															CAR(((obj_t) BgL_tzd2expzd2_36));
														BgL_cdrzd22352zd2_1905 =
															CDR(((obj_t) BgL_tzd2expzd2_36));
														{

															if (
																(BgL_carzd22351zd2_1904 == CNST_TABLE_REF(17)))
																{	/* Module/foreign.scm 303 */
																BgL_kapzd22353zd2_1906:
																	if (PAIRP(BgL_cdrzd22352zd2_1905))
																		{	/* Module/foreign.scm 303 */
																			bool_t BgL_test2580z00_4245;

																			{	/* Module/foreign.scm 303 */
																				obj_t BgL_tmpz00_4246;

																				BgL_tmpz00_4246 =
																					CAR(BgL_cdrzd22352zd2_1905);
																				BgL_test2580z00_4245 =
																					SYMBOLP(BgL_tmpz00_4246);
																			}
																			if (BgL_test2580z00_4245)
																				{	/* Module/foreign.scm 303 */
																					if (NULLP(CDR
																							(BgL_cdrzd22352zd2_1905)))
																						{	/* Module/foreign.scm 303 */
																							return BgL_tzd2expzd2_36;
																						}
																					else
																						{	/* Module/foreign.scm 303 */
																							obj_t BgL_cdrzd22362zd2_1950;

																							BgL_cdrzd22362zd2_1950 =
																								CDR(
																								((obj_t) BgL_tzd2expzd2_36));
																							if (
																								(CAR(
																										((obj_t) BgL_tzd2expzd2_36))
																									== CNST_TABLE_REF(14)))
																								{	/* Module/foreign.scm 303 */
																									obj_t BgL_cdrzd22365zd2_1953;

																									BgL_cdrzd22365zd2_1953 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd22362zd2_1950));
																									{	/* Module/foreign.scm 303 */
																										bool_t BgL_test2583z00_4261;

																										{	/* Module/foreign.scm 303 */
																											obj_t BgL_tmpz00_4262;

																											BgL_tmpz00_4262 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd22362zd2_1950));
																											BgL_test2583z00_4261 =
																												SYMBOLP
																												(BgL_tmpz00_4262);
																										}
																										if (BgL_test2583z00_4261)
																											{	/* Module/foreign.scm 303 */
																												if (PAIRP
																													(BgL_cdrzd22365zd2_1953))
																													{	/* Module/foreign.scm 303 */
																														obj_t
																															BgL_carzd22367zd2_1957;
																														BgL_carzd22367zd2_1957
																															=
																															CAR
																															(BgL_cdrzd22365zd2_1953);
																														{

																															if (NULLP
																																(BgL_carzd22367zd2_1957))
																																{	/* Module/foreign.scm 303 */
																																BgL_kapzd22371zd2_1958:
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd22365zd2_1953))))
																																		{	/* Module/foreign.scm 303 */
																																			if (BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00(BgL_carzd22367zd2_1957))
																																				{	/* Module/foreign.scm 330 */
																																					return
																																						BgL_tzd2expzd2_36;
																																				}
																																			else
																																				{	/* Module/foreign.scm 330 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Module/foreign.scm 303 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Module/foreign.scm 303 */
																																	if (PAIRP
																																		(BgL_carzd22367zd2_1957))
																																		{	/* Module/foreign.scm 303 */
																																			goto
																																				BgL_kapzd22371zd2_1958;
																																		}
																																	else
																																		{	/* Module/foreign.scm 303 */
																																			return
																																				BFALSE;
																																		}
																																}
																														}
																													}
																												else
																													{	/* Module/foreign.scm 303 */
																														return BFALSE;
																													}
																											}
																										else
																											{	/* Module/foreign.scm 303 */
																												return BFALSE;
																											}
																									}
																								}
																							else
																								{	/* Module/foreign.scm 303 */
																									if (
																										(CAR(
																												((obj_t)
																													BgL_tzd2expzd2_36)) ==
																											CNST_TABLE_REF(15)))
																										{	/* Module/foreign.scm 303 */
																											BgL_slotsz00_1877 =
																												BgL_cdrzd22362zd2_1950;
																										BgL_tagzd22284zd2_1878:
																											{
																												obj_t BgL_slotsz00_2046;

																												BgL_slotsz00_2046 =
																													BgL_slotsz00_1877;
																											BgL_zc3z04anonymousza32213ze3z87_2047:
																												if (NULLP
																													(BgL_slotsz00_2046))
																													{	/* Module/foreign.scm 335 */
																														return
																															BgL_tzd2expzd2_36;
																													}
																												else
																													{

																														{	/* Module/foreign.scm 337 */
																															obj_t
																																BgL_ezd22524zd2_2051;
																															BgL_ezd22524zd2_2051
																																=
																																CAR(((obj_t)
																																	BgL_slotsz00_2046));
																															if (PAIRP
																																(BgL_ezd22524zd2_2051))
																																{	/* Module/foreign.scm 337 */
																																	obj_t
																																		BgL_cdrzd22526zd2_2053;
																																	BgL_cdrzd22526zd2_2053
																																		=
																																		CDR
																																		(BgL_ezd22524zd2_2051);
																																	{	/* Module/foreign.scm 337 */
																																		bool_t
																																			BgL_test2592z00_4291;
																																		{	/* Module/foreign.scm 337 */
																																			obj_t
																																				BgL_tmpz00_4292;
																																			BgL_tmpz00_4292
																																				=
																																				CAR
																																				(BgL_ezd22524zd2_2051);
																																			BgL_test2592z00_4291
																																				=
																																				SYMBOLP
																																				(BgL_tmpz00_4292);
																																		}
																																		if (BgL_test2592z00_4291)
																																			{	/* Module/foreign.scm 337 */
																																				if (PAIRP(BgL_cdrzd22526zd2_2053))
																																					{	/* Module/foreign.scm 337 */
																																						bool_t
																																							BgL_test2594z00_4297;
																																						{	/* Module/foreign.scm 337 */
																																							obj_t
																																								BgL_tmpz00_4298;
																																							BgL_tmpz00_4298
																																								=
																																								CAR
																																								(BgL_cdrzd22526zd2_2053);
																																							BgL_test2594z00_4297
																																								=
																																								STRINGP
																																								(BgL_tmpz00_4298);
																																						}
																																						if (BgL_test2594z00_4297)
																																							{	/* Module/foreign.scm 337 */
																																								if (NULLP(CDR(BgL_cdrzd22526zd2_2053)))
																																									{	/* Module/foreign.scm 337 */
																																										{
																																											obj_t
																																												BgL_slotsz00_4304;
																																											BgL_slotsz00_4304
																																												=
																																												CDR
																																												(
																																												((obj_t) BgL_slotsz00_2046));
																																											BgL_slotsz00_2046
																																												=
																																												BgL_slotsz00_4304;
																																											goto
																																												BgL_zc3z04anonymousza32213ze3z87_2047;
																																										}
																																									}
																																								else
																																									{	/* Module/foreign.scm 337 */
																																										return
																																											BFALSE;
																																									}
																																							}
																																						else
																																							{	/* Module/foreign.scm 337 */
																																								return
																																									BFALSE;
																																							}
																																					}
																																				else
																																					{	/* Module/foreign.scm 337 */
																																						return
																																							BFALSE;
																																					}
																																			}
																																		else
																																			{	/* Module/foreign.scm 337 */
																																				return
																																					BFALSE;
																																			}
																																	}
																																}
																															else
																																{	/* Module/foreign.scm 337 */
																																	return BFALSE;
																																}
																														}
																													}
																											}
																										}
																									else
																										{	/* Module/foreign.scm 303 */
																											return BFALSE;
																										}
																								}
																						}
																				}
																			else
																				{	/* Module/foreign.scm 303 */
																					obj_t BgL_cdrzd22389zd2_1971;

																					BgL_cdrzd22389zd2_1971 =
																						CDR(((obj_t) BgL_tzd2expzd2_36));
																					if (
																						(CAR(
																								((obj_t) BgL_tzd2expzd2_36)) ==
																							CNST_TABLE_REF(16)))
																						{	/* Module/foreign.scm 303 */
																							bool_t BgL_test2597z00_4314;

																							{	/* Module/foreign.scm 303 */
																								obj_t BgL_tmpz00_4315;

																								BgL_tmpz00_4315 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd22389zd2_1971));
																								BgL_test2597z00_4314 =
																									SYMBOLP(BgL_tmpz00_4315);
																							}
																							if (BgL_test2597z00_4314)
																								{	/* Module/foreign.scm 303 */
																									if (NULLP(CDR(
																												((obj_t)
																													BgL_cdrzd22389zd2_1971))))
																										{	/* Module/foreign.scm 303 */
																											return BgL_tzd2expzd2_36;
																										}
																									else
																										{	/* Module/foreign.scm 303 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Module/foreign.scm 303 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Module/foreign.scm 303 */
																							if (
																								(CAR(
																										((obj_t) BgL_tzd2expzd2_36))
																									== CNST_TABLE_REF(14)))
																								{	/* Module/foreign.scm 303 */
																									obj_t BgL_cdrzd22409zd2_1983;

																									BgL_cdrzd22409zd2_1983 =
																										CDR(
																										((obj_t)
																											BgL_cdrzd22389zd2_1971));
																									{	/* Module/foreign.scm 303 */
																										bool_t BgL_test2600z00_4330;

																										{	/* Module/foreign.scm 303 */
																											obj_t BgL_tmpz00_4331;

																											BgL_tmpz00_4331 =
																												CAR(
																												((obj_t)
																													BgL_cdrzd22389zd2_1971));
																											BgL_test2600z00_4330 =
																												SYMBOLP
																												(BgL_tmpz00_4331);
																										}
																										if (BgL_test2600z00_4330)
																											{	/* Module/foreign.scm 303 */
																												if (PAIRP
																													(BgL_cdrzd22409zd2_1983))
																													{	/* Module/foreign.scm 303 */
																														obj_t
																															BgL_carzd22411zd2_1987;
																														BgL_carzd22411zd2_1987
																															=
																															CAR
																															(BgL_cdrzd22409zd2_1983);
																														{

																															if (NULLP
																																(BgL_carzd22411zd2_1987))
																																{	/* Module/foreign.scm 303 */
																																BgL_kapzd22415zd2_1988:
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd22409zd2_1983))))
																																		{	/* Module/foreign.scm 303 */
																																			if (BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00(BgL_carzd22411zd2_1987))
																																				{	/* Module/foreign.scm 330 */
																																					return
																																						BgL_tzd2expzd2_36;
																																				}
																																			else
																																				{	/* Module/foreign.scm 330 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Module/foreign.scm 303 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Module/foreign.scm 303 */
																																	if (PAIRP
																																		(BgL_carzd22411zd2_1987))
																																		{	/* Module/foreign.scm 303 */
																																			goto
																																				BgL_kapzd22415zd2_1988;
																																		}
																																	else
																																		{	/* Module/foreign.scm 303 */
																																			return
																																				BFALSE;
																																		}
																																}
																														}
																													}
																												else
																													{	/* Module/foreign.scm 303 */
																														return BFALSE;
																													}
																											}
																										else
																											{	/* Module/foreign.scm 303 */
																												return BFALSE;
																											}
																									}
																								}
																							else
																								{	/* Module/foreign.scm 303 */
																									if (
																										(CAR(
																												((obj_t)
																													BgL_tzd2expzd2_36)) ==
																											CNST_TABLE_REF(15)))
																										{
																											obj_t BgL_slotsz00_4353;

																											BgL_slotsz00_4353 =
																												CDR(
																												((obj_t)
																													BgL_tzd2expzd2_36));
																											BgL_slotsz00_1877 =
																												BgL_slotsz00_4353;
																											goto
																												BgL_tagzd22284zd2_1878;
																										}
																									else
																										{	/* Module/foreign.scm 303 */
																											return BFALSE;
																										}
																								}
																						}
																				}
																		}
																	else
																		{	/* Module/foreign.scm 303 */
																			if (
																				(CAR(
																						((obj_t) BgL_tzd2expzd2_36)) ==
																					CNST_TABLE_REF(15)))
																				{
																					obj_t BgL_slotsz00_4361;

																					BgL_slotsz00_4361 =
																						CDR(((obj_t) BgL_tzd2expzd2_36));
																					BgL_slotsz00_1877 = BgL_slotsz00_4361;
																					goto BgL_tagzd22284zd2_1878;
																				}
																			else
																				{	/* Module/foreign.scm 303 */
																					if (
																						(CAR(
																								((obj_t) BgL_tzd2expzd2_36)) ==
																							CNST_TABLE_REF(12)))
																						{	/* Module/foreign.scm 303 */
																							if (NULLP(CDR(
																										((obj_t)
																											BgL_tzd2expzd2_36))))
																								{	/* Module/foreign.scm 303 */
																									return BgL_tzd2expzd2_36;
																								}
																							else
																								{	/* Module/foreign.scm 303 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Module/foreign.scm 303 */
																							return BFALSE;
																						}
																				}
																		}
																}
															else
																{	/* Module/foreign.scm 303 */
																	if (
																		(BgL_carzd22351zd2_1904 ==
																			CNST_TABLE_REF(18)))
																		{	/* Module/foreign.scm 303 */
																			goto BgL_kapzd22353zd2_1906;
																		}
																	else
																		{	/* Module/foreign.scm 303 */
																			if (
																				(CAR(
																						((obj_t) BgL_tzd2expzd2_36)) ==
																					CNST_TABLE_REF(16)))
																				{	/* Module/foreign.scm 303 */
																					if (PAIRP(BgL_cdrzd22352zd2_1905))
																						{	/* Module/foreign.scm 303 */
																							bool_t BgL_test2613z00_4383;

																							{	/* Module/foreign.scm 303 */
																								obj_t BgL_tmpz00_4384;

																								BgL_tmpz00_4384 =
																									CAR(BgL_cdrzd22352zd2_1905);
																								BgL_test2613z00_4383 =
																									SYMBOLP(BgL_tmpz00_4384);
																							}
																							if (BgL_test2613z00_4383)
																								{	/* Module/foreign.scm 303 */
																									if (NULLP(CDR
																											(BgL_cdrzd22352zd2_1905)))
																										{	/* Module/foreign.scm 303 */
																											return BgL_tzd2expzd2_36;
																										}
																									else
																										{	/* Module/foreign.scm 303 */
																											return BFALSE;
																										}
																								}
																							else
																								{	/* Module/foreign.scm 303 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Module/foreign.scm 303 */
																							return BFALSE;
																						}
																				}
																			else
																				{	/* Module/foreign.scm 303 */
																					obj_t BgL_cdrzd22467zd2_1917;

																					BgL_cdrzd22467zd2_1917 =
																						CDR(((obj_t) BgL_tzd2expzd2_36));
																					if (
																						(CAR(
																								((obj_t) BgL_tzd2expzd2_36)) ==
																							CNST_TABLE_REF(14)))
																						{	/* Module/foreign.scm 303 */
																							if (PAIRP(BgL_cdrzd22467zd2_1917))
																								{	/* Module/foreign.scm 303 */
																									obj_t BgL_cdrzd22470zd2_1921;

																									BgL_cdrzd22470zd2_1921 =
																										CDR(BgL_cdrzd22467zd2_1917);
																									{	/* Module/foreign.scm 303 */
																										bool_t BgL_test2617z00_4400;

																										{	/* Module/foreign.scm 303 */
																											obj_t BgL_tmpz00_4401;

																											BgL_tmpz00_4401 =
																												CAR
																												(BgL_cdrzd22467zd2_1917);
																											BgL_test2617z00_4400 =
																												SYMBOLP
																												(BgL_tmpz00_4401);
																										}
																										if (BgL_test2617z00_4400)
																											{	/* Module/foreign.scm 303 */
																												if (PAIRP
																													(BgL_cdrzd22470zd2_1921))
																													{	/* Module/foreign.scm 303 */
																														obj_t
																															BgL_carzd22472zd2_1925;
																														BgL_carzd22472zd2_1925
																															=
																															CAR
																															(BgL_cdrzd22470zd2_1921);
																														{

																															if (NULLP
																																(BgL_carzd22472zd2_1925))
																																{	/* Module/foreign.scm 303 */
																																BgL_kapzd22476zd2_1926:
																																	if (NULLP(CDR(
																																				((obj_t)
																																					BgL_cdrzd22470zd2_1921))))
																																		{	/* Module/foreign.scm 303 */
																																			if (BGl_checkzd2czd2argszf3zf3zzmodule_foreignz00(BgL_carzd22472zd2_1925))
																																				{	/* Module/foreign.scm 330 */
																																					return
																																						BgL_tzd2expzd2_36;
																																				}
																																			else
																																				{	/* Module/foreign.scm 330 */
																																					return
																																						BFALSE;
																																				}
																																		}
																																	else
																																		{	/* Module/foreign.scm 303 */
																																			return
																																				BFALSE;
																																		}
																																}
																															else
																																{	/* Module/foreign.scm 303 */
																																	if (PAIRP
																																		(BgL_carzd22472zd2_1925))
																																		{	/* Module/foreign.scm 303 */
																																			goto
																																				BgL_kapzd22476zd2_1926;
																																		}
																																	else
																																		{	/* Module/foreign.scm 303 */
																																			return
																																				BFALSE;
																																		}
																																}
																														}
																													}
																												else
																													{	/* Module/foreign.scm 303 */
																														return BFALSE;
																													}
																											}
																										else
																											{	/* Module/foreign.scm 303 */
																												return BFALSE;
																											}
																									}
																								}
																							else
																								{	/* Module/foreign.scm 303 */
																									return BFALSE;
																								}
																						}
																					else
																						{	/* Module/foreign.scm 303 */
																							if (
																								(CAR(
																										((obj_t) BgL_tzd2expzd2_36))
																									== CNST_TABLE_REF(15)))
																								{
																									obj_t BgL_slotsz00_4422;

																									BgL_slotsz00_4422 =
																										BgL_cdrzd22467zd2_1917;
																									BgL_slotsz00_1877 =
																										BgL_slotsz00_4422;
																									goto BgL_tagzd22284zd2_1878;
																								}
																							else
																								{	/* Module/foreign.scm 303 */
																									if (
																										(CAR(
																												((obj_t)
																													BgL_tzd2expzd2_36)) ==
																											CNST_TABLE_REF(12)))
																										{	/* Module/foreign.scm 303 */
																											if (NULLP
																												(BgL_cdrzd22467zd2_1917))
																												{	/* Module/foreign.scm 303 */
																													return
																														BgL_tzd2expzd2_36;
																												}
																											else
																												{	/* Module/foreign.scm 303 */
																													return BFALSE;
																												}
																										}
																									else
																										{	/* Module/foreign.scm 303 */
																											return BFALSE;
																										}
																								}
																						}
																				}
																		}
																}
														}
													}
											}
									}
							}
						else
							{	/* Module/foreign.scm 303 */
								return BFALSE;
							}
					}
			}
		}

	}



/* foreign-accesses-add! */
	BGL_EXPORTED_DEF obj_t
		BGl_foreignzd2accesseszd2addz12z12zzmodule_foreignz00(obj_t
		BgL_accessesz00_37)
	{
		{	/* Module/foreign.scm 360 */
			return (BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00 =
				BGl_appendzd221011zd2zzmodule_foreignz00(BgL_accessesz00_37,
					BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00), BUNSPEC);
		}

	}



/* &foreign-accesses-add! */
	obj_t BGl_z62foreignzd2accesseszd2addz12z70zzmodule_foreignz00(obj_t
		BgL_envz00_2906, obj_t BgL_accessesz00_2907)
	{
		{	/* Module/foreign.scm 360 */
			return
				BGl_foreignzd2accesseszd2addz12z12zzmodule_foreignz00
				(BgL_accessesz00_2907);
		}

	}



/* &foreign-finalizer */
	obj_t BGl_z62foreignzd2finaliza7erz17zzmodule_foreignz00(obj_t
		BgL_envz00_2895)
	{
		{	/* Module/foreign.scm 366 */
			{
				obj_t BgL_l1143z00_2926;

				BgL_l1143z00_2926 = BGl_za2foreignzd2exportedza2zd2zzmodule_foreignz00;
			BgL_zc3z04anonymousza32227ze3z87_2925:
				if (PAIRP(BgL_l1143z00_2926))
					{	/* Module/foreign.scm 368 */
						{	/* Module/foreign.scm 369 */
							obj_t BgL_foreignz00_2927;

							BgL_foreignz00_2927 = CAR(BgL_l1143z00_2926);
							{	/* Module/foreign.scm 369 */
								obj_t BgL_foz00_2928;

								BgL_foz00_2928 = CAR(((obj_t) BgL_foreignz00_2927));
								{	/* Module/foreign.scm 369 */
									obj_t BgL_exz00_2929;

									BgL_exz00_2929 = CDR(((obj_t) BgL_foreignz00_2927));
									{	/* Module/foreign.scm 370 */
										obj_t BgL_globalz00_2930;

										{	/* Module/foreign.scm 371 */
											obj_t BgL_arg2238z00_2931;

											{	/* Module/foreign.scm 371 */
												obj_t BgL_pairz00_2932;

												BgL_pairz00_2932 = CDR(((obj_t) BgL_foz00_2928));
												BgL_arg2238z00_2931 = CAR(BgL_pairz00_2932);
											}
											BgL_globalz00_2930 =
												BGl_findzd2globalzd2zzast_envz00(BgL_arg2238z00_2931,
												BNIL);
										}
										{	/* Module/foreign.scm 371 */
											obj_t BgL_namez00_2933;

											{	/* Module/foreign.scm 372 */
												obj_t BgL_pairz00_2934;

												{	/* Module/foreign.scm 372 */
													obj_t BgL_pairz00_2935;

													BgL_pairz00_2935 = CDR(((obj_t) BgL_foz00_2928));
													BgL_pairz00_2934 = CDR(BgL_pairz00_2935);
												}
												BgL_namez00_2933 = CAR(BgL_pairz00_2934);
											}
											{	/* Module/foreign.scm 372 */

												{	/* Module/foreign.scm 374 */
													bool_t BgL_test2627z00_4447;

													{	/* Module/foreign.scm 374 */
														obj_t BgL_classz00_2936;

														BgL_classz00_2936 = BGl_globalz00zzast_varz00;
														if (BGL_OBJECTP(BgL_globalz00_2930))
															{	/* Module/foreign.scm 374 */
																BgL_objectz00_bglt BgL_arg1807z00_2937;

																BgL_arg1807z00_2937 =
																	(BgL_objectz00_bglt) (BgL_globalz00_2930);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Module/foreign.scm 374 */
																		long BgL_idxz00_2938;

																		BgL_idxz00_2938 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2937);
																		BgL_test2627z00_4447 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2938 + 2L)) ==
																			BgL_classz00_2936);
																	}
																else
																	{	/* Module/foreign.scm 374 */
																		bool_t BgL_res2281z00_2941;

																		{	/* Module/foreign.scm 374 */
																			obj_t BgL_oclassz00_2942;

																			{	/* Module/foreign.scm 374 */
																				obj_t BgL_arg1815z00_2943;
																				long BgL_arg1816z00_2944;

																				BgL_arg1815z00_2943 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Module/foreign.scm 374 */
																					long BgL_arg1817z00_2945;

																					BgL_arg1817z00_2945 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2937);
																					BgL_arg1816z00_2944 =
																						(BgL_arg1817z00_2945 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2942 =
																					VECTOR_REF(BgL_arg1815z00_2943,
																					BgL_arg1816z00_2944);
																			}
																			{	/* Module/foreign.scm 374 */
																				bool_t BgL__ortest_1115z00_2946;

																				BgL__ortest_1115z00_2946 =
																					(BgL_classz00_2936 ==
																					BgL_oclassz00_2942);
																				if (BgL__ortest_1115z00_2946)
																					{	/* Module/foreign.scm 374 */
																						BgL_res2281z00_2941 =
																							BgL__ortest_1115z00_2946;
																					}
																				else
																					{	/* Module/foreign.scm 374 */
																						long BgL_odepthz00_2947;

																						{	/* Module/foreign.scm 374 */
																							obj_t BgL_arg1804z00_2948;

																							BgL_arg1804z00_2948 =
																								(BgL_oclassz00_2942);
																							BgL_odepthz00_2947 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2948);
																						}
																						if ((2L < BgL_odepthz00_2947))
																							{	/* Module/foreign.scm 374 */
																								obj_t BgL_arg1802z00_2949;

																								{	/* Module/foreign.scm 374 */
																									obj_t BgL_arg1803z00_2950;

																									BgL_arg1803z00_2950 =
																										(BgL_oclassz00_2942);
																									BgL_arg1802z00_2949 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2950, 2L);
																								}
																								BgL_res2281z00_2941 =
																									(BgL_arg1802z00_2949 ==
																									BgL_classz00_2936);
																							}
																						else
																							{	/* Module/foreign.scm 374 */
																								BgL_res2281z00_2941 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2627z00_4447 = BgL_res2281z00_2941;
																	}
															}
														else
															{	/* Module/foreign.scm 374 */
																BgL_test2627z00_4447 = ((bool_t) 0);
															}
													}
													if (BgL_test2627z00_4447)
														{	/* Module/foreign.scm 383 */
															bool_t BgL_test2632z00_4470;

															{	/* Module/foreign.scm 383 */
																obj_t BgL_tmpz00_4471;

																BgL_tmpz00_4471 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_globalz00_2930))))->BgL_namez00);
																BgL_test2632z00_4470 = STRINGP(BgL_tmpz00_4471);
															}
															if (BgL_test2632z00_4470)
																{	/* Module/foreign.scm 383 */
																	BGl_userzd2warningzd2zztools_errorz00
																		(BGl_string2302z00zzmodule_foreignz00,
																		BGl_string2303z00zzmodule_foreignz00,
																		BgL_foreignz00_2927);
																}
															else
																{	/* Module/foreign.scm 383 */
																	((((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_globalz00_2930))))->
																			BgL_namez00) =
																		((obj_t) BgL_namez00_2933), BUNSPEC);
																}
														}
													else
														{	/* Module/foreign.scm 374 */
															if (CBOOL(BgL_exz00_2929))
																{	/* Module/foreign.scm 376 */
																	bool_t BgL_test2634z00_4482;

																	if (
																		(BGl_za2passza2z00zzengine_paramz00 ==
																			CNST_TABLE_REF(19)))
																		{	/* Module/foreign.scm 376 */
																			BgL_test2634z00_4482 = ((bool_t) 1);
																		}
																	else
																		{	/* Module/foreign.scm 376 */
																			BgL_test2634z00_4482 =
																				(BGl_za2passza2z00zzengine_paramz00 ==
																				CNST_TABLE_REF(20));
																		}
																	if (BgL_test2634z00_4482)
																		{	/* Module/foreign.scm 376 */
																			BFALSE;
																		}
																	else
																		{	/* Module/foreign.scm 380 */
																			obj_t BgL_arg2234z00_2951;

																			{	/* Module/foreign.scm 380 */
																				obj_t BgL_arg2236z00_2952;

																				{	/* Module/foreign.scm 380 */
																					obj_t BgL_pairz00_2953;

																					BgL_pairz00_2953 =
																						CDR(((obj_t) BgL_foz00_2928));
																					BgL_arg2236z00_2952 =
																						CAR(BgL_pairz00_2953);
																				}
																				{	/* Module/foreign.scm 379 */
																					obj_t BgL_list2237z00_2954;

																					BgL_list2237z00_2954 =
																						MAKE_YOUNG_PAIR(BgL_arg2236z00_2952,
																						BNIL);
																					BgL_arg2234z00_2951 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string2304z00zzmodule_foreignz00,
																						BgL_list2237z00_2954);
																				}
																			}
																			{	/* Module/foreign.scm 378 */
																				obj_t BgL_list2235z00_2955;

																				BgL_list2235z00_2955 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				BGl_userzd2errorzd2zztools_errorz00
																					(BGl_string2302z00zzmodule_foreignz00,
																					BgL_arg2234z00_2951,
																					BgL_foreignz00_2927,
																					BgL_list2235z00_2955);
																			}
																		}
																}
															else
																{	/* Module/foreign.scm 375 */
																	BFALSE;
																}
														}
												}
											}
										}
									}
								}
							}
						}
						{
							obj_t BgL_l1143z00_4495;

							BgL_l1143z00_4495 = CDR(BgL_l1143z00_2926);
							BgL_l1143z00_2926 = BgL_l1143z00_4495;
							goto BgL_zc3z04anonymousza32227ze3z87_2925;
						}
					}
				else
					{	/* Module/foreign.scm 368 */
						((bool_t) 1);
					}
			}
			BGl_za2foreignzd2exportedza2zd2zzmodule_foreignz00 = BNIL;
			if (NULLP(BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00))
				{	/* Module/foreign.scm 393 */
					return CNST_TABLE_REF(2);
				}
			else
				{	/* Module/foreign.scm 395 */
					obj_t BgL_accessesz00_2956;

					BgL_accessesz00_2956 =
						bgl_reverse_bang
						(BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00);
					BGl_za2foreignzd2accessesza2zd2zzmodule_foreignz00 = BNIL;
					{	/* Module/foreign.scm 397 */
						obj_t BgL_arg2242z00_2957;

						{	/* Module/foreign.scm 397 */
							obj_t BgL_idz00_2958;

							BgL_idz00_2958 = CNST_TABLE_REF(0);
							{	/* Module/foreign.scm 397 */
								obj_t BgL_newz00_2959;

								BgL_newz00_2959 = create_struct(CNST_TABLE_REF(21), (int) (5L));
								{	/* Module/foreign.scm 397 */
									int BgL_tmpz00_4505;

									BgL_tmpz00_4505 = (int) (4L);
									STRUCT_SET(BgL_newz00_2959, BgL_tmpz00_4505, BFALSE);
								}
								{	/* Module/foreign.scm 397 */
									int BgL_tmpz00_4508;

									BgL_tmpz00_4508 = (int) (3L);
									STRUCT_SET(BgL_newz00_2959, BgL_tmpz00_4508, BTRUE);
								}
								{	/* Module/foreign.scm 397 */
									int BgL_tmpz00_4511;

									BgL_tmpz00_4511 = (int) (2L);
									STRUCT_SET(BgL_newz00_2959, BgL_tmpz00_4511,
										BgL_accessesz00_2956);
								}
								{	/* Module/foreign.scm 397 */
									obj_t BgL_auxz00_4516;
									int BgL_tmpz00_4514;

									BgL_auxz00_4516 = BINT(48L);
									BgL_tmpz00_4514 = (int) (1L);
									STRUCT_SET(BgL_newz00_2959, BgL_tmpz00_4514, BgL_auxz00_4516);
								}
								{	/* Module/foreign.scm 397 */
									int BgL_tmpz00_4519;

									BgL_tmpz00_4519 = (int) (0L);
									STRUCT_SET(BgL_newz00_2959, BgL_tmpz00_4519, BgL_idz00_2958);
								}
								BgL_arg2242z00_2957 = BgL_newz00_2959;
						}}
						{	/* Module/foreign.scm 397 */
							obj_t BgL_list2243z00_2960;

							BgL_list2243z00_2960 = MAKE_YOUNG_PAIR(BgL_arg2242z00_2957, BNIL);
							return BgL_list2243z00_2960;
						}
					}
				}
		}

	}



/* default-c-type */
	obj_t BGl_defaultzd2czd2typez00zzmodule_foreignz00(obj_t BgL_typez00_38,
		obj_t BgL_srcz00_39)
	{
		{	/* Module/foreign.scm 402 */
			{	/* Module/foreign.scm 403 */
				bool_t BgL_test2637z00_4523;

				{	/* Module/foreign.scm 403 */
					BgL_typez00_bglt BgL_arg2248z00_2097;

					BgL_arg2248z00_2097 = BGl_getzd2defaultzd2typez00zztype_cachez00();
					BgL_test2637z00_4523 =
						(BgL_typez00_38 == ((obj_t) BgL_arg2248z00_2097));
				}
				if (BgL_test2637z00_4523)
					{	/* Module/foreign.scm 404 */
						BgL_typez00_bglt BgL_defaultz00_2094;

						BgL_defaultz00_2094 =
							BGl_getzd2defaultzd2czd2typezd2zztype_cachez00();
						BGl_userzd2warningzf2locationz20zztools_errorz00
							(BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_39),
							BGl_string2302z00zzmodule_foreignz00,
							BGl_string2305z00zzmodule_foreignz00,
							(((BgL_typez00_bglt) COBJECT(BgL_defaultz00_2094))->BgL_idz00));
						return ((obj_t) BgL_defaultz00_2094);
					}
				else
					{	/* Module/foreign.scm 403 */
						return BgL_typez00_38;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 18 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 18 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_foreignz00(void)
	{
		{	/* Module/foreign.scm 18 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzmodule_checksumz00(457474182L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzforeign_ctypez00(396356879L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2306z00zzmodule_foreignz00));
		}

	}

#ifdef __cplusplus
}
#endif
