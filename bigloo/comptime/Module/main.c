/*===========================================================================*/
/*   (Module/main.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/main.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_MAIN_TYPE_DEFINITIONS
#define BGL_MODULE_MAIN_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;


#endif													// BGL_MODULE_MAIN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62zc3z04anonymousza31103ze3ze5zzmodule_mainz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_mainz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_genericzd2initzd2zzmodule_mainz00(void);
	static obj_t BGl_objectzd2initzd2zzmodule_mainz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_EXPORTED_DECL obj_t BGl_makezd2mainzd2compilerz00zzmodule_mainz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_mainz00(void);
	static obj_t BGl_z62mainzd2consumerzb0zzmodule_mainz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_z62mainzd2producerzb0zzmodule_mainz00(obj_t, obj_t);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzmodule_mainz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzmodule_mainz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_mainz00(void);
	static obj_t BGl_duplicatezd2mainzd2errorz00zzmodule_mainz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_mainz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_mainz00(void);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2mainzd2compilerz62zzmodule_mainz00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_correctzd2mainzf3z21zzmodule_mainz00(BgL_globalz00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_za2mainza2z00zzmodule_modulez00;
	static obj_t __cnst[7];


	   
		 
		DEFINE_STRING(BGl_string1199z00zzmodule_mainz00,
		BgL_bgl_string1199za700za7za7m1207za7,
		"Illegal declaration of main function", 36);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1198z00zzmodule_mainz00,
		BgL_bgl_za762za7c3za704anonymo1208za7,
		BGl_z62zc3z04anonymousza31103ze3ze5zzmodule_mainz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2mainzd2compilerzd2envzd2zzmodule_mainz00,
		BgL_bgl_za762makeza7d2mainza7d1209za7,
		BGl_z62makezd2mainzd2compilerz62zzmodule_mainz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_mainzd2consumerzd2envz00zzmodule_mainz00,
		BgL_bgl_za762mainza7d2consum1210z00,
		BGl_z62mainzd2consumerzb0zzmodule_mainz00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_mainzd2producerzd2envz00zzmodule_mainz00,
		BgL_bgl_za762mainza7d2produc1211z00,
		BGl_z62mainzd2producerzb0zzmodule_mainz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1200z00zzmodule_mainz00,
		BgL_bgl_string1200za700za7za7m1212za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1201z00zzmodule_mainz00,
		BgL_bgl_string1201za700za7za7m1213za7, "Illegal \"main\" clause", 21);
	      DEFINE_STRING(BGl_string1202z00zzmodule_mainz00,
		BgL_bgl_string1202za700za7za7m1214za7, "Duplicated main clause", 22);
	      DEFINE_STRING(BGl_string1203z00zzmodule_mainz00,
		BgL_bgl_string1203za700za7za7m1215za7, "module_main", 11);
	      DEFINE_STRING(BGl_string1204z00zzmodule_mainz00,
		BgL_bgl_string1204za700za7za7m1216za7,
		"argv::pair export argv::obj bdb imported void main ", 51);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzmodule_mainz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_mainz00(long
		BgL_checksumz00_947, char *BgL_fromz00_948)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_mainz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_mainz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_mainz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_mainz00();
					BGl_cnstzd2initzd2zzmodule_mainz00();
					BGl_importedzd2moduleszd2initz00zzmodule_mainz00();
					return BGl_methodzd2initzd2zzmodule_mainz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_mainz00(void)
	{
		{	/* Module/main.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_main");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_main");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_main");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "module_main");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "module_main");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "module_main");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_main");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "module_main");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_main");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_mainz00(void)
	{
		{	/* Module/main.scm 15 */
			{	/* Module/main.scm 15 */
				obj_t BgL_cportz00_871;

				{	/* Module/main.scm 15 */
					obj_t BgL_stringz00_878;

					BgL_stringz00_878 = BGl_string1204z00zzmodule_mainz00;
					{	/* Module/main.scm 15 */
						obj_t BgL_startz00_879;

						BgL_startz00_879 = BINT(0L);
						{	/* Module/main.scm 15 */
							obj_t BgL_endz00_880;

							BgL_endz00_880 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_878)));
							{	/* Module/main.scm 15 */

								BgL_cportz00_871 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_878, BgL_startz00_879, BgL_endz00_880);
				}}}}
				{
					long BgL_iz00_872;

					BgL_iz00_872 = 6L;
				BgL_loopz00_873:
					if ((BgL_iz00_872 == -1L))
						{	/* Module/main.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/main.scm 15 */
							{	/* Module/main.scm 15 */
								obj_t BgL_arg1206z00_874;

								{	/* Module/main.scm 15 */

									{	/* Module/main.scm 15 */
										obj_t BgL_locationz00_876;

										BgL_locationz00_876 = BBOOL(((bool_t) 0));
										{	/* Module/main.scm 15 */

											BgL_arg1206z00_874 =
												BGl_readz00zz__readerz00(BgL_cportz00_871,
												BgL_locationz00_876);
										}
									}
								}
								{	/* Module/main.scm 15 */
									int BgL_tmpz00_975;

									BgL_tmpz00_975 = (int) (BgL_iz00_872);
									CNST_TABLE_SET(BgL_tmpz00_975, BgL_arg1206z00_874);
							}}
							{	/* Module/main.scm 15 */
								int BgL_auxz00_877;

								BgL_auxz00_877 = (int) ((BgL_iz00_872 - 1L));
								{
									long BgL_iz00_980;

									BgL_iz00_980 = (long) (BgL_auxz00_877);
									BgL_iz00_872 = BgL_iz00_980;
									goto BgL_loopz00_873;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_mainz00(void)
	{
		{	/* Module/main.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* make-main-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2mainzd2compilerz00zzmodule_mainz00(void)
	{
		{	/* Module/main.scm 33 */
			{	/* Module/main.scm 34 */
				BgL_ccompz00_bglt BgL_new1063z00_553;

				{	/* Module/main.scm 35 */
					BgL_ccompz00_bglt BgL_new1062z00_555;

					BgL_new1062z00_555 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/main.scm 35 */
						long BgL_arg1104z00_556;

						BgL_arg1104z00_556 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1062z00_555), BgL_arg1104z00_556);
					}
					BgL_new1063z00_553 = BgL_new1062z00_555;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1063z00_553))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1063z00_553))->BgL_producerz00) =
					((obj_t) BGl_mainzd2producerzd2envz00zzmodule_mainz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1063z00_553))->BgL_consumerz00) =
					((obj_t) BGl_mainzd2consumerzd2envz00zzmodule_mainz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1063z00_553))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_proc1198z00zzmodule_mainz00), BUNSPEC);
				return ((obj_t) BgL_new1063z00_553);
			}
		}

	}



/* &make-main-compiler */
	obj_t BGl_z62makezd2mainzd2compilerz62zzmodule_mainz00(obj_t BgL_envz00_864)
	{
		{	/* Module/main.scm 33 */
			return BGl_makezd2mainzd2compilerz00zzmodule_mainz00();
		}

	}



/* &<@anonymous:1103> */
	obj_t BGl_z62zc3z04anonymousza31103ze3ze5zzmodule_mainz00(obj_t
		BgL_envz00_865)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(1);
		}

	}



/* correct-main? */
	bool_t BGl_correctzd2mainzf3z21zzmodule_mainz00(BgL_globalz00_bglt
		BgL_globalz00_3)
	{
		{	/* Module/main.scm 45 */
			{	/* Module/main.scm 46 */
				BgL_valuez00_bglt BgL_sfunz00_557;

				BgL_sfunz00_557 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_globalz00_3)))->BgL_valuez00);
				{	/* Module/main.scm 47 */
					bool_t BgL_test1219z00_997;

					{	/* Module/main.scm 47 */
						obj_t BgL_classz00_646;

						BgL_classz00_646 = BGl_sfunz00zzast_varz00;
						{	/* Module/main.scm 47 */
							BgL_objectz00_bglt BgL_arg1807z00_648;

							{	/* Module/main.scm 47 */
								obj_t BgL_tmpz00_998;

								BgL_tmpz00_998 =
									((obj_t) ((BgL_objectz00_bglt) BgL_sfunz00_557));
								BgL_arg1807z00_648 = (BgL_objectz00_bglt) (BgL_tmpz00_998);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Module/main.scm 47 */
									long BgL_idxz00_654;

									BgL_idxz00_654 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_648);
									BgL_test1219z00_997 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_654 + 3L)) == BgL_classz00_646);
								}
							else
								{	/* Module/main.scm 47 */
									bool_t BgL_res1192z00_679;

									{	/* Module/main.scm 47 */
										obj_t BgL_oclassz00_662;

										{	/* Module/main.scm 47 */
											obj_t BgL_arg1815z00_670;
											long BgL_arg1816z00_671;

											BgL_arg1815z00_670 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Module/main.scm 47 */
												long BgL_arg1817z00_672;

												BgL_arg1817z00_672 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_648);
												BgL_arg1816z00_671 = (BgL_arg1817z00_672 - OBJECT_TYPE);
											}
											BgL_oclassz00_662 =
												VECTOR_REF(BgL_arg1815z00_670, BgL_arg1816z00_671);
										}
										{	/* Module/main.scm 47 */
											bool_t BgL__ortest_1115z00_663;

											BgL__ortest_1115z00_663 =
												(BgL_classz00_646 == BgL_oclassz00_662);
											if (BgL__ortest_1115z00_663)
												{	/* Module/main.scm 47 */
													BgL_res1192z00_679 = BgL__ortest_1115z00_663;
												}
											else
												{	/* Module/main.scm 47 */
													long BgL_odepthz00_664;

													{	/* Module/main.scm 47 */
														obj_t BgL_arg1804z00_665;

														BgL_arg1804z00_665 = (BgL_oclassz00_662);
														BgL_odepthz00_664 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_665);
													}
													if ((3L < BgL_odepthz00_664))
														{	/* Module/main.scm 47 */
															obj_t BgL_arg1802z00_667;

															{	/* Module/main.scm 47 */
																obj_t BgL_arg1803z00_668;

																BgL_arg1803z00_668 = (BgL_oclassz00_662);
																BgL_arg1802z00_667 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_668,
																	3L);
															}
															BgL_res1192z00_679 =
																(BgL_arg1802z00_667 == BgL_classz00_646);
														}
													else
														{	/* Module/main.scm 47 */
															BgL_res1192z00_679 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1219z00_997 = BgL_res1192z00_679;
								}
						}
					}
					if (BgL_test1219z00_997)
						{	/* Module/main.scm 47 */
							if (
								((((BgL_funz00_bglt) COBJECT(
												((BgL_funz00_bglt)
													((BgL_sfunz00_bglt) BgL_sfunz00_557))))->
										BgL_arityz00) == 1L))
								{	/* Module/main.scm 49 */
									obj_t BgL_argsz00_560;

									{	/* Module/main.scm 49 */
										obj_t BgL_pairz00_683;

										BgL_pairz00_683 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_sfunz00_557)))->BgL_argsz00);
										BgL_argsz00_560 = CAR(BgL_pairz00_683);
									}
									{	/* Module/main.scm 49 */
										BgL_typez00_bglt BgL_typez00_561;

										BgL_typez00_561 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_argsz00_560))))->
											BgL_typez00);
										{	/* Module/main.scm 50 */

											{	/* Module/main.scm 51 */
												bool_t BgL__ortest_1066z00_562;

												BgL__ortest_1066z00_562 =
													(
													((obj_t) BgL_typez00_561) ==
													BGl_za2objza2z00zztype_cachez00);
												if (BgL__ortest_1066z00_562)
													{	/* Module/main.scm 51 */
														return BgL__ortest_1066z00_562;
													}
												else
													{	/* Module/main.scm 51 */
														return
															(
															((obj_t) BgL_typez00_561) ==
															BGl_za2pairza2z00zztype_cachez00);
													}
											}
										}
									}
								}
							else
								{	/* Module/main.scm 48 */
									return ((bool_t) 0);
								}
						}
					else
						{	/* Module/main.scm 47 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* &main-producer */
	obj_t BGl_z62mainzd2producerzb0zzmodule_mainz00(obj_t BgL_envz00_866,
		obj_t BgL_clausez00_867)
	{
		{	/* Module/main.scm 56 */
			{	/* Module/main.scm 57 */
				bool_t BgL_test1225z00_1037;

				if ((BGl_za2mainza2z00zzmodule_modulez00 == CNST_TABLE_REF(2)))
					{	/* Module/main.scm 57 */
						BgL_test1225z00_1037 = ((bool_t) 1);
					}
				else
					{	/* Module/main.scm 57 */
						obj_t BgL_objz00_882;

						BgL_objz00_882 = BGl_za2mainza2z00zzmodule_modulez00;
						{	/* Module/main.scm 57 */
							obj_t BgL_classz00_883;

							BgL_classz00_883 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_objz00_882))
								{	/* Module/main.scm 57 */
									BgL_objectz00_bglt BgL_arg1807z00_884;

									BgL_arg1807z00_884 = (BgL_objectz00_bglt) (BgL_objz00_882);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Module/main.scm 57 */
											long BgL_idxz00_885;

											BgL_idxz00_885 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_884);
											BgL_test1225z00_1037 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_885 + 2L)) == BgL_classz00_883);
										}
									else
										{	/* Module/main.scm 57 */
											bool_t BgL_res1193z00_888;

											{	/* Module/main.scm 57 */
												obj_t BgL_oclassz00_889;

												{	/* Module/main.scm 57 */
													obj_t BgL_arg1815z00_890;
													long BgL_arg1816z00_891;

													BgL_arg1815z00_890 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Module/main.scm 57 */
														long BgL_arg1817z00_892;

														BgL_arg1817z00_892 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_884);
														BgL_arg1816z00_891 =
															(BgL_arg1817z00_892 - OBJECT_TYPE);
													}
													BgL_oclassz00_889 =
														VECTOR_REF(BgL_arg1815z00_890, BgL_arg1816z00_891);
												}
												{	/* Module/main.scm 57 */
													bool_t BgL__ortest_1115z00_893;

													BgL__ortest_1115z00_893 =
														(BgL_classz00_883 == BgL_oclassz00_889);
													if (BgL__ortest_1115z00_893)
														{	/* Module/main.scm 57 */
															BgL_res1193z00_888 = BgL__ortest_1115z00_893;
														}
													else
														{	/* Module/main.scm 57 */
															long BgL_odepthz00_894;

															{	/* Module/main.scm 57 */
																obj_t BgL_arg1804z00_895;

																BgL_arg1804z00_895 = (BgL_oclassz00_889);
																BgL_odepthz00_894 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_895);
															}
															if ((2L < BgL_odepthz00_894))
																{	/* Module/main.scm 57 */
																	obj_t BgL_arg1802z00_896;

																	{	/* Module/main.scm 57 */
																		obj_t BgL_arg1803z00_897;

																		BgL_arg1803z00_897 = (BgL_oclassz00_889);
																		BgL_arg1802z00_896 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_897, 2L);
																	}
																	BgL_res1193z00_888 =
																		(BgL_arg1802z00_896 == BgL_classz00_883);
																}
															else
																{	/* Module/main.scm 57 */
																	BgL_res1193z00_888 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1225z00_1037 = BgL_res1193z00_888;
										}
								}
							else
								{	/* Module/main.scm 57 */
									BgL_test1225z00_1037 = ((bool_t) 0);
								}
						}
					}
				if (BgL_test1225z00_1037)
					{	/* Module/main.scm 57 */
						return
							BGl_duplicatezd2mainzd2errorz00zzmodule_mainz00
							(BgL_clausez00_867);
					}
				else
					{
						obj_t BgL_mainz00_900;

						if (PAIRP(BgL_clausez00_867))
							{	/* Module/main.scm 59 */
								obj_t BgL_cdrzd2107zd2_929;

								BgL_cdrzd2107zd2_929 = CDR(((obj_t) BgL_clausez00_867));
								if (PAIRP(BgL_cdrzd2107zd2_929))
									{	/* Module/main.scm 59 */
										obj_t BgL_carzd2109zd2_930;

										BgL_carzd2109zd2_930 = CAR(BgL_cdrzd2107zd2_929);
										if (SYMBOLP(BgL_carzd2109zd2_930))
											{	/* Module/main.scm 59 */
												if (NULLP(CDR(BgL_cdrzd2107zd2_929)))
													{	/* Module/main.scm 59 */
														BgL_mainz00_900 = BgL_carzd2109zd2_930;
														{	/* Module/main.scm 61 */
															obj_t BgL_globalz00_901;

															BgL_globalz00_901 =
																BGl_findzd2globalzf2modulez20zzast_envz00
																(BgL_mainz00_900,
																BGl_za2moduleza2z00zzmodule_modulez00);
															{	/* Module/main.scm 62 */
																bool_t BgL_test1236z00_1077;

																{	/* Module/main.scm 62 */
																	obj_t BgL_classz00_902;

																	BgL_classz00_902 = BGl_globalz00zzast_varz00;
																	if (BGL_OBJECTP(BgL_globalz00_901))
																		{	/* Module/main.scm 62 */
																			BgL_objectz00_bglt BgL_arg1807z00_903;

																			BgL_arg1807z00_903 =
																				(BgL_objectz00_bglt)
																				(BgL_globalz00_901);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Module/main.scm 62 */
																					long BgL_idxz00_904;

																					BgL_idxz00_904 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_903);
																					BgL_test1236z00_1077 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_904 + 2L)) ==
																						BgL_classz00_902);
																				}
																			else
																				{	/* Module/main.scm 62 */
																					bool_t BgL_res1194z00_907;

																					{	/* Module/main.scm 62 */
																						obj_t BgL_oclassz00_908;

																						{	/* Module/main.scm 62 */
																							obj_t BgL_arg1815z00_909;
																							long BgL_arg1816z00_910;

																							BgL_arg1815z00_909 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Module/main.scm 62 */
																								long BgL_arg1817z00_911;

																								BgL_arg1817z00_911 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_903);
																								BgL_arg1816z00_910 =
																									(BgL_arg1817z00_911 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_908 =
																								VECTOR_REF(BgL_arg1815z00_909,
																								BgL_arg1816z00_910);
																						}
																						{	/* Module/main.scm 62 */
																							bool_t BgL__ortest_1115z00_912;

																							BgL__ortest_1115z00_912 =
																								(BgL_classz00_902 ==
																								BgL_oclassz00_908);
																							if (BgL__ortest_1115z00_912)
																								{	/* Module/main.scm 62 */
																									BgL_res1194z00_907 =
																										BgL__ortest_1115z00_912;
																								}
																							else
																								{	/* Module/main.scm 62 */
																									long BgL_odepthz00_913;

																									{	/* Module/main.scm 62 */
																										obj_t BgL_arg1804z00_914;

																										BgL_arg1804z00_914 =
																											(BgL_oclassz00_908);
																										BgL_odepthz00_913 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_914);
																									}
																									if ((2L < BgL_odepthz00_913))
																										{	/* Module/main.scm 62 */
																											obj_t BgL_arg1802z00_915;

																											{	/* Module/main.scm 62 */
																												obj_t
																													BgL_arg1803z00_916;
																												BgL_arg1803z00_916 =
																													(BgL_oclassz00_908);
																												BgL_arg1802z00_915 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_916,
																													2L);
																											}
																											BgL_res1194z00_907 =
																												(BgL_arg1802z00_915 ==
																												BgL_classz00_902);
																										}
																									else
																										{	/* Module/main.scm 62 */
																											BgL_res1194z00_907 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1236z00_1077 =
																						BgL_res1194z00_907;
																				}
																		}
																	else
																		{	/* Module/main.scm 62 */
																			BgL_test1236z00_1077 = ((bool_t) 0);
																		}
																}
																if (BgL_test1236z00_1077)
																	{	/* Module/main.scm 62 */
																		if (BGl_correctzd2mainzf3z21zzmodule_mainz00
																			(((BgL_globalz00_bglt)
																					BgL_globalz00_901)))
																			{	/* Module/main.scm 63 */
																				return BFALSE;
																			}
																		else
																			{	/* Module/main.scm 64 */
																				obj_t BgL_list1128z00_917;

																				BgL_list1128z00_917 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				return
																					BGl_userzd2errorzd2zztools_errorz00
																					(BGl_za2moduleza2z00zzmodule_modulez00,
																					BGl_string1199z00zzmodule_mainz00,
																					BgL_mainz00_900, BgL_list1128z00_917);
																			}
																	}
																else
																	{	/* Module/main.scm 62 */
																		{	/* Module/main.scm 67 */
																			bool_t BgL_test1243z00_1105;

																			if (
																				((long)
																					CINT
																					(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
																					> 0L))
																				{	/* Module/main.scm 69 */
																					obj_t BgL_arg1149z00_918;

																					{	/* Module/main.scm 69 */
																						obj_t BgL_arg1152z00_919;

																						BgL_arg1152z00_919 =
																							BGl_thezd2backendzd2zzbackend_backendz00
																							();
																						BgL_arg1149z00_918 =
																							(((BgL_backendz00_bglt)
																								COBJECT(((BgL_backendz00_bglt)
																										BgL_arg1152z00_919)))->
																							BgL_debugzd2supportzd2);
																					}
																					BgL_test1243z00_1105 =
																						CBOOL
																						(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																						(CNST_TABLE_REF(3),
																							BgL_arg1149z00_918));
																				}
																			else
																				{	/* Module/main.scm 67 */
																					BgL_test1243z00_1105 = ((bool_t) 0);
																				}
																			if (BgL_test1243z00_1105)
																				{	/* Module/main.scm 70 */
																					obj_t BgL_arg1137z00_920;

																					{	/* Module/main.scm 70 */
																						obj_t BgL_arg1138z00_921;

																						{	/* Module/main.scm 70 */
																							obj_t BgL_arg1140z00_922;

																							{	/* Module/main.scm 70 */
																								obj_t BgL_arg1141z00_923;

																								BgL_arg1141z00_923 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(4), BNIL);
																								BgL_arg1140z00_922 =
																									MAKE_YOUNG_PAIR
																									(BgL_mainz00_900,
																									BgL_arg1141z00_923);
																							}
																							BgL_arg1138z00_921 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1140z00_922, BNIL);
																						}
																						BgL_arg1137z00_920 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																							BgL_arg1138z00_921);
																					}
																					BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																						(BgL_arg1137z00_920);
																				}
																			else
																				{	/* Module/main.scm 71 */
																					obj_t BgL_arg1142z00_924;

																					{	/* Module/main.scm 71 */
																						obj_t BgL_arg1143z00_925;

																						{	/* Module/main.scm 71 */
																							obj_t BgL_arg1145z00_926;

																							{	/* Module/main.scm 71 */
																								obj_t BgL_arg1148z00_927;

																								BgL_arg1148z00_927 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(6), BNIL);
																								BgL_arg1145z00_926 =
																									MAKE_YOUNG_PAIR
																									(BgL_mainz00_900,
																									BgL_arg1148z00_927);
																							}
																							BgL_arg1143z00_925 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1145z00_926, BNIL);
																						}
																						BgL_arg1142z00_924 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																							BgL_arg1143z00_925);
																					}
																					BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																						(BgL_arg1142z00_924);
																				}
																		}
																		return (BGl_za2mainza2z00zzmodule_modulez00
																			=
																			BGl_findzd2globalzf2modulez20zzast_envz00
																			(BgL_mainz00_900,
																				BGl_za2moduleza2z00zzmodule_modulez00),
																			BUNSPEC);
																	}
															}
														}
													}
												else
													{	/* Module/main.scm 59 */
													BgL_tagzd2102zd2_899:
														{	/* Module/main.scm 74 */
															obj_t BgL_list1153z00_928;

															BgL_list1153z00_928 = MAKE_YOUNG_PAIR(BNIL, BNIL);
															return
																BGl_userzd2errorzd2zztools_errorz00
																(BGl_string1200z00zzmodule_mainz00,
																BGl_string1201z00zzmodule_mainz00,
																BgL_clausez00_867, BgL_list1153z00_928);
														}
													}
											}
										else
											{	/* Module/main.scm 59 */
												goto BgL_tagzd2102zd2_899;
											}
									}
								else
									{	/* Module/main.scm 59 */
										goto BgL_tagzd2102zd2_899;
									}
							}
						else
							{	/* Module/main.scm 59 */
								goto BgL_tagzd2102zd2_899;
							}
					}
			}
		}

	}



/* &main-consumer */
	obj_t BGl_z62mainzd2consumerzb0zzmodule_mainz00(obj_t BgL_envz00_868,
		obj_t BgL_modulez00_869, obj_t BgL_clausez00_870)
	{
		{	/* Module/main.scm 79 */
			{	/* Module/main.scm 80 */
				bool_t BgL_test1246z00_1132;

				{	/* Module/main.scm 80 */
					obj_t BgL_objz00_931;

					BgL_objz00_931 = BGl_za2mainza2z00zzmodule_modulez00;
					{	/* Module/main.scm 80 */
						obj_t BgL_classz00_932;

						BgL_classz00_932 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_objz00_931))
							{	/* Module/main.scm 80 */
								BgL_objectz00_bglt BgL_arg1807z00_933;

								BgL_arg1807z00_933 = (BgL_objectz00_bglt) (BgL_objz00_931);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Module/main.scm 80 */
										long BgL_idxz00_934;

										BgL_idxz00_934 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_933);
										BgL_test1246z00_1132 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_934 + 2L)) == BgL_classz00_932);
									}
								else
									{	/* Module/main.scm 80 */
										bool_t BgL_res1195z00_937;

										{	/* Module/main.scm 80 */
											obj_t BgL_oclassz00_938;

											{	/* Module/main.scm 80 */
												obj_t BgL_arg1815z00_939;
												long BgL_arg1816z00_940;

												BgL_arg1815z00_939 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Module/main.scm 80 */
													long BgL_arg1817z00_941;

													BgL_arg1817z00_941 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_933);
													BgL_arg1816z00_940 =
														(BgL_arg1817z00_941 - OBJECT_TYPE);
												}
												BgL_oclassz00_938 =
													VECTOR_REF(BgL_arg1815z00_939, BgL_arg1816z00_940);
											}
											{	/* Module/main.scm 80 */
												bool_t BgL__ortest_1115z00_942;

												BgL__ortest_1115z00_942 =
													(BgL_classz00_932 == BgL_oclassz00_938);
												if (BgL__ortest_1115z00_942)
													{	/* Module/main.scm 80 */
														BgL_res1195z00_937 = BgL__ortest_1115z00_942;
													}
												else
													{	/* Module/main.scm 80 */
														long BgL_odepthz00_943;

														{	/* Module/main.scm 80 */
															obj_t BgL_arg1804z00_944;

															BgL_arg1804z00_944 = (BgL_oclassz00_938);
															BgL_odepthz00_943 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_944);
														}
														if ((2L < BgL_odepthz00_943))
															{	/* Module/main.scm 80 */
																obj_t BgL_arg1802z00_945;

																{	/* Module/main.scm 80 */
																	obj_t BgL_arg1803z00_946;

																	BgL_arg1803z00_946 = (BgL_oclassz00_938);
																	BgL_arg1802z00_945 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_946,
																		2L);
																}
																BgL_res1195z00_937 =
																	(BgL_arg1802z00_945 == BgL_classz00_932);
															}
														else
															{	/* Module/main.scm 80 */
																BgL_res1195z00_937 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1246z00_1132 = BgL_res1195z00_937;
									}
							}
						else
							{	/* Module/main.scm 80 */
								BgL_test1246z00_1132 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1246z00_1132)
					{	/* Module/main.scm 80 */
						BGl_duplicatezd2mainzd2errorz00zzmodule_mainz00(BgL_clausez00_870);
					}
				else
					{	/* Module/main.scm 80 */
						BGl_za2mainza2z00zzmodule_modulez00 = CNST_TABLE_REF(2);
					}
			}
			return BNIL;
		}

	}



/* duplicate-main-error */
	obj_t BGl_duplicatezd2mainzd2errorz00zzmodule_mainz00(obj_t BgL_clausez00_7)
	{
		{	/* Module/main.scm 88 */
			{	/* Module/main.scm 89 */
				obj_t BgL_list1155z00_599;

				BgL_list1155z00_599 = MAKE_YOUNG_PAIR(BNIL, BNIL);
				return
					BGl_userzd2errorzd2zztools_errorz00(BGl_string1200z00zzmodule_mainz00,
					BGl_string1202z00zzmodule_mainz00, BgL_clausez00_7,
					BgL_list1155z00_599);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_mainz00(void)
	{
		{	/* Module/main.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_mainz00(void)
	{
		{	/* Module/main.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_mainz00(void)
	{
		{	/* Module/main.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_mainz00(void)
	{
		{	/* Module/main.scm 15 */
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1203z00zzmodule_mainz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1203z00zzmodule_mainz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1203z00zzmodule_mainz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1203z00zzmodule_mainz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1203z00zzmodule_mainz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1203z00zzmodule_mainz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1203z00zzmodule_mainz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1203z00zzmodule_mainz00));
		}

	}

#ifdef __cplusplus
}
#endif
