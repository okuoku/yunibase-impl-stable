/*===========================================================================*/
/*   (Module/module.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/module.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_MODULE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;


#endif													// BGL_MODULE_MODULE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_producezd2modulez12zc0zzmodule_modulez00(obj_t);
	static obj_t BGl_z62ccompzd2producerzb0zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_z62producezd2modulez12za2zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31838ze3ze5zzmodule_modulez00(obj_t);
	static obj_t BGl_z62lambda1772z62zzmodule_modulez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_consumezd2modulezd2clausez12z12zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1773z62zzmodule_modulez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	static BgL_ccompz00_bglt BGl_z62ccompzd2nilzb0zzmodule_modulez00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ccompzd2idzd2zzmodule_modulez00(BgL_ccompz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_modulez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31766ze3ze5zzmodule_modulez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_ccompz00_bglt
		BGl_makezd2ccompzd2zzmodule_modulez00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_restorezd2additionalzd2heapsz00zzheap_restorez00(void);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_makezd2includezd2compilerz00zzmodule_includez00(void);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static BgL_ccompz00_bglt BGl_z62makezd2ccompzb0zzmodule_modulez00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2fromzd2compilerz00zzmodule_impusez00(void);
	extern obj_t BGl_za2passza2z00zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_ccompzd2consumerzd2zzmodule_modulez00(BgL_ccompz00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzmodule_modulez00(void);
	BGL_EXPORTED_DECL BgL_ccompz00_bglt
		BGl_ccompzd2nilzd2zzmodule_modulez00(void);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_modulez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ccompzd2producerzd2zzmodule_modulez00(BgL_ccompz00_bglt);
	extern long BGl_modulezd2checksumzd2zzmodule_checksumz00(obj_t, obj_t);
	extern obj_t BGl_makezd2importzd2compilerz00zzmodule_impusez00(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_modulez00(void);
	static obj_t BGl_dozd2modulezd2zzmodule_modulez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_makezd2externzd2compilerz00zzmodule_foreignz00(void);
	extern obj_t BGl_makezd2mainzd2compilerz00zzmodule_mainz00(void);
	static obj_t
		BGl_finaliza7ezd2clausezd2compilationsza7zzmodule_modulez00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_makezd2usezd2compilerz00zzmodule_impusez00(void);
	BGL_IMPORT obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ccompzf3zf3zzmodule_modulez00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_z62consumezd2modulezd2clausez12z70zzmodule_modulez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_modulez00(void);
	extern obj_t BGl_makezd2alibraryzd2compilerz00zzmodule_alibraryz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_EXPORTED_DECL obj_t BGl_dumpzd2modulezd2zzmodule_modulez00(obj_t);
	static obj_t BGl_zc3z04anonymousza31518ze3ze70z60zzmodule_modulez00(obj_t,
		obj_t);
	extern obj_t BGl_readzd2directiveszd2zzread_includez00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	extern obj_t BGl_makezd2loadzd2compilerz00zzmodule_loadz00(void);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_makezd2evalzd2compilerz00zzmodule_evalz00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t
		BGl_installzd2modulezd2clauseszd2compilerz12zc0zzmodule_modulez00(void);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	extern obj_t
		BGl_comptimezd2expandzd2condzd2expandzd2onlyz00zzexpand_epsz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2modulezd2clauseza2zd2zzmodule_modulez00 =
		BUNSPEC;
	static obj_t BGl_dozd2consumezd2modulez00zzmodule_modulez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ccompz00zzmodule_modulez00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2moduleza2z00zzmodule_modulez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	static obj_t BGl_modulezd2mclausezd2zzmodule_modulez00(obj_t);
	static bool_t
		BGl_producezd2includezd2libraryzd2clauseszd2zzmodule_modulez00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2withzd2compilerz00zzmodule_withz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	extern obj_t BGl_makezd2javazd2compilerz00zzmodule_javaz00(void);
	static obj_t BGl_z62unknownzd2clausezd2consumerz62zzmodule_modulez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_includez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_optionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_checksumz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_pragmaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_loadz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_javaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_wasmz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_foreignz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_withz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_statexpz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzheap_restorez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2clausezd2compilersza2zd2zzmodule_modulez00 = BUNSPEC;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	BGL_IMPORT bool_t fexists(char *);
	static obj_t BGl_z62modulezd2initializa7ationzd2idzc5zzmodule_modulez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2checksumzd2objectz00zzmodule_modulez00(void);
	extern obj_t BGl_importzd2macrozd2finaliza7erza7zzread_inlinez00(void);
	static obj_t BGl_z62dumpzd2modulezb0zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_z62unknownzd2clausezd2producerz62zzmodule_modulez00(obj_t,
		obj_t);
	static obj_t BGl_z62ccompzf3z91zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzmodule_modulez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_modulez00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_modulez00(void);
	BGL_EXPORTED_DECL obj_t BGl_consumezd2modulez12zc0zzmodule_modulez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_modulez00(void);
	static obj_t BGl_z62consumezd2modulez12za2zzmodule_modulez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2modulezd2locationza2zd2zzmodule_modulez00 =
		BUNSPEC;
	BGL_IMPORT obj_t
		BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00(void);
	static obj_t BGl_z62ccompzd2idzb0zzmodule_modulez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_ccompzd2finaliza7erz75zzmodule_modulez00(BgL_ccompz00_bglt);
	static obj_t BGl_z62ccompzd2finaliza7erz17zzmodule_modulez00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2modulezd2checksumza2zd2zzmodule_modulez00 =
		BUNSPEC;
	extern obj_t BGl_earlyzd2withzd2clausesz00zzmodule_withz00(void);
	static obj_t BGl_zc3z04anonymousza31234ze3ze70z60zzmodule_modulez00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_z62zc3z04anonymousza31808ze3ze5zzmodule_modulez00(obj_t);
	extern obj_t BGl_prognzd2firstzd2expressionz00zztools_prognz00(obj_t);
	static obj_t BGl_findzd2clausezd2consumerz00zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_checksumzd2modulezd2zzmodule_modulez00(obj_t);
	extern obj_t BGl_makezd2optionzd2compilerz00zzmodule_optionz00(void);
	static obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzmodule_modulez00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2typezd2compilerz00zzmodule_typez00(void);
	static obj_t BGl_z62lambda1806z62zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1807z62zzmodule_modulez00(obj_t, obj_t, obj_t);
	static obj_t BGl_producezd2libraryzd2clausesz00zzmodule_modulez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31826ze3ze5zzmodule_modulez00(obj_t);
	static obj_t BGl_z62producezd2modulezd2clausez12z70zzmodule_modulez00(obj_t,
		obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2staticzd2compilerz00zzmodule_statexpz00(void);
	static obj_t BGl_findzd2clausezd2producerz00zzmodule_modulez00(obj_t, obj_t);
	extern obj_t BGl_makezd2wasmzd2compilerz00zzmodule_wasmz00(void);
	static obj_t BGl_z62zc3z04anonymousza31827ze3ze5zzmodule_modulez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2exportzd2compilerz00zzmodule_statexpz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00(obj_t);
	static obj_t BGl_z62lambda1824z62zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1825z62zzmodule_modulez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2foreignzd2compilerz00zzmodule_foreignz00(void);
	extern obj_t BGl_za2mcozd2includezd2pathza2z00zzengine_paramz00;
	static obj_t BGl_z62modulezd2checksumzd2objectz62zzmodule_modulez00(obj_t);
	static obj_t BGl_z62ccompzd2consumerzb0zzmodule_modulez00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31658ze3ze5zzmodule_modulez00(obj_t);
	static obj_t BGl_z62lambda1835z62zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1836z62zzmodule_modulez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2mainza2z00zzmodule_modulez00 = BUNSPEC;
	static BgL_ccompz00_bglt BGl_z62lambda1756z62zzmodule_modulez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2pragmazd2compilerz00zzmodule_pragmaz00(void);
	static obj_t BGl_z62zc3z04anonymousza31837ze3ze5zzmodule_modulez00(obj_t);
	static BgL_ccompz00_bglt BGl_z62lambda1762z62zzmodule_modulez00(obj_t);
	static obj_t __cnst[23];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ccompzd2finaliza7erzd2envza7zzmodule_modulez00,
		BgL_bgl_za762ccompza7d2final1892z00,
		BGl_z62ccompzd2finaliza7erz17zzmodule_modulez00, 0L, BUNSPEC, 1);
	extern obj_t BGl_leavezd2functionzd2envz00zztools_errorz00;
	BGL_IMPORT obj_t BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccompzd2nilzd2envz00zzmodule_modulez00,
		BgL_bgl_za762ccompza7d2nilza7b1893za7,
		BGl_z62ccompzd2nilzb0zzmodule_modulez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_consumezd2modulez12zd2envz12zzmodule_modulez00,
		BgL_bgl_za762consumeza7d2mod1894z00,
		BGl_z62consumezd2modulez12za2zzmodule_modulez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccompzf3zd2envz21zzmodule_modulez00,
		BgL_bgl_za762ccompza7f3za791za7za71895za7,
		BGl_z62ccompzf3z91zzmodule_modulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1847z00zzmodule_modulez00,
		BgL_bgl_string1847za700za7za7m1896za7, "Module", 6);
	      DEFINE_STRING(BGl_string1848z00zzmodule_modulez00,
		BgL_bgl_string1848za700za7za7m1897za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1849z00zzmodule_modulez00,
		BgL_bgl_string1849za700za7za7m1898za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1850z00zzmodule_modulez00,
		BgL_bgl_string1850za700za7za7m1899za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1851z00zzmodule_modulez00,
		BgL_bgl_string1851za700za7za7m1900za7, "Illegal module form", 19);
	      DEFINE_STRING(BGl_string1852z00zzmodule_modulez00,
		BgL_bgl_string1852za700za7za7m1901za7, "module ~a", 9);
	      DEFINE_STRING(BGl_string1853z00zzmodule_modulez00,
		BgL_bgl_string1853za700za7za7m1902za7, " error", 6);
	      DEFINE_STRING(BGl_string1854z00zzmodule_modulez00,
		BgL_bgl_string1854za700za7za7m1903za7, "s", 1);
	      DEFINE_STRING(BGl_string1855z00zzmodule_modulez00,
		BgL_bgl_string1855za700za7za7m1904za7, "", 0);
	      DEFINE_STRING(BGl_string1856z00zzmodule_modulez00,
		BgL_bgl_string1856za700za7za7m1905za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1857z00zzmodule_modulez00,
		BgL_bgl_string1857za700za7za7m1906za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1858z00zzmodule_modulez00,
		BgL_bgl_string1858za700za7za7m1907za7, "Illegal module name", 19);
	      DEFINE_STRING(BGl_string1859z00zzmodule_modulez00,
		BgL_bgl_string1859za700za7za7m1908za7, "Illegal module clause", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpzd2modulezd2envz00zzmodule_modulez00,
		BgL_bgl_za762dumpza7d2module1909z00,
		BGl_z62dumpzd2modulezb0zzmodule_modulez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccompzd2consumerzd2envz00zzmodule_modulez00,
		BgL_bgl_za762ccompza7d2consu1910z00,
		BGl_z62ccompzd2consumerzb0zzmodule_modulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1860z00zzmodule_modulez00,
		BgL_bgl_string1860za700za7za7m1911za7, "Unknown module clause", 21);
	      DEFINE_STRING(BGl_string1861z00zzmodule_modulez00,
		BgL_bgl_string1861za700za7za7m1912za7, " vs ", 4);
	      DEFINE_STRING(BGl_string1862z00zzmodule_modulez00,
		BgL_bgl_string1862za700za7za7m1913za7, "conflict in module's name: ", 27);
	      DEFINE_STRING(BGl_string1863z00zzmodule_modulez00,
		BgL_bgl_string1863za700za7za7m1914za7, "Module declaration", 18);
	      DEFINE_STRING(BGl_string1864z00zzmodule_modulez00,
		BgL_bgl_string1864za700za7za7m1915za7, "Illegal module declaration", 26);
	      DEFINE_STRING(BGl_string1865z00zzmodule_modulez00,
		BgL_bgl_string1865za700za7za7m1916za7, ".ebgl", 5);
	      DEFINE_STRING(BGl_string1866z00zzmodule_modulez00,
		BgL_bgl_string1866za700za7za7m1917za7, "Can't open output file", 22);
	      DEFINE_STRING(BGl_string1867z00zzmodule_modulez00,
		BgL_bgl_string1867za700za7za7m1918za7, "Module checksum object", 22);
	      DEFINE_STRING(BGl_string1868z00zzmodule_modulez00,
		BgL_bgl_string1868za700za7za7m1919za7, ".mco", 4);
	      DEFINE_STRING(BGl_string1869z00zzmodule_modulez00,
		BgL_bgl_string1869za700za7za7m1920za7, "module checksum", 15);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccompzd2idzd2envz00zzmodule_modulez00,
		BgL_bgl_za762ccompza7d2idza7b01921za7,
		BGl_z62ccompzd2idzb0zzmodule_modulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1870z00zzmodule_modulez00,
		BgL_bgl_string1870za700za7za7m1922za7, "Can't open file for output", 26);
	      DEFINE_STRING(BGl_string1871z00zzmodule_modulez00,
		BgL_bgl_string1871za700za7za7m1923za7, "Can't open file for input", 25);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2initializa7ationzd2idzd2envz75zzmodule_modulez00,
		BgL_bgl_za762moduleza7d2init1924z00,
		BGl_z62modulezd2initializa7ationzd2idzc5zzmodule_modulez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccompzd2producerzd2envz00zzmodule_modulez00,
		BgL_bgl_za762ccompza7d2produ1925z00,
		BGl_z62ccompzd2producerzb0zzmodule_modulez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1872z00zzmodule_modulez00,
		BgL_bgl_za762lambda1773za7621926z00, BGl_z62lambda1773z62zzmodule_modulez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_producezd2modulezd2clausez12zd2envzc0zzmodule_modulez00,
		BgL_bgl_za762produceza7d2mod1927z00,
		BGl_z62producezd2modulezd2clausez12z70zzmodule_modulez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzmodule_modulez00,
		BgL_bgl_za762lambda1772za7621928z00, BGl_z62lambda1772z62zzmodule_modulez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzmodule_modulez00,
		BgL_bgl_za762za7c3za704anonymo1929za7,
		BGl_z62zc3z04anonymousza31808ze3ze5zzmodule_modulez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzmodule_modulez00,
		BgL_bgl_za762lambda1807za7621930z00, BGl_z62lambda1807z62zzmodule_modulez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzmodule_modulez00,
		BgL_bgl_za762lambda1806za7621931z00, BGl_z62lambda1806z62zzmodule_modulez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzmodule_modulez00,
		BgL_bgl_za762za7c3za704anonymo1932za7,
		BGl_z62zc3z04anonymousza31826ze3ze5zzmodule_modulez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzmodule_modulez00,
		BgL_bgl_za762lambda1825za7621933z00, BGl_z62lambda1825z62zzmodule_modulez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzmodule_modulez00,
		BgL_bgl_za762lambda1824za7621934z00, BGl_z62lambda1824z62zzmodule_modulez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1889z00zzmodule_modulez00,
		BgL_bgl_string1889za700za7za7m1935za7, "module_module", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzmodule_modulez00,
		BgL_bgl_za762za7c3za704anonymo1936za7,
		BGl_z62zc3z04anonymousza31837ze3ze5zzmodule_modulez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzmodule_modulez00,
		BgL_bgl_za762lambda1836za7621937z00, BGl_z62lambda1836z62zzmodule_modulez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzmodule_modulez00,
		BgL_bgl_za762lambda1835za7621938z00, BGl_z62lambda1835z62zzmodule_modulez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzmodule_modulez00,
		BgL_bgl_za762za7c3za704anonymo1939za7,
		BGl_z62zc3z04anonymousza31766ze3ze5zzmodule_modulez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1890z00zzmodule_modulez00,
		BgL_bgl_string1890za700za7za7m1940za7,
		"void _ module_module ccomp finalizer consumer procedure producer symbol id mco dump-module --to-stdout module-initialization include cond-expand (library _) library (leave-function additional-heap-restore-globals!) (eval foreign t) main module pass-started ",
		257);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1884z00zzmodule_modulez00,
		BgL_bgl_za762lambda1762za7621941z00, BGl_z62lambda1762z62zzmodule_modulez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1885z00zzmodule_modulez00,
		BgL_bgl_za762lambda1756za7621942z00, BGl_z62lambda1756z62zzmodule_modulez00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzmodule_modulez00,
		BgL_bgl_za762za7c3za704anonymo1943za7,
		BGl_z62zc3z04anonymousza31838ze3ze5zzmodule_modulez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1887z00zzmodule_modulez00,
		BgL_bgl_za762za7c3za704anonymo1944za7,
		BGl_z62zc3z04anonymousza31827ze3ze5zzmodule_modulez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzmodule_modulez00,
		BgL_bgl_za762za7c3za704anonymo1945za7,
		BGl_z62zc3z04anonymousza31809ze3ze5zzmodule_modulez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2ccompzd2envz00zzmodule_modulez00,
		BgL_bgl_za762makeza7d2ccompza71946za7,
		BGl_z62makezd2ccompzb0zzmodule_modulez00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_producezd2modulez12zd2envz12zzmodule_modulez00,
		BgL_bgl_za762produceza7d2mod1947z00,
		BGl_z62producezd2modulez12za2zzmodule_modulez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2checksumzd2objectzd2envzd2zzmodule_modulez00,
		BgL_bgl_za762moduleza7d2chec1948z00,
		BGl_z62modulezd2checksumzd2objectz62zzmodule_modulez00, 0L, BUNSPEC, 0);
	extern obj_t
		BGl_additionalzd2heapzd2restorezd2globalsz12zd2envz12zzast_envz00;
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_consumezd2modulezd2clausez12zd2envzc0zzmodule_modulez00,
		BgL_bgl_za762consumeza7d2mod1949z00,
		BGl_z62consumezd2modulezd2clausez12z70zzmodule_modulez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_modulez00));
		     ADD_ROOT((void *) (&BGl_za2modulezd2clauseza2zd2zzmodule_modulez00));
		     ADD_ROOT((void *) (&BGl_ccompz00zzmodule_modulez00));
		     ADD_ROOT((void *) (&BGl_za2moduleza2z00zzmodule_modulez00));
		   
			 ADD_ROOT((void *) (&BGl_za2clausezd2compilersza2zd2zzmodule_modulez00));
		     ADD_ROOT((void *) (&BGl_za2modulezd2locationza2zd2zzmodule_modulez00));
		     ADD_ROOT((void *) (&BGl_za2modulezd2checksumza2zd2zzmodule_modulez00));
		     ADD_ROOT((void *) (&BGl_za2mainza2z00zzmodule_modulez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long
		BgL_checksumz00_962, char *BgL_fromz00_963)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_modulez00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_modulez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_modulez00();
					BGl_libraryzd2moduleszd2initz00zzmodule_modulez00();
					BGl_cnstzd2initzd2zzmodule_modulez00();
					BGl_importedzd2moduleszd2initz00zzmodule_modulez00();
					BGl_objectzd2initzd2zzmodule_modulez00();
					return BGl_toplevelzd2initzd2zzmodule_modulez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_module");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_module");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "module_module");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "module_module");
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "module_module");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "module_module");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"module_module");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_module");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 15 */
			{	/* Module/module.scm 15 */
				obj_t BgL_cportz00_907;

				{	/* Module/module.scm 15 */
					obj_t BgL_stringz00_914;

					BgL_stringz00_914 = BGl_string1890z00zzmodule_modulez00;
					{	/* Module/module.scm 15 */
						obj_t BgL_startz00_915;

						BgL_startz00_915 = BINT(0L);
						{	/* Module/module.scm 15 */
							obj_t BgL_endz00_916;

							BgL_endz00_916 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_914)));
							{	/* Module/module.scm 15 */

								BgL_cportz00_907 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_914, BgL_startz00_915, BgL_endz00_916);
				}}}}
				{
					long BgL_iz00_908;

					BgL_iz00_908 = 22L;
				BgL_loopz00_909:
					if ((BgL_iz00_908 == -1L))
						{	/* Module/module.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/module.scm 15 */
							{	/* Module/module.scm 15 */
								obj_t BgL_arg1891z00_910;

								{	/* Module/module.scm 15 */

									{	/* Module/module.scm 15 */
										obj_t BgL_locationz00_912;

										BgL_locationz00_912 = BBOOL(((bool_t) 0));
										{	/* Module/module.scm 15 */

											BgL_arg1891z00_910 =
												BGl_readz00zz__readerz00(BgL_cportz00_907,
												BgL_locationz00_912);
										}
									}
								}
								{	/* Module/module.scm 15 */
									int BgL_tmpz00_999;

									BgL_tmpz00_999 = (int) (BgL_iz00_908);
									CNST_TABLE_SET(BgL_tmpz00_999, BgL_arg1891z00_910);
							}}
							{	/* Module/module.scm 15 */
								int BgL_auxz00_913;

								BgL_auxz00_913 = (int) ((BgL_iz00_908 - 1L));
								{
									long BgL_iz00_1004;

									BgL_iz00_1004 = (long) (BgL_auxz00_913);
									BgL_iz00_908 = BgL_iz00_1004;
									goto BgL_loopz00_909;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 15 */
			BGl_za2moduleza2z00zzmodule_modulez00 = BFALSE;
			BGl_za2modulezd2clauseza2zd2zzmodule_modulez00 = BFALSE;
			BGl_za2mainza2z00zzmodule_modulez00 = BFALSE;
			BGl_za2modulezd2checksumza2zd2zzmodule_modulez00 = BFALSE;
			BGl_za2modulezd2locationza2zd2zzmodule_modulez00 = BFALSE;
			return (BGl_za2clausezd2compilersza2zd2zzmodule_modulez00 =
				BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzmodule_modulez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_67;

				BgL_headz00_67 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_68;
					obj_t BgL_tailz00_69;

					BgL_prevz00_68 = BgL_headz00_67;
					BgL_tailz00_69 = BgL_l1z00_1;
				BgL_loopz00_70:
					if (PAIRP(BgL_tailz00_69))
						{
							obj_t BgL_newzd2prevzd2_72;

							BgL_newzd2prevzd2_72 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_69), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_68, BgL_newzd2prevzd2_72);
							{
								obj_t BgL_tailz00_1014;
								obj_t BgL_prevz00_1013;

								BgL_prevz00_1013 = BgL_newzd2prevzd2_72;
								BgL_tailz00_1014 = CDR(BgL_tailz00_69);
								BgL_tailz00_69 = BgL_tailz00_1014;
								BgL_prevz00_68 = BgL_prevz00_1013;
								goto BgL_loopz00_70;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_67);
				}
			}
		}

	}



/* make-ccomp */
	BGL_EXPORTED_DEF BgL_ccompz00_bglt BGl_makezd2ccompzd2zzmodule_modulez00(obj_t
		BgL_id1019z00_25, obj_t BgL_producer1020z00_26,
		obj_t BgL_consumer1021z00_27, obj_t BgL_finaliza7er1022za7_28)
	{
		{	/* Module/module.sch 25 */
			{	/* Module/module.sch 25 */
				BgL_ccompz00_bglt BgL_new1059z00_918;

				{	/* Module/module.sch 25 */
					BgL_ccompz00_bglt BgL_new1058z00_919;

					BgL_new1058z00_919 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/module.sch 25 */
						long BgL_arg1126z00_920;

						BgL_arg1126z00_920 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1058z00_919), BgL_arg1126z00_920);
					}
					BgL_new1059z00_918 = BgL_new1058z00_919;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1059z00_918))->BgL_idz00) =
					((obj_t) BgL_id1019z00_25), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1059z00_918))->BgL_producerz00) =
					((obj_t) BgL_producer1020z00_26), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1059z00_918))->BgL_consumerz00) =
					((obj_t) BgL_consumer1021z00_27), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1059z00_918))->
						BgL_finaliza7erza7) = ((obj_t) BgL_finaliza7er1022za7_28), BUNSPEC);
				return BgL_new1059z00_918;
			}
		}

	}



/* &make-ccomp */
	BgL_ccompz00_bglt BGl_z62makezd2ccompzb0zzmodule_modulez00(obj_t
		BgL_envz00_803, obj_t BgL_id1019z00_804, obj_t BgL_producer1020z00_805,
		obj_t BgL_consumer1021z00_806, obj_t BgL_finaliza7er1022za7_807)
	{
		{	/* Module/module.sch 25 */
			return
				BGl_makezd2ccompzd2zzmodule_modulez00(BgL_id1019z00_804,
				BgL_producer1020z00_805, BgL_consumer1021z00_806,
				BgL_finaliza7er1022za7_807);
		}

	}



/* ccomp? */
	BGL_EXPORTED_DEF bool_t BGl_ccompzf3zf3zzmodule_modulez00(obj_t BgL_objz00_29)
	{
		{	/* Module/module.sch 26 */
			{	/* Module/module.sch 26 */
				obj_t BgL_classz00_921;

				BgL_classz00_921 = BGl_ccompz00zzmodule_modulez00;
				if (BGL_OBJECTP(BgL_objz00_29))
					{	/* Module/module.sch 26 */
						BgL_objectz00_bglt BgL_arg1807z00_922;

						BgL_arg1807z00_922 = (BgL_objectz00_bglt) (BgL_objz00_29);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Module/module.sch 26 */
								long BgL_idxz00_923;

								BgL_idxz00_923 = BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_922);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_923 + 1L)) == BgL_classz00_921);
							}
						else
							{	/* Module/module.sch 26 */
								bool_t BgL_res1840z00_926;

								{	/* Module/module.sch 26 */
									obj_t BgL_oclassz00_927;

									{	/* Module/module.sch 26 */
										obj_t BgL_arg1815z00_928;
										long BgL_arg1816z00_929;

										BgL_arg1815z00_928 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Module/module.sch 26 */
											long BgL_arg1817z00_930;

											BgL_arg1817z00_930 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_922);
											BgL_arg1816z00_929 = (BgL_arg1817z00_930 - OBJECT_TYPE);
										}
										BgL_oclassz00_927 =
											VECTOR_REF(BgL_arg1815z00_928, BgL_arg1816z00_929);
									}
									{	/* Module/module.sch 26 */
										bool_t BgL__ortest_1115z00_931;

										BgL__ortest_1115z00_931 =
											(BgL_classz00_921 == BgL_oclassz00_927);
										if (BgL__ortest_1115z00_931)
											{	/* Module/module.sch 26 */
												BgL_res1840z00_926 = BgL__ortest_1115z00_931;
											}
										else
											{	/* Module/module.sch 26 */
												long BgL_odepthz00_932;

												{	/* Module/module.sch 26 */
													obj_t BgL_arg1804z00_933;

													BgL_arg1804z00_933 = (BgL_oclassz00_927);
													BgL_odepthz00_932 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_933);
												}
												if ((1L < BgL_odepthz00_932))
													{	/* Module/module.sch 26 */
														obj_t BgL_arg1802z00_934;

														{	/* Module/module.sch 26 */
															obj_t BgL_arg1803z00_935;

															BgL_arg1803z00_935 = (BgL_oclassz00_927);
															BgL_arg1802z00_934 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_935, 1L);
														}
														BgL_res1840z00_926 =
															(BgL_arg1802z00_934 == BgL_classz00_921);
													}
												else
													{	/* Module/module.sch 26 */
														BgL_res1840z00_926 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1840z00_926;
							}
					}
				else
					{	/* Module/module.sch 26 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &ccomp? */
	obj_t BGl_z62ccompzf3z91zzmodule_modulez00(obj_t BgL_envz00_808,
		obj_t BgL_objz00_809)
	{
		{	/* Module/module.sch 26 */
			return BBOOL(BGl_ccompzf3zf3zzmodule_modulez00(BgL_objz00_809));
		}

	}



/* ccomp-nil */
	BGL_EXPORTED_DEF BgL_ccompz00_bglt BGl_ccompzd2nilzd2zzmodule_modulez00(void)
	{
		{	/* Module/module.sch 27 */
			{	/* Module/module.sch 27 */
				obj_t BgL_classz00_656;

				BgL_classz00_656 = BGl_ccompz00zzmodule_modulez00;
				{	/* Module/module.sch 27 */
					obj_t BgL__ortest_1117z00_657;

					BgL__ortest_1117z00_657 = BGL_CLASS_NIL(BgL_classz00_656);
					if (CBOOL(BgL__ortest_1117z00_657))
						{	/* Module/module.sch 27 */
							return ((BgL_ccompz00_bglt) BgL__ortest_1117z00_657);
						}
					else
						{	/* Module/module.sch 27 */
							return
								((BgL_ccompz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_656));
						}
				}
			}
		}

	}



/* &ccomp-nil */
	BgL_ccompz00_bglt BGl_z62ccompzd2nilzb0zzmodule_modulez00(obj_t
		BgL_envz00_810)
	{
		{	/* Module/module.sch 27 */
			return BGl_ccompzd2nilzd2zzmodule_modulez00();
		}

	}



/* ccomp-finalizer */
	BGL_EXPORTED_DEF obj_t
		BGl_ccompzd2finaliza7erz75zzmodule_modulez00(BgL_ccompz00_bglt BgL_oz00_30)
	{
		{	/* Module/module.sch 28 */
			return (((BgL_ccompz00_bglt) COBJECT(BgL_oz00_30))->BgL_finaliza7erza7);
		}

	}



/* &ccomp-finalizer */
	obj_t BGl_z62ccompzd2finaliza7erz17zzmodule_modulez00(obj_t BgL_envz00_811,
		obj_t BgL_oz00_812)
	{
		{	/* Module/module.sch 28 */
			return
				BGl_ccompzd2finaliza7erz75zzmodule_modulez00(
				((BgL_ccompz00_bglt) BgL_oz00_812));
		}

	}



/* ccomp-consumer */
	BGL_EXPORTED_DEF obj_t
		BGl_ccompzd2consumerzd2zzmodule_modulez00(BgL_ccompz00_bglt BgL_oz00_33)
	{
		{	/* Module/module.sch 30 */
			return (((BgL_ccompz00_bglt) COBJECT(BgL_oz00_33))->BgL_consumerz00);
		}

	}



/* &ccomp-consumer */
	obj_t BGl_z62ccompzd2consumerzb0zzmodule_modulez00(obj_t BgL_envz00_813,
		obj_t BgL_oz00_814)
	{
		{	/* Module/module.sch 30 */
			return
				BGl_ccompzd2consumerzd2zzmodule_modulez00(
				((BgL_ccompz00_bglt) BgL_oz00_814));
		}

	}



/* ccomp-producer */
	BGL_EXPORTED_DEF obj_t
		BGl_ccompzd2producerzd2zzmodule_modulez00(BgL_ccompz00_bglt BgL_oz00_36)
	{
		{	/* Module/module.sch 32 */
			return (((BgL_ccompz00_bglt) COBJECT(BgL_oz00_36))->BgL_producerz00);
		}

	}



/* &ccomp-producer */
	obj_t BGl_z62ccompzd2producerzb0zzmodule_modulez00(obj_t BgL_envz00_815,
		obj_t BgL_oz00_816)
	{
		{	/* Module/module.sch 32 */
			return
				BGl_ccompzd2producerzd2zzmodule_modulez00(
				((BgL_ccompz00_bglt) BgL_oz00_816));
		}

	}



/* ccomp-id */
	BGL_EXPORTED_DEF obj_t BGl_ccompzd2idzd2zzmodule_modulez00(BgL_ccompz00_bglt
		BgL_oz00_39)
	{
		{	/* Module/module.sch 34 */
			return (((BgL_ccompz00_bglt) COBJECT(BgL_oz00_39))->BgL_idz00);
		}

	}



/* &ccomp-id */
	obj_t BGl_z62ccompzd2idzb0zzmodule_modulez00(obj_t BgL_envz00_817,
		obj_t BgL_oz00_818)
	{
		{	/* Module/module.sch 34 */
			return
				BGl_ccompzd2idzd2zzmodule_modulez00(((BgL_ccompz00_bglt) BgL_oz00_818));
		}

	}



/* module-mclause */
	obj_t BGl_modulezd2mclausezd2zzmodule_modulez00(obj_t BgL_modz00_42)
	{
		{	/* Module/module.scm 87 */
			{	/* Module/module.scm 88 */
				bool_t BgL_test1958z00_1069;

				{	/* Module/module.scm 88 */
					obj_t BgL_tmpz00_1070;

					BgL_tmpz00_1070 =
						BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00();
					BgL_test1958z00_1069 = PROCEDUREP(BgL_tmpz00_1070);
				}
				if (BgL_test1958z00_1069)
					{	/* Module/module.scm 89 */
						obj_t BgL_fun1129z00_94;

						BgL_fun1129z00_94 =
							BGl_bigloozd2modulezd2extensionzd2handlerzd2zz__paramz00();
						return BGL_PROCEDURE_CALL1(BgL_fun1129z00_94, BgL_modz00_42);
					}
				else
					{	/* Module/module.scm 88 */
						return BgL_modz00_42;
					}
			}
		}

	}



/* produce-module! */
	BGL_EXPORTED_DEF obj_t BGl_producezd2modulez12zc0zzmodule_modulez00(obj_t
		BgL_modz00_43)
	{
		{	/* Module/module.scm 102 */
			{	/* Module/module.scm 103 */
				obj_t BgL_list1132z00_96;

				{	/* Module/module.scm 103 */
					obj_t BgL_arg1137z00_97;

					{	/* Module/module.scm 103 */
						obj_t BgL_arg1138z00_98;

						BgL_arg1138z00_98 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1137z00_97 =
							MAKE_YOUNG_PAIR(BGl_string1847z00zzmodule_modulez00,
							BgL_arg1138z00_98);
					}
					BgL_list1132z00_96 =
						MAKE_YOUNG_PAIR(BGl_string1848z00zzmodule_modulez00,
						BgL_arg1137z00_97);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1132z00_96);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1847z00zzmodule_modulez00;
			{	/* Module/module.scm 103 */
				obj_t BgL_g1060z00_99;

				BgL_g1060z00_99 = BNIL;
				{
					obj_t BgL_hooksz00_102;
					obj_t BgL_hnamesz00_103;

					BgL_hooksz00_102 = BgL_g1060z00_99;
					BgL_hnamesz00_103 = BNIL;
				BgL_zc3z04anonymousza31139ze3z87_104:
					if (NULLP(BgL_hooksz00_102))
						{	/* Module/module.scm 103 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Module/module.scm 103 */
							bool_t BgL_test1960z00_1088;

							{	/* Module/module.scm 103 */
								obj_t BgL_fun1149z00_111;

								BgL_fun1149z00_111 = CAR(((obj_t) BgL_hooksz00_102));
								BgL_test1960z00_1088 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1149z00_111));
							}
							if (BgL_test1960z00_1088)
								{	/* Module/module.scm 103 */
									obj_t BgL_arg1143z00_108;
									obj_t BgL_arg1145z00_109;

									BgL_arg1143z00_108 = CDR(((obj_t) BgL_hooksz00_102));
									BgL_arg1145z00_109 = CDR(((obj_t) BgL_hnamesz00_103));
									{
										obj_t BgL_hnamesz00_1100;
										obj_t BgL_hooksz00_1099;

										BgL_hooksz00_1099 = BgL_arg1143z00_108;
										BgL_hnamesz00_1100 = BgL_arg1145z00_109;
										BgL_hnamesz00_103 = BgL_hnamesz00_1100;
										BgL_hooksz00_102 = BgL_hooksz00_1099;
										goto BgL_zc3z04anonymousza31139ze3z87_104;
									}
								}
							else
								{	/* Module/module.scm 103 */
									obj_t BgL_arg1148z00_110;

									BgL_arg1148z00_110 = CAR(((obj_t) BgL_hnamesz00_103));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1847z00zzmodule_modulez00,
										BGl_string1849z00zzmodule_modulez00, BgL_arg1148z00_110);
								}
						}
				}
			}
			{	/* Module/module.scm 104 */
				obj_t BgL_mclausez00_114;

				BgL_mclausez00_114 =
					BGl_modulezd2mclausezd2zzmodule_modulez00(BgL_modz00_43);
				{

					if (PAIRP(BgL_mclausez00_114))
						{	/* Module/module.scm 105 */
							obj_t BgL_cdrzd2143zd2_121;

							BgL_cdrzd2143zd2_121 = CDR(((obj_t) BgL_mclausez00_114));
							if ((CAR(((obj_t) BgL_mclausez00_114)) == CNST_TABLE_REF(1)))
								{	/* Module/module.scm 105 */
									if (PAIRP(BgL_cdrzd2143zd2_121))
										{	/* Module/module.scm 105 */
											obj_t BgL_carzd2146zd2_125;

											BgL_carzd2146zd2_125 = CAR(BgL_cdrzd2143zd2_121);
											if (SYMBOLP(BgL_carzd2146zd2_125))
												{	/* Module/module.scm 105 */
													return
														BGl_dozd2modulezd2zzmodule_modulez00
														(BgL_mclausez00_114, BgL_carzd2146zd2_125,
														CDR(BgL_cdrzd2143zd2_121));
												}
											else
												{	/* Module/module.scm 105 */
												BgL_tagzd2136zd2_118:
													return
														BGl_userzd2errorzd2zztools_errorz00
														(BGl_string1850z00zzmodule_modulez00,
														BGl_string1851z00zzmodule_modulez00, BgL_modz00_43,
														BNIL);
												}
										}
									else
										{	/* Module/module.scm 105 */
											goto BgL_tagzd2136zd2_118;
										}
								}
							else
								{	/* Module/module.scm 105 */
									goto BgL_tagzd2136zd2_118;
								}
						}
					else
						{	/* Module/module.scm 105 */
							goto BgL_tagzd2136zd2_118;
						}
				}
			}
		}

	}



/* &produce-module! */
	obj_t BGl_z62producezd2modulez12za2zzmodule_modulez00(obj_t BgL_envz00_819,
		obj_t BgL_modz00_820)
	{
		{	/* Module/module.scm 102 */
			return BGl_producezd2modulez12zc0zzmodule_modulez00(BgL_modz00_820);
		}

	}



/* do-module */
	obj_t BGl_dozd2modulezd2zzmodule_modulez00(obj_t BgL_mclausez00_44,
		obj_t BgL_namez00_45, obj_t BgL_clausesz00_46)
	{
		{	/* Module/module.scm 114 */
			{	/* Module/module.scm 115 */
				obj_t BgL_clausesz00_130;

				{	/* Module/module.scm 115 */
					obj_t BgL_arg1199z00_169;
					obj_t BgL_arg1200z00_170;

					BgL_arg1199z00_169 = BGl_earlyzd2withzd2clausesz00zzmodule_withz00();
					if (SYMBOLP(BGl_za2mainza2z00zzmodule_modulez00))
						{	/* Module/module.scm 117 */
							obj_t BgL_arg1202z00_172;

							{	/* Module/module.scm 117 */
								obj_t BgL_arg1203z00_173;

								BgL_arg1203z00_173 =
									MAKE_YOUNG_PAIR(BGl_za2mainza2z00zzmodule_modulez00, BNIL);
								BgL_arg1202z00_172 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1203z00_173);
							}
							BgL_arg1200z00_170 =
								MAKE_YOUNG_PAIR(BgL_arg1202z00_172, BgL_clausesz00_46);
						}
					else
						{	/* Module/module.scm 116 */
							BgL_arg1200z00_170 = BgL_clausesz00_46;
						}
					BgL_clausesz00_130 =
						MAKE_YOUNG_PAIR(BgL_arg1199z00_169, BgL_arg1200z00_170);
				}
				{	/* Module/module.scm 119 */
					bool_t BgL_test1966z00_1131;

					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_namez00_45,
								CNST_TABLE_REF(3))))
						{	/* Module/module.scm 337 */
							BgL_test1966z00_1131 = ((bool_t) 0);
						}
					else
						{	/* Module/module.scm 337 */
							BgL_test1966z00_1131 = ((bool_t) 1);
						}
					if (BgL_test1966z00_1131)
						{	/* Module/module.scm 119 */
							{	/* Module/module.scm 122 */
								obj_t BgL_arg1162z00_132;

								{	/* Module/module.scm 122 */
									obj_t BgL_arg1164z00_133;

									{	/* Module/module.scm 122 */
										obj_t BgL_list1165z00_134;

										BgL_list1165z00_134 = MAKE_YOUNG_PAIR(BgL_namez00_45, BNIL);
										BgL_arg1164z00_133 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string1852z00zzmodule_modulez00,
											BgL_list1165z00_134);
									}
									BgL_arg1162z00_132 = bstring_to_symbol(BgL_arg1164z00_133);
								}
								BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1162z00_132);
							}
							BGl_za2moduleza2z00zzmodule_modulez00 = BgL_namez00_45;
							BGl_za2modulezd2clauseza2zd2zzmodule_modulez00 =
								BgL_mclausez00_44;
							if (EPAIRP(BgL_mclausez00_44))
								{	/* Module/module.scm 125 */
									BGl_za2modulezd2locationza2zd2zzmodule_modulez00 =
										CER(BgL_mclausez00_44);
								}
							else
								{	/* Module/module.scm 125 */
									BGl_za2modulezd2locationza2zd2zzmodule_modulez00 = BFALSE;
								}
							BGl_installzd2modulezd2clauseszd2compilerz12zc0zzmodule_modulez00
								();
							{	/* Module/module.scm 129 */
								obj_t BgL_clausesz00_136;

								BgL_clausesz00_136 =
									BGl_producezd2libraryzd2clausesz00zzmodule_modulez00
									(BgL_clausesz00_130);
								BGl_restorezd2additionalzd2heapsz00zzheap_restorez00();
								{
									obj_t BgL_l1077z00_138;

									BgL_l1077z00_138 = BgL_clausesz00_136;
								BgL_zc3z04anonymousza31167ze3z87_139:
									if (PAIRP(BgL_l1077z00_138))
										{	/* Module/module.scm 134 */
											BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(CAR
												(BgL_l1077z00_138));
											{
												obj_t BgL_l1077z00_1150;

												BgL_l1077z00_1150 = CDR(BgL_l1077z00_138);
												BgL_l1077z00_138 = BgL_l1077z00_1150;
												goto BgL_zc3z04anonymousza31167ze3z87_139;
											}
										}
									else
										{	/* Module/module.scm 134 */
											((bool_t) 1);
										}
								}
								BGl_za2modulezd2checksumza2zd2zzmodule_modulez00 =
									BGl_checksumzd2modulezd2zzmodule_modulez00(BgL_mclausez00_44);
								{	/* Module/module.scm 136 */
									obj_t BgL_valuez00_144;

									BgL_valuez00_144 =
										BGl_finaliza7ezd2clausezd2compilationsza7zzmodule_modulez00
										();
									if (((long)
											CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
											> 0L))
										{	/* Module/module.scm 136 */
											{	/* Module/module.scm 136 */
												obj_t BgL_port1079z00_146;

												{	/* Module/module.scm 136 */
													obj_t BgL_tmpz00_1157;

													BgL_tmpz00_1157 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_port1079z00_146 =
														BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1157);
												}
												bgl_display_obj
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BgL_port1079z00_146);
												bgl_display_string(BGl_string1853z00zzmodule_modulez00,
													BgL_port1079z00_146);
												{	/* Module/module.scm 136 */
													obj_t BgL_arg1182z00_147;

													{	/* Module/module.scm 136 */
														bool_t BgL_test1971z00_1162;

														if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
															{	/* Module/module.scm 136 */
																if (INTEGERP
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
																	{	/* Module/module.scm 136 */
																		BgL_test1971z00_1162 =
																			(
																			(long)
																			CINT
																			(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																			> 1L);
																	}
																else
																	{	/* Module/module.scm 136 */
																		BgL_test1971z00_1162 =
																			BGl_2ze3ze3zz__r4_numbers_6_5z00
																			(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																			BINT(1L));
																	}
															}
														else
															{	/* Module/module.scm 136 */
																BgL_test1971z00_1162 = ((bool_t) 0);
															}
														if (BgL_test1971z00_1162)
															{	/* Module/module.scm 136 */
																BgL_arg1182z00_147 =
																	BGl_string1854z00zzmodule_modulez00;
															}
														else
															{	/* Module/module.scm 136 */
																BgL_arg1182z00_147 =
																	BGl_string1855z00zzmodule_modulez00;
															}
													}
													bgl_display_obj(BgL_arg1182z00_147,
														BgL_port1079z00_146);
												}
												bgl_display_string(BGl_string1856z00zzmodule_modulez00,
													BgL_port1079z00_146);
												bgl_display_char(((unsigned char) 10),
													BgL_port1079z00_146);
											}
											{	/* Module/module.scm 136 */
												obj_t BgL_list1185z00_151;

												BgL_list1185z00_151 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
												return BGl_exitz00zz__errorz00(BgL_list1185z00_151);
											}
										}
									else
										{	/* Module/module.scm 136 */
											obj_t BgL_g1062z00_152;
											obj_t BgL_g1063z00_153;

											{	/* Module/module.scm 136 */
												obj_t BgL_list1196z00_166;

												{	/* Module/module.scm 136 */
													obj_t BgL_arg1197z00_167;

													BgL_arg1197z00_167 =
														MAKE_YOUNG_PAIR
														(BGl_additionalzd2heapzd2restorezd2globalsz12zd2envz12zzast_envz00,
														BNIL);
													BgL_list1196z00_166 =
														MAKE_YOUNG_PAIR
														(BGl_leavezd2functionzd2envz00zztools_errorz00,
														BgL_arg1197z00_167);
												}
												BgL_g1062z00_152 = BgL_list1196z00_166;
											}
											BgL_g1063z00_153 = CNST_TABLE_REF(4);
											{
												obj_t BgL_hooksz00_155;
												obj_t BgL_hnamesz00_156;

												BgL_hooksz00_155 = BgL_g1062z00_152;
												BgL_hnamesz00_156 = BgL_g1063z00_153;
											BgL_zc3z04anonymousza31186ze3z87_157:
												if (NULLP(BgL_hooksz00_155))
													{	/* Module/module.scm 136 */
														return BgL_valuez00_144;
													}
												else
													{	/* Module/module.scm 136 */
														bool_t BgL_test1975z00_1182;

														{	/* Module/module.scm 136 */
															obj_t BgL_fun1195z00_164;

															BgL_fun1195z00_164 =
																CAR(((obj_t) BgL_hooksz00_155));
															BgL_test1975z00_1182 =
																CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1195z00_164));
														}
														if (BgL_test1975z00_1182)
															{	/* Module/module.scm 136 */
																obj_t BgL_arg1190z00_161;
																obj_t BgL_arg1191z00_162;

																BgL_arg1190z00_161 =
																	CDR(((obj_t) BgL_hooksz00_155));
																BgL_arg1191z00_162 =
																	CDR(((obj_t) BgL_hnamesz00_156));
																{
																	obj_t BgL_hnamesz00_1194;
																	obj_t BgL_hooksz00_1193;

																	BgL_hooksz00_1193 = BgL_arg1190z00_161;
																	BgL_hnamesz00_1194 = BgL_arg1191z00_162;
																	BgL_hnamesz00_156 = BgL_hnamesz00_1194;
																	BgL_hooksz00_155 = BgL_hooksz00_1193;
																	goto BgL_zc3z04anonymousza31186ze3z87_157;
																}
															}
														else
															{	/* Module/module.scm 136 */
																obj_t BgL_arg1193z00_163;

																BgL_arg1193z00_163 =
																	CAR(((obj_t) BgL_hnamesz00_156));
																return
																	BGl_internalzd2errorzd2zztools_errorz00
																	(BGl_za2currentzd2passza2zd2zzengine_passz00,
																	BGl_string1857z00zzmodule_modulez00,
																	BgL_arg1193z00_163);
															}
													}
											}
										}
								}
							}
						}
					else
						{	/* Module/module.scm 119 */
							return
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string1850z00zzmodule_modulez00,
								BGl_string1858z00zzmodule_modulez00, BgL_mclausez00_44, BNIL);
						}
				}
			}
		}

	}



/* produce-library-clauses */
	obj_t BGl_producezd2libraryzd2clausesz00zzmodule_modulez00(obj_t
		BgL_clausesz00_47)
	{
		{	/* Module/module.scm 143 */
			{	/* Module/module.scm 144 */
				obj_t BgL_producerz00_174;

				BgL_producerz00_174 =
					BGl_findzd2clausezd2producerz00zzmodule_modulez00(CNST_TABLE_REF(5),
					CNST_TABLE_REF(6));
				{
					obj_t BgL_clausesz00_177;
					obj_t BgL_resz00_178;

					BgL_clausesz00_177 = BgL_clausesz00_47;
					BgL_resz00_178 = BNIL;
				BgL_zc3z04anonymousza31204ze3z87_179:
					if (NULLP(BgL_clausesz00_177))
						{	/* Module/module.scm 148 */
							BGL_TAIL return bgl_reverse_bang(BgL_resz00_178);
						}
					else
						{	/* Module/module.scm 151 */
							obj_t BgL_g1065z00_181;

							BgL_g1065z00_181 = CAR(((obj_t) BgL_clausesz00_177));
							{
								obj_t BgL_cz00_183;

								BgL_cz00_183 = BgL_g1065z00_181;
							BgL_zc3z04anonymousza31206ze3z87_184:
								{
									obj_t BgL_filesz00_187;

									if (PAIRP(BgL_cz00_183))
										{	/* Module/module.scm 151 */
											if ((CAR(((obj_t) BgL_cz00_183)) == CNST_TABLE_REF(7)))
												{	/* Module/module.scm 151 */
													{	/* Module/module.scm 154 */
														obj_t BgL_ncz00_202;

														BgL_ncz00_202 =
															BGl_prognzd2firstzd2expressionz00zztools_prognz00
															(BGl_comptimezd2expandzd2condzd2expandzd2onlyz00zzexpand_epsz00
															(BgL_cz00_183));
														{	/* Module/module.scm 156 */
															bool_t BgL_test1979z00_1216;

															if ((BgL_ncz00_202 == BUNSPEC))
																{	/* Module/module.scm 156 */
																	BgL_test1979z00_1216 = ((bool_t) 1);
																}
															else
																{	/* Module/module.scm 156 */
																	BgL_test1979z00_1216 =
																		(BgL_ncz00_202 == BFALSE);
																}
															if (BgL_test1979z00_1216)
																{	/* Module/module.scm 157 */
																	obj_t BgL_arg1221z00_204;

																	BgL_arg1221z00_204 =
																		CDR(((obj_t) BgL_clausesz00_177));
																	{
																		obj_t BgL_clausesz00_1222;

																		BgL_clausesz00_1222 = BgL_arg1221z00_204;
																		BgL_clausesz00_177 = BgL_clausesz00_1222;
																		goto BgL_zc3z04anonymousza31204ze3z87_179;
																	}
																}
															else
																{
																	obj_t BgL_cz00_1223;

																	BgL_cz00_1223 = BgL_ncz00_202;
																	BgL_cz00_183 = BgL_cz00_1223;
																	goto BgL_zc3z04anonymousza31206ze3z87_184;
																}
														}
													}
												}
											else
												{	/* Module/module.scm 151 */
													if (
														(CAR(((obj_t) BgL_cz00_183)) == CNST_TABLE_REF(5)))
														{	/* Module/module.scm 151 */
															BGL_PROCEDURE_CALL1(BgL_producerz00_174,
																BgL_cz00_183);
															{	/* Module/module.scm 161 */
																obj_t BgL_arg1225z00_206;

																BgL_arg1225z00_206 =
																	CDR(((obj_t) BgL_clausesz00_177));
																{
																	obj_t BgL_clausesz00_1235;

																	BgL_clausesz00_1235 = BgL_arg1225z00_206;
																	BgL_clausesz00_177 = BgL_clausesz00_1235;
																	goto BgL_zc3z04anonymousza31204ze3z87_179;
																}
															}
														}
													else
														{	/* Module/module.scm 151 */
															if (
																(CAR(
																		((obj_t) BgL_cz00_183)) ==
																	CNST_TABLE_REF(8)))
																{	/* Module/module.scm 151 */
																	obj_t BgL_arg1215z00_198;

																	BgL_arg1215z00_198 =
																		CDR(((obj_t) BgL_cz00_183));
																	BgL_filesz00_187 = BgL_arg1215z00_198;
																	BGl_producezd2includezd2libraryzd2clauseszd2zzmodule_modulez00
																		(BgL_producerz00_174, BgL_filesz00_187);
																	{	/* Module/module.scm 164 */
																		obj_t BgL_arg1226z00_207;
																		obj_t BgL_arg1227z00_208;

																		BgL_arg1226z00_207 =
																			CDR(((obj_t) BgL_clausesz00_177));
																		BgL_arg1227z00_208 =
																			MAKE_YOUNG_PAIR(BgL_cz00_183,
																			BgL_resz00_178);
																		{
																			obj_t BgL_resz00_1248;
																			obj_t BgL_clausesz00_1247;

																			BgL_clausesz00_1247 = BgL_arg1226z00_207;
																			BgL_resz00_1248 = BgL_arg1227z00_208;
																			BgL_resz00_178 = BgL_resz00_1248;
																			BgL_clausesz00_177 = BgL_clausesz00_1247;
																			goto BgL_zc3z04anonymousza31204ze3z87_179;
																		}
																	}
																}
															else
																{	/* Module/module.scm 151 */
																BgL_tagzd2155zd2_189:
																	{	/* Module/module.scm 166 */
																		obj_t BgL_arg1228z00_209;
																		obj_t BgL_arg1229z00_210;

																		BgL_arg1228z00_209 =
																			CDR(((obj_t) BgL_clausesz00_177));
																		BgL_arg1229z00_210 =
																			MAKE_YOUNG_PAIR(BgL_cz00_183,
																			BgL_resz00_178);
																		{
																			obj_t BgL_resz00_1253;
																			obj_t BgL_clausesz00_1252;

																			BgL_clausesz00_1252 = BgL_arg1228z00_209;
																			BgL_resz00_1253 = BgL_arg1229z00_210;
																			BgL_resz00_178 = BgL_resz00_1253;
																			BgL_clausesz00_177 = BgL_clausesz00_1252;
																			goto BgL_zc3z04anonymousza31204ze3z87_179;
																		}
																	}
																}
														}
												}
										}
									else
										{	/* Module/module.scm 151 */
											goto BgL_tagzd2155zd2_189;
										}
								}
							}
						}
				}
			}
		}

	}



/* produce-include-library-clauses */
	bool_t BGl_producezd2includezd2libraryzd2clauseszd2zzmodule_modulez00(obj_t
		BgL_producerz00_48, obj_t BgL_filesz00_49)
	{
		{	/* Module/module.scm 171 */
			{
				obj_t BgL_clausez00_231;

				{	/* Module/module.scm 187 */
					obj_t BgL_g1085z00_214;

					BgL_g1085z00_214 =
						BGl_zc3z04anonymousza31234ze3ze70z60zzmodule_modulez00
						(BgL_filesz00_49);
					{
						obj_t BgL_l1082z00_216;

						BgL_l1082z00_216 = BgL_g1085z00_214;
					BgL_zc3z04anonymousza31230ze3z87_217:
						if (PAIRP(BgL_l1082z00_216))
							{	/* Module/module.scm 188 */
								BgL_clausez00_231 = CAR(BgL_l1082z00_216);
								{

									if (PAIRP(BgL_clausez00_231))
										{	/* Module/module.scm 185 */
											if (
												(CAR(((obj_t) BgL_clausez00_231)) == CNST_TABLE_REF(5)))
												{	/* Module/module.scm 185 */
													BGL_PROCEDURE_CALL1(BgL_producerz00_48,
														BgL_clausez00_231);
												}
											else
												{	/* Module/module.scm 185 */
													if (
														(CAR(
																((obj_t) BgL_clausez00_231)) ==
															CNST_TABLE_REF(8)))
														{	/* Module/module.scm 185 */
															obj_t BgL_arg1268z00_244;

															BgL_arg1268z00_244 =
																CDR(((obj_t) BgL_clausez00_231));
															BBOOL
																(BGl_producezd2includezd2libraryzd2clauseszd2zzmodule_modulez00
																(BgL_producerz00_48, BgL_arg1268z00_244));
														}
													else
														{	/* Module/module.scm 185 */
															if (
																(CAR(
																		((obj_t) BgL_clausez00_231)) ==
																	CNST_TABLE_REF(7)))
																{	/* Module/module.scm 185 */
																	{	/* Module/module.scm 180 */
																		obj_t BgL_ncz00_250;

																		BgL_ncz00_250 =
																			BGl_prognzd2firstzd2expressionz00zztools_prognz00
																			(BGl_comptimezd2expandzd2condzd2expandzd2onlyz00zzexpand_epsz00
																			(BgL_clausez00_231));
																		{	/* Module/module.scm 182 */
																			bool_t BgL_test1988z00_1284;

																			if ((BgL_ncz00_250 == BUNSPEC))
																				{	/* Module/module.scm 182 */
																					BgL_test1988z00_1284 = ((bool_t) 1);
																				}
																			else
																				{	/* Module/module.scm 182 */
																					BgL_test1988z00_1284 =
																						(BgL_ncz00_250 == BFALSE);
																				}
																			if (BgL_test1988z00_1284)
																				{	/* Module/module.scm 182 */
																					BFALSE;
																				}
																			else
																				{	/* Module/module.scm 183 */
																					obj_t BgL_arg1308z00_252;

																					{	/* Module/module.scm 183 */
																						obj_t BgL_list1309z00_253;

																						BgL_list1309z00_253 =
																							MAKE_YOUNG_PAIR(BgL_ncz00_250,
																							BNIL);
																						BgL_arg1308z00_252 =
																							BgL_list1309z00_253;
																					}
																					BGl_producezd2libraryzd2clausesz00zzmodule_modulez00
																						(BgL_arg1308z00_252);
																				}
																		}
																	}
																}
															else
																{	/* Module/module.scm 185 */
																	BUNSPEC;
																}
														}
												}
										}
									else
										{	/* Module/module.scm 185 */
											BUNSPEC;
										}
								}
								{
									obj_t BgL_l1082z00_1291;

									BgL_l1082z00_1291 = CDR(BgL_l1082z00_216);
									BgL_l1082z00_216 = BgL_l1082z00_1291;
									goto BgL_zc3z04anonymousza31230ze3z87_217;
								}
							}
						else
							{	/* Module/module.scm 188 */
								return ((bool_t) 1);
							}
					}
				}
			}
		}

	}



/* <@anonymous:1234>~0 */
	obj_t BGl_zc3z04anonymousza31234ze3ze70z60zzmodule_modulez00(obj_t
		BgL_l1081z00_223)
	{
		{	/* Module/module.scm 188 */
			if (NULLP(BgL_l1081z00_223))
				{	/* Module/module.scm 188 */
					return BNIL;
				}
			else
				{	/* Module/module.scm 188 */
					obj_t BgL_arg1236z00_226;
					obj_t BgL_arg1238z00_227;

					{	/* Module/module.scm 188 */
						obj_t BgL_arg1239z00_228;

						BgL_arg1239z00_228 = CAR(((obj_t) BgL_l1081z00_223));
						BgL_arg1236z00_226 =
							BGl_readzd2directiveszd2zzread_includez00(BgL_arg1239z00_228);
					}
					{	/* Module/module.scm 188 */
						obj_t BgL_arg1242z00_229;

						BgL_arg1242z00_229 = CDR(((obj_t) BgL_l1081z00_223));
						BgL_arg1238z00_227 =
							BGl_zc3z04anonymousza31234ze3ze70z60zzmodule_modulez00
							(BgL_arg1242z00_229);
					}
					return bgl_append2(BgL_arg1236z00_226, BgL_arg1238z00_227);
				}
		}

	}



/* finalize-clause-compilations */
	obj_t BGl_finaliza7ezd2clausezd2compilationsza7zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 193 */
			{
				obj_t BgL_ccz00_258;
				obj_t BgL_unitsz00_259;

				BgL_ccz00_258 = BGl_za2clausezd2compilersza2zd2zzmodule_modulez00;
				BgL_unitsz00_259 = BNIL;
			BgL_zc3z04anonymousza31311ze3z87_260:
				if (NULLP(BgL_ccz00_258))
					{	/* Module/module.scm 196 */
						return
							BGl_appendzd221011zd2zzmodule_modulez00
							(BGl_importzd2macrozd2finaliza7erza7zzread_inlinez00(),
							BgL_unitsz00_259);
					}
				else
					{	/* Module/module.scm 198 */
						obj_t BgL_finaliza7erza7_263;

						BgL_finaliza7erza7_263 =
							(((BgL_ccompz00_bglt) COBJECT(
									((BgL_ccompz00_bglt)
										CAR(((obj_t) BgL_ccz00_258)))))->BgL_finaliza7erza7);
						{	/* Module/module.scm 198 */
							obj_t BgL_finalresz00_264;

							BgL_finalresz00_264 = BGL_PROCEDURE_CALL0(BgL_finaliza7erza7_263);
							{	/* Module/module.scm 199 */

								{	/* Module/module.scm 200 */
									obj_t BgL_arg1315z00_265;
									obj_t BgL_arg1316z00_266;

									BgL_arg1315z00_265 = CDR(((obj_t) BgL_ccz00_258));
									if (PAIRP(BgL_finalresz00_264))
										{	/* Module/module.scm 201 */
											BgL_arg1316z00_266 =
												BGl_appendzd221011zd2zzmodule_modulez00
												(BgL_finalresz00_264, BgL_unitsz00_259);
										}
									else
										{	/* Module/module.scm 201 */
											BgL_arg1316z00_266 = BgL_unitsz00_259;
										}
									{
										obj_t BgL_unitsz00_1319;
										obj_t BgL_ccz00_1318;

										BgL_ccz00_1318 = BgL_arg1315z00_265;
										BgL_unitsz00_1319 = BgL_arg1316z00_266;
										BgL_unitsz00_259 = BgL_unitsz00_1319;
										BgL_ccz00_258 = BgL_ccz00_1318;
										goto BgL_zc3z04anonymousza31311ze3z87_260;
									}
								}
							}
						}
					}
			}
		}

	}



/* produce-module-clause! */
	BGL_EXPORTED_DEF obj_t
		BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t
		BgL_clausez00_50)
	{
		{	/* Module/module.scm 208 */
			{
				obj_t BgL_cz00_271;

				BgL_cz00_271 = BgL_clausez00_50;
			BgL_zc3z04anonymousza31319ze3z87_272:
				{

					if (PAIRP(BgL_cz00_271))
						{	/* Module/module.scm 209 */
							if ((CAR(((obj_t) BgL_cz00_271)) == CNST_TABLE_REF(7)))
								{	/* Module/module.scm 209 */
									{	/* Module/module.scm 212 */
										obj_t BgL_ncz00_284;

										BgL_ncz00_284 =
											BGl_prognzd2firstzd2expressionz00zztools_prognz00
											(BGl_comptimezd2expandzd2condzd2expandzd2onlyz00zzexpand_epsz00
											(BgL_cz00_271));
										{	/* Module/module.scm 214 */
											bool_t BgL_test1995z00_1329;

											if ((BgL_ncz00_284 == BFALSE))
												{	/* Module/module.scm 214 */
													BgL_test1995z00_1329 = ((bool_t) 1);
												}
											else
												{	/* Module/module.scm 214 */
													BgL_test1995z00_1329 = (BgL_ncz00_284 == BUNSPEC);
												}
											if (BgL_test1995z00_1329)
												{	/* Module/module.scm 214 */
													return BFALSE;
												}
											else
												{
													obj_t BgL_cz00_1333;

													BgL_cz00_1333 = BgL_ncz00_284;
													BgL_cz00_271 = BgL_cz00_1333;
													goto BgL_zc3z04anonymousza31319ze3z87_272;
												}
										}
									}
								}
							else
								{	/* Module/module.scm 209 */
									obj_t BgL_carzd2197zd2_281;

									BgL_carzd2197zd2_281 = CAR(((obj_t) BgL_cz00_271));
									if (SYMBOLP(BgL_carzd2197zd2_281))
										{	/* Module/module.scm 217 */
											obj_t BgL_fun1328z00_710;

											BgL_fun1328z00_710 =
												BGl_findzd2clausezd2producerz00zzmodule_modulez00
												(BgL_carzd2197zd2_281, BgL_clausez00_50);
											return BGL_PROCEDURE_CALL1(BgL_fun1328z00_710,
												BgL_cz00_271);
										}
									else
										{	/* Module/module.scm 209 */
										BgL_tagzd2190zd2_276:
											{	/* Module/module.scm 219 */
												obj_t BgL_list1329z00_288;

												BgL_list1329z00_288 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												return
													BGl_userzd2errorzd2zztools_errorz00
													(BGl_string1850z00zzmodule_modulez00,
													BGl_string1859z00zzmodule_modulez00, BgL_clausez00_50,
													BgL_list1329z00_288);
											}
										}
								}
						}
					else
						{	/* Module/module.scm 209 */
							goto BgL_tagzd2190zd2_276;
						}
				}
			}
		}

	}



/* &produce-module-clause! */
	obj_t BGl_z62producezd2modulezd2clausez12z70zzmodule_modulez00(obj_t
		BgL_envz00_823, obj_t BgL_clausez00_824)
	{
		{	/* Module/module.scm 208 */
			return
				BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
				(BgL_clausez00_824);
		}

	}



/* install-module-clauses-compiler! */
	obj_t BGl_installzd2modulezd2clauseszd2compilerz12zc0zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 229 */
			if (NULLP(BGl_za2clausezd2compilersza2zd2zzmodule_modulez00))
				{	/* Module/module.scm 233 */
					obj_t BgL_arg1331z00_291;
					obj_t BgL_arg1332z00_292;
					obj_t BgL_arg1333z00_293;
					obj_t BgL_arg1335z00_294;
					obj_t BgL_arg1339z00_295;
					obj_t BgL_arg1340z00_296;
					obj_t BgL_arg1342z00_297;
					obj_t BgL_arg1343z00_298;
					obj_t BgL_arg1346z00_299;
					obj_t BgL_arg1348z00_300;
					obj_t BgL_arg1349z00_301;
					obj_t BgL_arg1351z00_302;
					obj_t BgL_arg1352z00_303;
					obj_t BgL_arg1361z00_304;
					obj_t BgL_arg1364z00_305;
					obj_t BgL_arg1367z00_306;
					obj_t BgL_arg1370z00_307;
					obj_t BgL_arg1371z00_308;

					BgL_arg1331z00_291 = BGl_makezd2evalzd2compilerz00zzmodule_evalz00();
					BgL_arg1332z00_292 = BGl_makezd2mainzd2compilerz00zzmodule_mainz00();
					BgL_arg1333z00_293 = BGl_makezd2loadzd2compilerz00zzmodule_loadz00();
					BgL_arg1335z00_294 = BGl_makezd2usezd2compilerz00zzmodule_impusez00();
					BgL_arg1339z00_295 =
						BGl_makezd2importzd2compilerz00zzmodule_impusez00();
					BgL_arg1340z00_296 =
						BGl_makezd2fromzd2compilerz00zzmodule_impusez00();
					BgL_arg1342z00_297 = BGl_makezd2javazd2compilerz00zzmodule_javaz00();
					BgL_arg1343z00_298 =
						BGl_makezd2staticzd2compilerz00zzmodule_statexpz00();
					BgL_arg1346z00_299 =
						BGl_makezd2exportzd2compilerz00zzmodule_statexpz00();
					BgL_arg1348z00_300 =
						BGl_makezd2includezd2compilerz00zzmodule_includez00();
					BgL_arg1349z00_301 = BGl_makezd2withzd2compilerz00zzmodule_withz00();
					BgL_arg1351z00_302 =
						BGl_makezd2foreignzd2compilerz00zzmodule_foreignz00();
					BgL_arg1352z00_303 =
						BGl_makezd2externzd2compilerz00zzmodule_foreignz00();
					BgL_arg1361z00_304 = BGl_makezd2wasmzd2compilerz00zzmodule_wasmz00();
					BgL_arg1364z00_305 = BGl_makezd2typezd2compilerz00zzmodule_typez00();
					BgL_arg1367z00_306 =
						BGl_makezd2pragmazd2compilerz00zzmodule_pragmaz00();
					BgL_arg1370z00_307 =
						BGl_makezd2optionzd2compilerz00zzmodule_optionz00();
					BgL_arg1371z00_308 =
						BGl_makezd2alibraryzd2compilerz00zzmodule_alibraryz00();
					{	/* Module/module.scm 233 */
						obj_t BgL_list1372z00_309;

						{	/* Module/module.scm 233 */
							obj_t BgL_arg1375z00_310;

							{	/* Module/module.scm 233 */
								obj_t BgL_arg1376z00_311;

								{	/* Module/module.scm 233 */
									obj_t BgL_arg1377z00_312;

									{	/* Module/module.scm 233 */
										obj_t BgL_arg1378z00_313;

										{	/* Module/module.scm 233 */
											obj_t BgL_arg1379z00_314;

											{	/* Module/module.scm 233 */
												obj_t BgL_arg1380z00_315;

												{	/* Module/module.scm 233 */
													obj_t BgL_arg1408z00_316;

													{	/* Module/module.scm 233 */
														obj_t BgL_arg1410z00_317;

														{	/* Module/module.scm 233 */
															obj_t BgL_arg1421z00_318;

															{	/* Module/module.scm 233 */
																obj_t BgL_arg1422z00_319;

																{	/* Module/module.scm 233 */
																	obj_t BgL_arg1434z00_320;

																	{	/* Module/module.scm 233 */
																		obj_t BgL_arg1437z00_321;

																		{	/* Module/module.scm 233 */
																			obj_t BgL_arg1448z00_322;

																			{	/* Module/module.scm 233 */
																				obj_t BgL_arg1453z00_323;

																				{	/* Module/module.scm 233 */
																					obj_t BgL_arg1454z00_324;

																					{	/* Module/module.scm 233 */
																						obj_t BgL_arg1472z00_325;

																						{	/* Module/module.scm 233 */
																							obj_t BgL_arg1473z00_326;

																							BgL_arg1473z00_326 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1371z00_308, BNIL);
																							BgL_arg1472z00_325 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1370z00_307,
																								BgL_arg1473z00_326);
																						}
																						BgL_arg1454z00_324 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1367z00_306,
																							BgL_arg1472z00_325);
																					}
																					BgL_arg1453z00_323 =
																						MAKE_YOUNG_PAIR(BgL_arg1364z00_305,
																						BgL_arg1454z00_324);
																				}
																				BgL_arg1448z00_322 =
																					MAKE_YOUNG_PAIR(BgL_arg1361z00_304,
																					BgL_arg1453z00_323);
																			}
																			BgL_arg1437z00_321 =
																				MAKE_YOUNG_PAIR(BgL_arg1352z00_303,
																				BgL_arg1448z00_322);
																		}
																		BgL_arg1434z00_320 =
																			MAKE_YOUNG_PAIR(BgL_arg1351z00_302,
																			BgL_arg1437z00_321);
																	}
																	BgL_arg1422z00_319 =
																		MAKE_YOUNG_PAIR(BgL_arg1349z00_301,
																		BgL_arg1434z00_320);
																}
																BgL_arg1421z00_318 =
																	MAKE_YOUNG_PAIR(BgL_arg1348z00_300,
																	BgL_arg1422z00_319);
															}
															BgL_arg1410z00_317 =
																MAKE_YOUNG_PAIR(BgL_arg1346z00_299,
																BgL_arg1421z00_318);
														}
														BgL_arg1408z00_316 =
															MAKE_YOUNG_PAIR(BgL_arg1343z00_298,
															BgL_arg1410z00_317);
													}
													BgL_arg1380z00_315 =
														MAKE_YOUNG_PAIR(BgL_arg1342z00_297,
														BgL_arg1408z00_316);
												}
												BgL_arg1379z00_314 =
													MAKE_YOUNG_PAIR(BgL_arg1340z00_296,
													BgL_arg1380z00_315);
											}
											BgL_arg1378z00_313 =
												MAKE_YOUNG_PAIR(BgL_arg1339z00_295, BgL_arg1379z00_314);
										}
										BgL_arg1377z00_312 =
											MAKE_YOUNG_PAIR(BgL_arg1335z00_294, BgL_arg1378z00_313);
									}
									BgL_arg1376z00_311 =
										MAKE_YOUNG_PAIR(BgL_arg1333z00_293, BgL_arg1377z00_312);
								}
								BgL_arg1375z00_310 =
									MAKE_YOUNG_PAIR(BgL_arg1332z00_292, BgL_arg1376z00_311);
							}
							BgL_list1372z00_309 =
								MAKE_YOUNG_PAIR(BgL_arg1331z00_291, BgL_arg1375z00_310);
						}
						return (BGl_za2clausezd2compilersza2zd2zzmodule_modulez00 =
							BgL_list1372z00_309, BUNSPEC);
					}
				}
			else
				{	/* Module/module.scm 231 */
					return BFALSE;
				}
		}

	}



/* find-clause-producer */
	obj_t BGl_findzd2clausezd2producerz00zzmodule_modulez00(obj_t
		BgL_keywordz00_51, obj_t BgL_clausez00_52)
	{
		{	/* Module/module.scm 255 */
			{	/* Module/module.scm 257 */
				obj_t BgL_unknownzd2clausezd2producerz00_825;

				BgL_unknownzd2clausezd2producerz00_825 =
					MAKE_FX_PROCEDURE
					(BGl_z62unknownzd2clausezd2producerz62zzmodule_modulez00, (int) (1L),
					(int) (1L));
				PROCEDURE_SET(BgL_unknownzd2clausezd2producerz00_825, (int) (0L),
					BgL_clausez00_52);
				{
					obj_t BgL_ccz00_329;

					BgL_ccz00_329 = BGl_za2clausezd2compilersza2zd2zzmodule_modulez00;
				BgL_zc3z04anonymousza31474ze3z87_330:
					if (NULLP(BgL_ccz00_329))
						{	/* Module/module.scm 260 */
							return BgL_unknownzd2clausezd2producerz00_825;
						}
					else
						{	/* Module/module.scm 260 */
							if (
								((((BgL_ccompz00_bglt) COBJECT(
												((BgL_ccompz00_bglt)
													CAR(
														((obj_t) BgL_ccz00_329)))))->BgL_idz00) ==
									BgL_keywordz00_51))
								{	/* Module/module.scm 262 */
									return
										(((BgL_ccompz00_bglt) COBJECT(
												((BgL_ccompz00_bglt)
													CAR(((obj_t) BgL_ccz00_329)))))->BgL_producerz00);
								}
							else
								{
									obj_t BgL_ccz00_1401;

									BgL_ccz00_1401 = CDR(((obj_t) BgL_ccz00_329));
									BgL_ccz00_329 = BgL_ccz00_1401;
									goto BgL_zc3z04anonymousza31474ze3z87_330;
								}
						}
				}
			}
		}

	}



/* &unknown-clause-producer */
	obj_t BGl_z62unknownzd2clausezd2producerz62zzmodule_modulez00(obj_t
		BgL_envz00_826, obj_t BgL_valuesz00_828)
	{
		{	/* Module/module.scm 257 */
			{	/* Module/module.scm 257 */
				obj_t BgL_clausez00_827;

				BgL_clausez00_827 = PROCEDURE_REF(BgL_envz00_826, (int) (0L));
				{	/* Module/module.scm 257 */
					obj_t BgL_list1516z00_936;

					BgL_list1516z00_936 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					return
						BGl_userzd2errorzd2zztools_errorz00
						(BGl_string1850z00zzmodule_modulez00,
						BGl_string1860z00zzmodule_modulez00, BgL_clausez00_827,
						BgL_list1516z00_936);
				}
			}
		}

	}



/* module-initialization-id */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00(obj_t BgL_idz00_53)
	{
		{	/* Module/module.scm 270 */
			return CNST_TABLE_REF(9);
		}

	}



/* &module-initialization-id */
	obj_t BGl_z62modulezd2initializa7ationzd2idzc5zzmodule_modulez00(obj_t
		BgL_envz00_829, obj_t BgL_idz00_830)
	{
		{	/* Module/module.scm 270 */
			return
				BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00(BgL_idz00_830);
		}

	}



/* do-consume-module */
	obj_t BGl_dozd2consumezd2modulez00zzmodule_modulez00(obj_t BgL_pnamez00_54,
		obj_t BgL_mclausez00_55, obj_t BgL_namez00_56, obj_t BgL_clausesz00_57)
	{
		{	/* Module/module.scm 276 */
			{	/* Module/module.scm 278 */
				bool_t BgL_test2001z00_1410;

				if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_namez00_56,
							CNST_TABLE_REF(3))))
					{	/* Module/module.scm 337 */
						BgL_test2001z00_1410 = ((bool_t) 0);
					}
				else
					{	/* Module/module.scm 337 */
						BgL_test2001z00_1410 = ((bool_t) 1);
					}
				if (BgL_test2001z00_1410)
					{	/* Module/module.scm 278 */
						if ((BgL_pnamez00_54 == BgL_namez00_56))
							{	/* Module/module.scm 280 */
								return
									BGl_zc3z04anonymousza31518ze3ze70z60zzmodule_modulez00
									(BgL_namez00_56, BgL_clausesz00_57);
							}
						else
							{	/* Module/module.scm 283 */
								obj_t BgL_arg1546z00_354;

								{	/* Module/module.scm 283 */
									obj_t BgL_arg1552z00_356;
									obj_t BgL_arg1553z00_357;

									{	/* Module/module.scm 283 */
										obj_t BgL_arg1455z00_722;

										BgL_arg1455z00_722 = SYMBOL_TO_STRING(BgL_namez00_56);
										BgL_arg1552z00_356 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_722);
									}
									{	/* Module/module.scm 284 */
										obj_t BgL_arg1455z00_724;

										BgL_arg1455z00_724 = SYMBOL_TO_STRING(BgL_pnamez00_54);
										BgL_arg1553z00_357 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_724);
									}
									{	/* Module/module.scm 282 */
										obj_t BgL_list1554z00_358;

										{	/* Module/module.scm 282 */
											obj_t BgL_arg1559z00_359;

											{	/* Module/module.scm 282 */
												obj_t BgL_arg1561z00_360;

												{	/* Module/module.scm 282 */
													obj_t BgL_arg1564z00_361;

													BgL_arg1564z00_361 =
														MAKE_YOUNG_PAIR(BgL_arg1553z00_357, BNIL);
													BgL_arg1561z00_360 =
														MAKE_YOUNG_PAIR(BGl_string1861z00zzmodule_modulez00,
														BgL_arg1564z00_361);
												}
												BgL_arg1559z00_359 =
													MAKE_YOUNG_PAIR(BgL_arg1552z00_356,
													BgL_arg1561z00_360);
											}
											BgL_list1554z00_358 =
												MAKE_YOUNG_PAIR(BGl_string1862z00zzmodule_modulez00,
												BgL_arg1559z00_359);
										}
										BgL_arg1546z00_354 =
											BGl_stringzd2appendzd2zz__r4_strings_6_7z00
											(BgL_list1554z00_358);
									}
								}
								{	/* Module/module.scm 281 */
									obj_t BgL_list1547z00_355;

									BgL_list1547z00_355 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									return
										BGl_userzd2errorzd2zztools_errorz00
										(BGl_string1863z00zzmodule_modulez00, BgL_arg1546z00_354,
										BgL_mclausez00_55, BgL_list1547z00_355);
								}
							}
					}
				else
					{	/* Module/module.scm 279 */
						obj_t BgL_list1565z00_362;

						BgL_list1565z00_362 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						return
							BGl_userzd2errorzd2zztools_errorz00
							(BGl_string1850z00zzmodule_modulez00,
							BGl_string1858z00zzmodule_modulez00, BgL_mclausez00_55,
							BgL_list1565z00_362);
					}
			}
		}

	}



/* <@anonymous:1518>~0 */
	obj_t BGl_zc3z04anonymousza31518ze3ze70z60zzmodule_modulez00(obj_t
		BgL_namez00_905, obj_t BgL_l1087z00_346)
	{
		{	/* Module/module.scm 288 */
			if (NULLP(BgL_l1087z00_346))
				{	/* Module/module.scm 288 */
					return BNIL;
				}
			else
				{	/* Module/module.scm 288 */
					obj_t BgL_arg1535z00_349;
					obj_t BgL_arg1540z00_350;

					{	/* Module/module.scm 288 */
						obj_t BgL_cz00_351;

						BgL_cz00_351 = CAR(((obj_t) BgL_l1087z00_346));
						BgL_arg1535z00_349 =
							BGl_consumezd2modulezd2clausez12z12zzmodule_modulez00
							(BgL_namez00_905, BgL_cz00_351);
					}
					{	/* Module/module.scm 288 */
						obj_t BgL_arg1544z00_352;

						BgL_arg1544z00_352 = CDR(((obj_t) BgL_l1087z00_346));
						BgL_arg1540z00_350 =
							BGl_zc3z04anonymousza31518ze3ze70z60zzmodule_modulez00
							(BgL_namez00_905, BgL_arg1544z00_352);
					}
					return bgl_append2(BgL_arg1535z00_349, BgL_arg1540z00_350);
				}
		}

	}



/* consume-module! */
	BGL_EXPORTED_DEF obj_t BGl_consumezd2modulez12zc0zzmodule_modulez00(obj_t
		BgL_pnamez00_58, obj_t BgL_modz00_59)
	{
		{	/* Module/module.scm 293 */
			{	/* Module/module.scm 294 */
				obj_t BgL_mclausez00_363;

				BgL_mclausez00_363 =
					BGl_modulezd2mclausezd2zzmodule_modulez00(BgL_modz00_59);
				{

					if (PAIRP(BgL_mclausez00_363))
						{	/* Module/module.scm 295 */
							obj_t BgL_cdrzd2212zd2_370;

							BgL_cdrzd2212zd2_370 = CDR(((obj_t) BgL_mclausez00_363));
							if ((CAR(((obj_t) BgL_mclausez00_363)) == CNST_TABLE_REF(1)))
								{	/* Module/module.scm 295 */
									if (PAIRP(BgL_cdrzd2212zd2_370))
										{	/* Module/module.scm 295 */
											obj_t BgL_carzd2215zd2_374;

											BgL_carzd2215zd2_374 = CAR(BgL_cdrzd2212zd2_370);
											if (SYMBOLP(BgL_carzd2215zd2_374))
												{	/* Module/module.scm 295 */
													return
														BGl_dozd2consumezd2modulez00zzmodule_modulez00
														(BgL_pnamez00_58, BgL_mclausez00_363,
														BgL_carzd2215zd2_374, CDR(BgL_cdrzd2212zd2_370));
												}
											else
												{	/* Module/module.scm 295 */
												BgL_tagzd2205zd2_367:
													{	/* Module/module.scm 299 */
														obj_t BgL_list1585z00_378;

														BgL_list1585z00_378 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														return
															BGl_userzd2errorzd2zztools_errorz00
															(BGl_string1850z00zzmodule_modulez00,
															BGl_string1864z00zzmodule_modulez00,
															BgL_modz00_59, BgL_list1585z00_378);
													}
												}
										}
									else
										{	/* Module/module.scm 295 */
											goto BgL_tagzd2205zd2_367;
										}
								}
							else
								{	/* Module/module.scm 295 */
									goto BgL_tagzd2205zd2_367;
								}
						}
					else
						{	/* Module/module.scm 295 */
							goto BgL_tagzd2205zd2_367;
						}
				}
			}
		}

	}



/* &consume-module! */
	obj_t BGl_z62consumezd2modulez12za2zzmodule_modulez00(obj_t BgL_envz00_831,
		obj_t BgL_pnamez00_832, obj_t BgL_modz00_833)
	{
		{	/* Module/module.scm 293 */
			return
				BGl_consumezd2modulez12zc0zzmodule_modulez00(BgL_pnamez00_832,
				BgL_modz00_833);
		}

	}



/* consume-module-clause! */
	BGL_EXPORTED_DEF obj_t
		BGl_consumezd2modulezd2clausez12z12zzmodule_modulez00(obj_t
		BgL_modulez00_60, obj_t BgL_clausez00_61)
	{
		{	/* Module/module.scm 304 */
			{
				obj_t BgL_cz00_380;

				BgL_cz00_380 = BgL_clausez00_61;
			BgL_zc3z04anonymousza31586ze3z87_381:
				{

					if (PAIRP(BgL_cz00_380))
						{	/* Module/module.scm 305 */
							if ((CAR(((obj_t) BgL_cz00_380)) == CNST_TABLE_REF(7)))
								{	/* Module/module.scm 305 */
									{	/* Module/module.scm 308 */
										obj_t BgL_ncz00_395;

										BgL_ncz00_395 =
											BGl_prognzd2firstzd2expressionz00zztools_prognz00
											(BGl_comptimezd2expandzd2condzd2expandzd2onlyz00zzexpand_epsz00
											(BgL_clausez00_61));
										{	/* Module/module.scm 310 */
											bool_t BgL_test2011z00_1469;

											if ((BgL_ncz00_395 == BUNSPEC))
												{	/* Module/module.scm 310 */
													BgL_test2011z00_1469 = ((bool_t) 1);
												}
											else
												{	/* Module/module.scm 310 */
													BgL_test2011z00_1469 = (BgL_ncz00_395 == BFALSE);
												}
											if (BgL_test2011z00_1469)
												{	/* Module/module.scm 310 */
													return BNIL;
												}
											else
												{
													obj_t BgL_cz00_1473;

													BgL_cz00_1473 = BgL_ncz00_395;
													BgL_cz00_380 = BgL_cz00_1473;
													goto BgL_zc3z04anonymousza31586ze3z87_381;
												}
										}
									}
								}
							else
								{	/* Module/module.scm 305 */
									obj_t BgL_carzd2233zd2_391;

									BgL_carzd2233zd2_391 = CAR(((obj_t) BgL_cz00_380));
									if (SYMBOLP(BgL_carzd2233zd2_391))
										{	/* Module/module.scm 314 */
											obj_t BgL_fun1606z00_733;

											BgL_fun1606z00_733 =
												BGl_findzd2clausezd2consumerz00zzmodule_modulez00
												(BgL_carzd2233zd2_391, BgL_clausez00_61);
											return BGL_PROCEDURE_CALL2(BgL_fun1606z00_733,
												BgL_modulez00_60, BgL_cz00_380);
										}
									else
										{	/* Module/module.scm 305 */
										BgL_tagzd2223zd2_386:
											{	/* Module/module.scm 316 */
												obj_t BgL_list1607z00_399;

												BgL_list1607z00_399 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												return
													BGl_userzd2errorzd2zztools_errorz00
													(BGl_string1850z00zzmodule_modulez00,
													BGl_string1859z00zzmodule_modulez00, BgL_clausez00_61,
													BgL_list1607z00_399);
											}
										}
								}
						}
					else
						{	/* Module/module.scm 305 */
							goto BgL_tagzd2223zd2_386;
						}
				}
			}
		}

	}



/* &consume-module-clause! */
	obj_t BGl_z62consumezd2modulezd2clausez12z70zzmodule_modulez00(obj_t
		BgL_envz00_834, obj_t BgL_modulez00_835, obj_t BgL_clausez00_836)
	{
		{	/* Module/module.scm 304 */
			return
				BGl_consumezd2modulezd2clausez12z12zzmodule_modulez00(BgL_modulez00_835,
				BgL_clausez00_836);
		}

	}



/* find-clause-consumer */
	obj_t BGl_findzd2clausezd2consumerz00zzmodule_modulez00(obj_t
		BgL_keywordz00_62, obj_t BgL_clausez00_63)
	{
		{	/* Module/module.scm 321 */
			{	/* Module/module.scm 323 */
				obj_t BgL_unknownzd2clausezd2consumerz00_837;

				BgL_unknownzd2clausezd2consumerz00_837 =
					MAKE_FX_PROCEDURE
					(BGl_z62unknownzd2clausezd2consumerz62zzmodule_modulez00, (int) (2L),
					(int) (1L));
				PROCEDURE_SET(BgL_unknownzd2clausezd2consumerz00_837, (int) (0L),
					BgL_clausez00_63);
				{
					obj_t BgL_ccz00_403;

					BgL_ccz00_403 = BGl_za2clausezd2compilersza2zd2zzmodule_modulez00;
				BgL_zc3z04anonymousza31608ze3z87_404:
					if (NULLP(BgL_ccz00_403))
						{	/* Module/module.scm 326 */
							return BgL_unknownzd2clausezd2consumerz00_837;
						}
					else
						{	/* Module/module.scm 326 */
							if (
								((((BgL_ccompz00_bglt) COBJECT(
												((BgL_ccompz00_bglt)
													CAR(
														((obj_t) BgL_ccz00_403)))))->BgL_idz00) ==
									BgL_keywordz00_62))
								{	/* Module/module.scm 328 */
									return
										(((BgL_ccompz00_bglt) COBJECT(
												((BgL_ccompz00_bglt)
													CAR(((obj_t) BgL_ccz00_403)))))->BgL_consumerz00);
								}
							else
								{
									obj_t BgL_ccz00_1504;

									BgL_ccz00_1504 = CDR(((obj_t) BgL_ccz00_403));
									BgL_ccz00_403 = BgL_ccz00_1504;
									goto BgL_zc3z04anonymousza31608ze3z87_404;
								}
						}
				}
			}
		}

	}



/* &unknown-clause-consumer */
	obj_t BGl_z62unknownzd2clausezd2consumerz62zzmodule_modulez00(obj_t
		BgL_envz00_838, obj_t BgL_modulez00_840, obj_t BgL_valuesz00_841)
	{
		{	/* Module/module.scm 323 */
			{	/* Module/module.scm 323 */
				obj_t BgL_clausez00_839;

				BgL_clausez00_839 = PROCEDURE_REF(BgL_envz00_838, (int) (0L));
				{	/* Module/module.scm 323 */
					obj_t BgL_list1628z00_939;

					BgL_list1628z00_939 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					return
						BGl_userzd2errorzd2zztools_errorz00
						(BGl_string1850z00zzmodule_modulez00,
						BGl_string1860z00zzmodule_modulez00, BgL_clausez00_839,
						BgL_list1628z00_939);
				}
			}
		}

	}



/* checksum-module */
	obj_t BGl_checksumzd2modulezd2zzmodule_modulez00(obj_t BgL_modz00_65)
	{
		{	/* Module/module.scm 342 */
			{	/* Module/module.scm 343 */
				obj_t BgL_mclausez00_420;

				BgL_mclausez00_420 =
					BGl_modulezd2mclausezd2zzmodule_modulez00(BgL_modz00_65);
				{
					obj_t BgL_namez00_421;
					obj_t BgL_clausesz00_422;

					if (PAIRP(BgL_mclausez00_420))
						{	/* Module/module.scm 344 */
							obj_t BgL_cdrzd2251zd2_427;

							BgL_cdrzd2251zd2_427 = CDR(((obj_t) BgL_mclausez00_420));
							if ((CAR(((obj_t) BgL_mclausez00_420)) == CNST_TABLE_REF(1)))
								{	/* Module/module.scm 344 */
									if (PAIRP(BgL_cdrzd2251zd2_427))
										{	/* Module/module.scm 344 */
											obj_t BgL_carzd2254zd2_431;

											BgL_carzd2254zd2_431 = CAR(BgL_cdrzd2251zd2_427);
											if (SYMBOLP(BgL_carzd2254zd2_431))
												{	/* Module/module.scm 344 */
													BgL_namez00_421 = BgL_carzd2254zd2_431;
													BgL_clausesz00_422 = CDR(BgL_cdrzd2251zd2_427);
													{	/* Module/module.scm 347 */
														bool_t BgL_test2020z00_1526;

														if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																(BgL_namez00_421, CNST_TABLE_REF(3))))
															{	/* Module/module.scm 337 */
																BgL_test2020z00_1526 = ((bool_t) 0);
															}
														else
															{	/* Module/module.scm 337 */
																BgL_test2020z00_1526 = ((bool_t) 1);
															}
														if (BgL_test2020z00_1526)
															{	/* Module/module.scm 347 */
																return
																	BINT
																	(BGl_modulezd2checksumzd2zzmodule_checksumz00
																	(BgL_mclausez00_420,
																		BGl_za2mcozd2includezd2pathza2z00zzengine_paramz00));
															}
														else
															{	/* Module/module.scm 348 */
																obj_t BgL_list1653z00_436;

																BgL_list1653z00_436 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																return
																	BGl_userzd2errorzd2zztools_errorz00
																	(BGl_string1850z00zzmodule_modulez00,
																	BGl_string1858z00zzmodule_modulez00,
																	BgL_modz00_65, BgL_list1653z00_436);
															}
													}
												}
											else
												{	/* Module/module.scm 344 */
												BgL_tagzd2244zd2_424:
													{	/* Module/module.scm 352 */
														obj_t BgL_list1655z00_437;

														BgL_list1655z00_437 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														return
															BGl_userzd2errorzd2zztools_errorz00
															(BGl_string1850z00zzmodule_modulez00,
															BGl_string1864z00zzmodule_modulez00,
															BgL_modz00_65, BgL_list1655z00_437);
													}
												}
										}
									else
										{	/* Module/module.scm 344 */
											goto BgL_tagzd2244zd2_424;
										}
								}
							else
								{	/* Module/module.scm 344 */
									goto BgL_tagzd2244zd2_424;
								}
						}
					else
						{	/* Module/module.scm 344 */
							goto BgL_tagzd2244zd2_424;
						}
				}
			}
		}

	}



/* dump-module */
	BGL_EXPORTED_DEF obj_t BGl_dumpzd2modulezd2zzmodule_modulez00(obj_t
		BgL_modulez00_66)
	{
		{	/* Module/module.scm 357 */
			{	/* Module/module.scm 358 */
				obj_t BgL_outz00_438;

				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* Module/module.scm 359 */
						BgL_outz00_438 = BGl_za2destza2z00zzengine_paramz00;
					}
				else
					{	/* Module/module.scm 359 */
						if ((BGl_za2destza2z00zzengine_paramz00 == CNST_TABLE_REF(10)))
							{	/* Module/module.scm 361 */
								BgL_outz00_438 = BFALSE;
							}
						else
							{	/* Module/module.scm 363 */
								bool_t BgL_test2024z00_1543;

								if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
									{	/* Module/module.scm 363 */
										obj_t BgL_tmpz00_1546;

										BgL_tmpz00_1546 =
											CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
										BgL_test2024z00_1543 = STRINGP(BgL_tmpz00_1546);
									}
								else
									{	/* Module/module.scm 363 */
										BgL_test2024z00_1543 = ((bool_t) 0);
									}
								if (BgL_test2024z00_1543)
									{	/* Module/module.scm 363 */
										BgL_outz00_438 =
											string_append(BGl_prefixz00zz__osz00(CAR
												(BGl_za2srczd2filesza2zd2zzengine_paramz00)),
											BGl_string1865z00zzmodule_modulez00);
									}
								else
									{	/* Module/module.scm 363 */
										BgL_outz00_438 = BFALSE;
									}
							}
					}
				{	/* Module/module.scm 358 */
					obj_t BgL_portz00_439;

					if (STRINGP(BgL_outz00_438))
						{	/* Module/module.scm 368 */

							BgL_portz00_439 =
								BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00
								(BgL_outz00_438, BTRUE);
						}
					else
						{	/* Module/module.scm 369 */
							obj_t BgL_tmpz00_1555;

							BgL_tmpz00_1555 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_portz00_439 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1555);
						}
					{	/* Module/module.scm 367 */

						if (OUTPUT_PORTP(BgL_portz00_439))
							{	/* Module/module.scm 372 */
								obj_t BgL_exitd1067z00_441;

								BgL_exitd1067z00_441 = BGL_EXITD_TOP_AS_OBJ();
								{	/* Module/module.scm 376 */
									obj_t BgL_zc3z04anonymousza31658ze3z87_842;

									BgL_zc3z04anonymousza31658ze3z87_842 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31658ze3ze5zzmodule_modulez00,
										(int) (0L), (int) (1L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31658ze3z87_842,
										(int) (0L), BgL_portz00_439);
									{	/* Module/module.scm 372 */
										obj_t BgL_arg1828z00_752;

										{	/* Module/module.scm 372 */
											obj_t BgL_arg1829z00_753;

											BgL_arg1829z00_753 =
												BGL_EXITD_PROTECT(BgL_exitd1067z00_441);
											BgL_arg1828z00_752 =
												MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31658ze3z87_842,
												BgL_arg1829z00_753);
										}
										BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_441,
											BgL_arg1828z00_752);
										BUNSPEC;
									}
									{	/* Module/module.scm 374 */
										obj_t BgL_tmp1069z00_443;

										{	/* Module/module.scm 374 */
											obj_t BgL_list1657z00_444;

											BgL_list1657z00_444 =
												MAKE_YOUNG_PAIR(BgL_portz00_439, BNIL);
											BGl_writez00zz__r4_output_6_10_3z00(BgL_modulez00_66,
												BgL_list1657z00_444);
										}
										BgL_tmp1069z00_443 =
											bgl_display_char(((unsigned char) 10), BgL_portz00_439);
										{	/* Module/module.scm 372 */
											bool_t BgL_test2028z00_1572;

											{	/* Module/module.scm 372 */
												obj_t BgL_arg1827z00_756;

												BgL_arg1827z00_756 =
													BGL_EXITD_PROTECT(BgL_exitd1067z00_441);
												BgL_test2028z00_1572 = PAIRP(BgL_arg1827z00_756);
											}
											if (BgL_test2028z00_1572)
												{	/* Module/module.scm 372 */
													obj_t BgL_arg1825z00_757;

													{	/* Module/module.scm 372 */
														obj_t BgL_arg1826z00_758;

														BgL_arg1826z00_758 =
															BGL_EXITD_PROTECT(BgL_exitd1067z00_441);
														BgL_arg1825z00_757 =
															CDR(((obj_t) BgL_arg1826z00_758));
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1067z00_441,
														BgL_arg1825z00_757);
													BUNSPEC;
												}
											else
												{	/* Module/module.scm 372 */
													BFALSE;
												}
										}
										bgl_close_output_port(BgL_portz00_439);
										return BgL_tmp1069z00_443;
									}
								}
							}
						else
							{	/* Module/module.scm 370 */
								return
									BGl_errorz00zz__errorz00(CNST_TABLE_REF(11),
									BGl_string1866z00zzmodule_modulez00, BgL_outz00_438);
							}
					}
				}
			}
		}

	}



/* &dump-module */
	obj_t BGl_z62dumpzd2modulezb0zzmodule_modulez00(obj_t BgL_envz00_843,
		obj_t BgL_modulez00_844)
	{
		{	/* Module/module.scm 357 */
			return BGl_dumpzd2modulezd2zzmodule_modulez00(BgL_modulez00_844);
		}

	}



/* &<@anonymous:1658> */
	obj_t BGl_z62zc3z04anonymousza31658ze3ze5zzmodule_modulez00(obj_t
		BgL_envz00_845)
	{
		{	/* Module/module.scm 372 */
			{	/* Module/module.scm 376 */
				obj_t BgL_portz00_846;

				BgL_portz00_846 = PROCEDURE_REF(BgL_envz00_845, (int) (0L));
				return bgl_close_output_port(((obj_t) BgL_portz00_846));
			}
		}

	}



/* module-checksum-object */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2checksumzd2objectz00zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 381 */
			{	/* Module/module.scm 382 */
				obj_t BgL_list1682z00_458;

				{	/* Module/module.scm 382 */
					obj_t BgL_arg1688z00_459;

					{	/* Module/module.scm 382 */
						obj_t BgL_arg1689z00_460;

						BgL_arg1689z00_460 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1688z00_459 =
							MAKE_YOUNG_PAIR(BGl_string1867z00zzmodule_modulez00,
							BgL_arg1689z00_460);
					}
					BgL_list1682z00_458 =
						MAKE_YOUNG_PAIR(BGl_string1848z00zzmodule_modulez00,
						BgL_arg1688z00_459);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1682z00_458);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1867z00zzmodule_modulez00;
			{	/* Module/module.scm 382 */
				obj_t BgL_g1071z00_461;

				BgL_g1071z00_461 = BNIL;
				{
					obj_t BgL_hooksz00_464;
					obj_t BgL_hnamesz00_465;

					BgL_hooksz00_464 = BgL_g1071z00_461;
					BgL_hnamesz00_465 = BNIL;
				BgL_zc3z04anonymousza31690ze3z87_466:
					if (NULLP(BgL_hooksz00_464))
						{	/* Module/module.scm 382 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Module/module.scm 382 */
							bool_t BgL_test2030z00_1597;

							{	/* Module/module.scm 382 */
								obj_t BgL_fun1702z00_473;

								BgL_fun1702z00_473 = CAR(((obj_t) BgL_hooksz00_464));
								BgL_test2030z00_1597 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1702z00_473));
							}
							if (BgL_test2030z00_1597)
								{	/* Module/module.scm 382 */
									obj_t BgL_arg1699z00_470;
									obj_t BgL_arg1700z00_471;

									BgL_arg1699z00_470 = CDR(((obj_t) BgL_hooksz00_464));
									BgL_arg1700z00_471 = CDR(((obj_t) BgL_hnamesz00_465));
									{
										obj_t BgL_hnamesz00_1609;
										obj_t BgL_hooksz00_1608;

										BgL_hooksz00_1608 = BgL_arg1699z00_470;
										BgL_hnamesz00_1609 = BgL_arg1700z00_471;
										BgL_hnamesz00_465 = BgL_hnamesz00_1609;
										BgL_hooksz00_464 = BgL_hooksz00_1608;
										goto BgL_zc3z04anonymousza31690ze3z87_466;
									}
								}
							else
								{	/* Module/module.scm 382 */
									obj_t BgL_arg1701z00_472;

									BgL_arg1701z00_472 = CAR(((obj_t) BgL_hnamesz00_465));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1867z00zzmodule_modulez00,
										BGl_string1849z00zzmodule_modulez00, BgL_arg1701z00_472);
								}
						}
				}
			}
			{	/* Module/module.scm 383 */
				obj_t BgL_checksumz00_476;
				obj_t BgL_destz00_477;

				BgL_checksumz00_476 = BGl_za2modulezd2checksumza2zd2zzmodule_modulez00;
				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* Module/module.scm 384 */
						if ((BGl_za2passza2z00zzengine_paramz00 == CNST_TABLE_REF(12)))
							{	/* Module/module.scm 389 */
								BgL_destz00_477 = BGl_za2destza2z00zzengine_paramz00;
							}
						else
							{	/* Module/module.scm 389 */
								BgL_destz00_477 =
									string_append(BGl_prefixz00zz__osz00
									(BGl_za2destza2z00zzengine_paramz00),
									BGl_string1868z00zzmodule_modulez00);
							}
					}
				else
					{	/* Module/module.scm 385 */
						bool_t BgL_test2033z00_1620;

						if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
							{	/* Module/module.scm 386 */
								obj_t BgL_tmpz00_1623;

								BgL_tmpz00_1623 =
									CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
								BgL_test2033z00_1620 = STRINGP(BgL_tmpz00_1623);
							}
						else
							{	/* Module/module.scm 385 */
								BgL_test2033z00_1620 = ((bool_t) 0);
							}
						if (BgL_test2033z00_1620)
							{	/* Module/module.scm 385 */
								BgL_destz00_477 =
									string_append(BGl_prefixz00zz__osz00(CAR
										(BGl_za2srczd2filesza2zd2zzengine_paramz00)),
									BGl_string1868z00zzmodule_modulez00);
							}
						else
							{	/* Module/module.scm 385 */
								BgL_destz00_477 = BFALSE;
							}
					}
				{

					{	/* Module/module.scm 404 */
						bool_t BgL_test2035z00_1629;

						if (STRINGP(BgL_destz00_477))
							{	/* Module/module.scm 404 */
								BgL_test2035z00_1629 =
									fexists(BSTRING_TO_STRING(BgL_destz00_477));
							}
						else
							{	/* Module/module.scm 404 */
								BgL_test2035z00_1629 = ((bool_t) 0);
							}
						if (BgL_test2035z00_1629)
							{	/* Module/module.scm 407 */
								obj_t BgL_iportz00_481;

								{	/* Module/module.scm 407 */

									BgL_iportz00_481 =
										BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
										(BgL_destz00_477, BTRUE, BINT(5000000L));
								}
								if (INPUT_PORTP(BgL_iportz00_481))
									{	/* Module/module.scm 412 */
										obj_t BgL_csz00_483;

										{	/* Module/module.scm 412 */

											{	/* Module/module.scm 412 */

												BgL_csz00_483 =
													BGl_readz00zz__readerz00(BgL_iportz00_481, BFALSE);
											}
										}
										bgl_close_input_port(BgL_iportz00_481);
										if (
											((long) CINT(BgL_csz00_483) ==
												(long) CINT(BgL_checksumz00_476)))
											{	/* Module/module.scm 414 */
												BFALSE;
											}
										else
											{	/* Module/module.scm 414 */
												{	/* Module/module.scm 416 */
													char *BgL_stringz00_778;

													BgL_stringz00_778 =
														BSTRING_TO_STRING(BgL_destz00_477);
													if (unlink(BgL_stringz00_778))
														{	/* Module/module.scm 416 */
															((bool_t) 0);
														}
													else
														{	/* Module/module.scm 416 */
															((bool_t) 1);
														}
												}
											BgL_zc3z04anonymousza31709ze3z87_492:
												if (STRINGP(BgL_destz00_477))
													{	/* Module/module.scm 394 */
														obj_t BgL_oportz00_494;

														{	/* Module/module.scm 394 */

															BgL_oportz00_494 =
																BGl_openzd2outputzd2filez00zz__r4_ports_6_10_1z00
																(BgL_destz00_477, BTRUE);
														}
														if (OUTPUT_PORTP(BgL_oportz00_494))
															{	/* Module/module.scm 395 */
																bgl_display_obj(BgL_checksumz00_476,
																	BgL_oportz00_494);
																bgl_display_char(((unsigned char) 10),
																	BgL_oportz00_494);
																bgl_close_output_port(BgL_oportz00_494);
															}
														else
															{	/* Module/module.scm 395 */
																BGl_errorz00zz__errorz00
																	(BGl_string1869z00zzmodule_modulez00,
																	BGl_string1870z00zzmodule_modulez00,
																	BgL_destz00_477);
															}
													}
												else
													{	/* Module/module.scm 402 */
														obj_t BgL_port1089z00_499;

														{	/* Module/module.scm 402 */
															obj_t BgL_tmpz00_1656;

															BgL_tmpz00_1656 = BGL_CURRENT_DYNAMIC_ENV();
															BgL_port1089z00_499 =
																BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_1656);
														}
														bgl_display_obj(BgL_checksumz00_476,
															BgL_port1089z00_499);
														bgl_display_char(((unsigned char) 10),
															BgL_port1089z00_499);
									}}}
								else
									{	/* Module/module.scm 408 */
										BGl_userzd2errorzd2zztools_errorz00
											(BGl_string1869z00zzmodule_modulez00,
											BGl_string1871z00zzmodule_modulez00, BgL_destz00_477,
											BNIL);
									}
							}
						else
							{	/* Module/module.scm 404 */
								goto BgL_zc3z04anonymousza31709ze3z87_492;
							}
					}
				}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Module/module.scm 420 */
					{	/* Module/module.scm 420 */
						obj_t BgL_port1090z00_512;

						{	/* Module/module.scm 420 */
							obj_t BgL_tmpz00_1665;

							BgL_tmpz00_1665 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1090z00_512 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1665);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1090z00_512);
						bgl_display_string(BGl_string1853z00zzmodule_modulez00,
							BgL_port1090z00_512);
						{	/* Module/module.scm 420 */
							obj_t BgL_arg1735z00_513;

							{	/* Module/module.scm 420 */
								bool_t BgL_test2043z00_1670;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Module/module.scm 420 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Module/module.scm 420 */
												BgL_test2043z00_1670 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Module/module.scm 420 */
												BgL_test2043z00_1670 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Module/module.scm 420 */
										BgL_test2043z00_1670 = ((bool_t) 0);
									}
								if (BgL_test2043z00_1670)
									{	/* Module/module.scm 420 */
										BgL_arg1735z00_513 = BGl_string1854z00zzmodule_modulez00;
									}
								else
									{	/* Module/module.scm 420 */
										BgL_arg1735z00_513 = BGl_string1855z00zzmodule_modulez00;
									}
							}
							bgl_display_obj(BgL_arg1735z00_513, BgL_port1090z00_512);
						}
						bgl_display_string(BGl_string1856z00zzmodule_modulez00,
							BgL_port1090z00_512);
						bgl_display_char(((unsigned char) 10), BgL_port1090z00_512);
					}
					{	/* Module/module.scm 420 */
						obj_t BgL_list1738z00_517;

						BgL_list1738z00_517 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						return BGl_exitz00zz__errorz00(BgL_list1738z00_517);
					}
				}
			else
				{	/* Module/module.scm 420 */
					obj_t BgL_g1073z00_518;

					BgL_g1073z00_518 = BNIL;
					{
						obj_t BgL_hooksz00_521;
						obj_t BgL_hnamesz00_522;

						BgL_hooksz00_521 = BgL_g1073z00_518;
						BgL_hnamesz00_522 = BNIL;
					BgL_zc3z04anonymousza31739ze3z87_523:
						if (NULLP(BgL_hooksz00_521))
							{	/* Module/module.scm 420 */
								return BTRUE;
							}
						else
							{	/* Module/module.scm 420 */
								bool_t BgL_test2047z00_1687;

								{	/* Module/module.scm 420 */
									obj_t BgL_fun1749z00_530;

									BgL_fun1749z00_530 = CAR(((obj_t) BgL_hooksz00_521));
									BgL_test2047z00_1687 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1749z00_530));
								}
								if (BgL_test2047z00_1687)
									{	/* Module/module.scm 420 */
										obj_t BgL_arg1746z00_527;
										obj_t BgL_arg1747z00_528;

										BgL_arg1746z00_527 = CDR(((obj_t) BgL_hooksz00_521));
										BgL_arg1747z00_528 = CDR(((obj_t) BgL_hnamesz00_522));
										{
											obj_t BgL_hnamesz00_1699;
											obj_t BgL_hooksz00_1698;

											BgL_hooksz00_1698 = BgL_arg1746z00_527;
											BgL_hnamesz00_1699 = BgL_arg1747z00_528;
											BgL_hnamesz00_522 = BgL_hnamesz00_1699;
											BgL_hooksz00_521 = BgL_hooksz00_1698;
											goto BgL_zc3z04anonymousza31739ze3z87_523;
										}
									}
								else
									{	/* Module/module.scm 420 */
										obj_t BgL_arg1748z00_529;

										BgL_arg1748z00_529 = CAR(((obj_t) BgL_hnamesz00_522));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string1857z00zzmodule_modulez00, BgL_arg1748z00_529);
									}
							}
					}
				}
		}

	}



/* &module-checksum-object */
	obj_t BGl_z62modulezd2checksumzd2objectz62zzmodule_modulez00(obj_t
		BgL_envz00_847)
	{
		{	/* Module/module.scm 381 */
			return BGl_modulezd2checksumzd2objectz00zzmodule_modulez00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 15 */
			{	/* Module/module.scm 46 */
				obj_t BgL_arg1754z00_536;
				obj_t BgL_arg1755z00_537;

				{	/* Module/module.scm 46 */
					obj_t BgL_v1091z00_551;

					BgL_v1091z00_551 = create_vector(4L);
					{	/* Module/module.scm 46 */
						obj_t BgL_arg1767z00_552;

						BgL_arg1767z00_552 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(13),
							BGl_proc1873z00zzmodule_modulez00,
							BGl_proc1872z00zzmodule_modulez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1091z00_551, 0L, BgL_arg1767z00_552);
					}
					{	/* Module/module.scm 46 */
						obj_t BgL_arg1775z00_562;

						BgL_arg1775z00_562 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc1876z00zzmodule_modulez00,
							BGl_proc1875z00zzmodule_modulez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc1874z00zzmodule_modulez00, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1091z00_551, 1L, BgL_arg1775z00_562);
					}
					{	/* Module/module.scm 46 */
						obj_t BgL_arg1812z00_577;

						BgL_arg1812z00_577 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc1879z00zzmodule_modulez00,
							BGl_proc1878z00zzmodule_modulez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc1877z00zzmodule_modulez00, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1091z00_551, 2L, BgL_arg1812z00_577);
					}
					{	/* Module/module.scm 46 */
						obj_t BgL_arg1831z00_593;

						BgL_arg1831z00_593 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc1882z00zzmodule_modulez00,
							BGl_proc1881z00zzmodule_modulez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc1880z00zzmodule_modulez00, CNST_TABLE_REF(16));
						VECTOR_SET(BgL_v1091z00_551, 3L, BgL_arg1831z00_593);
					}
					BgL_arg1754z00_536 = BgL_v1091z00_551;
				}
				{	/* Module/module.scm 46 */
					obj_t BgL_v1092z00_607;

					BgL_v1092z00_607 = create_vector(0L);
					BgL_arg1755z00_537 = BgL_v1092z00_607;
				}
				return (BGl_ccompz00zzmodule_modulez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(19),
						CNST_TABLE_REF(20), BGl_objectz00zz__objectz00, 46649L,
						BGl_proc1885z00zzmodule_modulez00,
						BGl_proc1884z00zzmodule_modulez00, BFALSE,
						BGl_proc1883z00zzmodule_modulez00, BFALSE, BgL_arg1754z00_536,
						BgL_arg1755z00_537), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1766> */
	obj_t BGl_z62zc3z04anonymousza31766ze3ze5zzmodule_modulez00(obj_t
		BgL_envz00_868, obj_t BgL_new1053z00_869)
	{
		{	/* Module/module.scm 46 */
			{
				BgL_ccompz00_bglt BgL_auxz00_1725;

				((((BgL_ccompz00_bglt) COBJECT(
								((BgL_ccompz00_bglt) BgL_new1053z00_869)))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(21)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(((BgL_ccompz00_bglt)
									BgL_new1053z00_869)))->BgL_producerz00) =
					((obj_t) BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(((BgL_ccompz00_bglt)
									BgL_new1053z00_869)))->BgL_consumerz00) =
					((obj_t) BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(((BgL_ccompz00_bglt)
									BgL_new1053z00_869)))->BgL_finaliza7erza7) =
					((obj_t) BGl_conszd2envzd2zz__r4_pairs_and_lists_6_3z00), BUNSPEC);
				BgL_auxz00_1725 = ((BgL_ccompz00_bglt) BgL_new1053z00_869);
				return ((obj_t) BgL_auxz00_1725);
			}
		}

	}



/* &lambda1762 */
	BgL_ccompz00_bglt BGl_z62lambda1762z62zzmodule_modulez00(obj_t BgL_envz00_870)
	{
		{	/* Module/module.scm 46 */
			{	/* Module/module.scm 46 */
				BgL_ccompz00_bglt BgL_new1052z00_941;

				BgL_new1052z00_941 =
					((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ccompz00_bgl))));
				{	/* Module/module.scm 46 */
					long BgL_arg1765z00_942;

					BgL_arg1765z00_942 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1052z00_941), BgL_arg1765z00_942);
				}
				return BgL_new1052z00_941;
			}
		}

	}



/* &lambda1756 */
	BgL_ccompz00_bglt BGl_z62lambda1756z62zzmodule_modulez00(obj_t BgL_envz00_871,
		obj_t BgL_id1048z00_872, obj_t BgL_producer1049z00_873,
		obj_t BgL_consumer1050z00_874, obj_t BgL_finaliza7er1051za7_875)
	{
		{	/* Module/module.scm 46 */
			{	/* Module/module.scm 46 */
				BgL_ccompz00_bglt BgL_new1076z00_947;

				{	/* Module/module.scm 46 */
					BgL_ccompz00_bglt BgL_new1075z00_948;

					BgL_new1075z00_948 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/module.scm 46 */
						long BgL_arg1761z00_949;

						BgL_arg1761z00_949 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1075z00_948), BgL_arg1761z00_949);
					}
					BgL_new1076z00_947 = BgL_new1075z00_948;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1076z00_947))->BgL_idz00) =
					((obj_t) ((obj_t) BgL_id1048z00_872)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1076z00_947))->BgL_producerz00) =
					((obj_t) ((obj_t) BgL_producer1049z00_873)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1076z00_947))->BgL_consumerz00) =
					((obj_t) ((obj_t) BgL_consumer1050z00_874)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1076z00_947))->
						BgL_finaliza7erza7) =
					((obj_t) ((obj_t) BgL_finaliza7er1051za7_875)), BUNSPEC);
				return BgL_new1076z00_947;
			}
		}

	}



/* &<@anonymous:1837> */
	obj_t BGl_z62zc3z04anonymousza31837ze3ze5zzmodule_modulez00(obj_t
		BgL_envz00_876)
	{
		{	/* Module/module.scm 46 */
			return BGl_proc1886z00zzmodule_modulez00;
		}

	}



/* &lambda1836 */
	obj_t BGl_z62lambda1836z62zzmodule_modulez00(obj_t BgL_envz00_877,
		obj_t BgL_oz00_878, obj_t BgL_vz00_879)
	{
		{	/* Module/module.scm 46 */
			return
				((((BgL_ccompz00_bglt) COBJECT(
							((BgL_ccompz00_bglt) BgL_oz00_878)))->BgL_finaliza7erza7) =
				((obj_t) ((obj_t) BgL_vz00_879)), BUNSPEC);
		}

	}



/* &lambda1835 */
	obj_t BGl_z62lambda1835z62zzmodule_modulez00(obj_t BgL_envz00_880,
		obj_t BgL_oz00_881)
	{
		{	/* Module/module.scm 46 */
			return
				(((BgL_ccompz00_bglt) COBJECT(
						((BgL_ccompz00_bglt) BgL_oz00_881)))->BgL_finaliza7erza7);
		}

	}



/* &<@anonymous:1838> */
	obj_t BGl_z62zc3z04anonymousza31838ze3ze5zzmodule_modulez00(obj_t
		BgL_envz00_882)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(22);
		}

	}



/* &<@anonymous:1826> */
	obj_t BGl_z62zc3z04anonymousza31826ze3ze5zzmodule_modulez00(obj_t
		BgL_envz00_883)
	{
		{	/* Module/module.scm 46 */
			return BGl_proc1887z00zzmodule_modulez00;
		}

	}



/* &lambda1825 */
	obj_t BGl_z62lambda1825z62zzmodule_modulez00(obj_t BgL_envz00_884,
		obj_t BgL_oz00_885, obj_t BgL_vz00_886)
	{
		{	/* Module/module.scm 46 */
			return
				((((BgL_ccompz00_bglt) COBJECT(
							((BgL_ccompz00_bglt) BgL_oz00_885)))->BgL_consumerz00) = ((obj_t)
					((obj_t) BgL_vz00_886)), BUNSPEC);
		}

	}



/* &lambda1824 */
	obj_t BGl_z62lambda1824z62zzmodule_modulez00(obj_t BgL_envz00_887,
		obj_t BgL_oz00_888)
	{
		{	/* Module/module.scm 46 */
			return
				(((BgL_ccompz00_bglt) COBJECT(
						((BgL_ccompz00_bglt) BgL_oz00_888)))->BgL_consumerz00);
		}

	}



/* &<@anonymous:1827> */
	obj_t BGl_z62zc3z04anonymousza31827ze3ze5zzmodule_modulez00(obj_t
		BgL_envz00_889, obj_t BgL_mz00_890, obj_t BgL_cz00_891)
	{
		{	/* Module/module.scm 54 */
			return BNIL;
		}

	}



/* &<@anonymous:1808> */
	obj_t BGl_z62zc3z04anonymousza31808ze3ze5zzmodule_modulez00(obj_t
		BgL_envz00_892)
	{
		{	/* Module/module.scm 46 */
			return BGl_proc1888z00zzmodule_modulez00;
		}

	}



/* &lambda1807 */
	obj_t BGl_z62lambda1807z62zzmodule_modulez00(obj_t BgL_envz00_893,
		obj_t BgL_oz00_894, obj_t BgL_vz00_895)
	{
		{	/* Module/module.scm 46 */
			return
				((((BgL_ccompz00_bglt) COBJECT(
							((BgL_ccompz00_bglt) BgL_oz00_894)))->BgL_producerz00) = ((obj_t)
					((obj_t) BgL_vz00_895)), BUNSPEC);
		}

	}



/* &lambda1806 */
	obj_t BGl_z62lambda1806z62zzmodule_modulez00(obj_t BgL_envz00_896,
		obj_t BgL_oz00_897)
	{
		{	/* Module/module.scm 46 */
			return
				(((BgL_ccompz00_bglt) COBJECT(
						((BgL_ccompz00_bglt) BgL_oz00_897)))->BgL_producerz00);
		}

	}



/* &<@anonymous:1809> */
	obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzmodule_modulez00(obj_t
		BgL_envz00_898, obj_t BgL_cz00_899)
	{
		{	/* Module/module.scm 51 */
			return BNIL;
		}

	}



/* &lambda1773 */
	obj_t BGl_z62lambda1773z62zzmodule_modulez00(obj_t BgL_envz00_900,
		obj_t BgL_oz00_901, obj_t BgL_vz00_902)
	{
		{	/* Module/module.scm 46 */
			return
				((((BgL_ccompz00_bglt) COBJECT(
							((BgL_ccompz00_bglt) BgL_oz00_901)))->BgL_idz00) = ((obj_t)
					((obj_t) BgL_vz00_902)), BUNSPEC);
		}

	}



/* &lambda1772 */
	obj_t BGl_z62lambda1772z62zzmodule_modulez00(obj_t BgL_envz00_903,
		obj_t BgL_oz00_904)
	{
		{	/* Module/module.scm 46 */
			return
				(((BgL_ccompz00_bglt) COBJECT(
						((BgL_ccompz00_bglt) BgL_oz00_904)))->BgL_idz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_modulez00(void)
	{
		{	/* Module/module.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzheap_restorez00(147989063L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_mainz00(210000924L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_statexpz00(161201109L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_withz00(54249554L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_typez00(410405073L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_foreignz00(42743976L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_wasmz00(331440320L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_javaz00(93941037L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_evalz00(428236825L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_loadz00(422032804L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_pragmaz00(433305536L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_checksumz00(457474182L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_optionz00(130650116L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzmodule_alibraryz00(316727058L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzread_includez00(236488026L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
			return
				BGl_modulezd2initializa7ationz75zzread_inlinez00(500058826L,
				BSTRING_TO_STRING(BGl_string1889z00zzmodule_modulez00));
		}

	}

#ifdef __cplusplus
}
#endif
