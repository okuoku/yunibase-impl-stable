/*===========================================================================*/
/*   (Module/class.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/class.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_CLASS_TYPE_DEFINITIONS
#define BGL_MODULE_CLASS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_jclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		obj_t BgL_packagez00;
	}                *BgL_jclassz00_bglt;

	typedef struct BgL_slotz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_srcz00;
		obj_t BgL_classzd2ownerzd2;
		long BgL_indexz00;
		obj_t BgL_typez00;
		bool_t BgL_readzd2onlyzf3z21;
		obj_t BgL_defaultzd2valuezd2;
		obj_t BgL_virtualzd2numzd2;
		bool_t BgL_virtualzd2overridezd2;
		obj_t BgL_getterz00;
		obj_t BgL_setterz00;
		obj_t BgL_userzd2infozd2;
	}              *BgL_slotz00_bglt;


#endif													// BGL_MODULE_CLASS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62declarezd2classz12za2zzmodule_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_forcezd2classzd2accessesz00zzmodule_classz00(void);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_makezd2classzd2fieldsz00zzmodule_classz00(obj_t);
	extern obj_t
		BGl_classgenzd2makezd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_classz00 = BUNSPEC;
	static long BGl_getzd2classzd2hashz00zzmodule_classz00(obj_t);
	static obj_t
		BGl_z62classzd2finaliza7erzd2addzd2staticz12z05zzmodule_classz00(obj_t);
	static obj_t BGl_za2methodzd2unitza2zd2zzmodule_classz00 = BUNSPEC;
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	static obj_t BGl_za2genericzd2unitza2zd2zzmodule_classz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	extern bool_t BGl_widezd2classzf3z21zzobject_classz00(obj_t);
	extern obj_t BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00;
	static obj_t BGl_toplevelzd2initzd2zzmodule_classz00(void);
	BGL_EXPORTED_DECL obj_t BGl_classzd2finaliza7erz75zzmodule_classz00(void);
	static obj_t BGl_z62classzd2finaliza7erz17zzmodule_classz00(obj_t);
	extern bool_t
		BGl_checkzd2classzd2declarationzf3zf3zzobject_classz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_declarezd2classz12zc0zzmodule_classz00(obj_t,
		obj_t, obj_t, bool_t, bool_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_classz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_za2declaredzd2classesza2zd2zzmodule_classz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_declarezd2widezd2classz12z12zzmodule_classz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62delayzd2classzd2accessorsz12z70zzmodule_classz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_classz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_za2debugzd2moduleza2zd2zzengine_paramz00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzmodule_classz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_classz00(void);
	BGL_IMPORT obj_t BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t);
	static obj_t BGl_genzd2registerzd2classz12z12zzmodule_classz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31333ze3ze5zzmodule_classz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2objectzd2unitz00zzmodule_classz00(void);
	static obj_t BGl_z62getzd2objectzd2unitz62zzmodule_classz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31326ze3ze5zzmodule_classz00(obj_t);
	extern BgL_typez00_bglt
		BGl_declarezd2classzd2typez12z12zzobject_classz00(obj_t, BgL_globalz00_bglt,
		obj_t, bool_t, bool_t, obj_t);
	extern obj_t
		BGl_classgenzd2allocatezd2anonymousz00zzobject_classgenz00
		(BgL_typez00_bglt);
	BGL_IMPORT long BGl_getzd2hashnumberzd2persistentz00zz__hashz00(obj_t);
	extern obj_t BGl_idzd2ofzd2idz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_genzd2classzd2coercionsz12z12zzobject_coercionz00(obj_t);
	extern obj_t BGl_setzd2classzd2slotsz12z12zzobject_classz00(BgL_typez00_bglt,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2methodzd2unitz00zzmodule_classz00(void);
	extern obj_t BGl_jclassz00zzobject_classz00;
	static obj_t BGl_z62getzd2methodzd2unitz62zzmodule_classz00(obj_t);
	extern obj_t
		BGl_classgenzd2widenzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t BGl_makezd2classzd2virtualzd2fieldszd2zzmodule_classz00(obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	static obj_t BGl_setterze70ze7zzmodule_classz00(obj_t, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	extern bool_t BGl_slotzd2defaultzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classgenz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_coercionz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_objectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern bool_t BGl_slotzd2virtualzf3z21zzobject_slotsz00(BgL_slotz00_bglt);
	static obj_t BGl_za2objectzd2unitza2zd2zzmodule_classz00 = BUNSPEC;
	static obj_t BGl_slotzd2defaultzd2exprze70ze7zzmodule_classz00(obj_t);
	extern obj_t BGl_importzd2parserzd2zzmodule_impusez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_typezd2defaultzd2idze70ze7zzmodule_classz00(obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zzmodule_classz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_classz00(void);
	static BgL_typez00_bglt
		BGl_declarezd2importzd2classz12z12zzmodule_classz00(obj_t, obj_t, obj_t,
		bool_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_classz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_classz00(void);
	extern obj_t
		BGl_classgenzd2slotzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt,
		BgL_slotz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31546ze3ze5zzmodule_classz00(obj_t,
		obj_t);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_epairifyzd2reczd2zztools_miscz00(obj_t, obj_t);
	extern obj_t
		BGl_classgenzd2shrinkzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_getzd2genericzd2unitz00zzmodule_classz00(void);
	static obj_t BGl_z62zc3z04anonymousza31807ze3ze5zzmodule_classz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_classzd2finaliza7erzd2addzd2staticz12z67zzmodule_classz00(void);
	extern obj_t
		BGl_findzd2classzd2constructorz00zzobject_classz00(BgL_typez00_bglt);
	static obj_t BGl_epairifyzd2slotze70z35zzmodule_classz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_delayzd2classzd2accessorsz12z12zzmodule_classz00(BgL_typez00_bglt,
		obj_t);
	static long BGl_loopze70ze7zzmodule_classz00(obj_t, long);
	static obj_t BGl_za2classzd2accessesza2zd2zzmodule_classz00 = BUNSPEC;
	static obj_t BGl_getterze70ze7zzmodule_classz00(obj_t, obj_t);
	static obj_t BGl_z62declarezd2widezd2classz12z70zzmodule_classz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62getzd2genericzd2unitz62zzmodule_classz00(obj_t);
	static BgL_typez00_bglt
		BGl_declarezd2exportzd2classz12z12zzmodule_classz00(obj_t, obj_t, obj_t,
		bool_t, obj_t, obj_t, obj_t);
	extern obj_t
		BGl_classgenzd2nilzd2anonymousz00zzobject_classgenz00(BgL_typez00_bglt);
	static obj_t __cnst[33];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2finaliza7erzd2envza7zzmodule_classz00,
		BgL_bgl_za762classza7d2final2079z00,
		BGl_z62classzd2finaliza7erz17zzmodule_classz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2074z00zzmodule_classz00,
		BgL_bgl_string2074za700za7za7m2080za7, "bgl_init_module_debug_object(\"",
		30);
	      DEFINE_STRING(BGl_string2075z00zzmodule_classz00,
		BgL_bgl_string2075za700za7za7m2081za7, "\")", 2);
	      DEFINE_STRING(BGl_string2076z00zzmodule_classz00,
		BgL_bgl_string2076za700za7za7m2082za7, "module_class", 12);
	      DEFINE_STRING(BGl_string2077z00zzmodule_classz00,
		BgL_bgl_string2077za700za7za7m2083za7,
		"generic method unit object (#unspecified) begin pragma::void module void staged call-next-virtual-setter call-next-virtual-getter call-next-slot cons obj _ call-virtual-setter v lambda call-virtual-getter o make-class-field vector define quote register-class! __object @ widening wide plain final (export static) ",
		313);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_classzd2finaliza7erzd2addzd2staticz12zd2envzb5zzmodule_classz00,
		BgL_bgl_za762classza7d2final2084z00,
		BGl_z62classzd2finaliza7erzd2addzd2staticz12z05zzmodule_classz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2widezd2classz12zd2envzc0zzmodule_classz00,
		BgL_bgl_za762declareza7d2wid2085z00,
		BGl_z62declarezd2widezd2classz12z70zzmodule_classz00, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2objectzd2unitzd2envzd2zzmodule_classz00,
		BgL_bgl_za762getza7d2objectza72086za7,
		BGl_z62getzd2objectzd2unitz62zzmodule_classz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_delayzd2classzd2accessorsz12zd2envzc0zzmodule_classz00,
		BgL_bgl_za762delayza7d2class2087z00,
		BGl_z62delayzd2classzd2accessorsz12z70zzmodule_classz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2genericzd2unitzd2envzd2zzmodule_classz00,
		BgL_bgl_za762getza7d2generic2088z00,
		BGl_z62getzd2genericzd2unitz62zzmodule_classz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2classz12zd2envz12zzmodule_classz00,
		BgL_bgl_za762declareza7d2cla2089z00,
		BGl_z62declarezd2classz12za2zzmodule_classz00, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2methodzd2unitzd2envzd2zzmodule_classz00,
		BgL_bgl_za762getza7d2methodza72090za7,
		BGl_z62getzd2methodzd2unitz62zzmodule_classz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzmodule_classz00));
		     ADD_ROOT((void *) (&BGl_za2methodzd2unitza2zd2zzmodule_classz00));
		     ADD_ROOT((void *) (&BGl_za2genericzd2unitza2zd2zzmodule_classz00));
		     ADD_ROOT((void *) (&BGl_za2declaredzd2classesza2zd2zzmodule_classz00));
		     ADD_ROOT((void *) (&BGl_za2objectzd2unitza2zd2zzmodule_classz00));
		     ADD_ROOT((void *) (&BGl_za2classzd2accessesza2zd2zzmodule_classz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long
		BgL_checksumz00_3268, char *BgL_fromz00_3269)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_classz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_classz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_classz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_classz00();
					BGl_cnstzd2initzd2zzmodule_classz00();
					BGl_importedzd2moduleszd2initz00zzmodule_classz00();
					return BGl_toplevelzd2initzd2zzmodule_classz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_classz00(void)
	{
		{	/* Module/class.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_class");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_class");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "module_class");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"module_class");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_class");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_class");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "module_class");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "module_class");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_class");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "module_class");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_class");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "module_class");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "module_class");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_class");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_classz00(void)
	{
		{	/* Module/class.scm 15 */
			{	/* Module/class.scm 15 */
				obj_t BgL_cportz00_3142;

				{	/* Module/class.scm 15 */
					obj_t BgL_stringz00_3149;

					BgL_stringz00_3149 = BGl_string2077z00zzmodule_classz00;
					{	/* Module/class.scm 15 */
						obj_t BgL_startz00_3150;

						BgL_startz00_3150 = BINT(0L);
						{	/* Module/class.scm 15 */
							obj_t BgL_endz00_3151;

							BgL_endz00_3151 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3149)));
							{	/* Module/class.scm 15 */

								BgL_cportz00_3142 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3149, BgL_startz00_3150, BgL_endz00_3151);
				}}}}
				{
					long BgL_iz00_3143;

					BgL_iz00_3143 = 32L;
				BgL_loopz00_3144:
					if ((BgL_iz00_3143 == -1L))
						{	/* Module/class.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/class.scm 15 */
							{	/* Module/class.scm 15 */
								obj_t BgL_arg2078z00_3145;

								{	/* Module/class.scm 15 */

									{	/* Module/class.scm 15 */
										obj_t BgL_locationz00_3147;

										BgL_locationz00_3147 = BBOOL(((bool_t) 0));
										{	/* Module/class.scm 15 */

											BgL_arg2078z00_3145 =
												BGl_readz00zz__readerz00(BgL_cportz00_3142,
												BgL_locationz00_3147);
										}
									}
								}
								{	/* Module/class.scm 15 */
									int BgL_tmpz00_3301;

									BgL_tmpz00_3301 = (int) (BgL_iz00_3143);
									CNST_TABLE_SET(BgL_tmpz00_3301, BgL_arg2078z00_3145);
							}}
							{	/* Module/class.scm 15 */
								int BgL_auxz00_3148;

								BgL_auxz00_3148 = (int) ((BgL_iz00_3143 - 1L));
								{
									long BgL_iz00_3306;

									BgL_iz00_3306 = (long) (BgL_auxz00_3148);
									BgL_iz00_3143 = BgL_iz00_3306;
									goto BgL_loopz00_3144;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_classz00(void)
	{
		{	/* Module/class.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_classz00(void)
	{
		{	/* Module/class.scm 15 */
			BGl_za2objectzd2unitza2zd2zzmodule_classz00 = BUNSPEC;
			BGl_za2methodzd2unitza2zd2zzmodule_classz00 = BUNSPEC;
			BGl_za2genericzd2unitza2zd2zzmodule_classz00 = BUNSPEC;
			BGl_za2classzd2accessesza2zd2zzmodule_classz00 = BNIL;
			return (BGl_za2declaredzd2classesza2zd2zzmodule_classz00 = BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzmodule_classz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1764;

				BgL_headz00_1764 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1765;
					obj_t BgL_tailz00_1766;

					BgL_prevz00_1765 = BgL_headz00_1764;
					BgL_tailz00_1766 = BgL_l1z00_1;
				BgL_loopz00_1767:
					if (PAIRP(BgL_tailz00_1766))
						{
							obj_t BgL_newzd2prevzd2_1769;

							BgL_newzd2prevzd2_1769 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1766), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1765, BgL_newzd2prevzd2_1769);
							{
								obj_t BgL_tailz00_3316;
								obj_t BgL_prevz00_3315;

								BgL_prevz00_3315 = BgL_newzd2prevzd2_1769;
								BgL_tailz00_3316 = CDR(BgL_tailz00_1766);
								BgL_tailz00_1766 = BgL_tailz00_3316;
								BgL_prevz00_1765 = BgL_prevz00_3315;
								goto BgL_loopz00_1767;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1764);
				}
			}
		}

	}



/* get-class-hash */
	long BGl_getzd2classzd2hashz00zzmodule_classz00(obj_t BgL_defz00_25)
	{
		{	/* ../runtime/Llib/object.sch 19 */
			return BGl_loopze70ze7zzmodule_classz00(BgL_defz00_25, 1705L);
		}

	}



/* loop~0 */
	long BGl_loopze70ze7zzmodule_classz00(obj_t BgL_defz00_1788,
		long BgL_hashz00_1789)
	{
		{	/* ../runtime/Llib/object.sch 24 */
		BGl_loopze70ze7zzmodule_classz00:
			if (NULLP(BgL_defz00_1788))
				{	/* ../runtime/Llib/object.sch 27 */
					return BgL_hashz00_1789;
				}
			else
				{	/* ../runtime/Llib/object.sch 27 */
					if (PAIRP(BgL_defz00_1788))
						{
							long BgL_hashz00_3326;
							obj_t BgL_defz00_3324;

							BgL_defz00_3324 = CDR(BgL_defz00_1788);
							BgL_hashz00_3326 =
								BGl_loopze70ze7zzmodule_classz00(CAR(BgL_defz00_1788),
								(1966L ^ BgL_hashz00_1789));
							BgL_hashz00_1789 = BgL_hashz00_3326;
							BgL_defz00_1788 = BgL_defz00_3324;
							goto BGl_loopze70ze7zzmodule_classz00;
						}
					else
						{	/* ../runtime/Llib/object.sch 29 */
							return
								(
								(BGl_getzd2hashnumberzd2persistentz00zz__hashz00
									(BgL_defz00_1788) & 65535L) ^ BgL_hashz00_1789);
						}
				}
		}

	}



/* get-object-unit */
	BGL_EXPORTED_DEF obj_t BGl_getzd2objectzd2unitz00zzmodule_classz00(void)
	{
		{	/* Module/class.scm 60 */
			return BGl_za2objectzd2unitza2zd2zzmodule_classz00;
		}

	}



/* &get-object-unit */
	obj_t BGl_z62getzd2objectzd2unitz62zzmodule_classz00(obj_t BgL_envz00_3098)
	{
		{	/* Module/class.scm 60 */
			return BGl_getzd2objectzd2unitz00zzmodule_classz00();
		}

	}



/* get-method-unit */
	BGL_EXPORTED_DEF obj_t BGl_getzd2methodzd2unitz00zzmodule_classz00(void)
	{
		{	/* Module/class.scm 66 */
			return BGl_za2methodzd2unitza2zd2zzmodule_classz00;
		}

	}



/* &get-method-unit */
	obj_t BGl_z62getzd2methodzd2unitz62zzmodule_classz00(obj_t BgL_envz00_3099)
	{
		{	/* Module/class.scm 66 */
			return BGl_getzd2methodzd2unitz00zzmodule_classz00();
		}

	}



/* get-generic-unit */
	BGL_EXPORTED_DEF obj_t BGl_getzd2genericzd2unitz00zzmodule_classz00(void)
	{
		{	/* Module/class.scm 72 */
			return BGl_za2genericzd2unitza2zd2zzmodule_classz00;
		}

	}



/* &get-generic-unit */
	obj_t BGl_z62getzd2genericzd2unitz62zzmodule_classz00(obj_t BgL_envz00_3100)
	{
		{	/* Module/class.scm 72 */
			return BGl_getzd2genericzd2unitz00zzmodule_classz00();
		}

	}



/* declare-class! */
	BGL_EXPORTED_DEF obj_t BGl_declarezd2classz12zc0zzmodule_classz00(obj_t
		BgL_classzd2defzd2_26, obj_t BgL_modulez00_27, obj_t BgL_importz00_28,
		bool_t BgL_finalzf3zf3_29, bool_t BgL_abstractzf3zf3_30,
		obj_t BgL_defzd2srczd2_31, obj_t BgL_declzd2srczd2_32)
	{
		{	/* Module/class.scm 78 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_importz00_28,
						CNST_TABLE_REF(0))))
				{	/* Module/class.scm 83 */
					obj_t BgL_arg1319z00_2403;

					if (BgL_finalzf3zf3_29)
						{	/* Module/class.scm 83 */
							BgL_arg1319z00_2403 = CNST_TABLE_REF(1);
						}
					else
						{	/* Module/class.scm 83 */
							BgL_arg1319z00_2403 = CNST_TABLE_REF(2);
						}
					return
						((obj_t)
						BGl_declarezd2exportzd2classz12z12zzmodule_classz00
						(BgL_classzd2defzd2_26, BgL_modulez00_27, BgL_arg1319z00_2403,
							BgL_abstractzf3zf3_30, BgL_defzd2srczd2_31, BgL_declzd2srczd2_32,
							BgL_importz00_28));
				}
			else
				{	/* Module/class.scm 89 */
					obj_t BgL_arg1320z00_2404;

					if (BgL_finalzf3zf3_29)
						{	/* Module/class.scm 89 */
							BgL_arg1320z00_2404 = CNST_TABLE_REF(1);
						}
					else
						{	/* Module/class.scm 89 */
							BgL_arg1320z00_2404 = CNST_TABLE_REF(2);
						}
					return
						((obj_t)
						BGl_declarezd2importzd2classz12z12zzmodule_classz00
						(BgL_classzd2defzd2_26, BgL_modulez00_27, BgL_arg1320z00_2404,
							BgL_abstractzf3zf3_30, BgL_defzd2srczd2_31,
							BgL_declzd2srczd2_32));
				}
		}

	}



/* &declare-class! */
	obj_t BGl_z62declarezd2classz12za2zzmodule_classz00(obj_t BgL_envz00_3101,
		obj_t BgL_classzd2defzd2_3102, obj_t BgL_modulez00_3103,
		obj_t BgL_importz00_3104, obj_t BgL_finalzf3zf3_3105,
		obj_t BgL_abstractzf3zf3_3106, obj_t BgL_defzd2srczd2_3107,
		obj_t BgL_declzd2srczd2_3108)
	{
		{	/* Module/class.scm 78 */
			return
				BGl_declarezd2classz12zc0zzmodule_classz00(BgL_classzd2defzd2_3102,
				BgL_modulez00_3103, BgL_importz00_3104, CBOOL(BgL_finalzf3zf3_3105),
				CBOOL(BgL_abstractzf3zf3_3106), BgL_defzd2srczd2_3107,
				BgL_declzd2srczd2_3108);
		}

	}



/* declare-wide-class! */
	BGL_EXPORTED_DEF obj_t BGl_declarezd2widezd2classz12z12zzmodule_classz00(obj_t
		BgL_classzd2defzd2_33, obj_t BgL_modulez00_34, obj_t BgL_importz00_35,
		obj_t BgL_defzd2srczd2_36, obj_t BgL_declzd2srczd2_37)
	{
		{	/* Module/class.scm 96 */
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_importz00_35,
						CNST_TABLE_REF(0))))
				{	/* Module/class.scm 98 */
					return
						((obj_t)
						BGl_declarezd2exportzd2classz12z12zzmodule_classz00
						(BgL_classzd2defzd2_33, BgL_modulez00_34, CNST_TABLE_REF(3),
							((bool_t) 0), BgL_defzd2srczd2_36, BgL_declzd2srczd2_37,
							BgL_importz00_35));
				}
			else
				{	/* Module/class.scm 98 */
					return
						((obj_t)
						BGl_declarezd2importzd2classz12z12zzmodule_classz00
						(BgL_classzd2defzd2_33, BgL_modulez00_34, CNST_TABLE_REF(3),
							((bool_t) 0), BgL_defzd2srczd2_36, BgL_declzd2srczd2_37));
				}
		}

	}



/* &declare-wide-class! */
	obj_t BGl_z62declarezd2widezd2classz12z70zzmodule_classz00(obj_t
		BgL_envz00_3109, obj_t BgL_classzd2defzd2_3110, obj_t BgL_modulez00_3111,
		obj_t BgL_importz00_3112, obj_t BgL_defzd2srczd2_3113,
		obj_t BgL_declzd2srczd2_3114)
	{
		{	/* Module/class.scm 96 */
			return
				BGl_declarezd2widezd2classz12z12zzmodule_classz00
				(BgL_classzd2defzd2_3110, BgL_modulez00_3111, BgL_importz00_3112,
				BgL_defzd2srczd2_3113, BgL_declzd2srczd2_3114);
		}

	}



/* declare-export-class! */
	BgL_typez00_bglt BGl_declarezd2exportzd2classz12z12zzmodule_classz00(obj_t
		BgL_cdefz00_38, obj_t BgL_modulez00_39, obj_t BgL_kindz00_40,
		bool_t BgL_abstractzf3zf3_41, obj_t BgL_srczd2defzd2_42,
		obj_t BgL_srczd2declzd2_43, obj_t BgL_importz00_44)
	{
		{	/* Module/class.scm 110 */
			{	/* Module/class.scm 114 */
				obj_t BgL_locz00_1807;

				BgL_locz00_1807 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srczd2defzd2_42);
				{	/* Module/class.scm 114 */
					obj_t BgL_classzd2varzd2_1808;

					BgL_classzd2varzd2_1808 = CAR(BgL_cdefz00_38);
					{	/* Module/class.scm 115 */
						obj_t BgL_classzd2idzd2_1809;

						BgL_classzd2idzd2_1809 =
							BGl_idzd2ofzd2idz00zzast_identz00(BgL_classzd2varzd2_1808,
							BgL_locz00_1807);
						{	/* Module/class.scm 116 */
							obj_t BgL_holderz00_1810;

							{	/* Module/class.scm 118 */
								obj_t BgL_arg1327z00_1819;

								{	/* Module/class.scm 118 */
									obj_t BgL_arg1328z00_1820;

									BgL_arg1328z00_1820 =
										MAKE_YOUNG_PAIR(BgL_classzd2idzd2_1809, BNIL);
									BgL_arg1327z00_1819 =
										MAKE_YOUNG_PAIR(BgL_importz00_44, BgL_arg1328z00_1820);
								}
								BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
									(BgL_arg1327z00_1819);
							}
							BgL_holderz00_1810 =
								BGl_findzd2globalzf2modulez20zzast_envz00
								(BgL_classzd2idzd2_1809, BgL_modulez00_39);
							{	/* Module/class.scm 117 */
								bool_t BgL_finalzf3zf3_1811;

								BgL_finalzf3zf3_1811 = (BgL_kindz00_40 == CNST_TABLE_REF(1));
								{	/* Module/class.scm 120 */
									obj_t BgL_widez00_1812;

									if ((BgL_kindz00_40 == CNST_TABLE_REF(3)))
										{	/* Module/class.scm 121 */
											BgL_widez00_1812 = CNST_TABLE_REF(4);
										}
									else
										{	/* Module/class.scm 121 */
											BgL_widez00_1812 = BFALSE;
										}
									{	/* Module/class.scm 121 */
										BgL_typez00_bglt BgL_tclassz00_1813;

										BgL_tclassz00_1813 =
											BGl_declarezd2classzd2typez12z12zzobject_classz00
											(BgL_cdefz00_38,
											((BgL_globalz00_bglt) BgL_holderz00_1810),
											BgL_widez00_1812, BgL_finalzf3zf3_1811,
											BgL_abstractzf3zf3_41, BgL_srczd2defzd2_42);
										{	/* Module/class.scm 122 */

											((((BgL_globalz00_bglt) COBJECT(
															((BgL_globalz00_bglt) BgL_holderz00_1810)))->
													BgL_srcz00) = ((obj_t) BgL_srczd2defzd2_42), BUNSPEC);
											((((BgL_typez00_bglt) COBJECT(BgL_tclassz00_1813))->
													BgL_importzd2locationzd2) =
												((obj_t)
													BGl_findzd2locationzf2locz20zztools_locationz00
													(BgL_srczd2declzd2_43, BgL_locz00_1807)), BUNSPEC);
											{	/* Module/class.scm 136 */
												obj_t BgL_arg1323z00_1815;

												{	/* Module/class.scm 136 */
													obj_t BgL_zc3z04anonymousza31326ze3z87_3115;

													BgL_zc3z04anonymousza31326ze3z87_3115 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31326ze3ze5zzmodule_classz00,
														(int) (0L), (int) (4L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31326ze3z87_3115,
														(int) (0L), BgL_cdefz00_38);
													PROCEDURE_SET(BgL_zc3z04anonymousza31326ze3z87_3115,
														(int) (1L), BgL_holderz00_1810);
													PROCEDURE_SET(BgL_zc3z04anonymousza31326ze3z87_3115,
														(int) (2L), ((obj_t) BgL_tclassz00_1813));
													PROCEDURE_SET(BgL_zc3z04anonymousza31326ze3z87_3115,
														(int) (3L), BgL_srczd2defzd2_42);
													BgL_arg1323z00_1815 =
														BGl_makezd2promisezd2zz__r4_control_features_6_9z00
														(BgL_zc3z04anonymousza31326ze3z87_3115);
												}
												{	/* Module/class.scm 176 */
													obj_t BgL_arg1335z00_2410;

													BgL_arg1335z00_2410 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_tclassz00_1813), BgL_arg1323z00_1815);
													BGl_za2classzd2accessesza2zd2zzmodule_classz00 =
														MAKE_YOUNG_PAIR(BgL_arg1335z00_2410,
														BGl_za2classzd2accessesza2zd2zzmodule_classz00);
												}
												return BgL_tclassz00_1813;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1326> */
	obj_t BGl_z62zc3z04anonymousza31326ze3ze5zzmodule_classz00(obj_t
		BgL_envz00_3116)
	{
		{	/* Module/class.scm 135 */
			{	/* Module/class.scm 136 */
				obj_t BgL_cdefz00_3117;
				obj_t BgL_holderz00_3118;
				BgL_typez00_bglt BgL_tclassz00_3119;
				obj_t BgL_srczd2defzd2_3120;

				BgL_cdefz00_3117 = ((obj_t) PROCEDURE_REF(BgL_envz00_3116, (int) (0L)));
				BgL_holderz00_3118 = PROCEDURE_REF(BgL_envz00_3116, (int) (1L));
				BgL_tclassz00_3119 =
					((BgL_typez00_bglt) PROCEDURE_REF(BgL_envz00_3116, (int) (2L)));
				BgL_srczd2defzd2_3120 = PROCEDURE_REF(BgL_envz00_3116, (int) (3L));
				return
					BGl_genzd2registerzd2classz12z12zzmodule_classz00(BgL_cdefz00_3117,
					BgL_holderz00_3118, ((obj_t) BgL_tclassz00_3119),
					BgL_srczd2defzd2_3120);
			}
		}

	}



/* declare-import-class! */
	BgL_typez00_bglt BGl_declarezd2importzd2classz12z12zzmodule_classz00(obj_t
		BgL_cdefz00_45, obj_t BgL_modulez00_46, obj_t BgL_kindz00_47,
		bool_t BgL_abstractzf3zf3_48, obj_t BgL_srczd2defzd2_49,
		obj_t BgL_srczd2declzd2_50)
	{
		{	/* Module/class.scm 141 */
			{	/* Module/class.scm 145 */
				obj_t BgL_locz00_1821;

				BgL_locz00_1821 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_srczd2defzd2_49);
				{	/* Module/class.scm 145 */
					obj_t BgL_classzd2varzd2_1822;

					BgL_classzd2varzd2_1822 = CAR(BgL_cdefz00_45);
					{	/* Module/class.scm 146 */
						obj_t BgL_classzd2idzd2_1823;

						BgL_classzd2idzd2_1823 =
							BGl_idzd2ofzd2idz00zzast_identz00(BgL_classzd2varzd2_1822,
							BgL_locz00_1821);
						{	/* Module/class.scm 147 */
							obj_t BgL_holderz00_1824;

							BgL_holderz00_1824 =
								BGl_importzd2parserzd2zzmodule_impusez00(BgL_modulez00_46,
								BgL_classzd2idzd2_1823, BFALSE, BNIL);
							{	/* Module/class.scm 148 */
								bool_t BgL_finalzf3zf3_1825;

								BgL_finalzf3zf3_1825 = (BgL_kindz00_47 == CNST_TABLE_REF(1));
								{	/* Module/class.scm 149 */
									obj_t BgL_widez00_1826;

									if ((BgL_kindz00_47 == CNST_TABLE_REF(3)))
										{	/* Module/class.scm 150 */
											BgL_widez00_1826 = CNST_TABLE_REF(4);
										}
									else
										{	/* Module/class.scm 150 */
											BgL_widez00_1826 = BFALSE;
										}
									{	/* Module/class.scm 150 */
										BgL_typez00_bglt BgL_tclassz00_1827;

										BgL_tclassz00_1827 =
											BGl_declarezd2classzd2typez12z12zzobject_classz00
											(BgL_cdefz00_45,
											((BgL_globalz00_bglt) BgL_holderz00_1824),
											BgL_widez00_1826, BgL_finalzf3zf3_1825,
											BgL_abstractzf3zf3_48, BgL_srczd2defzd2_49);
										{	/* Module/class.scm 151 */

											((((BgL_typez00_bglt) COBJECT(BgL_tclassz00_1827))->
													BgL_importzd2locationzd2) =
												((obj_t)
													BGl_findzd2locationzf2locz20zztools_locationz00
													(BgL_srczd2declzd2_50, BgL_locz00_1821)), BUNSPEC);
											{	/* Module/class.scm 166 */
												obj_t BgL_arg1331z00_1829;

												{	/* Module/class.scm 166 */
													obj_t BgL_zc3z04anonymousza31333ze3z87_3121;

													BgL_zc3z04anonymousza31333ze3z87_3121 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31333ze3ze5zzmodule_classz00,
														(int) (0L), (int) (3L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31333ze3z87_3121,
														(int) (0L), ((obj_t) BgL_tclassz00_1827));
													PROCEDURE_SET(BgL_zc3z04anonymousza31333ze3z87_3121,
														(int) (1L), BgL_cdefz00_45);
													PROCEDURE_SET(BgL_zc3z04anonymousza31333ze3z87_3121,
														(int) (2L), BgL_srczd2defzd2_49);
													BgL_arg1331z00_1829 =
														BGl_makezd2promisezd2zz__r4_control_features_6_9z00
														(BgL_zc3z04anonymousza31333ze3z87_3121);
												}
												{	/* Module/class.scm 176 */
													obj_t BgL_arg1335z00_2414;

													BgL_arg1335z00_2414 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_tclassz00_1827), BgL_arg1331z00_1829);
													BGl_za2classzd2accessesza2zd2zzmodule_classz00 =
														MAKE_YOUNG_PAIR(BgL_arg1335z00_2414,
														BGl_za2classzd2accessesza2zd2zzmodule_classz00);
												}
												return BgL_tclassz00_1827;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1333> */
	obj_t BGl_z62zc3z04anonymousza31333ze3ze5zzmodule_classz00(obj_t
		BgL_envz00_3122)
	{
		{	/* Module/class.scm 162 */
			{	/* Module/class.scm 166 */
				BgL_typez00_bglt BgL_tclassz00_3123;
				obj_t BgL_cdefz00_3124;
				obj_t BgL_srczd2defzd2_3125;

				BgL_tclassz00_3123 =
					((BgL_typez00_bglt) PROCEDURE_REF(BgL_envz00_3122, (int) (0L)));
				BgL_cdefz00_3124 = ((obj_t) PROCEDURE_REF(BgL_envz00_3122, (int) (1L)));
				BgL_srczd2defzd2_3125 = PROCEDURE_REF(BgL_envz00_3122, (int) (2L));
				BGl_setzd2classzd2slotsz12z12zzobject_classz00(
					((BgL_typez00_bglt) BgL_tclassz00_3123), BgL_cdefz00_3124,
					BgL_srczd2defzd2_3125);
				BGl_genzd2classzd2coercionsz12z12zzobject_coercionz00(((obj_t)
						BgL_tclassz00_3123));
				return BNIL;
			}
		}

	}



/* delay-class-accessors! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_delayzd2classzd2accessorsz12z12zzmodule_classz00(BgL_typez00_bglt
		BgL_typez00_51, obj_t BgL_delayedz00_52)
	{
		{	/* Module/class.scm 175 */
			{	/* Module/class.scm 176 */
				obj_t BgL_arg1335z00_2415;

				BgL_arg1335z00_2415 =
					MAKE_YOUNG_PAIR(((obj_t) BgL_typez00_51), BgL_delayedz00_52);
				BGl_za2classzd2accessesza2zd2zzmodule_classz00 =
					MAKE_YOUNG_PAIR(BgL_arg1335z00_2415,
					BGl_za2classzd2accessesza2zd2zzmodule_classz00);
			}
			return BgL_typez00_51;
		}

	}



/* &delay-class-accessors! */
	BgL_typez00_bglt BGl_z62delayzd2classzd2accessorsz12z70zzmodule_classz00(obj_t
		BgL_envz00_3126, obj_t BgL_typez00_3127, obj_t BgL_delayedz00_3128)
	{
		{	/* Module/class.scm 175 */
			return
				BGl_delayzd2classzd2accessorsz12z12zzmodule_classz00(
				((BgL_typez00_bglt) BgL_typez00_3127), BgL_delayedz00_3128);
		}

	}



/* gen-register-class! */
	obj_t BGl_genzd2registerzd2classz12z12zzmodule_classz00(obj_t
		BgL_classzd2defzd2_53, obj_t BgL_holderz00_54, obj_t BgL_classz00_55,
		obj_t BgL_srczd2defzd2_56)
	{
		{	/* Module/class.scm 194 */
			if (BGl_checkzd2classzd2declarationzf3zf3zzobject_classz00(
					((BgL_typez00_bglt) BgL_classz00_55), BgL_srczd2defzd2_56))
				{	/* Module/class.scm 195 */
					BGl_setzd2classzd2slotsz12z12zzobject_classz00(
						((BgL_typez00_bglt) BgL_classz00_55), BgL_classzd2defzd2_53,
						BgL_srczd2defzd2_56);
					BGl_genzd2classzd2coercionsz12z12zzobject_coercionz00
						(BgL_classz00_55);
					{	/* Module/class.scm 202 */
						obj_t BgL_classidz00_1836;

						BgL_classidz00_1836 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_classz00_55)))->BgL_idz00);
						{	/* Module/class.scm 202 */
							obj_t BgL_classmodz00_1837;

							{	/* Module/class.scm 203 */
								BgL_globalz00_bglt BgL_arg1514z00_1885;

								{
									BgL_tclassz00_bglt BgL_auxz00_3464;

									{
										obj_t BgL_auxz00_3465;

										{	/* Module/class.scm 203 */
											BgL_objectz00_bglt BgL_tmpz00_3466;

											BgL_tmpz00_3466 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_classz00_55));
											BgL_auxz00_3465 = BGL_OBJECT_WIDENING(BgL_tmpz00_3466);
										}
										BgL_auxz00_3464 = ((BgL_tclassz00_bglt) BgL_auxz00_3465);
									}
									BgL_arg1514z00_1885 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3464))->
										BgL_holderz00);
								}
								BgL_classmodz00_1837 =
									(((BgL_globalz00_bglt) COBJECT(BgL_arg1514z00_1885))->
									BgL_modulez00);
							}
							{	/* Module/class.scm 203 */
								obj_t BgL_superz00_1838;

								{
									BgL_tclassz00_bglt BgL_auxz00_3473;

									{
										obj_t BgL_auxz00_3474;

										{	/* Module/class.scm 204 */
											BgL_objectz00_bglt BgL_tmpz00_3475;

											BgL_tmpz00_3475 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_classz00_55));
											BgL_auxz00_3474 = BGL_OBJECT_WIDENING(BgL_tmpz00_3475);
										}
										BgL_auxz00_3473 = ((BgL_tclassz00_bglt) BgL_auxz00_3474);
									}
									BgL_superz00_1838 =
										(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3473))->
										BgL_itszd2superzd2);
								}
								{	/* Module/class.scm 204 */
									obj_t BgL_supervz00_1839;

									{	/* Module/class.scm 205 */
										bool_t BgL_test2103z00_3481;

										{	/* Module/class.scm 205 */
											obj_t BgL_classz00_2422;

											BgL_classz00_2422 = BGl_tclassz00zzobject_classz00;
											if (BGL_OBJECTP(BgL_superz00_1838))
												{	/* Module/class.scm 205 */
													BgL_objectz00_bglt BgL_arg1807z00_2424;

													BgL_arg1807z00_2424 =
														(BgL_objectz00_bglt) (BgL_superz00_1838);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Module/class.scm 205 */
															long BgL_idxz00_2430;

															BgL_idxz00_2430 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2424);
															BgL_test2103z00_3481 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2430 + 2L)) == BgL_classz00_2422);
														}
													else
														{	/* Module/class.scm 205 */
															bool_t BgL_res2054z00_2455;

															{	/* Module/class.scm 205 */
																obj_t BgL_oclassz00_2438;

																{	/* Module/class.scm 205 */
																	obj_t BgL_arg1815z00_2446;
																	long BgL_arg1816z00_2447;

																	BgL_arg1815z00_2446 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Module/class.scm 205 */
																		long BgL_arg1817z00_2448;

																		BgL_arg1817z00_2448 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2424);
																		BgL_arg1816z00_2447 =
																			(BgL_arg1817z00_2448 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2438 =
																		VECTOR_REF(BgL_arg1815z00_2446,
																		BgL_arg1816z00_2447);
																}
																{	/* Module/class.scm 205 */
																	bool_t BgL__ortest_1115z00_2439;

																	BgL__ortest_1115z00_2439 =
																		(BgL_classz00_2422 == BgL_oclassz00_2438);
																	if (BgL__ortest_1115z00_2439)
																		{	/* Module/class.scm 205 */
																			BgL_res2054z00_2455 =
																				BgL__ortest_1115z00_2439;
																		}
																	else
																		{	/* Module/class.scm 205 */
																			long BgL_odepthz00_2440;

																			{	/* Module/class.scm 205 */
																				obj_t BgL_arg1804z00_2441;

																				BgL_arg1804z00_2441 =
																					(BgL_oclassz00_2438);
																				BgL_odepthz00_2440 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2441);
																			}
																			if ((2L < BgL_odepthz00_2440))
																				{	/* Module/class.scm 205 */
																					obj_t BgL_arg1802z00_2443;

																					{	/* Module/class.scm 205 */
																						obj_t BgL_arg1803z00_2444;

																						BgL_arg1803z00_2444 =
																							(BgL_oclassz00_2438);
																						BgL_arg1802z00_2443 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2444, 2L);
																					}
																					BgL_res2054z00_2455 =
																						(BgL_arg1802z00_2443 ==
																						BgL_classz00_2422);
																				}
																			else
																				{	/* Module/class.scm 205 */
																					BgL_res2054z00_2455 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2103z00_3481 = BgL_res2054z00_2455;
														}
												}
											else
												{	/* Module/class.scm 205 */
													BgL_test2103z00_3481 = ((bool_t) 0);
												}
										}
										if (BgL_test2103z00_3481)
											{	/* Module/class.scm 206 */
												BgL_globalz00_bglt BgL_sholderz00_1880;

												{
													BgL_tclassz00_bglt BgL_auxz00_3504;

													{
														obj_t BgL_auxz00_3505;

														{	/* Module/class.scm 206 */
															BgL_objectz00_bglt BgL_tmpz00_3506;

															BgL_tmpz00_3506 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_superz00_1838));
															BgL_auxz00_3505 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3506);
														}
														BgL_auxz00_3504 =
															((BgL_tclassz00_bglt) BgL_auxz00_3505);
													}
													BgL_sholderz00_1880 =
														(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3504))->
														BgL_holderz00);
												}
												{	/* Module/class.scm 206 */
													obj_t BgL_sholderidz00_1881;

													BgL_sholderidz00_1881 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_sholderz00_1880)))->
														BgL_idz00);
													{	/* Module/class.scm 207 */
														obj_t BgL_sholdermodulez00_1882;

														BgL_sholdermodulez00_1882 =
															(((BgL_globalz00_bglt)
																COBJECT(BgL_sholderz00_1880))->BgL_modulez00);
														{	/* Module/class.scm 208 */

															{	/* Module/class.scm 209 */
																obj_t BgL_arg1509z00_1883;

																{	/* Module/class.scm 209 */
																	obj_t BgL_arg1513z00_1884;

																	BgL_arg1513z00_1884 =
																		MAKE_YOUNG_PAIR(BgL_sholdermodulez00_1882,
																		BNIL);
																	BgL_arg1509z00_1883 =
																		MAKE_YOUNG_PAIR(BgL_sholderidz00_1881,
																		BgL_arg1513z00_1884);
																}
																BgL_supervz00_1839 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																	BgL_arg1509z00_1883);
															}
														}
													}
												}
											}
										else
											{	/* Module/class.scm 205 */
												BgL_supervz00_1839 = BFALSE;
											}
									}
									{	/* Module/class.scm 205 */
										obj_t BgL_declz00_1840;

										{	/* Module/class.scm 210 */
											obj_t BgL_arg1343z00_1846;

											{	/* Module/class.scm 210 */
												obj_t BgL_arg1346z00_1847;
												obj_t BgL_arg1348z00_1848;

												BgL_arg1346z00_1847 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_holderz00_54))))->
													BgL_idz00);
												{	/* Module/class.scm 211 */
													obj_t BgL_arg1349z00_1849;

													{	/* Module/class.scm 211 */
														obj_t BgL_arg1351z00_1850;
														obj_t BgL_arg1352z00_1851;

														{	/* Module/class.scm 211 */
															obj_t BgL_arg1361z00_1852;

															{	/* Module/class.scm 211 */
																obj_t BgL_arg1364z00_1853;

																BgL_arg1364z00_1853 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BNIL);
																BgL_arg1361z00_1852 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																	BgL_arg1364z00_1853);
															}
															BgL_arg1351z00_1850 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																BgL_arg1361z00_1852);
														}
														{	/* Module/class.scm 213 */
															obj_t BgL_arg1367z00_1854;
															obj_t BgL_arg1370z00_1855;

															{	/* Module/class.scm 213 */
																obj_t BgL_arg1371z00_1856;

																BgL_arg1371z00_1856 =
																	MAKE_YOUNG_PAIR(BgL_classidz00_1836, BNIL);
																BgL_arg1367z00_1854 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																	BgL_arg1371z00_1856);
															}
															{	/* Module/class.scm 215 */
																obj_t BgL_arg1375z00_1857;
																obj_t BgL_arg1376z00_1858;

																{	/* Module/class.scm 215 */
																	obj_t BgL_arg1377z00_1859;

																	BgL_arg1377z00_1859 =
																		MAKE_YOUNG_PAIR(BgL_classmodz00_1837, BNIL);
																	BgL_arg1375z00_1857 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																		BgL_arg1377z00_1859);
																}
																{	/* Module/class.scm 219 */
																	obj_t BgL_arg1378z00_1860;

																	{	/* Module/class.scm 219 */
																		long BgL_arg1379z00_1861;
																		obj_t BgL_arg1380z00_1862;

																		BgL_arg1379z00_1861 =
																			BGl_getzd2classzd2hashz00zzmodule_classz00
																			(BgL_srczd2defzd2_56);
																		{	/* Module/class.scm 221 */
																			obj_t BgL_arg1408z00_1863;
																			obj_t BgL_arg1410z00_1864;

																			{	/* Module/class.scm 221 */
																				bool_t BgL_test2108z00_3535;

																				{
																					BgL_tclassz00_bglt BgL_auxz00_3536;

																					{
																						obj_t BgL_auxz00_3537;

																						{	/* Module/class.scm 221 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_3538;
																							BgL_tmpz00_3538 =
																								((BgL_objectz00_bglt) (
																									(BgL_typez00_bglt)
																									BgL_classz00_55));
																							BgL_auxz00_3537 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_3538);
																						}
																						BgL_auxz00_3536 =
																							((BgL_tclassz00_bglt)
																							BgL_auxz00_3537);
																					}
																					BgL_test2108z00_3535 =
																						(((BgL_tclassz00_bglt)
																							COBJECT(BgL_auxz00_3536))->
																						BgL_abstractzf3zf3);
																				}
																				if (BgL_test2108z00_3535)
																					{	/* Module/class.scm 221 */
																						BgL_arg1408z00_1863 = BFALSE;
																					}
																				else
																					{	/* Module/class.scm 221 */
																						BgL_arg1408z00_1863 =
																							BGl_classgenzd2makezd2anonymousz00zzobject_classgenz00
																							(((BgL_typez00_bglt)
																								BgL_classz00_55));
																					}
																			}
																			{	/* Module/class.scm 225 */
																				obj_t BgL_arg1421z00_1866;
																				obj_t BgL_arg1422z00_1867;

																				if (BGl_widezd2classzf3z21zzobject_classz00(BgL_classz00_55))
																					{	/* Module/class.scm 225 */
																						BgL_arg1421z00_1866 =
																							BGl_classgenzd2widenzd2anonymousz00zzobject_classgenz00
																							(((BgL_typez00_bglt)
																								BgL_classz00_55));
																					}
																				else
																					{	/* Module/class.scm 225 */
																						BgL_arg1421z00_1866 =
																							BGl_classgenzd2allocatezd2anonymousz00zzobject_classgenz00
																							(((BgL_typez00_bglt)
																								BgL_classz00_55));
																					}
																				{	/* Module/class.scm 233 */
																					obj_t BgL_arg1434z00_1869;
																					obj_t BgL_arg1437z00_1870;

																					BgL_arg1434z00_1869 =
																						BGl_findzd2classzd2constructorz00zzobject_classz00
																						(((BgL_typez00_bglt)
																							BgL_classz00_55));
																					{	/* Module/class.scm 235 */
																						obj_t BgL_arg1448z00_1871;
																						obj_t BgL_arg1453z00_1872;

																						BgL_arg1448z00_1871 =
																							BGl_classgenzd2nilzd2anonymousz00zzobject_classgenz00
																							(((BgL_typez00_bglt)
																								BgL_classz00_55));
																						{	/* Module/class.scm 237 */
																							obj_t BgL_arg1454z00_1873;
																							obj_t BgL_arg1472z00_1874;

																							if (BGl_widezd2classzf3z21zzobject_classz00(BgL_classz00_55))
																								{	/* Module/class.scm 237 */
																									BgL_arg1454z00_1873 =
																										BGl_classgenzd2shrinkzd2anonymousz00zzobject_classgenz00
																										(((BgL_typez00_bglt)
																											BgL_classz00_55));
																								}
																							else
																								{	/* Module/class.scm 237 */
																									BgL_arg1454z00_1873 = BFALSE;
																								}
																							{	/* Module/class.scm 240 */
																								obj_t BgL_arg1485z00_1876;
																								obj_t BgL_arg1489z00_1877;

																								BgL_arg1485z00_1876 =
																									BGl_makezd2classzd2fieldsz00zzmodule_classz00
																									(BgL_classz00_55);
																								BgL_arg1489z00_1877 =
																									MAKE_YOUNG_PAIR
																									(BGl_makezd2classzd2virtualzd2fieldszd2zzmodule_classz00
																									(BgL_classz00_55), BNIL);
																								BgL_arg1472z00_1874 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1485z00_1876,
																									BgL_arg1489z00_1877);
																							}
																							BgL_arg1453z00_1872 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1454z00_1873,
																								BgL_arg1472z00_1874);
																						}
																						BgL_arg1437z00_1870 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1448z00_1871,
																							BgL_arg1453z00_1872);
																					}
																					BgL_arg1422z00_1867 =
																						MAKE_YOUNG_PAIR(BgL_arg1434z00_1869,
																						BgL_arg1437z00_1870);
																				}
																				BgL_arg1410z00_1864 =
																					MAKE_YOUNG_PAIR(BgL_arg1421z00_1866,
																					BgL_arg1422z00_1867);
																			}
																			BgL_arg1380z00_1862 =
																				MAKE_YOUNG_PAIR(BgL_arg1408z00_1863,
																				BgL_arg1410z00_1864);
																		}
																		BgL_arg1378z00_1860 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg1379z00_1861),
																			BgL_arg1380z00_1862);
																	}
																	BgL_arg1376z00_1858 =
																		MAKE_YOUNG_PAIR(BgL_supervz00_1839,
																		BgL_arg1378z00_1860);
																}
																BgL_arg1370z00_1855 =
																	MAKE_YOUNG_PAIR(BgL_arg1375z00_1857,
																	BgL_arg1376z00_1858);
															}
															BgL_arg1352z00_1851 =
																MAKE_YOUNG_PAIR(BgL_arg1367z00_1854,
																BgL_arg1370z00_1855);
														}
														BgL_arg1349z00_1849 =
															MAKE_YOUNG_PAIR(BgL_arg1351z00_1850,
															BgL_arg1352z00_1851);
													}
													BgL_arg1348z00_1848 =
														MAKE_YOUNG_PAIR(BgL_arg1349z00_1849, BNIL);
												}
												BgL_arg1343z00_1846 =
													MAKE_YOUNG_PAIR(BgL_arg1346z00_1847,
													BgL_arg1348z00_1848);
											}
											BgL_declz00_1840 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1343z00_1846);
										}
										{	/* Module/class.scm 210 */
											obj_t BgL_edeclz00_1841;

											if (EPAIRP(BgL_srczd2defzd2_56))
												{	/* Module/class.scm 244 */
													obj_t BgL_arg1339z00_1843;
													obj_t BgL_arg1340z00_1844;
													obj_t BgL_arg1342z00_1845;

													BgL_arg1339z00_1843 = CAR(BgL_declz00_1840);
													BgL_arg1340z00_1844 = CDR(BgL_declz00_1840);
													BgL_arg1342z00_1845 =
														CER(((obj_t) BgL_srczd2defzd2_56));
													{	/* Module/class.scm 244 */
														obj_t BgL_res2055z00_2466;

														BgL_res2055z00_2466 =
															MAKE_YOUNG_EPAIR(BgL_arg1339z00_1843,
															BgL_arg1340z00_1844, BgL_arg1342z00_1845);
														BgL_edeclz00_1841 = BgL_res2055z00_2466;
													}
												}
											else
												{	/* Module/class.scm 243 */
													BgL_edeclz00_1841 = BgL_declz00_1840;
												}
											{	/* Module/class.scm 243 */

												BGl_za2declaredzd2classesza2zd2zzmodule_classz00 =
													MAKE_YOUNG_PAIR(BgL_edeclz00_1841,
													BGl_za2declaredzd2classesza2zd2zzmodule_classz00);
												return BNIL;
											}
										}
									}
								}
							}
						}
					}
				}
			else
				{	/* Module/class.scm 195 */
					{
						BgL_tclassz00_bglt BgL_auxz00_3587;

						{
							obj_t BgL_auxz00_3588;

							{	/* Module/class.scm 252 */
								BgL_objectz00_bglt BgL_tmpz00_3589;

								BgL_tmpz00_3589 =
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_55));
								BgL_auxz00_3588 = BGL_OBJECT_WIDENING(BgL_tmpz00_3589);
							}
							BgL_auxz00_3587 = ((BgL_tclassz00_bglt) BgL_auxz00_3588);
						}
						((((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3587))->BgL_slotsz00) =
							((obj_t) BNIL), BUNSPEC);
					}
					return BNIL;
				}
		}

	}



/* make-class-fields */
	obj_t BGl_makezd2classzd2fieldsz00zzmodule_classz00(obj_t BgL_classz00_57)
	{
		{	/* Module/class.scm 258 */
			{	/* Module/class.scm 299 */
				obj_t BgL_arg1516z00_1890;

				{	/* Module/class.scm 299 */
					obj_t BgL_arg1535z00_1891;

					{	/* Module/class.scm 299 */
						obj_t BgL_arg1544z00_1893;

						{
							BgL_tclassz00_bglt BgL_auxz00_3595;

							{
								obj_t BgL_auxz00_3596;

								{	/* Module/class.scm 305 */
									BgL_objectz00_bglt BgL_tmpz00_3597;

									BgL_tmpz00_3597 =
										((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_57));
									BgL_auxz00_3596 = BGL_OBJECT_WIDENING(BgL_tmpz00_3597);
								}
								BgL_auxz00_3595 = ((BgL_tclassz00_bglt) BgL_auxz00_3596);
							}
							BgL_arg1544z00_1893 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3595))->BgL_slotsz00);
						}
						{	/* Module/class.scm 300 */
							obj_t BgL_zc3z04anonymousza31546ze3z87_3129;

							BgL_zc3z04anonymousza31546ze3z87_3129 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31546ze3ze5zzmodule_classz00,
								(int) (1L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31546ze3z87_3129, (int) (0L),
								BgL_classz00_57);
							{	/* Module/class.scm 299 */
								obj_t BgL_list1545z00_1894;

								BgL_list1545z00_1894 =
									MAKE_YOUNG_PAIR(BgL_arg1544z00_1893, BNIL);
								BgL_arg1535z00_1891 =
									BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
									(BgL_zc3z04anonymousza31546ze3z87_3129, BgL_list1545z00_1894);
					}}}
					BgL_arg1516z00_1890 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1535z00_1891,
						BNIL);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg1516z00_1890);
			}
		}

	}



/* &<@anonymous:1546> */
	obj_t BGl_z62zc3z04anonymousza31546ze3ze5zzmodule_classz00(obj_t
		BgL_envz00_3130, obj_t BgL_sz00_3132)
	{
		{	/* Module/class.scm 299 */
			{	/* Module/class.scm 300 */
				obj_t BgL_classz00_3131;

				BgL_classz00_3131 = PROCEDURE_REF(BgL_envz00_3130, (int) (0L));
				{
					obj_t BgL_sz00_3218;
					obj_t BgL_sz00_3155;

					if (
						((((BgL_slotz00_bglt) COBJECT(
										((BgL_slotz00_bglt) BgL_sz00_3132)))->
								BgL_classzd2ownerzd2) == BgL_classz00_3131))
						{	/* Module/class.scm 300 */
							if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
									((BgL_slotz00_bglt) BgL_sz00_3132)))
								{	/* Module/class.scm 301 */
									if (
										(((BgL_slotz00_bglt) COBJECT(
													((BgL_slotz00_bglt) BgL_sz00_3132)))->
											BgL_virtualzd2overridezd2))
										{	/* Module/class.scm 302 */
											return BFALSE;
										}
									else
										{	/* Module/class.scm 302 */
											BgL_sz00_3155 = BgL_sz00_3132;
											{	/* Module/class.scm 271 */
												obj_t BgL_arg1584z00_3156;
												obj_t BgL_arg1585z00_3157;

												{	/* Module/class.scm 271 */
													obj_t BgL_arg1589z00_3158;

													{	/* Module/class.scm 271 */
														obj_t BgL_arg1591z00_3159;

														BgL_arg1591z00_3159 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BNIL);
														BgL_arg1589z00_3158 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
															BgL_arg1591z00_3159);
													}
													BgL_arg1584z00_3156 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
														BgL_arg1589z00_3158);
												}
												{	/* Module/class.scm 272 */
													obj_t BgL_arg1593z00_3160;
													obj_t BgL_arg1594z00_3161;

													{	/* Module/class.scm 272 */
														obj_t BgL_arg1595z00_3162;

														{	/* Module/class.scm 272 */
															obj_t BgL_arg1602z00_3163;

															BgL_arg1602z00_3163 =
																(((BgL_slotz00_bglt) COBJECT(
																		((BgL_slotz00_bglt) BgL_sz00_3155)))->
																BgL_idz00);
															BgL_arg1595z00_3162 =
																MAKE_YOUNG_PAIR(BgL_arg1602z00_3163, BNIL);
														}
														BgL_arg1593z00_3160 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
															BgL_arg1595z00_3162);
													}
													{	/* Module/class.scm 273 */
														obj_t BgL_arg1605z00_3164;
														obj_t BgL_arg1606z00_3165;

														{	/* Module/class.scm 273 */
															obj_t BgL_arg1609z00_3166;

															{	/* Module/class.scm 273 */
																obj_t BgL_arg1611z00_3167;
																obj_t BgL_arg1613z00_3168;

																BgL_arg1611z00_3167 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BNIL);
																{	/* Module/class.scm 274 */
																	obj_t BgL_arg1615z00_3169;

																	{	/* Module/class.scm 274 */
																		obj_t BgL_arg1616z00_3170;
																		obj_t BgL_arg1625z00_3171;

																		{	/* Module/class.scm 274 */
																			obj_t BgL_arg1626z00_3172;

																			{	/* Module/class.scm 274 */
																				obj_t BgL_arg1627z00_3173;

																				BgL_arg1627z00_3173 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																					BNIL);
																				BgL_arg1626z00_3172 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(13),
																					BgL_arg1627z00_3173);
																			}
																			BgL_arg1616z00_3170 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																				BgL_arg1626z00_3172);
																		}
																		{	/* Module/class.scm 274 */
																			obj_t BgL_arg1629z00_3174;

																			{	/* Module/class.scm 274 */
																				obj_t BgL_arg1630z00_3175;

																				BgL_arg1630z00_3175 =
																					(((BgL_slotz00_bglt) COBJECT(
																							((BgL_slotz00_bglt)
																								BgL_sz00_3155)))->
																					BgL_virtualzd2numzd2);
																				BgL_arg1629z00_3174 =
																					MAKE_YOUNG_PAIR(BgL_arg1630z00_3175,
																					BNIL);
																			}
																			BgL_arg1625z00_3171 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																				BgL_arg1629z00_3174);
																		}
																		BgL_arg1615z00_3169 =
																			MAKE_YOUNG_PAIR(BgL_arg1616z00_3170,
																			BgL_arg1625z00_3171);
																	}
																	BgL_arg1613z00_3168 =
																		MAKE_YOUNG_PAIR(BgL_arg1615z00_3169, BNIL);
																}
																BgL_arg1609z00_3166 =
																	MAKE_YOUNG_PAIR(BgL_arg1611z00_3167,
																	BgL_arg1613z00_3168);
															}
															BgL_arg1605z00_3164 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																BgL_arg1609z00_3166);
														}
														{	/* Module/class.scm 275 */
															obj_t BgL_arg1642z00_3176;
															obj_t BgL_arg1646z00_3177;

															{	/* Module/class.scm 275 */
																obj_t BgL_arg1650z00_3178;

																{	/* Module/class.scm 275 */
																	obj_t BgL_arg1651z00_3179;
																	obj_t BgL_arg1654z00_3180;

																	{	/* Module/class.scm 275 */
																		obj_t BgL_arg1661z00_3181;

																		BgL_arg1661z00_3181 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(15), BNIL);
																		BgL_arg1651z00_3179 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																			BgL_arg1661z00_3181);
																	}
																	{	/* Module/class.scm 276 */
																		obj_t BgL_arg1663z00_3182;

																		{	/* Module/class.scm 276 */
																			obj_t BgL_arg1675z00_3183;
																			obj_t BgL_arg1678z00_3184;

																			{	/* Module/class.scm 276 */
																				obj_t BgL_arg1681z00_3185;

																				{	/* Module/class.scm 276 */
																					obj_t BgL_arg1688z00_3186;

																					BgL_arg1688z00_3186 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																						BNIL);
																					BgL_arg1681z00_3185 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																						BgL_arg1688z00_3186);
																				}
																				BgL_arg1675z00_3183 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																					BgL_arg1681z00_3185);
																			}
																			{	/* Module/class.scm 276 */
																				obj_t BgL_arg1689z00_3187;

																				{	/* Module/class.scm 276 */
																					obj_t BgL_arg1691z00_3188;
																					obj_t BgL_arg1692z00_3189;

																					BgL_arg1691z00_3188 =
																						(((BgL_slotz00_bglt) COBJECT(
																								((BgL_slotz00_bglt)
																									BgL_sz00_3155)))->
																						BgL_virtualzd2numzd2);
																					BgL_arg1692z00_3189 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																						BNIL);
																					BgL_arg1689z00_3187 =
																						MAKE_YOUNG_PAIR(BgL_arg1691z00_3188,
																						BgL_arg1692z00_3189);
																				}
																				BgL_arg1678z00_3184 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																					BgL_arg1689z00_3187);
																			}
																			BgL_arg1663z00_3182 =
																				MAKE_YOUNG_PAIR(BgL_arg1675z00_3183,
																				BgL_arg1678z00_3184);
																		}
																		BgL_arg1654z00_3180 =
																			MAKE_YOUNG_PAIR(BgL_arg1663z00_3182,
																			BNIL);
																	}
																	BgL_arg1650z00_3178 =
																		MAKE_YOUNG_PAIR(BgL_arg1651z00_3179,
																		BgL_arg1654z00_3180);
																}
																BgL_arg1642z00_3176 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																	BgL_arg1650z00_3178);
															}
															{	/* Module/class.scm 277 */
																bool_t BgL_arg1699z00_3190;
																obj_t BgL_arg1700z00_3191;

																BgL_arg1699z00_3190 =
																	(((BgL_slotz00_bglt) COBJECT(
																			((BgL_slotz00_bglt) BgL_sz00_3155)))->
																	BgL_readzd2onlyzf3z21);
																{	/* Module/class.scm 279 */
																	obj_t BgL_arg1701z00_3192;

																	{	/* Module/class.scm 279 */
																		obj_t BgL_arg1702z00_3193;
																		obj_t BgL_arg1703z00_3194;

																		BgL_arg1702z00_3193 =
																			(((BgL_slotz00_bglt) COBJECT(
																					((BgL_slotz00_bglt) BgL_sz00_3155)))->
																			BgL_userzd2infozd2);
																		{	/* Module/class.scm 280 */
																			obj_t BgL_arg1705z00_3195;
																			obj_t BgL_arg1708z00_3196;

																			BgL_arg1705z00_3195 =
																				BGl_slotzd2defaultzd2exprze70ze7zzmodule_classz00
																				(BgL_sz00_3155);
																			{	/* Module/class.scm 281 */
																				obj_t BgL_arg1709z00_3197;

																				{	/* Module/class.scm 281 */
																					bool_t BgL_test2115z00_3681;

																					{	/* Module/class.scm 281 */
																						obj_t BgL_arg1722z00_3198;

																						BgL_arg1722z00_3198 =
																							(((BgL_slotz00_bglt) COBJECT(
																									((BgL_slotz00_bglt)
																										BgL_sz00_3155)))->
																							BgL_typez00);
																						{	/* Module/class.scm 281 */
																							obj_t BgL_classz00_3199;

																							BgL_classz00_3199 =
																								BGl_tclassz00zzobject_classz00;
																							if (BGL_OBJECTP
																								(BgL_arg1722z00_3198))
																								{	/* Module/class.scm 281 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_3200;
																									BgL_arg1807z00_3200 =
																										(BgL_objectz00_bglt)
																										(BgL_arg1722z00_3198);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Module/class.scm 281 */
																											long BgL_idxz00_3201;

																											BgL_idxz00_3201 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_3200);
																											BgL_test2115z00_3681 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_3201 +
																														2L)) ==
																												BgL_classz00_3199);
																										}
																									else
																										{	/* Module/class.scm 281 */
																											bool_t
																												BgL_res2056z00_3204;
																											{	/* Module/class.scm 281 */
																												obj_t
																													BgL_oclassz00_3205;
																												{	/* Module/class.scm 281 */
																													obj_t
																														BgL_arg1815z00_3206;
																													long
																														BgL_arg1816z00_3207;
																													BgL_arg1815z00_3206 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Module/class.scm 281 */
																														long
																															BgL_arg1817z00_3208;
																														BgL_arg1817z00_3208
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_3200);
																														BgL_arg1816z00_3207
																															=
																															(BgL_arg1817z00_3208
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_3205 =
																														VECTOR_REF
																														(BgL_arg1815z00_3206,
																														BgL_arg1816z00_3207);
																												}
																												{	/* Module/class.scm 281 */
																													bool_t
																														BgL__ortest_1115z00_3209;
																													BgL__ortest_1115z00_3209
																														=
																														(BgL_classz00_3199
																														==
																														BgL_oclassz00_3205);
																													if (BgL__ortest_1115z00_3209)
																														{	/* Module/class.scm 281 */
																															BgL_res2056z00_3204
																																=
																																BgL__ortest_1115z00_3209;
																														}
																													else
																														{	/* Module/class.scm 281 */
																															long
																																BgL_odepthz00_3210;
																															{	/* Module/class.scm 281 */
																																obj_t
																																	BgL_arg1804z00_3211;
																																BgL_arg1804z00_3211
																																	=
																																	(BgL_oclassz00_3205);
																																BgL_odepthz00_3210
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_3211);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_3210))
																																{	/* Module/class.scm 281 */
																																	obj_t
																																		BgL_arg1802z00_3212;
																																	{	/* Module/class.scm 281 */
																																		obj_t
																																			BgL_arg1803z00_3213;
																																		BgL_arg1803z00_3213
																																			=
																																			(BgL_oclassz00_3205);
																																		BgL_arg1802z00_3212
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_3213,
																																			2L);
																																	}
																																	BgL_res2056z00_3204
																																		=
																																		(BgL_arg1802z00_3212
																																		==
																																		BgL_classz00_3199);
																																}
																															else
																																{	/* Module/class.scm 281 */
																																	BgL_res2056z00_3204
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2115z00_3681 =
																												BgL_res2056z00_3204;
																										}
																								}
																							else
																								{	/* Module/class.scm 281 */
																									BgL_test2115z00_3681 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test2115z00_3681)
																						{	/* Module/class.scm 282 */
																							BgL_typez00_bglt BgL_oz00_3214;

																							BgL_oz00_3214 =
																								((BgL_typez00_bglt)
																								(((BgL_slotz00_bglt) COBJECT(
																											((BgL_slotz00_bglt)
																												BgL_sz00_3155)))->
																									BgL_typez00));
																							{
																								BgL_globalz00_bglt
																									BgL_auxz00_3709;
																								{
																									BgL_tclassz00_bglt
																										BgL_auxz00_3710;
																									{
																										obj_t BgL_auxz00_3711;

																										{	/* Module/class.scm 282 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_3712;
																											BgL_tmpz00_3712 =
																												((BgL_objectz00_bglt)
																												BgL_oz00_3214);
																											BgL_auxz00_3711 =
																												BGL_OBJECT_WIDENING
																												(BgL_tmpz00_3712);
																										}
																										BgL_auxz00_3710 =
																											((BgL_tclassz00_bglt)
																											BgL_auxz00_3711);
																									}
																									BgL_auxz00_3709 =
																										(((BgL_tclassz00_bglt)
																											COBJECT
																											(BgL_auxz00_3710))->
																										BgL_holderz00);
																								}
																								BgL_arg1709z00_3197 =
																									((obj_t) BgL_auxz00_3709);
																							}
																						}
																					else
																						{	/* Module/class.scm 283 */
																							obj_t BgL_arg1717z00_3215;

																							{	/* Module/class.scm 283 */
																								obj_t BgL_arg1718z00_3216;

																								{	/* Module/class.scm 283 */
																									obj_t BgL_arg1720z00_3217;

																									BgL_arg1720z00_3217 =
																										(((BgL_slotz00_bglt)
																											COBJECT((
																													(BgL_slotz00_bglt)
																													BgL_sz00_3155)))->
																										BgL_typez00);
																									BgL_arg1718z00_3216 =
																										BGl_typezd2defaultzd2idze70ze7zzmodule_classz00
																										(BgL_arg1720z00_3217);
																								}
																								BgL_arg1717z00_3215 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1718z00_3216, BNIL);
																							}
																							BgL_arg1709z00_3197 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(8), BgL_arg1717z00_3215);
																						}
																				}
																				BgL_arg1708z00_3196 =
																					MAKE_YOUNG_PAIR(BgL_arg1709z00_3197,
																					BNIL);
																			}
																			BgL_arg1703z00_3194 =
																				MAKE_YOUNG_PAIR(BgL_arg1705z00_3195,
																				BgL_arg1708z00_3196);
																		}
																		BgL_arg1701z00_3192 =
																			MAKE_YOUNG_PAIR(BgL_arg1702z00_3193,
																			BgL_arg1703z00_3194);
																	}
																	BgL_arg1700z00_3191 =
																		MAKE_YOUNG_PAIR(BTRUE, BgL_arg1701z00_3192);
																}
																BgL_arg1646z00_3177 =
																	MAKE_YOUNG_PAIR(BBOOL(BgL_arg1699z00_3190),
																	BgL_arg1700z00_3191);
															}
															BgL_arg1606z00_3165 =
																MAKE_YOUNG_PAIR(BgL_arg1642z00_3176,
																BgL_arg1646z00_3177);
														}
														BgL_arg1594z00_3161 =
															MAKE_YOUNG_PAIR(BgL_arg1605z00_3164,
															BgL_arg1606z00_3165);
													}
													BgL_arg1585z00_3157 =
														MAKE_YOUNG_PAIR(BgL_arg1593z00_3160,
														BgL_arg1594z00_3161);
												}
												return
													MAKE_YOUNG_PAIR(BgL_arg1584z00_3156,
													BgL_arg1585z00_3157);
											}
										}
								}
							else
								{	/* Module/class.scm 301 */
									BgL_sz00_3218 = BgL_sz00_3132;
									{	/* Module/class.scm 286 */
										obj_t BgL_defsz00_3219;

										BgL_defsz00_3219 =
											BGl_classgenzd2slotzd2anonymousz00zzobject_classgenz00(
											((BgL_typez00_bglt) BgL_classz00_3131),
											((BgL_slotz00_bglt) BgL_sz00_3218));
										{	/* Module/class.scm 288 */
											obj_t BgL_arg1724z00_3220;
											obj_t BgL_arg1733z00_3221;

											{	/* Module/class.scm 288 */
												obj_t BgL_arg1734z00_3222;

												{	/* Module/class.scm 288 */
													obj_t BgL_arg1735z00_3223;

													BgL_arg1735z00_3223 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(6), BNIL);
													BgL_arg1734z00_3222 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
														BgL_arg1735z00_3223);
												}
												BgL_arg1724z00_3220 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
													BgL_arg1734z00_3222);
											}
											{	/* Module/class.scm 289 */
												obj_t BgL_arg1736z00_3224;
												obj_t BgL_arg1737z00_3225;

												{	/* Module/class.scm 289 */
													obj_t BgL_arg1738z00_3226;

													{	/* Module/class.scm 289 */
														obj_t BgL_arg1739z00_3227;

														BgL_arg1739z00_3227 =
															(((BgL_slotz00_bglt) COBJECT(
																	((BgL_slotz00_bglt) BgL_sz00_3218)))->
															BgL_idz00);
														BgL_arg1738z00_3226 =
															MAKE_YOUNG_PAIR(BgL_arg1739z00_3227, BNIL);
													}
													BgL_arg1736z00_3224 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
														BgL_arg1738z00_3226);
												}
												{	/* Module/class.scm 290 */
													obj_t BgL_arg1740z00_3228;
													obj_t BgL_arg1746z00_3229;

													BgL_arg1740z00_3228 = CAR(((obj_t) BgL_defsz00_3219));
													{	/* Module/class.scm 290 */
														obj_t BgL_arg1747z00_3230;
														obj_t BgL_arg1748z00_3231;

														{	/* Module/class.scm 290 */
															obj_t BgL_pairz00_3232;

															BgL_pairz00_3232 =
																CDR(((obj_t) BgL_defsz00_3219));
															BgL_arg1747z00_3230 = CAR(BgL_pairz00_3232);
														}
														{	/* Module/class.scm 290 */
															bool_t BgL_arg1749z00_3233;
															obj_t BgL_arg1750z00_3234;

															BgL_arg1749z00_3233 =
																(((BgL_slotz00_bglt) COBJECT(
																		((BgL_slotz00_bglt) BgL_sz00_3218)))->
																BgL_readzd2onlyzf3z21);
															{	/* Module/class.scm 292 */
																obj_t BgL_arg1751z00_3235;

																{	/* Module/class.scm 292 */
																	obj_t BgL_arg1752z00_3236;
																	obj_t BgL_arg1753z00_3237;

																	BgL_arg1752z00_3236 =
																		(((BgL_slotz00_bglt) COBJECT(
																				((BgL_slotz00_bglt) BgL_sz00_3218)))->
																		BgL_userzd2infozd2);
																	{	/* Module/class.scm 293 */
																		obj_t BgL_arg1754z00_3238;
																		obj_t BgL_arg1755z00_3239;

																		BgL_arg1754z00_3238 =
																			BGl_slotzd2defaultzd2exprze70ze7zzmodule_classz00
																			(BgL_sz00_3218);
																		{	/* Module/class.scm 294 */
																			obj_t BgL_arg1761z00_3240;

																			{	/* Module/class.scm 294 */
																				bool_t BgL_test2120z00_3758;

																				{	/* Module/class.scm 294 */
																					obj_t BgL_arg1773z00_3241;

																					BgL_arg1773z00_3241 =
																						(((BgL_slotz00_bglt) COBJECT(
																								((BgL_slotz00_bglt)
																									BgL_sz00_3218)))->
																						BgL_typez00);
																					{	/* Module/class.scm 294 */
																						obj_t BgL_classz00_3242;

																						BgL_classz00_3242 =
																							BGl_tclassz00zzobject_classz00;
																						if (BGL_OBJECTP
																							(BgL_arg1773z00_3241))
																							{	/* Module/class.scm 294 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_3243;
																								BgL_arg1807z00_3243 =
																									(BgL_objectz00_bglt)
																									(BgL_arg1773z00_3241);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* Module/class.scm 294 */
																										long BgL_idxz00_3244;

																										BgL_idxz00_3244 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_3243);
																										BgL_test2120z00_3758 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_3244 +
																													2L)) ==
																											BgL_classz00_3242);
																									}
																								else
																									{	/* Module/class.scm 294 */
																										bool_t BgL_res2057z00_3247;

																										{	/* Module/class.scm 294 */
																											obj_t BgL_oclassz00_3248;

																											{	/* Module/class.scm 294 */
																												obj_t
																													BgL_arg1815z00_3249;
																												long
																													BgL_arg1816z00_3250;
																												BgL_arg1815z00_3249 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* Module/class.scm 294 */
																													long
																														BgL_arg1817z00_3251;
																													BgL_arg1817z00_3251 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_3243);
																													BgL_arg1816z00_3250 =
																														(BgL_arg1817z00_3251
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_3248 =
																													VECTOR_REF
																													(BgL_arg1815z00_3249,
																													BgL_arg1816z00_3250);
																											}
																											{	/* Module/class.scm 294 */
																												bool_t
																													BgL__ortest_1115z00_3252;
																												BgL__ortest_1115z00_3252
																													=
																													(BgL_classz00_3242 ==
																													BgL_oclassz00_3248);
																												if (BgL__ortest_1115z00_3252)
																													{	/* Module/class.scm 294 */
																														BgL_res2057z00_3247
																															=
																															BgL__ortest_1115z00_3252;
																													}
																												else
																													{	/* Module/class.scm 294 */
																														long
																															BgL_odepthz00_3253;
																														{	/* Module/class.scm 294 */
																															obj_t
																																BgL_arg1804z00_3254;
																															BgL_arg1804z00_3254
																																=
																																(BgL_oclassz00_3248);
																															BgL_odepthz00_3253
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_3254);
																														}
																														if (
																															(2L <
																																BgL_odepthz00_3253))
																															{	/* Module/class.scm 294 */
																																obj_t
																																	BgL_arg1802z00_3255;
																																{	/* Module/class.scm 294 */
																																	obj_t
																																		BgL_arg1803z00_3256;
																																	BgL_arg1803z00_3256
																																		=
																																		(BgL_oclassz00_3248);
																																	BgL_arg1802z00_3255
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_3256,
																																		2L);
																																}
																																BgL_res2057z00_3247
																																	=
																																	(BgL_arg1802z00_3255
																																	==
																																	BgL_classz00_3242);
																															}
																														else
																															{	/* Module/class.scm 294 */
																																BgL_res2057z00_3247
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test2120z00_3758 =
																											BgL_res2057z00_3247;
																									}
																							}
																						else
																							{	/* Module/class.scm 294 */
																								BgL_test2120z00_3758 =
																									((bool_t) 0);
																							}
																					}
																				}
																				if (BgL_test2120z00_3758)
																					{	/* Module/class.scm 295 */
																						BgL_typez00_bglt BgL_oz00_3257;

																						BgL_oz00_3257 =
																							((BgL_typez00_bglt)
																							(((BgL_slotz00_bglt) COBJECT(
																										((BgL_slotz00_bglt)
																											BgL_sz00_3218)))->
																								BgL_typez00));
																						{
																							BgL_globalz00_bglt
																								BgL_auxz00_3786;
																							{
																								BgL_tclassz00_bglt
																									BgL_auxz00_3787;
																								{
																									obj_t BgL_auxz00_3788;

																									{	/* Module/class.scm 295 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_3789;
																										BgL_tmpz00_3789 =
																											((BgL_objectz00_bglt)
																											BgL_oz00_3257);
																										BgL_auxz00_3788 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_3789);
																									}
																									BgL_auxz00_3787 =
																										((BgL_tclassz00_bglt)
																										BgL_auxz00_3788);
																								}
																								BgL_auxz00_3786 =
																									(((BgL_tclassz00_bglt)
																										COBJECT(BgL_auxz00_3787))->
																									BgL_holderz00);
																							}
																							BgL_arg1761z00_3240 =
																								((obj_t) BgL_auxz00_3786);
																						}
																					}
																				else
																					{	/* Module/class.scm 296 */
																						obj_t BgL_arg1767z00_3258;

																						{	/* Module/class.scm 296 */
																							obj_t BgL_arg1770z00_3259;

																							{	/* Module/class.scm 296 */
																								obj_t BgL_arg1771z00_3260;

																								BgL_arg1771z00_3260 =
																									(((BgL_slotz00_bglt) COBJECT(
																											((BgL_slotz00_bglt)
																												BgL_sz00_3218)))->
																									BgL_typez00);
																								BgL_arg1770z00_3259 =
																									BGl_typezd2defaultzd2idze70ze7zzmodule_classz00
																									(BgL_arg1771z00_3260);
																							}
																							BgL_arg1767z00_3258 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1770z00_3259, BNIL);
																						}
																						BgL_arg1761z00_3240 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																							BgL_arg1767z00_3258);
																					}
																			}
																			BgL_arg1755z00_3239 =
																				MAKE_YOUNG_PAIR(BgL_arg1761z00_3240,
																				BNIL);
																		}
																		BgL_arg1753z00_3237 =
																			MAKE_YOUNG_PAIR(BgL_arg1754z00_3238,
																			BgL_arg1755z00_3239);
																	}
																	BgL_arg1751z00_3235 =
																		MAKE_YOUNG_PAIR(BgL_arg1752z00_3236,
																		BgL_arg1753z00_3237);
																}
																BgL_arg1750z00_3234 =
																	MAKE_YOUNG_PAIR(BFALSE, BgL_arg1751z00_3235);
															}
															BgL_arg1748z00_3231 =
																MAKE_YOUNG_PAIR(BBOOL(BgL_arg1749z00_3233),
																BgL_arg1750z00_3234);
														}
														BgL_arg1746z00_3229 =
															MAKE_YOUNG_PAIR(BgL_arg1747z00_3230,
															BgL_arg1748z00_3231);
													}
													BgL_arg1737z00_3225 =
														MAKE_YOUNG_PAIR(BgL_arg1740z00_3228,
														BgL_arg1746z00_3229);
												}
												BgL_arg1733z00_3221 =
													MAKE_YOUNG_PAIR(BgL_arg1736z00_3224,
													BgL_arg1737z00_3225);
											}
											return
												MAKE_YOUNG_PAIR(BgL_arg1724z00_3220,
												BgL_arg1733z00_3221);
										}
									}
								}
						}
					else
						{	/* Module/class.scm 300 */
							return BFALSE;
						}
				}
			}
		}

	}



/* slot-default-expr~0 */
	obj_t BGl_slotzd2defaultzd2exprze70ze7zzmodule_classz00(obj_t BgL_sz00_1908)
	{
		{	/* Module/class.scm 268 */
			if (BGl_slotzd2defaultzf3z21zzobject_slotsz00(
					((BgL_slotz00_bglt) BgL_sz00_1908)))
				{	/* Module/class.scm 267 */
					obj_t BgL_arg1571z00_1911;

					{	/* Module/class.scm 267 */
						obj_t BgL_arg1573z00_1912;

						{	/* Module/class.scm 267 */
							obj_t BgL_arg1575z00_1913;

							BgL_arg1575z00_1913 =
								(((BgL_slotz00_bglt) COBJECT(
										((BgL_slotz00_bglt) BgL_sz00_1908)))->
								BgL_defaultzd2valuezd2);
							BgL_arg1573z00_1912 = MAKE_YOUNG_PAIR(BgL_arg1575z00_1913, BNIL);
						}
						BgL_arg1571z00_1911 = MAKE_YOUNG_PAIR(BNIL, BgL_arg1573z00_1912);
					}
					return MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BgL_arg1571z00_1911);
				}
			else
				{	/* Module/class.scm 266 */
					return BFALSE;
				}
		}

	}



/* type-default-id~0 */
	obj_t BGl_typezd2defaultzd2idze70ze7zzmodule_classz00(obj_t BgL_typez00_1903)
	{
		{	/* Module/class.scm 263 */
			if (
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_typez00_1903)))->BgL_idz00) ==
					CNST_TABLE_REF(17)))
				{	/* Module/class.scm 261 */
					return CNST_TABLE_REF(18);
				}
			else
				{	/* Module/class.scm 261 */
					return
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_typez00_1903)))->BgL_idz00);
				}
		}

	}



/* make-class-virtual-fields */
	obj_t BGl_makezd2classzd2virtualzd2fieldszd2zzmodule_classz00(obj_t
		BgL_classz00_58)
	{
		{	/* Module/class.scm 310 */
			{	/* Module/class.scm 339 */
				obj_t BgL_arg1775z00_2002;

				{	/* Module/class.scm 339 */
					obj_t BgL_arg1798z00_2003;

					{	/* Module/class.scm 339 */
						obj_t BgL_arg1805z00_2005;

						{
							BgL_tclassz00_bglt BgL_auxz00_3828;

							{
								obj_t BgL_auxz00_3829;

								{	/* Module/class.scm 344 */
									BgL_objectz00_bglt BgL_tmpz00_3830;

									BgL_tmpz00_3830 =
										((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_classz00_58));
									BgL_auxz00_3829 = BGL_OBJECT_WIDENING(BgL_tmpz00_3830);
								}
								BgL_auxz00_3828 = ((BgL_tclassz00_bglt) BgL_auxz00_3829);
							}
							BgL_arg1805z00_2005 =
								(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3828))->BgL_slotsz00);
						}
						{	/* Module/class.scm 340 */
							obj_t BgL_zc3z04anonymousza31807ze3z87_3133;

							BgL_zc3z04anonymousza31807ze3z87_3133 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31807ze3ze5zzmodule_classz00,
								(int) (1L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31807ze3z87_3133, (int) (0L),
								BgL_classz00_58);
							{	/* Module/class.scm 339 */
								obj_t BgL_list1806z00_2006;

								BgL_list1806z00_2006 =
									MAKE_YOUNG_PAIR(BgL_arg1805z00_2005, BNIL);
								BgL_arg1798z00_2003 =
									BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
									(BgL_zc3z04anonymousza31807ze3z87_3133, BgL_list1806z00_2006);
					}}}
					BgL_arg1775z00_2002 =
						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_arg1798z00_2003,
						BNIL);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg1775z00_2002);
			}
		}

	}



/* &<@anonymous:1807> */
	obj_t BGl_z62zc3z04anonymousza31807ze3ze5zzmodule_classz00(obj_t
		BgL_envz00_3134, obj_t BgL_sz00_3136)
	{
		{	/* Module/class.scm 339 */
			{	/* Module/class.scm 340 */
				obj_t BgL_classz00_3135;

				BgL_classz00_3135 = PROCEDURE_REF(BgL_envz00_3134, (int) (0L));
				{	/* Module/class.scm 340 */
					bool_t BgL_test2127z00_3848;

					if (BGl_slotzd2virtualzf3z21zzobject_slotsz00(
							((BgL_slotz00_bglt) BgL_sz00_3136)))
						{	/* Module/class.scm 340 */
							BgL_test2127z00_3848 =
								(
								(((BgL_slotz00_bglt) COBJECT(
											((BgL_slotz00_bglt) BgL_sz00_3136)))->
									BgL_classzd2ownerzd2) == BgL_classz00_3135);
						}
					else
						{	/* Module/class.scm 340 */
							BgL_test2127z00_3848 = ((bool_t) 0);
						}
					if (BgL_test2127z00_3848)
						{	/* Module/class.scm 342 */
							obj_t BgL_arg1820z00_3261;

							{	/* Module/class.scm 342 */
								obj_t BgL_arg1822z00_3262;
								obj_t BgL_arg1823z00_3263;

								BgL_arg1822z00_3262 =
									(((BgL_slotz00_bglt) COBJECT(
											((BgL_slotz00_bglt) BgL_sz00_3136)))->
									BgL_virtualzd2numzd2);
								{	/* Module/class.scm 343 */
									obj_t BgL_arg1831z00_3264;

									{	/* Module/class.scm 343 */
										obj_t BgL_arg1832z00_3265;

										{	/* Module/class.scm 343 */
											obj_t BgL_arg1833z00_3266;
											obj_t BgL_arg1834z00_3267;

											BgL_arg1833z00_3266 =
												BGl_getterze70ze7zzmodule_classz00(BgL_classz00_3135,
												BgL_sz00_3136);
											BgL_arg1834z00_3267 =
												MAKE_YOUNG_PAIR(BGl_setterze70ze7zzmodule_classz00
												(BgL_classz00_3135, BgL_sz00_3136), BNIL);
											BgL_arg1832z00_3265 =
												MAKE_YOUNG_PAIR(BgL_arg1833z00_3266,
												BgL_arg1834z00_3267);
										}
										BgL_arg1831z00_3264 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg1832z00_3265);
									}
									BgL_arg1823z00_3263 =
										MAKE_YOUNG_PAIR(BgL_arg1831z00_3264, BNIL);
								}
								BgL_arg1820z00_3261 =
									MAKE_YOUNG_PAIR(BgL_arg1822z00_3262, BgL_arg1823z00_3263);
							}
							return MAKE_YOUNG_PAIR(CNST_TABLE_REF(19), BgL_arg1820z00_3261);
						}
					else
						{	/* Module/class.scm 340 */
							return BFALSE;
						}
				}
			}
		}

	}



/* epairify-slot~0 */
	obj_t BGl_epairifyzd2slotze70z35zzmodule_classz00(obj_t BgL_defz00_2023,
		obj_t BgL_slotz00_2024)
	{
		{	/* Module/class.scm 316 */
			{	/* Module/class.scm 314 */
				bool_t BgL_test2129z00_3867;

				{	/* Module/class.scm 314 */
					obj_t BgL_arg1842z00_2030;

					BgL_arg1842z00_2030 =
						(((BgL_slotz00_bglt) COBJECT(
								((BgL_slotz00_bglt) BgL_slotz00_2024)))->BgL_srcz00);
					BgL_test2129z00_3867 = EPAIRP(BgL_arg1842z00_2030);
				}
				if (BgL_test2129z00_3867)
					{	/* Module/class.scm 315 */
						obj_t BgL_arg1840z00_2029;

						BgL_arg1840z00_2029 =
							(((BgL_slotz00_bglt) COBJECT(
									((BgL_slotz00_bglt) BgL_slotz00_2024)))->BgL_srcz00);
						return
							BGl_epairifyzd2reczd2zztools_miscz00(BgL_defz00_2023,
							BgL_arg1840z00_2029);
					}
				else
					{	/* Module/class.scm 314 */
						return BgL_defz00_2023;
					}
			}
		}

	}



/* getter~0 */
	obj_t BGl_getterze70ze7zzmodule_classz00(obj_t BgL_classz00_3139,
		obj_t BgL_slotz00_2031)
	{
		{	/* Module/class.scm 325 */
			{	/* Module/class.scm 320 */
				obj_t BgL_arg1844z00_2033;

				{	/* Module/class.scm 320 */
					obj_t BgL_arg1845z00_2034;

					{	/* Module/class.scm 320 */
						obj_t BgL_arg1846z00_2035;
						obj_t BgL_arg1847z00_2036;

						BgL_arg1846z00_2035 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BNIL);
						{	/* Module/class.scm 321 */
							obj_t BgL_arg1848z00_2037;
							obj_t BgL_arg1849z00_2038;

							{	/* Module/class.scm 321 */
								obj_t BgL_arg1850z00_2039;

								{	/* Module/class.scm 321 */
									obj_t BgL_arg1851z00_2040;
									obj_t BgL_arg1852z00_2041;

									BgL_arg1851z00_2040 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BNIL);
									{	/* Module/class.scm 322 */
										obj_t BgL_arg1853z00_2042;

										{	/* Module/class.scm 322 */
											obj_t BgL_arg1854z00_2043;

											{	/* Module/class.scm 322 */
												obj_t BgL_arg1856z00_2044;
												obj_t BgL_arg1857z00_2045;

												BgL_arg1856z00_2044 =
													(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt)
																((BgL_typez00_bglt) BgL_classz00_3139))))->
													BgL_idz00);
												{	/* Module/class.scm 323 */
													obj_t BgL_arg1858z00_2046;

													{	/* Module/class.scm 323 */
														obj_t BgL_arg1859z00_2047;

														BgL_arg1859z00_2047 =
															(((BgL_slotz00_bglt) COBJECT(
																	((BgL_slotz00_bglt) BgL_slotz00_2031)))->
															BgL_virtualzd2numzd2);
														BgL_arg1858z00_2046 =
															MAKE_YOUNG_PAIR(BgL_arg1859z00_2047, BNIL);
													}
													BgL_arg1857z00_2045 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
														BgL_arg1858z00_2046);
												}
												BgL_arg1854z00_2043 =
													MAKE_YOUNG_PAIR(BgL_arg1856z00_2044,
													BgL_arg1857z00_2045);
											}
											BgL_arg1853z00_2042 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
												BgL_arg1854z00_2043);
										}
										BgL_arg1852z00_2041 =
											MAKE_YOUNG_PAIR(BgL_arg1853z00_2042, BNIL);
									}
									BgL_arg1850z00_2039 =
										MAKE_YOUNG_PAIR(BgL_arg1851z00_2040, BgL_arg1852z00_2041);
								}
								BgL_arg1848z00_2037 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1850z00_2039);
							}
							{	/* Module/class.scm 324 */
								obj_t BgL_arg1860z00_2048;

								{	/* Module/class.scm 324 */
									obj_t BgL_arg1862z00_2049;
									obj_t BgL_arg1863z00_2050;

									BgL_arg1862z00_2049 =
										(((BgL_slotz00_bglt) COBJECT(
												((BgL_slotz00_bglt) BgL_slotz00_2031)))->BgL_getterz00);
									BgL_arg1863z00_2050 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BNIL);
									BgL_arg1860z00_2048 =
										MAKE_YOUNG_PAIR(BgL_arg1862z00_2049, BgL_arg1863z00_2050);
								}
								BgL_arg1849z00_2038 =
									MAKE_YOUNG_PAIR(BgL_arg1860z00_2048, BNIL);
							}
							BgL_arg1847z00_2036 =
								MAKE_YOUNG_PAIR(BgL_arg1848z00_2037, BgL_arg1849z00_2038);
						}
						BgL_arg1845z00_2034 =
							MAKE_YOUNG_PAIR(BgL_arg1846z00_2035, BgL_arg1847z00_2036);
					}
					BgL_arg1844z00_2033 =
						MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BgL_arg1845z00_2034);
				}
				return
					BGl_epairifyzd2slotze70z35zzmodule_classz00(BgL_arg1844z00_2033,
					BgL_slotz00_2031);
			}
		}

	}



/* setter~0 */
	obj_t BGl_setterze70ze7zzmodule_classz00(obj_t BgL_classz00_3140,
		obj_t BgL_slotz00_2051)
	{
		{	/* Module/class.scm 336 */
			if (
				(((BgL_slotz00_bglt) COBJECT(
							((BgL_slotz00_bglt) BgL_slotz00_2051)))->BgL_readzd2onlyzf3z21))
				{	/* Module/class.scm 328 */
					return BFALSE;
				}
			else
				{	/* Module/class.scm 331 */
					obj_t BgL_arg1866z00_2054;

					{	/* Module/class.scm 331 */
						obj_t BgL_arg1868z00_2055;

						{	/* Module/class.scm 331 */
							obj_t BgL_arg1869z00_2056;
							obj_t BgL_arg1870z00_2057;

							{	/* Module/class.scm 331 */
								obj_t BgL_arg1872z00_2058;

								BgL_arg1872z00_2058 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(15), BNIL);
								BgL_arg1869z00_2056 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_arg1872z00_2058);
							}
							{	/* Module/class.scm 332 */
								obj_t BgL_arg1873z00_2059;
								obj_t BgL_arg1874z00_2060;

								{	/* Module/class.scm 332 */
									obj_t BgL_arg1875z00_2061;

									{	/* Module/class.scm 332 */
										obj_t BgL_arg1876z00_2062;
										obj_t BgL_arg1877z00_2063;

										BgL_arg1876z00_2062 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(20), BNIL);
										{	/* Module/class.scm 333 */
											obj_t BgL_arg1878z00_2064;

											{	/* Module/class.scm 333 */
												obj_t BgL_arg1879z00_2065;

												{	/* Module/class.scm 333 */
													obj_t BgL_arg1880z00_2066;
													obj_t BgL_arg1882z00_2067;

													BgL_arg1880z00_2066 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt)
																	((BgL_typez00_bglt) BgL_classz00_3140))))->
														BgL_idz00);
													{	/* Module/class.scm 334 */
														obj_t BgL_arg1883z00_2068;

														{	/* Module/class.scm 334 */
															obj_t BgL_arg1884z00_2069;
															obj_t BgL_arg1885z00_2070;

															BgL_arg1884z00_2069 =
																(((BgL_slotz00_bglt) COBJECT(
																		((BgL_slotz00_bglt) BgL_slotz00_2051)))->
																BgL_virtualzd2numzd2);
															BgL_arg1885z00_2070 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(15), BNIL);
															BgL_arg1883z00_2068 =
																MAKE_YOUNG_PAIR(BgL_arg1884z00_2069,
																BgL_arg1885z00_2070);
														}
														BgL_arg1882z00_2067 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
															BgL_arg1883z00_2068);
													}
													BgL_arg1879z00_2065 =
														MAKE_YOUNG_PAIR(BgL_arg1880z00_2066,
														BgL_arg1882z00_2067);
												}
												BgL_arg1878z00_2064 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
													BgL_arg1879z00_2065);
											}
											BgL_arg1877z00_2063 =
												MAKE_YOUNG_PAIR(BgL_arg1878z00_2064, BNIL);
										}
										BgL_arg1875z00_2061 =
											MAKE_YOUNG_PAIR(BgL_arg1876z00_2062, BgL_arg1877z00_2063);
									}
									BgL_arg1873z00_2059 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BgL_arg1875z00_2061);
								}
								{	/* Module/class.scm 335 */
									obj_t BgL_arg1887z00_2071;

									{	/* Module/class.scm 335 */
										obj_t BgL_arg1888z00_2072;
										obj_t BgL_arg1889z00_2073;

										BgL_arg1888z00_2072 =
											(((BgL_slotz00_bglt) COBJECT(
													((BgL_slotz00_bglt) BgL_slotz00_2051)))->
											BgL_setterz00);
										{	/* Module/class.scm 335 */
											obj_t BgL_arg1890z00_2074;

											BgL_arg1890z00_2074 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(15), BNIL);
											BgL_arg1889z00_2073 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
												BgL_arg1890z00_2074);
										}
										BgL_arg1887z00_2071 =
											MAKE_YOUNG_PAIR(BgL_arg1888z00_2072, BgL_arg1889z00_2073);
									}
									BgL_arg1874z00_2060 =
										MAKE_YOUNG_PAIR(BgL_arg1887z00_2071, BNIL);
								}
								BgL_arg1870z00_2057 =
									MAKE_YOUNG_PAIR(BgL_arg1873z00_2059, BgL_arg1874z00_2060);
							}
							BgL_arg1868z00_2055 =
								MAKE_YOUNG_PAIR(BgL_arg1869z00_2056, BgL_arg1870z00_2057);
						}
						BgL_arg1866z00_2054 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BgL_arg1868z00_2055);
					}
					return
						BGl_epairifyzd2slotze70z35zzmodule_classz00(BgL_arg1866z00_2054,
						BgL_slotz00_2051);
				}
		}

	}



/* class-finalizer */
	BGL_EXPORTED_DEF obj_t BGl_classzd2finaliza7erz75zzmodule_classz00(void)
	{
		{	/* Module/class.scm 351 */
			{	/* Module/class.scm 353 */
				bool_t BgL_test2131z00_3943;

				if (NULLP(BGl_za2classzd2accessesza2zd2zzmodule_classz00))
					{	/* Module/class.scm 353 */
						if (
							(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
								CNST_TABLE_REF(23)))
							{	/* Module/class.scm 353 */
								BgL_test2131z00_3943 = ((bool_t) 0);
							}
						else
							{	/* Module/class.scm 353 */
								BgL_test2131z00_3943 = ((bool_t) 1);
							}
					}
				else
					{	/* Module/class.scm 353 */
						BgL_test2131z00_3943 = ((bool_t) 0);
					}
				if (BgL_test2131z00_3943)
					{	/* Module/class.scm 353 */
						return CNST_TABLE_REF(24);
					}
				else
					{	/* Module/class.scm 368 */
						obj_t BgL_bodyz00_2080;

						BgL_bodyz00_2080 =
							BGl_forcezd2classzd2accessesz00zzmodule_classz00();
						{	/* Module/class.scm 368 */
							obj_t BgL_bodyz00_2081;

							BgL_bodyz00_2081 =
								BGl_appendzd221011zd2zzmodule_classz00(bgl_reverse
								(BGl_za2declaredzd2classesza2zd2zzmodule_classz00),
								BgL_bodyz00_2080);
							{	/* Module/class.scm 369 */
								obj_t BgL_bodyz00_2082;

								{	/* Module/class.scm 370 */
									bool_t BgL_test2134z00_3953;

									if (
										((long) CINT(BGl_za2debugzd2moduleza2zd2zzengine_paramz00) >
											0L))
										{	/* Module/class.scm 371 */
											bool_t BgL_test2136z00_3957;

											{	/* Module/class.scm 372 */
												obj_t BgL_arg1920z00_2107;

												{	/* Module/class.scm 372 */
													obj_t BgL_arg1923z00_2108;

													BgL_arg1923z00_2108 =
														BGl_thezd2backendzd2zzbackend_backendz00();
													BgL_arg1920z00_2107 =
														(((BgL_backendz00_bglt) COBJECT(
																((BgL_backendz00_bglt) BgL_arg1923z00_2108)))->
														BgL_debugzd2supportzd2);
												}
												BgL_test2136z00_3957 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(CNST_TABLE_REF(25), BgL_arg1920z00_2107));
											}
											if (BgL_test2136z00_3957)
												{	/* Module/class.scm 371 */
													BgL_test2134z00_3953 =
														PAIRP
														(BGl_za2declaredzd2classesza2zd2zzmodule_classz00);
												}
											else
												{	/* Module/class.scm 371 */
													BgL_test2134z00_3953 = ((bool_t) 0);
												}
										}
									else
										{	/* Module/class.scm 370 */
											BgL_test2134z00_3953 = ((bool_t) 0);
										}
									if (BgL_test2134z00_3953)
										{	/* Module/class.scm 377 */
											obj_t BgL_arg1912z00_2098;

											{	/* Module/class.scm 377 */
												obj_t BgL_arg1913z00_2099;

												{	/* Module/class.scm 377 */
													obj_t BgL_arg1914z00_2100;
													obj_t BgL_arg1916z00_2101;

													{	/* Module/class.scm 377 */
														obj_t BgL_arg1917z00_2102;

														{	/* Module/class.scm 377 */
															obj_t BgL_arg1918z00_2103;

															{	/* Module/class.scm 377 */
																obj_t BgL_arg1919z00_2104;

																{	/* Module/class.scm 377 */
																	obj_t BgL_symbolz00_2582;

																	BgL_symbolz00_2582 =
																		BGl_za2moduleza2z00zzmodule_modulez00;
																	{	/* Module/class.scm 377 */
																		obj_t BgL_arg1455z00_2583;

																		BgL_arg1455z00_2583 =
																			SYMBOL_TO_STRING(BgL_symbolz00_2582);
																		BgL_arg1919z00_2104 =
																			BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																			(BgL_arg1455z00_2583);
																	}
																}
																BgL_arg1918z00_2103 =
																	string_append_3
																	(BGl_string2074z00zzmodule_classz00,
																	BgL_arg1919z00_2104,
																	BGl_string2075z00zzmodule_classz00);
															}
															BgL_arg1917z00_2102 =
																MAKE_YOUNG_PAIR(BgL_arg1918z00_2103, BNIL);
														}
														BgL_arg1914z00_2100 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(26),
															BgL_arg1917z00_2102);
													}
													BgL_arg1916z00_2101 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_bodyz00_2081, BNIL);
													BgL_arg1913z00_2099 =
														MAKE_YOUNG_PAIR(BgL_arg1914z00_2100,
														BgL_arg1916z00_2101);
												}
												BgL_arg1912z00_2098 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(27),
													BgL_arg1913z00_2099);
											}
											BgL_bodyz00_2082 =
												MAKE_YOUNG_PAIR(BgL_arg1912z00_2098, BNIL);
										}
									else
										{	/* Module/class.scm 370 */
											BgL_bodyz00_2082 = BgL_bodyz00_2081;
										}
								}
								{	/* Module/class.scm 370 */

									{	/* Module/class.scm 383 */
										obj_t BgL_arg1893z00_2083;
										bool_t BgL_arg1894z00_2084;

										if (PAIRP(BgL_bodyz00_2082))
											{	/* Module/class.scm 383 */
												BgL_arg1893z00_2083 = BgL_bodyz00_2082;
											}
										else
											{	/* Module/class.scm 383 */
												BgL_arg1893z00_2083 = CNST_TABLE_REF(28);
											}
										BgL_arg1894z00_2084 =
											(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
											CNST_TABLE_REF(23));
										{	/* Module/class.scm 381 */
											obj_t BgL_idz00_2584;

											BgL_idz00_2584 = CNST_TABLE_REF(29);
											{	/* Module/class.scm 381 */
												obj_t BgL_newz00_2585;

												BgL_newz00_2585 =
													create_struct(CNST_TABLE_REF(30), (int) (5L));
												{	/* Module/class.scm 381 */
													obj_t BgL_auxz00_3987;
													int BgL_tmpz00_3985;

													BgL_auxz00_3987 = BBOOL(BgL_arg1894z00_2084);
													BgL_tmpz00_3985 = (int) (4L);
													STRUCT_SET(BgL_newz00_2585, BgL_tmpz00_3985,
														BgL_auxz00_3987);
												}
												{	/* Module/class.scm 381 */
													int BgL_tmpz00_3990;

													BgL_tmpz00_3990 = (int) (3L);
													STRUCT_SET(BgL_newz00_2585, BgL_tmpz00_3990, BTRUE);
												}
												{	/* Module/class.scm 381 */
													int BgL_tmpz00_3993;

													BgL_tmpz00_3993 = (int) (2L);
													STRUCT_SET(BgL_newz00_2585, BgL_tmpz00_3993,
														BgL_arg1893z00_2083);
												}
												{	/* Module/class.scm 381 */
													obj_t BgL_auxz00_3998;
													int BgL_tmpz00_3996;

													BgL_auxz00_3998 = BINT(19L);
													BgL_tmpz00_3996 = (int) (1L);
													STRUCT_SET(BgL_newz00_2585, BgL_tmpz00_3996,
														BgL_auxz00_3998);
												}
												{	/* Module/class.scm 381 */
													int BgL_tmpz00_4001;

													BgL_tmpz00_4001 = (int) (0L);
													STRUCT_SET(BgL_newz00_2585, BgL_tmpz00_4001,
														BgL_idz00_2584);
												}
												BGl_za2objectzd2unitza2zd2zzmodule_classz00 =
													BgL_newz00_2585;
									}}}
									{	/* Module/class.scm 398 */
										bool_t BgL_arg1896z00_2086;

										BgL_arg1896z00_2086 =
											(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
											CNST_TABLE_REF(23));
										{	/* Module/class.scm 392 */
											obj_t BgL_idz00_2591;
											obj_t BgL_sexpza2za2_2592;

											BgL_idz00_2591 = CNST_TABLE_REF(31);
											BgL_sexpza2za2_2592 = CNST_TABLE_REF(28);
											{	/* Module/class.scm 392 */
												obj_t BgL_newz00_2593;

												BgL_newz00_2593 =
													create_struct(CNST_TABLE_REF(30), (int) (5L));
												{	/* Module/class.scm 392 */
													obj_t BgL_auxz00_4013;
													int BgL_tmpz00_4011;

													BgL_auxz00_4013 = BBOOL(BgL_arg1896z00_2086);
													BgL_tmpz00_4011 = (int) (4L);
													STRUCT_SET(BgL_newz00_2593, BgL_tmpz00_4011,
														BgL_auxz00_4013);
												}
												{	/* Module/class.scm 392 */
													int BgL_tmpz00_4016;

													BgL_tmpz00_4016 = (int) (3L);
													STRUCT_SET(BgL_newz00_2593, BgL_tmpz00_4016, BTRUE);
												}
												{	/* Module/class.scm 392 */
													int BgL_tmpz00_4019;

													BgL_tmpz00_4019 = (int) (2L);
													STRUCT_SET(BgL_newz00_2593, BgL_tmpz00_4019,
														BgL_sexpza2za2_2592);
												}
												{	/* Module/class.scm 392 */
													obj_t BgL_auxz00_4024;
													int BgL_tmpz00_4022;

													BgL_auxz00_4024 = BINT(21L);
													BgL_tmpz00_4022 = (int) (1L);
													STRUCT_SET(BgL_newz00_2593, BgL_tmpz00_4022,
														BgL_auxz00_4024);
												}
												{	/* Module/class.scm 392 */
													int BgL_tmpz00_4027;

													BgL_tmpz00_4027 = (int) (0L);
													STRUCT_SET(BgL_newz00_2593, BgL_tmpz00_4027,
														BgL_idz00_2591);
												}
												BGl_za2methodzd2unitza2zd2zzmodule_classz00 =
													BgL_newz00_2593;
									}}}
									{	/* Module/class.scm 405 */
										bool_t BgL_arg1897z00_2087;

										BgL_arg1897z00_2087 =
											(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
											CNST_TABLE_REF(23));
										{	/* Module/class.scm 399 */
											obj_t BgL_idz00_2599;
											obj_t BgL_sexpza2za2_2600;

											BgL_idz00_2599 = CNST_TABLE_REF(32);
											BgL_sexpza2za2_2600 = CNST_TABLE_REF(28);
											{	/* Module/class.scm 399 */
												obj_t BgL_newz00_2601;

												BgL_newz00_2601 =
													create_struct(CNST_TABLE_REF(30), (int) (5L));
												{	/* Module/class.scm 399 */
													obj_t BgL_auxz00_4039;
													int BgL_tmpz00_4037;

													BgL_auxz00_4039 = BBOOL(BgL_arg1897z00_2087);
													BgL_tmpz00_4037 = (int) (4L);
													STRUCT_SET(BgL_newz00_2601, BgL_tmpz00_4037,
														BgL_auxz00_4039);
												}
												{	/* Module/class.scm 399 */
													int BgL_tmpz00_4042;

													BgL_tmpz00_4042 = (int) (3L);
													STRUCT_SET(BgL_newz00_2601, BgL_tmpz00_4042, BTRUE);
												}
												{	/* Module/class.scm 399 */
													int BgL_tmpz00_4045;

													BgL_tmpz00_4045 = (int) (2L);
													STRUCT_SET(BgL_newz00_2601, BgL_tmpz00_4045,
														BgL_sexpza2za2_2600);
												}
												{	/* Module/class.scm 399 */
													obj_t BgL_auxz00_4050;
													int BgL_tmpz00_4048;

													BgL_auxz00_4050 = BINT(20L);
													BgL_tmpz00_4048 = (int) (1L);
													STRUCT_SET(BgL_newz00_2601, BgL_tmpz00_4048,
														BgL_auxz00_4050);
												}
												{	/* Module/class.scm 399 */
													int BgL_tmpz00_4053;

													BgL_tmpz00_4053 = (int) (0L);
													STRUCT_SET(BgL_newz00_2601, BgL_tmpz00_4053,
														BgL_idz00_2599);
												}
												BGl_za2genericzd2unitza2zd2zzmodule_classz00 =
													BgL_newz00_2601;
									}}}
									BGl_za2classzd2accessesza2zd2zzmodule_classz00 = BNIL;
									BGl_za2declaredzd2classesza2zd2zzmodule_classz00 = BNIL;
									{	/* Module/class.scm 409 */
										obj_t BgL_list1898z00_2088;

										{	/* Module/class.scm 409 */
											obj_t BgL_arg1899z00_2089;

											{	/* Module/class.scm 409 */
												obj_t BgL_arg1901z00_2090;

												BgL_arg1901z00_2090 =
													MAKE_YOUNG_PAIR
													(BGl_za2methodzd2unitza2zd2zzmodule_classz00, BNIL);
												BgL_arg1899z00_2089 =
													MAKE_YOUNG_PAIR
													(BGl_za2genericzd2unitza2zd2zzmodule_classz00,
													BgL_arg1901z00_2090);
											}
											BgL_list1898z00_2088 =
												MAKE_YOUNG_PAIR
												(BGl_za2objectzd2unitza2zd2zzmodule_classz00,
												BgL_arg1899z00_2089);
										}
										return BgL_list1898z00_2088;
									}
								}
							}
						}
					}
			}
		}

	}



/* &class-finalizer */
	obj_t BGl_z62classzd2finaliza7erz17zzmodule_classz00(obj_t BgL_envz00_3137)
	{
		{	/* Module/class.scm 351 */
			return BGl_classzd2finaliza7erz75zzmodule_classz00();
		}

	}



/* class-finalizer-add-static! */
	BGL_EXPORTED_DEF obj_t
		BGl_classzd2finaliza7erzd2addzd2staticz12z67zzmodule_classz00(void)
	{
		{	/* Module/class.scm 417 */
			{	/* Module/class.scm 418 */
				obj_t BgL_bodyz00_2111;

				BgL_bodyz00_2111 = BGl_forcezd2classzd2accessesz00zzmodule_classz00();
				{	/* Module/class.scm 418 */
					obj_t BgL_bodyz00_2112;

					BgL_bodyz00_2112 =
						BGl_appendzd221011zd2zzmodule_classz00(bgl_reverse
						(BGl_za2declaredzd2classesza2zd2zzmodule_classz00),
						BgL_bodyz00_2111);
					{	/* Module/class.scm 419 */

						{	/* Module/class.scm 420 */
							bool_t BgL_test2138z00_4063;

							{	/* Module/class.scm 420 */
								obj_t BgL_oz00_2608;

								BgL_oz00_2608 = BGl_za2objectzd2unitza2zd2zzmodule_classz00;
								if (STRUCTP(BgL_oz00_2608))
									{	/* Module/class.scm 420 */
										BgL_test2138z00_4063 =
											(STRUCT_KEY(BgL_oz00_2608) == CNST_TABLE_REF(30));
									}
								else
									{	/* Module/class.scm 420 */
										BgL_test2138z00_4063 = ((bool_t) 0);
									}
							}
							if (BgL_test2138z00_4063)
								{	/* Module/class.scm 422 */
									obj_t BgL_arg1926z00_2114;

									BgL_arg1926z00_2114 =
										BGl_appendzd221011zd2zzmodule_classz00(STRUCT_REF
										(BGl_za2objectzd2unitza2zd2zzmodule_classz00, (int) (2L)),
										BgL_bodyz00_2112);
									{	/* Module/class.scm 421 */
										int BgL_tmpz00_4072;

										BgL_tmpz00_4072 = (int) (2L);
										STRUCT_SET(BGl_za2objectzd2unitza2zd2zzmodule_classz00,
											BgL_tmpz00_4072, BgL_arg1926z00_2114);
								}}
							else
								{	/* Module/class.scm 426 */
									obj_t BgL_arg1928z00_2116;
									bool_t BgL_arg1929z00_2117;

									if (PAIRP(BgL_bodyz00_2112))
										{	/* Module/class.scm 426 */
											BgL_arg1928z00_2116 = BgL_bodyz00_2112;
										}
									else
										{	/* Module/class.scm 426 */
											BgL_arg1928z00_2116 = CNST_TABLE_REF(28);
										}
									BgL_arg1929z00_2117 =
										(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
										CNST_TABLE_REF(23));
									{	/* Module/class.scm 424 */
										obj_t BgL_idz00_2617;

										BgL_idz00_2617 = CNST_TABLE_REF(29);
										{	/* Module/class.scm 424 */
											obj_t BgL_newz00_2618;

											BgL_newz00_2618 =
												create_struct(CNST_TABLE_REF(30), (int) (5L));
											{	/* Module/class.scm 424 */
												obj_t BgL_auxz00_4086;
												int BgL_tmpz00_4084;

												BgL_auxz00_4086 = BBOOL(BgL_arg1929z00_2117);
												BgL_tmpz00_4084 = (int) (4L);
												STRUCT_SET(BgL_newz00_2618, BgL_tmpz00_4084,
													BgL_auxz00_4086);
											}
											{	/* Module/class.scm 424 */
												int BgL_tmpz00_4089;

												BgL_tmpz00_4089 = (int) (3L);
												STRUCT_SET(BgL_newz00_2618, BgL_tmpz00_4089, BTRUE);
											}
											{	/* Module/class.scm 424 */
												int BgL_tmpz00_4092;

												BgL_tmpz00_4092 = (int) (2L);
												STRUCT_SET(BgL_newz00_2618, BgL_tmpz00_4092,
													BgL_arg1928z00_2116);
											}
											{	/* Module/class.scm 424 */
												obj_t BgL_auxz00_4097;
												int BgL_tmpz00_4095;

												BgL_auxz00_4097 = BINT(19L);
												BgL_tmpz00_4095 = (int) (1L);
												STRUCT_SET(BgL_newz00_2618, BgL_tmpz00_4095,
													BgL_auxz00_4097);
											}
											{	/* Module/class.scm 424 */
												int BgL_tmpz00_4100;

												BgL_tmpz00_4100 = (int) (0L);
												STRUCT_SET(BgL_newz00_2618, BgL_tmpz00_4100,
													BgL_idz00_2617);
											}
											BGl_za2objectzd2unitza2zd2zzmodule_classz00 =
												BgL_newz00_2618;
						}}}}
						{	/* Module/class.scm 429 */
							bool_t BgL_test2141z00_4103;

							{	/* Module/class.scm 429 */
								obj_t BgL_oz00_2624;

								BgL_oz00_2624 = BGl_za2methodzd2unitza2zd2zzmodule_classz00;
								if (STRUCTP(BgL_oz00_2624))
									{	/* Module/class.scm 429 */
										BgL_test2141z00_4103 =
											(STRUCT_KEY(BgL_oz00_2624) == CNST_TABLE_REF(30));
									}
								else
									{	/* Module/class.scm 429 */
										BgL_test2141z00_4103 = ((bool_t) 0);
									}
							}
							if (BgL_test2141z00_4103)
								{	/* Module/class.scm 429 */
									BFALSE;
								}
							else
								{	/* Module/class.scm 437 */
									bool_t BgL_arg1932z00_2120;

									BgL_arg1932z00_2120 =
										(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
										CNST_TABLE_REF(23));
									{	/* Module/class.scm 431 */
										obj_t BgL_idz00_2629;
										obj_t BgL_sexpza2za2_2630;

										BgL_idz00_2629 = CNST_TABLE_REF(31);
										BgL_sexpza2za2_2630 = CNST_TABLE_REF(28);
										{	/* Module/class.scm 431 */
											obj_t BgL_newz00_2631;

											BgL_newz00_2631 =
												create_struct(CNST_TABLE_REF(30), (int) (5L));
											{	/* Module/class.scm 431 */
												obj_t BgL_auxz00_4118;
												int BgL_tmpz00_4116;

												BgL_auxz00_4118 = BBOOL(BgL_arg1932z00_2120);
												BgL_tmpz00_4116 = (int) (4L);
												STRUCT_SET(BgL_newz00_2631, BgL_tmpz00_4116,
													BgL_auxz00_4118);
											}
											{	/* Module/class.scm 431 */
												int BgL_tmpz00_4121;

												BgL_tmpz00_4121 = (int) (3L);
												STRUCT_SET(BgL_newz00_2631, BgL_tmpz00_4121, BTRUE);
											}
											{	/* Module/class.scm 431 */
												int BgL_tmpz00_4124;

												BgL_tmpz00_4124 = (int) (2L);
												STRUCT_SET(BgL_newz00_2631, BgL_tmpz00_4124,
													BgL_sexpza2za2_2630);
											}
											{	/* Module/class.scm 431 */
												obj_t BgL_auxz00_4129;
												int BgL_tmpz00_4127;

												BgL_auxz00_4129 = BINT(21L);
												BgL_tmpz00_4127 = (int) (1L);
												STRUCT_SET(BgL_newz00_2631, BgL_tmpz00_4127,
													BgL_auxz00_4129);
											}
											{	/* Module/class.scm 431 */
												int BgL_tmpz00_4132;

												BgL_tmpz00_4132 = (int) (0L);
												STRUCT_SET(BgL_newz00_2631, BgL_tmpz00_4132,
													BgL_idz00_2629);
											}
											BGl_za2methodzd2unitza2zd2zzmodule_classz00 =
												BgL_newz00_2631;
						}}}}
						{	/* Module/class.scm 438 */
							bool_t BgL_test2143z00_4135;

							{	/* Module/class.scm 438 */
								obj_t BgL_oz00_2637;

								BgL_oz00_2637 = BGl_za2genericzd2unitza2zd2zzmodule_classz00;
								if (STRUCTP(BgL_oz00_2637))
									{	/* Module/class.scm 438 */
										BgL_test2143z00_4135 =
											(STRUCT_KEY(BgL_oz00_2637) == CNST_TABLE_REF(30));
									}
								else
									{	/* Module/class.scm 438 */
										BgL_test2143z00_4135 = ((bool_t) 0);
									}
							}
							if (BgL_test2143z00_4135)
								{	/* Module/class.scm 438 */
									BFALSE;
								}
							else
								{	/* Module/class.scm 446 */
									bool_t BgL_arg1934z00_2122;

									BgL_arg1934z00_2122 =
										(BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
										CNST_TABLE_REF(23));
									{	/* Module/class.scm 440 */
										obj_t BgL_idz00_2642;
										obj_t BgL_sexpza2za2_2643;

										BgL_idz00_2642 = CNST_TABLE_REF(32);
										BgL_sexpza2za2_2643 = CNST_TABLE_REF(28);
										{	/* Module/class.scm 440 */
											obj_t BgL_newz00_2644;

											BgL_newz00_2644 =
												create_struct(CNST_TABLE_REF(30), (int) (5L));
											{	/* Module/class.scm 440 */
												obj_t BgL_auxz00_4150;
												int BgL_tmpz00_4148;

												BgL_auxz00_4150 = BBOOL(BgL_arg1934z00_2122);
												BgL_tmpz00_4148 = (int) (4L);
												STRUCT_SET(BgL_newz00_2644, BgL_tmpz00_4148,
													BgL_auxz00_4150);
											}
											{	/* Module/class.scm 440 */
												int BgL_tmpz00_4153;

												BgL_tmpz00_4153 = (int) (3L);
												STRUCT_SET(BgL_newz00_2644, BgL_tmpz00_4153, BTRUE);
											}
											{	/* Module/class.scm 440 */
												int BgL_tmpz00_4156;

												BgL_tmpz00_4156 = (int) (2L);
												STRUCT_SET(BgL_newz00_2644, BgL_tmpz00_4156,
													BgL_sexpza2za2_2643);
											}
											{	/* Module/class.scm 440 */
												obj_t BgL_auxz00_4161;
												int BgL_tmpz00_4159;

												BgL_auxz00_4161 = BINT(21L);
												BgL_tmpz00_4159 = (int) (1L);
												STRUCT_SET(BgL_newz00_2644, BgL_tmpz00_4159,
													BgL_auxz00_4161);
											}
											{	/* Module/class.scm 440 */
												int BgL_tmpz00_4164;

												BgL_tmpz00_4164 = (int) (0L);
												STRUCT_SET(BgL_newz00_2644, BgL_tmpz00_4164,
													BgL_idz00_2642);
											}
											BGl_za2genericzd2unitza2zd2zzmodule_classz00 =
												BgL_newz00_2644;
						}}}}
						BGl_za2classzd2accessesza2zd2zzmodule_classz00 = BNIL;
						BGl_za2declaredzd2classesza2zd2zzmodule_classz00 = BNIL;
						return BUNSPEC;
					}
				}
			}
		}

	}



/* &class-finalizer-add-static! */
	obj_t BGl_z62classzd2finaliza7erzd2addzd2staticz12z05zzmodule_classz00(obj_t
		BgL_envz00_3138)
	{
		{	/* Module/class.scm 417 */
			return BGl_classzd2finaliza7erzd2addzd2staticz12z67zzmodule_classz00();
		}

	}



/* force-class-accesses */
	obj_t BGl_forcezd2classzd2accessesz00zzmodule_classz00(void)
	{
		{	/* Module/class.scm 458 */
			{	/* Module/class.scm 460 */
				obj_t BgL_g1113z00_2124;

				BgL_g1113z00_2124 =
					bgl_reverse(BGl_za2classzd2accessesza2zd2zzmodule_classz00);
				{
					obj_t BgL_curz00_2128;
					obj_t BgL_nextz00_2129;
					obj_t BgL_accessz00_2130;

					BgL_curz00_2128 = BgL_g1113z00_2124;
					BgL_nextz00_2129 = BNIL;
					BgL_accessz00_2130 = BNIL;
				BgL_zc3z04anonymousza31936ze3z87_2131:
					if (NULLP(BgL_curz00_2128))
						{	/* Module/class.scm 463 */
							if (NULLP(BgL_nextz00_2129))
								{	/* Module/class.scm 464 */
									return BgL_accessz00_2130;
								}
							else
								{
									obj_t BgL_nextz00_4174;
									obj_t BgL_curz00_4173;

									BgL_curz00_4173 = BgL_nextz00_2129;
									BgL_nextz00_4174 = BNIL;
									BgL_nextz00_2129 = BgL_nextz00_4174;
									BgL_curz00_2128 = BgL_curz00_4173;
									goto BgL_zc3z04anonymousza31936ze3z87_2131;
								}
						}
					else
						{	/* Module/class.scm 467 */
							obj_t BgL_classz00_2134;

							{	/* Module/class.scm 467 */
								obj_t BgL_pairz00_2651;

								BgL_pairz00_2651 = CAR(((obj_t) BgL_curz00_2128));
								BgL_classz00_2134 = CAR(BgL_pairz00_2651);
							}
							{	/* Module/class.scm 467 */
								obj_t BgL_superz00_2135;

								{	/* Module/class.scm 468 */
									bool_t BgL_test2147z00_4178;

									{	/* Module/class.scm 468 */
										obj_t BgL_classz00_2652;

										BgL_classz00_2652 = BGl_tclassz00zzobject_classz00;
										if (BGL_OBJECTP(BgL_classz00_2134))
											{	/* Module/class.scm 468 */
												BgL_objectz00_bglt BgL_arg1807z00_2654;

												BgL_arg1807z00_2654 =
													(BgL_objectz00_bglt) (BgL_classz00_2134);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Module/class.scm 468 */
														long BgL_idxz00_2660;

														BgL_idxz00_2660 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2654);
														BgL_test2147z00_4178 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2660 + 2L)) == BgL_classz00_2652);
													}
												else
													{	/* Module/class.scm 468 */
														bool_t BgL_res2062z00_2685;

														{	/* Module/class.scm 468 */
															obj_t BgL_oclassz00_2668;

															{	/* Module/class.scm 468 */
																obj_t BgL_arg1815z00_2676;
																long BgL_arg1816z00_2677;

																BgL_arg1815z00_2676 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Module/class.scm 468 */
																	long BgL_arg1817z00_2678;

																	BgL_arg1817z00_2678 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2654);
																	BgL_arg1816z00_2677 =
																		(BgL_arg1817z00_2678 - OBJECT_TYPE);
																}
																BgL_oclassz00_2668 =
																	VECTOR_REF(BgL_arg1815z00_2676,
																	BgL_arg1816z00_2677);
															}
															{	/* Module/class.scm 468 */
																bool_t BgL__ortest_1115z00_2669;

																BgL__ortest_1115z00_2669 =
																	(BgL_classz00_2652 == BgL_oclassz00_2668);
																if (BgL__ortest_1115z00_2669)
																	{	/* Module/class.scm 468 */
																		BgL_res2062z00_2685 =
																			BgL__ortest_1115z00_2669;
																	}
																else
																	{	/* Module/class.scm 468 */
																		long BgL_odepthz00_2670;

																		{	/* Module/class.scm 468 */
																			obj_t BgL_arg1804z00_2671;

																			BgL_arg1804z00_2671 =
																				(BgL_oclassz00_2668);
																			BgL_odepthz00_2670 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2671);
																		}
																		if ((2L < BgL_odepthz00_2670))
																			{	/* Module/class.scm 468 */
																				obj_t BgL_arg1802z00_2673;

																				{	/* Module/class.scm 468 */
																					obj_t BgL_arg1803z00_2674;

																					BgL_arg1803z00_2674 =
																						(BgL_oclassz00_2668);
																					BgL_arg1802z00_2673 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2674, 2L);
																				}
																				BgL_res2062z00_2685 =
																					(BgL_arg1802z00_2673 ==
																					BgL_classz00_2652);
																			}
																		else
																			{	/* Module/class.scm 468 */
																				BgL_res2062z00_2685 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2147z00_4178 = BgL_res2062z00_2685;
													}
											}
										else
											{	/* Module/class.scm 468 */
												BgL_test2147z00_4178 = ((bool_t) 0);
											}
									}
									if (BgL_test2147z00_4178)
										{
											BgL_tclassz00_bglt BgL_auxz00_4201;

											{
												obj_t BgL_auxz00_4202;

												{	/* Module/class.scm 469 */
													BgL_objectz00_bglt BgL_tmpz00_4203;

													BgL_tmpz00_4203 =
														((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_classz00_2134));
													BgL_auxz00_4202 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4203);
												}
												BgL_auxz00_4201 =
													((BgL_tclassz00_bglt) BgL_auxz00_4202);
											}
											BgL_superz00_2135 =
												(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4201))->
												BgL_itszd2superzd2);
										}
									else
										{
											BgL_jclassz00_bglt BgL_auxz00_4209;

											{
												obj_t BgL_auxz00_4210;

												{	/* Module/class.scm 470 */
													BgL_objectz00_bglt BgL_tmpz00_4211;

													BgL_tmpz00_4211 =
														((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_classz00_2134));
													BgL_auxz00_4210 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4211);
												}
												BgL_auxz00_4209 =
													((BgL_jclassz00_bglt) BgL_auxz00_4210);
											}
											BgL_superz00_2135 =
												(((BgL_jclassz00_bglt) COBJECT(BgL_auxz00_4209))->
												BgL_itszd2superzd2);
										}
								}
								{	/* Module/class.scm 468 */

									if ((BgL_superz00_2135 == BgL_classz00_2134))
										{	/* Module/class.scm 474 */
											obj_t BgL_arg1939z00_2136;
											obj_t BgL_arg1940z00_2137;

											BgL_arg1939z00_2136 = CDR(((obj_t) BgL_curz00_2128));
											{	/* Module/class.scm 476 */
												obj_t BgL_arg1941z00_2138;

												{	/* Module/class.scm 476 */
													obj_t BgL_arg1942z00_2139;

													{	/* Module/class.scm 476 */
														obj_t BgL_pairz00_2692;

														BgL_pairz00_2692 = CAR(((obj_t) BgL_curz00_2128));
														BgL_arg1942z00_2139 = CDR(BgL_pairz00_2692);
													}
													BgL_arg1941z00_2138 =
														BGL_PROCEDURE_CALL0(BgL_arg1942z00_2139);
												}
												BgL_arg1940z00_2137 =
													BGl_appendzd221011zd2zzmodule_classz00
													(BgL_arg1941z00_2138, BgL_accessz00_2130);
											}
											{
												obj_t BgL_accessz00_4229;
												obj_t BgL_curz00_4228;

												BgL_curz00_4228 = BgL_arg1939z00_2136;
												BgL_accessz00_4229 = BgL_arg1940z00_2137;
												BgL_accessz00_2130 = BgL_accessz00_4229;
												BgL_curz00_2128 = BgL_curz00_4228;
												goto BgL_zc3z04anonymousza31936ze3z87_2131;
											}
										}
									else
										{	/* Module/class.scm 477 */
											bool_t BgL_test2153z00_4230;

											{	/* Module/class.scm 477 */
												bool_t BgL_test2154z00_4231;

												{	/* Module/class.scm 477 */
													obj_t BgL_classz00_2693;

													BgL_classz00_2693 = BGl_tclassz00zzobject_classz00;
													if (BGL_OBJECTP(BgL_classz00_2134))
														{	/* Module/class.scm 477 */
															BgL_objectz00_bglt BgL_arg1807z00_2695;

															BgL_arg1807z00_2695 =
																(BgL_objectz00_bglt) (BgL_classz00_2134);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Module/class.scm 477 */
																	long BgL_idxz00_2701;

																	BgL_idxz00_2701 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2695);
																	BgL_test2154z00_4231 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2701 + 2L)) ==
																		BgL_classz00_2693);
																}
															else
																{	/* Module/class.scm 477 */
																	bool_t BgL_res2063z00_2726;

																	{	/* Module/class.scm 477 */
																		obj_t BgL_oclassz00_2709;

																		{	/* Module/class.scm 477 */
																			obj_t BgL_arg1815z00_2717;
																			long BgL_arg1816z00_2718;

																			BgL_arg1815z00_2717 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Module/class.scm 477 */
																				long BgL_arg1817z00_2719;

																				BgL_arg1817z00_2719 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2695);
																				BgL_arg1816z00_2718 =
																					(BgL_arg1817z00_2719 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2709 =
																				VECTOR_REF(BgL_arg1815z00_2717,
																				BgL_arg1816z00_2718);
																		}
																		{	/* Module/class.scm 477 */
																			bool_t BgL__ortest_1115z00_2710;

																			BgL__ortest_1115z00_2710 =
																				(BgL_classz00_2693 ==
																				BgL_oclassz00_2709);
																			if (BgL__ortest_1115z00_2710)
																				{	/* Module/class.scm 477 */
																					BgL_res2063z00_2726 =
																						BgL__ortest_1115z00_2710;
																				}
																			else
																				{	/* Module/class.scm 477 */
																					long BgL_odepthz00_2711;

																					{	/* Module/class.scm 477 */
																						obj_t BgL_arg1804z00_2712;

																						BgL_arg1804z00_2712 =
																							(BgL_oclassz00_2709);
																						BgL_odepthz00_2711 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2712);
																					}
																					if ((2L < BgL_odepthz00_2711))
																						{	/* Module/class.scm 477 */
																							obj_t BgL_arg1802z00_2714;

																							{	/* Module/class.scm 477 */
																								obj_t BgL_arg1803z00_2715;

																								BgL_arg1803z00_2715 =
																									(BgL_oclassz00_2709);
																								BgL_arg1802z00_2714 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2715, 2L);
																							}
																							BgL_res2063z00_2726 =
																								(BgL_arg1802z00_2714 ==
																								BgL_classz00_2693);
																						}
																					else
																						{	/* Module/class.scm 477 */
																							BgL_res2063z00_2726 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2154z00_4231 = BgL_res2063z00_2726;
																}
														}
													else
														{	/* Module/class.scm 477 */
															BgL_test2154z00_4231 = ((bool_t) 0);
														}
												}
												if (BgL_test2154z00_4231)
													{	/* Module/class.scm 477 */
														bool_t BgL_test2159z00_4254;

														{	/* Module/class.scm 477 */
															obj_t BgL_classz00_2727;

															BgL_classz00_2727 =
																BGl_tclassz00zzobject_classz00;
															if (BGL_OBJECTP(BgL_superz00_2135))
																{	/* Module/class.scm 477 */
																	BgL_objectz00_bglt BgL_arg1807z00_2729;

																	BgL_arg1807z00_2729 =
																		(BgL_objectz00_bglt) (BgL_superz00_2135);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Module/class.scm 477 */
																			long BgL_idxz00_2735;

																			BgL_idxz00_2735 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2729);
																			BgL_test2159z00_4254 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2735 + 2L)) ==
																				BgL_classz00_2727);
																		}
																	else
																		{	/* Module/class.scm 477 */
																			bool_t BgL_res2064z00_2760;

																			{	/* Module/class.scm 477 */
																				obj_t BgL_oclassz00_2743;

																				{	/* Module/class.scm 477 */
																					obj_t BgL_arg1815z00_2751;
																					long BgL_arg1816z00_2752;

																					BgL_arg1815z00_2751 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Module/class.scm 477 */
																						long BgL_arg1817z00_2753;

																						BgL_arg1817z00_2753 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2729);
																						BgL_arg1816z00_2752 =
																							(BgL_arg1817z00_2753 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2743 =
																						VECTOR_REF(BgL_arg1815z00_2751,
																						BgL_arg1816z00_2752);
																				}
																				{	/* Module/class.scm 477 */
																					bool_t BgL__ortest_1115z00_2744;

																					BgL__ortest_1115z00_2744 =
																						(BgL_classz00_2727 ==
																						BgL_oclassz00_2743);
																					if (BgL__ortest_1115z00_2744)
																						{	/* Module/class.scm 477 */
																							BgL_res2064z00_2760 =
																								BgL__ortest_1115z00_2744;
																						}
																					else
																						{	/* Module/class.scm 477 */
																							long BgL_odepthz00_2745;

																							{	/* Module/class.scm 477 */
																								obj_t BgL_arg1804z00_2746;

																								BgL_arg1804z00_2746 =
																									(BgL_oclassz00_2743);
																								BgL_odepthz00_2745 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2746);
																							}
																							if ((2L < BgL_odepthz00_2745))
																								{	/* Module/class.scm 477 */
																									obj_t BgL_arg1802z00_2748;

																									{	/* Module/class.scm 477 */
																										obj_t BgL_arg1803z00_2749;

																										BgL_arg1803z00_2749 =
																											(BgL_oclassz00_2743);
																										BgL_arg1802z00_2748 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2749, 2L);
																									}
																									BgL_res2064z00_2760 =
																										(BgL_arg1802z00_2748 ==
																										BgL_classz00_2727);
																								}
																							else
																								{	/* Module/class.scm 477 */
																									BgL_res2064z00_2760 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2159z00_4254 =
																				BgL_res2064z00_2760;
																		}
																}
															else
																{	/* Module/class.scm 477 */
																	BgL_test2159z00_4254 = ((bool_t) 0);
																}
														}
														if (BgL_test2159z00_4254)
															{	/* Module/class.scm 477 */
																BgL_test2153z00_4230 = ((bool_t) 0);
															}
														else
															{	/* Module/class.scm 477 */
																BgL_test2153z00_4230 = ((bool_t) 1);
															}
													}
												else
													{	/* Module/class.scm 477 */
														BgL_test2153z00_4230 = ((bool_t) 0);
													}
											}
											if (BgL_test2153z00_4230)
												{	/* Module/class.scm 478 */
													bool_t BgL_test2164z00_4277;

													{	/* Module/class.scm 478 */
														obj_t BgL_classz00_2761;

														BgL_classz00_2761 = BGl_typez00zztype_typez00;
														if (BGL_OBJECTP(BgL_superz00_2135))
															{	/* Module/class.scm 478 */
																BgL_objectz00_bglt BgL_arg1807z00_2763;

																BgL_arg1807z00_2763 =
																	(BgL_objectz00_bglt) (BgL_superz00_2135);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Module/class.scm 478 */
																		long BgL_idxz00_2769;

																		BgL_idxz00_2769 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2763);
																		BgL_test2164z00_4277 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2769 + 1L)) ==
																			BgL_classz00_2761);
																	}
																else
																	{	/* Module/class.scm 478 */
																		bool_t BgL_res2065z00_2794;

																		{	/* Module/class.scm 478 */
																			obj_t BgL_oclassz00_2777;

																			{	/* Module/class.scm 478 */
																				obj_t BgL_arg1815z00_2785;
																				long BgL_arg1816z00_2786;

																				BgL_arg1815z00_2785 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Module/class.scm 478 */
																					long BgL_arg1817z00_2787;

																					BgL_arg1817z00_2787 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2763);
																					BgL_arg1816z00_2786 =
																						(BgL_arg1817z00_2787 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2777 =
																					VECTOR_REF(BgL_arg1815z00_2785,
																					BgL_arg1816z00_2786);
																			}
																			{	/* Module/class.scm 478 */
																				bool_t BgL__ortest_1115z00_2778;

																				BgL__ortest_1115z00_2778 =
																					(BgL_classz00_2761 ==
																					BgL_oclassz00_2777);
																				if (BgL__ortest_1115z00_2778)
																					{	/* Module/class.scm 478 */
																						BgL_res2065z00_2794 =
																							BgL__ortest_1115z00_2778;
																					}
																				else
																					{	/* Module/class.scm 478 */
																						long BgL_odepthz00_2779;

																						{	/* Module/class.scm 478 */
																							obj_t BgL_arg1804z00_2780;

																							BgL_arg1804z00_2780 =
																								(BgL_oclassz00_2777);
																							BgL_odepthz00_2779 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2780);
																						}
																						if ((1L < BgL_odepthz00_2779))
																							{	/* Module/class.scm 478 */
																								obj_t BgL_arg1802z00_2782;

																								{	/* Module/class.scm 478 */
																									obj_t BgL_arg1803z00_2783;

																									BgL_arg1803z00_2783 =
																										(BgL_oclassz00_2777);
																									BgL_arg1802z00_2782 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2783, 1L);
																								}
																								BgL_res2065z00_2794 =
																									(BgL_arg1802z00_2782 ==
																									BgL_classz00_2761);
																							}
																						else
																							{	/* Module/class.scm 478 */
																								BgL_res2065z00_2794 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2164z00_4277 = BgL_res2065z00_2794;
																	}
															}
														else
															{	/* Module/class.scm 478 */
																BgL_test2164z00_4277 = ((bool_t) 0);
															}
													}
													if (BgL_test2164z00_4277)
														{	/* Module/class.scm 478 */
															{
																BgL_tclassz00_bglt BgL_auxz00_4300;

																{
																	obj_t BgL_auxz00_4301;

																	{	/* Module/class.scm 482 */
																		BgL_objectz00_bglt BgL_tmpz00_4302;

																		BgL_tmpz00_4302 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_classz00_2134));
																		BgL_auxz00_4301 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4302);
																	}
																	BgL_auxz00_4300 =
																		((BgL_tclassz00_bglt) BgL_auxz00_4301);
																}
																((((BgL_tclassz00_bglt)
																			COBJECT(BgL_auxz00_4300))->BgL_slotsz00) =
																	((obj_t) BNIL), BUNSPEC);
															}
															{	/* Module/class.scm 483 */
																obj_t BgL_arg1948z00_2145;

																BgL_arg1948z00_2145 =
																	CDR(((obj_t) BgL_curz00_2128));
																{
																	obj_t BgL_curz00_4310;

																	BgL_curz00_4310 = BgL_arg1948z00_2145;
																	BgL_curz00_2128 = BgL_curz00_4310;
																	goto BgL_zc3z04anonymousza31936ze3z87_2131;
																}
															}
														}
													else
														{	/* Module/class.scm 486 */
															obj_t BgL_arg1949z00_2146;
															obj_t BgL_arg1950z00_2147;

															BgL_arg1949z00_2146 =
																CDR(((obj_t) BgL_curz00_2128));
															{	/* Module/class.scm 488 */
																obj_t BgL_arg1951z00_2148;

																{	/* Module/class.scm 488 */
																	obj_t BgL_arg1952z00_2149;

																	{	/* Module/class.scm 488 */
																		obj_t BgL_pairz00_2800;

																		BgL_pairz00_2800 =
																			CAR(((obj_t) BgL_curz00_2128));
																		BgL_arg1952z00_2149 = CDR(BgL_pairz00_2800);
																	}
																	BgL_arg1951z00_2148 =
																		BGL_PROCEDURE_CALL0(BgL_arg1952z00_2149);
																}
																BgL_arg1950z00_2147 =
																	BGl_appendzd221011zd2zzmodule_classz00
																	(BgL_arg1951z00_2148, BgL_accessz00_2130);
															}
															{
																obj_t BgL_accessz00_4321;
																obj_t BgL_curz00_4320;

																BgL_curz00_4320 = BgL_arg1949z00_2146;
																BgL_accessz00_4321 = BgL_arg1950z00_2147;
																BgL_accessz00_2130 = BgL_accessz00_4321;
																BgL_curz00_2128 = BgL_curz00_4320;
																goto BgL_zc3z04anonymousza31936ze3z87_2131;
															}
														}
												}
											else
												{	/* Module/class.scm 489 */
													bool_t BgL_test2169z00_4322;

													{	/* Module/class.scm 489 */
														bool_t BgL_test2170z00_4323;

														{	/* Module/class.scm 489 */
															obj_t BgL_classz00_2801;

															BgL_classz00_2801 =
																BGl_jclassz00zzobject_classz00;
															if (BGL_OBJECTP(BgL_classz00_2134))
																{	/* Module/class.scm 489 */
																	BgL_objectz00_bglt BgL_arg1807z00_2803;

																	BgL_arg1807z00_2803 =
																		(BgL_objectz00_bglt) (BgL_classz00_2134);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Module/class.scm 489 */
																			long BgL_idxz00_2809;

																			BgL_idxz00_2809 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2803);
																			BgL_test2170z00_4323 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2809 + 2L)) ==
																				BgL_classz00_2801);
																		}
																	else
																		{	/* Module/class.scm 489 */
																			bool_t BgL_res2066z00_2834;

																			{	/* Module/class.scm 489 */
																				obj_t BgL_oclassz00_2817;

																				{	/* Module/class.scm 489 */
																					obj_t BgL_arg1815z00_2825;
																					long BgL_arg1816z00_2826;

																					BgL_arg1815z00_2825 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Module/class.scm 489 */
																						long BgL_arg1817z00_2827;

																						BgL_arg1817z00_2827 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2803);
																						BgL_arg1816z00_2826 =
																							(BgL_arg1817z00_2827 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2817 =
																						VECTOR_REF(BgL_arg1815z00_2825,
																						BgL_arg1816z00_2826);
																				}
																				{	/* Module/class.scm 489 */
																					bool_t BgL__ortest_1115z00_2818;

																					BgL__ortest_1115z00_2818 =
																						(BgL_classz00_2801 ==
																						BgL_oclassz00_2817);
																					if (BgL__ortest_1115z00_2818)
																						{	/* Module/class.scm 489 */
																							BgL_res2066z00_2834 =
																								BgL__ortest_1115z00_2818;
																						}
																					else
																						{	/* Module/class.scm 489 */
																							long BgL_odepthz00_2819;

																							{	/* Module/class.scm 489 */
																								obj_t BgL_arg1804z00_2820;

																								BgL_arg1804z00_2820 =
																									(BgL_oclassz00_2817);
																								BgL_odepthz00_2819 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2820);
																							}
																							if ((2L < BgL_odepthz00_2819))
																								{	/* Module/class.scm 489 */
																									obj_t BgL_arg1802z00_2822;

																									{	/* Module/class.scm 489 */
																										obj_t BgL_arg1803z00_2823;

																										BgL_arg1803z00_2823 =
																											(BgL_oclassz00_2817);
																										BgL_arg1802z00_2822 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2823, 2L);
																									}
																									BgL_res2066z00_2834 =
																										(BgL_arg1802z00_2822 ==
																										BgL_classz00_2801);
																								}
																							else
																								{	/* Module/class.scm 489 */
																									BgL_res2066z00_2834 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2170z00_4323 =
																				BgL_res2066z00_2834;
																		}
																}
															else
																{	/* Module/class.scm 489 */
																	BgL_test2170z00_4323 = ((bool_t) 0);
																}
														}
														if (BgL_test2170z00_4323)
															{	/* Module/class.scm 489 */
																bool_t BgL_test2175z00_4346;

																{	/* Module/class.scm 489 */
																	obj_t BgL_classz00_2835;

																	BgL_classz00_2835 =
																		BGl_jclassz00zzobject_classz00;
																	if (BGL_OBJECTP(BgL_superz00_2135))
																		{	/* Module/class.scm 489 */
																			BgL_objectz00_bglt BgL_arg1807z00_2837;

																			BgL_arg1807z00_2837 =
																				(BgL_objectz00_bglt)
																				(BgL_superz00_2135);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Module/class.scm 489 */
																					long BgL_idxz00_2843;

																					BgL_idxz00_2843 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2837);
																					BgL_test2175z00_4346 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2843 + 2L)) ==
																						BgL_classz00_2835);
																				}
																			else
																				{	/* Module/class.scm 489 */
																					bool_t BgL_res2067z00_2868;

																					{	/* Module/class.scm 489 */
																						obj_t BgL_oclassz00_2851;

																						{	/* Module/class.scm 489 */
																							obj_t BgL_arg1815z00_2859;
																							long BgL_arg1816z00_2860;

																							BgL_arg1815z00_2859 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Module/class.scm 489 */
																								long BgL_arg1817z00_2861;

																								BgL_arg1817z00_2861 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2837);
																								BgL_arg1816z00_2860 =
																									(BgL_arg1817z00_2861 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2851 =
																								VECTOR_REF(BgL_arg1815z00_2859,
																								BgL_arg1816z00_2860);
																						}
																						{	/* Module/class.scm 489 */
																							bool_t BgL__ortest_1115z00_2852;

																							BgL__ortest_1115z00_2852 =
																								(BgL_classz00_2835 ==
																								BgL_oclassz00_2851);
																							if (BgL__ortest_1115z00_2852)
																								{	/* Module/class.scm 489 */
																									BgL_res2067z00_2868 =
																										BgL__ortest_1115z00_2852;
																								}
																							else
																								{	/* Module/class.scm 489 */
																									long BgL_odepthz00_2853;

																									{	/* Module/class.scm 489 */
																										obj_t BgL_arg1804z00_2854;

																										BgL_arg1804z00_2854 =
																											(BgL_oclassz00_2851);
																										BgL_odepthz00_2853 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2854);
																									}
																									if ((2L < BgL_odepthz00_2853))
																										{	/* Module/class.scm 489 */
																											obj_t BgL_arg1802z00_2856;

																											{	/* Module/class.scm 489 */
																												obj_t
																													BgL_arg1803z00_2857;
																												BgL_arg1803z00_2857 =
																													(BgL_oclassz00_2851);
																												BgL_arg1802z00_2856 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2857,
																													2L);
																											}
																											BgL_res2067z00_2868 =
																												(BgL_arg1802z00_2856 ==
																												BgL_classz00_2835);
																										}
																									else
																										{	/* Module/class.scm 489 */
																											BgL_res2067z00_2868 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2175z00_4346 =
																						BgL_res2067z00_2868;
																				}
																		}
																	else
																		{	/* Module/class.scm 489 */
																			BgL_test2175z00_4346 = ((bool_t) 0);
																		}
																}
																if (BgL_test2175z00_4346)
																	{	/* Module/class.scm 489 */
																		BgL_test2169z00_4322 = ((bool_t) 0);
																	}
																else
																	{	/* Module/class.scm 489 */
																		BgL_test2169z00_4322 = ((bool_t) 1);
																	}
															}
														else
															{	/* Module/class.scm 489 */
																BgL_test2169z00_4322 = ((bool_t) 0);
															}
													}
													if (BgL_test2169z00_4322)
														{	/* Module/class.scm 490 */
															bool_t BgL_test2180z00_4369;

															{	/* Module/class.scm 490 */
																obj_t BgL_classz00_2869;

																BgL_classz00_2869 = BGl_typez00zztype_typez00;
																if (BGL_OBJECTP(BgL_superz00_2135))
																	{	/* Module/class.scm 490 */
																		BgL_objectz00_bglt BgL_arg1807z00_2871;

																		BgL_arg1807z00_2871 =
																			(BgL_objectz00_bglt) (BgL_superz00_2135);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Module/class.scm 490 */
																				long BgL_idxz00_2877;

																				BgL_idxz00_2877 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2871);
																				BgL_test2180z00_4369 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2877 + 1L)) ==
																					BgL_classz00_2869);
																			}
																		else
																			{	/* Module/class.scm 490 */
																				bool_t BgL_res2068z00_2902;

																				{	/* Module/class.scm 490 */
																					obj_t BgL_oclassz00_2885;

																					{	/* Module/class.scm 490 */
																						obj_t BgL_arg1815z00_2893;
																						long BgL_arg1816z00_2894;

																						BgL_arg1815z00_2893 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Module/class.scm 490 */
																							long BgL_arg1817z00_2895;

																							BgL_arg1817z00_2895 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2871);
																							BgL_arg1816z00_2894 =
																								(BgL_arg1817z00_2895 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2885 =
																							VECTOR_REF(BgL_arg1815z00_2893,
																							BgL_arg1816z00_2894);
																					}
																					{	/* Module/class.scm 490 */
																						bool_t BgL__ortest_1115z00_2886;

																						BgL__ortest_1115z00_2886 =
																							(BgL_classz00_2869 ==
																							BgL_oclassz00_2885);
																						if (BgL__ortest_1115z00_2886)
																							{	/* Module/class.scm 490 */
																								BgL_res2068z00_2902 =
																									BgL__ortest_1115z00_2886;
																							}
																						else
																							{	/* Module/class.scm 490 */
																								long BgL_odepthz00_2887;

																								{	/* Module/class.scm 490 */
																									obj_t BgL_arg1804z00_2888;

																									BgL_arg1804z00_2888 =
																										(BgL_oclassz00_2885);
																									BgL_odepthz00_2887 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2888);
																								}
																								if ((1L < BgL_odepthz00_2887))
																									{	/* Module/class.scm 490 */
																										obj_t BgL_arg1802z00_2890;

																										{	/* Module/class.scm 490 */
																											obj_t BgL_arg1803z00_2891;

																											BgL_arg1803z00_2891 =
																												(BgL_oclassz00_2885);
																											BgL_arg1802z00_2890 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2891,
																												1L);
																										}
																										BgL_res2068z00_2902 =
																											(BgL_arg1802z00_2890 ==
																											BgL_classz00_2869);
																									}
																								else
																									{	/* Module/class.scm 490 */
																										BgL_res2068z00_2902 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2180z00_4369 =
																					BgL_res2068z00_2902;
																			}
																	}
																else
																	{	/* Module/class.scm 490 */
																		BgL_test2180z00_4369 = ((bool_t) 0);
																	}
															}
															if (BgL_test2180z00_4369)
																{	/* Module/class.scm 490 */
																	{
																		BgL_jclassz00_bglt BgL_auxz00_4392;

																		{
																			obj_t BgL_auxz00_4393;

																			{	/* Module/class.scm 494 */
																				BgL_objectz00_bglt BgL_tmpz00_4394;

																				BgL_tmpz00_4394 =
																					((BgL_objectz00_bglt)
																					((BgL_typez00_bglt)
																						BgL_classz00_2134));
																				BgL_auxz00_4393 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_4394);
																			}
																			BgL_auxz00_4392 =
																				((BgL_jclassz00_bglt) BgL_auxz00_4393);
																		}
																		((((BgL_jclassz00_bglt)
																					COBJECT(BgL_auxz00_4392))->
																				BgL_slotsz00) =
																			((obj_t) BNIL), BUNSPEC);
																	}
																	{	/* Module/class.scm 495 */
																		obj_t BgL_arg1958z00_2155;

																		BgL_arg1958z00_2155 =
																			CDR(((obj_t) BgL_curz00_2128));
																		{
																			obj_t BgL_curz00_4402;

																			BgL_curz00_4402 = BgL_arg1958z00_2155;
																			BgL_curz00_2128 = BgL_curz00_4402;
																			goto
																				BgL_zc3z04anonymousza31936ze3z87_2131;
																		}
																	}
																}
															else
																{	/* Module/class.scm 498 */
																	obj_t BgL_arg1959z00_2156;
																	obj_t BgL_arg1960z00_2157;

																	BgL_arg1959z00_2156 =
																		CDR(((obj_t) BgL_curz00_2128));
																	{	/* Module/class.scm 500 */
																		obj_t BgL_arg1961z00_2158;

																		{	/* Module/class.scm 500 */
																			obj_t BgL_arg1962z00_2159;

																			{	/* Module/class.scm 500 */
																				obj_t BgL_pairz00_2908;

																				BgL_pairz00_2908 =
																					CAR(((obj_t) BgL_curz00_2128));
																				BgL_arg1962z00_2159 =
																					CDR(BgL_pairz00_2908);
																			}
																			BgL_arg1961z00_2158 =
																				BGL_PROCEDURE_CALL0
																				(BgL_arg1962z00_2159);
																		}
																		BgL_arg1960z00_2157 =
																			BGl_appendzd221011zd2zzmodule_classz00
																			(BgL_arg1961z00_2158, BgL_accessz00_2130);
																	}
																	{
																		obj_t BgL_accessz00_4413;
																		obj_t BgL_curz00_4412;

																		BgL_curz00_4412 = BgL_arg1959z00_2156;
																		BgL_accessz00_4413 = BgL_arg1960z00_2157;
																		BgL_accessz00_2130 = BgL_accessz00_4413;
																		BgL_curz00_2128 = BgL_curz00_4412;
																		goto BgL_zc3z04anonymousza31936ze3z87_2131;
																	}
																}
														}
													else
														{	/* Module/class.scm 501 */
															bool_t BgL_test2185z00_4414;

															{	/* Module/class.scm 501 */
																bool_t BgL_test2186z00_4415;

																{	/* Module/class.scm 501 */
																	obj_t BgL_classz00_2909;

																	BgL_classz00_2909 =
																		BGl_tclassz00zzobject_classz00;
																	if (BGL_OBJECTP(BgL_classz00_2134))
																		{	/* Module/class.scm 501 */
																			BgL_objectz00_bglt BgL_arg1807z00_2911;

																			BgL_arg1807z00_2911 =
																				(BgL_objectz00_bglt)
																				(BgL_classz00_2134);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Module/class.scm 501 */
																					long BgL_idxz00_2917;

																					BgL_idxz00_2917 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2911);
																					BgL_test2186z00_4415 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2917 + 2L)) ==
																						BgL_classz00_2909);
																				}
																			else
																				{	/* Module/class.scm 501 */
																					bool_t BgL_res2069z00_2942;

																					{	/* Module/class.scm 501 */
																						obj_t BgL_oclassz00_2925;

																						{	/* Module/class.scm 501 */
																							obj_t BgL_arg1815z00_2933;
																							long BgL_arg1816z00_2934;

																							BgL_arg1815z00_2933 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Module/class.scm 501 */
																								long BgL_arg1817z00_2935;

																								BgL_arg1817z00_2935 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2911);
																								BgL_arg1816z00_2934 =
																									(BgL_arg1817z00_2935 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2925 =
																								VECTOR_REF(BgL_arg1815z00_2933,
																								BgL_arg1816z00_2934);
																						}
																						{	/* Module/class.scm 501 */
																							bool_t BgL__ortest_1115z00_2926;

																							BgL__ortest_1115z00_2926 =
																								(BgL_classz00_2909 ==
																								BgL_oclassz00_2925);
																							if (BgL__ortest_1115z00_2926)
																								{	/* Module/class.scm 501 */
																									BgL_res2069z00_2942 =
																										BgL__ortest_1115z00_2926;
																								}
																							else
																								{	/* Module/class.scm 501 */
																									long BgL_odepthz00_2927;

																									{	/* Module/class.scm 501 */
																										obj_t BgL_arg1804z00_2928;

																										BgL_arg1804z00_2928 =
																											(BgL_oclassz00_2925);
																										BgL_odepthz00_2927 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2928);
																									}
																									if ((2L < BgL_odepthz00_2927))
																										{	/* Module/class.scm 501 */
																											obj_t BgL_arg1802z00_2930;

																											{	/* Module/class.scm 501 */
																												obj_t
																													BgL_arg1803z00_2931;
																												BgL_arg1803z00_2931 =
																													(BgL_oclassz00_2925);
																												BgL_arg1802z00_2930 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2931,
																													2L);
																											}
																											BgL_res2069z00_2942 =
																												(BgL_arg1802z00_2930 ==
																												BgL_classz00_2909);
																										}
																									else
																										{	/* Module/class.scm 501 */
																											BgL_res2069z00_2942 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2186z00_4415 =
																						BgL_res2069z00_2942;
																				}
																		}
																	else
																		{	/* Module/class.scm 501 */
																			BgL_test2186z00_4415 = ((bool_t) 0);
																		}
																}
																if (BgL_test2186z00_4415)
																	{	/* Module/class.scm 501 */
																		obj_t BgL_arg1982z00_2181;

																		{
																			BgL_tclassz00_bglt BgL_auxz00_4438;

																			{
																				obj_t BgL_auxz00_4439;

																				{	/* Module/class.scm 501 */
																					BgL_objectz00_bglt BgL_tmpz00_4440;

																					BgL_tmpz00_4440 =
																						((BgL_objectz00_bglt)
																						((BgL_typez00_bglt)
																							BgL_superz00_2135));
																					BgL_auxz00_4439 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_4440);
																				}
																				BgL_auxz00_4438 =
																					((BgL_tclassz00_bglt)
																					BgL_auxz00_4439);
																			}
																			BgL_arg1982z00_2181 =
																				(((BgL_tclassz00_bglt)
																					COBJECT(BgL_auxz00_4438))->
																				BgL_slotsz00);
																		}
																		BgL_test2185z00_4414 =
																			(BgL_arg1982z00_2181 == BUNSPEC);
																	}
																else
																	{	/* Module/class.scm 501 */
																		BgL_test2185z00_4414 = ((bool_t) 0);
																	}
															}
															if (BgL_test2185z00_4414)
																{	/* Module/class.scm 503 */
																	obj_t BgL_arg1967z00_2164;
																	obj_t BgL_arg1968z00_2165;

																	BgL_arg1967z00_2164 =
																		CDR(((obj_t) BgL_curz00_2128));
																	{	/* Module/class.scm 504 */
																		obj_t BgL_arg1969z00_2166;

																		BgL_arg1969z00_2166 =
																			CAR(((obj_t) BgL_curz00_2128));
																		BgL_arg1968z00_2165 =
																			MAKE_YOUNG_PAIR(BgL_arg1969z00_2166,
																			BgL_nextz00_2129);
																	}
																	{
																		obj_t BgL_nextz00_4453;
																		obj_t BgL_curz00_4452;

																		BgL_curz00_4452 = BgL_arg1967z00_2164;
																		BgL_nextz00_4453 = BgL_arg1968z00_2165;
																		BgL_nextz00_2129 = BgL_nextz00_4453;
																		BgL_curz00_2128 = BgL_curz00_4452;
																		goto BgL_zc3z04anonymousza31936ze3z87_2131;
																	}
																}
															else
																{	/* Module/class.scm 506 */
																	bool_t BgL_test2191z00_4454;

																	{	/* Module/class.scm 506 */
																		bool_t BgL_test2192z00_4455;

																		{	/* Module/class.scm 506 */
																			obj_t BgL_classz00_2947;

																			BgL_classz00_2947 =
																				BGl_jclassz00zzobject_classz00;
																			if (BGL_OBJECTP(BgL_classz00_2134))
																				{	/* Module/class.scm 506 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2949;
																					BgL_arg1807z00_2949 =
																						(BgL_objectz00_bglt)
																						(BgL_classz00_2134);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Module/class.scm 506 */
																							long BgL_idxz00_2955;

																							BgL_idxz00_2955 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2949);
																							BgL_test2192z00_4455 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2955 + 2L)) ==
																								BgL_classz00_2947);
																						}
																					else
																						{	/* Module/class.scm 506 */
																							bool_t BgL_res2070z00_2980;

																							{	/* Module/class.scm 506 */
																								obj_t BgL_oclassz00_2963;

																								{	/* Module/class.scm 506 */
																									obj_t BgL_arg1815z00_2971;
																									long BgL_arg1816z00_2972;

																									BgL_arg1815z00_2971 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Module/class.scm 506 */
																										long BgL_arg1817z00_2973;

																										BgL_arg1817z00_2973 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2949);
																										BgL_arg1816z00_2972 =
																											(BgL_arg1817z00_2973 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2963 =
																										VECTOR_REF
																										(BgL_arg1815z00_2971,
																										BgL_arg1816z00_2972);
																								}
																								{	/* Module/class.scm 506 */
																									bool_t
																										BgL__ortest_1115z00_2964;
																									BgL__ortest_1115z00_2964 =
																										(BgL_classz00_2947 ==
																										BgL_oclassz00_2963);
																									if (BgL__ortest_1115z00_2964)
																										{	/* Module/class.scm 506 */
																											BgL_res2070z00_2980 =
																												BgL__ortest_1115z00_2964;
																										}
																									else
																										{	/* Module/class.scm 506 */
																											long BgL_odepthz00_2965;

																											{	/* Module/class.scm 506 */
																												obj_t
																													BgL_arg1804z00_2966;
																												BgL_arg1804z00_2966 =
																													(BgL_oclassz00_2963);
																												BgL_odepthz00_2965 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2966);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_2965))
																												{	/* Module/class.scm 506 */
																													obj_t
																														BgL_arg1802z00_2968;
																													{	/* Module/class.scm 506 */
																														obj_t
																															BgL_arg1803z00_2969;
																														BgL_arg1803z00_2969
																															=
																															(BgL_oclassz00_2963);
																														BgL_arg1802z00_2968
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2969,
																															2L);
																													}
																													BgL_res2070z00_2980 =
																														(BgL_arg1802z00_2968
																														==
																														BgL_classz00_2947);
																												}
																											else
																												{	/* Module/class.scm 506 */
																													BgL_res2070z00_2980 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2192z00_4455 =
																								BgL_res2070z00_2980;
																						}
																				}
																			else
																				{	/* Module/class.scm 506 */
																					BgL_test2192z00_4455 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test2192z00_4455)
																			{	/* Module/class.scm 506 */
																				obj_t BgL_arg1981z00_2179;

																				{
																					BgL_jclassz00_bglt BgL_auxz00_4478;

																					{
																						obj_t BgL_auxz00_4479;

																						{	/* Module/class.scm 506 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_4480;
																							BgL_tmpz00_4480 =
																								((BgL_objectz00_bglt) (
																									(BgL_typez00_bglt)
																									BgL_superz00_2135));
																							BgL_auxz00_4479 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_4480);
																						}
																						BgL_auxz00_4478 =
																							((BgL_jclassz00_bglt)
																							BgL_auxz00_4479);
																					}
																					BgL_arg1981z00_2179 =
																						(((BgL_jclassz00_bglt)
																							COBJECT(BgL_auxz00_4478))->
																						BgL_slotsz00);
																				}
																				BgL_test2191z00_4454 =
																					(BgL_arg1981z00_2179 == BUNSPEC);
																			}
																		else
																			{	/* Module/class.scm 506 */
																				BgL_test2191z00_4454 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2191z00_4454)
																		{	/* Module/class.scm 508 */
																			obj_t BgL_arg1973z00_2170;
																			obj_t BgL_arg1974z00_2171;

																			BgL_arg1973z00_2170 =
																				CDR(((obj_t) BgL_curz00_2128));
																			{	/* Module/class.scm 509 */
																				obj_t BgL_arg1975z00_2172;

																				BgL_arg1975z00_2172 =
																					CAR(((obj_t) BgL_curz00_2128));
																				BgL_arg1974z00_2171 =
																					MAKE_YOUNG_PAIR(BgL_arg1975z00_2172,
																					BgL_nextz00_2129);
																			}
																			{
																				obj_t BgL_nextz00_4493;
																				obj_t BgL_curz00_4492;

																				BgL_curz00_4492 = BgL_arg1973z00_2170;
																				BgL_nextz00_4493 = BgL_arg1974z00_2171;
																				BgL_nextz00_2129 = BgL_nextz00_4493;
																				BgL_curz00_2128 = BgL_curz00_4492;
																				goto
																					BgL_zc3z04anonymousza31936ze3z87_2131;
																			}
																		}
																	else
																		{	/* Module/class.scm 513 */
																			obj_t BgL_arg1976z00_2173;
																			obj_t BgL_arg1977z00_2174;

																			BgL_arg1976z00_2173 =
																				CDR(((obj_t) BgL_curz00_2128));
																			{	/* Module/class.scm 515 */
																				obj_t BgL_arg1978z00_2175;

																				{	/* Module/class.scm 515 */
																					obj_t BgL_arg1979z00_2176;

																					{	/* Module/class.scm 515 */
																						obj_t BgL_pairz00_2987;

																						BgL_pairz00_2987 =
																							CAR(((obj_t) BgL_curz00_2128));
																						BgL_arg1979z00_2176 =
																							CDR(BgL_pairz00_2987);
																					}
																					BgL_arg1978z00_2175 =
																						BGL_PROCEDURE_CALL0
																						(BgL_arg1979z00_2176);
																				}
																				BgL_arg1977z00_2174 =
																					BGl_appendzd221011zd2zzmodule_classz00
																					(BgL_arg1978z00_2175,
																					BgL_accessz00_2130);
																			}
																			{
																				obj_t BgL_accessz00_4504;
																				obj_t BgL_curz00_4503;

																				BgL_curz00_4503 = BgL_arg1976z00_2173;
																				BgL_accessz00_4504 =
																					BgL_arg1977z00_2174;
																				BgL_accessz00_2130 = BgL_accessz00_4504;
																				BgL_curz00_2128 = BgL_curz00_4503;
																				goto
																					BgL_zc3z04anonymousza31936ze3z87_2131;
																			}
																		}
																}
														}
												}
										}
								}
							}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_classz00(void)
	{
		{	/* Module/class.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_classz00(void)
	{
		{	/* Module/class.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_classz00(void)
	{
		{	/* Module/class.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_classz00(void)
	{
		{	/* Module/class.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzmodule_impusez00(478324304L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzast_objectz00(520121776L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzobject_coercionz00(208160543L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			BGl_modulezd2initializa7ationz75zzobject_classgenz00(502726840L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
			return
				BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2076z00zzmodule_classz00));
		}

	}

#ifdef __cplusplus
}
#endif
