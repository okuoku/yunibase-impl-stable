/*===========================================================================*/
/*   (Module/eval.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/eval.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_EVAL_TYPE_DEFINITIONS
#define BGL_MODULE_EVAL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;


#endif													// BGL_MODULE_EVAL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62makezd2evalzd2compilerz62zzmodule_evalz00(obj_t);
	static obj_t BGl_setzd2evalzd2typesz12z12zzmodule_evalz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_evalz00 = BUNSPEC;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62evalzd2finaliza7erz17zzmodule_evalz00(obj_t);
	static obj_t BGl_library_ez00zzmodule_evalz00(obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t BGl_addzd2evalzd2libraryz12z12zzmodule_evalz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzmodule_evalz00(void);
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t bigloo_module_mangle(obj_t, obj_t);
	extern obj_t BGl_getzd2toplevelzd2unitzd2weightzd2zzmodule_includez00(void);
	static obj_t BGl_evmodulezd2compzd2zzmodule_evalz00(void);
	static obj_t BGl_genericzd2initzd2zzmodule_evalz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_za2evalzd2librariesza2zd2zzmodule_evalz00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zzmodule_evalz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_rememberzd2evalzd2exportedz12z12zzmodule_evalz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t BGl_libraryzd2initzd2filez00zz__libraryz00(obj_t);
	static obj_t BGl_za2evalzd2classesza2zd2zzmodule_evalz00 = BUNSPEC;
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	extern BgL_typez00_bglt BGl_findzd2typezf2locationz20zztype_envz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzmodule_evalz00(obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_definezd2primopzd2refzf2srczd2ze3nodezc3zzast_sexpz00
		(BgL_globalz00_bglt, BgL_nodez00_bglt, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_evalz00(void);
	BGL_IMPORT obj_t BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31340ze3ze5zzmodule_evalz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_libraryzd2infozd2zz__libraryz00(obj_t);
	static obj_t BGl_getzd2evalzd2srfizd2librarieszd2zzmodule_evalz00(void);
	static obj_t BGl_z62getzd2evalzd2librariesz62zzmodule_evalz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2allzd2exportzd2mutablezf3za2zf3zzmodule_evalz00
		= BUNSPEC;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2evalzd2compilerz00zzmodule_evalz00(void);
	static obj_t BGl_getzd2evaluatedzd2classzd2holderszd2zzmodule_evalz00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_svarz00zzast_varz00;
	static obj_t BGl_z62addzd2evalzd2libraryz12z70zzmodule_evalz00(obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern BgL_nodez00_bglt
		BGl_definezd2primopzd2ze3nodeze3zzast_sexpz00(BgL_globalz00_bglt);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern BgL_nodez00_bglt
		BGl_locationzd2ze3nodez31zzast_sexpz00(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzmodule_evalz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__libraryz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static bool_t BGl_za2onezd2evalzf3za2z21zzmodule_evalz00;
	static obj_t BGl_getzd2evaluatedzd2globalsz00zzmodule_evalz00(obj_t);
	static obj_t BGl_z62evalzd2producerzb0zzmodule_evalz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2allzd2exportzd2evalzf3za2zf3zzmodule_evalz00 =
		BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzmodule_evalz00(obj_t);
	BGL_IMPORT obj_t BGl_loadqz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t create_struct(obj_t, int);
	BGL_EXPORTED_DEF obj_t BGl_za2allzd2modulezd2evalzf3za2zf3zzmodule_evalz00 =
		BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzmodule_evalz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_evalz00(void);
	BGL_EXPORTED_DEF obj_t BGl_za2allzd2evalzf3za2z21zzmodule_evalz00 = BUNSPEC;
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_evalz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_evalz00(void);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2modulezd2locationza2zd2zzmodule_modulez00;
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_definezd2primopzd2refzd2ze3nodez31zzast_sexpz00(BgL_globalz00_bglt,
		BgL_nodez00_bglt);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_evalzd2parserzd2zzmodule_evalz00(obj_t, obj_t);
	static obj_t BGl_za2evalzd2exportedza2zd2zzmodule_evalz00 = BUNSPEC;
	static obj_t BGl_getzd2libraryzd2infoz00zzmodule_evalz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_getzd2evalzd2librariesz00zzmodule_evalz00(void);
	static obj_t BGl_exportzd2globalze70z35zzmodule_evalz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31921ze3ze5zzmodule_evalz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_getzd2evaluatedzd2classzd2macroszd2zzmodule_evalz00(void);
	extern obj_t BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_IMPORT obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t __cnst[39];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_evalzd2producerzd2envz00zzmodule_evalz00,
		BgL_bgl_za762evalza7d2produc2100z00,
		BGl_z62evalzd2producerzb0zzmodule_evalz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2085z00zzmodule_evalz00,
		BgL_bgl_string2085za700za7za7m2101za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string2086z00zzmodule_evalz00,
		BgL_bgl_string2086za700za7za7m2102za7, "Illegal `eval' clause", 21);
	      DEFINE_STRING(BGl_string2088z00zzmodule_evalz00,
		BgL_bgl_string2088za700za7za7m2103za7, "cannot find info \"~a\"", 21);
	      DEFINE_STRING(BGl_string2089z00zzmodule_evalz00,
		BgL_bgl_string2089za700za7za7m2104za7,
		"cannot find file \"~a\" in lib path", 33);
	      DEFINE_STRING(BGl_string2090z00zzmodule_evalz00,
		BgL_bgl_string2090za700za7za7m2105za7, "~s", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2084z00zzmodule_evalz00,
		BgL_bgl_za762za7c3za704anonymo2106za7,
		BGl_z62zc3z04anonymousza31340ze3ze5zzmodule_evalz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2091z00zzmodule_evalz00,
		BgL_bgl_string2091za700za7za7m2107za7, "cannot initialize library for eval",
		34);
	      DEFINE_STRING(BGl_string2092z00zzmodule_evalz00,
		BgL_bgl_string2092za700za7za7m2108za7, "eval", 4);
	      DEFINE_STRING(BGl_string2093z00zzmodule_evalz00,
		BgL_bgl_string2093za700za7za7m2109za7,
		"Non bigloo prototyped value can't be evaluated", 46);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2087z00zzmodule_evalz00,
		BgL_bgl_za762za7c3za704anonymo2110za7,
		BGl_z62zc3z04anonymousza31616ze3ze5zzmodule_evalz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2094z00zzmodule_evalz00,
		BgL_bgl_string2094za700za7za7m2111za7, "eval-init", 9);
	      DEFINE_STRING(BGl_string2095z00zzmodule_evalz00,
		BgL_bgl_string2095za700za7za7m2112za7,
		"This variable cannot be known by eval", 37);
	      DEFINE_STRING(BGl_string2096z00zzmodule_evalz00,
		BgL_bgl_string2096za700za7za7m2113za7, "Unbound eval variable", 21);
	      DEFINE_STRING(BGl_string2097z00zzmodule_evalz00,
		BgL_bgl_string2097za700za7za7m2114za7, "module_eval", 11);
	      DEFINE_STRING(BGl_string2098z00zzmodule_evalz00,
		BgL_bgl_string2098za700za7za7m2115za7,
		"eval-expand-with-access eval-expand-duplicate eval-expand-instantiate register-eval-srfi! (export) (import static export) (all module export) library-load library-mark-loaded! pragma::string bigloo-library-path library-load-init __library (int string) obj libinfo vector __evmeaning_address value module quote evmodule-comp! __evmodule begin (#unspecified) one all write void unit library import class @ export export-module export-exports export-all eval ",
		456);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2evalzd2libraryz12zd2envzc0zzmodule_evalz00,
		BgL_bgl_za762addza7d2evalza7d22116za7,
		BGl_z62addzd2evalzd2libraryz12z70zzmodule_evalz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_evalzd2finaliza7erzd2envza7zzmodule_evalz00,
		BgL_bgl_za762evalza7d2finali2117z00,
		BGl_z62evalzd2finaliza7erz17zzmodule_evalz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2evalzd2librarieszd2envzd2zzmodule_evalz00,
		BgL_bgl_za762getza7d2evalza7d22118za7,
		BGl_z62getzd2evalzd2librariesz62zzmodule_evalz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2evalzd2compilerzd2envzd2zzmodule_evalz00,
		BgL_bgl_za762makeza7d2evalza7d2119za7,
		BGl_z62makezd2evalzd2compilerz62zzmodule_evalz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzmodule_evalz00));
		     ADD_ROOT((void *) (&BGl_za2evalzd2librariesza2zd2zzmodule_evalz00));
		     ADD_ROOT((void *) (&BGl_za2evalzd2classesza2zd2zzmodule_evalz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2allzd2exportzd2mutablezf3za2zf3zzmodule_evalz00));
		     ADD_ROOT((void
				*) (&BGl_za2allzd2exportzd2evalzf3za2zf3zzmodule_evalz00));
		     ADD_ROOT((void
				*) (&BGl_za2allzd2modulezd2evalzf3za2zf3zzmodule_evalz00));
		     ADD_ROOT((void *) (&BGl_za2allzd2evalzf3za2z21zzmodule_evalz00));
		     ADD_ROOT((void *) (&BGl_za2evalzd2exportedza2zd2zzmodule_evalz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_evalz00(long
		BgL_checksumz00_3125, char *BgL_fromz00_3126)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_evalz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_evalz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_evalz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_evalz00();
					BGl_cnstzd2initzd2zzmodule_evalz00();
					BGl_importedzd2moduleszd2initz00zzmodule_evalz00();
					return BGl_toplevelzd2initzd2zzmodule_evalz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_eval");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"module_eval");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__libraryz00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"module_eval");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_eval");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_eval");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "module_eval");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "module_eval");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 15 */
			{	/* Module/eval.scm 15 */
				obj_t BgL_cportz00_3027;

				{	/* Module/eval.scm 15 */
					obj_t BgL_stringz00_3034;

					BgL_stringz00_3034 = BGl_string2098z00zzmodule_evalz00;
					{	/* Module/eval.scm 15 */
						obj_t BgL_startz00_3035;

						BgL_startz00_3035 = BINT(0L);
						{	/* Module/eval.scm 15 */
							obj_t BgL_endz00_3036;

							BgL_endz00_3036 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3034)));
							{	/* Module/eval.scm 15 */

								BgL_cportz00_3027 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3034, BgL_startz00_3035, BgL_endz00_3036);
				}}}}
				{
					long BgL_iz00_3028;

					BgL_iz00_3028 = 38L;
				BgL_loopz00_3029:
					if ((BgL_iz00_3028 == -1L))
						{	/* Module/eval.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/eval.scm 15 */
							{	/* Module/eval.scm 15 */
								obj_t BgL_arg2099z00_3030;

								{	/* Module/eval.scm 15 */

									{	/* Module/eval.scm 15 */
										obj_t BgL_locationz00_3032;

										BgL_locationz00_3032 = BBOOL(((bool_t) 0));
										{	/* Module/eval.scm 15 */

											BgL_arg2099z00_3030 =
												BGl_readz00zz__readerz00(BgL_cportz00_3027,
												BgL_locationz00_3032);
										}
									}
								}
								{	/* Module/eval.scm 15 */
									int BgL_tmpz00_3161;

									BgL_tmpz00_3161 = (int) (BgL_iz00_3028);
									CNST_TABLE_SET(BgL_tmpz00_3161, BgL_arg2099z00_3030);
							}}
							{	/* Module/eval.scm 15 */
								int BgL_auxz00_3033;

								BgL_auxz00_3033 = (int) ((BgL_iz00_3028 - 1L));
								{
									long BgL_iz00_3166;

									BgL_iz00_3166 = (long) (BgL_auxz00_3033);
									BgL_iz00_3028 = BgL_iz00_3166;
									goto BgL_loopz00_3029;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 15 */
			BGl_za2evalzd2librariesza2zd2zzmodule_evalz00 = BNIL;
			BGl_za2evalzd2exportedza2zd2zzmodule_evalz00 = BNIL;
			BGl_za2evalzd2classesza2zd2zzmodule_evalz00 = BNIL;
			BGl_za2allzd2evalzf3za2z21zzmodule_evalz00 = BFALSE;
			BGl_za2allzd2exportzd2evalzf3za2zf3zzmodule_evalz00 = BFALSE;
			BGl_za2allzd2modulezd2evalzf3za2zf3zzmodule_evalz00 = BFALSE;
			BGl_za2allzd2exportzd2mutablezf3za2zf3zzmodule_evalz00 = BFALSE;
			return (BGl_za2onezd2evalzf3za2z21zzmodule_evalz00 =
				((bool_t) 0), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzmodule_evalz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1791;

				BgL_headz00_1791 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1792;
					obj_t BgL_tailz00_1793;

					BgL_prevz00_1792 = BgL_headz00_1791;
					BgL_tailz00_1793 = BgL_l1z00_1;
				BgL_loopz00_1794:
					if (PAIRP(BgL_tailz00_1793))
						{
							obj_t BgL_newzd2prevzd2_1796;

							BgL_newzd2prevzd2_1796 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1793), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1792, BgL_newzd2prevzd2_1796);
							{
								obj_t BgL_tailz00_3176;
								obj_t BgL_prevz00_3175;

								BgL_prevz00_3175 = BgL_newzd2prevzd2_1796;
								BgL_tailz00_3176 = CDR(BgL_tailz00_1793);
								BgL_tailz00_1793 = BgL_tailz00_3176;
								BgL_prevz00_1792 = BgL_prevz00_3175;
								goto BgL_loopz00_1794;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1791);
				}
			}
		}

	}



/* make-eval-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2evalzd2compilerz00zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 45 */
			{	/* Module/eval.scm 46 */
				BgL_ccompz00_bglt BgL_new1109z00_1834;

				{	/* Module/eval.scm 47 */
					BgL_ccompz00_bglt BgL_new1108z00_1838;

					BgL_new1108z00_1838 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/eval.scm 47 */
						long BgL_arg1342z00_1839;

						BgL_arg1342z00_1839 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1108z00_1838), BgL_arg1342z00_1839);
					}
					BgL_new1109z00_1834 = BgL_new1108z00_1838;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1109z00_1834))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1109z00_1834))->BgL_producerz00) =
					((obj_t) BGl_evalzd2producerzd2envz00zzmodule_evalz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1109z00_1834))->BgL_consumerz00) =
					((obj_t) BGl_proc2084z00zzmodule_evalz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1109z00_1834))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_evalzd2finaliza7erzd2envza7zzmodule_evalz00), BUNSPEC);
				return ((obj_t) BgL_new1109z00_1834);
			}
		}

	}



/* &make-eval-compiler */
	obj_t BGl_z62makezd2evalzd2compilerz62zzmodule_evalz00(obj_t BgL_envz00_3005)
	{
		{	/* Module/eval.scm 45 */
			return BGl_makezd2evalzd2compilerz00zzmodule_evalz00();
		}

	}



/* &<@anonymous:1340> */
	obj_t BGl_z62zc3z04anonymousza31340ze3ze5zzmodule_evalz00(obj_t
		BgL_envz00_3006, obj_t BgL_mz00_3007, obj_t BgL_cz00_3008)
	{
		{	/* Module/module.scm 54 */
			return BNIL;
		}

	}



/* &eval-producer */
	obj_t BGl_z62evalzd2producerzb0zzmodule_evalz00(obj_t BgL_envz00_3009,
		obj_t BgL_clausez00_3010)
	{
		{	/* Module/eval.scm 54 */
			{
				obj_t BgL_protosz00_3040;

				if (PAIRP(BgL_clausez00_3010))
					{	/* Module/eval.scm 55 */
						obj_t BgL_arg1346z00_3044;

						BgL_arg1346z00_3044 = CDR(((obj_t) BgL_clausez00_3010));
						BgL_protosz00_3040 = BgL_arg1346z00_3044;
						{
							obj_t BgL_l1245z00_3042;

							BgL_l1245z00_3042 = BgL_protosz00_3040;
						BgL_zc3z04anonymousza31347ze3z87_3041:
							if (PAIRP(BgL_l1245z00_3042))
								{	/* Module/eval.scm 57 */
									BGl_evalzd2parserzd2zzmodule_evalz00(CAR(BgL_l1245z00_3042),
										BgL_clausez00_3010);
									{
										obj_t BgL_l1245z00_3198;

										BgL_l1245z00_3198 = CDR(BgL_l1245z00_3042);
										BgL_l1245z00_3042 = BgL_l1245z00_3198;
										goto BgL_zc3z04anonymousza31347ze3z87_3041;
									}
								}
							else
								{	/* Module/eval.scm 57 */
									((bool_t) 1);
								}
						}
						return BNIL;
					}
				else
					{	/* Module/eval.scm 55 */
						{	/* Module/eval.scm 60 */
							obj_t BgL_list1350z00_3043;

							BgL_list1350z00_3043 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							return
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string2085z00zzmodule_evalz00,
								BGl_string2086z00zzmodule_evalz00, BgL_clausez00_3010,
								BgL_list1350z00_3043);
						}
					}
			}
		}

	}



/* eval-parser */
	obj_t BGl_evalzd2parserzd2zzmodule_evalz00(obj_t BgL_protoz00_76,
		obj_t BgL_clausez00_77)
	{
		{	/* Module/eval.scm 68 */
			{
				obj_t BgL_libsz00_1866;

				if (PAIRP(BgL_protoz00_76))
					{	/* Module/eval.scm 69 */
						if ((CAR(((obj_t) BgL_protoz00_76)) == CNST_TABLE_REF(1)))
							{	/* Module/eval.scm 69 */
								if (NULLP(CDR(((obj_t) BgL_protoz00_76))))
									{	/* Module/eval.scm 69 */
										return (BGl_za2allzd2evalzf3za2z21zzmodule_evalz00 =
											BTRUE, BUNSPEC);
									}
								else
									{	/* Module/eval.scm 69 */
									BgL_tagzd2117zd2_1868:
										{	/* Module/eval.scm 95 */
											obj_t BgL_list1592z00_1952;

											BgL_list1592z00_1952 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											return
												BGl_userzd2errorzd2zztools_errorz00
												(BGl_string2085z00zzmodule_evalz00,
												BGl_string2086z00zzmodule_evalz00, BgL_clausez00_77,
												BgL_list1592z00_1952);
										}
									}
							}
						else
							{	/* Module/eval.scm 69 */
								if ((CAR(((obj_t) BgL_protoz00_76)) == CNST_TABLE_REF(2)))
									{	/* Module/eval.scm 69 */
										if (NULLP(CDR(((obj_t) BgL_protoz00_76))))
											{	/* Module/eval.scm 69 */
												return
													(BGl_za2allzd2exportzd2evalzf3za2zf3zzmodule_evalz00 =
													BTRUE, BUNSPEC);
											}
										else
											{	/* Module/eval.scm 69 */
												goto BgL_tagzd2117zd2_1868;
											}
									}
								else
									{	/* Module/eval.scm 69 */
										if ((CAR(((obj_t) BgL_protoz00_76)) == CNST_TABLE_REF(3)))
											{	/* Module/eval.scm 69 */
												if (NULLP(CDR(((obj_t) BgL_protoz00_76))))
													{	/* Module/eval.scm 69 */
														return
															(BGl_za2allzd2modulezd2evalzf3za2zf3zzmodule_evalz00
															= BTRUE, BUNSPEC);
													}
												else
													{	/* Module/eval.scm 69 */
														goto BgL_tagzd2117zd2_1868;
													}
											}
										else
											{	/* Module/eval.scm 69 */
												obj_t BgL_cdrzd2215zd2_1886;

												BgL_cdrzd2215zd2_1886 = CDR(((obj_t) BgL_protoz00_76));
												if (
													(CAR(((obj_t) BgL_protoz00_76)) == CNST_TABLE_REF(4)))
													{	/* Module/eval.scm 69 */
														if (PAIRP(BgL_cdrzd2215zd2_1886))
															{	/* Module/eval.scm 69 */
																obj_t BgL_carzd2217zd2_1890;

																BgL_carzd2217zd2_1890 =
																	CAR(BgL_cdrzd2215zd2_1886);
																if (SYMBOLP(BgL_carzd2217zd2_1890))
																	{	/* Module/eval.scm 69 */
																		if (NULLP(CDR(BgL_cdrzd2215zd2_1886)))
																			{	/* Module/eval.scm 69 */
																				BGl_za2onezd2evalzf3za2z21zzmodule_evalz00
																					= ((bool_t) 1);
																				return
																					BGl_rememberzd2evalzd2exportedz12z12zzmodule_evalz00
																					(BgL_carzd2217zd2_1890, BFALSE,
																					BgL_protoz00_76);
																			}
																		else
																			{	/* Module/eval.scm 69 */
																				goto BgL_tagzd2117zd2_1868;
																			}
																	}
																else
																	{	/* Module/eval.scm 69 */
																		obj_t BgL_carzd2247zd2_1896;

																		BgL_carzd2247zd2_1896 =
																			CAR(((obj_t) BgL_cdrzd2215zd2_1886));
																		if (PAIRP(BgL_carzd2247zd2_1896))
																			{	/* Module/eval.scm 69 */
																				obj_t BgL_cdrzd2252zd2_1898;

																				BgL_cdrzd2252zd2_1898 =
																					CDR(BgL_carzd2247zd2_1896);
																				if (
																					(CAR(BgL_carzd2247zd2_1896) ==
																						CNST_TABLE_REF(5)))
																					{	/* Module/eval.scm 69 */
																						if (PAIRP(BgL_cdrzd2252zd2_1898))
																							{	/* Module/eval.scm 69 */
																								obj_t BgL_carzd2255zd2_1902;
																								obj_t BgL_cdrzd2256zd2_1903;

																								BgL_carzd2255zd2_1902 =
																									CAR(BgL_cdrzd2252zd2_1898);
																								BgL_cdrzd2256zd2_1903 =
																									CDR(BgL_cdrzd2252zd2_1898);
																								if (SYMBOLP
																									(BgL_carzd2255zd2_1902))
																									{	/* Module/eval.scm 69 */
																										if (PAIRP
																											(BgL_cdrzd2256zd2_1903))
																											{	/* Module/eval.scm 69 */
																												obj_t
																													BgL_carzd2261zd2_1906;
																												BgL_carzd2261zd2_1906 =
																													CAR
																													(BgL_cdrzd2256zd2_1903);
																												if (SYMBOLP
																													(BgL_carzd2261zd2_1906))
																													{	/* Module/eval.scm 69 */
																														if (NULLP(CDR
																																(BgL_cdrzd2256zd2_1903)))
																															{	/* Module/eval.scm 69 */
																																if (NULLP(CDR(
																																			((obj_t)
																																				BgL_cdrzd2215zd2_1886))))
																																	{	/* Module/eval.scm 69 */
																																		BGl_za2onezd2evalzf3za2z21zzmodule_evalz00
																																			=
																																			((bool_t)
																																			1);
																																		return
																																			BGl_rememberzd2evalzd2exportedz12z12zzmodule_evalz00
																																			(BgL_carzd2255zd2_1902,
																																			BgL_carzd2261zd2_1906,
																																			BgL_protoz00_76);
																																	}
																																else
																																	{	/* Module/eval.scm 69 */
																																		goto
																																			BgL_tagzd2117zd2_1868;
																																	}
																															}
																														else
																															{	/* Module/eval.scm 69 */
																																goto
																																	BgL_tagzd2117zd2_1868;
																															}
																													}
																												else
																													{	/* Module/eval.scm 69 */
																														goto
																															BgL_tagzd2117zd2_1868;
																													}
																											}
																										else
																											{	/* Module/eval.scm 69 */
																												goto
																													BgL_tagzd2117zd2_1868;
																											}
																									}
																								else
																									{	/* Module/eval.scm 69 */
																										goto BgL_tagzd2117zd2_1868;
																									}
																							}
																						else
																							{	/* Module/eval.scm 69 */
																								goto BgL_tagzd2117zd2_1868;
																							}
																					}
																				else
																					{	/* Module/eval.scm 69 */
																						goto BgL_tagzd2117zd2_1868;
																					}
																			}
																		else
																			{	/* Module/eval.scm 69 */
																				goto BgL_tagzd2117zd2_1868;
																			}
																	}
															}
														else
															{	/* Module/eval.scm 69 */
																goto BgL_tagzd2117zd2_1868;
															}
													}
												else
													{	/* Module/eval.scm 69 */
														if (
															(CAR(
																	((obj_t) BgL_protoz00_76)) ==
																CNST_TABLE_REF(6)))
															{	/* Module/eval.scm 69 */
																if (PAIRP(BgL_cdrzd2215zd2_1886))
																	{	/* Module/eval.scm 69 */
																		obj_t BgL_carzd2366zd2_1919;

																		BgL_carzd2366zd2_1919 =
																			CAR(BgL_cdrzd2215zd2_1886);
																		if (SYMBOLP(BgL_carzd2366zd2_1919))
																			{	/* Module/eval.scm 69 */
																				if (NULLP(CDR(BgL_cdrzd2215zd2_1886)))
																					{	/* Module/eval.scm 69 */
																						return
																							(BGl_za2evalzd2classesza2zd2zzmodule_evalz00
																							=
																							MAKE_YOUNG_PAIR(BgL_protoz00_76,
																								BGl_za2evalzd2classesza2zd2zzmodule_evalz00),
																							BUNSPEC);
																					}
																				else
																					{	/* Module/eval.scm 69 */
																						goto BgL_tagzd2117zd2_1868;
																					}
																			}
																		else
																			{	/* Module/eval.scm 69 */
																				goto BgL_tagzd2117zd2_1868;
																			}
																	}
																else
																	{	/* Module/eval.scm 69 */
																		goto BgL_tagzd2117zd2_1868;
																	}
															}
														else
															{	/* Module/eval.scm 69 */
																obj_t BgL_cdrzd2390zd2_1924;

																BgL_cdrzd2390zd2_1924 =
																	CDR(((obj_t) BgL_protoz00_76));
																if (
																	(CAR(
																			((obj_t) BgL_protoz00_76)) ==
																		CNST_TABLE_REF(7)))
																	{	/* Module/eval.scm 69 */
																		if (PAIRP(BgL_cdrzd2390zd2_1924))
																			{	/* Module/eval.scm 69 */
																				obj_t BgL_carzd2392zd2_1928;

																				BgL_carzd2392zd2_1928 =
																					CAR(BgL_cdrzd2390zd2_1924);
																				if (SYMBOLP(BgL_carzd2392zd2_1928))
																					{	/* Module/eval.scm 69 */
																						if (NULLP(CDR
																								(BgL_cdrzd2390zd2_1924)))
																							{	/* Module/eval.scm 69 */
																								return
																									((obj_t)
																									BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2
																									(BgL_carzd2392zd2_1928,
																										BFALSE, CNST_TABLE_REF(0),
																										CNST_TABLE_REF(0),
																										BgL_clausez00_77, BFALSE));
																							}
																						else
																							{	/* Module/eval.scm 69 */
																								goto BgL_tagzd2117zd2_1868;
																							}
																					}
																				else
																					{	/* Module/eval.scm 69 */
																						goto BgL_tagzd2117zd2_1868;
																					}
																			}
																		else
																			{	/* Module/eval.scm 69 */
																				goto BgL_tagzd2117zd2_1868;
																			}
																	}
																else
																	{	/* Module/eval.scm 69 */
																		if (
																			(CAR(
																					((obj_t) BgL_protoz00_76)) ==
																				CNST_TABLE_REF(8)))
																			{	/* Module/eval.scm 69 */
																				bool_t BgL_tmpz00_3315;

																				BgL_libsz00_1866 =
																					BgL_cdrzd2390zd2_1924;
																				{
																					obj_t BgL_l1247z00_1944;

																					BgL_l1247z00_1944 = BgL_libsz00_1866;
																				BgL_zc3z04anonymousza31586ze3z87_1945:
																					if (PAIRP(BgL_l1247z00_1944))
																						{	/* Module/eval.scm 87 */
																							{	/* Module/eval.scm 88 */
																								obj_t BgL_libz00_1947;

																								BgL_libz00_1947 =
																									CAR(BgL_l1247z00_1944);
																								if (SYMBOLP(BgL_libz00_1947))
																									{	/* Module/eval.scm 88 */
																										BGl_za2evalzd2librariesza2zd2zzmodule_evalz00
																											=
																											MAKE_YOUNG_PAIR
																											(BgL_libz00_1947,
																											BGl_za2evalzd2librariesza2zd2zzmodule_evalz00);
																									}
																								else
																									{	/* Module/eval.scm 89 */
																										obj_t BgL_list1589z00_1949;

																										BgL_list1589z00_1949 =
																											MAKE_YOUNG_PAIR(BNIL,
																											BNIL);
																										BGl_userzd2errorzd2zztools_errorz00
																											(BGl_string2085z00zzmodule_evalz00,
																											BGl_string2086z00zzmodule_evalz00,
																											BgL_clausez00_77,
																											BgL_list1589z00_1949);
																									}
																							}
																							{
																								obj_t BgL_l1247z00_3324;

																								BgL_l1247z00_3324 =
																									CDR(BgL_l1247z00_1944);
																								BgL_l1247z00_1944 =
																									BgL_l1247z00_3324;
																								goto
																									BgL_zc3z04anonymousza31586ze3z87_1945;
																							}
																						}
																					else
																						{	/* Module/eval.scm 87 */
																							BgL_tmpz00_3315 = ((bool_t) 1);
																						}
																				}
																				return BBOOL(BgL_tmpz00_3315);
																			}
																		else
																			{	/* Module/eval.scm 69 */
																				goto BgL_tagzd2117zd2_1868;
																			}
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Module/eval.scm 69 */
						goto BgL_tagzd2117zd2_1868;
					}
			}
		}

	}



/* get-eval-libraries */
	BGL_EXPORTED_DEF obj_t BGl_getzd2evalzd2librariesz00zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 105 */
			return BGl_za2evalzd2librariesza2zd2zzmodule_evalz00;
		}

	}



/* &get-eval-libraries */
	obj_t BGl_z62getzd2evalzd2librariesz62zzmodule_evalz00(obj_t BgL_envz00_3012)
	{
		{	/* Module/eval.scm 105 */
			return BGl_getzd2evalzd2librariesz00zzmodule_evalz00();
		}

	}



/* add-eval-library! */
	BGL_EXPORTED_DEF obj_t BGl_addzd2evalzd2libraryz12z12zzmodule_evalz00(obj_t
		BgL_libz00_78)
	{
		{	/* Module/eval.scm 111 */
			return (BGl_za2evalzd2librariesza2zd2zzmodule_evalz00 =
				MAKE_YOUNG_PAIR(BgL_libz00_78,
					BGl_za2evalzd2librariesza2zd2zzmodule_evalz00), BUNSPEC);
		}

	}



/* &add-eval-library! */
	obj_t BGl_z62addzd2evalzd2libraryz12z70zzmodule_evalz00(obj_t BgL_envz00_3013,
		obj_t BgL_libz00_3014)
	{
		{	/* Module/eval.scm 111 */
			return BGl_addzd2evalzd2libraryz12z12zzmodule_evalz00(BgL_libz00_3014);
		}

	}



/* remember-eval-exported! */
	obj_t BGl_rememberzd2evalzd2exportedz12z12zzmodule_evalz00(obj_t
		BgL_varz00_79, obj_t BgL_modulez00_80, obj_t BgL_locz00_81)
	{
		{	/* Module/eval.scm 127 */
			{	/* Module/eval.scm 128 */
				obj_t BgL_arg1593z00_1953;

				{	/* Module/eval.scm 128 */
					obj_t BgL_list1594z00_1954;

					{	/* Module/eval.scm 128 */
						obj_t BgL_arg1595z00_1955;

						{	/* Module/eval.scm 128 */
							obj_t BgL_arg1602z00_1956;

							BgL_arg1602z00_1956 = MAKE_YOUNG_PAIR(BgL_locz00_81, BNIL);
							BgL_arg1595z00_1955 =
								MAKE_YOUNG_PAIR(BgL_modulez00_80, BgL_arg1602z00_1956);
						}
						BgL_list1594z00_1954 =
							MAKE_YOUNG_PAIR(BgL_varz00_79, BgL_arg1595z00_1955);
					}
					BgL_arg1593z00_1953 = BgL_list1594z00_1954;
				}
				return (BGl_za2evalzd2exportedza2zd2zzmodule_evalz00 =
					MAKE_YOUNG_PAIR(BgL_arg1593z00_1953,
						BGl_za2evalzd2exportedza2zd2zzmodule_evalz00), BUNSPEC);
			}
		}

	}



/* &eval-finalizer */
	obj_t BGl_z62evalzd2finaliza7erz17zzmodule_evalz00(obj_t BgL_envz00_3011)
	{
		{	/* Module/eval.scm 146 */
			{	/* Module/eval.scm 159 */
				bool_t BgL_test2155z00_3334;

				if (BGl_za2onezd2evalzf3za2z21zzmodule_evalz00)
					{	/* Module/eval.scm 159 */
						BgL_test2155z00_3334 = ((bool_t) 1);
					}
				else
					{	/* Module/eval.scm 159 */
						if (CBOOL(BGl_za2allzd2evalzf3za2z21zzmodule_evalz00))
							{	/* Module/eval.scm 159 */
								BgL_test2155z00_3334 = ((bool_t) 1);
							}
						else
							{	/* Module/eval.scm 159 */
								if (CBOOL(BGl_za2allzd2exportzd2evalzf3za2zf3zzmodule_evalz00))
									{	/* Module/eval.scm 159 */
										BgL_test2155z00_3334 = ((bool_t) 1);
									}
								else
									{	/* Module/eval.scm 159 */
										if (CBOOL
											(BGl_za2allzd2modulezd2evalzf3za2zf3zzmodule_evalz00))
											{	/* Module/eval.scm 159 */
												BgL_test2155z00_3334 = ((bool_t) 1);
											}
										else
											{	/* Module/eval.scm 159 */
												if (NULLP
													(BGl_za2evalzd2librariesza2zd2zzmodule_evalz00))
													{	/* Module/eval.scm 163 */
														BgL_test2155z00_3334 =
															PAIRP
															(BGl_za2evalzd2classesza2zd2zzmodule_evalz00);
													}
												else
													{	/* Module/eval.scm 163 */
														BgL_test2155z00_3334 = ((bool_t) 1);
													}
											}
									}
							}
					}
				if (BgL_test2155z00_3334)
					{	/* Module/eval.scm 167 */
						obj_t BgL_arg1605z00_3045;

						{	/* Module/eval.scm 167 */
							long BgL_arg1609z00_3046;
							obj_t BgL_arg1611z00_3047;

							{	/* Module/eval.scm 167 */
								obj_t BgL_arg1613z00_3048;

								BgL_arg1613z00_3048 =
									BGl_getzd2toplevelzd2unitzd2weightzd2zzmodule_includez00();
								BgL_arg1609z00_3046 = ((long) CINT(BgL_arg1613z00_3048) - 2L);
							}
							BgL_arg1611z00_3047 =
								BGl_makezd2promisezd2zz__r4_control_features_6_9z00
								(BGl_proc2087z00zzmodule_evalz00);
							{	/* Module/eval.scm 166 */
								obj_t BgL_idz00_3049;

								BgL_idz00_3049 = CNST_TABLE_REF(0);
								{	/* Module/eval.scm 166 */
									obj_t BgL_newz00_3050;

									BgL_newz00_3050 =
										create_struct(CNST_TABLE_REF(9), (int) (5L));
									{	/* Module/eval.scm 166 */
										int BgL_tmpz00_3353;

										BgL_tmpz00_3353 = (int) (4L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_3353, BFALSE);
									}
									{	/* Module/eval.scm 166 */
										int BgL_tmpz00_3356;

										BgL_tmpz00_3356 = (int) (3L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_3356, BFALSE);
									}
									{	/* Module/eval.scm 166 */
										int BgL_tmpz00_3359;

										BgL_tmpz00_3359 = (int) (2L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_3359,
											BgL_arg1611z00_3047);
									}
									{	/* Module/eval.scm 166 */
										obj_t BgL_auxz00_3364;
										int BgL_tmpz00_3362;

										BgL_auxz00_3364 = BINT(BgL_arg1609z00_3046);
										BgL_tmpz00_3362 = (int) (1L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_3362,
											BgL_auxz00_3364);
									}
									{	/* Module/eval.scm 166 */
										int BgL_tmpz00_3367;

										BgL_tmpz00_3367 = (int) (0L);
										STRUCT_SET(BgL_newz00_3050, BgL_tmpz00_3367,
											BgL_idz00_3049);
									}
									BgL_arg1605z00_3045 = BgL_newz00_3050;
						}}}
						{	/* Module/eval.scm 165 */
							obj_t BgL_list1606z00_3051;

							BgL_list1606z00_3051 = MAKE_YOUNG_PAIR(BgL_arg1605z00_3045, BNIL);
							return BgL_list1606z00_3051;
						}
					}
				else
					{	/* Module/eval.scm 159 */
						return CNST_TABLE_REF(10);
					}
			}
		}

	}



/* &<@anonymous:1616> */
	obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzmodule_evalz00(obj_t
		BgL_envz00_3016)
	{
		{	/* Module/eval.scm 168 */
			{
				obj_t BgL_gz00_3053;

				{	/* Module/eval.scm 169 */
					obj_t BgL_g1110z00_3090;
					obj_t BgL_g1111z00_3091;

					{	/* Module/eval.scm 171 */
						obj_t BgL_arg1702z00_3092;
						obj_t BgL_arg1703z00_3093;

						{	/* Module/eval.scm 171 */
							obj_t BgL_arg1705z00_3094;

							if (CBOOL(BGl_za2allzd2evalzf3za2z21zzmodule_evalz00))
								{	/* Module/eval.scm 171 */
									BgL_arg1705z00_3094 = CNST_TABLE_REF(12);
								}
							else
								{	/* Module/eval.scm 171 */
									if (CBOOL
										(BGl_za2allzd2exportzd2evalzf3za2zf3zzmodule_evalz00))
										{	/* Module/eval.scm 172 */
											BgL_arg1705z00_3094 = CNST_TABLE_REF(4);
										}
									else
										{	/* Module/eval.scm 172 */
											BgL_arg1705z00_3094 = CNST_TABLE_REF(13);
										}
								}
							BgL_arg1702z00_3092 =
								BGl_getzd2evaluatedzd2globalsz00zzmodule_evalz00
								(BgL_arg1705z00_3094);
						}
						BgL_arg1703z00_3093 =
							BGl_getzd2evaluatedzd2classzd2holderszd2zzmodule_evalz00();
						BgL_g1110z00_3090 =
							BGl_appendzd221011zd2zzmodule_evalz00(BgL_arg1702z00_3092,
							BgL_arg1703z00_3093);
					}
					BgL_g1111z00_3091 = CNST_TABLE_REF(14);
					{
						obj_t BgL_globalsz00_3096;
						obj_t BgL_initza2za2_3097;

						BgL_globalsz00_3096 = BgL_g1110z00_3090;
						BgL_initza2za2_3097 = BgL_g1111z00_3091;
					BgL_loopz00_3095:
						if (NULLP(BgL_globalsz00_3096))
							{	/* Module/eval.scm 179 */
								obj_t BgL_arg1625z00_3098;

								{	/* Module/eval.scm 179 */
									obj_t BgL_arg1626z00_3099;
									obj_t BgL_arg1627z00_3100;

									{	/* Module/eval.scm 179 */
										obj_t BgL_l1249z00_3101;

										BgL_l1249z00_3101 =
											BGl_za2evalzd2librariesza2zd2zzmodule_evalz00;
										if (NULLP(BgL_l1249z00_3101))
											{	/* Module/eval.scm 179 */
												BgL_arg1626z00_3099 = BNIL;
											}
										else
											{	/* Module/eval.scm 179 */
												obj_t BgL_head1251z00_3102;

												BgL_head1251z00_3102 =
													MAKE_YOUNG_PAIR(BGl_library_ez00zzmodule_evalz00(CAR
														(BgL_l1249z00_3101)), BNIL);
												{	/* Module/eval.scm 179 */
													obj_t BgL_g1254z00_3103;

													BgL_g1254z00_3103 = CDR(BgL_l1249z00_3101);
													{
														obj_t BgL_l1249z00_3105;
														obj_t BgL_tail1252z00_3106;

														BgL_l1249z00_3105 = BgL_g1254z00_3103;
														BgL_tail1252z00_3106 = BgL_head1251z00_3102;
													BgL_zc3z04anonymousza31629ze3z87_3104:
														if (NULLP(BgL_l1249z00_3105))
															{	/* Module/eval.scm 179 */
																BgL_arg1626z00_3099 = BgL_head1251z00_3102;
															}
														else
															{	/* Module/eval.scm 179 */
																obj_t BgL_newtail1253z00_3107;

																{	/* Module/eval.scm 179 */
																	obj_t BgL_arg1646z00_3108;

																	{	/* Module/eval.scm 179 */
																		obj_t BgL_arg1650z00_3109;

																		BgL_arg1650z00_3109 =
																			CAR(((obj_t) BgL_l1249z00_3105));
																		BgL_arg1646z00_3108 =
																			BGl_library_ez00zzmodule_evalz00
																			(BgL_arg1650z00_3109);
																	}
																	BgL_newtail1253z00_3107 =
																		MAKE_YOUNG_PAIR(BgL_arg1646z00_3108, BNIL);
																}
																SET_CDR(BgL_tail1252z00_3106,
																	BgL_newtail1253z00_3107);
																{	/* Module/eval.scm 179 */
																	obj_t BgL_arg1642z00_3110;

																	BgL_arg1642z00_3110 =
																		CDR(((obj_t) BgL_l1249z00_3105));
																	{
																		obj_t BgL_tail1252z00_3401;
																		obj_t BgL_l1249z00_3400;

																		BgL_l1249z00_3400 = BgL_arg1642z00_3110;
																		BgL_tail1252z00_3401 =
																			BgL_newtail1253z00_3107;
																		BgL_tail1252z00_3106 = BgL_tail1252z00_3401;
																		BgL_l1249z00_3105 = BgL_l1249z00_3400;
																		goto BgL_zc3z04anonymousza31629ze3z87_3104;
																	}
																}
															}
													}
												}
											}
									}
									{	/* Module/eval.scm 181 */
										obj_t BgL_arg1661z00_3111;
										obj_t BgL_arg1663z00_3112;

										BgL_arg1661z00_3111 =
											BGl_getzd2evalzd2srfizd2librarieszd2zzmodule_evalz00();
										{	/* Module/eval.scm 183 */
											obj_t BgL_arg1675z00_3113;
											obj_t BgL_arg1678z00_3114;

											BgL_arg1675z00_3113 =
												BGl_getzd2evaluatedzd2classzd2macroszd2zzmodule_evalz00
												();
											{	/* Module/eval.scm 185 */
												obj_t BgL_arg1681z00_3115;
												obj_t BgL_arg1688z00_3116;

												BgL_arg1681z00_3115 =
													bgl_reverse_bang(BgL_initza2za2_3097);
												{	/* Module/eval.scm 187 */
													obj_t BgL_arg1689z00_3117;
													obj_t BgL_arg1691z00_3118;

													BgL_arg1689z00_3117 =
														BGl_evmodulezd2compzd2zzmodule_evalz00();
													BgL_arg1691z00_3118 = MAKE_YOUNG_PAIR(BUNSPEC, BNIL);
													BgL_arg1688z00_3116 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
														(BgL_arg1689z00_3117, BgL_arg1691z00_3118);
												}
												BgL_arg1678z00_3114 =
													BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
													(BgL_arg1681z00_3115, BgL_arg1688z00_3116);
											}
											BgL_arg1663z00_3112 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1675z00_3113, BgL_arg1678z00_3114);
										}
										BgL_arg1627z00_3100 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1661z00_3111, BgL_arg1663z00_3112);
									}
									BgL_arg1625z00_3098 =
										BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1626z00_3099, BgL_arg1627z00_3100);
								}
								return MAKE_YOUNG_PAIR(CNST_TABLE_REF(15), BgL_arg1625z00_3098);
							}
						else
							{	/* Module/eval.scm 189 */
								obj_t BgL_gz00_3119;

								BgL_gz00_3119 = CAR(((obj_t) BgL_globalsz00_3096));
								BGl_setzd2evalzd2typesz12z12zzmodule_evalz00(BgL_gz00_3119);
								{	/* Module/eval.scm 191 */
									obj_t BgL_arg1692z00_3120;
									obj_t BgL_arg1699z00_3121;

									BgL_arg1692z00_3120 = CDR(((obj_t) BgL_globalsz00_3096));
									if (
										(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_gz00_3119)))->
											BgL_evalzf3zf3))
										{	/* Module/eval.scm 192 */
											BgL_arg1699z00_3121 = BgL_initza2za2_3097;
										}
									else
										{	/* Module/eval.scm 194 */
											BgL_nodez00_bglt BgL_arg1701z00_3122;

											BgL_gz00_3053 = BgL_gz00_3119;
											((((BgL_globalz00_bglt) COBJECT(
															((BgL_globalz00_bglt) BgL_gz00_3053)))->
													BgL_evalzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
											{	/* Module/eval.scm 151 */
												bool_t BgL_test2167z00_3424;

												{	/* Module/eval.scm 151 */
													BgL_valuez00_bglt BgL_arg1724z00_3054;

													BgL_arg1724z00_3054 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_gz00_3053))))->
														BgL_valuez00);
													{	/* Module/eval.scm 151 */
														obj_t BgL_classz00_3055;

														BgL_classz00_3055 = BGl_svarz00zzast_varz00;
														{	/* Module/eval.scm 151 */
															BgL_objectz00_bglt BgL_arg1807z00_3056;

															{	/* Module/eval.scm 151 */
																obj_t BgL_tmpz00_3428;

																BgL_tmpz00_3428 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_arg1724z00_3054));
																BgL_arg1807z00_3056 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_3428);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Module/eval.scm 151 */
																	long BgL_idxz00_3057;

																	BgL_idxz00_3057 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3056);
																	BgL_test2167z00_3424 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3057 + 2L)) ==
																		BgL_classz00_3055);
																}
															else
																{	/* Module/eval.scm 151 */
																	bool_t BgL_res2071z00_3060;

																	{	/* Module/eval.scm 151 */
																		obj_t BgL_oclassz00_3061;

																		{	/* Module/eval.scm 151 */
																			obj_t BgL_arg1815z00_3062;
																			long BgL_arg1816z00_3063;

																			BgL_arg1815z00_3062 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Module/eval.scm 151 */
																				long BgL_arg1817z00_3064;

																				BgL_arg1817z00_3064 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3056);
																				BgL_arg1816z00_3063 =
																					(BgL_arg1817z00_3064 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3061 =
																				VECTOR_REF(BgL_arg1815z00_3062,
																				BgL_arg1816z00_3063);
																		}
																		{	/* Module/eval.scm 151 */
																			bool_t BgL__ortest_1115z00_3065;

																			BgL__ortest_1115z00_3065 =
																				(BgL_classz00_3055 ==
																				BgL_oclassz00_3061);
																			if (BgL__ortest_1115z00_3065)
																				{	/* Module/eval.scm 151 */
																					BgL_res2071z00_3060 =
																						BgL__ortest_1115z00_3065;
																				}
																			else
																				{	/* Module/eval.scm 151 */
																					long BgL_odepthz00_3066;

																					{	/* Module/eval.scm 151 */
																						obj_t BgL_arg1804z00_3067;

																						BgL_arg1804z00_3067 =
																							(BgL_oclassz00_3061);
																						BgL_odepthz00_3066 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3067);
																					}
																					if ((2L < BgL_odepthz00_3066))
																						{	/* Module/eval.scm 151 */
																							obj_t BgL_arg1802z00_3068;

																							{	/* Module/eval.scm 151 */
																								obj_t BgL_arg1803z00_3069;

																								BgL_arg1803z00_3069 =
																									(BgL_oclassz00_3061);
																								BgL_arg1802z00_3068 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3069, 2L);
																							}
																							BgL_res2071z00_3060 =
																								(BgL_arg1802z00_3068 ==
																								BgL_classz00_3055);
																						}
																					else
																						{	/* Module/eval.scm 151 */
																							BgL_res2071z00_3060 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2167z00_3424 = BgL_res2071z00_3060;
																}
														}
													}
												}
												if (BgL_test2167z00_3424)
													{	/* Module/eval.scm 151 */
														{	/* Module/eval.scm 152 */
															obj_t BgL_vz00_3070;

															BgL_vz00_3070 = CNST_TABLE_REF(11);
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt) BgL_gz00_3053)))->
																	BgL_accessz00) =
																((obj_t) BgL_vz00_3070), BUNSPEC);
														}
														{	/* Module/eval.scm 153 */
															BgL_nodez00_bglt BgL_arg1714z00_3071;
															obj_t BgL_arg1717z00_3072;

															BgL_arg1714z00_3071 =
																BGl_locationzd2ze3nodez31zzast_sexpz00(
																((BgL_globalz00_bglt) BgL_gz00_3053));
															BgL_arg1717z00_3072 =
																(((BgL_globalz00_bglt) COBJECT(
																		((BgL_globalz00_bglt) BgL_gz00_3053)))->
																BgL_srcz00);
															BgL_arg1701z00_3122 =
																BGl_definezd2primopzd2refzf2srczd2ze3nodezc3zzast_sexpz00
																(((BgL_globalz00_bglt) BgL_gz00_3053),
																BgL_arg1714z00_3071, BgL_arg1717z00_3072);
														}
													}
												else
													{	/* Module/eval.scm 154 */
														bool_t BgL_test2171z00_3460;

														{	/* Module/eval.scm 154 */
															BgL_valuez00_bglt BgL_arg1722z00_3073;

															BgL_arg1722z00_3073 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_globalz00_bglt) BgL_gz00_3053))))->
																BgL_valuez00);
															{	/* Module/eval.scm 154 */
																obj_t BgL_classz00_3074;

																BgL_classz00_3074 = BGl_scnstz00zzast_varz00;
																{	/* Module/eval.scm 154 */
																	BgL_objectz00_bglt BgL_arg1807z00_3075;

																	{	/* Module/eval.scm 154 */
																		obj_t BgL_tmpz00_3464;

																		BgL_tmpz00_3464 =
																			((obj_t)
																			((BgL_objectz00_bglt)
																				BgL_arg1722z00_3073));
																		BgL_arg1807z00_3075 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_3464);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Module/eval.scm 154 */
																			long BgL_idxz00_3076;

																			BgL_idxz00_3076 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3075);
																			BgL_test2171z00_3460 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3076 + 2L)) ==
																				BgL_classz00_3074);
																		}
																	else
																		{	/* Module/eval.scm 154 */
																			bool_t BgL_res2072z00_3079;

																			{	/* Module/eval.scm 154 */
																				obj_t BgL_oclassz00_3080;

																				{	/* Module/eval.scm 154 */
																					obj_t BgL_arg1815z00_3081;
																					long BgL_arg1816z00_3082;

																					BgL_arg1815z00_3081 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Module/eval.scm 154 */
																						long BgL_arg1817z00_3083;

																						BgL_arg1817z00_3083 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3075);
																						BgL_arg1816z00_3082 =
																							(BgL_arg1817z00_3083 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3080 =
																						VECTOR_REF(BgL_arg1815z00_3081,
																						BgL_arg1816z00_3082);
																				}
																				{	/* Module/eval.scm 154 */
																					bool_t BgL__ortest_1115z00_3084;

																					BgL__ortest_1115z00_3084 =
																						(BgL_classz00_3074 ==
																						BgL_oclassz00_3080);
																					if (BgL__ortest_1115z00_3084)
																						{	/* Module/eval.scm 154 */
																							BgL_res2072z00_3079 =
																								BgL__ortest_1115z00_3084;
																						}
																					else
																						{	/* Module/eval.scm 154 */
																							long BgL_odepthz00_3085;

																							{	/* Module/eval.scm 154 */
																								obj_t BgL_arg1804z00_3086;

																								BgL_arg1804z00_3086 =
																									(BgL_oclassz00_3080);
																								BgL_odepthz00_3085 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3086);
																							}
																							if ((2L < BgL_odepthz00_3085))
																								{	/* Module/eval.scm 154 */
																									obj_t BgL_arg1802z00_3087;

																									{	/* Module/eval.scm 154 */
																										obj_t BgL_arg1803z00_3088;

																										BgL_arg1803z00_3088 =
																											(BgL_oclassz00_3080);
																										BgL_arg1802z00_3087 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3088, 2L);
																									}
																									BgL_res2072z00_3079 =
																										(BgL_arg1802z00_3087 ==
																										BgL_classz00_3074);
																								}
																							else
																								{	/* Module/eval.scm 154 */
																									BgL_res2072z00_3079 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2171z00_3460 =
																				BgL_res2072z00_3079;
																		}
																}
															}
														}
														if (BgL_test2171z00_3460)
															{	/* Module/eval.scm 155 */
																BgL_nodez00_bglt BgL_arg1720z00_3089;

																BgL_arg1720z00_3089 =
																	BGl_locationzd2ze3nodez31zzast_sexpz00(
																	((BgL_globalz00_bglt) BgL_gz00_3053));
																BgL_arg1701z00_3122 =
																	BGl_definezd2primopzd2refzd2ze3nodez31zzast_sexpz00
																	(((BgL_globalz00_bglt) BgL_gz00_3053),
																	BgL_arg1720z00_3089);
															}
														else
															{	/* Module/eval.scm 154 */
																BgL_arg1701z00_3122 =
																	BGl_definezd2primopzd2ze3nodeze3zzast_sexpz00(
																	((BgL_globalz00_bglt) BgL_gz00_3053));
															}
													}
											}
											BgL_arg1699z00_3121 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_arg1701z00_3122), BgL_initza2za2_3097);
										}
									{
										obj_t BgL_initza2za2_3496;
										obj_t BgL_globalsz00_3495;

										BgL_globalsz00_3495 = BgL_arg1692z00_3120;
										BgL_initza2za2_3496 = BgL_arg1699z00_3121;
										BgL_initza2za2_3097 = BgL_initza2za2_3496;
										BgL_globalsz00_3096 = BgL_globalsz00_3495;
										goto BgL_loopz00_3095;
									}
								}
							}
					}
				}
			}
		}

	}



/* evmodule-comp */
	obj_t BGl_evmodulezd2compzd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 202 */
			if (CBOOL(BGl_za2allzd2modulezd2evalzf3za2zf3zzmodule_evalz00))
				{	/* Module/eval.scm 219 */
					BgL_nodez00_bglt BgL_arg1733z00_2025;

					{	/* Module/eval.scm 219 */
						obj_t BgL_arg1735z00_2027;

						{	/* Module/eval.scm 219 */
							obj_t BgL_arg1736z00_2028;
							obj_t BgL_arg1737z00_2029;

							{	/* Module/eval.scm 219 */
								obj_t BgL_arg1738z00_2030;

								{	/* Module/eval.scm 219 */
									obj_t BgL_arg1739z00_2031;

									BgL_arg1739z00_2031 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BNIL);
									BgL_arg1738z00_2030 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg1739z00_2031);
								}
								BgL_arg1736z00_2028 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(5), BgL_arg1738z00_2030);
							}
							{	/* Module/eval.scm 220 */
								obj_t BgL_arg1740z00_2032;
								obj_t BgL_arg1746z00_2033;

								{	/* Module/eval.scm 220 */
									obj_t BgL_arg1747z00_2034;

									BgL_arg1747z00_2034 =
										MAKE_YOUNG_PAIR(BGl_za2moduleza2z00zzmodule_modulez00,
										BNIL);
									BgL_arg1740z00_2032 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1747z00_2034);
								}
								{	/* Module/eval.scm 220 */
									obj_t BgL_arg1748z00_2035;
									obj_t BgL_arg1749z00_2036;

									{	/* Module/eval.scm 220 */
										obj_t BgL_arg1750z00_2037;

										BgL_arg1750z00_2037 =
											MAKE_YOUNG_PAIR(BGl_za2srczd2filesza2zd2zzengine_paramz00,
											BNIL);
										BgL_arg1748z00_2035 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1750z00_2037);
									}
									{	/* Module/eval.scm 220 */
										obj_t BgL_arg1751z00_2038;
										obj_t BgL_arg1752z00_2039;

										{	/* Module/eval.scm 220 */
											obj_t BgL_arg1753z00_2040;

											BgL_arg1753z00_2040 =
												MAKE_YOUNG_PAIR
												(BGl_za2modulezd2locationza2zd2zzmodule_modulez00,
												BNIL);
											BgL_arg1751z00_2038 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
												BgL_arg1753z00_2040);
										}
										{	/* Module/eval.scm 221 */
											obj_t BgL_arg1754z00_2041;

											{	/* Module/eval.scm 221 */
												obj_t BgL_l1255z00_2042;

												BgL_l1255z00_2042 =
													BGl_getzd2evaluatedzd2globalsz00zzmodule_evalz00
													(CNST_TABLE_REF(19));
												if (NULLP(BgL_l1255z00_2042))
													{	/* Module/eval.scm 221 */
														BgL_arg1754z00_2041 = BNIL;
													}
												else
													{	/* Module/eval.scm 221 */
														obj_t BgL_head1257z00_2044;

														{	/* Module/eval.scm 221 */
															obj_t BgL_arg1767z00_2056;

															{	/* Module/eval.scm 221 */
																obj_t BgL_arg1770z00_2057;

																BgL_arg1770z00_2057 =
																	CAR(((obj_t) BgL_l1255z00_2042));
																BgL_arg1767z00_2056 =
																	BGl_exportzd2globalze70z35zzmodule_evalz00
																	(BgL_arg1770z00_2057);
															}
															BgL_head1257z00_2044 =
																MAKE_YOUNG_PAIR(BgL_arg1767z00_2056, BNIL);
														}
														{	/* Module/eval.scm 221 */
															obj_t BgL_g1260z00_2045;

															BgL_g1260z00_2045 =
																CDR(((obj_t) BgL_l1255z00_2042));
															{
																obj_t BgL_l1255z00_2047;
																obj_t BgL_tail1258z00_2048;

																BgL_l1255z00_2047 = BgL_g1260z00_2045;
																BgL_tail1258z00_2048 = BgL_head1257z00_2044;
															BgL_zc3z04anonymousza31757ze3z87_2049:
																if (NULLP(BgL_l1255z00_2047))
																	{	/* Module/eval.scm 221 */
																		BgL_arg1754z00_2041 = BgL_head1257z00_2044;
																	}
																else
																	{	/* Module/eval.scm 221 */
																		obj_t BgL_newtail1259z00_2051;

																		{	/* Module/eval.scm 221 */
																			obj_t BgL_arg1762z00_2053;

																			{	/* Module/eval.scm 221 */
																				obj_t BgL_arg1765z00_2054;

																				BgL_arg1765z00_2054 =
																					CAR(((obj_t) BgL_l1255z00_2047));
																				BgL_arg1762z00_2053 =
																					BGl_exportzd2globalze70z35zzmodule_evalz00
																					(BgL_arg1765z00_2054);
																			}
																			BgL_newtail1259z00_2051 =
																				MAKE_YOUNG_PAIR(BgL_arg1762z00_2053,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1258z00_2048,
																			BgL_newtail1259z00_2051);
																		{	/* Module/eval.scm 221 */
																			obj_t BgL_arg1761z00_2052;

																			BgL_arg1761z00_2052 =
																				CDR(((obj_t) BgL_l1255z00_2047));
																			{
																				obj_t BgL_tail1258z00_3534;
																				obj_t BgL_l1255z00_3533;

																				BgL_l1255z00_3533 = BgL_arg1761z00_2052;
																				BgL_tail1258z00_3534 =
																					BgL_newtail1259z00_2051;
																				BgL_tail1258z00_2048 =
																					BgL_tail1258z00_3534;
																				BgL_l1255z00_2047 = BgL_l1255z00_3533;
																				goto
																					BgL_zc3z04anonymousza31757ze3z87_2049;
																			}
																		}
																	}
															}
														}
													}
											}
											BgL_arg1752z00_2039 =
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
												(BgL_arg1754z00_2041, BNIL);
										}
										BgL_arg1749z00_2036 =
											MAKE_YOUNG_PAIR(BgL_arg1751z00_2038, BgL_arg1752z00_2039);
									}
									BgL_arg1746z00_2033 =
										MAKE_YOUNG_PAIR(BgL_arg1748z00_2035, BgL_arg1749z00_2036);
								}
								BgL_arg1737z00_2029 =
									MAKE_YOUNG_PAIR(BgL_arg1740z00_2032, BgL_arg1746z00_2033);
							}
							BgL_arg1735z00_2027 =
								MAKE_YOUNG_PAIR(BgL_arg1736z00_2028, BgL_arg1737z00_2029);
						}
						BgL_arg1733z00_2025 =
							BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1735z00_2027, BNIL,
							BFALSE, CNST_TABLE_REF(20));
					}
					{	/* Module/eval.scm 217 */
						obj_t BgL_list1734z00_2026;

						BgL_list1734z00_2026 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg1733z00_2025), BNIL);
						return BgL_list1734z00_2026;
					}
				}
			else
				{	/* Module/eval.scm 216 */
					return BNIL;
				}
		}

	}



/* export-global~0 */
	obj_t BGl_exportzd2globalze70z35zzmodule_evalz00(obj_t BgL_gz00_2058)
	{
		{	/* Module/eval.scm 205 */
			((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_gz00_2058)))->BgL_evalzf3zf3) =
				((bool_t) ((bool_t) 1)), BUNSPEC);
			{	/* Module/eval.scm 206 */
				obj_t BgL_idz00_2060;

				BgL_idz00_2060 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_gz00_2058))))->BgL_idz00);
				{	/* Module/eval.scm 208 */
					bool_t BgL_test2178z00_3549;

					{	/* Module/eval.scm 208 */
						BgL_valuez00_bglt BgL_arg1849z00_2091;

						BgL_arg1849z00_2091 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_gz00_2058))))->BgL_valuez00);
						{	/* Module/eval.scm 208 */
							obj_t BgL_classz00_2634;

							BgL_classz00_2634 = BGl_svarz00zzast_varz00;
							{	/* Module/eval.scm 208 */
								BgL_objectz00_bglt BgL_arg1807z00_2636;

								{	/* Module/eval.scm 208 */
									obj_t BgL_tmpz00_3553;

									BgL_tmpz00_3553 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1849z00_2091));
									BgL_arg1807z00_2636 = (BgL_objectz00_bglt) (BgL_tmpz00_3553);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Module/eval.scm 208 */
										long BgL_idxz00_2642;

										BgL_idxz00_2642 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2636);
										BgL_test2178z00_3549 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2642 + 2L)) == BgL_classz00_2634);
									}
								else
									{	/* Module/eval.scm 208 */
										bool_t BgL_res2074z00_2667;

										{	/* Module/eval.scm 208 */
											obj_t BgL_oclassz00_2650;

											{	/* Module/eval.scm 208 */
												obj_t BgL_arg1815z00_2658;
												long BgL_arg1816z00_2659;

												BgL_arg1815z00_2658 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Module/eval.scm 208 */
													long BgL_arg1817z00_2660;

													BgL_arg1817z00_2660 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2636);
													BgL_arg1816z00_2659 =
														(BgL_arg1817z00_2660 - OBJECT_TYPE);
												}
												BgL_oclassz00_2650 =
													VECTOR_REF(BgL_arg1815z00_2658, BgL_arg1816z00_2659);
											}
											{	/* Module/eval.scm 208 */
												bool_t BgL__ortest_1115z00_2651;

												BgL__ortest_1115z00_2651 =
													(BgL_classz00_2634 == BgL_oclassz00_2650);
												if (BgL__ortest_1115z00_2651)
													{	/* Module/eval.scm 208 */
														BgL_res2074z00_2667 = BgL__ortest_1115z00_2651;
													}
												else
													{	/* Module/eval.scm 208 */
														long BgL_odepthz00_2652;

														{	/* Module/eval.scm 208 */
															obj_t BgL_arg1804z00_2653;

															BgL_arg1804z00_2653 = (BgL_oclassz00_2650);
															BgL_odepthz00_2652 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2653);
														}
														if ((2L < BgL_odepthz00_2652))
															{	/* Module/eval.scm 208 */
																obj_t BgL_arg1802z00_2655;

																{	/* Module/eval.scm 208 */
																	obj_t BgL_arg1803z00_2656;

																	BgL_arg1803z00_2656 = (BgL_oclassz00_2650);
																	BgL_arg1802z00_2655 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2656,
																		2L);
																}
																BgL_res2074z00_2667 =
																	(BgL_arg1802z00_2655 == BgL_classz00_2634);
															}
														else
															{	/* Module/eval.scm 208 */
																BgL_res2074z00_2667 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2178z00_3549 = BgL_res2074z00_2667;
									}
							}
						}
					}
					if (BgL_test2178z00_3549)
						{	/* Module/eval.scm 208 */
							{	/* Module/eval.scm 209 */
								obj_t BgL_vz00_2669;

								BgL_vz00_2669 = CNST_TABLE_REF(11);
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_gz00_2058)))->
										BgL_accessz00) = ((obj_t) BgL_vz00_2669), BUNSPEC);
							}
							{	/* Module/eval.scm 210 */
								obj_t BgL_arg1775z00_2063;

								{	/* Module/eval.scm 210 */
									obj_t BgL_arg1798z00_2064;

									{	/* Module/eval.scm 210 */
										obj_t BgL_arg1799z00_2065;
										obj_t BgL_arg1805z00_2066;

										{	/* Module/eval.scm 210 */
											obj_t BgL_arg1806z00_2067;

											BgL_arg1806z00_2067 =
												MAKE_YOUNG_PAIR(BgL_idz00_2060, BNIL);
											BgL_arg1799z00_2065 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
												BgL_arg1806z00_2067);
										}
										{	/* Module/eval.scm 210 */
											obj_t BgL_arg1808z00_2068;
											obj_t BgL_arg1812z00_2069;

											{	/* Module/eval.scm 210 */
												obj_t BgL_arg1820z00_2070;

												BgL_arg1820z00_2070 =
													MAKE_YOUNG_PAIR(BgL_idz00_2060, BNIL);
												BgL_arg1808z00_2068 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
													BgL_arg1820z00_2070);
											}
											{	/* Module/eval.scm 210 */
												obj_t BgL_arg1822z00_2071;

												BgL_arg1822z00_2071 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
												BgL_arg1812z00_2069 =
													MAKE_YOUNG_PAIR(BFALSE, BgL_arg1822z00_2071);
											}
											BgL_arg1805z00_2066 =
												MAKE_YOUNG_PAIR(BgL_arg1808z00_2068,
												BgL_arg1812z00_2069);
										}
										BgL_arg1798z00_2064 =
											MAKE_YOUNG_PAIR(BgL_arg1799z00_2065, BgL_arg1805z00_2066);
									}
									BgL_arg1775z00_2063 =
										MAKE_YOUNG_PAIR(BINT(1L), BgL_arg1798z00_2064);
								}
								return MAKE_YOUNG_PAIR(CNST_TABLE_REF(22), BgL_arg1775z00_2063);
							}
						}
					else
						{	/* Module/eval.scm 211 */
							bool_t BgL_test2182z00_3593;

							{	/* Module/eval.scm 211 */
								BgL_valuez00_bglt BgL_arg1848z00_2090;

								BgL_arg1848z00_2090 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_gz00_2058))))->BgL_valuez00);
								{	/* Module/eval.scm 211 */
									obj_t BgL_classz00_2671;

									BgL_classz00_2671 = BGl_scnstz00zzast_varz00;
									{	/* Module/eval.scm 211 */
										BgL_objectz00_bglt BgL_arg1807z00_2673;

										{	/* Module/eval.scm 211 */
											obj_t BgL_tmpz00_3597;

											BgL_tmpz00_3597 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1848z00_2090));
											BgL_arg1807z00_2673 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3597);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Module/eval.scm 211 */
												long BgL_idxz00_2679;

												BgL_idxz00_2679 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2673);
												BgL_test2182z00_3593 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2679 + 2L)) == BgL_classz00_2671);
											}
										else
											{	/* Module/eval.scm 211 */
												bool_t BgL_res2075z00_2704;

												{	/* Module/eval.scm 211 */
													obj_t BgL_oclassz00_2687;

													{	/* Module/eval.scm 211 */
														obj_t BgL_arg1815z00_2695;
														long BgL_arg1816z00_2696;

														BgL_arg1815z00_2695 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Module/eval.scm 211 */
															long BgL_arg1817z00_2697;

															BgL_arg1817z00_2697 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2673);
															BgL_arg1816z00_2696 =
																(BgL_arg1817z00_2697 - OBJECT_TYPE);
														}
														BgL_oclassz00_2687 =
															VECTOR_REF(BgL_arg1815z00_2695,
															BgL_arg1816z00_2696);
													}
													{	/* Module/eval.scm 211 */
														bool_t BgL__ortest_1115z00_2688;

														BgL__ortest_1115z00_2688 =
															(BgL_classz00_2671 == BgL_oclassz00_2687);
														if (BgL__ortest_1115z00_2688)
															{	/* Module/eval.scm 211 */
																BgL_res2075z00_2704 = BgL__ortest_1115z00_2688;
															}
														else
															{	/* Module/eval.scm 211 */
																long BgL_odepthz00_2689;

																{	/* Module/eval.scm 211 */
																	obj_t BgL_arg1804z00_2690;

																	BgL_arg1804z00_2690 = (BgL_oclassz00_2687);
																	BgL_odepthz00_2689 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2690);
																}
																if ((2L < BgL_odepthz00_2689))
																	{	/* Module/eval.scm 211 */
																		obj_t BgL_arg1802z00_2692;

																		{	/* Module/eval.scm 211 */
																			obj_t BgL_arg1803z00_2693;

																			BgL_arg1803z00_2693 =
																				(BgL_oclassz00_2687);
																			BgL_arg1802z00_2692 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2693, 2L);
																		}
																		BgL_res2075z00_2704 =
																			(BgL_arg1802z00_2692 ==
																			BgL_classz00_2671);
																	}
																else
																	{	/* Module/eval.scm 211 */
																		BgL_res2075z00_2704 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2182z00_3593 = BgL_res2075z00_2704;
											}
									}
								}
							}
							if (BgL_test2182z00_3593)
								{	/* Module/eval.scm 212 */
									obj_t BgL_arg1831z00_2074;

									{	/* Module/eval.scm 212 */
										obj_t BgL_arg1832z00_2075;

										{	/* Module/eval.scm 212 */
											obj_t BgL_arg1833z00_2076;
											obj_t BgL_arg1834z00_2077;

											{	/* Module/eval.scm 212 */
												obj_t BgL_arg1835z00_2078;

												BgL_arg1835z00_2078 =
													MAKE_YOUNG_PAIR(BgL_idz00_2060, BNIL);
												BgL_arg1833z00_2076 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
													BgL_arg1835z00_2078);
											}
											{	/* Module/eval.scm 212 */
												obj_t BgL_arg1836z00_2079;
												obj_t BgL_arg1837z00_2080;

												{	/* Module/eval.scm 212 */
													obj_t BgL_arg1838z00_2081;

													BgL_arg1838z00_2081 =
														MAKE_YOUNG_PAIR(BgL_idz00_2060, BNIL);
													BgL_arg1836z00_2079 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
														BgL_arg1838z00_2081);
												}
												{	/* Module/eval.scm 212 */
													obj_t BgL_arg1839z00_2082;

													BgL_arg1839z00_2082 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
													BgL_arg1837z00_2080 =
														MAKE_YOUNG_PAIR(BFALSE, BgL_arg1839z00_2082);
												}
												BgL_arg1834z00_2077 =
													MAKE_YOUNG_PAIR(BgL_arg1836z00_2079,
													BgL_arg1837z00_2080);
											}
											BgL_arg1832z00_2075 =
												MAKE_YOUNG_PAIR(BgL_arg1833z00_2076,
												BgL_arg1834z00_2077);
										}
										BgL_arg1831z00_2074 =
											MAKE_YOUNG_PAIR(BINT(1L), BgL_arg1832z00_2075);
									}
									return
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(22), BgL_arg1831z00_2074);
								}
							else
								{	/* Module/eval.scm 214 */
									obj_t BgL_arg1840z00_2083;

									{	/* Module/eval.scm 214 */
										obj_t BgL_arg1842z00_2084;

										{	/* Module/eval.scm 214 */
											obj_t BgL_arg1843z00_2085;
											obj_t BgL_arg1844z00_2086;

											{	/* Module/eval.scm 214 */
												obj_t BgL_arg1845z00_2087;

												BgL_arg1845z00_2087 =
													MAKE_YOUNG_PAIR(BgL_idz00_2060, BNIL);
												BgL_arg1843z00_2085 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
													BgL_arg1845z00_2087);
											}
											{	/* Module/eval.scm 214 */
												obj_t BgL_arg1846z00_2088;

												{	/* Module/eval.scm 214 */
													obj_t BgL_arg1847z00_2089;

													BgL_arg1847z00_2089 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
													BgL_arg1846z00_2088 =
														MAKE_YOUNG_PAIR(BFALSE, BgL_arg1847z00_2089);
												}
												BgL_arg1844z00_2086 =
													MAKE_YOUNG_PAIR(BgL_idz00_2060, BgL_arg1846z00_2088);
											}
											BgL_arg1842z00_2084 =
												MAKE_YOUNG_PAIR(BgL_arg1843z00_2085,
												BgL_arg1844z00_2086);
										}
										BgL_arg1840z00_2083 =
											MAKE_YOUNG_PAIR(BINT(0L), BgL_arg1842z00_2084);
									}
									return
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(22), BgL_arg1840z00_2083);
								}
						}
				}
			}
		}

	}



/* get-library-info */
	obj_t BGl_getzd2libraryzd2infoz00zzmodule_evalz00(obj_t BgL_libraryz00_82)
	{
		{	/* Module/eval.scm 228 */
			{	/* Module/eval.scm 229 */
				obj_t BgL__ortest_1112z00_2093;

				BgL__ortest_1112z00_2093 =
					BGl_libraryzd2infozd2zz__libraryz00(BgL_libraryz00_82);
				if (CBOOL(BgL__ortest_1112z00_2093))
					{	/* Module/eval.scm 229 */
						return BgL__ortest_1112z00_2093;
					}
				else
					{	/* Module/eval.scm 230 */
						obj_t BgL_initz00_2094;

						BgL_initz00_2094 =
							BGl_libraryzd2initzd2filez00zz__libraryz00(BgL_libraryz00_82);
						{	/* Module/eval.scm 230 */
							obj_t BgL_pathz00_2095;

							BgL_pathz00_2095 =
								BGl_findzd2filezf2pathz20zz__osz00(BgL_initz00_2094,
								BGl_za2libzd2dirza2zd2zzengine_paramz00);
							{	/* Module/eval.scm 231 */

								if (CBOOL(BgL_pathz00_2095))
									{	/* Module/eval.scm 232 */
										{	/* Module/eval.scm 234 */
											obj_t BgL_envz00_2097;

											BgL_envz00_2097 =
												BGl_defaultzd2environmentzd2zz__evalz00();
											{	/* Module/eval.scm 234 */

												BGl_loadqz00zz__evalz00(BgL_pathz00_2095,
													BgL_envz00_2097);
											}
										}
										{	/* Module/eval.scm 235 */
											obj_t BgL_iz00_2098;

											BgL_iz00_2098 =
												BGl_libraryzd2infozd2zz__libraryz00(BgL_libraryz00_82);
											if (CBOOL(BgL_iz00_2098))
												{	/* Module/eval.scm 236 */
													return BgL_iz00_2098;
												}
											else
												{	/* Module/eval.scm 238 */
													obj_t BgL_arg1850z00_2099;

													{	/* Module/eval.scm 238 */
														obj_t BgL_list1853z00_2102;

														BgL_list1853z00_2102 =
															MAKE_YOUNG_PAIR(BgL_initz00_2094, BNIL);
														BgL_arg1850z00_2099 =
															BGl_formatz00zz__r4_output_6_10_3z00
															(BGl_string2088z00zzmodule_evalz00,
															BgL_list1853z00_2102);
													}
													{	/* Module/eval.scm 238 */
														obj_t BgL_list1851z00_2100;

														{	/* Module/eval.scm 238 */
															obj_t BgL_arg1852z00_2101;

															BgL_arg1852z00_2101 =
																MAKE_YOUNG_PAIR(BgL_arg1850z00_2099, BNIL);
															BgL_list1851z00_2100 =
																MAKE_YOUNG_PAIR(BgL_libraryz00_82,
																BgL_arg1852z00_2101);
														}
														BGL_TAIL return
															BGl_warningz00zz__errorz00(BgL_list1851z00_2100);
													}
												}
										}
									}
								else
									{	/* Module/eval.scm 239 */
										obj_t BgL_arg1854z00_2103;

										{	/* Module/eval.scm 239 */
											obj_t BgL_list1857z00_2106;

											BgL_list1857z00_2106 =
												MAKE_YOUNG_PAIR(BgL_initz00_2094, BNIL);
											BgL_arg1854z00_2103 =
												BGl_formatz00zz__r4_output_6_10_3z00
												(BGl_string2089z00zzmodule_evalz00,
												BgL_list1857z00_2106);
										}
										{	/* Module/eval.scm 239 */
											obj_t BgL_list1855z00_2104;

											{	/* Module/eval.scm 239 */
												obj_t BgL_arg1856z00_2105;

												BgL_arg1856z00_2105 =
													MAKE_YOUNG_PAIR(BgL_arg1854z00_2103, BNIL);
												BgL_list1855z00_2104 =
													MAKE_YOUNG_PAIR(BgL_libraryz00_82,
													BgL_arg1856z00_2105);
											}
											BGL_TAIL return
												BGl_warningz00zz__errorz00(BgL_list1855z00_2104);
										}
									}
							}
						}
					}
			}
		}

	}



/* library_e */
	obj_t BGl_library_ez00zzmodule_evalz00(obj_t BgL_libz00_83)
	{
		{	/* Module/eval.scm 246 */
			{	/* Module/eval.scm 247 */
				obj_t BgL_infoz00_2107;

				BgL_infoz00_2107 =
					BGl_getzd2libraryzd2infoz00zzmodule_evalz00(BgL_libz00_83);
				{	/* Module/eval.scm 248 */
					bool_t BgL_test2189z00_3668;

					{	/* Module/eval.scm 248 */
						bool_t BgL_test2190z00_3669;

						if (STRUCTP(BgL_infoz00_2107))
							{	/* Module/eval.scm 248 */
								BgL_test2190z00_3669 =
									(STRUCT_KEY(BgL_infoz00_2107) == CNST_TABLE_REF(23));
							}
						else
							{	/* Module/eval.scm 248 */
								BgL_test2190z00_3669 = ((bool_t) 0);
							}
						if (BgL_test2190z00_3669)
							{	/* Module/eval.scm 248 */
								BgL_test2189z00_3668 =
									CBOOL(STRUCT_REF(BgL_infoz00_2107, (int) (6L)));
							}
						else
							{	/* Module/eval.scm 248 */
								BgL_test2189z00_3668 = ((bool_t) 0);
							}
					}
					if (BgL_test2189z00_3668)
						{	/* Module/eval.scm 249 */
							obj_t BgL_initz00_2110;

							BgL_initz00_2110 =
								BGl_modulezd2initializa7ationzd2idza7zzmodule_modulez00
								(STRUCT_REF(BgL_infoz00_2107, (int) (6L)));
							{	/* Module/eval.scm 249 */
								BgL_globalz00_bglt BgL_gloz00_2111;

								{	/* Module/eval.scm 252 */
									obj_t BgL_arg1903z00_2148;
									obj_t BgL_arg1904z00_2149;

									BgL_arg1903z00_2148 =
										STRUCT_REF(BgL_infoz00_2107, (int) (6L));
									{	/* Module/eval.scm 254 */
										obj_t BgL_arg1906z00_2150;
										obj_t BgL_arg1910z00_2151;

										{	/* Module/eval.scm 254 */
											obj_t BgL_arg1455z00_2719;

											BgL_arg1455z00_2719 = SYMBOL_TO_STRING(BgL_initz00_2110);
											BgL_arg1906z00_2150 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2719);
										}
										{	/* Module/eval.scm 255 */
											obj_t BgL_arg1911z00_2152;

											BgL_arg1911z00_2152 =
												STRUCT_REF(BgL_infoz00_2107, (int) (6L));
											{	/* Module/eval.scm 255 */
												obj_t BgL_arg1455z00_2722;

												BgL_arg1455z00_2722 =
													SYMBOL_TO_STRING(((obj_t) BgL_arg1911z00_2152));
												BgL_arg1910z00_2151 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2722);
										}}
										BgL_arg1904z00_2149 =
											bigloo_module_mangle(BgL_arg1906z00_2150,
											BgL_arg1910z00_2151);
									}
									BgL_gloz00_2111 =
										BGl_declarezd2globalzd2cfunz12z12zzast_glozd2declzd2
										(BgL_initz00_2110, BgL_initz00_2110, BgL_arg1903z00_2148,
										BgL_arg1904z00_2149, CNST_TABLE_REF(24), CNST_TABLE_REF(25),
										((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE);
								}
								{	/* Module/eval.scm 250 */

									{	/* Module/eval.scm 257 */
										bool_t BgL_test2192z00_3694;

										{	/* Module/eval.scm 257 */
											obj_t BgL_arg1902z00_2147;

											BgL_arg1902z00_2147 =
												BGl_thezd2backendzd2zzbackend_backendz00();
											BgL_test2192z00_3694 =
												(((BgL_backendz00_bglt) COBJECT(
														((BgL_backendz00_bglt) BgL_arg1902z00_2147)))->
												BgL_pragmazd2supportzd2);
										}
										if (BgL_test2192z00_3694)
											{	/* Module/eval.scm 259 */
												obj_t BgL_arg1862z00_2114;

												{	/* Module/eval.scm 259 */
													obj_t BgL_arg1863z00_2115;
													obj_t BgL_arg1864z00_2116;

													{	/* Module/eval.scm 259 */
														obj_t BgL_arg1866z00_2117;
														obj_t BgL_arg1868z00_2118;

														{	/* Module/eval.scm 259 */
															obj_t BgL_arg1869z00_2119;

															{	/* Module/eval.scm 259 */
																obj_t BgL_arg1870z00_2120;

																BgL_arg1870z00_2120 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(26), BNIL);
																BgL_arg1869z00_2119 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(27),
																	BgL_arg1870z00_2120);
															}
															BgL_arg1866z00_2117 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																BgL_arg1869z00_2119);
														}
														{	/* Module/eval.scm 259 */
															obj_t BgL_arg1872z00_2121;
															obj_t BgL_arg1873z00_2122;

															{	/* Module/eval.scm 259 */
																obj_t BgL_arg1874z00_2123;

																BgL_arg1874z00_2123 =
																	MAKE_YOUNG_PAIR(BgL_libz00_83, BNIL);
																BgL_arg1872z00_2121 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																	BgL_arg1874z00_2123);
															}
															{	/* Module/eval.scm 259 */
																obj_t BgL_arg1875z00_2124;

																BgL_arg1875z00_2124 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(28), BNIL);
																BgL_arg1873z00_2122 =
																	MAKE_YOUNG_PAIR(BgL_arg1875z00_2124, BNIL);
															}
															BgL_arg1868z00_2118 =
																MAKE_YOUNG_PAIR(BgL_arg1872z00_2121,
																BgL_arg1873z00_2122);
														}
														BgL_arg1863z00_2115 =
															MAKE_YOUNG_PAIR(BgL_arg1866z00_2117,
															BgL_arg1868z00_2118);
													}
													{	/* Module/eval.scm 260 */
														obj_t BgL_arg1876z00_2125;
														obj_t BgL_arg1877z00_2126;

														{	/* Module/eval.scm 260 */
															obj_t BgL_arg1878z00_2127;

															{	/* Module/eval.scm 260 */
																obj_t BgL_arg1879z00_2128;

																{	/* Module/eval.scm 260 */
																	obj_t BgL_arg1880z00_2129;

																	{	/* Module/eval.scm 260 */
																		obj_t BgL_arg1882z00_2130;

																		{	/* Module/eval.scm 260 */
																			obj_t BgL_arg1883z00_2131;

																			{	/* Module/eval.scm 260 */
																				obj_t BgL_arg1884z00_2132;

																				{	/* Module/eval.scm 260 */
																					obj_t BgL_symbolz00_2724;

																					BgL_symbolz00_2724 =
																						BGl_za2moduleza2z00zzmodule_modulez00;
																					{	/* Module/eval.scm 260 */
																						obj_t BgL_arg1455z00_2725;

																						BgL_arg1455z00_2725 =
																							SYMBOL_TO_STRING
																							(BgL_symbolz00_2724);
																						BgL_arg1884z00_2132 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_2725);
																					}
																				}
																				{	/* Module/eval.scm 260 */
																					obj_t BgL_list1885z00_2133;

																					BgL_list1885z00_2133 =
																						MAKE_YOUNG_PAIR(BgL_arg1884z00_2132,
																						BNIL);
																					BgL_arg1883z00_2131 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string2090z00zzmodule_evalz00,
																						BgL_list1885z00_2133);
																				}
																			}
																			BgL_arg1882z00_2130 =
																				MAKE_YOUNG_PAIR(BgL_arg1883z00_2131,
																				BNIL);
																		}
																		BgL_arg1880z00_2129 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(29),
																			BgL_arg1882z00_2130);
																	}
																	BgL_arg1879z00_2128 =
																		MAKE_YOUNG_PAIR(BgL_arg1880z00_2129, BNIL);
																}
																BgL_arg1878z00_2127 =
																	MAKE_YOUNG_PAIR(BINT(0L),
																	BgL_arg1879z00_2128);
															}
															BgL_arg1876z00_2125 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_gloz00_2111), BgL_arg1878z00_2127);
														}
														{	/* Module/eval.scm 261 */
															obj_t BgL_arg1887z00_2134;

															{	/* Module/eval.scm 261 */
																obj_t BgL_arg1888z00_2135;
																obj_t BgL_arg1889z00_2136;

																{	/* Module/eval.scm 261 */
																	obj_t BgL_arg1890z00_2137;

																	{	/* Module/eval.scm 261 */
																		obj_t BgL_arg1891z00_2138;

																		BgL_arg1891z00_2138 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(26), BNIL);
																		BgL_arg1890z00_2137 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(30),
																			BgL_arg1891z00_2138);
																	}
																	BgL_arg1888z00_2135 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																		BgL_arg1890z00_2137);
																}
																{	/* Module/eval.scm 261 */
																	obj_t BgL_arg1892z00_2139;

																	{	/* Module/eval.scm 261 */
																		obj_t BgL_arg1893z00_2140;

																		BgL_arg1893z00_2140 =
																			MAKE_YOUNG_PAIR(BgL_libz00_83, BNIL);
																		BgL_arg1892z00_2139 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																			BgL_arg1893z00_2140);
																	}
																	BgL_arg1889z00_2136 =
																		MAKE_YOUNG_PAIR(BgL_arg1892z00_2139, BNIL);
																}
																BgL_arg1887z00_2134 =
																	MAKE_YOUNG_PAIR(BgL_arg1888z00_2135,
																	BgL_arg1889z00_2136);
															}
															BgL_arg1877z00_2126 =
																MAKE_YOUNG_PAIR(BgL_arg1887z00_2134, BNIL);
														}
														BgL_arg1864z00_2116 =
															MAKE_YOUNG_PAIR(BgL_arg1876z00_2125,
															BgL_arg1877z00_2126);
													}
													BgL_arg1862z00_2114 =
														MAKE_YOUNG_PAIR(BgL_arg1863z00_2115,
														BgL_arg1864z00_2116);
												}
												return
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
													BgL_arg1862z00_2114);
											}
										else
											{	/* Module/eval.scm 262 */
												obj_t BgL_arg1894z00_2141;
												obj_t BgL_arg1896z00_2142;

												{	/* Module/eval.scm 262 */
													obj_t BgL_arg1897z00_2143;

													{	/* Module/eval.scm 262 */
														obj_t BgL_arg1898z00_2144;

														BgL_arg1898z00_2144 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(26), BNIL);
														BgL_arg1897z00_2143 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(31),
															BgL_arg1898z00_2144);
													}
													BgL_arg1894z00_2141 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
														BgL_arg1897z00_2143);
												}
												{	/* Module/eval.scm 262 */
													obj_t BgL_arg1899z00_2145;

													{	/* Module/eval.scm 262 */
														obj_t BgL_arg1901z00_2146;

														BgL_arg1901z00_2146 =
															MAKE_YOUNG_PAIR(BgL_libz00_83, BNIL);
														BgL_arg1899z00_2145 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
															BgL_arg1901z00_2146);
													}
													BgL_arg1896z00_2142 =
														MAKE_YOUNG_PAIR(BgL_arg1899z00_2145, BNIL);
												}
												return
													MAKE_YOUNG_PAIR(BgL_arg1894z00_2141,
													BgL_arg1896z00_2142);
											}
									}
								}
							}
						}
					else
						{	/* Module/eval.scm 263 */
							obj_t BgL_list1913z00_2154;

							{	/* Module/eval.scm 263 */
								obj_t BgL_arg1914z00_2155;

								BgL_arg1914z00_2155 =
									MAKE_YOUNG_PAIR(BGl_string2091z00zzmodule_evalz00, BNIL);
								BgL_list1913z00_2154 =
									MAKE_YOUNG_PAIR(BgL_libz00_83, BgL_arg1914z00_2155);
							}
							BGL_TAIL return BGl_warningz00zz__errorz00(BgL_list1913z00_2154);
						}
				}
			}
		}

	}



/* set-eval-types! */
	obj_t BGl_setzd2evalzd2typesz12z12zzmodule_evalz00(obj_t BgL_globalz00_84)
	{
		{	/* Module/eval.scm 271 */
			{	/* Module/eval.scm 272 */
				BgL_valuez00_bglt BgL_valz00_2157;

				BgL_valz00_2157 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_84))))->BgL_valuez00);
				{	/* Module/eval.scm 273 */
					bool_t BgL_test2193z00_3757;

					{	/* Module/eval.scm 273 */
						obj_t BgL_classz00_2727;

						BgL_classz00_2727 = BGl_sfunz00zzast_varz00;
						{	/* Module/eval.scm 273 */
							BgL_objectz00_bglt BgL_arg1807z00_2729;

							{	/* Module/eval.scm 273 */
								obj_t BgL_tmpz00_3758;

								BgL_tmpz00_3758 =
									((obj_t) ((BgL_objectz00_bglt) BgL_valz00_2157));
								BgL_arg1807z00_2729 = (BgL_objectz00_bglt) (BgL_tmpz00_3758);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Module/eval.scm 273 */
									long BgL_idxz00_2735;

									BgL_idxz00_2735 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2729);
									BgL_test2193z00_3757 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2735 + 3L)) == BgL_classz00_2727);
								}
							else
								{	/* Module/eval.scm 273 */
									bool_t BgL_res2078z00_2760;

									{	/* Module/eval.scm 273 */
										obj_t BgL_oclassz00_2743;

										{	/* Module/eval.scm 273 */
											obj_t BgL_arg1815z00_2751;
											long BgL_arg1816z00_2752;

											BgL_arg1815z00_2751 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Module/eval.scm 273 */
												long BgL_arg1817z00_2753;

												BgL_arg1817z00_2753 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2729);
												BgL_arg1816z00_2752 =
													(BgL_arg1817z00_2753 - OBJECT_TYPE);
											}
											BgL_oclassz00_2743 =
												VECTOR_REF(BgL_arg1815z00_2751, BgL_arg1816z00_2752);
										}
										{	/* Module/eval.scm 273 */
											bool_t BgL__ortest_1115z00_2744;

											BgL__ortest_1115z00_2744 =
												(BgL_classz00_2727 == BgL_oclassz00_2743);
											if (BgL__ortest_1115z00_2744)
												{	/* Module/eval.scm 273 */
													BgL_res2078z00_2760 = BgL__ortest_1115z00_2744;
												}
											else
												{	/* Module/eval.scm 273 */
													long BgL_odepthz00_2745;

													{	/* Module/eval.scm 273 */
														obj_t BgL_arg1804z00_2746;

														BgL_arg1804z00_2746 = (BgL_oclassz00_2743);
														BgL_odepthz00_2745 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2746);
													}
													if ((3L < BgL_odepthz00_2745))
														{	/* Module/eval.scm 273 */
															obj_t BgL_arg1802z00_2748;

															{	/* Module/eval.scm 273 */
																obj_t BgL_arg1803z00_2749;

																BgL_arg1803z00_2749 = (BgL_oclassz00_2743);
																BgL_arg1802z00_2748 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2749,
																	3L);
															}
															BgL_res2078z00_2760 =
																(BgL_arg1802z00_2748 == BgL_classz00_2727);
														}
													else
														{	/* Module/eval.scm 273 */
															BgL_res2078z00_2760 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2193z00_3757 = BgL_res2078z00_2760;
								}
						}
					}
					if (BgL_test2193z00_3757)
						{	/* Module/eval.scm 273 */
							return BFALSE;
						}
					else
						{	/* Module/eval.scm 274 */
							BgL_typez00_bglt BgL_typez00_2159;

							BgL_typez00_2159 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_globalz00_84))))->BgL_typez00);
							if ((((obj_t) BgL_typez00_2159) == BGl_za2_za2z00zztype_cachez00))
								{	/* Module/eval.scm 277 */
									BgL_typez00_bglt BgL_vz00_2763;

									BgL_vz00_2763 =
										((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
									return
										((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_84))))->
											BgL_typez00) =
										((BgL_typez00_bglt) BgL_vz00_2763), BUNSPEC);
								}
							else
								{	/* Module/eval.scm 276 */
									if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_2159))
										{	/* Module/eval.scm 278 */
											return BFALSE;
										}
									else
										{	/* Module/eval.scm 281 */
											obj_t BgL_arg1917z00_2161;

											BgL_arg1917z00_2161 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_globalz00_84))))->
												BgL_idz00);
											return
												BGl_errorz00zz__errorz00
												(BGl_string2092z00zzmodule_evalz00,
												BGl_string2093z00zzmodule_evalz00, BgL_arg1917z00_2161);
										}
								}
						}
				}
			}
		}

	}



/* get-evaluated-globals */
	obj_t BGl_getzd2evaluatedzd2globalsz00zzmodule_evalz00(obj_t BgL_scopez00_85)
	{
		{	/* Module/eval.scm 286 */
			{	/* Module/eval.scm 287 */
				obj_t BgL_globalsz00_3023;

				BgL_globalsz00_3023 = MAKE_CELL(BNIL);
				{	/* Module/eval.scm 287 */

					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_scopez00_85,
								CNST_TABLE_REF(32))))
						{	/* Module/eval.scm 289 */
							obj_t BgL_scopezd2lstzd2_2164;

							if ((BgL_scopez00_85 == CNST_TABLE_REF(12)))
								{	/* Module/eval.scm 289 */
									BgL_scopezd2lstzd2_2164 = CNST_TABLE_REF(33);
								}
							else
								{	/* Module/eval.scm 289 */
									if ((BgL_scopez00_85 == CNST_TABLE_REF(19)))
										{	/* Module/eval.scm 289 */
											BgL_scopezd2lstzd2_2164 = CNST_TABLE_REF(34);
										}
									else
										{	/* Module/eval.scm 289 */
											BgL_scopezd2lstzd2_2164 = CNST_TABLE_REF(34);
										}
								}
							{	/* Module/eval.scm 295 */
								obj_t BgL_zc3z04anonymousza31921ze3z87_3017;

								BgL_zc3z04anonymousza31921ze3z87_3017 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31921ze3ze5zzmodule_evalz00,
									(int) (1L), (int) (2L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31921ze3z87_3017, (int) (0L),
									((obj_t) BgL_globalsz00_3023));
								PROCEDURE_SET(BgL_zc3z04anonymousza31921ze3z87_3017, (int) (1L),
									BgL_scopezd2lstzd2_2164);
								BGl_forzd2eachzd2globalz12z12zzast_envz00
									(BgL_zc3z04anonymousza31921ze3z87_3017, BNIL);
						}}
					else
						{	/* Module/eval.scm 288 */
							BFALSE;
						}
					{
						obj_t BgL_evalzd2exportedzd2_2186;
						obj_t BgL_resz00_2187;

						BgL_evalzd2exportedzd2_2186 =
							BGl_za2evalzd2exportedza2zd2zzmodule_evalz00;
						BgL_resz00_2187 = CELL_REF(BgL_globalsz00_3023);
					BgL_zc3z04anonymousza31932ze3z87_2188:
						if (NULLP(BgL_evalzd2exportedzd2_2186))
							{	/* Module/eval.scm 301 */
								return BgL_resz00_2187;
							}
						else
							{	/* Module/eval.scm 303 */
								obj_t BgL_varzd2modulezd2posz00_2190;

								BgL_varzd2modulezd2posz00_2190 =
									CAR(((obj_t) BgL_evalzd2exportedzd2_2186));
								{	/* Module/eval.scm 304 */
									obj_t BgL_gz00_2191;

									{	/* Module/eval.scm 304 */
										bool_t BgL_test2203z00_3823;

										{	/* Module/eval.scm 304 */
											obj_t BgL_pairz00_2774;

											BgL_pairz00_2774 =
												CDR(((obj_t) BgL_varzd2modulezd2posz00_2190));
											BgL_test2203z00_3823 = CBOOL(CAR(BgL_pairz00_2774));
										}
										if (BgL_test2203z00_3823)
											{	/* Module/eval.scm 305 */
												obj_t BgL_arg1949z00_2207;
												obj_t BgL_arg1950z00_2208;

												BgL_arg1949z00_2207 =
													CAR(((obj_t) BgL_varzd2modulezd2posz00_2190));
												{	/* Module/eval.scm 306 */
													obj_t BgL_pairz00_2779;

													BgL_pairz00_2779 =
														CDR(((obj_t) BgL_varzd2modulezd2posz00_2190));
													BgL_arg1950z00_2208 = CAR(BgL_pairz00_2779);
												}
												BgL_gz00_2191 =
													BGl_findzd2globalzf2modulez20zzast_envz00
													(BgL_arg1949z00_2207, BgL_arg1950z00_2208);
											}
										else
											{	/* Module/eval.scm 307 */
												obj_t BgL_arg1951z00_2209;

												BgL_arg1951z00_2209 =
													CAR(((obj_t) BgL_varzd2modulezd2posz00_2190));
												BgL_gz00_2191 =
													BGl_findzd2globalzd2zzast_envz00(BgL_arg1951z00_2209,
													BNIL);
											}
									}
									{	/* Module/eval.scm 309 */
										bool_t BgL_test2204z00_3837;

										{	/* Module/eval.scm 309 */
											obj_t BgL_classz00_2781;

											BgL_classz00_2781 = BGl_globalz00zzast_varz00;
											if (BGL_OBJECTP(BgL_gz00_2191))
												{	/* Module/eval.scm 309 */
													BgL_objectz00_bglt BgL_arg1807z00_2783;

													BgL_arg1807z00_2783 =
														(BgL_objectz00_bglt) (BgL_gz00_2191);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Module/eval.scm 309 */
															long BgL_idxz00_2789;

															BgL_idxz00_2789 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2783);
															BgL_test2204z00_3837 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2789 + 2L)) == BgL_classz00_2781);
														}
													else
														{	/* Module/eval.scm 309 */
															bool_t BgL_res2079z00_2814;

															{	/* Module/eval.scm 309 */
																obj_t BgL_oclassz00_2797;

																{	/* Module/eval.scm 309 */
																	obj_t BgL_arg1815z00_2805;
																	long BgL_arg1816z00_2806;

																	BgL_arg1815z00_2805 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Module/eval.scm 309 */
																		long BgL_arg1817z00_2807;

																		BgL_arg1817z00_2807 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2783);
																		BgL_arg1816z00_2806 =
																			(BgL_arg1817z00_2807 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2797 =
																		VECTOR_REF(BgL_arg1815z00_2805,
																		BgL_arg1816z00_2806);
																}
																{	/* Module/eval.scm 309 */
																	bool_t BgL__ortest_1115z00_2798;

																	BgL__ortest_1115z00_2798 =
																		(BgL_classz00_2781 == BgL_oclassz00_2797);
																	if (BgL__ortest_1115z00_2798)
																		{	/* Module/eval.scm 309 */
																			BgL_res2079z00_2814 =
																				BgL__ortest_1115z00_2798;
																		}
																	else
																		{	/* Module/eval.scm 309 */
																			long BgL_odepthz00_2799;

																			{	/* Module/eval.scm 309 */
																				obj_t BgL_arg1804z00_2800;

																				BgL_arg1804z00_2800 =
																					(BgL_oclassz00_2797);
																				BgL_odepthz00_2799 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2800);
																			}
																			if ((2L < BgL_odepthz00_2799))
																				{	/* Module/eval.scm 309 */
																					obj_t BgL_arg1802z00_2802;

																					{	/* Module/eval.scm 309 */
																						obj_t BgL_arg1803z00_2803;

																						BgL_arg1803z00_2803 =
																							(BgL_oclassz00_2797);
																						BgL_arg1802z00_2802 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2803, 2L);
																					}
																					BgL_res2079z00_2814 =
																						(BgL_arg1802z00_2802 ==
																						BgL_classz00_2781);
																				}
																			else
																				{	/* Module/eval.scm 309 */
																					BgL_res2079z00_2814 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2204z00_3837 = BgL_res2079z00_2814;
														}
												}
											else
												{	/* Module/eval.scm 309 */
													BgL_test2204z00_3837 = ((bool_t) 0);
												}
										}
										if (BgL_test2204z00_3837)
											{	/* Module/eval.scm 309 */
												if (
													(((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_gz00_2191)))->
														BgL_evaluablezf3zf3))
													{	/* Module/eval.scm 326 */
														obj_t BgL_arg1936z00_2194;
														obj_t BgL_arg1937z00_2195;

														BgL_arg1936z00_2194 =
															CDR(((obj_t) BgL_evalzd2exportedzd2_2186));
														BgL_arg1937z00_2195 =
															MAKE_YOUNG_PAIR(BgL_gz00_2191, BgL_resz00_2187);
														{
															obj_t BgL_resz00_3867;
															obj_t BgL_evalzd2exportedzd2_3866;

															BgL_evalzd2exportedzd2_3866 = BgL_arg1936z00_2194;
															BgL_resz00_3867 = BgL_arg1937z00_2195;
															BgL_resz00_2187 = BgL_resz00_3867;
															BgL_evalzd2exportedzd2_2186 =
																BgL_evalzd2exportedzd2_3866;
															goto BgL_zc3z04anonymousza31932ze3z87_2188;
														}
													}
												else
													{	/* Module/eval.scm 317 */
														{	/* Module/eval.scm 319 */
															obj_t BgL_arg1938z00_2196;
															obj_t BgL_arg1939z00_2197;

															{	/* Module/eval.scm 319 */
																obj_t BgL_arg1941z00_2199;

																{	/* Module/eval.scm 319 */
																	obj_t BgL_pairz00_2822;

																	{	/* Module/eval.scm 319 */
																		obj_t BgL_pairz00_2821;

																		BgL_pairz00_2821 =
																			CDR(
																			((obj_t) BgL_varzd2modulezd2posz00_2190));
																		BgL_pairz00_2822 = CDR(BgL_pairz00_2821);
																	}
																	BgL_arg1941z00_2199 = CAR(BgL_pairz00_2822);
																}
																BgL_arg1938z00_2196 =
																	BGl_findzd2locationzd2zztools_locationz00
																	(BgL_arg1941z00_2199);
															}
															BgL_arg1939z00_2197 =
																CAR(((obj_t) BgL_varzd2modulezd2posz00_2190));
															{	/* Module/eval.scm 318 */
																obj_t BgL_list1940z00_2198;

																BgL_list1940z00_2198 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																BGl_userzd2errorzf2locationz20zztools_errorz00
																	(BgL_arg1938z00_2196,
																	BGl_string2094z00zzmodule_evalz00,
																	BGl_string2095z00zzmodule_evalz00,
																	BgL_arg1939z00_2197, BgL_list1940z00_2198);
															}
														}
														{	/* Module/eval.scm 324 */
															obj_t BgL_arg1942z00_2200;

															BgL_arg1942z00_2200 =
																CDR(((obj_t) BgL_evalzd2exportedzd2_2186));
															{
																obj_t BgL_evalzd2exportedzd2_3879;

																BgL_evalzd2exportedzd2_3879 =
																	BgL_arg1942z00_2200;
																BgL_evalzd2exportedzd2_2186 =
																	BgL_evalzd2exportedzd2_3879;
																goto BgL_zc3z04anonymousza31932ze3z87_2188;
															}
														}
													}
											}
										else
											{	/* Module/eval.scm 309 */
												{	/* Module/eval.scm 311 */
													obj_t BgL_arg1943z00_2201;
													obj_t BgL_arg1944z00_2202;

													{	/* Module/eval.scm 311 */
														obj_t BgL_arg1946z00_2204;

														{	/* Module/eval.scm 311 */
															obj_t BgL_pairz00_2830;

															{	/* Module/eval.scm 311 */
																obj_t BgL_pairz00_2829;

																BgL_pairz00_2829 =
																	CDR(((obj_t) BgL_varzd2modulezd2posz00_2190));
																BgL_pairz00_2830 = CDR(BgL_pairz00_2829);
															}
															BgL_arg1946z00_2204 = CAR(BgL_pairz00_2830);
														}
														BgL_arg1943z00_2201 =
															BGl_findzd2locationzd2zztools_locationz00
															(BgL_arg1946z00_2204);
													}
													BgL_arg1944z00_2202 =
														CAR(((obj_t) BgL_varzd2modulezd2posz00_2190));
													{	/* Module/eval.scm 310 */
														obj_t BgL_list1945z00_2203;

														BgL_list1945z00_2203 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														BGl_userzd2errorzf2locationz20zztools_errorz00
															(BgL_arg1943z00_2201,
															BGl_string2094z00zzmodule_evalz00,
															BGl_string2096z00zzmodule_evalz00,
															BgL_arg1944z00_2202, BgL_list1945z00_2203);
													}
												}
												{	/* Module/eval.scm 316 */
													obj_t BgL_arg1947z00_2205;

													BgL_arg1947z00_2205 =
														CDR(((obj_t) BgL_evalzd2exportedzd2_2186));
													{
														obj_t BgL_evalzd2exportedzd2_3891;

														BgL_evalzd2exportedzd2_3891 = BgL_arg1947z00_2205;
														BgL_evalzd2exportedzd2_2186 =
															BgL_evalzd2exportedzd2_3891;
														goto BgL_zc3z04anonymousza31932ze3z87_2188;
													}
												}
											}
									}
								}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1921> */
	obj_t BGl_z62zc3z04anonymousza31921ze3ze5zzmodule_evalz00(obj_t
		BgL_envz00_3018, obj_t BgL_gz00_3021)
	{
		{	/* Module/eval.scm 294 */
			{	/* Module/eval.scm 295 */
				obj_t BgL_globalsz00_3019;
				obj_t BgL_scopezd2lstzd2_3020;

				BgL_globalsz00_3019 = PROCEDURE_REF(BgL_envz00_3018, (int) (0L));
				BgL_scopezd2lstzd2_3020 =
					((obj_t) PROCEDURE_REF(BgL_envz00_3018, (int) (1L)));
				{	/* Module/eval.scm 295 */
					bool_t BgL_test2210z00_3897;

					if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
								(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_gz00_3021)))->BgL_importz00),
								BgL_scopezd2lstzd2_3020)))
						{	/* Module/eval.scm 295 */
							if (
								(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_gz00_3021)))->
									BgL_evaluablezf3zf3))
								{	/* Module/eval.scm 297 */
									obj_t BgL__ortest_1113z00_3123;

									BgL__ortest_1113z00_3123 =
										BGl_za2libzd2modeza2zd2zzengine_paramz00;
									if (CBOOL(BgL__ortest_1113z00_3123))
										{	/* Module/eval.scm 297 */
											BgL_test2210z00_3897 = CBOOL(BgL__ortest_1113z00_3123);
										}
									else
										{	/* Module/eval.scm 297 */
											if (CBOOL(
													(((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_gz00_3021)))->
														BgL_libraryz00)))
												{	/* Module/eval.scm 297 */
													BgL_test2210z00_3897 = ((bool_t) 0);
												}
											else
												{	/* Module/eval.scm 297 */
													BgL_test2210z00_3897 = ((bool_t) 1);
												}
										}
								}
							else
								{	/* Module/eval.scm 296 */
									BgL_test2210z00_3897 = ((bool_t) 0);
								}
						}
					else
						{	/* Module/eval.scm 295 */
							BgL_test2210z00_3897 = ((bool_t) 0);
						}
					if (BgL_test2210z00_3897)
						{	/* Module/eval.scm 298 */
							obj_t BgL_auxz00_3124;

							BgL_auxz00_3124 =
								MAKE_YOUNG_PAIR(BgL_gz00_3021, CELL_REF(BgL_globalsz00_3019));
							return CELL_SET(BgL_globalsz00_3019, BgL_auxz00_3124);
						}
					else
						{	/* Module/eval.scm 295 */
							return BFALSE;
						}
				}
			}
		}

	}



/* get-eval-srfi-libraries */
	obj_t BGl_getzd2evalzd2srfizd2librarieszd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 337 */
			{	/* Module/eval.scm 338 */
				obj_t BgL_l1261z00_2212;

				BgL_l1261z00_2212 = BGl_za2evalzd2librariesza2zd2zzmodule_evalz00;
				if (NULLP(BgL_l1261z00_2212))
					{	/* Module/eval.scm 338 */
						return BNIL;
					}
				else
					{	/* Module/eval.scm 338 */
						obj_t BgL_head1263z00_2214;

						BgL_head1263z00_2214 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1261z00_2216;
							obj_t BgL_tail1264z00_2217;

							BgL_l1261z00_2216 = BgL_l1261z00_2212;
							BgL_tail1264z00_2217 = BgL_head1263z00_2214;
						BgL_zc3z04anonymousza31954ze3z87_2218:
							if (NULLP(BgL_l1261z00_2216))
								{	/* Module/eval.scm 338 */
									return CDR(BgL_head1263z00_2214);
								}
							else
								{	/* Module/eval.scm 338 */
									obj_t BgL_newtail1265z00_2220;

									{	/* Module/eval.scm 338 */
										obj_t BgL_arg1957z00_2222;

										{	/* Module/eval.scm 338 */
											obj_t BgL_lz00_2223;

											BgL_lz00_2223 = CAR(((obj_t) BgL_l1261z00_2216));
											{	/* Module/eval.scm 340 */
												obj_t BgL_arg1958z00_2224;

												{	/* Module/eval.scm 340 */
													obj_t BgL_arg1959z00_2225;

													{	/* Module/eval.scm 340 */
														obj_t BgL_arg1960z00_2226;

														{	/* Module/eval.scm 340 */
															obj_t BgL_arg1961z00_2227;

															{	/* Module/eval.scm 340 */
																obj_t BgL_arg1962z00_2228;

																BgL_arg1962z00_2228 =
																	MAKE_YOUNG_PAIR(BgL_lz00_2223, BNIL);
																BgL_arg1961z00_2227 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																	BgL_arg1962z00_2228);
															}
															BgL_arg1960z00_2226 =
																MAKE_YOUNG_PAIR(BgL_arg1961z00_2227, BNIL);
														}
														BgL_arg1959z00_2225 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(35),
															BgL_arg1960z00_2226);
													}
													BgL_arg1958z00_2224 =
														MAKE_YOUNG_PAIR(BgL_arg1959z00_2225, BNIL);
												}
												BgL_arg1957z00_2222 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
													BgL_arg1958z00_2224);
											}
										}
										BgL_newtail1265z00_2220 =
											MAKE_YOUNG_PAIR(BgL_arg1957z00_2222, BNIL);
									}
									SET_CDR(BgL_tail1264z00_2217, BgL_newtail1265z00_2220);
									{	/* Module/eval.scm 338 */
										obj_t BgL_arg1956z00_2221;

										BgL_arg1956z00_2221 = CDR(((obj_t) BgL_l1261z00_2216));
										{
											obj_t BgL_tail1264z00_3936;
											obj_t BgL_l1261z00_3935;

											BgL_l1261z00_3935 = BgL_arg1956z00_2221;
											BgL_tail1264z00_3936 = BgL_newtail1265z00_2220;
											BgL_tail1264z00_2217 = BgL_tail1264z00_3936;
											BgL_l1261z00_2216 = BgL_l1261z00_3935;
											goto BgL_zc3z04anonymousza31954ze3z87_2218;
										}
									}
								}
						}
					}
			}
		}

	}



/* get-evaluated-class-macros */
	obj_t BGl_getzd2evaluatedzd2classzd2macroszd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 346 */
			{	/* Module/eval.scm 347 */
				obj_t BgL_l1266z00_2230;

				BgL_l1266z00_2230 = BGl_za2evalzd2classesza2zd2zzmodule_evalz00;
				if (NULLP(BgL_l1266z00_2230))
					{	/* Module/eval.scm 347 */
						return BNIL;
					}
				else
					{	/* Module/eval.scm 347 */
						obj_t BgL_head1268z00_2232;

						BgL_head1268z00_2232 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1266z00_2234;
							obj_t BgL_tail1269z00_2235;

							BgL_l1266z00_2234 = BgL_l1266z00_2230;
							BgL_tail1269z00_2235 = BgL_head1268z00_2232;
						BgL_zc3z04anonymousza31964ze3z87_2236:
							if (NULLP(BgL_l1266z00_2234))
								{	/* Module/eval.scm 347 */
									return CDR(BgL_head1268z00_2232);
								}
							else
								{	/* Module/eval.scm 347 */
									obj_t BgL_newtail1270z00_2238;

									{	/* Module/eval.scm 347 */
										obj_t BgL_arg1967z00_2240;

										{	/* Module/eval.scm 347 */
											obj_t BgL_sz00_2241;

											BgL_sz00_2241 = CAR(((obj_t) BgL_l1266z00_2234));
											{	/* Module/eval.scm 348 */
												BgL_typez00_bglt BgL_tz00_2242;

												{	/* Module/eval.scm 348 */
													obj_t BgL_arg1988z00_2267;
													obj_t BgL_arg1989z00_2268;

													{	/* Module/eval.scm 348 */
														obj_t BgL_pairz00_2842;

														BgL_pairz00_2842 = CDR(((obj_t) BgL_sz00_2241));
														BgL_arg1988z00_2267 = CAR(BgL_pairz00_2842);
													}
													BgL_arg1989z00_2268 =
														BGl_findzd2locationzd2zztools_locationz00
														(BgL_sz00_2241);
													BgL_tz00_2242 =
														BGl_findzd2typezf2locationz20zztype_envz00
														(BgL_arg1988z00_2267, BgL_arg1989z00_2268);
												}
												{	/* Module/eval.scm 350 */
													BgL_globalz00_bglt BgL_holderz00_2245;

													{
														BgL_tclassz00_bglt BgL_auxz00_3950;

														{
															obj_t BgL_auxz00_3951;

															{	/* Module/eval.scm 351 */
																BgL_objectz00_bglt BgL_tmpz00_3952;

																BgL_tmpz00_3952 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_tz00_2242));
																BgL_auxz00_3951 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3952);
															}
															BgL_auxz00_3950 =
																((BgL_tclassz00_bglt) BgL_auxz00_3951);
														}
														BgL_holderz00_2245 =
															(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_3950))->
															BgL_holderz00);
													}
													{	/* Module/eval.scm 351 */
														obj_t BgL_holderez00_2246;

														{	/* Module/eval.scm 352 */
															obj_t BgL_arg1983z00_2262;

															{	/* Module/eval.scm 352 */
																obj_t BgL_arg1984z00_2263;
																obj_t BgL_arg1985z00_2264;

																BgL_arg1984z00_2263 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_holderz00_2245)))->BgL_idz00);
																BgL_arg1985z00_2264 =
																	MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																			COBJECT(BgL_holderz00_2245))->
																		BgL_modulez00), BNIL);
																BgL_arg1983z00_2262 =
																	MAKE_YOUNG_PAIR(BgL_arg1984z00_2263,
																	BgL_arg1985z00_2264);
															}
															BgL_holderez00_2246 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																BgL_arg1983z00_2262);
														}
														{	/* Module/eval.scm 352 */

															{	/* Module/eval.scm 354 */
																obj_t BgL_arg1968z00_2247;

																{	/* Module/eval.scm 354 */
																	obj_t BgL_arg1969z00_2248;
																	obj_t BgL_arg1970z00_2249;

																	{	/* Module/eval.scm 354 */
																		bool_t BgL_test2219z00_3965;

																		{
																			BgL_tclassz00_bglt BgL_auxz00_3966;

																			{
																				obj_t BgL_auxz00_3967;

																				{	/* Module/eval.scm 354 */
																					BgL_objectz00_bglt BgL_tmpz00_3968;

																					BgL_tmpz00_3968 =
																						((BgL_objectz00_bglt)
																						((BgL_typez00_bglt) BgL_tz00_2242));
																					BgL_auxz00_3967 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_3968);
																				}
																				BgL_auxz00_3966 =
																					((BgL_tclassz00_bglt)
																					BgL_auxz00_3967);
																			}
																			BgL_test2219z00_3965 =
																				(((BgL_tclassz00_bglt)
																					COBJECT(BgL_auxz00_3966))->
																				BgL_abstractzf3zf3);
																		}
																		if (BgL_test2219z00_3965)
																			{	/* Module/eval.scm 354 */
																				BgL_arg1969z00_2248 = BUNSPEC;
																			}
																		else
																			{	/* Module/eval.scm 357 */
																				obj_t BgL_arg1972z00_2251;

																				{	/* Module/eval.scm 357 */
																					obj_t BgL_arg1973z00_2252;
																					obj_t BgL_arg1974z00_2253;

																					{	/* Module/eval.scm 357 */
																						obj_t BgL_arg1975z00_2254;

																						BgL_arg1975z00_2254 =
																							MAKE_YOUNG_PAIR
																							(BgL_holderez00_2246, BNIL);
																						BgL_arg1973z00_2252 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(36), BgL_arg1975z00_2254);
																					}
																					{	/* Module/eval.scm 358 */
																						obj_t BgL_arg1976z00_2255;

																						{	/* Module/eval.scm 358 */
																							obj_t BgL_arg1977z00_2256;

																							BgL_arg1977z00_2256 =
																								MAKE_YOUNG_PAIR
																								(BgL_holderez00_2246, BNIL);
																							BgL_arg1976z00_2255 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(37), BgL_arg1977z00_2256);
																						}
																						BgL_arg1974z00_2253 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1976z00_2255, BNIL);
																					}
																					BgL_arg1972z00_2251 =
																						MAKE_YOUNG_PAIR(BgL_arg1973z00_2252,
																						BgL_arg1974z00_2253);
																				}
																				BgL_arg1969z00_2248 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																					BgL_arg1972z00_2251);
																			}
																	}
																	{	/* Module/eval.scm 359 */
																		obj_t BgL_arg1978z00_2257;
																		obj_t BgL_arg1979z00_2258;

																		{	/* Module/eval.scm 359 */
																			obj_t BgL_arg1980z00_2259;

																			BgL_arg1980z00_2259 =
																				MAKE_YOUNG_PAIR(BgL_holderez00_2246,
																				BNIL);
																			BgL_arg1978z00_2257 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(38),
																				BgL_arg1980z00_2259);
																		}
																		{	/* Module/eval.scm 360 */
																			obj_t BgL_arg1981z00_2260;

																			{	/* Module/eval.scm 360 */
																				obj_t BgL_arg1982z00_2261;

																				BgL_arg1982z00_2261 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				BgL_arg1981z00_2260 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																					BgL_arg1982z00_2261);
																			}
																			BgL_arg1979z00_2258 =
																				MAKE_YOUNG_PAIR(BgL_arg1981z00_2260,
																				BNIL);
																		}
																		BgL_arg1970z00_2249 =
																			MAKE_YOUNG_PAIR(BgL_arg1978z00_2257,
																			BgL_arg1979z00_2258);
																	}
																	BgL_arg1968z00_2247 =
																		MAKE_YOUNG_PAIR(BgL_arg1969z00_2248,
																		BgL_arg1970z00_2249);
																}
																BgL_arg1967z00_2240 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																	BgL_arg1968z00_2247);
															}
														}
													}
												}
											}
										}
										BgL_newtail1270z00_2238 =
											MAKE_YOUNG_PAIR(BgL_arg1967z00_2240, BNIL);
									}
									SET_CDR(BgL_tail1269z00_2235, BgL_newtail1270z00_2238);
									{	/* Module/eval.scm 347 */
										obj_t BgL_arg1966z00_2239;

										BgL_arg1966z00_2239 = CDR(((obj_t) BgL_l1266z00_2234));
										{
											obj_t BgL_tail1269z00_4000;
											obj_t BgL_l1266z00_3999;

											BgL_l1266z00_3999 = BgL_arg1966z00_2239;
											BgL_tail1269z00_4000 = BgL_newtail1270z00_2238;
											BgL_tail1269z00_2235 = BgL_tail1269z00_4000;
											BgL_l1266z00_2234 = BgL_l1266z00_3999;
											goto BgL_zc3z04anonymousza31964ze3z87_2236;
										}
									}
								}
						}
					}
			}
		}

	}



/* get-evaluated-class-holders */
	obj_t BGl_getzd2evaluatedzd2classzd2holderszd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 366 */
			{	/* Module/eval.scm 367 */
				obj_t BgL_l1271z00_2270;

				BgL_l1271z00_2270 = BGl_za2evalzd2classesza2zd2zzmodule_evalz00;
				if (NULLP(BgL_l1271z00_2270))
					{	/* Module/eval.scm 367 */
						return BNIL;
					}
				else
					{	/* Module/eval.scm 367 */
						obj_t BgL_head1273z00_2272;

						BgL_head1273z00_2272 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1271z00_2274;
							obj_t BgL_tail1274z00_2275;

							BgL_l1271z00_2274 = BgL_l1271z00_2270;
							BgL_tail1274z00_2275 = BgL_head1273z00_2272;
						BgL_zc3z04anonymousza31991ze3z87_2276:
							if (NULLP(BgL_l1271z00_2274))
								{	/* Module/eval.scm 367 */
									return CDR(BgL_head1273z00_2272);
								}
							else
								{	/* Module/eval.scm 367 */
									obj_t BgL_newtail1275z00_2278;

									{	/* Module/eval.scm 367 */
										BgL_globalz00_bglt BgL_arg1994z00_2280;

										{	/* Module/eval.scm 367 */
											obj_t BgL_sz00_2281;

											BgL_sz00_2281 = CAR(((obj_t) BgL_l1271z00_2274));
											{	/* Module/eval.scm 368 */
												BgL_typez00_bglt BgL_tz00_2282;

												{	/* Module/eval.scm 368 */
													obj_t BgL_arg1995z00_2283;
													obj_t BgL_arg1996z00_2284;

													{	/* Module/eval.scm 368 */
														obj_t BgL_pairz00_2861;

														BgL_pairz00_2861 = CDR(((obj_t) BgL_sz00_2281));
														BgL_arg1995z00_2283 = CAR(BgL_pairz00_2861);
													}
													BgL_arg1996z00_2284 =
														BGl_findzd2locationzd2zztools_locationz00
														(BgL_sz00_2281);
													BgL_tz00_2282 =
														BGl_findzd2typezf2locationz20zztype_envz00
														(BgL_arg1995z00_2283, BgL_arg1996z00_2284);
												}
												{
													BgL_tclassz00_bglt BgL_auxz00_4014;

													{
														obj_t BgL_auxz00_4015;

														{	/* Module/eval.scm 369 */
															BgL_objectz00_bglt BgL_tmpz00_4016;

															BgL_tmpz00_4016 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_tz00_2282));
															BgL_auxz00_4015 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_4016);
														}
														BgL_auxz00_4014 =
															((BgL_tclassz00_bglt) BgL_auxz00_4015);
													}
													BgL_arg1994z00_2280 =
														(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_4014))->
														BgL_holderz00);
												}
											}
										}
										BgL_newtail1275z00_2278 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg1994z00_2280), BNIL);
									}
									SET_CDR(BgL_tail1274z00_2275, BgL_newtail1275z00_2278);
									{	/* Module/eval.scm 367 */
										obj_t BgL_arg1993z00_2279;

										BgL_arg1993z00_2279 = CDR(((obj_t) BgL_l1271z00_2274));
										{
											obj_t BgL_tail1274z00_4028;
											obj_t BgL_l1271z00_4027;

											BgL_l1271z00_4027 = BgL_arg1993z00_2279;
											BgL_tail1274z00_4028 = BgL_newtail1275z00_2278;
											BgL_tail1274z00_2275 = BgL_tail1274z00_4028;
											BgL_l1271z00_2274 = BgL_l1271z00_4027;
											goto BgL_zc3z04anonymousza31991ze3z87_2276;
										}
									}
								}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_evalz00(void)
	{
		{	/* Module/eval.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2097z00zzmodule_evalz00));
		}

	}

#ifdef __cplusplus
}
#endif
