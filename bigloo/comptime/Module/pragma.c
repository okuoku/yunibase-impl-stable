/*===========================================================================*/
/*   (Module/pragma.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/pragma.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_PRAGMA_TYPE_DEFINITIONS
#define BGL_MODULE_PRAGMA_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_feffectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_readz00;
		obj_t BgL_writez00;
	}                 *BgL_feffectz00_bglt;


#endif													// BGL_MODULE_PRAGMA_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzmodule_pragmaz00 = BUNSPEC;
	extern obj_t BGl_funz00zzast_varz00;
	static bool_t BGl_setzd2pragmazd2propertiesz12z12zzmodule_pragmaz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzmodule_pragmaz00(void);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_pragmaz00(void);
	static obj_t BGl_objectzd2initzd2zzmodule_pragmaz00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31234ze3ze5zzmodule_pragmaz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzmodule_pragmaz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_pragmaz00(void);
	extern obj_t BGl_za2allzd2exportzd2mutablezf3za2zf3zzmodule_evalz00;
	static obj_t BGl_z62zc3z04anonymousza31237ze3ze5zzmodule_pragmaz00(obj_t,
		obj_t);
	extern obj_t BGl_removezd2varzd2fromz12z12zzast_removez00(obj_t,
		BgL_variablez00_bglt);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t BGl_pragmazd2finaliza7erz75zzmodule_pragmaz00(void);
	static obj_t BGl_za2pragmazd2listza2zd2zzmodule_pragmaz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_pragmaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_feffectz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_evalz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_pragmazd2parserzd2zzmodule_pragmaz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzmodule_pragmaz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_pragmaz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_pragmaz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_pragmaz00(void);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62pragmazd2finaliza7erz17zzmodule_pragmaz00(obj_t);
	static obj_t BGl_argszd2noescapezd2zzmodule_pragmaz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_pragmazd2producerzd2zzmodule_pragmaz00(obj_t);
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_sfunzd2errorzd2zzmodule_pragmaz00(obj_t, BgL_globalz00_bglt);
	extern BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62makezd2pragmazd2compilerz62zzmodule_pragmaz00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern BgL_feffectz00_bglt BGl_parsezd2effectzd2zzeffect_feffectz00(obj_t);
	static obj_t BGl_setzd2pragmazd2propertyz12z12zzmodule_pragmaz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2pragmazd2compilerz00zzmodule_pragmaz00(void);
	static obj_t __cnst[22];


	extern obj_t BGl_funzd2argszd2noescapezd2setz12zd2envz12zzast_varz00;
	   
		 
		DEFINE_STRING(BGl_string1800z00zzmodule_pragmaz00,
		BgL_bgl_string1800za700za7za7m1821za7, ")", 1);
	      DEFINE_STRING(BGl_string1801z00zzmodule_pragmaz00,
		BgL_bgl_string1801za700za7za7m1822za7,
		"property is not concerning a function", 37);
	      DEFINE_STRING(BGl_string1802z00zzmodule_pragmaz00,
		BgL_bgl_string1802za700za7za7m1823za7, "side-effect-free", 16);
	      DEFINE_STRING(BGl_string1803z00zzmodule_pragmaz00,
		BgL_bgl_string1803za700za7za7m1824za7, "no-cfa-top", 10);
	      DEFINE_STRING(BGl_string1804z00zzmodule_pragmaz00,
		BgL_bgl_string1804za700za7za7m1825za7, "no-init-flow", 12);
	      DEFINE_STRING(BGl_string1805z00zzmodule_pragmaz00,
		BgL_bgl_string1805za700za7za7m1826za7, "allocator", 9);
	      DEFINE_STRING(BGl_string1806z00zzmodule_pragmaz00,
		BgL_bgl_string1806za700za7za7m1827za7, "no-trace", 8);
	      DEFINE_STRING(BGl_string1807z00zzmodule_pragmaz00,
		BgL_bgl_string1807za700za7za7m1828za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1808z00zzmodule_pragmaz00,
		BgL_bgl_string1808za700za7za7m1829za7, "Illegal \"pragma\" form", 21);
	      DEFINE_STRING(BGl_string1809z00zzmodule_pragmaz00,
		BgL_bgl_string1809za700za7za7m1830za7, "predicate-of", 12);
	      DEFINE_STRING(BGl_string1810z00zzmodule_pragmaz00,
		BgL_bgl_string1810za700za7za7m1831za7, "Illegal \"predicate-of\" pragma",
		29);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2pragmazd2compilerzd2envzd2zzmodule_pragmaz00,
		BgL_bgl_za762makeza7d2pragma1832z00,
		BGl_z62makezd2pragmazd2compilerz62zzmodule_pragmaz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1811z00zzmodule_pragmaz00,
		BgL_bgl_string1811za700za7za7m1833za7, "effect", 6);
	      DEFINE_STRING(BGl_string1812z00zzmodule_pragmaz00,
		BgL_bgl_string1812za700za7za7m1834za7, "stack-allocator", 15);
	      DEFINE_STRING(BGl_string1813z00zzmodule_pragmaz00,
		BgL_bgl_string1813za700za7za7m1835za7, "Illegal \"pragma\" form (~a)", 26);
	      DEFINE_STRING(BGl_string1814z00zzmodule_pragmaz00,
		BgL_bgl_string1814za700za7za7m1836za7,
		"Illegal \"args-noescape\", cannot find argument", 45);
	      DEFINE_STRING(BGl_string1815z00zzmodule_pragmaz00,
		BgL_bgl_string1815za700za7za7m1837za7,
		"Illegal \"args-noescape\" on non-function value", 45);
	      DEFINE_STRING(BGl_string1816z00zzmodule_pragmaz00,
		BgL_bgl_string1816za700za7za7m1838za7, "args-noescape", 13);
	      DEFINE_STRING(BGl_string1817z00zzmodule_pragmaz00,
		BgL_bgl_string1817za700za7za7m1839za7, "module_pragma", 13);
	      DEFINE_STRING(BGl_string1818z00zzmodule_pragmaz00,
		BgL_bgl_string1818za700za7za7m1840za7,
		"* args-retescape args-noescape stack-allocator effect coerce predicate-of thread-local default-inline no-alloc fail-safe args-safe nesting no-trace allocator no-init-flow no-cfa-top side-effect-free void @ foreign pragma ",
		221);
	extern obj_t BGl_funzd2argszd2retescapezd2setz12zd2envz12zzast_varz00;
	   
		 
		DEFINE_STRING(BGl_string1794z00zzmodule_pragmaz00,
		BgL_bgl_string1794za700za7za7m1841za7, "pragma", 6);
	      DEFINE_STRING(BGl_string1795z00zzmodule_pragmaz00,
		BgL_bgl_string1795za700za7za7m1842za7, "Illegal clause", 14);
	      DEFINE_STRING(BGl_string1796z00zzmodule_pragmaz00,
		BgL_bgl_string1796za700za7za7m1843za7,
		"Can't find global variable for pragma", 37);
	      DEFINE_STRING(BGl_string1797z00zzmodule_pragmaz00,
		BgL_bgl_string1797za700za7za7m1844za7, "pragma-finalizer", 16);
	      DEFINE_STRING(BGl_string1798z00zzmodule_pragmaz00,
		BgL_bgl_string1798za700za7za7m1845za7, "Illegal \"pragma\" finalizer form",
		31);
	      DEFINE_STRING(BGl_string1799z00zzmodule_pragmaz00,
		BgL_bgl_string1799za700za7za7m1846za7, "pragma(", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1793z00zzmodule_pragmaz00,
		BgL_bgl_za762za7c3za704anonymo1847za7,
		BGl_z62zc3z04anonymousza31234ze3ze5zzmodule_pragmaz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_pragmazd2finaliza7erzd2envza7zzmodule_pragmaz00,
		BgL_bgl_za762pragmaza7d2fina1848z00,
		BGl_z62pragmazd2finaliza7erz17zzmodule_pragmaz00, 0L, BUNSPEC, 0);
	extern obj_t BGl_funzd2argszd2retescapezd2envzd2zzast_varz00;
	extern obj_t BGl_funzd2argszd2noescapezd2envzd2zzast_varz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_pragmaz00));
		     ADD_ROOT((void *) (&BGl_za2pragmazd2listza2zd2zzmodule_pragmaz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_pragmaz00(long
		BgL_checksumz00_2787, char *BgL_fromz00_2788)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_pragmaz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_pragmaz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_pragmaz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_pragmaz00();
					BGl_cnstzd2initzd2zzmodule_pragmaz00();
					BGl_importedzd2moduleszd2initz00zzmodule_pragmaz00();
					return BGl_toplevelzd2initzd2zzmodule_pragmaz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"module_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_pragma");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"module_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"module_pragma");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_pragma");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 15 */
			{	/* Module/pragma.scm 15 */
				obj_t BgL_cportz00_2768;

				{	/* Module/pragma.scm 15 */
					obj_t BgL_stringz00_2775;

					BgL_stringz00_2775 = BGl_string1818z00zzmodule_pragmaz00;
					{	/* Module/pragma.scm 15 */
						obj_t BgL_startz00_2776;

						BgL_startz00_2776 = BINT(0L);
						{	/* Module/pragma.scm 15 */
							obj_t BgL_endz00_2777;

							BgL_endz00_2777 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2775)));
							{	/* Module/pragma.scm 15 */

								BgL_cportz00_2768 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2775, BgL_startz00_2776, BgL_endz00_2777);
				}}}}
				{
					long BgL_iz00_2769;

					BgL_iz00_2769 = 21L;
				BgL_loopz00_2770:
					if ((BgL_iz00_2769 == -1L))
						{	/* Module/pragma.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/pragma.scm 15 */
							{	/* Module/pragma.scm 15 */
								obj_t BgL_arg1820z00_2771;

								{	/* Module/pragma.scm 15 */

									{	/* Module/pragma.scm 15 */
										obj_t BgL_locationz00_2773;

										BgL_locationz00_2773 = BBOOL(((bool_t) 0));
										{	/* Module/pragma.scm 15 */

											BgL_arg1820z00_2771 =
												BGl_readz00zz__readerz00(BgL_cportz00_2768,
												BgL_locationz00_2773);
										}
									}
								}
								{	/* Module/pragma.scm 15 */
									int BgL_tmpz00_2816;

									BgL_tmpz00_2816 = (int) (BgL_iz00_2769);
									CNST_TABLE_SET(BgL_tmpz00_2816, BgL_arg1820z00_2771);
							}}
							{	/* Module/pragma.scm 15 */
								int BgL_auxz00_2774;

								BgL_auxz00_2774 = (int) ((BgL_iz00_2769 - 1L));
								{
									long BgL_iz00_2821;

									BgL_iz00_2821 = (long) (BgL_auxz00_2774);
									BgL_iz00_2769 = BgL_iz00_2821;
									goto BgL_loopz00_2770;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 15 */
			return (BGl_za2pragmazd2listza2zd2zzmodule_pragmaz00 = BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzmodule_pragmaz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1369;

				BgL_headz00_1369 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1370;
					obj_t BgL_tailz00_1371;

					BgL_prevz00_1370 = BgL_headz00_1369;
					BgL_tailz00_1371 = BgL_l1z00_1;
				BgL_loopz00_1372:
					if (PAIRP(BgL_tailz00_1371))
						{
							obj_t BgL_newzd2prevzd2_1374;

							BgL_newzd2prevzd2_1374 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1371), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1370, BgL_newzd2prevzd2_1374);
							{
								obj_t BgL_tailz00_2831;
								obj_t BgL_prevz00_2830;

								BgL_prevz00_2830 = BgL_newzd2prevzd2_1374;
								BgL_tailz00_2831 = CDR(BgL_tailz00_1371);
								BgL_tailz00_1371 = BgL_tailz00_2831;
								BgL_prevz00_1370 = BgL_prevz00_2830;
								goto BgL_loopz00_1372;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1369);
				}
			}
		}

	}



/* make-pragma-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2pragmazd2compilerz00zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 35 */
			{	/* Module/pragma.scm 36 */
				BgL_ccompz00_bglt BgL_new1104z00_1377;

				{	/* Module/pragma.scm 37 */
					BgL_ccompz00_bglt BgL_new1103z00_1382;

					BgL_new1103z00_1382 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/pragma.scm 37 */
						long BgL_arg1236z00_1383;

						BgL_arg1236z00_1383 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1103z00_1382), BgL_arg1236z00_1383);
					}
					BgL_new1104z00_1377 = BgL_new1103z00_1382;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1104z00_1377))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1104z00_1377))->BgL_producerz00) =
					((obj_t)
						BGl_pragmazd2producerzd2zzmodule_pragmaz00
						(BGl_za2moduleza2z00zzmodule_modulez00)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1104z00_1377))->BgL_consumerz00) =
					((obj_t) BGl_proc1793z00zzmodule_pragmaz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1104z00_1377))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_pragmazd2finaliza7erzd2envza7zzmodule_pragmaz00),
					BUNSPEC);
				return ((obj_t) BgL_new1104z00_1377);
			}
		}

	}



/* &make-pragma-compiler */
	obj_t BGl_z62makezd2pragmazd2compilerz62zzmodule_pragmaz00(obj_t
		BgL_envz00_2749)
	{
		{	/* Module/pragma.scm 35 */
			return BGl_makezd2pragmazd2compilerz00zzmodule_pragmaz00();
		}

	}



/* &<@anonymous:1234> */
	obj_t BGl_z62zc3z04anonymousza31234ze3ze5zzmodule_pragmaz00(obj_t
		BgL_envz00_2750, obj_t BgL_mz00_2751, obj_t BgL_cz00_2752)
	{
		{	/* Module/pragma.scm 39 */
			{	/* Module/pragma.scm 39 */
				obj_t BgL_fun1235z00_2779;

				BgL_fun1235z00_2779 =
					BGl_pragmazd2producerzd2zzmodule_pragmaz00(BgL_mz00_2751);
				return BGL_PROCEDURE_CALL1(BgL_fun1235z00_2779, BgL_cz00_2752);
			}
		}

	}



/* pragma-producer */
	obj_t BGl_pragmazd2producerzd2zzmodule_pragmaz00(obj_t BgL_modulez00_3)
	{
		{	/* Module/pragma.scm 45 */
			{	/* Module/pragma.scm 46 */
				obj_t BgL_zc3z04anonymousza31237ze3z87_2754;

				BgL_zc3z04anonymousza31237ze3z87_2754 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza31237ze3ze5zzmodule_pragmaz00, (int) (1L),
					(int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31237ze3z87_2754, (int) (0L),
					BgL_modulez00_3);
				return BgL_zc3z04anonymousza31237ze3z87_2754;
			}
		}

	}



/* &<@anonymous:1237> */
	obj_t BGl_z62zc3z04anonymousza31237ze3ze5zzmodule_pragmaz00(obj_t
		BgL_envz00_2755, obj_t BgL_clausez00_2757)
	{
		{	/* Module/pragma.scm 46 */
			{	/* Module/pragma.scm 46 */
				obj_t BgL_modulez00_2756;

				BgL_modulez00_2756 = PROCEDURE_REF(BgL_envz00_2755, (int) (0L));
				{
					obj_t BgL_protosz00_2783;

					if (PAIRP(BgL_clausez00_2757))
						{	/* Module/pragma.scm 46 */
							obj_t BgL_arg1242z00_2786;

							BgL_arg1242z00_2786 = CDR(((obj_t) BgL_clausez00_2757));
							BgL_protosz00_2783 = BgL_arg1242z00_2786;
							{
								obj_t BgL_l1225z00_2785;

								BgL_l1225z00_2785 = BgL_protosz00_2783;
							BgL_zc3z04anonymousza31243ze3z87_2784:
								if (PAIRP(BgL_l1225z00_2785))
									{	/* Module/pragma.scm 49 */
										BGl_pragmazd2parserzd2zzmodule_pragmaz00(CAR
											(BgL_l1225z00_2785), BgL_modulez00_2756,
											BgL_clausez00_2757);
										{
											obj_t BgL_l1225z00_2866;

											BgL_l1225z00_2866 = CDR(BgL_l1225z00_2785);
											BgL_l1225z00_2785 = BgL_l1225z00_2866;
											goto BgL_zc3z04anonymousza31243ze3z87_2784;
										}
									}
								else
									{	/* Module/pragma.scm 49 */
										((bool_t) 1);
									}
							}
							return BNIL;
						}
					else
						{	/* Module/pragma.scm 46 */
							{	/* Module/pragma.scm 54 */
								obj_t BgL_list1249z00_2782;

								BgL_list1249z00_2782 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								return
									BGl_userzd2errorzd2zztools_errorz00
									(BGl_string1794z00zzmodule_pragmaz00,
									BGl_string1795z00zzmodule_pragmaz00, BgL_clausez00_2757,
									BgL_list1249z00_2782);
							}
						}
				}
			}
		}

	}



/* pragma-parser */
	obj_t BGl_pragmazd2parserzd2zzmodule_pragmaz00(obj_t BgL_protoz00_4,
		obj_t BgL_modulez00_5, obj_t BgL_clausez00_6)
	{
		{	/* Module/pragma.scm 59 */
			{
				obj_t BgL_idz00_1400;
				obj_t BgL_propz00_1401;

				if (PAIRP(BgL_protoz00_4))
					{	/* Module/pragma.scm 60 */
						obj_t BgL_carzd2116zd2_1406;

						BgL_carzd2116zd2_1406 = CAR(((obj_t) BgL_protoz00_4));
						if (SYMBOLP(BgL_carzd2116zd2_1406))
							{	/* Module/pragma.scm 60 */
								obj_t BgL_arg1252z00_1408;

								BgL_arg1252z00_1408 = CDR(((obj_t) BgL_protoz00_4));
								BgL_idz00_1400 = BgL_carzd2116zd2_1406;
								BgL_propz00_1401 = BgL_arg1252z00_1408;
								{	/* Module/pragma.scm 63 */
									obj_t BgL_pragz00_1409;

									if (EPAIRP(BgL_protoz00_4))
										{	/* Module/pragma.scm 64 */
											obj_t BgL_arg1268z00_1411;
											obj_t BgL_arg1272z00_1412;

											{	/* Module/pragma.scm 64 */
												obj_t BgL_list1273z00_1413;

												{	/* Module/pragma.scm 64 */
													obj_t BgL_arg1284z00_1414;

													{	/* Module/pragma.scm 64 */
														obj_t BgL_arg1304z00_1415;

														BgL_arg1304z00_1415 =
															MAKE_YOUNG_PAIR(BgL_clausez00_6, BNIL);
														BgL_arg1284z00_1414 =
															MAKE_YOUNG_PAIR(BgL_propz00_1401,
															BgL_arg1304z00_1415);
													}
													BgL_list1273z00_1413 =
														MAKE_YOUNG_PAIR(BgL_modulez00_5,
														BgL_arg1284z00_1414);
												}
												BgL_arg1268z00_1411 = BgL_list1273z00_1413;
											}
											BgL_arg1272z00_1412 = CER(((obj_t) BgL_protoz00_4));
											{	/* Module/pragma.scm 64 */
												obj_t BgL_res1764z00_1780;

												BgL_res1764z00_1780 =
													MAKE_YOUNG_EPAIR(BgL_idz00_1400, BgL_arg1268z00_1411,
													BgL_arg1272z00_1412);
												BgL_pragz00_1409 = BgL_res1764z00_1780;
											}
										}
									else
										{	/* Module/pragma.scm 65 */
											obj_t BgL_list1305z00_1416;

											{	/* Module/pragma.scm 65 */
												obj_t BgL_arg1306z00_1417;

												{	/* Module/pragma.scm 65 */
													obj_t BgL_arg1307z00_1418;

													{	/* Module/pragma.scm 65 */
														obj_t BgL_arg1308z00_1419;

														BgL_arg1308z00_1419 =
															MAKE_YOUNG_PAIR(BgL_clausez00_6, BNIL);
														BgL_arg1307z00_1418 =
															MAKE_YOUNG_PAIR(BgL_propz00_1401,
															BgL_arg1308z00_1419);
													}
													BgL_arg1306z00_1417 =
														MAKE_YOUNG_PAIR(BgL_modulez00_5,
														BgL_arg1307z00_1418);
												}
												BgL_list1305z00_1416 =
													MAKE_YOUNG_PAIR(BgL_idz00_1400, BgL_arg1306z00_1417);
											}
											BgL_pragz00_1409 = BgL_list1305z00_1416;
										}
									return (BGl_za2pragmazd2listza2zd2zzmodule_pragmaz00 =
										MAKE_YOUNG_PAIR(BgL_pragz00_1409,
											BGl_za2pragmazd2listza2zd2zzmodule_pragmaz00), BUNSPEC);
								}
							}
						else
							{	/* Module/pragma.scm 60 */
							BgL_tagzd2110zd2_1403:
								{	/* Module/pragma.scm 68 */
									obj_t BgL_list1309z00_1420;

									BgL_list1309z00_1420 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									return
										BGl_userzd2errorzd2zztools_errorz00
										(BGl_string1794z00zzmodule_pragmaz00,
										BGl_string1795z00zzmodule_pragmaz00, BgL_clausez00_6,
										BgL_list1309z00_1420);
								}
							}
					}
				else
					{	/* Module/pragma.scm 60 */
						goto BgL_tagzd2110zd2_1403;
					}
			}
		}

	}



/* pragma-finalizer */
	BGL_EXPORTED_DEF obj_t BGl_pragmazd2finaliza7erz75zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 78 */
			{
				obj_t BgL_l1227z00_1422;

				BgL_l1227z00_1422 = BGl_za2pragmazd2listza2zd2zzmodule_pragmaz00;
			BgL_zc3z04anonymousza31310ze3z87_1423:
				if (PAIRP(BgL_l1227z00_1422))
					{	/* Module/pragma.scm 79 */
						{	/* Module/pragma.scm 80 */
							obj_t BgL_pragmaz00_1425;

							BgL_pragmaz00_1425 = CAR(BgL_l1227z00_1422);
							{
								obj_t BgL_idz00_1426;
								obj_t BgL_modulez00_1427;
								obj_t BgL_propza2za2_1428;
								obj_t BgL_clausez00_1429;

								if (PAIRP(BgL_pragmaz00_1425))
									{	/* Module/pragma.scm 80 */
										obj_t BgL_cdrzd2133zd2_1434;

										BgL_cdrzd2133zd2_1434 = CDR(((obj_t) BgL_pragmaz00_1425));
										if (PAIRP(BgL_cdrzd2133zd2_1434))
											{	/* Module/pragma.scm 80 */
												obj_t BgL_cdrzd2139zd2_1436;

												BgL_cdrzd2139zd2_1436 = CDR(BgL_cdrzd2133zd2_1434);
												if (PAIRP(BgL_cdrzd2139zd2_1436))
													{	/* Module/pragma.scm 80 */
														obj_t BgL_cdrzd2144zd2_1438;

														BgL_cdrzd2144zd2_1438 = CDR(BgL_cdrzd2139zd2_1436);
														if (PAIRP(BgL_cdrzd2144zd2_1438))
															{	/* Module/pragma.scm 80 */
																if (NULLP(CDR(BgL_cdrzd2144zd2_1438)))
																	{	/* Module/pragma.scm 80 */
																		obj_t BgL_arg1318z00_1442;
																		obj_t BgL_arg1319z00_1443;
																		obj_t BgL_arg1320z00_1444;
																		obj_t BgL_arg1321z00_1445;

																		BgL_arg1318z00_1442 =
																			CAR(((obj_t) BgL_pragmaz00_1425));
																		BgL_arg1319z00_1443 =
																			CAR(BgL_cdrzd2133zd2_1434);
																		BgL_arg1320z00_1444 =
																			CAR(BgL_cdrzd2139zd2_1436);
																		BgL_arg1321z00_1445 =
																			CAR(BgL_cdrzd2144zd2_1438);
																		BgL_idz00_1426 = BgL_arg1318z00_1442;
																		BgL_modulez00_1427 = BgL_arg1319z00_1443;
																		BgL_propza2za2_1428 = BgL_arg1320z00_1444;
																		BgL_clausez00_1429 = BgL_arg1321z00_1445;
																		{	/* Module/pragma.scm 82 */
																			obj_t BgL_globalz00_1447;

																			{	/* Module/pragma.scm 82 */
																				obj_t BgL_globalz00_1453;

																				BgL_globalz00_1453 =
																					BGl_findzd2globalzf2modulez20zzast_envz00
																					(BgL_idz00_1426, BgL_modulez00_1427);
																				{	/* Module/pragma.scm 83 */
																					bool_t BgL_test1863z00_2917;

																					{	/* Module/pragma.scm 83 */
																						obj_t BgL_classz00_1785;

																						BgL_classz00_1785 =
																							BGl_globalz00zzast_varz00;
																						if (BGL_OBJECTP(BgL_globalz00_1453))
																							{	/* Module/pragma.scm 83 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_1787;
																								BgL_arg1807z00_1787 =
																									(BgL_objectz00_bglt)
																									(BgL_globalz00_1453);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* Module/pragma.scm 83 */
																										long BgL_idxz00_1793;

																										BgL_idxz00_1793 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_1787);
																										BgL_test1863z00_2917 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_1793 +
																													2L)) ==
																											BgL_classz00_1785);
																									}
																								else
																									{	/* Module/pragma.scm 83 */
																										bool_t BgL_res1766z00_1818;

																										{	/* Module/pragma.scm 83 */
																											obj_t BgL_oclassz00_1801;

																											{	/* Module/pragma.scm 83 */
																												obj_t
																													BgL_arg1815z00_1809;
																												long
																													BgL_arg1816z00_1810;
																												BgL_arg1815z00_1809 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* Module/pragma.scm 83 */
																													long
																														BgL_arg1817z00_1811;
																													BgL_arg1817z00_1811 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_1787);
																													BgL_arg1816z00_1810 =
																														(BgL_arg1817z00_1811
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_1801 =
																													VECTOR_REF
																													(BgL_arg1815z00_1809,
																													BgL_arg1816z00_1810);
																											}
																											{	/* Module/pragma.scm 83 */
																												bool_t
																													BgL__ortest_1115z00_1802;
																												BgL__ortest_1115z00_1802
																													=
																													(BgL_classz00_1785 ==
																													BgL_oclassz00_1801);
																												if (BgL__ortest_1115z00_1802)
																													{	/* Module/pragma.scm 83 */
																														BgL_res1766z00_1818
																															=
																															BgL__ortest_1115z00_1802;
																													}
																												else
																													{	/* Module/pragma.scm 83 */
																														long
																															BgL_odepthz00_1803;
																														{	/* Module/pragma.scm 83 */
																															obj_t
																																BgL_arg1804z00_1804;
																															BgL_arg1804z00_1804
																																=
																																(BgL_oclassz00_1801);
																															BgL_odepthz00_1803
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_1804);
																														}
																														if (
																															(2L <
																																BgL_odepthz00_1803))
																															{	/* Module/pragma.scm 83 */
																																obj_t
																																	BgL_arg1802z00_1806;
																																{	/* Module/pragma.scm 83 */
																																	obj_t
																																		BgL_arg1803z00_1807;
																																	BgL_arg1803z00_1807
																																		=
																																		(BgL_oclassz00_1801);
																																	BgL_arg1802z00_1806
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_1807,
																																		2L);
																																}
																																BgL_res1766z00_1818
																																	=
																																	(BgL_arg1802z00_1806
																																	==
																																	BgL_classz00_1785);
																															}
																														else
																															{	/* Module/pragma.scm 83 */
																																BgL_res1766z00_1818
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test1863z00_2917 =
																											BgL_res1766z00_1818;
																									}
																							}
																						else
																							{	/* Module/pragma.scm 83 */
																								BgL_test1863z00_2917 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test1863z00_2917)
																						{	/* Module/pragma.scm 83 */
																							BgL_globalz00_1447 =
																								BgL_globalz00_1453;
																						}
																					else
																						{	/* Module/pragma.scm 83 */
																							BgL_globalz00_1447 =
																								BGl_findzd2globalzf2modulez20zzast_envz00
																								(BgL_idz00_1426,
																								CNST_TABLE_REF(1));
																						}
																				}
																			}
																			{	/* Module/pragma.scm 86 */
																				bool_t BgL_test1868z00_2942;

																				{	/* Module/pragma.scm 86 */
																					obj_t BgL_classz00_1819;

																					BgL_classz00_1819 =
																						BGl_globalz00zzast_varz00;
																					if (BGL_OBJECTP(BgL_globalz00_1447))
																						{	/* Module/pragma.scm 86 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_1821;
																							BgL_arg1807z00_1821 =
																								(BgL_objectz00_bglt)
																								(BgL_globalz00_1447);
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Module/pragma.scm 86 */
																									long BgL_idxz00_1827;

																									BgL_idxz00_1827 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_1821);
																									BgL_test1868z00_2942 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_1827 + 2L)) ==
																										BgL_classz00_1819);
																								}
																							else
																								{	/* Module/pragma.scm 86 */
																									bool_t BgL_res1767z00_1852;

																									{	/* Module/pragma.scm 86 */
																										obj_t BgL_oclassz00_1835;

																										{	/* Module/pragma.scm 86 */
																											obj_t BgL_arg1815z00_1843;
																											long BgL_arg1816z00_1844;

																											BgL_arg1815z00_1843 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Module/pragma.scm 86 */
																												long
																													BgL_arg1817z00_1845;
																												BgL_arg1817z00_1845 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_1821);
																												BgL_arg1816z00_1844 =
																													(BgL_arg1817z00_1845 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_1835 =
																												VECTOR_REF
																												(BgL_arg1815z00_1843,
																												BgL_arg1816z00_1844);
																										}
																										{	/* Module/pragma.scm 86 */
																											bool_t
																												BgL__ortest_1115z00_1836;
																											BgL__ortest_1115z00_1836 =
																												(BgL_classz00_1819 ==
																												BgL_oclassz00_1835);
																											if (BgL__ortest_1115z00_1836)
																												{	/* Module/pragma.scm 86 */
																													BgL_res1767z00_1852 =
																														BgL__ortest_1115z00_1836;
																												}
																											else
																												{	/* Module/pragma.scm 86 */
																													long
																														BgL_odepthz00_1837;
																													{	/* Module/pragma.scm 86 */
																														obj_t
																															BgL_arg1804z00_1838;
																														BgL_arg1804z00_1838
																															=
																															(BgL_oclassz00_1835);
																														BgL_odepthz00_1837 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_1838);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_1837))
																														{	/* Module/pragma.scm 86 */
																															obj_t
																																BgL_arg1802z00_1840;
																															{	/* Module/pragma.scm 86 */
																																obj_t
																																	BgL_arg1803z00_1841;
																																BgL_arg1803z00_1841
																																	=
																																	(BgL_oclassz00_1835);
																																BgL_arg1802z00_1840
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_1841,
																																	2L);
																															}
																															BgL_res1767z00_1852
																																=
																																(BgL_arg1802z00_1840
																																==
																																BgL_classz00_1819);
																														}
																													else
																														{	/* Module/pragma.scm 86 */
																															BgL_res1767z00_1852
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test1868z00_2942 =
																										BgL_res1767z00_1852;
																								}
																						}
																					else
																						{	/* Module/pragma.scm 86 */
																							BgL_test1868z00_2942 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test1868z00_2942)
																					{	/* Module/pragma.scm 86 */
																						BBOOL
																							(BGl_setzd2pragmazd2propertiesz12z12zzmodule_pragmaz00
																							(BgL_globalz00_1447,
																								BgL_propza2za2_1428,
																								BgL_clausez00_1429));
																					}
																				else
																					{	/* Module/pragma.scm 86 */
																						if (
																							(BgL_modulez00_1427 ==
																								BGl_za2moduleza2z00zzmodule_modulez00))
																							{	/* Module/pragma.scm 89 */
																								obj_t BgL_arg1325z00_1449;
																								obj_t BgL_arg1326z00_1450;

																								BgL_arg1325z00_1449 =
																									BGl_findzd2locationzd2zztools_locationz00
																									(BgL_pragmaz00_1425);
																								{	/* Module/pragma.scm 92 */
																									obj_t BgL_arg1327z00_1451;

																									{	/* Module/pragma.scm 92 */
																										obj_t BgL_arg1328z00_1452;

																										BgL_arg1328z00_1452 =
																											MAKE_YOUNG_PAIR
																											(BgL_modulez00_1427,
																											BNIL);
																										BgL_arg1327z00_1451 =
																											MAKE_YOUNG_PAIR
																											(BgL_idz00_1426,
																											BgL_arg1328z00_1452);
																									}
																									BgL_arg1326z00_1450 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(2),
																										BgL_arg1327z00_1451);
																								}
																								BGl_userzd2warningzf2locationz20zztools_errorz00
																									(BgL_arg1325z00_1449,
																									BGl_string1794z00zzmodule_pragmaz00,
																									BGl_string1796z00zzmodule_pragmaz00,
																									BgL_arg1326z00_1450);
																							}
																						else
																							{	/* Module/pragma.scm 87 */
																								BFALSE;
																							}
																					}
																			}
																		}
																	}
																else
																	{	/* Module/pragma.scm 80 */
																	BgL_tagzd2122zd2_1431:
																		BGl_internalzd2errorzd2zztools_errorz00
																			(BGl_string1797z00zzmodule_pragmaz00,
																			BGl_string1798z00zzmodule_pragmaz00,
																			BgL_pragmaz00_1425);
																	}
															}
														else
															{	/* Module/pragma.scm 80 */
																goto BgL_tagzd2122zd2_1431;
															}
													}
												else
													{	/* Module/pragma.scm 80 */
														goto BgL_tagzd2122zd2_1431;
													}
											}
										else
											{	/* Module/pragma.scm 80 */
												goto BgL_tagzd2122zd2_1431;
											}
									}
								else
									{	/* Module/pragma.scm 80 */
										goto BgL_tagzd2122zd2_1431;
									}
							}
						}
						{
							obj_t BgL_l1227z00_2976;

							BgL_l1227z00_2976 = CDR(BgL_l1227z00_1422);
							BgL_l1227z00_1422 = BgL_l1227z00_2976;
							goto BgL_zc3z04anonymousza31310ze3z87_1423;
						}
					}
				else
					{	/* Module/pragma.scm 79 */
						((bool_t) 1);
					}
			}
			BGl_za2pragmazd2listza2zd2zzmodule_pragmaz00 = BNIL;
			return CNST_TABLE_REF(3);
		}

	}



/* &pragma-finalizer */
	obj_t BGl_z62pragmazd2finaliza7erz17zzmodule_pragmaz00(obj_t BgL_envz00_2753)
	{
		{	/* Module/pragma.scm 78 */
			return BGl_pragmazd2finaliza7erz75zzmodule_pragmaz00();
		}

	}



/* set-pragma-properties! */
	bool_t BGl_setzd2pragmazd2propertiesz12z12zzmodule_pragmaz00(obj_t
		BgL_globalz00_7, obj_t BgL_propza2za2_8, obj_t BgL_clausez00_9)
	{
		{	/* Module/pragma.scm 105 */
			{
				obj_t BgL_l1229z00_1458;

				BgL_l1229z00_1458 = BgL_propza2za2_8;
			BgL_zc3z04anonymousza31332ze3z87_1459:
				if (PAIRP(BgL_l1229z00_1458))
					{	/* Module/pragma.scm 106 */
						BGl_setzd2pragmazd2propertyz12z12zzmodule_pragmaz00(BgL_globalz00_7,
							CAR(BgL_l1229z00_1458), BgL_clausez00_9);
						{
							obj_t BgL_l1229z00_2984;

							BgL_l1229z00_2984 = CDR(BgL_l1229z00_1458);
							BgL_l1229z00_1458 = BgL_l1229z00_2984;
							goto BgL_zc3z04anonymousza31332ze3z87_1459;
						}
					}
				else
					{	/* Module/pragma.scm 106 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* sfun-error */
	obj_t BGl_sfunzd2errorzd2zzmodule_pragmaz00(obj_t BgL_pz00_10,
		BgL_globalz00_bglt BgL_gz00_11)
	{
		{	/* Module/pragma.scm 113 */
			if (CBOOL(BGl_za2allzd2exportzd2mutablezf3za2zf3zzmodule_evalz00))
				{	/* Module/pragma.scm 114 */
					return BFALSE;
				}
			else
				{	/* Module/pragma.scm 115 */
					obj_t BgL_arg1339z00_1464;
					obj_t BgL_arg1340z00_1465;

					BgL_arg1339z00_1464 =
						string_append_3(BGl_string1799z00zzmodule_pragmaz00, BgL_pz00_10,
						BGl_string1800z00zzmodule_pragmaz00);
					BgL_arg1340z00_1465 =
						BGl_shapez00zztools_shapez00(((obj_t) BgL_gz00_11));
					{	/* Module/pragma.scm 115 */
						obj_t BgL_list1341z00_1466;

						BgL_list1341z00_1466 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						return
							BGl_userzd2errorzd2zztools_errorz00(BgL_arg1339z00_1464,
							BGl_string1801z00zzmodule_pragmaz00, BgL_arg1340z00_1465,
							BgL_list1341z00_1466);
					}
				}
		}

	}



/* set-pragma-property! */
	obj_t BGl_setzd2pragmazd2propertyz12z12zzmodule_pragmaz00(obj_t
		BgL_globalz00_12, obj_t BgL_propz00_13, obj_t BgL_clausez00_14)
	{
		{	/* Module/pragma.scm 123 */
			{
				obj_t BgL_keyz00_1468;
				obj_t BgL_valz00_1469;

				if (SYMBOLP(BgL_propz00_13))
					{	/* Module/pragma.scm 124 */
						if ((BgL_propz00_13 == CNST_TABLE_REF(4)))
							{	/* Module/pragma.scm 129 */
								BgL_valuez00_bglt BgL_valz00_1480;

								BgL_valz00_1480 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_12))))->
									BgL_valuez00);
								{	/* Module/pragma.scm 130 */
									bool_t BgL_test1878z00_3001;

									{	/* Module/pragma.scm 130 */
										obj_t BgL_classz00_1866;

										BgL_classz00_1866 = BGl_funz00zzast_varz00;
										{	/* Module/pragma.scm 130 */
											BgL_objectz00_bglt BgL_arg1807z00_1868;

											{	/* Module/pragma.scm 130 */
												obj_t BgL_tmpz00_3002;

												BgL_tmpz00_3002 =
													((obj_t) ((BgL_objectz00_bglt) BgL_valz00_1480));
												BgL_arg1807z00_1868 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3002);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Module/pragma.scm 130 */
													long BgL_idxz00_1874;

													BgL_idxz00_1874 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1868);
													BgL_test1878z00_3001 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1874 + 2L)) == BgL_classz00_1866);
												}
											else
												{	/* Module/pragma.scm 130 */
													bool_t BgL_res1768z00_1899;

													{	/* Module/pragma.scm 130 */
														obj_t BgL_oclassz00_1882;

														{	/* Module/pragma.scm 130 */
															obj_t BgL_arg1815z00_1890;
															long BgL_arg1816z00_1891;

															BgL_arg1815z00_1890 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Module/pragma.scm 130 */
																long BgL_arg1817z00_1892;

																BgL_arg1817z00_1892 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1868);
																BgL_arg1816z00_1891 =
																	(BgL_arg1817z00_1892 - OBJECT_TYPE);
															}
															BgL_oclassz00_1882 =
																VECTOR_REF(BgL_arg1815z00_1890,
																BgL_arg1816z00_1891);
														}
														{	/* Module/pragma.scm 130 */
															bool_t BgL__ortest_1115z00_1883;

															BgL__ortest_1115z00_1883 =
																(BgL_classz00_1866 == BgL_oclassz00_1882);
															if (BgL__ortest_1115z00_1883)
																{	/* Module/pragma.scm 130 */
																	BgL_res1768z00_1899 =
																		BgL__ortest_1115z00_1883;
																}
															else
																{	/* Module/pragma.scm 130 */
																	long BgL_odepthz00_1884;

																	{	/* Module/pragma.scm 130 */
																		obj_t BgL_arg1804z00_1885;

																		BgL_arg1804z00_1885 = (BgL_oclassz00_1882);
																		BgL_odepthz00_1884 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1885);
																	}
																	if ((2L < BgL_odepthz00_1884))
																		{	/* Module/pragma.scm 130 */
																			obj_t BgL_arg1802z00_1887;

																			{	/* Module/pragma.scm 130 */
																				obj_t BgL_arg1803z00_1888;

																				BgL_arg1803z00_1888 =
																					(BgL_oclassz00_1882);
																				BgL_arg1802z00_1887 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1888, 2L);
																			}
																			BgL_res1768z00_1899 =
																				(BgL_arg1802z00_1887 ==
																				BgL_classz00_1866);
																		}
																	else
																		{	/* Module/pragma.scm 130 */
																			BgL_res1768z00_1899 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1878z00_3001 = BgL_res1768z00_1899;
												}
										}
									}
									if (BgL_test1878z00_3001)
										{	/* Module/pragma.scm 130 */
											return
												((((BgL_funz00_bglt) COBJECT(
															((BgL_funz00_bglt) BgL_valz00_1480)))->
													BgL_sidezd2effectzd2) = ((obj_t) BFALSE), BUNSPEC);
										}
									else
										{	/* Module/pragma.scm 130 */
											return
												BGl_sfunzd2errorzd2zzmodule_pragmaz00
												(BGl_string1802z00zzmodule_pragmaz00,
												((BgL_globalz00_bglt) BgL_globalz00_12));
										}
								}
							}
						else
							{	/* Module/pragma.scm 126 */
								if ((BgL_propz00_13 == CNST_TABLE_REF(5)))
									{	/* Module/pragma.scm 135 */
										BgL_valuez00_bglt BgL_valz00_1483;

										BgL_valz00_1483 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_12))))->
											BgL_valuez00);
										{	/* Module/pragma.scm 136 */
											bool_t BgL_test1883z00_3035;

											{	/* Module/pragma.scm 136 */
												obj_t BgL_classz00_1903;

												BgL_classz00_1903 = BGl_funz00zzast_varz00;
												{	/* Module/pragma.scm 136 */
													BgL_objectz00_bglt BgL_arg1807z00_1905;

													{	/* Module/pragma.scm 136 */
														obj_t BgL_tmpz00_3036;

														BgL_tmpz00_3036 =
															((obj_t) ((BgL_objectz00_bglt) BgL_valz00_1483));
														BgL_arg1807z00_1905 =
															(BgL_objectz00_bglt) (BgL_tmpz00_3036);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Module/pragma.scm 136 */
															long BgL_idxz00_1911;

															BgL_idxz00_1911 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1905);
															BgL_test1883z00_3035 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_1911 + 2L)) == BgL_classz00_1903);
														}
													else
														{	/* Module/pragma.scm 136 */
															bool_t BgL_res1769z00_1936;

															{	/* Module/pragma.scm 136 */
																obj_t BgL_oclassz00_1919;

																{	/* Module/pragma.scm 136 */
																	obj_t BgL_arg1815z00_1927;
																	long BgL_arg1816z00_1928;

																	BgL_arg1815z00_1927 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Module/pragma.scm 136 */
																		long BgL_arg1817z00_1929;

																		BgL_arg1817z00_1929 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1905);
																		BgL_arg1816z00_1928 =
																			(BgL_arg1817z00_1929 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_1919 =
																		VECTOR_REF(BgL_arg1815z00_1927,
																		BgL_arg1816z00_1928);
																}
																{	/* Module/pragma.scm 136 */
																	bool_t BgL__ortest_1115z00_1920;

																	BgL__ortest_1115z00_1920 =
																		(BgL_classz00_1903 == BgL_oclassz00_1919);
																	if (BgL__ortest_1115z00_1920)
																		{	/* Module/pragma.scm 136 */
																			BgL_res1769z00_1936 =
																				BgL__ortest_1115z00_1920;
																		}
																	else
																		{	/* Module/pragma.scm 136 */
																			long BgL_odepthz00_1921;

																			{	/* Module/pragma.scm 136 */
																				obj_t BgL_arg1804z00_1922;

																				BgL_arg1804z00_1922 =
																					(BgL_oclassz00_1919);
																				BgL_odepthz00_1921 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_1922);
																			}
																			if ((2L < BgL_odepthz00_1921))
																				{	/* Module/pragma.scm 136 */
																					obj_t BgL_arg1802z00_1924;

																					{	/* Module/pragma.scm 136 */
																						obj_t BgL_arg1803z00_1925;

																						BgL_arg1803z00_1925 =
																							(BgL_oclassz00_1919);
																						BgL_arg1802z00_1924 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_1925, 2L);
																					}
																					BgL_res1769z00_1936 =
																						(BgL_arg1802z00_1924 ==
																						BgL_classz00_1903);
																				}
																			else
																				{	/* Module/pragma.scm 136 */
																					BgL_res1769z00_1936 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1883z00_3035 = BgL_res1769z00_1936;
														}
												}
											}
											if (BgL_test1883z00_3035)
												{	/* Module/pragma.scm 136 */
													return
														((((BgL_funz00_bglt) COBJECT(
																	((BgL_funz00_bglt) BgL_valz00_1483)))->
															BgL_topzf3zf3) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
												}
											else
												{	/* Module/pragma.scm 136 */
													return
														BGl_sfunzd2errorzd2zzmodule_pragmaz00
														(BGl_string1803z00zzmodule_pragmaz00,
														((BgL_globalz00_bglt) BgL_globalz00_12));
												}
										}
									}
								else
									{	/* Module/pragma.scm 126 */
										if ((BgL_propz00_13 == CNST_TABLE_REF(6)))
											{	/* Module/pragma.scm 142 */
												BgL_valuez00_bglt BgL_valz00_1486;

												BgL_valz00_1486 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_globalz00_12))))->
													BgL_valuez00);
												{	/* Module/pragma.scm 143 */
													bool_t BgL_test1888z00_3069;

													{	/* Module/pragma.scm 143 */
														obj_t BgL_classz00_1941;

														BgL_classz00_1941 = BGl_funz00zzast_varz00;
														{	/* Module/pragma.scm 143 */
															BgL_objectz00_bglt BgL_arg1807z00_1943;

															{	/* Module/pragma.scm 143 */
																obj_t BgL_tmpz00_3070;

																BgL_tmpz00_3070 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_valz00_1486));
																BgL_arg1807z00_1943 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_3070);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Module/pragma.scm 143 */
																	long BgL_idxz00_1949;

																	BgL_idxz00_1949 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_1943);
																	BgL_test1888z00_3069 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_1949 + 2L)) ==
																		BgL_classz00_1941);
																}
															else
																{	/* Module/pragma.scm 143 */
																	bool_t BgL_res1770z00_1974;

																	{	/* Module/pragma.scm 143 */
																		obj_t BgL_oclassz00_1957;

																		{	/* Module/pragma.scm 143 */
																			obj_t BgL_arg1815z00_1965;
																			long BgL_arg1816z00_1966;

																			BgL_arg1815z00_1965 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Module/pragma.scm 143 */
																				long BgL_arg1817z00_1967;

																				BgL_arg1817z00_1967 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_1943);
																				BgL_arg1816z00_1966 =
																					(BgL_arg1817z00_1967 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_1957 =
																				VECTOR_REF(BgL_arg1815z00_1965,
																				BgL_arg1816z00_1966);
																		}
																		{	/* Module/pragma.scm 143 */
																			bool_t BgL__ortest_1115z00_1958;

																			BgL__ortest_1115z00_1958 =
																				(BgL_classz00_1941 ==
																				BgL_oclassz00_1957);
																			if (BgL__ortest_1115z00_1958)
																				{	/* Module/pragma.scm 143 */
																					BgL_res1770z00_1974 =
																						BgL__ortest_1115z00_1958;
																				}
																			else
																				{	/* Module/pragma.scm 143 */
																					long BgL_odepthz00_1959;

																					{	/* Module/pragma.scm 143 */
																						obj_t BgL_arg1804z00_1960;

																						BgL_arg1804z00_1960 =
																							(BgL_oclassz00_1957);
																						BgL_odepthz00_1959 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_1960);
																					}
																					if ((2L < BgL_odepthz00_1959))
																						{	/* Module/pragma.scm 143 */
																							obj_t BgL_arg1802z00_1962;

																							{	/* Module/pragma.scm 143 */
																								obj_t BgL_arg1803z00_1963;

																								BgL_arg1803z00_1963 =
																									(BgL_oclassz00_1957);
																								BgL_arg1802z00_1962 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_1963, 2L);
																							}
																							BgL_res1770z00_1974 =
																								(BgL_arg1802z00_1962 ==
																								BgL_classz00_1941);
																						}
																					else
																						{	/* Module/pragma.scm 143 */
																							BgL_res1770z00_1974 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1888z00_3069 = BgL_res1770z00_1974;
																}
														}
													}
													if (BgL_test1888z00_3069)
														{	/* Module/pragma.scm 145 */
															obj_t BgL_arg1361z00_1488;

															{	/* Module/pragma.scm 145 */
																obj_t BgL_arg1364z00_1489;

																BgL_arg1364z00_1489 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt) BgL_valz00_1486)))->
																	BgL_propertyz00);
																BgL_arg1361z00_1488 =
																	MAKE_YOUNG_PAIR(BgL_propz00_13,
																	BgL_arg1364z00_1489);
															}
															return
																((((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt) BgL_valz00_1486)))->
																	BgL_propertyz00) =
																((obj_t) BgL_arg1361z00_1488), BUNSPEC);
														}
													else
														{	/* Module/pragma.scm 143 */
															return
																BGl_sfunzd2errorzd2zzmodule_pragmaz00
																(BGl_string1804z00zzmodule_pragmaz00,
																((BgL_globalz00_bglt) BgL_globalz00_12));
														}
												}
											}
										else
											{	/* Module/pragma.scm 126 */
												if ((BgL_propz00_13 == CNST_TABLE_REF(7)))
													{	/* Module/pragma.scm 149 */
														BgL_valuez00_bglt BgL_valz00_1491;

														BgL_valz00_1491 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_globalz00_12))))->
															BgL_valuez00);
														{	/* Module/pragma.scm 150 */
															bool_t BgL_test1893z00_3106;

															{	/* Module/pragma.scm 150 */
																obj_t BgL_classz00_1979;

																BgL_classz00_1979 = BGl_sfunz00zzast_varz00;
																{	/* Module/pragma.scm 150 */
																	BgL_objectz00_bglt BgL_arg1807z00_1981;

																	{	/* Module/pragma.scm 150 */
																		obj_t BgL_tmpz00_3107;

																		BgL_tmpz00_3107 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_valz00_1491));
																		BgL_arg1807z00_1981 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_3107);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Module/pragma.scm 150 */
																			long BgL_idxz00_1987;

																			BgL_idxz00_1987 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_1981);
																			BgL_test1893z00_3106 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_1987 + 3L)) ==
																				BgL_classz00_1979);
																		}
																	else
																		{	/* Module/pragma.scm 150 */
																			bool_t BgL_res1771z00_2012;

																			{	/* Module/pragma.scm 150 */
																				obj_t BgL_oclassz00_1995;

																				{	/* Module/pragma.scm 150 */
																					obj_t BgL_arg1815z00_2003;
																					long BgL_arg1816z00_2004;

																					BgL_arg1815z00_2003 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Module/pragma.scm 150 */
																						long BgL_arg1817z00_2005;

																						BgL_arg1817z00_2005 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_1981);
																						BgL_arg1816z00_2004 =
																							(BgL_arg1817z00_2005 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_1995 =
																						VECTOR_REF(BgL_arg1815z00_2003,
																						BgL_arg1816z00_2004);
																				}
																				{	/* Module/pragma.scm 150 */
																					bool_t BgL__ortest_1115z00_1996;

																					BgL__ortest_1115z00_1996 =
																						(BgL_classz00_1979 ==
																						BgL_oclassz00_1995);
																					if (BgL__ortest_1115z00_1996)
																						{	/* Module/pragma.scm 150 */
																							BgL_res1771z00_2012 =
																								BgL__ortest_1115z00_1996;
																						}
																					else
																						{	/* Module/pragma.scm 150 */
																							long BgL_odepthz00_1997;

																							{	/* Module/pragma.scm 150 */
																								obj_t BgL_arg1804z00_1998;

																								BgL_arg1804z00_1998 =
																									(BgL_oclassz00_1995);
																								BgL_odepthz00_1997 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_1998);
																							}
																							if ((3L < BgL_odepthz00_1997))
																								{	/* Module/pragma.scm 150 */
																									obj_t BgL_arg1802z00_2000;

																									{	/* Module/pragma.scm 150 */
																										obj_t BgL_arg1803z00_2001;

																										BgL_arg1803z00_2001 =
																											(BgL_oclassz00_1995);
																										BgL_arg1802z00_2000 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2001, 3L);
																									}
																									BgL_res1771z00_2012 =
																										(BgL_arg1802z00_2000 ==
																										BgL_classz00_1979);
																								}
																							else
																								{	/* Module/pragma.scm 150 */
																									BgL_res1771z00_2012 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1893z00_3106 =
																				BgL_res1771z00_2012;
																		}
																}
															}
															if (BgL_test1893z00_3106)
																{	/* Module/pragma.scm 152 */
																	obj_t BgL_arg1367z00_1493;

																	{	/* Module/pragma.scm 152 */
																		obj_t BgL_arg1370z00_1494;

																		BgL_arg1370z00_1494 =
																			(((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						BgL_valz00_1491)))->
																			BgL_propertyz00);
																		BgL_arg1367z00_1493 =
																			MAKE_YOUNG_PAIR(BgL_propz00_13,
																			BgL_arg1370z00_1494);
																	}
																	return
																		((((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						BgL_valz00_1491)))->
																			BgL_propertyz00) =
																		((obj_t) BgL_arg1367z00_1493), BUNSPEC);
																}
															else
																{	/* Module/pragma.scm 150 */
																	return
																		BGl_sfunzd2errorzd2zzmodule_pragmaz00
																		(BGl_string1805z00zzmodule_pragmaz00,
																		((BgL_globalz00_bglt) BgL_globalz00_12));
																}
														}
													}
												else
													{	/* Module/pragma.scm 126 */
														if ((BgL_propz00_13 == CNST_TABLE_REF(8)))
															{	/* Module/pragma.scm 154 */
																BgL_valuez00_bglt BgL_valz00_1496;

																BgL_valz00_1496 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_globalz00_12))))->BgL_valuez00);
																{	/* Module/pragma.scm 155 */
																	bool_t BgL_test1898z00_3143;

																	{	/* Module/pragma.scm 155 */
																		obj_t BgL_classz00_2017;

																		BgL_classz00_2017 = BGl_sfunz00zzast_varz00;
																		{	/* Module/pragma.scm 155 */
																			BgL_objectz00_bglt BgL_arg1807z00_2019;

																			{	/* Module/pragma.scm 155 */
																				obj_t BgL_tmpz00_3144;

																				BgL_tmpz00_3144 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_valz00_1496));
																				BgL_arg1807z00_2019 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_3144);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Module/pragma.scm 155 */
																					long BgL_idxz00_2025;

																					BgL_idxz00_2025 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2019);
																					BgL_test1898z00_3143 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2025 + 3L)) ==
																						BgL_classz00_2017);
																				}
																			else
																				{	/* Module/pragma.scm 155 */
																					bool_t BgL_res1772z00_2050;

																					{	/* Module/pragma.scm 155 */
																						obj_t BgL_oclassz00_2033;

																						{	/* Module/pragma.scm 155 */
																							obj_t BgL_arg1815z00_2041;
																							long BgL_arg1816z00_2042;

																							BgL_arg1815z00_2041 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Module/pragma.scm 155 */
																								long BgL_arg1817z00_2043;

																								BgL_arg1817z00_2043 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2019);
																								BgL_arg1816z00_2042 =
																									(BgL_arg1817z00_2043 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2033 =
																								VECTOR_REF(BgL_arg1815z00_2041,
																								BgL_arg1816z00_2042);
																						}
																						{	/* Module/pragma.scm 155 */
																							bool_t BgL__ortest_1115z00_2034;

																							BgL__ortest_1115z00_2034 =
																								(BgL_classz00_2017 ==
																								BgL_oclassz00_2033);
																							if (BgL__ortest_1115z00_2034)
																								{	/* Module/pragma.scm 155 */
																									BgL_res1772z00_2050 =
																										BgL__ortest_1115z00_2034;
																								}
																							else
																								{	/* Module/pragma.scm 155 */
																									long BgL_odepthz00_2035;

																									{	/* Module/pragma.scm 155 */
																										obj_t BgL_arg1804z00_2036;

																										BgL_arg1804z00_2036 =
																											(BgL_oclassz00_2033);
																										BgL_odepthz00_2035 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2036);
																									}
																									if ((3L < BgL_odepthz00_2035))
																										{	/* Module/pragma.scm 155 */
																											obj_t BgL_arg1802z00_2038;

																											{	/* Module/pragma.scm 155 */
																												obj_t
																													BgL_arg1803z00_2039;
																												BgL_arg1803z00_2039 =
																													(BgL_oclassz00_2033);
																												BgL_arg1802z00_2038 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2039,
																													3L);
																											}
																											BgL_res1772z00_2050 =
																												(BgL_arg1802z00_2038 ==
																												BgL_classz00_2017);
																										}
																									else
																										{	/* Module/pragma.scm 155 */
																											BgL_res1772z00_2050 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1898z00_3143 =
																						BgL_res1772z00_2050;
																				}
																		}
																	}
																	if (BgL_test1898z00_3143)
																		{	/* Module/pragma.scm 157 */
																			obj_t BgL_arg1375z00_1498;

																			{	/* Module/pragma.scm 157 */
																				obj_t BgL_arg1376z00_1499;

																				BgL_arg1376z00_1499 =
																					(((BgL_sfunz00_bglt) COBJECT(
																							((BgL_sfunz00_bglt)
																								BgL_valz00_1496)))->
																					BgL_propertyz00);
																				BgL_arg1375z00_1498 =
																					MAKE_YOUNG_PAIR(BgL_propz00_13,
																					BgL_arg1376z00_1499);
																			}
																			return
																				((((BgL_sfunz00_bglt) COBJECT(
																							((BgL_sfunz00_bglt)
																								BgL_valz00_1496)))->
																					BgL_propertyz00) =
																				((obj_t) BgL_arg1375z00_1498), BUNSPEC);
																		}
																	else
																		{	/* Module/pragma.scm 155 */
																			return
																				BGl_sfunzd2errorzd2zzmodule_pragmaz00
																				(BGl_string1806z00zzmodule_pragmaz00,
																				((BgL_globalz00_bglt)
																					BgL_globalz00_12));
																		}
																}
															}
														else
															{	/* Module/pragma.scm 126 */
																if ((BgL_propz00_13 == CNST_TABLE_REF(9)))
																	{	/* Module/pragma.scm 159 */
																		BgL_valuez00_bglt BgL_valz00_1501;

																		BgL_valz00_1501 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_globalz00_12))))->
																			BgL_valuez00);
																		{	/* Module/pragma.scm 160 */
																			bool_t BgL_test1903z00_3180;

																			{	/* Module/pragma.scm 160 */
																				obj_t BgL_classz00_2055;

																				BgL_classz00_2055 =
																					BGl_cfunz00zzast_varz00;
																				{	/* Module/pragma.scm 160 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_2057;
																					{	/* Module/pragma.scm 160 */
																						obj_t BgL_tmpz00_3181;

																						BgL_tmpz00_3181 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_valz00_1501));
																						BgL_arg1807z00_2057 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_3181);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Module/pragma.scm 160 */
																							long BgL_idxz00_2063;

																							BgL_idxz00_2063 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_2057);
																							BgL_test1903z00_3180 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_2063 + 3L)) ==
																								BgL_classz00_2055);
																						}
																					else
																						{	/* Module/pragma.scm 160 */
																							bool_t BgL_res1773z00_2088;

																							{	/* Module/pragma.scm 160 */
																								obj_t BgL_oclassz00_2071;

																								{	/* Module/pragma.scm 160 */
																									obj_t BgL_arg1815z00_2079;
																									long BgL_arg1816z00_2080;

																									BgL_arg1815z00_2079 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Module/pragma.scm 160 */
																										long BgL_arg1817z00_2081;

																										BgL_arg1817z00_2081 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_2057);
																										BgL_arg1816z00_2080 =
																											(BgL_arg1817z00_2081 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_2071 =
																										VECTOR_REF
																										(BgL_arg1815z00_2079,
																										BgL_arg1816z00_2080);
																								}
																								{	/* Module/pragma.scm 160 */
																									bool_t
																										BgL__ortest_1115z00_2072;
																									BgL__ortest_1115z00_2072 =
																										(BgL_classz00_2055 ==
																										BgL_oclassz00_2071);
																									if (BgL__ortest_1115z00_2072)
																										{	/* Module/pragma.scm 160 */
																											BgL_res1773z00_2088 =
																												BgL__ortest_1115z00_2072;
																										}
																									else
																										{	/* Module/pragma.scm 160 */
																											long BgL_odepthz00_2073;

																											{	/* Module/pragma.scm 160 */
																												obj_t
																													BgL_arg1804z00_2074;
																												BgL_arg1804z00_2074 =
																													(BgL_oclassz00_2071);
																												BgL_odepthz00_2073 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_2074);
																											}
																											if (
																												(3L <
																													BgL_odepthz00_2073))
																												{	/* Module/pragma.scm 160 */
																													obj_t
																														BgL_arg1802z00_2076;
																													{	/* Module/pragma.scm 160 */
																														obj_t
																															BgL_arg1803z00_2077;
																														BgL_arg1803z00_2077
																															=
																															(BgL_oclassz00_2071);
																														BgL_arg1802z00_2076
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_2077,
																															3L);
																													}
																													BgL_res1773z00_2088 =
																														(BgL_arg1802z00_2076
																														==
																														BgL_classz00_2055);
																												}
																											else
																												{	/* Module/pragma.scm 160 */
																													BgL_res1773z00_2088 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test1903z00_3180 =
																								BgL_res1773z00_2088;
																						}
																				}
																			}
																			if (BgL_test1903z00_3180)
																				{	/* Module/pragma.scm 162 */
																					obj_t BgL_arg1408z00_1503;

																					{	/* Module/pragma.scm 162 */
																						obj_t BgL_arg1410z00_1504;

																						BgL_arg1410z00_1504 =
																							(((BgL_globalz00_bglt) COBJECT(
																									((BgL_globalz00_bglt)
																										BgL_globalz00_12)))->
																							BgL_pragmaz00);
																						BgL_arg1408z00_1503 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(9),
																							BgL_arg1410z00_1504);
																					}
																					return
																						((((BgL_globalz00_bglt) COBJECT(
																									((BgL_globalz00_bglt)
																										BgL_globalz00_12)))->
																							BgL_pragmaz00) =
																						((obj_t) BgL_arg1408z00_1503),
																						BUNSPEC);
																				}
																			else
																				{	/* Module/pragma.scm 160 */
																					return BFALSE;
																				}
																		}
																	}
																else
																	{	/* Module/pragma.scm 126 */
																		if ((BgL_propz00_13 == CNST_TABLE_REF(10)))
																			{	/* Module/pragma.scm 164 */
																				BgL_valuez00_bglt BgL_valz00_1506;

																				BgL_valz00_1506 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_globalz00_12))))->
																					BgL_valuez00);
																				{	/* Module/pragma.scm 165 */
																					bool_t BgL_test1908z00_3216;

																					{	/* Module/pragma.scm 165 */
																						obj_t BgL_classz00_2093;

																						BgL_classz00_2093 =
																							BGl_cfunz00zzast_varz00;
																						{	/* Module/pragma.scm 165 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_2095;
																							{	/* Module/pragma.scm 165 */
																								obj_t BgL_tmpz00_3217;

																								BgL_tmpz00_3217 =
																									((obj_t)
																									((BgL_objectz00_bglt)
																										BgL_valz00_1506));
																								BgL_arg1807z00_2095 =
																									(BgL_objectz00_bglt)
																									(BgL_tmpz00_3217);
																							}
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Module/pragma.scm 165 */
																									long BgL_idxz00_2101;

																									BgL_idxz00_2101 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_2095);
																									BgL_test1908z00_3216 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_2101 + 3L)) ==
																										BgL_classz00_2093);
																								}
																							else
																								{	/* Module/pragma.scm 165 */
																									bool_t BgL_res1774z00_2126;

																									{	/* Module/pragma.scm 165 */
																										obj_t BgL_oclassz00_2109;

																										{	/* Module/pragma.scm 165 */
																											obj_t BgL_arg1815z00_2117;
																											long BgL_arg1816z00_2118;

																											BgL_arg1815z00_2117 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Module/pragma.scm 165 */
																												long
																													BgL_arg1817z00_2119;
																												BgL_arg1817z00_2119 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_2095);
																												BgL_arg1816z00_2118 =
																													(BgL_arg1817z00_2119 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_2109 =
																												VECTOR_REF
																												(BgL_arg1815z00_2117,
																												BgL_arg1816z00_2118);
																										}
																										{	/* Module/pragma.scm 165 */
																											bool_t
																												BgL__ortest_1115z00_2110;
																											BgL__ortest_1115z00_2110 =
																												(BgL_classz00_2093 ==
																												BgL_oclassz00_2109);
																											if (BgL__ortest_1115z00_2110)
																												{	/* Module/pragma.scm 165 */
																													BgL_res1774z00_2126 =
																														BgL__ortest_1115z00_2110;
																												}
																											else
																												{	/* Module/pragma.scm 165 */
																													long
																														BgL_odepthz00_2111;
																													{	/* Module/pragma.scm 165 */
																														obj_t
																															BgL_arg1804z00_2112;
																														BgL_arg1804z00_2112
																															=
																															(BgL_oclassz00_2109);
																														BgL_odepthz00_2111 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_2112);
																													}
																													if (
																														(3L <
																															BgL_odepthz00_2111))
																														{	/* Module/pragma.scm 165 */
																															obj_t
																																BgL_arg1802z00_2114;
																															{	/* Module/pragma.scm 165 */
																																obj_t
																																	BgL_arg1803z00_2115;
																																BgL_arg1803z00_2115
																																	=
																																	(BgL_oclassz00_2109);
																																BgL_arg1802z00_2114
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_2115,
																																	3L);
																															}
																															BgL_res1774z00_2126
																																=
																																(BgL_arg1802z00_2114
																																==
																																BgL_classz00_2093);
																														}
																													else
																														{	/* Module/pragma.scm 165 */
																															BgL_res1774z00_2126
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test1908z00_3216 =
																										BgL_res1774z00_2126;
																								}
																						}
																					}
																					if (BgL_test1908z00_3216)
																						{	/* Module/pragma.scm 168 */
																							obj_t BgL_arg1421z00_1508;

																							{	/* Module/pragma.scm 168 */
																								obj_t BgL_arg1422z00_1509;

																								BgL_arg1422z00_1509 =
																									(((BgL_globalz00_bglt)
																										COBJECT((
																												(BgL_globalz00_bglt)
																												BgL_globalz00_12)))->
																									BgL_pragmaz00);
																								BgL_arg1421z00_1508 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(10), BgL_arg1422z00_1509);
																							}
																							return
																								((((BgL_globalz00_bglt) COBJECT(
																											((BgL_globalz00_bglt)
																												BgL_globalz00_12)))->
																									BgL_pragmaz00) =
																								((obj_t) BgL_arg1421z00_1508),
																								BUNSPEC);
																						}
																					else
																						{	/* Module/pragma.scm 165 */
																							return BFALSE;
																						}
																				}
																			}
																		else
																			{	/* Module/pragma.scm 126 */
																				if (
																					(BgL_propz00_13 ==
																						CNST_TABLE_REF(11)))
																					{	/* Module/pragma.scm 170 */
																						BgL_valuez00_bglt BgL_valz00_1511;

																						BgL_valz00_1511 =
																							(((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										((BgL_globalz00_bglt)
																											BgL_globalz00_12))))->
																							BgL_valuez00);
																						{	/* Module/pragma.scm 171 */
																							bool_t BgL_test1913z00_3252;

																							{	/* Module/pragma.scm 171 */
																								bool_t BgL_test1914z00_3253;

																								{	/* Module/pragma.scm 171 */
																									obj_t BgL_classz00_2131;

																									BgL_classz00_2131 =
																										BGl_sfunz00zzast_varz00;
																									{	/* Module/pragma.scm 171 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_2133;
																										{	/* Module/pragma.scm 171 */
																											obj_t BgL_tmpz00_3254;

																											BgL_tmpz00_3254 =
																												((obj_t)
																												((BgL_objectz00_bglt)
																													BgL_valz00_1511));
																											BgL_arg1807z00_2133 =
																												(BgL_objectz00_bglt)
																												(BgL_tmpz00_3254);
																										}
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Module/pragma.scm 171 */
																												long BgL_idxz00_2139;

																												BgL_idxz00_2139 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_2133);
																												BgL_test1914z00_3253 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_2139 +
																															3L)) ==
																													BgL_classz00_2131);
																											}
																										else
																											{	/* Module/pragma.scm 171 */
																												bool_t
																													BgL_res1775z00_2164;
																												{	/* Module/pragma.scm 171 */
																													obj_t
																														BgL_oclassz00_2147;
																													{	/* Module/pragma.scm 171 */
																														obj_t
																															BgL_arg1815z00_2155;
																														long
																															BgL_arg1816z00_2156;
																														BgL_arg1815z00_2155
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Module/pragma.scm 171 */
																															long
																																BgL_arg1817z00_2157;
																															BgL_arg1817z00_2157
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_2133);
																															BgL_arg1816z00_2156
																																=
																																(BgL_arg1817z00_2157
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_2147 =
																															VECTOR_REF
																															(BgL_arg1815z00_2155,
																															BgL_arg1816z00_2156);
																													}
																													{	/* Module/pragma.scm 171 */
																														bool_t
																															BgL__ortest_1115z00_2148;
																														BgL__ortest_1115z00_2148
																															=
																															(BgL_classz00_2131
																															==
																															BgL_oclassz00_2147);
																														if (BgL__ortest_1115z00_2148)
																															{	/* Module/pragma.scm 171 */
																																BgL_res1775z00_2164
																																	=
																																	BgL__ortest_1115z00_2148;
																															}
																														else
																															{	/* Module/pragma.scm 171 */
																																long
																																	BgL_odepthz00_2149;
																																{	/* Module/pragma.scm 171 */
																																	obj_t
																																		BgL_arg1804z00_2150;
																																	BgL_arg1804z00_2150
																																		=
																																		(BgL_oclassz00_2147);
																																	BgL_odepthz00_2149
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_2150);
																																}
																																if (
																																	(3L <
																																		BgL_odepthz00_2149))
																																	{	/* Module/pragma.scm 171 */
																																		obj_t
																																			BgL_arg1802z00_2152;
																																		{	/* Module/pragma.scm 171 */
																																			obj_t
																																				BgL_arg1803z00_2153;
																																			BgL_arg1803z00_2153
																																				=
																																				(BgL_oclassz00_2147);
																																			BgL_arg1802z00_2152
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_2153,
																																				3L);
																																		}
																																		BgL_res1775z00_2164
																																			=
																																			(BgL_arg1802z00_2152
																																			==
																																			BgL_classz00_2131);
																																	}
																																else
																																	{	/* Module/pragma.scm 171 */
																																		BgL_res1775z00_2164
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test1914z00_3253 =
																													BgL_res1775z00_2164;
																											}
																									}
																								}
																								if (BgL_test1914z00_3253)
																									{	/* Module/pragma.scm 171 */
																										BgL_test1913z00_3252 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Module/pragma.scm 171 */
																										obj_t BgL_classz00_2165;

																										BgL_classz00_2165 =
																											BGl_cfunz00zzast_varz00;
																										{	/* Module/pragma.scm 171 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_2167;
																											{	/* Module/pragma.scm 171 */
																												obj_t BgL_tmpz00_3277;

																												BgL_tmpz00_3277 =
																													((obj_t)
																													((BgL_objectz00_bglt)
																														BgL_valz00_1511));
																												BgL_arg1807z00_2167 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_3277);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Module/pragma.scm 171 */
																													long BgL_idxz00_2173;

																													BgL_idxz00_2173 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_2167);
																													BgL_test1913z00_3252 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_2173 +
																																3L)) ==
																														BgL_classz00_2165);
																												}
																											else
																												{	/* Module/pragma.scm 171 */
																													bool_t
																														BgL_res1776z00_2198;
																													{	/* Module/pragma.scm 171 */
																														obj_t
																															BgL_oclassz00_2181;
																														{	/* Module/pragma.scm 171 */
																															obj_t
																																BgL_arg1815z00_2189;
																															long
																																BgL_arg1816z00_2190;
																															BgL_arg1815z00_2189
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Module/pragma.scm 171 */
																																long
																																	BgL_arg1817z00_2191;
																																BgL_arg1817z00_2191
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_2167);
																																BgL_arg1816z00_2190
																																	=
																																	(BgL_arg1817z00_2191
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_2181
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_2189,
																																BgL_arg1816z00_2190);
																														}
																														{	/* Module/pragma.scm 171 */
																															bool_t
																																BgL__ortest_1115z00_2182;
																															BgL__ortest_1115z00_2182
																																=
																																(BgL_classz00_2165
																																==
																																BgL_oclassz00_2181);
																															if (BgL__ortest_1115z00_2182)
																																{	/* Module/pragma.scm 171 */
																																	BgL_res1776z00_2198
																																		=
																																		BgL__ortest_1115z00_2182;
																																}
																															else
																																{	/* Module/pragma.scm 171 */
																																	long
																																		BgL_odepthz00_2183;
																																	{	/* Module/pragma.scm 171 */
																																		obj_t
																																			BgL_arg1804z00_2184;
																																		BgL_arg1804z00_2184
																																			=
																																			(BgL_oclassz00_2181);
																																		BgL_odepthz00_2183
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_2184);
																																	}
																																	if (
																																		(3L <
																																			BgL_odepthz00_2183))
																																		{	/* Module/pragma.scm 171 */
																																			obj_t
																																				BgL_arg1802z00_2186;
																																			{	/* Module/pragma.scm 171 */
																																				obj_t
																																					BgL_arg1803z00_2187;
																																				BgL_arg1803z00_2187
																																					=
																																					(BgL_oclassz00_2181);
																																				BgL_arg1802z00_2186
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_2187,
																																					3L);
																																			}
																																			BgL_res1776z00_2198
																																				=
																																				(BgL_arg1802z00_2186
																																				==
																																				BgL_classz00_2165);
																																		}
																																	else
																																		{	/* Module/pragma.scm 171 */
																																			BgL_res1776z00_2198
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test1913z00_3252 =
																														BgL_res1776z00_2198;
																												}
																										}
																									}
																							}
																							if (BgL_test1913z00_3252)
																								{	/* Module/pragma.scm 174 */
																									obj_t BgL_arg1434z00_1514;

																									{	/* Module/pragma.scm 174 */
																										obj_t BgL_arg1437z00_1515;

																										BgL_arg1437z00_1515 =
																											(((BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_globalz00_12)))->
																											BgL_pragmaz00);
																										BgL_arg1434z00_1514 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(11),
																											BgL_arg1437z00_1515);
																									}
																									return
																										((((BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_globalz00_12)))->
																											BgL_pragmaz00) =
																										((obj_t)
																											BgL_arg1434z00_1514),
																										BUNSPEC);
																								}
																							else
																								{	/* Module/pragma.scm 171 */
																									return BFALSE;
																								}
																						}
																					}
																				else
																					{	/* Module/pragma.scm 126 */
																						if (
																							(BgL_propz00_13 ==
																								CNST_TABLE_REF(12)))
																							{	/* Module/pragma.scm 177 */
																								BgL_valuez00_bglt
																									BgL_valz00_1518;
																								BgL_valz00_1518 =
																									(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_globalz00_bglt)
																													BgL_globalz00_12))))->
																									BgL_valuez00);
																								{	/* Module/pragma.scm 178 */
																									bool_t BgL_test1922z00_3312;

																									{	/* Module/pragma.scm 178 */
																										bool_t BgL_test1923z00_3313;

																										{	/* Module/pragma.scm 178 */
																											obj_t BgL_classz00_2203;

																											BgL_classz00_2203 =
																												BGl_sfunz00zzast_varz00;
																											{	/* Module/pragma.scm 178 */
																												BgL_objectz00_bglt
																													BgL_arg1807z00_2205;
																												{	/* Module/pragma.scm 178 */
																													obj_t BgL_tmpz00_3314;

																													BgL_tmpz00_3314 =
																														((obj_t)
																														((BgL_objectz00_bglt) BgL_valz00_1518));
																													BgL_arg1807z00_2205 =
																														(BgL_objectz00_bglt)
																														(BgL_tmpz00_3314);
																												}
																												if (BGL_CONDEXPAND_ISA_ARCH64())
																													{	/* Module/pragma.scm 178 */
																														long
																															BgL_idxz00_2211;
																														BgL_idxz00_2211 =
																															BGL_OBJECT_INHERITANCE_NUM
																															(BgL_arg1807z00_2205);
																														BgL_test1923z00_3313
																															=
																															(VECTOR_REF
																															(BGl_za2inheritancesza2z00zz__objectz00,
																																(BgL_idxz00_2211
																																	+ 3L)) ==
																															BgL_classz00_2203);
																													}
																												else
																													{	/* Module/pragma.scm 178 */
																														bool_t
																															BgL_res1777z00_2236;
																														{	/* Module/pragma.scm 178 */
																															obj_t
																																BgL_oclassz00_2219;
																															{	/* Module/pragma.scm 178 */
																																obj_t
																																	BgL_arg1815z00_2227;
																																long
																																	BgL_arg1816z00_2228;
																																BgL_arg1815z00_2227
																																	=
																																	(BGl_za2classesza2z00zz__objectz00);
																																{	/* Module/pragma.scm 178 */
																																	long
																																		BgL_arg1817z00_2229;
																																	BgL_arg1817z00_2229
																																		=
																																		BGL_OBJECT_CLASS_NUM
																																		(BgL_arg1807z00_2205);
																																	BgL_arg1816z00_2228
																																		=
																																		(BgL_arg1817z00_2229
																																		-
																																		OBJECT_TYPE);
																																}
																																BgL_oclassz00_2219
																																	=
																																	VECTOR_REF
																																	(BgL_arg1815z00_2227,
																																	BgL_arg1816z00_2228);
																															}
																															{	/* Module/pragma.scm 178 */
																																bool_t
																																	BgL__ortest_1115z00_2220;
																																BgL__ortest_1115z00_2220
																																	=
																																	(BgL_classz00_2203
																																	==
																																	BgL_oclassz00_2219);
																																if (BgL__ortest_1115z00_2220)
																																	{	/* Module/pragma.scm 178 */
																																		BgL_res1777z00_2236
																																			=
																																			BgL__ortest_1115z00_2220;
																																	}
																																else
																																	{	/* Module/pragma.scm 178 */
																																		long
																																			BgL_odepthz00_2221;
																																		{	/* Module/pragma.scm 178 */
																																			obj_t
																																				BgL_arg1804z00_2222;
																																			BgL_arg1804z00_2222
																																				=
																																				(BgL_oclassz00_2219);
																																			BgL_odepthz00_2221
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_arg1804z00_2222);
																																		}
																																		if (
																																			(3L <
																																				BgL_odepthz00_2221))
																																			{	/* Module/pragma.scm 178 */
																																				obj_t
																																					BgL_arg1802z00_2224;
																																				{	/* Module/pragma.scm 178 */
																																					obj_t
																																						BgL_arg1803z00_2225;
																																					BgL_arg1803z00_2225
																																						=
																																						(BgL_oclassz00_2219);
																																					BgL_arg1802z00_2224
																																						=
																																						BGL_CLASS_ANCESTORS_REF
																																						(BgL_arg1803z00_2225,
																																						3L);
																																				}
																																				BgL_res1777z00_2236
																																					=
																																					(BgL_arg1802z00_2224
																																					==
																																					BgL_classz00_2203);
																																			}
																																		else
																																			{	/* Module/pragma.scm 178 */
																																				BgL_res1777z00_2236
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																															}
																														}
																														BgL_test1923z00_3313
																															=
																															BgL_res1777z00_2236;
																													}
																											}
																										}
																										if (BgL_test1923z00_3313)
																											{	/* Module/pragma.scm 178 */
																												BgL_test1922z00_3312 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Module/pragma.scm 178 */
																												obj_t BgL_classz00_2237;

																												BgL_classz00_2237 =
																													BGl_cfunz00zzast_varz00;
																												{	/* Module/pragma.scm 178 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_2239;
																													{	/* Module/pragma.scm 178 */
																														obj_t
																															BgL_tmpz00_3337;
																														BgL_tmpz00_3337 =
																															((obj_t) (
																																(BgL_objectz00_bglt)
																																BgL_valz00_1518));
																														BgL_arg1807z00_2239
																															=
																															(BgL_objectz00_bglt)
																															(BgL_tmpz00_3337);
																													}
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Module/pragma.scm 178 */
																															long
																																BgL_idxz00_2245;
																															BgL_idxz00_2245 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_2239);
																															BgL_test1922z00_3312
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_2245
																																		+ 3L)) ==
																																BgL_classz00_2237);
																														}
																													else
																														{	/* Module/pragma.scm 178 */
																															bool_t
																																BgL_res1778z00_2270;
																															{	/* Module/pragma.scm 178 */
																																obj_t
																																	BgL_oclassz00_2253;
																																{	/* Module/pragma.scm 178 */
																																	obj_t
																																		BgL_arg1815z00_2261;
																																	long
																																		BgL_arg1816z00_2262;
																																	BgL_arg1815z00_2261
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Module/pragma.scm 178 */
																																		long
																																			BgL_arg1817z00_2263;
																																		BgL_arg1817z00_2263
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_2239);
																																		BgL_arg1816z00_2262
																																			=
																																			(BgL_arg1817z00_2263
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_2253
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_2261,
																																		BgL_arg1816z00_2262);
																																}
																																{	/* Module/pragma.scm 178 */
																																	bool_t
																																		BgL__ortest_1115z00_2254;
																																	BgL__ortest_1115z00_2254
																																		=
																																		(BgL_classz00_2237
																																		==
																																		BgL_oclassz00_2253);
																																	if (BgL__ortest_1115z00_2254)
																																		{	/* Module/pragma.scm 178 */
																																			BgL_res1778z00_2270
																																				=
																																				BgL__ortest_1115z00_2254;
																																		}
																																	else
																																		{	/* Module/pragma.scm 178 */
																																			long
																																				BgL_odepthz00_2255;
																																			{	/* Module/pragma.scm 178 */
																																				obj_t
																																					BgL_arg1804z00_2256;
																																				BgL_arg1804z00_2256
																																					=
																																					(BgL_oclassz00_2253);
																																				BgL_odepthz00_2255
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_2256);
																																			}
																																			if (
																																				(3L <
																																					BgL_odepthz00_2255))
																																				{	/* Module/pragma.scm 178 */
																																					obj_t
																																						BgL_arg1802z00_2258;
																																					{	/* Module/pragma.scm 178 */
																																						obj_t
																																							BgL_arg1803z00_2259;
																																						BgL_arg1803z00_2259
																																							=
																																							(BgL_oclassz00_2253);
																																						BgL_arg1802z00_2258
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_2259,
																																							3L);
																																					}
																																					BgL_res1778z00_2270
																																						=
																																						(BgL_arg1802z00_2258
																																						==
																																						BgL_classz00_2237);
																																				}
																																			else
																																				{	/* Module/pragma.scm 178 */
																																					BgL_res1778z00_2270
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test1922z00_3312
																																=
																																BgL_res1778z00_2270;
																														}
																												}
																											}
																									}
																									if (BgL_test1922z00_3312)
																										{	/* Module/pragma.scm 181 */
																											obj_t BgL_arg1448z00_1521;

																											{	/* Module/pragma.scm 181 */
																												obj_t
																													BgL_arg1453z00_1522;
																												BgL_arg1453z00_1522 =
																													(((BgL_globalz00_bglt)
																														COBJECT((
																																(BgL_globalz00_bglt)
																																BgL_globalz00_12)))->
																													BgL_pragmaz00);
																												BgL_arg1448z00_1521 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(12),
																													BgL_arg1453z00_1522);
																											}
																											return
																												((((BgL_globalz00_bglt)
																														COBJECT((
																																(BgL_globalz00_bglt)
																																BgL_globalz00_12)))->
																													BgL_pragmaz00) =
																												((obj_t)
																													BgL_arg1448z00_1521),
																												BUNSPEC);
																										}
																									else
																										{	/* Module/pragma.scm 178 */
																											return BFALSE;
																										}
																								}
																							}
																						else
																							{	/* Module/pragma.scm 126 */
																								if (
																									(BgL_propz00_13 ==
																										CNST_TABLE_REF(13)))
																									{	/* Module/pragma.scm 185 */
																										BgL_valuez00_bglt
																											BgL_valz00_1525;
																										BgL_valz00_1525 =
																											(((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_globalz00_bglt) BgL_globalz00_12))))->BgL_valuez00);
																										{	/* Module/pragma.scm 186 */
																											bool_t
																												BgL_test1931z00_3372;
																											{	/* Module/pragma.scm 186 */
																												bool_t
																													BgL_test1932z00_3373;
																												{	/* Module/pragma.scm 186 */
																													obj_t
																														BgL_classz00_2275;
																													BgL_classz00_2275 =
																														BGl_sfunz00zzast_varz00;
																													{	/* Module/pragma.scm 186 */
																														BgL_objectz00_bglt
																															BgL_arg1807z00_2277;
																														{	/* Module/pragma.scm 186 */
																															obj_t
																																BgL_tmpz00_3374;
																															BgL_tmpz00_3374 =
																																((obj_t) (
																																	(BgL_objectz00_bglt)
																																	BgL_valz00_1525));
																															BgL_arg1807z00_2277
																																=
																																(BgL_objectz00_bglt)
																																(BgL_tmpz00_3374);
																														}
																														if (BGL_CONDEXPAND_ISA_ARCH64())
																															{	/* Module/pragma.scm 186 */
																																long
																																	BgL_idxz00_2283;
																																BgL_idxz00_2283
																																	=
																																	BGL_OBJECT_INHERITANCE_NUM
																																	(BgL_arg1807z00_2277);
																																BgL_test1932z00_3373
																																	=
																																	(VECTOR_REF
																																	(BGl_za2inheritancesza2z00zz__objectz00,
																																		(BgL_idxz00_2283
																																			+ 3L)) ==
																																	BgL_classz00_2275);
																															}
																														else
																															{	/* Module/pragma.scm 186 */
																																bool_t
																																	BgL_res1779z00_2308;
																																{	/* Module/pragma.scm 186 */
																																	obj_t
																																		BgL_oclassz00_2291;
																																	{	/* Module/pragma.scm 186 */
																																		obj_t
																																			BgL_arg1815z00_2299;
																																		long
																																			BgL_arg1816z00_2300;
																																		BgL_arg1815z00_2299
																																			=
																																			(BGl_za2classesza2z00zz__objectz00);
																																		{	/* Module/pragma.scm 186 */
																																			long
																																				BgL_arg1817z00_2301;
																																			BgL_arg1817z00_2301
																																				=
																																				BGL_OBJECT_CLASS_NUM
																																				(BgL_arg1807z00_2277);
																																			BgL_arg1816z00_2300
																																				=
																																				(BgL_arg1817z00_2301
																																				-
																																				OBJECT_TYPE);
																																		}
																																		BgL_oclassz00_2291
																																			=
																																			VECTOR_REF
																																			(BgL_arg1815z00_2299,
																																			BgL_arg1816z00_2300);
																																	}
																																	{	/* Module/pragma.scm 186 */
																																		bool_t
																																			BgL__ortest_1115z00_2292;
																																		BgL__ortest_1115z00_2292
																																			=
																																			(BgL_classz00_2275
																																			==
																																			BgL_oclassz00_2291);
																																		if (BgL__ortest_1115z00_2292)
																																			{	/* Module/pragma.scm 186 */
																																				BgL_res1779z00_2308
																																					=
																																					BgL__ortest_1115z00_2292;
																																			}
																																		else
																																			{	/* Module/pragma.scm 186 */
																																				long
																																					BgL_odepthz00_2293;
																																				{	/* Module/pragma.scm 186 */
																																					obj_t
																																						BgL_arg1804z00_2294;
																																					BgL_arg1804z00_2294
																																						=
																																						(BgL_oclassz00_2291);
																																					BgL_odepthz00_2293
																																						=
																																						BGL_CLASS_DEPTH
																																						(BgL_arg1804z00_2294);
																																				}
																																				if (
																																					(3L <
																																						BgL_odepthz00_2293))
																																					{	/* Module/pragma.scm 186 */
																																						obj_t
																																							BgL_arg1802z00_2296;
																																						{	/* Module/pragma.scm 186 */
																																							obj_t
																																								BgL_arg1803z00_2297;
																																							BgL_arg1803z00_2297
																																								=
																																								(BgL_oclassz00_2291);
																																							BgL_arg1802z00_2296
																																								=
																																								BGL_CLASS_ANCESTORS_REF
																																								(BgL_arg1803z00_2297,
																																								3L);
																																						}
																																						BgL_res1779z00_2308
																																							=
																																							(BgL_arg1802z00_2296
																																							==
																																							BgL_classz00_2275);
																																					}
																																				else
																																					{	/* Module/pragma.scm 186 */
																																						BgL_res1779z00_2308
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																	}
																																}
																																BgL_test1932z00_3373
																																	=
																																	BgL_res1779z00_2308;
																															}
																													}
																												}
																												if (BgL_test1932z00_3373)
																													{	/* Module/pragma.scm 186 */
																														BgL_test1931z00_3372
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Module/pragma.scm 186 */
																														obj_t
																															BgL_classz00_2309;
																														BgL_classz00_2309 =
																															BGl_cfunz00zzast_varz00;
																														{	/* Module/pragma.scm 186 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_2311;
																															{	/* Module/pragma.scm 186 */
																																obj_t
																																	BgL_tmpz00_3397;
																																BgL_tmpz00_3397
																																	=
																																	((obj_t) (
																																		(BgL_objectz00_bglt)
																																		BgL_valz00_1525));
																																BgL_arg1807z00_2311
																																	=
																																	(BgL_objectz00_bglt)
																																	(BgL_tmpz00_3397);
																															}
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Module/pragma.scm 186 */
																																	long
																																		BgL_idxz00_2317;
																																	BgL_idxz00_2317
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_2311);
																																	BgL_test1931z00_3372
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_2317
																																				+
																																				3L)) ==
																																		BgL_classz00_2309);
																																}
																															else
																																{	/* Module/pragma.scm 186 */
																																	bool_t
																																		BgL_res1780z00_2342;
																																	{	/* Module/pragma.scm 186 */
																																		obj_t
																																			BgL_oclassz00_2325;
																																		{	/* Module/pragma.scm 186 */
																																			obj_t
																																				BgL_arg1815z00_2333;
																																			long
																																				BgL_arg1816z00_2334;
																																			BgL_arg1815z00_2333
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Module/pragma.scm 186 */
																																				long
																																					BgL_arg1817z00_2335;
																																				BgL_arg1817z00_2335
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_2311);
																																				BgL_arg1816z00_2334
																																					=
																																					(BgL_arg1817z00_2335
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_2325
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_2333,
																																				BgL_arg1816z00_2334);
																																		}
																																		{	/* Module/pragma.scm 186 */
																																			bool_t
																																				BgL__ortest_1115z00_2326;
																																			BgL__ortest_1115z00_2326
																																				=
																																				(BgL_classz00_2309
																																				==
																																				BgL_oclassz00_2325);
																																			if (BgL__ortest_1115z00_2326)
																																				{	/* Module/pragma.scm 186 */
																																					BgL_res1780z00_2342
																																						=
																																						BgL__ortest_1115z00_2326;
																																				}
																																			else
																																				{	/* Module/pragma.scm 186 */
																																					long
																																						BgL_odepthz00_2327;
																																					{	/* Module/pragma.scm 186 */
																																						obj_t
																																							BgL_arg1804z00_2328;
																																						BgL_arg1804z00_2328
																																							=
																																							(BgL_oclassz00_2325);
																																						BgL_odepthz00_2327
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_2328);
																																					}
																																					if (
																																						(3L
																																							<
																																							BgL_odepthz00_2327))
																																						{	/* Module/pragma.scm 186 */
																																							obj_t
																																								BgL_arg1802z00_2330;
																																							{	/* Module/pragma.scm 186 */
																																								obj_t
																																									BgL_arg1803z00_2331;
																																								BgL_arg1803z00_2331
																																									=
																																									(BgL_oclassz00_2325);
																																								BgL_arg1802z00_2330
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_2331,
																																									3L);
																																							}
																																							BgL_res1780z00_2342
																																								=
																																								(BgL_arg1802z00_2330
																																								==
																																								BgL_classz00_2309);
																																						}
																																					else
																																						{	/* Module/pragma.scm 186 */
																																							BgL_res1780z00_2342
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test1931z00_3372
																																		=
																																		BgL_res1780z00_2342;
																																}
																														}
																													}
																											}
																											if (BgL_test1931z00_3372)
																												{	/* Module/pragma.scm 189 */
																													obj_t
																														BgL_arg1472z00_1528;
																													{	/* Module/pragma.scm 189 */
																														obj_t
																															BgL_arg1473z00_1529;
																														BgL_arg1473z00_1529
																															=
																															(((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt) BgL_globalz00_12)))->BgL_pragmaz00);
																														BgL_arg1472z00_1528
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(13),
																															BgL_arg1473z00_1529);
																													}
																													return
																														((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt) BgL_globalz00_12)))->BgL_pragmaz00) = ((obj_t) BgL_arg1472z00_1528), BUNSPEC);
																												}
																											else
																												{	/* Module/pragma.scm 186 */
																													return BFALSE;
																												}
																										}
																									}
																								else
																									{	/* Module/pragma.scm 126 */
																										if (
																											(BgL_propz00_13 ==
																												CNST_TABLE_REF(14)))
																											{	/* Module/pragma.scm 192 */
																												BgL_valuez00_bglt
																													BgL_valz00_1532;
																												BgL_valz00_1532 =
																													(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_globalz00_12))))->BgL_valuez00);
																												{	/* Module/pragma.scm 193 */
																													bool_t
																														BgL_test1940z00_3432;
																													{	/* Module/pragma.scm 193 */
																														bool_t
																															BgL_test1941z00_3433;
																														{	/* Module/pragma.scm 193 */
																															obj_t
																																BgL_classz00_2347;
																															BgL_classz00_2347
																																=
																																BGl_sfunz00zzast_varz00;
																															{	/* Module/pragma.scm 193 */
																																BgL_objectz00_bglt
																																	BgL_arg1807z00_2349;
																																{	/* Module/pragma.scm 193 */
																																	obj_t
																																		BgL_tmpz00_3434;
																																	BgL_tmpz00_3434
																																		=
																																		((obj_t) (
																																			(BgL_objectz00_bglt)
																																			BgL_valz00_1532));
																																	BgL_arg1807z00_2349
																																		=
																																		(BgL_objectz00_bglt)
																																		(BgL_tmpz00_3434);
																																}
																																if (BGL_CONDEXPAND_ISA_ARCH64())
																																	{	/* Module/pragma.scm 193 */
																																		long
																																			BgL_idxz00_2355;
																																		BgL_idxz00_2355
																																			=
																																			BGL_OBJECT_INHERITANCE_NUM
																																			(BgL_arg1807z00_2349);
																																		BgL_test1941z00_3433
																																			=
																																			(VECTOR_REF
																																			(BGl_za2inheritancesza2z00zz__objectz00,
																																				(BgL_idxz00_2355
																																					+
																																					3L))
																																			==
																																			BgL_classz00_2347);
																																	}
																																else
																																	{	/* Module/pragma.scm 193 */
																																		bool_t
																																			BgL_res1781z00_2380;
																																		{	/* Module/pragma.scm 193 */
																																			obj_t
																																				BgL_oclassz00_2363;
																																			{	/* Module/pragma.scm 193 */
																																				obj_t
																																					BgL_arg1815z00_2371;
																																				long
																																					BgL_arg1816z00_2372;
																																				BgL_arg1815z00_2371
																																					=
																																					(BGl_za2classesza2z00zz__objectz00);
																																				{	/* Module/pragma.scm 193 */
																																					long
																																						BgL_arg1817z00_2373;
																																					BgL_arg1817z00_2373
																																						=
																																						BGL_OBJECT_CLASS_NUM
																																						(BgL_arg1807z00_2349);
																																					BgL_arg1816z00_2372
																																						=
																																						(BgL_arg1817z00_2373
																																						-
																																						OBJECT_TYPE);
																																				}
																																				BgL_oclassz00_2363
																																					=
																																					VECTOR_REF
																																					(BgL_arg1815z00_2371,
																																					BgL_arg1816z00_2372);
																																			}
																																			{	/* Module/pragma.scm 193 */
																																				bool_t
																																					BgL__ortest_1115z00_2364;
																																				BgL__ortest_1115z00_2364
																																					=
																																					(BgL_classz00_2347
																																					==
																																					BgL_oclassz00_2363);
																																				if (BgL__ortest_1115z00_2364)
																																					{	/* Module/pragma.scm 193 */
																																						BgL_res1781z00_2380
																																							=
																																							BgL__ortest_1115z00_2364;
																																					}
																																				else
																																					{	/* Module/pragma.scm 193 */
																																						long
																																							BgL_odepthz00_2365;
																																						{	/* Module/pragma.scm 193 */
																																							obj_t
																																								BgL_arg1804z00_2366;
																																							BgL_arg1804z00_2366
																																								=
																																								(BgL_oclassz00_2363);
																																							BgL_odepthz00_2365
																																								=
																																								BGL_CLASS_DEPTH
																																								(BgL_arg1804z00_2366);
																																						}
																																						if (
																																							(3L
																																								<
																																								BgL_odepthz00_2365))
																																							{	/* Module/pragma.scm 193 */
																																								obj_t
																																									BgL_arg1802z00_2368;
																																								{	/* Module/pragma.scm 193 */
																																									obj_t
																																										BgL_arg1803z00_2369;
																																									BgL_arg1803z00_2369
																																										=
																																										(BgL_oclassz00_2363);
																																									BgL_arg1802z00_2368
																																										=
																																										BGL_CLASS_ANCESTORS_REF
																																										(BgL_arg1803z00_2369,
																																										3L);
																																								}
																																								BgL_res1781z00_2380
																																									=
																																									(BgL_arg1802z00_2368
																																									==
																																									BgL_classz00_2347);
																																							}
																																						else
																																							{	/* Module/pragma.scm 193 */
																																								BgL_res1781z00_2380
																																									=
																																									(
																																									(bool_t)
																																									0);
																																							}
																																					}
																																			}
																																		}
																																		BgL_test1941z00_3433
																																			=
																																			BgL_res1781z00_2380;
																																	}
																															}
																														}
																														if (BgL_test1941z00_3433)
																															{	/* Module/pragma.scm 193 */
																																BgL_test1940z00_3432
																																	=
																																	((bool_t) 1);
																															}
																														else
																															{	/* Module/pragma.scm 193 */
																																obj_t
																																	BgL_classz00_2381;
																																BgL_classz00_2381
																																	=
																																	BGl_cfunz00zzast_varz00;
																																{	/* Module/pragma.scm 193 */
																																	BgL_objectz00_bglt
																																		BgL_arg1807z00_2383;
																																	{	/* Module/pragma.scm 193 */
																																		obj_t
																																			BgL_tmpz00_3457;
																																		BgL_tmpz00_3457
																																			=
																																			((obj_t) (
																																				(BgL_objectz00_bglt)
																																				BgL_valz00_1532));
																																		BgL_arg1807z00_2383
																																			=
																																			(BgL_objectz00_bglt)
																																			(BgL_tmpz00_3457);
																																	}
																																	if (BGL_CONDEXPAND_ISA_ARCH64())
																																		{	/* Module/pragma.scm 193 */
																																			long
																																				BgL_idxz00_2389;
																																			BgL_idxz00_2389
																																				=
																																				BGL_OBJECT_INHERITANCE_NUM
																																				(BgL_arg1807z00_2383);
																																			BgL_test1940z00_3432
																																				=
																																				(VECTOR_REF
																																				(BGl_za2inheritancesza2z00zz__objectz00,
																																					(BgL_idxz00_2389
																																						+
																																						3L))
																																				==
																																				BgL_classz00_2381);
																																		}
																																	else
																																		{	/* Module/pragma.scm 193 */
																																			bool_t
																																				BgL_res1782z00_2414;
																																			{	/* Module/pragma.scm 193 */
																																				obj_t
																																					BgL_oclassz00_2397;
																																				{	/* Module/pragma.scm 193 */
																																					obj_t
																																						BgL_arg1815z00_2405;
																																					long
																																						BgL_arg1816z00_2406;
																																					BgL_arg1815z00_2405
																																						=
																																						(BGl_za2classesza2z00zz__objectz00);
																																					{	/* Module/pragma.scm 193 */
																																						long
																																							BgL_arg1817z00_2407;
																																						BgL_arg1817z00_2407
																																							=
																																							BGL_OBJECT_CLASS_NUM
																																							(BgL_arg1807z00_2383);
																																						BgL_arg1816z00_2406
																																							=
																																							(BgL_arg1817z00_2407
																																							-
																																							OBJECT_TYPE);
																																					}
																																					BgL_oclassz00_2397
																																						=
																																						VECTOR_REF
																																						(BgL_arg1815z00_2405,
																																						BgL_arg1816z00_2406);
																																				}
																																				{	/* Module/pragma.scm 193 */
																																					bool_t
																																						BgL__ortest_1115z00_2398;
																																					BgL__ortest_1115z00_2398
																																						=
																																						(BgL_classz00_2381
																																						==
																																						BgL_oclassz00_2397);
																																					if (BgL__ortest_1115z00_2398)
																																						{	/* Module/pragma.scm 193 */
																																							BgL_res1782z00_2414
																																								=
																																								BgL__ortest_1115z00_2398;
																																						}
																																					else
																																						{	/* Module/pragma.scm 193 */
																																							long
																																								BgL_odepthz00_2399;
																																							{	/* Module/pragma.scm 193 */
																																								obj_t
																																									BgL_arg1804z00_2400;
																																								BgL_arg1804z00_2400
																																									=
																																									(BgL_oclassz00_2397);
																																								BgL_odepthz00_2399
																																									=
																																									BGL_CLASS_DEPTH
																																									(BgL_arg1804z00_2400);
																																							}
																																							if ((3L < BgL_odepthz00_2399))
																																								{	/* Module/pragma.scm 193 */
																																									obj_t
																																										BgL_arg1802z00_2402;
																																									{	/* Module/pragma.scm 193 */
																																										obj_t
																																											BgL_arg1803z00_2403;
																																										BgL_arg1803z00_2403
																																											=
																																											(BgL_oclassz00_2397);
																																										BgL_arg1802z00_2402
																																											=
																																											BGL_CLASS_ANCESTORS_REF
																																											(BgL_arg1803z00_2403,
																																											3L);
																																									}
																																									BgL_res1782z00_2414
																																										=
																																										(BgL_arg1802z00_2402
																																										==
																																										BgL_classz00_2381);
																																								}
																																							else
																																								{	/* Module/pragma.scm 193 */
																																									BgL_res1782z00_2414
																																										=
																																										(
																																										(bool_t)
																																										0);
																																								}
																																						}
																																				}
																																			}
																																			BgL_test1940z00_3432
																																				=
																																				BgL_res1782z00_2414;
																																		}
																																}
																															}
																													}
																													if (BgL_test1940z00_3432)
																														{	/* Module/pragma.scm 193 */
																															return BFALSE;
																														}
																													else
																														{	/* Module/pragma.scm 196 */
																															obj_t
																																BgL_arg1485z00_1535;
																															{	/* Module/pragma.scm 196 */
																																obj_t
																																	BgL_arg1489z00_1536;
																																BgL_arg1489z00_1536
																																	=
																																	(((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt) BgL_globalz00_12)))->BgL_pragmaz00);
																																BgL_arg1485z00_1535
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(14),
																																	BgL_arg1489z00_1536);
																															}
																															return
																																((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt) BgL_globalz00_12)))->BgL_pragmaz00) = ((obj_t) BgL_arg1485z00_1535), BUNSPEC);
																														}
																												}
																											}
																										else
																											{	/* Module/pragma.scm 198 */
																												obj_t
																													BgL_list1490z00_1538;
																												BgL_list1490z00_1538 =
																													MAKE_YOUNG_PAIR(BNIL,
																													BNIL);
																												return
																													BGl_userzd2errorzd2zztools_errorz00
																													(BGl_string1807z00zzmodule_pragmaz00,
																													BGl_string1808z00zzmodule_pragmaz00,
																													BgL_clausez00_14,
																													BgL_list1490z00_1538);
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Module/pragma.scm 124 */
						if (PAIRP(BgL_propz00_13))
							{	/* Module/pragma.scm 124 */
								obj_t BgL_carzd2160zd2_1475;

								BgL_carzd2160zd2_1475 = CAR(((obj_t) BgL_propz00_13));
								if (SYMBOLP(BgL_carzd2160zd2_1475))
									{	/* Module/pragma.scm 124 */
										obj_t BgL_arg1346z00_1477;

										BgL_arg1346z00_1477 = CDR(((obj_t) BgL_propz00_13));
										BgL_keyz00_1468 = BgL_carzd2160zd2_1475;
										BgL_valz00_1469 = BgL_arg1346z00_1477;
										if ((BgL_keyz00_1468 == CNST_TABLE_REF(15)))
											{	/* Module/pragma.scm 203 */
												bool_t BgL_test1951z00_3499;

												if (PAIRP(BgL_valz00_1469))
													{	/* Module/pragma.scm 203 */
														obj_t BgL_tmpz00_3502;

														BgL_tmpz00_3502 = CAR(BgL_valz00_1469);
														BgL_test1951z00_3499 = SYMBOLP(BgL_tmpz00_3502);
													}
												else
													{	/* Module/pragma.scm 203 */
														BgL_test1951z00_3499 = ((bool_t) 0);
													}
												if (BgL_test1951z00_3499)
													{	/* Module/pragma.scm 205 */
														BgL_typez00_bglt BgL_typez00_1544;
														BgL_valuez00_bglt BgL_valuez00_1545;

														BgL_typez00_1544 =
															BGl_usezd2typez12zc0zztype_envz00(CAR
															(BgL_valz00_1469),
															BGl_findzd2locationzd2zztools_locationz00
															(BgL_propz00_13));
														BgL_valuez00_1545 =
															(((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt) (
																			(BgL_globalz00_bglt)
																			BgL_globalz00_12))))->BgL_valuez00);
														{	/* Module/pragma.scm 207 */
															bool_t BgL_test1953z00_3511;

															{	/* Module/pragma.scm 207 */
																obj_t BgL_classz00_2421;

																BgL_classz00_2421 = BGl_funz00zzast_varz00;
																{	/* Module/pragma.scm 207 */
																	BgL_objectz00_bglt BgL_arg1807z00_2423;

																	{	/* Module/pragma.scm 207 */
																		obj_t BgL_tmpz00_3512;

																		BgL_tmpz00_3512 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_valuez00_1545));
																		BgL_arg1807z00_2423 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_3512);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Module/pragma.scm 207 */
																			long BgL_idxz00_2429;

																			BgL_idxz00_2429 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2423);
																			BgL_test1953z00_3511 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2429 + 2L)) ==
																				BgL_classz00_2421);
																		}
																	else
																		{	/* Module/pragma.scm 207 */
																			bool_t BgL_res1783z00_2454;

																			{	/* Module/pragma.scm 207 */
																				obj_t BgL_oclassz00_2437;

																				{	/* Module/pragma.scm 207 */
																					obj_t BgL_arg1815z00_2445;
																					long BgL_arg1816z00_2446;

																					BgL_arg1815z00_2445 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Module/pragma.scm 207 */
																						long BgL_arg1817z00_2447;

																						BgL_arg1817z00_2447 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2423);
																						BgL_arg1816z00_2446 =
																							(BgL_arg1817z00_2447 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2437 =
																						VECTOR_REF(BgL_arg1815z00_2445,
																						BgL_arg1816z00_2446);
																				}
																				{	/* Module/pragma.scm 207 */
																					bool_t BgL__ortest_1115z00_2438;

																					BgL__ortest_1115z00_2438 =
																						(BgL_classz00_2421 ==
																						BgL_oclassz00_2437);
																					if (BgL__ortest_1115z00_2438)
																						{	/* Module/pragma.scm 207 */
																							BgL_res1783z00_2454 =
																								BgL__ortest_1115z00_2438;
																						}
																					else
																						{	/* Module/pragma.scm 207 */
																							long BgL_odepthz00_2439;

																							{	/* Module/pragma.scm 207 */
																								obj_t BgL_arg1804z00_2440;

																								BgL_arg1804z00_2440 =
																									(BgL_oclassz00_2437);
																								BgL_odepthz00_2439 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2440);
																							}
																							if ((2L < BgL_odepthz00_2439))
																								{	/* Module/pragma.scm 207 */
																									obj_t BgL_arg1802z00_2442;

																									{	/* Module/pragma.scm 207 */
																										obj_t BgL_arg1803z00_2443;

																										BgL_arg1803z00_2443 =
																											(BgL_oclassz00_2437);
																										BgL_arg1802z00_2442 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2443, 2L);
																									}
																									BgL_res1783z00_2454 =
																										(BgL_arg1802z00_2442 ==
																										BgL_classz00_2421);
																								}
																							else
																								{	/* Module/pragma.scm 207 */
																									BgL_res1783z00_2454 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1953z00_3511 =
																				BgL_res1783z00_2454;
																		}
																}
															}
															if (BgL_test1953z00_3511)
																{	/* Module/pragma.scm 207 */
																	((((BgL_funz00_bglt) COBJECT(
																					((BgL_funz00_bglt)
																						BgL_valuez00_1545)))->
																			BgL_predicatezd2ofzd2) =
																		((obj_t) ((obj_t) BgL_typez00_1544)),
																		BUNSPEC);
																	BGl_removezd2varzd2fromz12z12zzast_removez00
																		(CNST_TABLE_REF(16),
																		((BgL_variablez00_bglt) BgL_globalz00_12));
																	return ((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_valuez00_1545)))->
																			BgL_sidezd2effectzd2) =
																		((obj_t) BFALSE), BUNSPEC);
																}
															else
																{	/* Module/pragma.scm 207 */
																	return
																		BGl_sfunzd2errorzd2zzmodule_pragmaz00
																		(BGl_string1809z00zzmodule_pragmaz00,
																		((BgL_globalz00_bglt) BgL_globalz00_12));
																}
														}
													}
												else
													{	/* Module/pragma.scm 203 */
														return
															BGl_userzd2errorzd2zztools_errorz00
															(BGl_string1807z00zzmodule_pragmaz00,
															BGl_string1810z00zzmodule_pragmaz00,
															BgL_propz00_13, BNIL);
													}
											}
										else
											{	/* Module/pragma.scm 200 */
												if ((BgL_keyz00_1468 == CNST_TABLE_REF(17)))
													{	/* Module/pragma.scm 218 */
														BgL_valuez00_bglt BgL_valuez00_1553;

														BgL_valuez00_1553 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_globalz00_12))))->
															BgL_valuez00);
														{	/* Module/pragma.scm 219 */
															bool_t BgL_test1958z00_3552;

															{	/* Module/pragma.scm 219 */
																obj_t BgL_classz00_2459;

																BgL_classz00_2459 = BGl_funz00zzast_varz00;
																{	/* Module/pragma.scm 219 */
																	BgL_objectz00_bglt BgL_arg1807z00_2461;

																	{	/* Module/pragma.scm 219 */
																		obj_t BgL_tmpz00_3553;

																		BgL_tmpz00_3553 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_valuez00_1553));
																		BgL_arg1807z00_2461 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_3553);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Module/pragma.scm 219 */
																			long BgL_idxz00_2467;

																			BgL_idxz00_2467 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2461);
																			BgL_test1958z00_3552 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2467 + 2L)) ==
																				BgL_classz00_2459);
																		}
																	else
																		{	/* Module/pragma.scm 219 */
																			bool_t BgL_res1784z00_2492;

																			{	/* Module/pragma.scm 219 */
																				obj_t BgL_oclassz00_2475;

																				{	/* Module/pragma.scm 219 */
																					obj_t BgL_arg1815z00_2483;
																					long BgL_arg1816z00_2484;

																					BgL_arg1815z00_2483 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Module/pragma.scm 219 */
																						long BgL_arg1817z00_2485;

																						BgL_arg1817z00_2485 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2461);
																						BgL_arg1816z00_2484 =
																							(BgL_arg1817z00_2485 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2475 =
																						VECTOR_REF(BgL_arg1815z00_2483,
																						BgL_arg1816z00_2484);
																				}
																				{	/* Module/pragma.scm 219 */
																					bool_t BgL__ortest_1115z00_2476;

																					BgL__ortest_1115z00_2476 =
																						(BgL_classz00_2459 ==
																						BgL_oclassz00_2475);
																					if (BgL__ortest_1115z00_2476)
																						{	/* Module/pragma.scm 219 */
																							BgL_res1784z00_2492 =
																								BgL__ortest_1115z00_2476;
																						}
																					else
																						{	/* Module/pragma.scm 219 */
																							long BgL_odepthz00_2477;

																							{	/* Module/pragma.scm 219 */
																								obj_t BgL_arg1804z00_2478;

																								BgL_arg1804z00_2478 =
																									(BgL_oclassz00_2475);
																								BgL_odepthz00_2477 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2478);
																							}
																							if ((2L < BgL_odepthz00_2477))
																								{	/* Module/pragma.scm 219 */
																									obj_t BgL_arg1802z00_2480;

																									{	/* Module/pragma.scm 219 */
																										obj_t BgL_arg1803z00_2481;

																										BgL_arg1803z00_2481 =
																											(BgL_oclassz00_2475);
																										BgL_arg1802z00_2480 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2481, 2L);
																									}
																									BgL_res1784z00_2492 =
																										(BgL_arg1802z00_2480 ==
																										BgL_classz00_2459);
																								}
																							else
																								{	/* Module/pragma.scm 219 */
																									BgL_res1784z00_2492 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1958z00_3552 =
																				BgL_res1784z00_2492;
																		}
																}
															}
															if (BgL_test1958z00_3552)
																{	/* Module/pragma.scm 221 */
																	BgL_feffectz00_bglt BgL_arg1535z00_1555;

																	BgL_arg1535z00_1555 =
																		BGl_parsezd2effectzd2zzeffect_feffectz00
																		(BgL_propz00_13);
																	return ((((BgL_funz00_bglt)
																				COBJECT(((BgL_funz00_bglt)
																						BgL_valuez00_1553)))->
																			BgL_effectz00) =
																		((obj_t) ((obj_t) BgL_arg1535z00_1555)),
																		BUNSPEC);
																}
															else
																{	/* Module/pragma.scm 219 */
																	return
																		BGl_sfunzd2errorzd2zzmodule_pragmaz00
																		(BGl_string1811z00zzmodule_pragmaz00,
																		((BgL_globalz00_bglt) BgL_globalz00_12));
																}
														}
													}
												else
													{	/* Module/pragma.scm 200 */
														if ((BgL_keyz00_1468 == CNST_TABLE_REF(18)))
															{	/* Module/pragma.scm 223 */
																BgL_valuez00_bglt BgL_valuez00_1557;

																BgL_valuez00_1557 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_globalz00_12))))->BgL_valuez00);
																{	/* Module/pragma.scm 224 */
																	bool_t BgL_test1963z00_3588;

																	{	/* Module/pragma.scm 224 */
																		obj_t BgL_classz00_2496;

																		BgL_classz00_2496 = BGl_funz00zzast_varz00;
																		{	/* Module/pragma.scm 224 */
																			BgL_objectz00_bglt BgL_arg1807z00_2498;

																			{	/* Module/pragma.scm 224 */
																				obj_t BgL_tmpz00_3589;

																				BgL_tmpz00_3589 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_valuez00_1557));
																				BgL_arg1807z00_2498 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_3589);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Module/pragma.scm 224 */
																					long BgL_idxz00_2504;

																					BgL_idxz00_2504 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2498);
																					BgL_test1963z00_3588 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2504 + 2L)) ==
																						BgL_classz00_2496);
																				}
																			else
																				{	/* Module/pragma.scm 224 */
																					bool_t BgL_res1785z00_2529;

																					{	/* Module/pragma.scm 224 */
																						obj_t BgL_oclassz00_2512;

																						{	/* Module/pragma.scm 224 */
																							obj_t BgL_arg1815z00_2520;
																							long BgL_arg1816z00_2521;

																							BgL_arg1815z00_2520 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Module/pragma.scm 224 */
																								long BgL_arg1817z00_2522;

																								BgL_arg1817z00_2522 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2498);
																								BgL_arg1816z00_2521 =
																									(BgL_arg1817z00_2522 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2512 =
																								VECTOR_REF(BgL_arg1815z00_2520,
																								BgL_arg1816z00_2521);
																						}
																						{	/* Module/pragma.scm 224 */
																							bool_t BgL__ortest_1115z00_2513;

																							BgL__ortest_1115z00_2513 =
																								(BgL_classz00_2496 ==
																								BgL_oclassz00_2512);
																							if (BgL__ortest_1115z00_2513)
																								{	/* Module/pragma.scm 224 */
																									BgL_res1785z00_2529 =
																										BgL__ortest_1115z00_2513;
																								}
																							else
																								{	/* Module/pragma.scm 224 */
																									long BgL_odepthz00_2514;

																									{	/* Module/pragma.scm 224 */
																										obj_t BgL_arg1804z00_2515;

																										BgL_arg1804z00_2515 =
																											(BgL_oclassz00_2512);
																										BgL_odepthz00_2514 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2515);
																									}
																									if ((2L < BgL_odepthz00_2514))
																										{	/* Module/pragma.scm 224 */
																											obj_t BgL_arg1802z00_2517;

																											{	/* Module/pragma.scm 224 */
																												obj_t
																													BgL_arg1803z00_2518;
																												BgL_arg1803z00_2518 =
																													(BgL_oclassz00_2512);
																												BgL_arg1802z00_2517 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2518,
																													2L);
																											}
																											BgL_res1785z00_2529 =
																												(BgL_arg1802z00_2517 ==
																												BgL_classz00_2496);
																										}
																									else
																										{	/* Module/pragma.scm 224 */
																											BgL_res1785z00_2529 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1963z00_3588 =
																						BgL_res1785z00_2529;
																				}
																		}
																	}
																	if (BgL_test1963z00_3588)
																		{	/* Module/pragma.scm 224 */
																			return
																				((((BgL_funz00_bglt) COBJECT(
																							((BgL_funz00_bglt)
																								BgL_valuez00_1557)))->
																					BgL_stackzd2allocatorzd2) =
																				((obj_t) BgL_valz00_1469), BUNSPEC);
																		}
																	else
																		{	/* Module/pragma.scm 224 */
																			return
																				BGl_sfunzd2errorzd2zzmodule_pragmaz00
																				(BGl_string1812z00zzmodule_pragmaz00,
																				((BgL_globalz00_bglt)
																					BgL_globalz00_12));
																		}
																}
															}
														else
															{	/* Module/pragma.scm 200 */
																if ((BgL_keyz00_1468 == CNST_TABLE_REF(19)))
																	{	/* Module/pragma.scm 200 */
																		return
																			BGl_argszd2noescapezd2zzmodule_pragmaz00
																			(BgL_globalz00_12, BgL_valz00_1469,
																			BGl_funzd2argszd2noescapezd2envzd2zzast_varz00,
																			BGl_funzd2argszd2noescapezd2setz12zd2envz12zzast_varz00);
																	}
																else
																	{	/* Module/pragma.scm 200 */
																		if ((BgL_keyz00_1468 == CNST_TABLE_REF(20)))
																			{	/* Module/pragma.scm 200 */
																				return
																					BGl_argszd2noescapezd2zzmodule_pragmaz00
																					(BgL_globalz00_12, BgL_valz00_1469,
																					BGl_funzd2argszd2retescapezd2envzd2zzast_varz00,
																					BGl_funzd2argszd2retescapezd2setz12zd2envz12zzast_varz00);
																			}
																		else
																			{	/* Module/pragma.scm 232 */
																				obj_t BgL_list1540z00_1561;

																				BgL_list1540z00_1561 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				return
																					BGl_userzd2errorzd2zztools_errorz00
																					(BGl_string1807z00zzmodule_pragmaz00,
																					BGl_string1808z00zzmodule_pragmaz00,
																					BgL_propz00_13, BgL_list1540z00_1561);
																			}
																	}
															}
													}
											}
									}
								else
									{	/* Module/pragma.scm 124 */
									BgL_tagzd2152zd2_1471:
										{	/* Module/pragma.scm 235 */
											obj_t BgL_arg1544z00_1562;

											{	/* Module/pragma.scm 235 */
												obj_t BgL_list1546z00_1564;

												BgL_list1546z00_1564 =
													MAKE_YOUNG_PAIR(BgL_propz00_13, BNIL);
												BgL_arg1544z00_1562 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string1813z00zzmodule_pragmaz00,
													BgL_list1546z00_1564);
											}
											{	/* Module/pragma.scm 234 */
												obj_t BgL_list1545z00_1563;

												BgL_list1545z00_1563 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												return
													BGl_userzd2errorzd2zztools_errorz00
													(BGl_string1807z00zzmodule_pragmaz00,
													BgL_arg1544z00_1562, BgL_clausez00_14,
													BgL_list1545z00_1563);
											}
										}
									}
							}
						else
							{	/* Module/pragma.scm 124 */
								goto BgL_tagzd2152zd2_1471;
							}
					}
			}
		}

	}



/* args-noescape */
	obj_t BGl_argszd2noescapezd2zzmodule_pragmaz00(obj_t BgL_globalz00_15,
		obj_t BgL_valz00_16, obj_t BgL_pgetz00_17, obj_t BgL_psetz00_18)
	{
		{	/* Module/pragma.scm 242 */
			{	/* Module/pragma.scm 244 */
				BgL_valuez00_bglt BgL_valuez00_1565;

				BgL_valuez00_1565 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_15))))->BgL_valuez00);
				{	/* Module/pragma.scm 245 */
					bool_t BgL_test1969z00_3633;

					{	/* Module/pragma.scm 245 */
						obj_t BgL_classz00_2536;

						BgL_classz00_2536 = BGl_funz00zzast_varz00;
						{	/* Module/pragma.scm 245 */
							BgL_objectz00_bglt BgL_arg1807z00_2538;

							{	/* Module/pragma.scm 245 */
								obj_t BgL_tmpz00_3634;

								BgL_tmpz00_3634 =
									((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_1565));
								BgL_arg1807z00_2538 = (BgL_objectz00_bglt) (BgL_tmpz00_3634);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Module/pragma.scm 245 */
									long BgL_idxz00_2544;

									BgL_idxz00_2544 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2538);
									BgL_test1969z00_3633 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2544 + 2L)) == BgL_classz00_2536);
								}
							else
								{	/* Module/pragma.scm 245 */
									bool_t BgL_res1786z00_2569;

									{	/* Module/pragma.scm 245 */
										obj_t BgL_oclassz00_2552;

										{	/* Module/pragma.scm 245 */
											obj_t BgL_arg1815z00_2560;
											long BgL_arg1816z00_2561;

											BgL_arg1815z00_2560 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Module/pragma.scm 245 */
												long BgL_arg1817z00_2562;

												BgL_arg1817z00_2562 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2538);
												BgL_arg1816z00_2561 =
													(BgL_arg1817z00_2562 - OBJECT_TYPE);
											}
											BgL_oclassz00_2552 =
												VECTOR_REF(BgL_arg1815z00_2560, BgL_arg1816z00_2561);
										}
										{	/* Module/pragma.scm 245 */
											bool_t BgL__ortest_1115z00_2553;

											BgL__ortest_1115z00_2553 =
												(BgL_classz00_2536 == BgL_oclassz00_2552);
											if (BgL__ortest_1115z00_2553)
												{	/* Module/pragma.scm 245 */
													BgL_res1786z00_2569 = BgL__ortest_1115z00_2553;
												}
											else
												{	/* Module/pragma.scm 245 */
													long BgL_odepthz00_2554;

													{	/* Module/pragma.scm 245 */
														obj_t BgL_arg1804z00_2555;

														BgL_arg1804z00_2555 = (BgL_oclassz00_2552);
														BgL_odepthz00_2554 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2555);
													}
													if ((2L < BgL_odepthz00_2554))
														{	/* Module/pragma.scm 245 */
															obj_t BgL_arg1802z00_2557;

															{	/* Module/pragma.scm 245 */
																obj_t BgL_arg1803z00_2558;

																BgL_arg1803z00_2558 = (BgL_oclassz00_2552);
																BgL_arg1802z00_2557 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2558,
																	2L);
															}
															BgL_res1786z00_2569 =
																(BgL_arg1802z00_2557 == BgL_classz00_2536);
														}
													else
														{	/* Module/pragma.scm 245 */
															BgL_res1786z00_2569 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1969z00_3633 = BgL_res1786z00_2569;
								}
						}
					}
					if (BgL_test1969z00_3633)
						{	/* Module/pragma.scm 245 */
							if (NULLP(BgL_valz00_16))
								{	/* Module/pragma.scm 248 */
									return
										BGL_PROCEDURE_CALL2(BgL_psetz00_18,
										((obj_t) BgL_valuez00_1565), CNST_TABLE_REF(21));
								}
							else
								{	/* Module/pragma.scm 248 */
									if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(CAR(
												((obj_t) BgL_valz00_16))))
										{	/* Module/pragma.scm 251 */
											obj_t BgL_arg1559z00_1570;

											{	/* Module/pragma.scm 251 */
												obj_t BgL_arg1561z00_1571;

												BgL_arg1561z00_1571 =
													BGL_PROCEDURE_CALL1(BgL_pgetz00_17,
													((obj_t) BgL_valuez00_1565));
												BgL_arg1559z00_1570 =
													BGl_appendzd221011zd2zzmodule_pragmaz00(BgL_valz00_16,
													BgL_arg1561z00_1571);
											}
											return
												BGL_PROCEDURE_CALL2(BgL_psetz00_18,
												((obj_t) BgL_valuez00_1565), BgL_arg1559z00_1570);
										}
									else
										{	/* Module/pragma.scm 252 */
											bool_t BgL_test1975z00_3682;

											{	/* Module/pragma.scm 252 */
												obj_t BgL_tmpz00_3683;

												BgL_tmpz00_3683 = CAR(((obj_t) BgL_valz00_16));
												BgL_test1975z00_3682 = SYMBOLP(BgL_tmpz00_3683);
											}
											if (BgL_test1975z00_3682)
												{	/* Module/pragma.scm 253 */
													bool_t BgL_test1976z00_3687;

													{	/* Module/pragma.scm 253 */
														obj_t BgL_classz00_2572;

														BgL_classz00_2572 = BGl_sfunz00zzast_varz00;
														{	/* Module/pragma.scm 253 */
															BgL_objectz00_bglt BgL_arg1807z00_2574;

															{	/* Module/pragma.scm 253 */
																obj_t BgL_tmpz00_3688;

																BgL_tmpz00_3688 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_valuez00_1565));
																BgL_arg1807z00_2574 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_3688);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Module/pragma.scm 253 */
																	long BgL_idxz00_2580;

																	BgL_idxz00_2580 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2574);
																	BgL_test1976z00_3687 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2580 + 3L)) ==
																		BgL_classz00_2572);
																}
															else
																{	/* Module/pragma.scm 253 */
																	bool_t BgL_res1787z00_2605;

																	{	/* Module/pragma.scm 253 */
																		obj_t BgL_oclassz00_2588;

																		{	/* Module/pragma.scm 253 */
																			obj_t BgL_arg1815z00_2596;
																			long BgL_arg1816z00_2597;

																			BgL_arg1815z00_2596 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Module/pragma.scm 253 */
																				long BgL_arg1817z00_2598;

																				BgL_arg1817z00_2598 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2574);
																				BgL_arg1816z00_2597 =
																					(BgL_arg1817z00_2598 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2588 =
																				VECTOR_REF(BgL_arg1815z00_2596,
																				BgL_arg1816z00_2597);
																		}
																		{	/* Module/pragma.scm 253 */
																			bool_t BgL__ortest_1115z00_2589;

																			BgL__ortest_1115z00_2589 =
																				(BgL_classz00_2572 ==
																				BgL_oclassz00_2588);
																			if (BgL__ortest_1115z00_2589)
																				{	/* Module/pragma.scm 253 */
																					BgL_res1787z00_2605 =
																						BgL__ortest_1115z00_2589;
																				}
																			else
																				{	/* Module/pragma.scm 253 */
																					long BgL_odepthz00_2590;

																					{	/* Module/pragma.scm 253 */
																						obj_t BgL_arg1804z00_2591;

																						BgL_arg1804z00_2591 =
																							(BgL_oclassz00_2588);
																						BgL_odepthz00_2590 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2591);
																					}
																					if ((3L < BgL_odepthz00_2590))
																						{	/* Module/pragma.scm 253 */
																							obj_t BgL_arg1802z00_2593;

																							{	/* Module/pragma.scm 253 */
																								obj_t BgL_arg1803z00_2594;

																								BgL_arg1803z00_2594 =
																									(BgL_oclassz00_2588);
																								BgL_arg1802z00_2593 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2594, 3L);
																							}
																							BgL_res1787z00_2605 =
																								(BgL_arg1802z00_2593 ==
																								BgL_classz00_2572);
																						}
																					else
																						{	/* Module/pragma.scm 253 */
																							BgL_res1787z00_2605 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1976z00_3687 = BgL_res1787z00_2605;
																}
														}
													}
													if (BgL_test1976z00_3687)
														{	/* Module/pragma.scm 253 */
															{	/* Module/pragma.scm 257 */
																bool_t BgL_test1980z00_3711;

																{	/* Module/pragma.scm 257 */
																	obj_t BgL_arg1575z00_1577;

																	BgL_arg1575z00_1577 =
																		BGL_PROCEDURE_CALL1(BgL_pgetz00_17,
																		((obj_t) BgL_valuez00_1565));
																	BgL_test1980z00_3711 =
																		(BgL_arg1575z00_1577 == BUNSPEC);
																}
																if (BgL_test1980z00_3711)
																	{	/* Module/pragma.scm 257 */
																		BGL_PROCEDURE_CALL2(BgL_psetz00_18,
																			((obj_t) BgL_valuez00_1565), BNIL);
																	}
																else
																	{	/* Module/pragma.scm 257 */
																		BFALSE;
																	}
															}
															{	/* Module/pragma.scm 259 */
																obj_t BgL_g1105z00_1578;

																BgL_g1105z00_1578 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt) BgL_valuez00_1565)))->
																	BgL_argszd2namezd2);
																{
																	long BgL_iz00_1580;
																	obj_t BgL_argsz00_1581;

																	BgL_iz00_1580 = 0L;
																	BgL_argsz00_1581 = BgL_g1105z00_1578;
																BgL_zc3z04anonymousza31576ze3z87_1582:
																	if (NULLP(BgL_argsz00_1581))
																		{	/* Module/pragma.scm 262 */
																			return
																				BGl_userzd2errorzd2zztools_errorz00
																				(BGl_string1807z00zzmodule_pragmaz00,
																				BGl_string1814z00zzmodule_pragmaz00,
																				BgL_valz00_16, BNIL);
																		}
																	else
																		{	/* Module/pragma.scm 262 */
																			if (
																				(CAR(
																						((obj_t) BgL_argsz00_1581)) ==
																					CAR(((obj_t) BgL_valz00_16))))
																				{	/* Module/pragma.scm 267 */
																					obj_t BgL_arg1589z00_1588;

																					{	/* Module/pragma.scm 267 */
																						obj_t BgL_arg1591z00_1589;

																						BgL_arg1591z00_1589 =
																							BGL_PROCEDURE_CALL1
																							(BgL_pgetz00_17,
																							((obj_t) BgL_valuez00_1565));
																						BgL_arg1589z00_1588 =
																							MAKE_YOUNG_PAIR(BINT
																							(BgL_iz00_1580),
																							BgL_arg1591z00_1589);
																					}
																					return
																						BGL_PROCEDURE_CALL2(BgL_psetz00_18,
																						((obj_t) BgL_valuez00_1565),
																						BgL_arg1589z00_1588);
																				}
																			else
																				{	/* Module/pragma.scm 269 */
																					long BgL_arg1593z00_1590;
																					obj_t BgL_arg1594z00_1591;

																					BgL_arg1593z00_1590 =
																						(BgL_iz00_1580 + 1L);
																					BgL_arg1594z00_1591 =
																						CDR(((obj_t) BgL_argsz00_1581));
																					{
																						obj_t BgL_argsz00_3752;
																						long BgL_iz00_3751;

																						BgL_iz00_3751 = BgL_arg1593z00_1590;
																						BgL_argsz00_3752 =
																							BgL_arg1594z00_1591;
																						BgL_argsz00_1581 = BgL_argsz00_3752;
																						BgL_iz00_1580 = BgL_iz00_3751;
																						goto
																							BgL_zc3z04anonymousza31576ze3z87_1582;
																					}
																				}
																		}
																}
															}
														}
													else
														{	/* Module/pragma.scm 253 */
															return
																BGl_userzd2errorzd2zztools_errorz00
																(BGl_string1807z00zzmodule_pragmaz00,
																BGl_string1815z00zzmodule_pragmaz00,
																BgL_valz00_16, BNIL);
														}
												}
											else
												{	/* Module/pragma.scm 270 */
													bool_t BgL_test1983z00_3754;

													{	/* Module/pragma.scm 270 */
														obj_t BgL_arg1615z00_1602;

														BgL_arg1615z00_1602 =
															BGL_PROCEDURE_CALL1(BgL_pgetz00_17,
															((obj_t) BgL_valuez00_1565));
														BgL_test1983z00_3754 =
															(BgL_arg1615z00_1602 == BUNSPEC);
													}
													if (BgL_test1983z00_3754)
														{	/* Module/pragma.scm 271 */
															obj_t BgL_arg1609z00_1598;

															{	/* Module/pragma.scm 271 */
																obj_t BgL_list1610z00_1599;

																BgL_list1610z00_1599 =
																	MAKE_YOUNG_PAIR(BgL_valz00_16, BNIL);
																BgL_arg1609z00_1598 = BgL_list1610z00_1599;
															}
															return
																BGL_PROCEDURE_CALL2(BgL_psetz00_18,
																((obj_t) BgL_valuez00_1565),
																BgL_arg1609z00_1598);
														}
													else
														{	/* Module/pragma.scm 273 */
															obj_t BgL_arg1611z00_1600;

															{	/* Module/pragma.scm 273 */
																obj_t BgL_arg1613z00_1601;

																BgL_arg1613z00_1601 =
																	BGL_PROCEDURE_CALL1(BgL_pgetz00_17,
																	((obj_t) BgL_valuez00_1565));
																BgL_arg1611z00_1600 =
																	MAKE_YOUNG_PAIR(BgL_valz00_16,
																	BgL_arg1613z00_1601);
															}
															return
																BGL_PROCEDURE_CALL2(BgL_psetz00_18,
																((obj_t) BgL_valuez00_1565),
																BgL_arg1611z00_1600);
														}
												}
										}
								}
						}
					else
						{	/* Module/pragma.scm 245 */
							return
								BGl_sfunzd2errorzd2zzmodule_pragmaz00
								(BGl_string1816z00zzmodule_pragmaz00,
								((BgL_globalz00_bglt) BgL_globalz00_15));
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_pragmaz00(void)
	{
		{	/* Module/pragma.scm 15 */
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zzmodule_evalz00(428236825L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			BGl_modulezd2initializa7ationz75zzeffect_feffectz00(516374368L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1817z00zzmodule_pragmaz00));
		}

	}

#ifdef __cplusplus
}
#endif
