/*===========================================================================*/
/*   (Module/statexp.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/statexp.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_STATEXP_TYPE_DEFINITIONS
#define BGL_MODULE_STATEXP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_MODULE_STATEXP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzmodule_statexpz00 = BUNSPEC;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31220ze3ze5zzmodule_statexpz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzmodule_statexpz00(void);
	static obj_t BGl_z62statexpzd2producerzb0zzmodule_statexpz00(obj_t, obj_t);
	extern obj_t BGl_classzd2finaliza7erz75zzmodule_classz00(void);
	BGL_IMPORT obj_t BGl_evalz00zz__evalz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31213ze3ze5zzmodule_statexpz00(obj_t);
	extern obj_t BGl_declarezd2classz12zc0zzmodule_classz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_statexpz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_declarezd2widezd2classz12z12zzmodule_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_statexpz00(void);
	static obj_t BGl_z62zc3z04anonymousza31207ze3ze5zzmodule_statexpz00(obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31153ze3ze5zzmodule_statexpz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31226ze3ze5zzmodule_statexpz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_statexpz00(void);
	BGL_IMPORT obj_t BGl_makezd2promisezd2zz__r4_control_features_6_9z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31154ze3ze5zzmodule_statexpz00(obj_t);
	extern obj_t BGl_tozd2bezd2macroz12z12zzexpand_expanderz00(obj_t, obj_t);
	extern obj_t BGl_za2modulezd2clauseza2zd2zzmodule_modulez00;
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_statexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_expanderz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62statexpzd2finaliza7erz17zzmodule_statexpz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzmodule_statexpz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_statexpz00(void);
	static obj_t BGl_statexpzd2parserzd2zzmodule_statexpz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_statexpz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_statexpz00(void);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62exportzd2consumerzb0zzmodule_statexpz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_parsezd2prototypezd2zzmodule_prototypez00(obj_t);
	static obj_t BGl_z62makezd2staticzd2compilerz62zzmodule_statexpz00(obj_t);
	static obj_t BGl_z62makezd2exportzd2compilerz62zzmodule_statexpz00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2staticzd2compilerz00zzmodule_statexpz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2exportzd2compilerz00zzmodule_statexpz00(void);
	static obj_t BGl_za2localzd2classesza2zd2zzmodule_statexpz00 = BUNSPEC;
	extern obj_t
		BGl_tozd2bezd2definez12z12zzast_findzd2gdefszd2(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_stringzd2downcasezd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t __cnst[15];


	   
		 
		DEFINE_STRING(BGl_string1323z00zzmodule_statexpz00,
		BgL_bgl_string1323za700za7za7m1330za7, "Parse error \"~a\" clause", 23);
	      DEFINE_STRING(BGl_string1324z00zzmodule_statexpz00,
		BgL_bgl_string1324za700za7za7m1331za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1325z00zzmodule_statexpz00,
		BgL_bgl_string1325za700za7za7m1332za7, "Illegal \"export\" clause", 23);
	      DEFINE_STRING(BGl_string1326z00zzmodule_statexpz00,
		BgL_bgl_string1326za700za7za7m1333za7, "Illegal prototype", 17);
	      DEFINE_STRING(BGl_string1327z00zzmodule_statexpz00,
		BgL_bgl_string1327za700za7za7m1334za7, "module_statexp", 14);
	      DEFINE_STRING(BGl_string1328z00zzmodule_statexpz00,
		BgL_bgl_string1328za700za7za7m1335za7,
		"expander syntax macro define-macro wide-class final-class abstract-class class svar sgfun sifun sfun export void static ",
		120);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_exportzd2consumerzd2envz00zzmodule_statexpz00,
		BgL_bgl_za762exportza7d2cons1336z00,
		BGl_z62exportzd2consumerzb0zzmodule_statexpz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1321z00zzmodule_statexpz00,
		BgL_bgl_za762za7c3za704anonymo1337za7,
		BGl_z62zc3z04anonymousza31153ze3ze5zzmodule_statexpz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1322z00zzmodule_statexpz00,
		BgL_bgl_za762za7c3za704anonymo1338za7,
		BGl_z62zc3z04anonymousza31154ze3ze5zzmodule_statexpz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_statexpzd2producerzd2envz00zzmodule_statexpz00,
		BgL_bgl_za762statexpza7d2pro1339z00,
		BGl_z62statexpzd2producerzb0zzmodule_statexpz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2staticzd2compilerzd2envzd2zzmodule_statexpz00,
		BgL_bgl_za762makeza7d2static1340z00,
		BGl_z62makezd2staticzd2compilerz62zzmodule_statexpz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_statexpzd2finaliza7erzd2envza7zzmodule_statexpz00,
		BgL_bgl_za762statexpza7d2fin1341z00,
		BGl_z62statexpzd2finaliza7erz17zzmodule_statexpz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2exportzd2compilerzd2envzd2zzmodule_statexpz00,
		BgL_bgl_za762makeza7d2export1342z00,
		BGl_z62makezd2exportzd2compilerz62zzmodule_statexpz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_statexpz00));
		     ADD_ROOT((void *) (&BGl_za2localzd2classesza2zd2zzmodule_statexpz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzmodule_statexpz00(long
		BgL_checksumz00_1005, char *BgL_fromz00_1006)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_statexpz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_statexpz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_statexpz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_statexpz00();
					BGl_cnstzd2initzd2zzmodule_statexpz00();
					BGl_importedzd2moduleszd2initz00zzmodule_statexpz00();
					return BGl_toplevelzd2initzd2zzmodule_statexpz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 15 */
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "module_statexp");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"module_statexp");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_statexp");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_statexp");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_statexp");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_statexp");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"module_statexp");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_statexp");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"module_statexp");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_statexp");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_statexp");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 15 */
			{	/* Module/statexp.scm 15 */
				obj_t BgL_cportz00_969;

				{	/* Module/statexp.scm 15 */
					obj_t BgL_stringz00_976;

					BgL_stringz00_976 = BGl_string1328z00zzmodule_statexpz00;
					{	/* Module/statexp.scm 15 */
						obj_t BgL_startz00_977;

						BgL_startz00_977 = BINT(0L);
						{	/* Module/statexp.scm 15 */
							obj_t BgL_endz00_978;

							BgL_endz00_978 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_976)));
							{	/* Module/statexp.scm 15 */

								BgL_cportz00_969 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_976, BgL_startz00_977, BgL_endz00_978);
				}}}}
				{
					long BgL_iz00_970;

					BgL_iz00_970 = 14L;
				BgL_loopz00_971:
					if ((BgL_iz00_970 == -1L))
						{	/* Module/statexp.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/statexp.scm 15 */
							{	/* Module/statexp.scm 15 */
								obj_t BgL_arg1329z00_972;

								{	/* Module/statexp.scm 15 */

									{	/* Module/statexp.scm 15 */
										obj_t BgL_locationz00_974;

										BgL_locationz00_974 = BBOOL(((bool_t) 0));
										{	/* Module/statexp.scm 15 */

											BgL_arg1329z00_972 =
												BGl_readz00zz__readerz00(BgL_cportz00_969,
												BgL_locationz00_974);
										}
									}
								}
								{	/* Module/statexp.scm 15 */
									int BgL_tmpz00_1035;

									BgL_tmpz00_1035 = (int) (BgL_iz00_970);
									CNST_TABLE_SET(BgL_tmpz00_1035, BgL_arg1329z00_972);
							}}
							{	/* Module/statexp.scm 15 */
								int BgL_auxz00_975;

								BgL_auxz00_975 = (int) ((BgL_iz00_970 - 1L));
								{
									long BgL_iz00_1040;

									BgL_iz00_1040 = (long) (BgL_auxz00_975);
									BgL_iz00_970 = BgL_iz00_1040;
									goto BgL_loopz00_971;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 15 */
			return (BGl_za2localzd2classesza2zd2zzmodule_statexpz00 = BNIL, BUNSPEC);
		}

	}



/* make-static-compiler */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2staticzd2compilerz00zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 36 */
			{	/* Module/statexp.scm 37 */
				BgL_ccompz00_bglt BgL_new1065z00_718;

				{	/* Module/statexp.scm 38 */
					BgL_ccompz00_bglt BgL_new1064z00_723;

					BgL_new1064z00_723 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/statexp.scm 38 */
						long BgL_arg1157z00_724;

						BgL_arg1157z00_724 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1064z00_723), BgL_arg1157z00_724);
					}
					BgL_new1065z00_718 = BgL_new1064z00_723;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1065z00_718))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1065z00_718))->BgL_producerz00) =
					((obj_t) BGl_statexpzd2producerzd2envz00zzmodule_statexpz00),
					BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1065z00_718))->BgL_consumerz00) =
					((obj_t) BGl_proc1321z00zzmodule_statexpz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1065z00_718))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_proc1322z00zzmodule_statexpz00), BUNSPEC);
				return ((obj_t) BgL_new1065z00_718);
			}
		}

	}



/* &make-static-compiler */
	obj_t BGl_z62makezd2staticzd2compilerz62zzmodule_statexpz00(obj_t
		BgL_envz00_936)
	{
		{	/* Module/statexp.scm 36 */
			return BGl_makezd2staticzd2compilerz00zzmodule_statexpz00();
		}

	}



/* &<@anonymous:1154> */
	obj_t BGl_z62zc3z04anonymousza31154ze3ze5zzmodule_statexpz00(obj_t
		BgL_envz00_937)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(1);
		}

	}



/* &<@anonymous:1153> */
	obj_t BGl_z62zc3z04anonymousza31153ze3ze5zzmodule_statexpz00(obj_t
		BgL_envz00_938, obj_t BgL_mz00_939, obj_t BgL_cz00_940)
	{
		{	/* Module/module.scm 54 */
			return BNIL;
		}

	}



/* make-export-compiler */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2exportzd2compilerz00zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 44 */
			{	/* Module/statexp.scm 45 */
				BgL_ccompz00_bglt BgL_new1067z00_725;

				{	/* Module/statexp.scm 46 */
					BgL_ccompz00_bglt BgL_new1066z00_726;

					BgL_new1066z00_726 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/statexp.scm 46 */
						long BgL_arg1158z00_727;

						BgL_arg1158z00_727 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1066z00_726), BgL_arg1158z00_727);
					}
					BgL_new1067z00_725 = BgL_new1066z00_726;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1067z00_725))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1067z00_725))->BgL_producerz00) =
					((obj_t) BGl_statexpzd2producerzd2envz00zzmodule_statexpz00),
					BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1067z00_725))->BgL_consumerz00) =
					((obj_t) BGl_exportzd2consumerzd2envz00zzmodule_statexpz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1067z00_725))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_statexpzd2finaliza7erzd2envza7zzmodule_statexpz00),
					BUNSPEC);
				return ((obj_t) BgL_new1067z00_725);
			}
		}

	}



/* &make-export-compiler */
	obj_t BGl_z62makezd2exportzd2compilerz62zzmodule_statexpz00(obj_t
		BgL_envz00_943)
	{
		{	/* Module/statexp.scm 44 */
			return BGl_makezd2exportzd2compilerz00zzmodule_statexpz00();
		}

	}



/* &statexp-producer */
	obj_t BGl_z62statexpzd2producerzb0zzmodule_statexpz00(obj_t BgL_envz00_941,
		obj_t BgL_clausez00_942)
	{
		{	/* Module/statexp.scm 54 */
			{	/* Module/statexp.scm 55 */
				obj_t BgL_modez00_980;

				BgL_modez00_980 = CAR(((obj_t) BgL_clausez00_942));
				{
					obj_t BgL_protosz00_983;

					if (PAIRP(BgL_clausez00_942))
						{	/* Module/statexp.scm 56 */
							obj_t BgL_arg1162z00_992;

							BgL_arg1162z00_992 = CDR(((obj_t) BgL_clausez00_942));
							BgL_protosz00_983 = BgL_arg1162z00_992;
							{
								obj_t BgL_l1124z00_985;

								BgL_l1124z00_985 = BgL_protosz00_983;
							BgL_zc3z04anonymousza31163ze3z87_984:
								if (PAIRP(BgL_l1124z00_985))
									{	/* Module/statexp.scm 58 */
										BGl_statexpzd2parserzd2zzmodule_statexpz00(CAR
											(BgL_l1124z00_985), BgL_modez00_980);
										{
											obj_t BgL_l1124z00_1076;

											BgL_l1124z00_1076 = CDR(BgL_l1124z00_985);
											BgL_l1124z00_985 = BgL_l1124z00_1076;
											goto BgL_zc3z04anonymousza31163ze3z87_984;
										}
									}
								else
									{	/* Module/statexp.scm 58 */
										((bool_t) 1);
									}
							}
							return BNIL;
						}
					else
						{	/* Module/statexp.scm 56 */
							{	/* Module/statexp.scm 61 */
								obj_t BgL_arg1171z00_986;
								obj_t BgL_arg1172z00_987;

								BgL_arg1171z00_986 =
									BGl_findzd2locationzd2zztools_locationz00
									(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00);
								{	/* Module/statexp.scm 63 */
									obj_t BgL_arg1182z00_988;

									{	/* Module/statexp.scm 63 */
										obj_t BgL_arg1187z00_989;

										{	/* Module/statexp.scm 63 */
											obj_t BgL_arg1455z00_990;

											BgL_arg1455z00_990 =
												SYMBOL_TO_STRING(((obj_t) BgL_modez00_980));
											BgL_arg1187z00_989 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_990);
										}
										BgL_arg1182z00_988 =
											BGl_stringzd2downcasezd2zz__r4_strings_6_7z00
											(BgL_arg1187z00_989);
									}
									{	/* Module/statexp.scm 62 */
										obj_t BgL_list1183z00_991;

										BgL_list1183z00_991 =
											MAKE_YOUNG_PAIR(BgL_arg1182z00_988, BNIL);
										BgL_arg1172z00_987 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string1323z00zzmodule_statexpz00,
											BgL_list1183z00_991);
									}
								}
								return
									BGl_userzd2errorzf2locationz20zztools_errorz00
									(BgL_arg1171z00_986, BgL_arg1172z00_987, BgL_clausez00_942,
									BNIL, BNIL);
							}
						}
				}
			}
		}

	}



/* &export-consumer */
	obj_t BGl_z62exportzd2consumerzb0zzmodule_statexpz00(obj_t BgL_envz00_944,
		obj_t BgL_modulez00_945, obj_t BgL_clausez00_946)
	{
		{	/* Module/statexp.scm 69 */
			{

				if (PAIRP(BgL_clausez00_946))
					{	/* Module/statexp.scm 70 */
						return CDR(((obj_t) BgL_clausez00_946));
					}
				else
					{	/* Module/statexp.scm 70 */
						{	/* Module/statexp.scm 74 */
							obj_t BgL_arg1190z00_994;

							BgL_arg1190z00_994 =
								BGl_findzd2locationzd2zztools_locationz00
								(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00);
							{	/* Module/statexp.scm 74 */
								obj_t BgL_list1191z00_995;

								BgL_list1191z00_995 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								return
									BGl_userzd2errorzf2locationz20zztools_errorz00
									(BgL_arg1190z00_994, BGl_string1324z00zzmodule_statexpz00,
									BGl_string1325z00zzmodule_statexpz00, BgL_clausez00_946,
									BgL_list1191z00_995);
							}
						}
					}
			}
		}

	}



/* statexp-parser */
	obj_t BGl_statexpzd2parserzd2zzmodule_statexpz00(obj_t BgL_prototypez00_28,
		obj_t BgL_importz00_29)
	{
		{	/* Module/statexp.scm 80 */
			{	/* Module/statexp.scm 81 */
				obj_t BgL_protoz00_756;

				BgL_protoz00_756 =
					BGl_parsezd2prototypezd2zzmodule_prototypez00(BgL_prototypez00_28);
				if (PAIRP(BgL_protoz00_756))
					{	/* Module/statexp.scm 85 */
						obj_t BgL_casezd2valuezd2_758;

						BgL_casezd2valuezd2_758 = CAR(BgL_protoz00_756);
						{	/* Module/statexp.scm 85 */
							bool_t BgL_test1355z00_1097;

							{	/* Module/statexp.scm 85 */
								bool_t BgL__ortest_1079z00_803;

								BgL__ortest_1079z00_803 =
									(BgL_casezd2valuezd2_758 == CNST_TABLE_REF(3));
								if (BgL__ortest_1079z00_803)
									{	/* Module/statexp.scm 85 */
										BgL_test1355z00_1097 = BgL__ortest_1079z00_803;
									}
								else
									{	/* Module/statexp.scm 85 */
										bool_t BgL__ortest_1080z00_804;

										BgL__ortest_1080z00_804 =
											(BgL_casezd2valuezd2_758 == CNST_TABLE_REF(4));
										if (BgL__ortest_1080z00_804)
											{	/* Module/statexp.scm 85 */
												BgL_test1355z00_1097 = BgL__ortest_1080z00_804;
											}
										else
											{	/* Module/statexp.scm 85 */
												BgL_test1355z00_1097 =
													(BgL_casezd2valuezd2_758 == CNST_TABLE_REF(5));
											}
									}
							}
							if (BgL_test1355z00_1097)
								{	/* Module/statexp.scm 88 */
									BgL_globalz00_bglt BgL_arg1194z00_762;

									{	/* Module/statexp.scm 88 */
										obj_t BgL_arg1196z00_763;
										obj_t BgL_arg1197z00_764;

										BgL_arg1196z00_763 = CAR(CDR(BgL_protoz00_756));
										BgL_arg1197z00_764 = CAR(CDR(CDR(BgL_protoz00_756)));
										BgL_arg1194z00_762 =
											BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2
											(BgL_arg1196z00_763, BFALSE, BgL_arg1197z00_764,
											BGl_za2moduleza2z00zzmodule_modulez00, BgL_importz00_29,
											BgL_casezd2valuezd2_758, BgL_prototypez00_28, BFALSE);
									}
									return
										BGl_tozd2bezd2definez12z12zzast_findzd2gdefszd2
										(BgL_arg1194z00_762);
								}
							else
								{	/* Module/statexp.scm 85 */
									if ((BgL_casezd2valuezd2_758 == CNST_TABLE_REF(6)))
										{	/* Module/statexp.scm 92 */
											BgL_globalz00_bglt BgL_arg1200z00_767;

											{	/* Module/statexp.scm 92 */
												obj_t BgL_arg1201z00_768;

												BgL_arg1201z00_768 = CAR(CDR(BgL_protoz00_756));
												BgL_arg1200z00_767 =
													BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2
													(BgL_arg1201z00_768, BFALSE,
													BGl_za2moduleza2z00zzmodule_modulez00,
													BgL_importz00_29, BgL_prototypez00_28, BFALSE);
											}
											return
												BGl_tozd2bezd2definez12z12zzast_findzd2gdefszd2
												(BgL_arg1200z00_767);
										}
									else
										{	/* Module/statexp.scm 85 */
											if ((BgL_casezd2valuezd2_758 == CNST_TABLE_REF(7)))
												{	/* Module/statexp.scm 97 */
													obj_t BgL_arg1203z00_770;

													{	/* Module/statexp.scm 97 */
														obj_t BgL_zc3z04anonymousza31207ze3z87_948;

														BgL_zc3z04anonymousza31207ze3z87_948 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza31207ze3ze5zzmodule_statexpz00,
															(int) (0L), (int) (3L));
														PROCEDURE_SET(BgL_zc3z04anonymousza31207ze3z87_948,
															(int) (0L), BgL_protoz00_756);
														PROCEDURE_SET(BgL_zc3z04anonymousza31207ze3z87_948,
															(int) (1L), BgL_importz00_29);
														PROCEDURE_SET(BgL_zc3z04anonymousza31207ze3z87_948,
															(int) (2L), BgL_prototypez00_28);
														BgL_arg1203z00_770 =
															BGl_makezd2promisezd2zz__r4_control_features_6_9z00
															(BgL_zc3z04anonymousza31207ze3z87_948);
													}
													return
														(BGl_za2localzd2classesza2zd2zzmodule_statexpz00 =
														MAKE_YOUNG_PAIR(BgL_arg1203z00_770,
															BGl_za2localzd2classesza2zd2zzmodule_statexpz00),
														BUNSPEC);
												}
											else
												{	/* Module/statexp.scm 85 */
													if ((BgL_casezd2valuezd2_758 == CNST_TABLE_REF(8)))
														{	/* Module/statexp.scm 102 */
															obj_t BgL_arg1210z00_776;

															{	/* Module/statexp.scm 102 */
																obj_t BgL_zc3z04anonymousza31213ze3z87_949;

																BgL_zc3z04anonymousza31213ze3z87_949 =
																	MAKE_FX_PROCEDURE
																	(BGl_z62zc3z04anonymousza31213ze3ze5zzmodule_statexpz00,
																	(int) (0L), (int) (3L));
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31213ze3z87_949,
																	(int) (0L), BgL_protoz00_756);
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31213ze3z87_949,
																	(int) (1L), BgL_importz00_29);
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31213ze3z87_949,
																	(int) (2L), BgL_prototypez00_28);
																BgL_arg1210z00_776 =
																	BGl_makezd2promisezd2zz__r4_control_features_6_9z00
																	(BgL_zc3z04anonymousza31213ze3z87_949);
															}
															return
																(BGl_za2localzd2classesza2zd2zzmodule_statexpz00
																=
																MAKE_YOUNG_PAIR(BgL_arg1210z00_776,
																	BGl_za2localzd2classesza2zd2zzmodule_statexpz00),
																BUNSPEC);
														}
													else
														{	/* Module/statexp.scm 85 */
															if (
																(BgL_casezd2valuezd2_758 == CNST_TABLE_REF(9)))
																{	/* Module/statexp.scm 107 */
																	obj_t BgL_arg1218z00_782;

																	{	/* Module/statexp.scm 107 */
																		obj_t BgL_zc3z04anonymousza31220ze3z87_950;

																		BgL_zc3z04anonymousza31220ze3z87_950 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza31220ze3ze5zzmodule_statexpz00,
																			(int) (0L), (int) (3L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31220ze3z87_950,
																			(int) (0L), BgL_protoz00_756);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31220ze3z87_950,
																			(int) (1L), BgL_importz00_29);
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza31220ze3z87_950,
																			(int) (2L), BgL_prototypez00_28);
																		BgL_arg1218z00_782 =
																			BGl_makezd2promisezd2zz__r4_control_features_6_9z00
																			(BgL_zc3z04anonymousza31220ze3z87_950);
																	}
																	return
																		(BGl_za2localzd2classesza2zd2zzmodule_statexpz00
																		=
																		MAKE_YOUNG_PAIR(BgL_arg1218z00_782,
																			BGl_za2localzd2classesza2zd2zzmodule_statexpz00),
																		BUNSPEC);
																}
															else
																{	/* Module/statexp.scm 85 */
																	if (
																		(BgL_casezd2valuezd2_758 ==
																			CNST_TABLE_REF(10)))
																		{	/* Module/statexp.scm 112 */
																			obj_t BgL_arg1223z00_788;

																			{	/* Module/statexp.scm 112 */
																				obj_t
																					BgL_zc3z04anonymousza31226ze3z87_951;
																				BgL_zc3z04anonymousza31226ze3z87_951 =
																					MAKE_FX_PROCEDURE
																					(BGl_z62zc3z04anonymousza31226ze3ze5zzmodule_statexpz00,
																					(int) (0L), (int) (3L));
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31226ze3z87_951,
																					(int) (0L), BgL_protoz00_756);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31226ze3z87_951,
																					(int) (1L), BgL_importz00_29);
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza31226ze3z87_951,
																					(int) (2L), BgL_prototypez00_28);
																				BgL_arg1223z00_788 =
																					BGl_makezd2promisezd2zz__r4_control_features_6_9z00
																					(BgL_zc3z04anonymousza31226ze3z87_951);
																			}
																			return
																				(BGl_za2localzd2classesza2zd2zzmodule_statexpz00
																				=
																				MAKE_YOUNG_PAIR(BgL_arg1223z00_788,
																					BGl_za2localzd2classesza2zd2zzmodule_statexpz00),
																				BUNSPEC);
																		}
																	else
																		{	/* Module/statexp.scm 85 */
																			if (
																				(BgL_casezd2valuezd2_758 ==
																					CNST_TABLE_REF(11)))
																				{	/* Module/statexp.scm 115 */
																					obj_t BgL_envz00_795;

																					BgL_envz00_795 =
																						BGl_defaultzd2environmentzd2zz__evalz00
																						();
																					{	/* Module/statexp.scm 115 */

																						BGL_TAIL return
																							BGl_evalz00zz__evalz00
																							(BgL_protoz00_756,
																							BgL_envz00_795);
																					}
																				}
																			else
																				{	/* Module/statexp.scm 85 */
																					if (
																						(BgL_casezd2valuezd2_758 ==
																							CNST_TABLE_REF(12)))
																						{	/* Module/statexp.scm 85 */
																							return
																								BGl_tozd2bezd2macroz12z12zzexpand_expanderz00
																								(CAR(CDR(BgL_protoz00_756)),
																								BgL_prototypez00_28);
																						}
																					else
																						{	/* Module/statexp.scm 85 */
																							if (
																								(BgL_casezd2valuezd2_758 ==
																									CNST_TABLE_REF(13)))
																								{	/* Module/statexp.scm 85 */
																									return
																										BGl_tozd2bezd2macroz12z12zzexpand_expanderz00
																										(CAR(CDR(BgL_protoz00_756)),
																										BgL_prototypez00_28);
																								}
																							else
																								{	/* Module/statexp.scm 85 */
																									if (
																										(BgL_casezd2valuezd2_758 ==
																											CNST_TABLE_REF(14)))
																										{	/* Module/statexp.scm 85 */
																											return
																												BGl_tozd2bezd2macroz12z12zzexpand_expanderz00
																												(CAR(CDR
																													(BgL_protoz00_756)),
																												BgL_prototypez00_28);
																										}
																									else
																										{	/* Module/statexp.scm 123 */
																											obj_t BgL_list1235z00_802;

																											BgL_list1235z00_802 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											return
																												BGl_userzd2errorzd2zztools_errorz00
																												(BGl_string1324z00zzmodule_statexpz00,
																												BGl_string1326z00zzmodule_statexpz00,
																												BgL_prototypez00_28,
																												BgL_list1235z00_802);
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
					}
				else
					{	/* Module/statexp.scm 83 */
						obj_t BgL_arg1236z00_805;

						BgL_arg1236z00_805 =
							BGl_findzd2locationzd2zztools_locationz00
							(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00);
						{	/* Module/statexp.scm 83 */
							obj_t BgL_list1237z00_806;

							BgL_list1237z00_806 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							return
								BGl_userzd2errorzf2locationz20zztools_errorz00
								(BgL_arg1236z00_805, BGl_string1324z00zzmodule_statexpz00,
								BGl_string1326z00zzmodule_statexpz00, BgL_prototypez00_28,
								BgL_list1237z00_806);
						}
					}
			}
		}

	}



/* &<@anonymous:1207> */
	obj_t BGl_z62zc3z04anonymousza31207ze3ze5zzmodule_statexpz00(obj_t
		BgL_envz00_952)
	{
		{	/* Module/statexp.scm 96 */
			{	/* Module/statexp.scm 97 */
				obj_t BgL_protoz00_953;
				obj_t BgL_importz00_954;
				obj_t BgL_prototypez00_955;

				BgL_protoz00_953 = PROCEDURE_REF(BgL_envz00_952, (int) (0L));
				BgL_importz00_954 = PROCEDURE_REF(BgL_envz00_952, (int) (1L));
				BgL_prototypez00_955 = PROCEDURE_REF(BgL_envz00_952, (int) (2L));
				{	/* Module/statexp.scm 97 */
					obj_t BgL_arg1208z00_996;

					BgL_arg1208z00_996 = CDR(((obj_t) BgL_protoz00_953));
					return
						BGl_declarezd2classz12zc0zzmodule_classz00(BgL_arg1208z00_996,
						BGl_za2moduleza2z00zzmodule_modulez00, BgL_importz00_954,
						((bool_t) 0), ((bool_t) 0), BgL_prototypez00_955, BFALSE);
				}
			}
		}

	}



/* &<@anonymous:1213> */
	obj_t BGl_z62zc3z04anonymousza31213ze3ze5zzmodule_statexpz00(obj_t
		BgL_envz00_956)
	{
		{	/* Module/statexp.scm 101 */
			{	/* Module/statexp.scm 102 */
				obj_t BgL_protoz00_957;
				obj_t BgL_importz00_958;
				obj_t BgL_prototypez00_959;

				BgL_protoz00_957 = PROCEDURE_REF(BgL_envz00_956, (int) (0L));
				BgL_importz00_958 = PROCEDURE_REF(BgL_envz00_956, (int) (1L));
				BgL_prototypez00_959 = PROCEDURE_REF(BgL_envz00_956, (int) (2L));
				{	/* Module/statexp.scm 102 */
					obj_t BgL_arg1215z00_997;

					BgL_arg1215z00_997 = CDR(((obj_t) BgL_protoz00_957));
					return
						BGl_declarezd2classz12zc0zzmodule_classz00(BgL_arg1215z00_997,
						BGl_za2moduleza2z00zzmodule_modulez00, BgL_importz00_958,
						((bool_t) 0), ((bool_t) 1), BgL_prototypez00_959, BFALSE);
				}
			}
		}

	}



/* &<@anonymous:1220> */
	obj_t BGl_z62zc3z04anonymousza31220ze3ze5zzmodule_statexpz00(obj_t
		BgL_envz00_960)
	{
		{	/* Module/statexp.scm 106 */
			{	/* Module/statexp.scm 107 */
				obj_t BgL_protoz00_961;
				obj_t BgL_importz00_962;
				obj_t BgL_prototypez00_963;

				BgL_protoz00_961 = PROCEDURE_REF(BgL_envz00_960, (int) (0L));
				BgL_importz00_962 = PROCEDURE_REF(BgL_envz00_960, (int) (1L));
				BgL_prototypez00_963 = PROCEDURE_REF(BgL_envz00_960, (int) (2L));
				{	/* Module/statexp.scm 107 */
					obj_t BgL_arg1221z00_998;

					BgL_arg1221z00_998 = CDR(((obj_t) BgL_protoz00_961));
					return
						BGl_declarezd2classz12zc0zzmodule_classz00(BgL_arg1221z00_998,
						BGl_za2moduleza2z00zzmodule_modulez00, BgL_importz00_962,
						((bool_t) 1), ((bool_t) 0), BgL_prototypez00_963, BFALSE);
				}
			}
		}

	}



/* &<@anonymous:1226> */
	obj_t BGl_z62zc3z04anonymousza31226ze3ze5zzmodule_statexpz00(obj_t
		BgL_envz00_964)
	{
		{	/* Module/statexp.scm 111 */
			{	/* Module/statexp.scm 112 */
				obj_t BgL_protoz00_965;
				obj_t BgL_importz00_966;
				obj_t BgL_prototypez00_967;

				BgL_protoz00_965 = PROCEDURE_REF(BgL_envz00_964, (int) (0L));
				BgL_importz00_966 = PROCEDURE_REF(BgL_envz00_964, (int) (1L));
				BgL_prototypez00_967 = PROCEDURE_REF(BgL_envz00_964, (int) (2L));
				{	/* Module/statexp.scm 112 */
					obj_t BgL_arg1227z00_999;

					BgL_arg1227z00_999 = CDR(((obj_t) BgL_protoz00_965));
					return
						BGl_declarezd2widezd2classz12z12zzmodule_classz00
						(BgL_arg1227z00_999, BGl_za2moduleza2z00zzmodule_modulez00,
						BgL_importz00_966, BgL_prototypez00_967, BFALSE);
				}
			}
		}

	}



/* &statexp-finalizer */
	obj_t BGl_z62statexpzd2finaliza7erz17zzmodule_statexpz00(obj_t BgL_envz00_947)
	{
		{	/* Module/statexp.scm 145 */
			{	/* Module/statexp.scm 147 */
				obj_t BgL_g1129z00_1000;

				BgL_g1129z00_1000 =
					bgl_reverse_bang(BGl_za2localzd2classesza2zd2zzmodule_statexpz00);
				{
					obj_t BgL_l1126z00_1002;

					BgL_l1126z00_1002 = BgL_g1129z00_1000;
				BgL_zc3z04anonymousza31238ze3z87_1001:
					if (PAIRP(BgL_l1126z00_1002))
						{	/* Module/statexp.scm 147 */
							{	/* Module/statexp.scm 147 */
								obj_t BgL_arg1242z00_1003;

								BgL_arg1242z00_1003 = CAR(BgL_l1126z00_1002);
								BGL_PROCEDURE_CALL0(BgL_arg1242z00_1003);
							}
							{
								obj_t BgL_l1126z00_1247;

								BgL_l1126z00_1247 = CDR(BgL_l1126z00_1002);
								BgL_l1126z00_1002 = BgL_l1126z00_1247;
								goto BgL_zc3z04anonymousza31238ze3z87_1001;
							}
						}
					else
						{	/* Module/statexp.scm 147 */
							((bool_t) 1);
						}
				}
			}
			BGl_za2localzd2classesza2zd2zzmodule_statexpz00 = BNIL;
			{	/* Module/statexp.scm 150 */
				obj_t BgL_classesz00_1004;

				BgL_classesz00_1004 = BGl_classzd2finaliza7erz75zzmodule_classz00();
				if (PAIRP(BgL_classesz00_1004))
					{	/* Module/statexp.scm 151 */
						return BgL_classesz00_1004;
					}
				else
					{	/* Module/statexp.scm 151 */
						return CNST_TABLE_REF(1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_statexpz00(void)
	{
		{	/* Module/statexp.scm 15 */
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zzmodule_prototypez00(499400840L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zzexpand_expanderz00(393376L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(502577483L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
			return
				BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string1327z00zzmodule_statexpz00));
		}

	}

#ifdef __cplusplus
}
#endif
