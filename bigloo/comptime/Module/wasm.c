/*===========================================================================*/
/*   (Module/wasm.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/wasm.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_WASM_TYPE_DEFINITIONS
#define BGL_MODULE_WASM_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_MODULE_WASM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62wasmzd2finaliza7erz17zzmodule_wasmz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_wasmz00 = BUNSPEC;
	static obj_t BGl_z62wasmzd2producerzb0zzmodule_wasmz00(obj_t, obj_t);
	static obj_t BGl_za2wasmzd2externza2zd2zzmodule_wasmz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzmodule_wasmz00(void);
	static obj_t BGl_z62zc3z04anonymousza31132ze3ze5zzmodule_wasmz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_wasmz00(void);
	static obj_t BGl_objectzd2initzd2zzmodule_wasmz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_wasmzd2parserzd2zzmodule_wasmz00(obj_t, bool_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_wasmz00(void);
	static obj_t BGl_wasmzd2producerzd2zzmodule_wasmz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62makezd2wasmzd2compilerz62zzmodule_wasmz00(obj_t);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzmodule_wasmz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_checksumz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzmodule_wasmz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_wasmz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_wasmz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_wasmz00(void);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2wasmzd2compilerz00zzmodule_wasmz00(void);
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2wasmzd2compilerzd2envzd2zzmodule_wasmz00,
		BgL_bgl_za762makeza7d2wasmza7d1213za7,
		BGl_z62makezd2wasmzd2compilerz62zzmodule_wasmz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_wasmzd2finaliza7erzd2envza7zzmodule_wasmz00,
		BgL_bgl_za762wasmza7d2finali1214z00,
		BGl_z62wasmzd2finaliza7erz17zzmodule_wasmz00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_wasmzd2producerzd2envz00zzmodule_wasmz00,
		BgL_bgl_za762wasmza7d2produc1215z00,
		BGl_z62wasmzd2producerzb0zzmodule_wasmz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1204z00zzmodule_wasmz00,
		BgL_bgl_string1204za700za7za7m1216za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string1205z00zzmodule_wasmz00,
		BgL_bgl_string1205za700za7za7m1217za7, "Illegal `wasm' clause", 21);
	      DEFINE_STRING(BGl_string1206z00zzmodule_wasmz00,
		BgL_bgl_string1206za700za7za7m1218za7, "Illegal wasm form", 17);
	      DEFINE_STRING(BGl_string1207z00zzmodule_wasmz00,
		BgL_bgl_string1207za700za7za7m1219za7, "wasm", 4);
	      DEFINE_STRING(BGl_string1208z00zzmodule_wasmz00,
		BgL_bgl_string1208za700za7za7m1220za7, "Cannot find extern definition", 29);
	      DEFINE_STRING(BGl_string1209z00zzmodule_wasmz00,
		BgL_bgl_string1209za700za7za7m1221za7, "module_wasm", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1203z00zzmodule_wasmz00,
		BgL_bgl_za762za7c3za704anonymo1222za7,
		BGl_z62zc3z04anonymousza31132ze3ze5zzmodule_wasmz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1210z00zzmodule_wasmz00,
		BgL_bgl_string1210za700za7za7m1223za7, "foreign wasm ", 13);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzmodule_wasmz00));
		     ADD_ROOT((void *) (&BGl_za2wasmzd2externza2zd2zzmodule_wasmz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_wasmz00(long
		BgL_checksumz00_739, char *BgL_fromz00_740)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_wasmz00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_wasmz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_wasmz00();
					BGl_libraryzd2moduleszd2initz00zzmodule_wasmz00();
					BGl_cnstzd2initzd2zzmodule_wasmz00();
					BGl_importedzd2moduleszd2initz00zzmodule_wasmz00();
					return BGl_toplevelzd2initzd2zzmodule_wasmz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_wasm");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_wasm");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "module_wasm");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "module_wasm");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "module_wasm");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "module_wasm");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "module_wasm");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_wasm");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_wasm");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 15 */
			{	/* Module/wasm.scm 15 */
				obj_t BgL_cportz00_718;

				{	/* Module/wasm.scm 15 */
					obj_t BgL_stringz00_725;

					BgL_stringz00_725 = BGl_string1210z00zzmodule_wasmz00;
					{	/* Module/wasm.scm 15 */
						obj_t BgL_startz00_726;

						BgL_startz00_726 = BINT(0L);
						{	/* Module/wasm.scm 15 */
							obj_t BgL_endz00_727;

							BgL_endz00_727 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_725)));
							{	/* Module/wasm.scm 15 */

								BgL_cportz00_718 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_725, BgL_startz00_726, BgL_endz00_727);
				}}}}
				{
					long BgL_iz00_719;

					BgL_iz00_719 = 1L;
				BgL_loopz00_720:
					if ((BgL_iz00_719 == -1L))
						{	/* Module/wasm.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/wasm.scm 15 */
							{	/* Module/wasm.scm 15 */
								obj_t BgL_arg1212z00_721;

								{	/* Module/wasm.scm 15 */

									{	/* Module/wasm.scm 15 */
										obj_t BgL_locationz00_723;

										BgL_locationz00_723 = BBOOL(((bool_t) 0));
										{	/* Module/wasm.scm 15 */

											BgL_arg1212z00_721 =
												BGl_readz00zz__readerz00(BgL_cportz00_718,
												BgL_locationz00_723);
										}
									}
								}
								{	/* Module/wasm.scm 15 */
									int BgL_tmpz00_767;

									BgL_tmpz00_767 = (int) (BgL_iz00_719);
									CNST_TABLE_SET(BgL_tmpz00_767, BgL_arg1212z00_721);
							}}
							{	/* Module/wasm.scm 15 */
								int BgL_auxz00_724;

								BgL_auxz00_724 = (int) ((BgL_iz00_719 - 1L));
								{
									long BgL_iz00_772;

									BgL_iz00_772 = (long) (BgL_auxz00_724);
									BgL_iz00_719 = BgL_iz00_772;
									goto BgL_loopz00_720;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 15 */
			return (BGl_za2wasmzd2externza2zd2zzmodule_wasmz00 = BNIL, BUNSPEC);
		}

	}



/* make-wasm-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2wasmzd2compilerz00zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 38 */
			{	/* Module/wasm.scm 39 */
				BgL_ccompz00_bglt BgL_new1067z00_587;

				{	/* Module/wasm.scm 40 */
					BgL_ccompz00_bglt BgL_new1066z00_591;

					BgL_new1066z00_591 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/wasm.scm 40 */
						long BgL_arg1137z00_592;

						BgL_arg1137z00_592 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1066z00_591), BgL_arg1137z00_592);
					}
					BgL_new1067z00_587 = BgL_new1066z00_591;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1067z00_587))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1067z00_587))->BgL_producerz00) =
					((obj_t) BGl_wasmzd2producerzd2envz00zzmodule_wasmz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1067z00_587))->BgL_consumerz00) =
					((obj_t) BGl_proc1203z00zzmodule_wasmz00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1067z00_587))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_wasmzd2finaliza7erzd2envza7zzmodule_wasmz00), BUNSPEC);
				return ((obj_t) BgL_new1067z00_587);
			}
		}

	}



/* &make-wasm-compiler */
	obj_t BGl_z62makezd2wasmzd2compilerz62zzmodule_wasmz00(obj_t BgL_envz00_710)
	{
		{	/* Module/wasm.scm 38 */
			return BGl_makezd2wasmzd2compilerz00zzmodule_wasmz00();
		}

	}



/* &<@anonymous:1132> */
	obj_t BGl_z62zc3z04anonymousza31132ze3ze5zzmodule_wasmz00(obj_t
		BgL_envz00_711, obj_t BgL_mz00_712, obj_t BgL_cz00_713)
	{
		{	/* Module/wasm.scm 42 */
			return BGl_wasmzd2producerzd2zzmodule_wasmz00(BgL_cz00_713);
		}

	}



/* wasm-producer */
	obj_t BGl_wasmzd2producerzd2zzmodule_wasmz00(obj_t BgL_clausez00_25)
	{
		{	/* Module/wasm.scm 48 */
			{	/* Module/wasm.scm 49 */
				bool_t BgL_test1226z00_787;

				{	/* Module/wasm.scm 49 */
					obj_t BgL_arg1152z00_610;

					{	/* Module/wasm.scm 49 */
						obj_t BgL_arg1153z00_611;

						BgL_arg1153z00_611 = BGl_thezd2backendzd2zzbackend_backendz00();
						BgL_arg1152z00_610 =
							(((BgL_backendz00_bglt) COBJECT(
									((BgL_backendz00_bglt) BgL_arg1153z00_611)))->
							BgL_foreignzd2clausezd2supportz00);
					}
					BgL_test1226z00_787 =
						CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF(0),
							BgL_arg1152z00_610));
				}
				if (BgL_test1226z00_787)
					{
						obj_t BgL_protosz00_596;

						if (PAIRP(BgL_clausez00_25))
							{	/* Module/wasm.scm 50 */
								obj_t BgL_arg1143z00_601;

								BgL_arg1143z00_601 = CDR(((obj_t) BgL_clausez00_25));
								BgL_protosz00_596 = BgL_arg1143z00_601;
								{
									obj_t BgL_l1096z00_603;

									BgL_l1096z00_603 = BgL_protosz00_596;
								BgL_zc3z04anonymousza31144ze3z87_604:
									if (PAIRP(BgL_l1096z00_603))
										{	/* Module/wasm.scm 52 */
											BGl_wasmzd2parserzd2zzmodule_wasmz00(CAR
												(BgL_l1096z00_603), ((bool_t) 1));
											{
												obj_t BgL_l1096z00_802;

												BgL_l1096z00_802 = CDR(BgL_l1096z00_603);
												BgL_l1096z00_603 = BgL_l1096z00_802;
												goto BgL_zc3z04anonymousza31144ze3z87_604;
											}
										}
									else
										{	/* Module/wasm.scm 52 */
											((bool_t) 1);
										}
								}
								return BNIL;
							}
						else
							{	/* Module/wasm.scm 50 */
								{	/* Module/wasm.scm 55 */
									obj_t BgL_list1149z00_609;

									BgL_list1149z00_609 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									return
										BGl_userzd2errorzd2zztools_errorz00
										(BGl_string1204z00zzmodule_wasmz00,
										BGl_string1205z00zzmodule_wasmz00, BgL_clausez00_25,
										BgL_list1149z00_609);
								}
							}
					}
				else
					{	/* Module/wasm.scm 49 */
						return BNIL;
					}
			}
		}

	}



/* &wasm-producer */
	obj_t BGl_z62wasmzd2producerzb0zzmodule_wasmz00(obj_t BgL_envz00_714,
		obj_t BgL_clausez00_715)
	{
		{	/* Module/wasm.scm 48 */
			return BGl_wasmzd2producerzd2zzmodule_wasmz00(BgL_clausez00_715);
		}

	}



/* wasm-parser */
	obj_t BGl_wasmzd2parserzd2zzmodule_wasmz00(obj_t BgL_wasmz00_26,
		bool_t BgL_exportpz00_27)
	{
		{	/* Module/wasm.scm 61 */
			{
				obj_t BgL_idz00_612;
				obj_t BgL_namez00_613;
				obj_t BgL_depsz00_614;

				if (PAIRP(BgL_wasmz00_26))
					{	/* Module/wasm.scm 63 */
						obj_t BgL_carzd2376zd2_619;
						obj_t BgL_cdrzd2377zd2_620;

						BgL_carzd2376zd2_619 = CAR(((obj_t) BgL_wasmz00_26));
						BgL_cdrzd2377zd2_620 = CDR(((obj_t) BgL_wasmz00_26));
						if (SYMBOLP(BgL_carzd2376zd2_619))
							{	/* Module/wasm.scm 63 */
								if (PAIRP(BgL_cdrzd2377zd2_620))
									{	/* Module/wasm.scm 63 */
										obj_t BgL_carzd2383zd2_623;

										BgL_carzd2383zd2_623 = CAR(BgL_cdrzd2377zd2_620);
										if (STRINGP(BgL_carzd2383zd2_623))
											{	/* Module/wasm.scm 63 */
												BgL_idz00_612 = BgL_carzd2376zd2_619;
												BgL_namez00_613 = BgL_carzd2383zd2_623;
												BgL_depsz00_614 = CDR(BgL_cdrzd2377zd2_620);
												{	/* Module/wasm.scm 65 */
													obj_t BgL_arg1162z00_626;

													{	/* Module/wasm.scm 65 */
														obj_t BgL_v1098z00_627;

														BgL_v1098z00_627 = create_vector(3L);
														VECTOR_SET(BgL_v1098z00_627, 0L, BgL_idz00_612);
														VECTOR_SET(BgL_v1098z00_627, 1L, BgL_namez00_613);
														VECTOR_SET(BgL_v1098z00_627, 2L, BgL_depsz00_614);
														BgL_arg1162z00_626 = BgL_v1098z00_627;
													}
													return (BGl_za2wasmzd2externza2zd2zzmodule_wasmz00 =
														MAKE_YOUNG_PAIR(BgL_arg1162z00_626,
															BGl_za2wasmzd2externza2zd2zzmodule_wasmz00),
														BUNSPEC);
												}
											}
										else
											{	/* Module/wasm.scm 63 */
											BgL_tagzd2368zd2_616:
												{	/* Module/wasm.scm 67 */
													obj_t BgL_list1163z00_628;

													BgL_list1163z00_628 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													return
														BGl_userzd2errorzd2zztools_errorz00
														(BGl_string1204z00zzmodule_wasmz00,
														BGl_string1206z00zzmodule_wasmz00, BgL_wasmz00_26,
														BgL_list1163z00_628);
												}
											}
									}
								else
									{	/* Module/wasm.scm 63 */
										goto BgL_tagzd2368zd2_616;
									}
							}
						else
							{	/* Module/wasm.scm 63 */
								goto BgL_tagzd2368zd2_616;
							}
					}
				else
					{	/* Module/wasm.scm 63 */
						goto BgL_tagzd2368zd2_616;
					}
			}
		}

	}



/* &wasm-finalizer */
	obj_t BGl_z62wasmzd2finaliza7erz17zzmodule_wasmz00(obj_t BgL_envz00_716)
	{
		{	/* Module/wasm.scm 77 */
			{	/* Module/wasm.scm 78 */
				bool_t BgL_tmpz00_828;

				{
					obj_t BgL_l1099z00_730;

					BgL_l1099z00_730 = BGl_za2wasmzd2externza2zd2zzmodule_wasmz00;
				BgL_zc3z04anonymousza31164ze3z87_729:
					if (PAIRP(BgL_l1099z00_730))
						{	/* Module/wasm.scm 78 */
							{	/* Module/wasm.scm 79 */
								obj_t BgL_wz00_731;

								BgL_wz00_731 = CAR(BgL_l1099z00_730);
								{	/* Module/wasm.scm 79 */
									obj_t BgL_idz00_732;
									obj_t BgL_namez00_733;
									obj_t BgL_depsz00_734;

									BgL_idz00_732 = VECTOR_REF(((obj_t) BgL_wz00_731), 0L);
									BgL_namez00_733 = VECTOR_REF(((obj_t) BgL_wz00_731), 1L);
									BgL_depsz00_734 = VECTOR_REF(((obj_t) BgL_wz00_731), 2L);
									{	/* Module/wasm.scm 82 */
										obj_t BgL_gz00_735;

										BgL_gz00_735 =
											BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idz00_732,
											CNST_TABLE_REF(1));
										if (CBOOL(BgL_gz00_735))
											{	/* Module/wasm.scm 83 */
												((((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_gz00_735)))->
														BgL_jvmzd2typezd2namez00) =
													((obj_t) ((obj_t) BgL_namez00_733)), BUNSPEC);
												{	/* Module/wasm.scm 87 */
													obj_t BgL_arg1166z00_736;

													{	/* Module/wasm.scm 87 */
														obj_t BgL_arg1171z00_737;
														obj_t BgL_arg1172z00_738;

														BgL_arg1171z00_737 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
															BgL_depsz00_734);
														BgL_arg1172z00_738 =
															(((BgL_globalz00_bglt)
																COBJECT(((BgL_globalz00_bglt) BgL_gz00_735)))->
															BgL_pragmaz00);
														BgL_arg1166z00_736 =
															MAKE_YOUNG_PAIR(BgL_arg1171z00_737,
															BgL_arg1172z00_738);
													}
													((((BgL_globalz00_bglt) COBJECT(
																	((BgL_globalz00_bglt) BgL_gz00_735)))->
															BgL_pragmaz00) =
														((obj_t) BgL_arg1166z00_736), BUNSPEC);
												}
											}
										else
											{	/* Module/wasm.scm 83 */
												BGl_errorz00zz__errorz00
													(BGl_string1207z00zzmodule_wasmz00,
													BGl_string1208z00zzmodule_wasmz00, BgL_idz00_732);
											}
									}
								}
							}
							{
								obj_t BgL_l1099z00_853;

								BgL_l1099z00_853 = CDR(BgL_l1099z00_730);
								BgL_l1099z00_730 = BgL_l1099z00_853;
								goto BgL_zc3z04anonymousza31164ze3z87_729;
							}
						}
					else
						{	/* Module/wasm.scm 78 */
							BgL_tmpz00_828 = ((bool_t) 1);
						}
				}
				return BBOOL(BgL_tmpz00_828);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_wasmz00(void)
	{
		{	/* Module/wasm.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zzmodule_checksumz00(457474182L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1209z00zzmodule_wasmz00));
		}

	}

#ifdef __cplusplus
}
#endif
