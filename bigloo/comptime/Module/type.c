/*===========================================================================*/
/*   (Module/type.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/type.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_TYPE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tvecz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}              *BgL_tvecz00_bglt;


#endif													// BGL_MODULE_TYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62zc3z04anonymousza31103ze3ze5zzmodule_typez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_delayzd2tvectorz12zc0zzmodule_typez00(obj_t,
		obj_t, bool_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_typez00 = BUNSPEC;
	static obj_t BGl_z62tvectorzd2finaliza7erz17zzmodule_typez00(obj_t);
	static obj_t BGl_z62typezd2producerzf2consumerz42zzmodule_typez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern BgL_typez00_bglt BGl_declarezd2subtypez12zc0zztype_envz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzmodule_typez00(void);
	static obj_t BGl_genericzd2initzd2zzmodule_typez00(void);
	extern BgL_typez00_bglt BGl_declarezd2typez12zc0zztype_envz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_typez00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62typezd2finaliza7erz17zzmodule_typez00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_typez00(void);
	static obj_t BGl_za2tvectorsza2z00zzmodule_typez00 = BUNSPEC;
	static obj_t BGl_z62makezd2typezd2compilerz62zzmodule_typez00(obj_t);
	static obj_t BGl_zc3z04anonymousza32857ze3ze70z60zzmodule_typez00(obj_t);
	extern obj_t BGl_addzd2coercionz12zc0zztype_coercionz00(BgL_typez00_bglt,
		BgL_typez00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	static obj_t BGl_typezd2parserzd2zzmodule_typez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzmodule_typez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_coercionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern BgL_typez00_bglt
		BGl_declarezd2tvectorzd2typez12z12zztvector_tvectorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	BGL_EXPORTED_DECL obj_t BGl_tvectorzd2finaliza7erz75zzmodule_typez00(void);
	static obj_t BGl_cnstzd2initzd2zzmodule_typez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_typez00(void);
	extern obj_t
		BGl_makezd2tvectorzd2accessesz00zztvector_accessz00(BgL_typez00_bglt, obj_t,
		bool_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_typez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_typez00(void);
	static obj_t BGl_typezd2producerzf2consumerz20zzmodule_typez00(obj_t, obj_t);
	static obj_t BGl_delayzd2tvectorzd2typez12z12zzmodule_typez00(obj_t, obj_t,
		obj_t, obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_za2tvectorzd2typesza2zd2zzmodule_typez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2tvectorzd2clausez00zzmodule_typez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62delayzd2tvectorz12za2zzmodule_typez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2typezd2compilerz00zzmodule_typez00(void);
	extern BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62modulezd2tvectorzd2clausez62zzmodule_typez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t __cnst[10];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_modulezd2tvectorzd2clausezd2envzd2zzmodule_typez00,
		BgL_bgl_za762moduleza7d2tvec2904z00,
		BGl_z62modulezd2tvectorzd2clausez62zzmodule_typez00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2900z00zzmodule_typez00,
		BgL_bgl_string2900za700za7za7m2905za7, "module_type", 11);
	      DEFINE_STRING(BGl_string2901z00zzmodule_typez00,
		BgL_bgl_string2901za700za7za7m2906za7,
		"void unit tvector coerce magic bigloo @ lambda subtype type ", 60);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2typezd2compilerzd2envzd2zzmodule_typez00,
		BgL_bgl_za762makeza7d2typeza7d2907za7,
		BGl_z62makezd2typezd2compilerz62zzmodule_typez00, 0L, BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_typezd2producerzf2consumerzd2envzf2zzmodule_typez00,
		BgL_bgl_za762typeza7d2produc2908z00,
		BGl_z62typezd2producerzf2consumerz42zzmodule_typez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_delayzd2tvectorz12zd2envz12zzmodule_typez00,
		BgL_bgl_za762delayza7d2tvect2909z00,
		BGl_z62delayzd2tvectorz12za2zzmodule_typez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tvectorzd2finaliza7erzd2envza7zzmodule_typez00,
		BgL_bgl_za762tvectorza7d2fin2910z00,
		BGl_z62tvectorzd2finaliza7erz17zzmodule_typez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2889z00zzmodule_typez00,
		BgL_bgl_string2889za700za7za7m2911za7, "Parse error", 11);
	      DEFINE_STRING(BGl_string2890z00zzmodule_typez00,
		BgL_bgl_string2890za700za7za7m2912za7, "Illegal `type' clause", 21);
	      DEFINE_STRING(BGl_string2891z00zzmodule_typez00,
		BgL_bgl_string2891za700za7za7m2913za7, "Subtype", 7);
	      DEFINE_STRING(BGl_string2892z00zzmodule_typez00,
		BgL_bgl_string2892za700za7za7m2914za7, "Parents are of different classes",
		32);
	      DEFINE_STRING(BGl_string2893z00zzmodule_typez00,
		BgL_bgl_string2893za700za7za7m2915za7, "Unknow parent type", 18);
	      DEFINE_STRING(BGl_string2894z00zzmodule_typez00,
		BgL_bgl_string2894za700za7za7m2916za7, "Illegal type declaration", 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2888z00zzmodule_typez00,
		BgL_bgl_za762za7c3za704anonymo2917za7,
		BGl_z62zc3z04anonymousza31103ze3ze5zzmodule_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2895z00zzmodule_typez00,
		BgL_bgl_string2895za700za7za7m2918za7, "Coercion", 8);
	      DEFINE_STRING(BGl_string2896z00zzmodule_typez00,
		BgL_bgl_string2896za700za7za7m2919za7, "Illegal coerce clause", 21);
	      DEFINE_STRING(BGl_string2897z00zzmodule_typez00,
		BgL_bgl_string2897za700za7za7m2920za7, "Illegal clause", 14);
	      DEFINE_STRING(BGl_string2898z00zzmodule_typez00,
		BgL_bgl_string2898za700za7za7m2921za7, "type coercion", 13);
	      DEFINE_STRING(BGl_string2899z00zzmodule_typez00,
		BgL_bgl_string2899za700za7za7m2922za7, "Unknow type", 11);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_typezd2finaliza7erzd2envza7zzmodule_typez00,
		BgL_bgl_za762typeza7d2finali2923z00,
		BGl_z62typezd2finaliza7erz17zzmodule_typez00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzmodule_typez00));
		     ADD_ROOT((void *) (&BGl_za2tvectorsza2z00zzmodule_typez00));
		     ADD_ROOT((void *) (&BGl_za2tvectorzd2typesza2zd2zzmodule_typez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_typez00(long
		BgL_checksumz00_3753, char *BgL_fromz00_3754)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_typez00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_typez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_typez00();
					BGl_libraryzd2moduleszd2initz00zzmodule_typez00();
					BGl_cnstzd2initzd2zzmodule_typez00();
					BGl_importedzd2moduleszd2initz00zzmodule_typez00();
					return BGl_toplevelzd2initzd2zzmodule_typez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_typez00(void)
	{
		{	/* Module/type.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_type");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_type");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_type");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "module_type");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "module_type");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "module_type");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_type");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "module_type");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_type");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "module_type");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_type");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_typez00(void)
	{
		{	/* Module/type.scm 15 */
			{	/* Module/type.scm 15 */
				obj_t BgL_cportz00_3740;

				{	/* Module/type.scm 15 */
					obj_t BgL_stringz00_3747;

					BgL_stringz00_3747 = BGl_string2901z00zzmodule_typez00;
					{	/* Module/type.scm 15 */
						obj_t BgL_startz00_3748;

						BgL_startz00_3748 = BINT(0L);
						{	/* Module/type.scm 15 */
							obj_t BgL_endz00_3749;

							BgL_endz00_3749 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3747)));
							{	/* Module/type.scm 15 */

								BgL_cportz00_3740 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3747, BgL_startz00_3748, BgL_endz00_3749);
				}}}}
				{
					long BgL_iz00_3741;

					BgL_iz00_3741 = 9L;
				BgL_loopz00_3742:
					if ((BgL_iz00_3741 == -1L))
						{	/* Module/type.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/type.scm 15 */
							{	/* Module/type.scm 15 */
								obj_t BgL_arg2903z00_3743;

								{	/* Module/type.scm 15 */

									{	/* Module/type.scm 15 */
										obj_t BgL_locationz00_3745;

										BgL_locationz00_3745 = BBOOL(((bool_t) 0));
										{	/* Module/type.scm 15 */

											BgL_arg2903z00_3743 =
												BGl_readz00zz__readerz00(BgL_cportz00_3740,
												BgL_locationz00_3745);
										}
									}
								}
								{	/* Module/type.scm 15 */
									int BgL_tmpz00_3783;

									BgL_tmpz00_3783 = (int) (BgL_iz00_3741);
									CNST_TABLE_SET(BgL_tmpz00_3783, BgL_arg2903z00_3743);
							}}
							{	/* Module/type.scm 15 */
								int BgL_auxz00_3746;

								BgL_auxz00_3746 = (int) ((BgL_iz00_3741 - 1L));
								{
									long BgL_iz00_3788;

									BgL_iz00_3788 = (long) (BgL_auxz00_3746);
									BgL_iz00_3741 = BgL_iz00_3788;
									goto BgL_loopz00_3742;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_typez00(void)
	{
		{	/* Module/type.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_typez00(void)
	{
		{	/* Module/type.scm 15 */
			BGl_za2tvectorzd2typesza2zd2zzmodule_typez00 = BNIL;
			return (BGl_za2tvectorsza2z00zzmodule_typez00 = BNIL, BUNSPEC);
		}

	}



/* make-type-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2typezd2compilerz00zzmodule_typez00(void)
	{
		{	/* Module/type.scm 35 */
			{	/* Module/type.scm 36 */
				BgL_ccompz00_bglt BgL_new1055z00_192;

				{	/* Module/type.scm 37 */
					BgL_ccompz00_bglt BgL_new1054z00_195;

					BgL_new1054z00_195 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/type.scm 37 */
						long BgL_arg1104z00_196;

						BgL_arg1104z00_196 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1054z00_195), BgL_arg1104z00_196);
					}
					BgL_new1055z00_192 = BgL_new1054z00_195;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1055z00_192))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1055z00_192))->BgL_producerz00) =
					((obj_t) BGl_proc2888z00zzmodule_typez00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1055z00_192))->BgL_consumerz00) =
					((obj_t) BGl_typezd2producerzf2consumerzd2envzf2zzmodule_typez00),
					BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1055z00_192))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_typezd2finaliza7erzd2envza7zzmodule_typez00), BUNSPEC);
				return ((obj_t) BgL_new1055z00_192);
			}
		}

	}



/* &make-type-compiler */
	obj_t BGl_z62makezd2typezd2compilerz62zzmodule_typez00(obj_t BgL_envz00_3722)
	{
		{	/* Module/type.scm 35 */
			return BGl_makezd2typezd2compilerz00zzmodule_typez00();
		}

	}



/* &<@anonymous:1103> */
	obj_t BGl_z62zc3z04anonymousza31103ze3ze5zzmodule_typez00(obj_t
		BgL_envz00_3723, obj_t BgL_cz00_3724)
	{
		{	/* Module/type.scm 38 */
			return
				BGl_typezd2producerzf2consumerz20zzmodule_typez00(BFALSE,
				BgL_cz00_3724);
		}

	}



/* type-producer/consumer */
	obj_t BGl_typezd2producerzf2consumerz20zzmodule_typez00(obj_t
		BgL_importz00_25, obj_t BgL_clausez00_26)
	{
		{	/* Module/type.scm 45 */
			{
				obj_t BgL_protosz00_197;

				if (PAIRP(BgL_clausez00_26))
					{	/* Module/type.scm 46 */
						obj_t BgL_arg1114z00_202;

						BgL_arg1114z00_202 = CDR(((obj_t) BgL_clausez00_26));
						BgL_protosz00_197 = BgL_arg1114z00_202;
						{
							obj_t BgL_l1066z00_204;

							BgL_l1066z00_204 = BgL_protosz00_197;
						BgL_zc3z04anonymousza31115ze3z87_205:
							if (PAIRP(BgL_l1066z00_204))
								{	/* Module/type.scm 48 */
									BGl_typezd2parserzd2zzmodule_typez00(BgL_importz00_25,
										CAR(BgL_l1066z00_204), BgL_clausez00_26);
									{
										obj_t BgL_l1066z00_3811;

										BgL_l1066z00_3811 = CDR(BgL_l1066z00_204);
										BgL_l1066z00_204 = BgL_l1066z00_3811;
										goto BgL_zc3z04anonymousza31115ze3z87_205;
									}
								}
							else
								{	/* Module/type.scm 48 */
									((bool_t) 1);
								}
						}
						return BNIL;
					}
				else
					{	/* Module/type.scm 46 */
						{	/* Module/type.scm 51 */
							obj_t BgL_list1123z00_210;

							BgL_list1123z00_210 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							return
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string2889z00zzmodule_typez00,
								BGl_string2890z00zzmodule_typez00, BgL_clausez00_26,
								BgL_list1123z00_210);
						}
					}
			}
		}

	}



/* &type-producer/consumer */
	obj_t BGl_z62typezd2producerzf2consumerz42zzmodule_typez00(obj_t
		BgL_envz00_3725, obj_t BgL_importz00_3726, obj_t BgL_clausez00_3727)
	{
		{	/* Module/type.scm 45 */
			return
				BGl_typezd2producerzf2consumerz20zzmodule_typez00(BgL_importz00_3726,
				BgL_clausez00_3727);
		}

	}



/* type-parser */
	obj_t BGl_typezd2parserzd2zzmodule_typez00(obj_t BgL_importz00_27,
		obj_t BgL_clausez00_28, obj_t BgL_clausesz00_29)
	{
		{	/* Module/type.scm 56 */
			{
				obj_t BgL_fromz00_234;
				obj_t BgL_toz00_235;
				obj_t BgL_checkz00_236;
				obj_t BgL_coercez00_237;
				obj_t BgL_childz00_226;
				obj_t BgL_namez00_227;
				obj_t BgL_parentz00_228;
				obj_t BgL_nullzd2valuezd2_229;
				obj_t BgL_childz00_222;
				obj_t BgL_namez00_223;
				obj_t BgL_parentz00_224;

				if (PAIRP(BgL_clausez00_28))
					{	/* Module/type.scm 58 */
						obj_t BgL_carzd2389zd2_242;
						obj_t BgL_cdrzd2390zd2_243;

						BgL_carzd2389zd2_242 = CAR(((obj_t) BgL_clausez00_28));
						BgL_cdrzd2390zd2_243 = CDR(((obj_t) BgL_clausez00_28));
						if (SYMBOLP(BgL_carzd2389zd2_242))
							{	/* Module/type.scm 58 */
								if (PAIRP(BgL_cdrzd2390zd2_243))
									{	/* Module/type.scm 58 */
										obj_t BgL_carzd2394zd2_246;

										BgL_carzd2394zd2_246 = CAR(BgL_cdrzd2390zd2_243);
										if (STRINGP(BgL_carzd2394zd2_246))
											{	/* Module/type.scm 58 */
												if (NULLP(CDR(BgL_cdrzd2390zd2_243)))
													{	/* Module/type.scm 58 */
														return
															((obj_t)
															BGl_declarezd2typez12zc0zztype_envz00
															(BgL_carzd2389zd2_242, BgL_carzd2394zd2_246,
																CNST_TABLE_REF(4)));
													}
												else
													{	/* Module/type.scm 58 */
														obj_t BgL_carzd2423zd2_253;
														obj_t BgL_cdrzd2424zd2_254;

														BgL_carzd2423zd2_253 =
															CAR(((obj_t) BgL_cdrzd2390zd2_243));
														BgL_cdrzd2424zd2_254 =
															CDR(((obj_t) BgL_cdrzd2390zd2_243));
														if (STRINGP(BgL_carzd2423zd2_253))
															{	/* Module/type.scm 58 */
																if (PAIRP(BgL_cdrzd2424zd2_254))
																	{	/* Module/type.scm 58 */
																		obj_t BgL_carzd2430zd2_257;

																		BgL_carzd2430zd2_257 =
																			CAR(BgL_cdrzd2424zd2_254);
																		if (SYMBOLP(BgL_carzd2430zd2_257))
																			{	/* Module/type.scm 58 */
																				if (NULLP(CDR(BgL_cdrzd2424zd2_254)))
																					{	/* Module/type.scm 58 */
																						return
																							((obj_t)
																							BGl_declarezd2typez12zc0zztype_envz00
																							(BgL_carzd2389zd2_242,
																								BgL_carzd2423zd2_253,
																								BgL_carzd2430zd2_257));
																					}
																				else
																					{	/* Module/type.scm 58 */
																						if (
																							(CAR(
																									((obj_t) BgL_clausez00_28)) ==
																								CNST_TABLE_REF(5)))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd2457zd2_264;
																								obj_t BgL_cdrzd2458zd2_265;

																								BgL_carzd2457zd2_264 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2390zd2_243));
																								BgL_cdrzd2458zd2_265 =
																									CDR(((obj_t)
																										BgL_cdrzd2390zd2_243));
																								if (SYMBOLP
																									(BgL_carzd2457zd2_264))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd2466zd2_267;
																										obj_t BgL_cdrzd2467zd2_268;

																										BgL_carzd2466zd2_267 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd2458zd2_265));
																										BgL_cdrzd2467zd2_268 =
																											CDR(((obj_t)
																												BgL_cdrzd2458zd2_265));
																										if (STRINGP
																											(BgL_carzd2466zd2_267))
																											{	/* Module/type.scm 58 */
																												if (PAIRP
																													(BgL_cdrzd2467zd2_268))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd2473zd2_271;
																														BgL_carzd2473zd2_271
																															=
																															CAR
																															(BgL_cdrzd2467zd2_268);
																														if (SYMBOLP
																															(BgL_carzd2473zd2_271))
																															{	/* Module/type.scm 58 */
																																if (NULLP(CDR
																																		(BgL_cdrzd2467zd2_268)))
																																	{	/* Module/type.scm 69 */
																																		BgL_typez00_bglt
																																			BgL_typez00_2672;
																																		BgL_typez00_2672
																																			=
																																			BGl_declarezd2typez12zc0zztype_envz00
																																			(BgL_carzd2457zd2_264,
																																			BgL_carzd2466zd2_267,
																																			BgL_carzd2473zd2_271);
																																		((((BgL_typez00_bglt) COBJECT(BgL_typez00_2672))->BgL_magiczf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																																		return
																																			((obj_t)
																																			BgL_typez00_2672);
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																	BgL_tagzd2374zd2_239:
																																		{	/* Module/type.scm 152 */
																																			obj_t
																																				BgL_list2824z00_2421;
																																			BgL_list2824z00_2421
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BNIL,
																																				BNIL);
																																			return
																																				BGl_userzd2errorzd2zztools_errorz00
																																				(BGl_string2889z00zzmodule_typez00,
																																				BGl_string2894z00zzmodule_typez00,
																																				BgL_clausez00_28,
																																				BgL_list2824z00_2421);
																																		}
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								if (
																									(CAR(
																											((obj_t)
																												BgL_clausez00_28)) ==
																										CNST_TABLE_REF(1)))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd2714zd2_279;
																										obj_t BgL_cdrzd2715zd2_280;

																										BgL_carzd2714zd2_279 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd2390zd2_243));
																										BgL_cdrzd2715zd2_280 =
																											CDR(((obj_t)
																												BgL_cdrzd2390zd2_243));
																										if (SYMBOLP
																											(BgL_carzd2714zd2_279))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd2724zd2_282;
																												obj_t
																													BgL_cdrzd2725zd2_283;
																												BgL_carzd2724zd2_282 =
																													CAR(((obj_t)
																														BgL_cdrzd2715zd2_280));
																												BgL_cdrzd2725zd2_283 =
																													CDR(((obj_t)
																														BgL_cdrzd2715zd2_280));
																												if (STRINGP
																													(BgL_carzd2724zd2_282))
																													{	/* Module/type.scm 58 */
																														if (PAIRP
																															(BgL_cdrzd2725zd2_283))
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_carzd2731zd2_286;
																																BgL_carzd2731zd2_286
																																	=
																																	CAR
																																	(BgL_cdrzd2725zd2_283);
																																if (PAIRP
																																	(BgL_carzd2731zd2_286))
																																	{	/* Module/type.scm 58 */
																																		if (NULLP
																																			(CDR
																																				(BgL_cdrzd2725zd2_283)))
																																			{	/* Module/type.scm 58 */
																																				BgL_childz00_222
																																					=
																																					BgL_carzd2714zd2_279;
																																				BgL_namez00_223
																																					=
																																					BgL_carzd2724zd2_282;
																																				BgL_parentz00_224
																																					=
																																					BgL_carzd2731zd2_286;
																																			BgL_tagzd2370zd2_225:
																																				{
																																					obj_t
																																						BgL_walkz00_2116;
																																					obj_t
																																						BgL_classz00_2117;
																																					BgL_walkz00_2116
																																						=
																																						BgL_parentz00_224;
																																					BgL_classz00_2117
																																						=
																																						BUNSPEC;
																																				BgL_zc3z04anonymousza32638ze3z87_2118:
																																					if (NULLP(BgL_walkz00_2116))
																																						{	/* Module/type.scm 78 */
																																							return
																																								(
																																								(obj_t)
																																								BGl_declarezd2subtypez12zc0zztype_envz00
																																								(BgL_childz00_222,
																																									BgL_namez00_223,
																																									BgL_parentz00_224,
																																									BgL_classz00_2117));
																																						}
																																					else
																																						{	/* Module/type.scm 80 */
																																							bool_t
																																								BgL_test2950z00_3910;
																																							{	/* Module/type.scm 80 */
																																								obj_t
																																									BgL_tmpz00_3911;
																																								BgL_tmpz00_3911
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_walkz00_2116));
																																								BgL_test2950z00_3910
																																									=
																																									SYMBOLP
																																									(BgL_tmpz00_3911);
																																							}
																																							if (BgL_test2950z00_3910)
																																								{	/* Module/type.scm 83 */
																																									BgL_typez00_bglt
																																										BgL_tparentz00_2122;
																																									{	/* Module/type.scm 83 */
																																										obj_t
																																											BgL_arg2656z00_2136;
																																										BgL_arg2656z00_2136
																																											=
																																											CAR
																																											(
																																											((obj_t) BgL_walkz00_2116));
																																										BgL_tparentz00_2122
																																											=
																																											BGl_findzd2typezd2zztype_envz00
																																											(BgL_arg2656z00_2136);
																																									}
																																									{	/* Module/type.scm 85 */
																																										bool_t
																																											BgL_test2951z00_3918;
																																										{	/* Module/type.scm 85 */
																																											obj_t
																																												BgL_classz00_2504;
																																											BgL_classz00_2504
																																												=
																																												BGl_typez00zztype_typez00;
																																											{	/* Module/type.scm 85 */
																																												BgL_objectz00_bglt
																																													BgL_arg1807z00_2506;
																																												{	/* Module/type.scm 85 */
																																													obj_t
																																														BgL_tmpz00_3919;
																																													BgL_tmpz00_3919
																																														=
																																														(
																																														(obj_t)
																																														((BgL_objectz00_bglt) BgL_tparentz00_2122));
																																													BgL_arg1807z00_2506
																																														=
																																														(BgL_objectz00_bglt)
																																														(BgL_tmpz00_3919);
																																												}
																																												if (BGL_CONDEXPAND_ISA_ARCH64())
																																													{	/* Module/type.scm 85 */
																																														long
																																															BgL_idxz00_2512;
																																														BgL_idxz00_2512
																																															=
																																															BGL_OBJECT_INHERITANCE_NUM
																																															(BgL_arg1807z00_2506);
																																														BgL_test2951z00_3918
																																															=
																																															(VECTOR_REF
																																															(BGl_za2inheritancesza2z00zz__objectz00,
																																																(BgL_idxz00_2512
																																																	+
																																																	1L))
																																															==
																																															BgL_classz00_2504);
																																													}
																																												else
																																													{	/* Module/type.scm 85 */
																																														bool_t
																																															BgL_res2877z00_2537;
																																														{	/* Module/type.scm 85 */
																																															obj_t
																																																BgL_oclassz00_2520;
																																															{	/* Module/type.scm 85 */
																																																obj_t
																																																	BgL_arg1815z00_2528;
																																																long
																																																	BgL_arg1816z00_2529;
																																																BgL_arg1815z00_2528
																																																	=
																																																	(BGl_za2classesza2z00zz__objectz00);
																																																{	/* Module/type.scm 85 */
																																																	long
																																																		BgL_arg1817z00_2530;
																																																	BgL_arg1817z00_2530
																																																		=
																																																		BGL_OBJECT_CLASS_NUM
																																																		(BgL_arg1807z00_2506);
																																																	BgL_arg1816z00_2529
																																																		=
																																																		(BgL_arg1817z00_2530
																																																		-
																																																		OBJECT_TYPE);
																																																}
																																																BgL_oclassz00_2520
																																																	=
																																																	VECTOR_REF
																																																	(BgL_arg1815z00_2528,
																																																	BgL_arg1816z00_2529);
																																															}
																																															{	/* Module/type.scm 85 */
																																																bool_t
																																																	BgL__ortest_1115z00_2521;
																																																BgL__ortest_1115z00_2521
																																																	=
																																																	(BgL_classz00_2504
																																																	==
																																																	BgL_oclassz00_2520);
																																																if (BgL__ortest_1115z00_2521)
																																																	{	/* Module/type.scm 85 */
																																																		BgL_res2877z00_2537
																																																			=
																																																			BgL__ortest_1115z00_2521;
																																																	}
																																																else
																																																	{	/* Module/type.scm 85 */
																																																		long
																																																			BgL_odepthz00_2522;
																																																		{	/* Module/type.scm 85 */
																																																			obj_t
																																																				BgL_arg1804z00_2523;
																																																			BgL_arg1804z00_2523
																																																				=
																																																				(BgL_oclassz00_2520);
																																																			BgL_odepthz00_2522
																																																				=
																																																				BGL_CLASS_DEPTH
																																																				(BgL_arg1804z00_2523);
																																																		}
																																																		if ((1L < BgL_odepthz00_2522))
																																																			{	/* Module/type.scm 85 */
																																																				obj_t
																																																					BgL_arg1802z00_2525;
																																																				{	/* Module/type.scm 85 */
																																																					obj_t
																																																						BgL_arg1803z00_2526;
																																																					BgL_arg1803z00_2526
																																																						=
																																																						(BgL_oclassz00_2520);
																																																					BgL_arg1802z00_2525
																																																						=
																																																						BGL_CLASS_ANCESTORS_REF
																																																						(BgL_arg1803z00_2526,
																																																						1L);
																																																				}
																																																				BgL_res2877z00_2537
																																																					=
																																																					(BgL_arg1802z00_2525
																																																					==
																																																					BgL_classz00_2504);
																																																			}
																																																		else
																																																			{	/* Module/type.scm 85 */
																																																				BgL_res2877z00_2537
																																																					=
																																																					(
																																																					(bool_t)
																																																					0);
																																																			}
																																																	}
																																															}
																																														}
																																														BgL_test2951z00_3918
																																															=
																																															BgL_res2877z00_2537;
																																													}
																																											}
																																										}
																																										if (BgL_test2951z00_3918)
																																											{	/* Module/type.scm 87 */
																																												bool_t
																																													BgL_test2955z00_3942;
																																												if (SYMBOLP(BgL_classz00_2117))
																																													{	/* Module/type.scm 87 */
																																														if ((BgL_classz00_2117 == (((BgL_typez00_bglt) COBJECT(BgL_tparentz00_2122))->BgL_classz00)))
																																															{	/* Module/type.scm 88 */
																																																BgL_test2955z00_3942
																																																	=
																																																	(
																																																	(bool_t)
																																																	0);
																																															}
																																														else
																																															{	/* Module/type.scm 88 */
																																																BgL_test2955z00_3942
																																																	=
																																																	(
																																																	(bool_t)
																																																	1);
																																															}
																																													}
																																												else
																																													{	/* Module/type.scm 87 */
																																														BgL_test2955z00_3942
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																												if (BgL_test2955z00_3942)
																																													{	/* Module/type.scm 87 */
																																														return
																																															BGl_userzd2errorzd2zztools_errorz00
																																															(BGl_string2891z00zzmodule_typez00,
																																															BGl_string2892z00zzmodule_typez00,
																																															BgL_clausez00_28,
																																															BNIL);
																																													}
																																												else
																																													{	/* Module/type.scm 93 */
																																														obj_t
																																															BgL_arg2650z00_2130;
																																														obj_t
																																															BgL_arg2651z00_2131;
																																														BgL_arg2650z00_2130
																																															=
																																															CDR
																																															(
																																															((obj_t) BgL_walkz00_2116));
																																														BgL_arg2651z00_2131
																																															=
																																															(
																																															((BgL_typez00_bglt) COBJECT(BgL_tparentz00_2122))->BgL_classz00);
																																														{
																																															obj_t
																																																BgL_classz00_3953;
																																															obj_t
																																																BgL_walkz00_3952;
																																															BgL_walkz00_3952
																																																=
																																																BgL_arg2650z00_2130;
																																															BgL_classz00_3953
																																																=
																																																BgL_arg2651z00_2131;
																																															BgL_classz00_2117
																																																=
																																																BgL_classz00_3953;
																																															BgL_walkz00_2116
																																																=
																																																BgL_walkz00_3952;
																																															goto
																																																BgL_zc3z04anonymousza32638ze3z87_2118;
																																														}
																																													}
																																											}
																																										else
																																											{	/* Module/type.scm 85 */
																																												return
																																													BGl_userzd2errorzd2zztools_errorz00
																																													(BGl_string2891z00zzmodule_typez00,
																																													BGl_string2893z00zzmodule_typez00,
																																													BgL_clausez00_28,
																																													BNIL);
																																											}
																																									}
																																								}
																																							else
																																								{	/* Module/type.scm 80 */
																																									return
																																										BGl_userzd2errorzd2zztools_errorz00
																																										(BGl_string2889z00zzmodule_typez00,
																																										BGl_string2894z00zzmodule_typez00,
																																										BgL_clausez00_28,
																																										BNIL);
																																								}
																																						}
																																				}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				obj_t
																																					BgL_cdrzd2751zd2_290;
																																				BgL_cdrzd2751zd2_290
																																					=
																																					CDR((
																																						(obj_t)
																																						BgL_clausez00_28));
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd2757zd2_291;
																																					obj_t
																																						BgL_cdrzd2758zd2_292;
																																					BgL_carzd2757zd2_291
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2751zd2_290));
																																					BgL_cdrzd2758zd2_292
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd2751zd2_290));
																																					if (SYMBOLP(BgL_carzd2757zd2_291))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd2766zd2_294;
																																							obj_t
																																								BgL_cdrzd2767zd2_295;
																																							BgL_carzd2766zd2_294
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2758zd2_292));
																																							BgL_cdrzd2767zd2_295
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd2758zd2_292));
																																							if (STRINGP(BgL_carzd2766zd2_294))
																																								{	/* Module/type.scm 58 */
																																									obj_t
																																										BgL_carzd2773zd2_297;
																																									obj_t
																																										BgL_cdrzd2774zd2_298;
																																									BgL_carzd2773zd2_297
																																										=
																																										CAR
																																										(
																																										((obj_t) BgL_cdrzd2767zd2_295));
																																									BgL_cdrzd2774zd2_298
																																										=
																																										CDR
																																										(
																																										((obj_t) BgL_cdrzd2767zd2_295));
																																									if (PAIRP(BgL_carzd2773zd2_297))
																																										{	/* Module/type.scm 58 */
																																											if (PAIRP(BgL_cdrzd2774zd2_298))
																																												{	/* Module/type.scm 58 */
																																													obj_t
																																														BgL_carzd2780zd2_301;
																																													BgL_carzd2780zd2_301
																																														=
																																														CAR
																																														(BgL_cdrzd2774zd2_298);
																																													if (SYMBOLP(BgL_carzd2780zd2_301))
																																														{	/* Module/type.scm 58 */
																																															if (NULLP(CDR(BgL_cdrzd2774zd2_298)))
																																																{	/* Module/type.scm 58 */
																																																	BgL_childz00_226
																																																		=
																																																		BgL_carzd2757zd2_291;
																																																	BgL_namez00_227
																																																		=
																																																		BgL_carzd2766zd2_294;
																																																	BgL_parentz00_228
																																																		=
																																																		BgL_carzd2773zd2_297;
																																																	BgL_nullzd2valuezd2_229
																																																		=
																																																		BgL_carzd2780zd2_301;
																																																BgL_tagzd2371zd2_230:
																																																	{	/* Module/type.scm 100 */
																																																		obj_t
																																																			BgL_tz00_2140;
																																																		{	/* Module/type.scm 100 */
																																																			obj_t
																																																				BgL_arg2659z00_2142;
																																																			{	/* Module/type.scm 100 */
																																																				obj_t
																																																					BgL_arg2660z00_2143;
																																																				{	/* Module/type.scm 100 */
																																																					obj_t
																																																						BgL_arg2662z00_2144;
																																																					{	/* Module/type.scm 100 */
																																																						obj_t
																																																							BgL_arg2664z00_2145;
																																																						BgL_arg2664z00_2145
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_parentz00_228,
																																																							BNIL);
																																																						BgL_arg2662z00_2144
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_namez00_227,
																																																							BgL_arg2664z00_2145);
																																																					}
																																																					BgL_arg2660z00_2143
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_childz00_226,
																																																						BgL_arg2662z00_2144);
																																																				}
																																																				BgL_arg2659z00_2142
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(CNST_TABLE_REF
																																																					(1),
																																																					BgL_arg2660z00_2143);
																																																			}
																																																			BgL_tz00_2140
																																																				=
																																																				BGl_typezd2parserzd2zzmodule_typez00
																																																				(BgL_importz00_27,
																																																				BgL_arg2659z00_2142,
																																																				BgL_clausesz00_29);
																																																		}
																																																		return
																																																			(
																																																			(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) BgL_tz00_2140)))->BgL_nullz00) = ((obj_t) BgL_nullzd2valuezd2_229), BUNSPEC);
																																																	}
																																																}
																																															else
																																																{	/* Module/type.scm 58 */
																																																	goto
																																																		BgL_tagzd2374zd2_239;
																																																}
																																														}
																																													else
																																														{	/* Module/type.scm 58 */
																																															goto
																																																BgL_tagzd2374zd2_239;
																																														}
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_cdrzd2896zd2_307;
																																		BgL_cdrzd2896zd2_307
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_clausez00_28));
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd2902zd2_308;
																																			obj_t
																																				BgL_cdrzd2903zd2_309;
																																			BgL_carzd2902zd2_308
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd2896zd2_307));
																																			BgL_cdrzd2903zd2_309
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd2896zd2_307));
																																			if (SYMBOLP(BgL_carzd2902zd2_308))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd2911zd2_311;
																																					obj_t
																																						BgL_cdrzd2912zd2_312;
																																					BgL_carzd2911zd2_311
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd2903zd2_309));
																																					BgL_cdrzd2912zd2_312
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd2903zd2_309));
																																					if (STRINGP(BgL_carzd2911zd2_311))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd2918zd2_314;
																																							obj_t
																																								BgL_cdrzd2919zd2_315;
																																							BgL_carzd2918zd2_314
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd2912zd2_312));
																																							BgL_cdrzd2919zd2_315
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd2912zd2_312));
																																							if (PAIRP(BgL_carzd2918zd2_314))
																																								{	/* Module/type.scm 58 */
																																									if (PAIRP(BgL_cdrzd2919zd2_315))
																																										{	/* Module/type.scm 58 */
																																											obj_t
																																												BgL_carzd2925zd2_318;
																																											BgL_carzd2925zd2_318
																																												=
																																												CAR
																																												(BgL_cdrzd2919zd2_315);
																																											if (SYMBOLP(BgL_carzd2925zd2_318))
																																												{	/* Module/type.scm 58 */
																																													if (NULLP(CDR(BgL_cdrzd2919zd2_315)))
																																														{
																																															obj_t
																																																BgL_nullzd2valuezd2_4023;
																																															obj_t
																																																BgL_parentz00_4022;
																																															obj_t
																																																BgL_namez00_4021;
																																															obj_t
																																																BgL_childz00_4020;
																																															BgL_childz00_4020
																																																=
																																																BgL_carzd2902zd2_308;
																																															BgL_namez00_4021
																																																=
																																																BgL_carzd2911zd2_311;
																																															BgL_parentz00_4022
																																																=
																																																BgL_carzd2918zd2_314;
																																															BgL_nullzd2valuezd2_4023
																																																=
																																																BgL_carzd2925zd2_318;
																																															BgL_nullzd2valuezd2_229
																																																=
																																																BgL_nullzd2valuezd2_4023;
																																															BgL_parentz00_228
																																																=
																																																BgL_parentz00_4022;
																																															BgL_namez00_227
																																																=
																																																BgL_namez00_4021;
																																															BgL_childz00_226
																																																=
																																																BgL_childz00_4020;
																																															goto
																																																BgL_tagzd2371zd2_230;
																																														}
																																													else
																																														{	/* Module/type.scm 58 */
																																															goto
																																																BgL_tagzd2374zd2_239;
																																														}
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_cdrzd21070zd2_323;
																														BgL_cdrzd21070zd2_323
																															=
																															CDR(((obj_t)
																																BgL_clausez00_28));
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd21075zd2_324;
																															obj_t
																																BgL_cdrzd21076zd2_325;
																															BgL_carzd21075zd2_324
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21070zd2_323));
																															BgL_cdrzd21076zd2_325
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd21070zd2_323));
																															if (SYMBOLP
																																(BgL_carzd21075zd2_324))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd21083zd2_327;
																																	obj_t
																																		BgL_cdrzd21084zd2_328;
																																	BgL_carzd21083zd2_327
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd21076zd2_325));
																																	BgL_cdrzd21084zd2_328
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd21076zd2_325));
																																	if (STRINGP
																																		(BgL_carzd21083zd2_327))
																																		{	/* Module/type.scm 58 */
																																			if (PAIRP
																																				(BgL_cdrzd21084zd2_328))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd21089zd2_331;
																																					obj_t
																																						BgL_cdrzd21090zd2_332;
																																					BgL_carzd21089zd2_331
																																						=
																																						CAR
																																						(BgL_cdrzd21084zd2_328);
																																					BgL_cdrzd21090zd2_332
																																						=
																																						CDR
																																						(BgL_cdrzd21084zd2_328);
																																					if (PAIRP(BgL_carzd21089zd2_331))
																																						{	/* Module/type.scm 58 */
																																							if (PAIRP(BgL_cdrzd21090zd2_332))
																																								{	/* Module/type.scm 58 */
																																									obj_t
																																										BgL_carzd21094zd2_335;
																																									BgL_carzd21094zd2_335
																																										=
																																										CAR
																																										(BgL_cdrzd21090zd2_332);
																																									if (SYMBOLP(BgL_carzd21094zd2_335))
																																										{	/* Module/type.scm 58 */
																																											if (NULLP(CDR(BgL_cdrzd21090zd2_332)))
																																												{
																																													obj_t
																																														BgL_nullzd2valuezd2_4055;
																																													obj_t
																																														BgL_parentz00_4054;
																																													obj_t
																																														BgL_namez00_4053;
																																													obj_t
																																														BgL_childz00_4052;
																																													BgL_childz00_4052
																																														=
																																														BgL_carzd21075zd2_324;
																																													BgL_namez00_4053
																																														=
																																														BgL_carzd21083zd2_327;
																																													BgL_parentz00_4054
																																														=
																																														BgL_carzd21089zd2_331;
																																													BgL_nullzd2valuezd2_4055
																																														=
																																														BgL_carzd21094zd2_335;
																																													BgL_nullzd2valuezd2_229
																																														=
																																														BgL_nullzd2valuezd2_4055;
																																													BgL_parentz00_228
																																														=
																																														BgL_parentz00_4054;
																																													BgL_namez00_227
																																														=
																																														BgL_namez00_4053;
																																													BgL_childz00_226
																																														=
																																														BgL_childz00_4052;
																																													goto
																																														BgL_tagzd2371zd2_230;
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_cdrzd21227zd2_340;
																												BgL_cdrzd21227zd2_340 =
																													CDR(((obj_t)
																														BgL_clausez00_28));
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd21234zd2_341;
																													obj_t
																														BgL_cdrzd21235zd2_342;
																													BgL_carzd21234zd2_341
																														=
																														CAR(((obj_t)
																															BgL_cdrzd21227zd2_340));
																													BgL_cdrzd21235zd2_342
																														=
																														CDR(((obj_t)
																															BgL_cdrzd21227zd2_340));
																													if (SYMBOLP
																														(BgL_carzd21234zd2_341))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd21245zd2_344;
																															obj_t
																																BgL_cdrzd21246zd2_345;
																															BgL_carzd21245zd2_344
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd21235zd2_342));
																															BgL_cdrzd21246zd2_345
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd21235zd2_342));
																															if (STRINGP
																																(BgL_carzd21245zd2_344))
																																{	/* Module/type.scm 58 */
																																	if (PAIRP
																																		(BgL_cdrzd21246zd2_345))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd21253zd2_348;
																																			obj_t
																																				BgL_cdrzd21254zd2_349;
																																			BgL_carzd21253zd2_348
																																				=
																																				CAR
																																				(BgL_cdrzd21246zd2_345);
																																			BgL_cdrzd21254zd2_349
																																				=
																																				CDR
																																				(BgL_cdrzd21246zd2_345);
																																			if (PAIRP
																																				(BgL_carzd21253zd2_348))
																																				{	/* Module/type.scm 58 */
																																					if (PAIRP(BgL_cdrzd21254zd2_349))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd21258zd2_352;
																																							BgL_carzd21258zd2_352
																																								=
																																								CAR
																																								(BgL_cdrzd21254zd2_349);
																																							if (SYMBOLP(BgL_carzd21258zd2_352))
																																								{	/* Module/type.scm 58 */
																																									if (NULLP(CDR(BgL_cdrzd21254zd2_349)))
																																										{
																																											obj_t
																																												BgL_nullzd2valuezd2_4087;
																																											obj_t
																																												BgL_parentz00_4086;
																																											obj_t
																																												BgL_namez00_4085;
																																											obj_t
																																												BgL_childz00_4084;
																																											BgL_childz00_4084
																																												=
																																												BgL_carzd21234zd2_341;
																																											BgL_namez00_4085
																																												=
																																												BgL_carzd21245zd2_344;
																																											BgL_parentz00_4086
																																												=
																																												BgL_carzd21253zd2_348;
																																											BgL_nullzd2valuezd2_4087
																																												=
																																												BgL_carzd21258zd2_352;
																																											BgL_nullzd2valuezd2_229
																																												=
																																												BgL_nullzd2valuezd2_4087;
																																											BgL_parentz00_228
																																												=
																																												BgL_parentz00_4086;
																																											BgL_namez00_227
																																												=
																																												BgL_namez00_4085;
																																											BgL_childz00_226
																																												=
																																												BgL_childz00_4084;
																																											goto
																																												BgL_tagzd2371zd2_230;
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										obj_t BgL_cdrzd21391zd2_357;

																										BgL_cdrzd21391zd2_357 =
																											CDR(
																											((obj_t)
																												BgL_clausez00_28));
																										if ((CAR(((obj_t)
																														BgL_clausez00_28))
																												== CNST_TABLE_REF(1)))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd21398zd2_360;
																												obj_t
																													BgL_cdrzd21399zd2_361;
																												BgL_carzd21398zd2_360 =
																													CAR(((obj_t)
																														BgL_cdrzd21391zd2_357));
																												BgL_cdrzd21399zd2_361 =
																													CDR(((obj_t)
																														BgL_cdrzd21391zd2_357));
																												if (SYMBOLP
																													(BgL_carzd21398zd2_360))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd21409zd2_363;
																														obj_t
																															BgL_cdrzd21410zd2_364;
																														BgL_carzd21409zd2_363
																															=
																															CAR(((obj_t)
																																BgL_cdrzd21399zd2_361));
																														BgL_cdrzd21410zd2_364
																															=
																															CDR(((obj_t)
																																BgL_cdrzd21399zd2_361));
																														if (STRINGP
																															(BgL_carzd21409zd2_363))
																															{	/* Module/type.scm 58 */
																																if (PAIRP
																																	(BgL_cdrzd21410zd2_364))
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_carzd21417zd2_367;
																																		obj_t
																																			BgL_cdrzd21418zd2_368;
																																		BgL_carzd21417zd2_367
																																			=
																																			CAR
																																			(BgL_cdrzd21410zd2_364);
																																		BgL_cdrzd21418zd2_368
																																			=
																																			CDR
																																			(BgL_cdrzd21410zd2_364);
																																		if (PAIRP
																																			(BgL_carzd21417zd2_367))
																																			{	/* Module/type.scm 58 */
																																				if (PAIRP(BgL_cdrzd21418zd2_368))
																																					{	/* Module/type.scm 58 */
																																						obj_t
																																							BgL_carzd21422zd2_371;
																																						BgL_carzd21422zd2_371
																																							=
																																							CAR
																																							(BgL_cdrzd21418zd2_368);
																																						if (SYMBOLP(BgL_carzd21422zd2_371))
																																							{	/* Module/type.scm 58 */
																																								if (NULLP(CDR(BgL_cdrzd21418zd2_368)))
																																									{
																																										obj_t
																																											BgL_nullzd2valuezd2_4124;
																																										obj_t
																																											BgL_parentz00_4123;
																																										obj_t
																																											BgL_namez00_4122;
																																										obj_t
																																											BgL_childz00_4121;
																																										BgL_childz00_4121
																																											=
																																											BgL_carzd21398zd2_360;
																																										BgL_namez00_4122
																																											=
																																											BgL_carzd21409zd2_363;
																																										BgL_parentz00_4123
																																											=
																																											BgL_carzd21417zd2_367;
																																										BgL_nullzd2valuezd2_4124
																																											=
																																											BgL_carzd21422zd2_371;
																																										BgL_nullzd2valuezd2_229
																																											=
																																											BgL_nullzd2valuezd2_4124;
																																										BgL_parentz00_228
																																											=
																																											BgL_parentz00_4123;
																																										BgL_namez00_227
																																											=
																																											BgL_namez00_4122;
																																										BgL_childz00_226
																																											=
																																											BgL_childz00_4121;
																																										goto
																																											BgL_tagzd2371zd2_230;
																																									}
																																								else
																																									{	/* Module/type.scm 58 */
																																										goto
																																											BgL_tagzd2374zd2_239;
																																									}
																																							}
																																						else
																																							{	/* Module/type.scm 58 */
																																								goto
																																									BgL_tagzd2374zd2_239;
																																							}
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												if (
																													(CAR(
																															((obj_t)
																																BgL_clausez00_28))
																														==
																														CNST_TABLE_REF(6)))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd21565zd2_379;
																														obj_t
																															BgL_cdrzd21566zd2_380;
																														BgL_carzd21565zd2_379
																															=
																															CAR(((obj_t)
																																BgL_cdrzd21391zd2_357));
																														BgL_cdrzd21566zd2_380
																															=
																															CDR(((obj_t)
																																BgL_cdrzd21391zd2_357));
																														if (SYMBOLP
																															(BgL_carzd21565zd2_379))
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_carzd21576zd2_382;
																																obj_t
																																	BgL_cdrzd21577zd2_383;
																																BgL_carzd21576zd2_382
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd21566zd2_380));
																																BgL_cdrzd21577zd2_383
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd21566zd2_380));
																																if (SYMBOLP
																																	(BgL_carzd21576zd2_382))
																																	{	/* Module/type.scm 58 */
																																		if (PAIRP
																																			(BgL_cdrzd21577zd2_383))
																																			{	/* Module/type.scm 58 */
																																				obj_t
																																					BgL_cdrzd21586zd2_386;
																																				BgL_cdrzd21586zd2_386
																																					=
																																					CDR
																																					(BgL_cdrzd21577zd2_383);
																																				if (PAIRP(BgL_cdrzd21586zd2_386))
																																					{	/* Module/type.scm 58 */
																																						if (NULLP(CDR(BgL_cdrzd21586zd2_386)))
																																							{	/* Module/type.scm 58 */
																																								BgL_fromz00_234
																																									=
																																									BgL_carzd21565zd2_379;
																																								BgL_toz00_235
																																									=
																																									BgL_carzd21576zd2_382;
																																								BgL_checkz00_236
																																									=
																																									CAR
																																									(BgL_cdrzd21577zd2_383);
																																								BgL_coercez00_237
																																									=
																																									CAR
																																									(BgL_cdrzd21586zd2_386);
																																							BgL_tagzd2373zd2_238:
																																								{	/* Module/type.scm 106 */
																																									bool_t
																																										BgL_test2998z00_4150;
																																									{	/* Module/type.scm 106 */
																																										bool_t
																																											BgL_test2999z00_4151;
																																										{
																																											obj_t
																																												BgL_checkz00_2382;
																																											{	/* Module/type.scm 106 */
																																												obj_t
																																													BgL_tmpz00_4152;
																																												BgL_checkz00_2382
																																													=
																																													BgL_checkz00_236;
																																											BgL_zc3z04anonymousza32800ze3z87_2383:
																																												if (NULLP(BgL_checkz00_2382))
																																													{	/* Module/type.scm 108 */
																																														BgL_tmpz00_4152
																																															=
																																															BTRUE;
																																													}
																																												else
																																													{	/* Module/type.scm 110 */
																																														bool_t
																																															BgL_test3001z00_4155;
																																														{	/* Module/type.scm 110 */
																																															obj_t
																																																BgL_tmpz00_4156;
																																															BgL_tmpz00_4156
																																																=
																																																CAR
																																																(
																																																((obj_t) BgL_checkz00_2382));
																																															BgL_test3001z00_4155
																																																=
																																																SYMBOLP
																																																(BgL_tmpz00_4156);
																																														}
																																														if (BgL_test3001z00_4155)
																																															{	/* Module/type.scm 121 */
																																																obj_t
																																																	BgL_arg2804z00_2386;
																																																BgL_arg2804z00_2386
																																																	=
																																																	CDR
																																																	(
																																																	((obj_t) BgL_checkz00_2382));
																																																{
																																																	obj_t
																																																		BgL_checkz00_4162;
																																																	BgL_checkz00_4162
																																																		=
																																																		BgL_arg2804z00_2386;
																																																	BgL_checkz00_2382
																																																		=
																																																		BgL_checkz00_4162;
																																																	goto
																																																		BgL_zc3z04anonymousza32800ze3z87_2383;
																																																}
																																															}
																																														else
																																															{

																																																{	/* Module/type.scm 111 */
																																																	obj_t
																																																		BgL_ezd216047zd2_2390;
																																																	BgL_ezd216047zd2_2390
																																																		=
																																																		CAR
																																																		(
																																																		((obj_t) BgL_checkz00_2382));
																																																	if (PAIRP(BgL_ezd216047zd2_2390))
																																																		{	/* Module/type.scm 111 */
																																																			obj_t
																																																				BgL_cdrzd216049zd2_2392;
																																																			BgL_cdrzd216049zd2_2392
																																																				=
																																																				CDR
																																																				(BgL_ezd216047zd2_2390);
																																																			if ((CAR(BgL_ezd216047zd2_2390) == CNST_TABLE_REF(2)))
																																																				{	/* Module/type.scm 111 */
																																																					if (PAIRP(BgL_cdrzd216049zd2_2392))
																																																						{	/* Module/type.scm 111 */
																																																							obj_t
																																																								BgL_carzd216050zd2_2395;
																																																							obj_t
																																																								BgL_cdrzd216051zd2_2396;
																																																							BgL_carzd216050zd2_2395
																																																								=
																																																								CAR
																																																								(BgL_cdrzd216049zd2_2392);
																																																							BgL_cdrzd216051zd2_2396
																																																								=
																																																								CDR
																																																								(BgL_cdrzd216049zd2_2392);
																																																							if (PAIRP(BgL_carzd216050zd2_2395))
																																																								{	/* Module/type.scm 111 */
																																																									if (NULLP(CDR(BgL_carzd216050zd2_2395)))
																																																										{	/* Module/type.scm 111 */
																																																											if (PAIRP(BgL_cdrzd216051zd2_2396))
																																																												{	/* Module/type.scm 111 */
																																																													if (NULLP(CDR(BgL_cdrzd216051zd2_2396)))
																																																														{	/* Module/type.scm 111 */
																																																															{	/* Module/type.scm 113 */
																																																																obj_t
																																																																	BgL_arg2818z00_2416;
																																																																BgL_arg2818z00_2416
																																																																	=
																																																																	CDR
																																																																	(
																																																																	((obj_t) BgL_checkz00_2382));
																																																																{
																																																																	obj_t
																																																																		BgL_checkz00_4188;
																																																																	BgL_checkz00_4188
																																																																		=
																																																																		BgL_arg2818z00_2416;
																																																																	BgL_checkz00_2382
																																																																		=
																																																																		BgL_checkz00_4188;
																																																																	goto
																																																																		BgL_zc3z04anonymousza32800ze3z87_2383;
																																																																}
																																																															}
																																																														}
																																																													else
																																																														{	/* Module/type.scm 111 */
																																																														BgL_tagzd216046zd2_2389:
																																																															{	/* Module/type.scm 117 */
																																																																obj_t
																																																																	BgL_list2821z00_2418;
																																																																BgL_list2821z00_2418
																																																																	=
																																																																	MAKE_YOUNG_PAIR
																																																																	(BFALSE,
																																																																	BNIL);
																																																																BgL_tmpz00_4152
																																																																	=
																																																																	BGl_userzd2errorzd2zztools_errorz00
																																																																	(BGl_string2895z00zzmodule_typez00,
																																																																	BGl_string2896z00zzmodule_typez00,
																																																																	BgL_clausez00_28,
																																																																	BgL_list2821z00_2418);
																																																															}
																																																														}
																																																												}
																																																											else
																																																												{	/* Module/type.scm 111 */
																																																													goto
																																																														BgL_tagzd216046zd2_2389;
																																																												}
																																																										}
																																																									else
																																																										{	/* Module/type.scm 111 */
																																																											goto
																																																												BgL_tagzd216046zd2_2389;
																																																										}
																																																								}
																																																							else
																																																								{	/* Module/type.scm 111 */
																																																									goto
																																																										BgL_tagzd216046zd2_2389;
																																																								}
																																																						}
																																																					else
																																																						{	/* Module/type.scm 111 */
																																																							goto
																																																								BgL_tagzd216046zd2_2389;
																																																						}
																																																				}
																																																			else
																																																				{	/* Module/type.scm 111 */
																																																					if ((CAR(BgL_ezd216047zd2_2390) == CNST_TABLE_REF(3)))
																																																						{	/* Module/type.scm 111 */
																																																							if (PAIRP(BgL_cdrzd216049zd2_2392))
																																																								{	/* Module/type.scm 111 */
																																																									obj_t
																																																										BgL_cdrzd216059zd2_2406;
																																																									BgL_cdrzd216059zd2_2406
																																																										=
																																																										CDR
																																																										(BgL_cdrzd216049zd2_2392);
																																																									{	/* Module/type.scm 111 */
																																																										bool_t
																																																											BgL_test3011z00_4198;
																																																										{	/* Module/type.scm 111 */
																																																											obj_t
																																																												BgL_tmpz00_4199;
																																																											BgL_tmpz00_4199
																																																												=
																																																												CAR
																																																												(BgL_cdrzd216049zd2_2392);
																																																											BgL_test3011z00_4198
																																																												=
																																																												SYMBOLP
																																																												(BgL_tmpz00_4199);
																																																										}
																																																										if (BgL_test3011z00_4198)
																																																											{	/* Module/type.scm 111 */
																																																												if (PAIRP(BgL_cdrzd216059zd2_2406))
																																																													{	/* Module/type.scm 111 */
																																																														bool_t
																																																															BgL_test3013z00_4204;
																																																														{	/* Module/type.scm 111 */
																																																															obj_t
																																																																BgL_tmpz00_4205;
																																																															BgL_tmpz00_4205
																																																																=
																																																																CAR
																																																																(BgL_cdrzd216059zd2_2406);
																																																															BgL_test3013z00_4204
																																																																=
																																																																SYMBOLP
																																																																(BgL_tmpz00_4205);
																																																														}
																																																														if (BgL_test3013z00_4204)
																																																															{	/* Module/type.scm 111 */
																																																																if (NULLP(CDR(BgL_cdrzd216059zd2_2406)))
																																																																	{	/* Module/type.scm 111 */
																																																																		{	/* Module/type.scm 115 */
																																																																			obj_t
																																																																				BgL_arg2820z00_2417;
																																																																			BgL_arg2820z00_2417
																																																																				=
																																																																				CDR
																																																																				(
																																																																				((obj_t) BgL_checkz00_2382));
																																																																			{
																																																																				obj_t
																																																																					BgL_checkz00_4213;
																																																																				BgL_checkz00_4213
																																																																					=
																																																																					BgL_arg2820z00_2417;
																																																																				BgL_checkz00_2382
																																																																					=
																																																																					BgL_checkz00_4213;
																																																																				goto
																																																																					BgL_zc3z04anonymousza32800ze3z87_2383;
																																																																			}
																																																																		}
																																																																	}
																																																																else
																																																																	{	/* Module/type.scm 111 */
																																																																		goto
																																																																			BgL_tagzd216046zd2_2389;
																																																																	}
																																																															}
																																																														else
																																																															{	/* Module/type.scm 111 */
																																																																goto
																																																																	BgL_tagzd216046zd2_2389;
																																																															}
																																																													}
																																																												else
																																																													{	/* Module/type.scm 111 */
																																																														goto
																																																															BgL_tagzd216046zd2_2389;
																																																													}
																																																											}
																																																										else
																																																											{	/* Module/type.scm 111 */
																																																												goto
																																																													BgL_tagzd216046zd2_2389;
																																																											}
																																																									}
																																																								}
																																																							else
																																																								{	/* Module/type.scm 111 */
																																																									goto
																																																										BgL_tagzd216046zd2_2389;
																																																								}
																																																						}
																																																					else
																																																						{	/* Module/type.scm 111 */
																																																							goto
																																																								BgL_tagzd216046zd2_2389;
																																																						}
																																																				}
																																																		}
																																																	else
																																																		{	/* Module/type.scm 111 */
																																																			goto
																																																				BgL_tagzd216046zd2_2389;
																																																		}
																																																}
																																															}
																																													}
																																												BgL_test2999z00_4151
																																													=
																																													CBOOL
																																													(BgL_tmpz00_4152);
																																											}
																																										}
																																										if (BgL_test2999z00_4151)
																																											{
																																												obj_t
																																													BgL_coercez00_2347;
																																												{	/* Module/type.scm 122 */
																																													obj_t
																																														BgL_tmpz00_4215;
																																													BgL_coercez00_2347
																																														=
																																														BgL_coercez00_237;
																																												BgL_zc3z04anonymousza32782ze3z87_2348:
																																													if (NULLP(BgL_coercez00_2347))
																																														{	/* Module/type.scm 124 */
																																															BgL_tmpz00_4215
																																																=
																																																BTRUE;
																																														}
																																													else
																																														{	/* Module/type.scm 126 */
																																															bool_t
																																																BgL_test3016z00_4218;
																																															{	/* Module/type.scm 126 */
																																																obj_t
																																																	BgL_ezd216066zd2_2357;
																																																BgL_ezd216066zd2_2357
																																																	=
																																																	CAR
																																																	(
																																																	((obj_t) BgL_coercez00_2347));
																																																if (SYMBOLP(BgL_ezd216066zd2_2357))
																																																	{	/* Module/type.scm 126 */
																																																		BgL_test3016z00_4218
																																																			=
																																																			(
																																																			(bool_t)
																																																			0);
																																																	}
																																																else
																																																	{	/* Module/type.scm 126 */
																																																		if (PAIRP(BgL_ezd216066zd2_2357))
																																																			{	/* Module/type.scm 126 */
																																																				obj_t
																																																					BgL_cdrzd216068zd2_2360;
																																																				BgL_cdrzd216068zd2_2360
																																																					=
																																																					CDR
																																																					(BgL_ezd216066zd2_2357);
																																																				if ((CAR(BgL_ezd216066zd2_2357) == CNST_TABLE_REF(3)))
																																																					{	/* Module/type.scm 126 */
																																																						if (PAIRP(BgL_cdrzd216068zd2_2360))
																																																							{	/* Module/type.scm 126 */
																																																								obj_t
																																																									BgL_cdrzd216070zd2_2363;
																																																								BgL_cdrzd216070zd2_2363
																																																									=
																																																									CDR
																																																									(BgL_cdrzd216068zd2_2360);
																																																								{	/* Module/type.scm 126 */
																																																									bool_t
																																																										BgL_test3021z00_4233;
																																																									{	/* Module/type.scm 126 */
																																																										obj_t
																																																											BgL_tmpz00_4234;
																																																										BgL_tmpz00_4234
																																																											=
																																																											CAR
																																																											(BgL_cdrzd216068zd2_2360);
																																																										BgL_test3021z00_4233
																																																											=
																																																											SYMBOLP
																																																											(BgL_tmpz00_4234);
																																																									}
																																																									if (BgL_test3021z00_4233)
																																																										{	/* Module/type.scm 126 */
																																																											if (PAIRP(BgL_cdrzd216070zd2_2363))
																																																												{	/* Module/type.scm 126 */
																																																													bool_t
																																																														BgL_test3023z00_4239;
																																																													{	/* Module/type.scm 126 */
																																																														obj_t
																																																															BgL_tmpz00_4240;
																																																														BgL_tmpz00_4240
																																																															=
																																																															CAR
																																																															(BgL_cdrzd216070zd2_2363);
																																																														BgL_test3023z00_4239
																																																															=
																																																															SYMBOLP
																																																															(BgL_tmpz00_4240);
																																																													}
																																																													if (BgL_test3023z00_4239)
																																																														{	/* Module/type.scm 126 */
																																																															if (NULLP(CDR(BgL_cdrzd216070zd2_2363)))
																																																																{	/* Module/type.scm 126 */
																																																																	BgL_test3016z00_4218
																																																																		=
																																																																		(
																																																																		(bool_t)
																																																																		0);
																																																																}
																																																															else
																																																																{	/* Module/type.scm 126 */
																																																																	BgL_test3016z00_4218
																																																																		=
																																																																		(
																																																																		(bool_t)
																																																																		1);
																																																																}
																																																														}
																																																													else
																																																														{	/* Module/type.scm 126 */
																																																															BgL_test3016z00_4218
																																																																=
																																																																(
																																																																(bool_t)
																																																																1);
																																																														}
																																																												}
																																																											else
																																																												{	/* Module/type.scm 126 */
																																																													BgL_test3016z00_4218
																																																														=
																																																														(
																																																														(bool_t)
																																																														1);
																																																												}
																																																										}
																																																									else
																																																										{	/* Module/type.scm 126 */
																																																											BgL_test3016z00_4218
																																																												=
																																																												(
																																																												(bool_t)
																																																												1);
																																																										}
																																																								}
																																																							}
																																																						else
																																																							{	/* Module/type.scm 126 */
																																																								BgL_test3016z00_4218
																																																									=
																																																									(
																																																									(bool_t)
																																																									1);
																																																							}
																																																					}
																																																				else
																																																					{	/* Module/type.scm 126 */
																																																						if ((CAR(BgL_ezd216066zd2_2357) == CNST_TABLE_REF(2)))
																																																							{	/* Module/type.scm 126 */
																																																								if (PAIRP(BgL_cdrzd216068zd2_2360))
																																																									{	/* Module/type.scm 126 */
																																																										obj_t
																																																											BgL_carzd216075zd2_2374;
																																																										BgL_carzd216075zd2_2374
																																																											=
																																																											CAR
																																																											(BgL_cdrzd216068zd2_2360);
																																																										if (PAIRP(BgL_carzd216075zd2_2374))
																																																											{	/* Module/type.scm 126 */
																																																												if (NULLP(CDR(BgL_carzd216075zd2_2374)))
																																																													{	/* Module/type.scm 126 */
																																																														BgL_test3016z00_4218
																																																															=
																																																															(
																																																															(bool_t)
																																																															0);
																																																													}
																																																												else
																																																													{	/* Module/type.scm 126 */
																																																														BgL_test3016z00_4218
																																																															=
																																																															(
																																																															(bool_t)
																																																															1);
																																																													}
																																																											}
																																																										else
																																																											{	/* Module/type.scm 126 */
																																																												BgL_test3016z00_4218
																																																													=
																																																													(
																																																													(bool_t)
																																																													1);
																																																											}
																																																									}
																																																								else
																																																									{	/* Module/type.scm 126 */
																																																										BgL_test3016z00_4218
																																																											=
																																																											(
																																																											(bool_t)
																																																											1);
																																																									}
																																																							}
																																																						else
																																																							{	/* Module/type.scm 126 */
																																																								BgL_test3016z00_4218
																																																									=
																																																									(
																																																									(bool_t)
																																																									1);
																																																							}
																																																					}
																																																			}
																																																		else
																																																			{	/* Module/type.scm 126 */
																																																				BgL_test3016z00_4218
																																																					=
																																																					(
																																																					(bool_t)
																																																					1);
																																																			}
																																																	}
																																															}
																																															if (BgL_test3016z00_4218)
																																																{	/* Module/type.scm 131 */
																																																	obj_t
																																																		BgL_list2783z00_2351;
																																																	BgL_list2783z00_2351
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BFALSE,
																																																		BNIL);
																																																	BgL_tmpz00_4215
																																																		=
																																																		BGl_userzd2errorzd2zztools_errorz00
																																																		(BGl_string2895z00zzmodule_typez00,
																																																		BGl_string2897z00zzmodule_typez00,
																																																		BgL_clausez00_28,
																																																		BgL_list2783z00_2351);
																																																}
																																															else
																																																{	/* Module/type.scm 133 */
																																																	obj_t
																																																		BgL_arg2784z00_2352;
																																																	BgL_arg2784z00_2352
																																																		=
																																																		CDR
																																																		(
																																																		((obj_t) BgL_coercez00_2347));
																																																	{
																																																		obj_t
																																																			BgL_coercez00_4262;
																																																		BgL_coercez00_4262
																																																			=
																																																			BgL_arg2784z00_2352;
																																																		BgL_coercez00_2347
																																																			=
																																																			BgL_coercez00_4262;
																																																		goto
																																																			BgL_zc3z04anonymousza32782ze3z87_2348;
																																																	}
																																																}
																																														}
																																													BgL_test2998z00_4150
																																														=
																																														CBOOL
																																														(BgL_tmpz00_4215);
																																												}
																																											}
																																										else
																																											{	/* Module/type.scm 106 */
																																												BgL_test2998z00_4150
																																													=
																																													(
																																													(bool_t)
																																													0);
																																											}
																																									}
																																									if (BgL_test2998z00_4150)
																																										{	/* Module/type.scm 134 */
																																											obj_t
																																												BgL_locz00_2304;
																																											BgL_locz00_2304
																																												=
																																												BGl_findzd2locationzd2zztools_locationz00
																																												(BgL_clausez00_28);
																																											{	/* Module/type.scm 134 */
																																												BgL_typez00_bglt
																																													BgL_tfromz00_2305;
																																												BgL_tfromz00_2305
																																													=
																																													BGl_usezd2typez12zc0zztype_envz00
																																													(BgL_fromz00_234,
																																													BgL_locz00_2304);
																																												{	/* Module/type.scm 135 */
																																													BgL_typez00_bglt
																																														BgL_ttoz00_2306;
																																													BgL_ttoz00_2306
																																														=
																																														BGl_usezd2typez12zc0zztype_envz00
																																														(BgL_toz00_235,
																																														BgL_locz00_2304);
																																													{	/* Module/type.scm 136 */
																																														obj_t
																																															BgL_checksz00_2307;
																																														if (NULLP(BgL_checkz00_236))
																																															{	/* Module/type.scm 138 */
																																																obj_t
																																																	BgL_arg2774z00_2330;
																																																BgL_arg2774z00_2330
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BTRUE,
																																																	((obj_t) BgL_tfromz00_2305));
																																																{	/* Module/type.scm 138 */
																																																	obj_t
																																																		BgL_list2775z00_2331;
																																																	BgL_list2775z00_2331
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BgL_arg2774z00_2330,
																																																		BNIL);
																																																	BgL_checksz00_2307
																																																		=
																																																		BgL_list2775z00_2331;
																																																}
																																															}
																																														else
																																															{	/* Module/type.scm 139 */
																																																obj_t
																																																	BgL_head1070z00_2334;
																																																BgL_head1070z00_2334
																																																	=
																																																	MAKE_YOUNG_PAIR
																																																	(BNIL,
																																																	BNIL);
																																																{
																																																	obj_t
																																																		BgL_l1068z00_2336;
																																																	obj_t
																																																		BgL_tail1071z00_2337;
																																																	BgL_l1068z00_2336
																																																		=
																																																		BgL_checkz00_236;
																																																	BgL_tail1071z00_2337
																																																		=
																																																		BgL_head1070z00_2334;
																																																BgL_zc3z04anonymousza32777ze3z87_2338:
																																																	if (NULLP(BgL_l1068z00_2336))
																																																		{	/* Module/type.scm 139 */
																																																			BgL_checksz00_2307
																																																				=
																																																				CDR
																																																				(BgL_head1070z00_2334);
																																																		}
																																																	else
																																																		{	/* Module/type.scm 139 */
																																																			obj_t
																																																				BgL_newtail1072z00_2340;
																																																			{	/* Module/type.scm 139 */
																																																				obj_t
																																																					BgL_arg2781z00_2342;
																																																				{	/* Module/type.scm 139 */
																																																					obj_t
																																																						BgL_cz00_2343;
																																																					BgL_cz00_2343
																																																						=
																																																						CAR
																																																						(
																																																						((obj_t) BgL_l1068z00_2336));
																																																					BgL_arg2781z00_2342
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_cz00_2343,
																																																						((obj_t) BgL_tfromz00_2305));
																																																				}
																																																				BgL_newtail1072z00_2340
																																																					=
																																																					MAKE_YOUNG_PAIR
																																																					(BgL_arg2781z00_2342,
																																																					BNIL);
																																																			}
																																																			SET_CDR
																																																				(BgL_tail1071z00_2337,
																																																				BgL_newtail1072z00_2340);
																																																			{	/* Module/type.scm 139 */
																																																				obj_t
																																																					BgL_arg2780z00_2341;
																																																				BgL_arg2780z00_2341
																																																					=
																																																					CDR
																																																					(
																																																					((obj_t) BgL_l1068z00_2336));
																																																				{
																																																					obj_t
																																																						BgL_tail1071z00_4285;
																																																					obj_t
																																																						BgL_l1068z00_4284;
																																																					BgL_l1068z00_4284
																																																						=
																																																						BgL_arg2780z00_2341;
																																																					BgL_tail1071z00_4285
																																																						=
																																																						BgL_newtail1072z00_2340;
																																																					BgL_tail1071z00_2337
																																																						=
																																																						BgL_tail1071z00_4285;
																																																					BgL_l1068z00_2336
																																																						=
																																																						BgL_l1068z00_4284;
																																																					goto
																																																						BgL_zc3z04anonymousza32777ze3z87_2338;
																																																				}
																																																			}
																																																		}
																																																}
																																															}
																																														{	/* Module/type.scm 137 */
																																															obj_t
																																																BgL_coercesz00_2308;
																																															if (NULLP(BgL_coercez00_237))
																																																{	/* Module/type.scm 141 */
																																																	obj_t
																																																		BgL_arg2764z00_2314;
																																																	BgL_arg2764z00_2314
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BTRUE,
																																																		((obj_t) BgL_ttoz00_2306));
																																																	{	/* Module/type.scm 141 */
																																																		obj_t
																																																			BgL_list2765z00_2315;
																																																		BgL_list2765z00_2315
																																																			=
																																																			MAKE_YOUNG_PAIR
																																																			(BgL_arg2764z00_2314,
																																																			BNIL);
																																																		BgL_coercesz00_2308
																																																			=
																																																			BgL_list2765z00_2315;
																																																	}
																																																}
																																															else
																																																{	/* Module/type.scm 142 */
																																																	obj_t
																																																		BgL_head1075z00_2318;
																																																	BgL_head1075z00_2318
																																																		=
																																																		MAKE_YOUNG_PAIR
																																																		(BNIL,
																																																		BNIL);
																																																	{
																																																		obj_t
																																																			BgL_l1073z00_2320;
																																																		obj_t
																																																			BgL_tail1076z00_2321;
																																																		BgL_l1073z00_2320
																																																			=
																																																			BgL_coercez00_237;
																																																		BgL_tail1076z00_2321
																																																			=
																																																			BgL_head1075z00_2318;
																																																	BgL_zc3z04anonymousza32767ze3z87_2322:
																																																		if (NULLP(BgL_l1073z00_2320))
																																																			{	/* Module/type.scm 142 */
																																																				BgL_coercesz00_2308
																																																					=
																																																					CDR
																																																					(BgL_head1075z00_2318);
																																																			}
																																																		else
																																																			{	/* Module/type.scm 142 */
																																																				obj_t
																																																					BgL_newtail1077z00_2324;
																																																				{	/* Module/type.scm 142 */
																																																					obj_t
																																																						BgL_arg2772z00_2326;
																																																					{	/* Module/type.scm 142 */
																																																						obj_t
																																																							BgL_cz00_2327;
																																																						BgL_cz00_2327
																																																							=
																																																							CAR
																																																							(
																																																							((obj_t) BgL_l1073z00_2320));
																																																						BgL_arg2772z00_2326
																																																							=
																																																							MAKE_YOUNG_PAIR
																																																							(BgL_cz00_2327,
																																																							((obj_t) BgL_ttoz00_2306));
																																																					}
																																																					BgL_newtail1077z00_2324
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BgL_arg2772z00_2326,
																																																						BNIL);
																																																				}
																																																				SET_CDR
																																																					(BgL_tail1076z00_2321,
																																																					BgL_newtail1077z00_2324);
																																																				{	/* Module/type.scm 142 */
																																																					obj_t
																																																						BgL_arg2771z00_2325;
																																																					BgL_arg2771z00_2325
																																																						=
																																																						CDR
																																																						(
																																																						((obj_t) BgL_l1073z00_2320));
																																																					{
																																																						obj_t
																																																							BgL_tail1076z00_4304;
																																																						obj_t
																																																							BgL_l1073z00_4303;
																																																						BgL_l1073z00_4303
																																																							=
																																																							BgL_arg2771z00_2325;
																																																						BgL_tail1076z00_4304
																																																							=
																																																							BgL_newtail1077z00_2324;
																																																						BgL_tail1076z00_2321
																																																							=
																																																							BgL_tail1076z00_4304;
																																																						BgL_l1073z00_2320
																																																							=
																																																							BgL_l1073z00_4303;
																																																						goto
																																																							BgL_zc3z04anonymousza32767ze3z87_2322;
																																																					}
																																																				}
																																																			}
																																																	}
																																																}
																																															{	/* Module/type.scm 140 */

																																																{	/* Module/type.scm 144 */
																																																	bool_t
																																																		BgL_test3033z00_4305;
																																																	{	/* Module/type.scm 144 */
																																																		obj_t
																																																			BgL_classz00_2586;
																																																		BgL_classz00_2586
																																																			=
																																																			BGl_typez00zztype_typez00;
																																																		{	/* Module/type.scm 144 */
																																																			BgL_objectz00_bglt
																																																				BgL_arg1807z00_2588;
																																																			{	/* Module/type.scm 144 */
																																																				obj_t
																																																					BgL_tmpz00_4306;
																																																				BgL_tmpz00_4306
																																																					=
																																																					(
																																																					(obj_t)
																																																					((BgL_objectz00_bglt) BgL_tfromz00_2305));
																																																				BgL_arg1807z00_2588
																																																					=
																																																					(BgL_objectz00_bglt)
																																																					(BgL_tmpz00_4306);
																																																			}
																																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																																				{	/* Module/type.scm 144 */
																																																					long
																																																						BgL_idxz00_2594;
																																																					BgL_idxz00_2594
																																																						=
																																																						BGL_OBJECT_INHERITANCE_NUM
																																																						(BgL_arg1807z00_2588);
																																																					BgL_test3033z00_4305
																																																						=
																																																						(VECTOR_REF
																																																						(BGl_za2inheritancesza2z00zz__objectz00,
																																																							(BgL_idxz00_2594
																																																								+
																																																								1L))
																																																						==
																																																						BgL_classz00_2586);
																																																				}
																																																			else
																																																				{	/* Module/type.scm 144 */
																																																					bool_t
																																																						BgL_res2881z00_2619;
																																																					{	/* Module/type.scm 144 */
																																																						obj_t
																																																							BgL_oclassz00_2602;
																																																						{	/* Module/type.scm 144 */
																																																							obj_t
																																																								BgL_arg1815z00_2610;
																																																							long
																																																								BgL_arg1816z00_2611;
																																																							BgL_arg1815z00_2610
																																																								=
																																																								(BGl_za2classesza2z00zz__objectz00);
																																																							{	/* Module/type.scm 144 */
																																																								long
																																																									BgL_arg1817z00_2612;
																																																								BgL_arg1817z00_2612
																																																									=
																																																									BGL_OBJECT_CLASS_NUM
																																																									(BgL_arg1807z00_2588);
																																																								BgL_arg1816z00_2611
																																																									=
																																																									(BgL_arg1817z00_2612
																																																									-
																																																									OBJECT_TYPE);
																																																							}
																																																							BgL_oclassz00_2602
																																																								=
																																																								VECTOR_REF
																																																								(BgL_arg1815z00_2610,
																																																								BgL_arg1816z00_2611);
																																																						}
																																																						{	/* Module/type.scm 144 */
																																																							bool_t
																																																								BgL__ortest_1115z00_2603;
																																																							BgL__ortest_1115z00_2603
																																																								=
																																																								(BgL_classz00_2586
																																																								==
																																																								BgL_oclassz00_2602);
																																																							if (BgL__ortest_1115z00_2603)
																																																								{	/* Module/type.scm 144 */
																																																									BgL_res2881z00_2619
																																																										=
																																																										BgL__ortest_1115z00_2603;
																																																								}
																																																							else
																																																								{	/* Module/type.scm 144 */
																																																									long
																																																										BgL_odepthz00_2604;
																																																									{	/* Module/type.scm 144 */
																																																										obj_t
																																																											BgL_arg1804z00_2605;
																																																										BgL_arg1804z00_2605
																																																											=
																																																											(BgL_oclassz00_2602);
																																																										BgL_odepthz00_2604
																																																											=
																																																											BGL_CLASS_DEPTH
																																																											(BgL_arg1804z00_2605);
																																																									}
																																																									if ((1L < BgL_odepthz00_2604))
																																																										{	/* Module/type.scm 144 */
																																																											obj_t
																																																												BgL_arg1802z00_2607;
																																																											{	/* Module/type.scm 144 */
																																																												obj_t
																																																													BgL_arg1803z00_2608;
																																																												BgL_arg1803z00_2608
																																																													=
																																																													(BgL_oclassz00_2602);
																																																												BgL_arg1802z00_2607
																																																													=
																																																													BGL_CLASS_ANCESTORS_REF
																																																													(BgL_arg1803z00_2608,
																																																													1L);
																																																											}
																																																											BgL_res2881z00_2619
																																																												=
																																																												(BgL_arg1802z00_2607
																																																												==
																																																												BgL_classz00_2586);
																																																										}
																																																									else
																																																										{	/* Module/type.scm 144 */
																																																											BgL_res2881z00_2619
																																																												=
																																																												(
																																																												(bool_t)
																																																												0);
																																																										}
																																																								}
																																																						}
																																																					}
																																																					BgL_test3033z00_4305
																																																						=
																																																						BgL_res2881z00_2619;
																																																				}
																																																		}
																																																	}
																																																	if (BgL_test3033z00_4305)
																																																		{	/* Module/type.scm 146 */
																																																			bool_t
																																																				BgL_test3037z00_4329;
																																																			{	/* Module/type.scm 146 */
																																																				obj_t
																																																					BgL_classz00_2620;
																																																				BgL_classz00_2620
																																																					=
																																																					BGl_typez00zztype_typez00;
																																																				{	/* Module/type.scm 146 */
																																																					BgL_objectz00_bglt
																																																						BgL_arg1807z00_2622;
																																																					{	/* Module/type.scm 146 */
																																																						obj_t
																																																							BgL_tmpz00_4330;
																																																						BgL_tmpz00_4330
																																																							=
																																																							(
																																																							(obj_t)
																																																							((BgL_objectz00_bglt) BgL_ttoz00_2306));
																																																						BgL_arg1807z00_2622
																																																							=
																																																							(BgL_objectz00_bglt)
																																																							(BgL_tmpz00_4330);
																																																					}
																																																					if (BGL_CONDEXPAND_ISA_ARCH64())
																																																						{	/* Module/type.scm 146 */
																																																							long
																																																								BgL_idxz00_2628;
																																																							BgL_idxz00_2628
																																																								=
																																																								BGL_OBJECT_INHERITANCE_NUM
																																																								(BgL_arg1807z00_2622);
																																																							BgL_test3037z00_4329
																																																								=
																																																								(VECTOR_REF
																																																								(BGl_za2inheritancesza2z00zz__objectz00,
																																																									(BgL_idxz00_2628
																																																										+
																																																										1L))
																																																								==
																																																								BgL_classz00_2620);
																																																						}
																																																					else
																																																						{	/* Module/type.scm 146 */
																																																							bool_t
																																																								BgL_res2882z00_2653;
																																																							{	/* Module/type.scm 146 */
																																																								obj_t
																																																									BgL_oclassz00_2636;
																																																								{	/* Module/type.scm 146 */
																																																									obj_t
																																																										BgL_arg1815z00_2644;
																																																									long
																																																										BgL_arg1816z00_2645;
																																																									BgL_arg1815z00_2644
																																																										=
																																																										(BGl_za2classesza2z00zz__objectz00);
																																																									{	/* Module/type.scm 146 */
																																																										long
																																																											BgL_arg1817z00_2646;
																																																										BgL_arg1817z00_2646
																																																											=
																																																											BGL_OBJECT_CLASS_NUM
																																																											(BgL_arg1807z00_2622);
																																																										BgL_arg1816z00_2645
																																																											=
																																																											(BgL_arg1817z00_2646
																																																											-
																																																											OBJECT_TYPE);
																																																									}
																																																									BgL_oclassz00_2636
																																																										=
																																																										VECTOR_REF
																																																										(BgL_arg1815z00_2644,
																																																										BgL_arg1816z00_2645);
																																																								}
																																																								{	/* Module/type.scm 146 */
																																																									bool_t
																																																										BgL__ortest_1115z00_2637;
																																																									BgL__ortest_1115z00_2637
																																																										=
																																																										(BgL_classz00_2620
																																																										==
																																																										BgL_oclassz00_2636);
																																																									if (BgL__ortest_1115z00_2637)
																																																										{	/* Module/type.scm 146 */
																																																											BgL_res2882z00_2653
																																																												=
																																																												BgL__ortest_1115z00_2637;
																																																										}
																																																									else
																																																										{	/* Module/type.scm 146 */
																																																											long
																																																												BgL_odepthz00_2638;
																																																											{	/* Module/type.scm 146 */
																																																												obj_t
																																																													BgL_arg1804z00_2639;
																																																												BgL_arg1804z00_2639
																																																													=
																																																													(BgL_oclassz00_2636);
																																																												BgL_odepthz00_2638
																																																													=
																																																													BGL_CLASS_DEPTH
																																																													(BgL_arg1804z00_2639);
																																																											}
																																																											if ((1L < BgL_odepthz00_2638))
																																																												{	/* Module/type.scm 146 */
																																																													obj_t
																																																														BgL_arg1802z00_2641;
																																																													{	/* Module/type.scm 146 */
																																																														obj_t
																																																															BgL_arg1803z00_2642;
																																																														BgL_arg1803z00_2642
																																																															=
																																																															(BgL_oclassz00_2636);
																																																														BgL_arg1802z00_2641
																																																															=
																																																															BGL_CLASS_ANCESTORS_REF
																																																															(BgL_arg1803z00_2642,
																																																															1L);
																																																													}
																																																													BgL_res2882z00_2653
																																																														=
																																																														(BgL_arg1802z00_2641
																																																														==
																																																														BgL_classz00_2620);
																																																												}
																																																											else
																																																												{	/* Module/type.scm 146 */
																																																													BgL_res2882z00_2653
																																																														=
																																																														(
																																																														(bool_t)
																																																														0);
																																																												}
																																																										}
																																																								}
																																																							}
																																																							BgL_test3037z00_4329
																																																								=
																																																								BgL_res2882z00_2653;
																																																						}
																																																				}
																																																			}
																																																			if (BgL_test3037z00_4329)
																																																				{	/* Module/type.scm 146 */
																																																					return
																																																						BGl_addzd2coercionz12zc0zztype_coercionz00
																																																						(BgL_tfromz00_2305,
																																																						BgL_ttoz00_2306,
																																																						BgL_checksz00_2307,
																																																						BgL_coercesz00_2308);
																																																				}
																																																			else
																																																				{	/* Module/type.scm 147 */
																																																					obj_t
																																																						BgL_list2761z00_2311;
																																																					BgL_list2761z00_2311
																																																						=
																																																						MAKE_YOUNG_PAIR
																																																						(BNIL,
																																																						BNIL);
																																																					return
																																																						BGl_userzd2errorzd2zztools_errorz00
																																																						(BGl_string2898z00zzmodule_typez00,
																																																						BGl_string2899z00zzmodule_typez00,
																																																						BgL_toz00_235,
																																																						BgL_list2761z00_2311);
																																																				}
																																																		}
																																																	else
																																																		{	/* Module/type.scm 145 */
																																																			obj_t
																																																				BgL_list2762z00_2312;
																																																			BgL_list2762z00_2312
																																																				=
																																																				MAKE_YOUNG_PAIR
																																																				(BNIL,
																																																				BNIL);
																																																			return
																																																				BGl_userzd2errorzd2zztools_errorz00
																																																				(BGl_string2898z00zzmodule_typez00,
																																																				BGl_string2899z00zzmodule_typez00,
																																																				BgL_fromz00_234,
																																																				BgL_list2762z00_2312);
																																																		}
																																																}
																																															}
																																														}
																																													}
																																												}
																																											}
																																										}
																																									else
																																										{	/* Module/type.scm 106 */
																																											return
																																												BNIL;
																																										}
																																								}
																																							}
																																						else
																																							{	/* Module/type.scm 58 */
																																								goto
																																									BgL_tagzd2374zd2_239;
																																							}
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																									}
																							}
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				if (
																					(CAR(
																							((obj_t) BgL_clausez00_28)) ==
																						CNST_TABLE_REF(5)))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd21615zd2_401;
																						obj_t BgL_cdrzd21616zd2_402;

																						BgL_carzd21615zd2_401 =
																							CAR(
																							((obj_t) BgL_cdrzd2390zd2_243));
																						BgL_cdrzd21616zd2_402 =
																							CDR(
																							((obj_t) BgL_cdrzd2390zd2_243));
																						if (SYMBOLP(BgL_carzd21615zd2_401))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd21624zd2_404;
																								obj_t BgL_cdrzd21625zd2_405;

																								BgL_carzd21624zd2_404 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd21616zd2_402));
																								BgL_cdrzd21625zd2_405 =
																									CDR(((obj_t)
																										BgL_cdrzd21616zd2_402));
																								if (STRINGP
																									(BgL_carzd21624zd2_404))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd21625zd2_405))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd21631zd2_408;
																												BgL_carzd21631zd2_408 =
																													CAR
																													(BgL_cdrzd21625zd2_405);
																												if (SYMBOLP
																													(BgL_carzd21631zd2_408))
																													{	/* Module/type.scm 58 */
																														if (NULLP(CDR
																																(BgL_cdrzd21625zd2_405)))
																															{	/* Module/type.scm 69 */
																																BgL_typez00_bglt
																																	BgL_typez00_2747;
																																BgL_typez00_2747
																																	=
																																	BGl_declarezd2typez12zc0zztype_envz00
																																	(BgL_carzd21615zd2_401,
																																	BgL_carzd21624zd2_404,
																																	BgL_carzd21631zd2_408);
																																((((BgL_typez00_bglt) COBJECT(BgL_typez00_2747))->BgL_magiczf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																																return
																																	((obj_t)
																																	BgL_typez00_2747);
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						if (
																							(CAR(
																									((obj_t) BgL_clausez00_28)) ==
																								CNST_TABLE_REF(1)))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd21872zd2_416;
																								obj_t BgL_cdrzd21873zd2_417;

																								BgL_carzd21872zd2_416 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2390zd2_243));
																								BgL_cdrzd21873zd2_417 =
																									CDR(((obj_t)
																										BgL_cdrzd2390zd2_243));
																								if (SYMBOLP
																									(BgL_carzd21872zd2_416))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd21882zd2_419;
																										obj_t BgL_cdrzd21883zd2_420;

																										BgL_carzd21882zd2_419 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd21873zd2_417));
																										BgL_cdrzd21883zd2_420 =
																											CDR(((obj_t)
																												BgL_cdrzd21873zd2_417));
																										if (STRINGP
																											(BgL_carzd21882zd2_419))
																											{	/* Module/type.scm 58 */
																												if (PAIRP
																													(BgL_cdrzd21883zd2_420))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd21889zd2_423;
																														BgL_carzd21889zd2_423
																															=
																															CAR
																															(BgL_cdrzd21883zd2_420);
																														if (PAIRP
																															(BgL_carzd21889zd2_423))
																															{	/* Module/type.scm 58 */
																																if (NULLP(CDR
																																		(BgL_cdrzd21883zd2_420)))
																																	{
																																		obj_t
																																			BgL_parentz00_4415;
																																		obj_t
																																			BgL_namez00_4414;
																																		obj_t
																																			BgL_childz00_4413;
																																		BgL_childz00_4413
																																			=
																																			BgL_carzd21872zd2_416;
																																		BgL_namez00_4414
																																			=
																																			BgL_carzd21882zd2_419;
																																		BgL_parentz00_4415
																																			=
																																			BgL_carzd21889zd2_423;
																																		BgL_parentz00_224
																																			=
																																			BgL_parentz00_4415;
																																		BgL_namez00_223
																																			=
																																			BgL_namez00_4414;
																																		BgL_childz00_222
																																			=
																																			BgL_childz00_4413;
																																		goto
																																			BgL_tagzd2370zd2_225;
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_cdrzd21909zd2_427;
																																		BgL_cdrzd21909zd2_427
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_clausez00_28));
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd21915zd2_428;
																																			obj_t
																																				BgL_cdrzd21916zd2_429;
																																			BgL_carzd21915zd2_428
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd21909zd2_427));
																																			BgL_cdrzd21916zd2_429
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd21909zd2_427));
																																			if (SYMBOLP(BgL_carzd21915zd2_428))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd21924zd2_431;
																																					obj_t
																																						BgL_cdrzd21925zd2_432;
																																					BgL_carzd21924zd2_431
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd21916zd2_429));
																																					BgL_cdrzd21925zd2_432
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd21916zd2_429));
																																					if (STRINGP(BgL_carzd21924zd2_431))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd21931zd2_434;
																																							obj_t
																																								BgL_cdrzd21932zd2_435;
																																							BgL_carzd21931zd2_434
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd21925zd2_432));
																																							BgL_cdrzd21932zd2_435
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd21925zd2_432));
																																							if (PAIRP(BgL_carzd21931zd2_434))
																																								{	/* Module/type.scm 58 */
																																									if (PAIRP(BgL_cdrzd21932zd2_435))
																																										{	/* Module/type.scm 58 */
																																											obj_t
																																												BgL_carzd21938zd2_438;
																																											BgL_carzd21938zd2_438
																																												=
																																												CAR
																																												(BgL_cdrzd21932zd2_435);
																																											if (SYMBOLP(BgL_carzd21938zd2_438))
																																												{	/* Module/type.scm 58 */
																																													if (NULLP(CDR(BgL_cdrzd21932zd2_435)))
																																														{
																																															obj_t
																																																BgL_nullzd2valuezd2_4447;
																																															obj_t
																																																BgL_parentz00_4446;
																																															obj_t
																																																BgL_namez00_4445;
																																															obj_t
																																																BgL_childz00_4444;
																																															BgL_childz00_4444
																																																=
																																																BgL_carzd21915zd2_428;
																																															BgL_namez00_4445
																																																=
																																																BgL_carzd21924zd2_431;
																																															BgL_parentz00_4446
																																																=
																																																BgL_carzd21931zd2_434;
																																															BgL_nullzd2valuezd2_4447
																																																=
																																																BgL_carzd21938zd2_438;
																																															BgL_nullzd2valuezd2_229
																																																=
																																																BgL_nullzd2valuezd2_4447;
																																															BgL_parentz00_228
																																																=
																																																BgL_parentz00_4446;
																																															BgL_namez00_227
																																																=
																																																BgL_namez00_4445;
																																															BgL_childz00_226
																																																=
																																																BgL_childz00_4444;
																																															goto
																																																BgL_tagzd2371zd2_230;
																																														}
																																													else
																																														{	/* Module/type.scm 58 */
																																															goto
																																																BgL_tagzd2374zd2_239;
																																														}
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_cdrzd22054zd2_444;
																																BgL_cdrzd22054zd2_444
																																	=
																																	CDR(((obj_t)
																																		BgL_clausez00_28));
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd22060zd2_445;
																																	obj_t
																																		BgL_cdrzd22061zd2_446;
																																	BgL_carzd22060zd2_445
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd22054zd2_444));
																																	BgL_cdrzd22061zd2_446
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd22054zd2_444));
																																	if (SYMBOLP
																																		(BgL_carzd22060zd2_445))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd22069zd2_448;
																																			obj_t
																																				BgL_cdrzd22070zd2_449;
																																			BgL_carzd22069zd2_448
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd22061zd2_446));
																																			BgL_cdrzd22070zd2_449
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd22061zd2_446));
																																			if (STRINGP(BgL_carzd22069zd2_448))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd22076zd2_451;
																																					obj_t
																																						BgL_cdrzd22077zd2_452;
																																					BgL_carzd22076zd2_451
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd22070zd2_449));
																																					BgL_cdrzd22077zd2_452
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd22070zd2_449));
																																					if (PAIRP(BgL_carzd22076zd2_451))
																																						{	/* Module/type.scm 58 */
																																							if (PAIRP(BgL_cdrzd22077zd2_452))
																																								{	/* Module/type.scm 58 */
																																									obj_t
																																										BgL_carzd22083zd2_455;
																																									BgL_carzd22083zd2_455
																																										=
																																										CAR
																																										(BgL_cdrzd22077zd2_452);
																																									if (SYMBOLP(BgL_carzd22083zd2_455))
																																										{	/* Module/type.scm 58 */
																																											if (NULLP(CDR(BgL_cdrzd22077zd2_452)))
																																												{
																																													obj_t
																																														BgL_nullzd2valuezd2_4479;
																																													obj_t
																																														BgL_parentz00_4478;
																																													obj_t
																																														BgL_namez00_4477;
																																													obj_t
																																														BgL_childz00_4476;
																																													BgL_childz00_4476
																																														=
																																														BgL_carzd22060zd2_445;
																																													BgL_namez00_4477
																																														=
																																														BgL_carzd22069zd2_448;
																																													BgL_parentz00_4478
																																														=
																																														BgL_carzd22076zd2_451;
																																													BgL_nullzd2valuezd2_4479
																																														=
																																														BgL_carzd22083zd2_455;
																																													BgL_nullzd2valuezd2_229
																																														=
																																														BgL_nullzd2valuezd2_4479;
																																													BgL_parentz00_228
																																														=
																																														BgL_parentz00_4478;
																																													BgL_namez00_227
																																														=
																																														BgL_namez00_4477;
																																													BgL_childz00_226
																																														=
																																														BgL_childz00_4476;
																																													goto
																																														BgL_tagzd2371zd2_230;
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_cdrzd22228zd2_460;
																												BgL_cdrzd22228zd2_460 =
																													CDR(((obj_t)
																														BgL_clausez00_28));
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd22233zd2_461;
																													obj_t
																														BgL_cdrzd22234zd2_462;
																													BgL_carzd22233zd2_461
																														=
																														CAR(((obj_t)
																															BgL_cdrzd22228zd2_460));
																													BgL_cdrzd22234zd2_462
																														=
																														CDR(((obj_t)
																															BgL_cdrzd22228zd2_460));
																													if (SYMBOLP
																														(BgL_carzd22233zd2_461))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd22241zd2_464;
																															obj_t
																																BgL_cdrzd22242zd2_465;
																															BgL_carzd22241zd2_464
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd22234zd2_462));
																															BgL_cdrzd22242zd2_465
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd22234zd2_462));
																															if (STRINGP
																																(BgL_carzd22241zd2_464))
																																{	/* Module/type.scm 58 */
																																	if (PAIRP
																																		(BgL_cdrzd22242zd2_465))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd22247zd2_468;
																																			obj_t
																																				BgL_cdrzd22248zd2_469;
																																			BgL_carzd22247zd2_468
																																				=
																																				CAR
																																				(BgL_cdrzd22242zd2_465);
																																			BgL_cdrzd22248zd2_469
																																				=
																																				CDR
																																				(BgL_cdrzd22242zd2_465);
																																			if (PAIRP
																																				(BgL_carzd22247zd2_468))
																																				{	/* Module/type.scm 58 */
																																					if (PAIRP(BgL_cdrzd22248zd2_469))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd22252zd2_472;
																																							BgL_carzd22252zd2_472
																																								=
																																								CAR
																																								(BgL_cdrzd22248zd2_469);
																																							if (SYMBOLP(BgL_carzd22252zd2_472))
																																								{	/* Module/type.scm 58 */
																																									if (NULLP(CDR(BgL_cdrzd22248zd2_469)))
																																										{
																																											obj_t
																																												BgL_nullzd2valuezd2_4511;
																																											obj_t
																																												BgL_parentz00_4510;
																																											obj_t
																																												BgL_namez00_4509;
																																											obj_t
																																												BgL_childz00_4508;
																																											BgL_childz00_4508
																																												=
																																												BgL_carzd22233zd2_461;
																																											BgL_namez00_4509
																																												=
																																												BgL_carzd22241zd2_464;
																																											BgL_parentz00_4510
																																												=
																																												BgL_carzd22247zd2_468;
																																											BgL_nullzd2valuezd2_4511
																																												=
																																												BgL_carzd22252zd2_472;
																																											BgL_nullzd2valuezd2_229
																																												=
																																												BgL_nullzd2valuezd2_4511;
																																											BgL_parentz00_228
																																												=
																																												BgL_parentz00_4510;
																																											BgL_namez00_227
																																												=
																																												BgL_namez00_4509;
																																											BgL_childz00_226
																																												=
																																												BgL_childz00_4508;
																																											goto
																																												BgL_tagzd2371zd2_230;
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										obj_t BgL_cdrzd22385zd2_477;

																										BgL_cdrzd22385zd2_477 =
																											CDR(
																											((obj_t)
																												BgL_clausez00_28));
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd22392zd2_478;
																											obj_t
																												BgL_cdrzd22393zd2_479;
																											BgL_carzd22392zd2_478 =
																												CAR(((obj_t)
																													BgL_cdrzd22385zd2_477));
																											BgL_cdrzd22393zd2_479 =
																												CDR(((obj_t)
																													BgL_cdrzd22385zd2_477));
																											if (SYMBOLP
																												(BgL_carzd22392zd2_478))
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd22403zd2_481;
																													obj_t
																														BgL_cdrzd22404zd2_482;
																													BgL_carzd22403zd2_481
																														=
																														CAR(((obj_t)
																															BgL_cdrzd22393zd2_479));
																													BgL_cdrzd22404zd2_482
																														=
																														CDR(((obj_t)
																															BgL_cdrzd22393zd2_479));
																													if (STRINGP
																														(BgL_carzd22403zd2_481))
																														{	/* Module/type.scm 58 */
																															if (PAIRP
																																(BgL_cdrzd22404zd2_482))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd22411zd2_485;
																																	obj_t
																																		BgL_cdrzd22412zd2_486;
																																	BgL_carzd22411zd2_485
																																		=
																																		CAR
																																		(BgL_cdrzd22404zd2_482);
																																	BgL_cdrzd22412zd2_486
																																		=
																																		CDR
																																		(BgL_cdrzd22404zd2_482);
																																	if (PAIRP
																																		(BgL_carzd22411zd2_485))
																																		{	/* Module/type.scm 58 */
																																			if (PAIRP
																																				(BgL_cdrzd22412zd2_486))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd22416zd2_489;
																																					BgL_carzd22416zd2_489
																																						=
																																						CAR
																																						(BgL_cdrzd22412zd2_486);
																																					if (SYMBOLP(BgL_carzd22416zd2_489))
																																						{	/* Module/type.scm 58 */
																																							if (NULLP(CDR(BgL_cdrzd22412zd2_486)))
																																								{
																																									obj_t
																																										BgL_nullzd2valuezd2_4543;
																																									obj_t
																																										BgL_parentz00_4542;
																																									obj_t
																																										BgL_namez00_4541;
																																									obj_t
																																										BgL_childz00_4540;
																																									BgL_childz00_4540
																																										=
																																										BgL_carzd22392zd2_478;
																																									BgL_namez00_4541
																																										=
																																										BgL_carzd22403zd2_481;
																																									BgL_parentz00_4542
																																										=
																																										BgL_carzd22411zd2_485;
																																									BgL_nullzd2valuezd2_4543
																																										=
																																										BgL_carzd22416zd2_489;
																																									BgL_nullzd2valuezd2_229
																																										=
																																										BgL_nullzd2valuezd2_4543;
																																									BgL_parentz00_228
																																										=
																																										BgL_parentz00_4542;
																																									BgL_namez00_227
																																										=
																																										BgL_namez00_4541;
																																									BgL_childz00_226
																																										=
																																										BgL_childz00_4540;
																																									goto
																																										BgL_tagzd2371zd2_230;
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								obj_t BgL_cdrzd22549zd2_494;

																								BgL_cdrzd22549zd2_494 =
																									CDR(
																									((obj_t) BgL_clausez00_28));
																								if (
																									(CAR(
																											((obj_t)
																												BgL_clausez00_28)) ==
																										CNST_TABLE_REF(1)))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd22556zd2_497;
																										obj_t BgL_cdrzd22557zd2_498;

																										BgL_carzd22556zd2_497 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd22549zd2_494));
																										BgL_cdrzd22557zd2_498 =
																											CDR(((obj_t)
																												BgL_cdrzd22549zd2_494));
																										if (SYMBOLP
																											(BgL_carzd22556zd2_497))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd22567zd2_500;
																												obj_t
																													BgL_cdrzd22568zd2_501;
																												BgL_carzd22567zd2_500 =
																													CAR(((obj_t)
																														BgL_cdrzd22557zd2_498));
																												BgL_cdrzd22568zd2_501 =
																													CDR(((obj_t)
																														BgL_cdrzd22557zd2_498));
																												if (STRINGP
																													(BgL_carzd22567zd2_500))
																													{	/* Module/type.scm 58 */
																														if (PAIRP
																															(BgL_cdrzd22568zd2_501))
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_carzd22575zd2_504;
																																obj_t
																																	BgL_cdrzd22576zd2_505;
																																BgL_carzd22575zd2_504
																																	=
																																	CAR
																																	(BgL_cdrzd22568zd2_501);
																																BgL_cdrzd22576zd2_505
																																	=
																																	CDR
																																	(BgL_cdrzd22568zd2_501);
																																if (PAIRP
																																	(BgL_carzd22575zd2_504))
																																	{	/* Module/type.scm 58 */
																																		if (PAIRP
																																			(BgL_cdrzd22576zd2_505))
																																			{	/* Module/type.scm 58 */
																																				obj_t
																																					BgL_carzd22580zd2_508;
																																				BgL_carzd22580zd2_508
																																					=
																																					CAR
																																					(BgL_cdrzd22576zd2_505);
																																				if (SYMBOLP(BgL_carzd22580zd2_508))
																																					{	/* Module/type.scm 58 */
																																						if (NULLP(CDR(BgL_cdrzd22576zd2_505)))
																																							{
																																								obj_t
																																									BgL_nullzd2valuezd2_4580;
																																								obj_t
																																									BgL_parentz00_4579;
																																								obj_t
																																									BgL_namez00_4578;
																																								obj_t
																																									BgL_childz00_4577;
																																								BgL_childz00_4577
																																									=
																																									BgL_carzd22556zd2_497;
																																								BgL_namez00_4578
																																									=
																																									BgL_carzd22567zd2_500;
																																								BgL_parentz00_4579
																																									=
																																									BgL_carzd22575zd2_504;
																																								BgL_nullzd2valuezd2_4580
																																									=
																																									BgL_carzd22580zd2_508;
																																								BgL_nullzd2valuezd2_229
																																									=
																																									BgL_nullzd2valuezd2_4580;
																																								BgL_parentz00_228
																																									=
																																									BgL_parentz00_4579;
																																								BgL_namez00_227
																																									=
																																									BgL_namez00_4578;
																																								BgL_childz00_226
																																									=
																																									BgL_childz00_4577;
																																								goto
																																									BgL_tagzd2371zd2_230;
																																							}
																																						else
																																							{	/* Module/type.scm 58 */
																																								goto
																																									BgL_tagzd2374zd2_239;
																																							}
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										if (
																											(CAR(
																													((obj_t)
																														BgL_clausez00_28))
																												== CNST_TABLE_REF(7)))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd22710zd2_516;
																												obj_t
																													BgL_cdrzd22711zd2_517;
																												BgL_carzd22710zd2_516 =
																													CAR(((obj_t)
																														BgL_cdrzd22549zd2_494));
																												BgL_cdrzd22711zd2_517 =
																													CDR(((obj_t)
																														BgL_cdrzd22549zd2_494));
																												if (SYMBOLP
																													(BgL_carzd22710zd2_516))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd22718zd2_519;
																														BgL_carzd22718zd2_519
																															=
																															CAR(((obj_t)
																																BgL_cdrzd22711zd2_517));
																														if (PAIRP
																															(BgL_carzd22718zd2_519))
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_carzd22721zd2_521;
																																BgL_carzd22721zd2_521
																																	=
																																	CAR
																																	(BgL_carzd22718zd2_519);
																																if (SYMBOLP
																																	(BgL_carzd22721zd2_521))
																																	{	/* Module/type.scm 58 */
																																		if (NULLP
																																			(CDR
																																				(BgL_carzd22718zd2_519)))
																																			{	/* Module/type.scm 58 */
																																				if (NULLP(CDR(((obj_t) BgL_cdrzd22711zd2_517))))
																																					{	/* Module/type.scm 58 */
																																						return
																																							BGl_delayzd2tvectorzd2typez12z12zzmodule_typez00
																																							(BgL_carzd22710zd2_516,
																																							BgL_carzd22721zd2_521,
																																							BgL_clausez00_28,
																																							BgL_importz00_27);
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_cdrzd22777zd2_529;
																												BgL_cdrzd22777zd2_529 =
																													CDR(((obj_t)
																														BgL_clausez00_28));
																												if ((CAR(((obj_t)
																																BgL_clausez00_28))
																														==
																														CNST_TABLE_REF(6)))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd22784zd2_532;
																														obj_t
																															BgL_cdrzd22785zd2_533;
																														BgL_carzd22784zd2_532
																															=
																															CAR(((obj_t)
																																BgL_cdrzd22777zd2_529));
																														BgL_cdrzd22785zd2_533
																															=
																															CDR(((obj_t)
																																BgL_cdrzd22777zd2_529));
																														if (SYMBOLP
																															(BgL_carzd22784zd2_532))
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_carzd22795zd2_535;
																																obj_t
																																	BgL_cdrzd22796zd2_536;
																																BgL_carzd22795zd2_535
																																	=
																																	CAR(((obj_t)
																																		BgL_cdrzd22785zd2_533));
																																BgL_cdrzd22796zd2_536
																																	=
																																	CDR(((obj_t)
																																		BgL_cdrzd22785zd2_533));
																																if (SYMBOLP
																																	(BgL_carzd22795zd2_535))
																																	{	/* Module/type.scm 58 */
																																		if (PAIRP
																																			(BgL_cdrzd22796zd2_536))
																																			{	/* Module/type.scm 58 */
																																				obj_t
																																					BgL_cdrzd22805zd2_539;
																																				BgL_cdrzd22805zd2_539
																																					=
																																					CDR
																																					(BgL_cdrzd22796zd2_536);
																																				if (PAIRP(BgL_cdrzd22805zd2_539))
																																					{	/* Module/type.scm 58 */
																																						if (NULLP(CDR(BgL_cdrzd22805zd2_539)))
																																							{
																																								obj_t
																																									BgL_coercez00_4638;
																																								obj_t
																																									BgL_checkz00_4636;
																																								obj_t
																																									BgL_toz00_4635;
																																								obj_t
																																									BgL_fromz00_4634;
																																								BgL_fromz00_4634
																																									=
																																									BgL_carzd22784zd2_532;
																																								BgL_toz00_4635
																																									=
																																									BgL_carzd22795zd2_535;
																																								BgL_checkz00_4636
																																									=
																																									CAR
																																									(BgL_cdrzd22796zd2_536);
																																								BgL_coercez00_4638
																																									=
																																									CAR
																																									(BgL_cdrzd22805zd2_539);
																																								BgL_coercez00_237
																																									=
																																									BgL_coercez00_4638;
																																								BgL_checkz00_236
																																									=
																																									BgL_checkz00_4636;
																																								BgL_toz00_235
																																									=
																																									BgL_toz00_4635;
																																								BgL_fromz00_234
																																									=
																																									BgL_fromz00_4634;
																																								goto
																																									BgL_tagzd2373zd2_238;
																																							}
																																						else
																																							{	/* Module/type.scm 58 */
																																								goto
																																									BgL_tagzd2374zd2_239;
																																							}
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																									}
																							}
																					}
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		goto BgL_tagzd2374zd2_239;
																	}
															}
														else
															{	/* Module/type.scm 58 */
																if (
																	(CAR(
																			((obj_t) BgL_clausez00_28)) ==
																		CNST_TABLE_REF(5)))
																	{	/* Module/type.scm 58 */
																		obj_t BgL_carzd22894zd2_554;
																		obj_t BgL_cdrzd22895zd2_555;

																		BgL_carzd22894zd2_554 =
																			CAR(((obj_t) BgL_cdrzd2390zd2_243));
																		BgL_cdrzd22895zd2_555 =
																			CDR(((obj_t) BgL_cdrzd2390zd2_243));
																		if (SYMBOLP(BgL_carzd22894zd2_554))
																			{	/* Module/type.scm 58 */
																				if (PAIRP(BgL_cdrzd22895zd2_555))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd22902zd2_558;
																						obj_t BgL_cdrzd22903zd2_559;

																						BgL_carzd22902zd2_558 =
																							CAR(BgL_cdrzd22895zd2_555);
																						BgL_cdrzd22903zd2_559 =
																							CDR(BgL_cdrzd22895zd2_555);
																						if (STRINGP(BgL_carzd22902zd2_558))
																							{	/* Module/type.scm 58 */
																								if (PAIRP
																									(BgL_cdrzd22903zd2_559))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd22907zd2_562;

																										BgL_carzd22907zd2_562 =
																											CAR
																											(BgL_cdrzd22903zd2_559);
																										if (SYMBOLP
																											(BgL_carzd22907zd2_562))
																											{	/* Module/type.scm 58 */
																												if (NULLP(CDR
																														(BgL_cdrzd22903zd2_559)))
																													{	/* Module/type.scm 69 */
																														BgL_typez00_bglt
																															BgL_typez00_2830;
																														BgL_typez00_2830 =
																															BGl_declarezd2typez12zc0zztype_envz00
																															(BgL_carzd22894zd2_554,
																															BgL_carzd22902zd2_558,
																															BgL_carzd22907zd2_562);
																														((((BgL_typez00_bglt) COBJECT(BgL_typez00_2830))->BgL_magiczf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																														return
																															((obj_t)
																															BgL_typez00_2830);
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				goto BgL_tagzd2374zd2_239;
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		if (
																			(CAR(
																					((obj_t) BgL_clausez00_28)) ==
																				CNST_TABLE_REF(1)))
																			{	/* Module/type.scm 58 */
																				obj_t BgL_carzd23189zd2_570;
																				obj_t BgL_cdrzd23190zd2_571;

																				BgL_carzd23189zd2_570 =
																					CAR(((obj_t) BgL_cdrzd2390zd2_243));
																				BgL_cdrzd23190zd2_571 =
																					CDR(((obj_t) BgL_cdrzd2390zd2_243));
																				if (SYMBOLP(BgL_carzd23189zd2_570))
																					{	/* Module/type.scm 58 */
																						if (PAIRP(BgL_cdrzd23190zd2_571))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd23198zd2_574;
																								obj_t BgL_cdrzd23199zd2_575;

																								BgL_carzd23198zd2_574 =
																									CAR(BgL_cdrzd23190zd2_571);
																								BgL_cdrzd23199zd2_575 =
																									CDR(BgL_cdrzd23190zd2_571);
																								if (STRINGP
																									(BgL_carzd23198zd2_574))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd23199zd2_575))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd23203zd2_578;
																												BgL_carzd23203zd2_578 =
																													CAR
																													(BgL_cdrzd23199zd2_575);
																												if (PAIRP
																													(BgL_carzd23203zd2_578))
																													{	/* Module/type.scm 58 */
																														if (NULLP(CDR
																																(BgL_cdrzd23199zd2_575)))
																															{
																																obj_t
																																	BgL_parentz00_4695;
																																obj_t
																																	BgL_namez00_4694;
																																obj_t
																																	BgL_childz00_4693;
																																BgL_childz00_4693
																																	=
																																	BgL_carzd23189zd2_570;
																																BgL_namez00_4694
																																	=
																																	BgL_carzd23198zd2_574;
																																BgL_parentz00_4695
																																	=
																																	BgL_carzd23203zd2_578;
																																BgL_parentz00_224
																																	=
																																	BgL_parentz00_4695;
																																BgL_namez00_223
																																	=
																																	BgL_namez00_4694;
																																BgL_childz00_222
																																	=
																																	BgL_childz00_4693;
																																goto
																																	BgL_tagzd2370zd2_225;
																															}
																														else
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_cdrzd23224zd2_582;
																																BgL_cdrzd23224zd2_582
																																	=
																																	CDR(((obj_t)
																																		BgL_clausez00_28));
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd23231zd2_583;
																																	obj_t
																																		BgL_cdrzd23232zd2_584;
																																	BgL_carzd23231zd2_583
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd23224zd2_582));
																																	BgL_cdrzd23232zd2_584
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd23224zd2_582));
																																	if (SYMBOLP
																																		(BgL_carzd23231zd2_583))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd23241zd2_586;
																																			obj_t
																																				BgL_cdrzd23242zd2_587;
																																			BgL_carzd23241zd2_586
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd23232zd2_584));
																																			BgL_cdrzd23242zd2_587
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd23232zd2_584));
																																			if (STRINGP(BgL_carzd23241zd2_586))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd23250zd2_589;
																																					obj_t
																																						BgL_cdrzd23251zd2_590;
																																					BgL_carzd23250zd2_589
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd23242zd2_587));
																																					BgL_cdrzd23251zd2_590
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd23242zd2_587));
																																					if (PAIRP(BgL_carzd23250zd2_589))
																																						{	/* Module/type.scm 58 */
																																							if (PAIRP(BgL_cdrzd23251zd2_590))
																																								{	/* Module/type.scm 58 */
																																									obj_t
																																										BgL_carzd23257zd2_593;
																																									BgL_carzd23257zd2_593
																																										=
																																										CAR
																																										(BgL_cdrzd23251zd2_590);
																																									if (SYMBOLP(BgL_carzd23257zd2_593))
																																										{	/* Module/type.scm 58 */
																																											if (NULLP(CDR(BgL_cdrzd23251zd2_590)))
																																												{
																																													obj_t
																																														BgL_nullzd2valuezd2_4727;
																																													obj_t
																																														BgL_parentz00_4726;
																																													obj_t
																																														BgL_namez00_4725;
																																													obj_t
																																														BgL_childz00_4724;
																																													BgL_childz00_4724
																																														=
																																														BgL_carzd23231zd2_583;
																																													BgL_namez00_4725
																																														=
																																														BgL_carzd23241zd2_586;
																																													BgL_parentz00_4726
																																														=
																																														BgL_carzd23250zd2_589;
																																													BgL_nullzd2valuezd2_4727
																																														=
																																														BgL_carzd23257zd2_593;
																																													BgL_nullzd2valuezd2_229
																																														=
																																														BgL_nullzd2valuezd2_4727;
																																													BgL_parentz00_228
																																														=
																																														BgL_parentz00_4726;
																																													BgL_namez00_227
																																														=
																																														BgL_namez00_4725;
																																													BgL_childz00_226
																																														=
																																														BgL_childz00_4724;
																																													goto
																																														BgL_tagzd2371zd2_230;
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_cdrzd23374zd2_599;
																														BgL_cdrzd23374zd2_599
																															=
																															CDR(((obj_t)
																																BgL_clausez00_28));
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd23381zd2_600;
																															obj_t
																																BgL_cdrzd23382zd2_601;
																															BgL_carzd23381zd2_600
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd23374zd2_599));
																															BgL_cdrzd23382zd2_601
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd23374zd2_599));
																															if (SYMBOLP
																																(BgL_carzd23381zd2_600))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd23391zd2_603;
																																	obj_t
																																		BgL_cdrzd23392zd2_604;
																																	BgL_carzd23391zd2_603
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd23382zd2_601));
																																	BgL_cdrzd23392zd2_604
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd23382zd2_601));
																																	if (STRINGP
																																		(BgL_carzd23391zd2_603))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd23400zd2_606;
																																			obj_t
																																				BgL_cdrzd23401zd2_607;
																																			BgL_carzd23400zd2_606
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd23392zd2_604));
																																			BgL_cdrzd23401zd2_607
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd23392zd2_604));
																																			if (PAIRP
																																				(BgL_carzd23400zd2_606))
																																				{	/* Module/type.scm 58 */
																																					if (PAIRP(BgL_cdrzd23401zd2_607))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd23407zd2_610;
																																							BgL_carzd23407zd2_610
																																								=
																																								CAR
																																								(BgL_cdrzd23401zd2_607);
																																							if (SYMBOLP(BgL_carzd23407zd2_610))
																																								{	/* Module/type.scm 58 */
																																									if (NULLP(CDR(BgL_cdrzd23401zd2_607)))
																																										{
																																											obj_t
																																												BgL_nullzd2valuezd2_4759;
																																											obj_t
																																												BgL_parentz00_4758;
																																											obj_t
																																												BgL_namez00_4757;
																																											obj_t
																																												BgL_childz00_4756;
																																											BgL_childz00_4756
																																												=
																																												BgL_carzd23381zd2_600;
																																											BgL_namez00_4757
																																												=
																																												BgL_carzd23391zd2_603;
																																											BgL_parentz00_4758
																																												=
																																												BgL_carzd23400zd2_606;
																																											BgL_nullzd2valuezd2_4759
																																												=
																																												BgL_carzd23407zd2_610;
																																											BgL_nullzd2valuezd2_229
																																												=
																																												BgL_nullzd2valuezd2_4759;
																																											BgL_parentz00_228
																																												=
																																												BgL_parentz00_4758;
																																											BgL_namez00_227
																																												=
																																												BgL_namez00_4757;
																																											BgL_childz00_226
																																												=
																																												BgL_childz00_4756;
																																											goto
																																												BgL_tagzd2371zd2_230;
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										obj_t BgL_cdrzd23554zd2_615;

																										BgL_cdrzd23554zd2_615 =
																											CDR(
																											((obj_t)
																												BgL_clausez00_28));
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd23560zd2_616;
																											obj_t
																												BgL_cdrzd23561zd2_617;
																											BgL_carzd23560zd2_616 =
																												CAR(((obj_t)
																													BgL_cdrzd23554zd2_615));
																											BgL_cdrzd23561zd2_617 =
																												CDR(((obj_t)
																													BgL_cdrzd23554zd2_615));
																											if (SYMBOLP
																												(BgL_carzd23560zd2_616))
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd23569zd2_619;
																													obj_t
																														BgL_cdrzd23570zd2_620;
																													BgL_carzd23569zd2_619
																														=
																														CAR(((obj_t)
																															BgL_cdrzd23561zd2_617));
																													BgL_cdrzd23570zd2_620
																														=
																														CDR(((obj_t)
																															BgL_cdrzd23561zd2_617));
																													if (STRINGP
																														(BgL_carzd23569zd2_619))
																														{	/* Module/type.scm 58 */
																															if (PAIRP
																																(BgL_cdrzd23570zd2_620))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd23577zd2_623;
																																	obj_t
																																		BgL_cdrzd23578zd2_624;
																																	BgL_carzd23577zd2_623
																																		=
																																		CAR
																																		(BgL_cdrzd23570zd2_620);
																																	BgL_cdrzd23578zd2_624
																																		=
																																		CDR
																																		(BgL_cdrzd23570zd2_620);
																																	if (PAIRP
																																		(BgL_carzd23577zd2_623))
																																		{	/* Module/type.scm 58 */
																																			if (PAIRP
																																				(BgL_cdrzd23578zd2_624))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd23582zd2_627;
																																					BgL_carzd23582zd2_627
																																						=
																																						CAR
																																						(BgL_cdrzd23578zd2_624);
																																					if (SYMBOLP(BgL_carzd23582zd2_627))
																																						{	/* Module/type.scm 58 */
																																							if (NULLP(CDR(BgL_cdrzd23578zd2_624)))
																																								{
																																									obj_t
																																										BgL_nullzd2valuezd2_4791;
																																									obj_t
																																										BgL_parentz00_4790;
																																									obj_t
																																										BgL_namez00_4789;
																																									obj_t
																																										BgL_childz00_4788;
																																									BgL_childz00_4788
																																										=
																																										BgL_carzd23560zd2_616;
																																									BgL_namez00_4789
																																										=
																																										BgL_carzd23569zd2_619;
																																									BgL_parentz00_4790
																																										=
																																										BgL_carzd23577zd2_623;
																																									BgL_nullzd2valuezd2_4791
																																										=
																																										BgL_carzd23582zd2_627;
																																									BgL_nullzd2valuezd2_229
																																										=
																																										BgL_nullzd2valuezd2_4791;
																																									BgL_parentz00_228
																																										=
																																										BgL_parentz00_4790;
																																									BgL_namez00_227
																																										=
																																										BgL_namez00_4789;
																																									BgL_childz00_226
																																										=
																																										BgL_childz00_4788;
																																									goto
																																										BgL_tagzd2371zd2_230;
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						obj_t BgL_cdrzd23744zd2_632;

																						BgL_cdrzd23744zd2_632 =
																							CDR(((obj_t) BgL_clausez00_28));
																						{	/* Module/type.scm 58 */
																							obj_t BgL_carzd23750zd2_633;
																							obj_t BgL_cdrzd23751zd2_634;

																							BgL_carzd23750zd2_633 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd23744zd2_632));
																							BgL_cdrzd23751zd2_634 =
																								CDR(((obj_t)
																									BgL_cdrzd23744zd2_632));
																							if (SYMBOLP
																								(BgL_carzd23750zd2_633))
																								{	/* Module/type.scm 58 */
																									if (PAIRP
																										(BgL_cdrzd23751zd2_634))
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd23760zd2_637;
																											obj_t
																												BgL_cdrzd23761zd2_638;
																											BgL_carzd23760zd2_637 =
																												CAR
																												(BgL_cdrzd23751zd2_634);
																											BgL_cdrzd23761zd2_638 =
																												CDR
																												(BgL_cdrzd23751zd2_634);
																											if (STRINGP
																												(BgL_carzd23760zd2_637))
																												{	/* Module/type.scm 58 */
																													if (PAIRP
																														(BgL_cdrzd23761zd2_638))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd23766zd2_641;
																															obj_t
																																BgL_cdrzd23767zd2_642;
																															BgL_carzd23766zd2_641
																																=
																																CAR
																																(BgL_cdrzd23761zd2_638);
																															BgL_cdrzd23767zd2_642
																																=
																																CDR
																																(BgL_cdrzd23761zd2_638);
																															if (PAIRP
																																(BgL_carzd23766zd2_641))
																																{	/* Module/type.scm 58 */
																																	if (PAIRP
																																		(BgL_cdrzd23767zd2_642))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd23771zd2_645;
																																			BgL_carzd23771zd2_645
																																				=
																																				CAR
																																				(BgL_cdrzd23767zd2_642);
																																			if (SYMBOLP(BgL_carzd23771zd2_645))
																																				{	/* Module/type.scm 58 */
																																					if (NULLP(CDR(BgL_cdrzd23767zd2_642)))
																																						{
																																							obj_t
																																								BgL_nullzd2valuezd2_4823;
																																							obj_t
																																								BgL_parentz00_4822;
																																							obj_t
																																								BgL_namez00_4821;
																																							obj_t
																																								BgL_childz00_4820;
																																							BgL_childz00_4820
																																								=
																																								BgL_carzd23750zd2_633;
																																							BgL_namez00_4821
																																								=
																																								BgL_carzd23760zd2_637;
																																							BgL_parentz00_4822
																																								=
																																								BgL_carzd23766zd2_641;
																																							BgL_nullzd2valuezd2_4823
																																								=
																																								BgL_carzd23771zd2_645;
																																							BgL_nullzd2valuezd2_229
																																								=
																																								BgL_nullzd2valuezd2_4823;
																																							BgL_parentz00_228
																																								=
																																								BgL_parentz00_4822;
																																							BgL_namez00_227
																																								=
																																								BgL_namez00_4821;
																																							BgL_childz00_226
																																								=
																																								BgL_childz00_4820;
																																							goto
																																								BgL_tagzd2371zd2_230;
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									else
																										{	/* Module/type.scm 58 */
																											goto BgL_tagzd2374zd2_239;
																										}
																								}
																							else
																								{	/* Module/type.scm 58 */
																									goto BgL_tagzd2374zd2_239;
																								}
																						}
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				obj_t BgL_cdrzd23919zd2_650;

																				BgL_cdrzd23919zd2_650 =
																					CDR(((obj_t) BgL_clausez00_28));
																				if (
																					(CAR(
																							((obj_t) BgL_clausez00_28)) ==
																						CNST_TABLE_REF(1)))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd23925zd2_653;
																						obj_t BgL_cdrzd23926zd2_654;

																						BgL_carzd23925zd2_653 =
																							CAR(
																							((obj_t) BgL_cdrzd23919zd2_650));
																						BgL_cdrzd23926zd2_654 =
																							CDR(
																							((obj_t) BgL_cdrzd23919zd2_650));
																						if (SYMBOLP(BgL_carzd23925zd2_653))
																							{	/* Module/type.scm 58 */
																								if (PAIRP
																									(BgL_cdrzd23926zd2_654))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd23935zd2_657;
																										obj_t BgL_cdrzd23936zd2_658;

																										BgL_carzd23935zd2_657 =
																											CAR
																											(BgL_cdrzd23926zd2_654);
																										BgL_cdrzd23936zd2_658 =
																											CDR
																											(BgL_cdrzd23926zd2_654);
																										if (STRINGP
																											(BgL_carzd23935zd2_657))
																											{	/* Module/type.scm 58 */
																												if (PAIRP
																													(BgL_cdrzd23936zd2_658))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd23941zd2_661;
																														obj_t
																															BgL_cdrzd23942zd2_662;
																														BgL_carzd23941zd2_661
																															=
																															CAR
																															(BgL_cdrzd23936zd2_658);
																														BgL_cdrzd23942zd2_662
																															=
																															CDR
																															(BgL_cdrzd23936zd2_658);
																														if (PAIRP
																															(BgL_carzd23941zd2_661))
																															{	/* Module/type.scm 58 */
																																if (PAIRP
																																	(BgL_cdrzd23942zd2_662))
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_carzd23946zd2_665;
																																		BgL_carzd23946zd2_665
																																			=
																																			CAR
																																			(BgL_cdrzd23942zd2_662);
																																		if (SYMBOLP
																																			(BgL_carzd23946zd2_665))
																																			{	/* Module/type.scm 58 */
																																				if (NULLP(CDR(BgL_cdrzd23942zd2_662)))
																																					{
																																						obj_t
																																							BgL_nullzd2valuezd2_4860;
																																						obj_t
																																							BgL_parentz00_4859;
																																						obj_t
																																							BgL_namez00_4858;
																																						obj_t
																																							BgL_childz00_4857;
																																						BgL_childz00_4857
																																							=
																																							BgL_carzd23925zd2_653;
																																						BgL_namez00_4858
																																							=
																																							BgL_carzd23935zd2_657;
																																						BgL_parentz00_4859
																																							=
																																							BgL_carzd23941zd2_661;
																																						BgL_nullzd2valuezd2_4860
																																							=
																																							BgL_carzd23946zd2_665;
																																						BgL_nullzd2valuezd2_229
																																							=
																																							BgL_nullzd2valuezd2_4860;
																																						BgL_parentz00_228
																																							=
																																							BgL_parentz00_4859;
																																						BgL_namez00_227
																																							=
																																							BgL_namez00_4858;
																																						BgL_childz00_226
																																							=
																																							BgL_childz00_4857;
																																						goto
																																							BgL_tagzd2371zd2_230;
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						if (
																							(CAR(
																									((obj_t) BgL_clausez00_28)) ==
																								CNST_TABLE_REF(7)))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd24092zd2_673;
																								obj_t BgL_cdrzd24093zd2_674;

																								BgL_carzd24092zd2_673 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd23919zd2_650));
																								BgL_cdrzd24093zd2_674 =
																									CDR(((obj_t)
																										BgL_cdrzd23919zd2_650));
																								if (SYMBOLP
																									(BgL_carzd24092zd2_673))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd24093zd2_674))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd24100zd2_677;
																												BgL_carzd24100zd2_677 =
																													CAR
																													(BgL_cdrzd24093zd2_674);
																												if (PAIRP
																													(BgL_carzd24100zd2_677))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd24103zd2_679;
																														BgL_carzd24103zd2_679
																															=
																															CAR
																															(BgL_carzd24100zd2_677);
																														if (SYMBOLP
																															(BgL_carzd24103zd2_679))
																															{	/* Module/type.scm 58 */
																																if (NULLP(CDR
																																		(BgL_carzd24100zd2_677)))
																																	{	/* Module/type.scm 58 */
																																		if (NULLP
																																			(CDR
																																				(BgL_cdrzd24093zd2_674)))
																																			{	/* Module/type.scm 58 */
																																				return
																																					BGl_delayzd2tvectorzd2typez12z12zzmodule_typez00
																																					(BgL_carzd24092zd2_673,
																																					BgL_carzd24103zd2_679,
																																					BgL_clausez00_28,
																																					BgL_importz00_27);
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								obj_t BgL_cdrzd24166zd2_687;

																								BgL_cdrzd24166zd2_687 =
																									CDR(
																									((obj_t) BgL_clausez00_28));
																								if (
																									(CAR(
																											((obj_t)
																												BgL_clausez00_28)) ==
																										CNST_TABLE_REF(6)))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd24172zd2_690;
																										obj_t BgL_cdrzd24173zd2_691;

																										BgL_carzd24172zd2_690 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd24166zd2_687));
																										BgL_cdrzd24173zd2_691 =
																											CDR(((obj_t)
																												BgL_cdrzd24166zd2_687));
																										if (SYMBOLP
																											(BgL_carzd24172zd2_690))
																											{	/* Module/type.scm 58 */
																												if (PAIRP
																													(BgL_cdrzd24173zd2_691))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd24182zd2_694;
																														obj_t
																															BgL_cdrzd24183zd2_695;
																														BgL_carzd24182zd2_694
																															=
																															CAR
																															(BgL_cdrzd24173zd2_691);
																														BgL_cdrzd24183zd2_695
																															=
																															CDR
																															(BgL_cdrzd24173zd2_691);
																														if (SYMBOLP
																															(BgL_carzd24182zd2_694))
																															{	/* Module/type.scm 58 */
																																if (PAIRP
																																	(BgL_cdrzd24183zd2_695))
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_cdrzd24190zd2_698;
																																		BgL_cdrzd24190zd2_698
																																			=
																																			CDR
																																			(BgL_cdrzd24183zd2_695);
																																		if (PAIRP
																																			(BgL_cdrzd24190zd2_698))
																																			{	/* Module/type.scm 58 */
																																				if (NULLP(CDR(BgL_cdrzd24190zd2_698)))
																																					{
																																						obj_t
																																							BgL_coercez00_4918;
																																						obj_t
																																							BgL_checkz00_4916;
																																						obj_t
																																							BgL_toz00_4915;
																																						obj_t
																																							BgL_fromz00_4914;
																																						BgL_fromz00_4914
																																							=
																																							BgL_carzd24172zd2_690;
																																						BgL_toz00_4915
																																							=
																																							BgL_carzd24182zd2_694;
																																						BgL_checkz00_4916
																																							=
																																							CAR
																																							(BgL_cdrzd24183zd2_695);
																																						BgL_coercez00_4918
																																							=
																																							CAR
																																							(BgL_cdrzd24190zd2_698);
																																						BgL_coercez00_237
																																							=
																																							BgL_coercez00_4918;
																																						BgL_checkz00_236
																																							=
																																							BgL_checkz00_4916;
																																						BgL_toz00_235
																																							=
																																							BgL_toz00_4915;
																																						BgL_fromz00_234
																																							=
																																							BgL_fromz00_4914;
																																						goto
																																							BgL_tagzd2373zd2_238;
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
										else
											{	/* Module/type.scm 58 */
												obj_t BgL_carzd25544zd2_873;
												obj_t BgL_cdrzd25545zd2_874;

												BgL_carzd25544zd2_873 =
													CAR(((obj_t) BgL_cdrzd2390zd2_243));
												BgL_cdrzd25545zd2_874 =
													CDR(((obj_t) BgL_cdrzd2390zd2_243));
												if (STRINGP(BgL_carzd25544zd2_873))
													{	/* Module/type.scm 58 */
														if (PAIRP(BgL_cdrzd25545zd2_874))
															{	/* Module/type.scm 58 */
																obj_t BgL_carzd25551zd2_877;

																BgL_carzd25551zd2_877 =
																	CAR(BgL_cdrzd25545zd2_874);
																if (SYMBOLP(BgL_carzd25551zd2_877))
																	{	/* Module/type.scm 58 */
																		if (NULLP(CDR(BgL_cdrzd25545zd2_874)))
																			{	/* Module/type.scm 58 */
																				return
																					((obj_t)
																					BGl_declarezd2typez12zc0zztype_envz00
																					(BgL_carzd2389zd2_242,
																						BgL_carzd25544zd2_873,
																						BgL_carzd25551zd2_877));
																			}
																		else
																			{	/* Module/type.scm 58 */
																				if (
																					(CAR(
																							((obj_t) BgL_clausez00_28)) ==
																						CNST_TABLE_REF(5)))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd25578zd2_884;
																						obj_t BgL_cdrzd25579zd2_885;

																						BgL_carzd25578zd2_884 =
																							CAR(
																							((obj_t) BgL_cdrzd2390zd2_243));
																						BgL_cdrzd25579zd2_885 =
																							CDR(
																							((obj_t) BgL_cdrzd2390zd2_243));
																						if (SYMBOLP(BgL_carzd25578zd2_884))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd25587zd2_887;
																								obj_t BgL_cdrzd25588zd2_888;

																								BgL_carzd25587zd2_887 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd25579zd2_885));
																								BgL_cdrzd25588zd2_888 =
																									CDR(((obj_t)
																										BgL_cdrzd25579zd2_885));
																								if (STRINGP
																									(BgL_carzd25587zd2_887))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd25588zd2_888))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd25594zd2_891;
																												BgL_carzd25594zd2_891 =
																													CAR
																													(BgL_cdrzd25588zd2_888);
																												if (SYMBOLP
																													(BgL_carzd25594zd2_891))
																													{	/* Module/type.scm 58 */
																														if (NULLP(CDR
																																(BgL_cdrzd25588zd2_888)))
																															{	/* Module/type.scm 69 */
																																BgL_typez00_bglt
																																	BgL_typez00_3002;
																																BgL_typez00_3002
																																	=
																																	BGl_declarezd2typez12zc0zztype_envz00
																																	(BgL_carzd25578zd2_884,
																																	BgL_carzd25587zd2_887,
																																	BgL_carzd25594zd2_891);
																																((((BgL_typez00_bglt) COBJECT(BgL_typez00_3002))->BgL_magiczf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																																return
																																	((obj_t)
																																	BgL_typez00_3002);
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						if (
																							(CAR(
																									((obj_t) BgL_clausez00_28)) ==
																								CNST_TABLE_REF(1)))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd25835zd2_899;
																								obj_t BgL_cdrzd25836zd2_900;

																								BgL_carzd25835zd2_899 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2390zd2_243));
																								BgL_cdrzd25836zd2_900 =
																									CDR(((obj_t)
																										BgL_cdrzd2390zd2_243));
																								if (SYMBOLP
																									(BgL_carzd25835zd2_899))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd25845zd2_902;
																										obj_t BgL_cdrzd25846zd2_903;

																										BgL_carzd25845zd2_902 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd25836zd2_900));
																										BgL_cdrzd25846zd2_903 =
																											CDR(((obj_t)
																												BgL_cdrzd25836zd2_900));
																										if (STRINGP
																											(BgL_carzd25845zd2_902))
																											{	/* Module/type.scm 58 */
																												if (PAIRP
																													(BgL_cdrzd25846zd2_903))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd25852zd2_906;
																														BgL_carzd25852zd2_906
																															=
																															CAR
																															(BgL_cdrzd25846zd2_903);
																														if (PAIRP
																															(BgL_carzd25852zd2_906))
																															{	/* Module/type.scm 58 */
																																if (NULLP(CDR
																																		(BgL_cdrzd25846zd2_903)))
																																	{
																																		obj_t
																																			BgL_parentz00_4991;
																																		obj_t
																																			BgL_namez00_4990;
																																		obj_t
																																			BgL_childz00_4989;
																																		BgL_childz00_4989
																																			=
																																			BgL_carzd25835zd2_899;
																																		BgL_namez00_4990
																																			=
																																			BgL_carzd25845zd2_902;
																																		BgL_parentz00_4991
																																			=
																																			BgL_carzd25852zd2_906;
																																		BgL_parentz00_224
																																			=
																																			BgL_parentz00_4991;
																																		BgL_namez00_223
																																			=
																																			BgL_namez00_4990;
																																		BgL_childz00_222
																																			=
																																			BgL_childz00_4989;
																																		goto
																																			BgL_tagzd2370zd2_225;
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_cdrzd25872zd2_910;
																																		BgL_cdrzd25872zd2_910
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_clausez00_28));
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd25878zd2_911;
																																			obj_t
																																				BgL_cdrzd25879zd2_912;
																																			BgL_carzd25878zd2_911
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd25872zd2_910));
																																			BgL_cdrzd25879zd2_912
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd25872zd2_910));
																																			if (SYMBOLP(BgL_carzd25878zd2_911))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd25887zd2_914;
																																					obj_t
																																						BgL_cdrzd25888zd2_915;
																																					BgL_carzd25887zd2_914
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd25879zd2_912));
																																					BgL_cdrzd25888zd2_915
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd25879zd2_912));
																																					if (STRINGP(BgL_carzd25887zd2_914))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd25894zd2_917;
																																							obj_t
																																								BgL_cdrzd25895zd2_918;
																																							BgL_carzd25894zd2_917
																																								=
																																								CAR
																																								(
																																								((obj_t) BgL_cdrzd25888zd2_915));
																																							BgL_cdrzd25895zd2_918
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_cdrzd25888zd2_915));
																																							if (PAIRP(BgL_carzd25894zd2_917))
																																								{	/* Module/type.scm 58 */
																																									if (PAIRP(BgL_cdrzd25895zd2_918))
																																										{	/* Module/type.scm 58 */
																																											obj_t
																																												BgL_carzd25901zd2_921;
																																											BgL_carzd25901zd2_921
																																												=
																																												CAR
																																												(BgL_cdrzd25895zd2_918);
																																											if (SYMBOLP(BgL_carzd25901zd2_921))
																																												{	/* Module/type.scm 58 */
																																													if (NULLP(CDR(BgL_cdrzd25895zd2_918)))
																																														{
																																															obj_t
																																																BgL_nullzd2valuezd2_5023;
																																															obj_t
																																																BgL_parentz00_5022;
																																															obj_t
																																																BgL_namez00_5021;
																																															obj_t
																																																BgL_childz00_5020;
																																															BgL_childz00_5020
																																																=
																																																BgL_carzd25878zd2_911;
																																															BgL_namez00_5021
																																																=
																																																BgL_carzd25887zd2_914;
																																															BgL_parentz00_5022
																																																=
																																																BgL_carzd25894zd2_917;
																																															BgL_nullzd2valuezd2_5023
																																																=
																																																BgL_carzd25901zd2_921;
																																															BgL_nullzd2valuezd2_229
																																																=
																																																BgL_nullzd2valuezd2_5023;
																																															BgL_parentz00_228
																																																=
																																																BgL_parentz00_5022;
																																															BgL_namez00_227
																																																=
																																																BgL_namez00_5021;
																																															BgL_childz00_226
																																																=
																																																BgL_childz00_5020;
																																															goto
																																																BgL_tagzd2371zd2_230;
																																														}
																																													else
																																														{	/* Module/type.scm 58 */
																																															goto
																																																BgL_tagzd2374zd2_239;
																																														}
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_cdrzd26017zd2_927;
																																BgL_cdrzd26017zd2_927
																																	=
																																	CDR(((obj_t)
																																		BgL_clausez00_28));
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd26023zd2_928;
																																	obj_t
																																		BgL_cdrzd26024zd2_929;
																																	BgL_carzd26023zd2_928
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd26017zd2_927));
																																	BgL_cdrzd26024zd2_929
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd26017zd2_927));
																																	if (SYMBOLP
																																		(BgL_carzd26023zd2_928))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd26032zd2_931;
																																			obj_t
																																				BgL_cdrzd26033zd2_932;
																																			BgL_carzd26032zd2_931
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd26024zd2_929));
																																			BgL_cdrzd26033zd2_932
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd26024zd2_929));
																																			if (STRINGP(BgL_carzd26032zd2_931))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd26039zd2_934;
																																					obj_t
																																						BgL_cdrzd26040zd2_935;
																																					BgL_carzd26039zd2_934
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd26033zd2_932));
																																					BgL_cdrzd26040zd2_935
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd26033zd2_932));
																																					if (PAIRP(BgL_carzd26039zd2_934))
																																						{	/* Module/type.scm 58 */
																																							if (PAIRP(BgL_cdrzd26040zd2_935))
																																								{	/* Module/type.scm 58 */
																																									obj_t
																																										BgL_carzd26046zd2_938;
																																									BgL_carzd26046zd2_938
																																										=
																																										CAR
																																										(BgL_cdrzd26040zd2_935);
																																									if (SYMBOLP(BgL_carzd26046zd2_938))
																																										{	/* Module/type.scm 58 */
																																											if (NULLP(CDR(BgL_cdrzd26040zd2_935)))
																																												{
																																													obj_t
																																														BgL_nullzd2valuezd2_5055;
																																													obj_t
																																														BgL_parentz00_5054;
																																													obj_t
																																														BgL_namez00_5053;
																																													obj_t
																																														BgL_childz00_5052;
																																													BgL_childz00_5052
																																														=
																																														BgL_carzd26023zd2_928;
																																													BgL_namez00_5053
																																														=
																																														BgL_carzd26032zd2_931;
																																													BgL_parentz00_5054
																																														=
																																														BgL_carzd26039zd2_934;
																																													BgL_nullzd2valuezd2_5055
																																														=
																																														BgL_carzd26046zd2_938;
																																													BgL_nullzd2valuezd2_229
																																														=
																																														BgL_nullzd2valuezd2_5055;
																																													BgL_parentz00_228
																																														=
																																														BgL_parentz00_5054;
																																													BgL_namez00_227
																																														=
																																														BgL_namez00_5053;
																																													BgL_childz00_226
																																														=
																																														BgL_childz00_5052;
																																													goto
																																														BgL_tagzd2371zd2_230;
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_cdrzd26191zd2_943;
																												BgL_cdrzd26191zd2_943 =
																													CDR(((obj_t)
																														BgL_clausez00_28));
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd26196zd2_944;
																													obj_t
																														BgL_cdrzd26197zd2_945;
																													BgL_carzd26196zd2_944
																														=
																														CAR(((obj_t)
																															BgL_cdrzd26191zd2_943));
																													BgL_cdrzd26197zd2_945
																														=
																														CDR(((obj_t)
																															BgL_cdrzd26191zd2_943));
																													if (SYMBOLP
																														(BgL_carzd26196zd2_944))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd26204zd2_947;
																															obj_t
																																BgL_cdrzd26205zd2_948;
																															BgL_carzd26204zd2_947
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd26197zd2_945));
																															BgL_cdrzd26205zd2_948
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd26197zd2_945));
																															if (STRINGP
																																(BgL_carzd26204zd2_947))
																																{	/* Module/type.scm 58 */
																																	if (PAIRP
																																		(BgL_cdrzd26205zd2_948))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd26210zd2_951;
																																			obj_t
																																				BgL_cdrzd26211zd2_952;
																																			BgL_carzd26210zd2_951
																																				=
																																				CAR
																																				(BgL_cdrzd26205zd2_948);
																																			BgL_cdrzd26211zd2_952
																																				=
																																				CDR
																																				(BgL_cdrzd26205zd2_948);
																																			if (PAIRP
																																				(BgL_carzd26210zd2_951))
																																				{	/* Module/type.scm 58 */
																																					if (PAIRP(BgL_cdrzd26211zd2_952))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd26215zd2_955;
																																							BgL_carzd26215zd2_955
																																								=
																																								CAR
																																								(BgL_cdrzd26211zd2_952);
																																							if (SYMBOLP(BgL_carzd26215zd2_955))
																																								{	/* Module/type.scm 58 */
																																									if (NULLP(CDR(BgL_cdrzd26211zd2_952)))
																																										{
																																											obj_t
																																												BgL_nullzd2valuezd2_5087;
																																											obj_t
																																												BgL_parentz00_5086;
																																											obj_t
																																												BgL_namez00_5085;
																																											obj_t
																																												BgL_childz00_5084;
																																											BgL_childz00_5084
																																												=
																																												BgL_carzd26196zd2_944;
																																											BgL_namez00_5085
																																												=
																																												BgL_carzd26204zd2_947;
																																											BgL_parentz00_5086
																																												=
																																												BgL_carzd26210zd2_951;
																																											BgL_nullzd2valuezd2_5087
																																												=
																																												BgL_carzd26215zd2_955;
																																											BgL_nullzd2valuezd2_229
																																												=
																																												BgL_nullzd2valuezd2_5087;
																																											BgL_parentz00_228
																																												=
																																												BgL_parentz00_5086;
																																											BgL_namez00_227
																																												=
																																												BgL_namez00_5085;
																																											BgL_childz00_226
																																												=
																																												BgL_childz00_5084;
																																											goto
																																												BgL_tagzd2371zd2_230;
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										obj_t BgL_cdrzd26348zd2_960;

																										BgL_cdrzd26348zd2_960 =
																											CDR(
																											((obj_t)
																												BgL_clausez00_28));
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd26355zd2_961;
																											obj_t
																												BgL_cdrzd26356zd2_962;
																											BgL_carzd26355zd2_961 =
																												CAR(((obj_t)
																													BgL_cdrzd26348zd2_960));
																											BgL_cdrzd26356zd2_962 =
																												CDR(((obj_t)
																													BgL_cdrzd26348zd2_960));
																											if (SYMBOLP
																												(BgL_carzd26355zd2_961))
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd26366zd2_964;
																													obj_t
																														BgL_cdrzd26367zd2_965;
																													BgL_carzd26366zd2_964
																														=
																														CAR(((obj_t)
																															BgL_cdrzd26356zd2_962));
																													BgL_cdrzd26367zd2_965
																														=
																														CDR(((obj_t)
																															BgL_cdrzd26356zd2_962));
																													if (STRINGP
																														(BgL_carzd26366zd2_964))
																														{	/* Module/type.scm 58 */
																															if (PAIRP
																																(BgL_cdrzd26367zd2_965))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd26374zd2_968;
																																	obj_t
																																		BgL_cdrzd26375zd2_969;
																																	BgL_carzd26374zd2_968
																																		=
																																		CAR
																																		(BgL_cdrzd26367zd2_965);
																																	BgL_cdrzd26375zd2_969
																																		=
																																		CDR
																																		(BgL_cdrzd26367zd2_965);
																																	if (PAIRP
																																		(BgL_carzd26374zd2_968))
																																		{	/* Module/type.scm 58 */
																																			if (PAIRP
																																				(BgL_cdrzd26375zd2_969))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd26379zd2_972;
																																					BgL_carzd26379zd2_972
																																						=
																																						CAR
																																						(BgL_cdrzd26375zd2_969);
																																					if (SYMBOLP(BgL_carzd26379zd2_972))
																																						{	/* Module/type.scm 58 */
																																							if (NULLP(CDR(BgL_cdrzd26375zd2_969)))
																																								{
																																									obj_t
																																										BgL_nullzd2valuezd2_5119;
																																									obj_t
																																										BgL_parentz00_5118;
																																									obj_t
																																										BgL_namez00_5117;
																																									obj_t
																																										BgL_childz00_5116;
																																									BgL_childz00_5116
																																										=
																																										BgL_carzd26355zd2_961;
																																									BgL_namez00_5117
																																										=
																																										BgL_carzd26366zd2_964;
																																									BgL_parentz00_5118
																																										=
																																										BgL_carzd26374zd2_968;
																																									BgL_nullzd2valuezd2_5119
																																										=
																																										BgL_carzd26379zd2_972;
																																									BgL_nullzd2valuezd2_229
																																										=
																																										BgL_nullzd2valuezd2_5119;
																																									BgL_parentz00_228
																																										=
																																										BgL_parentz00_5118;
																																									BgL_namez00_227
																																										=
																																										BgL_namez00_5117;
																																									BgL_childz00_226
																																										=
																																										BgL_childz00_5116;
																																									goto
																																										BgL_tagzd2371zd2_230;
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								obj_t BgL_cdrzd26512zd2_977;

																								BgL_cdrzd26512zd2_977 =
																									CDR(
																									((obj_t) BgL_clausez00_28));
																								if (
																									(CAR(
																											((obj_t)
																												BgL_clausez00_28)) ==
																										CNST_TABLE_REF(1)))
																									{	/* Module/type.scm 58 */
																										obj_t BgL_carzd26519zd2_980;
																										obj_t BgL_cdrzd26520zd2_981;

																										BgL_carzd26519zd2_980 =
																											CAR(
																											((obj_t)
																												BgL_cdrzd26512zd2_977));
																										BgL_cdrzd26520zd2_981 =
																											CDR(((obj_t)
																												BgL_cdrzd26512zd2_977));
																										if (SYMBOLP
																											(BgL_carzd26519zd2_980))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd26530zd2_983;
																												obj_t
																													BgL_cdrzd26531zd2_984;
																												BgL_carzd26530zd2_983 =
																													CAR(((obj_t)
																														BgL_cdrzd26520zd2_981));
																												BgL_cdrzd26531zd2_984 =
																													CDR(((obj_t)
																														BgL_cdrzd26520zd2_981));
																												if (STRINGP
																													(BgL_carzd26530zd2_983))
																													{	/* Module/type.scm 58 */
																														if (PAIRP
																															(BgL_cdrzd26531zd2_984))
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_carzd26538zd2_987;
																																obj_t
																																	BgL_cdrzd26539zd2_988;
																																BgL_carzd26538zd2_987
																																	=
																																	CAR
																																	(BgL_cdrzd26531zd2_984);
																																BgL_cdrzd26539zd2_988
																																	=
																																	CDR
																																	(BgL_cdrzd26531zd2_984);
																																if (PAIRP
																																	(BgL_carzd26538zd2_987))
																																	{	/* Module/type.scm 58 */
																																		if (PAIRP
																																			(BgL_cdrzd26539zd2_988))
																																			{	/* Module/type.scm 58 */
																																				obj_t
																																					BgL_carzd26543zd2_991;
																																				BgL_carzd26543zd2_991
																																					=
																																					CAR
																																					(BgL_cdrzd26539zd2_988);
																																				if (SYMBOLP(BgL_carzd26543zd2_991))
																																					{	/* Module/type.scm 58 */
																																						if (NULLP(CDR(BgL_cdrzd26539zd2_988)))
																																							{
																																								obj_t
																																									BgL_nullzd2valuezd2_5156;
																																								obj_t
																																									BgL_parentz00_5155;
																																								obj_t
																																									BgL_namez00_5154;
																																								obj_t
																																									BgL_childz00_5153;
																																								BgL_childz00_5153
																																									=
																																									BgL_carzd26519zd2_980;
																																								BgL_namez00_5154
																																									=
																																									BgL_carzd26530zd2_983;
																																								BgL_parentz00_5155
																																									=
																																									BgL_carzd26538zd2_987;
																																								BgL_nullzd2valuezd2_5156
																																									=
																																									BgL_carzd26543zd2_991;
																																								BgL_nullzd2valuezd2_229
																																									=
																																									BgL_nullzd2valuezd2_5156;
																																								BgL_parentz00_228
																																									=
																																									BgL_parentz00_5155;
																																								BgL_namez00_227
																																									=
																																									BgL_namez00_5154;
																																								BgL_childz00_226
																																									=
																																									BgL_childz00_5153;
																																								goto
																																									BgL_tagzd2371zd2_230;
																																							}
																																						else
																																							{	/* Module/type.scm 58 */
																																								goto
																																									BgL_tagzd2374zd2_239;
																																							}
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										if (
																											(CAR(
																													((obj_t)
																														BgL_clausez00_28))
																												== CNST_TABLE_REF(6)))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd26686zd2_999;
																												obj_t
																													BgL_cdrzd26687zd2_1000;
																												BgL_carzd26686zd2_999 =
																													CAR(((obj_t)
																														BgL_cdrzd26512zd2_977));
																												BgL_cdrzd26687zd2_1000 =
																													CDR(((obj_t)
																														BgL_cdrzd26512zd2_977));
																												if (SYMBOLP
																													(BgL_carzd26686zd2_999))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd26697zd2_1002;
																														obj_t
																															BgL_cdrzd26698zd2_1003;
																														BgL_carzd26697zd2_1002
																															=
																															CAR(((obj_t)
																																BgL_cdrzd26687zd2_1000));
																														BgL_cdrzd26698zd2_1003
																															=
																															CDR(((obj_t)
																																BgL_cdrzd26687zd2_1000));
																														if (SYMBOLP
																															(BgL_carzd26697zd2_1002))
																															{	/* Module/type.scm 58 */
																																if (PAIRP
																																	(BgL_cdrzd26698zd2_1003))
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_cdrzd26707zd2_1006;
																																		BgL_cdrzd26707zd2_1006
																																			=
																																			CDR
																																			(BgL_cdrzd26698zd2_1003);
																																		if (PAIRP
																																			(BgL_cdrzd26707zd2_1006))
																																			{	/* Module/type.scm 58 */
																																				if (NULLP(CDR(BgL_cdrzd26707zd2_1006)))
																																					{
																																						obj_t
																																							BgL_coercez00_5186;
																																						obj_t
																																							BgL_checkz00_5184;
																																						obj_t
																																							BgL_toz00_5183;
																																						obj_t
																																							BgL_fromz00_5182;
																																						BgL_fromz00_5182
																																							=
																																							BgL_carzd26686zd2_999;
																																						BgL_toz00_5183
																																							=
																																							BgL_carzd26697zd2_1002;
																																						BgL_checkz00_5184
																																							=
																																							CAR
																																							(BgL_cdrzd26698zd2_1003);
																																						BgL_coercez00_5186
																																							=
																																							CAR
																																							(BgL_cdrzd26707zd2_1006);
																																						BgL_coercez00_237
																																							=
																																							BgL_coercez00_5186;
																																						BgL_checkz00_236
																																							=
																																							BgL_checkz00_5184;
																																						BgL_toz00_235
																																							=
																																							BgL_toz00_5183;
																																						BgL_fromz00_234
																																							=
																																							BgL_fromz00_5182;
																																						goto
																																							BgL_tagzd2373zd2_238;
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																							}
																					}
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		if (
																			(CAR(
																					((obj_t) BgL_clausez00_28)) ==
																				CNST_TABLE_REF(5)))
																			{	/* Module/type.scm 58 */
																				obj_t BgL_carzd26736zd2_1021;
																				obj_t BgL_cdrzd26737zd2_1022;

																				BgL_carzd26736zd2_1021 =
																					CAR(((obj_t) BgL_cdrzd2390zd2_243));
																				BgL_cdrzd26737zd2_1022 =
																					CDR(((obj_t) BgL_cdrzd2390zd2_243));
																				if (SYMBOLP(BgL_carzd26736zd2_1021))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd26745zd2_1024;
																						obj_t BgL_cdrzd26746zd2_1025;

																						BgL_carzd26745zd2_1024 =
																							CAR(
																							((obj_t) BgL_cdrzd26737zd2_1022));
																						BgL_cdrzd26746zd2_1025 =
																							CDR(
																							((obj_t) BgL_cdrzd26737zd2_1022));
																						if (STRINGP(BgL_carzd26745zd2_1024))
																							{	/* Module/type.scm 58 */
																								if (PAIRP
																									(BgL_cdrzd26746zd2_1025))
																									{	/* Module/type.scm 58 */
																										obj_t
																											BgL_carzd26752zd2_1028;
																										BgL_carzd26752zd2_1028 =
																											CAR
																											(BgL_cdrzd26746zd2_1025);
																										if (SYMBOLP
																											(BgL_carzd26752zd2_1028))
																											{	/* Module/type.scm 58 */
																												if (NULLP(CDR
																														(BgL_cdrzd26746zd2_1025)))
																													{	/* Module/type.scm 69 */
																														BgL_typez00_bglt
																															BgL_typez00_3077;
																														BgL_typez00_3077 =
																															BGl_declarezd2typez12zc0zztype_envz00
																															(BgL_carzd26736zd2_1021,
																															BgL_carzd26745zd2_1024,
																															BgL_carzd26752zd2_1028);
																														((((BgL_typez00_bglt) COBJECT(BgL_typez00_3077))->BgL_magiczf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																														return
																															((obj_t)
																															BgL_typez00_3077);
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				if (
																					(CAR(
																							((obj_t) BgL_clausez00_28)) ==
																						CNST_TABLE_REF(1)))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd26993zd2_1036;
																						obj_t BgL_cdrzd26994zd2_1037;

																						BgL_carzd26993zd2_1036 =
																							CAR(
																							((obj_t) BgL_cdrzd2390zd2_243));
																						BgL_cdrzd26994zd2_1037 =
																							CDR(
																							((obj_t) BgL_cdrzd2390zd2_243));
																						if (SYMBOLP(BgL_carzd26993zd2_1036))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd27003zd2_1039;
																								obj_t BgL_cdrzd27004zd2_1040;

																								BgL_carzd27003zd2_1039 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd26994zd2_1037));
																								BgL_cdrzd27004zd2_1040 =
																									CDR(((obj_t)
																										BgL_cdrzd26994zd2_1037));
																								if (STRINGP
																									(BgL_carzd27003zd2_1039))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd27004zd2_1040))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd27010zd2_1043;
																												BgL_carzd27010zd2_1043 =
																													CAR
																													(BgL_cdrzd27004zd2_1040);
																												if (PAIRP
																													(BgL_carzd27010zd2_1043))
																													{	/* Module/type.scm 58 */
																														if (NULLP(CDR
																																(BgL_cdrzd27004zd2_1040)))
																															{
																																obj_t
																																	BgL_parentz00_5243;
																																obj_t
																																	BgL_namez00_5242;
																																obj_t
																																	BgL_childz00_5241;
																																BgL_childz00_5241
																																	=
																																	BgL_carzd26993zd2_1036;
																																BgL_namez00_5242
																																	=
																																	BgL_carzd27003zd2_1039;
																																BgL_parentz00_5243
																																	=
																																	BgL_carzd27010zd2_1043;
																																BgL_parentz00_224
																																	=
																																	BgL_parentz00_5243;
																																BgL_namez00_223
																																	=
																																	BgL_namez00_5242;
																																BgL_childz00_222
																																	=
																																	BgL_childz00_5241;
																																goto
																																	BgL_tagzd2370zd2_225;
																															}
																														else
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_cdrzd27030zd2_1047;
																																BgL_cdrzd27030zd2_1047
																																	=
																																	CDR(((obj_t)
																																		BgL_clausez00_28));
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd27036zd2_1048;
																																	obj_t
																																		BgL_cdrzd27037zd2_1049;
																																	BgL_carzd27036zd2_1048
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd27030zd2_1047));
																																	BgL_cdrzd27037zd2_1049
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd27030zd2_1047));
																																	if (SYMBOLP
																																		(BgL_carzd27036zd2_1048))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd27045zd2_1051;
																																			obj_t
																																				BgL_cdrzd27046zd2_1052;
																																			BgL_carzd27045zd2_1051
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd27037zd2_1049));
																																			BgL_cdrzd27046zd2_1052
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd27037zd2_1049));
																																			if (STRINGP(BgL_carzd27045zd2_1051))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd27052zd2_1054;
																																					obj_t
																																						BgL_cdrzd27053zd2_1055;
																																					BgL_carzd27052zd2_1054
																																						=
																																						CAR(
																																						((obj_t) BgL_cdrzd27046zd2_1052));
																																					BgL_cdrzd27053zd2_1055
																																						=
																																						CDR(
																																						((obj_t) BgL_cdrzd27046zd2_1052));
																																					if (PAIRP(BgL_carzd27052zd2_1054))
																																						{	/* Module/type.scm 58 */
																																							if (PAIRP(BgL_cdrzd27053zd2_1055))
																																								{	/* Module/type.scm 58 */
																																									obj_t
																																										BgL_carzd27059zd2_1058;
																																									BgL_carzd27059zd2_1058
																																										=
																																										CAR
																																										(BgL_cdrzd27053zd2_1055);
																																									if (SYMBOLP(BgL_carzd27059zd2_1058))
																																										{	/* Module/type.scm 58 */
																																											if (NULLP(CDR(BgL_cdrzd27053zd2_1055)))
																																												{
																																													obj_t
																																														BgL_nullzd2valuezd2_5275;
																																													obj_t
																																														BgL_parentz00_5274;
																																													obj_t
																																														BgL_namez00_5273;
																																													obj_t
																																														BgL_childz00_5272;
																																													BgL_childz00_5272
																																														=
																																														BgL_carzd27036zd2_1048;
																																													BgL_namez00_5273
																																														=
																																														BgL_carzd27045zd2_1051;
																																													BgL_parentz00_5274
																																														=
																																														BgL_carzd27052zd2_1054;
																																													BgL_nullzd2valuezd2_5275
																																														=
																																														BgL_carzd27059zd2_1058;
																																													BgL_nullzd2valuezd2_229
																																														=
																																														BgL_nullzd2valuezd2_5275;
																																													BgL_parentz00_228
																																														=
																																														BgL_parentz00_5274;
																																													BgL_namez00_227
																																														=
																																														BgL_namez00_5273;
																																													BgL_childz00_226
																																														=
																																														BgL_childz00_5272;
																																													goto
																																														BgL_tagzd2371zd2_230;
																																												}
																																											else
																																												{	/* Module/type.scm 58 */
																																													goto
																																														BgL_tagzd2374zd2_239;
																																												}
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_cdrzd27175zd2_1064;
																														BgL_cdrzd27175zd2_1064
																															=
																															CDR(((obj_t)
																																BgL_clausez00_28));
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd27181zd2_1065;
																															obj_t
																																BgL_cdrzd27182zd2_1066;
																															BgL_carzd27181zd2_1065
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd27175zd2_1064));
																															BgL_cdrzd27182zd2_1066
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd27175zd2_1064));
																															if (SYMBOLP
																																(BgL_carzd27181zd2_1065))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd27190zd2_1068;
																																	obj_t
																																		BgL_cdrzd27191zd2_1069;
																																	BgL_carzd27190zd2_1068
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd27182zd2_1066));
																																	BgL_cdrzd27191zd2_1069
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd27182zd2_1066));
																																	if (STRINGP
																																		(BgL_carzd27190zd2_1068))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd27197zd2_1071;
																																			obj_t
																																				BgL_cdrzd27198zd2_1072;
																																			BgL_carzd27197zd2_1071
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd27191zd2_1069));
																																			BgL_cdrzd27198zd2_1072
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd27191zd2_1069));
																																			if (PAIRP
																																				(BgL_carzd27197zd2_1071))
																																				{	/* Module/type.scm 58 */
																																					if (PAIRP(BgL_cdrzd27198zd2_1072))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd27204zd2_1075;
																																							BgL_carzd27204zd2_1075
																																								=
																																								CAR
																																								(BgL_cdrzd27198zd2_1072);
																																							if (SYMBOLP(BgL_carzd27204zd2_1075))
																																								{	/* Module/type.scm 58 */
																																									if (NULLP(CDR(BgL_cdrzd27198zd2_1072)))
																																										{
																																											obj_t
																																												BgL_nullzd2valuezd2_5307;
																																											obj_t
																																												BgL_parentz00_5306;
																																											obj_t
																																												BgL_namez00_5305;
																																											obj_t
																																												BgL_childz00_5304;
																																											BgL_childz00_5304
																																												=
																																												BgL_carzd27181zd2_1065;
																																											BgL_namez00_5305
																																												=
																																												BgL_carzd27190zd2_1068;
																																											BgL_parentz00_5306
																																												=
																																												BgL_carzd27197zd2_1071;
																																											BgL_nullzd2valuezd2_5307
																																												=
																																												BgL_carzd27204zd2_1075;
																																											BgL_nullzd2valuezd2_229
																																												=
																																												BgL_nullzd2valuezd2_5307;
																																											BgL_parentz00_228
																																												=
																																												BgL_parentz00_5306;
																																											BgL_namez00_227
																																												=
																																												BgL_namez00_5305;
																																											BgL_childz00_226
																																												=
																																												BgL_childz00_5304;
																																											goto
																																												BgL_tagzd2371zd2_230;
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										obj_t
																											BgL_cdrzd27349zd2_1080;
																										BgL_cdrzd27349zd2_1080 =
																											CDR(((obj_t)
																												BgL_clausez00_28));
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd27354zd2_1081;
																											obj_t
																												BgL_cdrzd27355zd2_1082;
																											BgL_carzd27354zd2_1081 =
																												CAR(((obj_t)
																													BgL_cdrzd27349zd2_1080));
																											BgL_cdrzd27355zd2_1082 =
																												CDR(((obj_t)
																													BgL_cdrzd27349zd2_1080));
																											if (SYMBOLP
																												(BgL_carzd27354zd2_1081))
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd27362zd2_1084;
																													obj_t
																														BgL_cdrzd27363zd2_1085;
																													BgL_carzd27362zd2_1084
																														=
																														CAR(((obj_t)
																															BgL_cdrzd27355zd2_1082));
																													BgL_cdrzd27363zd2_1085
																														=
																														CDR(((obj_t)
																															BgL_cdrzd27355zd2_1082));
																													if (STRINGP
																														(BgL_carzd27362zd2_1084))
																														{	/* Module/type.scm 58 */
																															if (PAIRP
																																(BgL_cdrzd27363zd2_1085))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd27368zd2_1088;
																																	obj_t
																																		BgL_cdrzd27369zd2_1089;
																																	BgL_carzd27368zd2_1088
																																		=
																																		CAR
																																		(BgL_cdrzd27363zd2_1085);
																																	BgL_cdrzd27369zd2_1089
																																		=
																																		CDR
																																		(BgL_cdrzd27363zd2_1085);
																																	if (PAIRP
																																		(BgL_carzd27368zd2_1088))
																																		{	/* Module/type.scm 58 */
																																			if (PAIRP
																																				(BgL_cdrzd27369zd2_1089))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd27373zd2_1092;
																																					BgL_carzd27373zd2_1092
																																						=
																																						CAR
																																						(BgL_cdrzd27369zd2_1089);
																																					if (SYMBOLP(BgL_carzd27373zd2_1092))
																																						{	/* Module/type.scm 58 */
																																							if (NULLP(CDR(BgL_cdrzd27369zd2_1089)))
																																								{
																																									obj_t
																																										BgL_nullzd2valuezd2_5339;
																																									obj_t
																																										BgL_parentz00_5338;
																																									obj_t
																																										BgL_namez00_5337;
																																									obj_t
																																										BgL_childz00_5336;
																																									BgL_childz00_5336
																																										=
																																										BgL_carzd27354zd2_1081;
																																									BgL_namez00_5337
																																										=
																																										BgL_carzd27362zd2_1084;
																																									BgL_parentz00_5338
																																										=
																																										BgL_carzd27368zd2_1088;
																																									BgL_nullzd2valuezd2_5339
																																										=
																																										BgL_carzd27373zd2_1092;
																																									BgL_nullzd2valuezd2_229
																																										=
																																										BgL_nullzd2valuezd2_5339;
																																									BgL_parentz00_228
																																										=
																																										BgL_parentz00_5338;
																																									BgL_namez00_227
																																										=
																																										BgL_namez00_5337;
																																									BgL_childz00_226
																																										=
																																										BgL_childz00_5336;
																																									goto
																																										BgL_tagzd2371zd2_230;
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								obj_t BgL_cdrzd27506zd2_1097;

																								BgL_cdrzd27506zd2_1097 =
																									CDR(
																									((obj_t) BgL_clausez00_28));
																								{	/* Module/type.scm 58 */
																									obj_t BgL_carzd27513zd2_1098;
																									obj_t BgL_cdrzd27514zd2_1099;

																									BgL_carzd27513zd2_1098 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd27506zd2_1097));
																									BgL_cdrzd27514zd2_1099 =
																										CDR(((obj_t)
																											BgL_cdrzd27506zd2_1097));
																									if (SYMBOLP
																										(BgL_carzd27513zd2_1098))
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd27524zd2_1101;
																											obj_t
																												BgL_cdrzd27525zd2_1102;
																											BgL_carzd27524zd2_1101 =
																												CAR(((obj_t)
																													BgL_cdrzd27514zd2_1099));
																											BgL_cdrzd27525zd2_1102 =
																												CDR(((obj_t)
																													BgL_cdrzd27514zd2_1099));
																											if (STRINGP
																												(BgL_carzd27524zd2_1101))
																												{	/* Module/type.scm 58 */
																													if (PAIRP
																														(BgL_cdrzd27525zd2_1102))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd27532zd2_1105;
																															obj_t
																																BgL_cdrzd27533zd2_1106;
																															BgL_carzd27532zd2_1105
																																=
																																CAR
																																(BgL_cdrzd27525zd2_1102);
																															BgL_cdrzd27533zd2_1106
																																=
																																CDR
																																(BgL_cdrzd27525zd2_1102);
																															if (PAIRP
																																(BgL_carzd27532zd2_1105))
																																{	/* Module/type.scm 58 */
																																	if (PAIRP
																																		(BgL_cdrzd27533zd2_1106))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd27537zd2_1109;
																																			BgL_carzd27537zd2_1109
																																				=
																																				CAR
																																				(BgL_cdrzd27533zd2_1106);
																																			if (SYMBOLP(BgL_carzd27537zd2_1109))
																																				{	/* Module/type.scm 58 */
																																					if (NULLP(CDR(BgL_cdrzd27533zd2_1106)))
																																						{
																																							obj_t
																																								BgL_nullzd2valuezd2_5371;
																																							obj_t
																																								BgL_parentz00_5370;
																																							obj_t
																																								BgL_namez00_5369;
																																							obj_t
																																								BgL_childz00_5368;
																																							BgL_childz00_5368
																																								=
																																								BgL_carzd27513zd2_1098;
																																							BgL_namez00_5369
																																								=
																																								BgL_carzd27524zd2_1101;
																																							BgL_parentz00_5370
																																								=
																																								BgL_carzd27532zd2_1105;
																																							BgL_nullzd2valuezd2_5371
																																								=
																																								BgL_carzd27537zd2_1109;
																																							BgL_nullzd2valuezd2_229
																																								=
																																								BgL_nullzd2valuezd2_5371;
																																							BgL_parentz00_228
																																								=
																																								BgL_parentz00_5370;
																																							BgL_namez00_227
																																								=
																																								BgL_namez00_5369;
																																							BgL_childz00_226
																																								=
																																								BgL_childz00_5368;
																																							goto
																																								BgL_tagzd2371zd2_230;
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									else
																										{	/* Module/type.scm 58 */
																											goto BgL_tagzd2374zd2_239;
																										}
																								}
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						obj_t BgL_cdrzd27670zd2_1114;

																						BgL_cdrzd27670zd2_1114 =
																							CDR(((obj_t) BgL_clausez00_28));
																						if (
																							(CAR(
																									((obj_t) BgL_clausez00_28)) ==
																								CNST_TABLE_REF(1)))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd27677zd2_1117;
																								obj_t BgL_cdrzd27678zd2_1118;

																								BgL_carzd27677zd2_1117 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd27670zd2_1114));
																								BgL_cdrzd27678zd2_1118 =
																									CDR(((obj_t)
																										BgL_cdrzd27670zd2_1114));
																								if (SYMBOLP
																									(BgL_carzd27677zd2_1117))
																									{	/* Module/type.scm 58 */
																										obj_t
																											BgL_carzd27688zd2_1120;
																										obj_t
																											BgL_cdrzd27689zd2_1121;
																										BgL_carzd27688zd2_1120 =
																											CAR(((obj_t)
																												BgL_cdrzd27678zd2_1118));
																										BgL_cdrzd27689zd2_1121 =
																											CDR(((obj_t)
																												BgL_cdrzd27678zd2_1118));
																										if (STRINGP
																											(BgL_carzd27688zd2_1120))
																											{	/* Module/type.scm 58 */
																												if (PAIRP
																													(BgL_cdrzd27689zd2_1121))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd27696zd2_1124;
																														obj_t
																															BgL_cdrzd27697zd2_1125;
																														BgL_carzd27696zd2_1124
																															=
																															CAR
																															(BgL_cdrzd27689zd2_1121);
																														BgL_cdrzd27697zd2_1125
																															=
																															CDR
																															(BgL_cdrzd27689zd2_1121);
																														if (PAIRP
																															(BgL_carzd27696zd2_1124))
																															{	/* Module/type.scm 58 */
																																if (PAIRP
																																	(BgL_cdrzd27697zd2_1125))
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_carzd27701zd2_1128;
																																		BgL_carzd27701zd2_1128
																																			=
																																			CAR
																																			(BgL_cdrzd27697zd2_1125);
																																		if (SYMBOLP
																																			(BgL_carzd27701zd2_1128))
																																			{	/* Module/type.scm 58 */
																																				if (NULLP(CDR(BgL_cdrzd27697zd2_1125)))
																																					{
																																						obj_t
																																							BgL_nullzd2valuezd2_5408;
																																						obj_t
																																							BgL_parentz00_5407;
																																						obj_t
																																							BgL_namez00_5406;
																																						obj_t
																																							BgL_childz00_5405;
																																						BgL_childz00_5405
																																							=
																																							BgL_carzd27677zd2_1117;
																																						BgL_namez00_5406
																																							=
																																							BgL_carzd27688zd2_1120;
																																						BgL_parentz00_5407
																																							=
																																							BgL_carzd27696zd2_1124;
																																						BgL_nullzd2valuezd2_5408
																																							=
																																							BgL_carzd27701zd2_1128;
																																						BgL_nullzd2valuezd2_229
																																							=
																																							BgL_nullzd2valuezd2_5408;
																																						BgL_parentz00_228
																																							=
																																							BgL_parentz00_5407;
																																						BgL_namez00_227
																																							=
																																							BgL_namez00_5406;
																																						BgL_childz00_226
																																							=
																																							BgL_childz00_5405;
																																						goto
																																							BgL_tagzd2371zd2_230;
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								if (
																									(CAR(
																											((obj_t)
																												BgL_clausez00_28)) ==
																										CNST_TABLE_REF(7)))
																									{	/* Module/type.scm 58 */
																										obj_t
																											BgL_carzd27831zd2_1136;
																										obj_t
																											BgL_cdrzd27832zd2_1137;
																										BgL_carzd27831zd2_1136 =
																											CAR(((obj_t)
																												BgL_cdrzd27670zd2_1114));
																										BgL_cdrzd27832zd2_1137 =
																											CDR(((obj_t)
																												BgL_cdrzd27670zd2_1114));
																										if (SYMBOLP
																											(BgL_carzd27831zd2_1136))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd27839zd2_1139;
																												BgL_carzd27839zd2_1139 =
																													CAR(((obj_t)
																														BgL_cdrzd27832zd2_1137));
																												if (PAIRP
																													(BgL_carzd27839zd2_1139))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd27842zd2_1141;
																														BgL_carzd27842zd2_1141
																															=
																															CAR
																															(BgL_carzd27839zd2_1139);
																														if (SYMBOLP
																															(BgL_carzd27842zd2_1141))
																															{	/* Module/type.scm 58 */
																																if (NULLP(CDR
																																		(BgL_carzd27839zd2_1139)))
																																	{	/* Module/type.scm 58 */
																																		if (NULLP
																																			(CDR(((obj_t) BgL_cdrzd27832zd2_1137))))
																																			{	/* Module/type.scm 58 */
																																				return
																																					BGl_delayzd2tvectorzd2typez12z12zzmodule_typez00
																																					(BgL_carzd27831zd2_1136,
																																					BgL_carzd27842zd2_1141,
																																					BgL_clausez00_28,
																																					BgL_importz00_27);
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										obj_t
																											BgL_cdrzd27898zd2_1149;
																										BgL_cdrzd27898zd2_1149 =
																											CDR(((obj_t)
																												BgL_clausez00_28));
																										if ((CAR(((obj_t)
																														BgL_clausez00_28))
																												== CNST_TABLE_REF(6)))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd27905zd2_1152;
																												obj_t
																													BgL_cdrzd27906zd2_1153;
																												BgL_carzd27905zd2_1152 =
																													CAR(((obj_t)
																														BgL_cdrzd27898zd2_1149));
																												BgL_cdrzd27906zd2_1153 =
																													CDR(((obj_t)
																														BgL_cdrzd27898zd2_1149));
																												if (SYMBOLP
																													(BgL_carzd27905zd2_1152))
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_carzd27916zd2_1155;
																														obj_t
																															BgL_cdrzd27917zd2_1156;
																														BgL_carzd27916zd2_1155
																															=
																															CAR(((obj_t)
																																BgL_cdrzd27906zd2_1153));
																														BgL_cdrzd27917zd2_1156
																															=
																															CDR(((obj_t)
																																BgL_cdrzd27906zd2_1153));
																														if (SYMBOLP
																															(BgL_carzd27916zd2_1155))
																															{	/* Module/type.scm 58 */
																																if (PAIRP
																																	(BgL_cdrzd27917zd2_1156))
																																	{	/* Module/type.scm 58 */
																																		obj_t
																																			BgL_cdrzd27926zd2_1159;
																																		BgL_cdrzd27926zd2_1159
																																			=
																																			CDR
																																			(BgL_cdrzd27917zd2_1156);
																																		if (PAIRP
																																			(BgL_cdrzd27926zd2_1159))
																																			{	/* Module/type.scm 58 */
																																				if (NULLP(CDR(BgL_cdrzd27926zd2_1159)))
																																					{
																																						obj_t
																																							BgL_coercez00_5466;
																																						obj_t
																																							BgL_checkz00_5464;
																																						obj_t
																																							BgL_toz00_5463;
																																						obj_t
																																							BgL_fromz00_5462;
																																						BgL_fromz00_5462
																																							=
																																							BgL_carzd27905zd2_1152;
																																						BgL_toz00_5463
																																							=
																																							BgL_carzd27916zd2_1155;
																																						BgL_checkz00_5464
																																							=
																																							CAR
																																							(BgL_cdrzd27917zd2_1156);
																																						BgL_coercez00_5466
																																							=
																																							CAR
																																							(BgL_cdrzd27926zd2_1159);
																																						BgL_coercez00_237
																																							=
																																							BgL_coercez00_5466;
																																						BgL_checkz00_236
																																							=
																																							BgL_checkz00_5464;
																																						BgL_toz00_235
																																							=
																																							BgL_toz00_5463;
																																						BgL_fromz00_234
																																							=
																																							BgL_fromz00_5462;
																																						goto
																																							BgL_tagzd2373zd2_238;
																																					}
																																				else
																																					{	/* Module/type.scm 58 */
																																						goto
																																							BgL_tagzd2374zd2_239;
																																					}
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																							}
																					}
																			}
																	}
															}
														else
															{	/* Module/type.scm 58 */
																goto BgL_tagzd2374zd2_239;
															}
													}
												else
													{	/* Module/type.scm 58 */
														if (
															(CAR(
																	((obj_t) BgL_clausez00_28)) ==
																CNST_TABLE_REF(5)))
															{	/* Module/type.scm 58 */
																obj_t BgL_carzd28015zd2_1174;
																obj_t BgL_cdrzd28016zd2_1175;

																BgL_carzd28015zd2_1174 =
																	CAR(((obj_t) BgL_cdrzd2390zd2_243));
																BgL_cdrzd28016zd2_1175 =
																	CDR(((obj_t) BgL_cdrzd2390zd2_243));
																if (SYMBOLP(BgL_carzd28015zd2_1174))
																	{	/* Module/type.scm 58 */
																		if (PAIRP(BgL_cdrzd28016zd2_1175))
																			{	/* Module/type.scm 58 */
																				obj_t BgL_carzd28023zd2_1178;
																				obj_t BgL_cdrzd28024zd2_1179;

																				BgL_carzd28023zd2_1178 =
																					CAR(BgL_cdrzd28016zd2_1175);
																				BgL_cdrzd28024zd2_1179 =
																					CDR(BgL_cdrzd28016zd2_1175);
																				if (STRINGP(BgL_carzd28023zd2_1178))
																					{	/* Module/type.scm 58 */
																						if (PAIRP(BgL_cdrzd28024zd2_1179))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd28028zd2_1182;

																								BgL_carzd28028zd2_1182 =
																									CAR(BgL_cdrzd28024zd2_1179);
																								if (SYMBOLP
																									(BgL_carzd28028zd2_1182))
																									{	/* Module/type.scm 58 */
																										if (NULLP(CDR
																												(BgL_cdrzd28024zd2_1179)))
																											{	/* Module/type.scm 69 */
																												BgL_typez00_bglt
																													BgL_typez00_3160;
																												BgL_typez00_3160 =
																													BGl_declarezd2typez12zc0zztype_envz00
																													(BgL_carzd28015zd2_1174,
																													BgL_carzd28023zd2_1178,
																													BgL_carzd28028zd2_1182);
																												((((BgL_typez00_bglt)
																															COBJECT
																															(BgL_typez00_3160))->
																														BgL_magiczf3zf3) =
																													((bool_t) ((bool_t)
																															1)), BUNSPEC);
																												return ((obj_t)
																													BgL_typez00_3160);
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				goto BgL_tagzd2374zd2_239;
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		goto BgL_tagzd2374zd2_239;
																	}
															}
														else
															{	/* Module/type.scm 58 */
																if (
																	(CAR(
																			((obj_t) BgL_clausez00_28)) ==
																		CNST_TABLE_REF(1)))
																	{	/* Module/type.scm 58 */
																		obj_t BgL_carzd28310zd2_1190;
																		obj_t BgL_cdrzd28311zd2_1191;

																		BgL_carzd28310zd2_1190 =
																			CAR(((obj_t) BgL_cdrzd2390zd2_243));
																		BgL_cdrzd28311zd2_1191 =
																			CDR(((obj_t) BgL_cdrzd2390zd2_243));
																		if (SYMBOLP(BgL_carzd28310zd2_1190))
																			{	/* Module/type.scm 58 */
																				if (PAIRP(BgL_cdrzd28311zd2_1191))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd28319zd2_1194;
																						obj_t BgL_cdrzd28320zd2_1195;

																						BgL_carzd28319zd2_1194 =
																							CAR(BgL_cdrzd28311zd2_1191);
																						BgL_cdrzd28320zd2_1195 =
																							CDR(BgL_cdrzd28311zd2_1191);
																						if (STRINGP(BgL_carzd28319zd2_1194))
																							{	/* Module/type.scm 58 */
																								if (PAIRP
																									(BgL_cdrzd28320zd2_1195))
																									{	/* Module/type.scm 58 */
																										obj_t
																											BgL_carzd28324zd2_1198;
																										BgL_carzd28324zd2_1198 =
																											CAR
																											(BgL_cdrzd28320zd2_1195);
																										if (PAIRP
																											(BgL_carzd28324zd2_1198))
																											{	/* Module/type.scm 58 */
																												if (NULLP(CDR
																														(BgL_cdrzd28320zd2_1195)))
																													{
																														obj_t
																															BgL_parentz00_5523;
																														obj_t
																															BgL_namez00_5522;
																														obj_t
																															BgL_childz00_5521;
																														BgL_childz00_5521 =
																															BgL_carzd28310zd2_1190;
																														BgL_namez00_5522 =
																															BgL_carzd28319zd2_1194;
																														BgL_parentz00_5523 =
																															BgL_carzd28324zd2_1198;
																														BgL_parentz00_224 =
																															BgL_parentz00_5523;
																														BgL_namez00_223 =
																															BgL_namez00_5522;
																														BgL_childz00_222 =
																															BgL_childz00_5521;
																														goto
																															BgL_tagzd2370zd2_225;
																													}
																												else
																													{	/* Module/type.scm 58 */
																														obj_t
																															BgL_cdrzd28345zd2_1202;
																														BgL_cdrzd28345zd2_1202
																															=
																															CDR(((obj_t)
																																BgL_clausez00_28));
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd28352zd2_1203;
																															obj_t
																																BgL_cdrzd28353zd2_1204;
																															BgL_carzd28352zd2_1203
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd28345zd2_1202));
																															BgL_cdrzd28353zd2_1204
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd28345zd2_1202));
																															if (SYMBOLP
																																(BgL_carzd28352zd2_1203))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd28362zd2_1206;
																																	obj_t
																																		BgL_cdrzd28363zd2_1207;
																																	BgL_carzd28362zd2_1206
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd28353zd2_1204));
																																	BgL_cdrzd28363zd2_1207
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd28353zd2_1204));
																																	if (STRINGP
																																		(BgL_carzd28362zd2_1206))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd28371zd2_1209;
																																			obj_t
																																				BgL_cdrzd28372zd2_1210;
																																			BgL_carzd28371zd2_1209
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_cdrzd28363zd2_1207));
																																			BgL_cdrzd28372zd2_1210
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_cdrzd28363zd2_1207));
																																			if (PAIRP
																																				(BgL_carzd28371zd2_1209))
																																				{	/* Module/type.scm 58 */
																																					if (PAIRP(BgL_cdrzd28372zd2_1210))
																																						{	/* Module/type.scm 58 */
																																							obj_t
																																								BgL_carzd28378zd2_1213;
																																							BgL_carzd28378zd2_1213
																																								=
																																								CAR
																																								(BgL_cdrzd28372zd2_1210);
																																							if (SYMBOLP(BgL_carzd28378zd2_1213))
																																								{	/* Module/type.scm 58 */
																																									if (NULLP(CDR(BgL_cdrzd28372zd2_1210)))
																																										{
																																											obj_t
																																												BgL_nullzd2valuezd2_5555;
																																											obj_t
																																												BgL_parentz00_5554;
																																											obj_t
																																												BgL_namez00_5553;
																																											obj_t
																																												BgL_childz00_5552;
																																											BgL_childz00_5552
																																												=
																																												BgL_carzd28352zd2_1203;
																																											BgL_namez00_5553
																																												=
																																												BgL_carzd28362zd2_1206;
																																											BgL_parentz00_5554
																																												=
																																												BgL_carzd28371zd2_1209;
																																											BgL_nullzd2valuezd2_5555
																																												=
																																												BgL_carzd28378zd2_1213;
																																											BgL_nullzd2valuezd2_229
																																												=
																																												BgL_nullzd2valuezd2_5555;
																																											BgL_parentz00_228
																																												=
																																												BgL_parentz00_5554;
																																											BgL_namez00_227
																																												=
																																												BgL_namez00_5553;
																																											BgL_childz00_226
																																												=
																																												BgL_childz00_5552;
																																											goto
																																												BgL_tagzd2371zd2_230;
																																										}
																																									else
																																										{	/* Module/type.scm 58 */
																																											goto
																																												BgL_tagzd2374zd2_239;
																																										}
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_cdrzd28495zd2_1219;
																												BgL_cdrzd28495zd2_1219 =
																													CDR(((obj_t)
																														BgL_clausez00_28));
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd28502zd2_1220;
																													obj_t
																														BgL_cdrzd28503zd2_1221;
																													BgL_carzd28502zd2_1220
																														=
																														CAR(((obj_t)
																															BgL_cdrzd28495zd2_1219));
																													BgL_cdrzd28503zd2_1221
																														=
																														CDR(((obj_t)
																															BgL_cdrzd28495zd2_1219));
																													if (SYMBOLP
																														(BgL_carzd28502zd2_1220))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd28512zd2_1223;
																															obj_t
																																BgL_cdrzd28513zd2_1224;
																															BgL_carzd28512zd2_1223
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd28503zd2_1221));
																															BgL_cdrzd28513zd2_1224
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd28503zd2_1221));
																															if (STRINGP
																																(BgL_carzd28512zd2_1223))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd28521zd2_1226;
																																	obj_t
																																		BgL_cdrzd28522zd2_1227;
																																	BgL_carzd28521zd2_1226
																																		=
																																		CAR(((obj_t)
																																			BgL_cdrzd28513zd2_1224));
																																	BgL_cdrzd28522zd2_1227
																																		=
																																		CDR(((obj_t)
																																			BgL_cdrzd28513zd2_1224));
																																	if (PAIRP
																																		(BgL_carzd28521zd2_1226))
																																		{	/* Module/type.scm 58 */
																																			if (PAIRP
																																				(BgL_cdrzd28522zd2_1227))
																																				{	/* Module/type.scm 58 */
																																					obj_t
																																						BgL_carzd28528zd2_1230;
																																					BgL_carzd28528zd2_1230
																																						=
																																						CAR
																																						(BgL_cdrzd28522zd2_1227);
																																					if (SYMBOLP(BgL_carzd28528zd2_1230))
																																						{	/* Module/type.scm 58 */
																																							if (NULLP(CDR(BgL_cdrzd28522zd2_1227)))
																																								{
																																									obj_t
																																										BgL_nullzd2valuezd2_5587;
																																									obj_t
																																										BgL_parentz00_5586;
																																									obj_t
																																										BgL_namez00_5585;
																																									obj_t
																																										BgL_childz00_5584;
																																									BgL_childz00_5584
																																										=
																																										BgL_carzd28502zd2_1220;
																																									BgL_namez00_5585
																																										=
																																										BgL_carzd28512zd2_1223;
																																									BgL_parentz00_5586
																																										=
																																										BgL_carzd28521zd2_1226;
																																									BgL_nullzd2valuezd2_5587
																																										=
																																										BgL_carzd28528zd2_1230;
																																									BgL_nullzd2valuezd2_229
																																										=
																																										BgL_nullzd2valuezd2_5587;
																																									BgL_parentz00_228
																																										=
																																										BgL_parentz00_5586;
																																									BgL_namez00_227
																																										=
																																										BgL_namez00_5585;
																																									BgL_childz00_226
																																										=
																																										BgL_childz00_5584;
																																									goto
																																										BgL_tagzd2371zd2_230;
																																								}
																																							else
																																								{	/* Module/type.scm 58 */
																																									goto
																																										BgL_tagzd2374zd2_239;
																																								}
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								obj_t BgL_cdrzd28675zd2_1235;

																								BgL_cdrzd28675zd2_1235 =
																									CDR(
																									((obj_t) BgL_clausez00_28));
																								{	/* Module/type.scm 58 */
																									obj_t BgL_carzd28681zd2_1236;
																									obj_t BgL_cdrzd28682zd2_1237;

																									BgL_carzd28681zd2_1236 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd28675zd2_1235));
																									BgL_cdrzd28682zd2_1237 =
																										CDR(((obj_t)
																											BgL_cdrzd28675zd2_1235));
																									if (SYMBOLP
																										(BgL_carzd28681zd2_1236))
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd28690zd2_1239;
																											obj_t
																												BgL_cdrzd28691zd2_1240;
																											BgL_carzd28690zd2_1239 =
																												CAR(((obj_t)
																													BgL_cdrzd28682zd2_1237));
																											BgL_cdrzd28691zd2_1240 =
																												CDR(((obj_t)
																													BgL_cdrzd28682zd2_1237));
																											if (STRINGP
																												(BgL_carzd28690zd2_1239))
																												{	/* Module/type.scm 58 */
																													if (PAIRP
																														(BgL_cdrzd28691zd2_1240))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd28698zd2_1243;
																															obj_t
																																BgL_cdrzd28699zd2_1244;
																															BgL_carzd28698zd2_1243
																																=
																																CAR
																																(BgL_cdrzd28691zd2_1240);
																															BgL_cdrzd28699zd2_1244
																																=
																																CDR
																																(BgL_cdrzd28691zd2_1240);
																															if (PAIRP
																																(BgL_carzd28698zd2_1243))
																																{	/* Module/type.scm 58 */
																																	if (PAIRP
																																		(BgL_cdrzd28699zd2_1244))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd28703zd2_1247;
																																			BgL_carzd28703zd2_1247
																																				=
																																				CAR
																																				(BgL_cdrzd28699zd2_1244);
																																			if (SYMBOLP(BgL_carzd28703zd2_1247))
																																				{	/* Module/type.scm 58 */
																																					if (NULLP(CDR(BgL_cdrzd28699zd2_1244)))
																																						{
																																							obj_t
																																								BgL_nullzd2valuezd2_5619;
																																							obj_t
																																								BgL_parentz00_5618;
																																							obj_t
																																								BgL_namez00_5617;
																																							obj_t
																																								BgL_childz00_5616;
																																							BgL_childz00_5616
																																								=
																																								BgL_carzd28681zd2_1236;
																																							BgL_namez00_5617
																																								=
																																								BgL_carzd28690zd2_1239;
																																							BgL_parentz00_5618
																																								=
																																								BgL_carzd28698zd2_1243;
																																							BgL_nullzd2valuezd2_5619
																																								=
																																								BgL_carzd28703zd2_1247;
																																							BgL_nullzd2valuezd2_229
																																								=
																																								BgL_nullzd2valuezd2_5619;
																																							BgL_parentz00_228
																																								=
																																								BgL_parentz00_5618;
																																							BgL_namez00_227
																																								=
																																								BgL_namez00_5617;
																																							BgL_childz00_226
																																								=
																																								BgL_childz00_5616;
																																							goto
																																								BgL_tagzd2371zd2_230;
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									else
																										{	/* Module/type.scm 58 */
																											goto BgL_tagzd2374zd2_239;
																										}
																								}
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				obj_t BgL_cdrzd28865zd2_1252;

																				BgL_cdrzd28865zd2_1252 =
																					CDR(((obj_t) BgL_clausez00_28));
																				{	/* Module/type.scm 58 */
																					obj_t BgL_carzd28871zd2_1253;
																					obj_t BgL_cdrzd28872zd2_1254;

																					BgL_carzd28871zd2_1253 =
																						CAR(
																						((obj_t) BgL_cdrzd28865zd2_1252));
																					BgL_cdrzd28872zd2_1254 =
																						CDR(
																						((obj_t) BgL_cdrzd28865zd2_1252));
																					if (SYMBOLP(BgL_carzd28871zd2_1253))
																						{	/* Module/type.scm 58 */
																							if (PAIRP(BgL_cdrzd28872zd2_1254))
																								{	/* Module/type.scm 58 */
																									obj_t BgL_carzd28881zd2_1257;
																									obj_t BgL_cdrzd28882zd2_1258;

																									BgL_carzd28881zd2_1257 =
																										CAR(BgL_cdrzd28872zd2_1254);
																									BgL_cdrzd28882zd2_1258 =
																										CDR(BgL_cdrzd28872zd2_1254);
																									if (STRINGP
																										(BgL_carzd28881zd2_1257))
																										{	/* Module/type.scm 58 */
																											if (PAIRP
																												(BgL_cdrzd28882zd2_1258))
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd28887zd2_1261;
																													obj_t
																														BgL_cdrzd28888zd2_1262;
																													BgL_carzd28887zd2_1261
																														=
																														CAR
																														(BgL_cdrzd28882zd2_1258);
																													BgL_cdrzd28888zd2_1262
																														=
																														CDR
																														(BgL_cdrzd28882zd2_1258);
																													if (PAIRP
																														(BgL_carzd28887zd2_1261))
																														{	/* Module/type.scm 58 */
																															if (PAIRP
																																(BgL_cdrzd28888zd2_1262))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd28892zd2_1265;
																																	BgL_carzd28892zd2_1265
																																		=
																																		CAR
																																		(BgL_cdrzd28888zd2_1262);
																																	if (SYMBOLP
																																		(BgL_carzd28892zd2_1265))
																																		{	/* Module/type.scm 58 */
																																			if (NULLP
																																				(CDR
																																					(BgL_cdrzd28888zd2_1262)))
																																				{
																																					obj_t
																																						BgL_nullzd2valuezd2_5651;
																																					obj_t
																																						BgL_parentz00_5650;
																																					obj_t
																																						BgL_namez00_5649;
																																					obj_t
																																						BgL_childz00_5648;
																																					BgL_childz00_5648
																																						=
																																						BgL_carzd28871zd2_1253;
																																					BgL_namez00_5649
																																						=
																																						BgL_carzd28881zd2_1257;
																																					BgL_parentz00_5650
																																						=
																																						BgL_carzd28887zd2_1261;
																																					BgL_nullzd2valuezd2_5651
																																						=
																																						BgL_carzd28892zd2_1265;
																																					BgL_nullzd2valuezd2_229
																																						=
																																						BgL_nullzd2valuezd2_5651;
																																					BgL_parentz00_228
																																						=
																																						BgL_parentz00_5650;
																																					BgL_namez00_227
																																						=
																																						BgL_namez00_5649;
																																					BgL_childz00_226
																																						=
																																						BgL_childz00_5648;
																																					goto
																																						BgL_tagzd2371zd2_230;
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									else
																										{	/* Module/type.scm 58 */
																											goto BgL_tagzd2374zd2_239;
																										}
																								}
																							else
																								{	/* Module/type.scm 58 */
																									goto BgL_tagzd2374zd2_239;
																								}
																						}
																					else
																						{	/* Module/type.scm 58 */
																							goto BgL_tagzd2374zd2_239;
																						}
																				}
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		obj_t BgL_cdrzd29040zd2_1270;

																		BgL_cdrzd29040zd2_1270 =
																			CDR(((obj_t) BgL_clausez00_28));
																		if (
																			(CAR(
																					((obj_t) BgL_clausez00_28)) ==
																				CNST_TABLE_REF(1)))
																			{	/* Module/type.scm 58 */
																				obj_t BgL_carzd29046zd2_1273;
																				obj_t BgL_cdrzd29047zd2_1274;

																				BgL_carzd29046zd2_1273 =
																					CAR(((obj_t) BgL_cdrzd29040zd2_1270));
																				BgL_cdrzd29047zd2_1274 =
																					CDR(((obj_t) BgL_cdrzd29040zd2_1270));
																				if (SYMBOLP(BgL_carzd29046zd2_1273))
																					{	/* Module/type.scm 58 */
																						if (PAIRP(BgL_cdrzd29047zd2_1274))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd29056zd2_1277;
																								obj_t BgL_cdrzd29057zd2_1278;

																								BgL_carzd29056zd2_1277 =
																									CAR(BgL_cdrzd29047zd2_1274);
																								BgL_cdrzd29057zd2_1278 =
																									CDR(BgL_cdrzd29047zd2_1274);
																								if (STRINGP
																									(BgL_carzd29056zd2_1277))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd29057zd2_1278))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd29062zd2_1281;
																												obj_t
																													BgL_cdrzd29063zd2_1282;
																												BgL_carzd29062zd2_1281 =
																													CAR
																													(BgL_cdrzd29057zd2_1278);
																												BgL_cdrzd29063zd2_1282 =
																													CDR
																													(BgL_cdrzd29057zd2_1278);
																												if (PAIRP
																													(BgL_carzd29062zd2_1281))
																													{	/* Module/type.scm 58 */
																														if (PAIRP
																															(BgL_cdrzd29063zd2_1282))
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_carzd29067zd2_1285;
																																BgL_carzd29067zd2_1285
																																	=
																																	CAR
																																	(BgL_cdrzd29063zd2_1282);
																																if (SYMBOLP
																																	(BgL_carzd29067zd2_1285))
																																	{	/* Module/type.scm 58 */
																																		if (NULLP
																																			(CDR
																																				(BgL_cdrzd29063zd2_1282)))
																																			{
																																				obj_t
																																					BgL_nullzd2valuezd2_5688;
																																				obj_t
																																					BgL_parentz00_5687;
																																				obj_t
																																					BgL_namez00_5686;
																																				obj_t
																																					BgL_childz00_5685;
																																				BgL_childz00_5685
																																					=
																																					BgL_carzd29046zd2_1273;
																																				BgL_namez00_5686
																																					=
																																					BgL_carzd29056zd2_1277;
																																				BgL_parentz00_5687
																																					=
																																					BgL_carzd29062zd2_1281;
																																				BgL_nullzd2valuezd2_5688
																																					=
																																					BgL_carzd29067zd2_1285;
																																				BgL_nullzd2valuezd2_229
																																					=
																																					BgL_nullzd2valuezd2_5688;
																																				BgL_parentz00_228
																																					=
																																					BgL_parentz00_5687;
																																				BgL_namez00_227
																																					=
																																					BgL_namez00_5686;
																																				BgL_childz00_226
																																					=
																																					BgL_childz00_5685;
																																				goto
																																					BgL_tagzd2371zd2_230;
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				if (
																					(CAR(
																							((obj_t) BgL_clausez00_28)) ==
																						CNST_TABLE_REF(7)))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd29213zd2_1293;
																						obj_t BgL_cdrzd29214zd2_1294;

																						BgL_carzd29213zd2_1293 =
																							CAR(
																							((obj_t) BgL_cdrzd29040zd2_1270));
																						BgL_cdrzd29214zd2_1294 =
																							CDR(
																							((obj_t) BgL_cdrzd29040zd2_1270));
																						if (SYMBOLP(BgL_carzd29213zd2_1293))
																							{	/* Module/type.scm 58 */
																								if (PAIRP
																									(BgL_cdrzd29214zd2_1294))
																									{	/* Module/type.scm 58 */
																										obj_t
																											BgL_carzd29221zd2_1297;
																										BgL_carzd29221zd2_1297 =
																											CAR
																											(BgL_cdrzd29214zd2_1294);
																										if (PAIRP
																											(BgL_carzd29221zd2_1297))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd29224zd2_1299;
																												BgL_carzd29224zd2_1299 =
																													CAR
																													(BgL_carzd29221zd2_1297);
																												if (SYMBOLP
																													(BgL_carzd29224zd2_1299))
																													{	/* Module/type.scm 58 */
																														if (NULLP(CDR
																																(BgL_carzd29221zd2_1297)))
																															{	/* Module/type.scm 58 */
																																if (NULLP(CDR
																																		(BgL_cdrzd29214zd2_1294)))
																																	{	/* Module/type.scm 58 */
																																		return
																																			BGl_delayzd2tvectorzd2typez12z12zzmodule_typez00
																																			(BgL_carzd29213zd2_1293,
																																			BgL_carzd29224zd2_1299,
																																			BgL_clausez00_28,
																																			BgL_importz00_27);
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						obj_t BgL_cdrzd29287zd2_1307;

																						BgL_cdrzd29287zd2_1307 =
																							CDR(((obj_t) BgL_clausez00_28));
																						if (
																							(CAR(
																									((obj_t) BgL_clausez00_28)) ==
																								CNST_TABLE_REF(6)))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd29293zd2_1310;
																								obj_t BgL_cdrzd29294zd2_1311;

																								BgL_carzd29293zd2_1310 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd29287zd2_1307));
																								BgL_cdrzd29294zd2_1311 =
																									CDR(((obj_t)
																										BgL_cdrzd29287zd2_1307));
																								if (SYMBOLP
																									(BgL_carzd29293zd2_1310))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd29294zd2_1311))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd29303zd2_1314;
																												obj_t
																													BgL_cdrzd29304zd2_1315;
																												BgL_carzd29303zd2_1314 =
																													CAR
																													(BgL_cdrzd29294zd2_1311);
																												BgL_cdrzd29304zd2_1315 =
																													CDR
																													(BgL_cdrzd29294zd2_1311);
																												if (SYMBOLP
																													(BgL_carzd29303zd2_1314))
																													{	/* Module/type.scm 58 */
																														if (PAIRP
																															(BgL_cdrzd29304zd2_1315))
																															{	/* Module/type.scm 58 */
																																obj_t
																																	BgL_cdrzd29311zd2_1318;
																																BgL_cdrzd29311zd2_1318
																																	=
																																	CDR
																																	(BgL_cdrzd29304zd2_1315);
																																if (PAIRP
																																	(BgL_cdrzd29311zd2_1318))
																																	{	/* Module/type.scm 58 */
																																		if (NULLP
																																			(CDR
																																				(BgL_cdrzd29311zd2_1318)))
																																			{
																																				obj_t
																																					BgL_coercez00_5746;
																																				obj_t
																																					BgL_checkz00_5744;
																																				obj_t
																																					BgL_toz00_5743;
																																				obj_t
																																					BgL_fromz00_5742;
																																				BgL_fromz00_5742
																																					=
																																					BgL_carzd29293zd2_1310;
																																				BgL_toz00_5743
																																					=
																																					BgL_carzd29303zd2_1314;
																																				BgL_checkz00_5744
																																					=
																																					CAR
																																					(BgL_cdrzd29304zd2_1315);
																																				BgL_coercez00_5746
																																					=
																																					CAR
																																					(BgL_cdrzd29311zd2_1318);
																																				BgL_coercez00_237
																																					=
																																					BgL_coercez00_5746;
																																				BgL_checkz00_236
																																					=
																																					BgL_checkz00_5744;
																																				BgL_toz00_235
																																					=
																																					BgL_toz00_5743;
																																				BgL_fromz00_234
																																					=
																																					BgL_fromz00_5742;
																																				goto
																																					BgL_tagzd2373zd2_238;
																																			}
																																		else
																																			{	/* Module/type.scm 58 */
																																				goto
																																					BgL_tagzd2374zd2_239;
																																			}
																																	}
																																else
																																	{	/* Module/type.scm 58 */
																																		goto
																																			BgL_tagzd2374zd2_239;
																																	}
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																			}
																	}
															}
													}
											}
									}
								else
									{	/* Module/type.scm 58 */
										goto BgL_tagzd2374zd2_239;
									}
							}
						else
							{	/* Module/type.scm 58 */
								if ((CAR(((obj_t) BgL_clausez00_28)) == CNST_TABLE_REF(5)))
									{	/* Module/type.scm 58 */
										if (PAIRP(BgL_cdrzd2390zd2_243))
											{	/* Module/type.scm 58 */
												obj_t BgL_carzd214582zd2_1954;
												obj_t BgL_cdrzd214583zd2_1955;

												BgL_carzd214582zd2_1954 = CAR(BgL_cdrzd2390zd2_243);
												BgL_cdrzd214583zd2_1955 = CDR(BgL_cdrzd2390zd2_243);
												if (SYMBOLP(BgL_carzd214582zd2_1954))
													{	/* Module/type.scm 58 */
														if (PAIRP(BgL_cdrzd214583zd2_1955))
															{	/* Module/type.scm 58 */
																obj_t BgL_carzd214588zd2_1958;
																obj_t BgL_cdrzd214589zd2_1959;

																BgL_carzd214588zd2_1958 =
																	CAR(BgL_cdrzd214583zd2_1955);
																BgL_cdrzd214589zd2_1959 =
																	CDR(BgL_cdrzd214583zd2_1955);
																if (STRINGP(BgL_carzd214588zd2_1958))
																	{	/* Module/type.scm 58 */
																		if (PAIRP(BgL_cdrzd214589zd2_1959))
																			{	/* Module/type.scm 58 */
																				obj_t BgL_carzd214593zd2_1962;

																				BgL_carzd214593zd2_1962 =
																					CAR(BgL_cdrzd214589zd2_1959);
																				if (SYMBOLP(BgL_carzd214593zd2_1962))
																					{	/* Module/type.scm 58 */
																						if (NULLP(CDR
																								(BgL_cdrzd214589zd2_1959)))
																							{	/* Module/type.scm 69 */
																								BgL_typez00_bglt
																									BgL_typez00_3573;
																								BgL_typez00_3573 =
																									BGl_declarezd2typez12zc0zztype_envz00
																									(BgL_carzd214582zd2_1954,
																									BgL_carzd214588zd2_1958,
																									BgL_carzd214593zd2_1962);
																								((((BgL_typez00_bglt)
																											COBJECT
																											(BgL_typez00_3573))->
																										BgL_magiczf3zf3) =
																									((bool_t) ((bool_t) 1)),
																									BUNSPEC);
																								return ((obj_t)
																									BgL_typez00_3573);
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				goto BgL_tagzd2374zd2_239;
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		goto BgL_tagzd2374zd2_239;
																	}
															}
														else
															{	/* Module/type.scm 58 */
																goto BgL_tagzd2374zd2_239;
															}
													}
												else
													{	/* Module/type.scm 58 */
														goto BgL_tagzd2374zd2_239;
													}
											}
										else
											{	/* Module/type.scm 58 */
												goto BgL_tagzd2374zd2_239;
											}
									}
								else
									{	/* Module/type.scm 58 */
										if ((CAR(((obj_t) BgL_clausez00_28)) == CNST_TABLE_REF(1)))
											{	/* Module/type.scm 58 */
												if (PAIRP(BgL_cdrzd2390zd2_243))
													{	/* Module/type.scm 58 */
														obj_t BgL_carzd214916zd2_1971;
														obj_t BgL_cdrzd214917zd2_1972;

														BgL_carzd214916zd2_1971 = CAR(BgL_cdrzd2390zd2_243);
														BgL_cdrzd214917zd2_1972 = CDR(BgL_cdrzd2390zd2_243);
														if (SYMBOLP(BgL_carzd214916zd2_1971))
															{	/* Module/type.scm 58 */
																if (PAIRP(BgL_cdrzd214917zd2_1972))
																	{	/* Module/type.scm 58 */
																		obj_t BgL_carzd214923zd2_1975;
																		obj_t BgL_cdrzd214924zd2_1976;

																		BgL_carzd214923zd2_1975 =
																			CAR(BgL_cdrzd214917zd2_1972);
																		BgL_cdrzd214924zd2_1976 =
																			CDR(BgL_cdrzd214917zd2_1972);
																		if (STRINGP(BgL_carzd214923zd2_1975))
																			{	/* Module/type.scm 58 */
																				if (PAIRP(BgL_cdrzd214924zd2_1976))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd214928zd2_1979;

																						BgL_carzd214928zd2_1979 =
																							CAR(BgL_cdrzd214924zd2_1976);
																						if (PAIRP(BgL_carzd214928zd2_1979))
																							{	/* Module/type.scm 58 */
																								if (NULLP(CDR
																										(BgL_cdrzd214924zd2_1976)))
																									{
																										obj_t BgL_parentz00_5803;
																										obj_t BgL_namez00_5802;
																										obj_t BgL_childz00_5801;

																										BgL_childz00_5801 =
																											BgL_carzd214916zd2_1971;
																										BgL_namez00_5802 =
																											BgL_carzd214923zd2_1975;
																										BgL_parentz00_5803 =
																											BgL_carzd214928zd2_1979;
																										BgL_parentz00_224 =
																											BgL_parentz00_5803;
																										BgL_namez00_223 =
																											BgL_namez00_5802;
																										BgL_childz00_222 =
																											BgL_childz00_5801;
																										goto BgL_tagzd2370zd2_225;
																									}
																								else
																									{	/* Module/type.scm 58 */
																										obj_t
																											BgL_cdrzd214950zd2_1983;
																										BgL_cdrzd214950zd2_1983 =
																											CDR(((obj_t)
																												BgL_clausez00_28));
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd214958zd2_1984;
																											obj_t
																												BgL_cdrzd214959zd2_1985;
																											BgL_carzd214958zd2_1984 =
																												CAR(((obj_t)
																													BgL_cdrzd214950zd2_1983));
																											BgL_cdrzd214959zd2_1985 =
																												CDR(((obj_t)
																													BgL_cdrzd214950zd2_1983));
																											if (SYMBOLP
																												(BgL_carzd214958zd2_1984))
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd214970zd2_1987;
																													obj_t
																														BgL_cdrzd214971zd2_1988;
																													BgL_carzd214970zd2_1987
																														=
																														CAR(((obj_t)
																															BgL_cdrzd214959zd2_1985));
																													BgL_cdrzd214971zd2_1988
																														=
																														CDR(((obj_t)
																															BgL_cdrzd214959zd2_1985));
																													if (STRINGP
																														(BgL_carzd214970zd2_1987))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd214979zd2_1990;
																															obj_t
																																BgL_cdrzd214980zd2_1991;
																															BgL_carzd214979zd2_1990
																																=
																																CAR(((obj_t)
																																	BgL_cdrzd214971zd2_1988));
																															BgL_cdrzd214980zd2_1991
																																=
																																CDR(((obj_t)
																																	BgL_cdrzd214971zd2_1988));
																															if (PAIRP
																																(BgL_carzd214979zd2_1990))
																																{	/* Module/type.scm 58 */
																																	if (PAIRP
																																		(BgL_cdrzd214980zd2_1991))
																																		{	/* Module/type.scm 58 */
																																			obj_t
																																				BgL_carzd214986zd2_1994;
																																			BgL_carzd214986zd2_1994
																																				=
																																				CAR
																																				(BgL_cdrzd214980zd2_1991);
																																			if (SYMBOLP(BgL_carzd214986zd2_1994))
																																				{	/* Module/type.scm 58 */
																																					if (NULLP(CDR(BgL_cdrzd214980zd2_1991)))
																																						{
																																							obj_t
																																								BgL_nullzd2valuezd2_5835;
																																							obj_t
																																								BgL_parentz00_5834;
																																							obj_t
																																								BgL_namez00_5833;
																																							obj_t
																																								BgL_childz00_5832;
																																							BgL_childz00_5832
																																								=
																																								BgL_carzd214958zd2_1984;
																																							BgL_namez00_5833
																																								=
																																								BgL_carzd214970zd2_1987;
																																							BgL_parentz00_5834
																																								=
																																								BgL_carzd214979zd2_1990;
																																							BgL_nullzd2valuezd2_5835
																																								=
																																								BgL_carzd214986zd2_1994;
																																							BgL_nullzd2valuezd2_229
																																								=
																																								BgL_nullzd2valuezd2_5835;
																																							BgL_parentz00_228
																																								=
																																								BgL_parentz00_5834;
																																							BgL_namez00_227
																																								=
																																								BgL_namez00_5833;
																																							BgL_childz00_226
																																								=
																																								BgL_childz00_5832;
																																							goto
																																								BgL_tagzd2371zd2_230;
																																						}
																																					else
																																						{	/* Module/type.scm 58 */
																																							goto
																																								BgL_tagzd2374zd2_239;
																																						}
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								obj_t BgL_cdrzd215104zd2_2000;

																								BgL_cdrzd215104zd2_2000 =
																									CDR(
																									((obj_t) BgL_clausez00_28));
																								{	/* Module/type.scm 58 */
																									obj_t BgL_carzd215112zd2_2001;
																									obj_t BgL_cdrzd215113zd2_2002;

																									BgL_carzd215112zd2_2001 =
																										CAR(
																										((obj_t)
																											BgL_cdrzd215104zd2_2000));
																									BgL_cdrzd215113zd2_2002 =
																										CDR(((obj_t)
																											BgL_cdrzd215104zd2_2000));
																									if (SYMBOLP
																										(BgL_carzd215112zd2_2001))
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd215124zd2_2004;
																											obj_t
																												BgL_cdrzd215125zd2_2005;
																											BgL_carzd215124zd2_2004 =
																												CAR(((obj_t)
																													BgL_cdrzd215113zd2_2002));
																											BgL_cdrzd215125zd2_2005 =
																												CDR(((obj_t)
																													BgL_cdrzd215113zd2_2002));
																											if (STRINGP
																												(BgL_carzd215124zd2_2004))
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd215133zd2_2007;
																													obj_t
																														BgL_cdrzd215134zd2_2008;
																													BgL_carzd215133zd2_2007
																														=
																														CAR(((obj_t)
																															BgL_cdrzd215125zd2_2005));
																													BgL_cdrzd215134zd2_2008
																														=
																														CDR(((obj_t)
																															BgL_cdrzd215125zd2_2005));
																													if (PAIRP
																														(BgL_carzd215133zd2_2007))
																														{	/* Module/type.scm 58 */
																															if (PAIRP
																																(BgL_cdrzd215134zd2_2008))
																																{	/* Module/type.scm 58 */
																																	obj_t
																																		BgL_carzd215140zd2_2011;
																																	BgL_carzd215140zd2_2011
																																		=
																																		CAR
																																		(BgL_cdrzd215134zd2_2008);
																																	if (SYMBOLP
																																		(BgL_carzd215140zd2_2011))
																																		{	/* Module/type.scm 58 */
																																			if (NULLP
																																				(CDR
																																					(BgL_cdrzd215134zd2_2008)))
																																				{
																																					obj_t
																																						BgL_nullzd2valuezd2_5867;
																																					obj_t
																																						BgL_parentz00_5866;
																																					obj_t
																																						BgL_namez00_5865;
																																					obj_t
																																						BgL_childz00_5864;
																																					BgL_childz00_5864
																																						=
																																						BgL_carzd215112zd2_2001;
																																					BgL_namez00_5865
																																						=
																																						BgL_carzd215124zd2_2004;
																																					BgL_parentz00_5866
																																						=
																																						BgL_carzd215133zd2_2007;
																																					BgL_nullzd2valuezd2_5867
																																						=
																																						BgL_carzd215140zd2_2011;
																																					BgL_nullzd2valuezd2_229
																																						=
																																						BgL_nullzd2valuezd2_5867;
																																					BgL_parentz00_228
																																						=
																																						BgL_parentz00_5866;
																																					BgL_namez00_227
																																						=
																																						BgL_namez00_5865;
																																					BgL_childz00_226
																																						=
																																						BgL_childz00_5864;
																																					goto
																																						BgL_tagzd2371zd2_230;
																																				}
																																			else
																																				{	/* Module/type.scm 58 */
																																					goto
																																						BgL_tagzd2374zd2_239;
																																				}
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									else
																										{	/* Module/type.scm 58 */
																											goto BgL_tagzd2374zd2_239;
																										}
																								}
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				obj_t BgL_cdrzd215289zd2_2016;

																				BgL_cdrzd215289zd2_2016 =
																					CDR(((obj_t) BgL_clausez00_28));
																				{	/* Module/type.scm 58 */
																					obj_t BgL_carzd215296zd2_2017;
																					obj_t BgL_cdrzd215297zd2_2018;

																					BgL_carzd215296zd2_2017 =
																						CAR(
																						((obj_t) BgL_cdrzd215289zd2_2016));
																					BgL_cdrzd215297zd2_2018 =
																						CDR(
																						((obj_t) BgL_cdrzd215289zd2_2016));
																					if (SYMBOLP(BgL_carzd215296zd2_2017))
																						{	/* Module/type.scm 58 */
																							obj_t BgL_carzd215307zd2_2020;
																							obj_t BgL_cdrzd215308zd2_2021;

																							BgL_carzd215307zd2_2020 =
																								CAR(
																								((obj_t)
																									BgL_cdrzd215297zd2_2018));
																							BgL_cdrzd215308zd2_2021 =
																								CDR(((obj_t)
																									BgL_cdrzd215297zd2_2018));
																							if (STRINGP
																								(BgL_carzd215307zd2_2020))
																								{	/* Module/type.scm 58 */
																									if (PAIRP
																										(BgL_cdrzd215308zd2_2021))
																										{	/* Module/type.scm 58 */
																											obj_t
																												BgL_carzd215315zd2_2024;
																											obj_t
																												BgL_cdrzd215316zd2_2025;
																											BgL_carzd215315zd2_2024 =
																												CAR
																												(BgL_cdrzd215308zd2_2021);
																											BgL_cdrzd215316zd2_2025 =
																												CDR
																												(BgL_cdrzd215308zd2_2021);
																											if (PAIRP
																												(BgL_carzd215315zd2_2024))
																												{	/* Module/type.scm 58 */
																													if (PAIRP
																														(BgL_cdrzd215316zd2_2025))
																														{	/* Module/type.scm 58 */
																															obj_t
																																BgL_carzd215320zd2_2028;
																															BgL_carzd215320zd2_2028
																																=
																																CAR
																																(BgL_cdrzd215316zd2_2025);
																															if (SYMBOLP
																																(BgL_carzd215320zd2_2028))
																																{	/* Module/type.scm 58 */
																																	if (NULLP(CDR
																																			(BgL_cdrzd215316zd2_2025)))
																																		{
																																			obj_t
																																				BgL_nullzd2valuezd2_5899;
																																			obj_t
																																				BgL_parentz00_5898;
																																			obj_t
																																				BgL_namez00_5897;
																																			obj_t
																																				BgL_childz00_5896;
																																			BgL_childz00_5896
																																				=
																																				BgL_carzd215296zd2_2017;
																																			BgL_namez00_5897
																																				=
																																				BgL_carzd215307zd2_2020;
																																			BgL_parentz00_5898
																																				=
																																				BgL_carzd215315zd2_2024;
																																			BgL_nullzd2valuezd2_5899
																																				=
																																				BgL_carzd215320zd2_2028;
																																			BgL_nullzd2valuezd2_229
																																				=
																																				BgL_nullzd2valuezd2_5899;
																																			BgL_parentz00_228
																																				=
																																				BgL_parentz00_5898;
																																			BgL_namez00_227
																																				=
																																				BgL_namez00_5897;
																																			BgL_childz00_226
																																				=
																																				BgL_childz00_5896;
																																			goto
																																				BgL_tagzd2371zd2_230;
																																		}
																																	else
																																		{	/* Module/type.scm 58 */
																																			goto
																																				BgL_tagzd2374zd2_239;
																																		}
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									else
																										{	/* Module/type.scm 58 */
																											goto BgL_tagzd2374zd2_239;
																										}
																								}
																							else
																								{	/* Module/type.scm 58 */
																									goto BgL_tagzd2374zd2_239;
																								}
																						}
																					else
																						{	/* Module/type.scm 58 */
																							goto BgL_tagzd2374zd2_239;
																						}
																				}
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		goto BgL_tagzd2374zd2_239;
																	}
															}
														else
															{	/* Module/type.scm 58 */
																obj_t BgL_cdrzd215482zd2_2033;

																BgL_cdrzd215482zd2_2033 =
																	CDR(((obj_t) BgL_clausez00_28));
																{	/* Module/type.scm 58 */
																	obj_t BgL_carzd215487zd2_2034;
																	obj_t BgL_cdrzd215488zd2_2035;

																	BgL_carzd215487zd2_2034 =
																		CAR(((obj_t) BgL_cdrzd215482zd2_2033));
																	BgL_cdrzd215488zd2_2035 =
																		CDR(((obj_t) BgL_cdrzd215482zd2_2033));
																	if (SYMBOLP(BgL_carzd215487zd2_2034))
																		{	/* Module/type.scm 58 */
																			if (PAIRP(BgL_cdrzd215488zd2_2035))
																				{	/* Module/type.scm 58 */
																					obj_t BgL_carzd215495zd2_2038;
																					obj_t BgL_cdrzd215496zd2_2039;

																					BgL_carzd215495zd2_2038 =
																						CAR(BgL_cdrzd215488zd2_2035);
																					BgL_cdrzd215496zd2_2039 =
																						CDR(BgL_cdrzd215488zd2_2035);
																					if (STRINGP(BgL_carzd215495zd2_2038))
																						{	/* Module/type.scm 58 */
																							if (PAIRP
																								(BgL_cdrzd215496zd2_2039))
																								{	/* Module/type.scm 58 */
																									obj_t BgL_carzd215501zd2_2042;
																									obj_t BgL_cdrzd215502zd2_2043;

																									BgL_carzd215501zd2_2042 =
																										CAR
																										(BgL_cdrzd215496zd2_2039);
																									BgL_cdrzd215502zd2_2043 =
																										CDR
																										(BgL_cdrzd215496zd2_2039);
																									if (PAIRP
																										(BgL_carzd215501zd2_2042))
																										{	/* Module/type.scm 58 */
																											if (PAIRP
																												(BgL_cdrzd215502zd2_2043))
																												{	/* Module/type.scm 58 */
																													obj_t
																														BgL_carzd215506zd2_2046;
																													BgL_carzd215506zd2_2046
																														=
																														CAR
																														(BgL_cdrzd215502zd2_2043);
																													if (SYMBOLP
																														(BgL_carzd215506zd2_2046))
																														{	/* Module/type.scm 58 */
																															if (NULLP(CDR
																																	(BgL_cdrzd215502zd2_2043)))
																																{
																																	obj_t
																																		BgL_nullzd2valuezd2_5931;
																																	obj_t
																																		BgL_parentz00_5930;
																																	obj_t
																																		BgL_namez00_5929;
																																	obj_t
																																		BgL_childz00_5928;
																																	BgL_childz00_5928
																																		=
																																		BgL_carzd215487zd2_2034;
																																	BgL_namez00_5929
																																		=
																																		BgL_carzd215495zd2_2038;
																																	BgL_parentz00_5930
																																		=
																																		BgL_carzd215501zd2_2042;
																																	BgL_nullzd2valuezd2_5931
																																		=
																																		BgL_carzd215506zd2_2046;
																																	BgL_nullzd2valuezd2_229
																																		=
																																		BgL_nullzd2valuezd2_5931;
																																	BgL_parentz00_228
																																		=
																																		BgL_parentz00_5930;
																																	BgL_namez00_227
																																		=
																																		BgL_namez00_5929;
																																	BgL_childz00_226
																																		=
																																		BgL_childz00_5928;
																																	goto
																																		BgL_tagzd2371zd2_230;
																																}
																															else
																																{	/* Module/type.scm 58 */
																																	goto
																																		BgL_tagzd2374zd2_239;
																																}
																														}
																													else
																														{	/* Module/type.scm 58 */
																															goto
																																BgL_tagzd2374zd2_239;
																														}
																												}
																											else
																												{	/* Module/type.scm 58 */
																													goto
																														BgL_tagzd2374zd2_239;
																												}
																										}
																									else
																										{	/* Module/type.scm 58 */
																											goto BgL_tagzd2374zd2_239;
																										}
																								}
																							else
																								{	/* Module/type.scm 58 */
																									goto BgL_tagzd2374zd2_239;
																								}
																						}
																					else
																						{	/* Module/type.scm 58 */
																							goto BgL_tagzd2374zd2_239;
																						}
																				}
																			else
																				{	/* Module/type.scm 58 */
																					goto BgL_tagzd2374zd2_239;
																				}
																		}
																	else
																		{	/* Module/type.scm 58 */
																			goto BgL_tagzd2374zd2_239;
																		}
																}
															}
													}
												else
													{	/* Module/type.scm 58 */
														goto BgL_tagzd2374zd2_239;
													}
											}
										else
											{	/* Module/type.scm 58 */
												obj_t BgL_cdrzd215683zd2_2051;

												BgL_cdrzd215683zd2_2051 =
													CDR(((obj_t) BgL_clausez00_28));
												if (
													(CAR(
															((obj_t) BgL_clausez00_28)) == CNST_TABLE_REF(1)))
													{	/* Module/type.scm 58 */
														if (PAIRP(BgL_cdrzd215683zd2_2051))
															{	/* Module/type.scm 58 */
																obj_t BgL_carzd215688zd2_2055;
																obj_t BgL_cdrzd215689zd2_2056;

																BgL_carzd215688zd2_2055 =
																	CAR(BgL_cdrzd215683zd2_2051);
																BgL_cdrzd215689zd2_2056 =
																	CDR(BgL_cdrzd215683zd2_2051);
																if (SYMBOLP(BgL_carzd215688zd2_2055))
																	{	/* Module/type.scm 58 */
																		if (PAIRP(BgL_cdrzd215689zd2_2056))
																			{	/* Module/type.scm 58 */
																				obj_t BgL_carzd215696zd2_2059;
																				obj_t BgL_cdrzd215697zd2_2060;

																				BgL_carzd215696zd2_2059 =
																					CAR(BgL_cdrzd215689zd2_2056);
																				BgL_cdrzd215697zd2_2060 =
																					CDR(BgL_cdrzd215689zd2_2056);
																				if (STRINGP(BgL_carzd215696zd2_2059))
																					{	/* Module/type.scm 58 */
																						if (PAIRP(BgL_cdrzd215697zd2_2060))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd215702zd2_2063;
																								obj_t BgL_cdrzd215703zd2_2064;

																								BgL_carzd215702zd2_2063 =
																									CAR(BgL_cdrzd215697zd2_2060);
																								BgL_cdrzd215703zd2_2064 =
																									CDR(BgL_cdrzd215697zd2_2060);
																								if (PAIRP
																									(BgL_carzd215702zd2_2063))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd215703zd2_2064))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_carzd215707zd2_2067;
																												BgL_carzd215707zd2_2067
																													=
																													CAR
																													(BgL_cdrzd215703zd2_2064);
																												if (SYMBOLP
																													(BgL_carzd215707zd2_2067))
																													{	/* Module/type.scm 58 */
																														if (NULLP(CDR
																																(BgL_cdrzd215703zd2_2064)))
																															{
																																obj_t
																																	BgL_nullzd2valuezd2_5968;
																																obj_t
																																	BgL_parentz00_5967;
																																obj_t
																																	BgL_namez00_5966;
																																obj_t
																																	BgL_childz00_5965;
																																BgL_childz00_5965
																																	=
																																	BgL_carzd215688zd2_2055;
																																BgL_namez00_5966
																																	=
																																	BgL_carzd215696zd2_2059;
																																BgL_parentz00_5967
																																	=
																																	BgL_carzd215702zd2_2063;
																																BgL_nullzd2valuezd2_5968
																																	=
																																	BgL_carzd215707zd2_2067;
																																BgL_nullzd2valuezd2_229
																																	=
																																	BgL_nullzd2valuezd2_5968;
																																BgL_parentz00_228
																																	=
																																	BgL_parentz00_5967;
																																BgL_namez00_227
																																	=
																																	BgL_namez00_5966;
																																BgL_childz00_226
																																	=
																																	BgL_childz00_5965;
																																goto
																																	BgL_tagzd2371zd2_230;
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				goto BgL_tagzd2374zd2_239;
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		goto BgL_tagzd2374zd2_239;
																	}
															}
														else
															{	/* Module/type.scm 58 */
																goto BgL_tagzd2374zd2_239;
															}
													}
												else
													{	/* Module/type.scm 58 */
														if (
															(CAR(
																	((obj_t) BgL_clausez00_28)) ==
																CNST_TABLE_REF(7)))
															{	/* Module/type.scm 58 */
																if (PAIRP(BgL_cdrzd215683zd2_2051))
																	{	/* Module/type.scm 58 */
																		obj_t BgL_carzd215867zd2_2076;
																		obj_t BgL_cdrzd215868zd2_2077;

																		BgL_carzd215867zd2_2076 =
																			CAR(BgL_cdrzd215683zd2_2051);
																		BgL_cdrzd215868zd2_2077 =
																			CDR(BgL_cdrzd215683zd2_2051);
																		if (SYMBOLP(BgL_carzd215867zd2_2076))
																			{	/* Module/type.scm 58 */
																				if (PAIRP(BgL_cdrzd215868zd2_2077))
																					{	/* Module/type.scm 58 */
																						obj_t BgL_carzd215873zd2_2080;

																						BgL_carzd215873zd2_2080 =
																							CAR(BgL_cdrzd215868zd2_2077);
																						if (PAIRP(BgL_carzd215873zd2_2080))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd215876zd2_2082;

																								BgL_carzd215876zd2_2082 =
																									CAR(BgL_carzd215873zd2_2080);
																								if (SYMBOLP
																									(BgL_carzd215876zd2_2082))
																									{	/* Module/type.scm 58 */
																										if (NULLP(CDR
																												(BgL_carzd215873zd2_2080)))
																											{	/* Module/type.scm 58 */
																												if (NULLP(CDR
																														(BgL_cdrzd215868zd2_2077)))
																													{	/* Module/type.scm 58 */
																														return
																															BGl_delayzd2tvectorzd2typez12z12zzmodule_typez00
																															(BgL_carzd215867zd2_2076,
																															BgL_carzd215876zd2_2082,
																															BgL_clausez00_28,
																															BgL_importz00_27);
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				goto BgL_tagzd2374zd2_239;
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		goto BgL_tagzd2374zd2_239;
																	}
															}
														else
															{	/* Module/type.scm 58 */
																obj_t BgL_cdrzd215946zd2_2090;

																BgL_cdrzd215946zd2_2090 =
																	CDR(((obj_t) BgL_clausez00_28));
																if (
																	(CAR(
																			((obj_t) BgL_clausez00_28)) ==
																		CNST_TABLE_REF(6)))
																	{	/* Module/type.scm 58 */
																		if (PAIRP(BgL_cdrzd215946zd2_2090))
																			{	/* Module/type.scm 58 */
																				obj_t BgL_carzd215951zd2_2094;
																				obj_t BgL_cdrzd215952zd2_2095;

																				BgL_carzd215951zd2_2094 =
																					CAR(BgL_cdrzd215946zd2_2090);
																				BgL_cdrzd215952zd2_2095 =
																					CDR(BgL_cdrzd215946zd2_2090);
																				if (SYMBOLP(BgL_carzd215951zd2_2094))
																					{	/* Module/type.scm 58 */
																						if (PAIRP(BgL_cdrzd215952zd2_2095))
																							{	/* Module/type.scm 58 */
																								obj_t BgL_carzd215959zd2_2098;
																								obj_t BgL_cdrzd215960zd2_2099;

																								BgL_carzd215959zd2_2098 =
																									CAR(BgL_cdrzd215952zd2_2095);
																								BgL_cdrzd215960zd2_2099 =
																									CDR(BgL_cdrzd215952zd2_2095);
																								if (SYMBOLP
																									(BgL_carzd215959zd2_2098))
																									{	/* Module/type.scm 58 */
																										if (PAIRP
																											(BgL_cdrzd215960zd2_2099))
																											{	/* Module/type.scm 58 */
																												obj_t
																													BgL_cdrzd215967zd2_2102;
																												BgL_cdrzd215967zd2_2102
																													=
																													CDR
																													(BgL_cdrzd215960zd2_2099);
																												if (PAIRP
																													(BgL_cdrzd215967zd2_2102))
																													{	/* Module/type.scm 58 */
																														if (NULLP(CDR
																																(BgL_cdrzd215967zd2_2102)))
																															{
																																obj_t
																																	BgL_coercez00_6026;
																																obj_t
																																	BgL_checkz00_6024;
																																obj_t
																																	BgL_toz00_6023;
																																obj_t
																																	BgL_fromz00_6022;
																																BgL_fromz00_6022
																																	=
																																	BgL_carzd215951zd2_2094;
																																BgL_toz00_6023 =
																																	BgL_carzd215959zd2_2098;
																																BgL_checkz00_6024
																																	=
																																	CAR
																																	(BgL_cdrzd215960zd2_2099);
																																BgL_coercez00_6026
																																	=
																																	CAR
																																	(BgL_cdrzd215967zd2_2102);
																																BgL_coercez00_237
																																	=
																																	BgL_coercez00_6026;
																																BgL_checkz00_236
																																	=
																																	BgL_checkz00_6024;
																																BgL_toz00_235 =
																																	BgL_toz00_6023;
																																BgL_fromz00_234
																																	=
																																	BgL_fromz00_6022;
																																goto
																																	BgL_tagzd2373zd2_238;
																															}
																														else
																															{	/* Module/type.scm 58 */
																																goto
																																	BgL_tagzd2374zd2_239;
																															}
																													}
																												else
																													{	/* Module/type.scm 58 */
																														goto
																															BgL_tagzd2374zd2_239;
																													}
																											}
																										else
																											{	/* Module/type.scm 58 */
																												goto
																													BgL_tagzd2374zd2_239;
																											}
																									}
																								else
																									{	/* Module/type.scm 58 */
																										goto BgL_tagzd2374zd2_239;
																									}
																							}
																						else
																							{	/* Module/type.scm 58 */
																								goto BgL_tagzd2374zd2_239;
																							}
																					}
																				else
																					{	/* Module/type.scm 58 */
																						goto BgL_tagzd2374zd2_239;
																					}
																			}
																		else
																			{	/* Module/type.scm 58 */
																				goto BgL_tagzd2374zd2_239;
																			}
																	}
																else
																	{	/* Module/type.scm 58 */
																		goto BgL_tagzd2374zd2_239;
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* Module/type.scm 58 */
						goto BgL_tagzd2374zd2_239;
					}
			}
		}

	}



/* module-tvector-clause */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2tvectorzd2clausez00zzmodule_typez00(obj_t
		BgL_idz00_30, obj_t BgL_itemzd2typezd2_31, obj_t BgL_clausez00_32,
		obj_t BgL_importz00_33)
	{
		{	/* Module/type.scm 157 */
			{	/* Module/type.scm 158 */
				BgL_typez00_bglt BgL_arg2826z00_3648;

				BgL_arg2826z00_3648 =
					BGl_declarezd2tvectorzd2typez12z12zztvector_tvectorz00(BgL_idz00_30,
					BgL_itemzd2typezd2_31, BgL_clausez00_32);
				return BGl_delayzd2tvectorz12zc0zzmodule_typez00(((obj_t)
						BgL_arg2826z00_3648), BgL_clausez00_32, CBOOL(BgL_importz00_33));
			}
		}

	}



/* &module-tvector-clause */
	obj_t BGl_z62modulezd2tvectorzd2clausez62zzmodule_typez00(obj_t
		BgL_envz00_3729, obj_t BgL_idz00_3730, obj_t BgL_itemzd2typezd2_3731,
		obj_t BgL_clausez00_3732, obj_t BgL_importz00_3733)
	{
		{	/* Module/type.scm 157 */
			return
				BGl_modulezd2tvectorzd2clausez00zzmodule_typez00(BgL_idz00_3730,
				BgL_itemzd2typezd2_3731, BgL_clausez00_3732, BgL_importz00_3733);
		}

	}



/* &type-finalizer */
	obj_t BGl_z62typezd2finaliza7erz17zzmodule_typez00(obj_t BgL_envz00_3728)
	{
		{	/* Module/type.scm 163 */
			{	/* Module/type.scm 164 */
				obj_t BgL_tvfz00_3751;

				BgL_tvfz00_3751 = BGl_tvectorzd2finaliza7erz75zzmodule_typez00();
				{	/* Module/type.scm 165 */
					bool_t BgL_test3410z00_6034;

					if (STRUCTP(BgL_tvfz00_3751))
						{	/* Module/type.scm 165 */
							BgL_test3410z00_6034 =
								(STRUCT_KEY(BgL_tvfz00_3751) == CNST_TABLE_REF(8));
						}
					else
						{	/* Module/type.scm 165 */
							BgL_test3410z00_6034 = ((bool_t) 0);
						}
					if (BgL_test3410z00_6034)
						{	/* Module/type.scm 166 */
							obj_t BgL_list2828z00_3752;

							BgL_list2828z00_3752 = MAKE_YOUNG_PAIR(BgL_tvfz00_3751, BNIL);
							return BgL_list2828z00_3752;
						}
					else
						{	/* Module/type.scm 165 */
							return CNST_TABLE_REF(9);
						}
				}
			}
		}

	}



/* delay-tvector-type! */
	obj_t BGl_delayzd2tvectorzd2typez12z12zzmodule_typez00(obj_t BgL_idz00_34,
		obj_t BgL_itemzd2typezd2_35, obj_t BgL_clausez00_36, obj_t BgL_importz00_37)
	{
		{	/* Module/type.scm 177 */
			{	/* Module/type.scm 179 */
				obj_t BgL_arg2829z00_3654;

				{	/* Module/type.scm 179 */
					obj_t BgL_list2830z00_3655;

					{	/* Module/type.scm 179 */
						obj_t BgL_arg2831z00_3656;

						{	/* Module/type.scm 179 */
							obj_t BgL_arg2832z00_3657;

							{	/* Module/type.scm 179 */
								obj_t BgL_arg2833z00_3658;

								BgL_arg2833z00_3658 = MAKE_YOUNG_PAIR(BgL_importz00_37, BNIL);
								BgL_arg2832z00_3657 =
									MAKE_YOUNG_PAIR(BgL_clausez00_36, BgL_arg2833z00_3658);
							}
							BgL_arg2831z00_3656 =
								MAKE_YOUNG_PAIR(BgL_itemzd2typezd2_35, BgL_arg2832z00_3657);
						}
						BgL_list2830z00_3655 =
							MAKE_YOUNG_PAIR(BgL_idz00_34, BgL_arg2831z00_3656);
					}
					BgL_arg2829z00_3654 = BgL_list2830z00_3655;
				}
				return (BGl_za2tvectorzd2typesza2zd2zzmodule_typez00 =
					MAKE_YOUNG_PAIR(BgL_arg2829z00_3654,
						BGl_za2tvectorzd2typesza2zd2zzmodule_typez00), BUNSPEC);
			}
		}

	}



/* delay-tvector! */
	BGL_EXPORTED_DEF obj_t BGl_delayzd2tvectorz12zc0zzmodule_typez00(obj_t
		BgL_tvz00_38, obj_t BgL_clausez00_39, bool_t BgL_importz00_40)
	{
		{	/* Module/type.scm 189 */
			{	/* Module/type.scm 190 */
				obj_t BgL_arg2834z00_2431;

				{	/* Module/type.scm 190 */
					obj_t BgL_list2835z00_2432;

					{	/* Module/type.scm 190 */
						obj_t BgL_arg2836z00_2433;

						{	/* Module/type.scm 190 */
							obj_t BgL_arg2837z00_2434;

							BgL_arg2837z00_2434 =
								MAKE_YOUNG_PAIR(BBOOL(BgL_importz00_40), BNIL);
							BgL_arg2836z00_2433 =
								MAKE_YOUNG_PAIR(BgL_clausez00_39, BgL_arg2837z00_2434);
						}
						BgL_list2835z00_2432 =
							MAKE_YOUNG_PAIR(BgL_tvz00_38, BgL_arg2836z00_2433);
					}
					BgL_arg2834z00_2431 = BgL_list2835z00_2432;
				}
				BGl_za2tvectorsza2z00zzmodule_typez00 =
					MAKE_YOUNG_PAIR(BgL_arg2834z00_2431,
					BGl_za2tvectorsza2z00zzmodule_typez00);
			}
			return BgL_tvz00_38;
		}

	}



/* &delay-tvector! */
	obj_t BGl_z62delayzd2tvectorz12za2zzmodule_typez00(obj_t BgL_envz00_3734,
		obj_t BgL_tvz00_3735, obj_t BgL_clausez00_3736, obj_t BgL_importz00_3737)
	{
		{	/* Module/type.scm 189 */
			return
				BGl_delayzd2tvectorz12zc0zzmodule_typez00(BgL_tvz00_3735,
				BgL_clausez00_3736, CBOOL(BgL_importz00_3737));
		}

	}



/* tvector-finalizer */
	BGL_EXPORTED_DEF obj_t BGl_tvectorzd2finaliza7erz75zzmodule_typez00(void)
	{
		{	/* Module/type.scm 196 */
			{
				obj_t BgL_l1078z00_2436;

				BgL_l1078z00_2436 = BGl_za2tvectorzd2typesza2zd2zzmodule_typez00;
			BgL_zc3z04anonymousza32838ze3z87_2437:
				if (PAIRP(BgL_l1078z00_2436))
					{	/* Module/type.scm 198 */
						{	/* Module/type.scm 198 */
							obj_t BgL_tz00_2439;

							BgL_tz00_2439 = CAR(BgL_l1078z00_2436);
							{	/* Module/type.scm 198 */
								obj_t BgL_runner2844z00_2444;

								BgL_runner2844z00_2444 = BgL_tz00_2439;
								{	/* Module/type.scm 198 */
									obj_t BgL_aux2840z00_2440;

									BgL_aux2840z00_2440 = CAR(BgL_runner2844z00_2444);
									BgL_runner2844z00_2444 = CDR(BgL_runner2844z00_2444);
									{	/* Module/type.scm 198 */
										obj_t BgL_aux2841z00_2441;

										BgL_aux2841z00_2441 = CAR(BgL_runner2844z00_2444);
										BgL_runner2844z00_2444 = CDR(BgL_runner2844z00_2444);
										{	/* Module/type.scm 198 */
											obj_t BgL_aux2842z00_2442;

											BgL_aux2842z00_2442 = CAR(BgL_runner2844z00_2444);
											BgL_runner2844z00_2444 = CDR(BgL_runner2844z00_2444);
											{	/* Module/type.scm 198 */
												obj_t BgL_aux2843z00_2443;

												BgL_aux2843z00_2443 = CAR(BgL_runner2844z00_2444);
												{	/* Module/type.scm 158 */
													BgL_typez00_bglt BgL_arg2826z00_3671;

													BgL_arg2826z00_3671 =
														BGl_declarezd2tvectorzd2typez12z12zztvector_tvectorz00
														(BgL_aux2840z00_2440, BgL_aux2841z00_2441,
														BgL_aux2842z00_2442);
													BGl_delayzd2tvectorz12zc0zzmodule_typez00(((obj_t)
															BgL_arg2826z00_3671), BgL_aux2842z00_2442,
														CBOOL(BgL_aux2843z00_2443));
												}
											}
										}
									}
								}
							}
						}
						{
							obj_t BgL_l1078z00_6068;

							BgL_l1078z00_6068 = CDR(BgL_l1078z00_2436);
							BgL_l1078z00_2436 = BgL_l1078z00_6068;
							goto BgL_zc3z04anonymousza32838ze3z87_2437;
						}
					}
				else
					{	/* Module/type.scm 198 */
						((bool_t) 1);
					}
			}
			BGl_za2tvectorzd2typesza2zd2zzmodule_typez00 = BNIL;
			if (NULLP(BGl_za2tvectorsza2z00zzmodule_typez00))
				{	/* Module/type.scm 201 */
					return CNST_TABLE_REF(9);
				}
			else
				{	/* Module/type.scm 203 */
					obj_t BgL_accessesz00_2448;

					BgL_accessesz00_2448 =
						BGl_zc3z04anonymousza32857ze3ze70z60zzmodule_typez00
						(BGl_za2tvectorsza2z00zzmodule_typez00);
					{	/* Module/type.scm 206 */
						obj_t BgL_resz00_2449;

						{	/* Module/type.scm 206 */
							obj_t BgL_arg2848z00_2450;
							obj_t BgL_arg2856z00_2451;

							BgL_arg2848z00_2450 =
								BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(7));
							BgL_arg2856z00_2451 = bgl_reverse_bang(BgL_accessesz00_2448);
							{	/* Module/type.scm 206 */
								obj_t BgL_newz00_3681;

								BgL_newz00_3681 = create_struct(CNST_TABLE_REF(8), (int) (5L));
								{	/* Module/type.scm 206 */
									int BgL_tmpz00_6080;

									BgL_tmpz00_6080 = (int) (4L);
									STRUCT_SET(BgL_newz00_3681, BgL_tmpz00_6080, BFALSE);
								}
								{	/* Module/type.scm 206 */
									int BgL_tmpz00_6083;

									BgL_tmpz00_6083 = (int) (3L);
									STRUCT_SET(BgL_newz00_3681, BgL_tmpz00_6083, BTRUE);
								}
								{	/* Module/type.scm 206 */
									int BgL_tmpz00_6086;

									BgL_tmpz00_6086 = (int) (2L);
									STRUCT_SET(BgL_newz00_3681, BgL_tmpz00_6086,
										BgL_arg2856z00_2451);
								}
								{	/* Module/type.scm 206 */
									obj_t BgL_auxz00_6091;
									int BgL_tmpz00_6089;

									BgL_auxz00_6091 = BINT(6L);
									BgL_tmpz00_6089 = (int) (1L);
									STRUCT_SET(BgL_newz00_3681, BgL_tmpz00_6089, BgL_auxz00_6091);
								}
								{	/* Module/type.scm 206 */
									int BgL_tmpz00_6094;

									BgL_tmpz00_6094 = (int) (0L);
									STRUCT_SET(BgL_newz00_3681, BgL_tmpz00_6094,
										BgL_arg2848z00_2450);
								}
								BgL_resz00_2449 = BgL_newz00_3681;
						}}
						BGl_za2tvectorsza2z00zzmodule_typez00 = BNIL;
						return BgL_resz00_2449;
					}
				}
		}

	}



/* <@anonymous:2857>~0 */
	obj_t BGl_zc3z04anonymousza32857ze3ze70z60zzmodule_typez00(obj_t
		BgL_l1081z00_2453)
	{
		{	/* Module/type.scm 203 */
			if (NULLP(BgL_l1081z00_2453))
				{	/* Module/type.scm 203 */
					return BNIL;
				}
			else
				{	/* Module/type.scm 204 */
					obj_t BgL_arg2859z00_2456;
					obj_t BgL_arg2864z00_2457;

					{	/* Module/type.scm 204 */
						obj_t BgL_tvz00_2458;

						BgL_tvz00_2458 = CAR(((obj_t) BgL_l1081z00_2453));
						{	/* Module/type.scm 204 */
							obj_t BgL_runner2868z00_2462;

							BgL_runner2868z00_2462 = BgL_tvz00_2458;
							{	/* Module/type.scm 204 */
								BgL_typez00_bglt BgL_aux2865z00_2459;

								{	/* Module/type.scm 204 */
									obj_t BgL_pairz00_3675;

									BgL_pairz00_3675 = BgL_runner2868z00_2462;
									BgL_aux2865z00_2459 =
										((BgL_typez00_bglt) CAR(BgL_pairz00_3675));
								}
								BgL_runner2868z00_2462 = CDR(BgL_runner2868z00_2462);
								{	/* Module/type.scm 204 */
									obj_t BgL_aux2866z00_2460;

									BgL_aux2866z00_2460 = CAR(BgL_runner2868z00_2462);
									BgL_runner2868z00_2462 = CDR(BgL_runner2868z00_2462);
									{	/* Module/type.scm 204 */
										bool_t BgL_aux2867z00_2461;

										{	/* Module/type.scm 204 */
											obj_t BgL_pairz00_3679;

											BgL_pairz00_3679 = BgL_runner2868z00_2462;
											BgL_aux2867z00_2461 = CBOOL(CAR(BgL_pairz00_3679));
										}
										BgL_arg2859z00_2456 =
											BGl_makezd2tvectorzd2accessesz00zztvector_accessz00
											(BgL_aux2865z00_2459, BgL_aux2866z00_2460,
											BgL_aux2867z00_2461);
									}
								}
							}
						}
					}
					{	/* Module/type.scm 203 */
						obj_t BgL_arg2870z00_2463;

						BgL_arg2870z00_2463 = CDR(((obj_t) BgL_l1081z00_2453));
						BgL_arg2864z00_2457 =
							BGl_zc3z04anonymousza32857ze3ze70z60zzmodule_typez00
							(BgL_arg2870z00_2463);
					}
					return bgl_append2(BgL_arg2859z00_2456, BgL_arg2864z00_2457);
				}
		}

	}



/* &tvector-finalizer */
	obj_t BGl_z62tvectorzd2finaliza7erz17zzmodule_typez00(obj_t BgL_envz00_3738)
	{
		{	/* Module/type.scm 196 */
			return BGl_tvectorzd2finaliza7erz75zzmodule_typez00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_typez00(void)
	{
		{	/* Module/type.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_typez00(void)
	{
		{	/* Module/type.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_typez00(void)
	{
		{	/* Module/type.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_typez00(void)
	{
		{	/* Module/type.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			BGl_modulezd2initializa7ationz75zztype_coercionz00(116865673L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			BGl_modulezd2initializa7ationz75zztvector_accessz00(189470597L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2900z00zzmodule_typez00));
		}

	}

#ifdef __cplusplus
}
#endif
