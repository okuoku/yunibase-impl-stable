/*===========================================================================*/
/*   (Module/impuse.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Module/impuse.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_MODULE_IMPUSE_TYPE_DEFINITIONS
#define BGL_MODULE_IMPUSE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_ccompz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_producerz00;
		obj_t BgL_consumerz00;
		obj_t BgL_finaliza7erza7;
	}               *BgL_ccompz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_importz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_modulez00;
		long BgL_numberz00;
		obj_t BgL_modez00;
		obj_t BgL_varsz00;
		obj_t BgL_aliasesz00;
		obj_t BgL_checksumz00;
		obj_t BgL_locz00;
		obj_t BgL_srcz00;
		obj_t BgL_declz00;
		obj_t BgL_providez00;
		obj_t BgL_codez00;
		obj_t BgL_accessz00;
	}                *BgL_importz00_bglt;


#endif													// BGL_MODULE_IMPUSE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_readzd2importz12zc0zzmodule_impusez00(obj_t);
	static obj_t BGl_importzd21zd2modulez00zzmodule_impusez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_z62importzd2withzd2modulez12z70zzmodule_impusez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzmodule_impusez00 = BUNSPEC;
	extern obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_comptimezd2expandzf2errorz20zzexpand_epsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32004ze3ze5zzmodule_impusez00(obj_t,
		obj_t);
	extern obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00;
	extern obj_t BGl_fastzd2idzd2ofzd2idzd2zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2fromzd2compilerz00zzmodule_impusez00(void);
	static obj_t BGl_toplevelzd2initzd2zzmodule_impusez00(void);
	static obj_t BGl_z62makezd2importzd2compilerz62zzmodule_impusez00(obj_t);
	extern obj_t BGl_getzd2includezd2consumedzd2codezd2zzmodule_includez00(void);
	extern obj_t
		BGl_getzd2includezd2consumedzd2directivezd2zzmodule_includez00(void);
	BGL_IMPORT obj_t BGl_evalz00zz__evalz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t
		BGl_z62initializa7ezd2importedzd2moduleszc5zzmodule_impusez00(obj_t, obj_t);
	extern obj_t BGl_declarezd2classz12zc0zzmodule_classz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzmodule_impusez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_impusezd2producerzd2zzmodule_impusez00(obj_t);
	BGL_IMPORT obj_t BGl_readerzd2resetz12zc0zz__readerz00(void);
	extern long BGl_modulezd2checksumzd2zzmodule_checksumz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32040ze3ze5zzmodule_impusez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32032ze3ze5zzmodule_impusez00(obj_t);
	static obj_t BGl_z62impusezd2producerzb0zzmodule_impusez00(obj_t, obj_t);
	extern obj_t BGl_declarezd2widezd2classz12z12zzmodule_classz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32024ze3ze5zzmodule_impusez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2importzd2compilerz00zzmodule_impusez00(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzmodule_impusez00(void);
	extern obj_t
		BGl_resetzd2includezd2consumedzd2directivez12zc0zzmodule_includez00(void);
	static obj_t BGl_z62zc3z04anonymousza32017ze3ze5zzmodule_impusez00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzmodule_impusez00(obj_t);
	BGL_IMPORT obj_t BGl_dirnamez00zz__osz00(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62zc3z04anonymousza31305ze3ze5zzmodule_impusez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2debugzd2moduleza2zd2zzengine_paramz00;
	static obj_t BGl_za2importedzd2moduleszd2inzd2unitza2zd2zzmodule_impusez00 =
		BUNSPEC;
	static obj_t BGl_importzd2allzd2modulez00zzmodule_impusez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_importzd2withzd2modulez12z12zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62importzd2parserzb0zzmodule_impusez00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62getzd2importedzd2modulesz62zzmodule_impusez00(obj_t);
	static obj_t BGl_impusezd2parserzd2zzmodule_impusez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2usezd2compilerz00zzmodule_impusez00(void);
	static obj_t BGl_za2importsza2z00zzmodule_impusez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2importedzd2modulesz00zzmodule_impusez00(void);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzmodule_impusez00(void);
	extern obj_t BGl_inlinezd2finaliza7erz75zzread_inlinez00(void);
	static obj_t BGl_z62zc3z04anonymousza31309ze3ze5zzmodule_impusez00(obj_t);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	static obj_t BGl_importzd2wantedzd2zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_tracezd2initializa7ezd2moduleze70z40zzmodule_impusez00(obj_t,
		obj_t, obj_t);
	static BgL_importz00_bglt BGl_z62lambda2000z62zzmodule_impusez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_importz00_bglt BGl_z62lambda2002z62zzmodule_impusez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32080ze3ze5zzmodule_impusez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32048ze3ze5zzmodule_impusez00(obj_t);
	static obj_t BGl_z62lambda2009z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2010z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2015z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32073ze3ze5zzmodule_impusez00(obj_t);
	static obj_t BGl_z62lambda2016z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	static obj_t BGl_initializa7ezd2moduleze70z92zzmodule_impusez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2ze3listz31zz__hashz00(obj_t);
	static obj_t BGl_z62lambda2022z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2023z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32066ze3ze5zzmodule_impusez00(obj_t);
	extern obj_t BGl_za2modulezd2clauseza2zd2zzmodule_modulez00;
	static obj_t BGl_z62lambda2030z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2031z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31273ze3ze5zzmodule_impusez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_ccompz00zzmodule_modulez00;
	static obj_t BGl_z62lambda2038z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2039z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_remq_bang(obj_t, obj_t);
	extern obj_t BGl_lookzd2forzd2inlineszd2andzd2macrosz00zzread_inlinez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	static obj_t BGl_importzd2everythingzd2zzmodule_impusez00(obj_t);
	static obj_t
		BGl_registerzd2importz12zc0zzmodule_impusez00(BgL_importz00_bglt);
	BGL_IMPORT obj_t BGl_bigloozd2modulezd2resolverz00zz__modulez00(void);
	static obj_t BGl_importzd2modulez12zc0zzmodule_impusez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31274ze3ze5zzmodule_impusez00(obj_t);
	static obj_t BGl_z62lambda2046z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2047z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzmodule_impusez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzexpand_epsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_accessz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_checksumz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzread_readerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__modulez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static long BGl_za2importzd2numberza2zd2zzmodule_impusez00 = 0L;
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62lambda2052z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2053z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static obj_t BGl_z62lambda2058z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2059z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	extern obj_t
		BGl_resetzd2includezd2consumedzd2codez12zc0zzmodule_includez00(void);
	extern obj_t BGl_prognzd2tailzd2expressionsz00zztools_prognz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzmodule_impusez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2064z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2065z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_importzd2parserzd2zzmodule_impusez00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_z62lambda2071z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2072z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31285ze3ze5zzmodule_impusez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32087ze3ze5zzmodule_impusez00(obj_t);
	static obj_t BGl_z62lambda2078z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzmodule_impusez00(void);
	static obj_t BGl_z62lambda2079z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzmodule_impusez00(void);
	extern obj_t BGl_compilerzd2readzd2zzread_readerz00(obj_t);
	static obj_t BGl_z62makezd2usezd2compilerz62zzmodule_impusez00(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzmodule_impusez00(void);
	static obj_t BGl_z62makezd2fromzd2compilerz62zzmodule_impusez00(obj_t);
	extern obj_t BGl_consumezd2modulez12zc0zzmodule_modulez00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzmodule_impusez00(void);
	static obj_t BGl_z62lambda2085z62zzmodule_impusez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2086z62zzmodule_impusez00(obj_t, obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2forzd2eachz00zz__hashz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	extern obj_t BGl_parsezd2prototypezd2zzmodule_prototypez00(obj_t);
	BGL_IMPORT obj_t BGl_za2loadzd2pathza2zd2zz__evalz00;
	static obj_t BGl_z62zc3z04anonymousza31572ze3ze5zzmodule_impusez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2unitz00zzmodule_impusez00(obj_t);
	extern obj_t BGl_prognzd2firstzd2expressionz00zztools_prognz00(obj_t);
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	static obj_t BGl_importz00zzmodule_impusez00 = BUNSPEC;
	static obj_t BGl_importzd2finaliza7erz75zzmodule_impusez00(void);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_aliaszf3ze70z14zzmodule_impusez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_initializa7ezd2importedzd2modulesza7zzmodule_impusez00(obj_t);
	extern obj_t BGl_za2mcozd2includezd2pathza2z00zzengine_paramz00;
	extern obj_t BGl_za2accesszd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_modulezd2addzd2accessz12z12zz__modulez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	static obj_t BGl_z62impusezd2finaliza7erz17zzmodule_impusez00(obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t __cnst[47];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initializa7ezd2importedzd2moduleszd2envz75zzmodule_impusez00,
		BgL_bgl_za762initializa7a7eza72183za7,
		BGl_z62initializa7ezd2importedzd2moduleszc5zzmodule_impusez00, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2128z00zzmodule_impusez00,
		BgL_bgl_string2128za700za7za7m2184za7, "module ~a", 9);
	      DEFINE_STRING(BGl_string2129z00zzmodule_impusez00,
		BgL_bgl_string2129za700za7za7m2185za7, "Illegal \"~a\" clause", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2186za7,
		BGl_z62zc3z04anonymousza31273ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2130z00zzmodule_impusez00,
		BgL_bgl_string2130za700za7za7m2187za7, "Parse error", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2188za7,
		BGl_z62zc3z04anonymousza31274ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2131z00zzmodule_impusez00,
		BgL_bgl_string2131za700za7za7m2189za7, ".", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2190za7,
		BGl_z62zc3z04anonymousza31285ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2191za7,
		BGl_z62zc3z04anonymousza31305ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2192za7,
		BGl_z62zc3z04anonymousza31309ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2134z00zzmodule_impusez00,
		BgL_bgl_string2134za700za7za7m2193za7,
		"bgl_init_module_debug_import(\"~a\", \"~a\")", 40);
	      DEFINE_STRING(BGl_string2135z00zzmodule_impusez00,
		BgL_bgl_string2135za700za7za7m2194za7, "Illegal prototype", 17);
	      DEFINE_STRING(BGl_string2136z00zzmodule_impusez00,
		BgL_bgl_string2136za700za7za7m2195za7, "Cannot find module", 18);
	      DEFINE_STRING(BGl_string2137z00zzmodule_impusez00,
		BgL_bgl_string2137za700za7za7m2196za7,
		"Cannot open source file for module \"~a\"", 39);
	      DEFINE_STRING(BGl_string2138z00zzmodule_impusez00,
		BgL_bgl_string2138za700za7za7m2197za7, "used", 4);
	      DEFINE_STRING(BGl_string2139z00zzmodule_impusez00,
		BgL_bgl_string2139za700za7za7m2198za7, "imported", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2199za7,
		BGl_z62zc3z04anonymousza31572ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2200za7,
		BGl_z62zc3z04anonymousza31616ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2140z00zzmodule_impusez00,
		BgL_bgl_string2140za700za7za7m2201za7, "]", 1);
	      DEFINE_STRING(BGl_string2141z00zzmodule_impusez00,
		BgL_bgl_string2141za700za7za7m2202za7, " module ", 8);
	      DEFINE_STRING(BGl_string2142z00zzmodule_impusez00,
		BgL_bgl_string2142za700za7za7m2203za7, "      [reading ", 15);
	      DEFINE_STRING(BGl_string2143z00zzmodule_impusez00,
		BgL_bgl_string2143za700za7za7m2204za7,
		"Can't find export for these identifiers", 39);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzmodule_impusez00,
		BgL_bgl_za762lambda2010za7622205z00, BGl_z62lambda2010z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2145z00zzmodule_impusez00,
		BgL_bgl_za762lambda2009za7622206z00, BGl_z62lambda2009z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2207za7,
		BGl_z62zc3z04anonymousza32017ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzmodule_impusez00,
		BgL_bgl_za762lambda2016za7622208z00, BGl_z62lambda2016z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzmodule_impusez00,
		BgL_bgl_za762lambda2015za7622209z00, BGl_z62lambda2015z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2210za7,
		BGl_z62zc3z04anonymousza32024ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzmodule_impusez00,
		BgL_bgl_za762lambda2023za7622211z00, BGl_z62lambda2023z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzmodule_impusez00,
		BgL_bgl_za762lambda2022za7622212z00, BGl_z62lambda2022z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2213za7,
		BGl_z62zc3z04anonymousza32032ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2153z00zzmodule_impusez00,
		BgL_bgl_za762lambda2031za7622214z00, BGl_z62lambda2031z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzmodule_impusez00,
		BgL_bgl_za762lambda2030za7622215z00, BGl_z62lambda2030z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2216za7,
		BGl_z62zc3z04anonymousza32040ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzmodule_impusez00,
		BgL_bgl_za762lambda2039za7622217z00, BGl_z62lambda2039z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzmodule_impusez00,
		BgL_bgl_za762lambda2038za7622218z00, BGl_z62lambda2038z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2219za7,
		BGl_z62zc3z04anonymousza32048ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzmodule_impusez00,
		BgL_bgl_za762lambda2047za7622220z00, BGl_z62lambda2047z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzmodule_impusez00,
		BgL_bgl_za762lambda2046za7622221z00, BGl_z62lambda2046z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzmodule_impusez00,
		BgL_bgl_za762lambda2053za7622222z00, BGl_z62lambda2053z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2162z00zzmodule_impusez00,
		BgL_bgl_za762lambda2052za7622223z00, BGl_z62lambda2052z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzmodule_impusez00,
		BgL_bgl_za762lambda2059za7622224z00, BGl_z62lambda2059z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzmodule_impusez00,
		BgL_bgl_za762lambda2058za7622225z00, BGl_z62lambda2058z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2165z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2226za7,
		BGl_z62zc3z04anonymousza32066ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzmodule_impusez00,
		BgL_bgl_za762lambda2065za7622227z00, BGl_z62lambda2065z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzmodule_impusez00,
		BgL_bgl_za762lambda2064za7622228z00, BGl_z62lambda2064z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2229za7,
		BGl_z62zc3z04anonymousza32073ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzmodule_impusez00,
		BgL_bgl_za762lambda2072za7622230z00, BGl_z62lambda2072z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2importedzd2moduleszd2envzd2zzmodule_impusez00,
		BgL_bgl_za762getza7d2importe2231z00,
		BGl_z62getzd2importedzd2modulesz62zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzmodule_impusez00,
		BgL_bgl_za762lambda2071za7622232z00, BGl_z62lambda2071z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2233za7,
		BGl_z62zc3z04anonymousza32080ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2172z00zzmodule_impusez00,
		BgL_bgl_za762lambda2079za7622234z00, BGl_z62lambda2079z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2173z00zzmodule_impusez00,
		BgL_bgl_za762lambda2078za7622235z00, BGl_z62lambda2078z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2180z00zzmodule_impusez00,
		BgL_bgl_string2180za700za7za7m2236za7, "module_impuse", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2174z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2237za7,
		BGl_z62zc3z04anonymousza32087ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2181z00zzmodule_impusez00,
		BgL_bgl_string2181za700za7za7m2238za7,
		"_ module_impuse access code pair-nil provide decl src loc checksum aliases obj vars mode long number symbol expander syntax macro define-macro wide-class final-class abstract-class class svar sgfun sifun sfun unit imported-modules staged pragma::void @ checksum::long from::string begin module (import with from) error nothing with all from use void import ",
		357);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2175z00zzmodule_impusez00,
		BgL_bgl_za762lambda2086za7622239z00, BGl_z62lambda2086z62zzmodule_impusez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2176z00zzmodule_impusez00,
		BgL_bgl_za762lambda2085za7622240z00, BGl_z62lambda2085z62zzmodule_impusez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2177z00zzmodule_impusez00,
		BgL_bgl_za762za7c3za704anonymo2241za7,
		BGl_z62zc3z04anonymousza32004ze3ze5zzmodule_impusez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2178z00zzmodule_impusez00,
		BgL_bgl_za762lambda2002za7622242z00, BGl_z62lambda2002z62zzmodule_impusez00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2179z00zzmodule_impusez00,
		BgL_bgl_za762lambda2000za7622243z00, BGl_z62lambda2000z62zzmodule_impusez00,
		0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_importzd2parserzd2envz00zzmodule_impusez00,
		BgL_bgl_za762importza7d2pars2244z00, va_generic_entry,
		BGl_z62importzd2parserzb0zzmodule_impusez00, BUNSPEC, -4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_importzd2withzd2modulez12zd2envzc0zzmodule_impusez00,
		BgL_bgl_za762importza7d2with2245z00,
		BGl_z62importzd2withzd2modulez12z70zzmodule_impusez00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_impusezd2finaliza7erzd2envza7zzmodule_impusez00,
		BgL_bgl_za762impuseza7d2fina2246z00,
		BGl_z62impusezd2finaliza7erz17zzmodule_impusez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2usezd2compilerzd2envzd2zzmodule_impusez00,
		BgL_bgl_za762makeza7d2useza7d22247za7,
		BGl_z62makezd2usezd2compilerz62zzmodule_impusez00, 0L, BUNSPEC, 0);
	extern obj_t BGl_modulezd2initializa7ationzd2idzd2envz75zzmodule_modulez00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_impusezd2producerzd2envz00zzmodule_impusez00,
		BgL_bgl_za762impuseza7d2prod2248z00,
		BGl_z62impusezd2producerzb0zzmodule_impusez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2importzd2compilerzd2envzd2zzmodule_impusez00,
		BgL_bgl_za762makeza7d2import2249z00,
		BGl_z62makezd2importzd2compilerz62zzmodule_impusez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2fromzd2compilerzd2envzd2zzmodule_impusez00,
		BgL_bgl_za762makeza7d2fromza7d2250za7,
		BGl_z62makezd2fromzd2compilerz62zzmodule_impusez00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzmodule_impusez00));
		     ADD_ROOT((void
				*) (&BGl_za2importedzd2moduleszd2inzd2unitza2zd2zzmodule_impusez00));
		     ADD_ROOT((void *) (&BGl_za2importsza2z00zzmodule_impusez00));
		     ADD_ROOT((void *) (&BGl_importz00zzmodule_impusez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzmodule_impusez00(long
		BgL_checksumz00_2573, char *BgL_fromz00_2574)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzmodule_impusez00))
				{
					BGl_requirezd2initializa7ationz75zzmodule_impusez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzmodule_impusez00();
					BGl_libraryzd2moduleszd2initz00zzmodule_impusez00();
					BGl_cnstzd2initzd2zzmodule_impusez00();
					BGl_importedzd2moduleszd2initz00zzmodule_impusez00();
					BGl_objectzd2initzd2zzmodule_impusez00();
					return BGl_toplevelzd2initzd2zzmodule_impusez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "module_impuse");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "module_impuse");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "module_impuse");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "module_impuse");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "module_impuse");
			BGl_modulezd2initializa7ationz75zz__modulez00(0L, "module_impuse");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "module_impuse");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"module_impuse");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "module_impuse");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "module_impuse");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 15 */
			{	/* Module/impuse.scm 15 */
				obj_t BgL_cportz00_2518;

				{	/* Module/impuse.scm 15 */
					obj_t BgL_stringz00_2525;

					BgL_stringz00_2525 = BGl_string2181z00zzmodule_impusez00;
					{	/* Module/impuse.scm 15 */
						obj_t BgL_startz00_2526;

						BgL_startz00_2526 = BINT(0L);
						{	/* Module/impuse.scm 15 */
							obj_t BgL_endz00_2527;

							BgL_endz00_2527 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2525)));
							{	/* Module/impuse.scm 15 */

								BgL_cportz00_2518 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2525, BgL_startz00_2526, BgL_endz00_2527);
				}}}}
				{
					long BgL_iz00_2519;

					BgL_iz00_2519 = 46L;
				BgL_loopz00_2520:
					if ((BgL_iz00_2519 == -1L))
						{	/* Module/impuse.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Module/impuse.scm 15 */
							{	/* Module/impuse.scm 15 */
								obj_t BgL_arg2182z00_2521;

								{	/* Module/impuse.scm 15 */

									{	/* Module/impuse.scm 15 */
										obj_t BgL_locationz00_2523;

										BgL_locationz00_2523 = BBOOL(((bool_t) 0));
										{	/* Module/impuse.scm 15 */

											BgL_arg2182z00_2521 =
												BGl_readz00zz__readerz00(BgL_cportz00_2518,
												BgL_locationz00_2523);
										}
									}
								}
								{	/* Module/impuse.scm 15 */
									int BgL_tmpz00_2611;

									BgL_tmpz00_2611 = (int) (BgL_iz00_2519);
									CNST_TABLE_SET(BgL_tmpz00_2611, BgL_arg2182z00_2521);
							}}
							{	/* Module/impuse.scm 15 */
								int BgL_auxz00_2524;

								BgL_auxz00_2524 = (int) ((BgL_iz00_2519 - 1L));
								{
									long BgL_iz00_2616;

									BgL_iz00_2616 = (long) (BgL_auxz00_2524);
									BgL_iz00_2519 = BgL_iz00_2616;
									goto BgL_loopz00_2520;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 15 */
			BGl_za2importsza2z00zzmodule_impusez00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL);
			BGl_za2importzd2numberza2zd2zzmodule_impusez00 = 0L;
			return (BGl_za2importedzd2moduleszd2inzd2unitza2zd2zzmodule_impusez00 =
				BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzmodule_impusez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_863;

				BgL_headz00_863 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_864;
					obj_t BgL_tailz00_865;

					BgL_prevz00_864 = BgL_headz00_863;
					BgL_tailz00_865 = BgL_l1z00_1;
				BgL_loopz00_866:
					if (PAIRP(BgL_tailz00_865))
						{
							obj_t BgL_newzd2prevzd2_868;

							BgL_newzd2prevzd2_868 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_865), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_864, BgL_newzd2prevzd2_868);
							{
								obj_t BgL_tailz00_2627;
								obj_t BgL_prevz00_2626;

								BgL_prevz00_2626 = BgL_newzd2prevzd2_868;
								BgL_tailz00_2627 = CDR(BgL_tailz00_865);
								BgL_tailz00_865 = BgL_tailz00_2627;
								BgL_prevz00_864 = BgL_prevz00_2626;
								goto BgL_loopz00_866;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_863);
				}
			}
		}

	}



/* make-import-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2importzd2compilerz00zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 69 */
			{	/* Module/impuse.scm 70 */
				BgL_ccompz00_bglt BgL_new1093z00_888;

				{	/* Module/impuse.scm 71 */
					BgL_ccompz00_bglt BgL_new1092z00_893;

					BgL_new1092z00_893 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/impuse.scm 71 */
						long BgL_arg1284z00_894;

						BgL_arg1284z00_894 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1092z00_893), BgL_arg1284z00_894);
					}
					BgL_new1093z00_888 = BgL_new1092z00_893;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1093z00_888))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1093z00_888))->BgL_producerz00) =
					((obj_t) BGl_impusezd2producerzd2envz00zzmodule_impusez00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1093z00_888))->BgL_consumerz00) =
					((obj_t) BGl_proc2123z00zzmodule_impusez00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1093z00_888))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_proc2124z00zzmodule_impusez00), BUNSPEC);
				return ((obj_t) BgL_new1093z00_888);
			}
		}

	}



/* &make-import-compiler */
	obj_t BGl_z62makezd2importzd2compilerz62zzmodule_impusez00(obj_t
		BgL_envz00_2350)
	{
		{	/* Module/impuse.scm 69 */
			return BGl_makezd2importzd2compilerz00zzmodule_impusez00();
		}

	}



/* &<@anonymous:1274> */
	obj_t BGl_z62zc3z04anonymousza31274ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2351)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(1);
		}

	}



/* &<@anonymous:1273> */
	obj_t BGl_z62zc3z04anonymousza31273ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2352, obj_t BgL_mz00_2353, obj_t BgL_cz00_2354)
	{
		{	/* Module/module.scm 54 */
			return BNIL;
		}

	}



/* make-use-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2usezd2compilerz00zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 77 */
			{	/* Module/impuse.scm 78 */
				BgL_ccompz00_bglt BgL_new1095z00_895;

				{	/* Module/impuse.scm 79 */
					BgL_ccompz00_bglt BgL_new1094z00_899;

					BgL_new1094z00_899 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/impuse.scm 79 */
						long BgL_arg1304z00_900;

						BgL_arg1304z00_900 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1094z00_899), BgL_arg1304z00_900);
					}
					BgL_new1095z00_895 = BgL_new1094z00_899;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1095z00_895))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1095z00_895))->BgL_producerz00) =
					((obj_t) BGl_impusezd2producerzd2envz00zzmodule_impusez00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1095z00_895))->BgL_consumerz00) =
					((obj_t) BGl_proc2125z00zzmodule_impusez00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1095z00_895))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_impusezd2finaliza7erzd2envza7zzmodule_impusez00),
					BUNSPEC);
				return ((obj_t) BgL_new1095z00_895);
			}
		}

	}



/* &make-use-compiler */
	obj_t BGl_z62makezd2usezd2compilerz62zzmodule_impusez00(obj_t BgL_envz00_2358)
	{
		{	/* Module/impuse.scm 77 */
			return BGl_makezd2usezd2compilerz00zzmodule_impusez00();
		}

	}



/* &<@anonymous:1285> */
	obj_t BGl_z62zc3z04anonymousza31285ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2359, obj_t BgL_mz00_2360, obj_t BgL_cz00_2361)
	{
		{	/* Module/module.scm 54 */
			return BNIL;
		}

	}



/* make-from-compiler */
	BGL_EXPORTED_DEF obj_t BGl_makezd2fromzd2compilerz00zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 86 */
			{	/* Module/impuse.scm 87 */
				BgL_ccompz00_bglt BgL_new1097z00_901;

				{	/* Module/impuse.scm 88 */
					BgL_ccompz00_bglt BgL_new1096z00_909;

					BgL_new1096z00_909 =
						((BgL_ccompz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccompz00_bgl))));
					{	/* Module/impuse.scm 88 */
						long BgL_arg1310z00_910;

						BgL_arg1310z00_910 = BGL_CLASS_NUM(BGl_ccompz00zzmodule_modulez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1096z00_909), BgL_arg1310z00_910);
					}
					BgL_new1097z00_901 = BgL_new1096z00_909;
				}
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1097z00_901))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1097z00_901))->BgL_producerz00) =
					((obj_t) BGl_impusezd2producerzd2envz00zzmodule_impusez00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1097z00_901))->BgL_consumerz00) =
					((obj_t) BGl_proc2126z00zzmodule_impusez00), BUNSPEC);
				((((BgL_ccompz00_bglt) COBJECT(BgL_new1097z00_901))->
						BgL_finaliza7erza7) =
					((obj_t) BGl_proc2127z00zzmodule_impusez00), BUNSPEC);
				return ((obj_t) BgL_new1097z00_901);
			}
		}

	}



/* &make-from-compiler */
	obj_t BGl_z62makezd2fromzd2compilerz62zzmodule_impusez00(obj_t
		BgL_envz00_2365)
	{
		{	/* Module/impuse.scm 86 */
			return BGl_makezd2fromzd2compilerz00zzmodule_impusez00();
		}

	}



/* &<@anonymous:1309> */
	obj_t BGl_z62zc3z04anonymousza31309ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2366)
	{
		{	/* Module/module.scm 56 */
			return CNST_TABLE_REF(1);
		}

	}



/* &<@anonymous:1305> */
	obj_t BGl_z62zc3z04anonymousza31305ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2367, obj_t BgL_modulez00_2368, obj_t BgL_clausez00_2369)
	{
		{	/* Module/impuse.scm 90 */
			{	/* Module/impuse.scm 92 */
				obj_t BgL_arg1306z00_2529;

				{	/* Module/impuse.scm 92 */
					obj_t BgL_arg1307z00_2530;

					{	/* Module/impuse.scm 92 */
						obj_t BgL_list1308z00_2531;

						BgL_list1308z00_2531 = MAKE_YOUNG_PAIR(BgL_modulez00_2368, BNIL);
						BgL_arg1307z00_2530 =
							BGl_formatz00zz__r4_output_6_10_3z00
							(BGl_string2128z00zzmodule_impusez00, BgL_list1308z00_2531);
					}
					BgL_arg1306z00_2529 = bstring_to_symbol(BgL_arg1307z00_2530);
				}
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1306z00_2529);
			}
			BGl_impusezd2producerzd2zzmodule_impusez00(BgL_clausez00_2369);
			BGl_leavezd2functionzd2zztools_errorz00();
			return BNIL;
		}

	}



/* register-import! */
	obj_t BGl_registerzd2importz12zc0zzmodule_impusez00(BgL_importz00_bglt
		BgL_importz00_74)
	{
		{	/* Module/impuse.scm 106 */
			BGl_za2importzd2numberza2zd2zzmodule_impusez00 =
				(1L + BGl_za2importzd2numberza2zd2zzmodule_impusez00);
			((((BgL_importz00_bglt) COBJECT(BgL_importz00_74))->BgL_numberz00) =
				((long) BGl_za2importzd2numberza2zd2zzmodule_impusez00), BUNSPEC);
			{	/* Module/impuse.scm 109 */
				obj_t BgL_arg1311z00_911;

				BgL_arg1311z00_911 =
					(((BgL_importz00_bglt) COBJECT(BgL_importz00_74))->BgL_modulez00);
				return
					BGl_hashtablezd2putz12zc0zz__hashz00
					(BGl_za2importsza2z00zzmodule_impusez00, BgL_arg1311z00_911,
					((obj_t) BgL_importz00_74));
			}
		}

	}



/* impuse-producer */
	obj_t BGl_impusezd2producerzd2zzmodule_impusez00(obj_t BgL_clausez00_75)
	{
		{	/* Module/impuse.scm 114 */
			{	/* Module/impuse.scm 115 */
				obj_t BgL_modez00_912;

				BgL_modez00_912 = CAR(((obj_t) BgL_clausez00_75));
				{
					obj_t BgL_protosz00_913;

					if (PAIRP(BgL_clausez00_75))
						{	/* Module/impuse.scm 116 */
							obj_t BgL_arg1314z00_918;

							BgL_arg1314z00_918 = CDR(((obj_t) BgL_clausez00_75));
							{	/* Module/impuse.scm 116 */
								bool_t BgL_tmpz00_2682;

								BgL_protosz00_913 = BgL_arg1314z00_918;
								{
									obj_t BgL_l1170z00_920;

									BgL_l1170z00_920 = BgL_protosz00_913;
								BgL_zc3z04anonymousza31315ze3z87_921:
									if (PAIRP(BgL_l1170z00_920))
										{	/* Module/impuse.scm 118 */
											BGl_impusezd2parserzd2zzmodule_impusez00(CAR
												(BgL_l1170z00_920), BgL_modez00_912, BgL_clausez00_75);
											{
												obj_t BgL_l1170z00_2687;

												BgL_l1170z00_2687 = CDR(BgL_l1170z00_920);
												BgL_l1170z00_920 = BgL_l1170z00_2687;
												goto BgL_zc3z04anonymousza31315ze3z87_921;
											}
										}
									else
										{	/* Module/impuse.scm 118 */
											BgL_tmpz00_2682 = ((bool_t) 1);
										}
								}
								return BBOOL(BgL_tmpz00_2682);
							}
						}
					else
						{	/* Module/impuse.scm 116 */
							{	/* Module/impuse.scm 120 */
								obj_t BgL_arg1318z00_926;
								obj_t BgL_arg1319z00_927;

								BgL_arg1318z00_926 =
									BGl_findzd2locationzd2zztools_locationz00
									(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00);
								{	/* Module/impuse.scm 121 */
									obj_t BgL_list1321z00_929;

									BgL_list1321z00_929 = MAKE_YOUNG_PAIR(BgL_modez00_912, BNIL);
									BgL_arg1319z00_927 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string2129z00zzmodule_impusez00, BgL_list1321z00_929);
								}
								{	/* Module/impuse.scm 120 */
									obj_t BgL_list1320z00_928;

									BgL_list1320z00_928 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									return
										BGl_userzd2errorzf2locationz20zztools_errorz00
										(BgL_arg1318z00_926, BGl_string2130z00zzmodule_impusez00,
										BgL_arg1319z00_927, BgL_clausez00_75, BgL_list1320z00_928);
								}
							}
						}
				}
			}
		}

	}



/* &impuse-producer */
	obj_t BGl_z62impusezd2producerzb0zzmodule_impusez00(obj_t BgL_envz00_2355,
		obj_t BgL_clausez00_2356)
	{
		{	/* Module/impuse.scm 114 */
			return BGl_impusezd2producerzd2zzmodule_impusez00(BgL_clausez00_2356);
		}

	}



/* import-all-module */
	obj_t BGl_importzd2allzd2modulez00zzmodule_impusez00(obj_t BgL_modulez00_76,
		obj_t BgL_modez00_77, obj_t BgL_srcz00_78)
	{
		{	/* Module/impuse.scm 126 */
			{	/* Module/impuse.scm 127 */
				obj_t BgL_miz00_930;

				BgL_miz00_930 =
					BGl_hashtablezd2getzd2zz__hashz00
					(BGl_za2importsza2z00zzmodule_impusez00, BgL_modulez00_76);
				{	/* Module/impuse.scm 128 */
					bool_t BgL_test2256z00_2697;

					{	/* Module/impuse.scm 128 */
						obj_t BgL_classz00_1807;

						BgL_classz00_1807 = BGl_importz00zzmodule_impusez00;
						if (BGL_OBJECTP(BgL_miz00_930))
							{	/* Module/impuse.scm 128 */
								obj_t BgL_oclassz00_1809;

								{	/* Module/impuse.scm 128 */
									obj_t BgL_arg1815z00_1811;
									long BgL_arg1816z00_1812;

									BgL_arg1815z00_1811 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Module/impuse.scm 128 */
										long BgL_arg1817z00_1813;

										BgL_arg1817z00_1813 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt) BgL_miz00_930));
										BgL_arg1816z00_1812 = (BgL_arg1817z00_1813 - OBJECT_TYPE);
									}
									BgL_oclassz00_1809 =
										VECTOR_REF(BgL_arg1815z00_1811, BgL_arg1816z00_1812);
								}
								BgL_test2256z00_2697 =
									(BgL_oclassz00_1809 == BgL_classz00_1807);
							}
						else
							{	/* Module/impuse.scm 128 */
								BgL_test2256z00_2697 = ((bool_t) 0);
							}
					}
					if (BgL_test2256z00_2697)
						{	/* Module/impuse.scm 129 */
							obj_t BgL_vz00_1820;

							BgL_vz00_1820 = CNST_TABLE_REF(4);
							return
								((((BgL_importz00_bglt) COBJECT(
											((BgL_importz00_bglt) BgL_miz00_930)))->BgL_varsz00) =
								((obj_t) BgL_vz00_1820), BUNSPEC);
						}
					else
						{	/* Module/impuse.scm 130 */
							obj_t BgL_locz00_932;

							BgL_locz00_932 =
								BGl_findzd2locationzf2locz20zztools_locationz00(BgL_srcz00_78,
								BGl_findzd2locationzd2zztools_locationz00
								(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00));
							{	/* Module/impuse.scm 132 */
								BgL_importz00_bglt BgL_arg1323z00_933;

								{	/* Module/impuse.scm 132 */
									BgL_importz00_bglt BgL_new1100z00_934;

									{	/* Module/impuse.scm 133 */
										BgL_importz00_bglt BgL_new1098z00_935;

										BgL_new1098z00_935 =
											((BgL_importz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_importz00_bgl))));
										{	/* Module/impuse.scm 133 */
											long BgL_arg1325z00_936;

											BgL_arg1325z00_936 =
												BGL_CLASS_NUM(BGl_importz00zzmodule_impusez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1098z00_935),
												BgL_arg1325z00_936);
										}
										BgL_new1100z00_934 = BgL_new1098z00_935;
									}
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_modulez00) = ((obj_t) BgL_modulez00_76), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_numberz00) = ((long) -1L), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_modez00) = ((obj_t) BgL_modez00_77), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_varsz00) = ((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_aliasesz00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_checksumz00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_locz00) = ((obj_t) BgL_locz00_932), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_srcz00) = ((obj_t) BgL_srcz00_78), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_declz00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_providez00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_codez00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1100z00_934))->
											BgL_accessz00) = ((obj_t) BNIL), BUNSPEC);
									BgL_arg1323z00_933 = BgL_new1100z00_934;
								}
								return
									BGl_registerzd2importz12zc0zzmodule_impusez00
									(BgL_arg1323z00_933);
							}
						}
				}
			}
		}

	}



/* import-1-module */
	obj_t BGl_importzd21zd2modulez00zzmodule_impusez00(obj_t BgL_modulez00_79,
		obj_t BgL_varz00_80, obj_t BgL_aliasz00_81, obj_t BgL_modez00_82,
		obj_t BgL_srcz00_83)
	{
		{	/* Module/impuse.scm 147 */
			{	/* Module/impuse.scm 148 */
				obj_t BgL_miz00_938;

				BgL_miz00_938 =
					BGl_hashtablezd2getzd2zz__hashz00
					(BGl_za2importsza2z00zzmodule_impusez00, BgL_modulez00_79);
				{	/* Module/impuse.scm 149 */
					bool_t BgL_test2258z00_2730;

					{	/* Module/impuse.scm 149 */
						obj_t BgL_classz00_1824;

						BgL_classz00_1824 = BGl_importz00zzmodule_impusez00;
						if (BGL_OBJECTP(BgL_miz00_938))
							{	/* Module/impuse.scm 149 */
								obj_t BgL_oclassz00_1826;

								{	/* Module/impuse.scm 149 */
									obj_t BgL_arg1815z00_1828;
									long BgL_arg1816z00_1829;

									BgL_arg1815z00_1828 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Module/impuse.scm 149 */
										long BgL_arg1817z00_1830;

										BgL_arg1817z00_1830 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt) BgL_miz00_938));
										BgL_arg1816z00_1829 = (BgL_arg1817z00_1830 - OBJECT_TYPE);
									}
									BgL_oclassz00_1826 =
										VECTOR_REF(BgL_arg1815z00_1828, BgL_arg1816z00_1829);
								}
								BgL_test2258z00_2730 =
									(BgL_oclassz00_1826 == BgL_classz00_1824);
							}
						else
							{	/* Module/impuse.scm 149 */
								BgL_test2258z00_2730 = ((bool_t) 0);
							}
					}
					if (BgL_test2258z00_2730)
						{	/* Module/impuse.scm 149 */
							if (CBOOL(BgL_aliasz00_81))
								{	/* Module/impuse.scm 154 */
									obj_t BgL_arg1328z00_940;

									{	/* Module/impuse.scm 154 */
										obj_t BgL_arg1329z00_941;
										obj_t BgL_arg1331z00_942;

										BgL_arg1329z00_941 =
											MAKE_YOUNG_PAIR(BgL_varz00_80, BgL_aliasz00_81);
										BgL_arg1331z00_942 =
											(((BgL_importz00_bglt) COBJECT(
													((BgL_importz00_bglt) BgL_miz00_938)))->
											BgL_aliasesz00);
										BgL_arg1328z00_940 =
											MAKE_YOUNG_PAIR(BgL_arg1329z00_941, BgL_arg1331z00_942);
									}
									return
										((((BgL_importz00_bglt) COBJECT(
													((BgL_importz00_bglt) BgL_miz00_938)))->
											BgL_aliasesz00) = ((obj_t) BgL_arg1328z00_940), BUNSPEC);
								}
							else
								{	/* Module/impuse.scm 155 */
									obj_t BgL_casezd2valuezd2_943;

									BgL_casezd2valuezd2_943 =
										(((BgL_importz00_bglt) COBJECT(
												((BgL_importz00_bglt) BgL_miz00_938)))->BgL_varsz00);
									if ((BgL_casezd2valuezd2_943 == CNST_TABLE_REF(5)))
										{	/* Module/impuse.scm 155 */
											((((BgL_importz00_bglt) COBJECT(
															((BgL_importz00_bglt) BgL_miz00_938)))->
													BgL_modez00) =
												((obj_t) ((obj_t) BgL_modez00_82)), BUNSPEC);
											{	/* Module/impuse.scm 158 */
												obj_t BgL_arg1333z00_945;

												{	/* Module/impuse.scm 158 */
													obj_t BgL_arg1335z00_946;

													BgL_arg1335z00_946 =
														MAKE_YOUNG_PAIR(BgL_varz00_80, BFALSE);
													{	/* Module/impuse.scm 158 */
														obj_t BgL_list1336z00_947;

														BgL_list1336z00_947 =
															MAKE_YOUNG_PAIR(BgL_arg1335z00_946, BNIL);
														BgL_arg1333z00_945 = BgL_list1336z00_947;
													}
												}
												return
													((((BgL_importz00_bglt) COBJECT(
																((BgL_importz00_bglt) BgL_miz00_938)))->
														BgL_varsz00) =
													((obj_t) BgL_arg1333z00_945), BUNSPEC);
											}
										}
									else
										{	/* Module/impuse.scm 155 */
											if ((BgL_casezd2valuezd2_943 == CNST_TABLE_REF(4)))
												{	/* Module/impuse.scm 155 */
													return CNST_TABLE_REF(6);
												}
											else
												{	/* Module/impuse.scm 163 */
													obj_t BgL_arg1339z00_949;

													{	/* Module/impuse.scm 163 */
														obj_t BgL_arg1340z00_950;
														obj_t BgL_arg1342z00_951;

														BgL_arg1340z00_950 =
															MAKE_YOUNG_PAIR(BgL_varz00_80, BFALSE);
														BgL_arg1342z00_951 =
															(((BgL_importz00_bglt) COBJECT(
																	((BgL_importz00_bglt) BgL_miz00_938)))->
															BgL_varsz00);
														BgL_arg1339z00_949 =
															MAKE_YOUNG_PAIR(BgL_arg1340z00_950,
															BgL_arg1342z00_951);
													}
													return
														((((BgL_importz00_bglt) COBJECT(
																	((BgL_importz00_bglt) BgL_miz00_938)))->
															BgL_varsz00) =
														((obj_t) BgL_arg1339z00_949), BUNSPEC);
												}
										}
								}
						}
					else
						{	/* Module/impuse.scm 165 */
							obj_t BgL_locz00_952;

							BgL_locz00_952 =
								BGl_findzd2locationzf2locz20zztools_locationz00(BgL_srcz00_83,
								BGl_findzd2locationzd2zztools_locationz00
								(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00));
							{	/* Module/impuse.scm 167 */
								BgL_importz00_bglt BgL_arg1343z00_953;

								{	/* Module/impuse.scm 167 */
									BgL_importz00_bglt BgL_new1102z00_954;

									{	/* Module/impuse.scm 168 */
										BgL_importz00_bglt BgL_new1101z00_959;

										BgL_new1101z00_959 =
											((BgL_importz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_importz00_bgl))));
										{	/* Module/impuse.scm 168 */
											long BgL_arg1351z00_960;

											BgL_arg1351z00_960 =
												BGL_CLASS_NUM(BGl_importz00zzmodule_impusez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1101z00_959),
												BgL_arg1351z00_960);
										}
										BgL_new1102z00_954 = BgL_new1101z00_959;
									}
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_modulez00) = ((obj_t) BgL_modulez00_79), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_numberz00) = ((long) -1L), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_modez00) = ((obj_t) BgL_modez00_82), BUNSPEC);
									{
										obj_t BgL_auxz00_2778;

										if (CBOOL(BgL_aliasz00_81))
											{	/* Module/impuse.scm 171 */
												BgL_auxz00_2778 = BNIL;
											}
										else
											{	/* Module/impuse.scm 171 */
												obj_t BgL_arg1346z00_955;

												BgL_arg1346z00_955 =
													MAKE_YOUNG_PAIR(BgL_varz00_80, BFALSE);
												{	/* Module/impuse.scm 171 */
													obj_t BgL_list1347z00_956;

													BgL_list1347z00_956 =
														MAKE_YOUNG_PAIR(BgL_arg1346z00_955, BNIL);
													BgL_auxz00_2778 = BgL_list1347z00_956;
												}
											}
										((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
												BgL_varsz00) = ((obj_t) BgL_auxz00_2778), BUNSPEC);
									}
									{
										obj_t BgL_auxz00_2784;

										if (CBOOL(BgL_aliasz00_81))
											{	/* Module/impuse.scm 170 */
												obj_t BgL_arg1348z00_957;

												BgL_arg1348z00_957 =
													MAKE_YOUNG_PAIR(BgL_varz00_80, BgL_aliasz00_81);
												{	/* Module/impuse.scm 170 */
													obj_t BgL_list1349z00_958;

													BgL_list1349z00_958 =
														MAKE_YOUNG_PAIR(BgL_arg1348z00_957, BNIL);
													BgL_auxz00_2784 = BgL_list1349z00_958;
												}
											}
										else
											{	/* Module/impuse.scm 170 */
												BgL_auxz00_2784 = BNIL;
											}
										((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
												BgL_aliasesz00) = ((obj_t) BgL_auxz00_2784), BUNSPEC);
									}
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_checksumz00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_locz00) = ((obj_t) BgL_locz00_952), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_srcz00) = ((obj_t) BgL_srcz00_83), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_declz00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_providez00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_codez00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1102z00_954))->
											BgL_accessz00) = ((obj_t) BNIL), BUNSPEC);
									BgL_arg1343z00_953 = BgL_new1102z00_954;
								}
								return
									BGl_registerzd2importz12zc0zzmodule_impusez00
									(BgL_arg1343z00_953);
							}
						}
				}
			}
		}

	}



/* import-with-module! */
	BGL_EXPORTED_DEF obj_t
		BGl_importzd2withzd2modulez12z12zzmodule_impusez00(obj_t BgL_modulez00_84,
		obj_t BgL_srcz00_85)
	{
		{	/* Module/impuse.scm 178 */
			{	/* Module/impuse.scm 179 */
				obj_t BgL_miz00_962;

				BgL_miz00_962 =
					BGl_hashtablezd2getzd2zz__hashz00
					(BGl_za2importsza2z00zzmodule_impusez00, BgL_modulez00_84);
				{	/* Module/impuse.scm 180 */
					bool_t BgL_test2265z00_2799;

					{	/* Module/impuse.scm 180 */
						obj_t BgL_classz00_1852;

						BgL_classz00_1852 = BGl_importz00zzmodule_impusez00;
						if (BGL_OBJECTP(BgL_miz00_962))
							{	/* Module/impuse.scm 180 */
								obj_t BgL_oclassz00_1854;

								{	/* Module/impuse.scm 180 */
									obj_t BgL_arg1815z00_1856;
									long BgL_arg1816z00_1857;

									BgL_arg1815z00_1856 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Module/impuse.scm 180 */
										long BgL_arg1817z00_1858;

										BgL_arg1817z00_1858 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt) BgL_miz00_962));
										BgL_arg1816z00_1857 = (BgL_arg1817z00_1858 - OBJECT_TYPE);
									}
									BgL_oclassz00_1854 =
										VECTOR_REF(BgL_arg1815z00_1856, BgL_arg1816z00_1857);
								}
								BgL_test2265z00_2799 =
									(BgL_oclassz00_1854 == BgL_classz00_1852);
							}
						else
							{	/* Module/impuse.scm 180 */
								BgL_test2265z00_2799 = ((bool_t) 0);
							}
					}
					if (BgL_test2265z00_2799)
						{	/* Module/impuse.scm 180 */
							return BFALSE;
						}
					else
						{	/* Module/impuse.scm 181 */
							obj_t BgL_locz00_964;

							BgL_locz00_964 =
								BGl_findzd2locationzf2locz20zztools_locationz00(BgL_srcz00_85,
								BGl_findzd2locationzd2zztools_locationz00
								(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00));
							{	/* Module/impuse.scm 183 */
								BgL_importz00_bglt BgL_arg1361z00_965;

								{	/* Module/impuse.scm 183 */
									BgL_importz00_bglt BgL_new1104z00_966;

									{	/* Module/impuse.scm 184 */
										BgL_importz00_bglt BgL_new1103z00_967;

										BgL_new1103z00_967 =
											((BgL_importz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_importz00_bgl))));
										{	/* Module/impuse.scm 184 */
											long BgL_arg1364z00_968;

											BgL_arg1364z00_968 =
												BGL_CLASS_NUM(BGl_importz00zzmodule_impusez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1103z00_967),
												BgL_arg1364z00_968);
										}
										BgL_new1104z00_966 = BgL_new1103z00_967;
									}
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_modulez00) = ((obj_t) BgL_modulez00_84), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_numberz00) = ((long) -1L), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_modez00) = ((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_varsz00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_aliasesz00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_checksumz00) = ((obj_t) BINT(0L)), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_locz00) = ((obj_t) BgL_locz00_964), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_srcz00) = ((obj_t) BgL_srcz00_85), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_declz00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_providez00) = ((obj_t) BNIL), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_codez00) = ((obj_t) BUNSPEC), BUNSPEC);
									((((BgL_importz00_bglt) COBJECT(BgL_new1104z00_966))->
											BgL_accessz00) = ((obj_t) BNIL), BUNSPEC);
									BgL_arg1361z00_965 = BgL_new1104z00_966;
								}
								return
									BGl_registerzd2importz12zc0zzmodule_impusez00
									(BgL_arg1361z00_965);
							}
						}
				}
			}
		}

	}



/* &import-with-module! */
	obj_t BGl_z62importzd2withzd2modulez12z70zzmodule_impusez00(obj_t
		BgL_envz00_2370, obj_t BgL_modulez00_2371, obj_t BgL_srcz00_2372)
	{
		{	/* Module/impuse.scm 178 */
			return
				BGl_importzd2withzd2modulez12z12zzmodule_impusez00(BgL_modulez00_2371,
				BgL_srcz00_2372);
		}

	}



/* impuse-parser */
	obj_t BGl_impusezd2parserzd2zzmodule_impusez00(obj_t BgL_prototypez00_86,
		obj_t BgL_modez00_87, obj_t BgL_importzd2srczd2_88)
	{
		{	/* Module/impuse.scm 202 */
			{

				if (SYMBOLP(BgL_prototypez00_86))
					{	/* Module/impuse.scm 218 */
						return
							BGl_importzd2allzd2modulez00zzmodule_impusez00
							(BgL_prototypez00_86, BgL_modez00_87, BgL_importzd2srczd2_88);
					}
				else
					{	/* Module/impuse.scm 218 */
						if (BGl_listzf3zf3zz__r4_pairs_and_lists_6_3z00
							(BgL_prototypez00_86))
							{	/* Module/impuse.scm 222 */
								obj_t BgL_invz00_975;

								BgL_invz00_975 = bgl_reverse(BgL_prototypez00_86);
								{
									obj_t BgL_lstz00_978;
									obj_t BgL_filesz00_979;

									BgL_lstz00_978 = BgL_invz00_975;
									BgL_filesz00_979 = BNIL;
								BgL_zc3z04anonymousza31370ze3z87_980:
									if (PAIRP(BgL_lstz00_978))
										{	/* Module/impuse.scm 228 */
											bool_t BgL_test2270z00_2838;

											{	/* Module/impuse.scm 228 */
												obj_t BgL_tmpz00_2839;

												BgL_tmpz00_2839 = CAR(BgL_lstz00_978);
												BgL_test2270z00_2838 = STRINGP(BgL_tmpz00_2839);
											}
											if (BgL_test2270z00_2838)
												{	/* Module/impuse.scm 229 */
													obj_t BgL_arg1408z00_984;
													obj_t BgL_arg1410z00_985;

													BgL_arg1408z00_984 = CDR(BgL_lstz00_978);
													BgL_arg1410z00_985 =
														MAKE_YOUNG_PAIR(CAR(BgL_lstz00_978),
														BgL_filesz00_979);
													{
														obj_t BgL_filesz00_2846;
														obj_t BgL_lstz00_2845;

														BgL_lstz00_2845 = BgL_arg1408z00_984;
														BgL_filesz00_2846 = BgL_arg1410z00_985;
														BgL_filesz00_979 = BgL_filesz00_2846;
														BgL_lstz00_978 = BgL_lstz00_2845;
														goto BgL_zc3z04anonymousza31370ze3z87_980;
													}
												}
											else
												{	/* Module/impuse.scm 230 */
													bool_t BgL_test2271z00_2847;

													{	/* Module/impuse.scm 230 */
														obj_t BgL_tmpz00_2848;

														BgL_tmpz00_2848 = CAR(BgL_lstz00_978);
														BgL_test2271z00_2847 = SYMBOLP(BgL_tmpz00_2848);
													}
													if (BgL_test2271z00_2847)
														{	/* Module/impuse.scm 231 */
															obj_t BgL_modz00_989;
															obj_t BgL_varsz00_990;

															BgL_modz00_989 = CAR(BgL_lstz00_978);
															BgL_varsz00_990 = CDR(BgL_lstz00_978);
															if (NULLP(BgL_varsz00_990))
																{	/* Module/impuse.scm 234 */
																	if (NULLP(BgL_filesz00_979))
																		{	/* Module/impuse.scm 236 */
																			BFALSE;
																		}
																	else
																		{	/* Module/impuse.scm 236 */
																			BGl_modulezd2addzd2accessz12z12zz__modulez00
																				(BgL_modz00_989, BgL_filesz00_979,
																				BGl_string2131z00zzmodule_impusez00);
																		}
																	return
																		BGl_importzd2allzd2modulez00zzmodule_impusez00
																		(BgL_modz00_989, BgL_modez00_87,
																		BgL_prototypez00_86);
																}
															else
																{	/* Module/impuse.scm 238 */
																	bool_t BgL_test2274z00_2859;

																	{
																		obj_t BgL_l1172z00_1015;

																		BgL_l1172z00_1015 = BgL_varsz00_990;
																	BgL_zc3z04anonymousza31455ze3z87_1016:
																		if (NULLP(BgL_l1172z00_1015))
																			{	/* Module/impuse.scm 238 */
																				BgL_test2274z00_2859 = ((bool_t) 1);
																			}
																		else
																			{	/* Module/impuse.scm 238 */
																				bool_t BgL_test2276z00_2862;

																				{	/* Module/impuse.scm 238 */
																					obj_t BgL_arg1473z00_1021;

																					BgL_arg1473z00_1021 =
																						CAR(((obj_t) BgL_l1172z00_1015));
																					{	/* Module/impuse.scm 215 */
																						bool_t BgL__ortest_1105z00_1879;

																						BgL__ortest_1105z00_1879 =
																							SYMBOLP(BgL_arg1473z00_1021);
																						if (BgL__ortest_1105z00_1879)
																							{	/* Module/impuse.scm 215 */
																								BgL_test2276z00_2862 =
																									BgL__ortest_1105z00_1879;
																							}
																						else
																							{	/* Module/impuse.scm 215 */
																								BgL_test2276z00_2862 =
																									BGl_aliaszf3ze70z14zzmodule_impusez00
																									(BgL_arg1473z00_1021);
																							}
																					}
																				}
																				if (BgL_test2276z00_2862)
																					{
																						obj_t BgL_l1172z00_2868;

																						BgL_l1172z00_2868 =
																							CDR(((obj_t) BgL_l1172z00_1015));
																						BgL_l1172z00_1015 =
																							BgL_l1172z00_2868;
																						goto
																							BgL_zc3z04anonymousza31455ze3z87_1016;
																					}
																				else
																					{	/* Module/impuse.scm 238 */
																						BgL_test2274z00_2859 = ((bool_t) 0);
																					}
																			}
																	}
																	if (BgL_test2274z00_2859)
																		{	/* Module/impuse.scm 238 */
																			if (NULLP(BgL_filesz00_979))
																				{	/* Module/impuse.scm 240 */
																					BFALSE;
																				}
																			else
																				{	/* Module/impuse.scm 240 */
																					BGl_modulezd2addzd2accessz12z12zz__modulez00
																						(BgL_modz00_989, BgL_filesz00_979,
																						BGl_string2131z00zzmodule_impusez00);
																				}
																			{
																				obj_t BgL_l1175z00_1005;

																				{	/* Module/impuse.scm 242 */
																					bool_t BgL_tmpz00_2874;

																					BgL_l1175z00_1005 = BgL_varsz00_990;
																				BgL_zc3z04anonymousza31440ze3z87_1006:
																					if (PAIRP(BgL_l1175z00_1005))
																						{	/* Module/impuse.scm 242 */
																							{	/* Module/impuse.scm 243 */
																								obj_t BgL_vz00_1008;

																								BgL_vz00_1008 =
																									CAR(BgL_l1175z00_1005);
																								if (BGl_aliaszf3ze70z14zzmodule_impusez00(BgL_vz00_1008))
																									{	/* Module/impuse.scm 244 */
																										obj_t BgL_arg1448z00_1010;
																										obj_t BgL_arg1453z00_1011;

																										{	/* Module/impuse.scm 244 */
																											obj_t BgL_pairz00_1885;

																											BgL_pairz00_1885 =
																												CDR(
																												((obj_t)
																													BgL_vz00_1008));
																											BgL_arg1448z00_1010 =
																												CAR(BgL_pairz00_1885);
																										}
																										BgL_arg1453z00_1011 =
																											CAR(
																											((obj_t) BgL_vz00_1008));
																										BGl_importzd21zd2modulez00zzmodule_impusez00
																											(BgL_modz00_989,
																											BgL_arg1448z00_1010,
																											BgL_arg1453z00_1011,
																											BgL_modez00_87,
																											BgL_prototypez00_86);
																									}
																								else
																									{	/* Module/impuse.scm 243 */
																										BGl_importzd21zd2modulez00zzmodule_impusez00
																											(BgL_modz00_989,
																											BgL_vz00_1008, BFALSE,
																											BgL_modez00_87,
																											BgL_prototypez00_86);
																									}
																							}
																							{
																								obj_t BgL_l1175z00_2887;

																								BgL_l1175z00_2887 =
																									CDR(BgL_l1175z00_1005);
																								BgL_l1175z00_1005 =
																									BgL_l1175z00_2887;
																								goto
																									BgL_zc3z04anonymousza31440ze3z87_1006;
																							}
																						}
																					else
																						{	/* Module/impuse.scm 242 */
																							BgL_tmpz00_2874 = ((bool_t) 1);
																						}
																					return BBOOL(BgL_tmpz00_2874);
																				}
																			}
																		}
																	else
																		{	/* Module/impuse.scm 238 */
																		BgL_zc3z04anonymousza31490ze3z87_1026:
																			{	/* Module/impuse.scm 207 */
																				obj_t BgL_arg1502z00_1027;

																				{	/* Module/impuse.scm 207 */
																					obj_t BgL_list1504z00_1029;

																					BgL_list1504z00_1029 =
																						MAKE_YOUNG_PAIR(BgL_modez00_87,
																						BNIL);
																					BgL_arg1502z00_1027 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string2129z00zzmodule_impusez00,
																						BgL_list1504z00_1029);
																				}
																				{	/* Module/impuse.scm 206 */
																					obj_t BgL_list1503z00_1028;

																					BgL_list1503z00_1028 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					return
																						BGl_userzd2errorzd2zztools_errorz00
																						(BGl_string2130z00zzmodule_impusez00,
																						BgL_arg1502z00_1027,
																						BgL_prototypez00_86,
																						BgL_list1503z00_1028);
																				}
																			}
																		}
																}
														}
													else
														{	/* Module/impuse.scm 230 */
															goto BgL_zc3z04anonymousza31490ze3z87_1026;
														}
												}
										}
									else
										{	/* Module/impuse.scm 226 */
											goto BgL_zc3z04anonymousza31490ze3z87_1026;
										}
								}
							}
						else
							{	/* Module/impuse.scm 221 */
								goto BgL_zc3z04anonymousza31490ze3z87_1026;
							}
					}
			}
		}

	}



/* alias?~0 */
	bool_t BGl_aliaszf3ze70z14zzmodule_impusez00(obj_t BgL_vz00_1030)
	{
		{	/* Module/impuse.scm 212 */
			if (PAIRP(BgL_vz00_1030))
				{	/* Module/impuse.scm 212 */
					obj_t BgL_cdrzd2371zd2_1036;

					BgL_cdrzd2371zd2_1036 = CDR(((obj_t) BgL_vz00_1030));
					{	/* Module/impuse.scm 212 */
						bool_t BgL_test2282z00_2898;

						{	/* Module/impuse.scm 212 */
							obj_t BgL_tmpz00_2899;

							BgL_tmpz00_2899 = CAR(((obj_t) BgL_vz00_1030));
							BgL_test2282z00_2898 = SYMBOLP(BgL_tmpz00_2899);
						}
						if (BgL_test2282z00_2898)
							{	/* Module/impuse.scm 212 */
								if (PAIRP(BgL_cdrzd2371zd2_1036))
									{	/* Module/impuse.scm 212 */
										bool_t BgL_test2284z00_2905;

										{	/* Module/impuse.scm 212 */
											obj_t BgL_tmpz00_2906;

											BgL_tmpz00_2906 = CAR(BgL_cdrzd2371zd2_1036);
											BgL_test2284z00_2905 = SYMBOLP(BgL_tmpz00_2906);
										}
										if (BgL_test2284z00_2905)
											{	/* Module/impuse.scm 212 */
												return NULLP(CDR(BgL_cdrzd2371zd2_1036));
											}
										else
											{	/* Module/impuse.scm 212 */
												return ((bool_t) 0);
											}
									}
								else
									{	/* Module/impuse.scm 212 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Module/impuse.scm 212 */
								return ((bool_t) 0);
							}
					}
				}
			else
				{	/* Module/impuse.scm 212 */
					return ((bool_t) 0);
				}
		}

	}



/* get-imported-modules */
	BGL_EXPORTED_DEF obj_t BGl_getzd2importedzd2modulesz00zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 257 */
			{	/* Module/impuse.scm 258 */
				obj_t BgL_l1177z00_1053;

				BgL_l1177z00_1053 =
					BGl_hashtablezd2ze3listz31zz__hashz00
					(BGl_za2importsza2z00zzmodule_impusez00);
				if (NULLP(BgL_l1177z00_1053))
					{	/* Module/impuse.scm 258 */
						return BNIL;
					}
				else
					{	/* Module/impuse.scm 258 */
						obj_t BgL_head1179z00_1055;

						{	/* Module/impuse.scm 258 */
							obj_t BgL_arg1561z00_1067;

							BgL_arg1561z00_1067 =
								(((BgL_importz00_bglt) COBJECT(
										((BgL_importz00_bglt)
											CAR(BgL_l1177z00_1053))))->BgL_modulez00);
							BgL_head1179z00_1055 = MAKE_YOUNG_PAIR(BgL_arg1561z00_1067, BNIL);
						}
						{	/* Module/impuse.scm 258 */
							obj_t BgL_g1182z00_1056;

							BgL_g1182z00_1056 = CDR(BgL_l1177z00_1053);
							{
								obj_t BgL_l1177z00_1058;
								obj_t BgL_tail1180z00_1059;

								BgL_l1177z00_1058 = BgL_g1182z00_1056;
								BgL_tail1180z00_1059 = BgL_head1179z00_1055;
							BgL_zc3z04anonymousza31547ze3z87_1060:
								if (NULLP(BgL_l1177z00_1058))
									{	/* Module/impuse.scm 258 */
										return BgL_head1179z00_1055;
									}
								else
									{	/* Module/impuse.scm 258 */
										obj_t BgL_newtail1181z00_1062;

										{	/* Module/impuse.scm 258 */
											obj_t BgL_arg1553z00_1064;

											BgL_arg1553z00_1064 =
												(((BgL_importz00_bglt) COBJECT(
														((BgL_importz00_bglt)
															CAR(
																((obj_t) BgL_l1177z00_1058)))))->BgL_modulez00);
											BgL_newtail1181z00_1062 =
												MAKE_YOUNG_PAIR(BgL_arg1553z00_1064, BNIL);
										}
										SET_CDR(BgL_tail1180z00_1059, BgL_newtail1181z00_1062);
										{	/* Module/impuse.scm 258 */
											obj_t BgL_arg1552z00_1063;

											BgL_arg1552z00_1063 = CDR(((obj_t) BgL_l1177z00_1058));
											{
												obj_t BgL_tail1180z00_2930;
												obj_t BgL_l1177z00_2929;

												BgL_l1177z00_2929 = BgL_arg1552z00_1063;
												BgL_tail1180z00_2930 = BgL_newtail1181z00_1062;
												BgL_tail1180z00_1059 = BgL_tail1180z00_2930;
												BgL_l1177z00_1058 = BgL_l1177z00_2929;
												goto BgL_zc3z04anonymousza31547ze3z87_1060;
											}
										}
									}
							}
						}
					}
			}
		}

	}



/* &get-imported-modules */
	obj_t BGl_z62getzd2importedzd2modulesz62zzmodule_impusez00(obj_t
		BgL_envz00_2373)
	{
		{	/* Module/impuse.scm 257 */
			return BGl_getzd2importedzd2modulesz00zzmodule_impusez00();
		}

	}



/* import-finalizer */
	obj_t BGl_importzd2finaliza7erz75zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 269 */
			{
				long BgL_numz00_1070;

				BgL_numz00_1070 = BGl_za2importzd2numberza2zd2zzmodule_impusez00;
			BgL_zc3z04anonymousza31565ze3z87_1071:
				BGl_hashtablezd2forzd2eachz00zz__hashz00
					(BGl_za2importsza2z00zzmodule_impusez00,
					BGl_proc2132z00zzmodule_impusez00);
				if ((BGl_za2importzd2numberza2zd2zzmodule_impusez00 > BgL_numz00_1070))
					{
						long BgL_numz00_2935;

						BgL_numz00_2935 = BGl_za2importzd2numberza2zd2zzmodule_impusez00;
						BgL_numz00_1070 = BgL_numz00_2935;
						goto BgL_zc3z04anonymousza31565ze3z87_1071;
					}
				else
					{	/* Module/impuse.scm 273 */
						((bool_t) 0);
					}
			}
			{	/* Module/impuse.scm 276 */
				obj_t BgL_importsz00_1079;

				BgL_importsz00_1079 =
					BGl_sortz00zz__r4_vectors_6_8z00(BGl_proc2133z00zzmodule_impusez00,
					BGl_hashtablezd2ze3listz31zz__hashz00
					(BGl_za2importsza2z00zzmodule_impusez00));
				{
					obj_t BgL_l1183z00_1081;

					BgL_l1183z00_1081 = BgL_importsz00_1079;
				BgL_zc3z04anonymousza31574ze3z87_1082:
					if (PAIRP(BgL_l1183z00_1081))
						{	/* Module/impuse.scm 280 */
							{	/* Module/impuse.scm 281 */
								obj_t BgL_iz00_1084;

								BgL_iz00_1084 = CAR(BgL_l1183z00_1081);
								{	/* Module/impuse.scm 281 */
									bool_t BgL_test2289z00_2941;

									if (
										((((BgL_importz00_bglt) COBJECT(
														((BgL_importz00_bglt) BgL_iz00_1084)))->
												BgL_modez00) == CNST_TABLE_REF(5)))
										{	/* Module/impuse.scm 281 */
											BgL_test2289z00_2941 = ((bool_t) 1);
										}
									else
										{	/* Module/impuse.scm 281 */
											BgL_test2289z00_2941 =
												(
												(((BgL_importz00_bglt) COBJECT(
															((BgL_importz00_bglt) BgL_iz00_1084)))->
													BgL_declz00) == CNST_TABLE_REF(7));
										}
									if (BgL_test2289z00_2941)
										{	/* Module/impuse.scm 281 */
											BFALSE;
										}
									else
										{	/* Module/impuse.scm 281 */
											BGl_importzd2modulez12zc0zzmodule_impusez00
												(BgL_iz00_1084);
										}
								}
							}
							{
								obj_t BgL_l1183z00_2952;

								BgL_l1183z00_2952 = CDR(BgL_l1183z00_1081);
								BgL_l1183z00_1081 = BgL_l1183z00_2952;
								goto BgL_zc3z04anonymousza31574ze3z87_1082;
							}
						}
					else
						{	/* Module/impuse.scm 280 */
							((bool_t) 1);
						}
				}
				{	/* Module/impuse.scm 286 */
					obj_t BgL_initsz00_1095;

					{	/* Module/impuse.scm 286 */
						obj_t BgL_hook1189z00_1099;

						BgL_hook1189z00_1099 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{
							obj_t BgL_l1186z00_1101;
							obj_t BgL_h1187z00_1102;

							BgL_l1186z00_1101 = BgL_importsz00_1079;
							BgL_h1187z00_1102 = BgL_hook1189z00_1099;
						BgL_zc3z04anonymousza31597ze3z87_1103:
							if (NULLP(BgL_l1186z00_1101))
								{	/* Module/impuse.scm 286 */
									BgL_initsz00_1095 = CDR(BgL_hook1189z00_1099);
								}
							else
								{	/* Module/impuse.scm 286 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
												(((BgL_importz00_bglt) COBJECT(
															((BgL_importz00_bglt)
																CAR(
																	((obj_t) BgL_l1186z00_1101)))))->BgL_modez00),
												CNST_TABLE_REF(8))))
										{	/* Module/impuse.scm 286 */
											obj_t BgL_nh1188z00_1108;

											{	/* Module/impuse.scm 286 */
												obj_t BgL_arg1606z00_1110;

												BgL_arg1606z00_1110 = CAR(((obj_t) BgL_l1186z00_1101));
												BgL_nh1188z00_1108 =
													MAKE_YOUNG_PAIR(BgL_arg1606z00_1110, BNIL);
											}
											SET_CDR(BgL_h1187z00_1102, BgL_nh1188z00_1108);
											{	/* Module/impuse.scm 286 */
												obj_t BgL_arg1605z00_1109;

												BgL_arg1605z00_1109 = CDR(((obj_t) BgL_l1186z00_1101));
												{
													obj_t BgL_h1187z00_2973;
													obj_t BgL_l1186z00_2972;

													BgL_l1186z00_2972 = BgL_arg1605z00_1109;
													BgL_h1187z00_2973 = BgL_nh1188z00_1108;
													BgL_h1187z00_1102 = BgL_h1187z00_2973;
													BgL_l1186z00_1101 = BgL_l1186z00_2972;
													goto BgL_zc3z04anonymousza31597ze3z87_1103;
												}
											}
										}
									else
										{	/* Module/impuse.scm 286 */
											obj_t BgL_arg1609z00_1111;

											BgL_arg1609z00_1111 = CDR(((obj_t) BgL_l1186z00_1101));
											{
												obj_t BgL_l1186z00_2976;

												BgL_l1186z00_2976 = BgL_arg1609z00_1111;
												BgL_l1186z00_1101 = BgL_l1186z00_2976;
												goto BgL_zc3z04anonymousza31597ze3z87_1103;
											}
										}
								}
						}
					}
					if (PAIRP(BgL_initsz00_1095))
						{	/* Module/impuse.scm 290 */
							obj_t BgL_arg1595z00_1097;

							BgL_arg1595z00_1097 =
								BGl_importedzd2moduleszd2unitz00zzmodule_impusez00
								(BgL_initsz00_1095);
							{	/* Module/impuse.scm 290 */
								obj_t BgL_list1596z00_1098;

								BgL_list1596z00_1098 =
									MAKE_YOUNG_PAIR(BgL_arg1595z00_1097, BNIL);
								return BgL_list1596z00_1098;
							}
						}
					else
						{	/* Module/impuse.scm 289 */
							return BNIL;
						}
				}
			}
		}

	}



/* &<@anonymous:1616> */
	obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2376, obj_t BgL_i1z00_2377, obj_t BgL_i2z00_2378)
	{
		{	/* Module/impuse.scm 276 */
			return
				BBOOL(
				((((BgL_importz00_bglt) COBJECT(
								((BgL_importz00_bglt) BgL_i1z00_2377)))->BgL_numberz00) <
					(((BgL_importz00_bglt) COBJECT(
								((BgL_importz00_bglt) BgL_i2z00_2378)))->BgL_numberz00)));
		}

	}



/* &<@anonymous:1572> */
	obj_t BGl_z62zc3z04anonymousza31572ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2379, obj_t BgL_kz00_2380, obj_t BgL_iz00_2381)
	{
		{	/* Module/impuse.scm 272 */
			return BGl_readzd2importz12zc0zzmodule_impusez00(BgL_iz00_2381);
		}

	}



/* initialize-imported-modules */
	BGL_EXPORTED_DEF obj_t
		BGl_initializa7ezd2importedzd2modulesza7zzmodule_impusez00(obj_t
		BgL_getzd2initzd2_89)
	{
		{	/* Module/impuse.scm 301 */
			{	/* Module/impuse.scm 317 */
				obj_t BgL_callsz00_1125;

				{	/* Module/impuse.scm 317 */
					obj_t BgL_l1190z00_1158;

					BgL_l1190z00_1158 =
						BGl_za2importedzd2moduleszd2inzd2unitza2zd2zzmodule_impusez00;
					if (NULLP(BgL_l1190z00_1158))
						{	/* Module/impuse.scm 317 */
							BgL_callsz00_1125 = BNIL;
						}
					else
						{	/* Module/impuse.scm 317 */
							obj_t BgL_head1192z00_1160;

							BgL_head1192z00_1160 =
								MAKE_YOUNG_PAIR
								(BGl_initializa7ezd2moduleze70z92zzmodule_impusez00
								(BgL_getzd2initzd2_89, CAR(BgL_l1190z00_1158)), BNIL);
							{	/* Module/impuse.scm 317 */
								obj_t BgL_g1195z00_1161;

								BgL_g1195z00_1161 = CDR(BgL_l1190z00_1158);
								{
									obj_t BgL_l1190z00_1163;
									obj_t BgL_tail1193z00_1164;

									BgL_l1190z00_1163 = BgL_g1195z00_1161;
									BgL_tail1193z00_1164 = BgL_head1192z00_1160;
								BgL_zc3z04anonymousza31701ze3z87_1165:
									if (NULLP(BgL_l1190z00_1163))
										{	/* Module/impuse.scm 317 */
											BgL_callsz00_1125 = BgL_head1192z00_1160;
										}
									else
										{	/* Module/impuse.scm 317 */
											obj_t BgL_newtail1194z00_1167;

											{	/* Module/impuse.scm 317 */
												obj_t BgL_arg1705z00_1169;

												{	/* Module/impuse.scm 317 */
													obj_t BgL_arg1708z00_1170;

													BgL_arg1708z00_1170 =
														CAR(((obj_t) BgL_l1190z00_1163));
													BgL_arg1705z00_1169 =
														BGl_initializa7ezd2moduleze70z92zzmodule_impusez00
														(BgL_getzd2initzd2_89, BgL_arg1708z00_1170);
												}
												BgL_newtail1194z00_1167 =
													MAKE_YOUNG_PAIR(BgL_arg1705z00_1169, BNIL);
											}
											SET_CDR(BgL_tail1193z00_1164, BgL_newtail1194z00_1167);
											{	/* Module/impuse.scm 317 */
												obj_t BgL_arg1703z00_1168;

												BgL_arg1703z00_1168 = CDR(((obj_t) BgL_l1190z00_1163));
												{
													obj_t BgL_tail1193z00_3004;
													obj_t BgL_l1190z00_3003;

													BgL_l1190z00_3003 = BgL_arg1703z00_1168;
													BgL_tail1193z00_3004 = BgL_newtail1194z00_1167;
													BgL_tail1193z00_1164 = BgL_tail1193z00_3004;
													BgL_l1190z00_1163 = BgL_l1190z00_3003;
													goto BgL_zc3z04anonymousza31701ze3z87_1165;
												}
											}
										}
								}
							}
						}
				}
				{	/* Module/impuse.scm 317 */

					{	/* Module/impuse.scm 318 */
						bool_t BgL_test2296z00_3005;

						if (
							((long) CINT(BGl_za2debugzd2moduleza2zd2zzengine_paramz00) > 0L))
							{	/* Module/impuse.scm 319 */
								obj_t BgL_arg1692z00_1156;

								{	/* Module/impuse.scm 319 */
									obj_t BgL_arg1699z00_1157;

									BgL_arg1699z00_1157 =
										BGl_thezd2backendzd2zzbackend_backendz00();
									BgL_arg1692z00_1156 =
										(((BgL_backendz00_bglt) COBJECT(
												((BgL_backendz00_bglt) BgL_arg1699z00_1157)))->
										BgL_debugzd2supportzd2);
								}
								BgL_test2296z00_3005 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CNST_TABLE_REF
										(9), BgL_arg1692z00_1156));
							}
						else
							{	/* Module/impuse.scm 318 */
								BgL_test2296z00_3005 = ((bool_t) 0);
							}
						if (BgL_test2296z00_3005)
							{	/* Module/impuse.scm 321 */
								obj_t BgL_arg1642z00_1130;

								{	/* Module/impuse.scm 321 */
									obj_t BgL_arg1646z00_1131;

									{	/* Module/impuse.scm 321 */
										obj_t BgL_arg1650z00_1132;

										{	/* Module/impuse.scm 321 */
											obj_t BgL_ll1196z00_1133;

											BgL_ll1196z00_1133 =
												BGl_za2importedzd2moduleszd2inzd2unitza2zd2zzmodule_impusez00;
											if (NULLP(BgL_ll1196z00_1133))
												{	/* Module/impuse.scm 321 */
													BgL_arg1650z00_1132 = BNIL;
												}
											else
												{	/* Module/impuse.scm 321 */
													obj_t BgL_head1198z00_1136;

													{	/* Module/impuse.scm 321 */
														obj_t BgL_arg1688z00_1152;

														{	/* Module/impuse.scm 321 */
															obj_t BgL_arg1689z00_1153;
															obj_t BgL_arg1691z00_1154;

															BgL_arg1689z00_1153 = CAR(BgL_ll1196z00_1133);
															BgL_arg1691z00_1154 =
																CAR(((obj_t) BgL_callsz00_1125));
															BgL_arg1688z00_1152 =
																BGl_tracezd2initializa7ezd2moduleze70z40zzmodule_impusez00
																(BgL_getzd2initzd2_89, BgL_arg1689z00_1153,
																BgL_arg1691z00_1154);
														}
														BgL_head1198z00_1136 =
															MAKE_YOUNG_PAIR(BgL_arg1688z00_1152, BNIL);
													}
													{	/* Module/impuse.scm 321 */
														obj_t BgL_g1202z00_1137;
														obj_t BgL_g1208z00_1138;

														BgL_g1202z00_1137 = CDR(BgL_ll1196z00_1133);
														BgL_g1208z00_1138 =
															CDR(((obj_t) BgL_callsz00_1125));
														{
															obj_t BgL_ll1196z00_1140;
															obj_t BgL_ll1197z00_1141;
															obj_t BgL_tail1199z00_1142;

															BgL_ll1196z00_1140 = BgL_g1202z00_1137;
															BgL_ll1197z00_1141 = BgL_g1208z00_1138;
															BgL_tail1199z00_1142 = BgL_head1198z00_1136;
														BgL_zc3z04anonymousza31653ze3z87_1143:
															if (NULLP(BgL_ll1196z00_1140))
																{	/* Module/impuse.scm 321 */
																	BgL_arg1650z00_1132 = BgL_head1198z00_1136;
																}
															else
																{	/* Module/impuse.scm 321 */
																	obj_t BgL_newtail1200z00_1145;

																	{	/* Module/impuse.scm 321 */
																		obj_t BgL_arg1675z00_1148;

																		{	/* Module/impuse.scm 321 */
																			obj_t BgL_arg1678z00_1149;
																			obj_t BgL_arg1681z00_1150;

																			BgL_arg1678z00_1149 =
																				CAR(((obj_t) BgL_ll1196z00_1140));
																			BgL_arg1681z00_1150 =
																				CAR(((obj_t) BgL_ll1197z00_1141));
																			BgL_arg1675z00_1148 =
																				BGl_tracezd2initializa7ezd2moduleze70z40zzmodule_impusez00
																				(BgL_getzd2initzd2_89,
																				BgL_arg1678z00_1149,
																				BgL_arg1681z00_1150);
																		}
																		BgL_newtail1200z00_1145 =
																			MAKE_YOUNG_PAIR(BgL_arg1675z00_1148,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1199z00_1142,
																		BgL_newtail1200z00_1145);
																	{	/* Module/impuse.scm 321 */
																		obj_t BgL_arg1661z00_1146;
																		obj_t BgL_arg1663z00_1147;

																		BgL_arg1661z00_1146 =
																			CDR(((obj_t) BgL_ll1196z00_1140));
																		BgL_arg1663z00_1147 =
																			CDR(((obj_t) BgL_ll1197z00_1141));
																		{
																			obj_t BgL_tail1199z00_3040;
																			obj_t BgL_ll1197z00_3039;
																			obj_t BgL_ll1196z00_3038;

																			BgL_ll1196z00_3038 = BgL_arg1661z00_1146;
																			BgL_ll1197z00_3039 = BgL_arg1663z00_1147;
																			BgL_tail1199z00_3040 =
																				BgL_newtail1200z00_1145;
																			BgL_tail1199z00_1142 =
																				BgL_tail1199z00_3040;
																			BgL_ll1197z00_1141 = BgL_ll1197z00_3039;
																			BgL_ll1196z00_1140 = BgL_ll1196z00_3038;
																			goto
																				BgL_zc3z04anonymousza31653ze3z87_1143;
																		}
																	}
																}
														}
													}
												}
										}
										BgL_arg1646z00_1131 =
											BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
											(BgL_arg1650z00_1132, BNIL);
									}
									BgL_arg1642z00_1130 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg1646z00_1131);
								}
								return MAKE_YOUNG_PAIR(BgL_arg1642z00_1130, BNIL);
							}
						else
							{	/* Module/impuse.scm 318 */
								return BgL_callsz00_1125;
							}
					}
				}
			}
		}

	}



/* initialize-module~0 */
	obj_t BGl_initializa7ezd2moduleze70z92zzmodule_impusez00(obj_t
		BgL_getzd2initzd2_2515, obj_t BgL_importz00_1174)
	{
		{	/* Module/impuse.scm 309 */
			{	/* Module/impuse.scm 304 */
				obj_t BgL_funz00_1177;

				{	/* Module/impuse.scm 304 */
					obj_t BgL_arg1740z00_1192;

					BgL_arg1740z00_1192 =
						(((BgL_importz00_bglt) COBJECT(
								((BgL_importz00_bglt) BgL_importz00_1174)))->BgL_modulez00);
					BgL_funz00_1177 =
						BGL_PROCEDURE_CALL1(BgL_getzd2initzd2_2515, BgL_arg1740z00_1192);
				}
				{	/* Module/impuse.scm 304 */
					obj_t BgL_varz00_1178;

					{	/* Module/impuse.scm 305 */
						obj_t BgL_arg1735z00_1187;
						obj_t BgL_arg1736z00_1188;

						BgL_arg1735z00_1187 =
							(((BgL_importz00_bglt) COBJECT(
									((BgL_importz00_bglt) BgL_importz00_1174)))->BgL_modulez00);
						{	/* Module/impuse.scm 305 */
							obj_t BgL_arg1738z00_1190;

							{	/* Module/impuse.scm 305 */
								obj_t BgL_arg1739z00_1191;

								BgL_arg1739z00_1191 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(11), BNIL);
								BgL_arg1738z00_1190 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(12), BgL_arg1739z00_1191);
							}
							BgL_arg1736z00_1188 =
								MAKE_YOUNG_PAIR(BgL_funz00_1177, BgL_arg1738z00_1190);
						}
						BgL_varz00_1178 =
							BGl_importzd2parserzd2zzmodule_impusez00(BgL_arg1735z00_1187,
							BgL_arg1736z00_1188, BFALSE, BNIL);
					}
					{	/* Module/impuse.scm 305 */

						((((BgL_globalz00_bglt) COBJECT(
										((BgL_globalz00_bglt) BgL_varz00_1178)))->
								BgL_evaluablezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
						{	/* Module/impuse.scm 309 */
							obj_t BgL_arg1714z00_1179;
							obj_t BgL_arg1717z00_1180;

							{	/* Module/impuse.scm 309 */
								obj_t BgL_arg1718z00_1181;

								{	/* Module/impuse.scm 309 */
									obj_t BgL_arg1720z00_1182;

									{	/* Module/impuse.scm 309 */
										obj_t BgL_arg1722z00_1183;

										BgL_arg1722z00_1183 =
											(((BgL_importz00_bglt) COBJECT(
													((BgL_importz00_bglt) BgL_importz00_1174)))->
											BgL_modulez00);
										BgL_arg1720z00_1182 =
											MAKE_YOUNG_PAIR(BgL_arg1722z00_1183, BNIL);
									}
									BgL_arg1718z00_1181 =
										MAKE_YOUNG_PAIR(BgL_funz00_1177, BgL_arg1720z00_1182);
								}
								BgL_arg1714z00_1179 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(13), BgL_arg1718z00_1181);
							}
							{	/* Module/impuse.scm 309 */
								obj_t BgL_arg1724z00_1184;
								obj_t BgL_arg1733z00_1185;

								BgL_arg1724z00_1184 =
									(((BgL_importz00_bglt) COBJECT(
											((BgL_importz00_bglt) BgL_importz00_1174)))->
									BgL_checksumz00);
								{	/* Module/impuse.scm 309 */
									obj_t BgL_arg1734z00_1186;

									{	/* Module/impuse.scm 309 */
										obj_t BgL_symbolz00_1917;

										BgL_symbolz00_1917 = BGl_za2moduleza2z00zzmodule_modulez00;
										{	/* Module/impuse.scm 309 */
											obj_t BgL_arg1455z00_1918;

											BgL_arg1455z00_1918 =
												SYMBOL_TO_STRING(BgL_symbolz00_1917);
											BgL_arg1734z00_1186 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_1918);
										}
									}
									BgL_arg1733z00_1185 =
										MAKE_YOUNG_PAIR(BgL_arg1734z00_1186, BNIL);
								}
								BgL_arg1717z00_1180 =
									MAKE_YOUNG_PAIR(BgL_arg1724z00_1184, BgL_arg1733z00_1185);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1714z00_1179, BgL_arg1717z00_1180);
						}
					}
				}
			}
		}

	}



/* trace-initialize-module~0 */
	obj_t BGl_tracezd2initializa7ezd2moduleze70z40zzmodule_impusez00(obj_t
		BgL_getzd2initzd2_2516, obj_t BgL_importz00_1193, obj_t BgL_callz00_1194)
	{
		{	/* Module/impuse.scm 316 */
			{	/* Module/impuse.scm 314 */
				obj_t BgL_arg1746z00_1196;

				{	/* Module/impuse.scm 314 */
					obj_t BgL_arg1747z00_1197;
					obj_t BgL_arg1748z00_1198;

					{	/* Module/impuse.scm 314 */
						obj_t BgL_arg1749z00_1199;

						{	/* Module/impuse.scm 314 */
							obj_t BgL_arg1750z00_1200;

							{	/* Module/impuse.scm 314 */
								obj_t BgL_arg1751z00_1201;
								obj_t BgL_arg1752z00_1202;

								{	/* Module/impuse.scm 314 */
									obj_t BgL_arg1755z00_1205;

									BgL_arg1755z00_1205 =
										BGL_PROCEDURE_CALL1(BgL_getzd2initzd2_2516,
										BGl_za2moduleza2z00zzmodule_modulez00);
									{	/* Module/impuse.scm 314 */
										obj_t BgL_arg1455z00_1920;

										BgL_arg1455z00_1920 =
											SYMBOL_TO_STRING(((obj_t) BgL_arg1755z00_1205));
										BgL_arg1751z00_1201 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1920);
									}
								}
								{	/* Module/impuse.scm 315 */
									obj_t BgL_arg1761z00_1206;

									BgL_arg1761z00_1206 =
										(((BgL_importz00_bglt) COBJECT(
												((BgL_importz00_bglt) BgL_importz00_1193)))->
										BgL_modulez00);
									{	/* Module/impuse.scm 315 */
										obj_t BgL_arg1455z00_1923;

										BgL_arg1455z00_1923 = SYMBOL_TO_STRING(BgL_arg1761z00_1206);
										BgL_arg1752z00_1202 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1923);
									}
								}
								{	/* Module/impuse.scm 313 */
									obj_t BgL_list1753z00_1203;

									{	/* Module/impuse.scm 313 */
										obj_t BgL_arg1754z00_1204;

										BgL_arg1754z00_1204 =
											MAKE_YOUNG_PAIR(BgL_arg1752z00_1202, BNIL);
										BgL_list1753z00_1203 =
											MAKE_YOUNG_PAIR(BgL_arg1751z00_1201, BgL_arg1754z00_1204);
									}
									BgL_arg1750z00_1200 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string2134z00zzmodule_impusez00, BgL_list1753z00_1203);
								}
							}
							BgL_arg1749z00_1199 = MAKE_YOUNG_PAIR(BgL_arg1750z00_1200, BNIL);
						}
						BgL_arg1747z00_1197 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(14), BgL_arg1749z00_1199);
					}
					BgL_arg1748z00_1198 = MAKE_YOUNG_PAIR(BgL_callz00_1194, BNIL);
					BgL_arg1746z00_1196 =
						MAKE_YOUNG_PAIR(BgL_arg1747z00_1197, BgL_arg1748z00_1198);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(10), BgL_arg1746z00_1196);
			}
		}

	}



/* &initialize-imported-modules */
	obj_t BGl_z62initializa7ezd2importedzd2moduleszc5zzmodule_impusez00(obj_t
		BgL_envz00_2382, obj_t BgL_getzd2initzd2_2383)
	{
		{	/* Module/impuse.scm 301 */
			return
				BGl_initializa7ezd2importedzd2modulesza7zzmodule_impusez00
				(BgL_getzd2initzd2_2383);
		}

	}



/* imported-modules-unit */
	obj_t BGl_importedzd2moduleszd2unitz00zzmodule_impusez00(obj_t
		BgL_importsz00_90)
	{
		{	/* Module/impuse.scm 328 */
			BGl_za2importedzd2moduleszd2inzd2unitza2zd2zzmodule_impusez00 =
				BgL_importsz00_90;
			{	/* Module/impuse.scm 330 */
				obj_t BgL_bodyz00_1209;
				long BgL_priorityz00_1210;

				BgL_bodyz00_1209 =
					BGl_initializa7ezd2importedzd2modulesza7zzmodule_impusez00
					(BGl_modulezd2initializa7ationzd2idzd2envz75zzmodule_modulez00);
				if ((BGl_za2objectzd2initzd2modeza2z00zzengine_paramz00 ==
						CNST_TABLE_REF(15)))
					{	/* Module/impuse.scm 331 */
						BgL_priorityz00_1210 = 22L;
					}
				else
					{	/* Module/impuse.scm 331 */
						BgL_priorityz00_1210 = 12L;
					}
				{	/* Module/impuse.scm 332 */
					obj_t BgL_idz00_1940;

					BgL_idz00_1940 = CNST_TABLE_REF(16);
					{	/* Module/impuse.scm 332 */
						obj_t BgL_newz00_1941;

						BgL_newz00_1941 = create_struct(CNST_TABLE_REF(17), (int) (5L));
						{	/* Module/impuse.scm 332 */
							int BgL_tmpz00_3104;

							BgL_tmpz00_3104 = (int) (4L);
							STRUCT_SET(BgL_newz00_1941, BgL_tmpz00_3104, BFALSE);
						}
						{	/* Module/impuse.scm 332 */
							int BgL_tmpz00_3107;

							BgL_tmpz00_3107 = (int) (3L);
							STRUCT_SET(BgL_newz00_1941, BgL_tmpz00_3107, BTRUE);
						}
						{	/* Module/impuse.scm 332 */
							int BgL_tmpz00_3110;

							BgL_tmpz00_3110 = (int) (2L);
							STRUCT_SET(BgL_newz00_1941, BgL_tmpz00_3110, BgL_bodyz00_1209);
						}
						{	/* Module/impuse.scm 332 */
							obj_t BgL_auxz00_3115;
							int BgL_tmpz00_3113;

							BgL_auxz00_3115 = BINT(BgL_priorityz00_1210);
							BgL_tmpz00_3113 = (int) (1L);
							STRUCT_SET(BgL_newz00_1941, BgL_tmpz00_3113, BgL_auxz00_3115);
						}
						{	/* Module/impuse.scm 332 */
							int BgL_tmpz00_3118;

							BgL_tmpz00_3118 = (int) (0L);
							STRUCT_SET(BgL_newz00_1941, BgL_tmpz00_3118, BgL_idz00_1940);
						}
						return BgL_newz00_1941;
					}
				}
			}
		}

	}



/* &impuse-finalizer */
	obj_t BGl_z62impusezd2finaliza7erz17zzmodule_impusez00(obj_t BgL_envz00_2362)
	{
		{	/* Module/impuse.scm 341 */
			{	/* Module/impuse.scm 342 */
				obj_t BgL_importsz00_2532;

				BgL_importsz00_2532 = BGl_importzd2finaliza7erz75zzmodule_impusez00();
				{	/* Module/impuse.scm 342 */
					obj_t BgL_inlinesz00_2533;

					BgL_inlinesz00_2533 = BGl_inlinezd2finaliza7erz75zzread_inlinez00();
					{	/* Module/impuse.scm 343 */
						obj_t BgL_finaliza7ersza7_2534;

						BgL_finaliza7ersza7_2534 =
							BGl_appendzd221011zd2zzmodule_impusez00(BgL_importsz00_2532,
							BgL_inlinesz00_2533);
						{	/* Module/impuse.scm 344 */

							if (NULLP(BgL_finaliza7ersza7_2534))
								{	/* Module/impuse.scm 345 */
									return CNST_TABLE_REF(1);
								}
							else
								{	/* Module/impuse.scm 345 */
									return BgL_finaliza7ersza7_2534;
								}
						}
					}
				}
			}
		}

	}



/* import-parser */
	BGL_EXPORTED_DEF obj_t BGl_importzd2parserzd2zzmodule_impusez00(obj_t
		BgL_modulez00_91, obj_t BgL_prototypez00_92, obj_t BgL_aliasz00_93,
		obj_t BgL_importzd2srczd2_94)
	{
		{	/* Module/impuse.scm 352 */
			{	/* Module/impuse.scm 353 */
				obj_t BgL_protoz00_1215;
				obj_t BgL_srcz00_1216;

				BgL_protoz00_1215 =
					BGl_parsezd2prototypezd2zzmodule_prototypez00(BgL_prototypez00_92);
				if (PAIRP(BgL_importzd2srczd2_94))
					{	/* Module/impuse.scm 354 */
						BgL_srcz00_1216 = CAR(BgL_importzd2srczd2_94);
					}
				else
					{	/* Module/impuse.scm 354 */
						BgL_srcz00_1216 = BFALSE;
					}
				if (PAIRP(BgL_protoz00_1215))
					{	/* Module/impuse.scm 358 */
						obj_t BgL_casezd2valuezd2_1218;

						BgL_casezd2valuezd2_1218 = CAR(BgL_protoz00_1215);
						{	/* Module/impuse.scm 358 */
							bool_t BgL_test2304z00_3134;

							{	/* Module/impuse.scm 358 */
								bool_t BgL__ortest_1108z00_1242;

								BgL__ortest_1108z00_1242 =
									(BgL_casezd2valuezd2_1218 == CNST_TABLE_REF(18));
								if (BgL__ortest_1108z00_1242)
									{	/* Module/impuse.scm 358 */
										BgL_test2304z00_3134 = BgL__ortest_1108z00_1242;
									}
								else
									{	/* Module/impuse.scm 358 */
										bool_t BgL__ortest_1109z00_1243;

										BgL__ortest_1109z00_1243 =
											(BgL_casezd2valuezd2_1218 == CNST_TABLE_REF(19));
										if (BgL__ortest_1109z00_1243)
											{	/* Module/impuse.scm 358 */
												BgL_test2304z00_3134 = BgL__ortest_1109z00_1243;
											}
										else
											{	/* Module/impuse.scm 358 */
												BgL_test2304z00_3134 =
													(BgL_casezd2valuezd2_1218 == CNST_TABLE_REF(20));
											}
									}
							}
							if (BgL_test2304z00_3134)
								{	/* Module/impuse.scm 360 */
									obj_t BgL_arg1767z00_1222;
									obj_t BgL_arg1770z00_1223;

									BgL_arg1767z00_1222 = CAR(CDR(BgL_protoz00_1215));
									BgL_arg1770z00_1223 = CAR(CDR(CDR(BgL_protoz00_1215)));
									return
										((obj_t)
										BGl_declarezd2globalzd2sfunz12z12zzast_glozd2declzd2
										(BgL_arg1767z00_1222, BgL_aliasz00_93, BgL_arg1770z00_1223,
											BgL_modulez00_91, CNST_TABLE_REF(0),
											BgL_casezd2valuezd2_1218, BgL_prototypez00_92,
											BgL_srcz00_1216));
								}
							else
								{	/* Module/impuse.scm 358 */
									if ((BgL_casezd2valuezd2_1218 == CNST_TABLE_REF(21)))
										{	/* Module/impuse.scm 363 */
											obj_t BgL_arg1773z00_1226;

											BgL_arg1773z00_1226 = CAR(CDR(BgL_protoz00_1215));
											return
												((obj_t)
												BGl_declarezd2globalzd2svarz12z12zzast_glozd2declzd2
												(BgL_arg1773z00_1226, BgL_aliasz00_93, BgL_modulez00_91,
													CNST_TABLE_REF(0), BgL_prototypez00_92,
													BgL_srcz00_1216));
										}
									else
										{	/* Module/impuse.scm 358 */
											if ((BgL_casezd2valuezd2_1218 == CNST_TABLE_REF(22)))
												{	/* Module/impuse.scm 366 */
													obj_t BgL_arg1775z00_1228;

													BgL_arg1775z00_1228 = CDR(BgL_protoz00_1215);
													return
														BGl_declarezd2classz12zc0zzmodule_classz00
														(BgL_arg1775z00_1228, BgL_modulez00_91,
														CNST_TABLE_REF(0), ((bool_t) 0), ((bool_t) 0),
														BgL_prototypez00_92, BgL_srcz00_1216);
												}
											else
												{	/* Module/impuse.scm 358 */
													if ((BgL_casezd2valuezd2_1218 == CNST_TABLE_REF(23)))
														{	/* Module/impuse.scm 368 */
															obj_t BgL_arg1798z00_1230;

															BgL_arg1798z00_1230 = CDR(BgL_protoz00_1215);
															return
																BGl_declarezd2classz12zc0zzmodule_classz00
																(BgL_arg1798z00_1230, BgL_modulez00_91,
																CNST_TABLE_REF(0), ((bool_t) 0), ((bool_t) 1),
																BgL_prototypez00_92, BgL_srcz00_1216);
														}
													else
														{	/* Module/impuse.scm 358 */
															if (
																(BgL_casezd2valuezd2_1218 ==
																	CNST_TABLE_REF(24)))
																{	/* Module/impuse.scm 370 */
																	obj_t BgL_arg1805z00_1232;

																	BgL_arg1805z00_1232 = CDR(BgL_protoz00_1215);
																	return
																		BGl_declarezd2classz12zc0zzmodule_classz00
																		(BgL_arg1805z00_1232, BgL_modulez00_91,
																		CNST_TABLE_REF(0), ((bool_t) 1),
																		((bool_t) 0), BgL_prototypez00_92,
																		BgL_srcz00_1216);
																}
															else
																{	/* Module/impuse.scm 358 */
																	if (
																		(BgL_casezd2valuezd2_1218 ==
																			CNST_TABLE_REF(25)))
																		{	/* Module/impuse.scm 372 */
																			obj_t BgL_arg1808z00_1234;

																			BgL_arg1808z00_1234 =
																				CDR(BgL_protoz00_1215);
																			return
																				BGl_declarezd2widezd2classz12z12zzmodule_classz00
																				(BgL_arg1808z00_1234, BgL_modulez00_91,
																				CNST_TABLE_REF(0), BgL_prototypez00_92,
																				BgL_srcz00_1216);
																		}
																	else
																		{	/* Module/impuse.scm 358 */
																			if (
																				(BgL_casezd2valuezd2_1218 ==
																					CNST_TABLE_REF(26)))
																				{	/* Module/impuse.scm 374 */
																					obj_t BgL_envz00_1237;

																					BgL_envz00_1237 =
																						BGl_defaultzd2environmentzd2zz__evalz00
																						();
																					{	/* Module/impuse.scm 374 */

																						return
																							BGl_evalz00zz__evalz00
																							(BgL_protoz00_1215,
																							BgL_envz00_1237);
																					}
																				}
																			else
																				{	/* Module/impuse.scm 358 */
																					if (
																						(BgL_casezd2valuezd2_1218 ==
																							CNST_TABLE_REF(27)))
																						{	/* Module/impuse.scm 358 */
																							return BgL_protoz00_1215;
																						}
																					else
																						{	/* Module/impuse.scm 358 */
																							if (
																								(BgL_casezd2valuezd2_1218 ==
																									CNST_TABLE_REF(28)))
																								{	/* Module/impuse.scm 358 */
																									return BgL_protoz00_1215;
																								}
																							else
																								{	/* Module/impuse.scm 358 */
																									if (
																										(BgL_casezd2valuezd2_1218 ==
																											CNST_TABLE_REF(29)))
																										{	/* Module/impuse.scm 358 */
																											return BgL_protoz00_1215;
																										}
																									else
																										{	/* Module/impuse.scm 382 */
																											obj_t
																												BgL_list1813z00_1241;
																											BgL_list1813z00_1241 =
																												MAKE_YOUNG_PAIR(BNIL,
																												BNIL);
																											return
																												BGl_userzd2errorzd2zztools_errorz00
																												(BGl_string2130z00zzmodule_impusez00,
																												BGl_string2135z00zzmodule_impusez00,
																												BgL_prototypez00_92,
																												BgL_list1813z00_1241);
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
					}
				else
					{	/* Module/impuse.scm 356 */
						obj_t BgL_arg1820z00_1244;

						BgL_arg1820z00_1244 =
							BGl_findzd2locationzd2zztools_locationz00
							(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00);
						{	/* Module/impuse.scm 356 */
							obj_t BgL_list1821z00_1245;

							BgL_list1821z00_1245 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							return
								BGl_userzd2errorzf2locationz20zztools_errorz00
								(BgL_arg1820z00_1244, BGl_string2130z00zzmodule_impusez00,
								BGl_string2135z00zzmodule_impusez00, BgL_prototypez00_92,
								BgL_list1821z00_1245);
						}
					}
			}
		}

	}



/* &import-parser */
	obj_t BGl_z62importzd2parserzb0zzmodule_impusez00(obj_t BgL_envz00_2386,
		obj_t BgL_modulez00_2387, obj_t BgL_prototypez00_2388,
		obj_t BgL_aliasz00_2389, obj_t BgL_importzd2srczd2_2390)
	{
		{	/* Module/impuse.scm 352 */
			return
				BGl_importzd2parserzd2zzmodule_impusez00(BgL_modulez00_2387,
				BgL_prototypez00_2388, BgL_aliasz00_2389, BgL_importzd2srczd2_2390);
		}

	}



/* import-module! */
	obj_t BGl_importzd2modulez12zc0zzmodule_impusez00(obj_t BgL_importz00_95)
	{
		{	/* Module/impuse.scm 387 */
			{	/* Module/impuse.scm 390 */
				obj_t BgL_inlinez00_1248;

				{	/* Module/impuse.scm 391 */
					bool_t BgL_test2316z00_3203;

					{	/* Module/impuse.scm 391 */
						obj_t BgL_tmpz00_3204;

						BgL_tmpz00_3204 =
							(((BgL_importz00_bglt) COBJECT(
									((BgL_importz00_bglt) BgL_importz00_95)))->BgL_varsz00);
						BgL_test2316z00_3203 = PAIRP(BgL_tmpz00_3204);
					}
					if (BgL_test2316z00_3203)
						{	/* Module/impuse.scm 392 */
							obj_t BgL_arg1837z00_1259;

							BgL_arg1837z00_1259 =
								(((BgL_importz00_bglt) COBJECT(
										((BgL_importz00_bglt) BgL_importz00_95)))->BgL_varsz00);
							BgL_inlinez00_1248 =
								BGl_importzd2wantedzd2zzmodule_impusez00(BgL_importz00_95,
								BgL_arg1837z00_1259);
						}
					else
						{	/* Module/impuse.scm 391 */
							BgL_inlinez00_1248 =
								BGl_importzd2everythingzd2zzmodule_impusez00(BgL_importz00_95);
						}
				}
				{	/* Module/impuse.scm 391 */
					obj_t BgL_macroz00_1249;
					obj_t BgL_syntaxz00_1250;
					obj_t BgL_expanderz00_1251;

					{	/* Module/impuse.scm 394 */
						obj_t BgL_tmpz00_1977;

						{	/* Module/impuse.scm 394 */
							int BgL_tmpz00_3212;

							BgL_tmpz00_3212 = (int) (1L);
							BgL_tmpz00_1977 = BGL_MVALUES_VAL(BgL_tmpz00_3212);
						}
						{	/* Module/impuse.scm 394 */
							int BgL_tmpz00_3215;

							BgL_tmpz00_3215 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3215, BUNSPEC);
						}
						BgL_macroz00_1249 = BgL_tmpz00_1977;
					}
					{	/* Module/impuse.scm 394 */
						obj_t BgL_tmpz00_1978;

						{	/* Module/impuse.scm 394 */
							int BgL_tmpz00_3218;

							BgL_tmpz00_3218 = (int) (2L);
							BgL_tmpz00_1978 = BGL_MVALUES_VAL(BgL_tmpz00_3218);
						}
						{	/* Module/impuse.scm 394 */
							int BgL_tmpz00_3221;

							BgL_tmpz00_3221 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3221, BUNSPEC);
						}
						BgL_syntaxz00_1250 = BgL_tmpz00_1978;
					}
					{	/* Module/impuse.scm 394 */
						obj_t BgL_tmpz00_1979;

						{	/* Module/impuse.scm 394 */
							int BgL_tmpz00_3224;

							BgL_tmpz00_3224 = (int) (3L);
							BgL_tmpz00_1979 = BGL_MVALUES_VAL(BgL_tmpz00_3224);
						}
						{	/* Module/impuse.scm 394 */
							int BgL_tmpz00_3227;

							BgL_tmpz00_3227 = (int) (3L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_3227, BUNSPEC);
						}
						BgL_expanderz00_1251 = BgL_tmpz00_1979;
					}
					{	/* Module/impuse.scm 394 */
						obj_t BgL_arg1823z00_1252;
						obj_t BgL_arg1831z00_1253;
						obj_t BgL_arg1832z00_1254;
						obj_t BgL_arg1833z00_1255;

						BgL_arg1823z00_1252 =
							(((BgL_importz00_bglt) COBJECT(
									((BgL_importz00_bglt) BgL_importz00_95)))->BgL_codez00);
						{	/* Module/impuse.scm 396 */
							obj_t BgL_arg1834z00_1256;

							BgL_arg1834z00_1256 =
								(((BgL_importz00_bglt) COBJECT(
										((BgL_importz00_bglt) BgL_importz00_95)))->BgL_declz00);
							BgL_arg1831z00_1253 =
								BGl_prognzd2tailzd2expressionsz00zztools_prognz00
								(BgL_arg1834z00_1256);
						}
						BgL_arg1832z00_1254 =
							(((BgL_importz00_bglt) COBJECT(
									((BgL_importz00_bglt) BgL_importz00_95)))->BgL_accessz00);
						BgL_arg1833z00_1255 =
							(((BgL_importz00_bglt) COBJECT(
									((BgL_importz00_bglt) BgL_importz00_95)))->BgL_modulez00);
						BGl_lookzd2forzd2inlineszd2andzd2macrosz00zzread_inlinez00
							(BgL_inlinez00_1248, BgL_macroz00_1249, BgL_syntaxz00_1250,
							BgL_expanderz00_1251, BgL_arg1823z00_1252, BgL_arg1831z00_1253,
							BgL_arg1832z00_1254, BgL_arg1833z00_1255);
			}}}
			{	/* Module/impuse.scm 398 */
				bool_t BgL_test2317z00_3240;

				{	/* Module/impuse.scm 398 */
					obj_t BgL_tmpz00_3241;

					BgL_tmpz00_3241 =
						(((BgL_importz00_bglt) COBJECT(
								((BgL_importz00_bglt) BgL_importz00_95)))->BgL_aliasesz00);
					BgL_test2317z00_3240 = PAIRP(BgL_tmpz00_3241);
				}
				if (BgL_test2317z00_3240)
					{	/* Module/impuse.scm 399 */
						obj_t BgL_inlinez00_1263;

						{	/* Module/impuse.scm 400 */
							obj_t BgL_arg1847z00_1272;

							BgL_arg1847z00_1272 =
								(((BgL_importz00_bglt) COBJECT(
										((BgL_importz00_bglt) BgL_importz00_95)))->BgL_aliasesz00);
							BgL_inlinez00_1263 =
								BGl_importzd2wantedzd2zzmodule_impusez00(BgL_importz00_95,
								BgL_arg1847z00_1272);
						}
						{	/* Module/impuse.scm 400 */
							obj_t BgL_macroz00_1264;
							obj_t BgL_syntaxz00_1265;
							obj_t BgL_expanderz00_1266;

							{	/* Module/impuse.scm 401 */
								obj_t BgL_tmpz00_1980;

								{	/* Module/impuse.scm 401 */
									int BgL_tmpz00_3248;

									BgL_tmpz00_3248 = (int) (1L);
									BgL_tmpz00_1980 = BGL_MVALUES_VAL(BgL_tmpz00_3248);
								}
								{	/* Module/impuse.scm 401 */
									int BgL_tmpz00_3251;

									BgL_tmpz00_3251 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3251, BUNSPEC);
								}
								BgL_macroz00_1264 = BgL_tmpz00_1980;
							}
							{	/* Module/impuse.scm 401 */
								obj_t BgL_tmpz00_1981;

								{	/* Module/impuse.scm 401 */
									int BgL_tmpz00_3254;

									BgL_tmpz00_3254 = (int) (2L);
									BgL_tmpz00_1981 = BGL_MVALUES_VAL(BgL_tmpz00_3254);
								}
								{	/* Module/impuse.scm 401 */
									int BgL_tmpz00_3257;

									BgL_tmpz00_3257 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3257, BUNSPEC);
								}
								BgL_syntaxz00_1265 = BgL_tmpz00_1981;
							}
							{	/* Module/impuse.scm 401 */
								obj_t BgL_tmpz00_1982;

								{	/* Module/impuse.scm 401 */
									int BgL_tmpz00_3260;

									BgL_tmpz00_3260 = (int) (3L);
									BgL_tmpz00_1982 = BGL_MVALUES_VAL(BgL_tmpz00_3260);
								}
								{	/* Module/impuse.scm 401 */
									int BgL_tmpz00_3263;

									BgL_tmpz00_3263 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_3263, BUNSPEC);
								}
								BgL_expanderz00_1266 = BgL_tmpz00_1982;
							}
							{	/* Module/impuse.scm 401 */
								obj_t BgL_arg1842z00_1267;
								obj_t BgL_arg1843z00_1268;
								obj_t BgL_arg1844z00_1269;
								obj_t BgL_arg1845z00_1270;

								BgL_arg1842z00_1267 =
									(((BgL_importz00_bglt) COBJECT(
											((BgL_importz00_bglt) BgL_importz00_95)))->BgL_codez00);
								{	/* Module/impuse.scm 403 */
									obj_t BgL_arg1846z00_1271;

									BgL_arg1846z00_1271 =
										(((BgL_importz00_bglt) COBJECT(
												((BgL_importz00_bglt) BgL_importz00_95)))->BgL_declz00);
									BgL_arg1843z00_1268 =
										BGl_prognzd2tailzd2expressionsz00zztools_prognz00
										(BgL_arg1846z00_1271);
								}
								BgL_arg1844z00_1269 =
									(((BgL_importz00_bglt) COBJECT(
											((BgL_importz00_bglt) BgL_importz00_95)))->BgL_accessz00);
								BgL_arg1845z00_1270 =
									(((BgL_importz00_bglt) COBJECT(
											((BgL_importz00_bglt) BgL_importz00_95)))->BgL_modulez00);
								return
									BGl_lookzd2forzd2inlineszd2andzd2macrosz00zzread_inlinez00
									(BgL_inlinez00_1263, BgL_macroz00_1264, BgL_syntaxz00_1265,
									BgL_expanderz00_1266, BgL_arg1842z00_1267,
									BgL_arg1843z00_1268, BgL_arg1844z00_1269,
									BgL_arg1845z00_1270);
							}
						}
					}
				else
					{	/* Module/impuse.scm 398 */
						return BFALSE;
					}
			}
		}

	}



/* read-import! */
	obj_t BGl_readzd2importz12zc0zzmodule_impusez00(obj_t BgL_importz00_96)
	{
		{	/* Module/impuse.scm 408 */
			{
				obj_t BgL_modulez00_1339;
				obj_t BgL_filez00_1340;
				obj_t BgL_objz00_1335;

				if (
					((((BgL_importz00_bglt) COBJECT(
									((BgL_importz00_bglt) BgL_importz00_96)))->BgL_declz00) ==
						BUNSPEC))
					{	/* Module/impuse.scm 424 */
						obj_t BgL_modulez00_1278;

						BgL_modulez00_1278 =
							(((BgL_importz00_bglt) COBJECT(
									((BgL_importz00_bglt) BgL_importz00_96)))->BgL_modulez00);
						{	/* Module/impuse.scm 424 */
							obj_t BgL_abasez00_1279;

							{	/* Module/impuse.scm 425 */
								obj_t BgL_l1209z00_1318;

								BgL_l1209z00_1318 =
									BGl_za2accesszd2filesza2zd2zzengine_paramz00;
								if (NULLP(BgL_l1209z00_1318))
									{	/* Module/impuse.scm 425 */
										BgL_abasez00_1279 = BNIL;
									}
								else
									{	/* Module/impuse.scm 425 */
										obj_t BgL_head1211z00_1320;

										{	/* Module/impuse.scm 425 */
											obj_t BgL_arg1882z00_1332;

											{	/* Module/impuse.scm 425 */
												obj_t BgL_arg1883z00_1333;

												BgL_arg1883z00_1333 = CAR(((obj_t) BgL_l1209z00_1318));
												BgL_arg1882z00_1332 =
													BGl_dirnamez00zz__osz00(BgL_arg1883z00_1333);
											}
											BgL_head1211z00_1320 =
												MAKE_YOUNG_PAIR(BgL_arg1882z00_1332, BNIL);
										}
										{	/* Module/impuse.scm 425 */
											obj_t BgL_g1214z00_1321;

											BgL_g1214z00_1321 = CDR(((obj_t) BgL_l1209z00_1318));
											{
												obj_t BgL_l1209z00_1323;
												obj_t BgL_tail1212z00_1324;

												BgL_l1209z00_1323 = BgL_g1214z00_1321;
												BgL_tail1212z00_1324 = BgL_head1211z00_1320;
											BgL_zc3z04anonymousza31876ze3z87_1325:
												if (NULLP(BgL_l1209z00_1323))
													{	/* Module/impuse.scm 425 */
														BgL_abasez00_1279 = BgL_head1211z00_1320;
													}
												else
													{	/* Module/impuse.scm 425 */
														obj_t BgL_newtail1213z00_1327;

														{	/* Module/impuse.scm 425 */
															obj_t BgL_arg1879z00_1329;

															{	/* Module/impuse.scm 425 */
																obj_t BgL_arg1880z00_1330;

																BgL_arg1880z00_1330 =
																	CAR(((obj_t) BgL_l1209z00_1323));
																BgL_arg1879z00_1329 =
																	BGl_dirnamez00zz__osz00(BgL_arg1880z00_1330);
															}
															BgL_newtail1213z00_1327 =
																MAKE_YOUNG_PAIR(BgL_arg1879z00_1329, BNIL);
														}
														SET_CDR(BgL_tail1212z00_1324,
															BgL_newtail1213z00_1327);
														{	/* Module/impuse.scm 425 */
															obj_t BgL_arg1878z00_1328;

															BgL_arg1878z00_1328 =
																CDR(((obj_t) BgL_l1209z00_1323));
															{
																obj_t BgL_tail1212z00_3300;
																obj_t BgL_l1209z00_3299;

																BgL_l1209z00_3299 = BgL_arg1878z00_1328;
																BgL_tail1212z00_3300 = BgL_newtail1213z00_1327;
																BgL_tail1212z00_1324 = BgL_tail1212z00_3300;
																BgL_l1209z00_1323 = BgL_l1209z00_3299;
																goto BgL_zc3z04anonymousza31876ze3z87_1325;
															}
														}
													}
											}
										}
									}
							}
							{	/* Module/impuse.scm 425 */
								obj_t BgL_fnamesz00_1280;

								{	/* Module/impuse.scm 426 */
									obj_t BgL_fun1874z00_1317;

									BgL_fun1874z00_1317 =
										BGl_bigloozd2modulezd2resolverz00zz__modulez00();
									BgL_fnamesz00_1280 =
										BGL_PROCEDURE_CALL3(BgL_fun1874z00_1317, BgL_modulez00_1278,
										BNIL, BgL_abasez00_1279);
								}
								{	/* Module/impuse.scm 426 */

									{	/* Module/impuse.scm 428 */
										obj_t BgL_arg1851z00_1281;

										if (
											((((BgL_importz00_bglt) COBJECT(
															((BgL_importz00_bglt) BgL_importz00_96)))->
													BgL_modez00) == CNST_TABLE_REF(2)))
											{	/* Module/impuse.scm 428 */
												BgL_arg1851z00_1281 =
													BGl_string2138z00zzmodule_impusez00;
											}
										else
											{	/* Module/impuse.scm 428 */
												BgL_arg1851z00_1281 =
													BGl_string2139z00zzmodule_impusez00;
											}
										{	/* Module/impuse.scm 427 */
											obj_t BgL_list1852z00_1282;

											{	/* Module/impuse.scm 427 */
												obj_t BgL_arg1853z00_1283;

												{	/* Module/impuse.scm 427 */
													obj_t BgL_arg1854z00_1284;

													{	/* Module/impuse.scm 427 */
														obj_t BgL_arg1856z00_1285;

														{	/* Module/impuse.scm 427 */
															obj_t BgL_arg1857z00_1286;

															{	/* Module/impuse.scm 427 */
																obj_t BgL_arg1858z00_1287;

																BgL_arg1858z00_1287 =
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
																	BNIL);
																BgL_arg1857z00_1286 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2140z00zzmodule_impusez00,
																	BgL_arg1858z00_1287);
															}
															BgL_arg1856z00_1285 =
																MAKE_YOUNG_PAIR(BgL_modulez00_1278,
																BgL_arg1857z00_1286);
														}
														BgL_arg1854z00_1284 =
															MAKE_YOUNG_PAIR
															(BGl_string2141z00zzmodule_impusez00,
															BgL_arg1856z00_1285);
													}
													BgL_arg1853z00_1283 =
														MAKE_YOUNG_PAIR(BgL_arg1851z00_1281,
														BgL_arg1854z00_1284);
												}
												BgL_list1852z00_1282 =
													MAKE_YOUNG_PAIR(BGl_string2142z00zzmodule_impusez00,
													BgL_arg1853z00_1283);
											}
											BGl_verbosez00zztools_speekz00(BINT(2L),
												BgL_list1852z00_1282);
									}}
									if (PAIRP(BgL_fnamesz00_1280))
										{	/* Module/impuse.scm 432 */
											obj_t BgL_fnamez00_1292;

											{	/* Module/impuse.scm 432 */
												obj_t BgL_arg1873z00_1316;

												BgL_arg1873z00_1316 = CAR(BgL_fnamesz00_1280);
												BgL_fnamez00_1292 =
													BGl_findzd2filezf2pathz20zz__osz00
													(BgL_arg1873z00_1316,
													BGl_za2loadzd2pathza2zd2zz__evalz00);
											}
											if (STRINGP(BgL_fnamez00_1292))
												{	/* Module/impuse.scm 435 */
													obj_t BgL_portz00_1294;

													{	/* Module/impuse.scm 435 */

														BgL_portz00_1294 =
															BGl_openzd2inputzd2filez00zz__r4_ports_6_10_1z00
															(BgL_fnamez00_1292, BTRUE, BINT(5000000L));
													}
													((((BgL_importz00_bglt) COBJECT(
																	((BgL_importz00_bglt) BgL_importz00_96)))->
															BgL_accessz00) =
														((obj_t) BgL_fnamesz00_1280), BUNSPEC);
													BGl_readerzd2resetz12zc0zz__readerz00();
													if (INPUT_PORTP(BgL_portz00_1294))
														{	/* Module/impuse.scm 440 */
															obj_t BgL_exitd1111z00_1296;

															BgL_exitd1111z00_1296 = BGL_EXITD_TOP_AS_OBJ();
															{	/* Module/impuse.scm 456 */
																obj_t BgL_zc3z04anonymousza31869ze3z87_2391;

																BgL_zc3z04anonymousza31869ze3z87_2391 =
																	MAKE_FX_PROCEDURE
																	(BGl_z62zc3z04anonymousza31869ze3ze5zzmodule_impusez00,
																	(int) (0L), (int) (1L));
																PROCEDURE_SET
																	(BgL_zc3z04anonymousza31869ze3z87_2391,
																	(int) (0L), BgL_portz00_1294);
																{	/* Module/impuse.scm 440 */
																	obj_t BgL_arg1828z00_2000;

																	{	/* Module/impuse.scm 440 */
																		obj_t BgL_arg1829z00_2001;

																		BgL_arg1829z00_2001 =
																			BGL_EXITD_PROTECT(BgL_exitd1111z00_1296);
																		BgL_arg1828z00_2000 =
																			MAKE_YOUNG_PAIR
																			(BgL_zc3z04anonymousza31869ze3z87_2391,
																			BgL_arg1829z00_2001);
																	}
																	BGL_EXITD_PROTECT_SET(BgL_exitd1111z00_1296,
																		BgL_arg1828z00_2000);
																	BUNSPEC;
																}
																{	/* Module/impuse.scm 441 */
																	obj_t BgL_tmp1113z00_1298;

																	{	/* Module/impuse.scm 441 */
																		obj_t BgL_mdeclz00_1299;

																		{	/* Module/impuse.scm 441 */
																			obj_t BgL_list1866z00_1307;

																			{	/* Module/impuse.scm 441 */
																				obj_t BgL_arg1868z00_1308;

																				BgL_arg1868z00_1308 =
																					MAKE_YOUNG_PAIR(BTRUE, BNIL);
																				BgL_list1866z00_1307 =
																					MAKE_YOUNG_PAIR(BgL_portz00_1294,
																					BgL_arg1868z00_1308);
																			}
																			BgL_mdeclz00_1299 =
																				BGl_compilerzd2readzd2zzread_readerz00
																				(BgL_list1866z00_1307);
																		}
																		{	/* Module/impuse.scm 441 */
																			obj_t BgL_emdeclz00_1300;

																			BgL_emdeclz00_1300 =
																				BGl_comptimezd2expandzf2errorz20zzexpand_epsz00
																				(BgL_mdeclz00_1299);
																			{	/* Module/impuse.scm 442 */
																				obj_t BgL_modz00_1301;

																				BgL_modz00_1301 =
																					BGl_prognzd2firstzd2expressionz00zztools_prognz00
																					(BgL_emdeclz00_1300);
																				{	/* Module/impuse.scm 443 */

																					BGl_resetzd2includezd2consumedzd2directivez12zc0zzmodule_includez00
																						();
																					BGl_resetzd2includezd2consumedzd2codez12zc0zzmodule_includez00
																						();
																					{	/* Module/impuse.scm 446 */
																						obj_t BgL_cmz00_1302;

																						BgL_cmz00_1302 =
																							BGl_consumezd2modulez12zc0zzmodule_modulez00
																							(BgL_modulez00_1278,
																							BgL_modz00_1301);
																						{	/* Module/impuse.scm 446 */
																							obj_t BgL_cimz00_1303;

																							BgL_cimz00_1303 =
																								BGl_getzd2includezd2consumedzd2directivezd2zzmodule_includez00
																								();
																							{	/* Module/impuse.scm 447 */
																								obj_t BgL_proz00_1304;

																								BgL_proz00_1304 =
																									BGl_appendzd221011zd2zzmodule_impusez00
																									(BgL_cmz00_1302,
																									BgL_cimz00_1303);
																								{	/* Module/impuse.scm 448 */
																									obj_t BgL_codez00_1305;

																									BgL_codez00_1305 =
																										BGl_getzd2includezd2consumedzd2codezd2zzmodule_includez00
																										();
																									{	/* Module/impuse.scm 449 */
																										long BgL_checkz00_1306;

																										BgL_checkz00_1306 =
																											BGl_modulezd2checksumzd2zzmodule_checksumz00
																											(BgL_modz00_1301,
																											BGl_za2mcozd2includezd2pathza2z00zzengine_paramz00);
																										{	/* Module/impuse.scm 450 */

																											((((BgL_importz00_bglt)
																														COBJECT((
																																(BgL_importz00_bglt)
																																BgL_importz00_96)))->
																													BgL_declz00) =
																												((obj_t)
																													BgL_emdeclz00_1300),
																												BUNSPEC);
																											((((BgL_importz00_bglt)
																														COBJECT((
																																(BgL_importz00_bglt)
																																BgL_importz00_96)))->
																													BgL_checksumz00) =
																												((obj_t)
																													BINT
																													(BgL_checkz00_1306)),
																												BUNSPEC);
																											((((BgL_importz00_bglt)
																														COBJECT((
																																(BgL_importz00_bglt)
																																BgL_importz00_96)))->
																													BgL_codez00) =
																												((obj_t)
																													BgL_codez00_1305),
																												BUNSPEC);
																											BgL_tmp1113z00_1298 =
																												((((BgL_importz00_bglt)
																														COBJECT((
																																(BgL_importz00_bglt)
																																BgL_importz00_96)))->
																													BgL_providez00) =
																												((obj_t) ((obj_t)
																														BgL_proz00_1304)),
																												BUNSPEC);
																	}}}}}}}}}}
																	{	/* Module/impuse.scm 440 */
																		bool_t BgL_test2325z00_3366;

																		{	/* Module/impuse.scm 440 */
																			obj_t BgL_arg1827z00_2008;

																			BgL_arg1827z00_2008 =
																				BGL_EXITD_PROTECT
																				(BgL_exitd1111z00_1296);
																			BgL_test2325z00_3366 =
																				PAIRP(BgL_arg1827z00_2008);
																		}
																		if (BgL_test2325z00_3366)
																			{	/* Module/impuse.scm 440 */
																				obj_t BgL_arg1825z00_2009;

																				{	/* Module/impuse.scm 440 */
																					obj_t BgL_arg1826z00_2010;

																					BgL_arg1826z00_2010 =
																						BGL_EXITD_PROTECT
																						(BgL_exitd1111z00_1296);
																					BgL_arg1825z00_2009 =
																						CDR(((obj_t) BgL_arg1826z00_2010));
																				}
																				BGL_EXITD_PROTECT_SET
																					(BgL_exitd1111z00_1296,
																					BgL_arg1825z00_2009);
																				BUNSPEC;
																			}
																		else
																			{	/* Module/impuse.scm 440 */
																				BFALSE;
																			}
																	}
																	bgl_close_input_port(BgL_portz00_1294);
																	return BgL_tmp1113z00_1298;
																}
															}
														}
													else
														{	/* Module/impuse.scm 438 */
															BgL_modulez00_1339 = BgL_modulez00_1278;
															BgL_filez00_1340 = CAR(BgL_fnamesz00_1280);
														BgL_zc3z04anonymousza31889ze3z87_1341:
															{	/* Module/impuse.scm 416 */
																obj_t BgL_vz00_1987;

																BgL_vz00_1987 = CNST_TABLE_REF(7);
																((((BgL_importz00_bglt) COBJECT(
																				((BgL_importz00_bglt)
																					BgL_importz00_96)))->BgL_declz00) =
																	((obj_t) BgL_vz00_1987), BUNSPEC);
															}
															{	/* Module/impuse.scm 418 */
																obj_t BgL_arg1890z00_1342;
																obj_t BgL_arg1891z00_1343;

																BgL_arg1890z00_1342 =
																	(((BgL_importz00_bglt) COBJECT(
																			((BgL_importz00_bglt)
																				BgL_importz00_96)))->BgL_locz00);
																{	/* Module/impuse.scm 420 */
																	obj_t BgL_list1893z00_1345;

																	BgL_list1893z00_1345 =
																		MAKE_YOUNG_PAIR(BgL_modulez00_1339, BNIL);
																	BgL_arg1891z00_1343 =
																		BGl_formatz00zz__r4_output_6_10_3z00
																		(BGl_string2137z00zzmodule_impusez00,
																		BgL_list1893z00_1345);
																}
																return
																	BGl_userzd2errorzf2locationz20zztools_errorz00
																	(BgL_arg1890z00_1342, CNST_TABLE_REF(0),
																	BgL_arg1891z00_1343, BgL_filez00_1340, BNIL);
															}
														}
												}
											else
												{
													obj_t BgL_filez00_3385;
													obj_t BgL_modulez00_3384;

													BgL_modulez00_3384 = BgL_modulez00_1278;
													BgL_filez00_3385 = CAR(BgL_fnamesz00_1280);
													BgL_filez00_1340 = BgL_filez00_3385;
													BgL_modulez00_1339 = BgL_modulez00_3384;
													goto BgL_zc3z04anonymousza31889ze3z87_1341;
												}
										}
									else
										{	/* Module/impuse.scm 430 */
											BgL_objz00_1335 = BgL_modulez00_1278;
											{	/* Module/impuse.scm 411 */
												obj_t BgL_vz00_1984;

												BgL_vz00_1984 = CNST_TABLE_REF(7);
												((((BgL_importz00_bglt) COBJECT(
																((BgL_importz00_bglt) BgL_importz00_96)))->
														BgL_declz00) = ((obj_t) BgL_vz00_1984), BUNSPEC);
											}
											{	/* Module/impuse.scm 412 */
												obj_t BgL_arg1887z00_1337;

												BgL_arg1887z00_1337 =
													(((BgL_importz00_bglt) COBJECT(
															((BgL_importz00_bglt) BgL_importz00_96)))->
													BgL_locz00);
												{	/* Module/impuse.scm 412 */
													obj_t BgL_list1888z00_1338;

													BgL_list1888z00_1338 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													return
														BGl_userzd2errorzf2locationz20zztools_errorz00
														(BgL_arg1887z00_1337, CNST_TABLE_REF(0),
														BGl_string2136z00zzmodule_impusez00,
														BgL_objz00_1335, BgL_list1888z00_1338);
												}
											}
										}
								}
							}
						}
					}
				else
					{	/* Module/impuse.scm 423 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1869> */
	obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2392)
	{
		{	/* Module/impuse.scm 440 */
			{	/* Module/impuse.scm 456 */
				obj_t BgL_portz00_2393;

				BgL_portz00_2393 = PROCEDURE_REF(BgL_envz00_2392, (int) (0L));
				return bgl_close_input_port(((obj_t) BgL_portz00_2393));
			}
		}

	}



/* import-everything */
	obj_t BGl_importzd2everythingzd2zzmodule_impusez00(obj_t BgL_importz00_97)
	{
		{	/* Module/impuse.scm 461 */
			{
				obj_t BgL_providedz00_1355;
				obj_t BgL_inlinez00_1356;
				obj_t BgL_macroz00_1357;
				obj_t BgL_syntaxz00_1358;
				obj_t BgL_expdz00_1359;

				{	/* Module/impuse.scm 463 */
					obj_t BgL_arg1894z00_1354;

					BgL_arg1894z00_1354 =
						(((BgL_importz00_bglt) COBJECT(
								((BgL_importz00_bglt) BgL_importz00_97)))->BgL_providez00);
					BgL_providedz00_1355 = BgL_arg1894z00_1354;
					BgL_inlinez00_1356 = BNIL;
					BgL_macroz00_1357 = BNIL;
					BgL_syntaxz00_1358 = BNIL;
					BgL_expdz00_1359 = BNIL;
				BgL_zc3z04anonymousza31895ze3z87_1360:
					if (NULLP(BgL_providedz00_1355))
						{	/* Module/impuse.scm 468 */
							{	/* Module/impuse.scm 469 */
								int BgL_tmpz00_3403;

								BgL_tmpz00_3403 = (int) (4L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3403);
							}
							{	/* Module/impuse.scm 469 */
								int BgL_tmpz00_3406;

								BgL_tmpz00_3406 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3406, BgL_macroz00_1357);
							}
							{	/* Module/impuse.scm 469 */
								int BgL_tmpz00_3409;

								BgL_tmpz00_3409 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3409, BgL_syntaxz00_1358);
							}
							{	/* Module/impuse.scm 469 */
								int BgL_tmpz00_3412;

								BgL_tmpz00_3412 = (int) (3L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3412, BgL_expdz00_1359);
							}
							return BgL_inlinez00_1356;
						}
					else
						{	/* Module/impuse.scm 470 */
							obj_t BgL_pz00_1366;

							{	/* Module/impuse.scm 470 */
								obj_t BgL_arg1933z00_1410;
								obj_t BgL_arg1934z00_1411;
								obj_t BgL_arg1935z00_1412;

								BgL_arg1933z00_1410 =
									(((BgL_importz00_bglt) COBJECT(
											((BgL_importz00_bglt) BgL_importz00_97)))->BgL_modulez00);
								BgL_arg1934z00_1411 = CAR(((obj_t) BgL_providedz00_1355));
								BgL_arg1935z00_1412 =
									(((BgL_importz00_bglt) COBJECT(
											((BgL_importz00_bglt) BgL_importz00_97)))->BgL_srcz00);
								{	/* Module/impuse.scm 470 */
									obj_t BgL_list1936z00_1413;

									BgL_list1936z00_1413 =
										MAKE_YOUNG_PAIR(BgL_arg1935z00_1412, BNIL);
									BgL_pz00_1366 =
										BGl_importzd2parserzd2zzmodule_impusez00
										(BgL_arg1933z00_1410, BgL_arg1934z00_1411, BFALSE,
										BgL_list1936z00_1413);
								}
							}
							{
								obj_t BgL_expz00_1372;
								obj_t BgL_synz00_1370;
								obj_t BgL_macz00_1368;

								{	/* Module/impuse.scm 471 */
									bool_t BgL_test2327z00_3423;

									{	/* Module/impuse.scm 471 */
										obj_t BgL_classz00_2092;

										BgL_classz00_2092 = BGl_globalz00zzast_varz00;
										if (BGL_OBJECTP(BgL_pz00_1366))
											{	/* Module/impuse.scm 471 */
												BgL_objectz00_bglt BgL_arg1807z00_2094;

												BgL_arg1807z00_2094 =
													(BgL_objectz00_bglt) (BgL_pz00_1366);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Module/impuse.scm 471 */
														long BgL_idxz00_2100;

														BgL_idxz00_2100 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2094);
														BgL_test2327z00_3423 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2100 + 2L)) == BgL_classz00_2092);
													}
												else
													{	/* Module/impuse.scm 471 */
														bool_t BgL_res2116z00_2125;

														{	/* Module/impuse.scm 471 */
															obj_t BgL_oclassz00_2108;

															{	/* Module/impuse.scm 471 */
																obj_t BgL_arg1815z00_2116;
																long BgL_arg1816z00_2117;

																BgL_arg1815z00_2116 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Module/impuse.scm 471 */
																	long BgL_arg1817z00_2118;

																	BgL_arg1817z00_2118 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2094);
																	BgL_arg1816z00_2117 =
																		(BgL_arg1817z00_2118 - OBJECT_TYPE);
																}
																BgL_oclassz00_2108 =
																	VECTOR_REF(BgL_arg1815z00_2116,
																	BgL_arg1816z00_2117);
															}
															{	/* Module/impuse.scm 471 */
																bool_t BgL__ortest_1115z00_2109;

																BgL__ortest_1115z00_2109 =
																	(BgL_classz00_2092 == BgL_oclassz00_2108);
																if (BgL__ortest_1115z00_2109)
																	{	/* Module/impuse.scm 471 */
																		BgL_res2116z00_2125 =
																			BgL__ortest_1115z00_2109;
																	}
																else
																	{	/* Module/impuse.scm 471 */
																		long BgL_odepthz00_2110;

																		{	/* Module/impuse.scm 471 */
																			obj_t BgL_arg1804z00_2111;

																			BgL_arg1804z00_2111 =
																				(BgL_oclassz00_2108);
																			BgL_odepthz00_2110 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2111);
																		}
																		if ((2L < BgL_odepthz00_2110))
																			{	/* Module/impuse.scm 471 */
																				obj_t BgL_arg1802z00_2113;

																				{	/* Module/impuse.scm 471 */
																					obj_t BgL_arg1803z00_2114;

																					BgL_arg1803z00_2114 =
																						(BgL_oclassz00_2108);
																					BgL_arg1802z00_2113 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2114, 2L);
																				}
																				BgL_res2116z00_2125 =
																					(BgL_arg1802z00_2113 ==
																					BgL_classz00_2092);
																			}
																		else
																			{	/* Module/impuse.scm 471 */
																				BgL_res2116z00_2125 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2327z00_3423 = BgL_res2116z00_2125;
													}
											}
										else
											{	/* Module/impuse.scm 471 */
												BgL_test2327z00_3423 = ((bool_t) 0);
											}
									}
									if (BgL_test2327z00_3423)
										{	/* Module/impuse.scm 471 */
											{	/* Module/impuse.scm 473 */
												BgL_valuez00_bglt BgL_valz00_1390;

												BgL_valz00_1390 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_pz00_1366))))->
													BgL_valuez00);
												{	/* Module/impuse.scm 474 */
													obj_t BgL_arg1914z00_1391;
													obj_t BgL_arg1916z00_1392;

													BgL_arg1914z00_1391 =
														CDR(((obj_t) BgL_providedz00_1355));
													{	/* Module/impuse.scm 476 */
														bool_t BgL_test2332z00_3451;

														{	/* Module/impuse.scm 476 */
															bool_t BgL_test2333z00_3452;

															{	/* Module/impuse.scm 476 */
																obj_t BgL_classz00_2018;

																BgL_classz00_2018 = BGl_globalz00zzast_varz00;
																if (BGL_OBJECTP(BgL_pz00_1366))
																	{	/* Module/impuse.scm 476 */
																		BgL_objectz00_bglt BgL_arg1807z00_2020;

																		BgL_arg1807z00_2020 =
																			(BgL_objectz00_bglt) (BgL_pz00_1366);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Module/impuse.scm 476 */
																				long BgL_idxz00_2026;

																				BgL_idxz00_2026 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2020);
																				BgL_test2333z00_3452 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2026 + 2L)) ==
																					BgL_classz00_2018);
																			}
																		else
																			{	/* Module/impuse.scm 476 */
																				bool_t BgL_res2114z00_2051;

																				{	/* Module/impuse.scm 476 */
																					obj_t BgL_oclassz00_2034;

																					{	/* Module/impuse.scm 476 */
																						obj_t BgL_arg1815z00_2042;
																						long BgL_arg1816z00_2043;

																						BgL_arg1815z00_2042 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Module/impuse.scm 476 */
																							long BgL_arg1817z00_2044;

																							BgL_arg1817z00_2044 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2020);
																							BgL_arg1816z00_2043 =
																								(BgL_arg1817z00_2044 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2034 =
																							VECTOR_REF(BgL_arg1815z00_2042,
																							BgL_arg1816z00_2043);
																					}
																					{	/* Module/impuse.scm 476 */
																						bool_t BgL__ortest_1115z00_2035;

																						BgL__ortest_1115z00_2035 =
																							(BgL_classz00_2018 ==
																							BgL_oclassz00_2034);
																						if (BgL__ortest_1115z00_2035)
																							{	/* Module/impuse.scm 476 */
																								BgL_res2114z00_2051 =
																									BgL__ortest_1115z00_2035;
																							}
																						else
																							{	/* Module/impuse.scm 476 */
																								long BgL_odepthz00_2036;

																								{	/* Module/impuse.scm 476 */
																									obj_t BgL_arg1804z00_2037;

																									BgL_arg1804z00_2037 =
																										(BgL_oclassz00_2034);
																									BgL_odepthz00_2036 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2037);
																								}
																								if ((2L < BgL_odepthz00_2036))
																									{	/* Module/impuse.scm 476 */
																										obj_t BgL_arg1802z00_2039;

																										{	/* Module/impuse.scm 476 */
																											obj_t BgL_arg1803z00_2040;

																											BgL_arg1803z00_2040 =
																												(BgL_oclassz00_2034);
																											BgL_arg1802z00_2039 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2040,
																												2L);
																										}
																										BgL_res2114z00_2051 =
																											(BgL_arg1802z00_2039 ==
																											BgL_classz00_2018);
																									}
																								else
																									{	/* Module/impuse.scm 476 */
																										BgL_res2114z00_2051 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2333z00_3452 =
																					BgL_res2114z00_2051;
																			}
																	}
																else
																	{	/* Module/impuse.scm 476 */
																		BgL_test2333z00_3452 = ((bool_t) 0);
																	}
															}
															if (BgL_test2333z00_3452)
																{	/* Module/impuse.scm 476 */
																	bool_t BgL_test2338z00_3475;

																	{	/* Module/impuse.scm 476 */
																		obj_t BgL_classz00_2052;

																		BgL_classz00_2052 = BGl_sfunz00zzast_varz00;
																		{	/* Module/impuse.scm 476 */
																			BgL_objectz00_bglt BgL_arg1807z00_2054;

																			{	/* Module/impuse.scm 476 */
																				obj_t BgL_tmpz00_3476;

																				BgL_tmpz00_3476 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_valz00_1390));
																				BgL_arg1807z00_2054 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_3476);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Module/impuse.scm 476 */
																					long BgL_idxz00_2060;

																					BgL_idxz00_2060 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2054);
																					BgL_test2338z00_3475 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2060 + 3L)) ==
																						BgL_classz00_2052);
																				}
																			else
																				{	/* Module/impuse.scm 476 */
																					bool_t BgL_res2115z00_2085;

																					{	/* Module/impuse.scm 476 */
																						obj_t BgL_oclassz00_2068;

																						{	/* Module/impuse.scm 476 */
																							obj_t BgL_arg1815z00_2076;
																							long BgL_arg1816z00_2077;

																							BgL_arg1815z00_2076 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Module/impuse.scm 476 */
																								long BgL_arg1817z00_2078;

																								BgL_arg1817z00_2078 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2054);
																								BgL_arg1816z00_2077 =
																									(BgL_arg1817z00_2078 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2068 =
																								VECTOR_REF(BgL_arg1815z00_2076,
																								BgL_arg1816z00_2077);
																						}
																						{	/* Module/impuse.scm 476 */
																							bool_t BgL__ortest_1115z00_2069;

																							BgL__ortest_1115z00_2069 =
																								(BgL_classz00_2052 ==
																								BgL_oclassz00_2068);
																							if (BgL__ortest_1115z00_2069)
																								{	/* Module/impuse.scm 476 */
																									BgL_res2115z00_2085 =
																										BgL__ortest_1115z00_2069;
																								}
																							else
																								{	/* Module/impuse.scm 476 */
																									long BgL_odepthz00_2070;

																									{	/* Module/impuse.scm 476 */
																										obj_t BgL_arg1804z00_2071;

																										BgL_arg1804z00_2071 =
																											(BgL_oclassz00_2068);
																										BgL_odepthz00_2070 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2071);
																									}
																									if ((3L < BgL_odepthz00_2070))
																										{	/* Module/impuse.scm 476 */
																											obj_t BgL_arg1802z00_2073;

																											{	/* Module/impuse.scm 476 */
																												obj_t
																													BgL_arg1803z00_2074;
																												BgL_arg1803z00_2074 =
																													(BgL_oclassz00_2068);
																												BgL_arg1802z00_2073 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2074,
																													3L);
																											}
																											BgL_res2115z00_2085 =
																												(BgL_arg1802z00_2073 ==
																												BgL_classz00_2052);
																										}
																									else
																										{	/* Module/impuse.scm 476 */
																											BgL_res2115z00_2085 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2338z00_3475 =
																						BgL_res2115z00_2085;
																				}
																		}
																	}
																	if (BgL_test2338z00_3475)
																		{	/* Module/impuse.scm 476 */
																			BgL_test2332z00_3451 = ((bool_t) 0);
																		}
																	else
																		{	/* Module/impuse.scm 476 */
																			BgL_test2332z00_3451 = ((bool_t) 1);
																		}
																}
															else
																{	/* Module/impuse.scm 476 */
																	BgL_test2332z00_3451 = ((bool_t) 1);
																}
														}
														if (BgL_test2332z00_3451)
															{	/* Module/impuse.scm 476 */
																BgL_arg1916z00_1392 = BgL_inlinez00_1356;
															}
														else
															{	/* Module/impuse.scm 476 */
																if (
																	((((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						BgL_valz00_1390)))->BgL_classz00) ==
																		CNST_TABLE_REF(19)))
																	{	/* Module/impuse.scm 479 */
																		obj_t BgL_arg1923z00_1398;

																		{	/* Module/impuse.scm 479 */
																			obj_t BgL_arg1924z00_1399;

																			BgL_arg1924z00_1399 =
																				(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_globalz00_bglt)
																								BgL_pz00_1366))))->BgL_idz00);
																			BgL_arg1923z00_1398 =
																				MAKE_YOUNG_PAIR(BgL_arg1924z00_1399,
																				CNST_TABLE_REF(19));
																		}
																		BgL_arg1916z00_1392 =
																			MAKE_YOUNG_PAIR(BgL_arg1923z00_1398,
																			BgL_inlinez00_1356);
																	}
																else
																	{	/* Module/impuse.scm 478 */
																		BgL_arg1916z00_1392 = BgL_inlinez00_1356;
																	}
															}
													}
													{
														obj_t BgL_inlinez00_3511;
														obj_t BgL_providedz00_3510;

														BgL_providedz00_3510 = BgL_arg1914z00_1391;
														BgL_inlinez00_3511 = BgL_arg1916z00_1392;
														BgL_inlinez00_1356 = BgL_inlinez00_3511;
														BgL_providedz00_1355 = BgL_providedz00_3510;
														goto BgL_zc3z04anonymousza31895ze3z87_1360;
													}
												}
											}
										}
									else
										{	/* Module/impuse.scm 471 */
											if (PAIRP(BgL_pz00_1366))
												{	/* Module/impuse.scm 471 */
													if (
														(CAR(
																((obj_t) BgL_pz00_1366)) == CNST_TABLE_REF(27)))
														{	/* Module/impuse.scm 471 */
															obj_t BgL_arg1901z00_1380;

															BgL_arg1901z00_1380 =
																CDR(((obj_t) BgL_pz00_1366));
															BgL_macz00_1368 = BgL_arg1901z00_1380;
															{	/* Module/impuse.scm 486 */
																obj_t BgL_arg1926z00_1403;
																obj_t BgL_arg1927z00_1404;

																BgL_arg1926z00_1403 =
																	CDR(((obj_t) BgL_providedz00_1355));
																BgL_arg1927z00_1404 =
																	MAKE_YOUNG_PAIR(BgL_macz00_1368,
																	BgL_macroz00_1357);
																{
																	obj_t BgL_macroz00_3525;
																	obj_t BgL_providedz00_3524;

																	BgL_providedz00_3524 = BgL_arg1926z00_1403;
																	BgL_macroz00_3525 = BgL_arg1927z00_1404;
																	BgL_macroz00_1357 = BgL_macroz00_3525;
																	BgL_providedz00_1355 = BgL_providedz00_3524;
																	goto BgL_zc3z04anonymousza31895ze3z87_1360;
																}
															}
														}
													else
														{	/* Module/impuse.scm 471 */
															if (
																(CAR(
																		((obj_t) BgL_pz00_1366)) ==
																	CNST_TABLE_REF(28)))
																{	/* Module/impuse.scm 471 */
																	obj_t BgL_arg1904z00_1383;

																	BgL_arg1904z00_1383 =
																		CDR(((obj_t) BgL_pz00_1366));
																	BgL_synz00_1370 = BgL_arg1904z00_1383;
																	{	/* Module/impuse.scm 488 */
																		obj_t BgL_arg1928z00_1405;
																		obj_t BgL_arg1929z00_1406;

																		BgL_arg1928z00_1405 =
																			CDR(((obj_t) BgL_providedz00_1355));
																		BgL_arg1929z00_1406 =
																			MAKE_YOUNG_PAIR(BgL_synz00_1370,
																			BgL_syntaxz00_1358);
																		{
																			obj_t BgL_syntaxz00_3537;
																			obj_t BgL_providedz00_3536;

																			BgL_providedz00_3536 =
																				BgL_arg1928z00_1405;
																			BgL_syntaxz00_3537 = BgL_arg1929z00_1406;
																			BgL_syntaxz00_1358 = BgL_syntaxz00_3537;
																			BgL_providedz00_1355 =
																				BgL_providedz00_3536;
																			goto
																				BgL_zc3z04anonymousza31895ze3z87_1360;
																		}
																	}
																}
															else
																{	/* Module/impuse.scm 471 */
																	if (
																		(CAR(
																				((obj_t) BgL_pz00_1366)) ==
																			CNST_TABLE_REF(29)))
																		{	/* Module/impuse.scm 471 */
																			obj_t BgL_arg1910z00_1386;

																			BgL_arg1910z00_1386 =
																				CDR(((obj_t) BgL_pz00_1366));
																			BgL_expz00_1372 = BgL_arg1910z00_1386;
																			{	/* Module/impuse.scm 490 */
																				obj_t BgL_arg1930z00_1407;
																				obj_t BgL_arg1931z00_1408;

																				BgL_arg1930z00_1407 =
																					CDR(((obj_t) BgL_providedz00_1355));
																				BgL_arg1931z00_1408 =
																					MAKE_YOUNG_PAIR(BgL_expz00_1372,
																					BgL_expdz00_1359);
																				{
																					obj_t BgL_expdz00_3549;
																					obj_t BgL_providedz00_3548;

																					BgL_providedz00_3548 =
																						BgL_arg1930z00_1407;
																					BgL_expdz00_3549 =
																						BgL_arg1931z00_1408;
																					BgL_expdz00_1359 = BgL_expdz00_3549;
																					BgL_providedz00_1355 =
																						BgL_providedz00_3548;
																					goto
																						BgL_zc3z04anonymousza31895ze3z87_1360;
																				}
																			}
																		}
																	else
																		{	/* Module/impuse.scm 471 */
																		BgL_tagzd2378zd2_1374:
																			{	/* Module/impuse.scm 492 */
																				obj_t BgL_arg1932z00_1409;

																				BgL_arg1932z00_1409 =
																					CDR(((obj_t) BgL_providedz00_1355));
																				{
																					obj_t BgL_providedz00_3552;

																					BgL_providedz00_3552 =
																						BgL_arg1932z00_1409;
																					BgL_providedz00_1355 =
																						BgL_providedz00_3552;
																					goto
																						BgL_zc3z04anonymousza31895ze3z87_1360;
																				}
																			}
																		}
																}
														}
												}
											else
												{	/* Module/impuse.scm 471 */
													goto BgL_tagzd2378zd2_1374;
												}
										}
								}
							}
						}
				}
			}
		}

	}



/* import-wanted */
	obj_t BGl_importzd2wantedzd2zzmodule_impusez00(obj_t BgL_importz00_98,
		obj_t BgL_varsz00_99)
	{
		{	/* Module/impuse.scm 497 */
			{
				obj_t BgL_providedz00_1422;
				obj_t BgL_inlinez00_1423;
				obj_t BgL_macroz00_1424;
				obj_t BgL_syntaxz00_1425;
				obj_t BgL_expanderz00_1426;
				obj_t BgL_wantedz00_1427;

				{	/* Module/impuse.scm 499 */
					obj_t BgL_arg1937z00_1421;

					BgL_arg1937z00_1421 =
						(((BgL_importz00_bglt) COBJECT(
								((BgL_importz00_bglt) BgL_importz00_98)))->BgL_providez00);
					BgL_providedz00_1422 = BgL_arg1937z00_1421;
					BgL_inlinez00_1423 = BNIL;
					BgL_macroz00_1424 = BNIL;
					BgL_syntaxz00_1425 = BNIL;
					BgL_expanderz00_1426 = BNIL;
					BgL_wantedz00_1427 = BgL_varsz00_99;
				BgL_zc3z04anonymousza31938ze3z87_1428:
					if (NULLP(BgL_wantedz00_1427))
						{	/* Module/impuse.scm 509 */
							{	/* Module/impuse.scm 510 */
								int BgL_tmpz00_3557;

								BgL_tmpz00_3557 = (int) (4L);
								BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3557);
							}
							{	/* Module/impuse.scm 510 */
								int BgL_tmpz00_3560;

								BgL_tmpz00_3560 = (int) (1L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3560, BgL_macroz00_1424);
							}
							{	/* Module/impuse.scm 510 */
								int BgL_tmpz00_3563;

								BgL_tmpz00_3563 = (int) (2L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3563, BgL_syntaxz00_1425);
							}
							{	/* Module/impuse.scm 510 */
								int BgL_tmpz00_3566;

								BgL_tmpz00_3566 = (int) (3L);
								BGL_MVALUES_VAL_SET(BgL_tmpz00_3566, BgL_expanderz00_1426);
							}
							return BgL_inlinez00_1423;
						}
					else
						{	/* Module/impuse.scm 509 */
							if (NULLP(BgL_providedz00_1422))
								{	/* Module/impuse.scm 511 */
									{	/* Module/impuse.scm 512 */
										obj_t BgL_arg1941z00_1435;
										obj_t BgL_arg1942z00_1436;
										obj_t BgL_arg1943z00_1437;

										BgL_arg1941z00_1435 =
											BGl_findzd2locationzd2zztools_locationz00
											(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00);
										BgL_arg1942z00_1436 =
											(((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
														BgL_importz00_98)))->BgL_modulez00);
										{	/* Module/impuse.scm 515 */
											obj_t BgL_head1225z00_1441;

											{	/* Module/impuse.scm 515 */
												obj_t BgL_arg1951z00_1453;

												{	/* Module/impuse.scm 515 */
													obj_t BgL_pairz00_2134;

													BgL_pairz00_2134 = CAR(((obj_t) BgL_wantedz00_1427));
													BgL_arg1951z00_1453 = CAR(BgL_pairz00_2134);
												}
												BgL_head1225z00_1441 =
													MAKE_YOUNG_PAIR(BgL_arg1951z00_1453, BNIL);
											}
											{	/* Module/impuse.scm 515 */
												obj_t BgL_g1228z00_1442;

												BgL_g1228z00_1442 = CDR(((obj_t) BgL_wantedz00_1427));
												{
													obj_t BgL_l1223z00_1444;
													obj_t BgL_tail1226z00_1445;

													BgL_l1223z00_1444 = BgL_g1228z00_1442;
													BgL_tail1226z00_1445 = BgL_head1225z00_1441;
												BgL_zc3z04anonymousza31946ze3z87_1446:
													if (NULLP(BgL_l1223z00_1444))
														{	/* Module/impuse.scm 515 */
															BgL_arg1943z00_1437 = BgL_head1225z00_1441;
														}
													else
														{	/* Module/impuse.scm 515 */
															obj_t BgL_newtail1227z00_1448;

															{	/* Module/impuse.scm 515 */
																obj_t BgL_arg1949z00_1450;

																{	/* Module/impuse.scm 515 */
																	obj_t BgL_pairz00_2137;

																	BgL_pairz00_2137 =
																		CAR(((obj_t) BgL_l1223z00_1444));
																	BgL_arg1949z00_1450 = CAR(BgL_pairz00_2137);
																}
																BgL_newtail1227z00_1448 =
																	MAKE_YOUNG_PAIR(BgL_arg1949z00_1450, BNIL);
															}
															SET_CDR(BgL_tail1226z00_1445,
																BgL_newtail1227z00_1448);
															{	/* Module/impuse.scm 515 */
																obj_t BgL_arg1948z00_1449;

																BgL_arg1948z00_1449 =
																	CDR(((obj_t) BgL_l1223z00_1444));
																{
																	obj_t BgL_tail1226z00_3590;
																	obj_t BgL_l1223z00_3589;

																	BgL_l1223z00_3589 = BgL_arg1948z00_1449;
																	BgL_tail1226z00_3590 =
																		BgL_newtail1227z00_1448;
																	BgL_tail1226z00_1445 = BgL_tail1226z00_3590;
																	BgL_l1223z00_1444 = BgL_l1223z00_3589;
																	goto BgL_zc3z04anonymousza31946ze3z87_1446;
																}
															}
														}
												}
											}
										}
										{	/* Module/impuse.scm 512 */
											obj_t BgL_list1944z00_1438;

											BgL_list1944z00_1438 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											BGl_userzd2errorzf2locationz20zztools_errorz00
												(BgL_arg1941z00_1435, BgL_arg1942z00_1436,
												BGl_string2143z00zzmodule_impusez00,
												BgL_arg1943z00_1437, BgL_list1944z00_1438);
										}
									}
									{	/* Module/impuse.scm 517 */
										int BgL_tmpz00_3593;

										BgL_tmpz00_3593 = (int) (4L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_3593);
									}
									{	/* Module/impuse.scm 517 */
										int BgL_tmpz00_3596;

										BgL_tmpz00_3596 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3596, BNIL);
									}
									{	/* Module/impuse.scm 517 */
										int BgL_tmpz00_3599;

										BgL_tmpz00_3599 = (int) (2L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3599, BNIL);
									}
									{	/* Module/impuse.scm 517 */
										int BgL_tmpz00_3602;

										BgL_tmpz00_3602 = (int) (3L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_3602, BNIL);
									}
									return BNIL;
								}
							else
								{	/* Module/impuse.scm 519 */
									obj_t BgL_protoz00_1459;

									{	/* Module/impuse.scm 519 */
										obj_t BgL_arg1994z00_1521;

										BgL_arg1994z00_1521 = CAR(((obj_t) BgL_providedz00_1422));
										BgL_protoz00_1459 =
											BGl_parsezd2prototypezd2zzmodule_prototypez00
											(BgL_arg1994z00_1521);
									}
									if (PAIRP(BgL_protoz00_1459))
										{	/* Module/impuse.scm 521 */
											obj_t BgL_idz00_1461;

											{	/* Module/impuse.scm 522 */
												obj_t BgL_arg1990z00_1517;
												obj_t BgL_arg1991z00_1518;

												BgL_arg1990z00_1517 = CAR(CDR(BgL_protoz00_1459));
												{	/* Module/impuse.scm 522 */
													obj_t BgL_arg1992z00_1519;

													BgL_arg1992z00_1519 =
														CAR(((obj_t) BgL_providedz00_1422));
													BgL_arg1991z00_1518 =
														BGl_findzd2locationzd2zztools_locationz00
														(BgL_arg1992z00_1519);
												}
												BgL_idz00_1461 =
													BGl_fastzd2idzd2ofzd2idzd2zzast_identz00
													(BgL_arg1990z00_1517, BgL_arg1991z00_1518);
											}
											{
												obj_t BgL_inlinez00_1463;
												obj_t BgL_macroz00_1464;
												obj_t BgL_syntaxz00_1465;
												obj_t BgL_expanderz00_1466;
												obj_t BgL_wantedz00_1467;

												BgL_inlinez00_1463 = BgL_inlinez00_1423;
												BgL_macroz00_1464 = BgL_macroz00_1424;
												BgL_syntaxz00_1465 = BgL_syntaxz00_1425;
												BgL_expanderz00_1466 = BgL_expanderz00_1426;
												BgL_wantedz00_1467 = BgL_wantedz00_1427;
											BgL_zc3z04anonymousza31954ze3z87_1468:
												{	/* Module/impuse.scm 528 */
													obj_t BgL_cz00_1469;

													BgL_cz00_1469 =
														BGl_assqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_idz00_1461, BgL_wantedz00_1467);
													if (CBOOL(BgL_cz00_1469))
														{	/* Module/impuse.scm 533 */
															obj_t BgL_pz00_1470;

															{	/* Module/impuse.scm 533 */
																obj_t BgL_arg1984z00_1510;
																obj_t BgL_arg1985z00_1511;
																obj_t BgL_arg1986z00_1512;
																obj_t BgL_arg1987z00_1513;

																BgL_arg1984z00_1510 =
																	(((BgL_importz00_bglt) COBJECT(
																			((BgL_importz00_bglt)
																				BgL_importz00_98)))->BgL_modulez00);
																BgL_arg1985z00_1511 =
																	CAR(((obj_t) BgL_providedz00_1422));
																BgL_arg1986z00_1512 =
																	CDR(((obj_t) BgL_cz00_1469));
																BgL_arg1987z00_1513 =
																	(((BgL_importz00_bglt)
																		COBJECT(((BgL_importz00_bglt)
																				BgL_importz00_98)))->BgL_srcz00);
																{	/* Module/impuse.scm 533 */
																	obj_t BgL_list1988z00_1514;

																	BgL_list1988z00_1514 =
																		MAKE_YOUNG_PAIR(BgL_arg1987z00_1513, BNIL);
																	BgL_pz00_1470 =
																		BGl_importzd2parserzd2zzmodule_impusez00
																		(BgL_arg1984z00_1510, BgL_arg1985z00_1511,
																		BgL_arg1986z00_1512, BgL_list1988z00_1514);
																}
															}
															{
																obj_t BgL_expz00_1477;
																obj_t BgL_synz00_1475;
																obj_t BgL_macz00_1473;

																{	/* Module/impuse.scm 534 */
																	bool_t BgL_test2352z00_3629;

																	{	/* Module/impuse.scm 534 */
																		obj_t BgL_classz00_2149;

																		BgL_classz00_2149 =
																			BGl_globalz00zzast_varz00;
																		if (BGL_OBJECTP(BgL_pz00_1470))
																			{	/* Module/impuse.scm 534 */
																				BgL_objectz00_bglt BgL_arg1807z00_2151;

																				BgL_arg1807z00_2151 =
																					(BgL_objectz00_bglt) (BgL_pz00_1470);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Module/impuse.scm 534 */
																						long BgL_idxz00_2157;

																						BgL_idxz00_2157 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_2151);
																						BgL_test2352z00_3629 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_2157 + 2L)) ==
																							BgL_classz00_2149);
																					}
																				else
																					{	/* Module/impuse.scm 534 */
																						bool_t BgL_res2117z00_2182;

																						{	/* Module/impuse.scm 534 */
																							obj_t BgL_oclassz00_2165;

																							{	/* Module/impuse.scm 534 */
																								obj_t BgL_arg1815z00_2173;
																								long BgL_arg1816z00_2174;

																								BgL_arg1815z00_2173 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Module/impuse.scm 534 */
																									long BgL_arg1817z00_2175;

																									BgL_arg1817z00_2175 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_2151);
																									BgL_arg1816z00_2174 =
																										(BgL_arg1817z00_2175 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_2165 =
																									VECTOR_REF
																									(BgL_arg1815z00_2173,
																									BgL_arg1816z00_2174);
																							}
																							{	/* Module/impuse.scm 534 */
																								bool_t BgL__ortest_1115z00_2166;

																								BgL__ortest_1115z00_2166 =
																									(BgL_classz00_2149 ==
																									BgL_oclassz00_2165);
																								if (BgL__ortest_1115z00_2166)
																									{	/* Module/impuse.scm 534 */
																										BgL_res2117z00_2182 =
																											BgL__ortest_1115z00_2166;
																									}
																								else
																									{	/* Module/impuse.scm 534 */
																										long BgL_odepthz00_2167;

																										{	/* Module/impuse.scm 534 */
																											obj_t BgL_arg1804z00_2168;

																											BgL_arg1804z00_2168 =
																												(BgL_oclassz00_2165);
																											BgL_odepthz00_2167 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_2168);
																										}
																										if (
																											(2L < BgL_odepthz00_2167))
																											{	/* Module/impuse.scm 534 */
																												obj_t
																													BgL_arg1802z00_2170;
																												{	/* Module/impuse.scm 534 */
																													obj_t
																														BgL_arg1803z00_2171;
																													BgL_arg1803z00_2171 =
																														(BgL_oclassz00_2165);
																													BgL_arg1802z00_2170 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_2171,
																														2L);
																												}
																												BgL_res2117z00_2182 =
																													(BgL_arg1802z00_2170
																													== BgL_classz00_2149);
																											}
																										else
																											{	/* Module/impuse.scm 534 */
																												BgL_res2117z00_2182 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2352z00_3629 =
																							BgL_res2117z00_2182;
																					}
																			}
																		else
																			{	/* Module/impuse.scm 534 */
																				BgL_test2352z00_3629 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2352z00_3629)
																		{	/* Module/impuse.scm 534 */
																			{	/* Module/impuse.scm 538 */
																				obj_t BgL_arg1970z00_1496;
																				obj_t BgL_arg1971z00_1497;

																				if (
																					(CAR(
																							((obj_t) BgL_protoz00_1459)) ==
																						CNST_TABLE_REF(19)))
																					{	/* Module/impuse.scm 539 */
																						obj_t BgL_arg1974z00_1500;

																						BgL_arg1974z00_1500 =
																							MAKE_YOUNG_PAIR(BgL_idz00_1461,
																							CNST_TABLE_REF(19));
																						BgL_arg1970z00_1496 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1974z00_1500,
																							BgL_inlinez00_1463);
																					}
																				else
																					{	/* Module/impuse.scm 538 */
																						BgL_arg1970z00_1496 =
																							BgL_inlinez00_1463;
																					}
																				BgL_arg1971z00_1497 =
																					bgl_remq_bang(BgL_cz00_1469,
																					BgL_wantedz00_1467);
																				{
																					obj_t BgL_wantedz00_3662;
																					obj_t BgL_inlinez00_3661;

																					BgL_inlinez00_3661 =
																						BgL_arg1970z00_1496;
																					BgL_wantedz00_3662 =
																						BgL_arg1971z00_1497;
																					BgL_wantedz00_1467 =
																						BgL_wantedz00_3662;
																					BgL_inlinez00_1463 =
																						BgL_inlinez00_3661;
																					goto
																						BgL_zc3z04anonymousza31954ze3z87_1468;
																				}
																			}
																		}
																	else
																		{	/* Module/impuse.scm 534 */
																			bool_t BgL_test2358z00_3663;

																			{	/* Module/impuse.scm 534 */
																				obj_t BgL_classz00_2183;

																				BgL_classz00_2183 =
																					BGl_typez00zztype_typez00;
																				if (BGL_OBJECTP(BgL_pz00_1470))
																					{	/* Module/impuse.scm 534 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2185;
																						BgL_arg1807z00_2185 =
																							(BgL_objectz00_bglt)
																							(BgL_pz00_1470);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Module/impuse.scm 534 */
																								long BgL_idxz00_2191;

																								BgL_idxz00_2191 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2185);
																								BgL_test2358z00_3663 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2191 + 1L)) ==
																									BgL_classz00_2183);
																							}
																						else
																							{	/* Module/impuse.scm 534 */
																								bool_t BgL_res2118z00_2216;

																								{	/* Module/impuse.scm 534 */
																									obj_t BgL_oclassz00_2199;

																									{	/* Module/impuse.scm 534 */
																										obj_t BgL_arg1815z00_2207;
																										long BgL_arg1816z00_2208;

																										BgL_arg1815z00_2207 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Module/impuse.scm 534 */
																											long BgL_arg1817z00_2209;

																											BgL_arg1817z00_2209 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2185);
																											BgL_arg1816z00_2208 =
																												(BgL_arg1817z00_2209 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2199 =
																											VECTOR_REF
																											(BgL_arg1815z00_2207,
																											BgL_arg1816z00_2208);
																									}
																									{	/* Module/impuse.scm 534 */
																										bool_t
																											BgL__ortest_1115z00_2200;
																										BgL__ortest_1115z00_2200 =
																											(BgL_classz00_2183 ==
																											BgL_oclassz00_2199);
																										if (BgL__ortest_1115z00_2200)
																											{	/* Module/impuse.scm 534 */
																												BgL_res2118z00_2216 =
																													BgL__ortest_1115z00_2200;
																											}
																										else
																											{	/* Module/impuse.scm 534 */
																												long BgL_odepthz00_2201;

																												{	/* Module/impuse.scm 534 */
																													obj_t
																														BgL_arg1804z00_2202;
																													BgL_arg1804z00_2202 =
																														(BgL_oclassz00_2199);
																													BgL_odepthz00_2201 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2202);
																												}
																												if (
																													(1L <
																														BgL_odepthz00_2201))
																													{	/* Module/impuse.scm 534 */
																														obj_t
																															BgL_arg1802z00_2204;
																														{	/* Module/impuse.scm 534 */
																															obj_t
																																BgL_arg1803z00_2205;
																															BgL_arg1803z00_2205
																																=
																																(BgL_oclassz00_2199);
																															BgL_arg1802z00_2204
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2205,
																																1L);
																														}
																														BgL_res2118z00_2216
																															=
																															(BgL_arg1802z00_2204
																															==
																															BgL_classz00_2183);
																													}
																												else
																													{	/* Module/impuse.scm 534 */
																														BgL_res2118z00_2216
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2358z00_3663 =
																									BgL_res2118z00_2216;
																							}
																					}
																				else
																					{	/* Module/impuse.scm 534 */
																						BgL_test2358z00_3663 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2358z00_3663)
																				{	/* Module/impuse.scm 534 */
																					{
																						obj_t BgL_wantedz00_3686;

																						BgL_wantedz00_3686 =
																							bgl_remq_bang(BgL_cz00_1469,
																							BgL_wantedz00_1467);
																						BgL_wantedz00_1467 =
																							BgL_wantedz00_3686;
																						goto
																							BgL_zc3z04anonymousza31954ze3z87_1468;
																					}
																				}
																			else
																				{	/* Module/impuse.scm 534 */
																					if (PAIRP(BgL_pz00_1470))
																						{	/* Module/impuse.scm 534 */
																							if (
																								(CAR(
																										((obj_t) BgL_pz00_1470)) ==
																									CNST_TABLE_REF(27)))
																								{	/* Module/impuse.scm 534 */
																									obj_t BgL_arg1960z00_1486;

																									BgL_arg1960z00_1486 =
																										CDR(
																										((obj_t) BgL_pz00_1470));
																									BgL_macz00_1473 =
																										BgL_arg1960z00_1486;
																									{	/* Module/impuse.scm 549 */
																										obj_t BgL_arg1977z00_1503;
																										obj_t BgL_arg1978z00_1504;

																										BgL_arg1977z00_1503 =
																											MAKE_YOUNG_PAIR
																											(BgL_macz00_1473,
																											BgL_macroz00_1464);
																										BgL_arg1978z00_1504 =
																											bgl_remq_bang
																											(BgL_cz00_1469,
																											BgL_wantedz00_1467);
																										{
																											obj_t BgL_wantedz00_3700;
																											obj_t BgL_macroz00_3699;

																											BgL_macroz00_3699 =
																												BgL_arg1977z00_1503;
																											BgL_wantedz00_3700 =
																												BgL_arg1978z00_1504;
																											BgL_wantedz00_1467 =
																												BgL_wantedz00_3700;
																											BgL_macroz00_1464 =
																												BgL_macroz00_3699;
																											goto
																												BgL_zc3z04anonymousza31954ze3z87_1468;
																										}
																									}
																								}
																							else
																								{	/* Module/impuse.scm 534 */
																									if (
																										(CAR(
																												((obj_t) BgL_pz00_1470))
																											== CNST_TABLE_REF(28)))
																										{	/* Module/impuse.scm 534 */
																											obj_t BgL_arg1963z00_1489;

																											BgL_arg1963z00_1489 =
																												CDR(
																												((obj_t)
																													BgL_pz00_1470));
																											BgL_synz00_1475 =
																												BgL_arg1963z00_1489;
																											{	/* Module/impuse.scm 553 */
																												obj_t
																													BgL_arg1979z00_1505;
																												obj_t
																													BgL_arg1980z00_1506;
																												BgL_arg1979z00_1505 =
																													MAKE_YOUNG_PAIR
																													(BgL_synz00_1475,
																													BgL_syntaxz00_1465);
																												BgL_arg1980z00_1506 =
																													bgl_remq_bang
																													(BgL_cz00_1469,
																													BgL_wantedz00_1467);
																												{
																													obj_t
																														BgL_wantedz00_3711;
																													obj_t
																														BgL_syntaxz00_3710;
																													BgL_syntaxz00_3710 =
																														BgL_arg1979z00_1505;
																													BgL_wantedz00_3711 =
																														BgL_arg1980z00_1506;
																													BgL_wantedz00_1467 =
																														BgL_wantedz00_3711;
																													BgL_syntaxz00_1465 =
																														BgL_syntaxz00_3710;
																													goto
																														BgL_zc3z04anonymousza31954ze3z87_1468;
																												}
																											}
																										}
																									else
																										{	/* Module/impuse.scm 534 */
																											if (
																												(CAR(
																														((obj_t)
																															BgL_pz00_1470)) ==
																													CNST_TABLE_REF(29)))
																												{	/* Module/impuse.scm 534 */
																													obj_t
																														BgL_arg1966z00_1492;
																													BgL_arg1966z00_1492 =
																														CDR(((obj_t)
																															BgL_pz00_1470));
																													BgL_expz00_1477 =
																														BgL_arg1966z00_1492;
																													{	/* Module/impuse.scm 558 */
																														obj_t
																															BgL_arg1981z00_1507;
																														obj_t
																															BgL_arg1982z00_1508;
																														BgL_arg1981z00_1507
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_expz00_1477,
																															BgL_expanderz00_1466);
																														BgL_arg1982z00_1508
																															=
																															bgl_remq_bang
																															(BgL_cz00_1469,
																															BgL_wantedz00_1467);
																														{
																															obj_t
																																BgL_wantedz00_3722;
																															obj_t
																																BgL_expanderz00_3721;
																															BgL_expanderz00_3721
																																=
																																BgL_arg1981z00_1507;
																															BgL_wantedz00_3722
																																=
																																BgL_arg1982z00_1508;
																															BgL_wantedz00_1467
																																=
																																BgL_wantedz00_3722;
																															BgL_expanderz00_1466
																																=
																																BgL_expanderz00_3721;
																															goto
																																BgL_zc3z04anonymousza31954ze3z87_1468;
																														}
																													}
																												}
																											else
																												{	/* Module/impuse.scm 534 */
																												BgL_tagzd2411zd2_1479:
																													{
																														obj_t
																															BgL_wantedz00_3723;
																														BgL_wantedz00_3723 =
																															bgl_remq_bang
																															(BgL_cz00_1469,
																															BgL_wantedz00_1467);
																														BgL_wantedz00_1467 =
																															BgL_wantedz00_3723;
																														goto
																															BgL_zc3z04anonymousza31954ze3z87_1468;
																													}
																												}
																										}
																								}
																						}
																					else
																						{	/* Module/impuse.scm 534 */
																							goto BgL_tagzd2411zd2_1479;
																						}
																				}
																		}
																}
															}
														}
													else
														{	/* Module/impuse.scm 530 */
															obj_t BgL_arg1989z00_1515;

															BgL_arg1989z00_1515 =
																CDR(((obj_t) BgL_providedz00_1422));
															{
																obj_t BgL_wantedz00_3732;
																obj_t BgL_expanderz00_3731;
																obj_t BgL_syntaxz00_3730;
																obj_t BgL_macroz00_3729;
																obj_t BgL_inlinez00_3728;
																obj_t BgL_providedz00_3727;

																BgL_providedz00_3727 = BgL_arg1989z00_1515;
																BgL_inlinez00_3728 = BgL_inlinez00_1463;
																BgL_macroz00_3729 = BgL_macroz00_1464;
																BgL_syntaxz00_3730 = BgL_syntaxz00_1465;
																BgL_expanderz00_3731 = BgL_expanderz00_1466;
																BgL_wantedz00_3732 = BgL_wantedz00_1467;
																BgL_wantedz00_1427 = BgL_wantedz00_3732;
																BgL_expanderz00_1426 = BgL_expanderz00_3731;
																BgL_syntaxz00_1425 = BgL_syntaxz00_3730;
																BgL_macroz00_1424 = BgL_macroz00_3729;
																BgL_inlinez00_1423 = BgL_inlinez00_3728;
																BgL_providedz00_1422 = BgL_providedz00_3727;
																goto BgL_zc3z04anonymousza31938ze3z87_1428;
															}
														}
												}
											}
										}
									else
										{	/* Module/impuse.scm 563 */
											obj_t BgL_arg1993z00_1520;

											BgL_arg1993z00_1520 = CDR(((obj_t) BgL_providedz00_1422));
											{
												obj_t BgL_providedz00_3735;

												BgL_providedz00_3735 = BgL_arg1993z00_1520;
												BgL_providedz00_1422 = BgL_providedz00_3735;
												goto BgL_zc3z04anonymousza31938ze3z87_1428;
											}
										}
								}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 15 */
			{	/* Module/impuse.scm 44 */
				obj_t BgL_arg1998z00_1526;
				obj_t BgL_arg1999z00_1527;

				{	/* Module/impuse.scm 44 */
					obj_t BgL_v1233z00_1549;

					BgL_v1233z00_1549 = create_vector(12L);
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2006z00_1550;

						BgL_arg2006z00_1550 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2145z00zzmodule_impusez00,
							BGl_proc2144z00zzmodule_impusez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(30));
						VECTOR_SET(BgL_v1233z00_1549, 0L, BgL_arg2006z00_1550);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2011z00_1560;

						BgL_arg2011z00_1560 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc2148z00zzmodule_impusez00,
							BGl_proc2147z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2146z00zzmodule_impusez00, CNST_TABLE_REF(32));
						VECTOR_SET(BgL_v1233z00_1549, 1L, BgL_arg2011z00_1560);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2018z00_1573;

						BgL_arg2018z00_1573 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc2151z00zzmodule_impusez00,
							BGl_proc2150z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2149z00zzmodule_impusez00, CNST_TABLE_REF(30));
						VECTOR_SET(BgL_v1233z00_1549, 2L, BgL_arg2018z00_1573);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2025z00_1586;

						BgL_arg2025z00_1586 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(34),
							BGl_proc2154z00zzmodule_impusez00,
							BGl_proc2153z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2152z00zzmodule_impusez00, CNST_TABLE_REF(35));
						VECTOR_SET(BgL_v1233z00_1549, 3L, BgL_arg2025z00_1586);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2033z00_1599;

						BgL_arg2033z00_1599 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(36),
							BGl_proc2157z00zzmodule_impusez00,
							BGl_proc2156z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2155z00zzmodule_impusez00, CNST_TABLE_REF(35));
						VECTOR_SET(BgL_v1233z00_1549, 4L, BgL_arg2033z00_1599);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2041z00_1612;

						BgL_arg2041z00_1612 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(37),
							BGl_proc2160z00zzmodule_impusez00,
							BGl_proc2159z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2158z00zzmodule_impusez00, CNST_TABLE_REF(35));
						VECTOR_SET(BgL_v1233z00_1549, 5L, BgL_arg2041z00_1612);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2049z00_1625;

						BgL_arg2049z00_1625 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(38),
							BGl_proc2162z00zzmodule_impusez00,
							BGl_proc2161z00zzmodule_impusez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(35));
						VECTOR_SET(BgL_v1233z00_1549, 6L, BgL_arg2049z00_1625);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2055z00_1635;

						BgL_arg2055z00_1635 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(39),
							BGl_proc2164z00zzmodule_impusez00,
							BGl_proc2163z00zzmodule_impusez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(35));
						VECTOR_SET(BgL_v1233z00_1549, 7L, BgL_arg2055z00_1635);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2060z00_1645;

						BgL_arg2060z00_1645 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(40),
							BGl_proc2167z00zzmodule_impusez00,
							BGl_proc2166z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2165z00zzmodule_impusez00, CNST_TABLE_REF(35));
						VECTOR_SET(BgL_v1233z00_1549, 8L, BgL_arg2060z00_1645);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2067z00_1658;

						BgL_arg2067z00_1658 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc2170z00zzmodule_impusez00,
							BGl_proc2169z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2168z00zzmodule_impusez00, CNST_TABLE_REF(42));
						VECTOR_SET(BgL_v1233z00_1549, 9L, BgL_arg2067z00_1658);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2074z00_1671;

						BgL_arg2074z00_1671 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(43),
							BGl_proc2173z00zzmodule_impusez00,
							BGl_proc2172z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2171z00zzmodule_impusez00, CNST_TABLE_REF(35));
						VECTOR_SET(BgL_v1233z00_1549, 10L, BgL_arg2074z00_1671);
					}
					{	/* Module/impuse.scm 44 */
						obj_t BgL_arg2081z00_1684;

						BgL_arg2081z00_1684 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(44),
							BGl_proc2176z00zzmodule_impusez00,
							BGl_proc2175z00zzmodule_impusez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2174z00zzmodule_impusez00, CNST_TABLE_REF(35));
						VECTOR_SET(BgL_v1233z00_1549, 11L, BgL_arg2081z00_1684);
					}
					BgL_arg1998z00_1526 = BgL_v1233z00_1549;
				}
				{	/* Module/impuse.scm 44 */
					obj_t BgL_v1234z00_1697;

					BgL_v1234z00_1697 = create_vector(0L);
					BgL_arg1999z00_1527 = BgL_v1234z00_1697;
				}
				return (BGl_importz00zzmodule_impusez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(0),
						CNST_TABLE_REF(45), BGl_objectz00zz__objectz00, 34362L,
						BGl_proc2179z00zzmodule_impusez00,
						BGl_proc2178z00zzmodule_impusez00, BFALSE,
						BGl_proc2177z00zzmodule_impusez00, BFALSE, BgL_arg1998z00_1526,
						BgL_arg1999z00_1527), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2004> */
	obj_t BGl_z62zc3z04anonymousza32004ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2430, obj_t BgL_new1079z00_2431)
	{
		{	/* Module/impuse.scm 44 */
			{
				BgL_importz00_bglt BgL_auxz00_3789;

				((((BgL_importz00_bglt) COBJECT(
								((BgL_importz00_bglt) BgL_new1079z00_2431)))->BgL_modulez00) =
					((obj_t) CNST_TABLE_REF(46)), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_numberz00) =
					((long) 0L), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_modez00) =
					((obj_t) CNST_TABLE_REF(46)), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_varsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_aliasesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_checksumz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_srcz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_declz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_providez00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_codez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_importz00_bglt) COBJECT(((BgL_importz00_bglt)
									BgL_new1079z00_2431)))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_3789 = ((BgL_importz00_bglt) BgL_new1079z00_2431);
				return ((obj_t) BgL_auxz00_3789);
			}
		}

	}



/* &lambda2002 */
	BgL_importz00_bglt BGl_z62lambda2002z62zzmodule_impusez00(obj_t
		BgL_envz00_2432)
	{
		{	/* Module/impuse.scm 44 */
			{	/* Module/impuse.scm 44 */
				BgL_importz00_bglt BgL_new1078z00_2536;

				BgL_new1078z00_2536 =
					((BgL_importz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_importz00_bgl))));
				{	/* Module/impuse.scm 44 */
					long BgL_arg2003z00_2537;

					BgL_arg2003z00_2537 = BGL_CLASS_NUM(BGl_importz00zzmodule_impusez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1078z00_2536), BgL_arg2003z00_2537);
				}
				return BgL_new1078z00_2536;
			}
		}

	}



/* &lambda2000 */
	BgL_importz00_bglt BGl_z62lambda2000z62zzmodule_impusez00(obj_t
		BgL_envz00_2433, obj_t BgL_module1066z00_2434, obj_t BgL_number1067z00_2435,
		obj_t BgL_mode1068z00_2436, obj_t BgL_vars1069z00_2437,
		obj_t BgL_aliases1070z00_2438, obj_t BgL_checksum1071z00_2439,
		obj_t BgL_loc1072z00_2440, obj_t BgL_src1073z00_2441,
		obj_t BgL_decl1074z00_2442, obj_t BgL_provide1075z00_2443,
		obj_t BgL_code1076z00_2444, obj_t BgL_access1077z00_2445)
	{
		{	/* Module/impuse.scm 44 */
			{	/* Module/impuse.scm 44 */
				long BgL_number1067z00_2539;

				BgL_number1067z00_2539 = (long) CINT(BgL_number1067z00_2435);
				{	/* Module/impuse.scm 44 */
					BgL_importz00_bglt BgL_new1127z00_2542;

					{	/* Module/impuse.scm 44 */
						BgL_importz00_bglt BgL_new1125z00_2543;

						BgL_new1125z00_2543 =
							((BgL_importz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_importz00_bgl))));
						{	/* Module/impuse.scm 44 */
							long BgL_arg2001z00_2544;

							BgL_arg2001z00_2544 =
								BGL_CLASS_NUM(BGl_importz00zzmodule_impusez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1125z00_2543),
								BgL_arg2001z00_2544);
						}
						BgL_new1127z00_2542 = BgL_new1125z00_2543;
					}
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->
							BgL_modulez00) =
						((obj_t) ((obj_t) BgL_module1066z00_2434)), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->
							BgL_numberz00) = ((long) BgL_number1067z00_2539), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->BgL_modez00) =
						((obj_t) ((obj_t) BgL_mode1068z00_2436)), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->BgL_varsz00) =
						((obj_t) BgL_vars1069z00_2437), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->
							BgL_aliasesz00) = ((obj_t) BgL_aliases1070z00_2438), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->
							BgL_checksumz00) = ((obj_t) BgL_checksum1071z00_2439), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->BgL_locz00) =
						((obj_t) BgL_loc1072z00_2440), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->BgL_srcz00) =
						((obj_t) BgL_src1073z00_2441), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->BgL_declz00) =
						((obj_t) BgL_decl1074z00_2442), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->
							BgL_providez00) =
						((obj_t) ((obj_t) BgL_provide1075z00_2443)), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->BgL_codez00) =
						((obj_t) BgL_code1076z00_2444), BUNSPEC);
					((((BgL_importz00_bglt) COBJECT(BgL_new1127z00_2542))->
							BgL_accessz00) = ((obj_t) BgL_access1077z00_2445), BUNSPEC);
					return BgL_new1127z00_2542;
				}
			}
		}

	}



/* &<@anonymous:2087> */
	obj_t BGl_z62zc3z04anonymousza32087ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2446)
	{
		{	/* Module/impuse.scm 44 */
			return BNIL;
		}

	}



/* &lambda2086 */
	obj_t BGl_z62lambda2086z62zzmodule_impusez00(obj_t BgL_envz00_2447,
		obj_t BgL_oz00_2448, obj_t BgL_vz00_2449)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2448)))->BgL_accessz00) =
				((obj_t) BgL_vz00_2449), BUNSPEC);
		}

	}



/* &lambda2085 */
	obj_t BGl_z62lambda2085z62zzmodule_impusez00(obj_t BgL_envz00_2450,
		obj_t BgL_oz00_2451)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2451)))->BgL_accessz00);
		}

	}



/* &<@anonymous:2080> */
	obj_t BGl_z62zc3z04anonymousza32080ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2452)
	{
		{	/* Module/impuse.scm 44 */
			return BUNSPEC;
		}

	}



/* &lambda2079 */
	obj_t BGl_z62lambda2079z62zzmodule_impusez00(obj_t BgL_envz00_2453,
		obj_t BgL_oz00_2454, obj_t BgL_vz00_2455)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2454)))->BgL_codez00) =
				((obj_t) BgL_vz00_2455), BUNSPEC);
		}

	}



/* &lambda2078 */
	obj_t BGl_z62lambda2078z62zzmodule_impusez00(obj_t BgL_envz00_2456,
		obj_t BgL_oz00_2457)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2457)))->BgL_codez00);
		}

	}



/* &<@anonymous:2073> */
	obj_t BGl_z62zc3z04anonymousza32073ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2458)
	{
		{	/* Module/impuse.scm 44 */
			return BNIL;
		}

	}



/* &lambda2072 */
	obj_t BGl_z62lambda2072z62zzmodule_impusez00(obj_t BgL_envz00_2459,
		obj_t BgL_oz00_2460, obj_t BgL_vz00_2461)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2460)))->BgL_providez00) = ((obj_t)
					((obj_t) BgL_vz00_2461)), BUNSPEC);
		}

	}



/* &lambda2071 */
	obj_t BGl_z62lambda2071z62zzmodule_impusez00(obj_t BgL_envz00_2462,
		obj_t BgL_oz00_2463)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2463)))->BgL_providez00);
		}

	}



/* &<@anonymous:2066> */
	obj_t BGl_z62zc3z04anonymousza32066ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2464)
	{
		{	/* Module/impuse.scm 44 */
			return BUNSPEC;
		}

	}



/* &lambda2065 */
	obj_t BGl_z62lambda2065z62zzmodule_impusez00(obj_t BgL_envz00_2465,
		obj_t BgL_oz00_2466, obj_t BgL_vz00_2467)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2466)))->BgL_declz00) =
				((obj_t) BgL_vz00_2467), BUNSPEC);
		}

	}



/* &lambda2064 */
	obj_t BGl_z62lambda2064z62zzmodule_impusez00(obj_t BgL_envz00_2468,
		obj_t BgL_oz00_2469)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2469)))->BgL_declz00);
		}

	}



/* &lambda2059 */
	obj_t BGl_z62lambda2059z62zzmodule_impusez00(obj_t BgL_envz00_2470,
		obj_t BgL_oz00_2471, obj_t BgL_vz00_2472)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2471)))->BgL_srcz00) =
				((obj_t) BgL_vz00_2472), BUNSPEC);
		}

	}



/* &lambda2058 */
	obj_t BGl_z62lambda2058z62zzmodule_impusez00(obj_t BgL_envz00_2473,
		obj_t BgL_oz00_2474)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2474)))->BgL_srcz00);
		}

	}



/* &lambda2053 */
	obj_t BGl_z62lambda2053z62zzmodule_impusez00(obj_t BgL_envz00_2475,
		obj_t BgL_oz00_2476, obj_t BgL_vz00_2477)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2476)))->BgL_locz00) =
				((obj_t) BgL_vz00_2477), BUNSPEC);
		}

	}



/* &lambda2052 */
	obj_t BGl_z62lambda2052z62zzmodule_impusez00(obj_t BgL_envz00_2478,
		obj_t BgL_oz00_2479)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2479)))->BgL_locz00);
		}

	}



/* &<@anonymous:2048> */
	obj_t BGl_z62zc3z04anonymousza32048ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2480)
	{
		{	/* Module/impuse.scm 44 */
			return BUNSPEC;
		}

	}



/* &lambda2047 */
	obj_t BGl_z62lambda2047z62zzmodule_impusez00(obj_t BgL_envz00_2481,
		obj_t BgL_oz00_2482, obj_t BgL_vz00_2483)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2482)))->BgL_checksumz00) =
				((obj_t) BgL_vz00_2483), BUNSPEC);
		}

	}



/* &lambda2046 */
	obj_t BGl_z62lambda2046z62zzmodule_impusez00(obj_t BgL_envz00_2484,
		obj_t BgL_oz00_2485)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2485)))->BgL_checksumz00);
		}

	}



/* &<@anonymous:2040> */
	obj_t BGl_z62zc3z04anonymousza32040ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2486)
	{
		{	/* Module/impuse.scm 44 */
			return BNIL;
		}

	}



/* &lambda2039 */
	obj_t BGl_z62lambda2039z62zzmodule_impusez00(obj_t BgL_envz00_2487,
		obj_t BgL_oz00_2488, obj_t BgL_vz00_2489)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2488)))->BgL_aliasesz00) =
				((obj_t) BgL_vz00_2489), BUNSPEC);
		}

	}



/* &lambda2038 */
	obj_t BGl_z62lambda2038z62zzmodule_impusez00(obj_t BgL_envz00_2490,
		obj_t BgL_oz00_2491)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2491)))->BgL_aliasesz00);
		}

	}



/* &<@anonymous:2032> */
	obj_t BGl_z62zc3z04anonymousza32032ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2492)
	{
		{	/* Module/impuse.scm 44 */
			return BNIL;
		}

	}



/* &lambda2031 */
	obj_t BGl_z62lambda2031z62zzmodule_impusez00(obj_t BgL_envz00_2493,
		obj_t BgL_oz00_2494, obj_t BgL_vz00_2495)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2494)))->BgL_varsz00) =
				((obj_t) BgL_vz00_2495), BUNSPEC);
		}

	}



/* &lambda2030 */
	obj_t BGl_z62lambda2030z62zzmodule_impusez00(obj_t BgL_envz00_2496,
		obj_t BgL_oz00_2497)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2497)))->BgL_varsz00);
		}

	}



/* &<@anonymous:2024> */
	obj_t BGl_z62zc3z04anonymousza32024ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2498)
	{
		{	/* Module/impuse.scm 44 */
			return CNST_TABLE_REF(0);
		}

	}



/* &lambda2023 */
	obj_t BGl_z62lambda2023z62zzmodule_impusez00(obj_t BgL_envz00_2499,
		obj_t BgL_oz00_2500, obj_t BgL_vz00_2501)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2500)))->BgL_modez00) = ((obj_t)
					((obj_t) BgL_vz00_2501)), BUNSPEC);
		}

	}



/* &lambda2022 */
	obj_t BGl_z62lambda2022z62zzmodule_impusez00(obj_t BgL_envz00_2502,
		obj_t BgL_oz00_2503)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2503)))->BgL_modez00);
		}

	}



/* &<@anonymous:2017> */
	obj_t BGl_z62zc3z04anonymousza32017ze3ze5zzmodule_impusez00(obj_t
		BgL_envz00_2504)
	{
		{	/* Module/impuse.scm 44 */
			return BINT(-1L);
		}

	}



/* &lambda2016 */
	obj_t BGl_z62lambda2016z62zzmodule_impusez00(obj_t BgL_envz00_2505,
		obj_t BgL_oz00_2506, obj_t BgL_vz00_2507)
	{
		{	/* Module/impuse.scm 44 */
			{	/* Module/impuse.scm 44 */
				long BgL_vz00_2568;

				BgL_vz00_2568 = (long) CINT(BgL_vz00_2507);
				return
					((((BgL_importz00_bglt) COBJECT(
								((BgL_importz00_bglt) BgL_oz00_2506)))->BgL_numberz00) =
					((long) BgL_vz00_2568), BUNSPEC);
		}}

	}



/* &lambda2015 */
	obj_t BGl_z62lambda2015z62zzmodule_impusez00(obj_t BgL_envz00_2508,
		obj_t BgL_oz00_2509)
	{
		{	/* Module/impuse.scm 44 */
			return
				BINT(
				(((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2509)))->BgL_numberz00));
		}

	}



/* &lambda2010 */
	obj_t BGl_z62lambda2010z62zzmodule_impusez00(obj_t BgL_envz00_2510,
		obj_t BgL_oz00_2511, obj_t BgL_vz00_2512)
	{
		{	/* Module/impuse.scm 44 */
			return
				((((BgL_importz00_bglt) COBJECT(
							((BgL_importz00_bglt) BgL_oz00_2511)))->BgL_modulez00) = ((obj_t)
					((obj_t) BgL_vz00_2512)), BUNSPEC);
		}

	}



/* &lambda2009 */
	obj_t BGl_z62lambda2009z62zzmodule_impusez00(obj_t BgL_envz00_2513,
		obj_t BgL_oz00_2514)
	{
		{	/* Module/impuse.scm 44 */
			return
				(((BgL_importz00_bglt) COBJECT(
						((BgL_importz00_bglt) BgL_oz00_2514)))->BgL_modulez00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzmodule_impusez00(void)
	{
		{	/* Module/impuse.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzread_readerz00(95801752L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzmodule_prototypez00(499400840L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzmodule_classz00(153808441L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzmodule_checksumz00(457474182L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zztools_prognz00(301998274L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzread_accessz00(11403588L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzread_inlinez00(500058826L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzast_findzd2gdefszd2(502577483L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzast_glozd2declzd2(374700232L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			BGl_modulezd2initializa7ationz75zzinit_mainz00(288050968L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
			return
				BGl_modulezd2initializa7ationz75zzexpand_epsz00(359337187L,
				BSTRING_TO_STRING(BGl_string2180z00zzmodule_impusez00));
		}

	}

#ifdef __cplusplus
}
#endif
