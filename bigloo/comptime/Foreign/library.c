/*===========================================================================*/
/*   (Foreign/library.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/library.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_LIBRARY_TYPE_DEFINITIONS
#define BGL_FOREIGN_LIBRARY_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_FOREIGN_LIBRARY_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzforeign_libraryz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzforeign_libraryz00(void);
	static obj_t BGl_genericzd2initzd2zzforeign_libraryz00(void);
	static obj_t BGl_za2registeredzd2identza2zd2zzforeign_libraryz00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zzforeign_libraryz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t
		BGl_z62preparezd2foreignzd2accessz12z70zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2definezd2inlinez62zzforeign_libraryz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzforeign_libraryz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzforeign_libraryz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00(obj_t);
	static obj_t
		BGl_z62registerzd2foreignzd2accesszd2identsz12za2zzforeign_libraryz00(obj_t,
		obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2protozd2inlinez00zzforeign_libraryz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzforeign_libraryz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_libraryz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_libraryz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_libraryz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2definezd2inlinez00zzforeign_libraryz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_preparezd2foreignzd2accessz12z12zzforeign_libraryz00(void);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62makezd2protozd2inlinez62zzforeign_libraryz00(obj_t,
		obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_STRING(BGl_string1380z00zzforeign_libraryz00,
		BgL_bgl_string1380za700za7za7f1409za7, "prepare-foreign-access!", 23);
	      DEFINE_STRING(BGl_string1381z00zzforeign_libraryz00,
		BgL_bgl_string1381za700za7za7f1410za7, "Can't find global", 17);
	      DEFINE_STRING(BGl_string1382z00zzforeign_libraryz00,
		BgL_bgl_string1382za700za7za7f1411za7, "foreign_library", 15);
	      DEFINE_STRING(BGl_string1383z00zzforeign_libraryz00,
		BgL_bgl_string1383za700za7za7f1412za7,
		"foreign export inline define-inline ", 36);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2foreignzd2accesszd2identsz12zd2envz12zzforeign_libraryz00,
		BgL_bgl_za762registerza7d2fo1413z00, va_generic_entry,
		BGl_z62registerzd2foreignzd2accesszd2identsz12za2zzforeign_libraryz00,
		BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_preparezd2foreignzd2accessz12zd2envzc0zzforeign_libraryz00,
		BgL_bgl_za762prepareza7d2for1414z00,
		BGl_z62preparezd2foreignzd2accessz12z70zzforeign_libraryz00, 0L, BUNSPEC,
		0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2protozd2inlinezd2envzd2zzforeign_libraryz00,
		BgL_bgl_za762makeza7d2protoza71415za7,
		BGl_z62makezd2protozd2inlinez62zzforeign_libraryz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2definezd2inlinezd2envzd2zzforeign_libraryz00,
		BgL_bgl_za762makeza7d2define1416z00,
		BGl_z62makezd2definezd2inlinez62zzforeign_libraryz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_libraryz00));
		     ADD_ROOT((void
				*) (&BGl_za2registeredzd2identza2zd2zzforeign_libraryz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long
		BgL_checksumz00_1629, char *BgL_fromz00_1630)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_libraryz00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_libraryz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_libraryz00();
					BGl_libraryzd2moduleszd2initz00zzforeign_libraryz00();
					BGl_cnstzd2initzd2zzforeign_libraryz00();
					BGl_importedzd2moduleszd2initz00zzforeign_libraryz00();
					return BGl_toplevelzd2initzd2zzforeign_libraryz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"foreign_library");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_library");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_library");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"foreign_library");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "foreign_library");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"foreign_library");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"foreign_library");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"foreign_library");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"foreign_library");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 15 */
			{	/* Foreign/library.scm 15 */
				obj_t BgL_cportz00_1618;

				{	/* Foreign/library.scm 15 */
					obj_t BgL_stringz00_1625;

					BgL_stringz00_1625 = BGl_string1383z00zzforeign_libraryz00;
					{	/* Foreign/library.scm 15 */
						obj_t BgL_startz00_1626;

						BgL_startz00_1626 = BINT(0L);
						{	/* Foreign/library.scm 15 */
							obj_t BgL_endz00_1627;

							BgL_endz00_1627 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1625)));
							{	/* Foreign/library.scm 15 */

								BgL_cportz00_1618 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1625, BgL_startz00_1626, BgL_endz00_1627);
				}}}}
				{
					long BgL_iz00_1619;

					BgL_iz00_1619 = 3L;
				BgL_loopz00_1620:
					if ((BgL_iz00_1619 == -1L))
						{	/* Foreign/library.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Foreign/library.scm 15 */
							{	/* Foreign/library.scm 15 */
								obj_t BgL_arg1408z00_1621;

								{	/* Foreign/library.scm 15 */

									{	/* Foreign/library.scm 15 */
										obj_t BgL_locationz00_1623;

										BgL_locationz00_1623 = BBOOL(((bool_t) 0));
										{	/* Foreign/library.scm 15 */

											BgL_arg1408z00_1621 =
												BGl_readz00zz__readerz00(BgL_cportz00_1618,
												BgL_locationz00_1623);
										}
									}
								}
								{	/* Foreign/library.scm 15 */
									int BgL_tmpz00_1657;

									BgL_tmpz00_1657 = (int) (BgL_iz00_1619);
									CNST_TABLE_SET(BgL_tmpz00_1657, BgL_arg1408z00_1621);
							}}
							{	/* Foreign/library.scm 15 */
								int BgL_auxz00_1624;

								BgL_auxz00_1624 = (int) ((BgL_iz00_1619 - 1L));
								{
									long BgL_iz00_1662;

									BgL_iz00_1662 = (long) (BgL_auxz00_1624);
									BgL_iz00_1619 = BgL_iz00_1662;
									goto BgL_loopz00_1620;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 15 */
			return (BGl_za2registeredzd2identza2zd2zzforeign_libraryz00 =
				BNIL, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzforeign_libraryz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1348;

				BgL_headz00_1348 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1349;
					obj_t BgL_tailz00_1350;

					BgL_prevz00_1349 = BgL_headz00_1348;
					BgL_tailz00_1350 = BgL_l1z00_1;
				BgL_loopz00_1351:
					if (PAIRP(BgL_tailz00_1350))
						{
							obj_t BgL_newzd2prevzd2_1353;

							BgL_newzd2prevzd2_1353 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1350), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1349, BgL_newzd2prevzd2_1353);
							{
								obj_t BgL_tailz00_1672;
								obj_t BgL_prevz00_1671;

								BgL_prevz00_1671 = BgL_newzd2prevzd2_1353;
								BgL_tailz00_1672 = CDR(BgL_tailz00_1350);
								BgL_tailz00_1350 = BgL_tailz00_1672;
								BgL_prevz00_1349 = BgL_prevz00_1671;
								goto BgL_loopz00_1351;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1348);
				}
			}
		}

	}



/* make-define-inline */
	BGL_EXPORTED_DEF obj_t BGl_makezd2definezd2inlinez00zzforeign_libraryz00(obj_t
		BgL_protoz00_3, obj_t BgL_bodyz00_4)
	{
		{	/* Foreign/library.scm 37 */
			{	/* Foreign/library.scm 39 */
				obj_t BgL_arg1227z00_1357;

				{	/* Foreign/library.scm 39 */
					obj_t BgL_arg1228z00_1358;

					BgL_arg1228z00_1358 = MAKE_YOUNG_PAIR(BgL_bodyz00_4, BNIL);
					BgL_arg1227z00_1357 =
						MAKE_YOUNG_PAIR(BgL_protoz00_3, BgL_arg1228z00_1358);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1227z00_1357);
			}
		}

	}



/* &make-define-inline */
	obj_t BGl_z62makezd2definezd2inlinez62zzforeign_libraryz00(obj_t
		BgL_envz00_1610, obj_t BgL_protoz00_1611, obj_t BgL_bodyz00_1612)
	{
		{	/* Foreign/library.scm 37 */
			return
				BGl_makezd2definezd2inlinez00zzforeign_libraryz00(BgL_protoz00_1611,
				BgL_bodyz00_1612);
		}

	}



/* make-proto-inline */
	BGL_EXPORTED_DEF obj_t BGl_makezd2protozd2inlinez00zzforeign_libraryz00(obj_t
		BgL_protoz00_5)
	{
		{	/* Foreign/library.scm 45 */
			{	/* Foreign/library.scm 47 */
				obj_t BgL_arg1232z00_1362;

				BgL_arg1232z00_1362 =
					BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(BgL_protoz00_5, BNIL);
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg1232z00_1362);
			}
		}

	}



/* &make-proto-inline */
	obj_t BGl_z62makezd2protozd2inlinez62zzforeign_libraryz00(obj_t
		BgL_envz00_1613, obj_t BgL_protoz00_1614)
	{
		{	/* Foreign/library.scm 45 */
			return
				BGl_makezd2protozd2inlinez00zzforeign_libraryz00(BgL_protoz00_1614);
		}

	}



/* register-foreign-access-idents! */
	BGL_EXPORTED_DEF obj_t
		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00(obj_t
		BgL_idsz00_6)
	{
		{	/* Foreign/library.scm 58 */
			return (BGl_za2registeredzd2identza2zd2zzforeign_libraryz00 =
				BGl_appendzd221011zd2zzforeign_libraryz00(BgL_idsz00_6,
					BGl_za2registeredzd2identza2zd2zzforeign_libraryz00), BUNSPEC);
		}

	}



/* &register-foreign-access-idents! */
	obj_t
		BGl_z62registerzd2foreignzd2accesszd2identsz12za2zzforeign_libraryz00(obj_t
		BgL_envz00_1615, obj_t BgL_idsz00_1616)
	{
		{	/* Foreign/library.scm 58 */
			return
				BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
				(BgL_idsz00_1616);
		}

	}



/* prepare-foreign-access! */
	BGL_EXPORTED_DEF obj_t
		BGl_preparezd2foreignzd2accessz12z12zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 75 */
			{
				obj_t BgL_l1219z00_1364;

				BgL_l1219z00_1364 = BGl_za2registeredzd2identza2zd2zzforeign_libraryz00;
			BgL_zc3z04anonymousza31233ze3z87_1365:
				if (PAIRP(BgL_l1219z00_1364))
					{	/* Foreign/library.scm 76 */
						{	/* Foreign/library.scm 77 */
							obj_t BgL_idz00_1367;

							BgL_idz00_1367 = CAR(BgL_l1219z00_1364);
							{	/* Foreign/library.scm 77 */
								obj_t BgL_gz00_1368;

								BgL_gz00_1368 =
									BGl_findzd2globalzd2zzast_envz00(BgL_idz00_1367, BNIL);
								{	/* Foreign/library.scm 78 */
									bool_t BgL_test1421z00_1690;

									{	/* Foreign/library.scm 78 */
										obj_t BgL_classz00_1537;

										BgL_classz00_1537 = BGl_globalz00zzast_varz00;
										if (BGL_OBJECTP(BgL_gz00_1368))
											{	/* Foreign/library.scm 78 */
												BgL_objectz00_bglt BgL_arg1807z00_1539;

												BgL_arg1807z00_1539 =
													(BgL_objectz00_bglt) (BgL_gz00_1368);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Foreign/library.scm 78 */
														long BgL_idxz00_1545;

														BgL_idxz00_1545 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1539);
														BgL_test1421z00_1690 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_1545 + 2L)) == BgL_classz00_1537);
													}
												else
													{	/* Foreign/library.scm 78 */
														bool_t BgL_res1378z00_1570;

														{	/* Foreign/library.scm 78 */
															obj_t BgL_oclassz00_1553;

															{	/* Foreign/library.scm 78 */
																obj_t BgL_arg1815z00_1561;
																long BgL_arg1816z00_1562;

																BgL_arg1815z00_1561 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Foreign/library.scm 78 */
																	long BgL_arg1817z00_1563;

																	BgL_arg1817z00_1563 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1539);
																	BgL_arg1816z00_1562 =
																		(BgL_arg1817z00_1563 - OBJECT_TYPE);
																}
																BgL_oclassz00_1553 =
																	VECTOR_REF(BgL_arg1815z00_1561,
																	BgL_arg1816z00_1562);
															}
															{	/* Foreign/library.scm 78 */
																bool_t BgL__ortest_1115z00_1554;

																BgL__ortest_1115z00_1554 =
																	(BgL_classz00_1537 == BgL_oclassz00_1553);
																if (BgL__ortest_1115z00_1554)
																	{	/* Foreign/library.scm 78 */
																		BgL_res1378z00_1570 =
																			BgL__ortest_1115z00_1554;
																	}
																else
																	{	/* Foreign/library.scm 78 */
																		long BgL_odepthz00_1555;

																		{	/* Foreign/library.scm 78 */
																			obj_t BgL_arg1804z00_1556;

																			BgL_arg1804z00_1556 =
																				(BgL_oclassz00_1553);
																			BgL_odepthz00_1555 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_1556);
																		}
																		if ((2L < BgL_odepthz00_1555))
																			{	/* Foreign/library.scm 78 */
																				obj_t BgL_arg1802z00_1558;

																				{	/* Foreign/library.scm 78 */
																					obj_t BgL_arg1803z00_1559;

																					BgL_arg1803z00_1559 =
																						(BgL_oclassz00_1553);
																					BgL_arg1802z00_1558 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_1559, 2L);
																				}
																				BgL_res1378z00_1570 =
																					(BgL_arg1802z00_1558 ==
																					BgL_classz00_1537);
																			}
																		else
																			{	/* Foreign/library.scm 78 */
																				BgL_res1378z00_1570 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1421z00_1690 = BgL_res1378z00_1570;
													}
											}
										else
											{	/* Foreign/library.scm 78 */
												BgL_test1421z00_1690 = ((bool_t) 0);
											}
									}
									if (BgL_test1421z00_1690)
										{	/* Foreign/library.scm 78 */
											{	/* Foreign/library.scm 80 */
												obj_t BgL_vz00_1572;

												BgL_vz00_1572 = CNST_TABLE_REF(2);
												((((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_gz00_1368)))->
														BgL_importz00) = ((obj_t) BgL_vz00_1572), BUNSPEC);
											}
											{	/* Foreign/library.scm 82 */
												obj_t BgL_vz00_1574;

												BgL_vz00_1574 = CNST_TABLE_REF(3);
												((((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_gz00_1368)))->
														BgL_modulez00) = ((obj_t) BgL_vz00_1574), BUNSPEC);
											}
										}
									else
										{	/* Foreign/library.scm 78 */
											BGl_errorz00zz__errorz00
												(BGl_string1380z00zzforeign_libraryz00,
												BGl_string1381z00zzforeign_libraryz00, BgL_idz00_1367);
										}
								}
							}
						}
						{
							obj_t BgL_l1219z00_1720;

							BgL_l1219z00_1720 = CDR(BgL_l1219z00_1364);
							BgL_l1219z00_1364 = BgL_l1219z00_1720;
							goto BgL_zc3z04anonymousza31233ze3z87_1365;
						}
					}
				else
					{	/* Foreign/library.scm 76 */
						((bool_t) 1);
					}
			}
			return (BGl_za2registeredzd2identza2zd2zzforeign_libraryz00 =
				BNIL, BUNSPEC);
		}

	}



/* &prepare-foreign-access! */
	obj_t BGl_z62preparezd2foreignzd2accessz12z70zzforeign_libraryz00(obj_t
		BgL_envz00_1617)
	{
		{	/* Foreign/library.scm 75 */
			return BGl_preparezd2foreignzd2accessz12z12zzforeign_libraryz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_libraryz00(void)
	{
		{	/* Foreign/library.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1382z00zzforeign_libraryz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1382z00zzforeign_libraryz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1382z00zzforeign_libraryz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1382z00zzforeign_libraryz00));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1382z00zzforeign_libraryz00));
		}

	}

#ifdef __cplusplus
}
#endif
