/*===========================================================================*/
/*   (Foreign/access.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/access.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_ACCESS_TYPE_DEFINITIONS
#define BGL_FOREIGN_ACCESS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;


#endif													// BGL_FOREIGN_ACCESS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzforeign_accessz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzforeign_accessz00(void);
	static obj_t BGl_genericzd2initzd2zzforeign_accessz00(void);
	static obj_t BGl_objectzd2initzd2zzforeign_accessz00(void);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_methodzd2initzd2zzforeign_accessz00(void);
	static obj_t
		BGl_z62makezd2ctypezd2accessesz121050z70zzforeign_accessz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_accessz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_cstructz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_cpointerz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_cfunctionz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_copaquez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_cenumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_caliasz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_accessz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_accessz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_accessz00(void);
	static obj_t BGl_z62makezd2ctypezd2accessesz12z70zzforeign_accessz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00(BgL_typez00_bglt,
		BgL_typez00_bglt, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1062z00zzforeign_accessz00,
		BgL_bgl_za762makeza7d2ctypeza71065za7,
		BGl_z62makezd2ctypezd2accessesz121050z70zzforeign_accessz00, 0L, BUNSPEC,
		3);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
		BgL_bgl_za762makeza7d2ctypeza71066za7,
		BGl_z62makezd2ctypezd2accessesz12z70zzforeign_accessz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1063z00zzforeign_accessz00,
		BgL_bgl_string1063za700za7za7f1067za7, "make-ctype-accesses!1050", 24);
	      DEFINE_STRING(BGl_string1064z00zzforeign_accessz00,
		BgL_bgl_string1064za700za7za7f1068za7, "foreign_access", 14);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_accessz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzforeign_accessz00(long
		BgL_checksumz00_125, char *BgL_fromz00_126)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_accessz00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_accessz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_accessz00();
					BGl_libraryzd2moduleszd2initz00zzforeign_accessz00();
					BGl_importedzd2moduleszd2initz00zzforeign_accessz00();
					BGl_genericzd2initzd2zzforeign_accessz00();
					return BGl_toplevelzd2initzd2zzforeign_accessz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_accessz00(void)
	{
		{	/* Foreign/access.scm 19 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_access");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"foreign_access");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "foreign_access");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"foreign_access");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_access");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_accessz00(void)
	{
		{	/* Foreign/access.scm 19 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzforeign_accessz00(void)
	{
		{	/* Foreign/access.scm 19 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_accessz00(void)
	{
		{	/* Foreign/access.scm 19 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_accessz00(void)
	{
		{	/* Foreign/access.scm 19 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
				BGl_proc1062z00zzforeign_accessz00, BGl_typez00zztype_typez00,
				BGl_string1063z00zzforeign_accessz00);
		}

	}



/* &make-ctype-accesses!1050 */
	obj_t BGl_z62makezd2ctypezd2accessesz121050z70zzforeign_accessz00(obj_t
		BgL_envz00_115, obj_t BgL_whatz00_116, obj_t BgL_whoz00_117,
		obj_t BgL_locz00_118)
	{
		{	/* Foreign/access.scm 34 */
			return BNIL;
		}

	}



/* make-ctype-accesses! */
	BGL_EXPORTED_DEF obj_t
		BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00(BgL_typez00_bglt
		BgL_whatz00_3, BgL_typez00_bglt BgL_whoz00_4, obj_t BgL_locz00_5)
	{
		{	/* Foreign/access.scm 34 */
			{	/* Foreign/access.scm 34 */
				obj_t BgL_method1051z00_79;

				{	/* Foreign/access.scm 34 */
					obj_t BgL_res1061z00_113;

					{	/* Foreign/access.scm 34 */
						long BgL_objzd2classzd2numz00_84;

						BgL_objzd2classzd2numz00_84 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_whatz00_3));
						{	/* Foreign/access.scm 34 */
							obj_t BgL_arg1811z00_85;

							BgL_arg1811z00_85 =
								PROCEDURE_REF
								(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
								(int) (1L));
							{	/* Foreign/access.scm 34 */
								int BgL_offsetz00_88;

								BgL_offsetz00_88 = (int) (BgL_objzd2classzd2numz00_84);
								{	/* Foreign/access.scm 34 */
									long BgL_offsetz00_89;

									BgL_offsetz00_89 = ((long) (BgL_offsetz00_88) - OBJECT_TYPE);
									{	/* Foreign/access.scm 34 */
										long BgL_modz00_90;

										BgL_modz00_90 =
											(BgL_offsetz00_89 >> (int) ((long) ((int) (4L))));
										{	/* Foreign/access.scm 34 */
											long BgL_restz00_92;

											BgL_restz00_92 =
												(BgL_offsetz00_89 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Foreign/access.scm 34 */

												{	/* Foreign/access.scm 34 */
													obj_t BgL_bucketz00_94;

													BgL_bucketz00_94 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_85), BgL_modz00_90);
													BgL_res1061z00_113 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_94), BgL_restz00_92);
					}}}}}}}}
					BgL_method1051z00_79 = BgL_res1061z00_113;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1051z00_79,
					((obj_t) BgL_whatz00_3), ((obj_t) BgL_whoz00_4), BgL_locz00_5);
			}
		}

	}



/* &make-ctype-accesses! */
	obj_t BGl_z62makezd2ctypezd2accessesz12z70zzforeign_accessz00(obj_t
		BgL_envz00_119, obj_t BgL_whatz00_120, obj_t BgL_whoz00_121,
		obj_t BgL_locz00_122)
	{
		{	/* Foreign/access.scm 34 */
			return
				BGl_makezd2ctypezd2accessesz12z12zzforeign_accessz00(
				((BgL_typez00_bglt) BgL_whatz00_120),
				((BgL_typez00_bglt) BgL_whoz00_121), BgL_locz00_122);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_accessz00(void)
	{
		{	/* Foreign/access.scm 19 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_accessz00(void)
	{
		{	/* Foreign/access.scm 19 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1064z00zzforeign_accessz00));
			BGl_modulezd2initializa7ationz75zzforeign_caliasz00(237925119L,
				BSTRING_TO_STRING(BGl_string1064z00zzforeign_accessz00));
			BGl_modulezd2initializa7ationz75zzforeign_cenumz00(263357126L,
				BSTRING_TO_STRING(BGl_string1064z00zzforeign_accessz00));
			BGl_modulezd2initializa7ationz75zzforeign_copaquez00(499858556L,
				BSTRING_TO_STRING(BGl_string1064z00zzforeign_accessz00));
			BGl_modulezd2initializa7ationz75zzforeign_cfunctionz00(225725399L,
				BSTRING_TO_STRING(BGl_string1064z00zzforeign_accessz00));
			BGl_modulezd2initializa7ationz75zzforeign_cpointerz00(166831446L,
				BSTRING_TO_STRING(BGl_string1064z00zzforeign_accessz00));
			return
				BGl_modulezd2initializa7ationz75zzforeign_cstructz00(216464466L,
				BSTRING_TO_STRING(BGl_string1064z00zzforeign_accessz00));
		}

	}

#ifdef __cplusplus
}
#endif
