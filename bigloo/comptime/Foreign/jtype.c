/*===========================================================================*/
/*   (Foreign/jtype.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/jtype.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_JTYPE_TYPE_DEFINITIONS
#define BGL_FOREIGN_JTYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_jarrayz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_jarrayz00_bglt;


#endif													// BGL_FOREIGN_JTYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2idzd2zzforeign_jtypez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzforeign_jtypez00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2siza7ez17zzforeign_jtypez00(obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2occurrencezd2setz12z70zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2initzf3zd2setz12ze1zzforeign_jtypez00(BgL_typez00_bglt,
		bool_t);
	extern BgL_typez00_bglt BGl_declarezd2subtypez12zc0zztype_envz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2parentszd2setz12z70zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2tvectorzd2setz12z70zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2tvectorzb0zzforeign_jtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2classzd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzforeign_jtypez00(void);
	static obj_t BGl_z62jarrayzd2importzd2locationz62zzforeign_jtypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_jarrayzd2initzf3z21zzforeign_jtypez00(BgL_typez00_bglt);
	static obj_t BGl_z62jarrayzd2locationzd2setz12z70zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2siza7ezd2setz12zd7zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2classzb0zzforeign_jtypez00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzforeign_jtypez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t
		BGl_z62jarrayzd2pointedzd2tozd2byzd2setz12z70zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2jarrayzd2zzforeign_jtypez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		BgL_typez00_bglt);
	static obj_t
		BGl_z62jarrayzd2importzd2locationzd2setz12za2zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzforeign_jtypez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2locationzd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62zc3z04anonymousza31216ze3ze5zzforeign_jtypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2z42zd2setz12z50zzforeign_jtypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2pointedzd2tozd2byzd2setz12z12zzforeign_jtypez00
		(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2tvectorzd2zzforeign_jtypez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	static obj_t BGl_z62jarrayzd2aliaszd2setz12z70zzforeign_jtypez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2typedzd2formalz00zzast_identz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzforeign_jtypez00(void);
	BGL_EXPORTED_DEF obj_t BGl_jarrayz00zzforeign_jtypez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2parentszd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62jarrayzf3z91zzforeign_jtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2locationzd2zzforeign_jtypez00(BgL_typez00_bglt);
	static obj_t BGl_z62jarrayzd2locationzb0zzforeign_jtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2coercezd2toz00zzforeign_jtypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2tvectorzd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL int
		BGl_jarrayzd2occurrencezd2zzforeign_jtypez00(BgL_typez00_bglt);
	extern obj_t
		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2namezd2zzforeign_jtypez00(BgL_typez00_bglt);
	static obj_t BGl_z62jarrayzd2z42zf2zzforeign_jtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2aliaszd2zzforeign_jtypez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2magiczf3zd2setz12z83zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62makezd2jarrayzb0zzforeign_jtypez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2initzf3zd2setz12z83zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2coercezd2tozd2setz12zc0zzforeign_jtypez00(BgL_typez00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1209z62zzforeign_jtypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_jarrayzd2itemzd2typez00zzforeign_jtypez00(BgL_typez00_bglt);
	static obj_t BGl_z62jarrayzd2classzd2setz12z70zzforeign_jtypez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1213z62zzforeign_jtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2namezd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1217z62zzforeign_jtypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1226z62zzforeign_jtypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1227z62zzforeign_jtypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2magiczf3z43zzforeign_jtypez00(obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2coercezd2toz62zzforeign_jtypez00(obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2pointedzd2tozd2byzb0zzforeign_jtypez00(obj_t,
		obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_declarezd2jvmzd2typez12z12zzforeign_jtypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_jtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62jarrayzd2initzf3z43zzforeign_jtypez00(obj_t, obj_t);
	static obj_t BGl_z62makezd2ctypezd2accessesz121141z70zzforeign_jtypez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2coercezd2tozd2setz12za2zzforeign_jtypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2pointedzd2tozd2byzd2zzforeign_jtypez00(BgL_typez00_bglt);
	extern obj_t BGl_makezd2privatezd2sexpz00zzast_privatez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62jarrayzd2namezb0zzforeign_jtypez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzforeign_jtypez00(void);
	static obj_t BGl_z62jarrayzd2parentszb0zzforeign_jtypez00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62jarrayzd2itemzd2typez62zzforeign_jtypez00(obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_jtypez00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_jtypez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_jtypez00(void);
	static obj_t BGl_z62jarrayzd2namezd2setz12z70zzforeign_jtypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2z42z90zzforeign_jtypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2classzd2zzforeign_jtypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2occurrencezd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt,
		int);
	static obj_t BGl_z62jarrayzd2occurrencezb0zzforeign_jtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2siza7ez75zzforeign_jtypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2importzd2locationz00zzforeign_jtypez00(BgL_typez00_bglt);
	extern BgL_typez00_bglt BGl_declarezd2czd2aliasz12z12zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_jarrayzd2nilzd2zzforeign_jtypez00(void);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2aliaszb0zzforeign_jtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_jarrayzf3zf3zzforeign_jtypez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2parentszd2zzforeign_jtypez00(BgL_typez00_bglt);
	extern BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t, obj_t);
	static obj_t BGl_z62jarrayzd2idzb0zzforeign_jtypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2magiczf3zd2setz12ze1zzforeign_jtypez00(BgL_typez00_bglt,
		bool_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static BgL_typez00_bglt BGl_z62jarrayzd2nilzb0zzforeign_jtypez00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_jarrayzd2magiczf3z21zzforeign_jtypez00(BgL_typez00_bglt);
	static obj_t BGl_z62jarrayzd2z42zd2setz12z32zzforeign_jtypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2aliaszd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2importzd2locationzd2setz12zc0zzforeign_jtypez00
		(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt
		BGl_z62declarezd2jvmzd2typez12z70zzforeign_jtypez00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_jarrayzd2siza7ezd2setz12zb5zzforeign_jtypez00(BgL_typez00_bglt, obj_t);
	static obj_t __cnst[37];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2importzd2locationzd2setz12zd2envz12zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2impo1747z00,
		BGl_z62jarrayzd2importzd2locationzd2setz12za2zzforeign_jtypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2aliaszd2setz12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2alia1748z00,
		BGl_z62jarrayzd2aliaszd2setz12z70zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2classzd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2clas1749z00,
		BGl_z62jarrayzd2classzb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2locationzd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2loca1750z00,
		BGl_z62jarrayzd2locationzb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2parentszd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2pare1751z00,
		BGl_z62jarrayzd2parentszb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2coercezd2tozd2envzd2zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2coer1752z00,
		BGl_z62jarrayzd2coercezd2toz62zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2tvectorzd2setz12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2tvec1753z00,
		BGl_z62jarrayzd2tvectorzd2setz12z70zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2occurrencezd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2occu1754z00,
		BGl_z62jarrayzd2occurrencezb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2namezd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2name1755z00,
		BGl_z62jarrayzd2namezb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2initzf3zd2envzf3zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2init1756z00,
		BGl_z62jarrayzd2initzf3z43zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2locationzd2setz12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2loca1757z00,
		BGl_z62jarrayzd2locationzd2setz12z70zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2siza7ezd2setz12zd2envz67zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2siza7a1758za7,
		BGl_z62jarrayzd2siza7ezd2setz12zd7zzforeign_jtypez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1737z00zzforeign_jtypez00,
		BgL_bgl_string1737za700za7za7f1759za7, "make-ctype-accesses!", 20);
	      DEFINE_STRING(BGl_string1738z00zzforeign_jtypez00,
		BgL_bgl_string1738za700za7za7f1760za7, "", 0);
	      DEFINE_STRING(BGl_string1739z00zzforeign_jtypez00,
		BgL_bgl_string1739za700za7za7f1761za7, "foreign_jtype", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2idzd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2idza7b1762za7,
		BGl_z62jarrayzd2idzb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1730z00zzforeign_jtypez00,
		BgL_bgl_za762lambda1227za7621763z00, BGl_z62lambda1227z62zzforeign_jtypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1731z00zzforeign_jtypez00,
		BgL_bgl_za762lambda1226za7621764z00, BGl_z62lambda1226z62zzforeign_jtypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1732z00zzforeign_jtypez00,
		BgL_bgl_za762lambda1217za7621765z00, BGl_z62lambda1217z62zzforeign_jtypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1733z00zzforeign_jtypez00,
		BgL_bgl_za762za7c3za704anonymo1766za7,
		BGl_z62zc3z04anonymousza31216ze3ze5zzforeign_jtypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1740z00zzforeign_jtypez00,
		BgL_bgl_string1740za700za7za7f1767za7,
		"static ::int ::obj inline object cast instanceof o::obj vlength valloc len len::int vref define-inline vset! offset offset::int -set! -ref make- val int -length bool ->obj o _ foreign_jtype jarray item-type type ? coerce obj bigloo (obj) obj-> ",
		244);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1734z00zzforeign_jtypez00,
		BgL_bgl_za762lambda1213za7621768z00, BGl_z62lambda1213z62zzforeign_jtypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1735z00zzforeign_jtypez00,
		BgL_bgl_za762lambda1209za7621769z00, BGl_z62lambda1209z62zzforeign_jtypez00,
		0L, BUNSPEC, 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1736z00zzforeign_jtypez00,
		BgL_bgl_za762makeza7d2ctypeza71770za7,
		BGl_z62makezd2ctypezd2accessesz121141z70zzforeign_jtypez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2aliaszd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2alia1771z00,
		BGl_z62jarrayzd2aliaszb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2initzf3zd2setz12zd2envz33zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2init1772z00,
		BGl_z62jarrayzd2initzf3zd2setz12z83zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2coercezd2tozd2setz12zd2envz12zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2coer1773z00,
		BGl_z62jarrayzd2coercezd2tozd2setz12za2zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2namezd2setz12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2name1774z00,
		BGl_z62jarrayzd2namezd2setz12z70zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2magiczf3zd2envzf3zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2magi1775z00,
		BGl_z62jarrayzd2magiczf3z43zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2pointedzd2tozd2byzd2setz12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2poin1776z00,
		BGl_z62jarrayzd2pointedzd2tozd2byzd2setz12z70zzforeign_jtypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2jarrayzd2envz00zzforeign_jtypez00,
		BgL_bgl_za762makeza7d2jarray1777z00,
		BGl_z62makezd2jarrayzb0zzforeign_jtypez00, 0L, BUNSPEC, 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2importzd2locationzd2envzd2zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2impo1778z00,
		BGl_z62jarrayzd2importzd2locationz62zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2pointedzd2tozd2byzd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2poin1779z00,
		BGl_z62jarrayzd2pointedzd2tozd2byzb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2z42zd2setz12zd2envz82zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2za742za71780z00,
		BGl_z62jarrayzd2z42zd2setz12z32zzforeign_jtypez00, 0L, BUNSPEC, 2);
	extern obj_t BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2jvmzd2typez12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762declareza7d2jvm1781z00,
		BGl_z62declarezd2jvmzd2typez12z70zzforeign_jtypez00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzf3zd2envz21zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7f3za791za71782z00,
		BGl_z62jarrayzf3z91zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2occurrencezd2setz12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2occu1783z00,
		BGl_z62jarrayzd2occurrencezd2setz12z70zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2classzd2setz12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2clas1784z00,
		BGl_z62jarrayzd2classzd2setz12z70zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2magiczf3zd2setz12zd2envz33zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2magi1785z00,
		BGl_z62jarrayzd2magiczf3zd2setz12z83zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2siza7ezd2envza7zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2siza7a1786za7,
		BGl_z62jarrayzd2siza7ez17zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2tvectorzd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2tvec1787z00,
		BGl_z62jarrayzd2tvectorzb0zzforeign_jtypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2nilzd2envz00zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2nilza71788za7,
		BGl_z62jarrayzd2nilzb0zzforeign_jtypez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_jarrayzd2z42zd2envz42zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2za742za71789z00,
		BGl_z62jarrayzd2z42zf2zzforeign_jtypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2parentszd2setz12zd2envzc0zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2pare1790z00,
		BGl_z62jarrayzd2parentszd2setz12z70zzforeign_jtypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_jarrayzd2itemzd2typezd2envzd2zzforeign_jtypez00,
		BgL_bgl_za762jarrayza7d2item1791z00,
		BGl_z62jarrayzd2itemzd2typez62zzforeign_jtypez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_jtypez00));
		     ADD_ROOT((void *) (&BGl_jarrayz00zzforeign_jtypez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzforeign_jtypez00(long
		BgL_checksumz00_1520, char *BgL_fromz00_1521)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_jtypez00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_jtypez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_jtypez00();
					BGl_libraryzd2moduleszd2initz00zzforeign_jtypez00();
					BGl_cnstzd2initzd2zzforeign_jtypez00();
					BGl_importedzd2moduleszd2initz00zzforeign_jtypez00();
					BGl_objectzd2initzd2zzforeign_jtypez00();
					BGl_methodzd2initzd2zzforeign_jtypez00();
					return BGl_toplevelzd2initzd2zzforeign_jtypez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_jtype");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"foreign_jtype");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_jtype");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"foreign_jtype");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"foreign_jtype");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"foreign_jtype");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"foreign_jtype");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"foreign_jtype");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"foreign_jtype");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.scm 17 */
			{	/* Foreign/jtype.scm 17 */
				obj_t BgL_cportz00_1278;

				{	/* Foreign/jtype.scm 17 */
					obj_t BgL_stringz00_1285;

					BgL_stringz00_1285 = BGl_string1740z00zzforeign_jtypez00;
					{	/* Foreign/jtype.scm 17 */
						obj_t BgL_startz00_1286;

						BgL_startz00_1286 = BINT(0L);
						{	/* Foreign/jtype.scm 17 */
							obj_t BgL_endz00_1287;

							BgL_endz00_1287 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1285)));
							{	/* Foreign/jtype.scm 17 */

								BgL_cportz00_1278 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1285, BgL_startz00_1286, BgL_endz00_1287);
				}}}}
				{
					long BgL_iz00_1279;

					BgL_iz00_1279 = 36L;
				BgL_loopz00_1280:
					if ((BgL_iz00_1279 == -1L))
						{	/* Foreign/jtype.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Foreign/jtype.scm 17 */
							{	/* Foreign/jtype.scm 17 */
								obj_t BgL_arg1746z00_1281;

								{	/* Foreign/jtype.scm 17 */

									{	/* Foreign/jtype.scm 17 */
										obj_t BgL_locationz00_1283;

										BgL_locationz00_1283 = BBOOL(((bool_t) 0));
										{	/* Foreign/jtype.scm 17 */

											BgL_arg1746z00_1281 =
												BGl_readz00zz__readerz00(BgL_cportz00_1278,
												BgL_locationz00_1283);
										}
									}
								}
								{	/* Foreign/jtype.scm 17 */
									int BgL_tmpz00_1550;

									BgL_tmpz00_1550 = (int) (BgL_iz00_1279);
									CNST_TABLE_SET(BgL_tmpz00_1550, BgL_arg1746z00_1281);
							}}
							{	/* Foreign/jtype.scm 17 */
								int BgL_auxz00_1284;

								BgL_auxz00_1284 = (int) ((BgL_iz00_1279 - 1L));
								{
									long BgL_iz00_1555;

									BgL_iz00_1555 = (long) (BgL_auxz00_1284);
									BgL_iz00_1279 = BgL_iz00_1555;
									goto BgL_loopz00_1280;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.scm 17 */
			return BUNSPEC;
		}

	}



/* make-jarray */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2jarrayzd2zzforeign_jtypez00(obj_t
		BgL_id1078z00_3, obj_t BgL_name1079z00_4, obj_t BgL_siza7e1080za7_5,
		obj_t BgL_class1081z00_6, obj_t BgL_coercezd2to1082zd2_7,
		obj_t BgL_parents1083z00_8, bool_t BgL_initzf31084zf3_9,
		bool_t BgL_magiczf31085zf3_10, obj_t BgL_z421086z42_11,
		obj_t BgL_alias1087z00_12, obj_t BgL_pointedzd2tozd2by1088z00_13,
		obj_t BgL_tvector1089z00_14, obj_t BgL_location1090z00_15,
		obj_t BgL_importzd2location1091zd2_16, int BgL_occurrence1092z00_17,
		BgL_typez00_bglt BgL_itemzd2type1093zd2_18)
	{
		{	/* Foreign/jtype.sch 51 */
			{	/* Foreign/jtype.sch 51 */
				BgL_typez00_bglt BgL_new1093z00_1289;

				{	/* Foreign/jtype.sch 51 */
					BgL_typez00_bglt BgL_tmp1091z00_1290;
					BgL_jarrayz00_bglt BgL_wide1092z00_1291;

					{
						BgL_typez00_bglt BgL_auxz00_1558;

						{	/* Foreign/jtype.sch 51 */
							BgL_typez00_bglt BgL_new1090z00_1292;

							BgL_new1090z00_1292 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Foreign/jtype.sch 51 */
								long BgL_arg1149z00_1293;

								BgL_arg1149z00_1293 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1090z00_1292),
									BgL_arg1149z00_1293);
							}
							{	/* Foreign/jtype.sch 51 */
								BgL_objectz00_bglt BgL_tmpz00_1563;

								BgL_tmpz00_1563 = ((BgL_objectz00_bglt) BgL_new1090z00_1292);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1563, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1090z00_1292);
							BgL_auxz00_1558 = BgL_new1090z00_1292;
						}
						BgL_tmp1091z00_1290 = ((BgL_typez00_bglt) BgL_auxz00_1558);
					}
					BgL_wide1092z00_1291 =
						((BgL_jarrayz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_jarrayz00_bgl))));
					{	/* Foreign/jtype.sch 51 */
						obj_t BgL_auxz00_1571;
						BgL_objectz00_bglt BgL_tmpz00_1569;

						BgL_auxz00_1571 = ((obj_t) BgL_wide1092z00_1291);
						BgL_tmpz00_1569 = ((BgL_objectz00_bglt) BgL_tmp1091z00_1290);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1569, BgL_auxz00_1571);
					}
					((BgL_objectz00_bglt) BgL_tmp1091z00_1290);
					{	/* Foreign/jtype.sch 51 */
						long BgL_arg1148z00_1294;

						BgL_arg1148z00_1294 =
							BGL_CLASS_NUM(BGl_jarrayz00zzforeign_jtypez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1091z00_1290), BgL_arg1148z00_1294);
					}
					BgL_new1093z00_1289 = ((BgL_typez00_bglt) BgL_tmp1091z00_1290);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1093z00_1289)))->BgL_idz00) =
					((obj_t) BgL_id1078z00_3), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_namez00) =
					((obj_t) BgL_name1079z00_4), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1080za7_5), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_classz00) =
					((obj_t) BgL_class1081z00_6), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1082zd2_7), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_parentsz00) =
					((obj_t) BgL_parents1083z00_8), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31084zf3_9), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31085zf3_10), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_z42z42) =
					((obj_t) BgL_z421086z42_11), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_aliasz00) =
					((obj_t) BgL_alias1087z00_12), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1088z00_13), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1089z00_14), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_locationz00) =
					((obj_t) BgL_location1090z00_15), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1091zd2_16), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1093z00_1289)))->BgL_occurrencez00) =
					((int) BgL_occurrence1092z00_17), BUNSPEC);
				{
					BgL_jarrayz00_bglt BgL_auxz00_1611;

					{
						obj_t BgL_auxz00_1612;

						{	/* Foreign/jtype.sch 51 */
							BgL_objectz00_bglt BgL_tmpz00_1613;

							BgL_tmpz00_1613 = ((BgL_objectz00_bglt) BgL_new1093z00_1289);
							BgL_auxz00_1612 = BGL_OBJECT_WIDENING(BgL_tmpz00_1613);
						}
						BgL_auxz00_1611 = ((BgL_jarrayz00_bglt) BgL_auxz00_1612);
					}
					((((BgL_jarrayz00_bglt) COBJECT(BgL_auxz00_1611))->
							BgL_itemzd2typezd2) =
						((BgL_typez00_bglt) BgL_itemzd2type1093zd2_18), BUNSPEC);
				}
				return BgL_new1093z00_1289;
			}
		}

	}



/* &make-jarray */
	BgL_typez00_bglt BGl_z62makezd2jarrayzb0zzforeign_jtypez00(obj_t
		BgL_envz00_1136, obj_t BgL_id1078z00_1137, obj_t BgL_name1079z00_1138,
		obj_t BgL_siza7e1080za7_1139, obj_t BgL_class1081z00_1140,
		obj_t BgL_coercezd2to1082zd2_1141, obj_t BgL_parents1083z00_1142,
		obj_t BgL_initzf31084zf3_1143, obj_t BgL_magiczf31085zf3_1144,
		obj_t BgL_z421086z42_1145, obj_t BgL_alias1087z00_1146,
		obj_t BgL_pointedzd2tozd2by1088z00_1147, obj_t BgL_tvector1089z00_1148,
		obj_t BgL_location1090z00_1149, obj_t BgL_importzd2location1091zd2_1150,
		obj_t BgL_occurrence1092z00_1151, obj_t BgL_itemzd2type1093zd2_1152)
	{
		{	/* Foreign/jtype.sch 51 */
			return
				BGl_makezd2jarrayzd2zzforeign_jtypez00(BgL_id1078z00_1137,
				BgL_name1079z00_1138, BgL_siza7e1080za7_1139, BgL_class1081z00_1140,
				BgL_coercezd2to1082zd2_1141, BgL_parents1083z00_1142,
				CBOOL(BgL_initzf31084zf3_1143), CBOOL(BgL_magiczf31085zf3_1144),
				BgL_z421086z42_1145, BgL_alias1087z00_1146,
				BgL_pointedzd2tozd2by1088z00_1147, BgL_tvector1089z00_1148,
				BgL_location1090z00_1149, BgL_importzd2location1091zd2_1150,
				CINT(BgL_occurrence1092z00_1151),
				((BgL_typez00_bglt) BgL_itemzd2type1093zd2_1152));
		}

	}



/* jarray? */
	BGL_EXPORTED_DEF bool_t BGl_jarrayzf3zf3zzforeign_jtypez00(obj_t
		BgL_objz00_19)
	{
		{	/* Foreign/jtype.sch 52 */
			{	/* Foreign/jtype.sch 52 */
				obj_t BgL_classz00_1295;

				BgL_classz00_1295 = BGl_jarrayz00zzforeign_jtypez00;
				if (BGL_OBJECTP(BgL_objz00_19))
					{	/* Foreign/jtype.sch 52 */
						BgL_objectz00_bglt BgL_arg1807z00_1296;

						BgL_arg1807z00_1296 = (BgL_objectz00_bglt) (BgL_objz00_19);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Foreign/jtype.sch 52 */
								long BgL_idxz00_1297;

								BgL_idxz00_1297 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1296);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_1297 + 2L)) == BgL_classz00_1295);
							}
						else
							{	/* Foreign/jtype.sch 52 */
								bool_t BgL_res1723z00_1300;

								{	/* Foreign/jtype.sch 52 */
									obj_t BgL_oclassz00_1301;

									{	/* Foreign/jtype.sch 52 */
										obj_t BgL_arg1815z00_1302;
										long BgL_arg1816z00_1303;

										BgL_arg1815z00_1302 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Foreign/jtype.sch 52 */
											long BgL_arg1817z00_1304;

											BgL_arg1817z00_1304 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1296);
											BgL_arg1816z00_1303 = (BgL_arg1817z00_1304 - OBJECT_TYPE);
										}
										BgL_oclassz00_1301 =
											VECTOR_REF(BgL_arg1815z00_1302, BgL_arg1816z00_1303);
									}
									{	/* Foreign/jtype.sch 52 */
										bool_t BgL__ortest_1115z00_1305;

										BgL__ortest_1115z00_1305 =
											(BgL_classz00_1295 == BgL_oclassz00_1301);
										if (BgL__ortest_1115z00_1305)
											{	/* Foreign/jtype.sch 52 */
												BgL_res1723z00_1300 = BgL__ortest_1115z00_1305;
											}
										else
											{	/* Foreign/jtype.sch 52 */
												long BgL_odepthz00_1306;

												{	/* Foreign/jtype.sch 52 */
													obj_t BgL_arg1804z00_1307;

													BgL_arg1804z00_1307 = (BgL_oclassz00_1301);
													BgL_odepthz00_1306 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_1307);
												}
												if ((2L < BgL_odepthz00_1306))
													{	/* Foreign/jtype.sch 52 */
														obj_t BgL_arg1802z00_1308;

														{	/* Foreign/jtype.sch 52 */
															obj_t BgL_arg1803z00_1309;

															BgL_arg1803z00_1309 = (BgL_oclassz00_1301);
															BgL_arg1802z00_1308 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1309,
																2L);
														}
														BgL_res1723z00_1300 =
															(BgL_arg1802z00_1308 == BgL_classz00_1295);
													}
												else
													{	/* Foreign/jtype.sch 52 */
														BgL_res1723z00_1300 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1723z00_1300;
							}
					}
				else
					{	/* Foreign/jtype.sch 52 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &jarray? */
	obj_t BGl_z62jarrayzf3z91zzforeign_jtypez00(obj_t BgL_envz00_1153,
		obj_t BgL_objz00_1154)
	{
		{	/* Foreign/jtype.sch 52 */
			return BBOOL(BGl_jarrayzf3zf3zzforeign_jtypez00(BgL_objz00_1154));
		}

	}



/* jarray-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_jarrayzd2nilzd2zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.sch 53 */
			{	/* Foreign/jtype.sch 53 */
				obj_t BgL_classz00_974;

				BgL_classz00_974 = BGl_jarrayz00zzforeign_jtypez00;
				{	/* Foreign/jtype.sch 53 */
					obj_t BgL__ortest_1117z00_975;

					BgL__ortest_1117z00_975 = BGL_CLASS_NIL(BgL_classz00_974);
					if (CBOOL(BgL__ortest_1117z00_975))
						{	/* Foreign/jtype.sch 53 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_975);
						}
					else
						{	/* Foreign/jtype.sch 53 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_974));
						}
				}
			}
		}

	}



/* &jarray-nil */
	BgL_typez00_bglt BGl_z62jarrayzd2nilzb0zzforeign_jtypez00(obj_t
		BgL_envz00_1155)
	{
		{	/* Foreign/jtype.sch 53 */
			return BGl_jarrayzd2nilzd2zzforeign_jtypez00();
		}

	}



/* jarray-item-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_jarrayzd2itemzd2typez00zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_20)
	{
		{	/* Foreign/jtype.sch 54 */
			{
				BgL_jarrayz00_bglt BgL_auxz00_1654;

				{
					obj_t BgL_auxz00_1655;

					{	/* Foreign/jtype.sch 54 */
						BgL_objectz00_bglt BgL_tmpz00_1656;

						BgL_tmpz00_1656 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_1655 = BGL_OBJECT_WIDENING(BgL_tmpz00_1656);
					}
					BgL_auxz00_1654 = ((BgL_jarrayz00_bglt) BgL_auxz00_1655);
				}
				return
					(((BgL_jarrayz00_bglt) COBJECT(BgL_auxz00_1654))->BgL_itemzd2typezd2);
			}
		}

	}



/* &jarray-item-type */
	BgL_typez00_bglt BGl_z62jarrayzd2itemzd2typez62zzforeign_jtypez00(obj_t
		BgL_envz00_1156, obj_t BgL_oz00_1157)
	{
		{	/* Foreign/jtype.sch 54 */
			return
				BGl_jarrayzd2itemzd2typez00zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1157));
		}

	}



/* jarray-occurrence */
	BGL_EXPORTED_DEF int
		BGl_jarrayzd2occurrencezd2zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_23)
	{
		{	/* Foreign/jtype.sch 56 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_23)))->BgL_occurrencez00);
		}

	}



/* &jarray-occurrence */
	obj_t BGl_z62jarrayzd2occurrencezb0zzforeign_jtypez00(obj_t BgL_envz00_1158,
		obj_t BgL_oz00_1159)
	{
		{	/* Foreign/jtype.sch 56 */
			return
				BINT(BGl_jarrayzd2occurrencezd2zzforeign_jtypez00(
					((BgL_typez00_bglt) BgL_oz00_1159)));
		}

	}



/* jarray-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2occurrencezd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_24, int BgL_vz00_25)
	{
		{	/* Foreign/jtype.sch 57 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_24)))->BgL_occurrencez00) =
				((int) BgL_vz00_25), BUNSPEC);
		}

	}



/* &jarray-occurrence-set! */
	obj_t BGl_z62jarrayzd2occurrencezd2setz12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1160, obj_t BgL_oz00_1161, obj_t BgL_vz00_1162)
	{
		{	/* Foreign/jtype.sch 57 */
			return
				BGl_jarrayzd2occurrencezd2setz12z12zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1161), CINT(BgL_vz00_1162));
		}

	}



/* jarray-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2importzd2locationz00zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_26)
	{
		{	/* Foreign/jtype.sch 58 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_26)))->BgL_importzd2locationzd2);
		}

	}



/* &jarray-import-location */
	obj_t BGl_z62jarrayzd2importzd2locationz62zzforeign_jtypez00(obj_t
		BgL_envz00_1163, obj_t BgL_oz00_1164)
	{
		{	/* Foreign/jtype.sch 58 */
			return
				BGl_jarrayzd2importzd2locationz00zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1164));
		}

	}



/* jarray-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2importzd2locationzd2setz12zc0zzforeign_jtypez00
		(BgL_typez00_bglt BgL_oz00_27, obj_t BgL_vz00_28)
	{
		{	/* Foreign/jtype.sch 59 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_27)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_28), BUNSPEC);
		}

	}



/* &jarray-import-location-set! */
	obj_t BGl_z62jarrayzd2importzd2locationzd2setz12za2zzforeign_jtypez00(obj_t
		BgL_envz00_1165, obj_t BgL_oz00_1166, obj_t BgL_vz00_1167)
	{
		{	/* Foreign/jtype.sch 59 */
			return
				BGl_jarrayzd2importzd2locationzd2setz12zc0zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1166), BgL_vz00_1167);
		}

	}



/* jarray-location */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2locationzd2zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_29)
	{
		{	/* Foreign/jtype.sch 60 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_29)))->BgL_locationz00);
		}

	}



/* &jarray-location */
	obj_t BGl_z62jarrayzd2locationzb0zzforeign_jtypez00(obj_t BgL_envz00_1168,
		obj_t BgL_oz00_1169)
	{
		{	/* Foreign/jtype.sch 60 */
			return
				BGl_jarrayzd2locationzd2zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1169));
		}

	}



/* jarray-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2locationzd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_30, obj_t BgL_vz00_31)
	{
		{	/* Foreign/jtype.sch 61 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_30)))->BgL_locationz00) =
				((obj_t) BgL_vz00_31), BUNSPEC);
		}

	}



/* &jarray-location-set! */
	obj_t BGl_z62jarrayzd2locationzd2setz12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1170, obj_t BgL_oz00_1171, obj_t BgL_vz00_1172)
	{
		{	/* Foreign/jtype.sch 61 */
			return
				BGl_jarrayzd2locationzd2setz12z12zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1171), BgL_vz00_1172);
		}

	}



/* jarray-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2tvectorzd2zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_32)
	{
		{	/* Foreign/jtype.sch 62 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_32)))->BgL_tvectorz00);
		}

	}



/* &jarray-tvector */
	obj_t BGl_z62jarrayzd2tvectorzb0zzforeign_jtypez00(obj_t BgL_envz00_1173,
		obj_t BgL_oz00_1174)
	{
		{	/* Foreign/jtype.sch 62 */
			return
				BGl_jarrayzd2tvectorzd2zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1174));
		}

	}



/* jarray-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2tvectorzd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_33, obj_t BgL_vz00_34)
	{
		{	/* Foreign/jtype.sch 63 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_33)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_34), BUNSPEC);
		}

	}



/* &jarray-tvector-set! */
	obj_t BGl_z62jarrayzd2tvectorzd2setz12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1175, obj_t BgL_oz00_1176, obj_t BgL_vz00_1177)
	{
		{	/* Foreign/jtype.sch 63 */
			return
				BGl_jarrayzd2tvectorzd2setz12z12zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1176), BgL_vz00_1177);
		}

	}



/* jarray-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2pointedzd2tozd2byzd2zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_35)
	{
		{	/* Foreign/jtype.sch 64 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_35)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &jarray-pointed-to-by */
	obj_t BGl_z62jarrayzd2pointedzd2tozd2byzb0zzforeign_jtypez00(obj_t
		BgL_envz00_1178, obj_t BgL_oz00_1179)
	{
		{	/* Foreign/jtype.sch 64 */
			return
				BGl_jarrayzd2pointedzd2tozd2byzd2zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1179));
		}

	}



/* jarray-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2pointedzd2tozd2byzd2setz12z12zzforeign_jtypez00
		(BgL_typez00_bglt BgL_oz00_36, obj_t BgL_vz00_37)
	{
		{	/* Foreign/jtype.sch 65 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_36)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_37), BUNSPEC);
		}

	}



/* &jarray-pointed-to-by-set! */
	obj_t BGl_z62jarrayzd2pointedzd2tozd2byzd2setz12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1180, obj_t BgL_oz00_1181, obj_t BgL_vz00_1182)
	{
		{	/* Foreign/jtype.sch 65 */
			return
				BGl_jarrayzd2pointedzd2tozd2byzd2setz12z12zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1181), BgL_vz00_1182);
		}

	}



/* jarray-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2aliaszd2zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_38)
	{
		{	/* Foreign/jtype.sch 66 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_38)))->BgL_aliasz00);
		}

	}



/* &jarray-alias */
	obj_t BGl_z62jarrayzd2aliaszb0zzforeign_jtypez00(obj_t BgL_envz00_1183,
		obj_t BgL_oz00_1184)
	{
		{	/* Foreign/jtype.sch 66 */
			return
				BGl_jarrayzd2aliaszd2zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1184));
		}

	}



/* jarray-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2aliaszd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_39, obj_t BgL_vz00_40)
	{
		{	/* Foreign/jtype.sch 67 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_39)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_40), BUNSPEC);
		}

	}



/* &jarray-alias-set! */
	obj_t BGl_z62jarrayzd2aliaszd2setz12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1185, obj_t BgL_oz00_1186, obj_t BgL_vz00_1187)
	{
		{	/* Foreign/jtype.sch 67 */
			return
				BGl_jarrayzd2aliaszd2setz12z12zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1186), BgL_vz00_1187);
		}

	}



/* jarray-$ */
	BGL_EXPORTED_DEF obj_t BGl_jarrayzd2z42z90zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_41)
	{
		{	/* Foreign/jtype.sch 68 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_41)))->BgL_z42z42);
		}

	}



/* &jarray-$ */
	obj_t BGl_z62jarrayzd2z42zf2zzforeign_jtypez00(obj_t BgL_envz00_1188,
		obj_t BgL_oz00_1189)
	{
		{	/* Foreign/jtype.sch 68 */
			return
				BGl_jarrayzd2z42z90zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1189));
		}

	}



/* jarray-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2z42zd2setz12z50zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_42,
		obj_t BgL_vz00_43)
	{
		{	/* Foreign/jtype.sch 69 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_42)))->BgL_z42z42) =
				((obj_t) BgL_vz00_43), BUNSPEC);
		}

	}



/* &jarray-$-set! */
	obj_t BGl_z62jarrayzd2z42zd2setz12z32zzforeign_jtypez00(obj_t BgL_envz00_1190,
		obj_t BgL_oz00_1191, obj_t BgL_vz00_1192)
	{
		{	/* Foreign/jtype.sch 69 */
			return
				BGl_jarrayzd2z42zd2setz12z50zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1191), BgL_vz00_1192);
		}

	}



/* jarray-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_jarrayzd2magiczf3z21zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_44)
	{
		{	/* Foreign/jtype.sch 70 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_44)))->BgL_magiczf3zf3);
		}

	}



/* &jarray-magic? */
	obj_t BGl_z62jarrayzd2magiczf3z43zzforeign_jtypez00(obj_t BgL_envz00_1193,
		obj_t BgL_oz00_1194)
	{
		{	/* Foreign/jtype.sch 70 */
			return
				BBOOL(BGl_jarrayzd2magiczf3z21zzforeign_jtypez00(
					((BgL_typez00_bglt) BgL_oz00_1194)));
		}

	}



/* jarray-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2magiczf3zd2setz12ze1zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_45, bool_t BgL_vz00_46)
	{
		{	/* Foreign/jtype.sch 71 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_45)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_46), BUNSPEC);
		}

	}



/* &jarray-magic?-set! */
	obj_t BGl_z62jarrayzd2magiczf3zd2setz12z83zzforeign_jtypez00(obj_t
		BgL_envz00_1195, obj_t BgL_oz00_1196, obj_t BgL_vz00_1197)
	{
		{	/* Foreign/jtype.sch 71 */
			return
				BGl_jarrayzd2magiczf3zd2setz12ze1zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1196), CBOOL(BgL_vz00_1197));
		}

	}



/* jarray-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_jarrayzd2initzf3z21zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_47)
	{
		{	/* Foreign/jtype.sch 72 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_47)))->BgL_initzf3zf3);
		}

	}



/* &jarray-init? */
	obj_t BGl_z62jarrayzd2initzf3z43zzforeign_jtypez00(obj_t BgL_envz00_1198,
		obj_t BgL_oz00_1199)
	{
		{	/* Foreign/jtype.sch 72 */
			return
				BBOOL(BGl_jarrayzd2initzf3z21zzforeign_jtypez00(
					((BgL_typez00_bglt) BgL_oz00_1199)));
		}

	}



/* jarray-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2initzf3zd2setz12ze1zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_48, bool_t BgL_vz00_49)
	{
		{	/* Foreign/jtype.sch 73 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_48)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_49), BUNSPEC);
		}

	}



/* &jarray-init?-set! */
	obj_t BGl_z62jarrayzd2initzf3zd2setz12z83zzforeign_jtypez00(obj_t
		BgL_envz00_1200, obj_t BgL_oz00_1201, obj_t BgL_vz00_1202)
	{
		{	/* Foreign/jtype.sch 73 */
			return
				BGl_jarrayzd2initzf3zd2setz12ze1zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1201), CBOOL(BgL_vz00_1202));
		}

	}



/* jarray-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2parentszd2zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_50)
	{
		{	/* Foreign/jtype.sch 74 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_50)))->BgL_parentsz00);
		}

	}



/* &jarray-parents */
	obj_t BGl_z62jarrayzd2parentszb0zzforeign_jtypez00(obj_t BgL_envz00_1203,
		obj_t BgL_oz00_1204)
	{
		{	/* Foreign/jtype.sch 74 */
			return
				BGl_jarrayzd2parentszd2zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1204));
		}

	}



/* jarray-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2parentszd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_51, obj_t BgL_vz00_52)
	{
		{	/* Foreign/jtype.sch 75 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_51)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_52), BUNSPEC);
		}

	}



/* &jarray-parents-set! */
	obj_t BGl_z62jarrayzd2parentszd2setz12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1205, obj_t BgL_oz00_1206, obj_t BgL_vz00_1207)
	{
		{	/* Foreign/jtype.sch 75 */
			return
				BGl_jarrayzd2parentszd2setz12z12zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1206), BgL_vz00_1207);
		}

	}



/* jarray-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2coercezd2toz00zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_53)
	{
		{	/* Foreign/jtype.sch 76 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_53)))->BgL_coercezd2tozd2);
		}

	}



/* &jarray-coerce-to */
	obj_t BGl_z62jarrayzd2coercezd2toz62zzforeign_jtypez00(obj_t BgL_envz00_1208,
		obj_t BgL_oz00_1209)
	{
		{	/* Foreign/jtype.sch 76 */
			return
				BGl_jarrayzd2coercezd2toz00zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1209));
		}

	}



/* jarray-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2coercezd2tozd2setz12zc0zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_54, obj_t BgL_vz00_55)
	{
		{	/* Foreign/jtype.sch 77 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_54)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_55), BUNSPEC);
		}

	}



/* &jarray-coerce-to-set! */
	obj_t BGl_z62jarrayzd2coercezd2tozd2setz12za2zzforeign_jtypez00(obj_t
		BgL_envz00_1210, obj_t BgL_oz00_1211, obj_t BgL_vz00_1212)
	{
		{	/* Foreign/jtype.sch 77 */
			return
				BGl_jarrayzd2coercezd2tozd2setz12zc0zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1211), BgL_vz00_1212);
		}

	}



/* jarray-class */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2classzd2zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_56)
	{
		{	/* Foreign/jtype.sch 78 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_56)))->BgL_classz00);
		}

	}



/* &jarray-class */
	obj_t BGl_z62jarrayzd2classzb0zzforeign_jtypez00(obj_t BgL_envz00_1213,
		obj_t BgL_oz00_1214)
	{
		{	/* Foreign/jtype.sch 78 */
			return
				BGl_jarrayzd2classzd2zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1214));
		}

	}



/* jarray-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2classzd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_57, obj_t BgL_vz00_58)
	{
		{	/* Foreign/jtype.sch 79 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_57)))->BgL_classz00) =
				((obj_t) BgL_vz00_58), BUNSPEC);
		}

	}



/* &jarray-class-set! */
	obj_t BGl_z62jarrayzd2classzd2setz12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1215, obj_t BgL_oz00_1216, obj_t BgL_vz00_1217)
	{
		{	/* Foreign/jtype.sch 79 */
			return
				BGl_jarrayzd2classzd2setz12z12zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1216), BgL_vz00_1217);
		}

	}



/* jarray-size */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2siza7ez75zzforeign_jtypez00(BgL_typez00_bglt BgL_oz00_59)
	{
		{	/* Foreign/jtype.sch 80 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_59)))->BgL_siza7eza7);
		}

	}



/* &jarray-size */
	obj_t BGl_z62jarrayzd2siza7ez17zzforeign_jtypez00(obj_t BgL_envz00_1218,
		obj_t BgL_oz00_1219)
	{
		{	/* Foreign/jtype.sch 80 */
			return
				BGl_jarrayzd2siza7ez75zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1219));
		}

	}



/* jarray-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2siza7ezd2setz12zb5zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_60, obj_t BgL_vz00_61)
	{
		{	/* Foreign/jtype.sch 81 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_60)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_61), BUNSPEC);
		}

	}



/* &jarray-size-set! */
	obj_t BGl_z62jarrayzd2siza7ezd2setz12zd7zzforeign_jtypez00(obj_t
		BgL_envz00_1220, obj_t BgL_oz00_1221, obj_t BgL_vz00_1222)
	{
		{	/* Foreign/jtype.sch 81 */
			return
				BGl_jarrayzd2siza7ezd2setz12zb5zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1221), BgL_vz00_1222);
		}

	}



/* jarray-name */
	BGL_EXPORTED_DEF obj_t BGl_jarrayzd2namezd2zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_62)
	{
		{	/* Foreign/jtype.sch 82 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_62)))->BgL_namez00);
		}

	}



/* &jarray-name */
	obj_t BGl_z62jarrayzd2namezb0zzforeign_jtypez00(obj_t BgL_envz00_1223,
		obj_t BgL_oz00_1224)
	{
		{	/* Foreign/jtype.sch 82 */
			return
				BGl_jarrayzd2namezd2zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1224));
		}

	}



/* jarray-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_jarrayzd2namezd2setz12z12zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_63, obj_t BgL_vz00_64)
	{
		{	/* Foreign/jtype.sch 83 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_63)))->BgL_namez00) =
				((obj_t) BgL_vz00_64), BUNSPEC);
		}

	}



/* &jarray-name-set! */
	obj_t BGl_z62jarrayzd2namezd2setz12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1225, obj_t BgL_oz00_1226, obj_t BgL_vz00_1227)
	{
		{	/* Foreign/jtype.sch 83 */
			return
				BGl_jarrayzd2namezd2setz12z12zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1226), BgL_vz00_1227);
		}

	}



/* jarray-id */
	BGL_EXPORTED_DEF obj_t BGl_jarrayzd2idzd2zzforeign_jtypez00(BgL_typez00_bglt
		BgL_oz00_65)
	{
		{	/* Foreign/jtype.sch 84 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_65)))->BgL_idz00);
		}

	}



/* &jarray-id */
	obj_t BGl_z62jarrayzd2idzb0zzforeign_jtypez00(obj_t BgL_envz00_1228,
		obj_t BgL_oz00_1229)
	{
		{	/* Foreign/jtype.sch 84 */
			return
				BGl_jarrayzd2idzd2zzforeign_jtypez00(
				((BgL_typez00_bglt) BgL_oz00_1229));
		}

	}



/* declare-jvm-type! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_declarezd2jvmzd2typez12z12zzforeign_jtypez00(obj_t BgL_idz00_68,
		obj_t BgL_ofz00_69, obj_t BgL_srcz00_70)
	{
		{	/* Foreign/jtype.scm 46 */
			{	/* Foreign/jtype.scm 48 */
				BgL_typez00_bglt BgL_pointedz00_605;

				BgL_pointedz00_605 =
					BGl_usezd2typez12zc0zztype_envz00(BgL_ofz00_69,
					BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_70));
				{	/* Foreign/jtype.scm 48 */
					obj_t BgL_pointerz00_606;

					BgL_pointerz00_606 =
						(((BgL_typez00_bglt) COBJECT(BgL_pointedz00_605))->
						BgL_pointedzd2tozd2byz00);
					{	/* Foreign/jtype.scm 49 */
						obj_t BgL_namez00_607;

						{	/* Foreign/jtype.scm 50 */
							obj_t BgL_arg1455z00_979;

							BgL_arg1455z00_979 = SYMBOL_TO_STRING(BgL_idz00_68);
							BgL_namez00_607 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_979);
						}
						{	/* Foreign/jtype.scm 50 */
							obj_t BgL_objzd2ze3idz31_608;

							{	/* Foreign/jtype.scm 51 */
								obj_t BgL_arg1196z00_634;

								{	/* Foreign/jtype.scm 51 */
									obj_t BgL_arg1197z00_635;
									obj_t BgL_arg1198z00_636;

									{	/* Foreign/jtype.scm 51 */
										obj_t BgL_symbolz00_980;

										BgL_symbolz00_980 = CNST_TABLE_REF(0);
										{	/* Foreign/jtype.scm 51 */
											obj_t BgL_arg1455z00_981;

											BgL_arg1455z00_981 = SYMBOL_TO_STRING(BgL_symbolz00_980);
											BgL_arg1197z00_635 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_981);
										}
									}
									{	/* Foreign/jtype.scm 51 */
										obj_t BgL_arg1455z00_983;

										BgL_arg1455z00_983 = SYMBOL_TO_STRING(BgL_idz00_68);
										BgL_arg1198z00_636 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_983);
									}
									BgL_arg1196z00_634 =
										string_append(BgL_arg1197z00_635, BgL_arg1198z00_636);
								}
								BgL_objzd2ze3idz31_608 = bstring_to_symbol(BgL_arg1196z00_634);
							}
							{	/* Foreign/jtype.scm 51 */

								{	/* Foreign/jtype.scm 54 */
									bool_t BgL_test1801z00_1797;

									{	/* Foreign/jtype.scm 54 */
										obj_t BgL_classz00_985;

										BgL_classz00_985 = BGl_typez00zztype_typez00;
										if (BGL_OBJECTP(BgL_pointerz00_606))
											{	/* Foreign/jtype.scm 54 */
												BgL_objectz00_bglt BgL_arg1807z00_987;

												BgL_arg1807z00_987 =
													(BgL_objectz00_bglt) (BgL_pointerz00_606);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Foreign/jtype.scm 54 */
														long BgL_idxz00_993;

														BgL_idxz00_993 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_987);
														BgL_test1801z00_1797 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_993 + 1L)) == BgL_classz00_985);
													}
												else
													{	/* Foreign/jtype.scm 54 */
														bool_t BgL_res1724z00_1018;

														{	/* Foreign/jtype.scm 54 */
															obj_t BgL_oclassz00_1001;

															{	/* Foreign/jtype.scm 54 */
																obj_t BgL_arg1815z00_1009;
																long BgL_arg1816z00_1010;

																BgL_arg1815z00_1009 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Foreign/jtype.scm 54 */
																	long BgL_arg1817z00_1011;

																	BgL_arg1817z00_1011 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_987);
																	BgL_arg1816z00_1010 =
																		(BgL_arg1817z00_1011 - OBJECT_TYPE);
																}
																BgL_oclassz00_1001 =
																	VECTOR_REF(BgL_arg1815z00_1009,
																	BgL_arg1816z00_1010);
															}
															{	/* Foreign/jtype.scm 54 */
																bool_t BgL__ortest_1115z00_1002;

																BgL__ortest_1115z00_1002 =
																	(BgL_classz00_985 == BgL_oclassz00_1001);
																if (BgL__ortest_1115z00_1002)
																	{	/* Foreign/jtype.scm 54 */
																		BgL_res1724z00_1018 =
																			BgL__ortest_1115z00_1002;
																	}
																else
																	{	/* Foreign/jtype.scm 54 */
																		long BgL_odepthz00_1003;

																		{	/* Foreign/jtype.scm 54 */
																			obj_t BgL_arg1804z00_1004;

																			BgL_arg1804z00_1004 =
																				(BgL_oclassz00_1001);
																			BgL_odepthz00_1003 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_1004);
																		}
																		if ((1L < BgL_odepthz00_1003))
																			{	/* Foreign/jtype.scm 54 */
																				obj_t BgL_arg1802z00_1006;

																				{	/* Foreign/jtype.scm 54 */
																					obj_t BgL_arg1803z00_1007;

																					BgL_arg1803z00_1007 =
																						(BgL_oclassz00_1001);
																					BgL_arg1802z00_1006 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_1007, 1L);
																				}
																				BgL_res1724z00_1018 =
																					(BgL_arg1802z00_1006 ==
																					BgL_classz00_985);
																			}
																		else
																			{	/* Foreign/jtype.scm 54 */
																				BgL_res1724z00_1018 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1801z00_1797 = BgL_res1724z00_1018;
													}
											}
										else
											{	/* Foreign/jtype.scm 54 */
												BgL_test1801z00_1797 = ((bool_t) 0);
											}
									}
									if (BgL_test1801z00_1797)
										{	/* Foreign/jtype.scm 54 */
											return
												BGl_declarezd2czd2aliasz12z12zzforeign_ctypez00
												(BgL_idz00_68, BgL_ofz00_69, BgL_namez00_607);
										}
									else
										{	/* Foreign/jtype.scm 58 */
											BgL_typez00_bglt BgL_typez00_610;

											BgL_typez00_610 =
												BGl_declarezd2subtypez12zc0zztype_envz00(BgL_idz00_68,
												BgL_namez00_607, CNST_TABLE_REF(1), CNST_TABLE_REF(2));
											{	/* Foreign/jtype.scm 58 */

												{	/* Foreign/jtype.scm 61 */
													obj_t BgL_arg1152z00_611;

													{	/* Foreign/jtype.scm 61 */
														obj_t BgL_arg1153z00_612;

														{	/* Foreign/jtype.scm 61 */
															obj_t BgL_arg1154z00_613;
															obj_t BgL_arg1157z00_614;

															{	/* Foreign/jtype.scm 61 */
																obj_t BgL_arg1158z00_615;

																{	/* Foreign/jtype.scm 61 */
																	obj_t BgL_arg1162z00_616;

																	{	/* Foreign/jtype.scm 61 */
																		obj_t BgL_arg1164z00_617;

																		{	/* Foreign/jtype.scm 61 */
																			obj_t BgL_arg1166z00_618;

																			BgL_arg1166z00_618 =
																				MAKE_YOUNG_PAIR(BNIL, BNIL);
																			BgL_arg1164z00_617 =
																				MAKE_YOUNG_PAIR(BNIL,
																				BgL_arg1166z00_618);
																		}
																		BgL_arg1162z00_616 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																			BgL_arg1164z00_617);
																	}
																	BgL_arg1158z00_615 =
																		MAKE_YOUNG_PAIR(BgL_idz00_68,
																		BgL_arg1162z00_616);
																}
																BgL_arg1154z00_613 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																	BgL_arg1158z00_615);
															}
															{	/* Foreign/jtype.scm 62 */
																obj_t BgL_arg1171z00_619;

																{	/* Foreign/jtype.scm 62 */
																	obj_t BgL_arg1172z00_620;

																	{	/* Foreign/jtype.scm 62 */
																		obj_t BgL_arg1182z00_621;

																		{	/* Foreign/jtype.scm 62 */
																			obj_t BgL_arg1183z00_622;

																			{	/* Foreign/jtype.scm 62 */
																				obj_t BgL_arg1187z00_623;
																				obj_t BgL_arg1188z00_624;

																				{	/* Foreign/jtype.scm 62 */
																					obj_t BgL_arg1189z00_625;

																					{	/* Foreign/jtype.scm 62 */
																						obj_t BgL_arg1190z00_626;

																						{	/* Foreign/jtype.scm 62 */
																							obj_t BgL_arg1191z00_627;
																							obj_t BgL_arg1193z00_628;

																							{	/* Foreign/jtype.scm 62 */
																								obj_t BgL_arg1455z00_1020;

																								BgL_arg1455z00_1020 =
																									SYMBOL_TO_STRING
																									(BgL_idz00_68);
																								BgL_arg1191z00_627 =
																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																									(BgL_arg1455z00_1020);
																							}
																							{	/* Foreign/jtype.scm 62 */
																								obj_t BgL_symbolz00_1021;

																								BgL_symbolz00_1021 =
																									CNST_TABLE_REF(5);
																								{	/* Foreign/jtype.scm 62 */
																									obj_t BgL_arg1455z00_1022;

																									BgL_arg1455z00_1022 =
																										SYMBOL_TO_STRING
																										(BgL_symbolz00_1021);
																									BgL_arg1193z00_628 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg1455z00_1022);
																								}
																							}
																							BgL_arg1190z00_626 =
																								string_append
																								(BgL_arg1191z00_627,
																								BgL_arg1193z00_628);
																						}
																						BgL_arg1189z00_625 =
																							bstring_to_symbol
																							(BgL_arg1190z00_626);
																					}
																					BgL_arg1187z00_623 =
																						MAKE_YOUNG_PAIR(BgL_arg1189z00_625,
																						BNIL);
																				}
																				BgL_arg1188z00_624 =
																					MAKE_YOUNG_PAIR(BNIL, BNIL);
																				BgL_arg1183z00_622 =
																					MAKE_YOUNG_PAIR(BgL_arg1187z00_623,
																					BgL_arg1188z00_624);
																			}
																			BgL_arg1182z00_621 =
																				MAKE_YOUNG_PAIR(BgL_idz00_68,
																				BgL_arg1183z00_622);
																		}
																		BgL_arg1172z00_620 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																			BgL_arg1182z00_621);
																	}
																	BgL_arg1171z00_619 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																		BgL_arg1172z00_620);
																}
																BgL_arg1157z00_614 =
																	MAKE_YOUNG_PAIR(BgL_arg1171z00_619, BNIL);
															}
															BgL_arg1153z00_612 =
																MAKE_YOUNG_PAIR(BgL_arg1154z00_613,
																BgL_arg1157z00_614);
														}
														BgL_arg1152z00_611 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
															BgL_arg1153z00_612);
													}
													BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
														(BgL_arg1152z00_611);
												}
												((((BgL_typez00_bglt) COBJECT(BgL_pointedz00_605))->
														BgL_pointedzd2tozd2byz00) =
													((obj_t) ((obj_t) BgL_typez00_610)), BUNSPEC);
												{	/* Foreign/jtype.scm 65 */
													BgL_jarrayz00_bglt BgL_wide1096z00_631;

													BgL_wide1096z00_631 =
														((BgL_jarrayz00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_jarrayz00_bgl))));
													{	/* Foreign/jtype.scm 65 */
														obj_t BgL_auxz00_1857;
														BgL_objectz00_bglt BgL_tmpz00_1854;

														BgL_auxz00_1857 = ((obj_t) BgL_wide1096z00_631);
														BgL_tmpz00_1854 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_typez00_610));
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1854,
															BgL_auxz00_1857);
													}
													((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_typez00_610));
													{	/* Foreign/jtype.scm 65 */
														long BgL_arg1194z00_632;

														BgL_arg1194z00_632 =
															BGL_CLASS_NUM(BGl_jarrayz00zzforeign_jtypez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_typez00_610)),
															BgL_arg1194z00_632);
													}
													((BgL_typez00_bglt)
														((BgL_typez00_bglt) BgL_typez00_610));
												}
												{
													BgL_jarrayz00_bglt BgL_auxz00_1868;

													{
														obj_t BgL_auxz00_1869;

														{	/* Foreign/jtype.scm 66 */
															BgL_objectz00_bglt BgL_tmpz00_1870;

															BgL_tmpz00_1870 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_typez00_610));
															BgL_auxz00_1869 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_1870);
														}
														BgL_auxz00_1868 =
															((BgL_jarrayz00_bglt) BgL_auxz00_1869);
													}
													((((BgL_jarrayz00_bglt) COBJECT(BgL_auxz00_1868))->
															BgL_itemzd2typezd2) =
														((BgL_typez00_bglt) BgL_pointedz00_605), BUNSPEC);
												}
												((BgL_typez00_bglt) BgL_typez00_610);
												return BgL_typez00_610;
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &declare-jvm-type! */
	BgL_typez00_bglt BGl_z62declarezd2jvmzd2typez12z70zzforeign_jtypez00(obj_t
		BgL_envz00_1230, obj_t BgL_idz00_1231, obj_t BgL_ofz00_1232,
		obj_t BgL_srcz00_1233)
	{
		{	/* Foreign/jtype.scm 46 */
			return
				BGl_declarezd2jvmzd2typez12z12zzforeign_jtypez00(BgL_idz00_1231,
				BgL_ofz00_1232, BgL_srcz00_1233);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.scm 17 */
			{	/* Foreign/jtype.scm 38 */
				obj_t BgL_arg1206z00_642;
				obj_t BgL_arg1208z00_643;

				{	/* Foreign/jtype.scm 38 */
					obj_t BgL_v1138z00_681;

					BgL_v1138z00_681 = create_vector(1L);
					{	/* Foreign/jtype.scm 38 */
						obj_t BgL_arg1221z00_682;

						BgL_arg1221z00_682 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc1731z00zzforeign_jtypez00,
							BGl_proc1730z00zzforeign_jtypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1138z00_681, 0L, BgL_arg1221z00_682);
					}
					BgL_arg1206z00_642 = BgL_v1138z00_681;
				}
				{	/* Foreign/jtype.scm 38 */
					obj_t BgL_v1139z00_692;

					BgL_v1139z00_692 = create_vector(0L);
					BgL_arg1208z00_643 = BgL_v1139z00_692;
				}
				return (BGl_jarrayz00zzforeign_jtypez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(8),
						CNST_TABLE_REF(9), BGl_typez00zztype_typez00, 10395L,
						BGl_proc1735z00zzforeign_jtypez00,
						BGl_proc1734z00zzforeign_jtypez00, BFALSE,
						BGl_proc1733z00zzforeign_jtypez00,
						BGl_proc1732z00zzforeign_jtypez00, BgL_arg1206z00_642,
						BgL_arg1208z00_643), BUNSPEC);
			}
		}

	}



/* &lambda1217 */
	BgL_typez00_bglt BGl_z62lambda1217z62zzforeign_jtypez00(obj_t BgL_envz00_1240,
		obj_t BgL_o1078z00_1241)
	{
		{	/* Foreign/jtype.scm 38 */
			{	/* Foreign/jtype.scm 38 */
				long BgL_arg1218z00_1311;

				{	/* Foreign/jtype.scm 38 */
					obj_t BgL_arg1219z00_1312;

					{	/* Foreign/jtype.scm 38 */
						obj_t BgL_arg1220z00_1313;

						{	/* Foreign/jtype.scm 38 */
							obj_t BgL_arg1815z00_1314;
							long BgL_arg1816z00_1315;

							BgL_arg1815z00_1314 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Foreign/jtype.scm 38 */
								long BgL_arg1817z00_1316;

								BgL_arg1817z00_1316 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1078z00_1241)));
								BgL_arg1816z00_1315 = (BgL_arg1817z00_1316 - OBJECT_TYPE);
							}
							BgL_arg1220z00_1313 =
								VECTOR_REF(BgL_arg1815z00_1314, BgL_arg1816z00_1315);
						}
						BgL_arg1219z00_1312 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1220z00_1313);
					}
					{	/* Foreign/jtype.scm 38 */
						obj_t BgL_tmpz00_1893;

						BgL_tmpz00_1893 = ((obj_t) BgL_arg1219z00_1312);
						BgL_arg1218z00_1311 = BGL_CLASS_NUM(BgL_tmpz00_1893);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1078z00_1241)), BgL_arg1218z00_1311);
			}
			{	/* Foreign/jtype.scm 38 */
				BgL_objectz00_bglt BgL_tmpz00_1899;

				BgL_tmpz00_1899 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1078z00_1241));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1899, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1078z00_1241));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1078z00_1241));
		}

	}



/* &<@anonymous:1216> */
	obj_t BGl_z62zc3z04anonymousza31216ze3ze5zzforeign_jtypez00(obj_t
		BgL_envz00_1242, obj_t BgL_new1077z00_1243)
	{
		{	/* Foreign/jtype.scm 38 */
			{
				BgL_typez00_bglt BgL_auxz00_1907;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1077z00_1243))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(10)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1077z00_1243))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_1965;
					BgL_jarrayz00_bglt BgL_auxz00_1958;

					{	/* Foreign/jtype.scm 38 */
						obj_t BgL_classz00_1318;

						BgL_classz00_1318 = BGl_typez00zztype_typez00;
						{	/* Foreign/jtype.scm 38 */
							obj_t BgL__ortest_1117z00_1319;

							BgL__ortest_1117z00_1319 = BGL_CLASS_NIL(BgL_classz00_1318);
							if (CBOOL(BgL__ortest_1117z00_1319))
								{	/* Foreign/jtype.scm 38 */
									BgL_auxz00_1965 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_1319);
								}
							else
								{	/* Foreign/jtype.scm 38 */
									BgL_auxz00_1965 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_1318));
								}
						}
					}
					{
						obj_t BgL_auxz00_1959;

						{	/* Foreign/jtype.scm 38 */
							BgL_objectz00_bglt BgL_tmpz00_1960;

							BgL_tmpz00_1960 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1077z00_1243));
							BgL_auxz00_1959 = BGL_OBJECT_WIDENING(BgL_tmpz00_1960);
						}
						BgL_auxz00_1958 = ((BgL_jarrayz00_bglt) BgL_auxz00_1959);
					}
					((((BgL_jarrayz00_bglt) COBJECT(BgL_auxz00_1958))->
							BgL_itemzd2typezd2) =
						((BgL_typez00_bglt) BgL_auxz00_1965), BUNSPEC);
				}
				BgL_auxz00_1907 = ((BgL_typez00_bglt) BgL_new1077z00_1243);
				return ((obj_t) BgL_auxz00_1907);
			}
		}

	}



/* &lambda1213 */
	BgL_typez00_bglt BGl_z62lambda1213z62zzforeign_jtypez00(obj_t BgL_envz00_1244,
		obj_t BgL_o1074z00_1245)
	{
		{	/* Foreign/jtype.scm 38 */
			{	/* Foreign/jtype.scm 38 */
				BgL_jarrayz00_bglt BgL_wide1076z00_1321;

				BgL_wide1076z00_1321 =
					((BgL_jarrayz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_jarrayz00_bgl))));
				{	/* Foreign/jtype.scm 38 */
					obj_t BgL_auxz00_1980;
					BgL_objectz00_bglt BgL_tmpz00_1976;

					BgL_auxz00_1980 = ((obj_t) BgL_wide1076z00_1321);
					BgL_tmpz00_1976 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1074z00_1245)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1976, BgL_auxz00_1980);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1074z00_1245)));
				{	/* Foreign/jtype.scm 38 */
					long BgL_arg1215z00_1322;

					BgL_arg1215z00_1322 = BGL_CLASS_NUM(BGl_jarrayz00zzforeign_jtypez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1074z00_1245))), BgL_arg1215z00_1322);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1074z00_1245)));
			}
		}

	}



/* &lambda1209 */
	BgL_typez00_bglt BGl_z62lambda1209z62zzforeign_jtypez00(obj_t BgL_envz00_1246,
		obj_t BgL_id1057z00_1247, obj_t BgL_name1058z00_1248,
		obj_t BgL_siza7e1059za7_1249, obj_t BgL_class1060z00_1250,
		obj_t BgL_coercezd2to1061zd2_1251, obj_t BgL_parents1062z00_1252,
		obj_t BgL_initzf31063zf3_1253, obj_t BgL_magiczf31064zf3_1254,
		obj_t BgL_null1065z00_1255, obj_t BgL_z421066z42_1256,
		obj_t BgL_alias1067z00_1257, obj_t BgL_pointedzd2tozd2by1068z00_1258,
		obj_t BgL_tvector1069z00_1259, obj_t BgL_location1070z00_1260,
		obj_t BgL_importzd2location1071zd2_1261, obj_t BgL_occurrence1072z00_1262,
		obj_t BgL_itemzd2type1073zd2_1263)
	{
		{	/* Foreign/jtype.scm 38 */
			{	/* Foreign/jtype.scm 38 */
				bool_t BgL_initzf31063zf3_1324;
				bool_t BgL_magiczf31064zf3_1325;
				int BgL_occurrence1072z00_1326;

				BgL_initzf31063zf3_1324 = CBOOL(BgL_initzf31063zf3_1253);
				BgL_magiczf31064zf3_1325 = CBOOL(BgL_magiczf31064zf3_1254);
				BgL_occurrence1072z00_1326 = CINT(BgL_occurrence1072z00_1262);
				{	/* Foreign/jtype.scm 38 */
					BgL_typez00_bglt BgL_new1101z00_1328;

					{	/* Foreign/jtype.scm 38 */
						BgL_typez00_bglt BgL_tmp1099z00_1329;
						BgL_jarrayz00_bglt BgL_wide1100z00_1330;

						{
							BgL_typez00_bglt BgL_auxz00_1997;

							{	/* Foreign/jtype.scm 38 */
								BgL_typez00_bglt BgL_new1098z00_1331;

								BgL_new1098z00_1331 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Foreign/jtype.scm 38 */
									long BgL_arg1212z00_1332;

									BgL_arg1212z00_1332 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1098z00_1331),
										BgL_arg1212z00_1332);
								}
								{	/* Foreign/jtype.scm 38 */
									BgL_objectz00_bglt BgL_tmpz00_2002;

									BgL_tmpz00_2002 = ((BgL_objectz00_bglt) BgL_new1098z00_1331);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2002, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1098z00_1331);
								BgL_auxz00_1997 = BgL_new1098z00_1331;
							}
							BgL_tmp1099z00_1329 = ((BgL_typez00_bglt) BgL_auxz00_1997);
						}
						BgL_wide1100z00_1330 =
							((BgL_jarrayz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_jarrayz00_bgl))));
						{	/* Foreign/jtype.scm 38 */
							obj_t BgL_auxz00_2010;
							BgL_objectz00_bglt BgL_tmpz00_2008;

							BgL_auxz00_2010 = ((obj_t) BgL_wide1100z00_1330);
							BgL_tmpz00_2008 = ((BgL_objectz00_bglt) BgL_tmp1099z00_1329);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2008, BgL_auxz00_2010);
						}
						((BgL_objectz00_bglt) BgL_tmp1099z00_1329);
						{	/* Foreign/jtype.scm 38 */
							long BgL_arg1210z00_1333;

							BgL_arg1210z00_1333 =
								BGL_CLASS_NUM(BGl_jarrayz00zzforeign_jtypez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1099z00_1329),
								BgL_arg1210z00_1333);
						}
						BgL_new1101z00_1328 = ((BgL_typez00_bglt) BgL_tmp1099z00_1329);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1101z00_1328)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1057z00_1247)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_namez00) =
						((obj_t) BgL_name1058z00_1248), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1059za7_1249), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_classz00) =
						((obj_t) BgL_class1060z00_1250), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1061zd2_1251), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_parentsz00) =
						((obj_t) BgL_parents1062z00_1252), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31063zf3_1324), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31064zf3_1325), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_nullz00) =
						((obj_t) BgL_null1065z00_1255), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_z42z42) =
						((obj_t) BgL_z421066z42_1256), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_aliasz00) =
						((obj_t) BgL_alias1067z00_1257), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1068z00_1258), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1069z00_1259), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_locationz00) =
						((obj_t) BgL_location1070z00_1260), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1071zd2_1261), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1101z00_1328)))->BgL_occurrencez00) =
						((int) BgL_occurrence1072z00_1326), BUNSPEC);
					{
						BgL_jarrayz00_bglt BgL_auxz00_2051;

						{
							obj_t BgL_auxz00_2052;

							{	/* Foreign/jtype.scm 38 */
								BgL_objectz00_bglt BgL_tmpz00_2053;

								BgL_tmpz00_2053 = ((BgL_objectz00_bglt) BgL_new1101z00_1328);
								BgL_auxz00_2052 = BGL_OBJECT_WIDENING(BgL_tmpz00_2053);
							}
							BgL_auxz00_2051 = ((BgL_jarrayz00_bglt) BgL_auxz00_2052);
						}
						((((BgL_jarrayz00_bglt) COBJECT(BgL_auxz00_2051))->
								BgL_itemzd2typezd2) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BgL_itemzd2type1073zd2_1263)), BUNSPEC);
					}
					return BgL_new1101z00_1328;
				}
			}
		}

	}



/* &lambda1227 */
	obj_t BGl_z62lambda1227z62zzforeign_jtypez00(obj_t BgL_envz00_1264,
		obj_t BgL_oz00_1265, obj_t BgL_vz00_1266)
	{
		{	/* Foreign/jtype.scm 38 */
			{
				BgL_jarrayz00_bglt BgL_auxz00_2059;

				{
					obj_t BgL_auxz00_2060;

					{	/* Foreign/jtype.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_2061;

						BgL_tmpz00_2061 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_1265));
						BgL_auxz00_2060 = BGL_OBJECT_WIDENING(BgL_tmpz00_2061);
					}
					BgL_auxz00_2059 = ((BgL_jarrayz00_bglt) BgL_auxz00_2060);
				}
				return
					((((BgL_jarrayz00_bglt) COBJECT(BgL_auxz00_2059))->
						BgL_itemzd2typezd2) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_1266)), BUNSPEC);
			}
		}

	}



/* &lambda1226 */
	BgL_typez00_bglt BGl_z62lambda1226z62zzforeign_jtypez00(obj_t BgL_envz00_1267,
		obj_t BgL_oz00_1268)
	{
		{	/* Foreign/jtype.scm 38 */
			{
				BgL_jarrayz00_bglt BgL_auxz00_2068;

				{
					obj_t BgL_auxz00_2069;

					{	/* Foreign/jtype.scm 38 */
						BgL_objectz00_bglt BgL_tmpz00_2070;

						BgL_tmpz00_2070 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_1268));
						BgL_auxz00_2069 = BGL_OBJECT_WIDENING(BgL_tmpz00_2070);
					}
					BgL_auxz00_2068 = ((BgL_jarrayz00_bglt) BgL_auxz00_2069);
				}
				return
					(((BgL_jarrayz00_bglt) COBJECT(BgL_auxz00_2068))->BgL_itemzd2typezd2);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.scm 17 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
				BGl_jarrayz00zzforeign_jtypez00, BGl_proc1736z00zzforeign_jtypez00,
				BGl_string1737z00zzforeign_jtypez00);
		}

	}



/* &make-ctype-accesses!1141 */
	obj_t BGl_z62makezd2ctypezd2accessesz121141z70zzforeign_jtypez00(obj_t
		BgL_envz00_1270, obj_t BgL_whatz00_1271, obj_t BgL_whoz00_1272,
		obj_t BgL_locz00_1273)
	{
		{	/* Foreign/jtype.scm 72 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_idz00_1339;

				BgL_idz00_1339 =
					(((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_whatz00_1271))))->BgL_idz00);
				{	/* Tools/trace.sch 53 */
					obj_t BgL_oidz00_1340;

					BgL_oidz00_1340 =
						BGl_makezd2typedzd2identz00zzast_identz00(CNST_TABLE_REF(11),
						BgL_idz00_1339);
					{	/* Tools/trace.sch 53 */
						obj_t BgL_tidz00_1341;

						BgL_tidz00_1341 =
							BGl_makezd2typedzd2formalz00zzast_identz00(BgL_idz00_1339);
						{	/* Tools/trace.sch 53 */
							obj_t BgL_idzd2ze3objz31_1342;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_arg1688z00_1343;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1689z00_1344;
									obj_t BgL_arg1691z00_1345;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1455z00_1346;

										BgL_arg1455z00_1346 = SYMBOL_TO_STRING(BgL_idz00_1339);
										BgL_arg1689z00_1344 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_1346);
									}
									{	/* Tools/trace.sch 53 */
										obj_t BgL_symbolz00_1347;

										BgL_symbolz00_1347 = CNST_TABLE_REF(12);
										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1455z00_1348;

											BgL_arg1455z00_1348 =
												SYMBOL_TO_STRING(BgL_symbolz00_1347);
											BgL_arg1691z00_1345 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_1348);
										}
									}
									BgL_arg1688z00_1343 =
										string_append(BgL_arg1689z00_1344, BgL_arg1691z00_1345);
								}
								BgL_idzd2ze3objz31_1342 =
									bstring_to_symbol(BgL_arg1688z00_1343);
							}
							{	/* Tools/trace.sch 53 */
								obj_t BgL_objzd2ze3idz31_1349;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1675z00_1350;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1678z00_1351;
										obj_t BgL_arg1681z00_1352;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_symbolz00_1353;

											BgL_symbolz00_1353 = CNST_TABLE_REF(0);
											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1455z00_1354;

												BgL_arg1455z00_1354 =
													SYMBOL_TO_STRING(BgL_symbolz00_1353);
												BgL_arg1678z00_1351 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1354);
											}
										}
										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1455z00_1355;

											BgL_arg1455z00_1355 = SYMBOL_TO_STRING(BgL_idz00_1339);
											BgL_arg1681z00_1352 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_1355);
										}
										BgL_arg1675z00_1350 =
											string_append(BgL_arg1678z00_1351, BgL_arg1681z00_1352);
									}
									BgL_objzd2ze3idz31_1349 =
										bstring_to_symbol(BgL_arg1675z00_1350);
								}
								{	/* Tools/trace.sch 53 */
									obj_t BgL_tobjzd2ze3idz31_1356;

									BgL_tobjzd2ze3idz31_1356 =
										BGl_makezd2typedzd2identz00zzast_identz00
										(BgL_objzd2ze3idz31_1349, BgL_idz00_1339);
									{	/* Tools/trace.sch 53 */
										obj_t BgL_idzf3zf3_1357;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1654z00_1358;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1661z00_1359;
												obj_t BgL_arg1663z00_1360;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1455z00_1361;

													BgL_arg1455z00_1361 =
														SYMBOL_TO_STRING(BgL_idz00_1339);
													BgL_arg1661z00_1359 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1361);
												}
												{	/* Tools/trace.sch 53 */
													obj_t BgL_symbolz00_1362;

													BgL_symbolz00_1362 = CNST_TABLE_REF(5);
													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1455z00_1363;

														BgL_arg1455z00_1363 =
															SYMBOL_TO_STRING(BgL_symbolz00_1362);
														BgL_arg1663z00_1360 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1363);
													}
												}
												BgL_arg1654z00_1358 =
													string_append(BgL_arg1661z00_1359,
													BgL_arg1663z00_1360);
											}
											BgL_idzf3zf3_1357 =
												bstring_to_symbol(BgL_arg1654z00_1358);
										}
										{	/* Tools/trace.sch 53 */
											obj_t BgL_tidzf3zf3_1364;

											BgL_tidzf3zf3_1364 =
												BGl_makezd2typedzd2identz00zzast_identz00
												(BgL_idzf3zf3_1357, CNST_TABLE_REF(13));
											{	/* Tools/trace.sch 53 */
												obj_t BgL_idzd2lengthzd2_1365;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1646z00_1366;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1650z00_1367;
														obj_t BgL_arg1651z00_1368;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1455z00_1369;

															BgL_arg1455z00_1369 =
																SYMBOL_TO_STRING(BgL_idz00_1339);
															BgL_arg1650z00_1367 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_1369);
														}
														{	/* Tools/trace.sch 53 */
															obj_t BgL_symbolz00_1370;

															BgL_symbolz00_1370 = CNST_TABLE_REF(14);
															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1455z00_1371;

																BgL_arg1455z00_1371 =
																	SYMBOL_TO_STRING(BgL_symbolz00_1370);
																BgL_arg1651z00_1368 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_1371);
															}
														}
														BgL_arg1646z00_1366 =
															string_append(BgL_arg1650z00_1367,
															BgL_arg1651z00_1368);
													}
													BgL_idzd2lengthzd2_1365 =
														bstring_to_symbol(BgL_arg1646z00_1366);
												}
												{	/* Tools/trace.sch 53 */
													obj_t BgL_tidzd2lengthzd2_1372;

													BgL_tidzd2lengthzd2_1372 =
														BGl_makezd2typedzd2identz00zzast_identz00
														(BgL_idzd2lengthzd2_1365, CNST_TABLE_REF(15));
													{	/* Tools/trace.sch 53 */
														BgL_typez00_bglt BgL_itemzd2typezd2_1373;

														{
															BgL_jarrayz00_bglt BgL_auxz00_2116;

															{
																obj_t BgL_auxz00_2117;

																{	/* Tools/trace.sch 53 */
																	BgL_objectz00_bglt BgL_tmpz00_2118;

																	BgL_tmpz00_2118 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_whatz00_1271));
																	BgL_auxz00_2117 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2118);
																}
																BgL_auxz00_2116 =
																	((BgL_jarrayz00_bglt) BgL_auxz00_2117);
															}
															BgL_itemzd2typezd2_1373 =
																(((BgL_jarrayz00_bglt)
																	COBJECT(BgL_auxz00_2116))->
																BgL_itemzd2typezd2);
														}
														{	/* Tools/trace.sch 53 */
															obj_t BgL_itemzd2typezd2idz00_1374;

															BgL_itemzd2typezd2idz00_1374 =
																(((BgL_typez00_bglt)
																	COBJECT(BgL_itemzd2typezd2_1373))->BgL_idz00);
															{	/* Tools/trace.sch 53 */
																obj_t BgL_fitemzd2typezd2idz00_1375;

																BgL_fitemzd2typezd2idz00_1375 =
																	BGl_makezd2typedzd2formalz00zzast_identz00
																	(BgL_itemzd2typezd2idz00_1374);
																{	/* Tools/trace.sch 53 */
																	obj_t BgL_oitemzd2typezd2idz00_1376;

																	BgL_oitemzd2typezd2idz00_1376 =
																		BGl_makezd2typedzd2identz00zzast_identz00
																		(CNST_TABLE_REF(16),
																		BgL_itemzd2typezd2idz00_1374);
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_makezd2idzd2_1377;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1629z00_1378;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1630z00_1379;
																				obj_t BgL_arg1642z00_1380;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_symbolz00_1381;

																					BgL_symbolz00_1381 =
																						CNST_TABLE_REF(17);
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1382;

																						BgL_arg1455z00_1382 =
																							SYMBOL_TO_STRING
																							(BgL_symbolz00_1381);
																						BgL_arg1630z00_1379 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1382);
																					}
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1455z00_1383;

																					BgL_arg1455z00_1383 =
																						SYMBOL_TO_STRING(BgL_idz00_1339);
																					BgL_arg1642z00_1380 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1455z00_1383);
																				}
																				BgL_arg1629z00_1378 =
																					string_append(BgL_arg1630z00_1379,
																					BgL_arg1642z00_1380);
																			}
																			BgL_makezd2idzd2_1377 =
																				bstring_to_symbol(BgL_arg1629z00_1378);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_tmakezd2idzd2_1384;

																			BgL_tmakezd2idzd2_1384 =
																				BGl_makezd2typedzd2identz00zzast_identz00
																				(BgL_makezd2idzd2_1377, BgL_idz00_1339);
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_idzd2refzd2_1385;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1625z00_1386;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1626z00_1387;
																						obj_t BgL_arg1627z00_1388;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1455z00_1389;

																							BgL_arg1455z00_1389 =
																								SYMBOL_TO_STRING
																								(BgL_idz00_1339);
																							BgL_arg1626z00_1387 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_1389);
																						}
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_symbolz00_1390;

																							BgL_symbolz00_1390 =
																								CNST_TABLE_REF(18);
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1455z00_1391;

																								BgL_arg1455z00_1391 =
																									SYMBOL_TO_STRING
																									(BgL_symbolz00_1390);
																								BgL_arg1627z00_1388 =
																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																									(BgL_arg1455z00_1391);
																							}
																						}
																						BgL_arg1625z00_1386 =
																							string_append(BgL_arg1626z00_1387,
																							BgL_arg1627z00_1388);
																					}
																					BgL_idzd2refzd2_1385 =
																						bstring_to_symbol
																						(BgL_arg1625z00_1386);
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_tidzd2refzd2_1392;

																					BgL_tidzd2refzd2_1392 =
																						BGl_makezd2typedzd2identz00zzast_identz00
																						(BgL_idzd2refzd2_1385,
																						BgL_itemzd2typezd2idz00_1374);
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_idzd2setz12zc0_1393;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1613z00_1394;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1615z00_1395;
																								obj_t BgL_arg1616z00_1396;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1455z00_1397;

																									BgL_arg1455z00_1397 =
																										SYMBOL_TO_STRING
																										(BgL_idz00_1339);
																									BgL_arg1615z00_1395 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg1455z00_1397);
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_symbolz00_1398;

																									BgL_symbolz00_1398 =
																										CNST_TABLE_REF(19);
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1455z00_1399;

																										BgL_arg1455z00_1399 =
																											SYMBOL_TO_STRING
																											(BgL_symbolz00_1398);
																										BgL_arg1616z00_1396 =
																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																											(BgL_arg1455z00_1399);
																									}
																								}
																								BgL_arg1613z00_1394 =
																									string_append
																									(BgL_arg1615z00_1395,
																									BgL_arg1616z00_1396);
																							}
																							BgL_idzd2setz12zc0_1393 =
																								bstring_to_symbol
																								(BgL_arg1613z00_1394);
																						}
																						{	/* Tools/trace.sch 53 */

																							{

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_list1228z00_1469;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1229z00_1470;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1230z00_1471;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1231z00_1472;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1232z00_1473;
																													BgL_arg1232z00_1473 =
																														MAKE_YOUNG_PAIR
																														(BgL_idzd2setz12zc0_1393,
																														BNIL);
																													BgL_arg1231z00_1472 =
																														MAKE_YOUNG_PAIR
																														(BgL_idzd2refzd2_1385,
																														BgL_arg1232z00_1473);
																												}
																												BgL_arg1230z00_1471 =
																													MAKE_YOUNG_PAIR
																													(BgL_makezd2idzd2_1377,
																													BgL_arg1231z00_1472);
																											}
																											BgL_arg1229z00_1470 =
																												MAKE_YOUNG_PAIR
																												(BgL_idzd2lengthzd2_1365,
																												BgL_arg1230z00_1471);
																										}
																										BgL_list1228z00_1469 =
																											MAKE_YOUNG_PAIR
																											(BgL_idzf3zf3_1357,
																											BgL_arg1229z00_1470);
																									}
																									BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
																										(BgL_list1228z00_1469);
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1233z00_1474;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1234z00_1475;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1236z00_1476;
																											obj_t BgL_arg1238z00_1477;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1239z00_1478;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1242z00_1479;
																													BgL_arg1242z00_1479 =
																														MAKE_YOUNG_PAIR
																														(BgL_tidz00_1341,
																														BNIL);
																													BgL_arg1239z00_1478 =
																														MAKE_YOUNG_PAIR
																														(BgL_idzd2ze3objz31_1342,
																														BgL_arg1242z00_1479);
																												}
																												BgL_arg1236z00_1476 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(33),
																													BgL_arg1239z00_1478);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1244z00_1480;
																												obj_t
																													BgL_arg1248z00_1481;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1249z00_1482;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1252z00_1483;
																														BgL_arg1252z00_1483
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(34), BNIL);
																														BgL_arg1249z00_1482
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_tobjzd2ze3idz31_1356,
																															BgL_arg1252z00_1483);
																													}
																													BgL_arg1244z00_1480 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(33),
																														BgL_arg1249z00_1482);
																												}
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1268z00_1484;
																													obj_t
																														BgL_arg1272z00_1485;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1284z00_1486;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1304z00_1487;
																															BgL_arg1304z00_1487
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(34), BNIL);
																															BgL_arg1284z00_1486
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_tidzf3zf3_1364,
																																BgL_arg1304z00_1487);
																														}
																														BgL_arg1268z00_1484
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(33),
																															BgL_arg1284z00_1486);
																													}
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1305z00_1488;
																														obj_t
																															BgL_arg1306z00_1489;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1307z00_1490;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1308z00_1491;
																																BgL_arg1308z00_1491
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_tidz00_1341,
																																	BNIL);
																																BgL_arg1307z00_1490
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_tidzd2lengthzd2_1372,
																																	BgL_arg1308z00_1491);
																															}
																															BgL_arg1305z00_1488
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(33),
																																BgL_arg1307z00_1490);
																														}
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1310z00_1492;
																															obj_t
																																BgL_arg1311z00_1493;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1312z00_1494;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1314z00_1495;
																																	BgL_arg1314z00_1495
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(35), BNIL);
																																	BgL_arg1312z00_1494
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_tmakezd2idzd2_1384,
																																		BgL_arg1314z00_1495);
																																}
																																BgL_arg1310z00_1492
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(33),
																																	BgL_arg1312z00_1494);
																															}
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1315z00_1496;
																																obj_t
																																	BgL_arg1316z00_1497;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1317z00_1498;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1318z00_1499;
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_arg1319z00_1500;
																																			BgL_arg1319z00_1500
																																				=
																																				MAKE_YOUNG_PAIR
																																				(CNST_TABLE_REF
																																				(35),
																																				BNIL);
																																			BgL_arg1318z00_1499
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_tidz00_1341,
																																				BgL_arg1319z00_1500);
																																		}
																																		BgL_arg1317z00_1498
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_tidzd2refzd2_1392,
																																			BgL_arg1318z00_1499);
																																	}
																																	BgL_arg1315z00_1496
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(33),
																																		BgL_arg1317z00_1498);
																																}
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1320z00_1501;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1321z00_1502;
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_arg1322z00_1503;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_arg1323z00_1504;
																																				{	/* Tools/trace.sch 53 */
																																					obj_t
																																						BgL_arg1325z00_1505;
																																					BgL_arg1325z00_1505
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_fitemzd2typezd2idz00_1375,
																																						BNIL);
																																					BgL_arg1323z00_1504
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(35),
																																						BgL_arg1325z00_1505);
																																				}
																																				BgL_arg1322z00_1503
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_tidz00_1341,
																																					BgL_arg1323z00_1504);
																																			}
																																			BgL_arg1321z00_1502
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_idzd2setz12zc0_1393,
																																				BgL_arg1322z00_1503);
																																		}
																																		BgL_arg1320z00_1501
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(33),
																																			BgL_arg1321z00_1502);
																																	}
																																	BgL_arg1316z00_1497
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1320z00_1501,
																																		BNIL);
																																}
																																BgL_arg1311z00_1493
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1315z00_1496,
																																	BgL_arg1316z00_1497);
																															}
																															BgL_arg1306z00_1489
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1310z00_1492,
																																BgL_arg1311z00_1493);
																														}
																														BgL_arg1272z00_1485
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1305z00_1488,
																															BgL_arg1306z00_1489);
																													}
																													BgL_arg1248z00_1481 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1268z00_1484,
																														BgL_arg1272z00_1485);
																												}
																												BgL_arg1238z00_1477 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1244z00_1480,
																													BgL_arg1248z00_1481);
																											}
																											BgL_arg1234z00_1475 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1236z00_1476,
																												BgL_arg1238z00_1477);
																										}
																										BgL_arg1233z00_1474 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(36),
																											BgL_arg1234z00_1475);
																									}
																									BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																										(BgL_arg1233z00_1474);
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1326z00_1506;
																									obj_t BgL_arg1327z00_1507;
																									obj_t BgL_arg1328z00_1508;
																									obj_t BgL_arg1329z00_1509;
																									obj_t BgL_arg1331z00_1510;
																									obj_t BgL_arg1332z00_1511;
																									obj_t BgL_arg1333z00_1512;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1348z00_1463;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1349z00_1464;
																											obj_t BgL_arg1351z00_1465;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1352z00_1466;
																												BgL_arg1352z00_1466 =
																													MAKE_YOUNG_PAIR
																													(BgL_oidz00_1340,
																													BNIL);
																												BgL_arg1349z00_1464 =
																													MAKE_YOUNG_PAIR
																													(BgL_idzd2ze3objz31_1342,
																													BgL_arg1352z00_1466);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1361z00_1467;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_list1362z00_1468;
																													BgL_list1362z00_1468 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(11),
																														BNIL);
																													BgL_arg1361z00_1467 =
																														BGl_makezd2privatezd2sexpz00zzast_privatez00
																														(CNST_TABLE_REF(31),
																														CNST_TABLE_REF(32),
																														BgL_list1362z00_1468);
																												}
																												BgL_arg1351z00_1465 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1361z00_1467,
																													BNIL);
																											}
																											BgL_arg1348z00_1463 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1349z00_1464,
																												BgL_arg1351z00_1465);
																										}
																										BgL_arg1326z00_1506 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(23),
																											BgL_arg1348z00_1463);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1364z00_1457;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1367z00_1458;
																											obj_t BgL_arg1370z00_1459;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1371z00_1460;
																												BgL_arg1371z00_1460 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(11),
																													BNIL);
																												BgL_arg1367z00_1458 =
																													MAKE_YOUNG_PAIR
																													(BgL_tobjzd2ze3idz31_1356,
																													BgL_arg1371z00_1460);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1375z00_1461;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_list1376z00_1462;
																													BgL_list1376z00_1462 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(11),
																														BNIL);
																													BgL_arg1375z00_1461 =
																														BGl_makezd2privatezd2sexpz00zzast_privatez00
																														(CNST_TABLE_REF(31),
																														BgL_idz00_1339,
																														BgL_list1376z00_1462);
																												}
																												BgL_arg1370z00_1459 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1375z00_1461,
																													BNIL);
																											}
																											BgL_arg1364z00_1457 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1367z00_1458,
																												BgL_arg1370z00_1459);
																										}
																										BgL_arg1327z00_1507 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(23),
																											BgL_arg1364z00_1457);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1378z00_1451;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1379z00_1452;
																											obj_t BgL_arg1380z00_1453;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1408z00_1454;
																												BgL_arg1408z00_1454 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(29),
																													BNIL);
																												BgL_arg1379z00_1452 =
																													MAKE_YOUNG_PAIR
																													(BgL_tidzf3zf3_1364,
																													BgL_arg1408z00_1454);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1410z00_1455;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_list1411z00_1456;
																													BgL_list1411z00_1456 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(11),
																														BNIL);
																													BgL_arg1410z00_1455 =
																														BGl_makezd2privatezd2sexpz00zzast_privatez00
																														(CNST_TABLE_REF(30),
																														BgL_idz00_1339,
																														BgL_list1411z00_1456);
																												}
																												BgL_arg1380z00_1453 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1410z00_1455,
																													BNIL);
																											}
																											BgL_arg1378z00_1451 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1379z00_1452,
																												BgL_arg1380z00_1453);
																										}
																										BgL_arg1328z00_1508 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(23),
																											BgL_arg1378z00_1451);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1421z00_1442;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1422z00_1443;
																											obj_t BgL_arg1434z00_1444;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1437z00_1445;
																												BgL_arg1437z00_1445 =
																													MAKE_YOUNG_PAIR
																													(BgL_oidz00_1340,
																													BNIL);
																												BgL_arg1422z00_1443 =
																													MAKE_YOUNG_PAIR
																													(BgL_tidzd2lengthzd2_1372,
																													BgL_arg1437z00_1445);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1448z00_1446;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_list1449z00_1447;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1453z00_1448;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1454z00_1449;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1472z00_1450;
																																BgL_arg1472z00_1450
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(11), BNIL);
																																BgL_arg1454z00_1449
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string1738z00zzforeign_jtypez00,
																																	BgL_arg1472z00_1450);
																															}
																															BgL_arg1453z00_1448
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(15),
																																BgL_arg1454z00_1449);
																														}
																														BgL_list1449z00_1447
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_itemzd2typezd2idz00_1374,
																															BgL_arg1453z00_1448);
																													}
																													BgL_arg1448z00_1446 =
																														BGl_makezd2privatezd2sexpz00zzast_privatez00
																														(CNST_TABLE_REF(28),
																														BgL_idz00_1339,
																														BgL_list1449z00_1447);
																												}
																												BgL_arg1434z00_1444 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1448z00_1446,
																													BNIL);
																											}
																											BgL_arg1421z00_1442 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1422z00_1443,
																												BgL_arg1434z00_1444);
																										}
																										BgL_arg1329z00_1509 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(23),
																											BgL_arg1421z00_1442);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1485z00_1431;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1489z00_1432;
																											obj_t BgL_arg1502z00_1433;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1509z00_1434;
																												BgL_arg1509z00_1434 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(25),
																													BNIL);
																												BgL_arg1489z00_1432 =
																													MAKE_YOUNG_PAIR
																													(BgL_tmakezd2idzd2_1384,
																													BgL_arg1509z00_1434);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1513z00_1435;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_list1514z00_1436;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1516z00_1437;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1535z00_1438;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1540z00_1439;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1544z00_1440;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1546z00_1441;
																																		BgL_arg1546z00_1441
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(26),
																																			BNIL);
																																		BgL_arg1544z00_1440
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BFALSE,
																																			BgL_arg1546z00_1441);
																																	}
																																	BgL_arg1540z00_1439
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string1738z00zzforeign_jtypez00,
																																		BgL_arg1544z00_1440);
																																}
																																BgL_arg1535z00_1438
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string1738z00zzforeign_jtypez00,
																																	BgL_arg1540z00_1439);
																															}
																															BgL_arg1516z00_1437
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(15),
																																BgL_arg1535z00_1438);
																														}
																														BgL_list1514z00_1436
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_itemzd2typezd2idz00_1374,
																															BgL_arg1516z00_1437);
																													}
																													BgL_arg1513z00_1435 =
																														BGl_makezd2privatezd2sexpz00zzast_privatez00
																														(CNST_TABLE_REF(27),
																														BgL_idz00_1339,
																														BgL_list1514z00_1436);
																												}
																												BgL_arg1502z00_1433 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1513z00_1435,
																													BNIL);
																											}
																											BgL_arg1485z00_1431 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1489z00_1432,
																												BgL_arg1502z00_1433);
																										}
																										BgL_arg1331z00_1510 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(23),
																											BgL_arg1485z00_1431);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1552z00_1420;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1553z00_1421;
																											obj_t BgL_arg1559z00_1422;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1561z00_1423;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1564z00_1424;
																													BgL_arg1564z00_1424 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(20),
																														BNIL);
																													BgL_arg1561z00_1423 =
																														MAKE_YOUNG_PAIR
																														(BgL_oidz00_1340,
																														BgL_arg1564z00_1424);
																												}
																												BgL_arg1553z00_1421 =
																													MAKE_YOUNG_PAIR
																													(BgL_tidzd2refzd2_1392,
																													BgL_arg1561z00_1423);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1565z00_1425;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_list1566z00_1426;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1571z00_1427;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1573z00_1428;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1575z00_1429;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1576z00_1430;
																																	BgL_arg1576z00_1430
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(21), BNIL);
																																	BgL_arg1575z00_1429
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(11),
																																		BgL_arg1576z00_1430);
																																}
																																BgL_arg1573z00_1428
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string1738z00zzforeign_jtypez00,
																																	BgL_arg1575z00_1429);
																															}
																															BgL_arg1571z00_1427
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(15),
																																BgL_arg1573z00_1428);
																														}
																														BgL_list1566z00_1426
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_itemzd2typezd2idz00_1374,
																															BgL_arg1571z00_1427);
																													}
																													BgL_arg1565z00_1425 =
																														BGl_makezd2privatezd2sexpz00zzast_privatez00
																														(CNST_TABLE_REF(24),
																														BgL_idz00_1339,
																														BgL_list1566z00_1426);
																												}
																												BgL_arg1559z00_1422 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1565z00_1425,
																													BNIL);
																											}
																											BgL_arg1552z00_1420 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1553z00_1421,
																												BgL_arg1559z00_1422);
																										}
																										BgL_arg1332z00_1511 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(23),
																											BgL_arg1552z00_1420);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1584z00_1407;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1585z00_1408;
																											obj_t BgL_arg1589z00_1409;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1591z00_1410;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1593z00_1411;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1594z00_1412;
																														BgL_arg1594z00_1412
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_oitemzd2typezd2idz00_1376,
																															BNIL);
																														BgL_arg1593z00_1411
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(20),
																															BgL_arg1594z00_1412);
																													}
																													BgL_arg1591z00_1410 =
																														MAKE_YOUNG_PAIR
																														(BgL_oidz00_1340,
																														BgL_arg1593z00_1411);
																												}
																												BgL_arg1585z00_1408 =
																													MAKE_YOUNG_PAIR
																													(BgL_idzd2setz12zc0_1393,
																													BgL_arg1591z00_1410);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1595z00_1413;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_list1596z00_1414;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1602z00_1415;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1605z00_1416;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1606z00_1417;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1609z00_1418;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1611z00_1419;
																																		BgL_arg1611z00_1419
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(16),
																																			BNIL);
																																		BgL_arg1609z00_1418
																																			=
																																			MAKE_YOUNG_PAIR
																																			(CNST_TABLE_REF
																																			(21),
																																			BgL_arg1611z00_1419);
																																	}
																																	BgL_arg1606z00_1417
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(11),
																																		BgL_arg1609z00_1418);
																																}
																																BgL_arg1605z00_1416
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string1738z00zzforeign_jtypez00,
																																	BgL_arg1606z00_1417);
																															}
																															BgL_arg1602z00_1415
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(15),
																																BgL_arg1605z00_1416);
																														}
																														BgL_list1596z00_1414
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_itemzd2typezd2idz00_1374,
																															BgL_arg1602z00_1415);
																													}
																													BgL_arg1595z00_1413 =
																														BGl_makezd2privatezd2sexpz00zzast_privatez00
																														(CNST_TABLE_REF(22),
																														BgL_idz00_1339,
																														BgL_list1596z00_1414);
																												}
																												BgL_arg1589z00_1409 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1595z00_1413,
																													BNIL);
																											}
																											BgL_arg1584z00_1407 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1585z00_1408,
																												BgL_arg1589z00_1409);
																										}
																										BgL_arg1333z00_1512 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(23),
																											BgL_arg1584z00_1407);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_list1334z00_1513;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1335z00_1514;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1339z00_1515;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1340z00_1516;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1342z00_1517;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1343z00_1518;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1346z00_1519;
																																BgL_arg1346z00_1519
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1333z00_1512,
																																	BNIL);
																																BgL_arg1343z00_1518
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1332z00_1511,
																																	BgL_arg1346z00_1519);
																															}
																															BgL_arg1342z00_1517
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1331z00_1510,
																																BgL_arg1343z00_1518);
																														}
																														BgL_arg1340z00_1516
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1329z00_1509,
																															BgL_arg1342z00_1517);
																													}
																													BgL_arg1339z00_1515 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1328z00_1508,
																														BgL_arg1340z00_1516);
																												}
																												BgL_arg1335z00_1514 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1327z00_1507,
																													BgL_arg1339z00_1515);
																											}
																											BgL_list1334z00_1513 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1326z00_1506,
																												BgL_arg1335z00_1514);
																										}
																										return BgL_list1334z00_1513;
																									}
																								}
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_jtypez00(void)
	{
		{	/* Foreign/jtype.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zzforeign_ctypez00(396356879L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
			return
				BGl_modulezd2initializa7ationz75zzforeign_libraryz00(419430976L,
				BSTRING_TO_STRING(BGl_string1739z00zzforeign_jtypez00));
		}

	}

#ifdef __cplusplus
}
#endif
