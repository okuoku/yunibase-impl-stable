/*===========================================================================*/
/*   (Foreign/cptr.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/cptr.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_CPOINTER_TYPE_DEFINITIONS
#define BGL_FOREIGN_CPOINTER_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_caliasz00_bgl
	{
		bool_t BgL_arrayzf3zf3;
	}                *BgL_caliasz00_bglt;

	typedef struct BgL_cptrz00_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
		struct BgL_typez00_bgl *BgL_pointzd2tozd2;
		bool_t BgL_arrayzf3zf3;
	}              *BgL_cptrz00_bglt;


#endif													// BGL_FOREIGN_CPOINTER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzforeign_cpointerz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	extern obj_t BGl_caliasz00zzforeign_ctypez00;
	static obj_t BGl_toplevelzd2initzd2zzforeign_cpointerz00(void);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzforeign_cpointerz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzforeign_cpointerz00(void);
	extern obj_t BGl_cptrz00zzforeign_ctypez00;
	extern obj_t BGl_cstructz00zzforeign_ctypez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzforeign_cpointerz00(void);
	extern obj_t
		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_cpointerz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_makezd2protozd2inlinez00zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzforeign_cpointerz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_cpointerz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_cpointerz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_cpointerz00(void);
	static obj_t
		BGl_z62makezd2ctypezd2accessesz121094z70zzforeign_cpointerz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2definezd2inlinez00zzforeign_libraryz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t __cnst[37];


	   
		 
		DEFINE_STRING(BGl_string1760z00zzforeign_cpointerz00,
		BgL_bgl_string1760za700za7za7f1799za7, "make-ctype-accesses!", 20);
	      DEFINE_STRING(BGl_string1761z00zzforeign_cpointerz00,
		BgL_bgl_string1761za700za7za7f1800za7, "&(((", 4);
	      DEFINE_STRING(BGl_string1762z00zzforeign_cpointerz00,
		BgL_bgl_string1762za700za7za7f1801za7, ")($1))[ $2 ])", 13);
	      DEFINE_STRING(BGl_string1763z00zzforeign_cpointerz00,
		BgL_bgl_string1763za700za7za7f1802za7, "((", 2);
	      DEFINE_STRING(BGl_string1764z00zzforeign_cpointerz00,
		BgL_bgl_string1764za700za7za7f1803za7, ")($1))[ $2 ]", 12);
	      DEFINE_STRING(BGl_string1765z00zzforeign_cpointerz00,
		BgL_bgl_string1765za700za7za7f1804za7, "(((", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1759z00zzforeign_cpointerz00,
		BgL_bgl_za762makeza7d2ctypeza71805za7,
		BGl_z62makezd2ctypezd2accessesz121094z70zzforeign_cpointerz00, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string1766z00zzforeign_cpointerz00,
		BgL_bgl_string1766za700za7za7f1806za7, ")($1))[ $2 ] = *($3), BUNSPEC)",
		30);
	      DEFINE_STRING(BGl_string1767z00zzforeign_cpointerz00,
		BgL_bgl_string1767za700za7za7f1807za7, ")($1))[ $2 ] = $3, BUNSPEC)", 27);
	      DEFINE_STRING(BGl_string1768z00zzforeign_cpointerz00,
		BgL_bgl_string1768za700za7za7f1808za7, " * $1 )", 7);
	      DEFINE_STRING(BGl_string1769z00zzforeign_cpointerz00,
		BgL_bgl_string1769za700za7za7f1809za7, " )", 2);
	      DEFINE_STRING(BGl_string1770z00zzforeign_cpointerz00,
		BgL_bgl_string1770za700za7za7f1810za7, "sizeof( ", 8);
	      DEFINE_STRING(BGl_string1771z00zzforeign_cpointerz00,
		BgL_bgl_string1771za700za7za7f1811za7, ")GC_MALLOC( ", 12);
	      DEFINE_STRING(BGl_string1772z00zzforeign_cpointerz00,
		BgL_bgl_string1772za700za7za7f1812za7, "(", 1);
	      DEFINE_STRING(BGl_string1773z00zzforeign_cpointerz00,
		BgL_bgl_string1773za700za7za7f1813za7, " ))", 3);
	      DEFINE_STRING(BGl_string1774z00zzforeign_cpointerz00,
		BgL_bgl_string1774za700za7za7f1814za7, ")0L)", 4);
	      DEFINE_STRING(BGl_string1775z00zzforeign_cpointerz00,
		BgL_bgl_string1775za700za7za7f1815za7, "($1 == (", 8);
	      DEFINE_STRING(BGl_string1776z00zzforeign_cpointerz00,
		BgL_bgl_string1776za700za7za7f1816za7, ")FOREIGN_TO_COBJ", 16);
	      DEFINE_STRING(BGl_string1777z00zzforeign_cpointerz00,
		BgL_bgl_string1777za700za7za7f1817za7, "cobj_to_foreign", 15);
	      DEFINE_STRING(BGl_string1778z00zzforeign_cpointerz00,
		BgL_bgl_string1778za700za7za7f1818za7, "foreign_cpointer", 16);
	      DEFINE_STRING(BGl_string1779z00zzforeign_cpointerz00,
		BgL_bgl_string1779za700za7za7f1819za7,
		"predicate-of static foreign -null? symbol macro if eq? quote foreign-id foreign? o::obj (free-pragma::bool \"($1 == $2)\" o1 o2) o2 o1 = ?::bool free-pragma::bool -null?::bool |free-pragma::| make-null- len len::long make- pragma v ::obj i free-pragma i::long o * -set! -ref ::bool ? -> ",
		285);
	extern obj_t BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_cpointerz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzforeign_cpointerz00(long
		BgL_checksumz00_1506, char *BgL_fromz00_1507)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_cpointerz00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_cpointerz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_cpointerz00();
					BGl_libraryzd2moduleszd2initz00zzforeign_cpointerz00();
					BGl_cnstzd2initzd2zzforeign_cpointerz00();
					BGl_importedzd2moduleszd2initz00zzforeign_cpointerz00();
					BGl_methodzd2initzd2zzforeign_cpointerz00();
					return BGl_toplevelzd2initzd2zzforeign_cpointerz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_cpointerz00(void)
	{
		{	/* Foreign/cptr.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_cpointer");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"foreign_cpointer");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"foreign_cpointer");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_cpointer");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"foreign_cpointer");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"foreign_cpointer");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"foreign_cpointer");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"foreign_cpointer");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"foreign_cpointer");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzforeign_cpointerz00(void)
	{
		{	/* Foreign/cptr.scm 15 */
			{	/* Foreign/cptr.scm 15 */
				obj_t BgL_cportz00_1161;

				{	/* Foreign/cptr.scm 15 */
					obj_t BgL_stringz00_1168;

					BgL_stringz00_1168 = BGl_string1779z00zzforeign_cpointerz00;
					{	/* Foreign/cptr.scm 15 */
						obj_t BgL_startz00_1169;

						BgL_startz00_1169 = BINT(0L);
						{	/* Foreign/cptr.scm 15 */
							obj_t BgL_endz00_1170;

							BgL_endz00_1170 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1168)));
							{	/* Foreign/cptr.scm 15 */

								BgL_cportz00_1161 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1168, BgL_startz00_1169, BgL_endz00_1170);
				}}}}
				{
					long BgL_iz00_1162;

					BgL_iz00_1162 = 36L;
				BgL_loopz00_1163:
					if ((BgL_iz00_1162 == -1L))
						{	/* Foreign/cptr.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Foreign/cptr.scm 15 */
							{	/* Foreign/cptr.scm 15 */
								obj_t BgL_arg1798z00_1164;

								{	/* Foreign/cptr.scm 15 */

									{	/* Foreign/cptr.scm 15 */
										obj_t BgL_locationz00_1166;

										BgL_locationz00_1166 = BBOOL(((bool_t) 0));
										{	/* Foreign/cptr.scm 15 */

											BgL_arg1798z00_1164 =
												BGl_readz00zz__readerz00(BgL_cportz00_1161,
												BgL_locationz00_1166);
										}
									}
								}
								{	/* Foreign/cptr.scm 15 */
									int BgL_tmpz00_1535;

									BgL_tmpz00_1535 = (int) (BgL_iz00_1162);
									CNST_TABLE_SET(BgL_tmpz00_1535, BgL_arg1798z00_1164);
							}}
							{	/* Foreign/cptr.scm 15 */
								int BgL_auxz00_1167;

								BgL_auxz00_1167 = (int) ((BgL_iz00_1162 - 1L));
								{
									long BgL_iz00_1540;

									BgL_iz00_1540 = (long) (BgL_auxz00_1167);
									BgL_iz00_1162 = BgL_iz00_1540;
									goto BgL_loopz00_1163;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_cpointerz00(void)
	{
		{	/* Foreign/cptr.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzforeign_cpointerz00(void)
	{
		{	/* Foreign/cptr.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_cpointerz00(void)
	{
		{	/* Foreign/cptr.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_cpointerz00(void)
	{
		{	/* Foreign/cptr.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_cpointerz00(void)
	{
		{	/* Foreign/cptr.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
				BGl_cptrz00zzforeign_ctypez00, BGl_proc1759z00zzforeign_cpointerz00,
				BGl_string1760z00zzforeign_cpointerz00);
		}

	}



/* &make-ctype-accesses!1094 */
	obj_t BGl_z62makezd2ctypezd2accessesz121094z70zzforeign_cpointerz00(obj_t
		BgL_envz00_1153, obj_t BgL_whatz00_1154, obj_t BgL_whoz00_1155,
		obj_t BgL_locz00_1156)
	{
		{	/* Foreign/cptr.scm 31 */
			{	/* Tools/trace.sch 53 */
				BgL_typez00_bglt BgL_btypez00_1174;

				{
					BgL_cptrz00_bglt BgL_auxz00_1544;

					{
						obj_t BgL_auxz00_1545;

						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_tmpz00_1546;

							BgL_tmpz00_1546 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_whatz00_1154));
							BgL_auxz00_1545 = BGL_OBJECT_WIDENING(BgL_tmpz00_1546);
						}
						BgL_auxz00_1544 = ((BgL_cptrz00_bglt) BgL_auxz00_1545);
					}
					BgL_btypez00_1174 =
						(((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_1544))->BgL_btypez00);
				}
				{	/* Tools/trace.sch 53 */
					obj_t BgL_idz00_1175;

					BgL_idz00_1175 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_whoz00_1155)))->BgL_idz00);
					{	/* Tools/trace.sch 53 */
						obj_t BgL_bidz00_1176;

						BgL_bidz00_1176 =
							(((BgL_typez00_bglt) COBJECT(BgL_btypez00_1174))->BgL_idz00);
						{	/* Tools/trace.sch 53 */
							obj_t BgL_idzd2ze3bidz31_1177;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_list1710z00_1178;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1711z00_1179;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1714z00_1180;

										BgL_arg1714z00_1180 =
											MAKE_YOUNG_PAIR(BgL_bidz00_1176, BNIL);
										BgL_arg1711z00_1179 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1714z00_1180);
									}
									BgL_list1710z00_1178 =
										MAKE_YOUNG_PAIR(BgL_idz00_1175, BgL_arg1711z00_1179);
								}
								BgL_idzd2ze3bidz31_1177 =
									BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
									(BgL_list1710z00_1178);
							}
							{	/* Tools/trace.sch 53 */
								obj_t BgL_bidzd2ze3idz31_1181;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_list1706z00_1182;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1708z00_1183;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1709z00_1184;

											BgL_arg1709z00_1184 =
												MAKE_YOUNG_PAIR(BgL_idz00_1175, BNIL);
											BgL_arg1708z00_1183 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1709z00_1184);
										}
										BgL_list1706z00_1182 =
											MAKE_YOUNG_PAIR(BgL_bidz00_1176, BgL_arg1708z00_1183);
									}
									BgL_bidzd2ze3idz31_1181 =
										BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
										(BgL_list1706z00_1182);
								}
								{	/* Tools/trace.sch 53 */
									obj_t BgL_bidzf3zf3_1185;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1702z00_1186;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1703z00_1187;
											obj_t BgL_arg1705z00_1188;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1455z00_1189;

												BgL_arg1455z00_1189 = SYMBOL_TO_STRING(BgL_idz00_1175);
												BgL_arg1703z00_1187 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1189);
											}
											{	/* Tools/trace.sch 53 */
												obj_t BgL_symbolz00_1190;

												BgL_symbolz00_1190 = CNST_TABLE_REF(1);
												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1455z00_1191;

													BgL_arg1455z00_1191 =
														SYMBOL_TO_STRING(BgL_symbolz00_1190);
													BgL_arg1705z00_1188 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1191);
												}
											}
											BgL_arg1702z00_1186 =
												string_append(BgL_arg1703z00_1187, BgL_arg1705z00_1188);
										}
										BgL_bidzf3zf3_1185 = bstring_to_symbol(BgL_arg1702z00_1186);
									}
									{	/* Tools/trace.sch 53 */
										obj_t BgL_bidzf3zd2boolz21_1192;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1699z00_1193;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1700z00_1194;
												obj_t BgL_arg1701z00_1195;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1455z00_1196;

													BgL_arg1455z00_1196 =
														SYMBOL_TO_STRING(BgL_bidzf3zf3_1185);
													BgL_arg1700z00_1194 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1196);
												}
												{	/* Tools/trace.sch 53 */
													obj_t BgL_symbolz00_1197;

													BgL_symbolz00_1197 = CNST_TABLE_REF(2);
													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1455z00_1198;

														BgL_arg1455z00_1198 =
															SYMBOL_TO_STRING(BgL_symbolz00_1197);
														BgL_arg1701z00_1195 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1198);
													}
												}
												BgL_arg1699z00_1193 =
													string_append(BgL_arg1700z00_1194,
													BgL_arg1701z00_1195);
											}
											BgL_bidzf3zd2boolz21_1192 =
												bstring_to_symbol(BgL_arg1699z00_1193);
										}
										{	/* Tools/trace.sch 53 */
											obj_t BgL_namez00_1199;

											BgL_namez00_1199 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_whoz00_1155)))->
												BgL_namez00);
											{	/* Tools/trace.sch 53 */
												obj_t BgL_namezd2sanszd2z42z42_1200;

												BgL_namezd2sanszd2z42z42_1200 =
													BGl_stringzd2sanszd2z42z42zztype_toolsz00
													(BgL_namez00_1199);
												{	/* Tools/trace.sch 53 */
													BgL_typez00_bglt BgL_pointzd2tozd2_1201;

													{
														BgL_cptrz00_bglt BgL_auxz00_1582;

														{
															obj_t BgL_auxz00_1583;

															{	/* Tools/trace.sch 53 */
																BgL_objectz00_bglt BgL_tmpz00_1584;

																BgL_tmpz00_1584 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_whatz00_1154));
																BgL_auxz00_1583 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_1584);
															}
															BgL_auxz00_1582 =
																((BgL_cptrz00_bglt) BgL_auxz00_1583);
														}
														BgL_pointzd2tozd2_1201 =
															(((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_1582))->
															BgL_pointzd2tozd2);
													}
													{	/* Tools/trace.sch 53 */
														obj_t BgL_pointzd2tozd2idz00_1202;

														BgL_pointzd2tozd2idz00_1202 =
															(((BgL_typez00_bglt)
																COBJECT(BgL_pointzd2tozd2_1201))->BgL_idz00);
														{	/* Tools/trace.sch 53 */
															obj_t BgL_pointzd2tozd2namez00_1203;

															BgL_pointzd2tozd2namez00_1203 =
																(((BgL_typez00_bglt)
																	COBJECT(BgL_pointzd2tozd2_1201))->
																BgL_namez00);
															{	/* Tools/trace.sch 53 */
																obj_t
																	BgL_pointzd2tozd2namezd2sanszd2z42z42_1204;
																BgL_pointzd2tozd2namezd2sanszd2z42z42_1204 =
																	BGl_stringzd2sanszd2z42z42zztype_toolsz00
																	(BgL_pointzd2tozd2namez00_1203);
																{	/* Tools/trace.sch 53 */

																	{

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1103z00_1437;
																			obj_t BgL_arg1104z00_1438;
																			obj_t BgL_arg1114z00_1439;
																			obj_t BgL_arg1115z00_1440;
																			obj_t BgL_arg1122z00_1441;
																			obj_t BgL_arg1123z00_1442;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_list1133z00_1443;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1137z00_1444;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1138z00_1445;

																						BgL_arg1138z00_1445 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																							BNIL);
																						BgL_arg1137z00_1444 =
																							MAKE_YOUNG_PAIR(BgL_idz00_1175,
																							BgL_arg1138z00_1445);
																					}
																					BgL_list1133z00_1443 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
																						BgL_arg1137z00_1444);
																				}
																				BgL_arg1103z00_1437 =
																					BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																					(BgL_list1133z00_1443);
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1140z00_1446;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1141z00_1447;
																					obj_t BgL_arg1142z00_1448;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_symbolz00_1449;

																						BgL_symbolz00_1449 =
																							CNST_TABLE_REF(16);
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1455z00_1450;

																							BgL_arg1455z00_1450 =
																								SYMBOL_TO_STRING
																								(BgL_symbolz00_1449);
																							BgL_arg1141z00_1447 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_1450);
																						}
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1451;

																						BgL_arg1455z00_1451 =
																							SYMBOL_TO_STRING(BgL_idz00_1175);
																						BgL_arg1142z00_1448 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1451);
																					}
																					BgL_arg1140z00_1446 =
																						string_append(BgL_arg1141z00_1447,
																						BgL_arg1142z00_1448);
																				}
																				BgL_arg1104z00_1438 =
																					bstring_to_symbol
																					(BgL_arg1140z00_1446);
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1143z00_1452;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1145z00_1453;
																					obj_t BgL_arg1148z00_1454;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1455;

																						BgL_arg1455z00_1455 =
																							SYMBOL_TO_STRING(BgL_idz00_1175);
																						BgL_arg1145z00_1453 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1455);
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_symbolz00_1456;

																						BgL_symbolz00_1456 =
																							CNST_TABLE_REF(33);
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1455z00_1457;

																							BgL_arg1455z00_1457 =
																								SYMBOL_TO_STRING
																								(BgL_symbolz00_1456);
																							BgL_arg1148z00_1454 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_1457);
																						}
																					}
																					BgL_arg1143z00_1452 =
																						string_append(BgL_arg1145z00_1453,
																						BgL_arg1148z00_1454);
																				}
																				BgL_arg1114z00_1439 =
																					bstring_to_symbol
																					(BgL_arg1143z00_1452);
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1149z00_1458;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1152z00_1459;
																					obj_t BgL_arg1153z00_1460;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_symbolz00_1461;

																						BgL_symbolz00_1461 =
																							CNST_TABLE_REF(13);
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1455z00_1462;

																							BgL_arg1455z00_1462 =
																								SYMBOL_TO_STRING
																								(BgL_symbolz00_1461);
																							BgL_arg1152z00_1459 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_1462);
																						}
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1463;

																						BgL_arg1455z00_1463 =
																							SYMBOL_TO_STRING(BgL_idz00_1175);
																						BgL_arg1153z00_1460 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1463);
																					}
																					BgL_arg1149z00_1458 =
																						string_append(BgL_arg1152z00_1459,
																						BgL_arg1153z00_1460);
																				}
																				BgL_arg1115z00_1440 =
																					bstring_to_symbol
																					(BgL_arg1149z00_1458);
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1154z00_1464;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1157z00_1465;
																					obj_t BgL_arg1158z00_1466;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1467;

																						BgL_arg1455z00_1467 =
																							SYMBOL_TO_STRING(BgL_idz00_1175);
																						BgL_arg1157z00_1465 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1467);
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_symbolz00_1468;

																						BgL_symbolz00_1468 =
																							CNST_TABLE_REF(3);
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1455z00_1469;

																							BgL_arg1455z00_1469 =
																								SYMBOL_TO_STRING
																								(BgL_symbolz00_1468);
																							BgL_arg1158z00_1466 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_1469);
																						}
																					}
																					BgL_arg1154z00_1464 =
																						string_append(BgL_arg1157z00_1465,
																						BgL_arg1158z00_1466);
																				}
																				BgL_arg1122z00_1441 =
																					bstring_to_symbol
																					(BgL_arg1154z00_1464);
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1162z00_1470;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1164z00_1471;
																					obj_t BgL_arg1166z00_1472;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1473;

																						BgL_arg1455z00_1473 =
																							SYMBOL_TO_STRING(BgL_idz00_1175);
																						BgL_arg1164z00_1471 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1473);
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_symbolz00_1474;

																						BgL_symbolz00_1474 =
																							CNST_TABLE_REF(4);
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1455z00_1475;

																							BgL_arg1455z00_1475 =
																								SYMBOL_TO_STRING
																								(BgL_symbolz00_1474);
																							BgL_arg1166z00_1472 =
																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																								(BgL_arg1455z00_1475);
																						}
																					}
																					BgL_arg1162z00_1470 =
																						string_append(BgL_arg1164z00_1471,
																						BgL_arg1166z00_1472);
																				}
																				BgL_arg1123z00_1442 =
																					bstring_to_symbol
																					(BgL_arg1162z00_1470);
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_list1124z00_1476;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1125z00_1477;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1126z00_1478;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1127z00_1479;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1129z00_1480;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1131z00_1481;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1132z00_1482;

																										BgL_arg1132z00_1482 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1123z00_1442,
																											BNIL);
																										BgL_arg1131z00_1481 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1122z00_1441,
																											BgL_arg1132z00_1482);
																									}
																									BgL_arg1129z00_1480 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1115z00_1440,
																										BgL_arg1131z00_1481);
																								}
																								BgL_arg1127z00_1479 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1114z00_1439,
																									BgL_arg1129z00_1480);
																							}
																							BgL_arg1126z00_1478 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1104z00_1438,
																								BgL_arg1127z00_1479);
																						}
																						BgL_arg1125z00_1477 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1103z00_1437,
																							BgL_arg1126z00_1478);
																					}
																					BgL_list1124z00_1476 =
																						MAKE_YOUNG_PAIR(BgL_bidzf3zf3_1185,
																						BgL_arg1125z00_1477);
																				}
																				BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
																					(BgL_list1124z00_1476);
																			}
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1171z00_1483;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1172z00_1484;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1182z00_1485;
																					obj_t BgL_arg1183z00_1486;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1218z00_1431;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1219z00_1432;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1220z00_1433;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1221z00_1434;
																									obj_t BgL_arg1223z00_1435;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1225z00_1436;

																										BgL_arg1225z00_1436 =
																											MAKE_YOUNG_PAIR
																											(BgL_idz00_1175, BNIL);
																										BgL_arg1221z00_1434 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(32),
																											BgL_arg1225z00_1436);
																									}
																									BgL_arg1223z00_1435 =
																										MAKE_YOUNG_PAIR
																										(BGl_string1777z00zzforeign_cpointerz00,
																										BNIL);
																									BgL_arg1220z00_1433 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1221z00_1434,
																										BgL_arg1223z00_1435);
																								}
																								BgL_arg1219z00_1432 =
																									MAKE_YOUNG_PAIR
																									(BgL_idzd2ze3bidz31_1177,
																									BgL_arg1220z00_1433);
																							}
																							BgL_arg1218z00_1431 =
																								MAKE_YOUNG_PAIR(BgL_bidz00_1176,
																								BgL_arg1219z00_1432);
																						}
																						BgL_arg1182z00_1485 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(31), BgL_arg1218z00_1431);
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_tmpz00_1651;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_mnamez00_1425;

																							BgL_mnamez00_1425 =
																								string_append_3
																								(BGl_string1772z00zzforeign_cpointerz00,
																								BgL_namezd2sanszd2z42z42_1200,
																								BGl_string1776z00zzforeign_cpointerz00);
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1227z00_1426;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1228z00_1427;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1229z00_1428;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1230z00_1429;
																											obj_t BgL_arg1231z00_1430;

																											BgL_arg1230z00_1429 =
																												MAKE_YOUNG_PAIR
																												(BgL_bidz00_1176, BNIL);
																											BgL_arg1231z00_1430 =
																												MAKE_YOUNG_PAIR
																												(BgL_mnamez00_1425,
																												BNIL);
																											BgL_arg1229z00_1428 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1230z00_1429,
																												BgL_arg1231z00_1430);
																										}
																										BgL_arg1228z00_1427 =
																											MAKE_YOUNG_PAIR
																											(BgL_bidzd2ze3idz31_1181,
																											BgL_arg1229z00_1428);
																									}
																									BgL_arg1227z00_1426 =
																										MAKE_YOUNG_PAIR
																										(BgL_idz00_1175,
																										BgL_arg1228z00_1427);
																								}
																								BgL_tmpz00_1651 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(31), BgL_arg1227z00_1426);
																							}
																						}
																						BgL_arg1183z00_1486 =
																							MAKE_YOUNG_PAIR(BgL_tmpz00_1651,
																							BNIL);
																					}
																					BgL_arg1172z00_1484 =
																						MAKE_YOUNG_PAIR(BgL_arg1182z00_1485,
																						BgL_arg1183z00_1486);
																				}
																				BgL_arg1171z00_1483 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(34),
																					BgL_arg1172z00_1484);
																			}
																			BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																				(BgL_arg1171z00_1483);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1188z00_1487;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1189z00_1488;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1190z00_1489;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1191z00_1490;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1193z00_1491;

																							BgL_arg1193z00_1491 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(10), BNIL);
																							BgL_arg1191z00_1490 =
																								MAKE_YOUNG_PAIR
																								(BgL_bidzf3zd2boolz21_1192,
																								BgL_arg1193z00_1491);
																						}
																						BgL_arg1190z00_1489 =
																							BGl_makezd2protozd2inlinez00zzforeign_libraryz00
																							(BgL_arg1191z00_1490);
																					}
																					BgL_arg1189z00_1488 =
																						MAKE_YOUNG_PAIR(BgL_arg1190z00_1489,
																						BNIL);
																				}
																				BgL_arg1188z00_1487 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(35),
																					BgL_arg1189z00_1488);
																			}
																			BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																				(BgL_arg1188z00_1487);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1194z00_1492;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1196z00_1493;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1197z00_1494;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1198z00_1495;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1199z00_1496;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1200z00_1497;

																								BgL_arg1200z00_1497 =
																									MAKE_YOUNG_PAIR
																									(BgL_bidz00_1176, BNIL);
																								BgL_arg1199z00_1496 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(36), BgL_arg1200z00_1497);
																							}
																							BgL_arg1198z00_1495 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1199z00_1496, BNIL);
																						}
																						BgL_arg1197z00_1494 =
																							MAKE_YOUNG_PAIR
																							(BgL_bidzf3zf3_1185,
																							BgL_arg1198z00_1495);
																					}
																					BgL_arg1196z00_1493 =
																						MAKE_YOUNG_PAIR(BgL_arg1197z00_1494,
																						BNIL);
																				}
																				BgL_arg1194z00_1492 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(12),
																					BgL_arg1196z00_1493);
																			}
																			BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																				(BgL_arg1194z00_1492);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1201z00_1498;
																			obj_t BgL_arg1202z00_1499;

																			{

																				{	/* Tools/trace.sch 53 */
																					bool_t BgL_test1824z00_1682;

																					{	/* Tools/trace.sch 53 */
																						bool_t BgL_test1825z00_1683;

																						{	/* Tools/trace.sch 53 */
																							bool_t BgL_test1826z00_1684;

																							{
																								BgL_cptrz00_bglt
																									BgL_auxz00_1685;
																								{
																									obj_t BgL_auxz00_1686;

																									{	/* Tools/trace.sch 53 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_1687;
																										BgL_tmpz00_1687 =
																											((BgL_objectz00_bglt) (
																												(BgL_typez00_bglt)
																												BgL_whatz00_1154));
																										BgL_auxz00_1686 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_1687);
																									}
																									BgL_auxz00_1685 =
																										((BgL_cptrz00_bglt)
																										BgL_auxz00_1686);
																								}
																								BgL_test1826z00_1684 =
																									(((BgL_cptrz00_bglt)
																										COBJECT(BgL_auxz00_1685))->
																									BgL_arrayzf3zf3);
																							}
																							if (BgL_test1826z00_1684)
																								{	/* Tools/trace.sch 53 */
																									BgL_test1825z00_1683 =
																										(
																										((obj_t)
																											((BgL_typez00_bglt)
																												BgL_whoz00_1155)) ==
																										((obj_t) ((BgL_typez00_bglt)
																												BgL_whatz00_1154)));
																								}
																							else
																								{	/* Tools/trace.sch 53 */
																									BgL_test1825z00_1683 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test1825z00_1683)
																							{	/* Tools/trace.sch 53 */
																								BgL_test1824z00_1682 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Tools/trace.sch 53 */
																								bool_t BgL_test1827z00_1698;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_1356;

																									BgL_classz00_1356 =
																										BGl_caliasz00zzforeign_ctypez00;
																									{	/* Tools/trace.sch 53 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_1357;
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_tmpz00_1699;

																											BgL_tmpz00_1699 =
																												((obj_t)
																												((BgL_typez00_bglt)
																													BgL_whoz00_1155));
																											BgL_arg1807z00_1357 =
																												(BgL_objectz00_bglt)
																												(BgL_tmpz00_1699);
																										}
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Tools/trace.sch 53 */
																												long BgL_idxz00_1358;

																												BgL_idxz00_1358 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_1357);
																												BgL_test1827z00_1698 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_1358 +
																															2L)) ==
																													BgL_classz00_1356);
																											}
																										else
																											{	/* Tools/trace.sch 53 */
																												bool_t
																													BgL_res1749z00_1361;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_oclassz00_1362;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1815z00_1363;
																														long
																															BgL_arg1816z00_1364;
																														BgL_arg1815z00_1363
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Tools/trace.sch 53 */
																															long
																																BgL_arg1817z00_1365;
																															BgL_arg1817z00_1365
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_1357);
																															BgL_arg1816z00_1364
																																=
																																(BgL_arg1817z00_1365
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_1362 =
																															VECTOR_REF
																															(BgL_arg1815z00_1363,
																															BgL_arg1816z00_1364);
																													}
																													{	/* Tools/trace.sch 53 */
																														bool_t
																															BgL__ortest_1115z00_1366;
																														BgL__ortest_1115z00_1366
																															=
																															(BgL_classz00_1356
																															==
																															BgL_oclassz00_1362);
																														if (BgL__ortest_1115z00_1366)
																															{	/* Tools/trace.sch 53 */
																																BgL_res1749z00_1361
																																	=
																																	BgL__ortest_1115z00_1366;
																															}
																														else
																															{	/* Tools/trace.sch 53 */
																																long
																																	BgL_odepthz00_1367;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1804z00_1368;
																																	BgL_arg1804z00_1368
																																		=
																																		(BgL_oclassz00_1362);
																																	BgL_odepthz00_1367
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_1368);
																																}
																																if (
																																	(2L <
																																		BgL_odepthz00_1367))
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1802z00_1369;
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_arg1803z00_1370;
																																			BgL_arg1803z00_1370
																																				=
																																				(BgL_oclassz00_1362);
																																			BgL_arg1802z00_1369
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_1370,
																																				2L);
																																		}
																																		BgL_res1749z00_1361
																																			=
																																			(BgL_arg1802z00_1369
																																			==
																																			BgL_classz00_1356);
																																	}
																																else
																																	{	/* Tools/trace.sch 53 */
																																		BgL_res1749z00_1361
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test1827z00_1698 =
																													BgL_res1749z00_1361;
																											}
																									}
																								}
																								if (BgL_test1827z00_1698)
																									{
																										BgL_caliasz00_bglt
																											BgL_auxz00_1722;
																										{
																											obj_t BgL_auxz00_1723;

																											{	/* Tools/trace.sch 53 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_1724;
																												BgL_tmpz00_1724 =
																													((BgL_objectz00_bglt)
																													((BgL_typez00_bglt) (
																															(BgL_typez00_bglt)
																															BgL_whoz00_1155)));
																												BgL_auxz00_1723 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_1724);
																											}
																											BgL_auxz00_1722 =
																												((BgL_caliasz00_bglt)
																												BgL_auxz00_1723);
																										}
																										BgL_test1824z00_1682 =
																											(((BgL_caliasz00_bglt)
																												COBJECT
																												(BgL_auxz00_1722))->
																											BgL_arrayzf3zf3);
																									}
																								else
																									{	/* Tools/trace.sch 53 */
																										BgL_test1824z00_1682 =
																											((bool_t) 0);
																									}
																							}
																					}
																					if (BgL_test1824z00_1682)
																						{	/* Tools/trace.sch 53 */
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1485z00_1336;
																								obj_t BgL_arg1489z00_1337;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1502z00_1338;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1509z00_1339;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1513z00_1340;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1514z00_1341;
																												obj_t
																													BgL_arg1516z00_1342;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_symbolz00_1343;
																													BgL_symbolz00_1343 =
																														CNST_TABLE_REF(13);
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1455z00_1344;
																														BgL_arg1455z00_1344
																															=
																															SYMBOL_TO_STRING
																															(BgL_symbolz00_1343);
																														BgL_arg1514z00_1341
																															=
																															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																															(BgL_arg1455z00_1344);
																													}
																												}
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1455z00_1345;
																													BgL_arg1455z00_1345 =
																														SYMBOL_TO_STRING
																														(BgL_idz00_1175);
																													BgL_arg1516z00_1342 =
																														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																														(BgL_arg1455z00_1345);
																												}
																												BgL_arg1513z00_1340 =
																													string_append
																													(BgL_arg1514z00_1341,
																													BgL_arg1516z00_1342);
																											}
																											BgL_arg1509z00_1339 =
																												bstring_to_symbol
																												(BgL_arg1513z00_1340);
																										}
																										BgL_arg1502z00_1338 =
																											BGl_makezd2typedzd2identz00zzast_identz00
																											(BgL_arg1509z00_1339,
																											BgL_idz00_1175);
																									}
																									BgL_arg1485z00_1336 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1502z00_1338, BNIL);
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1535z00_1346;
																									obj_t BgL_arg1540z00_1347;

																									BgL_arg1535z00_1346 =
																										BGl_makezd2typedzd2identz00zzast_identz00
																										(CNST_TABLE_REF(8),
																										BgL_idz00_1175);
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1544z00_1348;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1546z00_1349;

																											BgL_arg1546z00_1349 =
																												(((BgL_typez00_bglt)
																													COBJECT((
																															(BgL_typez00_bglt)
																															BgL_whoz00_1155)))->
																												BgL_siza7eza7);
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_list1547z00_1350;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1552z00_1351;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1553z00_1352;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1559z00_1353;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1561z00_1354;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1564z00_1355;
																																	BgL_arg1564z00_1355
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string1773z00zzforeign_cpointerz00,
																																		BNIL);
																																	BgL_arg1561z00_1354
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1546z00_1349,
																																		BgL_arg1564z00_1355);
																																}
																																BgL_arg1559z00_1353
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string1770z00zzforeign_cpointerz00,
																																	BgL_arg1561z00_1354);
																															}
																															BgL_arg1553z00_1352
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string1771z00zzforeign_cpointerz00,
																																BgL_arg1559z00_1353);
																														}
																														BgL_arg1552z00_1351
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_namezd2sanszd2z42z42_1200,
																															BgL_arg1553z00_1352);
																													}
																													BgL_list1547z00_1350 =
																														MAKE_YOUNG_PAIR
																														(BGl_string1772z00zzforeign_cpointerz00,
																														BgL_arg1552z00_1351);
																												}
																												BgL_arg1544z00_1348 =
																													BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																													(BgL_list1547z00_1350);
																											}
																										}
																										BgL_arg1540z00_1347 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1544z00_1348,
																											BNIL);
																									}
																									BgL_arg1489z00_1337 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1535z00_1346,
																										BgL_arg1540z00_1347);
																								}
																								BgL_arg1201z00_1498 =
																									BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																									(BgL_arg1485z00_1336,
																									BgL_arg1489z00_1337);
																							}
																						}
																					else
																						{	/* Tools/trace.sch 53 */
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1371z00_1314;
																								obj_t BgL_arg1375z00_1315;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1376z00_1316;
																									obj_t BgL_arg1377z00_1317;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1378z00_1318;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1379z00_1319;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1380z00_1320;
																												obj_t
																													BgL_arg1408z00_1321;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_symbolz00_1322;
																													BgL_symbolz00_1322 =
																														CNST_TABLE_REF(13);
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1455z00_1323;
																														BgL_arg1455z00_1323
																															=
																															SYMBOL_TO_STRING
																															(BgL_symbolz00_1322);
																														BgL_arg1380z00_1320
																															=
																															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																															(BgL_arg1455z00_1323);
																													}
																												}
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1455z00_1324;
																													BgL_arg1455z00_1324 =
																														SYMBOL_TO_STRING
																														(BgL_idz00_1175);
																													BgL_arg1408z00_1321 =
																														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																														(BgL_arg1455z00_1324);
																												}
																												BgL_arg1379z00_1319 =
																													string_append
																													(BgL_arg1380z00_1320,
																													BgL_arg1408z00_1321);
																											}
																											BgL_arg1378z00_1318 =
																												bstring_to_symbol
																												(BgL_arg1379z00_1319);
																										}
																										BgL_arg1376z00_1316 =
																											BGl_makezd2typedzd2identz00zzast_identz00
																											(BgL_arg1378z00_1318,
																											BgL_idz00_1175);
																									}
																									BgL_arg1377z00_1317 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(14), BNIL);
																									BgL_arg1371z00_1314 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1376z00_1316,
																										BgL_arg1377z00_1317);
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1410z00_1325;
																									obj_t BgL_arg1421z00_1326;

																									BgL_arg1410z00_1325 =
																										BGl_makezd2typedzd2identz00zzast_identz00
																										(CNST_TABLE_REF(8),
																										BgL_idz00_1175);
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1422z00_1327;
																										obj_t BgL_arg1434z00_1328;

																										{	/* Tools/trace.sch 53 */
																											obj_t
																												BgL_list1435z00_1329;
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1437z00_1330;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1448z00_1331;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1453z00_1332;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1454z00_1333;
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_arg1472z00_1334;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1473z00_1335;
																																	BgL_arg1473z00_1335
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string1768z00zzforeign_cpointerz00,
																																		BNIL);
																																	BgL_arg1472z00_1334
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string1769z00zzforeign_cpointerz00,
																																		BgL_arg1473z00_1335);
																																}
																																BgL_arg1454z00_1333
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_pointzd2tozd2namezd2sanszd2z42z42_1204,
																																	BgL_arg1472z00_1334);
																															}
																															BgL_arg1453z00_1332
																																=
																																MAKE_YOUNG_PAIR
																																(BGl_string1770z00zzforeign_cpointerz00,
																																BgL_arg1454z00_1333);
																														}
																														BgL_arg1448z00_1331
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string1771z00zzforeign_cpointerz00,
																															BgL_arg1453z00_1332);
																													}
																													BgL_arg1437z00_1330 =
																														MAKE_YOUNG_PAIR
																														(BgL_namezd2sanszd2z42z42_1200,
																														BgL_arg1448z00_1331);
																												}
																												BgL_list1435z00_1329 =
																													MAKE_YOUNG_PAIR
																													(BGl_string1772z00zzforeign_cpointerz00,
																													BgL_arg1437z00_1330);
																											}
																											BgL_arg1422z00_1327 =
																												BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																												(BgL_list1435z00_1329);
																										}
																										BgL_arg1434z00_1328 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(15),
																											BNIL);
																										BgL_arg1421z00_1326 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1422z00_1327,
																											BgL_arg1434z00_1328);
																									}
																									BgL_arg1375z00_1315 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1410z00_1325,
																										BgL_arg1421z00_1326);
																								}
																								BgL_arg1201z00_1498 =
																									BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																									(BgL_arg1371z00_1314,
																									BgL_arg1375z00_1315);
																							}
																						}
																				}
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1203z00_1500;
																				obj_t BgL_arg1206z00_1501;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1307z00_1402;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1308z00_1403;
																						obj_t BgL_arg1310z00_1404;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_list1311z00_1405;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1312z00_1406;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1314z00_1407;

																									BgL_arg1314z00_1407 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(20), BNIL);
																									BgL_arg1312z00_1406 =
																										MAKE_YOUNG_PAIR
																										(BgL_idz00_1175,
																										BgL_arg1314z00_1407);
																								}
																								BgL_list1311z00_1405 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(21), BgL_arg1312z00_1406);
																							}
																							BgL_arg1308z00_1403 =
																								BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																								(BgL_list1311z00_1405);
																						}
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1315z00_1408;
																							obj_t BgL_arg1316z00_1409;

																							BgL_arg1315z00_1408 =
																								BGl_makezd2typedzd2identz00zzast_identz00
																								(CNST_TABLE_REF(22),
																								BgL_idz00_1175);
																							BgL_arg1316z00_1409 =
																								MAKE_YOUNG_PAIR
																								(BGl_makezd2typedzd2identz00zzast_identz00
																								(CNST_TABLE_REF(23),
																									BgL_idz00_1175), BNIL);
																							BgL_arg1310z00_1404 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1315z00_1408,
																								BgL_arg1316z00_1409);
																						}
																						BgL_arg1307z00_1402 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1308z00_1403,
																							BgL_arg1310z00_1404);
																					}
																					BgL_arg1203z00_1500 =
																						BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																						(BgL_arg1307z00_1402,
																						CNST_TABLE_REF(24));
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1208z00_1502;
																					obj_t BgL_arg1209z00_1503;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1319z00_1389;
																						obj_t BgL_arg1320z00_1390;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1321z00_1391;
																							obj_t BgL_arg1322z00_1392;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1323z00_1393;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1325z00_1394;
																									obj_t BgL_arg1326z00_1395;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1455z00_1396;

																										BgL_arg1455z00_1396 =
																											SYMBOL_TO_STRING
																											(BgL_idz00_1175);
																										BgL_arg1325z00_1394 =
																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																											(BgL_arg1455z00_1396);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_symbolz00_1397;

																										BgL_symbolz00_1397 =
																											CNST_TABLE_REF(18);
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1455z00_1398;

																											BgL_arg1455z00_1398 =
																												SYMBOL_TO_STRING
																												(BgL_symbolz00_1397);
																											BgL_arg1326z00_1395 =
																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																												(BgL_arg1455z00_1398);
																										}
																									}
																									BgL_arg1323z00_1393 =
																										string_append
																										(BgL_arg1325z00_1394,
																										BgL_arg1326z00_1395);
																								}
																								BgL_arg1321z00_1391 =
																									bstring_to_symbol
																									(BgL_arg1323z00_1393);
																							}
																							BgL_arg1322z00_1392 =
																								MAKE_YOUNG_PAIR
																								(BGl_makezd2typedzd2identz00zzast_identz00
																								(CNST_TABLE_REF(6),
																									BgL_idz00_1175), BNIL);
																							BgL_arg1319z00_1389 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1321z00_1391,
																								BgL_arg1322z00_1392);
																						}
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1328z00_1399;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1329z00_1400;
																								obj_t BgL_arg1331z00_1401;

																								BgL_arg1329z00_1400 =
																									string_append_3
																									(BGl_string1775z00zzforeign_cpointerz00,
																									BgL_namezd2sanszd2z42z42_1200,
																									BGl_string1774z00zzforeign_cpointerz00);
																								BgL_arg1331z00_1401 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(6), BNIL);
																								BgL_arg1328z00_1399 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1329z00_1400,
																									BgL_arg1331z00_1401);
																							}
																							BgL_arg1320z00_1390 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(19), BgL_arg1328z00_1399);
																						}
																						BgL_arg1208z00_1502 =
																							BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																							(BgL_arg1319z00_1389,
																							BgL_arg1320z00_1390);
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1210z00_1504;
																						obj_t BgL_arg1212z00_1505;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1333z00_1371;
																							obj_t BgL_arg1335z00_1372;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1339z00_1373;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1340z00_1374;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1342z00_1375;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1343z00_1376;
																											obj_t BgL_arg1346z00_1377;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_symbolz00_1378;
																												BgL_symbolz00_1378 =
																													CNST_TABLE_REF(16);
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1455z00_1379;
																													BgL_arg1455z00_1379 =
																														SYMBOL_TO_STRING
																														(BgL_symbolz00_1378);
																													BgL_arg1343z00_1376 =
																														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																														(BgL_arg1455z00_1379);
																												}
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1455z00_1380;
																												BgL_arg1455z00_1380 =
																													SYMBOL_TO_STRING
																													(BgL_idz00_1175);
																												BgL_arg1346z00_1377 =
																													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																													(BgL_arg1455z00_1380);
																											}
																											BgL_arg1342z00_1375 =
																												string_append
																												(BgL_arg1343z00_1376,
																												BgL_arg1346z00_1377);
																										}
																										BgL_arg1340z00_1374 =
																											bstring_to_symbol
																											(BgL_arg1342z00_1375);
																									}
																									BgL_arg1339z00_1373 =
																										BGl_makezd2typedzd2identz00zzast_identz00
																										(BgL_arg1340z00_1374,
																										BgL_idz00_1175);
																								}
																								BgL_arg1333z00_1371 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1339z00_1373, BNIL);
																							}
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1348z00_1381;
																								obj_t BgL_arg1349z00_1382;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1351z00_1383;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1352z00_1384;
																										obj_t BgL_arg1361z00_1385;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_symbolz00_1386;

																											BgL_symbolz00_1386 =
																												CNST_TABLE_REF(17);
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1455z00_1387;
																												BgL_arg1455z00_1387 =
																													SYMBOL_TO_STRING
																													(BgL_symbolz00_1386);
																												BgL_arg1352z00_1384 =
																													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																													(BgL_arg1455z00_1387);
																											}
																										}
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1455z00_1388;

																											BgL_arg1455z00_1388 =
																												SYMBOL_TO_STRING
																												(BgL_idz00_1175);
																											BgL_arg1361z00_1385 =
																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																												(BgL_arg1455z00_1388);
																										}
																										BgL_arg1351z00_1383 =
																											string_append
																											(BgL_arg1352z00_1384,
																											BgL_arg1361z00_1385);
																									}
																									BgL_arg1348z00_1381 =
																										bstring_to_symbol
																										(BgL_arg1351z00_1383);
																								}
																								BgL_arg1349z00_1382 =
																									MAKE_YOUNG_PAIR
																									(string_append_3
																									(BGl_string1763z00zzforeign_cpointerz00,
																										BgL_namezd2sanszd2z42z42_1200,
																										BGl_string1774z00zzforeign_cpointerz00),
																									BNIL);
																								BgL_arg1335z00_1372 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1348z00_1381,
																									BgL_arg1349z00_1382);
																							}
																							BgL_arg1210z00_1504 =
																								BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																								(BgL_arg1333z00_1371,
																								BgL_arg1335z00_1372);
																						}
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_auxz00_1858;
																							obj_t BgL_tmpz00_1833;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_refzd2idzd2_1213;
																								obj_t BgL_setzd2idzd2_1214;
																								obj_t
																									BgL_refzd2typezd2idz00_1215;
																								obj_t BgL_refzd2fmtzd2_1216;
																								obj_t BgL_setzd2fmtzd2_1217;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1654z00_1218;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1661z00_1219;
																										obj_t BgL_arg1663z00_1220;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1455z00_1221;

																											BgL_arg1455z00_1221 =
																												SYMBOL_TO_STRING
																												(BgL_idz00_1175);
																											BgL_arg1661z00_1219 =
																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																												(BgL_arg1455z00_1221);
																										}
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_symbolz00_1222;

																											BgL_symbolz00_1222 =
																												CNST_TABLE_REF(3);
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1455z00_1223;
																												BgL_arg1455z00_1223 =
																													SYMBOL_TO_STRING
																													(BgL_symbolz00_1222);
																												BgL_arg1663z00_1220 =
																													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																													(BgL_arg1455z00_1223);
																											}
																										}
																										BgL_arg1654z00_1218 =
																											string_append
																											(BgL_arg1661z00_1219,
																											BgL_arg1663z00_1220);
																									}
																									BgL_refzd2idzd2_1213 =
																										bstring_to_symbol
																										(BgL_arg1654z00_1218);
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1675z00_1224;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1678z00_1225;
																										obj_t BgL_arg1681z00_1226;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1455z00_1227;

																											BgL_arg1455z00_1227 =
																												SYMBOL_TO_STRING
																												(BgL_idz00_1175);
																											BgL_arg1678z00_1225 =
																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																												(BgL_arg1455z00_1227);
																										}
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_symbolz00_1228;

																											BgL_symbolz00_1228 =
																												CNST_TABLE_REF(4);
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1455z00_1229;
																												BgL_arg1455z00_1229 =
																													SYMBOL_TO_STRING
																													(BgL_symbolz00_1228);
																												BgL_arg1681z00_1226 =
																													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																													(BgL_arg1455z00_1229);
																											}
																										}
																										BgL_arg1675z00_1224 =
																											string_append
																											(BgL_arg1678z00_1225,
																											BgL_arg1681z00_1226);
																									}
																									BgL_setzd2idzd2_1214 =
																										bstring_to_symbol
																										(BgL_arg1675z00_1224);
																								}
																								{	/* Tools/trace.sch 53 */
																									bool_t BgL_test1832z00_1873;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_classz00_1230;

																										BgL_classz00_1230 =
																											BGl_cstructz00zzforeign_ctypez00;
																										{	/* Tools/trace.sch 53 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_1231;
																											{	/* Tools/trace.sch 53 */
																												obj_t BgL_tmpz00_1874;

																												BgL_tmpz00_1874 =
																													((obj_t)
																													((BgL_objectz00_bglt)
																														BgL_pointzd2tozd2_1201));
																												BgL_arg1807z00_1231 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_1874);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Tools/trace.sch 53 */
																													long BgL_idxz00_1232;

																													BgL_idxz00_1232 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_1231);
																													BgL_test1832z00_1873 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_1232 +
																																2L)) ==
																														BgL_classz00_1230);
																												}
																											else
																												{	/* Tools/trace.sch 53 */
																													bool_t
																														BgL_res1750z00_1235;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_oclassz00_1236;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1815z00_1237;
																															long
																																BgL_arg1816z00_1238;
																															BgL_arg1815z00_1237
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Tools/trace.sch 53 */
																																long
																																	BgL_arg1817z00_1239;
																																BgL_arg1817z00_1239
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_1231);
																																BgL_arg1816z00_1238
																																	=
																																	(BgL_arg1817z00_1239
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_1236
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_1237,
																																BgL_arg1816z00_1238);
																														}
																														{	/* Tools/trace.sch 53 */
																															bool_t
																																BgL__ortest_1115z00_1240;
																															BgL__ortest_1115z00_1240
																																=
																																(BgL_classz00_1230
																																==
																																BgL_oclassz00_1236);
																															if (BgL__ortest_1115z00_1240)
																																{	/* Tools/trace.sch 53 */
																																	BgL_res1750z00_1235
																																		=
																																		BgL__ortest_1115z00_1240;
																																}
																															else
																																{	/* Tools/trace.sch 53 */
																																	long
																																		BgL_odepthz00_1241;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1804z00_1242;
																																		BgL_arg1804z00_1242
																																			=
																																			(BgL_oclassz00_1236);
																																		BgL_odepthz00_1241
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_1242);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_1241))
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_arg1802z00_1243;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_arg1803z00_1244;
																																				BgL_arg1803z00_1244
																																					=
																																					(BgL_oclassz00_1236);
																																				BgL_arg1802z00_1243
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_1244,
																																					2L);
																																			}
																																			BgL_res1750z00_1235
																																				=
																																				(BgL_arg1802z00_1243
																																				==
																																				BgL_classz00_1230);
																																		}
																																	else
																																		{	/* Tools/trace.sch 53 */
																																			BgL_res1750z00_1235
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test1832z00_1873 =
																														BgL_res1750z00_1235;
																												}
																										}
																									}
																									if (BgL_test1832z00_1873)
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1688z00_1245;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1689z00_1246;
																												obj_t
																													BgL_arg1691z00_1247;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1455z00_1248;
																													BgL_arg1455z00_1248 =
																														SYMBOL_TO_STRING
																														(BgL_pointzd2tozd2idz00_1202);
																													BgL_arg1689z00_1246 =
																														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																														(BgL_arg1455z00_1248);
																												}
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_symbolz00_1249;
																													BgL_symbolz00_1249 =
																														CNST_TABLE_REF(5);
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1455z00_1250;
																														BgL_arg1455z00_1250
																															=
																															SYMBOL_TO_STRING
																															(BgL_symbolz00_1249);
																														BgL_arg1691z00_1247
																															=
																															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																															(BgL_arg1455z00_1250);
																													}
																												}
																												BgL_arg1688z00_1245 =
																													string_append
																													(BgL_arg1689z00_1246,
																													BgL_arg1691z00_1247);
																											}
																											BgL_refzd2typezd2idz00_1215
																												=
																												bstring_to_symbol
																												(BgL_arg1688z00_1245);
																										}
																									else
																										{	/* Tools/trace.sch 53 */
																											BgL_refzd2typezd2idz00_1215
																												=
																												BgL_pointzd2tozd2idz00_1202;
																										}
																								}
																								{	/* Tools/trace.sch 53 */
																									bool_t BgL_test1836z00_1904;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_classz00_1251;

																										BgL_classz00_1251 =
																											BGl_cstructz00zzforeign_ctypez00;
																										{	/* Tools/trace.sch 53 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_1252;
																											{	/* Tools/trace.sch 53 */
																												obj_t BgL_tmpz00_1905;

																												BgL_tmpz00_1905 =
																													((obj_t)
																													((BgL_objectz00_bglt)
																														BgL_pointzd2tozd2_1201));
																												BgL_arg1807z00_1252 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_1905);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Tools/trace.sch 53 */
																													long BgL_idxz00_1253;

																													BgL_idxz00_1253 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_1252);
																													BgL_test1836z00_1904 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_1253 +
																																2L)) ==
																														BgL_classz00_1251);
																												}
																											else
																												{	/* Tools/trace.sch 53 */
																													bool_t
																														BgL_res1751z00_1256;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_oclassz00_1257;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1815z00_1258;
																															long
																																BgL_arg1816z00_1259;
																															BgL_arg1815z00_1258
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Tools/trace.sch 53 */
																																long
																																	BgL_arg1817z00_1260;
																																BgL_arg1817z00_1260
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_1252);
																																BgL_arg1816z00_1259
																																	=
																																	(BgL_arg1817z00_1260
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_1257
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_1258,
																																BgL_arg1816z00_1259);
																														}
																														{	/* Tools/trace.sch 53 */
																															bool_t
																																BgL__ortest_1115z00_1261;
																															BgL__ortest_1115z00_1261
																																=
																																(BgL_classz00_1251
																																==
																																BgL_oclassz00_1257);
																															if (BgL__ortest_1115z00_1261)
																																{	/* Tools/trace.sch 53 */
																																	BgL_res1751z00_1256
																																		=
																																		BgL__ortest_1115z00_1261;
																																}
																															else
																																{	/* Tools/trace.sch 53 */
																																	long
																																		BgL_odepthz00_1262;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1804z00_1263;
																																		BgL_arg1804z00_1263
																																			=
																																			(BgL_oclassz00_1257);
																																		BgL_odepthz00_1262
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_1263);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_1262))
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_arg1802z00_1264;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_arg1803z00_1265;
																																				BgL_arg1803z00_1265
																																					=
																																					(BgL_oclassz00_1257);
																																				BgL_arg1802z00_1264
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_1265,
																																					2L);
																																			}
																																			BgL_res1751z00_1256
																																				=
																																				(BgL_arg1802z00_1264
																																				==
																																				BgL_classz00_1251);
																																		}
																																	else
																																		{	/* Tools/trace.sch 53 */
																																			BgL_res1751z00_1256
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test1836z00_1904 =
																														BgL_res1751z00_1256;
																												}
																										}
																									}
																									if (BgL_test1836z00_1904)
																										{	/* Tools/trace.sch 53 */
																											BgL_refzd2fmtzd2_1216 =
																												string_append_3
																												(BGl_string1761z00zzforeign_cpointerz00,
																												BgL_namezd2sanszd2z42z42_1200,
																												BGl_string1762z00zzforeign_cpointerz00);
																										}
																									else
																										{	/* Tools/trace.sch 53 */
																											BgL_refzd2fmtzd2_1216 =
																												string_append_3
																												(BGl_string1763z00zzforeign_cpointerz00,
																												BgL_namezd2sanszd2z42z42_1200,
																												BGl_string1764z00zzforeign_cpointerz00);
																										}
																								}
																								{	/* Tools/trace.sch 53 */
																									bool_t BgL_test1840z00_1930;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_classz00_1266;

																										BgL_classz00_1266 =
																											BGl_cstructz00zzforeign_ctypez00;
																										{	/* Tools/trace.sch 53 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_1267;
																											{	/* Tools/trace.sch 53 */
																												obj_t BgL_tmpz00_1931;

																												BgL_tmpz00_1931 =
																													((obj_t)
																													((BgL_objectz00_bglt)
																														BgL_pointzd2tozd2_1201));
																												BgL_arg1807z00_1267 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_1931);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Tools/trace.sch 53 */
																													long BgL_idxz00_1268;

																													BgL_idxz00_1268 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_1267);
																													BgL_test1840z00_1930 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_1268 +
																																2L)) ==
																														BgL_classz00_1266);
																												}
																											else
																												{	/* Tools/trace.sch 53 */
																													bool_t
																														BgL_res1752z00_1271;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_oclassz00_1272;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1815z00_1273;
																															long
																																BgL_arg1816z00_1274;
																															BgL_arg1815z00_1273
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Tools/trace.sch 53 */
																																long
																																	BgL_arg1817z00_1275;
																																BgL_arg1817z00_1275
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_1267);
																																BgL_arg1816z00_1274
																																	=
																																	(BgL_arg1817z00_1275
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_1272
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_1273,
																																BgL_arg1816z00_1274);
																														}
																														{	/* Tools/trace.sch 53 */
																															bool_t
																																BgL__ortest_1115z00_1276;
																															BgL__ortest_1115z00_1276
																																=
																																(BgL_classz00_1266
																																==
																																BgL_oclassz00_1272);
																															if (BgL__ortest_1115z00_1276)
																																{	/* Tools/trace.sch 53 */
																																	BgL_res1752z00_1271
																																		=
																																		BgL__ortest_1115z00_1276;
																																}
																															else
																																{	/* Tools/trace.sch 53 */
																																	long
																																		BgL_odepthz00_1277;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1804z00_1278;
																																		BgL_arg1804z00_1278
																																			=
																																			(BgL_oclassz00_1272);
																																		BgL_odepthz00_1277
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_1278);
																																	}
																																	if (
																																		(2L <
																																			BgL_odepthz00_1277))
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_arg1802z00_1279;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_arg1803z00_1280;
																																				BgL_arg1803z00_1280
																																					=
																																					(BgL_oclassz00_1272);
																																				BgL_arg1802z00_1279
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_1280,
																																					2L);
																																			}
																																			BgL_res1752z00_1271
																																				=
																																				(BgL_arg1802z00_1279
																																				==
																																				BgL_classz00_1266);
																																		}
																																	else
																																		{	/* Tools/trace.sch 53 */
																																			BgL_res1752z00_1271
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test1840z00_1930 =
																														BgL_res1752z00_1271;
																												}
																										}
																									}
																									if (BgL_test1840z00_1930)
																										{	/* Tools/trace.sch 53 */
																											BgL_setzd2fmtzd2_1217 =
																												string_append_3
																												(BGl_string1765z00zzforeign_cpointerz00,
																												BgL_namezd2sanszd2z42z42_1200,
																												BGl_string1766z00zzforeign_cpointerz00);
																										}
																									else
																										{	/* Tools/trace.sch 53 */
																											BgL_setzd2fmtzd2_1217 =
																												string_append_3
																												(BGl_string1765z00zzforeign_cpointerz00,
																												BgL_namezd2sanszd2z42z42_1200,
																												BGl_string1767z00zzforeign_cpointerz00);
																										}
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1571z00_1281;
																									obj_t BgL_arg1573z00_1282;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1576z00_1283;
																										obj_t BgL_arg1584z00_1284;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1585z00_1285;
																											obj_t BgL_arg1589z00_1286;

																											BgL_arg1585z00_1285 =
																												BGl_makezd2typedzd2identz00zzast_identz00
																												(BgL_refzd2idzd2_1213,
																												BgL_refzd2typezd2idz00_1215);
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1591z00_1287;
																												obj_t
																													BgL_arg1593z00_1288;
																												BgL_arg1591z00_1287 =
																													BGl_makezd2typedzd2identz00zzast_identz00
																													(CNST_TABLE_REF(6),
																													BgL_idz00_1175);
																												BgL_arg1593z00_1288 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(7),
																													BNIL);
																												BgL_arg1589z00_1286 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1591z00_1287,
																													BgL_arg1593z00_1288);
																											}
																											BgL_arg1576z00_1283 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1585z00_1285,
																												BgL_arg1589z00_1286);
																										}
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1594z00_1289;
																											obj_t BgL_arg1595z00_1290;

																											BgL_arg1594z00_1289 =
																												BGl_makezd2typedzd2identz00zzast_identz00
																												(CNST_TABLE_REF(8),
																												BgL_refzd2typezd2idz00_1215);
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1602z00_1291;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1605z00_1292;
																													BgL_arg1605z00_1292 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(9),
																														BNIL);
																													BgL_arg1602z00_1291 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(6),
																														BgL_arg1605z00_1292);
																												}
																												BgL_arg1595z00_1290 =
																													MAKE_YOUNG_PAIR
																													(BgL_refzd2fmtzd2_1216,
																													BgL_arg1602z00_1291);
																											}
																											BgL_arg1584z00_1284 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1594z00_1289,
																												BgL_arg1595z00_1290);
																										}
																										BgL_arg1571z00_1281 =
																											BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																											(BgL_arg1576z00_1283,
																											BgL_arg1584z00_1284);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1606z00_1293;
																										obj_t BgL_arg1609z00_1294;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1611z00_1295;
																											obj_t BgL_arg1613z00_1296;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1615z00_1297;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1616z00_1298;
																													obj_t
																														BgL_arg1625z00_1299;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1455z00_1300;
																														BgL_arg1455z00_1300
																															=
																															SYMBOL_TO_STRING
																															(BgL_setzd2idzd2_1214);
																														BgL_arg1616z00_1298
																															=
																															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																															(BgL_arg1455z00_1300);
																													}
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_symbolz00_1301;
																														BgL_symbolz00_1301 =
																															CNST_TABLE_REF
																															(10);
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1455z00_1302;
																															BgL_arg1455z00_1302
																																=
																																SYMBOL_TO_STRING
																																(BgL_symbolz00_1301);
																															BgL_arg1625z00_1299
																																=
																																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																(BgL_arg1455z00_1302);
																														}
																													}
																													BgL_arg1615z00_1297 =
																														string_append
																														(BgL_arg1616z00_1298,
																														BgL_arg1625z00_1299);
																												}
																												BgL_arg1611z00_1295 =
																													bstring_to_symbol
																													(BgL_arg1615z00_1297);
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1626z00_1303;
																												obj_t
																													BgL_arg1627z00_1304;
																												BgL_arg1626z00_1303 =
																													BGl_makezd2typedzd2identz00zzast_identz00
																													(CNST_TABLE_REF(6),
																													BgL_idz00_1175);
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1629z00_1305;
																													BgL_arg1629z00_1305 =
																														MAKE_YOUNG_PAIR
																														(BGl_makezd2typedzd2identz00zzast_identz00
																														(CNST_TABLE_REF(11),
																															BgL_refzd2typezd2idz00_1215),
																														BNIL);
																													BgL_arg1627z00_1304 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(7),
																														BgL_arg1629z00_1305);
																												}
																												BgL_arg1613z00_1296 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1626z00_1303,
																													BgL_arg1627z00_1304);
																											}
																											BgL_arg1606z00_1293 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1611z00_1295,
																												BgL_arg1613z00_1296);
																										}
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1642z00_1306;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1646z00_1307;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1650z00_1308;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1651z00_1309;
																														BgL_arg1651z00_1309
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(11), BNIL);
																														BgL_arg1650z00_1308
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(9),
																															BgL_arg1651z00_1309);
																													}
																													BgL_arg1646z00_1307 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(6),
																														BgL_arg1650z00_1308);
																												}
																												BgL_arg1642z00_1306 =
																													MAKE_YOUNG_PAIR
																													(BgL_setzd2fmtzd2_1217,
																													BgL_arg1646z00_1307);
																											}
																											BgL_arg1609z00_1294 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(12),
																												BgL_arg1642z00_1306);
																										}
																										BgL_arg1573z00_1282 =
																											BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																											(BgL_arg1606z00_1293,
																											BgL_arg1609z00_1294);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_list1574z00_1310;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1575z00_1311;

																											BgL_arg1575z00_1311 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1573z00_1282,
																												BNIL);
																											BgL_list1574z00_1310 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1571z00_1281,
																												BgL_arg1575z00_1311);
																										}
																										BgL_auxz00_1858 =
																											BgL_list1574z00_1310;
																									}
																								}
																							}
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1233z00_1410;
																								obj_t BgL_arg1234z00_1411;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1236z00_1412;

																									BgL_arg1236z00_1412 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(25), BNIL);
																									BgL_arg1233z00_1410 =
																										MAKE_YOUNG_PAIR
																										(BgL_bidzf3zd2boolz21_1192,
																										BgL_arg1236z00_1412);
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1238z00_1413;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1239z00_1414;
																										obj_t BgL_arg1242z00_1415;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1244z00_1416;

																											BgL_arg1244z00_1416 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(6),
																												BNIL);
																											BgL_arg1239z00_1414 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(26),
																												BgL_arg1244z00_1416);
																										}
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1248z00_1417;
																											obj_t BgL_arg1249z00_1418;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1252z00_1419;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1268z00_1420;
																													obj_t
																														BgL_arg1272z00_1421;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1284z00_1422;
																														BgL_arg1284z00_1422
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(6), BNIL);
																														BgL_arg1268z00_1420
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(27),
																															BgL_arg1284z00_1422);
																													}
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1304z00_1423;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1305z00_1424;
																															BgL_arg1305z00_1424
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_bidz00_1176,
																																BNIL);
																															BgL_arg1304z00_1423
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(28),
																																BgL_arg1305z00_1424);
																														}
																														BgL_arg1272z00_1421
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1304z00_1423,
																															BNIL);
																													}
																													BgL_arg1252z00_1419 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1268z00_1420,
																														BgL_arg1272z00_1421);
																												}
																												BgL_arg1248z00_1417 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(29),
																													BgL_arg1252z00_1419);
																											}
																											BgL_arg1249z00_1418 =
																												MAKE_YOUNG_PAIR(BFALSE,
																												BNIL);
																											BgL_arg1242z00_1415 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1248z00_1417,
																												BgL_arg1249z00_1418);
																										}
																										BgL_arg1238z00_1413 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1239z00_1414,
																											BgL_arg1242z00_1415);
																									}
																									BgL_arg1234z00_1411 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(30),
																										BgL_arg1238z00_1413);
																								}
																								BgL_tmpz00_1833 =
																									BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																									(BgL_arg1233z00_1410,
																									BgL_arg1234z00_1411);
																							}
																							BgL_arg1212z00_1505 =
																								MAKE_YOUNG_PAIR(BgL_tmpz00_1833,
																								BgL_auxz00_1858);
																						}
																						BgL_arg1209z00_1503 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1210z00_1504,
																							BgL_arg1212z00_1505);
																					}
																					BgL_arg1206z00_1501 =
																						MAKE_YOUNG_PAIR(BgL_arg1208z00_1502,
																						BgL_arg1209z00_1503);
																				}
																				BgL_arg1202z00_1499 =
																					MAKE_YOUNG_PAIR(BgL_arg1203z00_1500,
																					BgL_arg1206z00_1501);
																			}
																			return
																				MAKE_YOUNG_PAIR(BgL_arg1201z00_1498,
																				BgL_arg1202z00_1499);
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_cpointerz00(void)
	{
		{	/* Foreign/cptr.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zzforeign_ctypez00(396356879L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zzforeign_libraryz00(419430976L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1778z00zzforeign_cpointerz00));
		}

	}

#ifdef __cplusplus
}
#endif
