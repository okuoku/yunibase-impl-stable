/*===========================================================================*/
/*   (Foreign/copaque.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/copaque.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_COPAQUE_TYPE_DEFINITIONS
#define BGL_FOREIGN_COPAQUE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_copaquez00_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
	}                 *BgL_copaquez00_bglt;


#endif													// BGL_FOREIGN_COPAQUE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzforeign_copaquez00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzforeign_copaquez00(void);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzforeign_copaquez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzforeign_copaquez00(void);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzforeign_copaquez00(void);
	extern obj_t
		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_copaquez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_makezd2protozd2inlinez00zzforeign_libraryz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzforeign_copaquez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_copaquez00(void);
	static obj_t
		BGl_z62makezd2ctypezd2accessesz121089z70zzforeign_copaquez00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_copaquez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_copaquez00(void);
	extern obj_t BGl_makezd2definezd2inlinez00zzforeign_libraryz00(obj_t, obj_t);
	extern obj_t BGl_copaquez00zzforeign_ctypez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t __cnst[23];


	   
		 
		DEFINE_STRING(BGl_string1317z00zzforeign_copaquez00,
		BgL_bgl_string1317za700za7za7f1324za7, "make-ctype-accesses!", 20);
	      DEFINE_STRING(BGl_string1318z00zzforeign_copaquez00,
		BgL_bgl_string1318za700za7za7f1325za7, "(", 1);
	      DEFINE_STRING(BGl_string1319z00zzforeign_copaquez00,
		BgL_bgl_string1319za700za7za7f1326za7, ")FOREIGN_TO_COBJ", 16);
	      DEFINE_STRING(BGl_string1320z00zzforeign_copaquez00,
		BgL_bgl_string1320za700za7za7f1327za7, "cobj_to_foreign", 15);
	      DEFINE_STRING(BGl_string1321z00zzforeign_copaquez00,
		BgL_bgl_string1321za700za7za7f1328za7, "foreign_copaque", 15);
	      DEFINE_STRING(BGl_string1322z00zzforeign_copaquez00,
		BgL_bgl_string1322za700za7za7f1329za7,
		"pragma predicate-of static ::obj foreign symbol macro if eq? quote foreign-id foreign? o o::obj (pragma::bool \"($1 == $2)\" o1 o2) o2 void* o1 = ?::bool ::bool ? -> ",
		164);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1316z00zzforeign_copaquez00,
		BgL_bgl_za762makeza7d2ctypeza71330za7,
		BGl_z62makezd2ctypezd2accessesz121089z70zzforeign_copaquez00, 0L, BUNSPEC,
		3);
	extern obj_t BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_copaquez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzforeign_copaquez00(long
		BgL_checksumz00_812, char *BgL_fromz00_813)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_copaquez00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_copaquez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_copaquez00();
					BGl_libraryzd2moduleszd2initz00zzforeign_copaquez00();
					BGl_cnstzd2initzd2zzforeign_copaquez00();
					BGl_importedzd2moduleszd2initz00zzforeign_copaquez00();
					BGl_methodzd2initzd2zzforeign_copaquez00();
					return BGl_toplevelzd2initzd2zzforeign_copaquez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_copaquez00(void)
	{
		{	/* Foreign/copaque.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"foreign_copaque");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"foreign_copaque");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_copaque");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"foreign_copaque");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_copaque");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"foreign_copaque");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzforeign_copaquez00(void)
	{
		{	/* Foreign/copaque.scm 15 */
			{	/* Foreign/copaque.scm 15 */
				obj_t BgL_cportz00_708;

				{	/* Foreign/copaque.scm 15 */
					obj_t BgL_stringz00_715;

					BgL_stringz00_715 = BGl_string1322z00zzforeign_copaquez00;
					{	/* Foreign/copaque.scm 15 */
						obj_t BgL_startz00_716;

						BgL_startz00_716 = BINT(0L);
						{	/* Foreign/copaque.scm 15 */
							obj_t BgL_endz00_717;

							BgL_endz00_717 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_715)));
							{	/* Foreign/copaque.scm 15 */

								BgL_cportz00_708 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_715, BgL_startz00_716, BgL_endz00_717);
				}}}}
				{
					long BgL_iz00_709;

					BgL_iz00_709 = 22L;
				BgL_loopz00_710:
					if ((BgL_iz00_709 == -1L))
						{	/* Foreign/copaque.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Foreign/copaque.scm 15 */
							{	/* Foreign/copaque.scm 15 */
								obj_t BgL_arg1323z00_711;

								{	/* Foreign/copaque.scm 15 */

									{	/* Foreign/copaque.scm 15 */
										obj_t BgL_locationz00_713;

										BgL_locationz00_713 = BBOOL(((bool_t) 0));
										{	/* Foreign/copaque.scm 15 */

											BgL_arg1323z00_711 =
												BGl_readz00zz__readerz00(BgL_cportz00_708,
												BgL_locationz00_713);
										}
									}
								}
								{	/* Foreign/copaque.scm 15 */
									int BgL_tmpz00_838;

									BgL_tmpz00_838 = (int) (BgL_iz00_709);
									CNST_TABLE_SET(BgL_tmpz00_838, BgL_arg1323z00_711);
							}}
							{	/* Foreign/copaque.scm 15 */
								int BgL_auxz00_714;

								BgL_auxz00_714 = (int) ((BgL_iz00_709 - 1L));
								{
									long BgL_iz00_843;

									BgL_iz00_843 = (long) (BgL_auxz00_714);
									BgL_iz00_709 = BgL_iz00_843;
									goto BgL_loopz00_710;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_copaquez00(void)
	{
		{	/* Foreign/copaque.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzforeign_copaquez00(void)
	{
		{	/* Foreign/copaque.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_copaquez00(void)
	{
		{	/* Foreign/copaque.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_copaquez00(void)
	{
		{	/* Foreign/copaque.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_copaquez00(void)
	{
		{	/* Foreign/copaque.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
				BGl_copaquez00zzforeign_ctypez00, BGl_proc1316z00zzforeign_copaquez00,
				BGl_string1317z00zzforeign_copaquez00);
		}

	}



/* &make-ctype-accesses!1089 */
	obj_t BGl_z62makezd2ctypezd2accessesz121089z70zzforeign_copaquez00(obj_t
		BgL_envz00_700, obj_t BgL_whatz00_701, obj_t BgL_whoz00_702,
		obj_t BgL_locz00_703)
	{
		{	/* Foreign/copaque.scm 28 */
			{	/* Foreign/copaque.scm 29 */
				BgL_typez00_bglt BgL_btypez00_721;

				{
					BgL_copaquez00_bglt BgL_auxz00_847;

					{
						obj_t BgL_auxz00_848;

						{	/* Foreign/copaque.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_849;

							BgL_tmpz00_849 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_whatz00_701));
							BgL_auxz00_848 = BGL_OBJECT_WIDENING(BgL_tmpz00_849);
						}
						BgL_auxz00_847 = ((BgL_copaquez00_bglt) BgL_auxz00_848);
					}
					BgL_btypez00_721 =
						(((BgL_copaquez00_bglt) COBJECT(BgL_auxz00_847))->BgL_btypez00);
				}
				{	/* Foreign/copaque.scm 29 */
					obj_t BgL_idz00_722;

					BgL_idz00_722 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_whoz00_702)))->BgL_idz00);
					{	/* Foreign/copaque.scm 31 */
						obj_t BgL_bidz00_723;

						BgL_bidz00_723 =
							(((BgL_typez00_bglt) COBJECT(BgL_btypez00_721))->BgL_idz00);
						{	/* Foreign/copaque.scm 32 */
							obj_t BgL_idzd2ze3bidz31_724;

							{	/* Foreign/copaque.scm 33 */
								obj_t BgL_list1234z00_725;

								{	/* Foreign/copaque.scm 33 */
									obj_t BgL_arg1236z00_726;

									{	/* Foreign/copaque.scm 33 */
										obj_t BgL_arg1238z00_727;

										BgL_arg1238z00_727 = MAKE_YOUNG_PAIR(BgL_bidz00_723, BNIL);
										BgL_arg1236z00_726 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1238z00_727);
									}
									BgL_list1234z00_725 =
										MAKE_YOUNG_PAIR(BgL_idz00_722, BgL_arg1236z00_726);
								}
								BgL_idzd2ze3bidz31_724 =
									BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
									(BgL_list1234z00_725);
							}
							{	/* Foreign/copaque.scm 33 */
								obj_t BgL_bidzd2ze3idz31_728;

								{	/* Foreign/copaque.scm 34 */
									obj_t BgL_list1231z00_729;

									{	/* Foreign/copaque.scm 34 */
										obj_t BgL_arg1232z00_730;

										{	/* Foreign/copaque.scm 34 */
											obj_t BgL_arg1233z00_731;

											BgL_arg1233z00_731 = MAKE_YOUNG_PAIR(BgL_idz00_722, BNIL);
											BgL_arg1232z00_730 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1233z00_731);
										}
										BgL_list1231z00_729 =
											MAKE_YOUNG_PAIR(BgL_bidz00_723, BgL_arg1232z00_730);
									}
									BgL_bidzd2ze3idz31_728 =
										BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
										(BgL_list1231z00_729);
								}
								{	/* Foreign/copaque.scm 34 */
									obj_t BgL_bidzf3zf3_732;

									{	/* Foreign/copaque.scm 35 */
										obj_t BgL_arg1228z00_733;

										{	/* Foreign/copaque.scm 35 */
											obj_t BgL_arg1229z00_734;
											obj_t BgL_arg1230z00_735;

											{	/* Foreign/copaque.scm 35 */
												obj_t BgL_arg1455z00_736;

												BgL_arg1455z00_736 = SYMBOL_TO_STRING(BgL_idz00_722);
												BgL_arg1229z00_734 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_736);
											}
											{	/* Foreign/copaque.scm 35 */
												obj_t BgL_symbolz00_737;

												BgL_symbolz00_737 = CNST_TABLE_REF(1);
												{	/* Foreign/copaque.scm 35 */
													obj_t BgL_arg1455z00_738;

													BgL_arg1455z00_738 =
														SYMBOL_TO_STRING(BgL_symbolz00_737);
													BgL_arg1230z00_735 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_738);
												}
											}
											BgL_arg1228z00_733 =
												string_append(BgL_arg1229z00_734, BgL_arg1230z00_735);
										}
										BgL_bidzf3zf3_732 = bstring_to_symbol(BgL_arg1228z00_733);
									}
									{	/* Foreign/copaque.scm 35 */
										obj_t BgL_bidzf3zd2boolz21_739;

										{	/* Foreign/copaque.scm 36 */
											obj_t BgL_arg1225z00_740;

											{	/* Foreign/copaque.scm 36 */
												obj_t BgL_arg1226z00_741;
												obj_t BgL_arg1227z00_742;

												{	/* Foreign/copaque.scm 36 */
													obj_t BgL_arg1455z00_743;

													BgL_arg1455z00_743 =
														SYMBOL_TO_STRING(BgL_bidzf3zf3_732);
													BgL_arg1226z00_741 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_743);
												}
												{	/* Foreign/copaque.scm 36 */
													obj_t BgL_symbolz00_744;

													BgL_symbolz00_744 = CNST_TABLE_REF(2);
													{	/* Foreign/copaque.scm 36 */
														obj_t BgL_arg1455z00_745;

														BgL_arg1455z00_745 =
															SYMBOL_TO_STRING(BgL_symbolz00_744);
														BgL_arg1227z00_742 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_745);
													}
												}
												BgL_arg1225z00_740 =
													string_append(BgL_arg1226z00_741, BgL_arg1227z00_742);
											}
											BgL_bidzf3zd2boolz21_739 =
												bstring_to_symbol(BgL_arg1225z00_740);
										}
										{	/* Foreign/copaque.scm 36 */
											obj_t BgL_namez00_746;

											BgL_namez00_746 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_whoz00_702)))->BgL_namez00);
											{	/* Foreign/copaque.scm 37 */
												obj_t BgL_namezd2sanszd2z42z42_747;

												BgL_namezd2sanszd2z42z42_747 =
													BGl_stringzd2sanszd2z42z42zztype_toolsz00
													(BgL_namez00_746);
												{	/* Foreign/copaque.scm 38 */

													{

														{	/* Foreign/copaque.scm 69 */
															obj_t BgL_arg1102z00_787;

															{	/* Foreign/copaque.scm 69 */
																obj_t BgL_list1105z00_788;

																{	/* Foreign/copaque.scm 69 */
																	obj_t BgL_arg1114z00_789;

																	{	/* Foreign/copaque.scm 69 */
																		obj_t BgL_arg1115z00_790;

																		BgL_arg1115z00_790 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BNIL);
																		BgL_arg1114z00_789 =
																			MAKE_YOUNG_PAIR(BgL_idz00_722,
																			BgL_arg1115z00_790);
																	}
																	BgL_list1105z00_788 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																		BgL_arg1114z00_789);
																}
																BgL_arg1102z00_787 =
																	BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																	(BgL_list1105z00_788);
															}
															{	/* Foreign/copaque.scm 67 */
																obj_t BgL_list1103z00_791;

																{	/* Foreign/copaque.scm 67 */
																	obj_t BgL_arg1104z00_792;

																	BgL_arg1104z00_792 =
																		MAKE_YOUNG_PAIR(BgL_arg1102z00_787, BNIL);
																	BgL_list1103z00_791 =
																		MAKE_YOUNG_PAIR(BgL_bidzf3zf3_732,
																		BgL_arg1104z00_792);
																}
																BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
																	(BgL_list1103z00_791);
															}
														}
														{	/* Foreign/copaque.scm 72 */
															obj_t BgL_arg1122z00_793;

															{	/* Foreign/copaque.scm 72 */
																obj_t BgL_arg1123z00_794;

																{	/* Foreign/copaque.scm 72 */
																	obj_t BgL_arg1125z00_795;
																	obj_t BgL_arg1126z00_796;

																	{	/* Foreign/copaque.scm 44 */
																		obj_t BgL_arg1157z00_781;

																		{	/* Foreign/copaque.scm 44 */
																			obj_t BgL_arg1158z00_782;

																			{	/* Foreign/copaque.scm 44 */
																				obj_t BgL_arg1162z00_783;

																				{	/* Foreign/copaque.scm 44 */
																					obj_t BgL_arg1164z00_784;
																					obj_t BgL_arg1166z00_785;

																					{	/* Foreign/copaque.scm 44 */
																						obj_t BgL_arg1171z00_786;

																						BgL_arg1171z00_786 =
																							MAKE_YOUNG_PAIR(BgL_idz00_722,
																							BNIL);
																						BgL_arg1164z00_784 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(17), BgL_arg1171z00_786);
																					}
																					BgL_arg1166z00_785 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1320z00zzforeign_copaquez00,
																						BNIL);
																					BgL_arg1162z00_783 =
																						MAKE_YOUNG_PAIR(BgL_arg1164z00_784,
																						BgL_arg1166z00_785);
																				}
																				BgL_arg1158z00_782 =
																					MAKE_YOUNG_PAIR
																					(BgL_idzd2ze3bidz31_724,
																					BgL_arg1162z00_783);
																			}
																			BgL_arg1157z00_781 =
																				MAKE_YOUNG_PAIR(BgL_bidz00_723,
																				BgL_arg1158z00_782);
																		}
																		BgL_arg1125z00_795 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																			BgL_arg1157z00_781);
																	}
																	{	/* Foreign/copaque.scm 72 */
																		obj_t BgL_tmpz00_903;

																		{	/* Foreign/copaque.scm 47 */
																			obj_t BgL_mnamez00_775;

																			BgL_mnamez00_775 =
																				string_append_3
																				(BGl_string1318z00zzforeign_copaquez00,
																				BgL_namezd2sanszd2z42z42_747,
																				BGl_string1319z00zzforeign_copaquez00);
																			{	/* Foreign/copaque.scm 48 */
																				obj_t BgL_arg1182z00_776;

																				{	/* Foreign/copaque.scm 48 */
																					obj_t BgL_arg1183z00_777;

																					{	/* Foreign/copaque.scm 48 */
																						obj_t BgL_arg1187z00_778;

																						{	/* Foreign/copaque.scm 48 */
																							obj_t BgL_arg1188z00_779;
																							obj_t BgL_arg1189z00_780;

																							BgL_arg1188z00_779 =
																								MAKE_YOUNG_PAIR(BgL_bidz00_723,
																								BNIL);
																							BgL_arg1189z00_780 =
																								MAKE_YOUNG_PAIR
																								(BgL_mnamez00_775, BNIL);
																							BgL_arg1187z00_778 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1188z00_779,
																								BgL_arg1189z00_780);
																						}
																						BgL_arg1183z00_777 =
																							MAKE_YOUNG_PAIR
																							(BgL_bidzd2ze3idz31_728,
																							BgL_arg1187z00_778);
																					}
																					BgL_arg1182z00_776 =
																						MAKE_YOUNG_PAIR(BgL_idz00_722,
																						BgL_arg1183z00_777);
																				}
																				BgL_tmpz00_903 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																					BgL_arg1182z00_776);
																			}
																		}
																		BgL_arg1126z00_796 =
																			MAKE_YOUNG_PAIR(BgL_tmpz00_903, BNIL);
																	}
																	BgL_arg1123z00_794 =
																		MAKE_YOUNG_PAIR(BgL_arg1125z00_795,
																		BgL_arg1126z00_796);
																}
																BgL_arg1122z00_793 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																	BgL_arg1123z00_794);
															}
															BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																(BgL_arg1122z00_793);
														}
														{	/* Foreign/copaque.scm 75 */
															obj_t BgL_arg1129z00_797;

															{	/* Foreign/copaque.scm 75 */
																obj_t BgL_arg1131z00_798;

																{	/* Foreign/copaque.scm 75 */
																	obj_t BgL_arg1132z00_799;

																	{	/* Foreign/copaque.scm 75 */
																		obj_t BgL_arg1137z00_800;

																		{	/* Foreign/copaque.scm 75 */
																			obj_t BgL_arg1138z00_801;

																			BgL_arg1138z00_801 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																				BNIL);
																			BgL_arg1137z00_800 =
																				MAKE_YOUNG_PAIR
																				(BgL_bidzf3zd2boolz21_739,
																				BgL_arg1138z00_801);
																		}
																		BgL_arg1132z00_799 =
																			BGl_makezd2protozd2inlinez00zzforeign_libraryz00
																			(BgL_arg1137z00_800);
																	}
																	BgL_arg1131z00_798 =
																		MAKE_YOUNG_PAIR(BgL_arg1132z00_799, BNIL);
																}
																BgL_arg1129z00_797 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
																	BgL_arg1131z00_798);
															}
															BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																(BgL_arg1129z00_797);
														}
														{	/* Foreign/copaque.scm 76 */
															obj_t BgL_arg1140z00_802;

															{	/* Foreign/copaque.scm 76 */
																obj_t BgL_arg1141z00_803;

																{	/* Foreign/copaque.scm 76 */
																	obj_t BgL_arg1142z00_804;

																	{	/* Foreign/copaque.scm 76 */
																		obj_t BgL_arg1143z00_805;

																		{	/* Foreign/copaque.scm 76 */
																			obj_t BgL_arg1145z00_806;

																			{	/* Foreign/copaque.scm 76 */
																				obj_t BgL_arg1148z00_807;

																				BgL_arg1148z00_807 =
																					MAKE_YOUNG_PAIR(BgL_bidz00_723, BNIL);
																				BgL_arg1145z00_806 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
																					BgL_arg1148z00_807);
																			}
																			BgL_arg1143z00_805 =
																				MAKE_YOUNG_PAIR(BgL_arg1145z00_806,
																				BNIL);
																		}
																		BgL_arg1142z00_804 =
																			MAKE_YOUNG_PAIR(BgL_bidzf3zf3_732,
																			BgL_arg1143z00_805);
																	}
																	BgL_arg1141z00_803 =
																		MAKE_YOUNG_PAIR(BgL_arg1142z00_804, BNIL);
																}
																BgL_arg1140z00_802 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																	BgL_arg1141z00_803);
															}
															BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																(BgL_arg1140z00_802);
														}
														{	/* Foreign/copaque.scm 79 */
															obj_t BgL_arg1149z00_808;
															obj_t BgL_arg1152z00_809;

															{	/* Foreign/copaque.scm 61 */
																obj_t BgL_arg1212z00_752;

																{	/* Foreign/copaque.scm 61 */
																	obj_t BgL_arg1215z00_753;
																	obj_t BgL_arg1216z00_754;

																	{	/* Foreign/copaque.scm 61 */
																		obj_t BgL_list1217z00_755;

																		{	/* Foreign/copaque.scm 61 */
																			obj_t BgL_arg1218z00_756;

																			{	/* Foreign/copaque.scm 61 */
																				obj_t BgL_arg1219z00_757;

																				BgL_arg1219z00_757 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(3),
																					BNIL);
																				BgL_arg1218z00_756 =
																					MAKE_YOUNG_PAIR(BgL_idz00_722,
																					BgL_arg1219z00_757);
																			}
																			BgL_list1217z00_755 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																				BgL_arg1218z00_756);
																		}
																		BgL_arg1215z00_753 =
																			BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																			(BgL_list1217z00_755);
																	}
																	{	/* Foreign/copaque.scm 62 */
																		obj_t BgL_arg1220z00_758;
																		obj_t BgL_arg1221z00_759;

																		BgL_arg1220z00_758 =
																			BGl_makezd2typedzd2identz00zzast_identz00
																			(CNST_TABLE_REF(5), CNST_TABLE_REF(6));
																		BgL_arg1221z00_759 =
																			MAKE_YOUNG_PAIR
																			(BGl_makezd2typedzd2identz00zzast_identz00
																			(CNST_TABLE_REF(7), CNST_TABLE_REF(6)),
																			BNIL);
																		BgL_arg1216z00_754 =
																			MAKE_YOUNG_PAIR(BgL_arg1220z00_758,
																			BgL_arg1221z00_759);
																	}
																	BgL_arg1212z00_752 =
																		MAKE_YOUNG_PAIR(BgL_arg1215z00_753,
																		BgL_arg1216z00_754);
																}
																BgL_arg1149z00_808 =
																	BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																	(BgL_arg1212z00_752, CNST_TABLE_REF(8));
															}
															{	/* Foreign/copaque.scm 53 */
																obj_t BgL_arg1191z00_760;
																obj_t BgL_arg1193z00_761;

																{	/* Foreign/copaque.scm 53 */
																	obj_t BgL_arg1194z00_762;

																	BgL_arg1194z00_762 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(9), BNIL);
																	BgL_arg1191z00_760 =
																		MAKE_YOUNG_PAIR(BgL_bidzf3zd2boolz21_739,
																		BgL_arg1194z00_762);
																}
																{	/* Foreign/copaque.scm 54 */
																	obj_t BgL_arg1196z00_763;

																	{	/* Foreign/copaque.scm 54 */
																		obj_t BgL_arg1197z00_764;
																		obj_t BgL_arg1198z00_765;

																		{	/* Foreign/copaque.scm 54 */
																			obj_t BgL_arg1199z00_766;

																			BgL_arg1199z00_766 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																				BNIL);
																			BgL_arg1197z00_764 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(11),
																				BgL_arg1199z00_766);
																		}
																		{	/* Foreign/copaque.scm 55 */
																			obj_t BgL_arg1200z00_767;
																			obj_t BgL_arg1201z00_768;

																			{	/* Foreign/copaque.scm 55 */
																				obj_t BgL_arg1202z00_769;

																				{	/* Foreign/copaque.scm 55 */
																					obj_t BgL_arg1203z00_770;
																					obj_t BgL_arg1206z00_771;

																					{	/* Foreign/copaque.scm 55 */
																						obj_t BgL_arg1208z00_772;

																						BgL_arg1208z00_772 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(10), BNIL);
																						BgL_arg1203z00_770 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(12), BgL_arg1208z00_772);
																					}
																					{	/* Foreign/copaque.scm 55 */
																						obj_t BgL_arg1209z00_773;

																						{	/* Foreign/copaque.scm 55 */
																							obj_t BgL_arg1210z00_774;

																							BgL_arg1210z00_774 =
																								MAKE_YOUNG_PAIR(BgL_bidz00_723,
																								BNIL);
																							BgL_arg1209z00_773 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(13), BgL_arg1210z00_774);
																						}
																						BgL_arg1206z00_771 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1209z00_773, BNIL);
																					}
																					BgL_arg1202z00_769 =
																						MAKE_YOUNG_PAIR(BgL_arg1203z00_770,
																						BgL_arg1206z00_771);
																				}
																				BgL_arg1200z00_767 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(14),
																					BgL_arg1202z00_769);
																			}
																			BgL_arg1201z00_768 =
																				MAKE_YOUNG_PAIR(BFALSE, BNIL);
																			BgL_arg1198z00_765 =
																				MAKE_YOUNG_PAIR(BgL_arg1200z00_767,
																				BgL_arg1201z00_768);
																		}
																		BgL_arg1196z00_763 =
																			MAKE_YOUNG_PAIR(BgL_arg1197z00_764,
																			BgL_arg1198z00_765);
																	}
																	BgL_arg1193z00_761 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(15),
																		BgL_arg1196z00_763);
																}
																BgL_arg1152z00_809 =
																	BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																	(BgL_arg1191z00_760, BgL_arg1193z00_761);
															}
															{	/* Foreign/copaque.scm 79 */
																obj_t BgL_list1153z00_810;

																{	/* Foreign/copaque.scm 79 */
																	obj_t BgL_arg1154z00_811;

																	BgL_arg1154z00_811 =
																		MAKE_YOUNG_PAIR(BgL_arg1152z00_809, BNIL);
																	BgL_list1153z00_810 =
																		MAKE_YOUNG_PAIR(BgL_arg1149z00_808,
																		BgL_arg1154z00_811);
																}
																return BgL_list1153z00_810;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_copaquez00(void)
	{
		{	/* Foreign/copaque.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1321z00zzforeign_copaquez00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1321z00zzforeign_copaquez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1321z00zzforeign_copaquez00));
			BGl_modulezd2initializa7ationz75zzforeign_ctypez00(396356879L,
				BSTRING_TO_STRING(BGl_string1321z00zzforeign_copaquez00));
			BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string1321z00zzforeign_copaquez00));
			BGl_modulezd2initializa7ationz75zzforeign_libraryz00(419430976L,
				BSTRING_TO_STRING(BGl_string1321z00zzforeign_copaquez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1321z00zzforeign_copaquez00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1321z00zzforeign_copaquez00));
		}

	}

#ifdef __cplusplus
}
#endif
