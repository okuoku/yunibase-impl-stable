/*===========================================================================*/
/*   (Foreign/ctype.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/ctype.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_CTYPE_TYPE_DEFINITIONS
#define BGL_FOREIGN_CTYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_caliasz00_bgl
	{
		bool_t BgL_arrayzf3zf3;
	}                *BgL_caliasz00_bglt;

	typedef struct BgL_cenumz00_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
		obj_t BgL_literalsz00;
	}               *BgL_cenumz00_bglt;

	typedef struct BgL_copaquez00_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
	}                 *BgL_copaquez00_bglt;

	typedef struct BgL_cfunctionz00_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
		long BgL_arityz00;
		struct BgL_typez00_bgl *BgL_typezd2reszd2;
		obj_t BgL_typezd2argszd2;
	}                   *BgL_cfunctionz00_bglt;

	typedef struct BgL_cptrz00_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
		struct BgL_typez00_bgl *BgL_pointzd2tozd2;
		bool_t BgL_arrayzf3zf3;
	}              *BgL_cptrz00_bglt;

	typedef struct BgL_cstructz00_bgl
	{
		bool_t BgL_structzf3zf3;
		obj_t BgL_fieldsz00;
		obj_t BgL_cstructza2za2;
	}                 *BgL_cstructz00_bglt;

	typedef struct BgL_cstructza2za2_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
		struct BgL_typez00_bgl *BgL_cstructz00;
	}                    *BgL_cstructza2za2_bglt;


#endif													// BGL_FOREIGN_CTYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62copaquezd2tvectorzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2idzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1932z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1933z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cenumzd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62lambda1937z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62lambda1938z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructzd2aliaszd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2occurrencezb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62cenumzd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_declarezd2czd2typez12z12zzforeign_ctypez00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62cptrzd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_caliaszd2arrayzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1945z62zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1948z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62copaquezd2namezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62cstructza2zd2tvectorz12zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1951z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_caliaszd2nilzd2zzforeign_ctypez00(void);
	static BgL_typez00_bglt BGl_z62lambda1958z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	BGL_EXPORTED_DECL int
		BGl_cfunctionzd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62lambda1959z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cptrzd2parentszd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_caliasz00zzforeign_ctypez00 = BUNSPEC;
	static obj_t BGl_z62cptrzd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_cenumzd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2classzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_declarezd2czd2structz12z12zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62caliaszd2initzf3z43zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2namezd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2namezd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cfunctionzd2typezd2argsz62zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t
		BGl_z62copaquezd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1966z62zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62cenumzd2locationzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1969z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2idzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62copaquezf3z91zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	static obj_t BGl_z62cstructzd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31971ze3ze5zzforeign_ctypez00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1972z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_copaquezd2btypezd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructza2zd2siza7ezb5zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1979z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62caliaszd2nilzb0zzforeign_ctypez00(obj_t);
	static obj_t BGl_z62cfunctionzd2idzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cptrzd2parentszb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	static obj_t
		BGl_z62cptrzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62copaquezd2initzf3z43zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1980z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2importzd2locationzd2setz12z62zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt
		BGl_declarezd2czd2structza2z12zb0zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1984z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1985z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62caliaszd2magiczf3z43zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1989z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t
		BGl_z62cptrzd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cptrzd2namezd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_objectzd2initzd2zzforeign_ctypez00(void);
	BGL_EXPORTED_DECL bool_t BGl_cptrzf3zf3zzforeign_ctypez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2locationz70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62lambda1990z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2cstructza2z70zzforeign_ctypez00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		BgL_typez00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62lambda1994z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1995z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2namezd2setz12zd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2magiczf3ze1zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_cstructz00zzforeign_ctypez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2z42z90zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cenumzd2literalszb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzd2classzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2aliasz12zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2classzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62caliaszd2locationzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cenumzd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ctypezf3zf3zzforeign_ctypez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	static obj_t
		BGl_z62cstructza2zd2occurrencezd2setz12zd2zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62makezd2cptrzb0zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2initzf3zd2setz12z21zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62copaquezd2magiczf3z43zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cstructza2zd2nilz70zzforeign_ctypez00(void);
	BGL_EXPORTED_DECL bool_t
		BGl_cptrzd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cptrzd2aliaszd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cstructzd2coercezd2toz62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2arityzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2namezd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2arrayzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	static obj_t BGl_makezd2foreignzd2coercersz00zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzforeign_ctypez00(void);
	static obj_t BGl_z62copaquezd2z42zf2zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2importzd2locationzc0zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62caliaszd2aliaszb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2typezd2argsz00zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62copaquezd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_makezd2cptrzd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, int, BgL_typez00_bglt, BgL_typez00_bglt, bool_t);
	static obj_t BGl_z62cenumzd2namezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62copaquezd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62cstructza2zd2cstructz12zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32054ze3ze5zzforeign_ctypez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	static BgL_typez00_bglt BGl_z62cptrzd2nilzb0zzforeign_ctypez00(obj_t);
	static obj_t BGl_z62cfunctionzd2parentszd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructzd2magiczf3z43zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62caliaszd2z42zf2zzforeign_ctypez00(obj_t, obj_t);
	extern BgL_typez00_bglt BGl_declarezd2aliastypez12zc0zztype_envz00(obj_t,
		obj_t, obj_t, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2classzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62copaquezd2aliaszb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2cstructzd2zzforeign_ctypez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		bool_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2parentsz70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62caliaszd2parentszb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2cenumzd2zzforeign_ctypez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62caliaszd2aliaszd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2classzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_copaquezf3zf3zzforeign_ctypez00(obj_t);
	static obj_t BGl_z62cfunctionzd2locationzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		int);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62copaquezd2siza7ez17zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2z42z90zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructzd2occurrencezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzd2initzf3z43zzforeign_ctypez00(obj_t, obj_t);
	static obj_t
		BGl_z62cenumzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t
		BGl_z62cfunctionzd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cfunctionzd2btypezd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cptrzd2classzd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62makezd2cenumzb0zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2initzf3zd2setz12z43zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cptrzd2pointzd2toz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62cptrzd2magiczf3z43zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzd2cstructza2z12zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cptrzd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cenumzd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cfunctionzd2nilzd2zzforeign_ctypez00(void);
	static obj_t BGl_z62cstructza2zd2classz12zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzd2namezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2coercezd2toz62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cptrzd2z42z90zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2fieldszd2zzforeign_ctypez00(BgL_typez00_bglt);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	static obj_t
		BGl_z62caliaszd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62copaquezd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2z42zd2setz12zf2zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, int);
	static obj_t BGl_z62cenumzd2z42zd2setz12z32zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62cstructza2zd2btypez12zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62cptrzd2tvectorzb0zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_declarezd2czd2functionz12z12zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cenumzd2coercezd2toz62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cptrzd2importzd2locationz62zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62cptrzd2namezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2classzd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cfunctionzd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2namezd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cptrzd2locationzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cptrzd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62copaquezd2aliaszd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62caliaszd2locationzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62caliaszd2classzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2siza7ezd2setz12z75zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		int);
	static obj_t BGl_z62caliaszd2arrayzf3z43zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_cptrzd2nilzd2zzforeign_ctypez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_ctypez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2z42z90zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62cstructzd2aliaszb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62cstructza2zd2nilz12zzforeign_ctypez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cptrzd2occurrencezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62copaquezd2classzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62copaquezd2coercezd2toz62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2namezd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cfunctionzd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62copaquezd2parentszd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2namezd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2idzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62copaquezd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cptrzd2arrayzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cenumzd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62copaquezd2btypezb0zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62cenumzd2siza7ez17zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_copaquez00zzforeign_ctypez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cstructzd2structzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cenumzd2initzf3z43zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, int);
	static obj_t BGl_z62cenumzd2parentszb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cfunctionzd2coercezd2toz62zzforeign_ctypez00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62makezd2cstructza2z12zzforeign_ctypez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62cstructzd2idzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cenumzd2aliaszd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62copaquezd2classzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_declarezd2czd2opaquez12z12zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cptrzd2idzb0zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62makezd2cstructzb0zzforeign_ctypez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2tvectorz70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62caliaszd2tvectorzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2occurrencezb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2pointedzd2tozd2byz70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cfunctionzd2initzf3z43zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31907ze3ze5zzforeign_ctypez00(obj_t);
	static obj_t
		BGl_z62cfunctionzd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_copaquezd2nilzd2zzforeign_ctypez00(void);
	BGL_EXPORTED_DECL int
		BGl_cstructzd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31924ze3ze5zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cfunctionzd2namezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cptrzd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cenumzd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2cfunctionzd2zzforeign_ctypez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		BgL_typez00_bglt, long, BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62cstructzd2siza7ez17zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2caliaszd2zzforeign_ctypez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		bool_t);
	static obj_t BGl_z62cfunctionzd2parentszb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62copaquezd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2cstructza2zd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cenumzd2locationzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62copaquezd2locationzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2copaquezd2zzforeign_ctypez00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cptrzd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2coercezd2tozd2setz12z62zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62declarezd2czd2aliasz12z70zzforeign_ctypez00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62caliaszd2namezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cptrzd2siza7ez17zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cptrzd2arrayzf3z43zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cenumzd2importzd2locationz62zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62cptrzd2coercezd2toz62zzforeign_ctypez00(obj_t, obj_t);
	extern bool_t BGl_typezd2existszf3z21zztype_envz00(obj_t);
	static obj_t BGl_z62cstructzd2classzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzforeign_ctypez00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	static obj_t BGl_z62cenumzd2aliaszb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62declarezd2czd2typez12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62cenumzd2classzd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructza2zd2coercezd2tozc0zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cfunctionz00zzforeign_ctypez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62cptrzd2initzf3z43zzforeign_ctypez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62cstructzd2cstructza2zd2setz12zd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2aliaszd2setz12zd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	extern BgL_typez00_bglt BGl_declarezd2subtypez12zc0zztype_envz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	static obj_t BGl_z62copaquezd2occurrencezb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2siza7ezd2setz12z17zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		int);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2classzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2namez70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cfunctionzd2aliaszb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzd2locationzb0zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1890z62zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1893z62zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1896z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62copaquezd2nilzb0zzforeign_ctypez00(obj_t);
	static BgL_typez00_bglt BGl_z62cptrzd2pointzd2toz62zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cenumzd2btypezd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2idz70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32007ze3ze5zzforeign_ctypez00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62cenumzd2nilzb0zzforeign_ctypez00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cstructza2zd2initzf3z83zzforeign_ctypez00(BgL_typez00_bglt);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_genericzd2initzd2zzforeign_ctypez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static BgL_typez00_bglt BGl_z62cfunctionzd2nilzb0zzforeign_ctypez00(obj_t);
	static obj_t BGl_z62copaquezd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructzd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cstructza2zd2cstructz70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructzd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2pointedzd2tozd2byz12zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructzd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cptrz00zzforeign_ctypez00 = BUNSPEC;
	static obj_t BGl_z62cfunctionzd2z42zd2setz12z32zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cenumzf3z91zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62makezd2caliaszb0zzforeign_ctypez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cstructza2zd2magiczf3z83zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t
		BGl_z62cstructza2zd2pointedzd2tozd2byzd2setz12zd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cenumzd2magiczf3z43zzforeign_ctypez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62caliaszf3z91zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt
		BGl_declarezd2czd2pointerz12z12zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cenumzd2tvectorzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62copaquezd2importzd2locationz62zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt BGl_cenumzd2nilzd2zzforeign_ctypez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32043ze3ze5zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62cenumzd2occurrencezb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2classzd2setz12zd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31895ze3ze5zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_EXPORTED_DECL long
		BGl_cfunctionzd2arityzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2importzd2locationzd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t
		BGl_z62caliaszd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2parentszd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cptrzd2aliaszb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2siza7ez17zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructza2zf3z33zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cptrzf3z91zzforeign_ctypez00(obj_t, obj_t);
	static obj_t
		BGl_z62cfunctionzd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2tvectorzd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2aliaszd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cptrzd2locationzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2cstructza2z70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32029ze3ze5zzforeign_ctypez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cptrzd2idzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_z62cfunctionzd2typezd2resz62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62cenumzd2classzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzf3z91zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzd2fieldszb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62copaquezd2locationzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzd2z42zf2zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2tvectorzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2literalszd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2aliasz70zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62cenumzd2btypezb0zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62cstructza2zd2idz12zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2z42z90zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62caliaszd2siza7ez17zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2importzd2locationz62zzforeign_ctypez00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62makezd2copaquezb0zzforeign_ctypez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t
		BGl_z62cenumzd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda2002z62zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32080ze3ze5zzforeign_ctypez00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda2005z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cstructza2zf3z51zzforeign_ctypez00(obj_t);
	static BgL_typez00_bglt BGl_z62lambda2008z62zzforeign_ctypez00(obj_t, obj_t);
	extern obj_t BGl_za2cobjza2z00zztype_cachez00;
	static obj_t BGl_z62cfunctionzd2classzb0zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cstructzd2namezd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cptrzd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_copaquezd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructza2zd2parentszd2setz12zd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	extern long BGl_foreignzd2arityzd2zztools_argsz00(obj_t);
	static obj_t
		BGl_z62cfunctionzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cenumzd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2tvectorzd2setz12zd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2015z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2016z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2locationzd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62caliaszd2parentszd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructzd2parentszb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		int);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2coercezd2toza2zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62cfunctionzd2btypezb0zzforeign_ctypez00(obj_t,
		obj_t);
	static obj_t BGl_z62caliaszd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_caliaszd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda2020z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2021z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32066ze3ze5zzforeign_ctypez00(obj_t);
	static obj_t BGl_z62lambda2027z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2028z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cenumzd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2z42z90zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2siza7ezd7zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructza2zd2magiczf3zd2setz12z21zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62cstructza2zd2coercezd2tozd2setz12z00zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_copaquezd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_copaquezd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructza2zd2occurrencez12zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2classzd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda2038z62zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_caliaszd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62caliaszd2z42zd2setz12z32zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62copaquezd2parentszb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2occurrencezd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt,
		int);
	BGL_EXPORTED_DECL bool_t BGl_cfunctionzf3zf3zzforeign_ctypez00(obj_t);
	static obj_t BGl_z62ctypezf3z91zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2041z62zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2044z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_cstructzd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cstructzd2nilzd2zzforeign_ctypez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2magiczf3zd2setz12z43zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2classzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructza2zd2locationz12zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2aliaszd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cenumzd2parentszd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2z42z32zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructza2zd2z42zd2setz12z90zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cptrzd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cenumzd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62caliaszd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	static obj_t BGl_z62lambda2052z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2053z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62cstructzd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2058z62zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62lambda2059z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2namezd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructzd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt,
		bool_t);
	static obj_t BGl_z62cstructza2zd2parentsz12zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cptrzd2btypezd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cfunctionzf3z91zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2064z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2065z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2locationzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62copaquezd2z42zd2setz12z32zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2importzd2locationza2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t
		BGl_z62cstructzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cptrzd2classzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62cstructza2zd2z42z50zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_caliaszd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	extern obj_t BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00;
	BGL_EXPORTED_DEF obj_t BGl_cstructza2za2zzforeign_ctypez00 = BUNSPEC;
	static BgL_typez00_bglt BGl_z62lambda2075z62zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62cenumzd2idzb0zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2078z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzforeign_ctypez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_ctypez00(void);
	static obj_t BGl_z62cfunctionzd2z42zf2zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_declarezd2czd2enumz12z12zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cstructzf3zf3zzforeign_ctypez00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_ctypez00(void);
	static BgL_typez00_bglt BGl_z62cptrzd2btypezb0zzforeign_ctypez00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda2081z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2089z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62caliaszd2namezd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cstructzd2importzd2locationz62zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2idzd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_caliaszf3zf3zzforeign_ctypez00(obj_t);
	BGL_EXPORTED_DECL int
		BGl_cptrzd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cenumzd2z42zf2zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62lambda2090z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2classz70zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cfunctionzd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2095z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda2096z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt, bool_t);
	static obj_t BGl_z62cstructza2zd2namez12zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructzd2parentszd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructzd2z42zd2setz12z32zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62makezd2cfunctionzb0zzforeign_ctypez00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62cstructzd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cstructza2zd2btypez70zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cenumzd2namezd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62copaquezd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_cstructza2zd2occurrencez70zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	extern obj_t
		BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00(BgL_typez00_bglt);
	static obj_t BGl_z62cfunctionzd2classzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructzd2locationzd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cfunctionzd2typezd2resz00zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructza2zd2locationzd2setz12zd2zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructzd2structzf3z43zzforeign_ctypez00(obj_t, obj_t);
	static obj_t
		BGl_z62cstructza2zd2importzd2locationzd2setz12z00zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62caliaszd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62copaquezd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_declarezd2czd2aliasz12z12zzforeign_ctypez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62cfunctionzd2magiczf3z43zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62cstructzd2nilzb0zzforeign_ctypez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cptrzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	static obj_t BGl_z62cfunctionzd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cstructzd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cenumzd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_cfunctionzd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cptrzd2z42zf2zzforeign_ctypez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cenumzf3zf3zzforeign_ctypez00(obj_t);
	static obj_t BGl_z62caliaszd2arrayzf3zd2setz12z83zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62copaquezd2namezd2setz12z70zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62caliaszd2importzd2locationz62zzforeign_ctypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_caliaszd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_copaquezd2idzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62cptrzd2z42zd2setz12z32zzforeign_ctypez00(obj_t, obj_t,
		obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62cstructzd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1905z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1906z62zzforeign_ctypez00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cenumz00zzforeign_ctypez00 = BUNSPEC;
	static obj_t BGl_z62cstructzd2tvectorzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfunctionzd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructzd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1917z62zzforeign_ctypez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cstructza2zd2pointedzd2tozd2byzd2setz12zb0zzforeign_ctypez00
		(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62copaquezd2idzb0zzforeign_ctypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cenumzd2idzd2zzforeign_ctypez00(BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31950ze3ze5zzforeign_ctypez00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1920z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t BGl_z62cfunctionzd2namezd2setz12z70zzforeign_ctypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cstructza2zd2initzf3ze1zzforeign_ctypez00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1925z62zzforeign_ctypez00(obj_t, obj_t);
	static obj_t __cnst[45];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2magiczf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2magicza72188za7,
		BGl_z62cptrzd2magiczf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2120z00zzforeign_ctypez00,
		BgL_bgl_string2120za700za7za7f2189za7, ") 0)", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2pointedzd2tozd2byzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2point2190z00,
		BGl_z62cenumzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2pointedzd2tozd2byzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2pointe2191z00,
		BGl_z62cptrzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2arrayzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2arra2192z00,
		BGl_z62caliaszd2arrayzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2pointedzd2tozd2byzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2poi2193z00,
		BGl_z62cstructzd2pointedzd2tozd2byzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2namezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2nameza72194za7,
		BGl_z62cenumzd2namezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2locationzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2l2195z00,
		BGl_z62cfunctionzd2locationzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2initzf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2initza72196za7,
		BGl_z62cenumzd2initzf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2parentszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2par2197z00,
		BGl_z62copaquezd2parentszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2classzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2classza72198za7,
		BGl_z62cptrzd2classzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2magiczf3zd2envz51zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22199za7,
		BGl_z62cstructza2zd2magiczf3ze1zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2cstructza2zd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2cst2200z00,
		BGl_z62cstructzd2cstructza2zd2setz12zd2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2initzf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2init2201z00,
		BGl_z62caliaszd2initzf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2coercezd2tozd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2coerce2202z00,
		BGl_z62cptrzd2coercezd2toz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2tvectorzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2tvec2203z00,
		BGl_z62caliaszd2tvectorzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezd2namezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2nam2204z00,
		BGl_z62copaquezd2namezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2occurrencezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2occu2205z00,
		BGl_z62caliaszd2occurrencezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2namezd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22206za7,
		BGl_z62cstructza2zd2namez12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2namezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2name2207z00,
		BGl_z62caliaszd2namezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2arityzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2a2208z00,
		BGl_z62cfunctionzd2arityzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2initzf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2i2209z00,
		BGl_z62cfunctionzd2initzf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzf3zd2envz21zzforeign_ctypez00,
		BgL_bgl_za762cptrza7f3za791za7za7f2210za7,
		BGl_z62cptrzf3z91zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2locationzd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22211za7,
		BGl_z62cstructza2zd2locationz12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2siza7ezd2setz12zd2envz67zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2siza7a7e2212za7,
		BGl_z62cptrzd2siza7ezd2setz12zd7zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszf3zd2envz21zzforeign_ctypez00,
		BgL_bgl_za762caliasza7f3za791za72213z00,
		BGl_z62caliaszf3z91zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2185z00zzforeign_ctypez00,
		BgL_bgl_string2185za700za7za7f2214za7, "foreign_ctype", 13);
	      DEFINE_STRING(BGl_string2186z00zzforeign_ctypez00,
		BgL_bgl_string2186za700za7za7f2215za7,
		"_ cstruct cstruct* fields struct? cptr point-to cfunction type-args type-res long arity copaque cenum obj literals btype foreign_ctype calias bool array? type lambda quote x foreign coerce cobj -> ? * bigloo (obj) (cobj) b C union* struct* union struct array pointer function opaque enum ",
		288);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2tvectorzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2tve2216z00,
		BGl_z62copaquezd2tvectorzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2initzf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2ini2217z00,
		BGl_z62copaquezd2initzf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2aliaszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2aliasza72218za7,
		BGl_z62cptrzd2aliaszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2namezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2nam2219z00,
		BGl_z62copaquezd2namezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2siza7ezd2envza7zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2siza7a7e2220za7,
		BGl_z62cptrzd2siza7ez17zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2arrayzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2arrayza72221za7,
		BGl_z62cptrzd2arrayzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2classzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2cla2222z00,
		BGl_z62cstructzd2classzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2initzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2init2223z00,
		BGl_z62caliaszd2initzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2initzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2ini2224z00,
		BGl_z62copaquezd2initzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2parentszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2par2225z00,
		BGl_z62cstructzd2parentszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2z42zd2setz12zd2envz82zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2za7422226za7,
		BGl_z62cstructzd2z42zd2setz12z32zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2tvectorzd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22227za7,
		BGl_z62cstructza2zd2tvectorzd2setz12zd2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2z42zd2envz42zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2za742za7f2228z00,
		BGl_z62cenumzd2z42zf2zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2locationzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2locat2229z00,
		BGl_z62cenumzd2locationzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2magiczf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2m2230z00,
		BGl_z62cfunctionzd2magiczf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2pointedzd2tozd2byzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2p2231z00,
		BGl_z62cfunctionzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2magiczf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2magic2232z00,
		BGl_z62cenumzd2magiczf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2classzd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22233za7,
		BGl_z62cstructza2zd2classzd2setz12zd2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2initzf3zd2envz51zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22234za7,
		BGl_z62cstructza2zd2initzf3ze1zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2importzd2locationzd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2imp2235z00,
		BGl_z62copaquezd2importzd2locationzd2setz12za2zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2importzd2locationzd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2i2236z00,
		BGl_z62cfunctionzd2importzd2locationz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2importzd2locationzd2setz12zd2envzb0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22237za7,
		BGl_z62cstructza2zd2importzd2locationzd2setz12z00zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2importzd2locationzd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2import2238z00,
		BGl_z62cptrzd2importzd2locationzd2setz12za2zzforeign_ctypez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cfunctionzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762makeza7d2cfunct2239z00,
		BGl_z62makezd2cfunctionzb0zzforeign_ctypez00, 0L, BUNSPEC, 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2pointedzd2tozd2byzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2p2240z00,
		BGl_z62cfunctionzd2pointedzd2tozd2byzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2coercezd2tozd2envz70zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22241za7,
		BGl_z62cstructza2zd2coercezd2tozc0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2namezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2nameza7d2242za7,
		BGl_z62cptrzd2namezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2parentszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2paren2243z00,
		BGl_z62cenumzd2parentszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2initzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2initza72244za7,
		BGl_z62cenumzd2initzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2parentszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2par2245z00,
		BGl_z62copaquezd2parentszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2importzd2locationzd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2import2246z00,
		BGl_z62cptrzd2importzd2locationz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructza2zf3zd2envz83zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7f32247za7,
		BGl_z62cstructza2zf3z33zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2occurrencezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2o2248z00,
		BGl_z62cfunctionzd2occurrencezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2aliaszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2ali2249z00,
		BGl_z62cstructzd2aliaszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2tvectorzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2tvecto2250z00,
		BGl_z62cptrzd2tvectorzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2occurrencezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2occurr2251z00,
		BGl_z62cptrzd2occurrencezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2magiczf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2m2252z00,
		BGl_z62cfunctionzd2magiczf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2locationzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2loca2253z00,
		BGl_z62caliaszd2locationzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2siza7ezd2setz12zd2envzc5zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22254za7,
		BGl_z62cstructza2zd2siza7ezd2setz12z75zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2tvectorzd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22255za7,
		BGl_z62cstructza2zd2tvectorz12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2namezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2nameza7b2256za7,
		BGl_z62cptrzd2namezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2tvectorzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2t2257z00,
		BGl_z62cfunctionzd2tvectorzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2importzd2locationzd2envz70zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22258za7,
		BGl_z62cstructza2zd2importzd2locationzc0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2importzd2locationzd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2impor2259z00,
		BGl_z62cenumzd2importzd2locationz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2typezd2reszd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2t2260z00,
		BGl_z62cfunctionzd2typezd2resz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2coercezd2tozd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2coe2261z00,
		BGl_z62cstructzd2coercezd2toz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunctionzd2nilzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2n2262z00,
		BGl_z62cfunctionzd2nilzb0zzforeign_ctypez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2parentszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2p2263z00,
		BGl_z62cfunctionzd2parentszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2locationzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2loc2264z00,
		BGl_z62cstructzd2locationzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2nilzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2nilza72265za7,
		BGl_z62caliaszd2nilzb0zzforeign_ctypez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2aliaszd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22266za7,
		BGl_z62cstructza2zd2aliaszd2setz12zd2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2occurrencezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2occurr2267z00,
		BGl_z62cptrzd2occurrencezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2locationzd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22268za7,
		BGl_z62cstructza2zd2locationzd2setz12zd2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2idzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2idza7b0za72269z00,
		BGl_z62cptrzd2idzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2btypezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2b2270z00,
		BGl_z62cfunctionzd2btypezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2magiczf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2magicza72271za7,
		BGl_z62cptrzd2magiczf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2pointedzd2tozd2byzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2point2272z00,
		BGl_z62cenumzd2pointedzd2tozd2byzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2siza7ezd2setz12zd2envz67zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2siza72273za7,
		BGl_z62cstructzd2siza7ezd2setz12zd7zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzd2classzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2cla2274z00,
		BGl_z62cstructzd2classzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2tvectorzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2tvecto2275z00,
		BGl_z62cptrzd2tvectorzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2siza7ezd2envza7zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2siza7a2276za7,
		BGl_z62caliaszd2siza7ez17zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2occurrencezd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22277za7,
		BGl_z62cstructza2zd2occurrencezd2setz12zd2zzforeign_ctypez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2occurrencezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2occur2278z00,
		BGl_z62cenumzd2occurrencezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2nilzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2nilza7b02279za7,
		BGl_z62cptrzd2nilzb0zzforeign_ctypez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2z42zd2envz42zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2za742za7f22280z00,
		BGl_z62cptrzd2z42zf2zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2magiczf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2mag2281z00,
		BGl_z62copaquezd2magiczf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2pointedzd2tozd2byzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2poi2282z00,
		BGl_z62cstructzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2cstructza2zd2envza2zzforeign_ctypez00,
		BgL_bgl_za762makeza7d2cstruc2283z00,
		BGl_z62makezd2cstructza2z12zzforeign_ctypez00, 0L, BUNSPEC, 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2occurrencezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2occur2284z00,
		BGl_z62cenumzd2occurrencezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2parentszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2pare2285z00,
		BGl_z62caliaszd2parentszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2initzf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2ini2286z00,
		BGl_z62cstructzd2initzf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2occurrencezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2occu2287z00,
		BGl_z62caliaszd2occurrencezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2namezd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22288za7,
		BGl_z62cstructza2zd2namezd2setz12zd2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2locationzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2locat2289z00,
		BGl_z62cenumzd2locationzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2z42zd2setz12zd2envz82zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2za742za72290z00,
		BGl_z62caliaszd2z42zd2setz12z32zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2structzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2str2291z00,
		BGl_z62cstructzd2structzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2parentszd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22292za7,
		BGl_z62cstructza2zd2parentsz12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2classzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2c2293z00,
		BGl_z62cfunctionzd2classzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2locationzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2l2294z00,
		BGl_z62cfunctionzd2locationzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2parentszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2p2295z00,
		BGl_z62cfunctionzd2parentszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2importzd2locationzd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2impo2296z00,
		BGl_z62caliaszd2importzd2locationzd2setz12za2zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2importzd2locationzd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2imp2297z00,
		BGl_z62cstructzd2importzd2locationz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2coercezd2tozd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2c2298z00,
		BGl_z62cfunctionzd2coercezd2toz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2caliaszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762makeza7d2calias2299z00,
		BGl_z62makezd2caliaszb0zzforeign_ctypez00, 0L, BUNSPEC, 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2z42zd2setz12zd2envz82zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2za7422300za7,
		BGl_z62copaquezd2z42zd2setz12z32zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2magiczf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2magic2301z00,
		BGl_z62cenumzd2magiczf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2occurrencezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2o2302z00,
		BGl_z62cfunctionzd2occurrencezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzd2aliaszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2ali2303z00,
		BGl_z62cstructzd2aliaszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2coercezd2tozd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2coerce2304z00,
		BGl_z62cptrzd2coercezd2tozd2setz12za2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructza2zd2z42zd2envze0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22305za7,
		BGl_z62cstructza2zd2z42z50zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2initzf3zd2setz12zd2envz91zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22306za7,
		BGl_z62cstructza2zd2initzf3zd2setz12z21zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezd2btypezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2bty2307z00,
		BGl_z62copaquezd2btypezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2occurrencezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2occ2308z00,
		BGl_z62copaquezd2occurrencezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2literalszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2liter2309z00,
		BGl_z62cenumzd2literalszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2czd2typez12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762declareza7d2cza7d2310za7,
		BGl_z62declarezd2czd2typez12z70zzforeign_ctypez00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2121z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2311za7,
		BGl_z62zc3z04anonymousza31907ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2122z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1906za7622312z00, BGl_z62lambda1906z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1905za7622313z00, BGl_z62lambda1905z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2cstructzd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22314za7,
		BGl_z62cstructza2zd2cstructz12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1896za7622315z00, BGl_z62lambda1896z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2316za7,
		BGl_z62zc3z04anonymousza31895ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1893za7622317z00, BGl_z62lambda1893z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1890za7622318z00, BGl_z62lambda1890z62zzforeign_ctypez00,
		0L, BUNSPEC, 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2siza7ezd2setz12zd2envz67zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2s2319z00,
		BGl_z62cfunctionzd2siza7ezd2setz12zd7zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1933za7622320z00, BGl_z62lambda1933z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzd2fieldszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2fie2321z00,
		BGl_z62cstructzd2fieldszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1932za7622322z00, BGl_z62lambda1932z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezf3zd2envz21zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7f3za7912323za7,
		BGl_z62copaquezf3z91zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2namezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2nam2324z00,
		BGl_z62cstructzd2namezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2parentszd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22325za7,
		BGl_z62cstructza2zd2parentszd2setz12zd2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2coercezd2tozd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2coerc2326z00,
		BGl_z62cenumzd2coercezd2tozd2setz12za2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunctionzd2idzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2i2327z00,
		BGl_z62cfunctionzd2idzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2parentszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2parent2328z00,
		BGl_z62cptrzd2parentszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1938za7622329z00, BGl_z62lambda1938z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1937za7622330z00, BGl_z62lambda1937z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1925za7622331z00, BGl_z62lambda1925z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2332za7,
		BGl_z62zc3z04anonymousza31924ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2134z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1920za7622333z00, BGl_z62lambda1920z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1917za7622334z00, BGl_z62lambda1917z62zzforeign_ctypez00,
		0L, BUNSPEC, 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2136z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1959za7622335z00, BGl_z62lambda1959z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2137z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1958za7622336z00, BGl_z62lambda1958z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2138z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1951za7622337z00, BGl_z62lambda1951z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2139z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2338za7,
		BGl_z62zc3z04anonymousza31950ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2occurrencezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2occ2339z00,
		BGl_z62cstructzd2occurrencezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2copaquezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762makeza7d2copaqu2340z00,
		BGl_z62makezd2copaquezb0zzforeign_ctypez00, 0L, BUNSPEC, 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2aliaszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2a2341z00,
		BGl_z62cfunctionzd2aliaszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2140z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1948za7622342z00, BGl_z62lambda1948z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2141z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1945za7622343z00, BGl_z62lambda1945z62zzforeign_ctypez00,
		0L, BUNSPEC, 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2142z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1980za7622344z00, BGl_z62lambda1980z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2143z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1979za7622345z00, BGl_z62lambda1979z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1985za7622346z00, BGl_z62lambda1985z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2145z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1984za7622347z00, BGl_z62lambda1984z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2pointedzd2tozd2byzd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22348za7,
		BGl_z62cstructza2zd2pointedzd2tozd2byz12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1990za7622349z00, BGl_z62lambda1990z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1989za7622350z00, BGl_z62lambda1989z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2tvectorzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2tvect2351z00,
		BGl_z62cenumzd2tvectorzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1995za7622352z00, BGl_z62lambda1995z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1994za7622353z00, BGl_z62lambda1994z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2initzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2initza7f2354za7,
		BGl_z62cptrzd2initzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2namezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2name2355z00,
		BGl_z62caliaszd2namezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2btypezd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22356za7,
		BGl_z62cstructza2zd2btypez12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2z42zd2setz12zd2envz82zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2za742za7d22357z00,
		BGl_z62cptrzd2z42zd2setz12z32zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1972za7622358z00, BGl_z62lambda1972z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2359za7,
		BGl_z62zc3z04anonymousza31971ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1969za7622360z00, BGl_z62lambda1969z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2153z00zzforeign_ctypez00,
		BgL_bgl_za762lambda1966za7622361z00, BGl_z62lambda1966z62zzforeign_ctypez00,
		0L, BUNSPEC, 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2016za7622362z00, BGl_z62lambda2016z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2015za7622363z00, BGl_z62lambda2015z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2021za7622364z00, BGl_z62lambda2021z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2020za7622365z00, BGl_z62lambda2020z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2366za7,
		BGl_z62zc3z04anonymousza32029ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2028za7622367z00, BGl_z62lambda2028z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2importzd2locationzd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2imp2368z00,
		BGl_z62cstructzd2importzd2locationzd2setz12za2zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2tvectorzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2tvec2369z00,
		BGl_z62caliaszd2tvectorzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2027za7622370z00, BGl_z62lambda2027z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2008za7622371z00, BGl_z62lambda2008z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2162z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2372za7,
		BGl_z62zc3z04anonymousza32007ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2005za7622373z00, BGl_z62lambda2005z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2002za7622374z00, BGl_z62lambda2002z62zzforeign_ctypez00,
		0L, BUNSPEC, 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2165z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2375za7,
		BGl_z62zc3z04anonymousza32054ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2053za7622376z00, BGl_z62lambda2053z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2052za7622377z00, BGl_z62lambda2052z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2059za7622378z00, BGl_z62lambda2059z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2058za7622379z00, BGl_z62lambda2058z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2magiczf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2mag2380z00,
		BGl_z62cstructzd2magiczf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2locationzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2locati2381z00,
		BGl_z62cptrzd2locationzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2z42zd2setz12zd2envz20zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22382za7,
		BGl_z62cstructza2zd2z42zd2setz12z90zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2cstructza2zd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2cst2383z00,
		BGl_z62cstructzd2cstructza2z12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2384za7,
		BGl_z62zc3z04anonymousza32066ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2065za7622385z00, BGl_z62lambda2065z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2172z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2064za7622386z00, BGl_z62lambda2064z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2173z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2044za7622387z00, BGl_z62lambda2044z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2174z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2388za7,
		BGl_z62zc3z04anonymousza32043ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2175z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2041za7622389z00, BGl_z62lambda2041z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2176z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2038za7622390z00, BGl_z62lambda2038z62zzforeign_ctypez00,
		0L, BUNSPEC, 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2177z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2090za7622391z00, BGl_z62lambda2090z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2178z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2089za7622392z00, BGl_z62lambda2089z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2classzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2clas2393z00,
		BGl_z62caliaszd2classzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2179z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2096za7622394z00, BGl_z62lambda2096z62zzforeign_ctypez00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezd2classzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2cla2395z00,
		BGl_z62copaquezd2classzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2idzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2idza7b02396za7,
		BGl_z62cenumzd2idzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2classzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2classza72397za7,
		BGl_z62cptrzd2classzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2180z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2095za7622398z00, BGl_z62lambda2095z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2181z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2081za7622399z00, BGl_z62lambda2081z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2182z00zzforeign_ctypez00,
		BgL_bgl_za762za7c3za704anonymo2400za7,
		BGl_z62zc3z04anonymousza32080ze3ze5zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2183z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2078za7622401z00, BGl_z62lambda2078z62zzforeign_ctypez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2184z00zzforeign_ctypez00,
		BgL_bgl_za762lambda2075za7622402z00, BGl_z62lambda2075z62zzforeign_ctypez00,
		0L, BUNSPEC, 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2magiczf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2mag2403z00,
		BGl_z62cstructzd2magiczf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2coercezd2tozd2setz12zd2envzb0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22404za7,
		BGl_z62cstructza2zd2coercezd2tozd2setz12z00zzforeign_ctypez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2importzd2locationzd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2imp2405z00,
		BGl_z62copaquezd2importzd2locationz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2nilzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2nilza7b2406za7,
		BGl_z62cenumzd2nilzb0zzforeign_ctypez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2btypezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2btype2407z00,
		BGl_z62cenumzd2btypezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2pointedzd2tozd2byzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2poin2408z00,
		BGl_z62caliaszd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2coercezd2tozd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2c2409z00,
		BGl_z62cfunctionzd2coercezd2tozd2setz12za2zzforeign_ctypez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2tvectorzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2tve2410z00,
		BGl_z62copaquezd2tvectorzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2parentszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2parent2411z00,
		BGl_z62cptrzd2parentszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezd2z42zd2envz42zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2za7422412za7,
		BGl_z62copaquezd2z42zf2zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2locationzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2loca2413z00,
		BGl_z62caliaszd2locationzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2namezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2n2414z00,
		BGl_z62cfunctionzd2namezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2classzd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22415za7,
		BGl_z62cstructza2zd2classz12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2importzd2locationzd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2impor2416z00,
		BGl_z62cenumzd2importzd2locationzd2setz12za2zzforeign_ctypez00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2coercezd2tozd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2coerc2417z00,
		BGl_z62cenumzd2coercezd2toz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2aliaszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2alia2418z00,
		BGl_z62caliaszd2aliaszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezd2aliaszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2ali2419z00,
		BGl_z62copaquezd2aliaszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2tvectorzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2tve2420z00,
		BGl_z62cstructzd2tvectorzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2occurrencezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2occ2421z00,
		BGl_z62copaquezd2occurrencezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2parentszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2paren2422z00,
		BGl_z62cenumzd2parentszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2z42zd2envz42zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2za742za72423z00,
		BGl_z62caliaszd2z42zf2zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2pointedzd2tozd2byzd2setz12zd2envz62zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22424za7,
		BGl_z62cstructza2zd2pointedzd2tozd2byzd2setz12zd2zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2classzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2class2425z00,
		BGl_z62cenumzd2classzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2parentszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2pare2426z00,
		BGl_z62caliaszd2parentszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2siza7ezd2setz12zd2envz67zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2siza7a72427za7,
		BGl_z62cenumzd2siza7ezd2setz12zd7zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2classzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2class2428z00,
		BGl_z62cenumzd2classzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2classzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2clas2429z00,
		BGl_z62caliaszd2classzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzd2siza7ezd2envza7zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2siza72430za7,
		BGl_z62cstructzd2siza7ez17zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2z42zd2setz12zd2envz82zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2za72431za7,
		BGl_z62cfunctionzd2z42zd2setz12z32zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2pointzd2tozd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2pointza72432za7,
		BGl_z62cptrzd2pointzd2toz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructza2zd2nilzd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22433za7,
		BGl_z62cstructza2zd2nilz12zzforeign_ctypez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2z42zd2setz12zd2envz82zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2za742za7d2434z00,
		BGl_z62cenumzd2z42zd2setz12z32zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2aliaszd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22435za7,
		BGl_z62cstructza2zd2aliasz12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2aliaszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2aliasza72436za7,
		BGl_z62cptrzd2aliaszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzf3zd2envz21zzforeign_ctypez00,
		BgL_bgl_za762cstructza7f3za7912437za7,
		BGl_z62cstructzf3z91zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunctionzf3zd2envz21zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7f3za72438za7,
		BGl_z62cfunctionzf3z91zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2arrayzf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2arra2439z00,
		BGl_z62caliaszd2arrayzf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2tvectorzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2tve2440z00,
		BGl_z62cstructzd2tvectorzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzd2nilzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2nil2441z00,
		BGl_z62cstructzd2nilzb0zzforeign_ctypez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2locationzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2locati2442z00,
		BGl_z62cptrzd2locationzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2classzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2c2443z00,
		BGl_z62cfunctionzd2classzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2locationzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2loc2444z00,
		BGl_z62copaquezd2locationzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2pointedzd2tozd2byzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2pointe2445z00,
		BGl_z62cptrzd2pointedzd2tozd2byzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2typezd2argszd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2t2446z00,
		BGl_z62cfunctionzd2typezd2argsz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2siza7ezd2envza7zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2s2447z00,
		BGl_z62cfunctionzd2siza7ez17zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2magiczf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2magi2448z00,
		BGl_z62caliaszd2magiczf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzd2z42zd2envz42zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2za7422449za7,
		BGl_z62cstructzd2z42zf2zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezd2nilzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2nil2450z00,
		BGl_z62copaquezd2nilzb0zzforeign_ctypez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2occurrencezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2occ2451z00,
		BGl_z62cstructzd2occurrencezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2locationzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2loc2452z00,
		BGl_z62cstructzd2locationzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2classzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2cla2453z00,
		BGl_z62copaquezd2classzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructza2zd2idzd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22454za7,
		BGl_z62cstructza2zd2idz12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2aliaszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2alias2455z00,
		BGl_z62cenumzd2aliaszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunctionzd2z42zd2envz42zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2za72456za7,
		BGl_z62cfunctionzd2z42zf2zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2locationzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2loc2457z00,
		BGl_z62copaquezd2locationzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2magiczf3zd2setz12zd2envz91zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22458za7,
		BGl_z62cstructza2zd2magiczf3zd2setz12z21zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzd2siza7ezd2envza7zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2siza7a72459za7,
		BGl_z62cenumzd2siza7ez17zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2pointedzd2tozd2byzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2poi2460z00,
		BGl_z62copaquezd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2parentszd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2par2461z00,
		BGl_z62cstructzd2parentszb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2coercezd2tozd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2coer2462z00,
		BGl_z62caliaszd2coercezd2tozd2setz12za2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2coercezd2tozd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2coer2463z00,
		BGl_z62caliaszd2coercezd2toz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ctypezf3zd2envz21zzforeign_ctypez00,
		BgL_bgl_za762ctypeza7f3za791za7za72464za7,
		BGl_z62ctypezf3z91zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2pointedzd2tozd2byzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2poin2465z00,
		BGl_z62caliaszd2pointedzd2tozd2byzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezd2siza7ezd2envza7zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2siza72466za7,
		BGl_z62copaquezd2siza7ez17zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cenumzf3zd2envz21zzforeign_ctypez00,
		BgL_bgl_za762cenumza7f3za791za7za72467za7,
		BGl_z62cenumzf3z91zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2siza7ezd2envz05zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22468za7,
		BGl_z62cstructza2zd2siza7ezb5zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2namezd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2nameza72469za7,
		BGl_z62cenumzd2namezd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2tvectorzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2tvect2470z00,
		BGl_z62cenumzd2tvectorzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructza2zd2occurrencezd2envza2zzforeign_ctypez00,
		BgL_bgl_za762cstructza7a2za7d22471za7,
		BGl_z62cstructza2zd2occurrencez12zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cenumzd2aliaszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cenumza7d2alias2472z00,
		BGl_z62cenumzd2aliaszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2importzd2locationzd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2i2473z00,
		BGl_z62cfunctionzd2importzd2locationzd2setz12za2zzforeign_ctypez00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cstructzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762makeza7d2cstruc2474z00,
		BGl_z62makezd2cstructzb0zzforeign_ctypez00, 0L, BUNSPEC, 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2magiczf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2magi2475z00,
		BGl_z62caliaszd2magiczf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2aliaszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2alia2476z00,
		BGl_z62caliaszd2aliaszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cenumzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762makeza7d2cenumza72477za7,
		BGl_z62makezd2cenumzb0zzforeign_ctypez00, 0L, BUNSPEC, 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cptrzd2initzf3zd2setz12zd2envz33zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2initza7f2478za7,
		BGl_z62cptrzd2initzf3zd2setz12z83zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2czd2aliasz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762declareza7d2cza7d2479za7,
		BGl_z62declarezd2czd2aliasz12z70zzforeign_ctypez00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2magiczf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2mag2480z00,
		BGl_z62copaquezd2magiczf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2initzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2ini2481z00,
		BGl_z62cstructzd2initzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2siza7ezd2setz12zd2envz67zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2siza7a2482za7,
		BGl_z62caliaszd2siza7ezd2setz12zd7zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzd2namezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2nam2483z00,
		BGl_z62cstructzd2namezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2coercezd2tozd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2coe2484z00,
		BGl_z62copaquezd2coercezd2toz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cptrzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762makeza7d2cptrza7b2485za7,
		BGl_z62makezd2cptrzb0zzforeign_ctypez00, 0L, BUNSPEC, 18);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cptrzd2btypezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cptrza7d2btypeza72486za7,
		BGl_z62cptrzd2btypezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cstructzd2coercezd2tozd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2coe2487z00,
		BGl_z62cstructzd2coercezd2tozd2setz12za2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2aliaszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2a2488z00,
		BGl_z62cfunctionzd2aliaszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_copaquezd2idzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2idza72489za7,
		BGl_z62copaquezd2idzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2tvectorzd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2t2490z00,
		BGl_z62cfunctionzd2tvectorzd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_caliaszd2importzd2locationzd2envzd2zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2impo2491z00,
		BGl_z62caliaszd2importzd2locationz62zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2aliaszd2setz12zd2envzc0zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2ali2492z00,
		BGl_z62copaquezd2aliaszd2setz12z70zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2siza7ezd2setz12zd2envz67zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2siza72493za7,
		BGl_z62copaquezd2siza7ezd2setz12zd7zzforeign_ctypez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfunctionzd2namezd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2n2494z00,
		BGl_z62cfunctionzd2namezb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2pointedzd2tozd2byzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2poi2495z00,
		BGl_z62copaquezd2pointedzd2tozd2byzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_caliaszd2idzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762caliasza7d2idza7b2496za7,
		BGl_z62caliaszd2idzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfunctionzd2initzf3zd2envzf3zzforeign_ctypez00,
		BgL_bgl_za762cfunctionza7d2i2497z00,
		BGl_z62cfunctionzd2initzf3z43zzforeign_ctypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2116z00zzforeign_ctypez00,
		BgL_bgl_string2116za700za7za7f2498za7, "Type redefinition -- ", 21);
	      DEFINE_STRING(BGl_string2117z00zzforeign_ctypez00,
		BgL_bgl_string2117za700za7za7f2499za7, "declare-c-type!", 15);
	      DEFINE_STRING(BGl_string2118z00zzforeign_ctypez00,
		BgL_bgl_string2118za700za7za7f2500za7, "Illegal c type declaration", 26);
	      DEFINE_STRING(BGl_string2119z00zzforeign_ctypez00,
		BgL_bgl_string2119za700za7za7f2501za7, "*((", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_copaquezd2coercezd2tozd2setz12zd2envz12zzforeign_ctypez00,
		BgL_bgl_za762copaqueza7d2coe2502z00,
		BGl_z62copaquezd2coercezd2tozd2setz12za2zzforeign_ctypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cstructzd2idzd2envz00zzforeign_ctypez00,
		BgL_bgl_za762cstructza7d2idza72503za7,
		BGl_z62cstructzd2idzb0zzforeign_ctypez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_caliasz00zzforeign_ctypez00));
		     ADD_ROOT((void *) (&BGl_cstructz00zzforeign_ctypez00));
		     ADD_ROOT((void *) (&BGl_copaquez00zzforeign_ctypez00));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_ctypez00));
		     ADD_ROOT((void *) (&BGl_cfunctionz00zzforeign_ctypez00));
		     ADD_ROOT((void *) (&BGl_cptrz00zzforeign_ctypez00));
		     ADD_ROOT((void *) (&BGl_cstructza2za2zzforeign_ctypez00));
		     ADD_ROOT((void *) (&BGl_cenumz00zzforeign_ctypez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long
		BgL_checksumz00_3984, char *BgL_fromz00_3985)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_ctypez00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_ctypez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_ctypez00();
					BGl_libraryzd2moduleszd2initz00zzforeign_ctypez00();
					BGl_cnstzd2initzd2zzforeign_ctypez00();
					BGl_importedzd2moduleszd2initz00zzforeign_ctypez00();
					BGl_objectzd2initzd2zzforeign_ctypez00();
					return BGl_methodzd2initzd2zzforeign_ctypez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"foreign_ctype");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"foreign_ctype");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.scm 15 */
			{	/* Foreign/ctype.scm 15 */
				obj_t BgL_cportz00_3607;

				{	/* Foreign/ctype.scm 15 */
					obj_t BgL_stringz00_3614;

					BgL_stringz00_3614 = BGl_string2186z00zzforeign_ctypez00;
					{	/* Foreign/ctype.scm 15 */
						obj_t BgL_startz00_3615;

						BgL_startz00_3615 = BINT(0L);
						{	/* Foreign/ctype.scm 15 */
							obj_t BgL_endz00_3616;

							BgL_endz00_3616 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3614)));
							{	/* Foreign/ctype.scm 15 */

								BgL_cportz00_3607 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3614, BgL_startz00_3615, BgL_endz00_3616);
				}}}}
				{
					long BgL_iz00_3608;

					BgL_iz00_3608 = 44L;
				BgL_loopz00_3609:
					if ((BgL_iz00_3608 == -1L))
						{	/* Foreign/ctype.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Foreign/ctype.scm 15 */
							{	/* Foreign/ctype.scm 15 */
								obj_t BgL_arg2187z00_3610;

								{	/* Foreign/ctype.scm 15 */

									{	/* Foreign/ctype.scm 15 */
										obj_t BgL_locationz00_3612;

										BgL_locationz00_3612 = BBOOL(((bool_t) 0));
										{	/* Foreign/ctype.scm 15 */

											BgL_arg2187z00_3610 =
												BGl_readz00zz__readerz00(BgL_cportz00_3607,
												BgL_locationz00_3612);
										}
									}
								}
								{	/* Foreign/ctype.scm 15 */
									int BgL_tmpz00_4014;

									BgL_tmpz00_4014 = (int) (BgL_iz00_3608);
									CNST_TABLE_SET(BgL_tmpz00_4014, BgL_arg2187z00_3610);
							}}
							{	/* Foreign/ctype.scm 15 */
								int BgL_auxz00_3613;

								BgL_auxz00_3613 = (int) ((BgL_iz00_3608 - 1L));
								{
									long BgL_iz00_4019;

									BgL_iz00_4019 = (long) (BgL_auxz00_3613);
									BgL_iz00_3608 = BgL_iz00_4019;
									goto BgL_loopz00_3609;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* make-calias */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2caliaszd2zzforeign_ctypez00(obj_t
		BgL_id1323z00_3, obj_t BgL_name1324z00_4, obj_t BgL_siza7e1325za7_5,
		obj_t BgL_class1326z00_6, obj_t BgL_coercezd2to1327zd2_7,
		obj_t BgL_parents1328z00_8, bool_t BgL_initzf31329zf3_9,
		bool_t BgL_magiczf31330zf3_10, obj_t BgL_z421331z42_11,
		obj_t BgL_alias1332z00_12, obj_t BgL_pointedzd2tozd2by1333z00_13,
		obj_t BgL_tvector1334z00_14, obj_t BgL_location1335z00_15,
		obj_t BgL_importzd2location1336zd2_16, int BgL_occurrence1337z00_17,
		bool_t BgL_arrayzf31338zf3_18)
	{
		{	/* Foreign/ctype.sch 284 */
			{	/* Foreign/ctype.sch 284 */
				BgL_typez00_bglt BgL_new1228z00_3618;

				{	/* Foreign/ctype.sch 284 */
					BgL_typez00_bglt BgL_tmp1225z00_3619;
					BgL_caliasz00_bglt BgL_wide1226z00_3620;

					{
						BgL_typez00_bglt BgL_auxz00_4022;

						{	/* Foreign/ctype.sch 284 */
							BgL_typez00_bglt BgL_new1224z00_3621;

							BgL_new1224z00_3621 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Foreign/ctype.sch 284 */
								long BgL_arg1370z00_3622;

								BgL_arg1370z00_3622 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1224z00_3621),
									BgL_arg1370z00_3622);
							}
							{	/* Foreign/ctype.sch 284 */
								BgL_objectz00_bglt BgL_tmpz00_4027;

								BgL_tmpz00_4027 = ((BgL_objectz00_bglt) BgL_new1224z00_3621);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4027, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1224z00_3621);
							BgL_auxz00_4022 = BgL_new1224z00_3621;
						}
						BgL_tmp1225z00_3619 = ((BgL_typez00_bglt) BgL_auxz00_4022);
					}
					BgL_wide1226z00_3620 =
						((BgL_caliasz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_caliasz00_bgl))));
					{	/* Foreign/ctype.sch 284 */
						obj_t BgL_auxz00_4035;
						BgL_objectz00_bglt BgL_tmpz00_4033;

						BgL_auxz00_4035 = ((obj_t) BgL_wide1226z00_3620);
						BgL_tmpz00_4033 = ((BgL_objectz00_bglt) BgL_tmp1225z00_3619);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4033, BgL_auxz00_4035);
					}
					((BgL_objectz00_bglt) BgL_tmp1225z00_3619);
					{	/* Foreign/ctype.sch 284 */
						long BgL_arg1367z00_3623;

						BgL_arg1367z00_3623 =
							BGL_CLASS_NUM(BGl_caliasz00zzforeign_ctypez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1225z00_3619), BgL_arg1367z00_3623);
					}
					BgL_new1228z00_3618 = ((BgL_typez00_bglt) BgL_tmp1225z00_3619);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1228z00_3618)))->BgL_idz00) =
					((obj_t) BgL_id1323z00_3), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_namez00) =
					((obj_t) BgL_name1324z00_4), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1325za7_5), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_classz00) =
					((obj_t) BgL_class1326z00_6), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1327zd2_7), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_parentsz00) =
					((obj_t) BgL_parents1328z00_8), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31329zf3_9), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31330zf3_10), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_z42z42) =
					((obj_t) BgL_z421331z42_11), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_aliasz00) =
					((obj_t) BgL_alias1332z00_12), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1333z00_13), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1334z00_14), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_locationz00) =
					((obj_t) BgL_location1335z00_15), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1336zd2_16), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1228z00_3618)))->BgL_occurrencez00) =
					((int) BgL_occurrence1337z00_17), BUNSPEC);
				{
					BgL_caliasz00_bglt BgL_auxz00_4075;

					{
						obj_t BgL_auxz00_4076;

						{	/* Foreign/ctype.sch 284 */
							BgL_objectz00_bglt BgL_tmpz00_4077;

							BgL_tmpz00_4077 = ((BgL_objectz00_bglt) BgL_new1228z00_3618);
							BgL_auxz00_4076 = BGL_OBJECT_WIDENING(BgL_tmpz00_4077);
						}
						BgL_auxz00_4075 = ((BgL_caliasz00_bglt) BgL_auxz00_4076);
					}
					((((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_4075))->BgL_arrayzf3zf3) =
						((bool_t) BgL_arrayzf31338zf3_18), BUNSPEC);
				}
				return BgL_new1228z00_3618;
			}
		}

	}



/* &make-calias */
	BgL_typez00_bglt BGl_z62makezd2caliaszb0zzforeign_ctypez00(obj_t
		BgL_envz00_2580, obj_t BgL_id1323z00_2581, obj_t BgL_name1324z00_2582,
		obj_t BgL_siza7e1325za7_2583, obj_t BgL_class1326z00_2584,
		obj_t BgL_coercezd2to1327zd2_2585, obj_t BgL_parents1328z00_2586,
		obj_t BgL_initzf31329zf3_2587, obj_t BgL_magiczf31330zf3_2588,
		obj_t BgL_z421331z42_2589, obj_t BgL_alias1332z00_2590,
		obj_t BgL_pointedzd2tozd2by1333z00_2591, obj_t BgL_tvector1334z00_2592,
		obj_t BgL_location1335z00_2593, obj_t BgL_importzd2location1336zd2_2594,
		obj_t BgL_occurrence1337z00_2595, obj_t BgL_arrayzf31338zf3_2596)
	{
		{	/* Foreign/ctype.sch 284 */
			return
				BGl_makezd2caliaszd2zzforeign_ctypez00(BgL_id1323z00_2581,
				BgL_name1324z00_2582, BgL_siza7e1325za7_2583, BgL_class1326z00_2584,
				BgL_coercezd2to1327zd2_2585, BgL_parents1328z00_2586,
				CBOOL(BgL_initzf31329zf3_2587), CBOOL(BgL_magiczf31330zf3_2588),
				BgL_z421331z42_2589, BgL_alias1332z00_2590,
				BgL_pointedzd2tozd2by1333z00_2591, BgL_tvector1334z00_2592,
				BgL_location1335z00_2593, BgL_importzd2location1336zd2_2594,
				CINT(BgL_occurrence1337z00_2595), CBOOL(BgL_arrayzf31338zf3_2596));
		}

	}



/* calias? */
	BGL_EXPORTED_DEF bool_t BGl_caliaszf3zf3zzforeign_ctypez00(obj_t
		BgL_objz00_19)
	{
		{	/* Foreign/ctype.sch 285 */
			{	/* Foreign/ctype.sch 285 */
				obj_t BgL_classz00_3624;

				BgL_classz00_3624 = BGl_caliasz00zzforeign_ctypez00;
				if (BGL_OBJECTP(BgL_objz00_19))
					{	/* Foreign/ctype.sch 285 */
						BgL_objectz00_bglt BgL_arg1807z00_3625;

						BgL_arg1807z00_3625 = (BgL_objectz00_bglt) (BgL_objz00_19);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Foreign/ctype.sch 285 */
								long BgL_idxz00_3626;

								BgL_idxz00_3626 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3625);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3626 + 2L)) == BgL_classz00_3624);
							}
						else
							{	/* Foreign/ctype.sch 285 */
								bool_t BgL_res2099z00_3629;

								{	/* Foreign/ctype.sch 285 */
									obj_t BgL_oclassz00_3630;

									{	/* Foreign/ctype.sch 285 */
										obj_t BgL_arg1815z00_3631;
										long BgL_arg1816z00_3632;

										BgL_arg1815z00_3631 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Foreign/ctype.sch 285 */
											long BgL_arg1817z00_3633;

											BgL_arg1817z00_3633 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3625);
											BgL_arg1816z00_3632 = (BgL_arg1817z00_3633 - OBJECT_TYPE);
										}
										BgL_oclassz00_3630 =
											VECTOR_REF(BgL_arg1815z00_3631, BgL_arg1816z00_3632);
									}
									{	/* Foreign/ctype.sch 285 */
										bool_t BgL__ortest_1115z00_3634;

										BgL__ortest_1115z00_3634 =
											(BgL_classz00_3624 == BgL_oclassz00_3630);
										if (BgL__ortest_1115z00_3634)
											{	/* Foreign/ctype.sch 285 */
												BgL_res2099z00_3629 = BgL__ortest_1115z00_3634;
											}
										else
											{	/* Foreign/ctype.sch 285 */
												long BgL_odepthz00_3635;

												{	/* Foreign/ctype.sch 285 */
													obj_t BgL_arg1804z00_3636;

													BgL_arg1804z00_3636 = (BgL_oclassz00_3630);
													BgL_odepthz00_3635 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3636);
												}
												if ((2L < BgL_odepthz00_3635))
													{	/* Foreign/ctype.sch 285 */
														obj_t BgL_arg1802z00_3637;

														{	/* Foreign/ctype.sch 285 */
															obj_t BgL_arg1803z00_3638;

															BgL_arg1803z00_3638 = (BgL_oclassz00_3630);
															BgL_arg1802z00_3637 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3638,
																2L);
														}
														BgL_res2099z00_3629 =
															(BgL_arg1802z00_3637 == BgL_classz00_3624);
													}
												else
													{	/* Foreign/ctype.sch 285 */
														BgL_res2099z00_3629 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2099z00_3629;
							}
					}
				else
					{	/* Foreign/ctype.sch 285 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &calias? */
	obj_t BGl_z62caliaszf3z91zzforeign_ctypez00(obj_t BgL_envz00_2597,
		obj_t BgL_objz00_2598)
	{
		{	/* Foreign/ctype.sch 285 */
			return BBOOL(BGl_caliaszf3zf3zzforeign_ctypez00(BgL_objz00_2598));
		}

	}



/* calias-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_caliaszd2nilzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.sch 286 */
			{	/* Foreign/ctype.sch 286 */
				obj_t BgL_classz00_1489;

				BgL_classz00_1489 = BGl_caliasz00zzforeign_ctypez00;
				{	/* Foreign/ctype.sch 286 */
					obj_t BgL__ortest_1117z00_1490;

					BgL__ortest_1117z00_1490 = BGL_CLASS_NIL(BgL_classz00_1489);
					if (CBOOL(BgL__ortest_1117z00_1490))
						{	/* Foreign/ctype.sch 286 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1490);
						}
					else
						{	/* Foreign/ctype.sch 286 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1489));
						}
				}
			}
		}

	}



/* &calias-nil */
	BgL_typez00_bglt BGl_z62caliaszd2nilzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2599)
	{
		{	/* Foreign/ctype.sch 286 */
			return BGl_caliaszd2nilzd2zzforeign_ctypez00();
		}

	}



/* calias-array? */
	BGL_EXPORTED_DEF bool_t
		BGl_caliaszd2arrayzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_20)
	{
		{	/* Foreign/ctype.sch 287 */
			{
				BgL_caliasz00_bglt BgL_auxz00_4118;

				{
					obj_t BgL_auxz00_4119;

					{	/* Foreign/ctype.sch 287 */
						BgL_objectz00_bglt BgL_tmpz00_4120;

						BgL_tmpz00_4120 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_4119 = BGL_OBJECT_WIDENING(BgL_tmpz00_4120);
					}
					BgL_auxz00_4118 = ((BgL_caliasz00_bglt) BgL_auxz00_4119);
				}
				return
					(((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_4118))->BgL_arrayzf3zf3);
			}
		}

	}



/* &calias-array? */
	obj_t BGl_z62caliaszd2arrayzf3z43zzforeign_ctypez00(obj_t BgL_envz00_2600,
		obj_t BgL_oz00_2601)
	{
		{	/* Foreign/ctype.sch 287 */
			return
				BBOOL(BGl_caliaszd2arrayzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2601)));
		}

	}



/* calias-array?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2arrayzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_21, bool_t BgL_vz00_22)
	{
		{	/* Foreign/ctype.sch 288 */
			{
				BgL_caliasz00_bglt BgL_auxz00_4128;

				{
					obj_t BgL_auxz00_4129;

					{	/* Foreign/ctype.sch 288 */
						BgL_objectz00_bglt BgL_tmpz00_4130;

						BgL_tmpz00_4130 = ((BgL_objectz00_bglt) BgL_oz00_21);
						BgL_auxz00_4129 = BGL_OBJECT_WIDENING(BgL_tmpz00_4130);
					}
					BgL_auxz00_4128 = ((BgL_caliasz00_bglt) BgL_auxz00_4129);
				}
				return
					((((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_4128))->BgL_arrayzf3zf3) =
					((bool_t) BgL_vz00_22), BUNSPEC);
			}
		}

	}



/* &calias-array?-set! */
	obj_t BGl_z62caliaszd2arrayzf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2602, obj_t BgL_oz00_2603, obj_t BgL_vz00_2604)
	{
		{	/* Foreign/ctype.sch 288 */
			return
				BGl_caliaszd2arrayzf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2603), CBOOL(BgL_vz00_2604));
		}

	}



/* calias-occurrence */
	BGL_EXPORTED_DEF int
		BGl_caliaszd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_23)
	{
		{	/* Foreign/ctype.sch 289 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_23)))->BgL_occurrencez00);
		}

	}



/* &calias-occurrence */
	obj_t BGl_z62caliaszd2occurrencezb0zzforeign_ctypez00(obj_t BgL_envz00_2605,
		obj_t BgL_oz00_2606)
	{
		{	/* Foreign/ctype.sch 289 */
			return
				BINT(BGl_caliaszd2occurrencezd2zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2606)));
		}

	}



/* calias-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_24, int BgL_vz00_25)
	{
		{	/* Foreign/ctype.sch 290 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_24)))->BgL_occurrencez00) =
				((int) BgL_vz00_25), BUNSPEC);
		}

	}



/* &calias-occurrence-set! */
	obj_t BGl_z62caliaszd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2607, obj_t BgL_oz00_2608, obj_t BgL_vz00_2609)
	{
		{	/* Foreign/ctype.sch 290 */
			return
				BGl_caliaszd2occurrencezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2608), CINT(BgL_vz00_2609));
		}

	}



/* calias-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_26)
	{
		{	/* Foreign/ctype.sch 291 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_26)))->BgL_importzd2locationzd2);
		}

	}



/* &calias-import-location */
	obj_t BGl_z62caliaszd2importzd2locationz62zzforeign_ctypez00(obj_t
		BgL_envz00_2610, obj_t BgL_oz00_2611)
	{
		{	/* Foreign/ctype.sch 291 */
			return
				BGl_caliaszd2importzd2locationz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2611));
		}

	}



/* calias-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_27, obj_t BgL_vz00_28)
	{
		{	/* Foreign/ctype.sch 292 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_27)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_28), BUNSPEC);
		}

	}



/* &calias-import-location-set! */
	obj_t BGl_z62caliaszd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_2612, obj_t BgL_oz00_2613, obj_t BgL_vz00_2614)
	{
		{	/* Foreign/ctype.sch 292 */
			return
				BGl_caliaszd2importzd2locationzd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2613), BgL_vz00_2614);
		}

	}



/* calias-location */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_29)
	{
		{	/* Foreign/ctype.sch 293 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_29)))->BgL_locationz00);
		}

	}



/* &calias-location */
	obj_t BGl_z62caliaszd2locationzb0zzforeign_ctypez00(obj_t BgL_envz00_2615,
		obj_t BgL_oz00_2616)
	{
		{	/* Foreign/ctype.sch 293 */
			return
				BGl_caliaszd2locationzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2616));
		}

	}



/* calias-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_30, obj_t BgL_vz00_31)
	{
		{	/* Foreign/ctype.sch 294 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_30)))->BgL_locationz00) =
				((obj_t) BgL_vz00_31), BUNSPEC);
		}

	}



/* &calias-location-set! */
	obj_t BGl_z62caliaszd2locationzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2617, obj_t BgL_oz00_2618, obj_t BgL_vz00_2619)
	{
		{	/* Foreign/ctype.sch 294 */
			return
				BGl_caliaszd2locationzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2618), BgL_vz00_2619);
		}

	}



/* calias-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_32)
	{
		{	/* Foreign/ctype.sch 295 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_32)))->BgL_tvectorz00);
		}

	}



/* &calias-tvector */
	obj_t BGl_z62caliaszd2tvectorzb0zzforeign_ctypez00(obj_t BgL_envz00_2620,
		obj_t BgL_oz00_2621)
	{
		{	/* Foreign/ctype.sch 295 */
			return
				BGl_caliaszd2tvectorzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2621));
		}

	}



/* calias-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_33, obj_t BgL_vz00_34)
	{
		{	/* Foreign/ctype.sch 296 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_33)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_34), BUNSPEC);
		}

	}



/* &calias-tvector-set! */
	obj_t BGl_z62caliaszd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2622, obj_t BgL_oz00_2623, obj_t BgL_vz00_2624)
	{
		{	/* Foreign/ctype.sch 296 */
			return
				BGl_caliaszd2tvectorzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2623), BgL_vz00_2624);
		}

	}



/* calias-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_35)
	{
		{	/* Foreign/ctype.sch 297 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_35)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &calias-pointed-to-by */
	obj_t BGl_z62caliaszd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2625, obj_t BgL_oz00_2626)
	{
		{	/* Foreign/ctype.sch 297 */
			return
				BGl_caliaszd2pointedzd2tozd2byzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2626));
		}

	}



/* calias-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_36, obj_t BgL_vz00_37)
	{
		{	/* Foreign/ctype.sch 298 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_36)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_37), BUNSPEC);
		}

	}



/* &calias-pointed-to-by-set! */
	obj_t BGl_z62caliaszd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2627, obj_t BgL_oz00_2628, obj_t BgL_vz00_2629)
	{
		{	/* Foreign/ctype.sch 298 */
			return
				BGl_caliaszd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2628), BgL_vz00_2629);
		}

	}



/* calias-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_38)
	{
		{	/* Foreign/ctype.sch 299 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_38)))->BgL_aliasz00);
		}

	}



/* &calias-alias */
	obj_t BGl_z62caliaszd2aliaszb0zzforeign_ctypez00(obj_t BgL_envz00_2630,
		obj_t BgL_oz00_2631)
	{
		{	/* Foreign/ctype.sch 299 */
			return
				BGl_caliaszd2aliaszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2631));
		}

	}



/* calias-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_39, obj_t BgL_vz00_40)
	{
		{	/* Foreign/ctype.sch 300 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_39)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_40), BUNSPEC);
		}

	}



/* &calias-alias-set! */
	obj_t BGl_z62caliaszd2aliaszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2632, obj_t BgL_oz00_2633, obj_t BgL_vz00_2634)
	{
		{	/* Foreign/ctype.sch 300 */
			return
				BGl_caliaszd2aliaszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2633), BgL_vz00_2634);
		}

	}



/* calias-$ */
	BGL_EXPORTED_DEF obj_t BGl_caliaszd2z42z90zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_41)
	{
		{	/* Foreign/ctype.sch 301 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_41)))->BgL_z42z42);
		}

	}



/* &calias-$ */
	obj_t BGl_z62caliaszd2z42zf2zzforeign_ctypez00(obj_t BgL_envz00_2635,
		obj_t BgL_oz00_2636)
	{
		{	/* Foreign/ctype.sch 301 */
			return
				BGl_caliaszd2z42z90zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2636));
		}

	}



/* calias-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_42,
		obj_t BgL_vz00_43)
	{
		{	/* Foreign/ctype.sch 302 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_42)))->BgL_z42z42) =
				((obj_t) BgL_vz00_43), BUNSPEC);
		}

	}



/* &calias-$-set! */
	obj_t BGl_z62caliaszd2z42zd2setz12z32zzforeign_ctypez00(obj_t BgL_envz00_2637,
		obj_t BgL_oz00_2638, obj_t BgL_vz00_2639)
	{
		{	/* Foreign/ctype.sch 302 */
			return
				BGl_caliaszd2z42zd2setz12z50zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2638), BgL_vz00_2639);
		}

	}



/* calias-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_caliaszd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_44)
	{
		{	/* Foreign/ctype.sch 303 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_44)))->BgL_magiczf3zf3);
		}

	}



/* &calias-magic? */
	obj_t BGl_z62caliaszd2magiczf3z43zzforeign_ctypez00(obj_t BgL_envz00_2640,
		obj_t BgL_oz00_2641)
	{
		{	/* Foreign/ctype.sch 303 */
			return
				BBOOL(BGl_caliaszd2magiczf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2641)));
		}

	}



/* calias-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_45, bool_t BgL_vz00_46)
	{
		{	/* Foreign/ctype.sch 304 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_45)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_46), BUNSPEC);
		}

	}



/* &calias-magic?-set! */
	obj_t BGl_z62caliaszd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2642, obj_t BgL_oz00_2643, obj_t BgL_vz00_2644)
	{
		{	/* Foreign/ctype.sch 304 */
			return
				BGl_caliaszd2magiczf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2643), CBOOL(BgL_vz00_2644));
		}

	}



/* calias-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_caliaszd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_47)
	{
		{	/* Foreign/ctype.sch 305 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_47)))->BgL_initzf3zf3);
		}

	}



/* &calias-init? */
	obj_t BGl_z62caliaszd2initzf3z43zzforeign_ctypez00(obj_t BgL_envz00_2645,
		obj_t BgL_oz00_2646)
	{
		{	/* Foreign/ctype.sch 305 */
			return
				BBOOL(BGl_caliaszd2initzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2646)));
		}

	}



/* calias-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_48, bool_t BgL_vz00_49)
	{
		{	/* Foreign/ctype.sch 306 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_48)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_49), BUNSPEC);
		}

	}



/* &calias-init?-set! */
	obj_t BGl_z62caliaszd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2647, obj_t BgL_oz00_2648, obj_t BgL_vz00_2649)
	{
		{	/* Foreign/ctype.sch 306 */
			return
				BGl_caliaszd2initzf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2648), CBOOL(BgL_vz00_2649));
		}

	}



/* calias-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_50)
	{
		{	/* Foreign/ctype.sch 307 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_50)))->BgL_parentsz00);
		}

	}



/* &calias-parents */
	obj_t BGl_z62caliaszd2parentszb0zzforeign_ctypez00(obj_t BgL_envz00_2650,
		obj_t BgL_oz00_2651)
	{
		{	/* Foreign/ctype.sch 307 */
			return
				BGl_caliaszd2parentszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2651));
		}

	}



/* calias-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_51, obj_t BgL_vz00_52)
	{
		{	/* Foreign/ctype.sch 308 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_51)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_52), BUNSPEC);
		}

	}



/* &calias-parents-set! */
	obj_t BGl_z62caliaszd2parentszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2652, obj_t BgL_oz00_2653, obj_t BgL_vz00_2654)
	{
		{	/* Foreign/ctype.sch 308 */
			return
				BGl_caliaszd2parentszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2653), BgL_vz00_2654);
		}

	}



/* calias-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_53)
	{
		{	/* Foreign/ctype.sch 309 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_53)))->BgL_coercezd2tozd2);
		}

	}



/* &calias-coerce-to */
	obj_t BGl_z62caliaszd2coercezd2toz62zzforeign_ctypez00(obj_t BgL_envz00_2655,
		obj_t BgL_oz00_2656)
	{
		{	/* Foreign/ctype.sch 309 */
			return
				BGl_caliaszd2coercezd2toz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2656));
		}

	}



/* calias-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_54, obj_t BgL_vz00_55)
	{
		{	/* Foreign/ctype.sch 310 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_54)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_55), BUNSPEC);
		}

	}



/* &calias-coerce-to-set! */
	obj_t BGl_z62caliaszd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_2657, obj_t BgL_oz00_2658, obj_t BgL_vz00_2659)
	{
		{	/* Foreign/ctype.sch 310 */
			return
				BGl_caliaszd2coercezd2tozd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2658), BgL_vz00_2659);
		}

	}



/* calias-class */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2classzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_56)
	{
		{	/* Foreign/ctype.sch 311 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_56)))->BgL_classz00);
		}

	}



/* &calias-class */
	obj_t BGl_z62caliaszd2classzb0zzforeign_ctypez00(obj_t BgL_envz00_2660,
		obj_t BgL_oz00_2661)
	{
		{	/* Foreign/ctype.sch 311 */
			return
				BGl_caliaszd2classzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2661));
		}

	}



/* calias-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_57, obj_t BgL_vz00_58)
	{
		{	/* Foreign/ctype.sch 312 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_57)))->BgL_classz00) =
				((obj_t) BgL_vz00_58), BUNSPEC);
		}

	}



/* &calias-class-set! */
	obj_t BGl_z62caliaszd2classzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2662, obj_t BgL_oz00_2663, obj_t BgL_vz00_2664)
	{
		{	/* Foreign/ctype.sch 312 */
			return
				BGl_caliaszd2classzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2663), BgL_vz00_2664);
		}

	}



/* calias-size */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_59)
	{
		{	/* Foreign/ctype.sch 313 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_59)))->BgL_siza7eza7);
		}

	}



/* &calias-size */
	obj_t BGl_z62caliaszd2siza7ez17zzforeign_ctypez00(obj_t BgL_envz00_2665,
		obj_t BgL_oz00_2666)
	{
		{	/* Foreign/ctype.sch 313 */
			return
				BGl_caliaszd2siza7ez75zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2666));
		}

	}



/* calias-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_60, obj_t BgL_vz00_61)
	{
		{	/* Foreign/ctype.sch 314 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_60)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_61), BUNSPEC);
		}

	}



/* &calias-size-set! */
	obj_t BGl_z62caliaszd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t
		BgL_envz00_2667, obj_t BgL_oz00_2668, obj_t BgL_vz00_2669)
	{
		{	/* Foreign/ctype.sch 314 */
			return
				BGl_caliaszd2siza7ezd2setz12zb5zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2668), BgL_vz00_2669);
		}

	}



/* calias-name */
	BGL_EXPORTED_DEF obj_t BGl_caliaszd2namezd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_62)
	{
		{	/* Foreign/ctype.sch 315 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_62)))->BgL_namez00);
		}

	}



/* &calias-name */
	obj_t BGl_z62caliaszd2namezb0zzforeign_ctypez00(obj_t BgL_envz00_2670,
		obj_t BgL_oz00_2671)
	{
		{	/* Foreign/ctype.sch 315 */
			return
				BGl_caliaszd2namezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2671));
		}

	}



/* calias-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_caliaszd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_63, obj_t BgL_vz00_64)
	{
		{	/* Foreign/ctype.sch 316 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_63)))->BgL_namez00) =
				((obj_t) BgL_vz00_64), BUNSPEC);
		}

	}



/* &calias-name-set! */
	obj_t BGl_z62caliaszd2namezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2672, obj_t BgL_oz00_2673, obj_t BgL_vz00_2674)
	{
		{	/* Foreign/ctype.sch 316 */
			return
				BGl_caliaszd2namezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2673), BgL_vz00_2674);
		}

	}



/* calias-id */
	BGL_EXPORTED_DEF obj_t BGl_caliaszd2idzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_65)
	{
		{	/* Foreign/ctype.sch 317 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_65)))->BgL_idz00);
		}

	}



/* &calias-id */
	obj_t BGl_z62caliaszd2idzb0zzforeign_ctypez00(obj_t BgL_envz00_2675,
		obj_t BgL_oz00_2676)
	{
		{	/* Foreign/ctype.sch 317 */
			return
				BGl_caliaszd2idzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2676));
		}

	}



/* make-cenum */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2cenumzd2zzforeign_ctypez00(obj_t
		BgL_id1305z00_68, obj_t BgL_name1306z00_69, obj_t BgL_siza7e1307za7_70,
		obj_t BgL_class1308z00_71, obj_t BgL_coercezd2to1309zd2_72,
		obj_t BgL_parents1310z00_73, bool_t BgL_initzf31311zf3_74,
		bool_t BgL_magiczf31312zf3_75, obj_t BgL_z421313z42_76,
		obj_t BgL_alias1314z00_77, obj_t BgL_pointedzd2tozd2by1315z00_78,
		obj_t BgL_tvector1316z00_79, obj_t BgL_location1317z00_80,
		obj_t BgL_importzd2location1318zd2_81, int BgL_occurrence1319z00_82,
		BgL_typez00_bglt BgL_btype1320z00_83, obj_t BgL_literals1321z00_84)
	{
		{	/* Foreign/ctype.sch 321 */
			{	/* Foreign/ctype.sch 321 */
				BgL_typez00_bglt BgL_new1232z00_3639;

				{	/* Foreign/ctype.sch 321 */
					BgL_typez00_bglt BgL_tmp1230z00_3640;
					BgL_cenumz00_bglt BgL_wide1231z00_3641;

					{
						BgL_typez00_bglt BgL_auxz00_4260;

						{	/* Foreign/ctype.sch 321 */
							BgL_typez00_bglt BgL_new1229z00_3642;

							BgL_new1229z00_3642 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Foreign/ctype.sch 321 */
								long BgL_arg1375z00_3643;

								BgL_arg1375z00_3643 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1229z00_3642),
									BgL_arg1375z00_3643);
							}
							{	/* Foreign/ctype.sch 321 */
								BgL_objectz00_bglt BgL_tmpz00_4265;

								BgL_tmpz00_4265 = ((BgL_objectz00_bglt) BgL_new1229z00_3642);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4265, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1229z00_3642);
							BgL_auxz00_4260 = BgL_new1229z00_3642;
						}
						BgL_tmp1230z00_3640 = ((BgL_typez00_bglt) BgL_auxz00_4260);
					}
					BgL_wide1231z00_3641 =
						((BgL_cenumz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cenumz00_bgl))));
					{	/* Foreign/ctype.sch 321 */
						obj_t BgL_auxz00_4273;
						BgL_objectz00_bglt BgL_tmpz00_4271;

						BgL_auxz00_4273 = ((obj_t) BgL_wide1231z00_3641);
						BgL_tmpz00_4271 = ((BgL_objectz00_bglt) BgL_tmp1230z00_3640);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4271, BgL_auxz00_4273);
					}
					((BgL_objectz00_bglt) BgL_tmp1230z00_3640);
					{	/* Foreign/ctype.sch 321 */
						long BgL_arg1371z00_3644;

						BgL_arg1371z00_3644 = BGL_CLASS_NUM(BGl_cenumz00zzforeign_ctypez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1230z00_3640), BgL_arg1371z00_3644);
					}
					BgL_new1232z00_3639 = ((BgL_typez00_bglt) BgL_tmp1230z00_3640);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1232z00_3639)))->BgL_idz00) =
					((obj_t) BgL_id1305z00_68), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_namez00) =
					((obj_t) BgL_name1306z00_69), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1307za7_70), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_classz00) =
					((obj_t) BgL_class1308z00_71), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1309zd2_72), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_parentsz00) =
					((obj_t) BgL_parents1310z00_73), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31311zf3_74), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31312zf3_75), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_z42z42) =
					((obj_t) BgL_z421313z42_76), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_aliasz00) =
					((obj_t) BgL_alias1314z00_77), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1315z00_78), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1316z00_79), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_locationz00) =
					((obj_t) BgL_location1317z00_80), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1318zd2_81), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1232z00_3639)))->BgL_occurrencez00) =
					((int) BgL_occurrence1319z00_82), BUNSPEC);
				{
					BgL_cenumz00_bglt BgL_auxz00_4313;

					{
						obj_t BgL_auxz00_4314;

						{	/* Foreign/ctype.sch 321 */
							BgL_objectz00_bglt BgL_tmpz00_4315;

							BgL_tmpz00_4315 = ((BgL_objectz00_bglt) BgL_new1232z00_3639);
							BgL_auxz00_4314 = BGL_OBJECT_WIDENING(BgL_tmpz00_4315);
						}
						BgL_auxz00_4313 = ((BgL_cenumz00_bglt) BgL_auxz00_4314);
					}
					((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_4313))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_btype1320z00_83), BUNSPEC);
				}
				{
					BgL_cenumz00_bglt BgL_auxz00_4320;

					{
						obj_t BgL_auxz00_4321;

						{	/* Foreign/ctype.sch 321 */
							BgL_objectz00_bglt BgL_tmpz00_4322;

							BgL_tmpz00_4322 = ((BgL_objectz00_bglt) BgL_new1232z00_3639);
							BgL_auxz00_4321 = BGL_OBJECT_WIDENING(BgL_tmpz00_4322);
						}
						BgL_auxz00_4320 = ((BgL_cenumz00_bglt) BgL_auxz00_4321);
					}
					((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_4320))->BgL_literalsz00) =
						((obj_t) BgL_literals1321z00_84), BUNSPEC);
				}
				return BgL_new1232z00_3639;
			}
		}

	}



/* &make-cenum */
	BgL_typez00_bglt BGl_z62makezd2cenumzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2677, obj_t BgL_id1305z00_2678, obj_t BgL_name1306z00_2679,
		obj_t BgL_siza7e1307za7_2680, obj_t BgL_class1308z00_2681,
		obj_t BgL_coercezd2to1309zd2_2682, obj_t BgL_parents1310z00_2683,
		obj_t BgL_initzf31311zf3_2684, obj_t BgL_magiczf31312zf3_2685,
		obj_t BgL_z421313z42_2686, obj_t BgL_alias1314z00_2687,
		obj_t BgL_pointedzd2tozd2by1315z00_2688, obj_t BgL_tvector1316z00_2689,
		obj_t BgL_location1317z00_2690, obj_t BgL_importzd2location1318zd2_2691,
		obj_t BgL_occurrence1319z00_2692, obj_t BgL_btype1320z00_2693,
		obj_t BgL_literals1321z00_2694)
	{
		{	/* Foreign/ctype.sch 321 */
			return
				BGl_makezd2cenumzd2zzforeign_ctypez00(BgL_id1305z00_2678,
				BgL_name1306z00_2679, BgL_siza7e1307za7_2680, BgL_class1308z00_2681,
				BgL_coercezd2to1309zd2_2682, BgL_parents1310z00_2683,
				CBOOL(BgL_initzf31311zf3_2684), CBOOL(BgL_magiczf31312zf3_2685),
				BgL_z421313z42_2686, BgL_alias1314z00_2687,
				BgL_pointedzd2tozd2by1315z00_2688, BgL_tvector1316z00_2689,
				BgL_location1317z00_2690, BgL_importzd2location1318zd2_2691,
				CINT(BgL_occurrence1319z00_2692),
				((BgL_typez00_bglt) BgL_btype1320z00_2693), BgL_literals1321z00_2694);
		}

	}



/* cenum? */
	BGL_EXPORTED_DEF bool_t BGl_cenumzf3zf3zzforeign_ctypez00(obj_t BgL_objz00_85)
	{
		{	/* Foreign/ctype.sch 322 */
			{	/* Foreign/ctype.sch 322 */
				obj_t BgL_classz00_3645;

				BgL_classz00_3645 = BGl_cenumz00zzforeign_ctypez00;
				if (BGL_OBJECTP(BgL_objz00_85))
					{	/* Foreign/ctype.sch 322 */
						BgL_objectz00_bglt BgL_arg1807z00_3646;

						BgL_arg1807z00_3646 = (BgL_objectz00_bglt) (BgL_objz00_85);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Foreign/ctype.sch 322 */
								long BgL_idxz00_3647;

								BgL_idxz00_3647 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3646);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3647 + 2L)) == BgL_classz00_3645);
							}
						else
							{	/* Foreign/ctype.sch 322 */
								bool_t BgL_res2100z00_3650;

								{	/* Foreign/ctype.sch 322 */
									obj_t BgL_oclassz00_3651;

									{	/* Foreign/ctype.sch 322 */
										obj_t BgL_arg1815z00_3652;
										long BgL_arg1816z00_3653;

										BgL_arg1815z00_3652 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Foreign/ctype.sch 322 */
											long BgL_arg1817z00_3654;

											BgL_arg1817z00_3654 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3646);
											BgL_arg1816z00_3653 = (BgL_arg1817z00_3654 - OBJECT_TYPE);
										}
										BgL_oclassz00_3651 =
											VECTOR_REF(BgL_arg1815z00_3652, BgL_arg1816z00_3653);
									}
									{	/* Foreign/ctype.sch 322 */
										bool_t BgL__ortest_1115z00_3655;

										BgL__ortest_1115z00_3655 =
											(BgL_classz00_3645 == BgL_oclassz00_3651);
										if (BgL__ortest_1115z00_3655)
											{	/* Foreign/ctype.sch 322 */
												BgL_res2100z00_3650 = BgL__ortest_1115z00_3655;
											}
										else
											{	/* Foreign/ctype.sch 322 */
												long BgL_odepthz00_3656;

												{	/* Foreign/ctype.sch 322 */
													obj_t BgL_arg1804z00_3657;

													BgL_arg1804z00_3657 = (BgL_oclassz00_3651);
													BgL_odepthz00_3656 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3657);
												}
												if ((2L < BgL_odepthz00_3656))
													{	/* Foreign/ctype.sch 322 */
														obj_t BgL_arg1802z00_3658;

														{	/* Foreign/ctype.sch 322 */
															obj_t BgL_arg1803z00_3659;

															BgL_arg1803z00_3659 = (BgL_oclassz00_3651);
															BgL_arg1802z00_3658 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3659,
																2L);
														}
														BgL_res2100z00_3650 =
															(BgL_arg1802z00_3658 == BgL_classz00_3645);
													}
												else
													{	/* Foreign/ctype.sch 322 */
														BgL_res2100z00_3650 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2100z00_3650;
							}
					}
				else
					{	/* Foreign/ctype.sch 322 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cenum? */
	obj_t BGl_z62cenumzf3z91zzforeign_ctypez00(obj_t BgL_envz00_2695,
		obj_t BgL_objz00_2696)
	{
		{	/* Foreign/ctype.sch 322 */
			return BBOOL(BGl_cenumzf3zf3zzforeign_ctypez00(BgL_objz00_2696));
		}

	}



/* cenum-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_cenumzd2nilzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.sch 323 */
			{	/* Foreign/ctype.sch 323 */
				obj_t BgL_classz00_1543;

				BgL_classz00_1543 = BGl_cenumz00zzforeign_ctypez00;
				{	/* Foreign/ctype.sch 323 */
					obj_t BgL__ortest_1117z00_1544;

					BgL__ortest_1117z00_1544 = BGL_CLASS_NIL(BgL_classz00_1543);
					if (CBOOL(BgL__ortest_1117z00_1544))
						{	/* Foreign/ctype.sch 323 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1544);
						}
					else
						{	/* Foreign/ctype.sch 323 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1543));
						}
				}
			}
		}

	}



/* &cenum-nil */
	BgL_typez00_bglt BGl_z62cenumzd2nilzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2697)
	{
		{	/* Foreign/ctype.sch 323 */
			return BGl_cenumzd2nilzd2zzforeign_ctypez00();
		}

	}



/* cenum-literals */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2literalszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_86)
	{
		{	/* Foreign/ctype.sch 324 */
			{
				BgL_cenumz00_bglt BgL_auxz00_4363;

				{
					obj_t BgL_auxz00_4364;

					{	/* Foreign/ctype.sch 324 */
						BgL_objectz00_bglt BgL_tmpz00_4365;

						BgL_tmpz00_4365 = ((BgL_objectz00_bglt) BgL_oz00_86);
						BgL_auxz00_4364 = BGL_OBJECT_WIDENING(BgL_tmpz00_4365);
					}
					BgL_auxz00_4363 = ((BgL_cenumz00_bglt) BgL_auxz00_4364);
				}
				return
					(((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_4363))->BgL_literalsz00);
			}
		}

	}



/* &cenum-literals */
	obj_t BGl_z62cenumzd2literalszb0zzforeign_ctypez00(obj_t BgL_envz00_2698,
		obj_t BgL_oz00_2699)
	{
		{	/* Foreign/ctype.sch 324 */
			return
				BGl_cenumzd2literalszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2699));
		}

	}



/* cenum-btype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cenumzd2btypezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_89)
	{
		{	/* Foreign/ctype.sch 326 */
			{
				BgL_cenumz00_bglt BgL_auxz00_4372;

				{
					obj_t BgL_auxz00_4373;

					{	/* Foreign/ctype.sch 326 */
						BgL_objectz00_bglt BgL_tmpz00_4374;

						BgL_tmpz00_4374 = ((BgL_objectz00_bglt) BgL_oz00_89);
						BgL_auxz00_4373 = BGL_OBJECT_WIDENING(BgL_tmpz00_4374);
					}
					BgL_auxz00_4372 = ((BgL_cenumz00_bglt) BgL_auxz00_4373);
				}
				return (((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_4372))->BgL_btypez00);
			}
		}

	}



/* &cenum-btype */
	BgL_typez00_bglt BGl_z62cenumzd2btypezb0zzforeign_ctypez00(obj_t
		BgL_envz00_2700, obj_t BgL_oz00_2701)
	{
		{	/* Foreign/ctype.sch 326 */
			return
				BGl_cenumzd2btypezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2701));
		}

	}



/* cenum-occurrence */
	BGL_EXPORTED_DEF int
		BGl_cenumzd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_92)
	{
		{	/* Foreign/ctype.sch 328 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_92)))->BgL_occurrencez00);
		}

	}



/* &cenum-occurrence */
	obj_t BGl_z62cenumzd2occurrencezb0zzforeign_ctypez00(obj_t BgL_envz00_2702,
		obj_t BgL_oz00_2703)
	{
		{	/* Foreign/ctype.sch 328 */
			return
				BINT(BGl_cenumzd2occurrencezd2zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2703)));
		}

	}



/* cenum-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_93, int BgL_vz00_94)
	{
		{	/* Foreign/ctype.sch 329 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_93)))->BgL_occurrencez00) =
				((int) BgL_vz00_94), BUNSPEC);
		}

	}



/* &cenum-occurrence-set! */
	obj_t BGl_z62cenumzd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2704, obj_t BgL_oz00_2705, obj_t BgL_vz00_2706)
	{
		{	/* Foreign/ctype.sch 329 */
			return
				BGl_cenumzd2occurrencezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2705), CINT(BgL_vz00_2706));
		}

	}



/* cenum-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_95)
	{
		{	/* Foreign/ctype.sch 330 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_95)))->BgL_importzd2locationzd2);
		}

	}



/* &cenum-import-location */
	obj_t BGl_z62cenumzd2importzd2locationz62zzforeign_ctypez00(obj_t
		BgL_envz00_2707, obj_t BgL_oz00_2708)
	{
		{	/* Foreign/ctype.sch 330 */
			return
				BGl_cenumzd2importzd2locationz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2708));
		}

	}



/* cenum-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2importzd2locationzd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_96, obj_t BgL_vz00_97)
	{
		{	/* Foreign/ctype.sch 331 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_96)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_97), BUNSPEC);
		}

	}



/* &cenum-import-location-set! */
	obj_t BGl_z62cenumzd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_2709, obj_t BgL_oz00_2710, obj_t BgL_vz00_2711)
	{
		{	/* Foreign/ctype.sch 331 */
			return
				BGl_cenumzd2importzd2locationzd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2710), BgL_vz00_2711);
		}

	}



/* cenum-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_98)
	{
		{	/* Foreign/ctype.sch 332 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_98)))->BgL_locationz00);
		}

	}



/* &cenum-location */
	obj_t BGl_z62cenumzd2locationzb0zzforeign_ctypez00(obj_t BgL_envz00_2712,
		obj_t BgL_oz00_2713)
	{
		{	/* Foreign/ctype.sch 332 */
			return
				BGl_cenumzd2locationzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2713));
		}

	}



/* cenum-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_99, obj_t BgL_vz00_100)
	{
		{	/* Foreign/ctype.sch 333 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_99)))->BgL_locationz00) =
				((obj_t) BgL_vz00_100), BUNSPEC);
		}

	}



/* &cenum-location-set! */
	obj_t BGl_z62cenumzd2locationzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2714, obj_t BgL_oz00_2715, obj_t BgL_vz00_2716)
	{
		{	/* Foreign/ctype.sch 333 */
			return
				BGl_cenumzd2locationzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2715), BgL_vz00_2716);
		}

	}



/* cenum-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_101)
	{
		{	/* Foreign/ctype.sch 334 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_101)))->BgL_tvectorz00);
		}

	}



/* &cenum-tvector */
	obj_t BGl_z62cenumzd2tvectorzb0zzforeign_ctypez00(obj_t BgL_envz00_2717,
		obj_t BgL_oz00_2718)
	{
		{	/* Foreign/ctype.sch 334 */
			return
				BGl_cenumzd2tvectorzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2718));
		}

	}



/* cenum-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_102, obj_t BgL_vz00_103)
	{
		{	/* Foreign/ctype.sch 335 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_102)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_103), BUNSPEC);
		}

	}



/* &cenum-tvector-set! */
	obj_t BGl_z62cenumzd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2719, obj_t BgL_oz00_2720, obj_t BgL_vz00_2721)
	{
		{	/* Foreign/ctype.sch 335 */
			return
				BGl_cenumzd2tvectorzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2720), BgL_vz00_2721);
		}

	}



/* cenum-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_104)
	{
		{	/* Foreign/ctype.sch 336 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_104)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &cenum-pointed-to-by */
	obj_t BGl_z62cenumzd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2722, obj_t BgL_oz00_2723)
	{
		{	/* Foreign/ctype.sch 336 */
			return
				BGl_cenumzd2pointedzd2tozd2byzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2723));
		}

	}



/* cenum-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_105, obj_t BgL_vz00_106)
	{
		{	/* Foreign/ctype.sch 337 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_105)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_106), BUNSPEC);
		}

	}



/* &cenum-pointed-to-by-set! */
	obj_t BGl_z62cenumzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2724, obj_t BgL_oz00_2725, obj_t BgL_vz00_2726)
	{
		{	/* Foreign/ctype.sch 337 */
			return
				BGl_cenumzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2725), BgL_vz00_2726);
		}

	}



/* cenum-alias */
	BGL_EXPORTED_DEF obj_t BGl_cenumzd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_107)
	{
		{	/* Foreign/ctype.sch 338 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_107)))->BgL_aliasz00);
		}

	}



/* &cenum-alias */
	obj_t BGl_z62cenumzd2aliaszb0zzforeign_ctypez00(obj_t BgL_envz00_2727,
		obj_t BgL_oz00_2728)
	{
		{	/* Foreign/ctype.sch 338 */
			return
				BGl_cenumzd2aliaszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2728));
		}

	}



/* cenum-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_108, obj_t BgL_vz00_109)
	{
		{	/* Foreign/ctype.sch 339 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_108)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_109), BUNSPEC);
		}

	}



/* &cenum-alias-set! */
	obj_t BGl_z62cenumzd2aliaszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2729, obj_t BgL_oz00_2730, obj_t BgL_vz00_2731)
	{
		{	/* Foreign/ctype.sch 339 */
			return
				BGl_cenumzd2aliaszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2730), BgL_vz00_2731);
		}

	}



/* cenum-$ */
	BGL_EXPORTED_DEF obj_t BGl_cenumzd2z42z90zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_110)
	{
		{	/* Foreign/ctype.sch 340 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_110)))->BgL_z42z42);
		}

	}



/* &cenum-$ */
	obj_t BGl_z62cenumzd2z42zf2zzforeign_ctypez00(obj_t BgL_envz00_2732,
		obj_t BgL_oz00_2733)
	{
		{	/* Foreign/ctype.sch 340 */
			return
				BGl_cenumzd2z42z90zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2733));
		}

	}



/* cenum-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_111,
		obj_t BgL_vz00_112)
	{
		{	/* Foreign/ctype.sch 341 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_111)))->BgL_z42z42) =
				((obj_t) BgL_vz00_112), BUNSPEC);
		}

	}



/* &cenum-$-set! */
	obj_t BGl_z62cenumzd2z42zd2setz12z32zzforeign_ctypez00(obj_t BgL_envz00_2734,
		obj_t BgL_oz00_2735, obj_t BgL_vz00_2736)
	{
		{	/* Foreign/ctype.sch 341 */
			return
				BGl_cenumzd2z42zd2setz12z50zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2735), BgL_vz00_2736);
		}

	}



/* cenum-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_cenumzd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_113)
	{
		{	/* Foreign/ctype.sch 342 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_113)))->BgL_magiczf3zf3);
		}

	}



/* &cenum-magic? */
	obj_t BGl_z62cenumzd2magiczf3z43zzforeign_ctypez00(obj_t BgL_envz00_2737,
		obj_t BgL_oz00_2738)
	{
		{	/* Foreign/ctype.sch 342 */
			return
				BBOOL(BGl_cenumzd2magiczf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2738)));
		}

	}



/* cenum-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_114, bool_t BgL_vz00_115)
	{
		{	/* Foreign/ctype.sch 343 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_114)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_115), BUNSPEC);
		}

	}



/* &cenum-magic?-set! */
	obj_t BGl_z62cenumzd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2739, obj_t BgL_oz00_2740, obj_t BgL_vz00_2741)
	{
		{	/* Foreign/ctype.sch 343 */
			return
				BGl_cenumzd2magiczf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2740), CBOOL(BgL_vz00_2741));
		}

	}



/* cenum-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_cenumzd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_116)
	{
		{	/* Foreign/ctype.sch 344 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_116)))->BgL_initzf3zf3);
		}

	}



/* &cenum-init? */
	obj_t BGl_z62cenumzd2initzf3z43zzforeign_ctypez00(obj_t BgL_envz00_2742,
		obj_t BgL_oz00_2743)
	{
		{	/* Foreign/ctype.sch 344 */
			return
				BBOOL(BGl_cenumzd2initzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2743)));
		}

	}



/* cenum-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_117, bool_t BgL_vz00_118)
	{
		{	/* Foreign/ctype.sch 345 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_117)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_118), BUNSPEC);
		}

	}



/* &cenum-init?-set! */
	obj_t BGl_z62cenumzd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2744, obj_t BgL_oz00_2745, obj_t BgL_vz00_2746)
	{
		{	/* Foreign/ctype.sch 345 */
			return
				BGl_cenumzd2initzf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2745), CBOOL(BgL_vz00_2746));
		}

	}



/* cenum-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_119)
	{
		{	/* Foreign/ctype.sch 346 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_119)))->BgL_parentsz00);
		}

	}



/* &cenum-parents */
	obj_t BGl_z62cenumzd2parentszb0zzforeign_ctypez00(obj_t BgL_envz00_2747,
		obj_t BgL_oz00_2748)
	{
		{	/* Foreign/ctype.sch 346 */
			return
				BGl_cenumzd2parentszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2748));
		}

	}



/* cenum-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_120, obj_t BgL_vz00_121)
	{
		{	/* Foreign/ctype.sch 347 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_120)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_121), BUNSPEC);
		}

	}



/* &cenum-parents-set! */
	obj_t BGl_z62cenumzd2parentszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2749, obj_t BgL_oz00_2750, obj_t BgL_vz00_2751)
	{
		{	/* Foreign/ctype.sch 347 */
			return
				BGl_cenumzd2parentszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2750), BgL_vz00_2751);
		}

	}



/* cenum-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_122)
	{
		{	/* Foreign/ctype.sch 348 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_122)))->BgL_coercezd2tozd2);
		}

	}



/* &cenum-coerce-to */
	obj_t BGl_z62cenumzd2coercezd2toz62zzforeign_ctypez00(obj_t BgL_envz00_2752,
		obj_t BgL_oz00_2753)
	{
		{	/* Foreign/ctype.sch 348 */
			return
				BGl_cenumzd2coercezd2toz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2753));
		}

	}



/* cenum-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_123, obj_t BgL_vz00_124)
	{
		{	/* Foreign/ctype.sch 349 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_123)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_124), BUNSPEC);
		}

	}



/* &cenum-coerce-to-set! */
	obj_t BGl_z62cenumzd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_2754, obj_t BgL_oz00_2755, obj_t BgL_vz00_2756)
	{
		{	/* Foreign/ctype.sch 349 */
			return
				BGl_cenumzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2755), BgL_vz00_2756);
		}

	}



/* cenum-class */
	BGL_EXPORTED_DEF obj_t BGl_cenumzd2classzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_125)
	{
		{	/* Foreign/ctype.sch 350 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_125)))->BgL_classz00);
		}

	}



/* &cenum-class */
	obj_t BGl_z62cenumzd2classzb0zzforeign_ctypez00(obj_t BgL_envz00_2757,
		obj_t BgL_oz00_2758)
	{
		{	/* Foreign/ctype.sch 350 */
			return
				BGl_cenumzd2classzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2758));
		}

	}



/* cenum-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_126, obj_t BgL_vz00_127)
	{
		{	/* Foreign/ctype.sch 351 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_126)))->BgL_classz00) =
				((obj_t) BgL_vz00_127), BUNSPEC);
		}

	}



/* &cenum-class-set! */
	obj_t BGl_z62cenumzd2classzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2759, obj_t BgL_oz00_2760, obj_t BgL_vz00_2761)
	{
		{	/* Foreign/ctype.sch 351 */
			return
				BGl_cenumzd2classzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2760), BgL_vz00_2761);
		}

	}



/* cenum-size */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_128)
	{
		{	/* Foreign/ctype.sch 352 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_128)))->BgL_siza7eza7);
		}

	}



/* &cenum-size */
	obj_t BGl_z62cenumzd2siza7ez17zzforeign_ctypez00(obj_t BgL_envz00_2762,
		obj_t BgL_oz00_2763)
	{
		{	/* Foreign/ctype.sch 352 */
			return
				BGl_cenumzd2siza7ez75zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2763));
		}

	}



/* cenum-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_129, obj_t BgL_vz00_130)
	{
		{	/* Foreign/ctype.sch 353 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_129)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_130), BUNSPEC);
		}

	}



/* &cenum-size-set! */
	obj_t BGl_z62cenumzd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t
		BgL_envz00_2764, obj_t BgL_oz00_2765, obj_t BgL_vz00_2766)
	{
		{	/* Foreign/ctype.sch 353 */
			return
				BGl_cenumzd2siza7ezd2setz12zb5zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2765), BgL_vz00_2766);
		}

	}



/* cenum-name */
	BGL_EXPORTED_DEF obj_t BGl_cenumzd2namezd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_131)
	{
		{	/* Foreign/ctype.sch 354 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_131)))->BgL_namez00);
		}

	}



/* &cenum-name */
	obj_t BGl_z62cenumzd2namezb0zzforeign_ctypez00(obj_t BgL_envz00_2767,
		obj_t BgL_oz00_2768)
	{
		{	/* Foreign/ctype.sch 354 */
			return
				BGl_cenumzd2namezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2768));
		}

	}



/* cenum-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cenumzd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_132, obj_t BgL_vz00_133)
	{
		{	/* Foreign/ctype.sch 355 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_132)))->BgL_namez00) =
				((obj_t) BgL_vz00_133), BUNSPEC);
		}

	}



/* &cenum-name-set! */
	obj_t BGl_z62cenumzd2namezd2setz12z70zzforeign_ctypez00(obj_t BgL_envz00_2769,
		obj_t BgL_oz00_2770, obj_t BgL_vz00_2771)
	{
		{	/* Foreign/ctype.sch 355 */
			return
				BGl_cenumzd2namezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2770), BgL_vz00_2771);
		}

	}



/* cenum-id */
	BGL_EXPORTED_DEF obj_t BGl_cenumzd2idzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_134)
	{
		{	/* Foreign/ctype.sch 356 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_134)))->BgL_idz00);
		}

	}



/* &cenum-id */
	obj_t BGl_z62cenumzd2idzb0zzforeign_ctypez00(obj_t BgL_envz00_2772,
		obj_t BgL_oz00_2773)
	{
		{	/* Foreign/ctype.sch 356 */
			return
				BGl_cenumzd2idzd2zzforeign_ctypez00(((BgL_typez00_bglt) BgL_oz00_2773));
		}

	}



/* make-copaque */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2copaquezd2zzforeign_ctypez00(obj_t BgL_id1288z00_137,
		obj_t BgL_name1289z00_138, obj_t BgL_siza7e1290za7_139,
		obj_t BgL_class1291z00_140, obj_t BgL_coercezd2to1292zd2_141,
		obj_t BgL_parents1293z00_142, bool_t BgL_initzf31294zf3_143,
		bool_t BgL_magiczf31295zf3_144, obj_t BgL_z421296z42_145,
		obj_t BgL_alias1297z00_146, obj_t BgL_pointedzd2tozd2by1298z00_147,
		obj_t BgL_tvector1299z00_148, obj_t BgL_location1300z00_149,
		obj_t BgL_importzd2location1301zd2_150, int BgL_occurrence1302z00_151,
		BgL_typez00_bglt BgL_btype1303z00_152)
	{
		{	/* Foreign/ctype.sch 360 */
			{	/* Foreign/ctype.sch 360 */
				BgL_typez00_bglt BgL_new1236z00_3660;

				{	/* Foreign/ctype.sch 360 */
					BgL_typez00_bglt BgL_tmp1234z00_3661;
					BgL_copaquez00_bglt BgL_wide1235z00_3662;

					{
						BgL_typez00_bglt BgL_auxz00_4503;

						{	/* Foreign/ctype.sch 360 */
							BgL_typez00_bglt BgL_new1233z00_3663;

							BgL_new1233z00_3663 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Foreign/ctype.sch 360 */
								long BgL_arg1377z00_3664;

								BgL_arg1377z00_3664 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1233z00_3663),
									BgL_arg1377z00_3664);
							}
							{	/* Foreign/ctype.sch 360 */
								BgL_objectz00_bglt BgL_tmpz00_4508;

								BgL_tmpz00_4508 = ((BgL_objectz00_bglt) BgL_new1233z00_3663);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4508, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1233z00_3663);
							BgL_auxz00_4503 = BgL_new1233z00_3663;
						}
						BgL_tmp1234z00_3661 = ((BgL_typez00_bglt) BgL_auxz00_4503);
					}
					BgL_wide1235z00_3662 =
						((BgL_copaquez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_copaquez00_bgl))));
					{	/* Foreign/ctype.sch 360 */
						obj_t BgL_auxz00_4516;
						BgL_objectz00_bglt BgL_tmpz00_4514;

						BgL_auxz00_4516 = ((obj_t) BgL_wide1235z00_3662);
						BgL_tmpz00_4514 = ((BgL_objectz00_bglt) BgL_tmp1234z00_3661);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4514, BgL_auxz00_4516);
					}
					((BgL_objectz00_bglt) BgL_tmp1234z00_3661);
					{	/* Foreign/ctype.sch 360 */
						long BgL_arg1376z00_3665;

						BgL_arg1376z00_3665 =
							BGL_CLASS_NUM(BGl_copaquez00zzforeign_ctypez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1234z00_3661), BgL_arg1376z00_3665);
					}
					BgL_new1236z00_3660 = ((BgL_typez00_bglt) BgL_tmp1234z00_3661);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1236z00_3660)))->BgL_idz00) =
					((obj_t) BgL_id1288z00_137), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_namez00) =
					((obj_t) BgL_name1289z00_138), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1290za7_139), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_classz00) =
					((obj_t) BgL_class1291z00_140), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1292zd2_141), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_parentsz00) =
					((obj_t) BgL_parents1293z00_142), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31294zf3_143), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31295zf3_144), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_z42z42) =
					((obj_t) BgL_z421296z42_145), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_aliasz00) =
					((obj_t) BgL_alias1297z00_146), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1298z00_147), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1299z00_148), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_locationz00) =
					((obj_t) BgL_location1300z00_149), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1301zd2_150), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1236z00_3660)))->BgL_occurrencez00) =
					((int) BgL_occurrence1302z00_151), BUNSPEC);
				{
					BgL_copaquez00_bglt BgL_auxz00_4556;

					{
						obj_t BgL_auxz00_4557;

						{	/* Foreign/ctype.sch 360 */
							BgL_objectz00_bglt BgL_tmpz00_4558;

							BgL_tmpz00_4558 = ((BgL_objectz00_bglt) BgL_new1236z00_3660);
							BgL_auxz00_4557 = BGL_OBJECT_WIDENING(BgL_tmpz00_4558);
						}
						BgL_auxz00_4556 = ((BgL_copaquez00_bglt) BgL_auxz00_4557);
					}
					((((BgL_copaquez00_bglt) COBJECT(BgL_auxz00_4556))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_btype1303z00_152), BUNSPEC);
				}
				return BgL_new1236z00_3660;
			}
		}

	}



/* &make-copaque */
	BgL_typez00_bglt BGl_z62makezd2copaquezb0zzforeign_ctypez00(obj_t
		BgL_envz00_2774, obj_t BgL_id1288z00_2775, obj_t BgL_name1289z00_2776,
		obj_t BgL_siza7e1290za7_2777, obj_t BgL_class1291z00_2778,
		obj_t BgL_coercezd2to1292zd2_2779, obj_t BgL_parents1293z00_2780,
		obj_t BgL_initzf31294zf3_2781, obj_t BgL_magiczf31295zf3_2782,
		obj_t BgL_z421296z42_2783, obj_t BgL_alias1297z00_2784,
		obj_t BgL_pointedzd2tozd2by1298z00_2785, obj_t BgL_tvector1299z00_2786,
		obj_t BgL_location1300z00_2787, obj_t BgL_importzd2location1301zd2_2788,
		obj_t BgL_occurrence1302z00_2789, obj_t BgL_btype1303z00_2790)
	{
		{	/* Foreign/ctype.sch 360 */
			return
				BGl_makezd2copaquezd2zzforeign_ctypez00(BgL_id1288z00_2775,
				BgL_name1289z00_2776, BgL_siza7e1290za7_2777, BgL_class1291z00_2778,
				BgL_coercezd2to1292zd2_2779, BgL_parents1293z00_2780,
				CBOOL(BgL_initzf31294zf3_2781), CBOOL(BgL_magiczf31295zf3_2782),
				BgL_z421296z42_2783, BgL_alias1297z00_2784,
				BgL_pointedzd2tozd2by1298z00_2785, BgL_tvector1299z00_2786,
				BgL_location1300z00_2787, BgL_importzd2location1301zd2_2788,
				CINT(BgL_occurrence1302z00_2789),
				((BgL_typez00_bglt) BgL_btype1303z00_2790));
		}

	}



/* copaque? */
	BGL_EXPORTED_DEF bool_t BGl_copaquezf3zf3zzforeign_ctypez00(obj_t
		BgL_objz00_153)
	{
		{	/* Foreign/ctype.sch 361 */
			{	/* Foreign/ctype.sch 361 */
				obj_t BgL_classz00_3666;

				BgL_classz00_3666 = BGl_copaquez00zzforeign_ctypez00;
				if (BGL_OBJECTP(BgL_objz00_153))
					{	/* Foreign/ctype.sch 361 */
						BgL_objectz00_bglt BgL_arg1807z00_3667;

						BgL_arg1807z00_3667 = (BgL_objectz00_bglt) (BgL_objz00_153);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Foreign/ctype.sch 361 */
								long BgL_idxz00_3668;

								BgL_idxz00_3668 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3667);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3668 + 2L)) == BgL_classz00_3666);
							}
						else
							{	/* Foreign/ctype.sch 361 */
								bool_t BgL_res2101z00_3671;

								{	/* Foreign/ctype.sch 361 */
									obj_t BgL_oclassz00_3672;

									{	/* Foreign/ctype.sch 361 */
										obj_t BgL_arg1815z00_3673;
										long BgL_arg1816z00_3674;

										BgL_arg1815z00_3673 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Foreign/ctype.sch 361 */
											long BgL_arg1817z00_3675;

											BgL_arg1817z00_3675 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3667);
											BgL_arg1816z00_3674 = (BgL_arg1817z00_3675 - OBJECT_TYPE);
										}
										BgL_oclassz00_3672 =
											VECTOR_REF(BgL_arg1815z00_3673, BgL_arg1816z00_3674);
									}
									{	/* Foreign/ctype.sch 361 */
										bool_t BgL__ortest_1115z00_3676;

										BgL__ortest_1115z00_3676 =
											(BgL_classz00_3666 == BgL_oclassz00_3672);
										if (BgL__ortest_1115z00_3676)
											{	/* Foreign/ctype.sch 361 */
												BgL_res2101z00_3671 = BgL__ortest_1115z00_3676;
											}
										else
											{	/* Foreign/ctype.sch 361 */
												long BgL_odepthz00_3677;

												{	/* Foreign/ctype.sch 361 */
													obj_t BgL_arg1804z00_3678;

													BgL_arg1804z00_3678 = (BgL_oclassz00_3672);
													BgL_odepthz00_3677 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3678);
												}
												if ((2L < BgL_odepthz00_3677))
													{	/* Foreign/ctype.sch 361 */
														obj_t BgL_arg1802z00_3679;

														{	/* Foreign/ctype.sch 361 */
															obj_t BgL_arg1803z00_3680;

															BgL_arg1803z00_3680 = (BgL_oclassz00_3672);
															BgL_arg1802z00_3679 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3680,
																2L);
														}
														BgL_res2101z00_3671 =
															(BgL_arg1802z00_3679 == BgL_classz00_3666);
													}
												else
													{	/* Foreign/ctype.sch 361 */
														BgL_res2101z00_3671 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2101z00_3671;
							}
					}
				else
					{	/* Foreign/ctype.sch 361 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &copaque? */
	obj_t BGl_z62copaquezf3z91zzforeign_ctypez00(obj_t BgL_envz00_2791,
		obj_t BgL_objz00_2792)
	{
		{	/* Foreign/ctype.sch 361 */
			return BBOOL(BGl_copaquezf3zf3zzforeign_ctypez00(BgL_objz00_2792));
		}

	}



/* copaque-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_copaquezd2nilzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.sch 362 */
			{	/* Foreign/ctype.sch 362 */
				obj_t BgL_classz00_1596;

				BgL_classz00_1596 = BGl_copaquez00zzforeign_ctypez00;
				{	/* Foreign/ctype.sch 362 */
					obj_t BgL__ortest_1117z00_1597;

					BgL__ortest_1117z00_1597 = BGL_CLASS_NIL(BgL_classz00_1596);
					if (CBOOL(BgL__ortest_1117z00_1597))
						{	/* Foreign/ctype.sch 362 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1597);
						}
					else
						{	/* Foreign/ctype.sch 362 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1596));
						}
				}
			}
		}

	}



/* &copaque-nil */
	BgL_typez00_bglt BGl_z62copaquezd2nilzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2793)
	{
		{	/* Foreign/ctype.sch 362 */
			return BGl_copaquezd2nilzd2zzforeign_ctypez00();
		}

	}



/* copaque-btype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_copaquezd2btypezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_154)
	{
		{	/* Foreign/ctype.sch 363 */
			{
				BgL_copaquez00_bglt BgL_auxz00_4599;

				{
					obj_t BgL_auxz00_4600;

					{	/* Foreign/ctype.sch 363 */
						BgL_objectz00_bglt BgL_tmpz00_4601;

						BgL_tmpz00_4601 = ((BgL_objectz00_bglt) BgL_oz00_154);
						BgL_auxz00_4600 = BGL_OBJECT_WIDENING(BgL_tmpz00_4601);
					}
					BgL_auxz00_4599 = ((BgL_copaquez00_bglt) BgL_auxz00_4600);
				}
				return (((BgL_copaquez00_bglt) COBJECT(BgL_auxz00_4599))->BgL_btypez00);
			}
		}

	}



/* &copaque-btype */
	BgL_typez00_bglt BGl_z62copaquezd2btypezb0zzforeign_ctypez00(obj_t
		BgL_envz00_2794, obj_t BgL_oz00_2795)
	{
		{	/* Foreign/ctype.sch 363 */
			return
				BGl_copaquezd2btypezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2795));
		}

	}



/* copaque-occurrence */
	BGL_EXPORTED_DEF int
		BGl_copaquezd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_157)
	{
		{	/* Foreign/ctype.sch 365 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_157)))->BgL_occurrencez00);
		}

	}



/* &copaque-occurrence */
	obj_t BGl_z62copaquezd2occurrencezb0zzforeign_ctypez00(obj_t BgL_envz00_2796,
		obj_t BgL_oz00_2797)
	{
		{	/* Foreign/ctype.sch 365 */
			return
				BINT(BGl_copaquezd2occurrencezd2zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2797)));
		}

	}



/* copaque-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_158, int BgL_vz00_159)
	{
		{	/* Foreign/ctype.sch 366 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_158)))->BgL_occurrencez00) =
				((int) BgL_vz00_159), BUNSPEC);
		}

	}



/* &copaque-occurrence-set! */
	obj_t BGl_z62copaquezd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2798, obj_t BgL_oz00_2799, obj_t BgL_vz00_2800)
	{
		{	/* Foreign/ctype.sch 366 */
			return
				BGl_copaquezd2occurrencezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2799), CINT(BgL_vz00_2800));
		}

	}



/* copaque-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_160)
	{
		{	/* Foreign/ctype.sch 367 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_160)))->BgL_importzd2locationzd2);
		}

	}



/* &copaque-import-location */
	obj_t BGl_z62copaquezd2importzd2locationz62zzforeign_ctypez00(obj_t
		BgL_envz00_2801, obj_t BgL_oz00_2802)
	{
		{	/* Foreign/ctype.sch 367 */
			return
				BGl_copaquezd2importzd2locationz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2802));
		}

	}



/* copaque-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_161, obj_t BgL_vz00_162)
	{
		{	/* Foreign/ctype.sch 368 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_161)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_162), BUNSPEC);
		}

	}



/* &copaque-import-location-set! */
	obj_t BGl_z62copaquezd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_2803, obj_t BgL_oz00_2804, obj_t BgL_vz00_2805)
	{
		{	/* Foreign/ctype.sch 368 */
			return
				BGl_copaquezd2importzd2locationzd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2804), BgL_vz00_2805);
		}

	}



/* copaque-location */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_163)
	{
		{	/* Foreign/ctype.sch 369 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_163)))->BgL_locationz00);
		}

	}



/* &copaque-location */
	obj_t BGl_z62copaquezd2locationzb0zzforeign_ctypez00(obj_t BgL_envz00_2806,
		obj_t BgL_oz00_2807)
	{
		{	/* Foreign/ctype.sch 369 */
			return
				BGl_copaquezd2locationzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2807));
		}

	}



/* copaque-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_164, obj_t BgL_vz00_165)
	{
		{	/* Foreign/ctype.sch 370 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_164)))->BgL_locationz00) =
				((obj_t) BgL_vz00_165), BUNSPEC);
		}

	}



/* &copaque-location-set! */
	obj_t BGl_z62copaquezd2locationzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2808, obj_t BgL_oz00_2809, obj_t BgL_vz00_2810)
	{
		{	/* Foreign/ctype.sch 370 */
			return
				BGl_copaquezd2locationzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2809), BgL_vz00_2810);
		}

	}



/* copaque-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_166)
	{
		{	/* Foreign/ctype.sch 371 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_166)))->BgL_tvectorz00);
		}

	}



/* &copaque-tvector */
	obj_t BGl_z62copaquezd2tvectorzb0zzforeign_ctypez00(obj_t BgL_envz00_2811,
		obj_t BgL_oz00_2812)
	{
		{	/* Foreign/ctype.sch 371 */
			return
				BGl_copaquezd2tvectorzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2812));
		}

	}



/* copaque-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_167, obj_t BgL_vz00_168)
	{
		{	/* Foreign/ctype.sch 372 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_167)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_168), BUNSPEC);
		}

	}



/* &copaque-tvector-set! */
	obj_t BGl_z62copaquezd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2813, obj_t BgL_oz00_2814, obj_t BgL_vz00_2815)
	{
		{	/* Foreign/ctype.sch 372 */
			return
				BGl_copaquezd2tvectorzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2814), BgL_vz00_2815);
		}

	}



/* copaque-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_169)
	{
		{	/* Foreign/ctype.sch 373 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_169)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &copaque-pointed-to-by */
	obj_t BGl_z62copaquezd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2816, obj_t BgL_oz00_2817)
	{
		{	/* Foreign/ctype.sch 373 */
			return
				BGl_copaquezd2pointedzd2tozd2byzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2817));
		}

	}



/* copaque-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_170, obj_t BgL_vz00_171)
	{
		{	/* Foreign/ctype.sch 374 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_170)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_171), BUNSPEC);
		}

	}



/* &copaque-pointed-to-by-set! */
	obj_t BGl_z62copaquezd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2818, obj_t BgL_oz00_2819, obj_t BgL_vz00_2820)
	{
		{	/* Foreign/ctype.sch 374 */
			return
				BGl_copaquezd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2819), BgL_vz00_2820);
		}

	}



/* copaque-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_172)
	{
		{	/* Foreign/ctype.sch 375 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_172)))->BgL_aliasz00);
		}

	}



/* &copaque-alias */
	obj_t BGl_z62copaquezd2aliaszb0zzforeign_ctypez00(obj_t BgL_envz00_2821,
		obj_t BgL_oz00_2822)
	{
		{	/* Foreign/ctype.sch 375 */
			return
				BGl_copaquezd2aliaszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2822));
		}

	}



/* copaque-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_173, obj_t BgL_vz00_174)
	{
		{	/* Foreign/ctype.sch 376 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_173)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_174), BUNSPEC);
		}

	}



/* &copaque-alias-set! */
	obj_t BGl_z62copaquezd2aliaszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2823, obj_t BgL_oz00_2824, obj_t BgL_vz00_2825)
	{
		{	/* Foreign/ctype.sch 376 */
			return
				BGl_copaquezd2aliaszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2824), BgL_vz00_2825);
		}

	}



/* copaque-$ */
	BGL_EXPORTED_DEF obj_t BGl_copaquezd2z42z90zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_175)
	{
		{	/* Foreign/ctype.sch 377 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_175)))->BgL_z42z42);
		}

	}



/* &copaque-$ */
	obj_t BGl_z62copaquezd2z42zf2zzforeign_ctypez00(obj_t BgL_envz00_2826,
		obj_t BgL_oz00_2827)
	{
		{	/* Foreign/ctype.sch 377 */
			return
				BGl_copaquezd2z42z90zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2827));
		}

	}



/* copaque-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_176, obj_t BgL_vz00_177)
	{
		{	/* Foreign/ctype.sch 378 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_176)))->BgL_z42z42) =
				((obj_t) BgL_vz00_177), BUNSPEC);
		}

	}



/* &copaque-$-set! */
	obj_t BGl_z62copaquezd2z42zd2setz12z32zzforeign_ctypez00(obj_t
		BgL_envz00_2828, obj_t BgL_oz00_2829, obj_t BgL_vz00_2830)
	{
		{	/* Foreign/ctype.sch 378 */
			return
				BGl_copaquezd2z42zd2setz12z50zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2829), BgL_vz00_2830);
		}

	}



/* copaque-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_copaquezd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_178)
	{
		{	/* Foreign/ctype.sch 379 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_178)))->BgL_magiczf3zf3);
		}

	}



/* &copaque-magic? */
	obj_t BGl_z62copaquezd2magiczf3z43zzforeign_ctypez00(obj_t BgL_envz00_2831,
		obj_t BgL_oz00_2832)
	{
		{	/* Foreign/ctype.sch 379 */
			return
				BBOOL(BGl_copaquezd2magiczf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2832)));
		}

	}



/* copaque-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_179, bool_t BgL_vz00_180)
	{
		{	/* Foreign/ctype.sch 380 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_179)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_180), BUNSPEC);
		}

	}



/* &copaque-magic?-set! */
	obj_t BGl_z62copaquezd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2833, obj_t BgL_oz00_2834, obj_t BgL_vz00_2835)
	{
		{	/* Foreign/ctype.sch 380 */
			return
				BGl_copaquezd2magiczf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2834), CBOOL(BgL_vz00_2835));
		}

	}



/* copaque-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_copaquezd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_181)
	{
		{	/* Foreign/ctype.sch 381 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_181)))->BgL_initzf3zf3);
		}

	}



/* &copaque-init? */
	obj_t BGl_z62copaquezd2initzf3z43zzforeign_ctypez00(obj_t BgL_envz00_2836,
		obj_t BgL_oz00_2837)
	{
		{	/* Foreign/ctype.sch 381 */
			return
				BBOOL(BGl_copaquezd2initzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2837)));
		}

	}



/* copaque-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_182, bool_t BgL_vz00_183)
	{
		{	/* Foreign/ctype.sch 382 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_182)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_183), BUNSPEC);
		}

	}



/* &copaque-init?-set! */
	obj_t BGl_z62copaquezd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2838, obj_t BgL_oz00_2839, obj_t BgL_vz00_2840)
	{
		{	/* Foreign/ctype.sch 382 */
			return
				BGl_copaquezd2initzf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2839), CBOOL(BgL_vz00_2840));
		}

	}



/* copaque-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_184)
	{
		{	/* Foreign/ctype.sch 383 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_184)))->BgL_parentsz00);
		}

	}



/* &copaque-parents */
	obj_t BGl_z62copaquezd2parentszb0zzforeign_ctypez00(obj_t BgL_envz00_2841,
		obj_t BgL_oz00_2842)
	{
		{	/* Foreign/ctype.sch 383 */
			return
				BGl_copaquezd2parentszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2842));
		}

	}



/* copaque-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_185, obj_t BgL_vz00_186)
	{
		{	/* Foreign/ctype.sch 384 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_185)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_186), BUNSPEC);
		}

	}



/* &copaque-parents-set! */
	obj_t BGl_z62copaquezd2parentszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2843, obj_t BgL_oz00_2844, obj_t BgL_vz00_2845)
	{
		{	/* Foreign/ctype.sch 384 */
			return
				BGl_copaquezd2parentszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2844), BgL_vz00_2845);
		}

	}



/* copaque-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_187)
	{
		{	/* Foreign/ctype.sch 385 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_187)))->BgL_coercezd2tozd2);
		}

	}



/* &copaque-coerce-to */
	obj_t BGl_z62copaquezd2coercezd2toz62zzforeign_ctypez00(obj_t BgL_envz00_2846,
		obj_t BgL_oz00_2847)
	{
		{	/* Foreign/ctype.sch 385 */
			return
				BGl_copaquezd2coercezd2toz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2847));
		}

	}



/* copaque-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_188, obj_t BgL_vz00_189)
	{
		{	/* Foreign/ctype.sch 386 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_188)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_189), BUNSPEC);
		}

	}



/* &copaque-coerce-to-set! */
	obj_t BGl_z62copaquezd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_2848, obj_t BgL_oz00_2849, obj_t BgL_vz00_2850)
	{
		{	/* Foreign/ctype.sch 386 */
			return
				BGl_copaquezd2coercezd2tozd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2849), BgL_vz00_2850);
		}

	}



/* copaque-class */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2classzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_190)
	{
		{	/* Foreign/ctype.sch 387 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_190)))->BgL_classz00);
		}

	}



/* &copaque-class */
	obj_t BGl_z62copaquezd2classzb0zzforeign_ctypez00(obj_t BgL_envz00_2851,
		obj_t BgL_oz00_2852)
	{
		{	/* Foreign/ctype.sch 387 */
			return
				BGl_copaquezd2classzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2852));
		}

	}



/* copaque-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_191, obj_t BgL_vz00_192)
	{
		{	/* Foreign/ctype.sch 388 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_191)))->BgL_classz00) =
				((obj_t) BgL_vz00_192), BUNSPEC);
		}

	}



/* &copaque-class-set! */
	obj_t BGl_z62copaquezd2classzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2853, obj_t BgL_oz00_2854, obj_t BgL_vz00_2855)
	{
		{	/* Foreign/ctype.sch 388 */
			return
				BGl_copaquezd2classzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2854), BgL_vz00_2855);
		}

	}



/* copaque-size */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_193)
	{
		{	/* Foreign/ctype.sch 389 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_193)))->BgL_siza7eza7);
		}

	}



/* &copaque-size */
	obj_t BGl_z62copaquezd2siza7ez17zzforeign_ctypez00(obj_t BgL_envz00_2856,
		obj_t BgL_oz00_2857)
	{
		{	/* Foreign/ctype.sch 389 */
			return
				BGl_copaquezd2siza7ez75zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2857));
		}

	}



/* copaque-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_194, obj_t BgL_vz00_195)
	{
		{	/* Foreign/ctype.sch 390 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_194)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_195), BUNSPEC);
		}

	}



/* &copaque-size-set! */
	obj_t BGl_z62copaquezd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t
		BgL_envz00_2858, obj_t BgL_oz00_2859, obj_t BgL_vz00_2860)
	{
		{	/* Foreign/ctype.sch 390 */
			return
				BGl_copaquezd2siza7ezd2setz12zb5zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2859), BgL_vz00_2860);
		}

	}



/* copaque-name */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2namezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_196)
	{
		{	/* Foreign/ctype.sch 391 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_196)))->BgL_namez00);
		}

	}



/* &copaque-name */
	obj_t BGl_z62copaquezd2namezb0zzforeign_ctypez00(obj_t BgL_envz00_2861,
		obj_t BgL_oz00_2862)
	{
		{	/* Foreign/ctype.sch 391 */
			return
				BGl_copaquezd2namezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2862));
		}

	}



/* copaque-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_copaquezd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_197, obj_t BgL_vz00_198)
	{
		{	/* Foreign/ctype.sch 392 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_197)))->BgL_namez00) =
				((obj_t) BgL_vz00_198), BUNSPEC);
		}

	}



/* &copaque-name-set! */
	obj_t BGl_z62copaquezd2namezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2863, obj_t BgL_oz00_2864, obj_t BgL_vz00_2865)
	{
		{	/* Foreign/ctype.sch 392 */
			return
				BGl_copaquezd2namezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2864), BgL_vz00_2865);
		}

	}



/* copaque-id */
	BGL_EXPORTED_DEF obj_t BGl_copaquezd2idzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_199)
	{
		{	/* Foreign/ctype.sch 393 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_199)))->BgL_idz00);
		}

	}



/* &copaque-id */
	obj_t BGl_z62copaquezd2idzb0zzforeign_ctypez00(obj_t BgL_envz00_2866,
		obj_t BgL_oz00_2867)
	{
		{	/* Foreign/ctype.sch 393 */
			return
				BGl_copaquezd2idzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2867));
		}

	}



/* make-cfunction */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2cfunctionzd2zzforeign_ctypez00(obj_t BgL_id1268z00_202,
		obj_t BgL_name1269z00_203, obj_t BgL_siza7e1270za7_204,
		obj_t BgL_class1271z00_205, obj_t BgL_coercezd2to1272zd2_206,
		obj_t BgL_parents1273z00_207, bool_t BgL_initzf31274zf3_208,
		bool_t BgL_magiczf31275zf3_209, obj_t BgL_z421276z42_210,
		obj_t BgL_alias1277z00_211, obj_t BgL_pointedzd2tozd2by1278z00_212,
		obj_t BgL_tvector1279z00_213, obj_t BgL_location1280z00_214,
		obj_t BgL_importzd2location1281zd2_215, int BgL_occurrence1282z00_216,
		BgL_typez00_bglt BgL_btype1283z00_217, long BgL_arity1284z00_218,
		BgL_typez00_bglt BgL_typezd2res1285zd2_219,
		obj_t BgL_typezd2args1286zd2_220)
	{
		{	/* Foreign/ctype.sch 397 */
			{	/* Foreign/ctype.sch 397 */
				BgL_typez00_bglt BgL_new1240z00_3681;

				{	/* Foreign/ctype.sch 397 */
					BgL_typez00_bglt BgL_tmp1238z00_3682;
					BgL_cfunctionz00_bglt BgL_wide1239z00_3683;

					{
						BgL_typez00_bglt BgL_auxz00_4730;

						{	/* Foreign/ctype.sch 397 */
							BgL_typez00_bglt BgL_new1237z00_3684;

							BgL_new1237z00_3684 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Foreign/ctype.sch 397 */
								long BgL_arg1379z00_3685;

								BgL_arg1379z00_3685 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1237z00_3684),
									BgL_arg1379z00_3685);
							}
							{	/* Foreign/ctype.sch 397 */
								BgL_objectz00_bglt BgL_tmpz00_4735;

								BgL_tmpz00_4735 = ((BgL_objectz00_bglt) BgL_new1237z00_3684);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4735, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1237z00_3684);
							BgL_auxz00_4730 = BgL_new1237z00_3684;
						}
						BgL_tmp1238z00_3682 = ((BgL_typez00_bglt) BgL_auxz00_4730);
					}
					BgL_wide1239z00_3683 =
						((BgL_cfunctionz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cfunctionz00_bgl))));
					{	/* Foreign/ctype.sch 397 */
						obj_t BgL_auxz00_4743;
						BgL_objectz00_bglt BgL_tmpz00_4741;

						BgL_auxz00_4743 = ((obj_t) BgL_wide1239z00_3683);
						BgL_tmpz00_4741 = ((BgL_objectz00_bglt) BgL_tmp1238z00_3682);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4741, BgL_auxz00_4743);
					}
					((BgL_objectz00_bglt) BgL_tmp1238z00_3682);
					{	/* Foreign/ctype.sch 397 */
						long BgL_arg1378z00_3686;

						BgL_arg1378z00_3686 =
							BGL_CLASS_NUM(BGl_cfunctionz00zzforeign_ctypez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1238z00_3682), BgL_arg1378z00_3686);
					}
					BgL_new1240z00_3681 = ((BgL_typez00_bglt) BgL_tmp1238z00_3682);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1240z00_3681)))->BgL_idz00) =
					((obj_t) BgL_id1268z00_202), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_namez00) =
					((obj_t) BgL_name1269z00_203), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1270za7_204), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_classz00) =
					((obj_t) BgL_class1271z00_205), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1272zd2_206), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_parentsz00) =
					((obj_t) BgL_parents1273z00_207), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31274zf3_208), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31275zf3_209), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_z42z42) =
					((obj_t) BgL_z421276z42_210), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_aliasz00) =
					((obj_t) BgL_alias1277z00_211), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1278z00_212), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1279z00_213), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_locationz00) =
					((obj_t) BgL_location1280z00_214), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1281zd2_215), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1240z00_3681)))->BgL_occurrencez00) =
					((int) BgL_occurrence1282z00_216), BUNSPEC);
				{
					BgL_cfunctionz00_bglt BgL_auxz00_4783;

					{
						obj_t BgL_auxz00_4784;

						{	/* Foreign/ctype.sch 397 */
							BgL_objectz00_bglt BgL_tmpz00_4785;

							BgL_tmpz00_4785 = ((BgL_objectz00_bglt) BgL_new1240z00_3681);
							BgL_auxz00_4784 = BGL_OBJECT_WIDENING(BgL_tmpz00_4785);
						}
						BgL_auxz00_4783 = ((BgL_cfunctionz00_bglt) BgL_auxz00_4784);
					}
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_4783))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_btype1283z00_217), BUNSPEC);
				}
				{
					BgL_cfunctionz00_bglt BgL_auxz00_4790;

					{
						obj_t BgL_auxz00_4791;

						{	/* Foreign/ctype.sch 397 */
							BgL_objectz00_bglt BgL_tmpz00_4792;

							BgL_tmpz00_4792 = ((BgL_objectz00_bglt) BgL_new1240z00_3681);
							BgL_auxz00_4791 = BGL_OBJECT_WIDENING(BgL_tmpz00_4792);
						}
						BgL_auxz00_4790 = ((BgL_cfunctionz00_bglt) BgL_auxz00_4791);
					}
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_4790))->BgL_arityz00) =
						((long) BgL_arity1284z00_218), BUNSPEC);
				}
				{
					BgL_cfunctionz00_bglt BgL_auxz00_4797;

					{
						obj_t BgL_auxz00_4798;

						{	/* Foreign/ctype.sch 397 */
							BgL_objectz00_bglt BgL_tmpz00_4799;

							BgL_tmpz00_4799 = ((BgL_objectz00_bglt) BgL_new1240z00_3681);
							BgL_auxz00_4798 = BGL_OBJECT_WIDENING(BgL_tmpz00_4799);
						}
						BgL_auxz00_4797 = ((BgL_cfunctionz00_bglt) BgL_auxz00_4798);
					}
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_4797))->
							BgL_typezd2reszd2) =
						((BgL_typez00_bglt) BgL_typezd2res1285zd2_219), BUNSPEC);
				}
				{
					BgL_cfunctionz00_bglt BgL_auxz00_4804;

					{
						obj_t BgL_auxz00_4805;

						{	/* Foreign/ctype.sch 397 */
							BgL_objectz00_bglt BgL_tmpz00_4806;

							BgL_tmpz00_4806 = ((BgL_objectz00_bglt) BgL_new1240z00_3681);
							BgL_auxz00_4805 = BGL_OBJECT_WIDENING(BgL_tmpz00_4806);
						}
						BgL_auxz00_4804 = ((BgL_cfunctionz00_bglt) BgL_auxz00_4805);
					}
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_4804))->
							BgL_typezd2argszd2) =
						((obj_t) BgL_typezd2args1286zd2_220), BUNSPEC);
				}
				return BgL_new1240z00_3681;
			}
		}

	}



/* &make-cfunction */
	BgL_typez00_bglt BGl_z62makezd2cfunctionzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2868, obj_t BgL_id1268z00_2869, obj_t BgL_name1269z00_2870,
		obj_t BgL_siza7e1270za7_2871, obj_t BgL_class1271z00_2872,
		obj_t BgL_coercezd2to1272zd2_2873, obj_t BgL_parents1273z00_2874,
		obj_t BgL_initzf31274zf3_2875, obj_t BgL_magiczf31275zf3_2876,
		obj_t BgL_z421276z42_2877, obj_t BgL_alias1277z00_2878,
		obj_t BgL_pointedzd2tozd2by1278z00_2879, obj_t BgL_tvector1279z00_2880,
		obj_t BgL_location1280z00_2881, obj_t BgL_importzd2location1281zd2_2882,
		obj_t BgL_occurrence1282z00_2883, obj_t BgL_btype1283z00_2884,
		obj_t BgL_arity1284z00_2885, obj_t BgL_typezd2res1285zd2_2886,
		obj_t BgL_typezd2args1286zd2_2887)
	{
		{	/* Foreign/ctype.sch 397 */
			return
				BGl_makezd2cfunctionzd2zzforeign_ctypez00(BgL_id1268z00_2869,
				BgL_name1269z00_2870, BgL_siza7e1270za7_2871, BgL_class1271z00_2872,
				BgL_coercezd2to1272zd2_2873, BgL_parents1273z00_2874,
				CBOOL(BgL_initzf31274zf3_2875), CBOOL(BgL_magiczf31275zf3_2876),
				BgL_z421276z42_2877, BgL_alias1277z00_2878,
				BgL_pointedzd2tozd2by1278z00_2879, BgL_tvector1279z00_2880,
				BgL_location1280z00_2881, BgL_importzd2location1281zd2_2882,
				CINT(BgL_occurrence1282z00_2883),
				((BgL_typez00_bglt) BgL_btype1283z00_2884),
				(long) CINT(BgL_arity1284z00_2885),
				((BgL_typez00_bglt) BgL_typezd2res1285zd2_2886),
				BgL_typezd2args1286zd2_2887);
		}

	}



/* cfunction? */
	BGL_EXPORTED_DEF bool_t BGl_cfunctionzf3zf3zzforeign_ctypez00(obj_t
		BgL_objz00_221)
	{
		{	/* Foreign/ctype.sch 398 */
			{	/* Foreign/ctype.sch 398 */
				obj_t BgL_classz00_3687;

				BgL_classz00_3687 = BGl_cfunctionz00zzforeign_ctypez00;
				if (BGL_OBJECTP(BgL_objz00_221))
					{	/* Foreign/ctype.sch 398 */
						BgL_objectz00_bglt BgL_arg1807z00_3688;

						BgL_arg1807z00_3688 = (BgL_objectz00_bglt) (BgL_objz00_221);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Foreign/ctype.sch 398 */
								long BgL_idxz00_3689;

								BgL_idxz00_3689 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3688);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3689 + 2L)) == BgL_classz00_3687);
							}
						else
							{	/* Foreign/ctype.sch 398 */
								bool_t BgL_res2102z00_3692;

								{	/* Foreign/ctype.sch 398 */
									obj_t BgL_oclassz00_3693;

									{	/* Foreign/ctype.sch 398 */
										obj_t BgL_arg1815z00_3694;
										long BgL_arg1816z00_3695;

										BgL_arg1815z00_3694 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Foreign/ctype.sch 398 */
											long BgL_arg1817z00_3696;

											BgL_arg1817z00_3696 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3688);
											BgL_arg1816z00_3695 = (BgL_arg1817z00_3696 - OBJECT_TYPE);
										}
										BgL_oclassz00_3693 =
											VECTOR_REF(BgL_arg1815z00_3694, BgL_arg1816z00_3695);
									}
									{	/* Foreign/ctype.sch 398 */
										bool_t BgL__ortest_1115z00_3697;

										BgL__ortest_1115z00_3697 =
											(BgL_classz00_3687 == BgL_oclassz00_3693);
										if (BgL__ortest_1115z00_3697)
											{	/* Foreign/ctype.sch 398 */
												BgL_res2102z00_3692 = BgL__ortest_1115z00_3697;
											}
										else
											{	/* Foreign/ctype.sch 398 */
												long BgL_odepthz00_3698;

												{	/* Foreign/ctype.sch 398 */
													obj_t BgL_arg1804z00_3699;

													BgL_arg1804z00_3699 = (BgL_oclassz00_3693);
													BgL_odepthz00_3698 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3699);
												}
												if ((2L < BgL_odepthz00_3698))
													{	/* Foreign/ctype.sch 398 */
														obj_t BgL_arg1802z00_3700;

														{	/* Foreign/ctype.sch 398 */
															obj_t BgL_arg1803z00_3701;

															BgL_arg1803z00_3701 = (BgL_oclassz00_3693);
															BgL_arg1802z00_3700 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3701,
																2L);
														}
														BgL_res2102z00_3692 =
															(BgL_arg1802z00_3700 == BgL_classz00_3687);
													}
												else
													{	/* Foreign/ctype.sch 398 */
														BgL_res2102z00_3692 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2102z00_3692;
							}
					}
				else
					{	/* Foreign/ctype.sch 398 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cfunction? */
	obj_t BGl_z62cfunctionzf3z91zzforeign_ctypez00(obj_t BgL_envz00_2888,
		obj_t BgL_objz00_2889)
	{
		{	/* Foreign/ctype.sch 398 */
			return BBOOL(BGl_cfunctionzf3zf3zzforeign_ctypez00(BgL_objz00_2889));
		}

	}



/* cfunction-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cfunctionzd2nilzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.sch 399 */
			{	/* Foreign/ctype.sch 399 */
				obj_t BgL_classz00_1651;

				BgL_classz00_1651 = BGl_cfunctionz00zzforeign_ctypez00;
				{	/* Foreign/ctype.sch 399 */
					obj_t BgL__ortest_1117z00_1652;

					BgL__ortest_1117z00_1652 = BGL_CLASS_NIL(BgL_classz00_1651);
					if (CBOOL(BgL__ortest_1117z00_1652))
						{	/* Foreign/ctype.sch 399 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1652);
						}
					else
						{	/* Foreign/ctype.sch 399 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1651));
						}
				}
			}
		}

	}



/* &cfunction-nil */
	BgL_typez00_bglt BGl_z62cfunctionzd2nilzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2890)
	{
		{	/* Foreign/ctype.sch 399 */
			return BGl_cfunctionzd2nilzd2zzforeign_ctypez00();
		}

	}



/* cfunction-type-args */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2typezd2argsz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_222)
	{
		{	/* Foreign/ctype.sch 400 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_4849;

				{
					obj_t BgL_auxz00_4850;

					{	/* Foreign/ctype.sch 400 */
						BgL_objectz00_bglt BgL_tmpz00_4851;

						BgL_tmpz00_4851 = ((BgL_objectz00_bglt) BgL_oz00_222);
						BgL_auxz00_4850 = BGL_OBJECT_WIDENING(BgL_tmpz00_4851);
					}
					BgL_auxz00_4849 = ((BgL_cfunctionz00_bglt) BgL_auxz00_4850);
				}
				return
					(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_4849))->
					BgL_typezd2argszd2);
			}
		}

	}



/* &cfunction-type-args */
	obj_t BGl_z62cfunctionzd2typezd2argsz62zzforeign_ctypez00(obj_t
		BgL_envz00_2891, obj_t BgL_oz00_2892)
	{
		{	/* Foreign/ctype.sch 400 */
			return
				BGl_cfunctionzd2typezd2argsz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2892));
		}

	}



/* cfunction-type-res */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cfunctionzd2typezd2resz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_225)
	{
		{	/* Foreign/ctype.sch 402 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_4858;

				{
					obj_t BgL_auxz00_4859;

					{	/* Foreign/ctype.sch 402 */
						BgL_objectz00_bglt BgL_tmpz00_4860;

						BgL_tmpz00_4860 = ((BgL_objectz00_bglt) BgL_oz00_225);
						BgL_auxz00_4859 = BGL_OBJECT_WIDENING(BgL_tmpz00_4860);
					}
					BgL_auxz00_4858 = ((BgL_cfunctionz00_bglt) BgL_auxz00_4859);
				}
				return
					(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_4858))->
					BgL_typezd2reszd2);
			}
		}

	}



/* &cfunction-type-res */
	BgL_typez00_bglt BGl_z62cfunctionzd2typezd2resz62zzforeign_ctypez00(obj_t
		BgL_envz00_2893, obj_t BgL_oz00_2894)
	{
		{	/* Foreign/ctype.sch 402 */
			return
				BGl_cfunctionzd2typezd2resz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2894));
		}

	}



/* cfunction-arity */
	BGL_EXPORTED_DEF long
		BGl_cfunctionzd2arityzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_228)
	{
		{	/* Foreign/ctype.sch 404 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_4867;

				{
					obj_t BgL_auxz00_4868;

					{	/* Foreign/ctype.sch 404 */
						BgL_objectz00_bglt BgL_tmpz00_4869;

						BgL_tmpz00_4869 = ((BgL_objectz00_bglt) BgL_oz00_228);
						BgL_auxz00_4868 = BGL_OBJECT_WIDENING(BgL_tmpz00_4869);
					}
					BgL_auxz00_4867 = ((BgL_cfunctionz00_bglt) BgL_auxz00_4868);
				}
				return
					(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_4867))->BgL_arityz00);
			}
		}

	}



/* &cfunction-arity */
	obj_t BGl_z62cfunctionzd2arityzb0zzforeign_ctypez00(obj_t BgL_envz00_2895,
		obj_t BgL_oz00_2896)
	{
		{	/* Foreign/ctype.sch 404 */
			return
				BINT(BGl_cfunctionzd2arityzd2zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2896)));
		}

	}



/* cfunction-btype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cfunctionzd2btypezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_231)
	{
		{	/* Foreign/ctype.sch 406 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_4877;

				{
					obj_t BgL_auxz00_4878;

					{	/* Foreign/ctype.sch 406 */
						BgL_objectz00_bglt BgL_tmpz00_4879;

						BgL_tmpz00_4879 = ((BgL_objectz00_bglt) BgL_oz00_231);
						BgL_auxz00_4878 = BGL_OBJECT_WIDENING(BgL_tmpz00_4879);
					}
					BgL_auxz00_4877 = ((BgL_cfunctionz00_bglt) BgL_auxz00_4878);
				}
				return
					(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_4877))->BgL_btypez00);
			}
		}

	}



/* &cfunction-btype */
	BgL_typez00_bglt BGl_z62cfunctionzd2btypezb0zzforeign_ctypez00(obj_t
		BgL_envz00_2897, obj_t BgL_oz00_2898)
	{
		{	/* Foreign/ctype.sch 406 */
			return
				BGl_cfunctionzd2btypezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2898));
		}

	}



/* cfunction-occurrence */
	BGL_EXPORTED_DEF int
		BGl_cfunctionzd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_234)
	{
		{	/* Foreign/ctype.sch 408 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_234)))->BgL_occurrencez00);
		}

	}



/* &cfunction-occurrence */
	obj_t BGl_z62cfunctionzd2occurrencezb0zzforeign_ctypez00(obj_t
		BgL_envz00_2899, obj_t BgL_oz00_2900)
	{
		{	/* Foreign/ctype.sch 408 */
			return
				BINT(BGl_cfunctionzd2occurrencezd2zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2900)));
		}

	}



/* cfunction-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_235, int BgL_vz00_236)
	{
		{	/* Foreign/ctype.sch 409 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_235)))->BgL_occurrencez00) =
				((int) BgL_vz00_236), BUNSPEC);
		}

	}



/* &cfunction-occurrence-set! */
	obj_t BGl_z62cfunctionzd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2901, obj_t BgL_oz00_2902, obj_t BgL_vz00_2903)
	{
		{	/* Foreign/ctype.sch 409 */
			return
				BGl_cfunctionzd2occurrencezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2902), CINT(BgL_vz00_2903));
		}

	}



/* cfunction-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_237)
	{
		{	/* Foreign/ctype.sch 410 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_237)))->BgL_importzd2locationzd2);
		}

	}



/* &cfunction-import-location */
	obj_t BGl_z62cfunctionzd2importzd2locationz62zzforeign_ctypez00(obj_t
		BgL_envz00_2904, obj_t BgL_oz00_2905)
	{
		{	/* Foreign/ctype.sch 410 */
			return
				BGl_cfunctionzd2importzd2locationz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2905));
		}

	}



/* cfunction-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_238, obj_t BgL_vz00_239)
	{
		{	/* Foreign/ctype.sch 411 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_238)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_239), BUNSPEC);
		}

	}



/* &cfunction-import-location-set! */
	obj_t BGl_z62cfunctionzd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_2906, obj_t BgL_oz00_2907, obj_t BgL_vz00_2908)
	{
		{	/* Foreign/ctype.sch 411 */
			return
				BGl_cfunctionzd2importzd2locationzd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2907), BgL_vz00_2908);
		}

	}



/* cfunction-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_240)
	{
		{	/* Foreign/ctype.sch 412 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_240)))->BgL_locationz00);
		}

	}



/* &cfunction-location */
	obj_t BGl_z62cfunctionzd2locationzb0zzforeign_ctypez00(obj_t BgL_envz00_2909,
		obj_t BgL_oz00_2910)
	{
		{	/* Foreign/ctype.sch 412 */
			return
				BGl_cfunctionzd2locationzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2910));
		}

	}



/* cfunction-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_241, obj_t BgL_vz00_242)
	{
		{	/* Foreign/ctype.sch 413 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_241)))->BgL_locationz00) =
				((obj_t) BgL_vz00_242), BUNSPEC);
		}

	}



/* &cfunction-location-set! */
	obj_t BGl_z62cfunctionzd2locationzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2911, obj_t BgL_oz00_2912, obj_t BgL_vz00_2913)
	{
		{	/* Foreign/ctype.sch 413 */
			return
				BGl_cfunctionzd2locationzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2912), BgL_vz00_2913);
		}

	}



/* cfunction-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_243)
	{
		{	/* Foreign/ctype.sch 414 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_243)))->BgL_tvectorz00);
		}

	}



/* &cfunction-tvector */
	obj_t BGl_z62cfunctionzd2tvectorzb0zzforeign_ctypez00(obj_t BgL_envz00_2914,
		obj_t BgL_oz00_2915)
	{
		{	/* Foreign/ctype.sch 414 */
			return
				BGl_cfunctionzd2tvectorzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2915));
		}

	}



/* cfunction-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_244, obj_t BgL_vz00_245)
	{
		{	/* Foreign/ctype.sch 415 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_244)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_245), BUNSPEC);
		}

	}



/* &cfunction-tvector-set! */
	obj_t BGl_z62cfunctionzd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2916, obj_t BgL_oz00_2917, obj_t BgL_vz00_2918)
	{
		{	/* Foreign/ctype.sch 415 */
			return
				BGl_cfunctionzd2tvectorzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2917), BgL_vz00_2918);
		}

	}



/* cfunction-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_246)
	{
		{	/* Foreign/ctype.sch 416 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_246)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &cfunction-pointed-to-by */
	obj_t BGl_z62cfunctionzd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2919, obj_t BgL_oz00_2920)
	{
		{	/* Foreign/ctype.sch 416 */
			return
				BGl_cfunctionzd2pointedzd2tozd2byzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2920));
		}

	}



/* cfunction-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_247, obj_t BgL_vz00_248)
	{
		{	/* Foreign/ctype.sch 417 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_247)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_248), BUNSPEC);
		}

	}



/* &cfunction-pointed-to-by-set! */
	obj_t BGl_z62cfunctionzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2921, obj_t BgL_oz00_2922, obj_t BgL_vz00_2923)
	{
		{	/* Foreign/ctype.sch 417 */
			return
				BGl_cfunctionzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2922), BgL_vz00_2923);
		}

	}



/* cfunction-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_249)
	{
		{	/* Foreign/ctype.sch 418 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_249)))->BgL_aliasz00);
		}

	}



/* &cfunction-alias */
	obj_t BGl_z62cfunctionzd2aliaszb0zzforeign_ctypez00(obj_t BgL_envz00_2924,
		obj_t BgL_oz00_2925)
	{
		{	/* Foreign/ctype.sch 418 */
			return
				BGl_cfunctionzd2aliaszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2925));
		}

	}



/* cfunction-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_250, obj_t BgL_vz00_251)
	{
		{	/* Foreign/ctype.sch 419 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_250)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_251), BUNSPEC);
		}

	}



/* &cfunction-alias-set! */
	obj_t BGl_z62cfunctionzd2aliaszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2926, obj_t BgL_oz00_2927, obj_t BgL_vz00_2928)
	{
		{	/* Foreign/ctype.sch 419 */
			return
				BGl_cfunctionzd2aliaszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2927), BgL_vz00_2928);
		}

	}



/* cfunction-$ */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2z42z90zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_252)
	{
		{	/* Foreign/ctype.sch 420 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_252)))->BgL_z42z42);
		}

	}



/* &cfunction-$ */
	obj_t BGl_z62cfunctionzd2z42zf2zzforeign_ctypez00(obj_t BgL_envz00_2929,
		obj_t BgL_oz00_2930)
	{
		{	/* Foreign/ctype.sch 420 */
			return
				BGl_cfunctionzd2z42z90zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2930));
		}

	}



/* cfunction-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_253, obj_t BgL_vz00_254)
	{
		{	/* Foreign/ctype.sch 421 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_253)))->BgL_z42z42) =
				((obj_t) BgL_vz00_254), BUNSPEC);
		}

	}



/* &cfunction-$-set! */
	obj_t BGl_z62cfunctionzd2z42zd2setz12z32zzforeign_ctypez00(obj_t
		BgL_envz00_2931, obj_t BgL_oz00_2932, obj_t BgL_vz00_2933)
	{
		{	/* Foreign/ctype.sch 421 */
			return
				BGl_cfunctionzd2z42zd2setz12z50zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2932), BgL_vz00_2933);
		}

	}



/* cfunction-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_cfunctionzd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_255)
	{
		{	/* Foreign/ctype.sch 422 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_255)))->BgL_magiczf3zf3);
		}

	}



/* &cfunction-magic? */
	obj_t BGl_z62cfunctionzd2magiczf3z43zzforeign_ctypez00(obj_t BgL_envz00_2934,
		obj_t BgL_oz00_2935)
	{
		{	/* Foreign/ctype.sch 422 */
			return
				BBOOL(BGl_cfunctionzd2magiczf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2935)));
		}

	}



/* cfunction-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_256, bool_t BgL_vz00_257)
	{
		{	/* Foreign/ctype.sch 423 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_256)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_257), BUNSPEC);
		}

	}



/* &cfunction-magic?-set! */
	obj_t BGl_z62cfunctionzd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2936, obj_t BgL_oz00_2937, obj_t BgL_vz00_2938)
	{
		{	/* Foreign/ctype.sch 423 */
			return
				BGl_cfunctionzd2magiczf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2937), CBOOL(BgL_vz00_2938));
		}

	}



/* cfunction-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_cfunctionzd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_258)
	{
		{	/* Foreign/ctype.sch 424 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_258)))->BgL_initzf3zf3);
		}

	}



/* &cfunction-init? */
	obj_t BGl_z62cfunctionzd2initzf3z43zzforeign_ctypez00(obj_t BgL_envz00_2939,
		obj_t BgL_oz00_2940)
	{
		{	/* Foreign/ctype.sch 424 */
			return
				BBOOL(BGl_cfunctionzd2initzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2940)));
		}

	}



/* cfunction-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_259, bool_t BgL_vz00_260)
	{
		{	/* Foreign/ctype.sch 425 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_259)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_260), BUNSPEC);
		}

	}



/* &cfunction-init?-set! */
	obj_t BGl_z62cfunctionzd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_2941, obj_t BgL_oz00_2942, obj_t BgL_vz00_2943)
	{
		{	/* Foreign/ctype.sch 425 */
			return
				BGl_cfunctionzd2initzf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2942), CBOOL(BgL_vz00_2943));
		}

	}



/* cfunction-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_261)
	{
		{	/* Foreign/ctype.sch 426 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_261)))->BgL_parentsz00);
		}

	}



/* &cfunction-parents */
	obj_t BGl_z62cfunctionzd2parentszb0zzforeign_ctypez00(obj_t BgL_envz00_2944,
		obj_t BgL_oz00_2945)
	{
		{	/* Foreign/ctype.sch 426 */
			return
				BGl_cfunctionzd2parentszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2945));
		}

	}



/* cfunction-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_262, obj_t BgL_vz00_263)
	{
		{	/* Foreign/ctype.sch 427 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_262)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_263), BUNSPEC);
		}

	}



/* &cfunction-parents-set! */
	obj_t BGl_z62cfunctionzd2parentszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2946, obj_t BgL_oz00_2947, obj_t BgL_vz00_2948)
	{
		{	/* Foreign/ctype.sch 427 */
			return
				BGl_cfunctionzd2parentszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2947), BgL_vz00_2948);
		}

	}



/* cfunction-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_264)
	{
		{	/* Foreign/ctype.sch 428 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_264)))->BgL_coercezd2tozd2);
		}

	}



/* &cfunction-coerce-to */
	obj_t BGl_z62cfunctionzd2coercezd2toz62zzforeign_ctypez00(obj_t
		BgL_envz00_2949, obj_t BgL_oz00_2950)
	{
		{	/* Foreign/ctype.sch 428 */
			return
				BGl_cfunctionzd2coercezd2toz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2950));
		}

	}



/* cfunction-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_265, obj_t BgL_vz00_266)
	{
		{	/* Foreign/ctype.sch 429 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_265)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_266), BUNSPEC);
		}

	}



/* &cfunction-coerce-to-set! */
	obj_t BGl_z62cfunctionzd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_2951, obj_t BgL_oz00_2952, obj_t BgL_vz00_2953)
	{
		{	/* Foreign/ctype.sch 429 */
			return
				BGl_cfunctionzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2952), BgL_vz00_2953);
		}

	}



/* cfunction-class */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2classzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_267)
	{
		{	/* Foreign/ctype.sch 430 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_267)))->BgL_classz00);
		}

	}



/* &cfunction-class */
	obj_t BGl_z62cfunctionzd2classzb0zzforeign_ctypez00(obj_t BgL_envz00_2954,
		obj_t BgL_oz00_2955)
	{
		{	/* Foreign/ctype.sch 430 */
			return
				BGl_cfunctionzd2classzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2955));
		}

	}



/* cfunction-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_268, obj_t BgL_vz00_269)
	{
		{	/* Foreign/ctype.sch 431 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_268)))->BgL_classz00) =
				((obj_t) BgL_vz00_269), BUNSPEC);
		}

	}



/* &cfunction-class-set! */
	obj_t BGl_z62cfunctionzd2classzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2956, obj_t BgL_oz00_2957, obj_t BgL_vz00_2958)
	{
		{	/* Foreign/ctype.sch 431 */
			return
				BGl_cfunctionzd2classzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2957), BgL_vz00_2958);
		}

	}



/* cfunction-size */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_270)
	{
		{	/* Foreign/ctype.sch 432 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_270)))->BgL_siza7eza7);
		}

	}



/* &cfunction-size */
	obj_t BGl_z62cfunctionzd2siza7ez17zzforeign_ctypez00(obj_t BgL_envz00_2959,
		obj_t BgL_oz00_2960)
	{
		{	/* Foreign/ctype.sch 432 */
			return
				BGl_cfunctionzd2siza7ez75zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2960));
		}

	}



/* cfunction-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_271, obj_t BgL_vz00_272)
	{
		{	/* Foreign/ctype.sch 433 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_271)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_272), BUNSPEC);
		}

	}



/* &cfunction-size-set! */
	obj_t BGl_z62cfunctionzd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t
		BgL_envz00_2961, obj_t BgL_oz00_2962, obj_t BgL_vz00_2963)
	{
		{	/* Foreign/ctype.sch 433 */
			return
				BGl_cfunctionzd2siza7ezd2setz12zb5zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2962), BgL_vz00_2963);
		}

	}



/* cfunction-name */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2namezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_273)
	{
		{	/* Foreign/ctype.sch 434 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_273)))->BgL_namez00);
		}

	}



/* &cfunction-name */
	obj_t BGl_z62cfunctionzd2namezb0zzforeign_ctypez00(obj_t BgL_envz00_2964,
		obj_t BgL_oz00_2965)
	{
		{	/* Foreign/ctype.sch 434 */
			return
				BGl_cfunctionzd2namezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2965));
		}

	}



/* cfunction-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_274, obj_t BgL_vz00_275)
	{
		{	/* Foreign/ctype.sch 435 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_274)))->BgL_namez00) =
				((obj_t) BgL_vz00_275), BUNSPEC);
		}

	}



/* &cfunction-name-set! */
	obj_t BGl_z62cfunctionzd2namezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_2966, obj_t BgL_oz00_2967, obj_t BgL_vz00_2968)
	{
		{	/* Foreign/ctype.sch 435 */
			return
				BGl_cfunctionzd2namezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2967), BgL_vz00_2968);
		}

	}



/* cfunction-id */
	BGL_EXPORTED_DEF obj_t
		BGl_cfunctionzd2idzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_276)
	{
		{	/* Foreign/ctype.sch 436 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_276)))->BgL_idz00);
		}

	}



/* &cfunction-id */
	obj_t BGl_z62cfunctionzd2idzb0zzforeign_ctypez00(obj_t BgL_envz00_2969,
		obj_t BgL_oz00_2970)
	{
		{	/* Foreign/ctype.sch 436 */
			return
				BGl_cfunctionzd2idzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2970));
		}

	}



/* make-cptr */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2cptrzd2zzforeign_ctypez00(obj_t
		BgL_id1249z00_279, obj_t BgL_name1250z00_280, obj_t BgL_siza7e1251za7_281,
		obj_t BgL_class1252z00_282, obj_t BgL_coercezd2to1253zd2_283,
		obj_t BgL_parents1254z00_284, bool_t BgL_initzf31255zf3_285,
		bool_t BgL_magiczf31256zf3_286, obj_t BgL_z421257z42_287,
		obj_t BgL_alias1258z00_288, obj_t BgL_pointedzd2tozd2by1259z00_289,
		obj_t BgL_tvector1260z00_290, obj_t BgL_location1261z00_291,
		obj_t BgL_importzd2location1262zd2_292, int BgL_occurrence1263z00_293,
		BgL_typez00_bglt BgL_btype1264z00_294,
		BgL_typez00_bglt BgL_pointzd2to1265zd2_295, bool_t BgL_arrayzf31266zf3_296)
	{
		{	/* Foreign/ctype.sch 440 */
			{	/* Foreign/ctype.sch 440 */
				BgL_typez00_bglt BgL_new1245z00_3702;

				{	/* Foreign/ctype.sch 440 */
					BgL_typez00_bglt BgL_tmp1243z00_3703;
					BgL_cptrz00_bglt BgL_wide1244z00_3704;

					{
						BgL_typez00_bglt BgL_auxz00_5008;

						{	/* Foreign/ctype.sch 440 */
							BgL_typez00_bglt BgL_new1242z00_3705;

							BgL_new1242z00_3705 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Foreign/ctype.sch 440 */
								long BgL_arg1408z00_3706;

								BgL_arg1408z00_3706 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1242z00_3705),
									BgL_arg1408z00_3706);
							}
							{	/* Foreign/ctype.sch 440 */
								BgL_objectz00_bglt BgL_tmpz00_5013;

								BgL_tmpz00_5013 = ((BgL_objectz00_bglt) BgL_new1242z00_3705);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5013, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1242z00_3705);
							BgL_auxz00_5008 = BgL_new1242z00_3705;
						}
						BgL_tmp1243z00_3703 = ((BgL_typez00_bglt) BgL_auxz00_5008);
					}
					BgL_wide1244z00_3704 =
						((BgL_cptrz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cptrz00_bgl))));
					{	/* Foreign/ctype.sch 440 */
						obj_t BgL_auxz00_5021;
						BgL_objectz00_bglt BgL_tmpz00_5019;

						BgL_auxz00_5021 = ((obj_t) BgL_wide1244z00_3704);
						BgL_tmpz00_5019 = ((BgL_objectz00_bglt) BgL_tmp1243z00_3703);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5019, BgL_auxz00_5021);
					}
					((BgL_objectz00_bglt) BgL_tmp1243z00_3703);
					{	/* Foreign/ctype.sch 440 */
						long BgL_arg1380z00_3707;

						BgL_arg1380z00_3707 = BGL_CLASS_NUM(BGl_cptrz00zzforeign_ctypez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1243z00_3703), BgL_arg1380z00_3707);
					}
					BgL_new1245z00_3702 = ((BgL_typez00_bglt) BgL_tmp1243z00_3703);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1245z00_3702)))->BgL_idz00) =
					((obj_t) BgL_id1249z00_279), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_namez00) =
					((obj_t) BgL_name1250z00_280), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1251za7_281), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_classz00) =
					((obj_t) BgL_class1252z00_282), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1253zd2_283), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_parentsz00) =
					((obj_t) BgL_parents1254z00_284), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31255zf3_285), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31256zf3_286), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_z42z42) =
					((obj_t) BgL_z421257z42_287), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_aliasz00) =
					((obj_t) BgL_alias1258z00_288), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1259z00_289), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1260z00_290), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_locationz00) =
					((obj_t) BgL_location1261z00_291), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1262zd2_292), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1245z00_3702)))->BgL_occurrencez00) =
					((int) BgL_occurrence1263z00_293), BUNSPEC);
				{
					BgL_cptrz00_bglt BgL_auxz00_5061;

					{
						obj_t BgL_auxz00_5062;

						{	/* Foreign/ctype.sch 440 */
							BgL_objectz00_bglt BgL_tmpz00_5063;

							BgL_tmpz00_5063 = ((BgL_objectz00_bglt) BgL_new1245z00_3702);
							BgL_auxz00_5062 = BGL_OBJECT_WIDENING(BgL_tmpz00_5063);
						}
						BgL_auxz00_5061 = ((BgL_cptrz00_bglt) BgL_auxz00_5062);
					}
					((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_5061))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_btype1264z00_294), BUNSPEC);
				}
				{
					BgL_cptrz00_bglt BgL_auxz00_5068;

					{
						obj_t BgL_auxz00_5069;

						{	/* Foreign/ctype.sch 440 */
							BgL_objectz00_bglt BgL_tmpz00_5070;

							BgL_tmpz00_5070 = ((BgL_objectz00_bglt) BgL_new1245z00_3702);
							BgL_auxz00_5069 = BGL_OBJECT_WIDENING(BgL_tmpz00_5070);
						}
						BgL_auxz00_5068 = ((BgL_cptrz00_bglt) BgL_auxz00_5069);
					}
					((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_5068))->BgL_pointzd2tozd2) =
						((BgL_typez00_bglt) BgL_pointzd2to1265zd2_295), BUNSPEC);
				}
				{
					BgL_cptrz00_bglt BgL_auxz00_5075;

					{
						obj_t BgL_auxz00_5076;

						{	/* Foreign/ctype.sch 440 */
							BgL_objectz00_bglt BgL_tmpz00_5077;

							BgL_tmpz00_5077 = ((BgL_objectz00_bglt) BgL_new1245z00_3702);
							BgL_auxz00_5076 = BGL_OBJECT_WIDENING(BgL_tmpz00_5077);
						}
						BgL_auxz00_5075 = ((BgL_cptrz00_bglt) BgL_auxz00_5076);
					}
					((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_5075))->BgL_arrayzf3zf3) =
						((bool_t) BgL_arrayzf31266zf3_296), BUNSPEC);
				}
				return BgL_new1245z00_3702;
			}
		}

	}



/* &make-cptr */
	BgL_typez00_bglt BGl_z62makezd2cptrzb0zzforeign_ctypez00(obj_t
		BgL_envz00_2971, obj_t BgL_id1249z00_2972, obj_t BgL_name1250z00_2973,
		obj_t BgL_siza7e1251za7_2974, obj_t BgL_class1252z00_2975,
		obj_t BgL_coercezd2to1253zd2_2976, obj_t BgL_parents1254z00_2977,
		obj_t BgL_initzf31255zf3_2978, obj_t BgL_magiczf31256zf3_2979,
		obj_t BgL_z421257z42_2980, obj_t BgL_alias1258z00_2981,
		obj_t BgL_pointedzd2tozd2by1259z00_2982, obj_t BgL_tvector1260z00_2983,
		obj_t BgL_location1261z00_2984, obj_t BgL_importzd2location1262zd2_2985,
		obj_t BgL_occurrence1263z00_2986, obj_t BgL_btype1264z00_2987,
		obj_t BgL_pointzd2to1265zd2_2988, obj_t BgL_arrayzf31266zf3_2989)
	{
		{	/* Foreign/ctype.sch 440 */
			return
				BGl_makezd2cptrzd2zzforeign_ctypez00(BgL_id1249z00_2972,
				BgL_name1250z00_2973, BgL_siza7e1251za7_2974, BgL_class1252z00_2975,
				BgL_coercezd2to1253zd2_2976, BgL_parents1254z00_2977,
				CBOOL(BgL_initzf31255zf3_2978), CBOOL(BgL_magiczf31256zf3_2979),
				BgL_z421257z42_2980, BgL_alias1258z00_2981,
				BgL_pointedzd2tozd2by1259z00_2982, BgL_tvector1260z00_2983,
				BgL_location1261z00_2984, BgL_importzd2location1262zd2_2985,
				CINT(BgL_occurrence1263z00_2986),
				((BgL_typez00_bglt) BgL_btype1264z00_2987),
				((BgL_typez00_bglt) BgL_pointzd2to1265zd2_2988),
				CBOOL(BgL_arrayzf31266zf3_2989));
		}

	}



/* cptr? */
	BGL_EXPORTED_DEF bool_t BGl_cptrzf3zf3zzforeign_ctypez00(obj_t BgL_objz00_297)
	{
		{	/* Foreign/ctype.sch 441 */
			{	/* Foreign/ctype.sch 441 */
				obj_t BgL_classz00_3708;

				BgL_classz00_3708 = BGl_cptrz00zzforeign_ctypez00;
				if (BGL_OBJECTP(BgL_objz00_297))
					{	/* Foreign/ctype.sch 441 */
						BgL_objectz00_bglt BgL_arg1807z00_3709;

						BgL_arg1807z00_3709 = (BgL_objectz00_bglt) (BgL_objz00_297);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Foreign/ctype.sch 441 */
								long BgL_idxz00_3710;

								BgL_idxz00_3710 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3709);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3710 + 2L)) == BgL_classz00_3708);
							}
						else
							{	/* Foreign/ctype.sch 441 */
								bool_t BgL_res2103z00_3713;

								{	/* Foreign/ctype.sch 441 */
									obj_t BgL_oclassz00_3714;

									{	/* Foreign/ctype.sch 441 */
										obj_t BgL_arg1815z00_3715;
										long BgL_arg1816z00_3716;

										BgL_arg1815z00_3715 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Foreign/ctype.sch 441 */
											long BgL_arg1817z00_3717;

											BgL_arg1817z00_3717 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3709);
											BgL_arg1816z00_3716 = (BgL_arg1817z00_3717 - OBJECT_TYPE);
										}
										BgL_oclassz00_3714 =
											VECTOR_REF(BgL_arg1815z00_3715, BgL_arg1816z00_3716);
									}
									{	/* Foreign/ctype.sch 441 */
										bool_t BgL__ortest_1115z00_3718;

										BgL__ortest_1115z00_3718 =
											(BgL_classz00_3708 == BgL_oclassz00_3714);
										if (BgL__ortest_1115z00_3718)
											{	/* Foreign/ctype.sch 441 */
												BgL_res2103z00_3713 = BgL__ortest_1115z00_3718;
											}
										else
											{	/* Foreign/ctype.sch 441 */
												long BgL_odepthz00_3719;

												{	/* Foreign/ctype.sch 441 */
													obj_t BgL_arg1804z00_3720;

													BgL_arg1804z00_3720 = (BgL_oclassz00_3714);
													BgL_odepthz00_3719 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3720);
												}
												if ((2L < BgL_odepthz00_3719))
													{	/* Foreign/ctype.sch 441 */
														obj_t BgL_arg1802z00_3721;

														{	/* Foreign/ctype.sch 441 */
															obj_t BgL_arg1803z00_3722;

															BgL_arg1803z00_3722 = (BgL_oclassz00_3714);
															BgL_arg1802z00_3721 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3722,
																2L);
														}
														BgL_res2103z00_3713 =
															(BgL_arg1802z00_3721 == BgL_classz00_3708);
													}
												else
													{	/* Foreign/ctype.sch 441 */
														BgL_res2103z00_3713 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2103z00_3713;
							}
					}
				else
					{	/* Foreign/ctype.sch 441 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cptr? */
	obj_t BGl_z62cptrzf3z91zzforeign_ctypez00(obj_t BgL_envz00_2990,
		obj_t BgL_objz00_2991)
	{
		{	/* Foreign/ctype.sch 441 */
			return BBOOL(BGl_cptrzf3zf3zzforeign_ctypez00(BgL_objz00_2991));
		}

	}



/* cptr-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_cptrzd2nilzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.sch 442 */
			{	/* Foreign/ctype.sch 442 */
				obj_t BgL_classz00_1708;

				BgL_classz00_1708 = BGl_cptrz00zzforeign_ctypez00;
				{	/* Foreign/ctype.sch 442 */
					obj_t BgL__ortest_1117z00_1709;

					BgL__ortest_1117z00_1709 = BGL_CLASS_NIL(BgL_classz00_1708);
					if (CBOOL(BgL__ortest_1117z00_1709))
						{	/* Foreign/ctype.sch 442 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1709);
						}
					else
						{	/* Foreign/ctype.sch 442 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1708));
						}
				}
			}
		}

	}



/* &cptr-nil */
	BgL_typez00_bglt BGl_z62cptrzd2nilzb0zzforeign_ctypez00(obj_t BgL_envz00_2992)
	{
		{	/* Foreign/ctype.sch 442 */
			return BGl_cptrzd2nilzd2zzforeign_ctypez00();
		}

	}



/* cptr-array? */
	BGL_EXPORTED_DEF bool_t
		BGl_cptrzd2arrayzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_298)
	{
		{	/* Foreign/ctype.sch 443 */
			{
				BgL_cptrz00_bglt BgL_auxz00_5120;

				{
					obj_t BgL_auxz00_5121;

					{	/* Foreign/ctype.sch 443 */
						BgL_objectz00_bglt BgL_tmpz00_5122;

						BgL_tmpz00_5122 = ((BgL_objectz00_bglt) BgL_oz00_298);
						BgL_auxz00_5121 = BGL_OBJECT_WIDENING(BgL_tmpz00_5122);
					}
					BgL_auxz00_5120 = ((BgL_cptrz00_bglt) BgL_auxz00_5121);
				}
				return (((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_5120))->BgL_arrayzf3zf3);
			}
		}

	}



/* &cptr-array? */
	obj_t BGl_z62cptrzd2arrayzf3z43zzforeign_ctypez00(obj_t BgL_envz00_2993,
		obj_t BgL_oz00_2994)
	{
		{	/* Foreign/ctype.sch 443 */
			return
				BBOOL(BGl_cptrzd2arrayzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_2994)));
		}

	}



/* cptr-point-to */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cptrzd2pointzd2toz00zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_301)
	{
		{	/* Foreign/ctype.sch 445 */
			{
				BgL_cptrz00_bglt BgL_auxz00_5130;

				{
					obj_t BgL_auxz00_5131;

					{	/* Foreign/ctype.sch 445 */
						BgL_objectz00_bglt BgL_tmpz00_5132;

						BgL_tmpz00_5132 = ((BgL_objectz00_bglt) BgL_oz00_301);
						BgL_auxz00_5131 = BGL_OBJECT_WIDENING(BgL_tmpz00_5132);
					}
					BgL_auxz00_5130 = ((BgL_cptrz00_bglt) BgL_auxz00_5131);
				}
				return
					(((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_5130))->BgL_pointzd2tozd2);
			}
		}

	}



/* &cptr-point-to */
	BgL_typez00_bglt BGl_z62cptrzd2pointzd2toz62zzforeign_ctypez00(obj_t
		BgL_envz00_2995, obj_t BgL_oz00_2996)
	{
		{	/* Foreign/ctype.sch 445 */
			return
				BGl_cptrzd2pointzd2toz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2996));
		}

	}



/* cptr-btype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cptrzd2btypezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_304)
	{
		{	/* Foreign/ctype.sch 447 */
			{
				BgL_cptrz00_bglt BgL_auxz00_5139;

				{
					obj_t BgL_auxz00_5140;

					{	/* Foreign/ctype.sch 447 */
						BgL_objectz00_bglt BgL_tmpz00_5141;

						BgL_tmpz00_5141 = ((BgL_objectz00_bglt) BgL_oz00_304);
						BgL_auxz00_5140 = BGL_OBJECT_WIDENING(BgL_tmpz00_5141);
					}
					BgL_auxz00_5139 = ((BgL_cptrz00_bglt) BgL_auxz00_5140);
				}
				return (((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_5139))->BgL_btypez00);
			}
		}

	}



/* &cptr-btype */
	BgL_typez00_bglt BGl_z62cptrzd2btypezb0zzforeign_ctypez00(obj_t
		BgL_envz00_2997, obj_t BgL_oz00_2998)
	{
		{	/* Foreign/ctype.sch 447 */
			return
				BGl_cptrzd2btypezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_2998));
		}

	}



/* cptr-occurrence */
	BGL_EXPORTED_DEF int
		BGl_cptrzd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_307)
	{
		{	/* Foreign/ctype.sch 449 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_307)))->BgL_occurrencez00);
		}

	}



/* &cptr-occurrence */
	obj_t BGl_z62cptrzd2occurrencezb0zzforeign_ctypez00(obj_t BgL_envz00_2999,
		obj_t BgL_oz00_3000)
	{
		{	/* Foreign/ctype.sch 449 */
			return
				BINT(BGl_cptrzd2occurrencezd2zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3000)));
		}

	}



/* cptr-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_308, int BgL_vz00_309)
	{
		{	/* Foreign/ctype.sch 450 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_308)))->BgL_occurrencez00) =
				((int) BgL_vz00_309), BUNSPEC);
		}

	}



/* &cptr-occurrence-set! */
	obj_t BGl_z62cptrzd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3001, obj_t BgL_oz00_3002, obj_t BgL_vz00_3003)
	{
		{	/* Foreign/ctype.sch 450 */
			return
				BGl_cptrzd2occurrencezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3002), CINT(BgL_vz00_3003));
		}

	}



/* cptr-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_310)
	{
		{	/* Foreign/ctype.sch 451 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_310)))->BgL_importzd2locationzd2);
		}

	}



/* &cptr-import-location */
	obj_t BGl_z62cptrzd2importzd2locationz62zzforeign_ctypez00(obj_t
		BgL_envz00_3004, obj_t BgL_oz00_3005)
	{
		{	/* Foreign/ctype.sch 451 */
			return
				BGl_cptrzd2importzd2locationz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3005));
		}

	}



/* cptr-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2importzd2locationzd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_311, obj_t BgL_vz00_312)
	{
		{	/* Foreign/ctype.sch 452 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_311)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_312), BUNSPEC);
		}

	}



/* &cptr-import-location-set! */
	obj_t BGl_z62cptrzd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_3006, obj_t BgL_oz00_3007, obj_t BgL_vz00_3008)
	{
		{	/* Foreign/ctype.sch 452 */
			return
				BGl_cptrzd2importzd2locationzd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3007), BgL_vz00_3008);
		}

	}



/* cptr-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_313)
	{
		{	/* Foreign/ctype.sch 453 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_313)))->BgL_locationz00);
		}

	}



/* &cptr-location */
	obj_t BGl_z62cptrzd2locationzb0zzforeign_ctypez00(obj_t BgL_envz00_3009,
		obj_t BgL_oz00_3010)
	{
		{	/* Foreign/ctype.sch 453 */
			return
				BGl_cptrzd2locationzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3010));
		}

	}



/* cptr-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_314, obj_t BgL_vz00_315)
	{
		{	/* Foreign/ctype.sch 454 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_314)))->BgL_locationz00) =
				((obj_t) BgL_vz00_315), BUNSPEC);
		}

	}



/* &cptr-location-set! */
	obj_t BGl_z62cptrzd2locationzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3011, obj_t BgL_oz00_3012, obj_t BgL_vz00_3013)
	{
		{	/* Foreign/ctype.sch 454 */
			return
				BGl_cptrzd2locationzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3012), BgL_vz00_3013);
		}

	}



/* cptr-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_316)
	{
		{	/* Foreign/ctype.sch 455 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_316)))->BgL_tvectorz00);
		}

	}



/* &cptr-tvector */
	obj_t BGl_z62cptrzd2tvectorzb0zzforeign_ctypez00(obj_t BgL_envz00_3014,
		obj_t BgL_oz00_3015)
	{
		{	/* Foreign/ctype.sch 455 */
			return
				BGl_cptrzd2tvectorzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3015));
		}

	}



/* cptr-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_317, obj_t BgL_vz00_318)
	{
		{	/* Foreign/ctype.sch 456 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_317)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_318), BUNSPEC);
		}

	}



/* &cptr-tvector-set! */
	obj_t BGl_z62cptrzd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3016, obj_t BgL_oz00_3017, obj_t BgL_vz00_3018)
	{
		{	/* Foreign/ctype.sch 456 */
			return
				BGl_cptrzd2tvectorzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3017), BgL_vz00_3018);
		}

	}



/* cptr-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_319)
	{
		{	/* Foreign/ctype.sch 457 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_319)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &cptr-pointed-to-by */
	obj_t BGl_z62cptrzd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t
		BgL_envz00_3019, obj_t BgL_oz00_3020)
	{
		{	/* Foreign/ctype.sch 457 */
			return
				BGl_cptrzd2pointedzd2tozd2byzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3020));
		}

	}



/* cptr-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_320, obj_t BgL_vz00_321)
	{
		{	/* Foreign/ctype.sch 458 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_320)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_321), BUNSPEC);
		}

	}



/* &cptr-pointed-to-by-set! */
	obj_t BGl_z62cptrzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3021, obj_t BgL_oz00_3022, obj_t BgL_vz00_3023)
	{
		{	/* Foreign/ctype.sch 458 */
			return
				BGl_cptrzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3022), BgL_vz00_3023);
		}

	}



/* cptr-alias */
	BGL_EXPORTED_DEF obj_t BGl_cptrzd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_322)
	{
		{	/* Foreign/ctype.sch 459 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_322)))->BgL_aliasz00);
		}

	}



/* &cptr-alias */
	obj_t BGl_z62cptrzd2aliaszb0zzforeign_ctypez00(obj_t BgL_envz00_3024,
		obj_t BgL_oz00_3025)
	{
		{	/* Foreign/ctype.sch 459 */
			return
				BGl_cptrzd2aliaszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3025));
		}

	}



/* cptr-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_323, obj_t BgL_vz00_324)
	{
		{	/* Foreign/ctype.sch 460 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_323)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_324), BUNSPEC);
		}

	}



/* &cptr-alias-set! */
	obj_t BGl_z62cptrzd2aliaszd2setz12z70zzforeign_ctypez00(obj_t BgL_envz00_3026,
		obj_t BgL_oz00_3027, obj_t BgL_vz00_3028)
	{
		{	/* Foreign/ctype.sch 460 */
			return
				BGl_cptrzd2aliaszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3027), BgL_vz00_3028);
		}

	}



/* cptr-$ */
	BGL_EXPORTED_DEF obj_t BGl_cptrzd2z42z90zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_325)
	{
		{	/* Foreign/ctype.sch 461 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_325)))->BgL_z42z42);
		}

	}



/* &cptr-$ */
	obj_t BGl_z62cptrzd2z42zf2zzforeign_ctypez00(obj_t BgL_envz00_3029,
		obj_t BgL_oz00_3030)
	{
		{	/* Foreign/ctype.sch 461 */
			return
				BGl_cptrzd2z42z90zzforeign_ctypez00(((BgL_typez00_bglt) BgL_oz00_3030));
		}

	}



/* cptr-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_326,
		obj_t BgL_vz00_327)
	{
		{	/* Foreign/ctype.sch 462 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_326)))->BgL_z42z42) =
				((obj_t) BgL_vz00_327), BUNSPEC);
		}

	}



/* &cptr-$-set! */
	obj_t BGl_z62cptrzd2z42zd2setz12z32zzforeign_ctypez00(obj_t BgL_envz00_3031,
		obj_t BgL_oz00_3032, obj_t BgL_vz00_3033)
	{
		{	/* Foreign/ctype.sch 462 */
			return
				BGl_cptrzd2z42zd2setz12z50zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3032), BgL_vz00_3033);
		}

	}



/* cptr-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_cptrzd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_328)
	{
		{	/* Foreign/ctype.sch 463 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_328)))->BgL_magiczf3zf3);
		}

	}



/* &cptr-magic? */
	obj_t BGl_z62cptrzd2magiczf3z43zzforeign_ctypez00(obj_t BgL_envz00_3034,
		obj_t BgL_oz00_3035)
	{
		{	/* Foreign/ctype.sch 463 */
			return
				BBOOL(BGl_cptrzd2magiczf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3035)));
		}

	}



/* cptr-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_329, bool_t BgL_vz00_330)
	{
		{	/* Foreign/ctype.sch 464 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_329)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_330), BUNSPEC);
		}

	}



/* &cptr-magic?-set! */
	obj_t BGl_z62cptrzd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_3036, obj_t BgL_oz00_3037, obj_t BgL_vz00_3038)
	{
		{	/* Foreign/ctype.sch 464 */
			return
				BGl_cptrzd2magiczf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3037), CBOOL(BgL_vz00_3038));
		}

	}



/* cptr-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_cptrzd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_331)
	{
		{	/* Foreign/ctype.sch 465 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_331)))->BgL_initzf3zf3);
		}

	}



/* &cptr-init? */
	obj_t BGl_z62cptrzd2initzf3z43zzforeign_ctypez00(obj_t BgL_envz00_3039,
		obj_t BgL_oz00_3040)
	{
		{	/* Foreign/ctype.sch 465 */
			return
				BBOOL(BGl_cptrzd2initzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3040)));
		}

	}



/* cptr-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_332, bool_t BgL_vz00_333)
	{
		{	/* Foreign/ctype.sch 466 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_332)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_333), BUNSPEC);
		}

	}



/* &cptr-init?-set! */
	obj_t BGl_z62cptrzd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_3041, obj_t BgL_oz00_3042, obj_t BgL_vz00_3043)
	{
		{	/* Foreign/ctype.sch 466 */
			return
				BGl_cptrzd2initzf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3042), CBOOL(BgL_vz00_3043));
		}

	}



/* cptr-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_334)
	{
		{	/* Foreign/ctype.sch 467 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_334)))->BgL_parentsz00);
		}

	}



/* &cptr-parents */
	obj_t BGl_z62cptrzd2parentszb0zzforeign_ctypez00(obj_t BgL_envz00_3044,
		obj_t BgL_oz00_3045)
	{
		{	/* Foreign/ctype.sch 467 */
			return
				BGl_cptrzd2parentszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3045));
		}

	}



/* cptr-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_335, obj_t BgL_vz00_336)
	{
		{	/* Foreign/ctype.sch 468 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_335)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_336), BUNSPEC);
		}

	}



/* &cptr-parents-set! */
	obj_t BGl_z62cptrzd2parentszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3046, obj_t BgL_oz00_3047, obj_t BgL_vz00_3048)
	{
		{	/* Foreign/ctype.sch 468 */
			return
				BGl_cptrzd2parentszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3047), BgL_vz00_3048);
		}

	}



/* cptr-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_337)
	{
		{	/* Foreign/ctype.sch 469 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_337)))->BgL_coercezd2tozd2);
		}

	}



/* &cptr-coerce-to */
	obj_t BGl_z62cptrzd2coercezd2toz62zzforeign_ctypez00(obj_t BgL_envz00_3049,
		obj_t BgL_oz00_3050)
	{
		{	/* Foreign/ctype.sch 469 */
			return
				BGl_cptrzd2coercezd2toz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3050));
		}

	}



/* cptr-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_338, obj_t BgL_vz00_339)
	{
		{	/* Foreign/ctype.sch 470 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_338)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_339), BUNSPEC);
		}

	}



/* &cptr-coerce-to-set! */
	obj_t BGl_z62cptrzd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_3051, obj_t BgL_oz00_3052, obj_t BgL_vz00_3053)
	{
		{	/* Foreign/ctype.sch 470 */
			return
				BGl_cptrzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3052), BgL_vz00_3053);
		}

	}



/* cptr-class */
	BGL_EXPORTED_DEF obj_t BGl_cptrzd2classzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_340)
	{
		{	/* Foreign/ctype.sch 471 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_340)))->BgL_classz00);
		}

	}



/* &cptr-class */
	obj_t BGl_z62cptrzd2classzb0zzforeign_ctypez00(obj_t BgL_envz00_3054,
		obj_t BgL_oz00_3055)
	{
		{	/* Foreign/ctype.sch 471 */
			return
				BGl_cptrzd2classzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3055));
		}

	}



/* cptr-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_341, obj_t BgL_vz00_342)
	{
		{	/* Foreign/ctype.sch 472 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_341)))->BgL_classz00) =
				((obj_t) BgL_vz00_342), BUNSPEC);
		}

	}



/* &cptr-class-set! */
	obj_t BGl_z62cptrzd2classzd2setz12z70zzforeign_ctypez00(obj_t BgL_envz00_3056,
		obj_t BgL_oz00_3057, obj_t BgL_vz00_3058)
	{
		{	/* Foreign/ctype.sch 472 */
			return
				BGl_cptrzd2classzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3057), BgL_vz00_3058);
		}

	}



/* cptr-size */
	BGL_EXPORTED_DEF obj_t BGl_cptrzd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_343)
	{
		{	/* Foreign/ctype.sch 473 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_343)))->BgL_siza7eza7);
		}

	}



/* &cptr-size */
	obj_t BGl_z62cptrzd2siza7ez17zzforeign_ctypez00(obj_t BgL_envz00_3059,
		obj_t BgL_oz00_3060)
	{
		{	/* Foreign/ctype.sch 473 */
			return
				BGl_cptrzd2siza7ez75zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3060));
		}

	}



/* cptr-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_344, obj_t BgL_vz00_345)
	{
		{	/* Foreign/ctype.sch 474 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_344)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_345), BUNSPEC);
		}

	}



/* &cptr-size-set! */
	obj_t BGl_z62cptrzd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t
		BgL_envz00_3061, obj_t BgL_oz00_3062, obj_t BgL_vz00_3063)
	{
		{	/* Foreign/ctype.sch 474 */
			return
				BGl_cptrzd2siza7ezd2setz12zb5zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3062), BgL_vz00_3063);
		}

	}



/* cptr-name */
	BGL_EXPORTED_DEF obj_t BGl_cptrzd2namezd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_346)
	{
		{	/* Foreign/ctype.sch 475 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_346)))->BgL_namez00);
		}

	}



/* &cptr-name */
	obj_t BGl_z62cptrzd2namezb0zzforeign_ctypez00(obj_t BgL_envz00_3064,
		obj_t BgL_oz00_3065)
	{
		{	/* Foreign/ctype.sch 475 */
			return
				BGl_cptrzd2namezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3065));
		}

	}



/* cptr-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cptrzd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_347,
		obj_t BgL_vz00_348)
	{
		{	/* Foreign/ctype.sch 476 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_347)))->BgL_namez00) =
				((obj_t) BgL_vz00_348), BUNSPEC);
		}

	}



/* &cptr-name-set! */
	obj_t BGl_z62cptrzd2namezd2setz12z70zzforeign_ctypez00(obj_t BgL_envz00_3066,
		obj_t BgL_oz00_3067, obj_t BgL_vz00_3068)
	{
		{	/* Foreign/ctype.sch 476 */
			return
				BGl_cptrzd2namezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3067), BgL_vz00_3068);
		}

	}



/* cptr-id */
	BGL_EXPORTED_DEF obj_t BGl_cptrzd2idzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_349)
	{
		{	/* Foreign/ctype.sch 477 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_349)))->BgL_idz00);
		}

	}



/* &cptr-id */
	obj_t BGl_z62cptrzd2idzb0zzforeign_ctypez00(obj_t BgL_envz00_3069,
		obj_t BgL_oz00_3070)
	{
		{	/* Foreign/ctype.sch 477 */
			return
				BGl_cptrzd2idzd2zzforeign_ctypez00(((BgL_typez00_bglt) BgL_oz00_3070));
		}

	}



/* make-cstruct */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2cstructzd2zzforeign_ctypez00(obj_t BgL_id1230z00_352,
		obj_t BgL_name1231z00_353, obj_t BgL_siza7e1232za7_354,
		obj_t BgL_class1233z00_355, obj_t BgL_coercezd2to1234zd2_356,
		obj_t BgL_parents1235z00_357, bool_t BgL_initzf31236zf3_358,
		bool_t BgL_magiczf31237zf3_359, obj_t BgL_z421238z42_360,
		obj_t BgL_alias1239z00_361, obj_t BgL_pointedzd2tozd2by1240z00_362,
		obj_t BgL_tvector1241z00_363, obj_t BgL_location1242z00_364,
		obj_t BgL_importzd2location1243zd2_365, int BgL_occurrence1244z00_366,
		bool_t BgL_structzf31245zf3_367, obj_t BgL_fields1246z00_368,
		obj_t BgL_cstructza21247za2_369)
	{
		{	/* Foreign/ctype.sch 481 */
			{	/* Foreign/ctype.sch 481 */
				BgL_typez00_bglt BgL_new1249z00_3723;

				{	/* Foreign/ctype.sch 481 */
					BgL_typez00_bglt BgL_tmp1247z00_3724;
					BgL_cstructz00_bglt BgL_wide1248z00_3725;

					{
						BgL_typez00_bglt BgL_auxz00_5270;

						{	/* Foreign/ctype.sch 481 */
							BgL_typez00_bglt BgL_new1246z00_3726;

							BgL_new1246z00_3726 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Foreign/ctype.sch 481 */
								long BgL_arg1421z00_3727;

								BgL_arg1421z00_3727 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1246z00_3726),
									BgL_arg1421z00_3727);
							}
							{	/* Foreign/ctype.sch 481 */
								BgL_objectz00_bglt BgL_tmpz00_5275;

								BgL_tmpz00_5275 = ((BgL_objectz00_bglt) BgL_new1246z00_3726);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5275, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1246z00_3726);
							BgL_auxz00_5270 = BgL_new1246z00_3726;
						}
						BgL_tmp1247z00_3724 = ((BgL_typez00_bglt) BgL_auxz00_5270);
					}
					BgL_wide1248z00_3725 =
						((BgL_cstructz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cstructz00_bgl))));
					{	/* Foreign/ctype.sch 481 */
						obj_t BgL_auxz00_5283;
						BgL_objectz00_bglt BgL_tmpz00_5281;

						BgL_auxz00_5283 = ((obj_t) BgL_wide1248z00_3725);
						BgL_tmpz00_5281 = ((BgL_objectz00_bglt) BgL_tmp1247z00_3724);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5281, BgL_auxz00_5283);
					}
					((BgL_objectz00_bglt) BgL_tmp1247z00_3724);
					{	/* Foreign/ctype.sch 481 */
						long BgL_arg1410z00_3728;

						BgL_arg1410z00_3728 =
							BGL_CLASS_NUM(BGl_cstructz00zzforeign_ctypez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1247z00_3724), BgL_arg1410z00_3728);
					}
					BgL_new1249z00_3723 = ((BgL_typez00_bglt) BgL_tmp1247z00_3724);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1249z00_3723)))->BgL_idz00) =
					((obj_t) BgL_id1230z00_352), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_namez00) =
					((obj_t) BgL_name1231z00_353), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1232za7_354), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_classz00) =
					((obj_t) BgL_class1233z00_355), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1234zd2_356), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_parentsz00) =
					((obj_t) BgL_parents1235z00_357), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31236zf3_358), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31237zf3_359), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_z42z42) =
					((obj_t) BgL_z421238z42_360), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_aliasz00) =
					((obj_t) BgL_alias1239z00_361), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1240z00_362), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1241z00_363), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_locationz00) =
					((obj_t) BgL_location1242z00_364), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1243zd2_365), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1249z00_3723)))->BgL_occurrencez00) =
					((int) BgL_occurrence1244z00_366), BUNSPEC);
				{
					BgL_cstructz00_bglt BgL_auxz00_5323;

					{
						obj_t BgL_auxz00_5324;

						{	/* Foreign/ctype.sch 481 */
							BgL_objectz00_bglt BgL_tmpz00_5325;

							BgL_tmpz00_5325 = ((BgL_objectz00_bglt) BgL_new1249z00_3723);
							BgL_auxz00_5324 = BGL_OBJECT_WIDENING(BgL_tmpz00_5325);
						}
						BgL_auxz00_5323 = ((BgL_cstructz00_bglt) BgL_auxz00_5324);
					}
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_5323))->
							BgL_structzf3zf3) = ((bool_t) BgL_structzf31245zf3_367), BUNSPEC);
				}
				{
					BgL_cstructz00_bglt BgL_auxz00_5330;

					{
						obj_t BgL_auxz00_5331;

						{	/* Foreign/ctype.sch 481 */
							BgL_objectz00_bglt BgL_tmpz00_5332;

							BgL_tmpz00_5332 = ((BgL_objectz00_bglt) BgL_new1249z00_3723);
							BgL_auxz00_5331 = BGL_OBJECT_WIDENING(BgL_tmpz00_5332);
						}
						BgL_auxz00_5330 = ((BgL_cstructz00_bglt) BgL_auxz00_5331);
					}
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_5330))->BgL_fieldsz00) =
						((obj_t) BgL_fields1246z00_368), BUNSPEC);
				}
				{
					BgL_cstructz00_bglt BgL_auxz00_5337;

					{
						obj_t BgL_auxz00_5338;

						{	/* Foreign/ctype.sch 481 */
							BgL_objectz00_bglt BgL_tmpz00_5339;

							BgL_tmpz00_5339 = ((BgL_objectz00_bglt) BgL_new1249z00_3723);
							BgL_auxz00_5338 = BGL_OBJECT_WIDENING(BgL_tmpz00_5339);
						}
						BgL_auxz00_5337 = ((BgL_cstructz00_bglt) BgL_auxz00_5338);
					}
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_5337))->
							BgL_cstructza2za2) =
						((obj_t) BgL_cstructza21247za2_369), BUNSPEC);
				}
				return BgL_new1249z00_3723;
			}
		}

	}



/* &make-cstruct */
	BgL_typez00_bglt BGl_z62makezd2cstructzb0zzforeign_ctypez00(obj_t
		BgL_envz00_3071, obj_t BgL_id1230z00_3072, obj_t BgL_name1231z00_3073,
		obj_t BgL_siza7e1232za7_3074, obj_t BgL_class1233z00_3075,
		obj_t BgL_coercezd2to1234zd2_3076, obj_t BgL_parents1235z00_3077,
		obj_t BgL_initzf31236zf3_3078, obj_t BgL_magiczf31237zf3_3079,
		obj_t BgL_z421238z42_3080, obj_t BgL_alias1239z00_3081,
		obj_t BgL_pointedzd2tozd2by1240z00_3082, obj_t BgL_tvector1241z00_3083,
		obj_t BgL_location1242z00_3084, obj_t BgL_importzd2location1243zd2_3085,
		obj_t BgL_occurrence1244z00_3086, obj_t BgL_structzf31245zf3_3087,
		obj_t BgL_fields1246z00_3088, obj_t BgL_cstructza21247za2_3089)
	{
		{	/* Foreign/ctype.sch 481 */
			return
				BGl_makezd2cstructzd2zzforeign_ctypez00(BgL_id1230z00_3072,
				BgL_name1231z00_3073, BgL_siza7e1232za7_3074, BgL_class1233z00_3075,
				BgL_coercezd2to1234zd2_3076, BgL_parents1235z00_3077,
				CBOOL(BgL_initzf31236zf3_3078), CBOOL(BgL_magiczf31237zf3_3079),
				BgL_z421238z42_3080, BgL_alias1239z00_3081,
				BgL_pointedzd2tozd2by1240z00_3082, BgL_tvector1241z00_3083,
				BgL_location1242z00_3084, BgL_importzd2location1243zd2_3085,
				CINT(BgL_occurrence1244z00_3086), CBOOL(BgL_structzf31245zf3_3087),
				BgL_fields1246z00_3088, BgL_cstructza21247za2_3089);
		}

	}



/* cstruct? */
	BGL_EXPORTED_DEF bool_t BGl_cstructzf3zf3zzforeign_ctypez00(obj_t
		BgL_objz00_370)
	{
		{	/* Foreign/ctype.sch 482 */
			{	/* Foreign/ctype.sch 482 */
				obj_t BgL_classz00_3729;

				BgL_classz00_3729 = BGl_cstructz00zzforeign_ctypez00;
				if (BGL_OBJECTP(BgL_objz00_370))
					{	/* Foreign/ctype.sch 482 */
						BgL_objectz00_bglt BgL_arg1807z00_3730;

						BgL_arg1807z00_3730 = (BgL_objectz00_bglt) (BgL_objz00_370);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Foreign/ctype.sch 482 */
								long BgL_idxz00_3731;

								BgL_idxz00_3731 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3730);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3731 + 2L)) == BgL_classz00_3729);
							}
						else
							{	/* Foreign/ctype.sch 482 */
								bool_t BgL_res2104z00_3734;

								{	/* Foreign/ctype.sch 482 */
									obj_t BgL_oclassz00_3735;

									{	/* Foreign/ctype.sch 482 */
										obj_t BgL_arg1815z00_3736;
										long BgL_arg1816z00_3737;

										BgL_arg1815z00_3736 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Foreign/ctype.sch 482 */
											long BgL_arg1817z00_3738;

											BgL_arg1817z00_3738 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3730);
											BgL_arg1816z00_3737 = (BgL_arg1817z00_3738 - OBJECT_TYPE);
										}
										BgL_oclassz00_3735 =
											VECTOR_REF(BgL_arg1815z00_3736, BgL_arg1816z00_3737);
									}
									{	/* Foreign/ctype.sch 482 */
										bool_t BgL__ortest_1115z00_3739;

										BgL__ortest_1115z00_3739 =
											(BgL_classz00_3729 == BgL_oclassz00_3735);
										if (BgL__ortest_1115z00_3739)
											{	/* Foreign/ctype.sch 482 */
												BgL_res2104z00_3734 = BgL__ortest_1115z00_3739;
											}
										else
											{	/* Foreign/ctype.sch 482 */
												long BgL_odepthz00_3740;

												{	/* Foreign/ctype.sch 482 */
													obj_t BgL_arg1804z00_3741;

													BgL_arg1804z00_3741 = (BgL_oclassz00_3735);
													BgL_odepthz00_3740 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3741);
												}
												if ((2L < BgL_odepthz00_3740))
													{	/* Foreign/ctype.sch 482 */
														obj_t BgL_arg1802z00_3742;

														{	/* Foreign/ctype.sch 482 */
															obj_t BgL_arg1803z00_3743;

															BgL_arg1803z00_3743 = (BgL_oclassz00_3735);
															BgL_arg1802z00_3742 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3743,
																2L);
														}
														BgL_res2104z00_3734 =
															(BgL_arg1802z00_3742 == BgL_classz00_3729);
													}
												else
													{	/* Foreign/ctype.sch 482 */
														BgL_res2104z00_3734 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2104z00_3734;
							}
					}
				else
					{	/* Foreign/ctype.sch 482 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cstruct? */
	obj_t BGl_z62cstructzf3z91zzforeign_ctypez00(obj_t BgL_envz00_3090,
		obj_t BgL_objz00_3091)
	{
		{	/* Foreign/ctype.sch 482 */
			return BBOOL(BGl_cstructzf3zf3zzforeign_ctypez00(BgL_objz00_3091));
		}

	}



/* cstruct-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_cstructzd2nilzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.sch 483 */
			{	/* Foreign/ctype.sch 483 */
				obj_t BgL_classz00_1764;

				BgL_classz00_1764 = BGl_cstructz00zzforeign_ctypez00;
				{	/* Foreign/ctype.sch 483 */
					obj_t BgL__ortest_1117z00_1765;

					BgL__ortest_1117z00_1765 = BGL_CLASS_NIL(BgL_classz00_1764);
					if (CBOOL(BgL__ortest_1117z00_1765))
						{	/* Foreign/ctype.sch 483 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1765);
						}
					else
						{	/* Foreign/ctype.sch 483 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1764));
						}
				}
			}
		}

	}



/* &cstruct-nil */
	BgL_typez00_bglt BGl_z62cstructzd2nilzb0zzforeign_ctypez00(obj_t
		BgL_envz00_3092)
	{
		{	/* Foreign/ctype.sch 483 */
			return BGl_cstructzd2nilzd2zzforeign_ctypez00();
		}

	}



/* cstruct-cstruct* */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2cstructza2z70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_371)
	{
		{	/* Foreign/ctype.sch 484 */
			{
				BgL_cstructz00_bglt BgL_auxz00_5380;

				{
					obj_t BgL_auxz00_5381;

					{	/* Foreign/ctype.sch 484 */
						BgL_objectz00_bglt BgL_tmpz00_5382;

						BgL_tmpz00_5382 = ((BgL_objectz00_bglt) BgL_oz00_371);
						BgL_auxz00_5381 = BGL_OBJECT_WIDENING(BgL_tmpz00_5382);
					}
					BgL_auxz00_5380 = ((BgL_cstructz00_bglt) BgL_auxz00_5381);
				}
				return
					(((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_5380))->BgL_cstructza2za2);
			}
		}

	}



/* &cstruct-cstruct* */
	obj_t BGl_z62cstructzd2cstructza2z12zzforeign_ctypez00(obj_t BgL_envz00_3093,
		obj_t BgL_oz00_3094)
	{
		{	/* Foreign/ctype.sch 484 */
			return
				BGl_cstructzd2cstructza2z70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3094));
		}

	}



/* cstruct-cstruct*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2cstructza2zd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_372, obj_t BgL_vz00_373)
	{
		{	/* Foreign/ctype.sch 485 */
			{
				BgL_cstructz00_bglt BgL_auxz00_5389;

				{
					obj_t BgL_auxz00_5390;

					{	/* Foreign/ctype.sch 485 */
						BgL_objectz00_bglt BgL_tmpz00_5391;

						BgL_tmpz00_5391 = ((BgL_objectz00_bglt) BgL_oz00_372);
						BgL_auxz00_5390 = BGL_OBJECT_WIDENING(BgL_tmpz00_5391);
					}
					BgL_auxz00_5389 = ((BgL_cstructz00_bglt) BgL_auxz00_5390);
				}
				return
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_5389))->
						BgL_cstructza2za2) = ((obj_t) BgL_vz00_373), BUNSPEC);
			}
		}

	}



/* &cstruct-cstruct*-set! */
	obj_t BGl_z62cstructzd2cstructza2zd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3095, obj_t BgL_oz00_3096, obj_t BgL_vz00_3097)
	{
		{	/* Foreign/ctype.sch 485 */
			return
				BGl_cstructzd2cstructza2zd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3096), BgL_vz00_3097);
		}

	}



/* cstruct-fields */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2fieldszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_374)
	{
		{	/* Foreign/ctype.sch 486 */
			{
				BgL_cstructz00_bglt BgL_auxz00_5398;

				{
					obj_t BgL_auxz00_5399;

					{	/* Foreign/ctype.sch 486 */
						BgL_objectz00_bglt BgL_tmpz00_5400;

						BgL_tmpz00_5400 = ((BgL_objectz00_bglt) BgL_oz00_374);
						BgL_auxz00_5399 = BGL_OBJECT_WIDENING(BgL_tmpz00_5400);
					}
					BgL_auxz00_5398 = ((BgL_cstructz00_bglt) BgL_auxz00_5399);
				}
				return
					(((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_5398))->BgL_fieldsz00);
			}
		}

	}



/* &cstruct-fields */
	obj_t BGl_z62cstructzd2fieldszb0zzforeign_ctypez00(obj_t BgL_envz00_3098,
		obj_t BgL_oz00_3099)
	{
		{	/* Foreign/ctype.sch 486 */
			return
				BGl_cstructzd2fieldszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3099));
		}

	}



/* cstruct-struct? */
	BGL_EXPORTED_DEF bool_t
		BGl_cstructzd2structzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_377)
	{
		{	/* Foreign/ctype.sch 488 */
			{
				BgL_cstructz00_bglt BgL_auxz00_5407;

				{
					obj_t BgL_auxz00_5408;

					{	/* Foreign/ctype.sch 488 */
						BgL_objectz00_bglt BgL_tmpz00_5409;

						BgL_tmpz00_5409 = ((BgL_objectz00_bglt) BgL_oz00_377);
						BgL_auxz00_5408 = BGL_OBJECT_WIDENING(BgL_tmpz00_5409);
					}
					BgL_auxz00_5407 = ((BgL_cstructz00_bglt) BgL_auxz00_5408);
				}
				return
					(((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_5407))->BgL_structzf3zf3);
			}
		}

	}



/* &cstruct-struct? */
	obj_t BGl_z62cstructzd2structzf3z43zzforeign_ctypez00(obj_t BgL_envz00_3100,
		obj_t BgL_oz00_3101)
	{
		{	/* Foreign/ctype.sch 488 */
			return
				BBOOL(BGl_cstructzd2structzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3101)));
		}

	}



/* cstruct-occurrence */
	BGL_EXPORTED_DEF int
		BGl_cstructzd2occurrencezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_380)
	{
		{	/* Foreign/ctype.sch 490 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_380)))->BgL_occurrencez00);
		}

	}



/* &cstruct-occurrence */
	obj_t BGl_z62cstructzd2occurrencezb0zzforeign_ctypez00(obj_t BgL_envz00_3102,
		obj_t BgL_oz00_3103)
	{
		{	/* Foreign/ctype.sch 490 */
			return
				BINT(BGl_cstructzd2occurrencezd2zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3103)));
		}

	}



/* cstruct-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2occurrencezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_381, int BgL_vz00_382)
	{
		{	/* Foreign/ctype.sch 491 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_381)))->BgL_occurrencez00) =
				((int) BgL_vz00_382), BUNSPEC);
		}

	}



/* &cstruct-occurrence-set! */
	obj_t BGl_z62cstructzd2occurrencezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3104, obj_t BgL_oz00_3105, obj_t BgL_vz00_3106)
	{
		{	/* Foreign/ctype.sch 491 */
			return
				BGl_cstructzd2occurrencezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3105), CINT(BgL_vz00_3106));
		}

	}



/* cstruct-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2importzd2locationz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_383)
	{
		{	/* Foreign/ctype.sch 492 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_383)))->BgL_importzd2locationzd2);
		}

	}



/* &cstruct-import-location */
	obj_t BGl_z62cstructzd2importzd2locationz62zzforeign_ctypez00(obj_t
		BgL_envz00_3107, obj_t BgL_oz00_3108)
	{
		{	/* Foreign/ctype.sch 492 */
			return
				BGl_cstructzd2importzd2locationz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3108));
		}

	}



/* cstruct-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2importzd2locationzd2setz12zc0zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_384, obj_t BgL_vz00_385)
	{
		{	/* Foreign/ctype.sch 493 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_384)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_385), BUNSPEC);
		}

	}



/* &cstruct-import-location-set! */
	obj_t BGl_z62cstructzd2importzd2locationzd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_3109, obj_t BgL_oz00_3110, obj_t BgL_vz00_3111)
	{
		{	/* Foreign/ctype.sch 493 */
			return
				BGl_cstructzd2importzd2locationzd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3110), BgL_vz00_3111);
		}

	}



/* cstruct-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2locationzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_386)
	{
		{	/* Foreign/ctype.sch 494 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_386)))->BgL_locationz00);
		}

	}



/* &cstruct-location */
	obj_t BGl_z62cstructzd2locationzb0zzforeign_ctypez00(obj_t BgL_envz00_3112,
		obj_t BgL_oz00_3113)
	{
		{	/* Foreign/ctype.sch 494 */
			return
				BGl_cstructzd2locationzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3113));
		}

	}



/* cstruct-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2locationzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_387, obj_t BgL_vz00_388)
	{
		{	/* Foreign/ctype.sch 495 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_387)))->BgL_locationz00) =
				((obj_t) BgL_vz00_388), BUNSPEC);
		}

	}



/* &cstruct-location-set! */
	obj_t BGl_z62cstructzd2locationzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3114, obj_t BgL_oz00_3115, obj_t BgL_vz00_3116)
	{
		{	/* Foreign/ctype.sch 495 */
			return
				BGl_cstructzd2locationzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3115), BgL_vz00_3116);
		}

	}



/* cstruct-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2tvectorzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_389)
	{
		{	/* Foreign/ctype.sch 496 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_389)))->BgL_tvectorz00);
		}

	}



/* &cstruct-tvector */
	obj_t BGl_z62cstructzd2tvectorzb0zzforeign_ctypez00(obj_t BgL_envz00_3117,
		obj_t BgL_oz00_3118)
	{
		{	/* Foreign/ctype.sch 496 */
			return
				BGl_cstructzd2tvectorzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3118));
		}

	}



/* cstruct-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2tvectorzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_390, obj_t BgL_vz00_391)
	{
		{	/* Foreign/ctype.sch 497 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_390)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_391), BUNSPEC);
		}

	}



/* &cstruct-tvector-set! */
	obj_t BGl_z62cstructzd2tvectorzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3119, obj_t BgL_oz00_3120, obj_t BgL_vz00_3121)
	{
		{	/* Foreign/ctype.sch 497 */
			return
				BGl_cstructzd2tvectorzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3120), BgL_vz00_3121);
		}

	}



/* cstruct-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2pointedzd2tozd2byzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_392)
	{
		{	/* Foreign/ctype.sch 498 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_392)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &cstruct-pointed-to-by */
	obj_t BGl_z62cstructzd2pointedzd2tozd2byzb0zzforeign_ctypez00(obj_t
		BgL_envz00_3122, obj_t BgL_oz00_3123)
	{
		{	/* Foreign/ctype.sch 498 */
			return
				BGl_cstructzd2pointedzd2tozd2byzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3123));
		}

	}



/* cstruct-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_393, obj_t BgL_vz00_394)
	{
		{	/* Foreign/ctype.sch 499 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_393)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_394), BUNSPEC);
		}

	}



/* &cstruct-pointed-to-by-set! */
	obj_t BGl_z62cstructzd2pointedzd2tozd2byzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3124, obj_t BgL_oz00_3125, obj_t BgL_vz00_3126)
	{
		{	/* Foreign/ctype.sch 499 */
			return
				BGl_cstructzd2pointedzd2tozd2byzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3125), BgL_vz00_3126);
		}

	}



/* cstruct-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2aliaszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_395)
	{
		{	/* Foreign/ctype.sch 500 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_395)))->BgL_aliasz00);
		}

	}



/* &cstruct-alias */
	obj_t BGl_z62cstructzd2aliaszb0zzforeign_ctypez00(obj_t BgL_envz00_3127,
		obj_t BgL_oz00_3128)
	{
		{	/* Foreign/ctype.sch 500 */
			return
				BGl_cstructzd2aliaszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3128));
		}

	}



/* cstruct-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2aliaszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_396, obj_t BgL_vz00_397)
	{
		{	/* Foreign/ctype.sch 501 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_396)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_397), BUNSPEC);
		}

	}



/* &cstruct-alias-set! */
	obj_t BGl_z62cstructzd2aliaszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3129, obj_t BgL_oz00_3130, obj_t BgL_vz00_3131)
	{
		{	/* Foreign/ctype.sch 501 */
			return
				BGl_cstructzd2aliaszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3130), BgL_vz00_3131);
		}

	}



/* cstruct-$ */
	BGL_EXPORTED_DEF obj_t BGl_cstructzd2z42z90zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_398)
	{
		{	/* Foreign/ctype.sch 502 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_398)))->BgL_z42z42);
		}

	}



/* &cstruct-$ */
	obj_t BGl_z62cstructzd2z42zf2zzforeign_ctypez00(obj_t BgL_envz00_3132,
		obj_t BgL_oz00_3133)
	{
		{	/* Foreign/ctype.sch 502 */
			return
				BGl_cstructzd2z42z90zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3133));
		}

	}



/* cstruct-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2z42zd2setz12z50zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_399, obj_t BgL_vz00_400)
	{
		{	/* Foreign/ctype.sch 503 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_399)))->BgL_z42z42) =
				((obj_t) BgL_vz00_400), BUNSPEC);
		}

	}



/* &cstruct-$-set! */
	obj_t BGl_z62cstructzd2z42zd2setz12z32zzforeign_ctypez00(obj_t
		BgL_envz00_3134, obj_t BgL_oz00_3135, obj_t BgL_vz00_3136)
	{
		{	/* Foreign/ctype.sch 503 */
			return
				BGl_cstructzd2z42zd2setz12z50zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3135), BgL_vz00_3136);
		}

	}



/* cstruct-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_cstructzd2magiczf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_401)
	{
		{	/* Foreign/ctype.sch 504 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_401)))->BgL_magiczf3zf3);
		}

	}



/* &cstruct-magic? */
	obj_t BGl_z62cstructzd2magiczf3z43zzforeign_ctypez00(obj_t BgL_envz00_3137,
		obj_t BgL_oz00_3138)
	{
		{	/* Foreign/ctype.sch 504 */
			return
				BBOOL(BGl_cstructzd2magiczf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3138)));
		}

	}



/* cstruct-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2magiczf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_402, bool_t BgL_vz00_403)
	{
		{	/* Foreign/ctype.sch 505 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_402)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_403), BUNSPEC);
		}

	}



/* &cstruct-magic?-set! */
	obj_t BGl_z62cstructzd2magiczf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_3139, obj_t BgL_oz00_3140, obj_t BgL_vz00_3141)
	{
		{	/* Foreign/ctype.sch 505 */
			return
				BGl_cstructzd2magiczf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3140), CBOOL(BgL_vz00_3141));
		}

	}



/* cstruct-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_cstructzd2initzf3z21zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_404)
	{
		{	/* Foreign/ctype.sch 506 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_404)))->BgL_initzf3zf3);
		}

	}



/* &cstruct-init? */
	obj_t BGl_z62cstructzd2initzf3z43zzforeign_ctypez00(obj_t BgL_envz00_3142,
		obj_t BgL_oz00_3143)
	{
		{	/* Foreign/ctype.sch 506 */
			return
				BBOOL(BGl_cstructzd2initzf3z21zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3143)));
		}

	}



/* cstruct-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2initzf3zd2setz12ze1zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_405, bool_t BgL_vz00_406)
	{
		{	/* Foreign/ctype.sch 507 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_405)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_406), BUNSPEC);
		}

	}



/* &cstruct-init?-set! */
	obj_t BGl_z62cstructzd2initzf3zd2setz12z83zzforeign_ctypez00(obj_t
		BgL_envz00_3144, obj_t BgL_oz00_3145, obj_t BgL_vz00_3146)
	{
		{	/* Foreign/ctype.sch 507 */
			return
				BGl_cstructzd2initzf3zd2setz12ze1zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3145), CBOOL(BgL_vz00_3146));
		}

	}



/* cstruct-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2parentszd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_407)
	{
		{	/* Foreign/ctype.sch 508 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_407)))->BgL_parentsz00);
		}

	}



/* &cstruct-parents */
	obj_t BGl_z62cstructzd2parentszb0zzforeign_ctypez00(obj_t BgL_envz00_3147,
		obj_t BgL_oz00_3148)
	{
		{	/* Foreign/ctype.sch 508 */
			return
				BGl_cstructzd2parentszd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3148));
		}

	}



/* cstruct-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2parentszd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_408, obj_t BgL_vz00_409)
	{
		{	/* Foreign/ctype.sch 509 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_408)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_409), BUNSPEC);
		}

	}



/* &cstruct-parents-set! */
	obj_t BGl_z62cstructzd2parentszd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3149, obj_t BgL_oz00_3150, obj_t BgL_vz00_3151)
	{
		{	/* Foreign/ctype.sch 509 */
			return
				BGl_cstructzd2parentszd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3150), BgL_vz00_3151);
		}

	}



/* cstruct-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2coercezd2toz00zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_410)
	{
		{	/* Foreign/ctype.sch 510 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_410)))->BgL_coercezd2tozd2);
		}

	}



/* &cstruct-coerce-to */
	obj_t BGl_z62cstructzd2coercezd2toz62zzforeign_ctypez00(obj_t BgL_envz00_3152,
		obj_t BgL_oz00_3153)
	{
		{	/* Foreign/ctype.sch 510 */
			return
				BGl_cstructzd2coercezd2toz00zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3153));
		}

	}



/* cstruct-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_411, obj_t BgL_vz00_412)
	{
		{	/* Foreign/ctype.sch 511 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_411)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_412), BUNSPEC);
		}

	}



/* &cstruct-coerce-to-set! */
	obj_t BGl_z62cstructzd2coercezd2tozd2setz12za2zzforeign_ctypez00(obj_t
		BgL_envz00_3154, obj_t BgL_oz00_3155, obj_t BgL_vz00_3156)
	{
		{	/* Foreign/ctype.sch 511 */
			return
				BGl_cstructzd2coercezd2tozd2setz12zc0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3155), BgL_vz00_3156);
		}

	}



/* cstruct-class */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2classzd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_413)
	{
		{	/* Foreign/ctype.sch 512 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_413)))->BgL_classz00);
		}

	}



/* &cstruct-class */
	obj_t BGl_z62cstructzd2classzb0zzforeign_ctypez00(obj_t BgL_envz00_3157,
		obj_t BgL_oz00_3158)
	{
		{	/* Foreign/ctype.sch 512 */
			return
				BGl_cstructzd2classzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3158));
		}

	}



/* cstruct-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2classzd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_414, obj_t BgL_vz00_415)
	{
		{	/* Foreign/ctype.sch 513 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_414)))->BgL_classz00) =
				((obj_t) BgL_vz00_415), BUNSPEC);
		}

	}



/* &cstruct-class-set! */
	obj_t BGl_z62cstructzd2classzd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3159, obj_t BgL_oz00_3160, obj_t BgL_vz00_3161)
	{
		{	/* Foreign/ctype.sch 513 */
			return
				BGl_cstructzd2classzd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3160), BgL_vz00_3161);
		}

	}



/* cstruct-size */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2siza7ez75zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_416)
	{
		{	/* Foreign/ctype.sch 514 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_416)))->BgL_siza7eza7);
		}

	}



/* &cstruct-size */
	obj_t BGl_z62cstructzd2siza7ez17zzforeign_ctypez00(obj_t BgL_envz00_3162,
		obj_t BgL_oz00_3163)
	{
		{	/* Foreign/ctype.sch 514 */
			return
				BGl_cstructzd2siza7ez75zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3163));
		}

	}



/* cstruct-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2siza7ezd2setz12zb5zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_417, obj_t BgL_vz00_418)
	{
		{	/* Foreign/ctype.sch 515 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_417)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_418), BUNSPEC);
		}

	}



/* &cstruct-size-set! */
	obj_t BGl_z62cstructzd2siza7ezd2setz12zd7zzforeign_ctypez00(obj_t
		BgL_envz00_3164, obj_t BgL_oz00_3165, obj_t BgL_vz00_3166)
	{
		{	/* Foreign/ctype.sch 515 */
			return
				BGl_cstructzd2siza7ezd2setz12zb5zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3165), BgL_vz00_3166);
		}

	}



/* cstruct-name */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2namezd2zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_419)
	{
		{	/* Foreign/ctype.sch 516 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_419)))->BgL_namez00);
		}

	}



/* &cstruct-name */
	obj_t BGl_z62cstructzd2namezb0zzforeign_ctypez00(obj_t BgL_envz00_3167,
		obj_t BgL_oz00_3168)
	{
		{	/* Foreign/ctype.sch 516 */
			return
				BGl_cstructzd2namezd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3168));
		}

	}



/* cstruct-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructzd2namezd2setz12z12zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_420, obj_t BgL_vz00_421)
	{
		{	/* Foreign/ctype.sch 517 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_420)))->BgL_namez00) =
				((obj_t) BgL_vz00_421), BUNSPEC);
		}

	}



/* &cstruct-name-set! */
	obj_t BGl_z62cstructzd2namezd2setz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3169, obj_t BgL_oz00_3170, obj_t BgL_vz00_3171)
	{
		{	/* Foreign/ctype.sch 517 */
			return
				BGl_cstructzd2namezd2setz12z12zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3170), BgL_vz00_3171);
		}

	}



/* cstruct-id */
	BGL_EXPORTED_DEF obj_t BGl_cstructzd2idzd2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_422)
	{
		{	/* Foreign/ctype.sch 518 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_422)))->BgL_idz00);
		}

	}



/* &cstruct-id */
	obj_t BGl_z62cstructzd2idzb0zzforeign_ctypez00(obj_t BgL_envz00_3172,
		obj_t BgL_oz00_3173)
	{
		{	/* Foreign/ctype.sch 518 */
			return
				BGl_cstructzd2idzd2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3173));
		}

	}



/* make-cstruct* */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_makezd2cstructza2z70zzforeign_ctypez00(obj_t BgL_id1212z00_425,
		obj_t BgL_name1213z00_426, obj_t BgL_siza7e1214za7_427,
		obj_t BgL_class1215z00_428, obj_t BgL_coercezd2to1216zd2_429,
		obj_t BgL_parents1217z00_430, bool_t BgL_initzf31218zf3_431,
		bool_t BgL_magiczf31219zf3_432, obj_t BgL_z421220z42_433,
		obj_t BgL_alias1221z00_434, obj_t BgL_pointedzd2tozd2by1222z00_435,
		obj_t BgL_tvector1223z00_436, obj_t BgL_location1224z00_437,
		obj_t BgL_importzd2location1225zd2_438, int BgL_occurrence1226z00_439,
		BgL_typez00_bglt BgL_btype1227z00_440,
		BgL_typez00_bglt BgL_cstruct1228z00_441)
	{
		{	/* Foreign/ctype.sch 522 */
			{	/* Foreign/ctype.sch 522 */
				BgL_typez00_bglt BgL_new1254z00_3744;

				{	/* Foreign/ctype.sch 522 */
					BgL_typez00_bglt BgL_tmp1252z00_3745;
					BgL_cstructza2za2_bglt BgL_wide1253z00_3746;

					{
						BgL_typez00_bglt BgL_auxz00_5539;

						{	/* Foreign/ctype.sch 522 */
							BgL_typez00_bglt BgL_new1251z00_3747;

							BgL_new1251z00_3747 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Foreign/ctype.sch 522 */
								long BgL_arg1434z00_3748;

								BgL_arg1434z00_3748 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1251z00_3747),
									BgL_arg1434z00_3748);
							}
							{	/* Foreign/ctype.sch 522 */
								BgL_objectz00_bglt BgL_tmpz00_5544;

								BgL_tmpz00_5544 = ((BgL_objectz00_bglt) BgL_new1251z00_3747);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5544, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1251z00_3747);
							BgL_auxz00_5539 = BgL_new1251z00_3747;
						}
						BgL_tmp1252z00_3745 = ((BgL_typez00_bglt) BgL_auxz00_5539);
					}
					BgL_wide1253z00_3746 =
						((BgL_cstructza2za2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cstructza2za2_bgl))));
					{	/* Foreign/ctype.sch 522 */
						obj_t BgL_auxz00_5552;
						BgL_objectz00_bglt BgL_tmpz00_5550;

						BgL_auxz00_5552 = ((obj_t) BgL_wide1253z00_3746);
						BgL_tmpz00_5550 = ((BgL_objectz00_bglt) BgL_tmp1252z00_3745);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5550, BgL_auxz00_5552);
					}
					((BgL_objectz00_bglt) BgL_tmp1252z00_3745);
					{	/* Foreign/ctype.sch 522 */
						long BgL_arg1422z00_3749;

						BgL_arg1422z00_3749 =
							BGL_CLASS_NUM(BGl_cstructza2za2zzforeign_ctypez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1252z00_3745), BgL_arg1422z00_3749);
					}
					BgL_new1254z00_3744 = ((BgL_typez00_bglt) BgL_tmp1252z00_3745);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1254z00_3744)))->BgL_idz00) =
					((obj_t) BgL_id1212z00_425), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_namez00) =
					((obj_t) BgL_name1213z00_426), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1214za7_427), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_classz00) =
					((obj_t) BgL_class1215z00_428), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1216zd2_429), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_parentsz00) =
					((obj_t) BgL_parents1217z00_430), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31218zf3_431), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31219zf3_432), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_z42z42) =
					((obj_t) BgL_z421220z42_433), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_aliasz00) =
					((obj_t) BgL_alias1221z00_434), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1222z00_435), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1223z00_436), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_locationz00) =
					((obj_t) BgL_location1224z00_437), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1225zd2_438), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1254z00_3744)))->BgL_occurrencez00) =
					((int) BgL_occurrence1226z00_439), BUNSPEC);
				{
					BgL_cstructza2za2_bglt BgL_auxz00_5592;

					{
						obj_t BgL_auxz00_5593;

						{	/* Foreign/ctype.sch 522 */
							BgL_objectz00_bglt BgL_tmpz00_5594;

							BgL_tmpz00_5594 = ((BgL_objectz00_bglt) BgL_new1254z00_3744);
							BgL_auxz00_5593 = BGL_OBJECT_WIDENING(BgL_tmpz00_5594);
						}
						BgL_auxz00_5592 = ((BgL_cstructza2za2_bglt) BgL_auxz00_5593);
					}
					((((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_5592))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_btype1227z00_440), BUNSPEC);
				}
				{
					BgL_cstructza2za2_bglt BgL_auxz00_5599;

					{
						obj_t BgL_auxz00_5600;

						{	/* Foreign/ctype.sch 522 */
							BgL_objectz00_bglt BgL_tmpz00_5601;

							BgL_tmpz00_5601 = ((BgL_objectz00_bglt) BgL_new1254z00_3744);
							BgL_auxz00_5600 = BGL_OBJECT_WIDENING(BgL_tmpz00_5601);
						}
						BgL_auxz00_5599 = ((BgL_cstructza2za2_bglt) BgL_auxz00_5600);
					}
					((((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_5599))->
							BgL_cstructz00) =
						((BgL_typez00_bglt) BgL_cstruct1228z00_441), BUNSPEC);
				}
				return BgL_new1254z00_3744;
			}
		}

	}



/* &make-cstruct* */
	BgL_typez00_bglt BGl_z62makezd2cstructza2z12zzforeign_ctypez00(obj_t
		BgL_envz00_3174, obj_t BgL_id1212z00_3175, obj_t BgL_name1213z00_3176,
		obj_t BgL_siza7e1214za7_3177, obj_t BgL_class1215z00_3178,
		obj_t BgL_coercezd2to1216zd2_3179, obj_t BgL_parents1217z00_3180,
		obj_t BgL_initzf31218zf3_3181, obj_t BgL_magiczf31219zf3_3182,
		obj_t BgL_z421220z42_3183, obj_t BgL_alias1221z00_3184,
		obj_t BgL_pointedzd2tozd2by1222z00_3185, obj_t BgL_tvector1223z00_3186,
		obj_t BgL_location1224z00_3187, obj_t BgL_importzd2location1225zd2_3188,
		obj_t BgL_occurrence1226z00_3189, obj_t BgL_btype1227z00_3190,
		obj_t BgL_cstruct1228z00_3191)
	{
		{	/* Foreign/ctype.sch 522 */
			return
				BGl_makezd2cstructza2z70zzforeign_ctypez00(BgL_id1212z00_3175,
				BgL_name1213z00_3176, BgL_siza7e1214za7_3177, BgL_class1215z00_3178,
				BgL_coercezd2to1216zd2_3179, BgL_parents1217z00_3180,
				CBOOL(BgL_initzf31218zf3_3181), CBOOL(BgL_magiczf31219zf3_3182),
				BgL_z421220z42_3183, BgL_alias1221z00_3184,
				BgL_pointedzd2tozd2by1222z00_3185, BgL_tvector1223z00_3186,
				BgL_location1224z00_3187, BgL_importzd2location1225zd2_3188,
				CINT(BgL_occurrence1226z00_3189),
				((BgL_typez00_bglt) BgL_btype1227z00_3190),
				((BgL_typez00_bglt) BgL_cstruct1228z00_3191));
		}

	}



/* cstruct*? */
	BGL_EXPORTED_DEF bool_t BGl_cstructza2zf3z51zzforeign_ctypez00(obj_t
		BgL_objz00_442)
	{
		{	/* Foreign/ctype.sch 523 */
			{	/* Foreign/ctype.sch 523 */
				obj_t BgL_classz00_3750;

				BgL_classz00_3750 = BGl_cstructza2za2zzforeign_ctypez00;
				if (BGL_OBJECTP(BgL_objz00_442))
					{	/* Foreign/ctype.sch 523 */
						BgL_objectz00_bglt BgL_arg1807z00_3751;

						BgL_arg1807z00_3751 = (BgL_objectz00_bglt) (BgL_objz00_442);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Foreign/ctype.sch 523 */
								long BgL_idxz00_3752;

								BgL_idxz00_3752 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3751);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3752 + 2L)) == BgL_classz00_3750);
							}
						else
							{	/* Foreign/ctype.sch 523 */
								bool_t BgL_res2105z00_3755;

								{	/* Foreign/ctype.sch 523 */
									obj_t BgL_oclassz00_3756;

									{	/* Foreign/ctype.sch 523 */
										obj_t BgL_arg1815z00_3757;
										long BgL_arg1816z00_3758;

										BgL_arg1815z00_3757 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Foreign/ctype.sch 523 */
											long BgL_arg1817z00_3759;

											BgL_arg1817z00_3759 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3751);
											BgL_arg1816z00_3758 = (BgL_arg1817z00_3759 - OBJECT_TYPE);
										}
										BgL_oclassz00_3756 =
											VECTOR_REF(BgL_arg1815z00_3757, BgL_arg1816z00_3758);
									}
									{	/* Foreign/ctype.sch 523 */
										bool_t BgL__ortest_1115z00_3760;

										BgL__ortest_1115z00_3760 =
											(BgL_classz00_3750 == BgL_oclassz00_3756);
										if (BgL__ortest_1115z00_3760)
											{	/* Foreign/ctype.sch 523 */
												BgL_res2105z00_3755 = BgL__ortest_1115z00_3760;
											}
										else
											{	/* Foreign/ctype.sch 523 */
												long BgL_odepthz00_3761;

												{	/* Foreign/ctype.sch 523 */
													obj_t BgL_arg1804z00_3762;

													BgL_arg1804z00_3762 = (BgL_oclassz00_3756);
													BgL_odepthz00_3761 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3762);
												}
												if ((2L < BgL_odepthz00_3761))
													{	/* Foreign/ctype.sch 523 */
														obj_t BgL_arg1802z00_3763;

														{	/* Foreign/ctype.sch 523 */
															obj_t BgL_arg1803z00_3764;

															BgL_arg1803z00_3764 = (BgL_oclassz00_3756);
															BgL_arg1802z00_3763 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3764,
																2L);
														}
														BgL_res2105z00_3755 =
															(BgL_arg1802z00_3763 == BgL_classz00_3750);
													}
												else
													{	/* Foreign/ctype.sch 523 */
														BgL_res2105z00_3755 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2105z00_3755;
							}
					}
				else
					{	/* Foreign/ctype.sch 523 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cstruct*? */
	obj_t BGl_z62cstructza2zf3z33zzforeign_ctypez00(obj_t BgL_envz00_3192,
		obj_t BgL_objz00_3193)
	{
		{	/* Foreign/ctype.sch 523 */
			return BBOOL(BGl_cstructza2zf3z51zzforeign_ctypez00(BgL_objz00_3193));
		}

	}



/* cstruct*-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cstructza2zd2nilz70zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.sch 524 */
			{	/* Foreign/ctype.sch 524 */
				obj_t BgL_classz00_1820;

				BgL_classz00_1820 = BGl_cstructza2za2zzforeign_ctypez00;
				{	/* Foreign/ctype.sch 524 */
					obj_t BgL__ortest_1117z00_1821;

					BgL__ortest_1117z00_1821 = BGL_CLASS_NIL(BgL_classz00_1820);
					if (CBOOL(BgL__ortest_1117z00_1821))
						{	/* Foreign/ctype.sch 524 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_1821);
						}
					else
						{	/* Foreign/ctype.sch 524 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_1820));
						}
				}
			}
		}

	}



/* &cstruct*-nil */
	BgL_typez00_bglt BGl_z62cstructza2zd2nilz12zzforeign_ctypez00(obj_t
		BgL_envz00_3194)
	{
		{	/* Foreign/ctype.sch 524 */
			return BGl_cstructza2zd2nilz70zzforeign_ctypez00();
		}

	}



/* cstruct*-cstruct */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cstructza2zd2cstructz70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_443)
	{
		{	/* Foreign/ctype.sch 525 */
			{
				BgL_cstructza2za2_bglt BgL_auxz00_5643;

				{
					obj_t BgL_auxz00_5644;

					{	/* Foreign/ctype.sch 525 */
						BgL_objectz00_bglt BgL_tmpz00_5645;

						BgL_tmpz00_5645 = ((BgL_objectz00_bglt) BgL_oz00_443);
						BgL_auxz00_5644 = BGL_OBJECT_WIDENING(BgL_tmpz00_5645);
					}
					BgL_auxz00_5643 = ((BgL_cstructza2za2_bglt) BgL_auxz00_5644);
				}
				return
					(((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_5643))->BgL_cstructz00);
			}
		}

	}



/* &cstruct*-cstruct */
	BgL_typez00_bglt BGl_z62cstructza2zd2cstructz12zzforeign_ctypez00(obj_t
		BgL_envz00_3195, obj_t BgL_oz00_3196)
	{
		{	/* Foreign/ctype.sch 525 */
			return
				BGl_cstructza2zd2cstructz70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3196));
		}

	}



/* cstruct*-btype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cstructza2zd2btypez70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_446)
	{
		{	/* Foreign/ctype.sch 527 */
			{
				BgL_cstructza2za2_bglt BgL_auxz00_5652;

				{
					obj_t BgL_auxz00_5653;

					{	/* Foreign/ctype.sch 527 */
						BgL_objectz00_bglt BgL_tmpz00_5654;

						BgL_tmpz00_5654 = ((BgL_objectz00_bglt) BgL_oz00_446);
						BgL_auxz00_5653 = BGL_OBJECT_WIDENING(BgL_tmpz00_5654);
					}
					BgL_auxz00_5652 = ((BgL_cstructza2za2_bglt) BgL_auxz00_5653);
				}
				return
					(((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_5652))->BgL_btypez00);
			}
		}

	}



/* &cstruct*-btype */
	BgL_typez00_bglt BGl_z62cstructza2zd2btypez12zzforeign_ctypez00(obj_t
		BgL_envz00_3197, obj_t BgL_oz00_3198)
	{
		{	/* Foreign/ctype.sch 527 */
			return
				BGl_cstructza2zd2btypez70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3198));
		}

	}



/* cstruct*-occurrence */
	BGL_EXPORTED_DEF int
		BGl_cstructza2zd2occurrencez70zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_449)
	{
		{	/* Foreign/ctype.sch 529 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_449)))->BgL_occurrencez00);
		}

	}



/* &cstruct*-occurrence */
	obj_t BGl_z62cstructza2zd2occurrencez12zzforeign_ctypez00(obj_t
		BgL_envz00_3199, obj_t BgL_oz00_3200)
	{
		{	/* Foreign/ctype.sch 529 */
			return
				BINT(BGl_cstructza2zd2occurrencez70zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3200)));
		}

	}



/* cstruct*-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2occurrencezd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_450, int BgL_vz00_451)
	{
		{	/* Foreign/ctype.sch 530 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_450)))->BgL_occurrencez00) =
				((int) BgL_vz00_451), BUNSPEC);
		}

	}



/* &cstruct*-occurrence-set! */
	obj_t BGl_z62cstructza2zd2occurrencezd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3201, obj_t BgL_oz00_3202, obj_t BgL_vz00_3203)
	{
		{	/* Foreign/ctype.sch 530 */
			return
				BGl_cstructza2zd2occurrencezd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3202), CINT(BgL_vz00_3203));
		}

	}



/* cstruct*-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2importzd2locationza2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_452)
	{
		{	/* Foreign/ctype.sch 531 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_452)))->BgL_importzd2locationzd2);
		}

	}



/* &cstruct*-import-location */
	obj_t BGl_z62cstructza2zd2importzd2locationzc0zzforeign_ctypez00(obj_t
		BgL_envz00_3204, obj_t BgL_oz00_3205)
	{
		{	/* Foreign/ctype.sch 531 */
			return
				BGl_cstructza2zd2importzd2locationza2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3205));
		}

	}



/* cstruct*-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2importzd2locationzd2setz12z62zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_453, obj_t BgL_vz00_454)
	{
		{	/* Foreign/ctype.sch 532 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_453)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_454), BUNSPEC);
		}

	}



/* &cstruct*-import-location-set! */
	obj_t
		BGl_z62cstructza2zd2importzd2locationzd2setz12z00zzforeign_ctypez00(obj_t
		BgL_envz00_3206, obj_t BgL_oz00_3207, obj_t BgL_vz00_3208)
	{
		{	/* Foreign/ctype.sch 532 */
			return
				BGl_cstructza2zd2importzd2locationzd2setz12z62zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3207), BgL_vz00_3208);
		}

	}



/* cstruct*-location */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2locationz70zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_455)
	{
		{	/* Foreign/ctype.sch 533 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_455)))->BgL_locationz00);
		}

	}



/* &cstruct*-location */
	obj_t BGl_z62cstructza2zd2locationz12zzforeign_ctypez00(obj_t BgL_envz00_3209,
		obj_t BgL_oz00_3210)
	{
		{	/* Foreign/ctype.sch 533 */
			return
				BGl_cstructza2zd2locationz70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3210));
		}

	}



/* cstruct*-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2locationzd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_456, obj_t BgL_vz00_457)
	{
		{	/* Foreign/ctype.sch 534 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_456)))->BgL_locationz00) =
				((obj_t) BgL_vz00_457), BUNSPEC);
		}

	}



/* &cstruct*-location-set! */
	obj_t BGl_z62cstructza2zd2locationzd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3211, obj_t BgL_oz00_3212, obj_t BgL_vz00_3213)
	{
		{	/* Foreign/ctype.sch 534 */
			return
				BGl_cstructza2zd2locationzd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3212), BgL_vz00_3213);
		}

	}



/* cstruct*-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2tvectorz70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_458)
	{
		{	/* Foreign/ctype.sch 535 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_458)))->BgL_tvectorz00);
		}

	}



/* &cstruct*-tvector */
	obj_t BGl_z62cstructza2zd2tvectorz12zzforeign_ctypez00(obj_t BgL_envz00_3214,
		obj_t BgL_oz00_3215)
	{
		{	/* Foreign/ctype.sch 535 */
			return
				BGl_cstructza2zd2tvectorz70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3215));
		}

	}



/* cstruct*-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2tvectorzd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_459, obj_t BgL_vz00_460)
	{
		{	/* Foreign/ctype.sch 536 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_459)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_460), BUNSPEC);
		}

	}



/* &cstruct*-tvector-set! */
	obj_t BGl_z62cstructza2zd2tvectorzd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3216, obj_t BgL_oz00_3217, obj_t BgL_vz00_3218)
	{
		{	/* Foreign/ctype.sch 536 */
			return
				BGl_cstructza2zd2tvectorzd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3217), BgL_vz00_3218);
		}

	}



/* cstruct*-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2pointedzd2tozd2byz70zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_461)
	{
		{	/* Foreign/ctype.sch 537 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_461)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &cstruct*-pointed-to-by */
	obj_t BGl_z62cstructza2zd2pointedzd2tozd2byz12zzforeign_ctypez00(obj_t
		BgL_envz00_3219, obj_t BgL_oz00_3220)
	{
		{	/* Foreign/ctype.sch 537 */
			return
				BGl_cstructza2zd2pointedzd2tozd2byz70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3220));
		}

	}



/* cstruct*-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2pointedzd2tozd2byzd2setz12zb0zzforeign_ctypez00
		(BgL_typez00_bglt BgL_oz00_462, obj_t BgL_vz00_463)
	{
		{	/* Foreign/ctype.sch 538 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_462)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_463), BUNSPEC);
		}

	}



/* &cstruct*-pointed-to-by-set! */
	obj_t
		BGl_z62cstructza2zd2pointedzd2tozd2byzd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3221, obj_t BgL_oz00_3222, obj_t BgL_vz00_3223)
	{
		{	/* Foreign/ctype.sch 538 */
			return
				BGl_cstructza2zd2pointedzd2tozd2byzd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3222), BgL_vz00_3223);
		}

	}



/* cstruct*-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2aliasz70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_464)
	{
		{	/* Foreign/ctype.sch 539 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_464)))->BgL_aliasz00);
		}

	}



/* &cstruct*-alias */
	obj_t BGl_z62cstructza2zd2aliasz12zzforeign_ctypez00(obj_t BgL_envz00_3224,
		obj_t BgL_oz00_3225)
	{
		{	/* Foreign/ctype.sch 539 */
			return
				BGl_cstructza2zd2aliasz70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3225));
		}

	}



/* cstruct*-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2aliaszd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_465, obj_t BgL_vz00_466)
	{
		{	/* Foreign/ctype.sch 540 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_465)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_466), BUNSPEC);
		}

	}



/* &cstruct*-alias-set! */
	obj_t BGl_z62cstructza2zd2aliaszd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3226, obj_t BgL_oz00_3227, obj_t BgL_vz00_3228)
	{
		{	/* Foreign/ctype.sch 540 */
			return
				BGl_cstructza2zd2aliaszd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3227), BgL_vz00_3228);
		}

	}



/* cstruct*-$ */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2z42z32zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_467)
	{
		{	/* Foreign/ctype.sch 541 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_467)))->BgL_z42z42);
		}

	}



/* &cstruct*-$ */
	obj_t BGl_z62cstructza2zd2z42z50zzforeign_ctypez00(obj_t BgL_envz00_3229,
		obj_t BgL_oz00_3230)
	{
		{	/* Foreign/ctype.sch 541 */
			return
				BGl_cstructza2zd2z42z32zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3230));
		}

	}



/* cstruct*-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2z42zd2setz12zf2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_468, obj_t BgL_vz00_469)
	{
		{	/* Foreign/ctype.sch 542 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_468)))->BgL_z42z42) =
				((obj_t) BgL_vz00_469), BUNSPEC);
		}

	}



/* &cstruct*-$-set! */
	obj_t BGl_z62cstructza2zd2z42zd2setz12z90zzforeign_ctypez00(obj_t
		BgL_envz00_3231, obj_t BgL_oz00_3232, obj_t BgL_vz00_3233)
	{
		{	/* Foreign/ctype.sch 542 */
			return
				BGl_cstructza2zd2z42zd2setz12zf2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3232), BgL_vz00_3233);
		}

	}



/* cstruct*-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_cstructza2zd2magiczf3z83zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_470)
	{
		{	/* Foreign/ctype.sch 543 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_470)))->BgL_magiczf3zf3);
		}

	}



/* &cstruct*-magic? */
	obj_t BGl_z62cstructza2zd2magiczf3ze1zzforeign_ctypez00(obj_t BgL_envz00_3234,
		obj_t BgL_oz00_3235)
	{
		{	/* Foreign/ctype.sch 543 */
			return
				BBOOL(BGl_cstructza2zd2magiczf3z83zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3235)));
		}

	}



/* cstruct*-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2magiczf3zd2setz12z43zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_471, bool_t BgL_vz00_472)
	{
		{	/* Foreign/ctype.sch 544 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_471)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_472), BUNSPEC);
		}

	}



/* &cstruct*-magic?-set! */
	obj_t BGl_z62cstructza2zd2magiczf3zd2setz12z21zzforeign_ctypez00(obj_t
		BgL_envz00_3236, obj_t BgL_oz00_3237, obj_t BgL_vz00_3238)
	{
		{	/* Foreign/ctype.sch 544 */
			return
				BGl_cstructza2zd2magiczf3zd2setz12z43zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3237), CBOOL(BgL_vz00_3238));
		}

	}



/* cstruct*-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_cstructza2zd2initzf3z83zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_473)
	{
		{	/* Foreign/ctype.sch 545 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_473)))->BgL_initzf3zf3);
		}

	}



/* &cstruct*-init? */
	obj_t BGl_z62cstructza2zd2initzf3ze1zzforeign_ctypez00(obj_t BgL_envz00_3239,
		obj_t BgL_oz00_3240)
	{
		{	/* Foreign/ctype.sch 545 */
			return
				BBOOL(BGl_cstructza2zd2initzf3z83zzforeign_ctypez00(
					((BgL_typez00_bglt) BgL_oz00_3240)));
		}

	}



/* cstruct*-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2initzf3zd2setz12z43zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_474, bool_t BgL_vz00_475)
	{
		{	/* Foreign/ctype.sch 546 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_474)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_475), BUNSPEC);
		}

	}



/* &cstruct*-init?-set! */
	obj_t BGl_z62cstructza2zd2initzf3zd2setz12z21zzforeign_ctypez00(obj_t
		BgL_envz00_3241, obj_t BgL_oz00_3242, obj_t BgL_vz00_3243)
	{
		{	/* Foreign/ctype.sch 546 */
			return
				BGl_cstructza2zd2initzf3zd2setz12z43zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3242), CBOOL(BgL_vz00_3243));
		}

	}



/* cstruct*-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2parentsz70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_476)
	{
		{	/* Foreign/ctype.sch 547 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_476)))->BgL_parentsz00);
		}

	}



/* &cstruct*-parents */
	obj_t BGl_z62cstructza2zd2parentsz12zzforeign_ctypez00(obj_t BgL_envz00_3244,
		obj_t BgL_oz00_3245)
	{
		{	/* Foreign/ctype.sch 547 */
			return
				BGl_cstructza2zd2parentsz70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3245));
		}

	}



/* cstruct*-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2parentszd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_477, obj_t BgL_vz00_478)
	{
		{	/* Foreign/ctype.sch 548 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_477)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_478), BUNSPEC);
		}

	}



/* &cstruct*-parents-set! */
	obj_t BGl_z62cstructza2zd2parentszd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3246, obj_t BgL_oz00_3247, obj_t BgL_vz00_3248)
	{
		{	/* Foreign/ctype.sch 548 */
			return
				BGl_cstructza2zd2parentszd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3247), BgL_vz00_3248);
		}

	}



/* cstruct*-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2coercezd2toza2zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_479)
	{
		{	/* Foreign/ctype.sch 549 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_479)))->BgL_coercezd2tozd2);
		}

	}



/* &cstruct*-coerce-to */
	obj_t BGl_z62cstructza2zd2coercezd2tozc0zzforeign_ctypez00(obj_t
		BgL_envz00_3249, obj_t BgL_oz00_3250)
	{
		{	/* Foreign/ctype.sch 549 */
			return
				BGl_cstructza2zd2coercezd2toza2zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3250));
		}

	}



/* cstruct*-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2coercezd2tozd2setz12z62zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_480, obj_t BgL_vz00_481)
	{
		{	/* Foreign/ctype.sch 550 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_480)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_481), BUNSPEC);
		}

	}



/* &cstruct*-coerce-to-set! */
	obj_t BGl_z62cstructza2zd2coercezd2tozd2setz12z00zzforeign_ctypez00(obj_t
		BgL_envz00_3251, obj_t BgL_oz00_3252, obj_t BgL_vz00_3253)
	{
		{	/* Foreign/ctype.sch 550 */
			return
				BGl_cstructza2zd2coercezd2tozd2setz12z62zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3252), BgL_vz00_3253);
		}

	}



/* cstruct*-class */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2classz70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_482)
	{
		{	/* Foreign/ctype.sch 551 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_482)))->BgL_classz00);
		}

	}



/* &cstruct*-class */
	obj_t BGl_z62cstructza2zd2classz12zzforeign_ctypez00(obj_t BgL_envz00_3254,
		obj_t BgL_oz00_3255)
	{
		{	/* Foreign/ctype.sch 551 */
			return
				BGl_cstructza2zd2classz70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3255));
		}

	}



/* cstruct*-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2classzd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_483, obj_t BgL_vz00_484)
	{
		{	/* Foreign/ctype.sch 552 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_483)))->BgL_classz00) =
				((obj_t) BgL_vz00_484), BUNSPEC);
		}

	}



/* &cstruct*-class-set! */
	obj_t BGl_z62cstructza2zd2classzd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3256, obj_t BgL_oz00_3257, obj_t BgL_vz00_3258)
	{
		{	/* Foreign/ctype.sch 552 */
			return
				BGl_cstructza2zd2classzd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3257), BgL_vz00_3258);
		}

	}



/* cstruct*-size */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2siza7ezd7zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_485)
	{
		{	/* Foreign/ctype.sch 553 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_485)))->BgL_siza7eza7);
		}

	}



/* &cstruct*-size */
	obj_t BGl_z62cstructza2zd2siza7ezb5zzforeign_ctypez00(obj_t BgL_envz00_3259,
		obj_t BgL_oz00_3260)
	{
		{	/* Foreign/ctype.sch 553 */
			return
				BGl_cstructza2zd2siza7ezd7zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3260));
		}

	}



/* cstruct*-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2siza7ezd2setz12z17zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_486, obj_t BgL_vz00_487)
	{
		{	/* Foreign/ctype.sch 554 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_486)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_487), BUNSPEC);
		}

	}



/* &cstruct*-size-set! */
	obj_t BGl_z62cstructza2zd2siza7ezd2setz12z75zzforeign_ctypez00(obj_t
		BgL_envz00_3261, obj_t BgL_oz00_3262, obj_t BgL_vz00_3263)
	{
		{	/* Foreign/ctype.sch 554 */
			return
				BGl_cstructza2zd2siza7ezd2setz12z17zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3262), BgL_vz00_3263);
		}

	}



/* cstruct*-name */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2namez70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_488)
	{
		{	/* Foreign/ctype.sch 555 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_488)))->BgL_namez00);
		}

	}



/* &cstruct*-name */
	obj_t BGl_z62cstructza2zd2namez12zzforeign_ctypez00(obj_t BgL_envz00_3264,
		obj_t BgL_oz00_3265)
	{
		{	/* Foreign/ctype.sch 555 */
			return
				BGl_cstructza2zd2namez70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3265));
		}

	}



/* cstruct*-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2namezd2setz12zb0zzforeign_ctypez00(BgL_typez00_bglt
		BgL_oz00_489, obj_t BgL_vz00_490)
	{
		{	/* Foreign/ctype.sch 556 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_489)))->BgL_namez00) =
				((obj_t) BgL_vz00_490), BUNSPEC);
		}

	}



/* &cstruct*-name-set! */
	obj_t BGl_z62cstructza2zd2namezd2setz12zd2zzforeign_ctypez00(obj_t
		BgL_envz00_3266, obj_t BgL_oz00_3267, obj_t BgL_vz00_3268)
	{
		{	/* Foreign/ctype.sch 556 */
			return
				BGl_cstructza2zd2namezd2setz12zb0zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3267), BgL_vz00_3268);
		}

	}



/* cstruct*-id */
	BGL_EXPORTED_DEF obj_t
		BGl_cstructza2zd2idz70zzforeign_ctypez00(BgL_typez00_bglt BgL_oz00_491)
	{
		{	/* Foreign/ctype.sch 557 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_491)))->BgL_idz00);
		}

	}



/* &cstruct*-id */
	obj_t BGl_z62cstructza2zd2idz12zzforeign_ctypez00(obj_t BgL_envz00_3269,
		obj_t BgL_oz00_3270)
	{
		{	/* Foreign/ctype.sch 557 */
			return
				BGl_cstructza2zd2idz70zzforeign_ctypez00(
				((BgL_typez00_bglt) BgL_oz00_3270));
		}

	}



/* ctype? */
	BGL_EXPORTED_DEF bool_t BGl_ctypezf3zf3zzforeign_ctypez00(obj_t
		BgL_objz00_494)
	{
		{	/* Foreign/ctype.scm 85 */
			{	/* Foreign/ctype.scm 86 */
				bool_t BgL__ortest_1255z00_641;

				{	/* Foreign/ctype.scm 86 */
					obj_t BgL_classz00_1824;

					BgL_classz00_1824 = BGl_caliasz00zzforeign_ctypez00;
					if (BGL_OBJECTP(BgL_objz00_494))
						{	/* Foreign/ctype.scm 86 */
							BgL_objectz00_bglt BgL_arg1807z00_1826;

							BgL_arg1807z00_1826 = (BgL_objectz00_bglt) (BgL_objz00_494);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Foreign/ctype.scm 86 */
									long BgL_idxz00_1832;

									BgL_idxz00_1832 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1826);
									BgL__ortest_1255z00_641 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_1832 + 2L)) == BgL_classz00_1824);
								}
							else
								{	/* Foreign/ctype.scm 86 */
									bool_t BgL_res2106z00_1857;

									{	/* Foreign/ctype.scm 86 */
										obj_t BgL_oclassz00_1840;

										{	/* Foreign/ctype.scm 86 */
											obj_t BgL_arg1815z00_1848;
											long BgL_arg1816z00_1849;

											BgL_arg1815z00_1848 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Foreign/ctype.scm 86 */
												long BgL_arg1817z00_1850;

												BgL_arg1817z00_1850 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1826);
												BgL_arg1816z00_1849 =
													(BgL_arg1817z00_1850 - OBJECT_TYPE);
											}
											BgL_oclassz00_1840 =
												VECTOR_REF(BgL_arg1815z00_1848, BgL_arg1816z00_1849);
										}
										{	/* Foreign/ctype.scm 86 */
											bool_t BgL__ortest_1115z00_1841;

											BgL__ortest_1115z00_1841 =
												(BgL_classz00_1824 == BgL_oclassz00_1840);
											if (BgL__ortest_1115z00_1841)
												{	/* Foreign/ctype.scm 86 */
													BgL_res2106z00_1857 = BgL__ortest_1115z00_1841;
												}
											else
												{	/* Foreign/ctype.scm 86 */
													long BgL_odepthz00_1842;

													{	/* Foreign/ctype.scm 86 */
														obj_t BgL_arg1804z00_1843;

														BgL_arg1804z00_1843 = (BgL_oclassz00_1840);
														BgL_odepthz00_1842 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_1843);
													}
													if ((2L < BgL_odepthz00_1842))
														{	/* Foreign/ctype.scm 86 */
															obj_t BgL_arg1802z00_1845;

															{	/* Foreign/ctype.scm 86 */
																obj_t BgL_arg1803z00_1846;

																BgL_arg1803z00_1846 = (BgL_oclassz00_1840);
																BgL_arg1802z00_1845 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1846,
																	2L);
															}
															BgL_res2106z00_1857 =
																(BgL_arg1802z00_1845 == BgL_classz00_1824);
														}
													else
														{	/* Foreign/ctype.scm 86 */
															BgL_res2106z00_1857 = ((bool_t) 0);
														}
												}
										}
									}
									BgL__ortest_1255z00_641 = BgL_res2106z00_1857;
								}
						}
					else
						{	/* Foreign/ctype.scm 86 */
							BgL__ortest_1255z00_641 = ((bool_t) 0);
						}
				}
				if (BgL__ortest_1255z00_641)
					{	/* Foreign/ctype.scm 86 */
						return BgL__ortest_1255z00_641;
					}
				else
					{	/* Foreign/ctype.scm 87 */
						bool_t BgL__ortest_1256z00_642;

						{	/* Foreign/ctype.scm 87 */
							obj_t BgL_classz00_1858;

							BgL_classz00_1858 = BGl_cenumz00zzforeign_ctypez00;
							if (BGL_OBJECTP(BgL_objz00_494))
								{	/* Foreign/ctype.scm 87 */
									BgL_objectz00_bglt BgL_arg1807z00_1860;

									BgL_arg1807z00_1860 = (BgL_objectz00_bglt) (BgL_objz00_494);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Foreign/ctype.scm 87 */
											long BgL_idxz00_1866;

											BgL_idxz00_1866 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1860);
											BgL__ortest_1256z00_642 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_1866 + 2L)) == BgL_classz00_1858);
										}
									else
										{	/* Foreign/ctype.scm 87 */
											bool_t BgL_res2107z00_1891;

											{	/* Foreign/ctype.scm 87 */
												obj_t BgL_oclassz00_1874;

												{	/* Foreign/ctype.scm 87 */
													obj_t BgL_arg1815z00_1882;
													long BgL_arg1816z00_1883;

													BgL_arg1815z00_1882 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Foreign/ctype.scm 87 */
														long BgL_arg1817z00_1884;

														BgL_arg1817z00_1884 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1860);
														BgL_arg1816z00_1883 =
															(BgL_arg1817z00_1884 - OBJECT_TYPE);
													}
													BgL_oclassz00_1874 =
														VECTOR_REF(BgL_arg1815z00_1882,
														BgL_arg1816z00_1883);
												}
												{	/* Foreign/ctype.scm 87 */
													bool_t BgL__ortest_1115z00_1875;

													BgL__ortest_1115z00_1875 =
														(BgL_classz00_1858 == BgL_oclassz00_1874);
													if (BgL__ortest_1115z00_1875)
														{	/* Foreign/ctype.scm 87 */
															BgL_res2107z00_1891 = BgL__ortest_1115z00_1875;
														}
													else
														{	/* Foreign/ctype.scm 87 */
															long BgL_odepthz00_1876;

															{	/* Foreign/ctype.scm 87 */
																obj_t BgL_arg1804z00_1877;

																BgL_arg1804z00_1877 = (BgL_oclassz00_1874);
																BgL_odepthz00_1876 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_1877);
															}
															if ((2L < BgL_odepthz00_1876))
																{	/* Foreign/ctype.scm 87 */
																	obj_t BgL_arg1802z00_1879;

																	{	/* Foreign/ctype.scm 87 */
																		obj_t BgL_arg1803z00_1880;

																		BgL_arg1803z00_1880 = (BgL_oclassz00_1874);
																		BgL_arg1802z00_1879 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_1880, 2L);
																	}
																	BgL_res2107z00_1891 =
																		(BgL_arg1802z00_1879 == BgL_classz00_1858);
																}
															else
																{	/* Foreign/ctype.scm 87 */
																	BgL_res2107z00_1891 = ((bool_t) 0);
																}
														}
												}
											}
											BgL__ortest_1256z00_642 = BgL_res2107z00_1891;
										}
								}
							else
								{	/* Foreign/ctype.scm 87 */
									BgL__ortest_1256z00_642 = ((bool_t) 0);
								}
						}
						if (BgL__ortest_1256z00_642)
							{	/* Foreign/ctype.scm 87 */
								return BgL__ortest_1256z00_642;
							}
						else
							{	/* Foreign/ctype.scm 88 */
								bool_t BgL__ortest_1257z00_643;

								{	/* Foreign/ctype.scm 88 */
									obj_t BgL_classz00_1892;

									BgL_classz00_1892 = BGl_copaquez00zzforeign_ctypez00;
									if (BGL_OBJECTP(BgL_objz00_494))
										{	/* Foreign/ctype.scm 88 */
											BgL_objectz00_bglt BgL_arg1807z00_1894;

											BgL_arg1807z00_1894 =
												(BgL_objectz00_bglt) (BgL_objz00_494);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Foreign/ctype.scm 88 */
													long BgL_idxz00_1900;

													BgL_idxz00_1900 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1894);
													BgL__ortest_1257z00_643 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1900 + 2L)) == BgL_classz00_1892);
												}
											else
												{	/* Foreign/ctype.scm 88 */
													bool_t BgL_res2108z00_1925;

													{	/* Foreign/ctype.scm 88 */
														obj_t BgL_oclassz00_1908;

														{	/* Foreign/ctype.scm 88 */
															obj_t BgL_arg1815z00_1916;
															long BgL_arg1816z00_1917;

															BgL_arg1815z00_1916 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Foreign/ctype.scm 88 */
																long BgL_arg1817z00_1918;

																BgL_arg1817z00_1918 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1894);
																BgL_arg1816z00_1917 =
																	(BgL_arg1817z00_1918 - OBJECT_TYPE);
															}
															BgL_oclassz00_1908 =
																VECTOR_REF(BgL_arg1815z00_1916,
																BgL_arg1816z00_1917);
														}
														{	/* Foreign/ctype.scm 88 */
															bool_t BgL__ortest_1115z00_1909;

															BgL__ortest_1115z00_1909 =
																(BgL_classz00_1892 == BgL_oclassz00_1908);
															if (BgL__ortest_1115z00_1909)
																{	/* Foreign/ctype.scm 88 */
																	BgL_res2108z00_1925 =
																		BgL__ortest_1115z00_1909;
																}
															else
																{	/* Foreign/ctype.scm 88 */
																	long BgL_odepthz00_1910;

																	{	/* Foreign/ctype.scm 88 */
																		obj_t BgL_arg1804z00_1911;

																		BgL_arg1804z00_1911 = (BgL_oclassz00_1908);
																		BgL_odepthz00_1910 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1911);
																	}
																	if ((2L < BgL_odepthz00_1910))
																		{	/* Foreign/ctype.scm 88 */
																			obj_t BgL_arg1802z00_1913;

																			{	/* Foreign/ctype.scm 88 */
																				obj_t BgL_arg1803z00_1914;

																				BgL_arg1803z00_1914 =
																					(BgL_oclassz00_1908);
																				BgL_arg1802z00_1913 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1914, 2L);
																			}
																			BgL_res2108z00_1925 =
																				(BgL_arg1802z00_1913 ==
																				BgL_classz00_1892);
																		}
																	else
																		{	/* Foreign/ctype.scm 88 */
																			BgL_res2108z00_1925 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL__ortest_1257z00_643 = BgL_res2108z00_1925;
												}
										}
									else
										{	/* Foreign/ctype.scm 88 */
											BgL__ortest_1257z00_643 = ((bool_t) 0);
										}
								}
								if (BgL__ortest_1257z00_643)
									{	/* Foreign/ctype.scm 88 */
										return BgL__ortest_1257z00_643;
									}
								else
									{	/* Foreign/ctype.scm 89 */
										bool_t BgL__ortest_1258z00_644;

										{	/* Foreign/ctype.scm 89 */
											obj_t BgL_classz00_1926;

											BgL_classz00_1926 = BGl_cfunctionz00zzforeign_ctypez00;
											if (BGL_OBJECTP(BgL_objz00_494))
												{	/* Foreign/ctype.scm 89 */
													BgL_objectz00_bglt BgL_arg1807z00_1928;

													BgL_arg1807z00_1928 =
														(BgL_objectz00_bglt) (BgL_objz00_494);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Foreign/ctype.scm 89 */
															long BgL_idxz00_1934;

															BgL_idxz00_1934 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1928);
															BgL__ortest_1258z00_644 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_1934 + 2L)) == BgL_classz00_1926);
														}
													else
														{	/* Foreign/ctype.scm 89 */
															bool_t BgL_res2109z00_1959;

															{	/* Foreign/ctype.scm 89 */
																obj_t BgL_oclassz00_1942;

																{	/* Foreign/ctype.scm 89 */
																	obj_t BgL_arg1815z00_1950;
																	long BgL_arg1816z00_1951;

																	BgL_arg1815z00_1950 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Foreign/ctype.scm 89 */
																		long BgL_arg1817z00_1952;

																		BgL_arg1817z00_1952 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1928);
																		BgL_arg1816z00_1951 =
																			(BgL_arg1817z00_1952 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_1942 =
																		VECTOR_REF(BgL_arg1815z00_1950,
																		BgL_arg1816z00_1951);
																}
																{	/* Foreign/ctype.scm 89 */
																	bool_t BgL__ortest_1115z00_1943;

																	BgL__ortest_1115z00_1943 =
																		(BgL_classz00_1926 == BgL_oclassz00_1942);
																	if (BgL__ortest_1115z00_1943)
																		{	/* Foreign/ctype.scm 89 */
																			BgL_res2109z00_1959 =
																				BgL__ortest_1115z00_1943;
																		}
																	else
																		{	/* Foreign/ctype.scm 89 */
																			long BgL_odepthz00_1944;

																			{	/* Foreign/ctype.scm 89 */
																				obj_t BgL_arg1804z00_1945;

																				BgL_arg1804z00_1945 =
																					(BgL_oclassz00_1942);
																				BgL_odepthz00_1944 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_1945);
																			}
																			if ((2L < BgL_odepthz00_1944))
																				{	/* Foreign/ctype.scm 89 */
																					obj_t BgL_arg1802z00_1947;

																					{	/* Foreign/ctype.scm 89 */
																						obj_t BgL_arg1803z00_1948;

																						BgL_arg1803z00_1948 =
																							(BgL_oclassz00_1942);
																						BgL_arg1802z00_1947 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_1948, 2L);
																					}
																					BgL_res2109z00_1959 =
																						(BgL_arg1802z00_1947 ==
																						BgL_classz00_1926);
																				}
																			else
																				{	/* Foreign/ctype.scm 89 */
																					BgL_res2109z00_1959 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL__ortest_1258z00_644 = BgL_res2109z00_1959;
														}
												}
											else
												{	/* Foreign/ctype.scm 89 */
													BgL__ortest_1258z00_644 = ((bool_t) 0);
												}
										}
										if (BgL__ortest_1258z00_644)
											{	/* Foreign/ctype.scm 89 */
												return BgL__ortest_1258z00_644;
											}
										else
											{	/* Foreign/ctype.scm 90 */
												bool_t BgL__ortest_1259z00_645;

												{	/* Foreign/ctype.scm 90 */
													obj_t BgL_classz00_1960;

													BgL_classz00_1960 = BGl_cptrz00zzforeign_ctypez00;
													if (BGL_OBJECTP(BgL_objz00_494))
														{	/* Foreign/ctype.scm 90 */
															BgL_objectz00_bglt BgL_arg1807z00_1962;

															BgL_arg1807z00_1962 =
																(BgL_objectz00_bglt) (BgL_objz00_494);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Foreign/ctype.scm 90 */
																	long BgL_idxz00_1968;

																	BgL_idxz00_1968 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_1962);
																	BgL__ortest_1259z00_645 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_1968 + 2L)) ==
																		BgL_classz00_1960);
																}
															else
																{	/* Foreign/ctype.scm 90 */
																	bool_t BgL_res2110z00_1993;

																	{	/* Foreign/ctype.scm 90 */
																		obj_t BgL_oclassz00_1976;

																		{	/* Foreign/ctype.scm 90 */
																			obj_t BgL_arg1815z00_1984;
																			long BgL_arg1816z00_1985;

																			BgL_arg1815z00_1984 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Foreign/ctype.scm 90 */
																				long BgL_arg1817z00_1986;

																				BgL_arg1817z00_1986 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_1962);
																				BgL_arg1816z00_1985 =
																					(BgL_arg1817z00_1986 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_1976 =
																				VECTOR_REF(BgL_arg1815z00_1984,
																				BgL_arg1816z00_1985);
																		}
																		{	/* Foreign/ctype.scm 90 */
																			bool_t BgL__ortest_1115z00_1977;

																			BgL__ortest_1115z00_1977 =
																				(BgL_classz00_1960 ==
																				BgL_oclassz00_1976);
																			if (BgL__ortest_1115z00_1977)
																				{	/* Foreign/ctype.scm 90 */
																					BgL_res2110z00_1993 =
																						BgL__ortest_1115z00_1977;
																				}
																			else
																				{	/* Foreign/ctype.scm 90 */
																					long BgL_odepthz00_1978;

																					{	/* Foreign/ctype.scm 90 */
																						obj_t BgL_arg1804z00_1979;

																						BgL_arg1804z00_1979 =
																							(BgL_oclassz00_1976);
																						BgL_odepthz00_1978 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_1979);
																					}
																					if ((2L < BgL_odepthz00_1978))
																						{	/* Foreign/ctype.scm 90 */
																							obj_t BgL_arg1802z00_1981;

																							{	/* Foreign/ctype.scm 90 */
																								obj_t BgL_arg1803z00_1982;

																								BgL_arg1803z00_1982 =
																									(BgL_oclassz00_1976);
																								BgL_arg1802z00_1981 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_1982, 2L);
																							}
																							BgL_res2110z00_1993 =
																								(BgL_arg1802z00_1981 ==
																								BgL_classz00_1960);
																						}
																					else
																						{	/* Foreign/ctype.scm 90 */
																							BgL_res2110z00_1993 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL__ortest_1259z00_645 = BgL_res2110z00_1993;
																}
														}
													else
														{	/* Foreign/ctype.scm 90 */
															BgL__ortest_1259z00_645 = ((bool_t) 0);
														}
												}
												if (BgL__ortest_1259z00_645)
													{	/* Foreign/ctype.scm 90 */
														return BgL__ortest_1259z00_645;
													}
												else
													{	/* Foreign/ctype.scm 91 */
														bool_t BgL__ortest_1260z00_646;

														{	/* Foreign/ctype.scm 91 */
															obj_t BgL_classz00_1994;

															BgL_classz00_1994 =
																BGl_cstructz00zzforeign_ctypez00;
															if (BGL_OBJECTP(BgL_objz00_494))
																{	/* Foreign/ctype.scm 91 */
																	BgL_objectz00_bglt BgL_arg1807z00_1996;

																	BgL_arg1807z00_1996 =
																		(BgL_objectz00_bglt) (BgL_objz00_494);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Foreign/ctype.scm 91 */
																			long BgL_idxz00_2002;

																			BgL_idxz00_2002 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_1996);
																			BgL__ortest_1260z00_646 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2002 + 2L)) ==
																				BgL_classz00_1994);
																		}
																	else
																		{	/* Foreign/ctype.scm 91 */
																			bool_t BgL_res2111z00_2027;

																			{	/* Foreign/ctype.scm 91 */
																				obj_t BgL_oclassz00_2010;

																				{	/* Foreign/ctype.scm 91 */
																					obj_t BgL_arg1815z00_2018;
																					long BgL_arg1816z00_2019;

																					BgL_arg1815z00_2018 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Foreign/ctype.scm 91 */
																						long BgL_arg1817z00_2020;

																						BgL_arg1817z00_2020 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_1996);
																						BgL_arg1816z00_2019 =
																							(BgL_arg1817z00_2020 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2010 =
																						VECTOR_REF(BgL_arg1815z00_2018,
																						BgL_arg1816z00_2019);
																				}
																				{	/* Foreign/ctype.scm 91 */
																					bool_t BgL__ortest_1115z00_2011;

																					BgL__ortest_1115z00_2011 =
																						(BgL_classz00_1994 ==
																						BgL_oclassz00_2010);
																					if (BgL__ortest_1115z00_2011)
																						{	/* Foreign/ctype.scm 91 */
																							BgL_res2111z00_2027 =
																								BgL__ortest_1115z00_2011;
																						}
																					else
																						{	/* Foreign/ctype.scm 91 */
																							long BgL_odepthz00_2012;

																							{	/* Foreign/ctype.scm 91 */
																								obj_t BgL_arg1804z00_2013;

																								BgL_arg1804z00_2013 =
																									(BgL_oclassz00_2010);
																								BgL_odepthz00_2012 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2013);
																							}
																							if ((2L < BgL_odepthz00_2012))
																								{	/* Foreign/ctype.scm 91 */
																									obj_t BgL_arg1802z00_2015;

																									{	/* Foreign/ctype.scm 91 */
																										obj_t BgL_arg1803z00_2016;

																										BgL_arg1803z00_2016 =
																											(BgL_oclassz00_2010);
																										BgL_arg1802z00_2015 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2016, 2L);
																									}
																									BgL_res2111z00_2027 =
																										(BgL_arg1802z00_2015 ==
																										BgL_classz00_1994);
																								}
																							else
																								{	/* Foreign/ctype.scm 91 */
																									BgL_res2111z00_2027 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL__ortest_1260z00_646 =
																				BgL_res2111z00_2027;
																		}
																}
															else
																{	/* Foreign/ctype.scm 91 */
																	BgL__ortest_1260z00_646 = ((bool_t) 0);
																}
														}
														if (BgL__ortest_1260z00_646)
															{	/* Foreign/ctype.scm 91 */
																return BgL__ortest_1260z00_646;
															}
														else
															{	/* Foreign/ctype.scm 92 */
																obj_t BgL_classz00_2028;

																BgL_classz00_2028 =
																	BGl_cstructza2za2zzforeign_ctypez00;
																if (BGL_OBJECTP(BgL_objz00_494))
																	{	/* Foreign/ctype.scm 92 */
																		BgL_objectz00_bglt BgL_arg1807z00_2030;

																		BgL_arg1807z00_2030 =
																			(BgL_objectz00_bglt) (BgL_objz00_494);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Foreign/ctype.scm 92 */
																				long BgL_idxz00_2036;

																				BgL_idxz00_2036 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_2030);
																				return (VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_2036 + 2L)) ==
																					BgL_classz00_2028);
																			}
																		else
																			{	/* Foreign/ctype.scm 92 */
																				bool_t BgL_res2112z00_2061;

																				{	/* Foreign/ctype.scm 92 */
																					obj_t BgL_oclassz00_2044;

																					{	/* Foreign/ctype.scm 92 */
																						obj_t BgL_arg1815z00_2052;
																						long BgL_arg1816z00_2053;

																						BgL_arg1815z00_2052 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Foreign/ctype.scm 92 */
																							long BgL_arg1817z00_2054;

																							BgL_arg1817z00_2054 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_2030);
																							BgL_arg1816z00_2053 =
																								(BgL_arg1817z00_2054 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_2044 =
																							VECTOR_REF(BgL_arg1815z00_2052,
																							BgL_arg1816z00_2053);
																					}
																					{	/* Foreign/ctype.scm 92 */
																						bool_t BgL__ortest_1115z00_2045;

																						BgL__ortest_1115z00_2045 =
																							(BgL_classz00_2028 ==
																							BgL_oclassz00_2044);
																						if (BgL__ortest_1115z00_2045)
																							{	/* Foreign/ctype.scm 92 */
																								BgL_res2112z00_2061 =
																									BgL__ortest_1115z00_2045;
																							}
																						else
																							{	/* Foreign/ctype.scm 92 */
																								long BgL_odepthz00_2046;

																								{	/* Foreign/ctype.scm 92 */
																									obj_t BgL_arg1804z00_2047;

																									BgL_arg1804z00_2047 =
																										(BgL_oclassz00_2044);
																									BgL_odepthz00_2046 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_2047);
																								}
																								if ((2L < BgL_odepthz00_2046))
																									{	/* Foreign/ctype.scm 92 */
																										obj_t BgL_arg1802z00_2049;

																										{	/* Foreign/ctype.scm 92 */
																											obj_t BgL_arg1803z00_2050;

																											BgL_arg1803z00_2050 =
																												(BgL_oclassz00_2044);
																											BgL_arg1802z00_2049 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_2050,
																												2L);
																										}
																										BgL_res2112z00_2061 =
																											(BgL_arg1802z00_2049 ==
																											BgL_classz00_2028);
																									}
																								else
																									{	/* Foreign/ctype.scm 92 */
																										BgL_res2112z00_2061 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				return BgL_res2112z00_2061;
																			}
																	}
																else
																	{	/* Foreign/ctype.scm 92 */
																		return ((bool_t) 0);
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &ctype? */
	obj_t BGl_z62ctypezf3z91zzforeign_ctypez00(obj_t BgL_envz00_3271,
		obj_t BgL_objz00_3272)
	{
		{	/* Foreign/ctype.scm 85 */
			return BBOOL(BGl_ctypezf3zf3zzforeign_ctypez00(BgL_objz00_3272));
		}

	}



/* declare-c-type! */
	BGL_EXPORTED_DEF obj_t BGl_declarezd2czd2typez12z12zzforeign_ctypez00(obj_t
		BgL_ctzd2defzd2_495, obj_t BgL_ctzd2idzd2_496, obj_t BgL_ctzd2expzd2_497,
		obj_t BgL_ctzd2namezd2_498)
	{
		{	/* Foreign/ctype.scm 97 */
			if (BGl_typezd2existszf3z21zztype_envz00(BgL_ctzd2idzd2_496))
				{	/* Foreign/ctype.scm 99 */
					if (CBOOL(BGl_za2allowzd2typezd2redefinitionza2z00zzengine_paramz00))
						{	/* Foreign/ctype.scm 100 */
							BFALSE;
						}
					else
						{	/* Foreign/ctype.scm 101 */
							obj_t BgL_list1436z00_648;

							{	/* Foreign/ctype.scm 101 */
								obj_t BgL_arg1437z00_649;

								{	/* Foreign/ctype.scm 101 */
									obj_t BgL_arg1448z00_650;

									BgL_arg1448z00_650 =
										MAKE_YOUNG_PAIR(BgL_ctzd2idzd2_496, BNIL);
									BgL_arg1437z00_649 =
										MAKE_YOUNG_PAIR(BGl_string2116z00zzforeign_ctypez00,
										BgL_arg1448z00_650);
								}
								BgL_list1436z00_648 =
									MAKE_YOUNG_PAIR(BGl_string2117z00zzforeign_ctypez00,
									BgL_arg1437z00_649);
							}
							BGl_warningz00zz__errorz00(BgL_list1436z00_648);
						}
					return BUNSPEC;
				}
			else
				{	/* Foreign/ctype.scm 99 */
					if (SYMBOLP(BgL_ctzd2expzd2_497))
						{	/* Foreign/ctype.scm 103 */
							return
								((obj_t)
								BGl_declarezd2czd2aliasz12z12zzforeign_ctypez00
								(BgL_ctzd2idzd2_496, BgL_ctzd2expzd2_497,
									BgL_ctzd2namezd2_498));
						}
					else
						{	/* Foreign/ctype.scm 103 */
							if (PAIRP(BgL_ctzd2expzd2_497))
								{	/* Foreign/ctype.scm 106 */
									obj_t BgL_casezd2valuezd2_653;

									BgL_casezd2valuezd2_653 = CAR(BgL_ctzd2expzd2_497);
									if ((BgL_casezd2valuezd2_653 == CNST_TABLE_REF(0)))
										{	/* Foreign/ctype.scm 106 */
											return
												((obj_t)
												BGl_declarezd2czd2enumz12z12zzforeign_ctypez00
												(BgL_ctzd2idzd2_496, BgL_ctzd2expzd2_497,
													BgL_ctzd2namezd2_498));
										}
									else
										{	/* Foreign/ctype.scm 106 */
											if ((BgL_casezd2valuezd2_653 == CNST_TABLE_REF(1)))
												{	/* Foreign/ctype.scm 106 */
													return
														((obj_t)
														BGl_declarezd2czd2opaquez12z12zzforeign_ctypez00
														(BgL_ctzd2idzd2_496, BgL_ctzd2expzd2_497,
															BgL_ctzd2namezd2_498));
												}
											else
												{	/* Foreign/ctype.scm 106 */
													if ((BgL_casezd2valuezd2_653 == CNST_TABLE_REF(2)))
														{	/* Foreign/ctype.scm 106 */
															return
																((obj_t)
																BGl_declarezd2czd2functionz12z12zzforeign_ctypez00
																(BgL_ctzd2idzd2_496, BgL_ctzd2expzd2_497,
																	BgL_ctzd2namezd2_498));
														}
													else
														{	/* Foreign/ctype.scm 106 */
															bool_t BgL_test2582z00_5975;

															{	/* Foreign/ctype.scm 106 */
																bool_t BgL__ortest_1261z00_665;

																BgL__ortest_1261z00_665 =
																	(BgL_casezd2valuezd2_653 ==
																	CNST_TABLE_REF(3));
																if (BgL__ortest_1261z00_665)
																	{	/* Foreign/ctype.scm 106 */
																		BgL_test2582z00_5975 =
																			BgL__ortest_1261z00_665;
																	}
																else
																	{	/* Foreign/ctype.scm 106 */
																		BgL_test2582z00_5975 =
																			(BgL_casezd2valuezd2_653 ==
																			CNST_TABLE_REF(4));
																	}
															}
															if (BgL_test2582z00_5975)
																{	/* Foreign/ctype.scm 106 */
																	return
																		((obj_t)
																		BGl_declarezd2czd2pointerz12z12zzforeign_ctypez00
																		(BgL_ctzd2idzd2_496, BgL_ctzd2expzd2_497,
																			BgL_ctzd2namezd2_498));
																}
															else
																{	/* Foreign/ctype.scm 106 */
																	bool_t BgL_test2584z00_5983;

																	{	/* Foreign/ctype.scm 106 */
																		bool_t BgL__ortest_1262z00_664;

																		BgL__ortest_1262z00_664 =
																			(BgL_casezd2valuezd2_653 ==
																			CNST_TABLE_REF(5));
																		if (BgL__ortest_1262z00_664)
																			{	/* Foreign/ctype.scm 106 */
																				BgL_test2584z00_5983 =
																					BgL__ortest_1262z00_664;
																			}
																		else
																			{	/* Foreign/ctype.scm 106 */
																				BgL_test2584z00_5983 =
																					(BgL_casezd2valuezd2_653 ==
																					CNST_TABLE_REF(6));
																			}
																	}
																	if (BgL_test2584z00_5983)
																		{	/* Foreign/ctype.scm 106 */
																			return
																				((obj_t)
																				BGl_declarezd2czd2structz12z12zzforeign_ctypez00
																				(BgL_ctzd2idzd2_496,
																					BgL_ctzd2expzd2_497,
																					BgL_ctzd2namezd2_498));
																		}
																	else
																		{	/* Foreign/ctype.scm 106 */
																			bool_t BgL_test2586z00_5991;

																			{	/* Foreign/ctype.scm 106 */
																				bool_t BgL__ortest_1263z00_663;

																				BgL__ortest_1263z00_663 =
																					(BgL_casezd2valuezd2_653 ==
																					CNST_TABLE_REF(7));
																				if (BgL__ortest_1263z00_663)
																					{	/* Foreign/ctype.scm 106 */
																						BgL_test2586z00_5991 =
																							BgL__ortest_1263z00_663;
																					}
																				else
																					{	/* Foreign/ctype.scm 106 */
																						BgL_test2586z00_5991 =
																							(BgL_casezd2valuezd2_653 ==
																							CNST_TABLE_REF(8));
																					}
																			}
																			if (BgL_test2586z00_5991)
																				{	/* Foreign/ctype.scm 106 */
																					return
																						((obj_t)
																						BGl_declarezd2czd2structza2z12zb0zzforeign_ctypez00
																						(BgL_ctzd2idzd2_496,
																							BgL_ctzd2expzd2_497,
																							BgL_ctzd2namezd2_498));
																				}
																			else
																				{	/* Foreign/ctype.scm 106 */
																					return
																						BGl_internalzd2errorzd2zztools_errorz00
																						(BGl_string2117z00zzforeign_ctypez00,
																						BGl_string2118z00zzforeign_ctypez00,
																						BgL_ctzd2defzd2_495);
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* Foreign/ctype.scm 105 */
									return
										BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2117z00zzforeign_ctypez00,
										BGl_string2118z00zzforeign_ctypez00, BgL_ctzd2defzd2_495);
								}
						}
				}
		}

	}



/* &declare-c-type! */
	obj_t BGl_z62declarezd2czd2typez12z70zzforeign_ctypez00(obj_t BgL_envz00_3273,
		obj_t BgL_ctzd2defzd2_3274, obj_t BgL_ctzd2idzd2_3275,
		obj_t BgL_ctzd2expzd2_3276, obj_t BgL_ctzd2namezd2_3277)
	{
		{	/* Foreign/ctype.scm 97 */
			return
				BGl_declarezd2czd2typez12z12zzforeign_ctypez00(BgL_ctzd2defzd2_3274,
				BgL_ctzd2idzd2_3275, BgL_ctzd2expzd2_3276, BgL_ctzd2namezd2_3277);
		}

	}



/* declare-c-alias! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_declarezd2czd2aliasz12z12zzforeign_ctypez00(obj_t BgL_idz00_499,
		obj_t BgL_expz00_500, obj_t BgL_namez00_501)
	{
		{	/* Foreign/ctype.scm 131 */
			{	/* Foreign/ctype.scm 133 */
				BgL_typez00_bglt BgL_atypez00_666;

				BgL_atypez00_666 =
					BGl_getzd2aliasedzd2typez00zztype_typez00
					(BGl_findzd2typezd2zztype_envz00(BgL_expz00_500));
				if ((BgL_idz00_499 ==
						(((BgL_typez00_bglt) COBJECT(BgL_atypez00_666))->BgL_idz00)))
					{	/* Foreign/ctype.scm 134 */
						return BgL_atypez00_666;
					}
				else
					{	/* Foreign/ctype.scm 136 */
						BgL_typez00_bglt BgL_ctypez00_669;

						BgL_ctypez00_669 =
							BGl_declarezd2aliastypez12zc0zztype_envz00(BgL_idz00_499,
							BgL_namez00_501, CNST_TABLE_REF(9), BgL_atypez00_666);
						{	/* Foreign/ctype.scm 137 */
							BgL_caliasz00_bglt BgL_wide1266z00_672;

							BgL_wide1266z00_672 =
								((BgL_caliasz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_caliasz00_bgl))));
							{	/* Foreign/ctype.scm 137 */
								obj_t BgL_auxz00_6013;
								BgL_objectz00_bglt BgL_tmpz00_6010;

								BgL_auxz00_6013 = ((obj_t) BgL_wide1266z00_672);
								BgL_tmpz00_6010 =
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_ctypez00_669));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6010, BgL_auxz00_6013);
							}
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_ctypez00_669));
							{	/* Foreign/ctype.scm 137 */
								long BgL_arg1485z00_673;

								BgL_arg1485z00_673 =
									BGL_CLASS_NUM(BGl_caliasz00zzforeign_ctypez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_ctypez00_669)), BgL_arg1485z00_673);
							}
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ctypez00_669));
						}
						{
							BgL_caliasz00_bglt BgL_auxz00_6024;

							{
								obj_t BgL_auxz00_6025;

								{	/* Foreign/ctype.scm 137 */
									BgL_objectz00_bglt BgL_tmpz00_6026;

									BgL_tmpz00_6026 =
										((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_ctypez00_669));
									BgL_auxz00_6025 = BGL_OBJECT_WIDENING(BgL_tmpz00_6026);
								}
								BgL_auxz00_6024 = ((BgL_caliasz00_bglt) BgL_auxz00_6025);
							}
							((((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_6024))->
									BgL_arrayzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
						}
						((BgL_typez00_bglt) BgL_ctypez00_669);
						return BgL_ctypez00_669;
					}
			}
		}

	}



/* &declare-c-alias! */
	BgL_typez00_bglt BGl_z62declarezd2czd2aliasz12z70zzforeign_ctypez00(obj_t
		BgL_envz00_3278, obj_t BgL_idz00_3279, obj_t BgL_expz00_3280,
		obj_t BgL_namez00_3281)
	{
		{	/* Foreign/ctype.scm 131 */
			return
				BGl_declarezd2czd2aliasz12z12zzforeign_ctypez00(BgL_idz00_3279,
				BgL_expz00_3280, BgL_namez00_3281);
		}

	}



/* declare-c-enum! */
	BgL_typez00_bglt BGl_declarezd2czd2enumz12z12zzforeign_ctypez00(obj_t
		BgL_idz00_502, obj_t BgL_expz00_503, obj_t BgL_namez00_504)
	{
		{	/* Foreign/ctype.scm 143 */
			{	/* Foreign/ctype.scm 145 */
				obj_t BgL_cobjz00_677;

				BgL_cobjz00_677 = BGl_za2cobjza2z00zztype_cachez00;
				{	/* Foreign/ctype.scm 145 */
					obj_t BgL_objz00_678;

					BgL_objz00_678 = BGl_za2objza2z00zztype_cachez00;
					{	/* Foreign/ctype.scm 146 */
						obj_t BgL_bidz00_679;

						{	/* Foreign/ctype.scm 147 */
							obj_t BgL_arg1535z00_690;

							{	/* Foreign/ctype.scm 147 */
								obj_t BgL_arg1540z00_691;
								obj_t BgL_arg1544z00_692;

								{	/* Foreign/ctype.scm 147 */
									obj_t BgL_symbolz00_2072;

									BgL_symbolz00_2072 = CNST_TABLE_REF(10);
									{	/* Foreign/ctype.scm 147 */
										obj_t BgL_arg1455z00_2073;

										BgL_arg1455z00_2073 = SYMBOL_TO_STRING(BgL_symbolz00_2072);
										BgL_arg1540z00_691 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2073);
									}
								}
								{	/* Foreign/ctype.scm 147 */
									obj_t BgL_arg1455z00_2075;

									BgL_arg1455z00_2075 = SYMBOL_TO_STRING(BgL_idz00_502);
									BgL_arg1544z00_692 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2075);
								}
								BgL_arg1535z00_690 =
									string_append(BgL_arg1540z00_691, BgL_arg1544z00_692);
							}
							BgL_bidz00_679 = bstring_to_symbol(BgL_arg1535z00_690);
						}
						{	/* Foreign/ctype.scm 147 */
							BgL_typez00_bglt BgL_ctypez00_680;

							BgL_ctypez00_680 =
								BGl_declarezd2subtypez12zc0zztype_envz00(BgL_idz00_502,
								BgL_namez00_504, CNST_TABLE_REF(11), CNST_TABLE_REF(9));
							{	/* Foreign/ctype.scm 148 */
								BgL_typez00_bglt BgL_btypez00_681;

								{	/* Foreign/ctype.scm 149 */
									obj_t BgL_arg1516z00_689;

									BgL_arg1516z00_689 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_objz00_678)))->BgL_namez00);
									BgL_btypez00_681 =
										BGl_declarezd2subtypez12zc0zztype_envz00(BgL_bidz00_679,
										BgL_arg1516z00_689, CNST_TABLE_REF(12), CNST_TABLE_REF(13));
								}
								{	/* Foreign/ctype.scm 149 */

									BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
										(BGl_makezd2foreignzd2coercersz00zzforeign_ctypez00
										(BGl_stringzd2sanszd2z42z42zztype_toolsz00(BgL_namez00_504),
											BgL_idz00_502, BgL_bidz00_679));
									{	/* Foreign/ctype.scm 155 */
										BgL_cenumz00_bglt BgL_wide1270z00_686;

										BgL_wide1270z00_686 =
											((BgL_cenumz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_cenumz00_bgl))));
										{	/* Foreign/ctype.scm 155 */
											obj_t BgL_auxz00_6056;
											BgL_objectz00_bglt BgL_tmpz00_6053;

											BgL_auxz00_6056 = ((obj_t) BgL_wide1270z00_686);
											BgL_tmpz00_6053 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_ctypez00_680));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6053, BgL_auxz00_6056);
										}
										((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_ctypez00_680));
										{	/* Foreign/ctype.scm 155 */
											long BgL_arg1514z00_687;

											BgL_arg1514z00_687 =
												BGL_CLASS_NUM(BGl_cenumz00zzforeign_ctypez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_ctypez00_680)),
												BgL_arg1514z00_687);
										}
										((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ctypez00_680));
									}
									{
										BgL_cenumz00_bglt BgL_auxz00_6067;

										{
											obj_t BgL_auxz00_6068;

											{	/* Foreign/ctype.scm 155 */
												BgL_objectz00_bglt BgL_tmpz00_6069;

												BgL_tmpz00_6069 =
													((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_ctypez00_680));
												BgL_auxz00_6068 = BGL_OBJECT_WIDENING(BgL_tmpz00_6069);
											}
											BgL_auxz00_6067 = ((BgL_cenumz00_bglt) BgL_auxz00_6068);
										}
										((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_6067))->
												BgL_btypez00) =
											((BgL_typez00_bglt) BgL_btypez00_681), BUNSPEC);
									}
									{
										BgL_cenumz00_bglt BgL_auxz00_6075;

										{
											obj_t BgL_auxz00_6076;

											{	/* Foreign/ctype.scm 155 */
												BgL_objectz00_bglt BgL_tmpz00_6077;

												BgL_tmpz00_6077 =
													((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_ctypez00_680));
												BgL_auxz00_6076 = BGL_OBJECT_WIDENING(BgL_tmpz00_6077);
											}
											BgL_auxz00_6075 = ((BgL_cenumz00_bglt) BgL_auxz00_6076);
										}
										((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_6075))->
												BgL_literalsz00) =
											((obj_t) CDR(BgL_expz00_503)), BUNSPEC);
									}
									((BgL_typez00_bglt) BgL_ctypez00_680);
									return BgL_ctypez00_680;
								}
							}
						}
					}
				}
			}
		}

	}



/* declare-c-opaque! */
	BgL_typez00_bglt BGl_declarezd2czd2opaquez12z12zzforeign_ctypez00(obj_t
		BgL_idz00_505, obj_t BgL_expz00_506, obj_t BgL_namez00_507)
	{
		{	/* Foreign/ctype.scm 161 */
			{	/* Foreign/ctype.scm 163 */
				obj_t BgL_cobjz00_693;

				BgL_cobjz00_693 = BGl_za2cobjza2z00zztype_cachez00;
				{	/* Foreign/ctype.scm 163 */
					obj_t BgL_objz00_694;

					BgL_objz00_694 = BGl_za2objza2z00zztype_cachez00;
					{	/* Foreign/ctype.scm 164 */
						obj_t BgL_bidz00_695;

						{	/* Foreign/ctype.scm 165 */
							obj_t BgL_arg1561z00_706;

							{	/* Foreign/ctype.scm 165 */
								obj_t BgL_arg1564z00_707;
								obj_t BgL_arg1565z00_708;

								{	/* Foreign/ctype.scm 165 */
									obj_t BgL_symbolz00_2085;

									BgL_symbolz00_2085 = CNST_TABLE_REF(10);
									{	/* Foreign/ctype.scm 165 */
										obj_t BgL_arg1455z00_2086;

										BgL_arg1455z00_2086 = SYMBOL_TO_STRING(BgL_symbolz00_2085);
										BgL_arg1564z00_707 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2086);
									}
								}
								{	/* Foreign/ctype.scm 165 */
									obj_t BgL_arg1455z00_2088;

									BgL_arg1455z00_2088 = SYMBOL_TO_STRING(BgL_idz00_505);
									BgL_arg1565z00_708 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2088);
								}
								BgL_arg1561z00_706 =
									string_append(BgL_arg1564z00_707, BgL_arg1565z00_708);
							}
							BgL_bidz00_695 = bstring_to_symbol(BgL_arg1561z00_706);
						}
						{	/* Foreign/ctype.scm 165 */
							BgL_typez00_bglt BgL_ctypez00_696;

							BgL_ctypez00_696 =
								BGl_declarezd2subtypez12zc0zztype_envz00(BgL_idz00_505,
								BgL_namez00_507, CNST_TABLE_REF(11), CNST_TABLE_REF(9));
							{	/* Foreign/ctype.scm 166 */
								BgL_typez00_bglt BgL_btypez00_697;

								{	/* Foreign/ctype.scm 167 */
									obj_t BgL_arg1559z00_705;

									BgL_arg1559z00_705 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_objz00_694)))->BgL_namez00);
									BgL_btypez00_697 =
										BGl_declarezd2subtypez12zc0zztype_envz00(BgL_bidz00_695,
										BgL_arg1559z00_705, CNST_TABLE_REF(12), CNST_TABLE_REF(13));
								}
								{	/* Foreign/ctype.scm 167 */

									BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
										(BGl_makezd2foreignzd2coercersz00zzforeign_ctypez00
										(BGl_stringzd2sanszd2z42z42zztype_toolsz00(BgL_namez00_507),
											BgL_idz00_505, BgL_bidz00_695));
									{	/* Foreign/ctype.scm 173 */
										BgL_copaquez00_bglt BgL_wide1274z00_702;

										BgL_wide1274z00_702 =
											((BgL_copaquez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_copaquez00_bgl))));
										{	/* Foreign/ctype.scm 173 */
											obj_t BgL_auxz00_6107;
											BgL_objectz00_bglt BgL_tmpz00_6104;

											BgL_auxz00_6107 = ((obj_t) BgL_wide1274z00_702);
											BgL_tmpz00_6104 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_ctypez00_696));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6104, BgL_auxz00_6107);
										}
										((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_ctypez00_696));
										{	/* Foreign/ctype.scm 173 */
											long BgL_arg1553z00_703;

											BgL_arg1553z00_703 =
												BGL_CLASS_NUM(BGl_copaquez00zzforeign_ctypez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_ctypez00_696)),
												BgL_arg1553z00_703);
										}
										((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ctypez00_696));
									}
									{
										BgL_copaquez00_bglt BgL_auxz00_6118;

										{
											obj_t BgL_auxz00_6119;

											{	/* Foreign/ctype.scm 173 */
												BgL_objectz00_bglt BgL_tmpz00_6120;

												BgL_tmpz00_6120 =
													((BgL_objectz00_bglt)
													((BgL_typez00_bglt) BgL_ctypez00_696));
												BgL_auxz00_6119 = BGL_OBJECT_WIDENING(BgL_tmpz00_6120);
											}
											BgL_auxz00_6118 = ((BgL_copaquez00_bglt) BgL_auxz00_6119);
										}
										((((BgL_copaquez00_bglt) COBJECT(BgL_auxz00_6118))->
												BgL_btypez00) =
											((BgL_typez00_bglt) BgL_btypez00_697), BUNSPEC);
									}
									((BgL_typez00_bglt) BgL_ctypez00_696);
									return BgL_ctypez00_696;
								}
							}
						}
					}
				}
			}
		}

	}



/* declare-c-function! */
	BgL_typez00_bglt BGl_declarezd2czd2functionz12z12zzforeign_ctypez00(obj_t
		BgL_idz00_508, obj_t BgL_expz00_509, obj_t BgL_namez00_510)
	{
		{	/* Foreign/ctype.scm 179 */
			{	/* Foreign/ctype.scm 181 */
				obj_t BgL_cobjz00_709;

				BgL_cobjz00_709 = BGl_za2cobjza2z00zztype_cachez00;
				{	/* Foreign/ctype.scm 181 */
					obj_t BgL_objz00_710;

					BgL_objz00_710 = BGl_za2objza2z00zztype_cachez00;
					{	/* Foreign/ctype.scm 182 */
						obj_t BgL_bidz00_711;

						{	/* Foreign/ctype.scm 183 */
							obj_t BgL_arg1594z00_741;

							{	/* Foreign/ctype.scm 183 */
								obj_t BgL_arg1595z00_742;
								obj_t BgL_arg1602z00_743;

								{	/* Foreign/ctype.scm 183 */
									obj_t BgL_symbolz00_2096;

									BgL_symbolz00_2096 = CNST_TABLE_REF(10);
									{	/* Foreign/ctype.scm 183 */
										obj_t BgL_arg1455z00_2097;

										BgL_arg1455z00_2097 = SYMBOL_TO_STRING(BgL_symbolz00_2096);
										BgL_arg1595z00_742 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2097);
									}
								}
								{	/* Foreign/ctype.scm 183 */
									obj_t BgL_arg1455z00_2099;

									BgL_arg1455z00_2099 = SYMBOL_TO_STRING(BgL_idz00_508);
									BgL_arg1602z00_743 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2099);
								}
								BgL_arg1594z00_741 =
									string_append(BgL_arg1595z00_742, BgL_arg1602z00_743);
							}
							BgL_bidz00_711 = bstring_to_symbol(BgL_arg1594z00_741);
						}
						{	/* Foreign/ctype.scm 183 */
							BgL_typez00_bglt BgL_ctypez00_712;

							BgL_ctypez00_712 =
								BGl_declarezd2subtypez12zc0zztype_envz00(BgL_idz00_508,
								BgL_namez00_510, CNST_TABLE_REF(11), CNST_TABLE_REF(9));
							{	/* Foreign/ctype.scm 184 */
								BgL_typez00_bglt BgL_btypez00_713;

								{	/* Foreign/ctype.scm 185 */
									obj_t BgL_arg1593z00_740;

									BgL_arg1593z00_740 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_objz00_710)))->BgL_namez00);
									BgL_btypez00_713 =
										BGl_declarezd2subtypez12zc0zztype_envz00(BgL_bidz00_711,
										BgL_arg1593z00_740, CNST_TABLE_REF(12), CNST_TABLE_REF(13));
								}
								{	/* Foreign/ctype.scm 185 */
									long BgL_arityz00_714;

									BgL_arityz00_714 =
										BGl_foreignzd2arityzd2zztools_argsz00(CAR(CDR(CDR
												(BgL_expz00_509))));
									{	/* Foreign/ctype.scm 186 */
										obj_t BgL_treszd2idzd2_715;

										BgL_treszd2idzd2_715 = CAR(CDR(BgL_expz00_509));
										{	/* Foreign/ctype.scm 187 */
											obj_t BgL_targszd2idzd2_716;

											BgL_targszd2idzd2_716 =
												BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(CAR(CDR
													(CDR(BgL_expz00_509))));
											{	/* Foreign/ctype.scm 188 */
												obj_t BgL_locz00_717;

												BgL_locz00_717 =
													BGl_findzd2locationzd2zztools_locationz00
													(BgL_expz00_509);
												{	/* Foreign/ctype.scm 189 */

													BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
														(BGl_makezd2foreignzd2coercersz00zzforeign_ctypez00
														(BGl_stringzd2sanszd2z42z42zztype_toolsz00
															(BgL_namez00_510), BgL_idz00_508,
															BgL_bidz00_711));
													{	/* Foreign/ctype.scm 195 */
														BgL_cfunctionz00_bglt BgL_wide1278z00_722;

														BgL_wide1278z00_722 =
															((BgL_cfunctionz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_cfunctionz00_bgl))));
														{	/* Foreign/ctype.scm 195 */
															obj_t BgL_auxz00_6160;
															BgL_objectz00_bglt BgL_tmpz00_6157;

															BgL_auxz00_6160 = ((obj_t) BgL_wide1278z00_722);
															BgL_tmpz00_6157 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_ctypez00_712));
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6157,
																BgL_auxz00_6160);
														}
														((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_ctypez00_712));
														{	/* Foreign/ctype.scm 195 */
															long BgL_arg1575z00_723;

															BgL_arg1575z00_723 =
																BGL_CLASS_NUM
																(BGl_cfunctionz00zzforeign_ctypez00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																		(BgL_typez00_bglt) BgL_ctypez00_712)),
																BgL_arg1575z00_723);
														}
														((BgL_typez00_bglt)
															((BgL_typez00_bglt) BgL_ctypez00_712));
													}
													{
														BgL_cfunctionz00_bglt BgL_auxz00_6171;

														{
															obj_t BgL_auxz00_6172;

															{	/* Foreign/ctype.scm 196 */
																BgL_objectz00_bglt BgL_tmpz00_6173;

																BgL_tmpz00_6173 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_ctypez00_712));
																BgL_auxz00_6172 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6173);
															}
															BgL_auxz00_6171 =
																((BgL_cfunctionz00_bglt) BgL_auxz00_6172);
														}
														((((BgL_cfunctionz00_bglt)
																	COBJECT(BgL_auxz00_6171))->BgL_btypez00) =
															((BgL_typez00_bglt) BgL_btypez00_713), BUNSPEC);
													}
													{
														BgL_cfunctionz00_bglt BgL_auxz00_6179;

														{
															obj_t BgL_auxz00_6180;

															{	/* Foreign/ctype.scm 197 */
																BgL_objectz00_bglt BgL_tmpz00_6181;

																BgL_tmpz00_6181 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_ctypez00_712));
																BgL_auxz00_6180 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6181);
															}
															BgL_auxz00_6179 =
																((BgL_cfunctionz00_bglt) BgL_auxz00_6180);
														}
														((((BgL_cfunctionz00_bglt)
																	COBJECT(BgL_auxz00_6179))->BgL_arityz00) =
															((long) BgL_arityz00_714), BUNSPEC);
													}
													{
														BgL_cfunctionz00_bglt BgL_auxz00_6187;

														{
															obj_t BgL_auxz00_6188;

															{	/* Foreign/ctype.scm 198 */
																BgL_objectz00_bglt BgL_tmpz00_6189;

																BgL_tmpz00_6189 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_ctypez00_712));
																BgL_auxz00_6188 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6189);
															}
															BgL_auxz00_6187 =
																((BgL_cfunctionz00_bglt) BgL_auxz00_6188);
														}
														((((BgL_cfunctionz00_bglt)
																	COBJECT(BgL_auxz00_6187))->
																BgL_typezd2reszd2) =
															((BgL_typez00_bglt)
																BGl_usezd2typez12zc0zztype_envz00
																(BgL_treszd2idzd2_715, BgL_locz00_717)),
															BUNSPEC);
													}
													{
														obj_t BgL_auxz00_6203;
														BgL_cfunctionz00_bglt BgL_auxz00_6196;

														if (NULLP(BgL_targszd2idzd2_716))
															{	/* Foreign/ctype.scm 199 */
																BgL_auxz00_6203 = BNIL;
															}
														else
															{	/* Foreign/ctype.scm 199 */
																obj_t BgL_head1337z00_727;

																BgL_head1337z00_727 =
																	MAKE_YOUNG_PAIR(BNIL, BNIL);
																{
																	obj_t BgL_l1335z00_729;
																	obj_t BgL_tail1338z00_730;

																	BgL_l1335z00_729 = BgL_targszd2idzd2_716;
																	BgL_tail1338z00_730 = BgL_head1337z00_727;
																BgL_zc3z04anonymousza31577ze3z87_731:
																	if (NULLP(BgL_l1335z00_729))
																		{	/* Foreign/ctype.scm 199 */
																			BgL_auxz00_6203 =
																				CDR(BgL_head1337z00_727);
																		}
																	else
																		{	/* Foreign/ctype.scm 199 */
																			obj_t BgL_newtail1339z00_733;

																			{	/* Foreign/ctype.scm 199 */
																				BgL_typez00_bglt BgL_arg1585z00_735;

																				{	/* Foreign/ctype.scm 199 */
																					obj_t BgL_az00_736;

																					BgL_az00_736 =
																						CAR(((obj_t) BgL_l1335z00_729));
																					BgL_arg1585z00_735 =
																						BGl_usezd2typez12zc0zztype_envz00
																						(BgL_az00_736, BgL_locz00_717);
																				}
																				BgL_newtail1339z00_733 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg1585z00_735), BNIL);
																			}
																			SET_CDR(BgL_tail1338z00_730,
																				BgL_newtail1339z00_733);
																			{	/* Foreign/ctype.scm 199 */
																				obj_t BgL_arg1584z00_734;

																				BgL_arg1584z00_734 =
																					CDR(((obj_t) BgL_l1335z00_729));
																				{
																					obj_t BgL_tail1338z00_6219;
																					obj_t BgL_l1335z00_6218;

																					BgL_l1335z00_6218 =
																						BgL_arg1584z00_734;
																					BgL_tail1338z00_6219 =
																						BgL_newtail1339z00_733;
																					BgL_tail1338z00_730 =
																						BgL_tail1338z00_6219;
																					BgL_l1335z00_729 = BgL_l1335z00_6218;
																					goto
																						BgL_zc3z04anonymousza31577ze3z87_731;
																				}
																			}
																		}
																}
															}
														{
															obj_t BgL_auxz00_6197;

															{	/* Foreign/ctype.scm 199 */
																BgL_objectz00_bglt BgL_tmpz00_6198;

																BgL_tmpz00_6198 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_ctypez00_712));
																BgL_auxz00_6197 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6198);
															}
															BgL_auxz00_6196 =
																((BgL_cfunctionz00_bglt) BgL_auxz00_6197);
														}
														((((BgL_cfunctionz00_bglt)
																	COBJECT(BgL_auxz00_6196))->
																BgL_typezd2argszd2) =
															((obj_t) BgL_auxz00_6203), BUNSPEC);
													}
													((BgL_typez00_bglt) BgL_ctypez00_712);
													return BgL_ctypez00_712;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* declare-c-pointer! */
	BgL_typez00_bglt BGl_declarezd2czd2pointerz12z12zzforeign_ctypez00(obj_t
		BgL_idz00_511, obj_t BgL_expz00_512, obj_t BgL_namez00_513)
	{
		{	/* Foreign/ctype.scm 205 */
			{	/* Foreign/ctype.scm 207 */
				BgL_typez00_bglt BgL_pointedz00_744;

				BgL_pointedz00_744 =
					BGl_findzd2typezd2zztype_envz00(CAR(CDR(BgL_expz00_512)));
				{	/* Foreign/ctype.scm 207 */
					obj_t BgL_pointerz00_745;

					BgL_pointerz00_745 =
						(((BgL_typez00_bglt) COBJECT(BgL_pointedz00_744))->
						BgL_pointedzd2tozd2byz00);
					{	/* Foreign/ctype.scm 208 */

						{	/* Foreign/ctype.scm 211 */
							bool_t BgL_test2591z00_6226;

							{	/* Foreign/ctype.scm 211 */
								obj_t BgL_classz00_2135;

								BgL_classz00_2135 = BGl_typez00zztype_typez00;
								if (BGL_OBJECTP(BgL_pointerz00_745))
									{	/* Foreign/ctype.scm 211 */
										BgL_objectz00_bglt BgL_arg1807z00_2137;

										BgL_arg1807z00_2137 =
											(BgL_objectz00_bglt) (BgL_pointerz00_745);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Foreign/ctype.scm 211 */
												long BgL_idxz00_2143;

												BgL_idxz00_2143 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2137);
												BgL_test2591z00_6226 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2143 + 1L)) == BgL_classz00_2135);
											}
										else
											{	/* Foreign/ctype.scm 211 */
												bool_t BgL_res2113z00_2168;

												{	/* Foreign/ctype.scm 211 */
													obj_t BgL_oclassz00_2151;

													{	/* Foreign/ctype.scm 211 */
														obj_t BgL_arg1815z00_2159;
														long BgL_arg1816z00_2160;

														BgL_arg1815z00_2159 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Foreign/ctype.scm 211 */
															long BgL_arg1817z00_2161;

															BgL_arg1817z00_2161 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2137);
															BgL_arg1816z00_2160 =
																(BgL_arg1817z00_2161 - OBJECT_TYPE);
														}
														BgL_oclassz00_2151 =
															VECTOR_REF(BgL_arg1815z00_2159,
															BgL_arg1816z00_2160);
													}
													{	/* Foreign/ctype.scm 211 */
														bool_t BgL__ortest_1115z00_2152;

														BgL__ortest_1115z00_2152 =
															(BgL_classz00_2135 == BgL_oclassz00_2151);
														if (BgL__ortest_1115z00_2152)
															{	/* Foreign/ctype.scm 211 */
																BgL_res2113z00_2168 = BgL__ortest_1115z00_2152;
															}
														else
															{	/* Foreign/ctype.scm 211 */
																long BgL_odepthz00_2153;

																{	/* Foreign/ctype.scm 211 */
																	obj_t BgL_arg1804z00_2154;

																	BgL_arg1804z00_2154 = (BgL_oclassz00_2151);
																	BgL_odepthz00_2153 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2154);
																}
																if ((1L < BgL_odepthz00_2153))
																	{	/* Foreign/ctype.scm 211 */
																		obj_t BgL_arg1802z00_2156;

																		{	/* Foreign/ctype.scm 211 */
																			obj_t BgL_arg1803z00_2157;

																			BgL_arg1803z00_2157 =
																				(BgL_oclassz00_2151);
																			BgL_arg1802z00_2156 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2157, 1L);
																		}
																		BgL_res2113z00_2168 =
																			(BgL_arg1802z00_2156 ==
																			BgL_classz00_2135);
																	}
																else
																	{	/* Foreign/ctype.scm 211 */
																		BgL_res2113z00_2168 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2591z00_6226 = BgL_res2113z00_2168;
											}
									}
								else
									{	/* Foreign/ctype.scm 211 */
										BgL_test2591z00_6226 = ((bool_t) 0);
									}
							}
							if (BgL_test2591z00_6226)
								{	/* Foreign/ctype.scm 212 */
									BgL_typez00_bglt BgL_aliasz00_747;

									{	/* Foreign/ctype.scm 213 */
										obj_t BgL_arg1615z00_757;
										obj_t BgL_arg1616z00_758;

										BgL_arg1615z00_757 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_pointerz00_745)))->BgL_idz00);
										BgL_arg1616z00_758 =
											BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00
											(BgL_pointedz00_744);
										BgL_aliasz00_747 =
											BGl_declarezd2czd2aliasz12z12zzforeign_ctypez00
											(BgL_idz00_511, BgL_arg1615z00_757, BgL_arg1616z00_758);
									}
									{	/* Foreign/ctype.scm 215 */
										bool_t BgL_test2596z00_6253;

										if ((CAR(BgL_expz00_512) == CNST_TABLE_REF(4)))
											{	/* Foreign/ctype.scm 215 */
												bool_t BgL_test2598z00_6258;

												{
													BgL_cptrz00_bglt BgL_auxz00_6259;

													{
														obj_t BgL_auxz00_6260;

														{	/* Foreign/ctype.scm 215 */
															BgL_objectz00_bglt BgL_tmpz00_6261;

															BgL_tmpz00_6261 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_pointerz00_745));
															BgL_auxz00_6260 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_6261);
														}
														BgL_auxz00_6259 =
															((BgL_cptrz00_bglt) BgL_auxz00_6260);
													}
													BgL_test2598z00_6258 =
														(((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_6259))->
														BgL_arrayzf3zf3);
												}
												if (BgL_test2598z00_6258)
													{	/* Foreign/ctype.scm 215 */
														BgL_test2596z00_6253 = ((bool_t) 0);
													}
												else
													{	/* Foreign/ctype.scm 215 */
														BgL_test2596z00_6253 = ((bool_t) 1);
													}
											}
										else
											{	/* Foreign/ctype.scm 215 */
												BgL_test2596z00_6253 = ((bool_t) 0);
											}
										if (BgL_test2596z00_6253)
											{	/* Foreign/ctype.scm 215 */
												((((BgL_typez00_bglt) COBJECT(BgL_aliasz00_747))->
														BgL_siza7eza7) =
													((obj_t)
														BGl_stringzd2sanszd2z42z42zztype_toolsz00
														(BgL_namez00_513)), BUNSPEC);
												{
													BgL_caliasz00_bglt BgL_auxz00_6269;

													{
														obj_t BgL_auxz00_6270;

														{	/* Foreign/ctype.scm 218 */
															BgL_objectz00_bglt BgL_tmpz00_6271;

															BgL_tmpz00_6271 =
																((BgL_objectz00_bglt)
																((BgL_typez00_bglt) BgL_aliasz00_747));
															BgL_auxz00_6270 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_6271);
														}
														BgL_auxz00_6269 =
															((BgL_caliasz00_bglt) BgL_auxz00_6270);
													}
													((((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_6269))->
															BgL_arrayzf3zf3) =
														((bool_t) ((bool_t) 1)), BUNSPEC);
												}
											}
										else
											{	/* Foreign/ctype.scm 215 */
												BFALSE;
											}
									}
									return BgL_aliasz00_747;
								}
							else
								{	/* Foreign/ctype.scm 222 */
									obj_t BgL_cobjz00_759;

									BgL_cobjz00_759 = BGl_za2cobjza2z00zztype_cachez00;
									{	/* Foreign/ctype.scm 222 */
										obj_t BgL_objz00_760;

										BgL_objz00_760 = BGl_za2objza2z00zztype_cachez00;
										{	/* Foreign/ctype.scm 223 */
											obj_t BgL_nobjz00_761;

											BgL_nobjz00_761 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_objz00_760)))->BgL_namez00);
											{	/* Foreign/ctype.scm 224 */
												obj_t BgL_bidz00_762;

												{	/* Foreign/ctype.scm 225 */
													obj_t BgL_arg1642z00_775;

													{	/* Foreign/ctype.scm 225 */
														obj_t BgL_arg1646z00_776;
														obj_t BgL_arg1650z00_777;

														{	/* Foreign/ctype.scm 225 */
															obj_t BgL_symbolz00_2178;

															BgL_symbolz00_2178 = CNST_TABLE_REF(10);
															{	/* Foreign/ctype.scm 225 */
																obj_t BgL_arg1455z00_2179;

																BgL_arg1455z00_2179 =
																	SYMBOL_TO_STRING(BgL_symbolz00_2178);
																BgL_arg1646z00_776 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_2179);
															}
														}
														{	/* Foreign/ctype.scm 225 */
															obj_t BgL_arg1455z00_2181;

															BgL_arg1455z00_2181 =
																SYMBOL_TO_STRING(BgL_idz00_511);
															BgL_arg1650z00_777 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_2181);
														}
														BgL_arg1642z00_775 =
															string_append(BgL_arg1646z00_776,
															BgL_arg1650z00_777);
													}
													BgL_bidz00_762 =
														bstring_to_symbol(BgL_arg1642z00_775);
												}
												{	/* Foreign/ctype.scm 225 */
													obj_t BgL_tnamez00_763;

													BgL_tnamez00_763 =
														BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00
														(BgL_pointedz00_744);
													{	/* Foreign/ctype.scm 226 */
														BgL_typez00_bglt BgL_ctypez00_764;

														BgL_ctypez00_764 =
															BGl_declarezd2subtypez12zc0zztype_envz00
															(BgL_idz00_511, BgL_tnamez00_763,
															CNST_TABLE_REF(11), CNST_TABLE_REF(9));
														{	/* Foreign/ctype.scm 227 */
															BgL_typez00_bglt BgL_btypez00_765;

															BgL_btypez00_765 =
																BGl_declarezd2subtypez12zc0zztype_envz00
																(BgL_bidz00_762, BgL_nobjz00_761,
																CNST_TABLE_REF(12), CNST_TABLE_REF(13));
															{	/* Foreign/ctype.scm 228 */

																((((BgL_typez00_bglt)
																			COBJECT(BgL_ctypez00_764))->
																		BgL_siza7eza7) =
																	((obj_t)
																		BGl_stringzd2sanszd2z42z42zztype_toolsz00
																		(BgL_namez00_513)), BUNSPEC);
																BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																	(BGl_makezd2foreignzd2coercersz00zzforeign_ctypez00
																	(BGl_stringzd2sanszd2z42z42zztype_toolsz00
																		(BgL_namez00_513), BgL_idz00_511,
																		BgL_bidz00_762));
																((((BgL_typez00_bglt)
																			COBJECT(BgL_pointedz00_744))->
																		BgL_pointedzd2tozd2byz00) =
																	((obj_t) ((obj_t) BgL_ctypez00_764)),
																	BUNSPEC);
																{	/* Foreign/ctype.scm 238 */
																	BgL_cptrz00_bglt BgL_wide1282z00_771;

																	BgL_wide1282z00_771 =
																		((BgL_cptrz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_cptrz00_bgl))));
																	{	/* Foreign/ctype.scm 238 */
																		obj_t BgL_auxz00_6304;
																		BgL_objectz00_bglt BgL_tmpz00_6301;

																		BgL_auxz00_6304 =
																			((obj_t) BgL_wide1282z00_771);
																		BgL_tmpz00_6301 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_ctypez00_764));
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6301,
																			BgL_auxz00_6304);
																	}
																	((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_ctypez00_764));
																	{	/* Foreign/ctype.scm 238 */
																		long BgL_arg1629z00_772;

																		BgL_arg1629z00_772 =
																			BGL_CLASS_NUM
																			(BGl_cptrz00zzforeign_ctypez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt) ((BgL_typez00_bglt)
																					BgL_ctypez00_764)),
																			BgL_arg1629z00_772);
																	}
																	((BgL_typez00_bglt)
																		((BgL_typez00_bglt) BgL_ctypez00_764));
																}
																{
																	BgL_cptrz00_bglt BgL_auxz00_6315;

																	{
																		obj_t BgL_auxz00_6316;

																		{	/* Foreign/ctype.scm 239 */
																			BgL_objectz00_bglt BgL_tmpz00_6317;

																			BgL_tmpz00_6317 =
																				((BgL_objectz00_bglt)
																				((BgL_typez00_bglt) BgL_ctypez00_764));
																			BgL_auxz00_6316 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_6317);
																		}
																		BgL_auxz00_6315 =
																			((BgL_cptrz00_bglt) BgL_auxz00_6316);
																	}
																	((((BgL_cptrz00_bglt)
																				COBJECT(BgL_auxz00_6315))->
																			BgL_btypez00) =
																		((BgL_typez00_bglt) BgL_btypez00_765),
																		BUNSPEC);
																}
																{
																	BgL_cptrz00_bglt BgL_auxz00_6323;

																	{
																		obj_t BgL_auxz00_6324;

																		{	/* Foreign/ctype.scm 240 */
																			BgL_objectz00_bglt BgL_tmpz00_6325;

																			BgL_tmpz00_6325 =
																				((BgL_objectz00_bglt)
																				((BgL_typez00_bglt) BgL_ctypez00_764));
																			BgL_auxz00_6324 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_6325);
																		}
																		BgL_auxz00_6323 =
																			((BgL_cptrz00_bglt) BgL_auxz00_6324);
																	}
																	((((BgL_cptrz00_bglt)
																				COBJECT(BgL_auxz00_6323))->
																			BgL_pointzd2tozd2) =
																		((BgL_typez00_bglt) BgL_pointedz00_744),
																		BUNSPEC);
																}
																{
																	BgL_cptrz00_bglt BgL_auxz00_6331;

																	{
																		obj_t BgL_auxz00_6332;

																		{	/* Foreign/ctype.scm 241 */
																			BgL_objectz00_bglt BgL_tmpz00_6333;

																			BgL_tmpz00_6333 =
																				((BgL_objectz00_bglt)
																				((BgL_typez00_bglt) BgL_ctypez00_764));
																			BgL_auxz00_6332 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_6333);
																		}
																		BgL_auxz00_6331 =
																			((BgL_cptrz00_bglt) BgL_auxz00_6332);
																	}
																	((((BgL_cptrz00_bglt)
																				COBJECT(BgL_auxz00_6331))->
																			BgL_arrayzf3zf3) =
																		((bool_t) (CAR(BgL_expz00_512) ==
																				CNST_TABLE_REF(4))), BUNSPEC);
																}
																((BgL_typez00_bglt) BgL_ctypez00_764);
																return BgL_ctypez00_764;
															}
														}
													}
												}
											}
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* declare-c-struct! */
	BgL_typez00_bglt BGl_declarezd2czd2structz12z12zzforeign_ctypez00(obj_t
		BgL_idz00_514, obj_t BgL_expz00_515, obj_t BgL_namez00_516)
	{
		{	/* Foreign/ctype.scm 247 */
			{	/* Foreign/ctype.scm 249 */
				obj_t BgL_cobjz00_779;

				BgL_cobjz00_779 = BGl_za2cobjza2z00zztype_cachez00;
				{	/* Foreign/ctype.scm 249 */
					obj_t BgL_objz00_780;

					BgL_objz00_780 = BGl_za2objza2z00zztype_cachez00;
					{	/* Foreign/ctype.scm 250 */
						obj_t BgL_bidz00_781;

						{	/* Foreign/ctype.scm 251 */
							obj_t BgL_arg1692z00_797;

							{	/* Foreign/ctype.scm 251 */
								obj_t BgL_arg1699z00_798;
								obj_t BgL_arg1700z00_799;

								{	/* Foreign/ctype.scm 251 */
									obj_t BgL_symbolz00_2193;

									BgL_symbolz00_2193 = CNST_TABLE_REF(10);
									{	/* Foreign/ctype.scm 251 */
										obj_t BgL_arg1455z00_2194;

										BgL_arg1455z00_2194 = SYMBOL_TO_STRING(BgL_symbolz00_2193);
										BgL_arg1699z00_798 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_2194);
									}
								}
								{	/* Foreign/ctype.scm 251 */
									obj_t BgL_arg1455z00_2196;

									BgL_arg1455z00_2196 = SYMBOL_TO_STRING(BgL_idz00_514);
									BgL_arg1700z00_799 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2196);
								}
								BgL_arg1692z00_797 =
									string_append(BgL_arg1699z00_798, BgL_arg1700z00_799);
							}
							BgL_bidz00_781 = bstring_to_symbol(BgL_arg1692z00_797);
						}
						{	/* Foreign/ctype.scm 251 */
							BgL_typez00_bglt BgL_ctypez00_782;

							BgL_ctypez00_782 =
								BGl_declarezd2subtypez12zc0zztype_envz00(BgL_idz00_514,
								BgL_namez00_516, CNST_TABLE_REF(11), CNST_TABLE_REF(9));
							{	/* Foreign/ctype.scm 252 */

								((((BgL_typez00_bglt) COBJECT(BgL_ctypez00_782))->
										BgL_siza7eza7) = ((obj_t) BgL_namez00_516), BUNSPEC);
								{	/* Foreign/ctype.scm 255 */
									BgL_cstructz00_bglt BgL_wide1286z00_785;

									BgL_wide1286z00_785 =
										((BgL_cstructz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_cstructz00_bgl))));
									{	/* Foreign/ctype.scm 255 */
										obj_t BgL_auxz00_6358;
										BgL_objectz00_bglt BgL_tmpz00_6355;

										BgL_auxz00_6358 = ((obj_t) BgL_wide1286z00_785);
										BgL_tmpz00_6355 =
											((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_ctypez00_782));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6355, BgL_auxz00_6358);
									}
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_ctypez00_782));
									{	/* Foreign/ctype.scm 255 */
										long BgL_arg1654z00_786;

										BgL_arg1654z00_786 =
											BGL_CLASS_NUM(BGl_cstructz00zzforeign_ctypez00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_ctypez00_782)),
											BgL_arg1654z00_786);
									}
									((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_ctypez00_782));
								}
								{
									BgL_cstructz00_bglt BgL_auxz00_6369;

									{
										obj_t BgL_auxz00_6370;

										{	/* Foreign/ctype.scm 256 */
											BgL_objectz00_bglt BgL_tmpz00_6371;

											BgL_tmpz00_6371 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_ctypez00_782));
											BgL_auxz00_6370 = BGL_OBJECT_WIDENING(BgL_tmpz00_6371);
										}
										BgL_auxz00_6369 = ((BgL_cstructz00_bglt) BgL_auxz00_6370);
									}
									((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_6369))->
											BgL_structzf3zf3) =
										((bool_t) (CAR(BgL_expz00_515) == CNST_TABLE_REF(5))),
										BUNSPEC);
								}
								{
									BgL_cstructz00_bglt BgL_auxz00_6380;

									{
										obj_t BgL_auxz00_6381;

										{	/* Foreign/ctype.scm 257 */
											BgL_objectz00_bglt BgL_tmpz00_6382;

											BgL_tmpz00_6382 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_ctypez00_782));
											BgL_auxz00_6381 = BGL_OBJECT_WIDENING(BgL_tmpz00_6382);
										}
										BgL_auxz00_6380 = ((BgL_cstructz00_bglt) BgL_auxz00_6381);
									}
									((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_6380))->
											BgL_fieldsz00) = ((obj_t) CDR(BgL_expz00_515)), BUNSPEC);
								}
								{
									BgL_cstructz00_bglt BgL_auxz00_6389;

									{
										obj_t BgL_auxz00_6390;

										{	/* Foreign/ctype.scm 257 */
											BgL_objectz00_bglt BgL_tmpz00_6391;

											BgL_tmpz00_6391 =
												((BgL_objectz00_bglt)
												((BgL_typez00_bglt) BgL_ctypez00_782));
											BgL_auxz00_6390 = BGL_OBJECT_WIDENING(BgL_tmpz00_6391);
										}
										BgL_auxz00_6389 = ((BgL_cstructz00_bglt) BgL_auxz00_6390);
									}
									((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_6389))->
											BgL_cstructza2za2) = ((obj_t) BFALSE), BUNSPEC);
								}
								((BgL_typez00_bglt) BgL_ctypez00_782);
								{	/* Foreign/ctype.scm 259 */
									obj_t BgL_ptrzd2idzd2_789;

									{	/* Foreign/ctype.scm 259 */
										obj_t BgL_arg1688z00_794;

										{	/* Foreign/ctype.scm 259 */
											obj_t BgL_arg1689z00_795;
											obj_t BgL_arg1691z00_796;

											{	/* Foreign/ctype.scm 259 */
												obj_t BgL_arg1455z00_2209;

												BgL_arg1455z00_2209 = SYMBOL_TO_STRING(BgL_idz00_514);
												BgL_arg1689z00_795 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2209);
											}
											{	/* Foreign/ctype.scm 259 */
												obj_t BgL_symbolz00_2210;

												BgL_symbolz00_2210 = CNST_TABLE_REF(14);
												{	/* Foreign/ctype.scm 259 */
													obj_t BgL_arg1455z00_2211;

													BgL_arg1455z00_2211 =
														SYMBOL_TO_STRING(BgL_symbolz00_2210);
													BgL_arg1691z00_796 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_2211);
											}}
											BgL_arg1688z00_794 =
												string_append(BgL_arg1689z00_795, BgL_arg1691z00_796);
										}
										BgL_ptrzd2idzd2_789 = bstring_to_symbol(BgL_arg1688z00_794);
									}
									{	/* Foreign/ctype.scm 261 */
										obj_t BgL_arg1663z00_790;
										obj_t BgL_arg1675z00_791;

										{	/* Foreign/ctype.scm 261 */
											obj_t BgL_arg1678z00_792;
											obj_t BgL_arg1681z00_793;

											BgL_arg1678z00_792 = CAR(BgL_expz00_515);
											BgL_arg1681z00_793 = MAKE_YOUNG_PAIR(BgL_idz00_514, BNIL);
											BgL_arg1663z00_790 =
												MAKE_YOUNG_PAIR(BgL_arg1678z00_792, BgL_arg1681z00_793);
										}
										BgL_arg1675z00_791 =
											BGl_makezd2pointerzd2tozd2namezd2zztype_toolsz00
											(BgL_ctypez00_782);
										BGL_TAIL return
											BGl_declarezd2czd2structza2z12zb0zzforeign_ctypez00
											(BgL_ptrzd2idzd2_789, BgL_arg1663z00_790,
											BgL_arg1675z00_791);
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* declare-c-struct*! */
	BgL_typez00_bglt BGl_declarezd2czd2structza2z12zb0zzforeign_ctypez00(obj_t
		BgL_idz00_517, obj_t BgL_expz00_518, obj_t BgL_namez00_519)
	{
		{	/* Foreign/ctype.scm 267 */
			{	/* Foreign/ctype.scm 269 */
				BgL_typez00_bglt BgL_structz00_800;

				BgL_structz00_800 =
					BGl_findzd2typezd2zztype_envz00(CAR(CDR(BgL_expz00_518)));
				{	/* Foreign/ctype.scm 269 */
					obj_t BgL_structza2za2_801;

					{
						BgL_cstructz00_bglt BgL_auxz00_6413;

						{
							obj_t BgL_auxz00_6414;

							{	/* Foreign/ctype.scm 270 */
								BgL_objectz00_bglt BgL_tmpz00_6415;

								BgL_tmpz00_6415 =
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_structz00_800));
								BgL_auxz00_6414 = BGL_OBJECT_WIDENING(BgL_tmpz00_6415);
							}
							BgL_auxz00_6413 = ((BgL_cstructz00_bglt) BgL_auxz00_6414);
						}
						BgL_structza2za2_801 =
							(((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_6413))->
							BgL_cstructza2za2);
					}
					{	/* Foreign/ctype.scm 270 */

						{	/* Foreign/ctype.scm 273 */
							bool_t BgL_test2599z00_6421;

							{	/* Foreign/ctype.scm 273 */
								obj_t BgL_classz00_2220;

								BgL_classz00_2220 = BGl_cstructza2za2zzforeign_ctypez00;
								if (BGL_OBJECTP(BgL_structza2za2_801))
									{	/* Foreign/ctype.scm 273 */
										BgL_objectz00_bglt BgL_arg1807z00_2222;

										BgL_arg1807z00_2222 =
											(BgL_objectz00_bglt) (BgL_structza2za2_801);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Foreign/ctype.scm 273 */
												long BgL_idxz00_2228;

												BgL_idxz00_2228 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2222);
												BgL_test2599z00_6421 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2228 + 2L)) == BgL_classz00_2220);
											}
										else
											{	/* Foreign/ctype.scm 273 */
												bool_t BgL_res2114z00_2253;

												{	/* Foreign/ctype.scm 273 */
													obj_t BgL_oclassz00_2236;

													{	/* Foreign/ctype.scm 273 */
														obj_t BgL_arg1815z00_2244;
														long BgL_arg1816z00_2245;

														BgL_arg1815z00_2244 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Foreign/ctype.scm 273 */
															long BgL_arg1817z00_2246;

															BgL_arg1817z00_2246 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2222);
															BgL_arg1816z00_2245 =
																(BgL_arg1817z00_2246 - OBJECT_TYPE);
														}
														BgL_oclassz00_2236 =
															VECTOR_REF(BgL_arg1815z00_2244,
															BgL_arg1816z00_2245);
													}
													{	/* Foreign/ctype.scm 273 */
														bool_t BgL__ortest_1115z00_2237;

														BgL__ortest_1115z00_2237 =
															(BgL_classz00_2220 == BgL_oclassz00_2236);
														if (BgL__ortest_1115z00_2237)
															{	/* Foreign/ctype.scm 273 */
																BgL_res2114z00_2253 = BgL__ortest_1115z00_2237;
															}
														else
															{	/* Foreign/ctype.scm 273 */
																long BgL_odepthz00_2238;

																{	/* Foreign/ctype.scm 273 */
																	obj_t BgL_arg1804z00_2239;

																	BgL_arg1804z00_2239 = (BgL_oclassz00_2236);
																	BgL_odepthz00_2238 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2239);
																}
																if ((2L < BgL_odepthz00_2238))
																	{	/* Foreign/ctype.scm 273 */
																		obj_t BgL_arg1802z00_2241;

																		{	/* Foreign/ctype.scm 273 */
																			obj_t BgL_arg1803z00_2242;

																			BgL_arg1803z00_2242 =
																				(BgL_oclassz00_2236);
																			BgL_arg1802z00_2241 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2242, 2L);
																		}
																		BgL_res2114z00_2253 =
																			(BgL_arg1802z00_2241 ==
																			BgL_classz00_2220);
																	}
																else
																	{	/* Foreign/ctype.scm 273 */
																		BgL_res2114z00_2253 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2599z00_6421 = BgL_res2114z00_2253;
											}
									}
								else
									{	/* Foreign/ctype.scm 273 */
										BgL_test2599z00_6421 = ((bool_t) 0);
									}
							}
							if (BgL_test2599z00_6421)
								{	/* Foreign/ctype.scm 274 */
									obj_t BgL_arg1702z00_803;

									BgL_arg1702z00_803 =
										(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_structza2za2_801)))->BgL_idz00);
									return
										BGl_declarezd2czd2aliasz12z12zzforeign_ctypez00
										(BgL_idz00_517, BgL_arg1702z00_803, BgL_namez00_519);
								}
							else
								{	/* Foreign/ctype.scm 276 */
									obj_t BgL_cobjz00_804;

									BgL_cobjz00_804 = BGl_za2cobjza2z00zztype_cachez00;
									{	/* Foreign/ctype.scm 276 */
										obj_t BgL_objz00_805;

										BgL_objz00_805 = BGl_za2objza2z00zztype_cachez00;
										{	/* Foreign/ctype.scm 277 */
											obj_t BgL_nobjz00_806;

											BgL_nobjz00_806 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_objz00_805)))->BgL_namez00);
											{	/* Foreign/ctype.scm 278 */
												obj_t BgL_bidz00_807;

												{	/* Foreign/ctype.scm 279 */
													obj_t BgL_arg1711z00_819;

													{	/* Foreign/ctype.scm 279 */
														obj_t BgL_arg1714z00_820;
														obj_t BgL_arg1717z00_821;

														{	/* Foreign/ctype.scm 279 */
															obj_t BgL_symbolz00_2256;

															BgL_symbolz00_2256 = CNST_TABLE_REF(10);
															{	/* Foreign/ctype.scm 279 */
																obj_t BgL_arg1455z00_2257;

																BgL_arg1455z00_2257 =
																	SYMBOL_TO_STRING(BgL_symbolz00_2256);
																BgL_arg1714z00_820 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_2257);
															}
														}
														{	/* Foreign/ctype.scm 279 */
															obj_t BgL_arg1455z00_2259;

															BgL_arg1455z00_2259 =
																SYMBOL_TO_STRING(BgL_idz00_517);
															BgL_arg1717z00_821 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_2259);
														}
														BgL_arg1711z00_819 =
															string_append(BgL_arg1714z00_820,
															BgL_arg1717z00_821);
													}
													BgL_bidz00_807 =
														bstring_to_symbol(BgL_arg1711z00_819);
												}
												{	/* Foreign/ctype.scm 279 */
													BgL_typez00_bglt BgL_ctypez00_808;

													BgL_ctypez00_808 =
														BGl_declarezd2subtypez12zc0zztype_envz00
														(BgL_idz00_517, BgL_namez00_519, CNST_TABLE_REF(11),
														CNST_TABLE_REF(9));
													{	/* Foreign/ctype.scm 280 */
														BgL_typez00_bglt BgL_btypez00_809;

														BgL_btypez00_809 =
															BGl_declarezd2subtypez12zc0zztype_envz00
															(BgL_bidz00_807, BgL_nobjz00_806,
															CNST_TABLE_REF(12), CNST_TABLE_REF(13));
														{	/* Foreign/ctype.scm 281 */

															((((BgL_typez00_bglt) COBJECT(BgL_ctypez00_808))->
																	BgL_siza7eza7) =
																((obj_t)
																	string_append_3
																	(BGl_string2119z00zzforeign_ctypez00,
																		BGl_stringzd2sanszd2z42z42zztype_toolsz00
																		(BgL_namez00_519),
																		BGl_string2120z00zzforeign_ctypez00)),
																BUNSPEC);
															BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																(BGl_makezd2foreignzd2coercersz00zzforeign_ctypez00
																(BGl_stringzd2sanszd2z42z42zztype_toolsz00
																	(BgL_namez00_519), BgL_idz00_517,
																	BgL_bidz00_807));
															{
																BgL_cstructz00_bglt BgL_auxz00_6468;

																{
																	obj_t BgL_auxz00_6469;

																	{	/* Foreign/ctype.scm 291 */
																		BgL_objectz00_bglt BgL_tmpz00_6470;

																		BgL_tmpz00_6470 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_structz00_800));
																		BgL_auxz00_6469 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6470);
																	}
																	BgL_auxz00_6468 =
																		((BgL_cstructz00_bglt) BgL_auxz00_6469);
																}
																((((BgL_cstructz00_bglt)
																			COBJECT(BgL_auxz00_6468))->
																		BgL_cstructza2za2) =
																	((obj_t) ((obj_t) BgL_ctypez00_808)),
																	BUNSPEC);
															}
															{	/* Foreign/ctype.scm 293 */
																BgL_cstructza2za2_bglt BgL_wide1290z00_816;

																BgL_wide1290z00_816 =
																	((BgL_cstructza2za2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_cstructza2za2_bgl))));
																{	/* Foreign/ctype.scm 293 */
																	obj_t BgL_auxz00_6481;
																	BgL_objectz00_bglt BgL_tmpz00_6478;

																	BgL_auxz00_6481 =
																		((obj_t) BgL_wide1290z00_816);
																	BgL_tmpz00_6478 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_ctypez00_808));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6478,
																		BgL_auxz00_6481);
																}
																((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_ctypez00_808));
																{	/* Foreign/ctype.scm 293 */
																	long BgL_arg1710z00_817;

																	BgL_arg1710z00_817 =
																		BGL_CLASS_NUM
																		(BGl_cstructza2za2zzforeign_ctypez00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_ctypez00_808)),
																		BgL_arg1710z00_817);
																}
																((BgL_typez00_bglt)
																	((BgL_typez00_bglt) BgL_ctypez00_808));
															}
															{
																BgL_cstructza2za2_bglt BgL_auxz00_6492;

																{
																	obj_t BgL_auxz00_6493;

																	{	/* Foreign/ctype.scm 294 */
																		BgL_objectz00_bglt BgL_tmpz00_6494;

																		BgL_tmpz00_6494 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_ctypez00_808));
																		BgL_auxz00_6493 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6494);
																	}
																	BgL_auxz00_6492 =
																		((BgL_cstructza2za2_bglt) BgL_auxz00_6493);
																}
																((((BgL_cstructza2za2_bglt)
																			COBJECT(BgL_auxz00_6492))->BgL_btypez00) =
																	((BgL_typez00_bglt) BgL_btypez00_809),
																	BUNSPEC);
															}
															{
																BgL_cstructza2za2_bglt BgL_auxz00_6500;

																{
																	obj_t BgL_auxz00_6501;

																	{	/* Foreign/ctype.scm 295 */
																		BgL_objectz00_bglt BgL_tmpz00_6502;

																		BgL_tmpz00_6502 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_ctypez00_808));
																		BgL_auxz00_6501 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6502);
																	}
																	BgL_auxz00_6500 =
																		((BgL_cstructza2za2_bglt) BgL_auxz00_6501);
																}
																((((BgL_cstructza2za2_bglt)
																			COBJECT(BgL_auxz00_6500))->
																		BgL_cstructz00) =
																	((BgL_typez00_bglt) ((BgL_typez00_bglt)
																			BgL_structz00_800)), BUNSPEC);
															}
															((BgL_typez00_bglt) BgL_ctypez00_808);
															return BgL_ctypez00_808;
														}
													}
												}
											}
										}
									}
								}
						}
					}
				}
			}
		}

	}



/* make-foreign-coercers */
	obj_t BGl_makezd2foreignzd2coercersz00zzforeign_ctypez00(obj_t
		BgL_namez00_520, obj_t BgL_idz00_521, obj_t BgL_bidz00_522)
	{
		{	/* Foreign/ctype.scm 301 */
			{	/* Foreign/ctype.scm 302 */
				obj_t BgL_idzf3zf3_823;
				obj_t BgL_idzd2ze3bidz31_824;
				obj_t BgL_bidzd2ze3idz31_825;

				{	/* Foreign/ctype.scm 302 */
					obj_t BgL_arg1873z00_901;

					{	/* Foreign/ctype.scm 302 */
						obj_t BgL_arg1874z00_902;
						obj_t BgL_arg1875z00_903;

						{	/* Foreign/ctype.scm 302 */
							obj_t BgL_arg1455z00_2271;

							BgL_arg1455z00_2271 = SYMBOL_TO_STRING(BgL_idz00_521);
							BgL_arg1874z00_902 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2271);
						}
						{	/* Foreign/ctype.scm 302 */
							obj_t BgL_symbolz00_2272;

							BgL_symbolz00_2272 = CNST_TABLE_REF(15);
							{	/* Foreign/ctype.scm 302 */
								obj_t BgL_arg1455z00_2273;

								BgL_arg1455z00_2273 = SYMBOL_TO_STRING(BgL_symbolz00_2272);
								BgL_arg1875z00_903 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_2273);
							}
						}
						BgL_arg1873z00_901 =
							string_append(BgL_arg1874z00_902, BgL_arg1875z00_903);
					}
					BgL_idzf3zf3_823 = bstring_to_symbol(BgL_arg1873z00_901);
				}
				{	/* Foreign/ctype.scm 303 */
					obj_t BgL_list1876z00_904;

					{	/* Foreign/ctype.scm 303 */
						obj_t BgL_arg1877z00_905;

						{	/* Foreign/ctype.scm 303 */
							obj_t BgL_arg1878z00_906;

							BgL_arg1878z00_906 = MAKE_YOUNG_PAIR(BgL_bidz00_522, BNIL);
							BgL_arg1877z00_905 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1878z00_906);
						}
						BgL_list1876z00_904 =
							MAKE_YOUNG_PAIR(BgL_idz00_521, BgL_arg1877z00_905);
					}
					BgL_idzd2ze3bidz31_824 =
						BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1876z00_904);
				}
				{	/* Foreign/ctype.scm 304 */
					obj_t BgL_list1879z00_907;

					{	/* Foreign/ctype.scm 304 */
						obj_t BgL_arg1880z00_908;

						{	/* Foreign/ctype.scm 304 */
							obj_t BgL_arg1882z00_909;

							BgL_arg1882z00_909 = MAKE_YOUNG_PAIR(BgL_idz00_521, BNIL);
							BgL_arg1880z00_908 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(16), BgL_arg1882z00_909);
						}
						BgL_list1879z00_907 =
							MAKE_YOUNG_PAIR(BgL_bidz00_522, BgL_arg1880z00_908);
					}
					BgL_bidzd2ze3idz31_825 =
						BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(BgL_list1879z00_907);
				}
				{	/* Foreign/ctype.scm 305 */
					obj_t BgL_resz00_826;

					{	/* Foreign/ctype.scm 306 */
						obj_t BgL_arg1720z00_827;

						{	/* Foreign/ctype.scm 306 */
							obj_t BgL_arg1722z00_828;
							obj_t BgL_arg1724z00_829;

							{	/* Foreign/ctype.scm 306 */
								obj_t BgL_arg1733z00_830;

								{	/* Foreign/ctype.scm 306 */
									obj_t BgL_arg1734z00_831;

									{	/* Foreign/ctype.scm 306 */
										obj_t BgL_arg1735z00_832;

										{	/* Foreign/ctype.scm 306 */
											obj_t BgL_arg1736z00_833;

											BgL_arg1736z00_833 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											BgL_arg1735z00_832 =
												MAKE_YOUNG_PAIR(BNIL, BgL_arg1736z00_833);
										}
										BgL_arg1734z00_831 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg1735z00_832);
									}
									BgL_arg1733z00_830 =
										MAKE_YOUNG_PAIR(BgL_idz00_521, BgL_arg1734z00_831);
								}
								BgL_arg1722z00_828 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1733z00_830);
							}
							{	/* Foreign/ctype.scm 307 */
								obj_t BgL_arg1737z00_834;
								obj_t BgL_arg1738z00_835;

								{	/* Foreign/ctype.scm 307 */
									obj_t BgL_arg1739z00_836;

									{	/* Foreign/ctype.scm 307 */
										obj_t BgL_arg1740z00_837;

										{	/* Foreign/ctype.scm 307 */
											obj_t BgL_arg1746z00_838;

											{	/* Foreign/ctype.scm 307 */
												obj_t BgL_arg1747z00_839;

												BgL_arg1747z00_839 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												BgL_arg1746z00_838 =
													MAKE_YOUNG_PAIR(BNIL, BgL_arg1747z00_839);
											}
											BgL_arg1740z00_837 =
												MAKE_YOUNG_PAIR(BgL_idz00_521, BgL_arg1746z00_838);
										}
										BgL_arg1739z00_836 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(17), BgL_arg1740z00_837);
									}
									BgL_arg1737z00_834 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1739z00_836);
								}
								{	/* Foreign/ctype.scm 308 */
									obj_t BgL_arg1748z00_840;
									obj_t BgL_arg1749z00_841;

									{	/* Foreign/ctype.scm 308 */
										obj_t BgL_arg1750z00_842;

										{	/* Foreign/ctype.scm 308 */
											obj_t BgL_arg1751z00_843;

											{	/* Foreign/ctype.scm 308 */
												obj_t BgL_arg1752z00_844;

												{	/* Foreign/ctype.scm 308 */
													obj_t BgL_arg1753z00_845;

													BgL_arg1753z00_845 = MAKE_YOUNG_PAIR(BNIL, BNIL);
													BgL_arg1752z00_844 =
														MAKE_YOUNG_PAIR(BNIL, BgL_arg1753z00_845);
												}
												BgL_arg1751z00_843 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
													BgL_arg1752z00_844);
											}
											BgL_arg1750z00_842 =
												MAKE_YOUNG_PAIR(BgL_bidz00_522, BgL_arg1751z00_843);
										}
										BgL_arg1748z00_840 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1750z00_842);
									}
									{	/* Foreign/ctype.scm 309 */
										obj_t BgL_arg1754z00_846;
										obj_t BgL_arg1755z00_847;

										{	/* Foreign/ctype.scm 309 */
											obj_t BgL_arg1761z00_848;

											{	/* Foreign/ctype.scm 309 */
												obj_t BgL_arg1762z00_849;

												{	/* Foreign/ctype.scm 309 */
													obj_t BgL_arg1765z00_850;

													{	/* Foreign/ctype.scm 309 */
														obj_t BgL_arg1767z00_851;
														obj_t BgL_arg1770z00_852;

														BgL_arg1767z00_851 =
															MAKE_YOUNG_PAIR(BgL_idzf3zf3_823, BNIL);
														BgL_arg1770z00_852 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														BgL_arg1765z00_850 =
															MAKE_YOUNG_PAIR(BgL_arg1767z00_851,
															BgL_arg1770z00_852);
													}
													BgL_arg1762z00_849 =
														MAKE_YOUNG_PAIR(BgL_bidz00_522, BgL_arg1765z00_850);
												}
												BgL_arg1761z00_848 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
													BgL_arg1762z00_849);
											}
											BgL_arg1754z00_846 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(18), BgL_arg1761z00_848);
										}
										{	/* Foreign/ctype.scm 311 */
											obj_t BgL_arg1771z00_853;
											obj_t BgL_arg1773z00_854;

											{	/* Foreign/ctype.scm 311 */
												obj_t BgL_arg1775z00_855;

												{	/* Foreign/ctype.scm 311 */
													obj_t BgL_arg1798z00_856;

													{	/* Foreign/ctype.scm 311 */
														obj_t BgL_arg1799z00_857;

														{	/* Foreign/ctype.scm 311 */
															obj_t BgL_arg1805z00_858;

															{	/* Foreign/ctype.scm 311 */
																obj_t BgL_arg1806z00_859;

																{	/* Foreign/ctype.scm 311 */
																	obj_t BgL_arg1808z00_860;

																	{	/* Foreign/ctype.scm 311 */
																		obj_t BgL_arg1812z00_861;

																		{	/* Foreign/ctype.scm 311 */
																			obj_t BgL_arg1820z00_862;
																			obj_t BgL_arg1822z00_863;

																			BgL_arg1820z00_862 =
																				MAKE_YOUNG_PAIR
																				(BGl_makezd2typedzd2identz00zzast_identz00
																				(CNST_TABLE_REF(20), BgL_idz00_521),
																				BNIL);
																			{	/* Foreign/ctype.scm 312 */
																				obj_t BgL_arg1831z00_865;

																				{	/* Foreign/ctype.scm 312 */
																					obj_t BgL_arg1832z00_866;

																					{	/* Foreign/ctype.scm 312 */
																						obj_t BgL_arg1833z00_867;
																						obj_t BgL_arg1834z00_868;

																						{	/* Foreign/ctype.scm 312 */
																							obj_t BgL_arg1835z00_869;

																							BgL_arg1835z00_869 =
																								MAKE_YOUNG_PAIR(BgL_bidz00_522,
																								BNIL);
																							BgL_arg1833z00_867 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(21), BgL_arg1835z00_869);
																						}
																						BgL_arg1834z00_868 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(20), BNIL);
																						BgL_arg1832z00_866 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1833z00_867,
																							BgL_arg1834z00_868);
																					}
																					BgL_arg1831z00_865 =
																						MAKE_YOUNG_PAIR
																						(BgL_idzd2ze3bidz31_824,
																						BgL_arg1832z00_866);
																				}
																				BgL_arg1822z00_863 =
																					MAKE_YOUNG_PAIR(BgL_arg1831z00_865,
																					BNIL);
																			}
																			BgL_arg1812z00_861 =
																				MAKE_YOUNG_PAIR(BgL_arg1820z00_862,
																				BgL_arg1822z00_863);
																		}
																		BgL_arg1808z00_860 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																			BgL_arg1812z00_861);
																	}
																	BgL_arg1806z00_859 =
																		MAKE_YOUNG_PAIR(BgL_arg1808z00_860, BNIL);
																}
																BgL_arg1805z00_858 =
																	MAKE_YOUNG_PAIR(BgL_arg1806z00_859, BNIL);
															}
															BgL_arg1799z00_857 =
																MAKE_YOUNG_PAIR(BNIL, BgL_arg1805z00_858);
														}
														BgL_arg1798z00_856 =
															MAKE_YOUNG_PAIR(BgL_bidz00_522,
															BgL_arg1799z00_857);
													}
													BgL_arg1775z00_855 =
														MAKE_YOUNG_PAIR(BgL_idz00_521, BgL_arg1798z00_856);
												}
												BgL_arg1771z00_853 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
													BgL_arg1775z00_855);
											}
											{	/* Foreign/ctype.scm 314 */
												obj_t BgL_arg1836z00_870;
												obj_t BgL_arg1837z00_871;

												{	/* Foreign/ctype.scm 314 */
													obj_t BgL_arg1838z00_872;

													{	/* Foreign/ctype.scm 314 */
														obj_t BgL_arg1839z00_873;

														{	/* Foreign/ctype.scm 314 */
															obj_t BgL_arg1840z00_874;

															{	/* Foreign/ctype.scm 314 */
																obj_t BgL_arg1842z00_875;

																{	/* Foreign/ctype.scm 314 */
																	obj_t BgL_arg1843z00_876;

																	{	/* Foreign/ctype.scm 314 */
																		obj_t BgL_arg1844z00_877;

																		{	/* Foreign/ctype.scm 314 */
																			obj_t BgL_arg1845z00_878;

																			{	/* Foreign/ctype.scm 314 */
																				obj_t BgL_arg1846z00_879;
																				obj_t BgL_arg1847z00_880;

																				BgL_arg1846z00_879 =
																					MAKE_YOUNG_PAIR
																					(BGl_makezd2typedzd2identz00zzast_identz00
																					(CNST_TABLE_REF(20), BgL_idz00_521),
																					BNIL);
																				{	/* Foreign/ctype.scm 315 */
																					obj_t BgL_arg1849z00_882;

																					{	/* Foreign/ctype.scm 315 */
																						obj_t BgL_arg1850z00_883;

																						{	/* Foreign/ctype.scm 315 */
																							obj_t BgL_arg1851z00_884;
																							obj_t BgL_arg1852z00_885;

																							{	/* Foreign/ctype.scm 315 */
																								obj_t BgL_arg1853z00_886;

																								BgL_arg1853z00_886 =
																									MAKE_YOUNG_PAIR
																									(BgL_bidz00_522, BNIL);
																								BgL_arg1851z00_884 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(21), BgL_arg1853z00_886);
																							}
																							BgL_arg1852z00_885 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(20), BNIL);
																							BgL_arg1850z00_883 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1851z00_884,
																								BgL_arg1852z00_885);
																						}
																						BgL_arg1849z00_882 =
																							MAKE_YOUNG_PAIR
																							(BgL_idzd2ze3bidz31_824,
																							BgL_arg1850z00_883);
																					}
																					BgL_arg1847z00_880 =
																						MAKE_YOUNG_PAIR(BgL_arg1849z00_882,
																						BNIL);
																				}
																				BgL_arg1845z00_878 =
																					MAKE_YOUNG_PAIR(BgL_arg1846z00_879,
																					BgL_arg1847z00_880);
																			}
																			BgL_arg1844z00_877 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																				BgL_arg1845z00_878);
																		}
																		BgL_arg1843z00_876 =
																			MAKE_YOUNG_PAIR(BgL_arg1844z00_877, BNIL);
																	}
																	BgL_arg1842z00_875 =
																		MAKE_YOUNG_PAIR(BgL_arg1843z00_876, BNIL);
																}
																BgL_arg1840z00_874 =
																	MAKE_YOUNG_PAIR(BNIL, BgL_arg1842z00_875);
															}
															BgL_arg1839z00_873 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																BgL_arg1840z00_874);
														}
														BgL_arg1838z00_872 =
															MAKE_YOUNG_PAIR(BgL_idz00_521,
															BgL_arg1839z00_873);
													}
													BgL_arg1836z00_870 =
														MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
														BgL_arg1838z00_872);
												}
												{	/* Foreign/ctype.scm 316 */
													obj_t BgL_arg1854z00_887;
													obj_t BgL_arg1856z00_888;

													{	/* Foreign/ctype.scm 316 */
														obj_t BgL_arg1857z00_889;

														{	/* Foreign/ctype.scm 316 */
															obj_t BgL_arg1858z00_890;

															{	/* Foreign/ctype.scm 316 */
																obj_t BgL_arg1859z00_891;

																{	/* Foreign/ctype.scm 316 */
																	obj_t BgL_arg1860z00_892;

																	{	/* Foreign/ctype.scm 316 */
																		obj_t BgL_arg1862z00_893;

																		BgL_arg1862z00_893 =
																			MAKE_YOUNG_PAIR(BgL_bidzd2ze3idz31_825,
																			BNIL);
																		BgL_arg1860z00_892 =
																			MAKE_YOUNG_PAIR(BgL_arg1862z00_893, BNIL);
																	}
																	BgL_arg1859z00_891 =
																		MAKE_YOUNG_PAIR(BNIL, BgL_arg1860z00_892);
																}
																BgL_arg1858z00_890 =
																	MAKE_YOUNG_PAIR(BgL_idz00_521,
																	BgL_arg1859z00_891);
															}
															BgL_arg1857z00_889 =
																MAKE_YOUNG_PAIR(BgL_bidz00_522,
																BgL_arg1858z00_890);
														}
														BgL_arg1854z00_887 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
															BgL_arg1857z00_889);
													}
													{	/* Foreign/ctype.scm 317 */
														obj_t BgL_arg1863z00_894;

														{	/* Foreign/ctype.scm 317 */
															obj_t BgL_arg1864z00_895;

															{	/* Foreign/ctype.scm 317 */
																obj_t BgL_arg1866z00_896;

																{	/* Foreign/ctype.scm 317 */
																	obj_t BgL_arg1868z00_897;

																	{	/* Foreign/ctype.scm 317 */
																		obj_t BgL_arg1869z00_898;
																		obj_t BgL_arg1870z00_899;

																		BgL_arg1869z00_898 =
																			MAKE_YOUNG_PAIR(BgL_idzf3zf3_823, BNIL);
																		{	/* Foreign/ctype.scm 317 */
																			obj_t BgL_arg1872z00_900;

																			BgL_arg1872z00_900 =
																				MAKE_YOUNG_PAIR(BgL_bidzd2ze3idz31_825,
																				BNIL);
																			BgL_arg1870z00_899 =
																				MAKE_YOUNG_PAIR(BgL_arg1872z00_900,
																				BNIL);
																		}
																		BgL_arg1868z00_897 =
																			MAKE_YOUNG_PAIR(BgL_arg1869z00_898,
																			BgL_arg1870z00_899);
																	}
																	BgL_arg1866z00_896 =
																		MAKE_YOUNG_PAIR(BgL_idz00_521,
																		BgL_arg1868z00_897);
																}
																BgL_arg1864z00_895 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																	BgL_arg1866z00_896);
															}
															BgL_arg1863z00_894 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																BgL_arg1864z00_895);
														}
														BgL_arg1856z00_888 =
															MAKE_YOUNG_PAIR(BgL_arg1863z00_894, BNIL);
													}
													BgL_arg1837z00_871 =
														MAKE_YOUNG_PAIR(BgL_arg1854z00_887,
														BgL_arg1856z00_888);
												}
												BgL_arg1773z00_854 =
													MAKE_YOUNG_PAIR(BgL_arg1836z00_870,
													BgL_arg1837z00_871);
											}
											BgL_arg1755z00_847 =
												MAKE_YOUNG_PAIR(BgL_arg1771z00_853, BgL_arg1773z00_854);
										}
										BgL_arg1749z00_841 =
											MAKE_YOUNG_PAIR(BgL_arg1754z00_846, BgL_arg1755z00_847);
									}
									BgL_arg1738z00_835 =
										MAKE_YOUNG_PAIR(BgL_arg1748z00_840, BgL_arg1749z00_841);
								}
								BgL_arg1724z00_829 =
									MAKE_YOUNG_PAIR(BgL_arg1737z00_834, BgL_arg1738z00_835);
							}
							BgL_arg1720z00_827 =
								MAKE_YOUNG_PAIR(BgL_arg1722z00_828, BgL_arg1724z00_829);
						}
						BgL_resz00_826 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(23), BgL_arg1720z00_827);
					}
					return BgL_resz00_826;
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.scm 15 */
			{	/* Foreign/ctype.scm 32 */
				obj_t BgL_arg1888z00_914;
				obj_t BgL_arg1889z00_915;

				{	/* Foreign/ctype.scm 32 */
					obj_t BgL_v1340z00_953;

					BgL_v1340z00_953 = create_vector(1L);
					{	/* Foreign/ctype.scm 32 */
						obj_t BgL_arg1901z00_954;

						BgL_arg1901z00_954 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2123z00zzforeign_ctypez00,
							BGl_proc2122z00zzforeign_ctypez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2121z00zzforeign_ctypez00, CNST_TABLE_REF(25));
						VECTOR_SET(BgL_v1340z00_953, 0L, BgL_arg1901z00_954);
					}
					BgL_arg1888z00_914 = BgL_v1340z00_953;
				}
				{	/* Foreign/ctype.scm 32 */
					obj_t BgL_v1341z00_967;

					BgL_v1341z00_967 = create_vector(0L);
					BgL_arg1889z00_915 = BgL_v1341z00_967;
				}
				BGl_caliasz00zzforeign_ctypez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(26),
					CNST_TABLE_REF(27), BGl_typez00zztype_typez00, 50670L,
					BGl_proc2127z00zzforeign_ctypez00, BGl_proc2126z00zzforeign_ctypez00,
					BFALSE, BGl_proc2125z00zzforeign_ctypez00,
					BGl_proc2124z00zzforeign_ctypez00, BgL_arg1888z00_914,
					BgL_arg1889z00_915);
			}
			{	/* Foreign/ctype.scm 36 */
				obj_t BgL_arg1914z00_976;
				obj_t BgL_arg1916z00_977;

				{	/* Foreign/ctype.scm 36 */
					obj_t BgL_v1342z00_1016;

					BgL_v1342z00_1016 = create_vector(2L);
					{	/* Foreign/ctype.scm 36 */
						obj_t BgL_arg1929z00_1017;

						BgL_arg1929z00_1017 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc2129z00zzforeign_ctypez00,
							BGl_proc2128z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1342z00_1016, 0L, BgL_arg1929z00_1017);
					}
					{	/* Foreign/ctype.scm 36 */
						obj_t BgL_arg1934z00_1027;

						BgL_arg1934z00_1027 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(29),
							BGl_proc2131z00zzforeign_ctypez00,
							BGl_proc2130z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(30));
						VECTOR_SET(BgL_v1342z00_1016, 1L, BgL_arg1934z00_1027);
					}
					BgL_arg1914z00_976 = BgL_v1342z00_1016;
				}
				{	/* Foreign/ctype.scm 36 */
					obj_t BgL_v1343z00_1037;

					BgL_v1343z00_1037 = create_vector(0L);
					BgL_arg1916z00_977 = BgL_v1343z00_1037;
				}
				BGl_cenumz00zzforeign_ctypez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(31),
					CNST_TABLE_REF(27), BGl_typez00zztype_typez00, 47913L,
					BGl_proc2135z00zzforeign_ctypez00, BGl_proc2134z00zzforeign_ctypez00,
					BFALSE, BGl_proc2133z00zzforeign_ctypez00,
					BGl_proc2132z00zzforeign_ctypez00, BgL_arg1914z00_976,
					BgL_arg1916z00_977);
			}
			{	/* Foreign/ctype.scm 42 */
				obj_t BgL_arg1943z00_1046;
				obj_t BgL_arg1944z00_1047;

				{	/* Foreign/ctype.scm 42 */
					obj_t BgL_v1344z00_1085;

					BgL_v1344z00_1085 = create_vector(1L);
					{	/* Foreign/ctype.scm 42 */
						obj_t BgL_arg1955z00_1086;

						BgL_arg1955z00_1086 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc2137z00zzforeign_ctypez00,
							BGl_proc2136z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1344z00_1085, 0L, BgL_arg1955z00_1086);
					}
					BgL_arg1943z00_1046 = BgL_v1344z00_1085;
				}
				{	/* Foreign/ctype.scm 42 */
					obj_t BgL_v1345z00_1096;

					BgL_v1345z00_1096 = create_vector(0L);
					BgL_arg1944z00_1047 = BgL_v1345z00_1096;
				}
				BGl_copaquez00zzforeign_ctypez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(32),
					CNST_TABLE_REF(27), BGl_typez00zztype_typez00, 8782L,
					BGl_proc2141z00zzforeign_ctypez00, BGl_proc2140z00zzforeign_ctypez00,
					BFALSE, BGl_proc2139z00zzforeign_ctypez00,
					BGl_proc2138z00zzforeign_ctypez00, BgL_arg1943z00_1046,
					BgL_arg1944z00_1047);
			}
			{	/* Foreign/ctype.scm 46 */
				obj_t BgL_arg1964z00_1105;
				obj_t BgL_arg1965z00_1106;

				{	/* Foreign/ctype.scm 46 */
					obj_t BgL_v1346z00_1147;

					BgL_v1346z00_1147 = create_vector(4L);
					{	/* Foreign/ctype.scm 46 */
						obj_t BgL_arg1976z00_1148;

						BgL_arg1976z00_1148 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc2143z00zzforeign_ctypez00,
							BGl_proc2142z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1346z00_1147, 0L, BgL_arg1976z00_1148);
					}
					{	/* Foreign/ctype.scm 46 */
						obj_t BgL_arg1981z00_1158;

						BgL_arg1981z00_1158 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc2145z00zzforeign_ctypez00,
							BGl_proc2144z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(34));
						VECTOR_SET(BgL_v1346z00_1147, 1L, BgL_arg1981z00_1158);
					}
					{	/* Foreign/ctype.scm 46 */
						obj_t BgL_arg1986z00_1168;

						BgL_arg1986z00_1168 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(35),
							BGl_proc2147z00zzforeign_ctypez00,
							BGl_proc2146z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1346z00_1147, 2L, BgL_arg1986z00_1168);
					}
					{	/* Foreign/ctype.scm 46 */
						obj_t BgL_arg1991z00_1178;

						BgL_arg1991z00_1178 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(36),
							BGl_proc2149z00zzforeign_ctypez00,
							BGl_proc2148z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(30));
						VECTOR_SET(BgL_v1346z00_1147, 3L, BgL_arg1991z00_1178);
					}
					BgL_arg1964z00_1105 = BgL_v1346z00_1147;
				}
				{	/* Foreign/ctype.scm 46 */
					obj_t BgL_v1347z00_1188;

					BgL_v1347z00_1188 = create_vector(0L);
					BgL_arg1965z00_1106 = BgL_v1347z00_1188;
				}
				BGl_cfunctionz00zzforeign_ctypez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(37),
					CNST_TABLE_REF(27), BGl_typez00zztype_typez00, 50106L,
					BGl_proc2153z00zzforeign_ctypez00, BGl_proc2152z00zzforeign_ctypez00,
					BFALSE, BGl_proc2151z00zzforeign_ctypez00,
					BGl_proc2150z00zzforeign_ctypez00, BgL_arg1964z00_1105,
					BgL_arg1965z00_1106);
			}
			{	/* Foreign/ctype.scm 56 */
				obj_t BgL_arg2000z00_1197;
				obj_t BgL_arg2001z00_1198;

				{	/* Foreign/ctype.scm 56 */
					obj_t BgL_v1348z00_1238;

					BgL_v1348z00_1238 = create_vector(3L);
					{	/* Foreign/ctype.scm 56 */
						obj_t BgL_arg2012z00_1239;

						BgL_arg2012z00_1239 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc2155z00zzforeign_ctypez00,
							BGl_proc2154z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1348z00_1238, 0L, BgL_arg2012z00_1239);
					}
					{	/* Foreign/ctype.scm 56 */
						obj_t BgL_arg2017z00_1249;

						BgL_arg2017z00_1249 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(38),
							BGl_proc2157z00zzforeign_ctypez00,
							BGl_proc2156z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1348z00_1238, 1L, BgL_arg2017z00_1249);
					}
					{	/* Foreign/ctype.scm 56 */
						obj_t BgL_arg2022z00_1259;

						BgL_arg2022z00_1259 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2160z00zzforeign_ctypez00,
							BGl_proc2159z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc2158z00zzforeign_ctypez00, CNST_TABLE_REF(25));
						VECTOR_SET(BgL_v1348z00_1238, 2L, BgL_arg2022z00_1259);
					}
					BgL_arg2000z00_1197 = BgL_v1348z00_1238;
				}
				{	/* Foreign/ctype.scm 56 */
					obj_t BgL_v1349z00_1272;

					BgL_v1349z00_1272 = create_vector(0L);
					BgL_arg2001z00_1198 = BgL_v1349z00_1272;
				}
				BGl_cptrz00zzforeign_ctypez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(39),
					CNST_TABLE_REF(27), BGl_typez00zztype_typez00, 21354L,
					BGl_proc2164z00zzforeign_ctypez00, BGl_proc2163z00zzforeign_ctypez00,
					BFALSE, BGl_proc2162z00zzforeign_ctypez00,
					BGl_proc2161z00zzforeign_ctypez00, BgL_arg2000z00_1197,
					BgL_arg2001z00_1198);
			}
			{	/* Foreign/ctype.scm 64 */
				obj_t BgL_arg2036z00_1281;
				obj_t BgL_arg2037z00_1282;

				{	/* Foreign/ctype.scm 64 */
					obj_t BgL_v1350z00_1322;

					BgL_v1350z00_1322 = create_vector(3L);
					{	/* Foreign/ctype.scm 64 */
						obj_t BgL_arg2048z00_1323;

						BgL_arg2048z00_1323 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(40),
							BGl_proc2167z00zzforeign_ctypez00,
							BGl_proc2166z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BGl_proc2165z00zzforeign_ctypez00, CNST_TABLE_REF(25));
						VECTOR_SET(BgL_v1350z00_1322, 0L, BgL_arg2048z00_1323);
					}
					{	/* Foreign/ctype.scm 64 */
						obj_t BgL_arg2055z00_1336;

						BgL_arg2055z00_1336 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc2169z00zzforeign_ctypez00,
							BGl_proc2168z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(30));
						VECTOR_SET(BgL_v1350z00_1322, 1L, BgL_arg2055z00_1336);
					}
					{	/* Foreign/ctype.scm 64 */
						obj_t BgL_arg2060z00_1346;

						BgL_arg2060z00_1346 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(42),
							BGl_proc2172z00zzforeign_ctypez00,
							BGl_proc2171z00zzforeign_ctypez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2170z00zzforeign_ctypez00, CNST_TABLE_REF(30));
						VECTOR_SET(BgL_v1350z00_1322, 2L, BgL_arg2060z00_1346);
					}
					BgL_arg2036z00_1281 = BgL_v1350z00_1322;
				}
				{	/* Foreign/ctype.scm 64 */
					obj_t BgL_v1351z00_1359;

					BgL_v1351z00_1359 = create_vector(0L);
					BgL_arg2037z00_1282 = BgL_v1351z00_1359;
				}
				BGl_cstructz00zzforeign_ctypez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(43),
					CNST_TABLE_REF(27), BGl_typez00zztype_typez00, 60705L,
					BGl_proc2176z00zzforeign_ctypez00, BGl_proc2175z00zzforeign_ctypez00,
					BFALSE, BGl_proc2174z00zzforeign_ctypez00,
					BGl_proc2173z00zzforeign_ctypez00, BgL_arg2036z00_1281,
					BgL_arg2037z00_1282);
			}
			{	/* Foreign/ctype.scm 72 */
				obj_t BgL_arg2072z00_1368;
				obj_t BgL_arg2074z00_1369;

				{	/* Foreign/ctype.scm 72 */
					obj_t BgL_v1352z00_1408;

					BgL_v1352z00_1408 = create_vector(2L);
					{	/* Foreign/ctype.scm 72 */
						obj_t BgL_arg2086z00_1409;

						BgL_arg2086z00_1409 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc2178z00zzforeign_ctypez00,
							BGl_proc2177z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1352z00_1408, 0L, BgL_arg2086z00_1409);
					}
					{	/* Foreign/ctype.scm 72 */
						obj_t BgL_arg2091z00_1419;

						BgL_arg2091z00_1419 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(43),
							BGl_proc2180z00zzforeign_ctypez00,
							BGl_proc2179z00zzforeign_ctypez00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_cstructz00zzforeign_ctypez00);
						VECTOR_SET(BgL_v1352z00_1408, 1L, BgL_arg2091z00_1419);
					}
					BgL_arg2072z00_1368 = BgL_v1352z00_1408;
				}
				{	/* Foreign/ctype.scm 72 */
					obj_t BgL_v1353z00_1429;

					BgL_v1353z00_1429 = create_vector(0L);
					BgL_arg2074z00_1369 = BgL_v1353z00_1429;
				}
				return (BGl_cstructza2za2zzforeign_ctypez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(42),
						CNST_TABLE_REF(27), BGl_typez00zztype_typez00, 17119L,
						BGl_proc2184z00zzforeign_ctypez00,
						BGl_proc2183z00zzforeign_ctypez00, BFALSE,
						BGl_proc2182z00zzforeign_ctypez00,
						BGl_proc2181z00zzforeign_ctypez00, BgL_arg2072z00_1368,
						BgL_arg2074z00_1369), BUNSPEC);
			}
		}

	}



/* &lambda2081 */
	BgL_typez00_bglt BGl_z62lambda2081z62zzforeign_ctypez00(obj_t BgL_envz00_3346,
		obj_t BgL_o1219z00_3347)
	{
		{	/* Foreign/ctype.scm 72 */
			{	/* Foreign/ctype.scm 72 */
				long BgL_arg2082z00_3766;

				{	/* Foreign/ctype.scm 72 */
					obj_t BgL_arg2083z00_3767;

					{	/* Foreign/ctype.scm 72 */
						obj_t BgL_arg2084z00_3768;

						{	/* Foreign/ctype.scm 72 */
							obj_t BgL_arg1815z00_3769;
							long BgL_arg1816z00_3770;

							BgL_arg1815z00_3769 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Foreign/ctype.scm 72 */
								long BgL_arg1817z00_3771;

								BgL_arg1817z00_3771 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1219z00_3347)));
								BgL_arg1816z00_3770 = (BgL_arg1817z00_3771 - OBJECT_TYPE);
							}
							BgL_arg2084z00_3768 =
								VECTOR_REF(BgL_arg1815z00_3769, BgL_arg1816z00_3770);
						}
						BgL_arg2083z00_3767 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2084z00_3768);
					}
					{	/* Foreign/ctype.scm 72 */
						obj_t BgL_tmpz00_6723;

						BgL_tmpz00_6723 = ((obj_t) BgL_arg2083z00_3767);
						BgL_arg2082z00_3766 = BGL_CLASS_NUM(BgL_tmpz00_6723);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1219z00_3347)), BgL_arg2082z00_3766);
			}
			{	/* Foreign/ctype.scm 72 */
				BgL_objectz00_bglt BgL_tmpz00_6729;

				BgL_tmpz00_6729 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1219z00_3347));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6729, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1219z00_3347));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1219z00_3347));
		}

	}



/* &<@anonymous:2080> */
	obj_t BGl_z62zc3z04anonymousza32080ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3348, obj_t BgL_new1218z00_3349)
	{
		{	/* Foreign/ctype.scm 72 */
			{
				BgL_typez00_bglt BgL_auxz00_6737;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1218z00_3349))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(44)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1218z00_3349))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_6795;
					BgL_cstructza2za2_bglt BgL_auxz00_6788;

					{	/* Foreign/ctype.scm 72 */
						obj_t BgL_classz00_3773;

						BgL_classz00_3773 = BGl_typez00zztype_typez00;
						{	/* Foreign/ctype.scm 72 */
							obj_t BgL__ortest_1117z00_3774;

							BgL__ortest_1117z00_3774 = BGL_CLASS_NIL(BgL_classz00_3773);
							if (CBOOL(BgL__ortest_1117z00_3774))
								{	/* Foreign/ctype.scm 72 */
									BgL_auxz00_6795 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3774);
								}
							else
								{	/* Foreign/ctype.scm 72 */
									BgL_auxz00_6795 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3773));
								}
						}
					}
					{
						obj_t BgL_auxz00_6789;

						{	/* Foreign/ctype.scm 72 */
							BgL_objectz00_bglt BgL_tmpz00_6790;

							BgL_tmpz00_6790 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1218z00_3349));
							BgL_auxz00_6789 = BGL_OBJECT_WIDENING(BgL_tmpz00_6790);
						}
						BgL_auxz00_6788 = ((BgL_cstructza2za2_bglt) BgL_auxz00_6789);
					}
					((((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_6788))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_auxz00_6795), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_6810;
					BgL_cstructza2za2_bglt BgL_auxz00_6803;

					{	/* Foreign/ctype.scm 72 */
						obj_t BgL_classz00_3775;

						BgL_classz00_3775 = BGl_cstructz00zzforeign_ctypez00;
						{	/* Foreign/ctype.scm 72 */
							obj_t BgL__ortest_1117z00_3776;

							BgL__ortest_1117z00_3776 = BGL_CLASS_NIL(BgL_classz00_3775);
							if (CBOOL(BgL__ortest_1117z00_3776))
								{	/* Foreign/ctype.scm 72 */
									BgL_auxz00_6810 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3776);
								}
							else
								{	/* Foreign/ctype.scm 72 */
									BgL_auxz00_6810 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3775));
								}
						}
					}
					{
						obj_t BgL_auxz00_6804;

						{	/* Foreign/ctype.scm 72 */
							BgL_objectz00_bglt BgL_tmpz00_6805;

							BgL_tmpz00_6805 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1218z00_3349));
							BgL_auxz00_6804 = BGL_OBJECT_WIDENING(BgL_tmpz00_6805);
						}
						BgL_auxz00_6803 = ((BgL_cstructza2za2_bglt) BgL_auxz00_6804);
					}
					((((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_6803))->
							BgL_cstructz00) = ((BgL_typez00_bglt) BgL_auxz00_6810), BUNSPEC);
				}
				BgL_auxz00_6737 = ((BgL_typez00_bglt) BgL_new1218z00_3349);
				return ((obj_t) BgL_auxz00_6737);
			}
		}

	}



/* &lambda2078 */
	BgL_typez00_bglt BGl_z62lambda2078z62zzforeign_ctypez00(obj_t BgL_envz00_3350,
		obj_t BgL_o1215z00_3351)
	{
		{	/* Foreign/ctype.scm 72 */
			{	/* Foreign/ctype.scm 72 */
				BgL_cstructza2za2_bglt BgL_wide1217z00_3778;

				BgL_wide1217z00_3778 =
					((BgL_cstructza2za2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cstructza2za2_bgl))));
				{	/* Foreign/ctype.scm 72 */
					obj_t BgL_auxz00_6825;
					BgL_objectz00_bglt BgL_tmpz00_6821;

					BgL_auxz00_6825 = ((obj_t) BgL_wide1217z00_3778);
					BgL_tmpz00_6821 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1215z00_3351)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6821, BgL_auxz00_6825);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1215z00_3351)));
				{	/* Foreign/ctype.scm 72 */
					long BgL_arg2079z00_3779;

					BgL_arg2079z00_3779 =
						BGL_CLASS_NUM(BGl_cstructza2za2zzforeign_ctypez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1215z00_3351))), BgL_arg2079z00_3779);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1215z00_3351)));
			}
		}

	}



/* &lambda2075 */
	BgL_typez00_bglt BGl_z62lambda2075z62zzforeign_ctypez00(obj_t BgL_envz00_3352,
		obj_t BgL_id1197z00_3353, obj_t BgL_name1198z00_3354,
		obj_t BgL_siza7e1199za7_3355, obj_t BgL_class1200z00_3356,
		obj_t BgL_coercezd2to1201zd2_3357, obj_t BgL_parents1202z00_3358,
		obj_t BgL_initzf31203zf3_3359, obj_t BgL_magiczf31204zf3_3360,
		obj_t BgL_null1205z00_3361, obj_t BgL_z421206z42_3362,
		obj_t BgL_alias1207z00_3363, obj_t BgL_pointedzd2tozd2by1208z00_3364,
		obj_t BgL_tvector1209z00_3365, obj_t BgL_location1210z00_3366,
		obj_t BgL_importzd2location1211zd2_3367, obj_t BgL_occurrence1212z00_3368,
		obj_t BgL_btype1213z00_3369, obj_t BgL_cstruct1214z00_3370)
	{
		{	/* Foreign/ctype.scm 72 */
			{	/* Foreign/ctype.scm 72 */
				bool_t BgL_initzf31203zf3_3781;
				bool_t BgL_magiczf31204zf3_3782;
				int BgL_occurrence1212z00_3783;

				BgL_initzf31203zf3_3781 = CBOOL(BgL_initzf31203zf3_3359);
				BgL_magiczf31204zf3_3782 = CBOOL(BgL_magiczf31204zf3_3360);
				BgL_occurrence1212z00_3783 = CINT(BgL_occurrence1212z00_3368);
				{	/* Foreign/ctype.scm 72 */
					BgL_typez00_bglt BgL_new1329z00_3786;

					{	/* Foreign/ctype.scm 72 */
						BgL_typez00_bglt BgL_tmp1327z00_3787;
						BgL_cstructza2za2_bglt BgL_wide1328z00_3788;

						{
							BgL_typez00_bglt BgL_auxz00_6842;

							{	/* Foreign/ctype.scm 72 */
								BgL_typez00_bglt BgL_new1326z00_3789;

								BgL_new1326z00_3789 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Foreign/ctype.scm 72 */
									long BgL_arg2077z00_3790;

									BgL_arg2077z00_3790 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1326z00_3789),
										BgL_arg2077z00_3790);
								}
								{	/* Foreign/ctype.scm 72 */
									BgL_objectz00_bglt BgL_tmpz00_6847;

									BgL_tmpz00_6847 = ((BgL_objectz00_bglt) BgL_new1326z00_3789);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6847, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1326z00_3789);
								BgL_auxz00_6842 = BgL_new1326z00_3789;
							}
							BgL_tmp1327z00_3787 = ((BgL_typez00_bglt) BgL_auxz00_6842);
						}
						BgL_wide1328z00_3788 =
							((BgL_cstructza2za2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cstructza2za2_bgl))));
						{	/* Foreign/ctype.scm 72 */
							obj_t BgL_auxz00_6855;
							BgL_objectz00_bglt BgL_tmpz00_6853;

							BgL_auxz00_6855 = ((obj_t) BgL_wide1328z00_3788);
							BgL_tmpz00_6853 = ((BgL_objectz00_bglt) BgL_tmp1327z00_3787);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6853, BgL_auxz00_6855);
						}
						((BgL_objectz00_bglt) BgL_tmp1327z00_3787);
						{	/* Foreign/ctype.scm 72 */
							long BgL_arg2076z00_3791;

							BgL_arg2076z00_3791 =
								BGL_CLASS_NUM(BGl_cstructza2za2zzforeign_ctypez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1327z00_3787),
								BgL_arg2076z00_3791);
						}
						BgL_new1329z00_3786 = ((BgL_typez00_bglt) BgL_tmp1327z00_3787);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1329z00_3786)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1197z00_3353)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_namez00) =
						((obj_t) BgL_name1198z00_3354), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1199za7_3355), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_classz00) =
						((obj_t) BgL_class1200z00_3356), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1201zd2_3357), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_parentsz00) =
						((obj_t) BgL_parents1202z00_3358), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31203zf3_3781), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31204zf3_3782), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_nullz00) =
						((obj_t) BgL_null1205z00_3361), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_z42z42) =
						((obj_t) BgL_z421206z42_3362), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_aliasz00) =
						((obj_t) BgL_alias1207z00_3363), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1208z00_3364), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1209z00_3365), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_locationz00) =
						((obj_t) BgL_location1210z00_3366), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1211zd2_3367), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1329z00_3786)))->BgL_occurrencez00) =
						((int) BgL_occurrence1212z00_3783), BUNSPEC);
					{
						BgL_cstructza2za2_bglt BgL_auxz00_6896;

						{
							obj_t BgL_auxz00_6897;

							{	/* Foreign/ctype.scm 72 */
								BgL_objectz00_bglt BgL_tmpz00_6898;

								BgL_tmpz00_6898 = ((BgL_objectz00_bglt) BgL_new1329z00_3786);
								BgL_auxz00_6897 = BGL_OBJECT_WIDENING(BgL_tmpz00_6898);
							}
							BgL_auxz00_6896 = ((BgL_cstructza2za2_bglt) BgL_auxz00_6897);
						}
						((((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_6896))->
								BgL_btypez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_btype1213z00_3369)),
							BUNSPEC);
					}
					{
						BgL_cstructza2za2_bglt BgL_auxz00_6904;

						{
							obj_t BgL_auxz00_6905;

							{	/* Foreign/ctype.scm 72 */
								BgL_objectz00_bglt BgL_tmpz00_6906;

								BgL_tmpz00_6906 = ((BgL_objectz00_bglt) BgL_new1329z00_3786);
								BgL_auxz00_6905 = BGL_OBJECT_WIDENING(BgL_tmpz00_6906);
							}
							BgL_auxz00_6904 = ((BgL_cstructza2za2_bglt) BgL_auxz00_6905);
						}
						((((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_6904))->
								BgL_cstructz00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_cstruct1214z00_3370)),
							BUNSPEC);
					}
					return BgL_new1329z00_3786;
				}
			}
		}

	}



/* &lambda2096 */
	obj_t BGl_z62lambda2096z62zzforeign_ctypez00(obj_t BgL_envz00_3371,
		obj_t BgL_oz00_3372, obj_t BgL_vz00_3373)
	{
		{	/* Foreign/ctype.scm 72 */
			{
				BgL_cstructza2za2_bglt BgL_auxz00_6912;

				{
					obj_t BgL_auxz00_6913;

					{	/* Foreign/ctype.scm 72 */
						BgL_objectz00_bglt BgL_tmpz00_6914;

						BgL_tmpz00_6914 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3372));
						BgL_auxz00_6913 = BGL_OBJECT_WIDENING(BgL_tmpz00_6914);
					}
					BgL_auxz00_6912 = ((BgL_cstructza2za2_bglt) BgL_auxz00_6913);
				}
				return
					((((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_6912))->
						BgL_cstructz00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3373)), BUNSPEC);
			}
		}

	}



/* &lambda2095 */
	BgL_typez00_bglt BGl_z62lambda2095z62zzforeign_ctypez00(obj_t BgL_envz00_3374,
		obj_t BgL_oz00_3375)
	{
		{	/* Foreign/ctype.scm 72 */
			{
				BgL_cstructza2za2_bglt BgL_auxz00_6921;

				{
					obj_t BgL_auxz00_6922;

					{	/* Foreign/ctype.scm 72 */
						BgL_objectz00_bglt BgL_tmpz00_6923;

						BgL_tmpz00_6923 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3375));
						BgL_auxz00_6922 = BGL_OBJECT_WIDENING(BgL_tmpz00_6923);
					}
					BgL_auxz00_6921 = ((BgL_cstructza2za2_bglt) BgL_auxz00_6922);
				}
				return
					(((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_6921))->BgL_cstructz00);
			}
		}

	}



/* &lambda2090 */
	obj_t BGl_z62lambda2090z62zzforeign_ctypez00(obj_t BgL_envz00_3376,
		obj_t BgL_oz00_3377, obj_t BgL_vz00_3378)
	{
		{	/* Foreign/ctype.scm 72 */
			{
				BgL_cstructza2za2_bglt BgL_auxz00_6929;

				{
					obj_t BgL_auxz00_6930;

					{	/* Foreign/ctype.scm 72 */
						BgL_objectz00_bglt BgL_tmpz00_6931;

						BgL_tmpz00_6931 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3377));
						BgL_auxz00_6930 = BGL_OBJECT_WIDENING(BgL_tmpz00_6931);
					}
					BgL_auxz00_6929 = ((BgL_cstructza2za2_bglt) BgL_auxz00_6930);
				}
				return
					((((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_6929))->BgL_btypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3378)), BUNSPEC);
			}
		}

	}



/* &lambda2089 */
	BgL_typez00_bglt BGl_z62lambda2089z62zzforeign_ctypez00(obj_t BgL_envz00_3379,
		obj_t BgL_oz00_3380)
	{
		{	/* Foreign/ctype.scm 72 */
			{
				BgL_cstructza2za2_bglt BgL_auxz00_6938;

				{
					obj_t BgL_auxz00_6939;

					{	/* Foreign/ctype.scm 72 */
						BgL_objectz00_bglt BgL_tmpz00_6940;

						BgL_tmpz00_6940 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3380));
						BgL_auxz00_6939 = BGL_OBJECT_WIDENING(BgL_tmpz00_6940);
					}
					BgL_auxz00_6938 = ((BgL_cstructza2za2_bglt) BgL_auxz00_6939);
				}
				return
					(((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_6938))->BgL_btypez00);
			}
		}

	}



/* &lambda2044 */
	BgL_typez00_bglt BGl_z62lambda2044z62zzforeign_ctypez00(obj_t BgL_envz00_3381,
		obj_t BgL_o1195z00_3382)
	{
		{	/* Foreign/ctype.scm 64 */
			{	/* Foreign/ctype.scm 64 */
				long BgL_arg2045z00_3799;

				{	/* Foreign/ctype.scm 64 */
					obj_t BgL_arg2046z00_3800;

					{	/* Foreign/ctype.scm 64 */
						obj_t BgL_arg2047z00_3801;

						{	/* Foreign/ctype.scm 64 */
							obj_t BgL_arg1815z00_3802;
							long BgL_arg1816z00_3803;

							BgL_arg1815z00_3802 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Foreign/ctype.scm 64 */
								long BgL_arg1817z00_3804;

								BgL_arg1817z00_3804 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1195z00_3382)));
								BgL_arg1816z00_3803 = (BgL_arg1817z00_3804 - OBJECT_TYPE);
							}
							BgL_arg2047z00_3801 =
								VECTOR_REF(BgL_arg1815z00_3802, BgL_arg1816z00_3803);
						}
						BgL_arg2046z00_3800 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2047z00_3801);
					}
					{	/* Foreign/ctype.scm 64 */
						obj_t BgL_tmpz00_6953;

						BgL_tmpz00_6953 = ((obj_t) BgL_arg2046z00_3800);
						BgL_arg2045z00_3799 = BGL_CLASS_NUM(BgL_tmpz00_6953);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1195z00_3382)), BgL_arg2045z00_3799);
			}
			{	/* Foreign/ctype.scm 64 */
				BgL_objectz00_bglt BgL_tmpz00_6959;

				BgL_tmpz00_6959 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1195z00_3382));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6959, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1195z00_3382));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1195z00_3382));
		}

	}



/* &<@anonymous:2043> */
	obj_t BGl_z62zc3z04anonymousza32043ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3383, obj_t BgL_new1194z00_3384)
	{
		{	/* Foreign/ctype.scm 64 */
			{
				BgL_typez00_bglt BgL_auxz00_6967;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1194z00_3384))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(44)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1194z00_3384))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_cstructz00_bglt BgL_auxz00_7018;

					{
						obj_t BgL_auxz00_7019;

						{	/* Foreign/ctype.scm 64 */
							BgL_objectz00_bglt BgL_tmpz00_7020;

							BgL_tmpz00_7020 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1194z00_3384));
							BgL_auxz00_7019 = BGL_OBJECT_WIDENING(BgL_tmpz00_7020);
						}
						BgL_auxz00_7018 = ((BgL_cstructz00_bglt) BgL_auxz00_7019);
					}
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7018))->
							BgL_structzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_cstructz00_bglt BgL_auxz00_7026;

					{
						obj_t BgL_auxz00_7027;

						{	/* Foreign/ctype.scm 64 */
							BgL_objectz00_bglt BgL_tmpz00_7028;

							BgL_tmpz00_7028 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1194z00_3384));
							BgL_auxz00_7027 = BGL_OBJECT_WIDENING(BgL_tmpz00_7028);
						}
						BgL_auxz00_7026 = ((BgL_cstructz00_bglt) BgL_auxz00_7027);
					}
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7026))->BgL_fieldsz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_cstructz00_bglt BgL_auxz00_7034;

					{
						obj_t BgL_auxz00_7035;

						{	/* Foreign/ctype.scm 64 */
							BgL_objectz00_bglt BgL_tmpz00_7036;

							BgL_tmpz00_7036 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1194z00_3384));
							BgL_auxz00_7035 = BGL_OBJECT_WIDENING(BgL_tmpz00_7036);
						}
						BgL_auxz00_7034 = ((BgL_cstructz00_bglt) BgL_auxz00_7035);
					}
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7034))->
							BgL_cstructza2za2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_6967 = ((BgL_typez00_bglt) BgL_new1194z00_3384);
				return ((obj_t) BgL_auxz00_6967);
			}
		}

	}



/* &lambda2041 */
	BgL_typez00_bglt BGl_z62lambda2041z62zzforeign_ctypez00(obj_t BgL_envz00_3385,
		obj_t BgL_o1191z00_3386)
	{
		{	/* Foreign/ctype.scm 64 */
			{	/* Foreign/ctype.scm 64 */
				BgL_cstructz00_bglt BgL_wide1193z00_3807;

				BgL_wide1193z00_3807 =
					((BgL_cstructz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cstructz00_bgl))));
				{	/* Foreign/ctype.scm 64 */
					obj_t BgL_auxz00_7049;
					BgL_objectz00_bglt BgL_tmpz00_7045;

					BgL_auxz00_7049 = ((obj_t) BgL_wide1193z00_3807);
					BgL_tmpz00_7045 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1191z00_3386)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7045, BgL_auxz00_7049);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1191z00_3386)));
				{	/* Foreign/ctype.scm 64 */
					long BgL_arg2042z00_3808;

					BgL_arg2042z00_3808 = BGL_CLASS_NUM(BGl_cstructz00zzforeign_ctypez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1191z00_3386))), BgL_arg2042z00_3808);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1191z00_3386)));
			}
		}

	}



/* &lambda2038 */
	BgL_typez00_bglt BGl_z62lambda2038z62zzforeign_ctypez00(obj_t BgL_envz00_3387,
		obj_t BgL_id1172z00_3388, obj_t BgL_name1173z00_3389,
		obj_t BgL_siza7e1174za7_3390, obj_t BgL_class1175z00_3391,
		obj_t BgL_coercezd2to1176zd2_3392, obj_t BgL_parents1177z00_3393,
		obj_t BgL_initzf31178zf3_3394, obj_t BgL_magiczf31179zf3_3395,
		obj_t BgL_null1180z00_3396, obj_t BgL_z421181z42_3397,
		obj_t BgL_alias1182z00_3398, obj_t BgL_pointedzd2tozd2by1183z00_3399,
		obj_t BgL_tvector1184z00_3400, obj_t BgL_location1185z00_3401,
		obj_t BgL_importzd2location1186zd2_3402, obj_t BgL_occurrence1187z00_3403,
		obj_t BgL_structzf31188zf3_3404, obj_t BgL_fields1189z00_3405,
		obj_t BgL_cstructza21190za2_3406)
	{
		{	/* Foreign/ctype.scm 64 */
			{	/* Foreign/ctype.scm 64 */
				bool_t BgL_initzf31178zf3_3810;
				bool_t BgL_magiczf31179zf3_3811;
				int BgL_occurrence1187z00_3812;
				bool_t BgL_structzf31188zf3_3813;

				BgL_initzf31178zf3_3810 = CBOOL(BgL_initzf31178zf3_3394);
				BgL_magiczf31179zf3_3811 = CBOOL(BgL_magiczf31179zf3_3395);
				BgL_occurrence1187z00_3812 = CINT(BgL_occurrence1187z00_3403);
				BgL_structzf31188zf3_3813 = CBOOL(BgL_structzf31188zf3_3404);
				{	/* Foreign/ctype.scm 64 */
					BgL_typez00_bglt BgL_new1324z00_3814;

					{	/* Foreign/ctype.scm 64 */
						BgL_typez00_bglt BgL_tmp1322z00_3815;
						BgL_cstructz00_bglt BgL_wide1323z00_3816;

						{
							BgL_typez00_bglt BgL_auxz00_7067;

							{	/* Foreign/ctype.scm 64 */
								BgL_typez00_bglt BgL_new1321z00_3817;

								BgL_new1321z00_3817 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Foreign/ctype.scm 64 */
									long BgL_arg2040z00_3818;

									BgL_arg2040z00_3818 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1321z00_3817),
										BgL_arg2040z00_3818);
								}
								{	/* Foreign/ctype.scm 64 */
									BgL_objectz00_bglt BgL_tmpz00_7072;

									BgL_tmpz00_7072 = ((BgL_objectz00_bglt) BgL_new1321z00_3817);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7072, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1321z00_3817);
								BgL_auxz00_7067 = BgL_new1321z00_3817;
							}
							BgL_tmp1322z00_3815 = ((BgL_typez00_bglt) BgL_auxz00_7067);
						}
						BgL_wide1323z00_3816 =
							((BgL_cstructz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cstructz00_bgl))));
						{	/* Foreign/ctype.scm 64 */
							obj_t BgL_auxz00_7080;
							BgL_objectz00_bglt BgL_tmpz00_7078;

							BgL_auxz00_7080 = ((obj_t) BgL_wide1323z00_3816);
							BgL_tmpz00_7078 = ((BgL_objectz00_bglt) BgL_tmp1322z00_3815);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7078, BgL_auxz00_7080);
						}
						((BgL_objectz00_bglt) BgL_tmp1322z00_3815);
						{	/* Foreign/ctype.scm 64 */
							long BgL_arg2039z00_3819;

							BgL_arg2039z00_3819 =
								BGL_CLASS_NUM(BGl_cstructz00zzforeign_ctypez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1322z00_3815),
								BgL_arg2039z00_3819);
						}
						BgL_new1324z00_3814 = ((BgL_typez00_bglt) BgL_tmp1322z00_3815);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1324z00_3814)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1172z00_3388)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_namez00) =
						((obj_t) BgL_name1173z00_3389), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1174za7_3390), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_classz00) =
						((obj_t) BgL_class1175z00_3391), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1176zd2_3392), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_parentsz00) =
						((obj_t) BgL_parents1177z00_3393), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31178zf3_3810), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31179zf3_3811), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_nullz00) =
						((obj_t) BgL_null1180z00_3396), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_z42z42) =
						((obj_t) BgL_z421181z42_3397), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_aliasz00) =
						((obj_t) BgL_alias1182z00_3398), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1183z00_3399), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1184z00_3400), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_locationz00) =
						((obj_t) BgL_location1185z00_3401), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1186zd2_3402), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1324z00_3814)))->BgL_occurrencez00) =
						((int) BgL_occurrence1187z00_3812), BUNSPEC);
					{
						BgL_cstructz00_bglt BgL_auxz00_7121;

						{
							obj_t BgL_auxz00_7122;

							{	/* Foreign/ctype.scm 64 */
								BgL_objectz00_bglt BgL_tmpz00_7123;

								BgL_tmpz00_7123 = ((BgL_objectz00_bglt) BgL_new1324z00_3814);
								BgL_auxz00_7122 = BGL_OBJECT_WIDENING(BgL_tmpz00_7123);
							}
							BgL_auxz00_7121 = ((BgL_cstructz00_bglt) BgL_auxz00_7122);
						}
						((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7121))->
								BgL_structzf3zf3) =
							((bool_t) BgL_structzf31188zf3_3813), BUNSPEC);
					}
					{
						BgL_cstructz00_bglt BgL_auxz00_7128;

						{
							obj_t BgL_auxz00_7129;

							{	/* Foreign/ctype.scm 64 */
								BgL_objectz00_bglt BgL_tmpz00_7130;

								BgL_tmpz00_7130 = ((BgL_objectz00_bglt) BgL_new1324z00_3814);
								BgL_auxz00_7129 = BGL_OBJECT_WIDENING(BgL_tmpz00_7130);
							}
							BgL_auxz00_7128 = ((BgL_cstructz00_bglt) BgL_auxz00_7129);
						}
						((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7128))->BgL_fieldsz00) =
							((obj_t) BgL_fields1189z00_3405), BUNSPEC);
					}
					{
						BgL_cstructz00_bglt BgL_auxz00_7135;

						{
							obj_t BgL_auxz00_7136;

							{	/* Foreign/ctype.scm 64 */
								BgL_objectz00_bglt BgL_tmpz00_7137;

								BgL_tmpz00_7137 = ((BgL_objectz00_bglt) BgL_new1324z00_3814);
								BgL_auxz00_7136 = BGL_OBJECT_WIDENING(BgL_tmpz00_7137);
							}
							BgL_auxz00_7135 = ((BgL_cstructz00_bglt) BgL_auxz00_7136);
						}
						((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7135))->
								BgL_cstructza2za2) =
							((obj_t) BgL_cstructza21190za2_3406), BUNSPEC);
					}
					return BgL_new1324z00_3814;
				}
			}
		}

	}



/* &<@anonymous:2066> */
	obj_t BGl_z62zc3z04anonymousza32066ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3407)
	{
		{	/* Foreign/ctype.scm 64 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2065 */
	obj_t BGl_z62lambda2065z62zzforeign_ctypez00(obj_t BgL_envz00_3408,
		obj_t BgL_oz00_3409, obj_t BgL_vz00_3410)
	{
		{	/* Foreign/ctype.scm 64 */
			{
				BgL_cstructz00_bglt BgL_auxz00_7143;

				{
					obj_t BgL_auxz00_7144;

					{	/* Foreign/ctype.scm 64 */
						BgL_objectz00_bglt BgL_tmpz00_7145;

						BgL_tmpz00_7145 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3409));
						BgL_auxz00_7144 = BGL_OBJECT_WIDENING(BgL_tmpz00_7145);
					}
					BgL_auxz00_7143 = ((BgL_cstructz00_bglt) BgL_auxz00_7144);
				}
				return
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7143))->
						BgL_cstructza2za2) = ((obj_t) BgL_vz00_3410), BUNSPEC);
			}
		}

	}



/* &lambda2064 */
	obj_t BGl_z62lambda2064z62zzforeign_ctypez00(obj_t BgL_envz00_3411,
		obj_t BgL_oz00_3412)
	{
		{	/* Foreign/ctype.scm 64 */
			{
				BgL_cstructz00_bglt BgL_auxz00_7151;

				{
					obj_t BgL_auxz00_7152;

					{	/* Foreign/ctype.scm 64 */
						BgL_objectz00_bglt BgL_tmpz00_7153;

						BgL_tmpz00_7153 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3412));
						BgL_auxz00_7152 = BGL_OBJECT_WIDENING(BgL_tmpz00_7153);
					}
					BgL_auxz00_7151 = ((BgL_cstructz00_bglt) BgL_auxz00_7152);
				}
				return
					(((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7151))->BgL_cstructza2za2);
			}
		}

	}



/* &lambda2059 */
	obj_t BGl_z62lambda2059z62zzforeign_ctypez00(obj_t BgL_envz00_3413,
		obj_t BgL_oz00_3414, obj_t BgL_vz00_3415)
	{
		{	/* Foreign/ctype.scm 64 */
			{
				BgL_cstructz00_bglt BgL_auxz00_7159;

				{
					obj_t BgL_auxz00_7160;

					{	/* Foreign/ctype.scm 64 */
						BgL_objectz00_bglt BgL_tmpz00_7161;

						BgL_tmpz00_7161 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3414));
						BgL_auxz00_7160 = BGL_OBJECT_WIDENING(BgL_tmpz00_7161);
					}
					BgL_auxz00_7159 = ((BgL_cstructz00_bglt) BgL_auxz00_7160);
				}
				return
					((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7159))->BgL_fieldsz00) =
					((obj_t) BgL_vz00_3415), BUNSPEC);
			}
		}

	}



/* &lambda2058 */
	obj_t BGl_z62lambda2058z62zzforeign_ctypez00(obj_t BgL_envz00_3416,
		obj_t BgL_oz00_3417)
	{
		{	/* Foreign/ctype.scm 64 */
			{
				BgL_cstructz00_bglt BgL_auxz00_7167;

				{
					obj_t BgL_auxz00_7168;

					{	/* Foreign/ctype.scm 64 */
						BgL_objectz00_bglt BgL_tmpz00_7169;

						BgL_tmpz00_7169 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3417));
						BgL_auxz00_7168 = BGL_OBJECT_WIDENING(BgL_tmpz00_7169);
					}
					BgL_auxz00_7167 = ((BgL_cstructz00_bglt) BgL_auxz00_7168);
				}
				return
					(((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7167))->BgL_fieldsz00);
			}
		}

	}



/* &<@anonymous:2054> */
	obj_t BGl_z62zc3z04anonymousza32054ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3418)
	{
		{	/* Foreign/ctype.scm 64 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda2053 */
	obj_t BGl_z62lambda2053z62zzforeign_ctypez00(obj_t BgL_envz00_3419,
		obj_t BgL_oz00_3420, obj_t BgL_vz00_3421)
	{
		{	/* Foreign/ctype.scm 64 */
			{	/* Foreign/ctype.scm 64 */
				bool_t BgL_vz00_3825;

				BgL_vz00_3825 = CBOOL(BgL_vz00_3421);
				{
					BgL_cstructz00_bglt BgL_auxz00_7177;

					{
						obj_t BgL_auxz00_7178;

						{	/* Foreign/ctype.scm 64 */
							BgL_objectz00_bglt BgL_tmpz00_7179;

							BgL_tmpz00_7179 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3420));
							BgL_auxz00_7178 = BGL_OBJECT_WIDENING(BgL_tmpz00_7179);
						}
						BgL_auxz00_7177 = ((BgL_cstructz00_bglt) BgL_auxz00_7178);
					}
					return
						((((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7177))->
							BgL_structzf3zf3) = ((bool_t) BgL_vz00_3825), BUNSPEC);
				}
			}
		}

	}



/* &lambda2052 */
	obj_t BGl_z62lambda2052z62zzforeign_ctypez00(obj_t BgL_envz00_3422,
		obj_t BgL_oz00_3423)
	{
		{	/* Foreign/ctype.scm 64 */
			{	/* Foreign/ctype.scm 64 */
				bool_t BgL_tmpz00_7185;

				{
					BgL_cstructz00_bglt BgL_auxz00_7186;

					{
						obj_t BgL_auxz00_7187;

						{	/* Foreign/ctype.scm 64 */
							BgL_objectz00_bglt BgL_tmpz00_7188;

							BgL_tmpz00_7188 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3423));
							BgL_auxz00_7187 = BGL_OBJECT_WIDENING(BgL_tmpz00_7188);
						}
						BgL_auxz00_7186 = ((BgL_cstructz00_bglt) BgL_auxz00_7187);
					}
					BgL_tmpz00_7185 =
						(((BgL_cstructz00_bglt) COBJECT(BgL_auxz00_7186))->
						BgL_structzf3zf3);
				}
				return BBOOL(BgL_tmpz00_7185);
			}
		}

	}



/* &lambda2008 */
	BgL_typez00_bglt BGl_z62lambda2008z62zzforeign_ctypez00(obj_t BgL_envz00_3424,
		obj_t BgL_o1170z00_3425)
	{
		{	/* Foreign/ctype.scm 56 */
			{	/* Foreign/ctype.scm 56 */
				long BgL_arg2009z00_3828;

				{	/* Foreign/ctype.scm 56 */
					obj_t BgL_arg2010z00_3829;

					{	/* Foreign/ctype.scm 56 */
						obj_t BgL_arg2011z00_3830;

						{	/* Foreign/ctype.scm 56 */
							obj_t BgL_arg1815z00_3831;
							long BgL_arg1816z00_3832;

							BgL_arg1815z00_3831 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Foreign/ctype.scm 56 */
								long BgL_arg1817z00_3833;

								BgL_arg1817z00_3833 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1170z00_3425)));
								BgL_arg1816z00_3832 = (BgL_arg1817z00_3833 - OBJECT_TYPE);
							}
							BgL_arg2011z00_3830 =
								VECTOR_REF(BgL_arg1815z00_3831, BgL_arg1816z00_3832);
						}
						BgL_arg2010z00_3829 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2011z00_3830);
					}
					{	/* Foreign/ctype.scm 56 */
						obj_t BgL_tmpz00_7202;

						BgL_tmpz00_7202 = ((obj_t) BgL_arg2010z00_3829);
						BgL_arg2009z00_3828 = BGL_CLASS_NUM(BgL_tmpz00_7202);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1170z00_3425)), BgL_arg2009z00_3828);
			}
			{	/* Foreign/ctype.scm 56 */
				BgL_objectz00_bglt BgL_tmpz00_7208;

				BgL_tmpz00_7208 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1170z00_3425));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7208, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1170z00_3425));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1170z00_3425));
		}

	}



/* &<@anonymous:2007> */
	obj_t BGl_z62zc3z04anonymousza32007ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3426, obj_t BgL_new1169z00_3427)
	{
		{	/* Foreign/ctype.scm 56 */
			{
				BgL_typez00_bglt BgL_auxz00_7216;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1169z00_3427))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(44)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1169z00_3427))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_7274;
					BgL_cptrz00_bglt BgL_auxz00_7267;

					{	/* Foreign/ctype.scm 56 */
						obj_t BgL_classz00_3835;

						BgL_classz00_3835 = BGl_typez00zztype_typez00;
						{	/* Foreign/ctype.scm 56 */
							obj_t BgL__ortest_1117z00_3836;

							BgL__ortest_1117z00_3836 = BGL_CLASS_NIL(BgL_classz00_3835);
							if (CBOOL(BgL__ortest_1117z00_3836))
								{	/* Foreign/ctype.scm 56 */
									BgL_auxz00_7274 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3836);
								}
							else
								{	/* Foreign/ctype.scm 56 */
									BgL_auxz00_7274 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3835));
								}
						}
					}
					{
						obj_t BgL_auxz00_7268;

						{	/* Foreign/ctype.scm 56 */
							BgL_objectz00_bglt BgL_tmpz00_7269;

							BgL_tmpz00_7269 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1169z00_3427));
							BgL_auxz00_7268 = BGL_OBJECT_WIDENING(BgL_tmpz00_7269);
						}
						BgL_auxz00_7267 = ((BgL_cptrz00_bglt) BgL_auxz00_7268);
					}
					((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7267))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_auxz00_7274), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_7289;
					BgL_cptrz00_bglt BgL_auxz00_7282;

					{	/* Foreign/ctype.scm 56 */
						obj_t BgL_classz00_3837;

						BgL_classz00_3837 = BGl_typez00zztype_typez00;
						{	/* Foreign/ctype.scm 56 */
							obj_t BgL__ortest_1117z00_3838;

							BgL__ortest_1117z00_3838 = BGL_CLASS_NIL(BgL_classz00_3837);
							if (CBOOL(BgL__ortest_1117z00_3838))
								{	/* Foreign/ctype.scm 56 */
									BgL_auxz00_7289 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3838);
								}
							else
								{	/* Foreign/ctype.scm 56 */
									BgL_auxz00_7289 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3837));
								}
						}
					}
					{
						obj_t BgL_auxz00_7283;

						{	/* Foreign/ctype.scm 56 */
							BgL_objectz00_bglt BgL_tmpz00_7284;

							BgL_tmpz00_7284 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1169z00_3427));
							BgL_auxz00_7283 = BGL_OBJECT_WIDENING(BgL_tmpz00_7284);
						}
						BgL_auxz00_7282 = ((BgL_cptrz00_bglt) BgL_auxz00_7283);
					}
					((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7282))->BgL_pointzd2tozd2) =
						((BgL_typez00_bglt) BgL_auxz00_7289), BUNSPEC);
				}
				{
					BgL_cptrz00_bglt BgL_auxz00_7297;

					{
						obj_t BgL_auxz00_7298;

						{	/* Foreign/ctype.scm 56 */
							BgL_objectz00_bglt BgL_tmpz00_7299;

							BgL_tmpz00_7299 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1169z00_3427));
							BgL_auxz00_7298 = BGL_OBJECT_WIDENING(BgL_tmpz00_7299);
						}
						BgL_auxz00_7297 = ((BgL_cptrz00_bglt) BgL_auxz00_7298);
					}
					((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7297))->BgL_arrayzf3zf3) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_7216 = ((BgL_typez00_bglt) BgL_new1169z00_3427);
				return ((obj_t) BgL_auxz00_7216);
			}
		}

	}



/* &lambda2005 */
	BgL_typez00_bglt BGl_z62lambda2005z62zzforeign_ctypez00(obj_t BgL_envz00_3428,
		obj_t BgL_o1166z00_3429)
	{
		{	/* Foreign/ctype.scm 56 */
			{	/* Foreign/ctype.scm 56 */
				BgL_cptrz00_bglt BgL_wide1168z00_3840;

				BgL_wide1168z00_3840 =
					((BgL_cptrz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cptrz00_bgl))));
				{	/* Foreign/ctype.scm 56 */
					obj_t BgL_auxz00_7312;
					BgL_objectz00_bglt BgL_tmpz00_7308;

					BgL_auxz00_7312 = ((obj_t) BgL_wide1168z00_3840);
					BgL_tmpz00_7308 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1166z00_3429)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7308, BgL_auxz00_7312);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1166z00_3429)));
				{	/* Foreign/ctype.scm 56 */
					long BgL_arg2006z00_3841;

					BgL_arg2006z00_3841 = BGL_CLASS_NUM(BGl_cptrz00zzforeign_ctypez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1166z00_3429))), BgL_arg2006z00_3841);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1166z00_3429)));
			}
		}

	}



/* &lambda2002 */
	BgL_typez00_bglt BGl_z62lambda2002z62zzforeign_ctypez00(obj_t BgL_envz00_3430,
		obj_t BgL_id1147z00_3431, obj_t BgL_name1148z00_3432,
		obj_t BgL_siza7e1149za7_3433, obj_t BgL_class1150z00_3434,
		obj_t BgL_coercezd2to1151zd2_3435, obj_t BgL_parents1152z00_3436,
		obj_t BgL_initzf31153zf3_3437, obj_t BgL_magiczf31154zf3_3438,
		obj_t BgL_null1155z00_3439, obj_t BgL_z421156z42_3440,
		obj_t BgL_alias1157z00_3441, obj_t BgL_pointedzd2tozd2by1158z00_3442,
		obj_t BgL_tvector1159z00_3443, obj_t BgL_location1160z00_3444,
		obj_t BgL_importzd2location1161zd2_3445, obj_t BgL_occurrence1162z00_3446,
		obj_t BgL_btype1163z00_3447, obj_t BgL_pointzd2to1164zd2_3448,
		obj_t BgL_arrayzf31165zf3_3449)
	{
		{	/* Foreign/ctype.scm 56 */
			{	/* Foreign/ctype.scm 56 */
				bool_t BgL_initzf31153zf3_3843;
				bool_t BgL_magiczf31154zf3_3844;
				int BgL_occurrence1162z00_3845;
				bool_t BgL_arrayzf31165zf3_3848;

				BgL_initzf31153zf3_3843 = CBOOL(BgL_initzf31153zf3_3437);
				BgL_magiczf31154zf3_3844 = CBOOL(BgL_magiczf31154zf3_3438);
				BgL_occurrence1162z00_3845 = CINT(BgL_occurrence1162z00_3446);
				BgL_arrayzf31165zf3_3848 = CBOOL(BgL_arrayzf31165zf3_3449);
				{	/* Foreign/ctype.scm 56 */
					BgL_typez00_bglt BgL_new1319z00_3849;

					{	/* Foreign/ctype.scm 56 */
						BgL_typez00_bglt BgL_tmp1316z00_3850;
						BgL_cptrz00_bglt BgL_wide1317z00_3851;

						{
							BgL_typez00_bglt BgL_auxz00_7330;

							{	/* Foreign/ctype.scm 56 */
								BgL_typez00_bglt BgL_new1315z00_3852;

								BgL_new1315z00_3852 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Foreign/ctype.scm 56 */
									long BgL_arg2004z00_3853;

									BgL_arg2004z00_3853 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1315z00_3852),
										BgL_arg2004z00_3853);
								}
								{	/* Foreign/ctype.scm 56 */
									BgL_objectz00_bglt BgL_tmpz00_7335;

									BgL_tmpz00_7335 = ((BgL_objectz00_bglt) BgL_new1315z00_3852);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7335, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1315z00_3852);
								BgL_auxz00_7330 = BgL_new1315z00_3852;
							}
							BgL_tmp1316z00_3850 = ((BgL_typez00_bglt) BgL_auxz00_7330);
						}
						BgL_wide1317z00_3851 =
							((BgL_cptrz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cptrz00_bgl))));
						{	/* Foreign/ctype.scm 56 */
							obj_t BgL_auxz00_7343;
							BgL_objectz00_bglt BgL_tmpz00_7341;

							BgL_auxz00_7343 = ((obj_t) BgL_wide1317z00_3851);
							BgL_tmpz00_7341 = ((BgL_objectz00_bglt) BgL_tmp1316z00_3850);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7341, BgL_auxz00_7343);
						}
						((BgL_objectz00_bglt) BgL_tmp1316z00_3850);
						{	/* Foreign/ctype.scm 56 */
							long BgL_arg2003z00_3854;

							BgL_arg2003z00_3854 =
								BGL_CLASS_NUM(BGl_cptrz00zzforeign_ctypez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1316z00_3850),
								BgL_arg2003z00_3854);
						}
						BgL_new1319z00_3849 = ((BgL_typez00_bglt) BgL_tmp1316z00_3850);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1319z00_3849)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1147z00_3431)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_namez00) =
						((obj_t) BgL_name1148z00_3432), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1149za7_3433), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_classz00) =
						((obj_t) BgL_class1150z00_3434), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1151zd2_3435), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_parentsz00) =
						((obj_t) BgL_parents1152z00_3436), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31153zf3_3843), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31154zf3_3844), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_nullz00) =
						((obj_t) BgL_null1155z00_3439), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_z42z42) =
						((obj_t) BgL_z421156z42_3440), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_aliasz00) =
						((obj_t) BgL_alias1157z00_3441), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1158z00_3442), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1159z00_3443), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_locationz00) =
						((obj_t) BgL_location1160z00_3444), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1161zd2_3445), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1319z00_3849)))->BgL_occurrencez00) =
						((int) BgL_occurrence1162z00_3845), BUNSPEC);
					{
						BgL_cptrz00_bglt BgL_auxz00_7384;

						{
							obj_t BgL_auxz00_7385;

							{	/* Foreign/ctype.scm 56 */
								BgL_objectz00_bglt BgL_tmpz00_7386;

								BgL_tmpz00_7386 = ((BgL_objectz00_bglt) BgL_new1319z00_3849);
								BgL_auxz00_7385 = BGL_OBJECT_WIDENING(BgL_tmpz00_7386);
							}
							BgL_auxz00_7384 = ((BgL_cptrz00_bglt) BgL_auxz00_7385);
						}
						((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7384))->BgL_btypez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_btype1163z00_3447)),
							BUNSPEC);
					}
					{
						BgL_cptrz00_bglt BgL_auxz00_7392;

						{
							obj_t BgL_auxz00_7393;

							{	/* Foreign/ctype.scm 56 */
								BgL_objectz00_bglt BgL_tmpz00_7394;

								BgL_tmpz00_7394 = ((BgL_objectz00_bglt) BgL_new1319z00_3849);
								BgL_auxz00_7393 = BGL_OBJECT_WIDENING(BgL_tmpz00_7394);
							}
							BgL_auxz00_7392 = ((BgL_cptrz00_bglt) BgL_auxz00_7393);
						}
						((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7392))->
								BgL_pointzd2tozd2) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BgL_pointzd2to1164zd2_3448)), BUNSPEC);
					}
					{
						BgL_cptrz00_bglt BgL_auxz00_7400;

						{
							obj_t BgL_auxz00_7401;

							{	/* Foreign/ctype.scm 56 */
								BgL_objectz00_bglt BgL_tmpz00_7402;

								BgL_tmpz00_7402 = ((BgL_objectz00_bglt) BgL_new1319z00_3849);
								BgL_auxz00_7401 = BGL_OBJECT_WIDENING(BgL_tmpz00_7402);
							}
							BgL_auxz00_7400 = ((BgL_cptrz00_bglt) BgL_auxz00_7401);
						}
						((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7400))->BgL_arrayzf3zf3) =
							((bool_t) BgL_arrayzf31165zf3_3848), BUNSPEC);
					}
					return BgL_new1319z00_3849;
				}
			}
		}

	}



/* &<@anonymous:2029> */
	obj_t BGl_z62zc3z04anonymousza32029ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3450)
	{
		{	/* Foreign/ctype.scm 56 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda2028 */
	obj_t BGl_z62lambda2028z62zzforeign_ctypez00(obj_t BgL_envz00_3451,
		obj_t BgL_oz00_3452, obj_t BgL_vz00_3453)
	{
		{	/* Foreign/ctype.scm 56 */
			{	/* Foreign/ctype.scm 56 */
				bool_t BgL_vz00_3856;

				BgL_vz00_3856 = CBOOL(BgL_vz00_3453);
				{
					BgL_cptrz00_bglt BgL_auxz00_7409;

					{
						obj_t BgL_auxz00_7410;

						{	/* Foreign/ctype.scm 56 */
							BgL_objectz00_bglt BgL_tmpz00_7411;

							BgL_tmpz00_7411 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3452));
							BgL_auxz00_7410 = BGL_OBJECT_WIDENING(BgL_tmpz00_7411);
						}
						BgL_auxz00_7409 = ((BgL_cptrz00_bglt) BgL_auxz00_7410);
					}
					return
						((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7409))->BgL_arrayzf3zf3) =
						((bool_t) BgL_vz00_3856), BUNSPEC);
				}
			}
		}

	}



/* &lambda2027 */
	obj_t BGl_z62lambda2027z62zzforeign_ctypez00(obj_t BgL_envz00_3454,
		obj_t BgL_oz00_3455)
	{
		{	/* Foreign/ctype.scm 56 */
			{	/* Foreign/ctype.scm 56 */
				bool_t BgL_tmpz00_7417;

				{
					BgL_cptrz00_bglt BgL_auxz00_7418;

					{
						obj_t BgL_auxz00_7419;

						{	/* Foreign/ctype.scm 56 */
							BgL_objectz00_bglt BgL_tmpz00_7420;

							BgL_tmpz00_7420 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3455));
							BgL_auxz00_7419 = BGL_OBJECT_WIDENING(BgL_tmpz00_7420);
						}
						BgL_auxz00_7418 = ((BgL_cptrz00_bglt) BgL_auxz00_7419);
					}
					BgL_tmpz00_7417 =
						(((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7418))->BgL_arrayzf3zf3);
				}
				return BBOOL(BgL_tmpz00_7417);
			}
		}

	}



/* &lambda2021 */
	obj_t BGl_z62lambda2021z62zzforeign_ctypez00(obj_t BgL_envz00_3456,
		obj_t BgL_oz00_3457, obj_t BgL_vz00_3458)
	{
		{	/* Foreign/ctype.scm 56 */
			{
				BgL_cptrz00_bglt BgL_auxz00_7427;

				{
					obj_t BgL_auxz00_7428;

					{	/* Foreign/ctype.scm 56 */
						BgL_objectz00_bglt BgL_tmpz00_7429;

						BgL_tmpz00_7429 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3457));
						BgL_auxz00_7428 = BGL_OBJECT_WIDENING(BgL_tmpz00_7429);
					}
					BgL_auxz00_7427 = ((BgL_cptrz00_bglt) BgL_auxz00_7428);
				}
				return
					((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7427))->BgL_pointzd2tozd2) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3458)), BUNSPEC);
			}
		}

	}



/* &lambda2020 */
	BgL_typez00_bglt BGl_z62lambda2020z62zzforeign_ctypez00(obj_t BgL_envz00_3459,
		obj_t BgL_oz00_3460)
	{
		{	/* Foreign/ctype.scm 56 */
			{
				BgL_cptrz00_bglt BgL_auxz00_7436;

				{
					obj_t BgL_auxz00_7437;

					{	/* Foreign/ctype.scm 56 */
						BgL_objectz00_bglt BgL_tmpz00_7438;

						BgL_tmpz00_7438 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3460));
						BgL_auxz00_7437 = BGL_OBJECT_WIDENING(BgL_tmpz00_7438);
					}
					BgL_auxz00_7436 = ((BgL_cptrz00_bglt) BgL_auxz00_7437);
				}
				return
					(((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7436))->BgL_pointzd2tozd2);
			}
		}

	}



/* &lambda2016 */
	obj_t BGl_z62lambda2016z62zzforeign_ctypez00(obj_t BgL_envz00_3461,
		obj_t BgL_oz00_3462, obj_t BgL_vz00_3463)
	{
		{	/* Foreign/ctype.scm 56 */
			{
				BgL_cptrz00_bglt BgL_auxz00_7444;

				{
					obj_t BgL_auxz00_7445;

					{	/* Foreign/ctype.scm 56 */
						BgL_objectz00_bglt BgL_tmpz00_7446;

						BgL_tmpz00_7446 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3462));
						BgL_auxz00_7445 = BGL_OBJECT_WIDENING(BgL_tmpz00_7446);
					}
					BgL_auxz00_7444 = ((BgL_cptrz00_bglt) BgL_auxz00_7445);
				}
				return
					((((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7444))->BgL_btypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3463)), BUNSPEC);
			}
		}

	}



/* &lambda2015 */
	BgL_typez00_bglt BGl_z62lambda2015z62zzforeign_ctypez00(obj_t BgL_envz00_3464,
		obj_t BgL_oz00_3465)
	{
		{	/* Foreign/ctype.scm 56 */
			{
				BgL_cptrz00_bglt BgL_auxz00_7453;

				{
					obj_t BgL_auxz00_7454;

					{	/* Foreign/ctype.scm 56 */
						BgL_objectz00_bglt BgL_tmpz00_7455;

						BgL_tmpz00_7455 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3465));
						BgL_auxz00_7454 = BGL_OBJECT_WIDENING(BgL_tmpz00_7455);
					}
					BgL_auxz00_7453 = ((BgL_cptrz00_bglt) BgL_auxz00_7454);
				}
				return (((BgL_cptrz00_bglt) COBJECT(BgL_auxz00_7453))->BgL_btypez00);
			}
		}

	}



/* &lambda1972 */
	BgL_typez00_bglt BGl_z62lambda1972z62zzforeign_ctypez00(obj_t BgL_envz00_3466,
		obj_t BgL_o1145z00_3467)
	{
		{	/* Foreign/ctype.scm 46 */
			{	/* Foreign/ctype.scm 46 */
				long BgL_arg1973z00_3865;

				{	/* Foreign/ctype.scm 46 */
					obj_t BgL_arg1974z00_3866;

					{	/* Foreign/ctype.scm 46 */
						obj_t BgL_arg1975z00_3867;

						{	/* Foreign/ctype.scm 46 */
							obj_t BgL_arg1815z00_3868;
							long BgL_arg1816z00_3869;

							BgL_arg1815z00_3868 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Foreign/ctype.scm 46 */
								long BgL_arg1817z00_3870;

								BgL_arg1817z00_3870 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1145z00_3467)));
								BgL_arg1816z00_3869 = (BgL_arg1817z00_3870 - OBJECT_TYPE);
							}
							BgL_arg1975z00_3867 =
								VECTOR_REF(BgL_arg1815z00_3868, BgL_arg1816z00_3869);
						}
						BgL_arg1974z00_3866 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1975z00_3867);
					}
					{	/* Foreign/ctype.scm 46 */
						obj_t BgL_tmpz00_7468;

						BgL_tmpz00_7468 = ((obj_t) BgL_arg1974z00_3866);
						BgL_arg1973z00_3865 = BGL_CLASS_NUM(BgL_tmpz00_7468);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1145z00_3467)), BgL_arg1973z00_3865);
			}
			{	/* Foreign/ctype.scm 46 */
				BgL_objectz00_bglt BgL_tmpz00_7474;

				BgL_tmpz00_7474 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1145z00_3467));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7474, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1145z00_3467));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1145z00_3467));
		}

	}



/* &<@anonymous:1971> */
	obj_t BGl_z62zc3z04anonymousza31971ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3468, obj_t BgL_new1144z00_3469)
	{
		{	/* Foreign/ctype.scm 46 */
			{
				BgL_typez00_bglt BgL_auxz00_7482;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1144z00_3469))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(44)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1144z00_3469))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_7540;
					BgL_cfunctionz00_bglt BgL_auxz00_7533;

					{	/* Foreign/ctype.scm 46 */
						obj_t BgL_classz00_3872;

						BgL_classz00_3872 = BGl_typez00zztype_typez00;
						{	/* Foreign/ctype.scm 46 */
							obj_t BgL__ortest_1117z00_3873;

							BgL__ortest_1117z00_3873 = BGL_CLASS_NIL(BgL_classz00_3872);
							if (CBOOL(BgL__ortest_1117z00_3873))
								{	/* Foreign/ctype.scm 46 */
									BgL_auxz00_7540 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3873);
								}
							else
								{	/* Foreign/ctype.scm 46 */
									BgL_auxz00_7540 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3872));
								}
						}
					}
					{
						obj_t BgL_auxz00_7534;

						{	/* Foreign/ctype.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_7535;

							BgL_tmpz00_7535 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1144z00_3469));
							BgL_auxz00_7534 = BGL_OBJECT_WIDENING(BgL_tmpz00_7535);
						}
						BgL_auxz00_7533 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7534);
					}
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7533))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_auxz00_7540), BUNSPEC);
				}
				{
					BgL_cfunctionz00_bglt BgL_auxz00_7548;

					{
						obj_t BgL_auxz00_7549;

						{	/* Foreign/ctype.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_7550;

							BgL_tmpz00_7550 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1144z00_3469));
							BgL_auxz00_7549 = BGL_OBJECT_WIDENING(BgL_tmpz00_7550);
						}
						BgL_auxz00_7548 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7549);
					}
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7548))->BgL_arityz00) =
						((long) 0L), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_7563;
					BgL_cfunctionz00_bglt BgL_auxz00_7556;

					{	/* Foreign/ctype.scm 46 */
						obj_t BgL_classz00_3874;

						BgL_classz00_3874 = BGl_typez00zztype_typez00;
						{	/* Foreign/ctype.scm 46 */
							obj_t BgL__ortest_1117z00_3875;

							BgL__ortest_1117z00_3875 = BGL_CLASS_NIL(BgL_classz00_3874);
							if (CBOOL(BgL__ortest_1117z00_3875))
								{	/* Foreign/ctype.scm 46 */
									BgL_auxz00_7563 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3875);
								}
							else
								{	/* Foreign/ctype.scm 46 */
									BgL_auxz00_7563 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3874));
								}
						}
					}
					{
						obj_t BgL_auxz00_7557;

						{	/* Foreign/ctype.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_7558;

							BgL_tmpz00_7558 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1144z00_3469));
							BgL_auxz00_7557 = BGL_OBJECT_WIDENING(BgL_tmpz00_7558);
						}
						BgL_auxz00_7556 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7557);
					}
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7556))->
							BgL_typezd2reszd2) =
						((BgL_typez00_bglt) BgL_auxz00_7563), BUNSPEC);
				}
				{
					BgL_cfunctionz00_bglt BgL_auxz00_7571;

					{
						obj_t BgL_auxz00_7572;

						{	/* Foreign/ctype.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_7573;

							BgL_tmpz00_7573 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1144z00_3469));
							BgL_auxz00_7572 = BGL_OBJECT_WIDENING(BgL_tmpz00_7573);
						}
						BgL_auxz00_7571 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7572);
					}
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7571))->
							BgL_typezd2argszd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_7482 = ((BgL_typez00_bglt) BgL_new1144z00_3469);
				return ((obj_t) BgL_auxz00_7482);
			}
		}

	}



/* &lambda1969 */
	BgL_typez00_bglt BGl_z62lambda1969z62zzforeign_ctypez00(obj_t BgL_envz00_3470,
		obj_t BgL_o1141z00_3471)
	{
		{	/* Foreign/ctype.scm 46 */
			{	/* Foreign/ctype.scm 46 */
				BgL_cfunctionz00_bglt BgL_wide1143z00_3877;

				BgL_wide1143z00_3877 =
					((BgL_cfunctionz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cfunctionz00_bgl))));
				{	/* Foreign/ctype.scm 46 */
					obj_t BgL_auxz00_7586;
					BgL_objectz00_bglt BgL_tmpz00_7582;

					BgL_auxz00_7586 = ((obj_t) BgL_wide1143z00_3877);
					BgL_tmpz00_7582 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1141z00_3471)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7582, BgL_auxz00_7586);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1141z00_3471)));
				{	/* Foreign/ctype.scm 46 */
					long BgL_arg1970z00_3878;

					BgL_arg1970z00_3878 =
						BGL_CLASS_NUM(BGl_cfunctionz00zzforeign_ctypez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1141z00_3471))), BgL_arg1970z00_3878);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1141z00_3471)));
			}
		}

	}



/* &lambda1966 */
	BgL_typez00_bglt BGl_z62lambda1966z62zzforeign_ctypez00(obj_t BgL_envz00_3472,
		obj_t BgL_id1121z00_3473, obj_t BgL_name1122z00_3474,
		obj_t BgL_siza7e1123za7_3475, obj_t BgL_class1124z00_3476,
		obj_t BgL_coercezd2to1125zd2_3477, obj_t BgL_parents1126z00_3478,
		obj_t BgL_initzf31127zf3_3479, obj_t BgL_magiczf31128zf3_3480,
		obj_t BgL_null1129z00_3481, obj_t BgL_z421130z42_3482,
		obj_t BgL_alias1131z00_3483, obj_t BgL_pointedzd2tozd2by1132z00_3484,
		obj_t BgL_tvector1133z00_3485, obj_t BgL_location1134z00_3486,
		obj_t BgL_importzd2location1135zd2_3487, obj_t BgL_occurrence1136z00_3488,
		obj_t BgL_btype1137z00_3489, obj_t BgL_arity1138z00_3490,
		obj_t BgL_typezd2res1139zd2_3491, obj_t BgL_typezd2args1140zd2_3492)
	{
		{	/* Foreign/ctype.scm 46 */
			{	/* Foreign/ctype.scm 46 */
				bool_t BgL_initzf31127zf3_3880;
				bool_t BgL_magiczf31128zf3_3881;
				int BgL_occurrence1136z00_3882;
				long BgL_arity1138z00_3884;

				BgL_initzf31127zf3_3880 = CBOOL(BgL_initzf31127zf3_3479);
				BgL_magiczf31128zf3_3881 = CBOOL(BgL_magiczf31128zf3_3480);
				BgL_occurrence1136z00_3882 = CINT(BgL_occurrence1136z00_3488);
				BgL_arity1138z00_3884 = (long) CINT(BgL_arity1138z00_3490);
				{	/* Foreign/ctype.scm 46 */
					BgL_typez00_bglt BgL_new1313z00_3886;

					{	/* Foreign/ctype.scm 46 */
						BgL_typez00_bglt BgL_tmp1310z00_3887;
						BgL_cfunctionz00_bglt BgL_wide1311z00_3888;

						{
							BgL_typez00_bglt BgL_auxz00_7604;

							{	/* Foreign/ctype.scm 46 */
								BgL_typez00_bglt BgL_new1309z00_3889;

								BgL_new1309z00_3889 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Foreign/ctype.scm 46 */
									long BgL_arg1968z00_3890;

									BgL_arg1968z00_3890 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1309z00_3889),
										BgL_arg1968z00_3890);
								}
								{	/* Foreign/ctype.scm 46 */
									BgL_objectz00_bglt BgL_tmpz00_7609;

									BgL_tmpz00_7609 = ((BgL_objectz00_bglt) BgL_new1309z00_3889);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7609, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1309z00_3889);
								BgL_auxz00_7604 = BgL_new1309z00_3889;
							}
							BgL_tmp1310z00_3887 = ((BgL_typez00_bglt) BgL_auxz00_7604);
						}
						BgL_wide1311z00_3888 =
							((BgL_cfunctionz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cfunctionz00_bgl))));
						{	/* Foreign/ctype.scm 46 */
							obj_t BgL_auxz00_7617;
							BgL_objectz00_bglt BgL_tmpz00_7615;

							BgL_auxz00_7617 = ((obj_t) BgL_wide1311z00_3888);
							BgL_tmpz00_7615 = ((BgL_objectz00_bglt) BgL_tmp1310z00_3887);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7615, BgL_auxz00_7617);
						}
						((BgL_objectz00_bglt) BgL_tmp1310z00_3887);
						{	/* Foreign/ctype.scm 46 */
							long BgL_arg1967z00_3891;

							BgL_arg1967z00_3891 =
								BGL_CLASS_NUM(BGl_cfunctionz00zzforeign_ctypez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1310z00_3887),
								BgL_arg1967z00_3891);
						}
						BgL_new1313z00_3886 = ((BgL_typez00_bglt) BgL_tmp1310z00_3887);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1313z00_3886)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1121z00_3473)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_namez00) =
						((obj_t) BgL_name1122z00_3474), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1123za7_3475), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_classz00) =
						((obj_t) BgL_class1124z00_3476), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1125zd2_3477), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_parentsz00) =
						((obj_t) BgL_parents1126z00_3478), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31127zf3_3880), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31128zf3_3881), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_nullz00) =
						((obj_t) BgL_null1129z00_3481), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_z42z42) =
						((obj_t) BgL_z421130z42_3482), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_aliasz00) =
						((obj_t) BgL_alias1131z00_3483), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1132z00_3484), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1133z00_3485), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_locationz00) =
						((obj_t) BgL_location1134z00_3486), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1135zd2_3487), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1313z00_3886)))->BgL_occurrencez00) =
						((int) BgL_occurrence1136z00_3882), BUNSPEC);
					{
						BgL_cfunctionz00_bglt BgL_auxz00_7658;

						{
							obj_t BgL_auxz00_7659;

							{	/* Foreign/ctype.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_7660;

								BgL_tmpz00_7660 = ((BgL_objectz00_bglt) BgL_new1313z00_3886);
								BgL_auxz00_7659 = BGL_OBJECT_WIDENING(BgL_tmpz00_7660);
							}
							BgL_auxz00_7658 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7659);
						}
						((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7658))->
								BgL_btypez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_btype1137z00_3489)),
							BUNSPEC);
					}
					{
						BgL_cfunctionz00_bglt BgL_auxz00_7666;

						{
							obj_t BgL_auxz00_7667;

							{	/* Foreign/ctype.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_7668;

								BgL_tmpz00_7668 = ((BgL_objectz00_bglt) BgL_new1313z00_3886);
								BgL_auxz00_7667 = BGL_OBJECT_WIDENING(BgL_tmpz00_7668);
							}
							BgL_auxz00_7666 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7667);
						}
						((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7666))->
								BgL_arityz00) = ((long) BgL_arity1138z00_3884), BUNSPEC);
					}
					{
						BgL_cfunctionz00_bglt BgL_auxz00_7673;

						{
							obj_t BgL_auxz00_7674;

							{	/* Foreign/ctype.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_7675;

								BgL_tmpz00_7675 = ((BgL_objectz00_bglt) BgL_new1313z00_3886);
								BgL_auxz00_7674 = BGL_OBJECT_WIDENING(BgL_tmpz00_7675);
							}
							BgL_auxz00_7673 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7674);
						}
						((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7673))->
								BgL_typezd2reszd2) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BgL_typezd2res1139zd2_3491)), BUNSPEC);
					}
					{
						BgL_cfunctionz00_bglt BgL_auxz00_7681;

						{
							obj_t BgL_auxz00_7682;

							{	/* Foreign/ctype.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_7683;

								BgL_tmpz00_7683 = ((BgL_objectz00_bglt) BgL_new1313z00_3886);
								BgL_auxz00_7682 = BGL_OBJECT_WIDENING(BgL_tmpz00_7683);
							}
							BgL_auxz00_7681 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7682);
						}
						((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7681))->
								BgL_typezd2argszd2) =
							((obj_t) BgL_typezd2args1140zd2_3492), BUNSPEC);
					}
					return BgL_new1313z00_3886;
				}
			}
		}

	}



/* &lambda1995 */
	obj_t BGl_z62lambda1995z62zzforeign_ctypez00(obj_t BgL_envz00_3493,
		obj_t BgL_oz00_3494, obj_t BgL_vz00_3495)
	{
		{	/* Foreign/ctype.scm 46 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_7688;

				{
					obj_t BgL_auxz00_7689;

					{	/* Foreign/ctype.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_7690;

						BgL_tmpz00_7690 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3494));
						BgL_auxz00_7689 = BGL_OBJECT_WIDENING(BgL_tmpz00_7690);
					}
					BgL_auxz00_7688 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7689);
				}
				return
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7688))->
						BgL_typezd2argszd2) = ((obj_t) BgL_vz00_3495), BUNSPEC);
			}
		}

	}



/* &lambda1994 */
	obj_t BGl_z62lambda1994z62zzforeign_ctypez00(obj_t BgL_envz00_3496,
		obj_t BgL_oz00_3497)
	{
		{	/* Foreign/ctype.scm 46 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_7696;

				{
					obj_t BgL_auxz00_7697;

					{	/* Foreign/ctype.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_7698;

						BgL_tmpz00_7698 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3497));
						BgL_auxz00_7697 = BGL_OBJECT_WIDENING(BgL_tmpz00_7698);
					}
					BgL_auxz00_7696 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7697);
				}
				return
					(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7696))->
					BgL_typezd2argszd2);
			}
		}

	}



/* &lambda1990 */
	obj_t BGl_z62lambda1990z62zzforeign_ctypez00(obj_t BgL_envz00_3498,
		obj_t BgL_oz00_3499, obj_t BgL_vz00_3500)
	{
		{	/* Foreign/ctype.scm 46 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_7704;

				{
					obj_t BgL_auxz00_7705;

					{	/* Foreign/ctype.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_7706;

						BgL_tmpz00_7706 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3499));
						BgL_auxz00_7705 = BGL_OBJECT_WIDENING(BgL_tmpz00_7706);
					}
					BgL_auxz00_7704 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7705);
				}
				return
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7704))->
						BgL_typezd2reszd2) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3500)), BUNSPEC);
			}
		}

	}



/* &lambda1989 */
	BgL_typez00_bglt BGl_z62lambda1989z62zzforeign_ctypez00(obj_t BgL_envz00_3501,
		obj_t BgL_oz00_3502)
	{
		{	/* Foreign/ctype.scm 46 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_7713;

				{
					obj_t BgL_auxz00_7714;

					{	/* Foreign/ctype.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_7715;

						BgL_tmpz00_7715 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3502));
						BgL_auxz00_7714 = BGL_OBJECT_WIDENING(BgL_tmpz00_7715);
					}
					BgL_auxz00_7713 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7714);
				}
				return
					(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7713))->
					BgL_typezd2reszd2);
			}
		}

	}



/* &lambda1985 */
	obj_t BGl_z62lambda1985z62zzforeign_ctypez00(obj_t BgL_envz00_3503,
		obj_t BgL_oz00_3504, obj_t BgL_vz00_3505)
	{
		{	/* Foreign/ctype.scm 46 */
			{	/* Foreign/ctype.scm 46 */
				long BgL_vz00_3898;

				BgL_vz00_3898 = (long) CINT(BgL_vz00_3505);
				{
					BgL_cfunctionz00_bglt BgL_auxz00_7722;

					{
						obj_t BgL_auxz00_7723;

						{	/* Foreign/ctype.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_7724;

							BgL_tmpz00_7724 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3504));
							BgL_auxz00_7723 = BGL_OBJECT_WIDENING(BgL_tmpz00_7724);
						}
						BgL_auxz00_7722 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7723);
					}
					return
						((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7722))->
							BgL_arityz00) = ((long) BgL_vz00_3898), BUNSPEC);
		}}}

	}



/* &lambda1984 */
	obj_t BGl_z62lambda1984z62zzforeign_ctypez00(obj_t BgL_envz00_3506,
		obj_t BgL_oz00_3507)
	{
		{	/* Foreign/ctype.scm 46 */
			{	/* Foreign/ctype.scm 46 */
				long BgL_tmpz00_7730;

				{
					BgL_cfunctionz00_bglt BgL_auxz00_7731;

					{
						obj_t BgL_auxz00_7732;

						{	/* Foreign/ctype.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_7733;

							BgL_tmpz00_7733 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3507));
							BgL_auxz00_7732 = BGL_OBJECT_WIDENING(BgL_tmpz00_7733);
						}
						BgL_auxz00_7731 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7732);
					}
					BgL_tmpz00_7730 =
						(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7731))->BgL_arityz00);
				}
				return BINT(BgL_tmpz00_7730);
			}
		}

	}



/* &lambda1980 */
	obj_t BGl_z62lambda1980z62zzforeign_ctypez00(obj_t BgL_envz00_3508,
		obj_t BgL_oz00_3509, obj_t BgL_vz00_3510)
	{
		{	/* Foreign/ctype.scm 46 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_7740;

				{
					obj_t BgL_auxz00_7741;

					{	/* Foreign/ctype.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_7742;

						BgL_tmpz00_7742 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3509));
						BgL_auxz00_7741 = BGL_OBJECT_WIDENING(BgL_tmpz00_7742);
					}
					BgL_auxz00_7740 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7741);
				}
				return
					((((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7740))->BgL_btypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3510)), BUNSPEC);
			}
		}

	}



/* &lambda1979 */
	BgL_typez00_bglt BGl_z62lambda1979z62zzforeign_ctypez00(obj_t BgL_envz00_3511,
		obj_t BgL_oz00_3512)
	{
		{	/* Foreign/ctype.scm 46 */
			{
				BgL_cfunctionz00_bglt BgL_auxz00_7749;

				{
					obj_t BgL_auxz00_7750;

					{	/* Foreign/ctype.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_7751;

						BgL_tmpz00_7751 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3512));
						BgL_auxz00_7750 = BGL_OBJECT_WIDENING(BgL_tmpz00_7751);
					}
					BgL_auxz00_7749 = ((BgL_cfunctionz00_bglt) BgL_auxz00_7750);
				}
				return
					(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_7749))->BgL_btypez00);
			}
		}

	}



/* &lambda1951 */
	BgL_typez00_bglt BGl_z62lambda1951z62zzforeign_ctypez00(obj_t BgL_envz00_3513,
		obj_t BgL_o1119z00_3514)
	{
		{	/* Foreign/ctype.scm 42 */
			{	/* Foreign/ctype.scm 42 */
				long BgL_arg1952z00_3904;

				{	/* Foreign/ctype.scm 42 */
					obj_t BgL_arg1953z00_3905;

					{	/* Foreign/ctype.scm 42 */
						obj_t BgL_arg1954z00_3906;

						{	/* Foreign/ctype.scm 42 */
							obj_t BgL_arg1815z00_3907;
							long BgL_arg1816z00_3908;

							BgL_arg1815z00_3907 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Foreign/ctype.scm 42 */
								long BgL_arg1817z00_3909;

								BgL_arg1817z00_3909 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1119z00_3514)));
								BgL_arg1816z00_3908 = (BgL_arg1817z00_3909 - OBJECT_TYPE);
							}
							BgL_arg1954z00_3906 =
								VECTOR_REF(BgL_arg1815z00_3907, BgL_arg1816z00_3908);
						}
						BgL_arg1953z00_3905 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1954z00_3906);
					}
					{	/* Foreign/ctype.scm 42 */
						obj_t BgL_tmpz00_7764;

						BgL_tmpz00_7764 = ((obj_t) BgL_arg1953z00_3905);
						BgL_arg1952z00_3904 = BGL_CLASS_NUM(BgL_tmpz00_7764);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1119z00_3514)), BgL_arg1952z00_3904);
			}
			{	/* Foreign/ctype.scm 42 */
				BgL_objectz00_bglt BgL_tmpz00_7770;

				BgL_tmpz00_7770 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1119z00_3514));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7770, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1119z00_3514));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1119z00_3514));
		}

	}



/* &<@anonymous:1950> */
	obj_t BGl_z62zc3z04anonymousza31950ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3515, obj_t BgL_new1118z00_3516)
	{
		{	/* Foreign/ctype.scm 42 */
			{
				BgL_typez00_bglt BgL_auxz00_7778;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1118z00_3516))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(44)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1118z00_3516))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_7836;
					BgL_copaquez00_bglt BgL_auxz00_7829;

					{	/* Foreign/ctype.scm 42 */
						obj_t BgL_classz00_3911;

						BgL_classz00_3911 = BGl_typez00zztype_typez00;
						{	/* Foreign/ctype.scm 42 */
							obj_t BgL__ortest_1117z00_3912;

							BgL__ortest_1117z00_3912 = BGL_CLASS_NIL(BgL_classz00_3911);
							if (CBOOL(BgL__ortest_1117z00_3912))
								{	/* Foreign/ctype.scm 42 */
									BgL_auxz00_7836 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3912);
								}
							else
								{	/* Foreign/ctype.scm 42 */
									BgL_auxz00_7836 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3911));
								}
						}
					}
					{
						obj_t BgL_auxz00_7830;

						{	/* Foreign/ctype.scm 42 */
							BgL_objectz00_bglt BgL_tmpz00_7831;

							BgL_tmpz00_7831 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1118z00_3516));
							BgL_auxz00_7830 = BGL_OBJECT_WIDENING(BgL_tmpz00_7831);
						}
						BgL_auxz00_7829 = ((BgL_copaquez00_bglt) BgL_auxz00_7830);
					}
					((((BgL_copaquez00_bglt) COBJECT(BgL_auxz00_7829))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_auxz00_7836), BUNSPEC);
				}
				BgL_auxz00_7778 = ((BgL_typez00_bglt) BgL_new1118z00_3516);
				return ((obj_t) BgL_auxz00_7778);
			}
		}

	}



/* &lambda1948 */
	BgL_typez00_bglt BGl_z62lambda1948z62zzforeign_ctypez00(obj_t BgL_envz00_3517,
		obj_t BgL_o1114z00_3518)
	{
		{	/* Foreign/ctype.scm 42 */
			{	/* Foreign/ctype.scm 42 */
				BgL_copaquez00_bglt BgL_wide1116z00_3914;

				BgL_wide1116z00_3914 =
					((BgL_copaquez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_copaquez00_bgl))));
				{	/* Foreign/ctype.scm 42 */
					obj_t BgL_auxz00_7851;
					BgL_objectz00_bglt BgL_tmpz00_7847;

					BgL_auxz00_7851 = ((obj_t) BgL_wide1116z00_3914);
					BgL_tmpz00_7847 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1114z00_3518)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7847, BgL_auxz00_7851);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1114z00_3518)));
				{	/* Foreign/ctype.scm 42 */
					long BgL_arg1949z00_3915;

					BgL_arg1949z00_3915 = BGL_CLASS_NUM(BGl_copaquez00zzforeign_ctypez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1114z00_3518))), BgL_arg1949z00_3915);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1114z00_3518)));
			}
		}

	}



/* &lambda1945 */
	BgL_typez00_bglt BGl_z62lambda1945z62zzforeign_ctypez00(obj_t BgL_envz00_3519,
		obj_t BgL_id1097z00_3520, obj_t BgL_name1098z00_3521,
		obj_t BgL_siza7e1099za7_3522, obj_t BgL_class1100z00_3523,
		obj_t BgL_coercezd2to1101zd2_3524, obj_t BgL_parents1102z00_3525,
		obj_t BgL_initzf31103zf3_3526, obj_t BgL_magiczf31104zf3_3527,
		obj_t BgL_null1105z00_3528, obj_t BgL_z421106z42_3529,
		obj_t BgL_alias1107z00_3530, obj_t BgL_pointedzd2tozd2by1108z00_3531,
		obj_t BgL_tvector1109z00_3532, obj_t BgL_location1110z00_3533,
		obj_t BgL_importzd2location1111zd2_3534, obj_t BgL_occurrence1112z00_3535,
		obj_t BgL_btype1113z00_3536)
	{
		{	/* Foreign/ctype.scm 42 */
			{	/* Foreign/ctype.scm 42 */
				bool_t BgL_initzf31103zf3_3917;
				bool_t BgL_magiczf31104zf3_3918;
				int BgL_occurrence1112z00_3919;

				BgL_initzf31103zf3_3917 = CBOOL(BgL_initzf31103zf3_3526);
				BgL_magiczf31104zf3_3918 = CBOOL(BgL_magiczf31104zf3_3527);
				BgL_occurrence1112z00_3919 = CINT(BgL_occurrence1112z00_3535);
				{	/* Foreign/ctype.scm 42 */
					BgL_typez00_bglt BgL_new1307z00_3921;

					{	/* Foreign/ctype.scm 42 */
						BgL_typez00_bglt BgL_tmp1304z00_3922;
						BgL_copaquez00_bglt BgL_wide1305z00_3923;

						{
							BgL_typez00_bglt BgL_auxz00_7868;

							{	/* Foreign/ctype.scm 42 */
								BgL_typez00_bglt BgL_new1303z00_3924;

								BgL_new1303z00_3924 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Foreign/ctype.scm 42 */
									long BgL_arg1947z00_3925;

									BgL_arg1947z00_3925 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1303z00_3924),
										BgL_arg1947z00_3925);
								}
								{	/* Foreign/ctype.scm 42 */
									BgL_objectz00_bglt BgL_tmpz00_7873;

									BgL_tmpz00_7873 = ((BgL_objectz00_bglt) BgL_new1303z00_3924);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7873, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1303z00_3924);
								BgL_auxz00_7868 = BgL_new1303z00_3924;
							}
							BgL_tmp1304z00_3922 = ((BgL_typez00_bglt) BgL_auxz00_7868);
						}
						BgL_wide1305z00_3923 =
							((BgL_copaquez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_copaquez00_bgl))));
						{	/* Foreign/ctype.scm 42 */
							obj_t BgL_auxz00_7881;
							BgL_objectz00_bglt BgL_tmpz00_7879;

							BgL_auxz00_7881 = ((obj_t) BgL_wide1305z00_3923);
							BgL_tmpz00_7879 = ((BgL_objectz00_bglt) BgL_tmp1304z00_3922);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7879, BgL_auxz00_7881);
						}
						((BgL_objectz00_bglt) BgL_tmp1304z00_3922);
						{	/* Foreign/ctype.scm 42 */
							long BgL_arg1946z00_3926;

							BgL_arg1946z00_3926 =
								BGL_CLASS_NUM(BGl_copaquez00zzforeign_ctypez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1304z00_3922),
								BgL_arg1946z00_3926);
						}
						BgL_new1307z00_3921 = ((BgL_typez00_bglt) BgL_tmp1304z00_3922);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1307z00_3921)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1097z00_3520)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_namez00) =
						((obj_t) BgL_name1098z00_3521), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1099za7_3522), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_classz00) =
						((obj_t) BgL_class1100z00_3523), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1101zd2_3524), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_parentsz00) =
						((obj_t) BgL_parents1102z00_3525), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31103zf3_3917), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31104zf3_3918), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_nullz00) =
						((obj_t) BgL_null1105z00_3528), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_z42z42) =
						((obj_t) BgL_z421106z42_3529), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_aliasz00) =
						((obj_t) BgL_alias1107z00_3530), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1108z00_3531), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1109z00_3532), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_locationz00) =
						((obj_t) BgL_location1110z00_3533), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1111zd2_3534), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1307z00_3921)))->BgL_occurrencez00) =
						((int) BgL_occurrence1112z00_3919), BUNSPEC);
					{
						BgL_copaquez00_bglt BgL_auxz00_7922;

						{
							obj_t BgL_auxz00_7923;

							{	/* Foreign/ctype.scm 42 */
								BgL_objectz00_bglt BgL_tmpz00_7924;

								BgL_tmpz00_7924 = ((BgL_objectz00_bglt) BgL_new1307z00_3921);
								BgL_auxz00_7923 = BGL_OBJECT_WIDENING(BgL_tmpz00_7924);
							}
							BgL_auxz00_7922 = ((BgL_copaquez00_bglt) BgL_auxz00_7923);
						}
						((((BgL_copaquez00_bglt) COBJECT(BgL_auxz00_7922))->BgL_btypez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_btype1113z00_3536)),
							BUNSPEC);
					}
					return BgL_new1307z00_3921;
				}
			}
		}

	}



/* &lambda1959 */
	obj_t BGl_z62lambda1959z62zzforeign_ctypez00(obj_t BgL_envz00_3537,
		obj_t BgL_oz00_3538, obj_t BgL_vz00_3539)
	{
		{	/* Foreign/ctype.scm 42 */
			{
				BgL_copaquez00_bglt BgL_auxz00_7930;

				{
					obj_t BgL_auxz00_7931;

					{	/* Foreign/ctype.scm 42 */
						BgL_objectz00_bglt BgL_tmpz00_7932;

						BgL_tmpz00_7932 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3538));
						BgL_auxz00_7931 = BGL_OBJECT_WIDENING(BgL_tmpz00_7932);
					}
					BgL_auxz00_7930 = ((BgL_copaquez00_bglt) BgL_auxz00_7931);
				}
				return
					((((BgL_copaquez00_bglt) COBJECT(BgL_auxz00_7930))->BgL_btypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3539)), BUNSPEC);
			}
		}

	}



/* &lambda1958 */
	BgL_typez00_bglt BGl_z62lambda1958z62zzforeign_ctypez00(obj_t BgL_envz00_3540,
		obj_t BgL_oz00_3541)
	{
		{	/* Foreign/ctype.scm 42 */
			{
				BgL_copaquez00_bglt BgL_auxz00_7939;

				{
					obj_t BgL_auxz00_7940;

					{	/* Foreign/ctype.scm 42 */
						BgL_objectz00_bglt BgL_tmpz00_7941;

						BgL_tmpz00_7941 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3541));
						BgL_auxz00_7940 = BGL_OBJECT_WIDENING(BgL_tmpz00_7941);
					}
					BgL_auxz00_7939 = ((BgL_copaquez00_bglt) BgL_auxz00_7940);
				}
				return (((BgL_copaquez00_bglt) COBJECT(BgL_auxz00_7939))->BgL_btypez00);
			}
		}

	}



/* &lambda1925 */
	BgL_typez00_bglt BGl_z62lambda1925z62zzforeign_ctypez00(obj_t BgL_envz00_3542,
		obj_t BgL_o1095z00_3543)
	{
		{	/* Foreign/ctype.scm 36 */
			{	/* Foreign/ctype.scm 36 */
				long BgL_arg1926z00_3931;

				{	/* Foreign/ctype.scm 36 */
					obj_t BgL_arg1927z00_3932;

					{	/* Foreign/ctype.scm 36 */
						obj_t BgL_arg1928z00_3933;

						{	/* Foreign/ctype.scm 36 */
							obj_t BgL_arg1815z00_3934;
							long BgL_arg1816z00_3935;

							BgL_arg1815z00_3934 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Foreign/ctype.scm 36 */
								long BgL_arg1817z00_3936;

								BgL_arg1817z00_3936 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1095z00_3543)));
								BgL_arg1816z00_3935 = (BgL_arg1817z00_3936 - OBJECT_TYPE);
							}
							BgL_arg1928z00_3933 =
								VECTOR_REF(BgL_arg1815z00_3934, BgL_arg1816z00_3935);
						}
						BgL_arg1927z00_3932 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1928z00_3933);
					}
					{	/* Foreign/ctype.scm 36 */
						obj_t BgL_tmpz00_7954;

						BgL_tmpz00_7954 = ((obj_t) BgL_arg1927z00_3932);
						BgL_arg1926z00_3931 = BGL_CLASS_NUM(BgL_tmpz00_7954);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1095z00_3543)), BgL_arg1926z00_3931);
			}
			{	/* Foreign/ctype.scm 36 */
				BgL_objectz00_bglt BgL_tmpz00_7960;

				BgL_tmpz00_7960 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1095z00_3543));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7960, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1095z00_3543));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1095z00_3543));
		}

	}



/* &<@anonymous:1924> */
	obj_t BGl_z62zc3z04anonymousza31924ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3544, obj_t BgL_new1094z00_3545)
	{
		{	/* Foreign/ctype.scm 36 */
			{
				BgL_typez00_bglt BgL_auxz00_7968;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1094z00_3545))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(44)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1094z00_3545))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_8026;
					BgL_cenumz00_bglt BgL_auxz00_8019;

					{	/* Foreign/ctype.scm 36 */
						obj_t BgL_classz00_3938;

						BgL_classz00_3938 = BGl_typez00zztype_typez00;
						{	/* Foreign/ctype.scm 36 */
							obj_t BgL__ortest_1117z00_3939;

							BgL__ortest_1117z00_3939 = BGL_CLASS_NIL(BgL_classz00_3938);
							if (CBOOL(BgL__ortest_1117z00_3939))
								{	/* Foreign/ctype.scm 36 */
									BgL_auxz00_8026 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3939);
								}
							else
								{	/* Foreign/ctype.scm 36 */
									BgL_auxz00_8026 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3938));
								}
						}
					}
					{
						obj_t BgL_auxz00_8020;

						{	/* Foreign/ctype.scm 36 */
							BgL_objectz00_bglt BgL_tmpz00_8021;

							BgL_tmpz00_8021 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3545));
							BgL_auxz00_8020 = BGL_OBJECT_WIDENING(BgL_tmpz00_8021);
						}
						BgL_auxz00_8019 = ((BgL_cenumz00_bglt) BgL_auxz00_8020);
					}
					((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_8019))->BgL_btypez00) =
						((BgL_typez00_bglt) BgL_auxz00_8026), BUNSPEC);
				}
				{
					BgL_cenumz00_bglt BgL_auxz00_8034;

					{
						obj_t BgL_auxz00_8035;

						{	/* Foreign/ctype.scm 36 */
							BgL_objectz00_bglt BgL_tmpz00_8036;

							BgL_tmpz00_8036 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1094z00_3545));
							BgL_auxz00_8035 = BGL_OBJECT_WIDENING(BgL_tmpz00_8036);
						}
						BgL_auxz00_8034 = ((BgL_cenumz00_bglt) BgL_auxz00_8035);
					}
					((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_8034))->BgL_literalsz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_7968 = ((BgL_typez00_bglt) BgL_new1094z00_3545);
				return ((obj_t) BgL_auxz00_7968);
			}
		}

	}



/* &lambda1920 */
	BgL_typez00_bglt BGl_z62lambda1920z62zzforeign_ctypez00(obj_t BgL_envz00_3546,
		obj_t BgL_o1091z00_3547)
	{
		{	/* Foreign/ctype.scm 36 */
			{	/* Foreign/ctype.scm 36 */
				BgL_cenumz00_bglt BgL_wide1093z00_3941;

				BgL_wide1093z00_3941 =
					((BgL_cenumz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cenumz00_bgl))));
				{	/* Foreign/ctype.scm 36 */
					obj_t BgL_auxz00_8049;
					BgL_objectz00_bglt BgL_tmpz00_8045;

					BgL_auxz00_8049 = ((obj_t) BgL_wide1093z00_3941);
					BgL_tmpz00_8045 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1091z00_3547)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8045, BgL_auxz00_8049);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1091z00_3547)));
				{	/* Foreign/ctype.scm 36 */
					long BgL_arg1923z00_3942;

					BgL_arg1923z00_3942 = BGL_CLASS_NUM(BGl_cenumz00zzforeign_ctypez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1091z00_3547))), BgL_arg1923z00_3942);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1091z00_3547)));
			}
		}

	}



/* &lambda1917 */
	BgL_typez00_bglt BGl_z62lambda1917z62zzforeign_ctypez00(obj_t BgL_envz00_3548,
		obj_t BgL_id1073z00_3549, obj_t BgL_name1074z00_3550,
		obj_t BgL_siza7e1075za7_3551, obj_t BgL_class1076z00_3552,
		obj_t BgL_coercezd2to1077zd2_3553, obj_t BgL_parents1078z00_3554,
		obj_t BgL_initzf31079zf3_3555, obj_t BgL_magiczf31080zf3_3556,
		obj_t BgL_null1081z00_3557, obj_t BgL_z421082z42_3558,
		obj_t BgL_alias1083z00_3559, obj_t BgL_pointedzd2tozd2by1084z00_3560,
		obj_t BgL_tvector1085z00_3561, obj_t BgL_location1086z00_3562,
		obj_t BgL_importzd2location1087zd2_3563, obj_t BgL_occurrence1088z00_3564,
		obj_t BgL_btype1089z00_3565, obj_t BgL_literals1090z00_3566)
	{
		{	/* Foreign/ctype.scm 36 */
			{	/* Foreign/ctype.scm 36 */
				bool_t BgL_initzf31079zf3_3944;
				bool_t BgL_magiczf31080zf3_3945;
				int BgL_occurrence1088z00_3946;

				BgL_initzf31079zf3_3944 = CBOOL(BgL_initzf31079zf3_3555);
				BgL_magiczf31080zf3_3945 = CBOOL(BgL_magiczf31080zf3_3556);
				BgL_occurrence1088z00_3946 = CINT(BgL_occurrence1088z00_3564);
				{	/* Foreign/ctype.scm 36 */
					BgL_typez00_bglt BgL_new1300z00_3948;

					{	/* Foreign/ctype.scm 36 */
						BgL_typez00_bglt BgL_tmp1298z00_3949;
						BgL_cenumz00_bglt BgL_wide1299z00_3950;

						{
							BgL_typez00_bglt BgL_auxz00_8066;

							{	/* Foreign/ctype.scm 36 */
								BgL_typez00_bglt BgL_new1297z00_3951;

								BgL_new1297z00_3951 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Foreign/ctype.scm 36 */
									long BgL_arg1919z00_3952;

									BgL_arg1919z00_3952 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1297z00_3951),
										BgL_arg1919z00_3952);
								}
								{	/* Foreign/ctype.scm 36 */
									BgL_objectz00_bglt BgL_tmpz00_8071;

									BgL_tmpz00_8071 = ((BgL_objectz00_bglt) BgL_new1297z00_3951);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8071, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1297z00_3951);
								BgL_auxz00_8066 = BgL_new1297z00_3951;
							}
							BgL_tmp1298z00_3949 = ((BgL_typez00_bglt) BgL_auxz00_8066);
						}
						BgL_wide1299z00_3950 =
							((BgL_cenumz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_cenumz00_bgl))));
						{	/* Foreign/ctype.scm 36 */
							obj_t BgL_auxz00_8079;
							BgL_objectz00_bglt BgL_tmpz00_8077;

							BgL_auxz00_8079 = ((obj_t) BgL_wide1299z00_3950);
							BgL_tmpz00_8077 = ((BgL_objectz00_bglt) BgL_tmp1298z00_3949);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8077, BgL_auxz00_8079);
						}
						((BgL_objectz00_bglt) BgL_tmp1298z00_3949);
						{	/* Foreign/ctype.scm 36 */
							long BgL_arg1918z00_3953;

							BgL_arg1918z00_3953 =
								BGL_CLASS_NUM(BGl_cenumz00zzforeign_ctypez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1298z00_3949),
								BgL_arg1918z00_3953);
						}
						BgL_new1300z00_3948 = ((BgL_typez00_bglt) BgL_tmp1298z00_3949);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1300z00_3948)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1073z00_3549)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_namez00) =
						((obj_t) BgL_name1074z00_3550), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1075za7_3551), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_classz00) =
						((obj_t) BgL_class1076z00_3552), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1077zd2_3553), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_parentsz00) =
						((obj_t) BgL_parents1078z00_3554), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31079zf3_3944), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31080zf3_3945), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_nullz00) =
						((obj_t) BgL_null1081z00_3557), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_z42z42) =
						((obj_t) BgL_z421082z42_3558), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_aliasz00) =
						((obj_t) BgL_alias1083z00_3559), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1084z00_3560), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1085z00_3561), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_locationz00) =
						((obj_t) BgL_location1086z00_3562), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1087zd2_3563), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1300z00_3948)))->BgL_occurrencez00) =
						((int) BgL_occurrence1088z00_3946), BUNSPEC);
					{
						BgL_cenumz00_bglt BgL_auxz00_8120;

						{
							obj_t BgL_auxz00_8121;

							{	/* Foreign/ctype.scm 36 */
								BgL_objectz00_bglt BgL_tmpz00_8122;

								BgL_tmpz00_8122 = ((BgL_objectz00_bglt) BgL_new1300z00_3948);
								BgL_auxz00_8121 = BGL_OBJECT_WIDENING(BgL_tmpz00_8122);
							}
							BgL_auxz00_8120 = ((BgL_cenumz00_bglt) BgL_auxz00_8121);
						}
						((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_8120))->BgL_btypez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_btype1089z00_3565)),
							BUNSPEC);
					}
					{
						BgL_cenumz00_bglt BgL_auxz00_8128;

						{
							obj_t BgL_auxz00_8129;

							{	/* Foreign/ctype.scm 36 */
								BgL_objectz00_bglt BgL_tmpz00_8130;

								BgL_tmpz00_8130 = ((BgL_objectz00_bglt) BgL_new1300z00_3948);
								BgL_auxz00_8129 = BGL_OBJECT_WIDENING(BgL_tmpz00_8130);
							}
							BgL_auxz00_8128 = ((BgL_cenumz00_bglt) BgL_auxz00_8129);
						}
						((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_8128))->BgL_literalsz00) =
							((obj_t) BgL_literals1090z00_3566), BUNSPEC);
					}
					return BgL_new1300z00_3948;
				}
			}
		}

	}



/* &lambda1938 */
	obj_t BGl_z62lambda1938z62zzforeign_ctypez00(obj_t BgL_envz00_3567,
		obj_t BgL_oz00_3568, obj_t BgL_vz00_3569)
	{
		{	/* Foreign/ctype.scm 36 */
			{
				BgL_cenumz00_bglt BgL_auxz00_8135;

				{
					obj_t BgL_auxz00_8136;

					{	/* Foreign/ctype.scm 36 */
						BgL_objectz00_bglt BgL_tmpz00_8137;

						BgL_tmpz00_8137 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3568));
						BgL_auxz00_8136 = BGL_OBJECT_WIDENING(BgL_tmpz00_8137);
					}
					BgL_auxz00_8135 = ((BgL_cenumz00_bglt) BgL_auxz00_8136);
				}
				return
					((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_8135))->BgL_literalsz00) =
					((obj_t) BgL_vz00_3569), BUNSPEC);
			}
		}

	}



/* &lambda1937 */
	obj_t BGl_z62lambda1937z62zzforeign_ctypez00(obj_t BgL_envz00_3570,
		obj_t BgL_oz00_3571)
	{
		{	/* Foreign/ctype.scm 36 */
			{
				BgL_cenumz00_bglt BgL_auxz00_8143;

				{
					obj_t BgL_auxz00_8144;

					{	/* Foreign/ctype.scm 36 */
						BgL_objectz00_bglt BgL_tmpz00_8145;

						BgL_tmpz00_8145 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3571));
						BgL_auxz00_8144 = BGL_OBJECT_WIDENING(BgL_tmpz00_8145);
					}
					BgL_auxz00_8143 = ((BgL_cenumz00_bglt) BgL_auxz00_8144);
				}
				return
					(((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_8143))->BgL_literalsz00);
			}
		}

	}



/* &lambda1933 */
	obj_t BGl_z62lambda1933z62zzforeign_ctypez00(obj_t BgL_envz00_3572,
		obj_t BgL_oz00_3573, obj_t BgL_vz00_3574)
	{
		{	/* Foreign/ctype.scm 36 */
			{
				BgL_cenumz00_bglt BgL_auxz00_8151;

				{
					obj_t BgL_auxz00_8152;

					{	/* Foreign/ctype.scm 36 */
						BgL_objectz00_bglt BgL_tmpz00_8153;

						BgL_tmpz00_8153 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3573));
						BgL_auxz00_8152 = BGL_OBJECT_WIDENING(BgL_tmpz00_8153);
					}
					BgL_auxz00_8151 = ((BgL_cenumz00_bglt) BgL_auxz00_8152);
				}
				return
					((((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_8151))->BgL_btypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_3574)), BUNSPEC);
			}
		}

	}



/* &lambda1932 */
	BgL_typez00_bglt BGl_z62lambda1932z62zzforeign_ctypez00(obj_t BgL_envz00_3575,
		obj_t BgL_oz00_3576)
	{
		{	/* Foreign/ctype.scm 36 */
			{
				BgL_cenumz00_bglt BgL_auxz00_8160;

				{
					obj_t BgL_auxz00_8161;

					{	/* Foreign/ctype.scm 36 */
						BgL_objectz00_bglt BgL_tmpz00_8162;

						BgL_tmpz00_8162 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3576));
						BgL_auxz00_8161 = BGL_OBJECT_WIDENING(BgL_tmpz00_8162);
					}
					BgL_auxz00_8160 = ((BgL_cenumz00_bglt) BgL_auxz00_8161);
				}
				return (((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_8160))->BgL_btypez00);
			}
		}

	}



/* &lambda1896 */
	BgL_typez00_bglt BGl_z62lambda1896z62zzforeign_ctypez00(obj_t BgL_envz00_3577,
		obj_t BgL_o1071z00_3578)
	{
		{	/* Foreign/ctype.scm 32 */
			{	/* Foreign/ctype.scm 32 */
				long BgL_arg1897z00_3960;

				{	/* Foreign/ctype.scm 32 */
					obj_t BgL_arg1898z00_3961;

					{	/* Foreign/ctype.scm 32 */
						obj_t BgL_arg1899z00_3962;

						{	/* Foreign/ctype.scm 32 */
							obj_t BgL_arg1815z00_3963;
							long BgL_arg1816z00_3964;

							BgL_arg1815z00_3963 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Foreign/ctype.scm 32 */
								long BgL_arg1817z00_3965;

								BgL_arg1817z00_3965 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_typez00_bglt) BgL_o1071z00_3578)));
								BgL_arg1816z00_3964 = (BgL_arg1817z00_3965 - OBJECT_TYPE);
							}
							BgL_arg1899z00_3962 =
								VECTOR_REF(BgL_arg1815z00_3963, BgL_arg1816z00_3964);
						}
						BgL_arg1898z00_3961 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1899z00_3962);
					}
					{	/* Foreign/ctype.scm 32 */
						obj_t BgL_tmpz00_8175;

						BgL_tmpz00_8175 = ((obj_t) BgL_arg1898z00_3961);
						BgL_arg1897z00_3960 = BGL_CLASS_NUM(BgL_tmpz00_8175);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1071z00_3578)), BgL_arg1897z00_3960);
			}
			{	/* Foreign/ctype.scm 32 */
				BgL_objectz00_bglt BgL_tmpz00_8181;

				BgL_tmpz00_8181 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1071z00_3578));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8181, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1071z00_3578));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1071z00_3578));
		}

	}



/* &<@anonymous:1895> */
	obj_t BGl_z62zc3z04anonymousza31895ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3579, obj_t BgL_new1070z00_3580)
	{
		{	/* Foreign/ctype.scm 32 */
			{
				BgL_typez00_bglt BgL_auxz00_8189;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1070z00_3580))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(44)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_3580))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_caliasz00_bglt BgL_auxz00_8240;

					{
						obj_t BgL_auxz00_8241;

						{	/* Foreign/ctype.scm 32 */
							BgL_objectz00_bglt BgL_tmpz00_8242;

							BgL_tmpz00_8242 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1070z00_3580));
							BgL_auxz00_8241 = BGL_OBJECT_WIDENING(BgL_tmpz00_8242);
						}
						BgL_auxz00_8240 = ((BgL_caliasz00_bglt) BgL_auxz00_8241);
					}
					((((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_8240))->BgL_arrayzf3zf3) =
						((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_8189 = ((BgL_typez00_bglt) BgL_new1070z00_3580);
				return ((obj_t) BgL_auxz00_8189);
			}
		}

	}



/* &lambda1893 */
	BgL_typez00_bglt BGl_z62lambda1893z62zzforeign_ctypez00(obj_t BgL_envz00_3581,
		obj_t BgL_o1067z00_3582)
	{
		{	/* Foreign/ctype.scm 32 */
			{	/* Foreign/ctype.scm 32 */
				BgL_caliasz00_bglt BgL_wide1069z00_3968;

				BgL_wide1069z00_3968 =
					((BgL_caliasz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_caliasz00_bgl))));
				{	/* Foreign/ctype.scm 32 */
					obj_t BgL_auxz00_8255;
					BgL_objectz00_bglt BgL_tmpz00_8251;

					BgL_auxz00_8255 = ((obj_t) BgL_wide1069z00_3968);
					BgL_tmpz00_8251 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1067z00_3582)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8251, BgL_auxz00_8255);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1067z00_3582)));
				{	/* Foreign/ctype.scm 32 */
					long BgL_arg1894z00_3969;

					BgL_arg1894z00_3969 = BGL_CLASS_NUM(BGl_caliasz00zzforeign_ctypez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1067z00_3582))), BgL_arg1894z00_3969);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1067z00_3582)));
			}
		}

	}



/* &lambda1890 */
	BgL_typez00_bglt BGl_z62lambda1890z62zzforeign_ctypez00(obj_t BgL_envz00_3583,
		obj_t BgL_id1050z00_3584, obj_t BgL_name1051z00_3585,
		obj_t BgL_siza7e1052za7_3586, obj_t BgL_class1053z00_3587,
		obj_t BgL_coercezd2to1054zd2_3588, obj_t BgL_parents1055z00_3589,
		obj_t BgL_initzf31056zf3_3590, obj_t BgL_magiczf31057zf3_3591,
		obj_t BgL_null1058z00_3592, obj_t BgL_z421059z42_3593,
		obj_t BgL_alias1060z00_3594, obj_t BgL_pointedzd2tozd2by1061z00_3595,
		obj_t BgL_tvector1062z00_3596, obj_t BgL_location1063z00_3597,
		obj_t BgL_importzd2location1064zd2_3598, obj_t BgL_occurrence1065z00_3599,
		obj_t BgL_arrayzf31066zf3_3600)
	{
		{	/* Foreign/ctype.scm 32 */
			{	/* Foreign/ctype.scm 32 */
				bool_t BgL_initzf31056zf3_3971;
				bool_t BgL_magiczf31057zf3_3972;
				int BgL_occurrence1065z00_3973;
				bool_t BgL_arrayzf31066zf3_3974;

				BgL_initzf31056zf3_3971 = CBOOL(BgL_initzf31056zf3_3590);
				BgL_magiczf31057zf3_3972 = CBOOL(BgL_magiczf31057zf3_3591);
				BgL_occurrence1065z00_3973 = CINT(BgL_occurrence1065z00_3599);
				BgL_arrayzf31066zf3_3974 = CBOOL(BgL_arrayzf31066zf3_3600);
				{	/* Foreign/ctype.scm 32 */
					BgL_typez00_bglt BgL_new1295z00_3975;

					{	/* Foreign/ctype.scm 32 */
						BgL_typez00_bglt BgL_tmp1293z00_3976;
						BgL_caliasz00_bglt BgL_wide1294z00_3977;

						{
							BgL_typez00_bglt BgL_auxz00_8273;

							{	/* Foreign/ctype.scm 32 */
								BgL_typez00_bglt BgL_new1292z00_3978;

								BgL_new1292z00_3978 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Foreign/ctype.scm 32 */
									long BgL_arg1892z00_3979;

									BgL_arg1892z00_3979 =
										BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1292z00_3978),
										BgL_arg1892z00_3979);
								}
								{	/* Foreign/ctype.scm 32 */
									BgL_objectz00_bglt BgL_tmpz00_8278;

									BgL_tmpz00_8278 = ((BgL_objectz00_bglt) BgL_new1292z00_3978);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8278, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1292z00_3978);
								BgL_auxz00_8273 = BgL_new1292z00_3978;
							}
							BgL_tmp1293z00_3976 = ((BgL_typez00_bglt) BgL_auxz00_8273);
						}
						BgL_wide1294z00_3977 =
							((BgL_caliasz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_caliasz00_bgl))));
						{	/* Foreign/ctype.scm 32 */
							obj_t BgL_auxz00_8286;
							BgL_objectz00_bglt BgL_tmpz00_8284;

							BgL_auxz00_8286 = ((obj_t) BgL_wide1294z00_3977);
							BgL_tmpz00_8284 = ((BgL_objectz00_bglt) BgL_tmp1293z00_3976);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8284, BgL_auxz00_8286);
						}
						((BgL_objectz00_bglt) BgL_tmp1293z00_3976);
						{	/* Foreign/ctype.scm 32 */
							long BgL_arg1891z00_3980;

							BgL_arg1891z00_3980 =
								BGL_CLASS_NUM(BGl_caliasz00zzforeign_ctypez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1293z00_3976),
								BgL_arg1891z00_3980);
						}
						BgL_new1295z00_3975 = ((BgL_typez00_bglt) BgL_tmp1293z00_3976);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1295z00_3975)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1050z00_3584)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_namez00) =
						((obj_t) BgL_name1051z00_3585), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1052za7_3586), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_classz00) =
						((obj_t) BgL_class1053z00_3587), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1054zd2_3588), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_parentsz00) =
						((obj_t) BgL_parents1055z00_3589), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31056zf3_3971), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31057zf3_3972), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_nullz00) =
						((obj_t) BgL_null1058z00_3592), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_z42z42) =
						((obj_t) BgL_z421059z42_3593), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_aliasz00) =
						((obj_t) BgL_alias1060z00_3594), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1061z00_3595), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1062z00_3596), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_locationz00) =
						((obj_t) BgL_location1063z00_3597), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1064zd2_3598), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1295z00_3975)))->BgL_occurrencez00) =
						((int) BgL_occurrence1065z00_3973), BUNSPEC);
					{
						BgL_caliasz00_bglt BgL_auxz00_8327;

						{
							obj_t BgL_auxz00_8328;

							{	/* Foreign/ctype.scm 32 */
								BgL_objectz00_bglt BgL_tmpz00_8329;

								BgL_tmpz00_8329 = ((BgL_objectz00_bglt) BgL_new1295z00_3975);
								BgL_auxz00_8328 = BGL_OBJECT_WIDENING(BgL_tmpz00_8329);
							}
							BgL_auxz00_8327 = ((BgL_caliasz00_bglt) BgL_auxz00_8328);
						}
						((((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_8327))->
								BgL_arrayzf3zf3) =
							((bool_t) BgL_arrayzf31066zf3_3974), BUNSPEC);
					}
					return BgL_new1295z00_3975;
				}
			}
		}

	}



/* &<@anonymous:1907> */
	obj_t BGl_z62zc3z04anonymousza31907ze3ze5zzforeign_ctypez00(obj_t
		BgL_envz00_3601)
	{
		{	/* Foreign/ctype.scm 32 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1906 */
	obj_t BGl_z62lambda1906z62zzforeign_ctypez00(obj_t BgL_envz00_3602,
		obj_t BgL_oz00_3603, obj_t BgL_vz00_3604)
	{
		{	/* Foreign/ctype.scm 32 */
			{	/* Foreign/ctype.scm 32 */
				bool_t BgL_vz00_3982;

				BgL_vz00_3982 = CBOOL(BgL_vz00_3604);
				{
					BgL_caliasz00_bglt BgL_auxz00_8336;

					{
						obj_t BgL_auxz00_8337;

						{	/* Foreign/ctype.scm 32 */
							BgL_objectz00_bglt BgL_tmpz00_8338;

							BgL_tmpz00_8338 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3603));
							BgL_auxz00_8337 = BGL_OBJECT_WIDENING(BgL_tmpz00_8338);
						}
						BgL_auxz00_8336 = ((BgL_caliasz00_bglt) BgL_auxz00_8337);
					}
					return
						((((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_8336))->
							BgL_arrayzf3zf3) = ((bool_t) BgL_vz00_3982), BUNSPEC);
				}
			}
		}

	}



/* &lambda1905 */
	obj_t BGl_z62lambda1905z62zzforeign_ctypez00(obj_t BgL_envz00_3605,
		obj_t BgL_oz00_3606)
	{
		{	/* Foreign/ctype.scm 32 */
			{	/* Foreign/ctype.scm 32 */
				bool_t BgL_tmpz00_8344;

				{
					BgL_caliasz00_bglt BgL_auxz00_8345;

					{
						obj_t BgL_auxz00_8346;

						{	/* Foreign/ctype.scm 32 */
							BgL_objectz00_bglt BgL_tmpz00_8347;

							BgL_tmpz00_8347 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_3606));
							BgL_auxz00_8346 = BGL_OBJECT_WIDENING(BgL_tmpz00_8347);
						}
						BgL_auxz00_8345 = ((BgL_caliasz00_bglt) BgL_auxz00_8346);
					}
					BgL_tmpz00_8344 =
						(((BgL_caliasz00_bglt) COBJECT(BgL_auxz00_8345))->BgL_arrayzf3zf3);
				}
				return BBOOL(BgL_tmpz00_8344);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_ctypez00(void)
	{
		{	/* Foreign/ctype.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2185z00zzforeign_ctypez00));
		}

	}

#ifdef __cplusplus
}
#endif
