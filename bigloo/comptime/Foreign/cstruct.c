/*===========================================================================*/
/*   (Foreign/cstruct.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/cstruct.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_CSTRUCT_TYPE_DEFINITIONS
#define BGL_FOREIGN_CSTRUCT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_cstructz00_bgl
	{
		bool_t BgL_structzf3zf3;
		obj_t BgL_fieldsz00;
		obj_t BgL_cstructza2za2;
	}                 *BgL_cstructz00_bglt;

	typedef struct BgL_cstructza2za2_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
		struct BgL_typez00_bgl *BgL_cstructz00;
	}                    *BgL_cstructza2za2_bglt;


#endif													// BGL_FOREIGN_CSTRUCT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzforeign_cstructz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzforeign_cstructz00(void);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzforeign_cstructz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzforeign_cstructz00(void);
	extern obj_t BGl_cstructz00zzforeign_ctypez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzforeign_cstructz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzforeign_cstructz00(void);
	extern obj_t
		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t
		BGl_z62makezd2ctypezd2accessesz121104z70zzforeign_cstructz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62makezd2ctypezd2accessesz121107z70zzforeign_cstructz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_cstructz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_makezd2protozd2inlinez00zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_cstructza2za2zzforeign_ctypez00;
	static obj_t BGl_cnstzd2initzd2zzforeign_cstructz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_cstructz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_cstructz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_cstructz00(void);
	extern obj_t BGl_makezd2definezd2inlinez00zzforeign_libraryz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2aliasedzd2typez00zztype_typez00(BgL_typez00_bglt);
	static obj_t __cnst[34];


	   
		 
		DEFINE_STRING(BGl_string1964z00zzforeign_cstructz00,
		BgL_bgl_string1964za700za7za7f1987za7, "make-ctype-accesses!", 20);
	      DEFINE_STRING(BGl_string1966z00zzforeign_cstructz00,
		BgL_bgl_string1966za700za7za7f1988za7, "*((", 3);
	      DEFINE_STRING(BGl_string1967z00zzforeign_cstructz00,
		BgL_bgl_string1967za700za7za7f1989za7, ") 0)", 4);
	      DEFINE_STRING(BGl_string1968z00zzforeign_cstructz00,
		BgL_bgl_string1968za700za7za7f1990za7, "))", 2);
	      DEFINE_STRING(BGl_string1969z00zzforeign_cstructz00,
		BgL_bgl_string1969za700za7za7f1991za7, ")$1)->", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1963z00zzforeign_cstructz00,
		BgL_bgl_za762makeza7d2ctypeza71992za7,
		BGl_z62makezd2ctypezd2accessesz121104z70zzforeign_cstructz00, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string1970z00zzforeign_cstructz00,
		BgL_bgl_string1970za700za7za7f1993za7, "&((((", 5);
	      DEFINE_STRING(BGl_string1971z00zzforeign_cstructz00,
		BgL_bgl_string1971za700za7za7f1994za7, ")", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1965z00zzforeign_cstructz00,
		BgL_bgl_za762makeza7d2ctypeza71995za7,
		BGl_z62makezd2ctypezd2accessesz121107z70zzforeign_cstructz00, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string1972z00zzforeign_cstructz00,
		BgL_bgl_string1972za700za7za7f1996za7, "(((", 3);
	      DEFINE_STRING(BGl_string1973z00zzforeign_cstructz00,
		BgL_bgl_string1973za700za7za7f1997za7, "((((", 4);
	      DEFINE_STRING(BGl_string1974z00zzforeign_cstructz00,
		BgL_bgl_string1974za700za7za7f1998za7, " = (*($2)), BUNSPEC)", 20);
	      DEFINE_STRING(BGl_string1975z00zzforeign_cstructz00,
		BgL_bgl_string1975za700za7za7f1999za7, " = ($2), BUNSPEC)", 17);
	      DEFINE_STRING(BGl_string1976z00zzforeign_cstructz00,
		BgL_bgl_string1976za700za7za7f2000za7, " ) )", 4);
	      DEFINE_STRING(BGl_string1977z00zzforeign_cstructz00,
		BgL_bgl_string1977za700za7za7f2001za7, ")GC_MALLOC( sizeof( ", 20);
	      DEFINE_STRING(BGl_string1978z00zzforeign_cstructz00,
		BgL_bgl_string1978za700za7za7f2002za7, "(", 1);
	      DEFINE_STRING(BGl_string1979z00zzforeign_cstructz00,
		BgL_bgl_string1979za700za7za7f2003za7, "((", 2);
	      DEFINE_STRING(BGl_string1980z00zzforeign_cstructz00,
		BgL_bgl_string1980za700za7za7f2004za7, ")0L)", 4);
	      DEFINE_STRING(BGl_string1981z00zzforeign_cstructz00,
		BgL_bgl_string1981za700za7za7f2005za7, "($1 == (", 8);
	      DEFINE_STRING(BGl_string1982z00zzforeign_cstructz00,
		BgL_bgl_string1982za700za7za7f2006za7, ")FOREIGN_TO_COBJ", 16);
	      DEFINE_STRING(BGl_string1983z00zzforeign_cstructz00,
		BgL_bgl_string1983za700za7za7f2007za7, "cobj_to_foreign", 15);
	      DEFINE_STRING(BGl_string1984z00zzforeign_cstructz00,
		BgL_bgl_string1984za700za7za7f2008za7, "foreign_cstruct", 15);
	      DEFINE_STRING(BGl_string1985z00zzforeign_cstructz00,
		BgL_bgl_string1985za700za7za7f2009za7,
		"predicate-of static foreign -null? symbol macro if eq? quote foreign-id foreign? o::obj (pragma::bool \"($1 == $2)\" o1 o2) o2 o1 = ?::bool pragma::bool -null?::bool |pragma::| make-null- let make- new v ::obj pragma o * -set! - ::bool ? -> ",
		239);
	extern obj_t BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_cstructz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzforeign_cstructz00(long
		BgL_checksumz00_1603, char *BgL_fromz00_1604)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_cstructz00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_cstructz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_cstructz00();
					BGl_libraryzd2moduleszd2initz00zzforeign_cstructz00();
					BGl_cnstzd2initzd2zzforeign_cstructz00();
					BGl_importedzd2moduleszd2initz00zzforeign_cstructz00();
					BGl_methodzd2initzd2zzforeign_cstructz00();
					return BGl_toplevelzd2initzd2zzforeign_cstructz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_cstructz00(void)
	{
		{	/* Foreign/cstruct.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"foreign_cstruct");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_cstruct");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"foreign_cstruct");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"foreign_cstruct");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_cstruct");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"foreign_cstruct");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"foreign_cstruct");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"foreign_cstruct");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"foreign_cstruct");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzforeign_cstructz00(void)
	{
		{	/* Foreign/cstruct.scm 15 */
			{	/* Foreign/cstruct.scm 15 */
				obj_t BgL_cportz00_1212;

				{	/* Foreign/cstruct.scm 15 */
					obj_t BgL_stringz00_1219;

					BgL_stringz00_1219 = BGl_string1985z00zzforeign_cstructz00;
					{	/* Foreign/cstruct.scm 15 */
						obj_t BgL_startz00_1220;

						BgL_startz00_1220 = BINT(0L);
						{	/* Foreign/cstruct.scm 15 */
							obj_t BgL_endz00_1221;

							BgL_endz00_1221 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1219)));
							{	/* Foreign/cstruct.scm 15 */

								BgL_cportz00_1212 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1219, BgL_startz00_1220, BgL_endz00_1221);
				}}}}
				{
					long BgL_iz00_1213;

					BgL_iz00_1213 = 33L;
				BgL_loopz00_1214:
					if ((BgL_iz00_1213 == -1L))
						{	/* Foreign/cstruct.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Foreign/cstruct.scm 15 */
							{	/* Foreign/cstruct.scm 15 */
								obj_t BgL_arg1986z00_1215;

								{	/* Foreign/cstruct.scm 15 */

									{	/* Foreign/cstruct.scm 15 */
										obj_t BgL_locationz00_1217;

										BgL_locationz00_1217 = BBOOL(((bool_t) 0));
										{	/* Foreign/cstruct.scm 15 */

											BgL_arg1986z00_1215 =
												BGl_readz00zz__readerz00(BgL_cportz00_1212,
												BgL_locationz00_1217);
										}
									}
								}
								{	/* Foreign/cstruct.scm 15 */
									int BgL_tmpz00_1632;

									BgL_tmpz00_1632 = (int) (BgL_iz00_1213);
									CNST_TABLE_SET(BgL_tmpz00_1632, BgL_arg1986z00_1215);
							}}
							{	/* Foreign/cstruct.scm 15 */
								int BgL_auxz00_1218;

								BgL_auxz00_1218 = (int) ((BgL_iz00_1213 - 1L));
								{
									long BgL_iz00_1637;

									BgL_iz00_1637 = (long) (BgL_auxz00_1218);
									BgL_iz00_1213 = BgL_iz00_1637;
									goto BgL_loopz00_1214;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_cstructz00(void)
	{
		{	/* Foreign/cstruct.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzforeign_cstructz00(void)
	{
		{	/* Foreign/cstruct.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzforeign_cstructz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_526;

				BgL_headz00_526 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_527;
					obj_t BgL_tailz00_528;

					BgL_prevz00_527 = BgL_headz00_526;
					BgL_tailz00_528 = BgL_l1z00_1;
				BgL_loopz00_529:
					if (PAIRP(BgL_tailz00_528))
						{
							obj_t BgL_newzd2prevzd2_531;

							BgL_newzd2prevzd2_531 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_528), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_527, BgL_newzd2prevzd2_531);
							{
								obj_t BgL_tailz00_1647;
								obj_t BgL_prevz00_1646;

								BgL_prevz00_1646 = BgL_newzd2prevzd2_531;
								BgL_tailz00_1647 = CDR(BgL_tailz00_528);
								BgL_tailz00_528 = BgL_tailz00_1647;
								BgL_prevz00_527 = BgL_prevz00_1646;
								goto BgL_loopz00_529;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_526);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_cstructz00(void)
	{
		{	/* Foreign/cstruct.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_cstructz00(void)
	{
		{	/* Foreign/cstruct.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_cstructz00(void)
	{
		{	/* Foreign/cstruct.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
				BGl_cstructz00zzforeign_ctypez00, BGl_proc1963z00zzforeign_cstructz00,
				BGl_string1964z00zzforeign_cstructz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
				BGl_cstructza2za2zzforeign_ctypez00,
				BGl_proc1965z00zzforeign_cstructz00,
				BGl_string1964z00zzforeign_cstructz00);
		}

	}



/* &make-ctype-accesses!1107 */
	obj_t BGl_z62makezd2ctypezd2accessesz121107z70zzforeign_cstructz00(obj_t
		BgL_envz00_1200, obj_t BgL_whatz00_1201, obj_t BgL_whoz00_1202,
		obj_t BgL_locz00_1203)
	{
		{	/* Foreign/cstruct.scm 38 */
			{	/* Tools/trace.sch 53 */
				BgL_typez00_bglt BgL_btypez00_1225;

				{
					BgL_cstructza2za2_bglt BgL_auxz00_1652;

					{
						obj_t BgL_auxz00_1653;

						{	/* Tools/trace.sch 53 */
							BgL_objectz00_bglt BgL_tmpz00_1654;

							BgL_tmpz00_1654 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_whatz00_1201));
							BgL_auxz00_1653 = BGL_OBJECT_WIDENING(BgL_tmpz00_1654);
						}
						BgL_auxz00_1652 = ((BgL_cstructza2za2_bglt) BgL_auxz00_1653);
					}
					BgL_btypez00_1225 =
						(((BgL_cstructza2za2_bglt) COBJECT(BgL_auxz00_1652))->BgL_btypez00);
				}
				{	/* Tools/trace.sch 53 */
					obj_t BgL_idz00_1226;

					BgL_idz00_1226 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_whoz00_1202)))->BgL_idz00);
					{	/* Tools/trace.sch 53 */
						obj_t BgL_bidz00_1227;

						BgL_bidz00_1227 =
							(((BgL_typez00_bglt) COBJECT(BgL_btypez00_1225))->BgL_idz00);
						{	/* Tools/trace.sch 53 */
							obj_t BgL_idzd2ze3bidz31_1228;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_list1763z00_1229;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1765z00_1230;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1767z00_1231;

										BgL_arg1767z00_1231 =
											MAKE_YOUNG_PAIR(BgL_bidz00_1227, BNIL);
										BgL_arg1765z00_1230 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1767z00_1231);
									}
									BgL_list1763z00_1229 =
										MAKE_YOUNG_PAIR(BgL_idz00_1226, BgL_arg1765z00_1230);
								}
								BgL_idzd2ze3bidz31_1228 =
									BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
									(BgL_list1763z00_1229);
							}
							{	/* Tools/trace.sch 53 */
								obj_t BgL_bidzd2ze3idz31_1232;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_list1756z00_1233;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1761z00_1234;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1762z00_1235;

											BgL_arg1762z00_1235 =
												MAKE_YOUNG_PAIR(BgL_idz00_1226, BNIL);
											BgL_arg1761z00_1234 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1762z00_1235);
										}
										BgL_list1756z00_1233 =
											MAKE_YOUNG_PAIR(BgL_bidz00_1227, BgL_arg1761z00_1234);
									}
									BgL_bidzd2ze3idz31_1232 =
										BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
										(BgL_list1756z00_1233);
								}
								{	/* Tools/trace.sch 53 */
									obj_t BgL_bidzf3zf3_1236;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1753z00_1237;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1754z00_1238;
											obj_t BgL_arg1755z00_1239;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1455z00_1240;

												BgL_arg1455z00_1240 = SYMBOL_TO_STRING(BgL_idz00_1226);
												BgL_arg1754z00_1238 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_1240);
											}
											{	/* Tools/trace.sch 53 */
												obj_t BgL_symbolz00_1241;

												BgL_symbolz00_1241 = CNST_TABLE_REF(1);
												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1455z00_1242;

													BgL_arg1455z00_1242 =
														SYMBOL_TO_STRING(BgL_symbolz00_1241);
													BgL_arg1755z00_1239 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1242);
												}
											}
											BgL_arg1753z00_1237 =
												string_append(BgL_arg1754z00_1238, BgL_arg1755z00_1239);
										}
										BgL_bidzf3zf3_1236 = bstring_to_symbol(BgL_arg1753z00_1237);
									}
									{	/* Tools/trace.sch 53 */
										obj_t BgL_bidzf3zd2boolz21_1243;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1750z00_1244;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1751z00_1245;
												obj_t BgL_arg1752z00_1246;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1455z00_1247;

													BgL_arg1455z00_1247 =
														SYMBOL_TO_STRING(BgL_bidzf3zf3_1236);
													BgL_arg1751z00_1245 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1247);
												}
												{	/* Tools/trace.sch 53 */
													obj_t BgL_symbolz00_1248;

													BgL_symbolz00_1248 = CNST_TABLE_REF(2);
													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1455z00_1249;

														BgL_arg1455z00_1249 =
															SYMBOL_TO_STRING(BgL_symbolz00_1248);
														BgL_arg1752z00_1246 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_1249);
													}
												}
												BgL_arg1750z00_1244 =
													string_append(BgL_arg1751z00_1245,
													BgL_arg1752z00_1246);
											}
											BgL_bidzf3zd2boolz21_1243 =
												bstring_to_symbol(BgL_arg1750z00_1244);
										}
										{	/* Tools/trace.sch 53 */
											obj_t BgL_namez00_1250;

											BgL_namez00_1250 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_whoz00_1202)))->
												BgL_namez00);
											{	/* Tools/trace.sch 53 */
												obj_t BgL_namezd2sanszd2z42z42_1251;

												BgL_namezd2sanszd2z42z42_1251 =
													BGl_stringzd2sanszd2z42z42zztype_toolsz00
													(BgL_namez00_1250);
												{	/* Tools/trace.sch 53 */
													BgL_typez00_bglt BgL_cstructz00_1252;

													{
														BgL_cstructza2za2_bglt BgL_auxz00_1690;

														{
															obj_t BgL_auxz00_1691;

															{	/* Tools/trace.sch 53 */
																BgL_objectz00_bglt BgL_tmpz00_1692;

																BgL_tmpz00_1692 =
																	((BgL_objectz00_bglt)
																	((BgL_typez00_bglt) BgL_whatz00_1201));
																BgL_auxz00_1691 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_1692);
															}
															BgL_auxz00_1690 =
																((BgL_cstructza2za2_bglt) BgL_auxz00_1691);
														}
														BgL_cstructz00_1252 =
															(((BgL_cstructza2za2_bglt)
																COBJECT(BgL_auxz00_1690))->BgL_cstructz00);
													}
													{	/* Tools/trace.sch 53 */
														obj_t BgL_cstructzd2fieldszd2_1253;

														{
															BgL_cstructz00_bglt BgL_auxz00_1698;

															{
																obj_t BgL_auxz00_1699;

																{	/* Tools/trace.sch 53 */
																	BgL_objectz00_bglt BgL_tmpz00_1700;

																	BgL_tmpz00_1700 =
																		((BgL_objectz00_bglt) BgL_cstructz00_1252);
																	BgL_auxz00_1699 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_1700);
																}
																BgL_auxz00_1698 =
																	((BgL_cstructz00_bglt) BgL_auxz00_1699);
															}
															BgL_cstructzd2fieldszd2_1253 =
																(((BgL_cstructz00_bglt)
																	COBJECT(BgL_auxz00_1698))->BgL_fieldsz00);
														}
														{	/* Tools/trace.sch 53 */
															obj_t BgL_siza7eofza7_1254;

															BgL_siza7eofza7_1254 =
																string_append_3
																(BGl_string1966z00zzforeign_cstructz00,
																BgL_namezd2sanszd2z42z42_1251,
																BGl_string1967z00zzforeign_cstructz00);
															{	/* Tools/trace.sch 53 */

																{
																	obj_t BgL_fieldz00_1272;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1122z00_1545;
																		obj_t BgL_arg1123z00_1546;
																		obj_t BgL_arg1125z00_1547;
																		obj_t BgL_arg1126z00_1548;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1140z00_1549;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1141z00_1550;
																				obj_t BgL_arg1142z00_1551;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_symbolz00_1552;

																					BgL_symbolz00_1552 =
																						CNST_TABLE_REF(11);
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1553;

																						BgL_arg1455z00_1553 =
																							SYMBOL_TO_STRING
																							(BgL_symbolz00_1552);
																						BgL_arg1141z00_1550 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1553);
																					}
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1455z00_1554;

																					BgL_arg1455z00_1554 =
																						SYMBOL_TO_STRING(BgL_idz00_1226);
																					BgL_arg1142z00_1551 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1455z00_1554);
																				}
																				BgL_arg1140z00_1549 =
																					string_append(BgL_arg1141z00_1550,
																					BgL_arg1142z00_1551);
																			}
																			BgL_arg1122z00_1545 =
																				bstring_to_symbol(BgL_arg1140z00_1549);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_list1143z00_1555;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1145z00_1556;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1148z00_1557;

																					BgL_arg1148z00_1557 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																						BNIL);
																					BgL_arg1145z00_1556 =
																						MAKE_YOUNG_PAIR(BgL_idz00_1226,
																						BgL_arg1148z00_1557);
																				}
																				BgL_list1143z00_1555 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(18),
																					BgL_arg1145z00_1556);
																			}
																			BgL_arg1123z00_1546 =
																				BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																				(BgL_list1143z00_1555);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1149z00_1558;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1152z00_1559;
																				obj_t BgL_arg1153z00_1560;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1455z00_1561;

																					BgL_arg1455z00_1561 =
																						SYMBOL_TO_STRING(BgL_idz00_1226);
																					BgL_arg1152z00_1559 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1455z00_1561);
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_symbolz00_1562;

																					BgL_symbolz00_1562 =
																						CNST_TABLE_REF(30);
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1563;

																						BgL_arg1455z00_1563 =
																							SYMBOL_TO_STRING
																							(BgL_symbolz00_1562);
																						BgL_arg1153z00_1560 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1563);
																					}
																				}
																				BgL_arg1149z00_1558 =
																					string_append(BgL_arg1152z00_1559,
																					BgL_arg1153z00_1560);
																			}
																			BgL_arg1125z00_1547 =
																				bstring_to_symbol(BgL_arg1149z00_1558);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1154z00_1564;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1157z00_1565;
																				obj_t BgL_arg1158z00_1566;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_symbolz00_1567;

																					BgL_symbolz00_1567 =
																						CNST_TABLE_REF(13);
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1455z00_1568;

																						BgL_arg1455z00_1568 =
																							SYMBOL_TO_STRING
																							(BgL_symbolz00_1567);
																						BgL_arg1157z00_1565 =
																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																							(BgL_arg1455z00_1568);
																					}
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1455z00_1569;

																					BgL_arg1455z00_1569 =
																						SYMBOL_TO_STRING(BgL_idz00_1226);
																					BgL_arg1158z00_1566 =
																						BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																						(BgL_arg1455z00_1569);
																				}
																				BgL_arg1154z00_1564 =
																					string_append(BgL_arg1157z00_1565,
																					BgL_arg1158z00_1566);
																			}
																			BgL_arg1126z00_1548 =
																				bstring_to_symbol(BgL_arg1154z00_1564);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_list1127z00_1570;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1129z00_1571;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1131z00_1572;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1132z00_1573;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1137z00_1574;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1138z00_1575;

																								BgL_arg1138z00_1575 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1126z00_1548, BNIL);
																								BgL_arg1137z00_1574 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1125z00_1547,
																									BgL_arg1138z00_1575);
																							}
																							BgL_arg1132z00_1573 =
																								MAKE_YOUNG_PAIR
																								(BgL_bidzf3zf3_1236,
																								BgL_arg1137z00_1574);
																						}
																						BgL_arg1131z00_1572 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1123z00_1546,
																							BgL_arg1132z00_1573);
																					}
																					BgL_arg1129z00_1571 =
																						MAKE_YOUNG_PAIR(BgL_idz00_1226,
																						BgL_arg1131z00_1572);
																				}
																				BgL_list1127z00_1570 =
																					MAKE_YOUNG_PAIR(BgL_arg1122z00_1545,
																					BgL_arg1129z00_1571);
																			}
																			BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
																				(BgL_list1127z00_1570);
																		}
																	}
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1162z00_1576;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1164z00_1577;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1166z00_1578;
																				obj_t BgL_arg1171z00_1579;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1218z00_1539;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1219z00_1540;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1220z00_1541;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1221z00_1542;
																								obj_t BgL_arg1223z00_1543;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1225z00_1544;

																									BgL_arg1225z00_1544 =
																										MAKE_YOUNG_PAIR
																										(BgL_idz00_1226, BNIL);
																									BgL_arg1221z00_1542 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(29),
																										BgL_arg1225z00_1544);
																								}
																								BgL_arg1223z00_1543 =
																									MAKE_YOUNG_PAIR
																									(BGl_string1983z00zzforeign_cstructz00,
																									BNIL);
																								BgL_arg1220z00_1541 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1221z00_1542,
																									BgL_arg1223z00_1543);
																							}
																							BgL_arg1219z00_1540 =
																								MAKE_YOUNG_PAIR
																								(BgL_idzd2ze3bidz31_1228,
																								BgL_arg1220z00_1541);
																						}
																						BgL_arg1218z00_1539 =
																							MAKE_YOUNG_PAIR(BgL_bidz00_1227,
																							BgL_arg1219z00_1540);
																					}
																					BgL_arg1166z00_1578 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(28),
																						BgL_arg1218z00_1539);
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_tmpz00_1749;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_mnamez00_1533;

																						BgL_mnamez00_1533 =
																							string_append_3
																							(BGl_string1978z00zzforeign_cstructz00,
																							BgL_namezd2sanszd2z42z42_1251,
																							BGl_string1982z00zzforeign_cstructz00);
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1227z00_1534;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1228z00_1535;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1229z00_1536;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1230z00_1537;
																										obj_t BgL_arg1231z00_1538;

																										BgL_arg1230z00_1537 =
																											MAKE_YOUNG_PAIR
																											(BgL_bidz00_1227, BNIL);
																										BgL_arg1231z00_1538 =
																											MAKE_YOUNG_PAIR
																											(BgL_mnamez00_1533, BNIL);
																										BgL_arg1229z00_1536 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1230z00_1537,
																											BgL_arg1231z00_1538);
																									}
																									BgL_arg1228z00_1535 =
																										MAKE_YOUNG_PAIR
																										(BgL_bidzd2ze3idz31_1232,
																										BgL_arg1229z00_1536);
																								}
																								BgL_arg1227z00_1534 =
																									MAKE_YOUNG_PAIR
																									(BgL_idz00_1226,
																									BgL_arg1228z00_1535);
																							}
																							BgL_tmpz00_1749 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(28), BgL_arg1227z00_1534);
																						}
																					}
																					BgL_arg1171z00_1579 =
																						MAKE_YOUNG_PAIR(BgL_tmpz00_1749,
																						BNIL);
																				}
																				BgL_arg1164z00_1577 =
																					MAKE_YOUNG_PAIR(BgL_arg1166z00_1578,
																					BgL_arg1171z00_1579);
																			}
																			BgL_arg1162z00_1576 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(31),
																				BgL_arg1164z00_1577);
																		}
																		BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																			(BgL_arg1162z00_1576);
																	}
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1182z00_1580;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1183z00_1581;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1187z00_1582;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1188z00_1583;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1189z00_1584;

																						BgL_arg1189z00_1584 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																							BNIL);
																						BgL_arg1188z00_1583 =
																							MAKE_YOUNG_PAIR
																							(BgL_bidzf3zd2boolz21_1243,
																							BgL_arg1189z00_1584);
																					}
																					BgL_arg1187z00_1582 =
																						BGl_makezd2protozd2inlinez00zzforeign_libraryz00
																						(BgL_arg1188z00_1583);
																				}
																				BgL_arg1183z00_1581 =
																					MAKE_YOUNG_PAIR(BgL_arg1187z00_1582,
																					BNIL);
																			}
																			BgL_arg1182z00_1580 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(32),
																				BgL_arg1183z00_1581);
																		}
																		BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																			(BgL_arg1182z00_1580);
																	}
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1190z00_1585;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1191z00_1586;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1193z00_1587;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1194z00_1588;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1196z00_1589;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1197z00_1590;

																							BgL_arg1197z00_1590 =
																								MAKE_YOUNG_PAIR(BgL_bidz00_1227,
																								BNIL);
																							BgL_arg1196z00_1589 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(33), BgL_arg1197z00_1590);
																						}
																						BgL_arg1194z00_1588 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1196z00_1589, BNIL);
																					}
																					BgL_arg1193z00_1587 =
																						MAKE_YOUNG_PAIR(BgL_bidzf3zf3_1236,
																						BgL_arg1194z00_1588);
																				}
																				BgL_arg1191z00_1586 =
																					MAKE_YOUNG_PAIR(BgL_arg1193z00_1587,
																					BNIL);
																			}
																			BgL_arg1190z00_1585 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(7),
																				BgL_arg1191z00_1586);
																		}
																		BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																			(BgL_arg1190z00_1585);
																	}
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1198z00_1591;
																		obj_t BgL_arg1199z00_1592;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1367z00_1461;
																			obj_t BgL_arg1370z00_1462;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1371z00_1463;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1375z00_1464;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1376z00_1465;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1377z00_1466;
																							obj_t BgL_arg1378z00_1467;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_symbolz00_1468;

																								BgL_symbolz00_1468 =
																									CNST_TABLE_REF(11);
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1455z00_1469;

																									BgL_arg1455z00_1469 =
																										SYMBOL_TO_STRING
																										(BgL_symbolz00_1468);
																									BgL_arg1377z00_1466 =
																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																										(BgL_arg1455z00_1469);
																								}
																							}
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1455z00_1470;

																								BgL_arg1455z00_1470 =
																									SYMBOL_TO_STRING
																									(BgL_idz00_1226);
																								BgL_arg1378z00_1467 =
																									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																									(BgL_arg1455z00_1470);
																							}
																							BgL_arg1376z00_1465 =
																								string_append
																								(BgL_arg1377z00_1466,
																								BgL_arg1378z00_1467);
																						}
																						BgL_arg1375z00_1464 =
																							bstring_to_symbol
																							(BgL_arg1376z00_1465);
																					}
																					BgL_arg1371z00_1463 =
																						BGl_makezd2typedzd2identz00zzast_identz00
																						(BgL_arg1375z00_1464,
																						BgL_idz00_1226);
																				}
																				BgL_arg1367z00_1461 =
																					MAKE_YOUNG_PAIR(BgL_arg1371z00_1463,
																					BNIL);
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1379z00_1471;
																				obj_t BgL_arg1380z00_1472;

																				BgL_arg1379z00_1471 =
																					BGl_makezd2typedzd2identz00zzast_identz00
																					(CNST_TABLE_REF(7), BgL_idz00_1226);
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1408z00_1473;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_list1409z00_1474;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1410z00_1475;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1421z00_1476;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1422z00_1477;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1434z00_1478;

																										BgL_arg1434z00_1478 =
																											MAKE_YOUNG_PAIR
																											(BGl_string1976z00zzforeign_cstructz00,
																											BNIL);
																										BgL_arg1422z00_1477 =
																											MAKE_YOUNG_PAIR
																											(BgL_siza7eofza7_1254,
																											BgL_arg1434z00_1478);
																									}
																									BgL_arg1421z00_1476 =
																										MAKE_YOUNG_PAIR
																										(BGl_string1977z00zzforeign_cstructz00,
																										BgL_arg1422z00_1477);
																								}
																								BgL_arg1410z00_1475 =
																									MAKE_YOUNG_PAIR
																									(BgL_namezd2sanszd2z42z42_1251,
																									BgL_arg1421z00_1476);
																							}
																							BgL_list1409z00_1474 =
																								MAKE_YOUNG_PAIR
																								(BGl_string1978z00zzforeign_cstructz00,
																								BgL_arg1410z00_1475);
																						}
																						BgL_arg1408z00_1473 =
																							BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																							(BgL_list1409z00_1474);
																					}
																					BgL_arg1380z00_1472 =
																						MAKE_YOUNG_PAIR(BgL_arg1408z00_1473,
																						BNIL);
																				}
																				BgL_arg1370z00_1462 =
																					MAKE_YOUNG_PAIR(BgL_arg1379z00_1471,
																					BgL_arg1380z00_1472);
																			}
																			BgL_arg1198z00_1591 =
																				BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																				(BgL_arg1367z00_1461,
																				BgL_arg1370z00_1462);
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1200z00_1593;
																			obj_t BgL_arg1201z00_1594;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_formalszd2typedzd2_1389;
																				obj_t BgL_newz00_1390;

																				if (NULLP(BgL_cstructzd2fieldszd2_1253))
																					{	/* Tools/trace.sch 53 */
																						BgL_formalszd2typedzd2_1389 = BNIL;
																					}
																				else
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_head1095z00_1391;

																						BgL_head1095z00_1391 =
																							MAKE_YOUNG_PAIR(BNIL, BNIL);
																						{
																							obj_t BgL_l1093z00_1393;
																							obj_t BgL_tail1096z00_1394;

																							BgL_l1093z00_1393 =
																								BgL_cstructzd2fieldszd2_1253;
																							BgL_tail1096z00_1394 =
																								BgL_head1095z00_1391;
																						BgL_zc3z04anonymousza31575ze3z87_1392:
																							if (NULLP
																								(BgL_l1093z00_1393))
																								{	/* Tools/trace.sch 53 */
																									BgL_formalszd2typedzd2_1389 =
																										CDR(BgL_head1095z00_1391);
																								}
																							else
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_newtail1097z00_1395;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1585z00_1396;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_fz00_1397;

																											BgL_fz00_1397 =
																												CAR(
																												((obj_t)
																													BgL_l1093z00_1393));
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_fzd2idzd2_1398;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_pairz00_1399;
																													BgL_pairz00_1399 =
																														CDR(((obj_t)
																															BgL_fz00_1397));
																													BgL_fzd2idzd2_1398 =
																														CAR
																														(BgL_pairz00_1399);
																												}
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_fzd2typezd2idz00_1400;
																													BgL_fzd2typezd2idz00_1400
																														=
																														CAR(((obj_t)
																															BgL_fz00_1397));
																													{	/* Tools/trace.sch 53 */
																														BgL_typez00_bglt
																															BgL_fzd2typezd2_1401;
																														BgL_fzd2typezd2_1401
																															=
																															BGl_usezd2typez12zc0zztype_envz00
																															(BgL_fzd2typezd2idz00_1400,
																															BgL_locz00_1203);
																														{	/* Tools/trace.sch 53 */
																															BgL_typez00_bglt
																																BgL_afzd2typezd2_1402;
																															BgL_afzd2typezd2_1402
																																=
																																BGl_getzd2aliasedzd2typez00zztype_typez00
																																(BgL_fzd2typezd2_1401);
																															{	/* Tools/trace.sch 53 */

																																{	/* Tools/trace.sch 53 */
																																	bool_t
																																		BgL_test2015z00_1815;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_classz00_1403;
																																		BgL_classz00_1403
																																			=
																																			BGl_cstructz00zzforeign_ctypez00;
																																		{	/* Tools/trace.sch 53 */
																																			BgL_objectz00_bglt
																																				BgL_arg1807z00_1404;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_tmpz00_1816;
																																				BgL_tmpz00_1816
																																					=
																																					(
																																					(obj_t)
																																					((BgL_objectz00_bglt) BgL_afzd2typezd2_1402));
																																				BgL_arg1807z00_1404
																																					=
																																					(BgL_objectz00_bglt)
																																					(BgL_tmpz00_1816);
																																			}
																																			if (BGL_CONDEXPAND_ISA_ARCH64())
																																				{	/* Tools/trace.sch 53 */
																																					long
																																						BgL_idxz00_1405;
																																					BgL_idxz00_1405
																																						=
																																						BGL_OBJECT_INHERITANCE_NUM
																																						(BgL_arg1807z00_1404);
																																					BgL_test2015z00_1815
																																						=
																																						(VECTOR_REF
																																						(BGl_za2inheritancesza2z00zz__objectz00,
																																							(BgL_idxz00_1405
																																								+
																																								2L))
																																						==
																																						BgL_classz00_1403);
																																				}
																																			else
																																				{	/* Tools/trace.sch 53 */
																																					bool_t
																																						BgL_res1834z00_1408;
																																					{	/* Tools/trace.sch 53 */
																																						obj_t
																																							BgL_oclassz00_1409;
																																						{	/* Tools/trace.sch 53 */
																																							obj_t
																																								BgL_arg1815z00_1410;
																																							long
																																								BgL_arg1816z00_1411;
																																							BgL_arg1815z00_1410
																																								=
																																								(BGl_za2classesza2z00zz__objectz00);
																																							{	/* Tools/trace.sch 53 */
																																								long
																																									BgL_arg1817z00_1412;
																																								BgL_arg1817z00_1412
																																									=
																																									BGL_OBJECT_CLASS_NUM
																																									(BgL_arg1807z00_1404);
																																								BgL_arg1816z00_1411
																																									=
																																									(BgL_arg1817z00_1412
																																									-
																																									OBJECT_TYPE);
																																							}
																																							BgL_oclassz00_1409
																																								=
																																								VECTOR_REF
																																								(BgL_arg1815z00_1410,
																																								BgL_arg1816z00_1411);
																																						}
																																						{	/* Tools/trace.sch 53 */
																																							bool_t
																																								BgL__ortest_1115z00_1413;
																																							BgL__ortest_1115z00_1413
																																								=
																																								(BgL_classz00_1403
																																								==
																																								BgL_oclassz00_1409);
																																							if (BgL__ortest_1115z00_1413)
																																								{	/* Tools/trace.sch 53 */
																																									BgL_res1834z00_1408
																																										=
																																										BgL__ortest_1115z00_1413;
																																								}
																																							else
																																								{	/* Tools/trace.sch 53 */
																																									long
																																										BgL_odepthz00_1414;
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_arg1804z00_1415;
																																										BgL_arg1804z00_1415
																																											=
																																											(BgL_oclassz00_1409);
																																										BgL_odepthz00_1414
																																											=
																																											BGL_CLASS_DEPTH
																																											(BgL_arg1804z00_1415);
																																									}
																																									if ((2L < BgL_odepthz00_1414))
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1802z00_1416;
																																											{	/* Tools/trace.sch 53 */
																																												obj_t
																																													BgL_arg1803z00_1417;
																																												BgL_arg1803z00_1417
																																													=
																																													(BgL_oclassz00_1409);
																																												BgL_arg1802z00_1416
																																													=
																																													BGL_CLASS_ANCESTORS_REF
																																													(BgL_arg1803z00_1417,
																																													2L);
																																											}
																																											BgL_res1834z00_1408
																																												=
																																												(BgL_arg1802z00_1416
																																												==
																																												BgL_classz00_1403);
																																										}
																																									else
																																										{	/* Tools/trace.sch 53 */
																																											BgL_res1834z00_1408
																																												=
																																												(
																																												(bool_t)
																																												0);
																																										}
																																								}
																																						}
																																					}
																																					BgL_test2015z00_1815
																																						=
																																						BgL_res1834z00_1408;
																																				}
																																		}
																																	}
																																	if (BgL_test2015z00_1815)
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_arg1589z00_1418;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_arg1591z00_1419;
																																				{	/* Tools/trace.sch 53 */
																																					obj_t
																																						BgL_arg1593z00_1420;
																																					obj_t
																																						BgL_arg1594z00_1421;
																																					{	/* Tools/trace.sch 53 */
																																						obj_t
																																							BgL_arg1455z00_1422;
																																						BgL_arg1455z00_1422
																																							=
																																							SYMBOL_TO_STRING
																																							(((obj_t) BgL_fzd2typezd2idz00_1400));
																																						BgL_arg1593z00_1420
																																							=
																																							BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																							(BgL_arg1455z00_1422);
																																					}
																																					{	/* Tools/trace.sch 53 */
																																						obj_t
																																							BgL_symbolz00_1423;
																																						BgL_symbolz00_1423
																																							=
																																							CNST_TABLE_REF
																																							(5);
																																						{	/* Tools/trace.sch 53 */
																																							obj_t
																																								BgL_arg1455z00_1424;
																																							BgL_arg1455z00_1424
																																								=
																																								SYMBOL_TO_STRING
																																								(BgL_symbolz00_1423);
																																							BgL_arg1594z00_1421
																																								=
																																								BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																								(BgL_arg1455z00_1424);
																																						}
																																					}
																																					BgL_arg1591z00_1419
																																						=
																																						string_append
																																						(BgL_arg1593z00_1420,
																																						BgL_arg1594z00_1421);
																																				}
																																				BgL_arg1589z00_1418
																																					=
																																					bstring_to_symbol
																																					(BgL_arg1591z00_1419);
																																			}
																																			BgL_arg1585z00_1396
																																				=
																																				BGl_makezd2typedzd2identz00zzast_identz00
																																				(BgL_fzd2idzd2_1398,
																																				BgL_arg1589z00_1418);
																																		}
																																	else
																																		{	/* Tools/trace.sch 53 */
																																			BgL_arg1585z00_1396
																																				=
																																				BGl_makezd2typedzd2identz00zzast_identz00
																																				(BgL_fzd2idzd2_1398,
																																				BgL_fzd2typezd2idz00_1400);
																																		}
																																}
																															}
																														}
																													}
																												}
																											}
																										}
																										BgL_newtail1097z00_1395 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1585z00_1396,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1096z00_1394,
																										BgL_newtail1097z00_1395);
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1584z00_1425;

																										BgL_arg1584z00_1425 =
																											CDR(
																											((obj_t)
																												BgL_l1093z00_1393));
																										{
																											obj_t
																												BgL_tail1096z00_1854;
																											obj_t BgL_l1093z00_1853;

																											BgL_l1093z00_1853 =
																												BgL_arg1584z00_1425;
																											BgL_tail1096z00_1854 =
																												BgL_newtail1097z00_1395;
																											BgL_tail1096z00_1394 =
																												BgL_tail1096z00_1854;
																											BgL_l1093z00_1393 =
																												BgL_l1093z00_1853;
																											goto
																												BgL_zc3z04anonymousza31575ze3z87_1392;
																										}
																									}
																								}
																						}
																					}
																				BgL_newz00_1390 =
																					BGl_gensymz00zz__r4_symbols_6_4z00
																					(CNST_TABLE_REF(10));
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1437z00_1426;
																					obj_t BgL_arg1448z00_1427;

																					BgL_arg1437z00_1426 =
																						MAKE_YOUNG_PAIR
																						(BGl_makezd2typedzd2identz00zzast_identz00
																						(BgL_idz00_1226, BgL_idz00_1226),
																						BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																						(BgL_formalszd2typedzd2_1389,
																							BNIL));
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1472z00_1428;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1473z00_1429;
																							obj_t BgL_arg1485z00_1430;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1489z00_1431;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1502z00_1432;
																									obj_t BgL_arg1509z00_1433;

																									BgL_arg1502z00_1432 =
																										BGl_makezd2typedzd2identz00zzast_identz00
																										(BgL_newz00_1390,
																										BgL_idz00_1226);
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1513z00_1434;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1514z00_1435;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1516z00_1436;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1535z00_1437;
																													obj_t
																														BgL_arg1540z00_1438;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_symbolz00_1439;
																														BgL_symbolz00_1439 =
																															CNST_TABLE_REF
																															(11);
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1455z00_1440;
																															BgL_arg1455z00_1440
																																=
																																SYMBOL_TO_STRING
																																(BgL_symbolz00_1439);
																															BgL_arg1535z00_1437
																																=
																																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																(BgL_arg1455z00_1440);
																														}
																													}
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1455z00_1441;
																														BgL_arg1455z00_1441
																															=
																															SYMBOL_TO_STRING
																															(BgL_idz00_1226);
																														BgL_arg1540z00_1438
																															=
																															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																															(BgL_arg1455z00_1441);
																													}
																													BgL_arg1516z00_1436 =
																														string_append
																														(BgL_arg1535z00_1437,
																														BgL_arg1540z00_1438);
																												}
																												BgL_arg1514z00_1435 =
																													bstring_to_symbol
																													(BgL_arg1516z00_1436);
																											}
																											BgL_arg1513z00_1434 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1514z00_1435,
																												BNIL);
																										}
																										BgL_arg1509z00_1433 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1513z00_1434,
																											BNIL);
																									}
																									BgL_arg1489z00_1431 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1502z00_1432,
																										BgL_arg1509z00_1433);
																								}
																								BgL_arg1473z00_1429 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1489z00_1431, BNIL);
																							}
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1544z00_1442;
																								obj_t BgL_arg1546z00_1443;

																								if (NULLP
																									(BgL_cstructzd2fieldszd2_1253))
																									{	/* Tools/trace.sch 53 */
																										BgL_arg1544z00_1442 = BNIL;
																									}
																								else
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_head1100z00_1444;

																										BgL_head1100z00_1444 =
																											MAKE_YOUNG_PAIR(BNIL,
																											BNIL);
																										{
																											obj_t BgL_l1098z00_1446;
																											obj_t
																												BgL_tail1101z00_1447;
																											BgL_l1098z00_1446 =
																												BgL_cstructzd2fieldszd2_1253;
																											BgL_tail1101z00_1447 =
																												BgL_head1100z00_1444;
																										BgL_zc3z04anonymousza31548ze3z87_1445:
																											if (NULLP
																												(BgL_l1098z00_1446))
																												{	/* Tools/trace.sch 53 */
																													BgL_arg1544z00_1442 =
																														CDR
																														(BgL_head1100z00_1444);
																												}
																											else
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_newtail1102z00_1448;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1553z00_1449;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_fz00_1450;
																															BgL_fz00_1450 =
																																CAR(((obj_t)
																																	BgL_l1098z00_1446));
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_fzd2idzd2_1451;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_pairz00_1452;
																																	BgL_pairz00_1452
																																		=
																																		CDR(((obj_t)
																																			BgL_fz00_1450));
																																	BgL_fzd2idzd2_1451
																																		=
																																		CAR
																																		(BgL_pairz00_1452);
																																}
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_arg1559z00_1453;
																																	obj_t
																																		BgL_arg1561z00_1454;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_list1562z00_1455;
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_arg1564z00_1456;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_arg1565z00_1457;
																																				{	/* Tools/trace.sch 53 */
																																					obj_t
																																						BgL_arg1571z00_1458;
																																					BgL_arg1571z00_1458
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(4),
																																						BNIL);
																																					BgL_arg1565z00_1457
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_fzd2idzd2_1451,
																																						BgL_arg1571z00_1458);
																																				}
																																				BgL_arg1564z00_1456
																																					=
																																					MAKE_YOUNG_PAIR
																																					(CNST_TABLE_REF
																																					(3),
																																					BgL_arg1565z00_1457);
																																			}
																																			BgL_list1562z00_1455
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_idz00_1226,
																																				BgL_arg1564z00_1456);
																																		}
																																		BgL_arg1559z00_1453
																																			=
																																			BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																																			(BgL_list1562z00_1455);
																																	}
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_arg1573z00_1459;
																																		BgL_arg1573z00_1459
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_fzd2idzd2_1451,
																																			BNIL);
																																		BgL_arg1561z00_1454
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_newz00_1390,
																																			BgL_arg1573z00_1459);
																																	}
																																	BgL_arg1553z00_1449
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1559z00_1453,
																																		BgL_arg1561z00_1454);
																																}
																															}
																														}
																														BgL_newtail1102z00_1448
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1553z00_1449,
																															BNIL);
																													}
																													SET_CDR
																														(BgL_tail1101z00_1447,
																														BgL_newtail1102z00_1448);
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1552z00_1460;
																														BgL_arg1552z00_1460
																															=
																															CDR(((obj_t)
																																BgL_l1098z00_1446));
																														{
																															obj_t
																																BgL_tail1101z00_1898;
																															obj_t
																																BgL_l1098z00_1897;
																															BgL_l1098z00_1897
																																=
																																BgL_arg1552z00_1460;
																															BgL_tail1101z00_1898
																																=
																																BgL_newtail1102z00_1448;
																															BgL_tail1101z00_1447
																																=
																																BgL_tail1101z00_1898;
																															BgL_l1098z00_1446
																																=
																																BgL_l1098z00_1897;
																															goto
																																BgL_zc3z04anonymousza31548ze3z87_1445;
																														}
																													}
																												}
																										}
																									}
																								BgL_arg1546z00_1443 =
																									MAKE_YOUNG_PAIR
																									(BgL_newz00_1390, BNIL);
																								BgL_arg1485z00_1430 =
																									BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																									(BgL_arg1544z00_1442,
																									BgL_arg1546z00_1443);
																							}
																							BgL_arg1472z00_1428 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1473z00_1429,
																								BgL_arg1485z00_1430);
																						}
																						BgL_arg1448z00_1427 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(12), BgL_arg1472z00_1428);
																					}
																					BgL_arg1200z00_1593 =
																						BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																						(BgL_arg1437z00_1426,
																						BgL_arg1448z00_1427);
																				}
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1202z00_1595;
																				obj_t BgL_arg1203z00_1596;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1307z00_1510;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1308z00_1511;
																						obj_t BgL_arg1310z00_1512;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_list1311z00_1513;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1312z00_1514;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1314z00_1515;

																									BgL_arg1314z00_1515 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(17), BNIL);
																									BgL_arg1312z00_1514 =
																										MAKE_YOUNG_PAIR
																										(BgL_idz00_1226,
																										BgL_arg1314z00_1515);
																								}
																								BgL_list1311z00_1513 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(18), BgL_arg1312z00_1514);
																							}
																							BgL_arg1308z00_1511 =
																								BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																								(BgL_list1311z00_1513);
																						}
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1315z00_1516;
																							obj_t BgL_arg1316z00_1517;

																							BgL_arg1315z00_1516 =
																								BGl_makezd2typedzd2identz00zzast_identz00
																								(CNST_TABLE_REF(19),
																								BgL_idz00_1226);
																							BgL_arg1316z00_1517 =
																								MAKE_YOUNG_PAIR
																								(BGl_makezd2typedzd2identz00zzast_identz00
																								(CNST_TABLE_REF(20),
																									BgL_idz00_1226), BNIL);
																							BgL_arg1310z00_1512 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1315z00_1516,
																								BgL_arg1316z00_1517);
																						}
																						BgL_arg1307z00_1510 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1308z00_1511,
																							BgL_arg1310z00_1512);
																					}
																					BgL_arg1202z00_1595 =
																						BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																						(BgL_arg1307z00_1510,
																						CNST_TABLE_REF(21));
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1206z00_1597;
																					obj_t BgL_arg1208z00_1598;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1319z00_1497;
																						obj_t BgL_arg1320z00_1498;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1321z00_1499;
																							obj_t BgL_arg1322z00_1500;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1323z00_1501;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1325z00_1502;
																									obj_t BgL_arg1326z00_1503;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1455z00_1504;

																										BgL_arg1455z00_1504 =
																											SYMBOL_TO_STRING
																											(BgL_idz00_1226);
																										BgL_arg1325z00_1502 =
																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																											(BgL_arg1455z00_1504);
																									}
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_symbolz00_1505;

																										BgL_symbolz00_1505 =
																											CNST_TABLE_REF(15);
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1455z00_1506;

																											BgL_arg1455z00_1506 =
																												SYMBOL_TO_STRING
																												(BgL_symbolz00_1505);
																											BgL_arg1326z00_1503 =
																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																												(BgL_arg1455z00_1506);
																										}
																									}
																									BgL_arg1323z00_1501 =
																										string_append
																										(BgL_arg1325z00_1502,
																										BgL_arg1326z00_1503);
																								}
																								BgL_arg1321z00_1499 =
																									bstring_to_symbol
																									(BgL_arg1323z00_1501);
																							}
																							BgL_arg1322z00_1500 =
																								MAKE_YOUNG_PAIR
																								(BGl_makezd2typedzd2identz00zzast_identz00
																								(CNST_TABLE_REF(6),
																									BgL_idz00_1226), BNIL);
																							BgL_arg1319z00_1497 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1321z00_1499,
																								BgL_arg1322z00_1500);
																						}
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1328z00_1507;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1329z00_1508;
																								obj_t BgL_arg1331z00_1509;

																								BgL_arg1329z00_1508 =
																									string_append_3
																									(BGl_string1981z00zzforeign_cstructz00,
																									BgL_namezd2sanszd2z42z42_1251,
																									BGl_string1980z00zzforeign_cstructz00);
																								BgL_arg1331z00_1509 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(6), BNIL);
																								BgL_arg1328z00_1507 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1329z00_1508,
																									BgL_arg1331z00_1509);
																							}
																							BgL_arg1320z00_1498 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(16), BgL_arg1328z00_1507);
																						}
																						BgL_arg1206z00_1597 =
																							BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																							(BgL_arg1319z00_1497,
																							BgL_arg1320z00_1498);
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1209z00_1599;
																						obj_t BgL_arg1210z00_1600;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1333z00_1479;
																							obj_t BgL_arg1335z00_1480;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1339z00_1481;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1340z00_1482;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1342z00_1483;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1343z00_1484;
																											obj_t BgL_arg1346z00_1485;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_symbolz00_1486;
																												BgL_symbolz00_1486 =
																													CNST_TABLE_REF(13);
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1455z00_1487;
																													BgL_arg1455z00_1487 =
																														SYMBOL_TO_STRING
																														(BgL_symbolz00_1486);
																													BgL_arg1343z00_1484 =
																														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																														(BgL_arg1455z00_1487);
																												}
																											}
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1455z00_1488;
																												BgL_arg1455z00_1488 =
																													SYMBOL_TO_STRING
																													(BgL_idz00_1226);
																												BgL_arg1346z00_1485 =
																													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																													(BgL_arg1455z00_1488);
																											}
																											BgL_arg1342z00_1483 =
																												string_append
																												(BgL_arg1343z00_1484,
																												BgL_arg1346z00_1485);
																										}
																										BgL_arg1340z00_1482 =
																											bstring_to_symbol
																											(BgL_arg1342z00_1483);
																									}
																									BgL_arg1339z00_1481 =
																										BGl_makezd2typedzd2identz00zzast_identz00
																										(BgL_arg1340z00_1482,
																										BgL_idz00_1226);
																								}
																								BgL_arg1333z00_1479 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1339z00_1481, BNIL);
																							}
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1348z00_1489;
																								obj_t BgL_arg1349z00_1490;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1351z00_1491;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1352z00_1492;
																										obj_t BgL_arg1361z00_1493;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_symbolz00_1494;

																											BgL_symbolz00_1494 =
																												CNST_TABLE_REF(14);
																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1455z00_1495;
																												BgL_arg1455z00_1495 =
																													SYMBOL_TO_STRING
																													(BgL_symbolz00_1494);
																												BgL_arg1352z00_1492 =
																													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																													(BgL_arg1455z00_1495);
																											}
																										}
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1455z00_1496;

																											BgL_arg1455z00_1496 =
																												SYMBOL_TO_STRING
																												(BgL_idz00_1226);
																											BgL_arg1361z00_1493 =
																												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																												(BgL_arg1455z00_1496);
																										}
																										BgL_arg1351z00_1491 =
																											string_append
																											(BgL_arg1352z00_1492,
																											BgL_arg1361z00_1493);
																									}
																									BgL_arg1348z00_1489 =
																										bstring_to_symbol
																										(BgL_arg1351z00_1491);
																								}
																								BgL_arg1349z00_1490 =
																									MAKE_YOUNG_PAIR
																									(string_append_3
																									(BGl_string1979z00zzforeign_cstructz00,
																										BgL_namezd2sanszd2z42z42_1251,
																										BGl_string1980z00zzforeign_cstructz00),
																									BNIL);
																								BgL_arg1335z00_1480 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1348z00_1489,
																									BgL_arg1349z00_1490);
																							}
																							BgL_arg1209z00_1599 =
																								BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																								(BgL_arg1333z00_1479,
																								BgL_arg1335z00_1480);
																						}
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_auxz00_1983;
																							obj_t BgL_tmpz00_1958;

																							{
																								obj_t BgL_fieldsz00_1266;
																								obj_t BgL_resz00_1267;

																								BgL_fieldsz00_1266 =
																									BgL_cstructzd2fieldszd2_1253;
																								BgL_resz00_1267 = BNIL;
																							BgL_loopz00_1265:
																								if (NULLP(BgL_fieldsz00_1266))
																									{	/* Tools/trace.sch 53 */
																										BgL_auxz00_1983 =
																											BgL_resz00_1267;
																									}
																								else
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1746z00_1268;
																										obj_t BgL_arg1747z00_1269;

																										BgL_arg1746z00_1268 =
																											CDR(
																											((obj_t)
																												BgL_fieldsz00_1266));
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1748z00_1270;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1749z00_1271;
																												BgL_arg1749z00_1271 =
																													CAR(((obj_t)
																														BgL_fieldsz00_1266));
																												BgL_fieldz00_1272 =
																													BgL_arg1749z00_1271;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_fzd2typezd2idz00_1273;
																													BgL_fzd2typezd2idz00_1273
																														=
																														CAR(((obj_t)
																															BgL_fieldz00_1272));
																													{	/* Tools/trace.sch 53 */
																														BgL_typez00_bglt
																															BgL_fzd2typezd2_1274;
																														BgL_fzd2typezd2_1274
																															=
																															BGl_usezd2typez12zc0zztype_envz00
																															(BgL_fzd2typezd2idz00_1273,
																															BgL_locz00_1203);
																														{	/* Tools/trace.sch 53 */
																															BgL_typez00_bglt
																																BgL_afzd2typezd2_1275;
																															BgL_afzd2typezd2_1275
																																=
																																BGl_getzd2aliasedzd2typez00zztype_typez00
																																(BgL_fzd2typezd2_1274);
																															{	/* Tools/trace.sch 53 */
																																obj_t
																																	BgL_fzd2idzd2_1276;
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_pairz00_1277;
																																	BgL_pairz00_1277
																																		=
																																		CDR(((obj_t)
																																			BgL_fieldz00_1272));
																																	BgL_fzd2idzd2_1276
																																		=
																																		CAR
																																		(BgL_pairz00_1277);
																																}
																																{	/* Tools/trace.sch 53 */
																																	obj_t
																																		BgL_fzd2namezd2_1278;
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_pairz00_1279;
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_pairz00_1280;
																																			BgL_pairz00_1280
																																				=
																																				CDR((
																																					(obj_t)
																																					BgL_fieldz00_1272));
																																			BgL_pairz00_1279
																																				=
																																				CDR
																																				(BgL_pairz00_1280);
																																		}
																																		BgL_fzd2namezd2_1278
																																			=
																																			CAR
																																			(BgL_pairz00_1279);
																																	}
																																	{	/* Tools/trace.sch 53 */
																																		obj_t
																																			BgL_getzd2namezd2_1281;
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_list1738z00_1282;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_arg1739z00_1283;
																																				{	/* Tools/trace.sch 53 */
																																					obj_t
																																						BgL_arg1740z00_1284;
																																					BgL_arg1740z00_1284
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_fzd2idzd2_1276,
																																						BNIL);
																																					BgL_arg1739z00_1283
																																						=
																																						MAKE_YOUNG_PAIR
																																						(CNST_TABLE_REF
																																						(3),
																																						BgL_arg1740z00_1284);
																																				}
																																				BgL_list1738z00_1282
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_idz00_1226,
																																					BgL_arg1739z00_1283);
																																			}
																																			BgL_getzd2namezd2_1281
																																				=
																																				BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																																				(BgL_list1738z00_1282);
																																		}
																																		{	/* Tools/trace.sch 53 */
																																			obj_t
																																				BgL_setzd2namezd2_1285;
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_list1734z00_1286;
																																				{	/* Tools/trace.sch 53 */
																																					obj_t
																																						BgL_arg1735z00_1287;
																																					{	/* Tools/trace.sch 53 */
																																						obj_t
																																							BgL_arg1736z00_1288;
																																						{	/* Tools/trace.sch 53 */
																																							obj_t
																																								BgL_arg1737z00_1289;
																																							BgL_arg1737z00_1289
																																								=
																																								MAKE_YOUNG_PAIR
																																								(CNST_TABLE_REF
																																								(4),
																																								BNIL);
																																							BgL_arg1736z00_1288
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_fzd2idzd2_1276,
																																								BgL_arg1737z00_1289);
																																						}
																																						BgL_arg1735z00_1287
																																							=
																																							MAKE_YOUNG_PAIR
																																							(CNST_TABLE_REF
																																							(3),
																																							BgL_arg1736z00_1288);
																																					}
																																					BgL_list1734z00_1286
																																						=
																																						MAKE_YOUNG_PAIR
																																						(BgL_idz00_1226,
																																						BgL_arg1735z00_1287);
																																				}
																																				BgL_setzd2namezd2_1285
																																					=
																																					BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																																					(BgL_list1734z00_1286);
																																			}
																																			{	/* Tools/trace.sch 53 */
																																				obj_t
																																					BgL_getzd2typezd2idz00_1290;
																																				{	/* Tools/trace.sch 53 */
																																					bool_t
																																						BgL_test2022z00_2013;
																																					{	/* Tools/trace.sch 53 */
																																						obj_t
																																							BgL_classz00_1291;
																																						BgL_classz00_1291
																																							=
																																							BGl_cstructz00zzforeign_ctypez00;
																																						{	/* Tools/trace.sch 53 */
																																							BgL_objectz00_bglt
																																								BgL_arg1807z00_1292;
																																							{	/* Tools/trace.sch 53 */
																																								obj_t
																																									BgL_tmpz00_2014;
																																								BgL_tmpz00_2014
																																									=
																																									(
																																									(obj_t)
																																									((BgL_objectz00_bglt) BgL_afzd2typezd2_1275));
																																								BgL_arg1807z00_1292
																																									=
																																									(BgL_objectz00_bglt)
																																									(BgL_tmpz00_2014);
																																							}
																																							if (BGL_CONDEXPAND_ISA_ARCH64())
																																								{	/* Tools/trace.sch 53 */
																																									long
																																										BgL_idxz00_1293;
																																									BgL_idxz00_1293
																																										=
																																										BGL_OBJECT_INHERITANCE_NUM
																																										(BgL_arg1807z00_1292);
																																									BgL_test2022z00_2013
																																										=
																																										(VECTOR_REF
																																										(BGl_za2inheritancesza2z00zz__objectz00,
																																											(BgL_idxz00_1293
																																												+
																																												2L))
																																										==
																																										BgL_classz00_1291);
																																								}
																																							else
																																								{	/* Tools/trace.sch 53 */
																																									bool_t
																																										BgL_res1835z00_1296;
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_oclassz00_1297;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1815z00_1298;
																																											long
																																												BgL_arg1816z00_1299;
																																											BgL_arg1815z00_1298
																																												=
																																												(BGl_za2classesza2z00zz__objectz00);
																																											{	/* Tools/trace.sch 53 */
																																												long
																																													BgL_arg1817z00_1300;
																																												BgL_arg1817z00_1300
																																													=
																																													BGL_OBJECT_CLASS_NUM
																																													(BgL_arg1807z00_1292);
																																												BgL_arg1816z00_1299
																																													=
																																													(BgL_arg1817z00_1300
																																													-
																																													OBJECT_TYPE);
																																											}
																																											BgL_oclassz00_1297
																																												=
																																												VECTOR_REF
																																												(BgL_arg1815z00_1298,
																																												BgL_arg1816z00_1299);
																																										}
																																										{	/* Tools/trace.sch 53 */
																																											bool_t
																																												BgL__ortest_1115z00_1301;
																																											BgL__ortest_1115z00_1301
																																												=
																																												(BgL_classz00_1291
																																												==
																																												BgL_oclassz00_1297);
																																											if (BgL__ortest_1115z00_1301)
																																												{	/* Tools/trace.sch 53 */
																																													BgL_res1835z00_1296
																																														=
																																														BgL__ortest_1115z00_1301;
																																												}
																																											else
																																												{	/* Tools/trace.sch 53 */
																																													long
																																														BgL_odepthz00_1302;
																																													{	/* Tools/trace.sch 53 */
																																														obj_t
																																															BgL_arg1804z00_1303;
																																														BgL_arg1804z00_1303
																																															=
																																															(BgL_oclassz00_1297);
																																														BgL_odepthz00_1302
																																															=
																																															BGL_CLASS_DEPTH
																																															(BgL_arg1804z00_1303);
																																													}
																																													if ((2L < BgL_odepthz00_1302))
																																														{	/* Tools/trace.sch 53 */
																																															obj_t
																																																BgL_arg1802z00_1304;
																																															{	/* Tools/trace.sch 53 */
																																																obj_t
																																																	BgL_arg1803z00_1305;
																																																BgL_arg1803z00_1305
																																																	=
																																																	(BgL_oclassz00_1297);
																																																BgL_arg1802z00_1304
																																																	=
																																																	BGL_CLASS_ANCESTORS_REF
																																																	(BgL_arg1803z00_1305,
																																																	2L);
																																															}
																																															BgL_res1835z00_1296
																																																=
																																																(BgL_arg1802z00_1304
																																																==
																																																BgL_classz00_1291);
																																														}
																																													else
																																														{	/* Tools/trace.sch 53 */
																																															BgL_res1835z00_1296
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																										}
																																									}
																																									BgL_test2022z00_2013
																																										=
																																										BgL_res1835z00_1296;
																																								}
																																						}
																																					}
																																					if (BgL_test2022z00_2013)
																																						{	/* Tools/trace.sch 53 */
																																							obj_t
																																								BgL_arg1722z00_1306;
																																							{	/* Tools/trace.sch 53 */
																																								obj_t
																																									BgL_arg1724z00_1307;
																																								obj_t
																																									BgL_arg1733z00_1308;
																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_arg1455z00_1309;
																																									BgL_arg1455z00_1309
																																										=
																																										SYMBOL_TO_STRING
																																										(
																																										((obj_t) BgL_fzd2typezd2idz00_1273));
																																									BgL_arg1724z00_1307
																																										=
																																										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																										(BgL_arg1455z00_1309);
																																								}
																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_symbolz00_1310;
																																									BgL_symbolz00_1310
																																										=
																																										CNST_TABLE_REF
																																										(5);
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_arg1455z00_1311;
																																										BgL_arg1455z00_1311
																																											=
																																											SYMBOL_TO_STRING
																																											(BgL_symbolz00_1310);
																																										BgL_arg1733z00_1308
																																											=
																																											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																											(BgL_arg1455z00_1311);
																																									}
																																								}
																																								BgL_arg1722z00_1306
																																									=
																																									string_append
																																									(BgL_arg1724z00_1307,
																																									BgL_arg1733z00_1308);
																																							}
																																							BgL_getzd2typezd2idz00_1290
																																								=
																																								bstring_to_symbol
																																								(BgL_arg1722z00_1306);
																																						}
																																					else
																																						{	/* Tools/trace.sch 53 */
																																							BgL_getzd2typezd2idz00_1290
																																								=
																																								BgL_fzd2typezd2idz00_1273;
																																						}
																																				}
																																				{	/* Tools/trace.sch 53 */
																																					obj_t
																																						BgL_structzd2refzd2fmtz00_1312;
																																					{	/* Tools/trace.sch 53 */
																																						bool_t
																																							BgL_test2026z00_2045;
																																						{	/* Tools/trace.sch 53 */
																																							obj_t
																																								BgL_classz00_1313;
																																							BgL_classz00_1313
																																								=
																																								BGl_cstructz00zzforeign_ctypez00;
																																							{	/* Tools/trace.sch 53 */
																																								BgL_objectz00_bglt
																																									BgL_arg1807z00_1314;
																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_tmpz00_2046;
																																									BgL_tmpz00_2046
																																										=
																																										(
																																										(obj_t)
																																										((BgL_objectz00_bglt) BgL_afzd2typezd2_1275));
																																									BgL_arg1807z00_1314
																																										=
																																										(BgL_objectz00_bglt)
																																										(BgL_tmpz00_2046);
																																								}
																																								if (BGL_CONDEXPAND_ISA_ARCH64())
																																									{	/* Tools/trace.sch 53 */
																																										long
																																											BgL_idxz00_1315;
																																										BgL_idxz00_1315
																																											=
																																											BGL_OBJECT_INHERITANCE_NUM
																																											(BgL_arg1807z00_1314);
																																										BgL_test2026z00_2045
																																											=
																																											(VECTOR_REF
																																											(BGl_za2inheritancesza2z00zz__objectz00,
																																												(BgL_idxz00_1315
																																													+
																																													2L))
																																											==
																																											BgL_classz00_1313);
																																									}
																																								else
																																									{	/* Tools/trace.sch 53 */
																																										bool_t
																																											BgL_res1836z00_1318;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_oclassz00_1319;
																																											{	/* Tools/trace.sch 53 */
																																												obj_t
																																													BgL_arg1815z00_1320;
																																												long
																																													BgL_arg1816z00_1321;
																																												BgL_arg1815z00_1320
																																													=
																																													(BGl_za2classesza2z00zz__objectz00);
																																												{	/* Tools/trace.sch 53 */
																																													long
																																														BgL_arg1817z00_1322;
																																													BgL_arg1817z00_1322
																																														=
																																														BGL_OBJECT_CLASS_NUM
																																														(BgL_arg1807z00_1314);
																																													BgL_arg1816z00_1321
																																														=
																																														(BgL_arg1817z00_1322
																																														-
																																														OBJECT_TYPE);
																																												}
																																												BgL_oclassz00_1319
																																													=
																																													VECTOR_REF
																																													(BgL_arg1815z00_1320,
																																													BgL_arg1816z00_1321);
																																											}
																																											{	/* Tools/trace.sch 53 */
																																												bool_t
																																													BgL__ortest_1115z00_1323;
																																												BgL__ortest_1115z00_1323
																																													=
																																													(BgL_classz00_1313
																																													==
																																													BgL_oclassz00_1319);
																																												if (BgL__ortest_1115z00_1323)
																																													{	/* Tools/trace.sch 53 */
																																														BgL_res1836z00_1318
																																															=
																																															BgL__ortest_1115z00_1323;
																																													}
																																												else
																																													{	/* Tools/trace.sch 53 */
																																														long
																																															BgL_odepthz00_1324;
																																														{	/* Tools/trace.sch 53 */
																																															obj_t
																																																BgL_arg1804z00_1325;
																																															BgL_arg1804z00_1325
																																																=
																																																(BgL_oclassz00_1319);
																																															BgL_odepthz00_1324
																																																=
																																																BGL_CLASS_DEPTH
																																																(BgL_arg1804z00_1325);
																																														}
																																														if ((2L < BgL_odepthz00_1324))
																																															{	/* Tools/trace.sch 53 */
																																																obj_t
																																																	BgL_arg1802z00_1326;
																																																{	/* Tools/trace.sch 53 */
																																																	obj_t
																																																		BgL_arg1803z00_1327;
																																																	BgL_arg1803z00_1327
																																																		=
																																																		(BgL_oclassz00_1319);
																																																	BgL_arg1802z00_1326
																																																		=
																																																		BGL_CLASS_ANCESTORS_REF
																																																		(BgL_arg1803z00_1327,
																																																		2L);
																																																}
																																																BgL_res1836z00_1318
																																																	=
																																																	(BgL_arg1802z00_1326
																																																	==
																																																	BgL_classz00_1313);
																																															}
																																														else
																																															{	/* Tools/trace.sch 53 */
																																																BgL_res1836z00_1318
																																																	=
																																																	(
																																																	(bool_t)
																																																	0);
																																															}
																																													}
																																											}
																																										}
																																										BgL_test2026z00_2045
																																											=
																																											BgL_res1836z00_1318;
																																									}
																																							}
																																						}
																																						if (BgL_test2026z00_2045)
																																							{	/* Tools/trace.sch 53 */
																																								obj_t
																																									BgL_list1704z00_1328;
																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_arg1705z00_1329;
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_arg1708z00_1330;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1709z00_1331;
																																											{	/* Tools/trace.sch 53 */
																																												obj_t
																																													BgL_arg1710z00_1332;
																																												BgL_arg1710z00_1332
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_string1968z00zzforeign_cstructz00,
																																													BNIL);
																																												BgL_arg1709z00_1331
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_fzd2namezd2_1278,
																																													BgL_arg1710z00_1332);
																																											}
																																											BgL_arg1708z00_1330
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_string1969z00zzforeign_cstructz00,
																																												BgL_arg1709z00_1331);
																																										}
																																										BgL_arg1705z00_1329
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_namezd2sanszd2z42z42_1251,
																																											BgL_arg1708z00_1330);
																																									}
																																									BgL_list1704z00_1328
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_string1970z00zzforeign_cstructz00,
																																										BgL_arg1705z00_1329);
																																								}
																																								BgL_structzd2refzd2fmtz00_1312
																																									=
																																									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																									(BgL_list1704z00_1328);
																																							}
																																						else
																																							{	/* Tools/trace.sch 53 */
																																								obj_t
																																									BgL_list1711z00_1333;
																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_arg1714z00_1334;
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_arg1717z00_1335;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1718z00_1336;
																																											{	/* Tools/trace.sch 53 */
																																												obj_t
																																													BgL_arg1720z00_1337;
																																												BgL_arg1720z00_1337
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_string1971z00zzforeign_cstructz00,
																																													BNIL);
																																												BgL_arg1718z00_1336
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_fzd2namezd2_1278,
																																													BgL_arg1720z00_1337);
																																											}
																																											BgL_arg1717z00_1335
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_string1969z00zzforeign_cstructz00,
																																												BgL_arg1718z00_1336);
																																										}
																																										BgL_arg1714z00_1334
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_namezd2sanszd2z42z42_1251,
																																											BgL_arg1717z00_1335);
																																									}
																																									BgL_list1711z00_1333
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BGl_string1972z00zzforeign_cstructz00,
																																										BgL_arg1714z00_1334);
																																								}
																																								BgL_structzd2refzd2fmtz00_1312
																																									=
																																									BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																									(BgL_list1711z00_1333);
																																							}
																																					}
																																					{	/* Tools/trace.sch 53 */
																																						obj_t
																																							BgL_structzd2setzd2fmtz00_1338;
																																						{	/* Tools/trace.sch 53 */
																																							obj_t
																																								BgL_list1693z00_1339;
																																							{	/* Tools/trace.sch 53 */
																																								obj_t
																																									BgL_arg1699z00_1340;
																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_arg1700z00_1341;
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_arg1701z00_1342;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1702z00_1343;
																																											BgL_arg1702z00_1343
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_string1971z00zzforeign_cstructz00,
																																												BNIL);
																																											BgL_arg1701z00_1342
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_fzd2namezd2_1278,
																																												BgL_arg1702z00_1343);
																																										}
																																										BgL_arg1700z00_1341
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BGl_string1969z00zzforeign_cstructz00,
																																											BgL_arg1701z00_1342);
																																									}
																																									BgL_arg1699z00_1340
																																										=
																																										MAKE_YOUNG_PAIR
																																										(BgL_namezd2sanszd2z42z42_1251,
																																										BgL_arg1700z00_1341);
																																								}
																																								BgL_list1693z00_1339
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BGl_string1973z00zzforeign_cstructz00,
																																									BgL_arg1699z00_1340);
																																							}
																																							BgL_structzd2setzd2fmtz00_1338
																																								=
																																								BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																																								(BgL_list1693z00_1339);
																																						}
																																						{	/* Tools/trace.sch 53 */
																																							obj_t
																																								BgL_structzd2setvzd2fmtz00_1344;
																																							{	/* Tools/trace.sch 53 */
																																								bool_t
																																									BgL_test2030z00_2087;
																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_classz00_1345;
																																									BgL_classz00_1345
																																										=
																																										BGl_cstructz00zzforeign_ctypez00;
																																									{	/* Tools/trace.sch 53 */
																																										BgL_objectz00_bglt
																																											BgL_arg1807z00_1346;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_tmpz00_2088;
																																											BgL_tmpz00_2088
																																												=
																																												(
																																												(obj_t)
																																												((BgL_objectz00_bglt) BgL_afzd2typezd2_1275));
																																											BgL_arg1807z00_1346
																																												=
																																												(BgL_objectz00_bglt)
																																												(BgL_tmpz00_2088);
																																										}
																																										if (BGL_CONDEXPAND_ISA_ARCH64())
																																											{	/* Tools/trace.sch 53 */
																																												long
																																													BgL_idxz00_1347;
																																												BgL_idxz00_1347
																																													=
																																													BGL_OBJECT_INHERITANCE_NUM
																																													(BgL_arg1807z00_1346);
																																												BgL_test2030z00_2087
																																													=
																																													(VECTOR_REF
																																													(BGl_za2inheritancesza2z00zz__objectz00,
																																														(BgL_idxz00_1347
																																															+
																																															2L))
																																													==
																																													BgL_classz00_1345);
																																											}
																																										else
																																											{	/* Tools/trace.sch 53 */
																																												bool_t
																																													BgL_res1837z00_1350;
																																												{	/* Tools/trace.sch 53 */
																																													obj_t
																																														BgL_oclassz00_1351;
																																													{	/* Tools/trace.sch 53 */
																																														obj_t
																																															BgL_arg1815z00_1352;
																																														long
																																															BgL_arg1816z00_1353;
																																														BgL_arg1815z00_1352
																																															=
																																															(BGl_za2classesza2z00zz__objectz00);
																																														{	/* Tools/trace.sch 53 */
																																															long
																																																BgL_arg1817z00_1354;
																																															BgL_arg1817z00_1354
																																																=
																																																BGL_OBJECT_CLASS_NUM
																																																(BgL_arg1807z00_1346);
																																															BgL_arg1816z00_1353
																																																=
																																																(BgL_arg1817z00_1354
																																																-
																																																OBJECT_TYPE);
																																														}
																																														BgL_oclassz00_1351
																																															=
																																															VECTOR_REF
																																															(BgL_arg1815z00_1352,
																																															BgL_arg1816z00_1353);
																																													}
																																													{	/* Tools/trace.sch 53 */
																																														bool_t
																																															BgL__ortest_1115z00_1355;
																																														BgL__ortest_1115z00_1355
																																															=
																																															(BgL_classz00_1345
																																															==
																																															BgL_oclassz00_1351);
																																														if (BgL__ortest_1115z00_1355)
																																															{	/* Tools/trace.sch 53 */
																																																BgL_res1837z00_1350
																																																	=
																																																	BgL__ortest_1115z00_1355;
																																															}
																																														else
																																															{	/* Tools/trace.sch 53 */
																																																long
																																																	BgL_odepthz00_1356;
																																																{	/* Tools/trace.sch 53 */
																																																	obj_t
																																																		BgL_arg1804z00_1357;
																																																	BgL_arg1804z00_1357
																																																		=
																																																		(BgL_oclassz00_1351);
																																																	BgL_odepthz00_1356
																																																		=
																																																		BGL_CLASS_DEPTH
																																																		(BgL_arg1804z00_1357);
																																																}
																																																if ((2L < BgL_odepthz00_1356))
																																																	{	/* Tools/trace.sch 53 */
																																																		obj_t
																																																			BgL_arg1802z00_1358;
																																																		{	/* Tools/trace.sch 53 */
																																																			obj_t
																																																				BgL_arg1803z00_1359;
																																																			BgL_arg1803z00_1359
																																																				=
																																																				(BgL_oclassz00_1351);
																																																			BgL_arg1802z00_1358
																																																				=
																																																				BGL_CLASS_ANCESTORS_REF
																																																				(BgL_arg1803z00_1359,
																																																				2L);
																																																		}
																																																		BgL_res1837z00_1350
																																																			=
																																																			(BgL_arg1802z00_1358
																																																			==
																																																			BgL_classz00_1345);
																																																	}
																																																else
																																																	{	/* Tools/trace.sch 53 */
																																																		BgL_res1837z00_1350
																																																			=
																																																			(
																																																			(bool_t)
																																																			0);
																																																	}
																																															}
																																													}
																																												}
																																												BgL_test2030z00_2087
																																													=
																																													BgL_res1837z00_1350;
																																											}
																																									}
																																								}
																																								if (BgL_test2030z00_2087)
																																									{	/* Tools/trace.sch 53 */
																																										BgL_structzd2setvzd2fmtz00_1344
																																											=
																																											BGl_string1974z00zzforeign_cstructz00;
																																									}
																																								else
																																									{	/* Tools/trace.sch 53 */
																																										BgL_structzd2setvzd2fmtz00_1344
																																											=
																																											BGl_string1975z00zzforeign_cstructz00;
																																									}
																																							}
																																							{	/* Tools/trace.sch 53 */

																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_list1596z00_1360;
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_arg1602z00_1361;
																																										BgL_arg1602z00_1361
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_setzd2namezd2_1285,
																																											BNIL);
																																										BgL_list1596z00_1360
																																											=
																																											MAKE_YOUNG_PAIR
																																											(BgL_getzd2namezd2_1281,
																																											BgL_arg1602z00_1361);
																																									}
																																									BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
																																										(BgL_list1596z00_1360);
																																								}
																																								{	/* Tools/trace.sch 53 */
																																									obj_t
																																										BgL_arg1605z00_1362;
																																									obj_t
																																										BgL_arg1606z00_1363;
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_arg1611z00_1364;
																																										obj_t
																																											BgL_arg1613z00_1365;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1615z00_1366;
																																											obj_t
																																												BgL_arg1616z00_1367;
																																											BgL_arg1615z00_1366
																																												=
																																												BGl_makezd2typedzd2identz00zzast_identz00
																																												(BgL_getzd2namezd2_1281,
																																												BgL_getzd2typezd2idz00_1290);
																																											BgL_arg1616z00_1367
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BGl_makezd2typedzd2identz00zzast_identz00
																																												(CNST_TABLE_REF
																																													(6),
																																													BgL_idz00_1226),
																																												BNIL);
																																											BgL_arg1611z00_1364
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1615z00_1366,
																																												BgL_arg1616z00_1367);
																																										}
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1626z00_1368;
																																											obj_t
																																												BgL_arg1627z00_1369;
																																											BgL_arg1626z00_1368
																																												=
																																												BGl_makezd2typedzd2identz00zzast_identz00
																																												(CNST_TABLE_REF
																																												(7),
																																												BgL_getzd2typezd2idz00_1290);
																																											{	/* Tools/trace.sch 53 */
																																												obj_t
																																													BgL_arg1629z00_1370;
																																												BgL_arg1629z00_1370
																																													=
																																													MAKE_YOUNG_PAIR
																																													(CNST_TABLE_REF
																																													(6),
																																													BNIL);
																																												BgL_arg1627z00_1369
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_structzd2refzd2fmtz00_1312,
																																													BgL_arg1629z00_1370);
																																											}
																																											BgL_arg1613z00_1365
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1626z00_1368,
																																												BgL_arg1627z00_1369);
																																										}
																																										BgL_arg1605z00_1362
																																											=
																																											BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																																											(BgL_arg1611z00_1364,
																																											BgL_arg1613z00_1365);
																																									}
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_arg1630z00_1371;
																																										obj_t
																																											BgL_arg1642z00_1372;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1646z00_1373;
																																											obj_t
																																												BgL_arg1650z00_1374;
																																											{	/* Tools/trace.sch 53 */
																																												obj_t
																																													BgL_arg1651z00_1375;
																																												{	/* Tools/trace.sch 53 */
																																													obj_t
																																														BgL_arg1654z00_1376;
																																													obj_t
																																														BgL_arg1661z00_1377;
																																													{	/* Tools/trace.sch 53 */
																																														obj_t
																																															BgL_arg1455z00_1378;
																																														BgL_arg1455z00_1378
																																															=
																																															SYMBOL_TO_STRING
																																															(BgL_setzd2namezd2_1285);
																																														BgL_arg1654z00_1376
																																															=
																																															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																															(BgL_arg1455z00_1378);
																																													}
																																													{	/* Tools/trace.sch 53 */
																																														obj_t
																																															BgL_symbolz00_1379;
																																														BgL_symbolz00_1379
																																															=
																																															CNST_TABLE_REF
																																															(8);
																																														{	/* Tools/trace.sch 53 */
																																															obj_t
																																																BgL_arg1455z00_1380;
																																															BgL_arg1455z00_1380
																																																=
																																																SYMBOL_TO_STRING
																																																(BgL_symbolz00_1379);
																																															BgL_arg1661z00_1377
																																																=
																																																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																																(BgL_arg1455z00_1380);
																																														}
																																													}
																																													BgL_arg1651z00_1375
																																														=
																																														string_append
																																														(BgL_arg1654z00_1376,
																																														BgL_arg1661z00_1377);
																																												}
																																												BgL_arg1646z00_1373
																																													=
																																													bstring_to_symbol
																																													(BgL_arg1651z00_1375);
																																											}
																																											{	/* Tools/trace.sch 53 */
																																												obj_t
																																													BgL_arg1663z00_1381;
																																												obj_t
																																													BgL_arg1675z00_1382;
																																												BgL_arg1663z00_1381
																																													=
																																													BGl_makezd2typedzd2identz00zzast_identz00
																																													(CNST_TABLE_REF
																																													(6),
																																													BgL_idz00_1226);
																																												BgL_arg1675z00_1382
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BGl_makezd2typedzd2identz00zzast_identz00
																																													(CNST_TABLE_REF
																																														(9),
																																														BgL_getzd2typezd2idz00_1290),
																																													BNIL);
																																												BgL_arg1650z00_1374
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1663z00_1381,
																																													BgL_arg1675z00_1382);
																																											}
																																											BgL_arg1630z00_1371
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1646z00_1373,
																																												BgL_arg1650z00_1374);
																																										}
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1681z00_1383;
																																											{	/* Tools/trace.sch 53 */
																																												obj_t
																																													BgL_arg1688z00_1384;
																																												obj_t
																																													BgL_arg1689z00_1385;
																																												BgL_arg1688z00_1384
																																													=
																																													string_append
																																													(BgL_structzd2setzd2fmtz00_1338,
																																													BgL_structzd2setvzd2fmtz00_1344);
																																												{	/* Tools/trace.sch 53 */
																																													obj_t
																																														BgL_arg1691z00_1386;
																																													BgL_arg1691z00_1386
																																														=
																																														MAKE_YOUNG_PAIR
																																														(CNST_TABLE_REF
																																														(9),
																																														BNIL);
																																													BgL_arg1689z00_1385
																																														=
																																														MAKE_YOUNG_PAIR
																																														(CNST_TABLE_REF
																																														(6),
																																														BgL_arg1691z00_1386);
																																												}
																																												BgL_arg1681z00_1383
																																													=
																																													MAKE_YOUNG_PAIR
																																													(BgL_arg1688z00_1384,
																																													BgL_arg1689z00_1385);
																																											}
																																											BgL_arg1642z00_1372
																																												=
																																												MAKE_YOUNG_PAIR
																																												(CNST_TABLE_REF
																																												(7),
																																												BgL_arg1681z00_1383);
																																										}
																																										BgL_arg1606z00_1363
																																											=
																																											BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																																											(BgL_arg1630z00_1371,
																																											BgL_arg1642z00_1372);
																																									}
																																									{	/* Tools/trace.sch 53 */
																																										obj_t
																																											BgL_list1607z00_1387;
																																										{	/* Tools/trace.sch 53 */
																																											obj_t
																																												BgL_arg1609z00_1388;
																																											BgL_arg1609z00_1388
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1606z00_1363,
																																												BNIL);
																																											BgL_list1607z00_1387
																																												=
																																												MAKE_YOUNG_PAIR
																																												(BgL_arg1605z00_1362,
																																												BgL_arg1609z00_1388);
																																										}
																																										BgL_arg1748z00_1270
																																											=
																																											BgL_list1607z00_1387;
																																									}
																																								}
																																							}
																																						}
																																					}
																																				}
																																			}
																																		}
																																	}
																																}
																															}
																														}
																													}
																												}
																											}
																											BgL_arg1747z00_1269 =
																												BGl_appendzd221011zd2zzforeign_cstructz00
																												(BgL_arg1748z00_1270,
																												BgL_resz00_1267);
																										}
																										{
																											obj_t BgL_resz00_2153;
																											obj_t BgL_fieldsz00_2152;

																											BgL_fieldsz00_2152 =
																												BgL_arg1746z00_1268;
																											BgL_resz00_2153 =
																												BgL_arg1747z00_1269;
																											BgL_resz00_1267 =
																												BgL_resz00_2153;
																											BgL_fieldsz00_1266 =
																												BgL_fieldsz00_2152;
																											goto BgL_loopz00_1265;
																										}
																									}
																							}
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1233z00_1518;
																								obj_t BgL_arg1234z00_1519;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1236z00_1520;

																									BgL_arg1236z00_1520 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(22), BNIL);
																									BgL_arg1233z00_1518 =
																										MAKE_YOUNG_PAIR
																										(BgL_bidzf3zd2boolz21_1243,
																										BgL_arg1236z00_1520);
																								}
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1238z00_1521;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1239z00_1522;
																										obj_t BgL_arg1242z00_1523;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1244z00_1524;

																											BgL_arg1244z00_1524 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(6),
																												BNIL);
																											BgL_arg1239z00_1522 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(23),
																												BgL_arg1244z00_1524);
																										}
																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1248z00_1525;
																											obj_t BgL_arg1249z00_1526;

																											{	/* Tools/trace.sch 53 */
																												obj_t
																													BgL_arg1252z00_1527;
																												{	/* Tools/trace.sch 53 */
																													obj_t
																														BgL_arg1268z00_1528;
																													obj_t
																														BgL_arg1272z00_1529;
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1284z00_1530;
																														BgL_arg1284z00_1530
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(6), BNIL);
																														BgL_arg1268z00_1528
																															=
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(24),
																															BgL_arg1284z00_1530);
																													}
																													{	/* Tools/trace.sch 53 */
																														obj_t
																															BgL_arg1304z00_1531;
																														{	/* Tools/trace.sch 53 */
																															obj_t
																																BgL_arg1305z00_1532;
																															BgL_arg1305z00_1532
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_bidz00_1227,
																																BNIL);
																															BgL_arg1304z00_1531
																																=
																																MAKE_YOUNG_PAIR
																																(CNST_TABLE_REF
																																(25),
																																BgL_arg1305z00_1532);
																														}
																														BgL_arg1272z00_1529
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1304z00_1531,
																															BNIL);
																													}
																													BgL_arg1252z00_1527 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1268z00_1528,
																														BgL_arg1272z00_1529);
																												}
																												BgL_arg1248z00_1525 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(26),
																													BgL_arg1252z00_1527);
																											}
																											BgL_arg1249z00_1526 =
																												MAKE_YOUNG_PAIR(BFALSE,
																												BNIL);
																											BgL_arg1242z00_1523 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1248z00_1525,
																												BgL_arg1249z00_1526);
																										}
																										BgL_arg1238z00_1521 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1239z00_1522,
																											BgL_arg1242z00_1523);
																									}
																									BgL_arg1234z00_1519 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(27),
																										BgL_arg1238z00_1521);
																								}
																								BgL_tmpz00_1958 =
																									BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																									(BgL_arg1233z00_1518,
																									BgL_arg1234z00_1519);
																							}
																							BgL_arg1210z00_1600 =
																								MAKE_YOUNG_PAIR(BgL_tmpz00_1958,
																								BgL_auxz00_1983);
																						}
																						BgL_arg1208z00_1598 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1209z00_1599,
																							BgL_arg1210z00_1600);
																					}
																					BgL_arg1203z00_1596 =
																						MAKE_YOUNG_PAIR(BgL_arg1206z00_1597,
																						BgL_arg1208z00_1598);
																				}
																				BgL_arg1201z00_1594 =
																					MAKE_YOUNG_PAIR(BgL_arg1202z00_1595,
																					BgL_arg1203z00_1596);
																			}
																			BgL_arg1199z00_1592 =
																				MAKE_YOUNG_PAIR(BgL_arg1200z00_1593,
																				BgL_arg1201z00_1594);
																		}
																		return
																			MAKE_YOUNG_PAIR(BgL_arg1198z00_1591,
																			BgL_arg1199z00_1592);
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &make-ctype-accesses!1104 */
	obj_t BGl_z62makezd2ctypezd2accessesz121104z70zzforeign_cstructz00(obj_t
		BgL_envz00_1204, obj_t BgL_whatz00_1205, obj_t BgL_whoz00_1206,
		obj_t BgL_locz00_1207)
	{
		{	/* Foreign/cstruct.scm 32 */
			return BNIL;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_cstructz00(void)
	{
		{	/* Foreign/cstruct.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zzforeign_ctypez00(396356879L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zzforeign_libraryz00(419430976L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1984z00zzforeign_cstructz00));
		}

	}

#ifdef __cplusplus
}
#endif
