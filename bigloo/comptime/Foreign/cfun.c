/*===========================================================================*/
/*   (Foreign/cfun.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/cfun.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_CFUNCTION_TYPE_DEFINITIONS
#define BGL_FOREIGN_CFUNCTION_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_cfunctionz00_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
		long BgL_arityz00;
		struct BgL_typez00_bgl *BgL_typezd2reszd2;
		obj_t BgL_typezd2argszd2;
	}                   *BgL_cfunctionz00_bglt;


#endif													// BGL_FOREIGN_CFUNCTION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzforeign_cfunctionz00 =
		BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_cfunctionz00zzforeign_ctypez00;
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzforeign_cfunctionz00(void);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	extern obj_t BGl_za2maxzd2czd2foreignzd2arityza2zd2zzengine_paramz00;
	static obj_t BGl_genericzd2initzd2zzforeign_cfunctionz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzforeign_cfunctionz00(void);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzforeign_cfunctionz00(void);
	extern obj_t
		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t
		BGl_z62makezd2ctypezd2accessesz121113z70zzforeign_cfunctionz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_cfunctionz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_makezd2protozd2inlinez00zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_cnstzd2initzd2zzforeign_cfunctionz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_cfunctionz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_cfunctionz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_cfunctionz00(void);
	extern obj_t BGl_makezd2definezd2inlinez00zzforeign_libraryz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t __cnst[25];


	   
		 
		DEFINE_STRING(BGl_string1565z00zzforeign_cfunctionz00,
		BgL_bgl_string1565za700za7za7f1585za7, "make-ctype-accesses!", 20);
	      DEFINE_STRING(BGl_string1566z00zzforeign_cfunctionz00,
		BgL_bgl_string1566za700za7za7f1586za7,
		"Too large arity for a foreign function (max", 43);
	      DEFINE_STRING(BGl_string1567z00zzforeign_cfunctionz00,
		BgL_bgl_string1567za700za7za7f1587za7, ")", 1);
	      DEFINE_STRING(BGl_string1568z00zzforeign_cfunctionz00,
		BgL_bgl_string1568za700za7za7f1588za7, " args provided", 14);
	      DEFINE_STRING(BGl_string1569z00zzforeign_cfunctionz00,
		BgL_bgl_string1569za700za7za7f1589za7, "bigloo", 6);
	      DEFINE_STRING(BGl_string1570z00zzforeign_cfunctionz00,
		BgL_bgl_string1570za700za7za7f1590za7,
		"Can't manage pointers on C multiple arity function", 50);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1564z00zzforeign_cfunctionz00,
		BgL_bgl_za762makeza7d2ctypeza71591za7,
		BGl_z62makezd2ctypezd2accessesz121113z70zzforeign_cfunctionz00, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string1571z00zzforeign_cfunctionz00,
		BgL_bgl_string1571za700za7za7f1592za7, "C_FUNCTION_CALL_", 16);
	      DEFINE_STRING(BGl_string1572z00zzforeign_cfunctionz00,
		BgL_bgl_string1572za700za7za7f1593za7, "(", 1);
	      DEFINE_STRING(BGl_string1573z00zzforeign_cfunctionz00,
		BgL_bgl_string1573za700za7za7f1594za7, ")FOREIGN_TO_COBJ", 16);
	      DEFINE_STRING(BGl_string1574z00zzforeign_cfunctionz00,
		BgL_bgl_string1574za700za7za7f1595za7, "cobj_to_foreign", 15);
	      DEFINE_STRING(BGl_string1575z00zzforeign_cfunctionz00,
		BgL_bgl_string1575za700za7za7f1596za7, "foreign_cfunction", 17);
	      DEFINE_STRING(BGl_string1576z00zzforeign_cfunctionz00,
		BgL_bgl_string1576za700za7za7f1597za7,
		"pragma predicate-of static ::obj foreign symbol if eq? quote foreign-id foreign? o o::obj (pragma::bool \"($1 == $2)\" o1 o2) o2 o1 = ?::bool macro f c- ::bool ? -> -call ",
		169);
	extern obj_t BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_cfunctionz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzforeign_cfunctionz00(long
		BgL_checksumz00_1089, char *BgL_fromz00_1090)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_cfunctionz00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_cfunctionz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_cfunctionz00();
					BGl_libraryzd2moduleszd2initz00zzforeign_cfunctionz00();
					BGl_cnstzd2initzd2zzforeign_cfunctionz00();
					BGl_importedzd2moduleszd2initz00zzforeign_cfunctionz00();
					BGl_methodzd2initzd2zzforeign_cfunctionz00();
					return BGl_toplevelzd2initzd2zzforeign_cfunctionz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_cfunctionz00(void)
	{
		{	/* Foreign/cfun.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"foreign_cfunction");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"foreign_cfunction");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"foreign_cfunction");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_cfunction");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"foreign_cfunction");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"foreign_cfunction");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_cfunction");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzforeign_cfunctionz00(void)
	{
		{	/* Foreign/cfun.scm 15 */
			{	/* Foreign/cfun.scm 15 */
				obj_t BgL_cportz00_888;

				{	/* Foreign/cfun.scm 15 */
					obj_t BgL_stringz00_895;

					BgL_stringz00_895 = BGl_string1576z00zzforeign_cfunctionz00;
					{	/* Foreign/cfun.scm 15 */
						obj_t BgL_startz00_896;

						BgL_startz00_896 = BINT(0L);
						{	/* Foreign/cfun.scm 15 */
							obj_t BgL_endz00_897;

							BgL_endz00_897 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_895)));
							{	/* Foreign/cfun.scm 15 */

								BgL_cportz00_888 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_895, BgL_startz00_896, BgL_endz00_897);
				}}}}
				{
					long BgL_iz00_889;

					BgL_iz00_889 = 24L;
				BgL_loopz00_890:
					if ((BgL_iz00_889 == -1L))
						{	/* Foreign/cfun.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Foreign/cfun.scm 15 */
							{	/* Foreign/cfun.scm 15 */
								obj_t BgL_arg1584z00_891;

								{	/* Foreign/cfun.scm 15 */

									{	/* Foreign/cfun.scm 15 */
										obj_t BgL_locationz00_893;

										BgL_locationz00_893 = BBOOL(((bool_t) 0));
										{	/* Foreign/cfun.scm 15 */

											BgL_arg1584z00_891 =
												BGl_readz00zz__readerz00(BgL_cportz00_888,
												BgL_locationz00_893);
										}
									}
								}
								{	/* Foreign/cfun.scm 15 */
									int BgL_tmpz00_1116;

									BgL_tmpz00_1116 = (int) (BgL_iz00_889);
									CNST_TABLE_SET(BgL_tmpz00_1116, BgL_arg1584z00_891);
							}}
							{	/* Foreign/cfun.scm 15 */
								int BgL_auxz00_894;

								BgL_auxz00_894 = (int) ((BgL_iz00_889 - 1L));
								{
									long BgL_iz00_1121;

									BgL_iz00_1121 = (long) (BgL_auxz00_894);
									BgL_iz00_889 = BgL_iz00_1121;
									goto BgL_loopz00_890;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_cfunctionz00(void)
	{
		{	/* Foreign/cfun.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzforeign_cfunctionz00(void)
	{
		{	/* Foreign/cfun.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_cfunctionz00(void)
	{
		{	/* Foreign/cfun.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_cfunctionz00(void)
	{
		{	/* Foreign/cfun.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_cfunctionz00(void)
	{
		{	/* Foreign/cfun.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
				BGl_cfunctionz00zzforeign_ctypez00,
				BGl_proc1564z00zzforeign_cfunctionz00,
				BGl_string1565z00zzforeign_cfunctionz00);
		}

	}



/* &make-ctype-accesses!1113 */
	obj_t BGl_z62makezd2ctypezd2accessesz121113z70zzforeign_cfunctionz00(obj_t
		BgL_envz00_880, obj_t BgL_whatz00_881, obj_t BgL_whoz00_882,
		obj_t BgL_locz00_883)
	{
		{	/* Foreign/cfun.scm 30 */
			{	/* Foreign/cfun.scm 31 */
				BgL_typez00_bglt BgL_btypez00_901;

				{
					BgL_cfunctionz00_bglt BgL_auxz00_1125;

					{
						obj_t BgL_auxz00_1126;

						{	/* Foreign/cfun.scm 31 */
							BgL_objectz00_bglt BgL_tmpz00_1127;

							BgL_tmpz00_1127 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_whatz00_881));
							BgL_auxz00_1126 = BGL_OBJECT_WIDENING(BgL_tmpz00_1127);
						}
						BgL_auxz00_1125 = ((BgL_cfunctionz00_bglt) BgL_auxz00_1126);
					}
					BgL_btypez00_901 =
						(((BgL_cfunctionz00_bglt) COBJECT(BgL_auxz00_1125))->BgL_btypez00);
				}
				{	/* Foreign/cfun.scm 31 */
					obj_t BgL_idz00_902;

					BgL_idz00_902 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_whoz00_882)))->BgL_idz00);
					{	/* Foreign/cfun.scm 33 */
						obj_t BgL_bidz00_903;

						BgL_bidz00_903 =
							(((BgL_typez00_bglt) COBJECT(BgL_btypez00_901))->BgL_idz00);
						{	/* Foreign/cfun.scm 34 */
							obj_t BgL_callzd2idzd2_904;

							{	/* Foreign/cfun.scm 35 */
								obj_t BgL_arg1448z00_905;

								{	/* Foreign/cfun.scm 35 */
									obj_t BgL_arg1453z00_906;
									obj_t BgL_arg1454z00_907;

									{	/* Foreign/cfun.scm 35 */
										obj_t BgL_arg1455z00_908;

										BgL_arg1455z00_908 = SYMBOL_TO_STRING(BgL_idz00_902);
										BgL_arg1453z00_906 =
											BGl_stringzd2copyzd2zz__r4_strings_6_7z00
											(BgL_arg1455z00_908);
									}
									{	/* Foreign/cfun.scm 35 */
										obj_t BgL_symbolz00_909;

										BgL_symbolz00_909 = CNST_TABLE_REF(0);
										{	/* Foreign/cfun.scm 35 */
											obj_t BgL_arg1455z00_910;

											BgL_arg1455z00_910 = SYMBOL_TO_STRING(BgL_symbolz00_909);
											BgL_arg1454z00_907 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_910);
										}
									}
									BgL_arg1448z00_905 =
										string_append(BgL_arg1453z00_906, BgL_arg1454z00_907);
								}
								BgL_callzd2idzd2_904 = bstring_to_symbol(BgL_arg1448z00_905);
							}
							{	/* Foreign/cfun.scm 35 */
								obj_t BgL_idzd2ze3bidz31_911;

								{	/* Foreign/cfun.scm 36 */
									obj_t BgL_list1423z00_912;

									{	/* Foreign/cfun.scm 36 */
										obj_t BgL_arg1434z00_913;

										{	/* Foreign/cfun.scm 36 */
											obj_t BgL_arg1437z00_914;

											BgL_arg1437z00_914 =
												MAKE_YOUNG_PAIR(BgL_bidz00_903, BNIL);
											BgL_arg1434z00_913 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg1437z00_914);
										}
										BgL_list1423z00_912 =
											MAKE_YOUNG_PAIR(BgL_idz00_902, BgL_arg1434z00_913);
									}
									BgL_idzd2ze3bidz31_911 =
										BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
										(BgL_list1423z00_912);
								}
								{	/* Foreign/cfun.scm 36 */
									obj_t BgL_bidzd2ze3idz31_915;

									{	/* Foreign/cfun.scm 37 */
										obj_t BgL_list1411z00_916;

										{	/* Foreign/cfun.scm 37 */
											obj_t BgL_arg1421z00_917;

											{	/* Foreign/cfun.scm 37 */
												obj_t BgL_arg1422z00_918;

												BgL_arg1422z00_918 =
													MAKE_YOUNG_PAIR(BgL_idz00_902, BNIL);
												BgL_arg1421z00_917 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
													BgL_arg1422z00_918);
											}
											BgL_list1411z00_916 =
												MAKE_YOUNG_PAIR(BgL_bidz00_903, BgL_arg1421z00_917);
										}
										BgL_bidzd2ze3idz31_915 =
											BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
											(BgL_list1411z00_916);
									}
									{	/* Foreign/cfun.scm 37 */
										obj_t BgL_bidzf3zf3_919;

										{	/* Foreign/cfun.scm 38 */
											obj_t BgL_arg1380z00_920;

											{	/* Foreign/cfun.scm 38 */
												obj_t BgL_arg1408z00_921;
												obj_t BgL_arg1410z00_922;

												{	/* Foreign/cfun.scm 38 */
													obj_t BgL_arg1455z00_923;

													BgL_arg1455z00_923 = SYMBOL_TO_STRING(BgL_idz00_902);
													BgL_arg1408z00_921 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_923);
												}
												{	/* Foreign/cfun.scm 38 */
													obj_t BgL_symbolz00_924;

													BgL_symbolz00_924 = CNST_TABLE_REF(2);
													{	/* Foreign/cfun.scm 38 */
														obj_t BgL_arg1455z00_925;

														BgL_arg1455z00_925 =
															SYMBOL_TO_STRING(BgL_symbolz00_924);
														BgL_arg1410z00_922 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_925);
													}
												}
												BgL_arg1380z00_920 =
													string_append(BgL_arg1408z00_921, BgL_arg1410z00_922);
											}
											BgL_bidzf3zf3_919 = bstring_to_symbol(BgL_arg1380z00_920);
										}
										{	/* Foreign/cfun.scm 38 */
											obj_t BgL_bidzf3zd2boolz21_926;

											{	/* Foreign/cfun.scm 39 */
												obj_t BgL_arg1377z00_927;

												{	/* Foreign/cfun.scm 39 */
													obj_t BgL_arg1378z00_928;
													obj_t BgL_arg1379z00_929;

													{	/* Foreign/cfun.scm 39 */
														obj_t BgL_arg1455z00_930;

														BgL_arg1455z00_930 =
															SYMBOL_TO_STRING(BgL_bidzf3zf3_919);
														BgL_arg1378z00_928 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_930);
													}
													{	/* Foreign/cfun.scm 39 */
														obj_t BgL_symbolz00_931;

														BgL_symbolz00_931 = CNST_TABLE_REF(3);
														{	/* Foreign/cfun.scm 39 */
															obj_t BgL_arg1455z00_932;

															BgL_arg1455z00_932 =
																SYMBOL_TO_STRING(BgL_symbolz00_931);
															BgL_arg1379z00_929 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_932);
														}
													}
													BgL_arg1377z00_927 =
														string_append(BgL_arg1378z00_928,
														BgL_arg1379z00_929);
												}
												BgL_bidzf3zd2boolz21_926 =
													bstring_to_symbol(BgL_arg1377z00_927);
											}
											{	/* Foreign/cfun.scm 39 */
												obj_t BgL_namez00_933;

												BgL_namez00_933 =
													(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_whoz00_882)))->
													BgL_namez00);
												{	/* Foreign/cfun.scm 40 */
													obj_t BgL_namezd2sanszd2z42z42_934;

													BgL_namezd2sanszd2z42z42_934 =
														BGl_stringzd2sanszd2z42z42zztype_toolsz00
														(BgL_namez00_933);
													{	/* Foreign/cfun.scm 41 */
														BgL_typez00_bglt BgL_typezd2reszd2_935;

														{
															BgL_cfunctionz00_bglt BgL_auxz00_1170;

															{
																obj_t BgL_auxz00_1171;

																{	/* Foreign/cfun.scm 42 */
																	BgL_objectz00_bglt BgL_tmpz00_1172;

																	BgL_tmpz00_1172 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_whatz00_881));
																	BgL_auxz00_1171 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_1172);
																}
																BgL_auxz00_1170 =
																	((BgL_cfunctionz00_bglt) BgL_auxz00_1171);
															}
															BgL_typezd2reszd2_935 =
																(((BgL_cfunctionz00_bglt)
																	COBJECT(BgL_auxz00_1170))->BgL_typezd2reszd2);
														}
														{	/* Foreign/cfun.scm 42 */
															obj_t BgL_typezd2argszd2_936;

															{
																BgL_cfunctionz00_bglt BgL_auxz00_1178;

																{
																	obj_t BgL_auxz00_1179;

																	{	/* Foreign/cfun.scm 43 */
																		BgL_objectz00_bglt BgL_tmpz00_1180;

																		BgL_tmpz00_1180 =
																			((BgL_objectz00_bglt)
																			((BgL_typez00_bglt) BgL_whatz00_881));
																		BgL_auxz00_1179 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_1180);
																	}
																	BgL_auxz00_1178 =
																		((BgL_cfunctionz00_bglt) BgL_auxz00_1179);
																}
																BgL_typezd2argszd2_936 =
																	(((BgL_cfunctionz00_bglt)
																		COBJECT(BgL_auxz00_1178))->
																	BgL_typezd2argszd2);
															}
															{	/* Foreign/cfun.scm 43 */
																long BgL_arityz00_937;

																{
																	BgL_cfunctionz00_bglt BgL_auxz00_1186;

																	{
																		obj_t BgL_auxz00_1187;

																		{	/* Foreign/cfun.scm 44 */
																			BgL_objectz00_bglt BgL_tmpz00_1188;

																			BgL_tmpz00_1188 =
																				((BgL_objectz00_bglt)
																				((BgL_typez00_bglt) BgL_whatz00_881));
																			BgL_auxz00_1187 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_1188);
																		}
																		BgL_auxz00_1186 =
																			((BgL_cfunctionz00_bglt) BgL_auxz00_1187);
																	}
																	BgL_arityz00_937 =
																		(((BgL_cfunctionz00_bglt)
																			COBJECT(BgL_auxz00_1186))->BgL_arityz00);
																}
																{	/* Foreign/cfun.scm 44 */
																	obj_t BgL_nbzd2argszd2_938;

																	{	/* Foreign/cfun.scm 45 */

																		BgL_nbzd2argszd2_938 =
																			BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																			(BgL_arityz00_937, 10L);
																	}
																	{	/* Foreign/cfun.scm 45 */

																		{

																			{	/* Foreign/cfun.scm 113 */
																				obj_t BgL_arg1123z00_1059;

																				{	/* Foreign/cfun.scm 113 */
																					obj_t BgL_list1127z00_1060;

																					{	/* Foreign/cfun.scm 113 */
																						obj_t BgL_arg1129z00_1061;

																						{	/* Foreign/cfun.scm 113 */
																							obj_t BgL_arg1131z00_1062;

																							BgL_arg1131z00_1062 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(2), BNIL);
																							BgL_arg1129z00_1061 =
																								MAKE_YOUNG_PAIR(BgL_idz00_902,
																								BgL_arg1131z00_1062);
																						}
																						BgL_list1127z00_1060 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(8),
																							BgL_arg1129z00_1061);
																					}
																					BgL_arg1123z00_1059 =
																						BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																						(BgL_list1127z00_1060);
																				}
																				{	/* Foreign/cfun.scm 111 */
																					obj_t BgL_list1124z00_1063;

																					{	/* Foreign/cfun.scm 111 */
																						obj_t BgL_arg1125z00_1064;

																						{	/* Foreign/cfun.scm 111 */
																							obj_t BgL_arg1126z00_1065;

																							BgL_arg1126z00_1065 =
																								MAKE_YOUNG_PAIR
																								(BgL_callzd2idzd2_904, BNIL);
																							BgL_arg1125z00_1064 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1123z00_1059,
																								BgL_arg1126z00_1065);
																						}
																						BgL_list1124z00_1063 =
																							MAKE_YOUNG_PAIR(BgL_bidzf3zf3_919,
																							BgL_arg1125z00_1064);
																					}
																					BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
																						(BgL_list1124z00_1063);
																			}}
																			{	/* Foreign/cfun.scm 117 */
																				obj_t BgL_arg1132z00_1066;

																				{	/* Foreign/cfun.scm 117 */
																					obj_t BgL_arg1137z00_1067;

																					{	/* Foreign/cfun.scm 117 */
																						obj_t BgL_arg1138z00_1068;
																						obj_t BgL_arg1140z00_1069;

																						{	/* Foreign/cfun.scm 51 */
																							obj_t BgL_arg1190z00_1053;

																							{	/* Foreign/cfun.scm 51 */
																								obj_t BgL_arg1191z00_1054;

																								{	/* Foreign/cfun.scm 51 */
																									obj_t BgL_arg1193z00_1055;

																									{	/* Foreign/cfun.scm 51 */
																										obj_t BgL_arg1194z00_1056;
																										obj_t BgL_arg1196z00_1057;

																										{	/* Foreign/cfun.scm 51 */
																											obj_t BgL_arg1197z00_1058;

																											BgL_arg1197z00_1058 =
																												MAKE_YOUNG_PAIR
																												(BgL_idz00_902, BNIL);
																											BgL_arg1194z00_1056 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(19),
																												BgL_arg1197z00_1058);
																										}
																										BgL_arg1196z00_1057 =
																											MAKE_YOUNG_PAIR
																											(BGl_string1574z00zzforeign_cfunctionz00,
																											BNIL);
																										BgL_arg1193z00_1055 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1194z00_1056,
																											BgL_arg1196z00_1057);
																									}
																									BgL_arg1191z00_1054 =
																										MAKE_YOUNG_PAIR
																										(BgL_idzd2ze3bidz31_911,
																										BgL_arg1193z00_1055);
																								}
																								BgL_arg1190z00_1053 =
																									MAKE_YOUNG_PAIR
																									(BgL_bidz00_903,
																									BgL_arg1191z00_1054);
																							}
																							BgL_arg1138z00_1068 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(6), BgL_arg1190z00_1053);
																						}
																						{	/* Foreign/cfun.scm 118 */
																							obj_t BgL_arg1141z00_1070;
																							obj_t BgL_arg1142z00_1071;

																							{	/* Foreign/cfun.scm 54 */
																								obj_t BgL_mnamez00_1047;

																								BgL_mnamez00_1047 =
																									string_append_3
																									(BGl_string1572z00zzforeign_cfunctionz00,
																									BgL_namezd2sanszd2z42z42_934,
																									BGl_string1573z00zzforeign_cfunctionz00);
																								{	/* Foreign/cfun.scm 55 */
																									obj_t BgL_arg1199z00_1048;

																									{	/* Foreign/cfun.scm 55 */
																										obj_t BgL_arg1200z00_1049;

																										{	/* Foreign/cfun.scm 55 */
																											obj_t BgL_arg1201z00_1050;

																											{	/* Foreign/cfun.scm 55 */
																												obj_t
																													BgL_arg1202z00_1051;
																												obj_t
																													BgL_arg1203z00_1052;
																												BgL_arg1202z00_1051 =
																													MAKE_YOUNG_PAIR
																													(BgL_bidz00_903,
																													BNIL);
																												BgL_arg1203z00_1052 =
																													MAKE_YOUNG_PAIR
																													(BgL_mnamez00_1047,
																													BNIL);
																												BgL_arg1201z00_1050 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1202z00_1051,
																													BgL_arg1203z00_1052);
																											}
																											BgL_arg1200z00_1049 =
																												MAKE_YOUNG_PAIR
																												(BgL_bidzd2ze3idz31_915,
																												BgL_arg1201z00_1050);
																										}
																										BgL_arg1199z00_1048 =
																											MAKE_YOUNG_PAIR
																											(BgL_idz00_902,
																											BgL_arg1200z00_1049);
																									}
																									BgL_arg1141z00_1070 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(6),
																										BgL_arg1199z00_1048);
																							}}
																							{	/* Foreign/cfun.scm 117 */
																								obj_t BgL_tmpz00_1222;

																								{	/* Foreign/cfun.scm 74 */
																									obj_t BgL_treszd2idzd2_1000;

																									BgL_treszd2idzd2_1000 =
																										(((BgL_typez00_bglt)
																											COBJECT
																											(BgL_typezd2reszd2_935))->
																										BgL_idz00);
																									{	/* Foreign/cfun.scm 74 */
																										obj_t
																											BgL_targszd2idzd2_1001;
																										if (NULLP
																											(BgL_typezd2argszd2_936))
																											{	/* Foreign/cfun.scm 75 */
																												BgL_targszd2idzd2_1001 =
																													BNIL;
																											}
																										else
																											{	/* Foreign/cfun.scm 75 */
																												obj_t
																													BgL_head1090z00_1002;
																												{	/* Foreign/cfun.scm 75 */
																													obj_t
																														BgL_arg1307z00_1003;
																													BgL_arg1307z00_1003 =
																														(((BgL_typez00_bglt)
																															COBJECT((
																																	(BgL_typez00_bglt)
																																	CAR(((obj_t)
																																			BgL_typezd2argszd2_936)))))->
																														BgL_idz00);
																													BgL_head1090z00_1002 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1307z00_1003,
																														BNIL);
																												}
																												{	/* Foreign/cfun.scm 75 */
																													obj_t
																														BgL_g1093z00_1004;
																													BgL_g1093z00_1004 =
																														CDR(((obj_t)
																															BgL_typezd2argszd2_936));
																													{
																														obj_t
																															BgL_l1088z00_1006;
																														obj_t
																															BgL_tail1091z00_1007;
																														BgL_l1088z00_1006 =
																															BgL_g1093z00_1004;
																														BgL_tail1091z00_1007
																															=
																															BgL_head1090z00_1002;
																													BgL_zc3z04anonymousza31286ze3z87_1005:
																														if (NULLP
																															(BgL_l1088z00_1006))
																															{	/* Foreign/cfun.scm 75 */
																																BgL_targszd2idzd2_1001
																																	=
																																	BgL_head1090z00_1002;
																															}
																														else
																															{	/* Foreign/cfun.scm 75 */
																																obj_t
																																	BgL_newtail1092z00_1008;
																																{	/* Foreign/cfun.scm 75 */
																																	obj_t
																																		BgL_arg1305z00_1009;
																																	BgL_arg1305z00_1009
																																		=
																																		(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) CAR(((obj_t) BgL_l1088z00_1006)))))->BgL_idz00);
																																	BgL_newtail1092z00_1008
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1305z00_1009,
																																		BNIL);
																																}
																																SET_CDR
																																	(BgL_tail1091z00_1007,
																																	BgL_newtail1092z00_1008);
																																{	/* Foreign/cfun.scm 75 */
																																	obj_t
																																		BgL_arg1304z00_1010;
																																	BgL_arg1304z00_1010
																																		=
																																		CDR(((obj_t)
																																			BgL_l1088z00_1006));
																																	{
																																		obj_t
																																			BgL_tail1091z00_1244;
																																		obj_t
																																			BgL_l1088z00_1243;
																																		BgL_l1088z00_1243
																																			=
																																			BgL_arg1304z00_1010;
																																		BgL_tail1091z00_1244
																																			=
																																			BgL_newtail1092z00_1008;
																																		BgL_tail1091z00_1007
																																			=
																																			BgL_tail1091z00_1244;
																																		BgL_l1088z00_1006
																																			=
																																			BgL_l1088z00_1243;
																																		goto
																																			BgL_zc3z04anonymousza31286ze3z87_1005;
																																	}
																																}
																															}
																													}
																												}
																											}
																										{	/* Foreign/cfun.scm 75 */
																											obj_t
																												BgL_callerzd2namezd2_1011;
																											BgL_callerzd2namezd2_1011
																												=
																												string_append
																												(BGl_string1571z00zzforeign_cfunctionz00,
																												BgL_nbzd2argszd2_938);
																											{	/* Foreign/cfun.scm 76 */
																												obj_t
																													BgL_czd2callzd2idz00_1012;
																												{	/* Foreign/cfun.scm 77 */
																													obj_t
																														BgL_arg1268z00_1013;
																													{	/* Foreign/cfun.scm 77 */
																														obj_t
																															BgL_arg1272z00_1014;
																														obj_t
																															BgL_arg1284z00_1015;
																														{	/* Foreign/cfun.scm 77 */
																															obj_t
																																BgL_symbolz00_1016;
																															BgL_symbolz00_1016
																																=
																																CNST_TABLE_REF
																																(4);
																															{	/* Foreign/cfun.scm 77 */
																																obj_t
																																	BgL_arg1455z00_1017;
																																BgL_arg1455z00_1017
																																	=
																																	SYMBOL_TO_STRING
																																	(BgL_symbolz00_1016);
																																BgL_arg1272z00_1014
																																	=
																																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																	(BgL_arg1455z00_1017);
																															}
																														}
																														{	/* Foreign/cfun.scm 77 */
																															obj_t
																																BgL_arg1455z00_1018;
																															BgL_arg1455z00_1018
																																=
																																SYMBOL_TO_STRING
																																(BgL_callzd2idzd2_904);
																															BgL_arg1284z00_1015
																																=
																																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																(BgL_arg1455z00_1018);
																														}
																														BgL_arg1268z00_1013
																															=
																															string_append
																															(BgL_arg1272z00_1014,
																															BgL_arg1284z00_1015);
																													}
																													BgL_czd2callzd2idz00_1012
																														=
																														bstring_to_symbol
																														(BgL_arg1268z00_1013);
																												}
																												{	/* Foreign/cfun.scm 77 */

																													{	/* Foreign/cfun.scm 78 */
																														obj_t
																															BgL_arg1242z00_1019;
																														{	/* Foreign/cfun.scm 78 */
																															obj_t
																																BgL_arg1244z00_1020;
																															{	/* Foreign/cfun.scm 78 */
																																obj_t
																																	BgL_arg1248z00_1021;
																																{	/* Foreign/cfun.scm 78 */
																																	obj_t
																																		BgL_arg1249z00_1022;
																																	obj_t
																																		BgL_arg1252z00_1023;
																																	BgL_arg1249z00_1022
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_idz00_902,
																																		BgL_targszd2idzd2_1001);
																																	BgL_arg1252z00_1023
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_callerzd2namezd2_1011,
																																		BNIL);
																																	BgL_arg1248z00_1021
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1249z00_1022,
																																		BgL_arg1252z00_1023);
																																}
																																BgL_arg1244z00_1020
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_czd2callzd2idz00_1012,
																																	BgL_arg1248z00_1021);
																															}
																															BgL_arg1242z00_1019
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_treszd2idzd2_1000,
																																BgL_arg1244z00_1020);
																														}
																														BgL_tmpz00_1222 =
																															MAKE_YOUNG_PAIR
																															(CNST_TABLE_REF
																															(6),
																															BgL_arg1242z00_1019);
																													}
																												}
																											}
																										}
																									}
																								}
																								BgL_arg1142z00_1071 =
																									MAKE_YOUNG_PAIR
																									(BgL_tmpz00_1222, BNIL);
																							}
																							BgL_arg1140z00_1069 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1141z00_1070,
																								BgL_arg1142z00_1071);
																						}
																						BgL_arg1137z00_1067 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1138z00_1068,
																							BgL_arg1140z00_1069);
																					}
																					BgL_arg1132z00_1066 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
																						BgL_arg1137z00_1067);
																				}
																				BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																					(BgL_arg1132z00_1066);
																			}
																			{	/* Foreign/cfun.scm 122 */
																				obj_t BgL_arg1145z00_1072;

																				{	/* Foreign/cfun.scm 122 */
																					obj_t BgL_arg1148z00_1073;

																					{	/* Foreign/cfun.scm 122 */
																						obj_t BgL_arg1149z00_1074;

																						{	/* Foreign/cfun.scm 122 */
																							obj_t BgL_arg1152z00_1075;

																							{	/* Foreign/cfun.scm 122 */
																								obj_t BgL_arg1153z00_1076;

																								BgL_arg1153z00_1076 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(21), BNIL);
																								BgL_arg1152z00_1075 =
																									MAKE_YOUNG_PAIR
																									(BgL_bidzf3zd2boolz21_926,
																									BgL_arg1153z00_1076);
																							}
																							BgL_arg1149z00_1074 =
																								BGl_makezd2protozd2inlinez00zzforeign_libraryz00
																								(BgL_arg1152z00_1075);
																						}
																						BgL_arg1148z00_1073 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1149z00_1074, BNIL);
																					}
																					BgL_arg1145z00_1072 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(22),
																						BgL_arg1148z00_1073);
																				}
																				BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																					(BgL_arg1145z00_1072);
																			}
																			{	/* Foreign/cfun.scm 123 */
																				obj_t BgL_arg1154z00_1077;

																				{	/* Foreign/cfun.scm 123 */
																					obj_t BgL_arg1157z00_1078;

																					{	/* Foreign/cfun.scm 123 */
																						obj_t BgL_arg1158z00_1079;

																						{	/* Foreign/cfun.scm 123 */
																							obj_t BgL_arg1162z00_1080;

																							{	/* Foreign/cfun.scm 123 */
																								obj_t BgL_arg1164z00_1081;

																								{	/* Foreign/cfun.scm 123 */
																									obj_t BgL_arg1166z00_1082;

																									BgL_arg1166z00_1082 =
																										MAKE_YOUNG_PAIR
																										(BgL_bidz00_903, BNIL);
																									BgL_arg1164z00_1081 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(23),
																										BgL_arg1166z00_1082);
																								}
																								BgL_arg1162z00_1080 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1164z00_1081, BNIL);
																							}
																							BgL_arg1158z00_1079 =
																								MAKE_YOUNG_PAIR
																								(BgL_bidzf3zf3_919,
																								BgL_arg1162z00_1080);
																						}
																						BgL_arg1157z00_1078 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1158z00_1079, BNIL);
																					}
																					BgL_arg1154z00_1077 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(24),
																						BgL_arg1157z00_1078);
																				}
																				BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																					(BgL_arg1154z00_1077);
																			}
																			{	/* Foreign/cfun.scm 126 */
																				obj_t BgL_arg1171z00_1083;
																				obj_t BgL_arg1172z00_1084;
																				obj_t BgL_arg1182z00_1085;

																				{	/* Foreign/cfun.scm 60 */
																					obj_t BgL_arg1206z00_1032;
																					obj_t BgL_arg1208z00_1033;

																					{	/* Foreign/cfun.scm 60 */
																						obj_t BgL_arg1209z00_1034;

																						BgL_arg1209z00_1034 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(12), BNIL);
																						BgL_arg1206z00_1032 =
																							MAKE_YOUNG_PAIR
																							(BgL_bidzf3zd2boolz21_926,
																							BgL_arg1209z00_1034);
																					}
																					{	/* Foreign/cfun.scm 61 */
																						obj_t BgL_arg1210z00_1035;

																						{	/* Foreign/cfun.scm 61 */
																							obj_t BgL_arg1212z00_1036;
																							obj_t BgL_arg1215z00_1037;

																							{	/* Foreign/cfun.scm 61 */
																								obj_t BgL_arg1216z00_1038;

																								BgL_arg1216z00_1038 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(13), BNIL);
																								BgL_arg1212z00_1036 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(14), BgL_arg1216z00_1038);
																							}
																							{	/* Foreign/cfun.scm 62 */
																								obj_t BgL_arg1218z00_1039;
																								obj_t BgL_arg1219z00_1040;

																								{	/* Foreign/cfun.scm 62 */
																									obj_t BgL_arg1220z00_1041;

																									{	/* Foreign/cfun.scm 62 */
																										obj_t BgL_arg1221z00_1042;
																										obj_t BgL_arg1223z00_1043;

																										{	/* Foreign/cfun.scm 62 */
																											obj_t BgL_arg1225z00_1044;

																											BgL_arg1225z00_1044 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(13),
																												BNIL);
																											BgL_arg1221z00_1042 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(15),
																												BgL_arg1225z00_1044);
																										}
																										{	/* Foreign/cfun.scm 62 */
																											obj_t BgL_arg1226z00_1045;

																											{	/* Foreign/cfun.scm 62 */
																												obj_t
																													BgL_arg1227z00_1046;
																												BgL_arg1227z00_1046 =
																													MAKE_YOUNG_PAIR
																													(BgL_bidz00_903,
																													BNIL);
																												BgL_arg1226z00_1045 =
																													MAKE_YOUNG_PAIR
																													(CNST_TABLE_REF(16),
																													BgL_arg1227z00_1046);
																											}
																											BgL_arg1223z00_1043 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1226z00_1045,
																												BNIL);
																										}
																										BgL_arg1220z00_1041 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1221z00_1042,
																											BgL_arg1223z00_1043);
																									}
																									BgL_arg1218z00_1039 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(17),
																										BgL_arg1220z00_1041);
																								}
																								BgL_arg1219z00_1040 =
																									MAKE_YOUNG_PAIR(BFALSE, BNIL);
																								BgL_arg1215z00_1037 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1218z00_1039,
																									BgL_arg1219z00_1040);
																							}
																							BgL_arg1210z00_1035 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1212z00_1036,
																								BgL_arg1215z00_1037);
																						}
																						BgL_arg1208z00_1033 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(18), BgL_arg1210z00_1035);
																					}
																					BgL_arg1171z00_1083 =
																						BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																						(BgL_arg1206z00_1032,
																						BgL_arg1208z00_1033);
																				}
																				{	/* Foreign/cfun.scm 68 */
																					obj_t BgL_arg1229z00_1024;

																					{	/* Foreign/cfun.scm 68 */
																						obj_t BgL_arg1230z00_1025;
																						obj_t BgL_arg1231z00_1026;

																						{	/* Foreign/cfun.scm 68 */
																							obj_t BgL_list1232z00_1027;

																							{	/* Foreign/cfun.scm 68 */
																								obj_t BgL_arg1233z00_1028;

																								{	/* Foreign/cfun.scm 68 */
																									obj_t BgL_arg1234z00_1029;

																									BgL_arg1234z00_1029 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(7), BNIL);
																									BgL_arg1233z00_1028 =
																										MAKE_YOUNG_PAIR
																										(BgL_idz00_902,
																										BgL_arg1234z00_1029);
																								}
																								BgL_list1232z00_1027 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(8), BgL_arg1233z00_1028);
																							}
																							BgL_arg1230z00_1025 =
																								BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																								(BgL_list1232z00_1027);
																						}
																						{	/* Foreign/cfun.scm 69 */
																							obj_t BgL_arg1236z00_1030;
																							obj_t BgL_arg1238z00_1031;

																							BgL_arg1236z00_1030 =
																								BGl_makezd2typedzd2identz00zzast_identz00
																								(CNST_TABLE_REF(9),
																								BgL_idz00_902);
																							BgL_arg1238z00_1031 =
																								MAKE_YOUNG_PAIR
																								(BGl_makezd2typedzd2identz00zzast_identz00
																								(CNST_TABLE_REF(10),
																									BgL_idz00_902), BNIL);
																							BgL_arg1231z00_1026 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1236z00_1030,
																								BgL_arg1238z00_1031);
																						}
																						BgL_arg1229z00_1024 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1230z00_1025,
																							BgL_arg1231z00_1026);
																					}
																					BgL_arg1172z00_1084 =
																						BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																						(BgL_arg1229z00_1024,
																						CNST_TABLE_REF(11));
																				}
																				if (
																					(BgL_arityz00_937 >=
																						(long)
																						CINT
																						(BGl_za2maxzd2czd2foreignzd2arityza2zd2zzengine_paramz00)))
																					{	/* Foreign/cfun.scm 87 */
																						obj_t BgL_arg1312z00_996;
																						obj_t BgL_arg1314z00_997;

																						{	/* Foreign/cfun.scm 87 */
																							obj_t BgL_arg1316z00_998;

																							{	/* Foreign/cfun.scm 87 */
																								obj_t BgL_g1770z00_999;

																								BgL_g1770z00_999 =
																									BGl_za2maxzd2czd2foreignzd2arityza2zd2zzengine_paramz00;
																								{	/* Foreign/cfun.scm 87 */

																									BgL_arg1316z00_998 =
																										BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
																										((long)
																										CINT(BgL_g1770z00_999),
																										10L);
																							}}
																							BgL_arg1312z00_996 =
																								string_append_3
																								(BGl_string1566z00zzforeign_cfunctionz00,
																								BgL_arg1316z00_998,
																								BGl_string1567z00zzforeign_cfunctionz00);
																						}
																						BgL_arg1314z00_997 =
																							string_append
																							(BgL_nbzd2argszd2_938,
																							BGl_string1568z00zzforeign_cfunctionz00);
																						BgL_arg1182z00_1085 =
																							BGl_userzd2errorzd2zztools_errorz00
																							(BgL_idz00_902,
																							BgL_arg1312z00_996,
																							BgL_arg1314z00_997, BNIL);
																					}
																				else
																					{	/* Foreign/cfun.scm 83 */
																						if ((BgL_arityz00_937 >= 0L))
																							{	/* Foreign/cfun.scm 90 */
																								{	/* Foreign/cfun.scm 98 */
																									obj_t BgL_treszd2idzd2_946;

																									BgL_treszd2idzd2_946 =
																										(((BgL_typez00_bglt)
																											COBJECT
																											(BgL_typezd2reszd2_935))->
																										BgL_idz00);
																									{	/* Foreign/cfun.scm 98 */
																										obj_t BgL_targszd2idzd2_947;

																										if (NULLP
																											(BgL_typezd2argszd2_936))
																											{	/* Foreign/cfun.scm 99 */
																												BgL_targszd2idzd2_947 =
																													BNIL;
																											}
																										else
																											{	/* Foreign/cfun.scm 99 */
																												obj_t
																													BgL_head1096z00_948;
																												{	/* Foreign/cfun.scm 99 */
																													obj_t
																														BgL_arg1375z00_949;
																													BgL_arg1375z00_949 =
																														(((BgL_typez00_bglt)
																															COBJECT((
																																	(BgL_typez00_bglt)
																																	CAR(((obj_t)
																																			BgL_typezd2argszd2_936)))))->
																														BgL_idz00);
																													BgL_head1096z00_948 =
																														MAKE_YOUNG_PAIR
																														(BgL_arg1375z00_949,
																														BNIL);
																												}
																												{	/* Foreign/cfun.scm 99 */
																													obj_t
																														BgL_g1099z00_950;
																													BgL_g1099z00_950 =
																														CDR(((obj_t)
																															BgL_typezd2argszd2_936));
																													{
																														obj_t
																															BgL_l1094z00_952;
																														obj_t
																															BgL_tail1097z00_953;
																														BgL_l1094z00_952 =
																															BgL_g1099z00_950;
																														BgL_tail1097z00_953
																															=
																															BgL_head1096z00_948;
																													BgL_zc3z04anonymousza31363ze3z87_951:
																														if (NULLP
																															(BgL_l1094z00_952))
																															{	/* Foreign/cfun.scm 99 */
																																BgL_targszd2idzd2_947
																																	=
																																	BgL_head1096z00_948;
																															}
																														else
																															{	/* Foreign/cfun.scm 99 */
																																obj_t
																																	BgL_newtail1098z00_954;
																																{	/* Foreign/cfun.scm 99 */
																																	obj_t
																																		BgL_arg1370z00_955;
																																	BgL_arg1370z00_955
																																		=
																																		(((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) CAR(((obj_t) BgL_l1094z00_952)))))->BgL_idz00);
																																	BgL_newtail1098z00_954
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_arg1370z00_955,
																																		BNIL);
																																}
																																SET_CDR
																																	(BgL_tail1097z00_953,
																																	BgL_newtail1098z00_954);
																																{	/* Foreign/cfun.scm 99 */
																																	obj_t
																																		BgL_arg1367z00_956;
																																	BgL_arg1367z00_956
																																		=
																																		CDR(((obj_t)
																																			BgL_l1094z00_952));
																																	{
																																		obj_t
																																			BgL_tail1097z00_1353;
																																		obj_t
																																			BgL_l1094z00_1352;
																																		BgL_l1094z00_1352
																																			=
																																			BgL_arg1367z00_956;
																																		BgL_tail1097z00_1353
																																			=
																																			BgL_newtail1098z00_954;
																																		BgL_tail1097z00_953
																																			=
																																			BgL_tail1097z00_1353;
																																		BgL_l1094z00_952
																																			=
																																			BgL_l1094z00_1352;
																																		goto
																																			BgL_zc3z04anonymousza31363ze3z87_951;
																																	}
																																}
																															}
																													}
																												}
																											}
																										{	/* Foreign/cfun.scm 99 */
																											obj_t BgL_argsz00_957;

																											if (NULLP
																												(BgL_targszd2idzd2_947))
																												{	/* Foreign/cfun.scm 100 */
																													BgL_argsz00_957 =
																														BNIL;
																												}
																											else
																												{	/* Foreign/cfun.scm 100 */
																													obj_t
																														BgL_head1102z00_958;
																													{	/* Foreign/cfun.scm 100 */
																														obj_t
																															BgL_arg1352z00_959;
																														{	/* Foreign/cfun.scm 100 */
																															obj_t
																																BgL_arg1361z00_960;
																															BgL_arg1361z00_960
																																=
																																CAR(((obj_t)
																																	BgL_targszd2idzd2_947));
																															BgL_arg1352z00_959
																																=
																																BGl_gensymz00zz__r4_symbols_6_4z00
																																(BgL_arg1361z00_960);
																														}
																														BgL_head1102z00_958
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1352z00_959,
																															BNIL);
																													}
																													{	/* Foreign/cfun.scm 100 */
																														obj_t
																															BgL_g1105z00_961;
																														BgL_g1105z00_961 =
																															CDR(((obj_t)
																																BgL_targszd2idzd2_947));
																														{
																															obj_t
																																BgL_l1100z00_963;
																															obj_t
																																BgL_tail1103z00_964;
																															BgL_l1100z00_963 =
																																BgL_g1105z00_961;
																															BgL_tail1103z00_964
																																=
																																BgL_head1102z00_958;
																														BgL_zc3z04anonymousza31345ze3z87_962:
																															if (NULLP
																																(BgL_l1100z00_963))
																																{	/* Foreign/cfun.scm 100 */
																																	BgL_argsz00_957
																																		=
																																		BgL_head1102z00_958;
																																}
																															else
																																{	/* Foreign/cfun.scm 100 */
																																	obj_t
																																		BgL_newtail1104z00_965;
																																	{	/* Foreign/cfun.scm 100 */
																																		obj_t
																																			BgL_arg1349z00_966;
																																		{	/* Foreign/cfun.scm 100 */
																																			obj_t
																																				BgL_arg1351z00_967;
																																			BgL_arg1351z00_967
																																				=
																																				CAR((
																																					(obj_t)
																																					BgL_l1100z00_963));
																																			BgL_arg1349z00_966
																																				=
																																				BGl_gensymz00zz__r4_symbols_6_4z00
																																				(BgL_arg1351z00_967);
																																		}
																																		BgL_newtail1104z00_965
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1349z00_966,
																																			BNIL);
																																	}
																																	SET_CDR
																																		(BgL_tail1103z00_964,
																																		BgL_newtail1104z00_965);
																																	{	/* Foreign/cfun.scm 100 */
																																		obj_t
																																			BgL_arg1348z00_968;
																																		BgL_arg1348z00_968
																																			=
																																			CDR((
																																				(obj_t)
																																				BgL_l1100z00_963));
																																		{
																																			obj_t
																																				BgL_tail1103z00_1372;
																																			obj_t
																																				BgL_l1100z00_1371;
																																			BgL_l1100z00_1371
																																				=
																																				BgL_arg1348z00_968;
																																			BgL_tail1103z00_1372
																																				=
																																				BgL_newtail1104z00_965;
																																			BgL_tail1103z00_964
																																				=
																																				BgL_tail1103z00_1372;
																																			BgL_l1100z00_963
																																				=
																																				BgL_l1100z00_1371;
																																			goto
																																				BgL_zc3z04anonymousza31345ze3z87_962;
																																		}
																																	}
																																}
																														}
																													}
																												}
																											{	/* Foreign/cfun.scm 100 */
																												obj_t
																													BgL_czd2callzd2idz00_969;
																												{	/* Foreign/cfun.scm 101 */
																													obj_t
																														BgL_arg1340z00_970;
																													{	/* Foreign/cfun.scm 101 */
																														obj_t
																															BgL_arg1342z00_971;
																														obj_t
																															BgL_arg1343z00_972;
																														{	/* Foreign/cfun.scm 101 */
																															obj_t
																																BgL_symbolz00_973;
																															BgL_symbolz00_973
																																=
																																CNST_TABLE_REF
																																(4);
																															{	/* Foreign/cfun.scm 101 */
																																obj_t
																																	BgL_arg1455z00_974;
																																BgL_arg1455z00_974
																																	=
																																	SYMBOL_TO_STRING
																																	(BgL_symbolz00_973);
																																BgL_arg1342z00_971
																																	=
																																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																	(BgL_arg1455z00_974);
																															}
																														}
																														{	/* Foreign/cfun.scm 101 */
																															obj_t
																																BgL_arg1455z00_975;
																															BgL_arg1455z00_975
																																=
																																SYMBOL_TO_STRING
																																(BgL_callzd2idzd2_904);
																															BgL_arg1343z00_972
																																=
																																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																																(BgL_arg1455z00_975);
																														}
																														BgL_arg1340z00_970 =
																															string_append
																															(BgL_arg1342z00_971,
																															BgL_arg1343z00_972);
																													}
																													BgL_czd2callzd2idz00_969
																														=
																														bstring_to_symbol
																														(BgL_arg1340z00_970);
																												}
																												{	/* Foreign/cfun.scm 101 */

																													{	/* Foreign/cfun.scm 103 */
																														obj_t
																															BgL_arg1320z00_976;
																														obj_t
																															BgL_arg1321z00_977;
																														{	/* Foreign/cfun.scm 103 */
																															obj_t
																																BgL_arg1322z00_978;
																															obj_t
																																BgL_arg1323z00_979;
																															BgL_arg1322z00_978
																																=
																																BGl_makezd2typedzd2identz00zzast_identz00
																																(BgL_callzd2idzd2_904,
																																BgL_treszd2idzd2_946);
																															{	/* Foreign/cfun.scm 104 */
																																obj_t
																																	BgL_arg1325z00_980;
																																obj_t
																																	BgL_arg1326z00_981;
																																BgL_arg1325z00_980
																																	=
																																	BGl_makezd2typedzd2identz00zzast_identz00
																																	(CNST_TABLE_REF
																																	(5),
																																	BgL_idz00_902);
																																{	/* Foreign/cfun.scm 105 */
																																	obj_t
																																		BgL_arg1327z00_982;
																																	if (NULLP
																																		(BgL_argsz00_957))
																																		{	/* Foreign/cfun.scm 105 */
																																			BgL_arg1327z00_982
																																				= BNIL;
																																		}
																																	else
																																		{	/* Foreign/cfun.scm 105 */
																																			obj_t
																																				BgL_head1108z00_983;
																																			BgL_head1108z00_983
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BNIL,
																																				BNIL);
																																			{
																																				obj_t
																																					BgL_ll1106z00_985;
																																				obj_t
																																					BgL_ll1107z00_986;
																																				obj_t
																																					BgL_tail1109z00_987;
																																				BgL_ll1106z00_985
																																					=
																																					BgL_argsz00_957;
																																				BgL_ll1107z00_986
																																					=
																																					BgL_targszd2idzd2_947;
																																				BgL_tail1109z00_987
																																					=
																																					BgL_head1108z00_983;
																																			BgL_zc3z04anonymousza31329ze3z87_984:
																																				if (NULLP(BgL_ll1106z00_985))
																																					{	/* Foreign/cfun.scm 105 */
																																						BgL_arg1327z00_982
																																							=
																																							CDR
																																							(BgL_head1108z00_983);
																																					}
																																				else
																																					{	/* Foreign/cfun.scm 105 */
																																						obj_t
																																							BgL_newtail1110z00_988;
																																						{	/* Foreign/cfun.scm 105 */
																																							obj_t
																																								BgL_arg1333z00_989;
																																							{	/* Foreign/cfun.scm 105 */
																																								obj_t
																																									BgL_argz00_990;
																																								obj_t
																																									BgL_typez00_991;
																																								BgL_argz00_990
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_ll1106z00_985));
																																								BgL_typez00_991
																																									=
																																									CAR
																																									(
																																									((obj_t) BgL_ll1107z00_986));
																																								BgL_arg1333z00_989
																																									=
																																									BGl_makezd2typedzd2identz00zzast_identz00
																																									(BgL_argz00_990,
																																									BgL_typez00_991);
																																							}
																																							BgL_newtail1110z00_988
																																								=
																																								MAKE_YOUNG_PAIR
																																								(BgL_arg1333z00_989,
																																								BNIL);
																																						}
																																						SET_CDR
																																							(BgL_tail1109z00_987,
																																							BgL_newtail1110z00_988);
																																						{	/* Foreign/cfun.scm 105 */
																																							obj_t
																																								BgL_arg1331z00_992;
																																							obj_t
																																								BgL_arg1332z00_993;
																																							BgL_arg1331z00_992
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_ll1106z00_985));
																																							BgL_arg1332z00_993
																																								=
																																								CDR
																																								(
																																								((obj_t) BgL_ll1107z00_986));
																																							{
																																								obj_t
																																									BgL_tail1109z00_1402;
																																								obj_t
																																									BgL_ll1107z00_1401;
																																								obj_t
																																									BgL_ll1106z00_1400;
																																								BgL_ll1106z00_1400
																																									=
																																									BgL_arg1331z00_992;
																																								BgL_ll1107z00_1401
																																									=
																																									BgL_arg1332z00_993;
																																								BgL_tail1109z00_1402
																																									=
																																									BgL_newtail1110z00_988;
																																								BgL_tail1109z00_987
																																									=
																																									BgL_tail1109z00_1402;
																																								BgL_ll1107z00_986
																																									=
																																									BgL_ll1107z00_1401;
																																								BgL_ll1106z00_985
																																									=
																																									BgL_ll1106z00_1400;
																																								goto
																																									BgL_zc3z04anonymousza31329ze3z87_984;
																																							}
																																						}
																																					}
																																			}
																																		}
																																	BgL_arg1326z00_981
																																		=
																																		BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																		(BgL_arg1327z00_982,
																																		BNIL);
																																}
																																BgL_arg1323z00_979
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_arg1325z00_980,
																																	BgL_arg1326z00_981);
																															}
																															BgL_arg1320z00_976
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1322z00_978,
																																BgL_arg1323z00_979);
																														}
																														{	/* Foreign/cfun.scm 108 */
																															obj_t
																																BgL_arg1335z00_994;
																															{	/* Foreign/cfun.scm 108 */
																																obj_t
																																	BgL_arg1339z00_995;
																																BgL_arg1339z00_995
																																	=
																																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																																	(BgL_argsz00_957,
																																	BNIL);
																																BgL_arg1335z00_994
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(5),
																																	BgL_arg1339z00_995);
																															}
																															BgL_arg1321z00_977
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_czd2callzd2idz00_969,
																																BgL_arg1335z00_994);
																														}
																														BgL_arg1182z00_1085
																															=
																															BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																															(BgL_arg1320z00_976,
																															BgL_arg1321z00_977);
																													}
																												}
																											}
																										}
																									}
																								}
																							}
																						else
																							{	/* Foreign/cfun.scm 90 */
																								BgL_arg1182z00_1085 =
																									BGl_userzd2errorzd2zztools_errorz00
																									(BGl_string1569z00zzforeign_cfunctionz00,
																									BGl_string1570z00zzforeign_cfunctionz00,
																									BgL_idz00_902, BNIL);
																							}
																					}
																				{	/* Foreign/cfun.scm 126 */
																					obj_t BgL_list1183z00_1086;

																					{	/* Foreign/cfun.scm 126 */
																						obj_t BgL_arg1187z00_1087;

																						{	/* Foreign/cfun.scm 126 */
																							obj_t BgL_arg1188z00_1088;

																							BgL_arg1188z00_1088 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1182z00_1085, BNIL);
																							BgL_arg1187z00_1087 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1172z00_1084,
																								BgL_arg1188z00_1088);
																						}
																						BgL_list1183z00_1086 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1171z00_1083,
																							BgL_arg1187z00_1087);
																					}
																					return BgL_list1183z00_1086;
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_cfunctionz00(void)
	{
		{	/* Foreign/cfun.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			BGl_modulezd2initializa7ationz75zzforeign_ctypez00(396356879L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			BGl_modulezd2initializa7ationz75zzforeign_libraryz00(419430976L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1575z00zzforeign_cfunctionz00));
		}

	}

#ifdef __cplusplus
}
#endif
