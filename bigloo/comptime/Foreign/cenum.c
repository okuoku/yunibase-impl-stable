/*===========================================================================*/
/*   (Foreign/cenum.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Foreign/cenum.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_FOREIGN_CENUM_TYPE_DEFINITIONS
#define BGL_FOREIGN_CENUM_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_cenumz00_bgl
	{
		struct BgL_typez00_bgl *BgL_btypez00;
		obj_t BgL_literalsz00;
	}               *BgL_cenumz00_bglt;


#endif													// BGL_FOREIGN_CENUM_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzforeign_cenumz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzforeign_cenumz00(void);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_genericzd2initzd2zzforeign_cenumz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzforeign_cenumz00(void);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzforeign_cenumz00(void);
	extern obj_t
		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00(obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzforeign_cenumz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_libraryz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_accessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzforeign_ctypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_makezd2protozd2inlinez00zzforeign_libraryz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzforeign_cenumz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzforeign_cenumz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzforeign_cenumz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzforeign_cenumz00(void);
	static obj_t BGl_z62makezd2ctypezd2accessesz121090z70zzforeign_cenumz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_makezd2definezd2inlinez00zzforeign_libraryz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_cenumz00zzforeign_ctypez00;
	static obj_t __cnst[23];


	   
		 
		DEFINE_STRING(BGl_string1327z00zzforeign_cenumz00,
		BgL_bgl_string1327za700za7za7f1334za7, "make-ctype-accesses!", 20);
	      DEFINE_STRING(BGl_string1328z00zzforeign_cenumz00,
		BgL_bgl_string1328za700za7za7f1335za7, "(", 1);
	      DEFINE_STRING(BGl_string1329z00zzforeign_cenumz00,
		BgL_bgl_string1329za700za7za7f1336za7, ")FOREIGN_TO_COBJ", 16);
	      DEFINE_STRING(BGl_string1330z00zzforeign_cenumz00,
		BgL_bgl_string1330za700za7za7f1337za7, "CENUM_TO_FOREIGN", 16);
	      DEFINE_STRING(BGl_string1331z00zzforeign_cenumz00,
		BgL_bgl_string1331za700za7za7f1338za7, "foreign_cenum", 13);
	      DEFINE_STRING(BGl_string1332z00zzforeign_cenumz00,
		BgL_bgl_string1332za700za7za7f1339za7,
		"predicate-of static ::obj foreign symbol macro if eq? quote foreign-id foreign? o o::obj (pragma::bool \"($1 == $2)\" o1 o2) o2 o1 = ?::bool pragma - ::bool ? -> ",
		160);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1326z00zzforeign_cenumz00,
		BgL_bgl_za762makeza7d2ctypeza71340za7,
		BGl_z62makezd2ctypezd2accessesz121090z70zzforeign_cenumz00, 0L, BUNSPEC, 3);
	extern obj_t BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzforeign_cenumz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzforeign_cenumz00(long
		BgL_checksumz00_868, char *BgL_fromz00_869)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzforeign_cenumz00))
				{
					BGl_requirezd2initializa7ationz75zzforeign_cenumz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzforeign_cenumz00();
					BGl_libraryzd2moduleszd2initz00zzforeign_cenumz00();
					BGl_cnstzd2initzd2zzforeign_cenumz00();
					BGl_importedzd2moduleszd2initz00zzforeign_cenumz00();
					BGl_methodzd2initzd2zzforeign_cenumz00();
					return BGl_toplevelzd2initzd2zzforeign_cenumz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzforeign_cenumz00(void)
	{
		{	/* Foreign/cenum.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"foreign_cenum");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"foreign_cenum");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "foreign_cenum");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"foreign_cenum");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "foreign_cenum");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"foreign_cenum");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzforeign_cenumz00(void)
	{
		{	/* Foreign/cenum.scm 15 */
			{	/* Foreign/cenum.scm 15 */
				obj_t BgL_cportz00_744;

				{	/* Foreign/cenum.scm 15 */
					obj_t BgL_stringz00_751;

					BgL_stringz00_751 = BGl_string1332z00zzforeign_cenumz00;
					{	/* Foreign/cenum.scm 15 */
						obj_t BgL_startz00_752;

						BgL_startz00_752 = BINT(0L);
						{	/* Foreign/cenum.scm 15 */
							obj_t BgL_endz00_753;

							BgL_endz00_753 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_751)));
							{	/* Foreign/cenum.scm 15 */

								BgL_cportz00_744 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_751, BgL_startz00_752, BgL_endz00_753);
				}}}}
				{
					long BgL_iz00_745;

					BgL_iz00_745 = 22L;
				BgL_loopz00_746:
					if ((BgL_iz00_745 == -1L))
						{	/* Foreign/cenum.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Foreign/cenum.scm 15 */
							{	/* Foreign/cenum.scm 15 */
								obj_t BgL_arg1333z00_747;

								{	/* Foreign/cenum.scm 15 */

									{	/* Foreign/cenum.scm 15 */
										obj_t BgL_locationz00_749;

										BgL_locationz00_749 = BBOOL(((bool_t) 0));
										{	/* Foreign/cenum.scm 15 */

											BgL_arg1333z00_747 =
												BGl_readz00zz__readerz00(BgL_cportz00_744,
												BgL_locationz00_749);
										}
									}
								}
								{	/* Foreign/cenum.scm 15 */
									int BgL_tmpz00_894;

									BgL_tmpz00_894 = (int) (BgL_iz00_745);
									CNST_TABLE_SET(BgL_tmpz00_894, BgL_arg1333z00_747);
							}}
							{	/* Foreign/cenum.scm 15 */
								int BgL_auxz00_750;

								BgL_auxz00_750 = (int) ((BgL_iz00_745 - 1L));
								{
									long BgL_iz00_899;

									BgL_iz00_899 = (long) (BgL_auxz00_750);
									BgL_iz00_745 = BgL_iz00_899;
									goto BgL_loopz00_746;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzforeign_cenumz00(void)
	{
		{	/* Foreign/cenum.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzforeign_cenumz00(void)
	{
		{	/* Foreign/cenum.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzforeign_cenumz00(void)
	{
		{	/* Foreign/cenum.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzforeign_cenumz00(void)
	{
		{	/* Foreign/cenum.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzforeign_cenumz00(void)
	{
		{	/* Foreign/cenum.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_makezd2ctypezd2accessesz12zd2envzc0zzforeign_accessz00,
				BGl_cenumz00zzforeign_ctypez00, BGl_proc1326z00zzforeign_cenumz00,
				BGl_string1327z00zzforeign_cenumz00);
		}

	}



/* &make-ctype-accesses!1090 */
	obj_t BGl_z62makezd2ctypezd2accessesz121090z70zzforeign_cenumz00(obj_t
		BgL_envz00_736, obj_t BgL_whatz00_737, obj_t BgL_whoz00_738,
		obj_t BgL_locz00_739)
	{
		{	/* Foreign/cenum.scm 28 */
			{	/* Foreign/cenum.scm 29 */
				BgL_typez00_bglt BgL_btypez00_757;

				{
					BgL_cenumz00_bglt BgL_auxz00_903;

					{
						obj_t BgL_auxz00_904;

						{	/* Foreign/cenum.scm 29 */
							BgL_objectz00_bglt BgL_tmpz00_905;

							BgL_tmpz00_905 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_whatz00_737));
							BgL_auxz00_904 = BGL_OBJECT_WIDENING(BgL_tmpz00_905);
						}
						BgL_auxz00_903 = ((BgL_cenumz00_bglt) BgL_auxz00_904);
					}
					BgL_btypez00_757 =
						(((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_903))->BgL_btypez00);
				}
				{	/* Foreign/cenum.scm 29 */
					obj_t BgL_idz00_758;

					BgL_idz00_758 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_whoz00_738)))->BgL_idz00);
					{	/* Foreign/cenum.scm 30 */
						obj_t BgL_widz00_759;

						BgL_widz00_759 =
							(((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt)
										((BgL_typez00_bglt) BgL_whatz00_737))))->BgL_idz00);
						{	/* Foreign/cenum.scm 31 */
							obj_t BgL_bidz00_760;

							BgL_bidz00_760 =
								(((BgL_typez00_bglt) COBJECT(BgL_btypez00_757))->BgL_idz00);
							{	/* Foreign/cenum.scm 32 */
								obj_t BgL_idzd2ze3bidz31_761;

								{	/* Foreign/cenum.scm 33 */
									obj_t BgL_list1273z00_762;

									{	/* Foreign/cenum.scm 33 */
										obj_t BgL_arg1284z00_763;

										{	/* Foreign/cenum.scm 33 */
											obj_t BgL_arg1304z00_764;

											BgL_arg1304z00_764 =
												MAKE_YOUNG_PAIR(BgL_bidz00_760, BNIL);
											BgL_arg1284z00_763 =
												MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1304z00_764);
										}
										BgL_list1273z00_762 =
											MAKE_YOUNG_PAIR(BgL_idz00_758, BgL_arg1284z00_763);
									}
									BgL_idzd2ze3bidz31_761 =
										BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
										(BgL_list1273z00_762);
								}
								{	/* Foreign/cenum.scm 33 */
									obj_t BgL_bidzd2ze3idz31_765;

									{	/* Foreign/cenum.scm 34 */
										obj_t BgL_list1253z00_766;

										{	/* Foreign/cenum.scm 34 */
											obj_t BgL_arg1268z00_767;

											{	/* Foreign/cenum.scm 34 */
												obj_t BgL_arg1272z00_768;

												BgL_arg1272z00_768 =
													MAKE_YOUNG_PAIR(BgL_idz00_758, BNIL);
												BgL_arg1268z00_767 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
													BgL_arg1272z00_768);
											}
											BgL_list1253z00_766 =
												MAKE_YOUNG_PAIR(BgL_bidz00_760, BgL_arg1268z00_767);
										}
										BgL_bidzd2ze3idz31_765 =
											BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
											(BgL_list1253z00_766);
									}
									{	/* Foreign/cenum.scm 34 */
										obj_t BgL_bidzf3zf3_769;

										{	/* Foreign/cenum.scm 35 */
											obj_t BgL_arg1248z00_770;

											{	/* Foreign/cenum.scm 35 */
												obj_t BgL_arg1249z00_771;
												obj_t BgL_arg1252z00_772;

												{	/* Foreign/cenum.scm 35 */
													obj_t BgL_arg1455z00_773;

													BgL_arg1455z00_773 = SYMBOL_TO_STRING(BgL_idz00_758);
													BgL_arg1249z00_771 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_773);
												}
												{	/* Foreign/cenum.scm 35 */
													obj_t BgL_symbolz00_774;

													BgL_symbolz00_774 = CNST_TABLE_REF(1);
													{	/* Foreign/cenum.scm 35 */
														obj_t BgL_arg1455z00_775;

														BgL_arg1455z00_775 =
															SYMBOL_TO_STRING(BgL_symbolz00_774);
														BgL_arg1252z00_772 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_775);
													}
												}
												BgL_arg1248z00_770 =
													string_append(BgL_arg1249z00_771, BgL_arg1252z00_772);
											}
											BgL_bidzf3zf3_769 = bstring_to_symbol(BgL_arg1248z00_770);
										}
										{	/* Foreign/cenum.scm 35 */
											obj_t BgL_bidzf3zd2boolz21_776;

											{	/* Foreign/cenum.scm 36 */
												obj_t BgL_arg1239z00_777;

												{	/* Foreign/cenum.scm 36 */
													obj_t BgL_arg1242z00_778;
													obj_t BgL_arg1244z00_779;

													{	/* Foreign/cenum.scm 36 */
														obj_t BgL_arg1455z00_780;

														BgL_arg1455z00_780 =
															SYMBOL_TO_STRING(BgL_bidzf3zf3_769);
														BgL_arg1242z00_778 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_780);
													}
													{	/* Foreign/cenum.scm 36 */
														obj_t BgL_symbolz00_781;

														BgL_symbolz00_781 = CNST_TABLE_REF(2);
														{	/* Foreign/cenum.scm 36 */
															obj_t BgL_arg1455z00_782;

															BgL_arg1455z00_782 =
																SYMBOL_TO_STRING(BgL_symbolz00_781);
															BgL_arg1244z00_779 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_782);
														}
													}
													BgL_arg1239z00_777 =
														string_append(BgL_arg1242z00_778,
														BgL_arg1244z00_779);
												}
												BgL_bidzf3zd2boolz21_776 =
													bstring_to_symbol(BgL_arg1239z00_777);
											}
											{	/* Foreign/cenum.scm 36 */
												obj_t BgL_namez00_783;

												BgL_namez00_783 =
													(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_whoz00_738)))->
													BgL_namez00);
												{	/* Foreign/cenum.scm 37 */
													obj_t BgL_namezd2sanszd2z42z42_784;

													BgL_namezd2sanszd2z42z42_784 =
														BGl_stringzd2sanszd2z42z42zztype_toolsz00
														(BgL_namez00_783);
													{	/* Foreign/cenum.scm 38 */
														obj_t BgL_literalsz00_785;

														{
															BgL_cenumz00_bglt BgL_auxz00_944;

															{
																obj_t BgL_auxz00_945;

																{	/* Foreign/cenum.scm 39 */
																	BgL_objectz00_bglt BgL_tmpz00_946;

																	BgL_tmpz00_946 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_whatz00_737));
																	BgL_auxz00_945 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_946);
																}
																BgL_auxz00_944 =
																	((BgL_cenumz00_bglt) BgL_auxz00_945);
															}
															BgL_literalsz00_785 =
																(((BgL_cenumz00_bglt) COBJECT(BgL_auxz00_944))->
																BgL_literalsz00);
														}
														{	/* Foreign/cenum.scm 39 */

															{

																{	/* Foreign/cenum.scm 88 */
																	obj_t BgL_arg1102z00_845;

																	{	/* Foreign/cenum.scm 88 */
																		obj_t BgL_list1105z00_846;

																		{	/* Foreign/cenum.scm 88 */
																			obj_t BgL_arg1114z00_847;

																			{	/* Foreign/cenum.scm 88 */
																				obj_t BgL_arg1115z00_848;

																				BgL_arg1115z00_848 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(1),
																					BNIL);
																				BgL_arg1114z00_847 =
																					MAKE_YOUNG_PAIR(BgL_idz00_758,
																					BgL_arg1115z00_848);
																			}
																			BgL_list1105z00_846 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																				BgL_arg1114z00_847);
																		}
																		BgL_arg1102z00_845 =
																			BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																			(BgL_list1105z00_846);
																	}
																	{	/* Foreign/cenum.scm 86 */
																		obj_t BgL_list1103z00_849;

																		{	/* Foreign/cenum.scm 86 */
																			obj_t BgL_arg1104z00_850;

																			BgL_arg1104z00_850 =
																				MAKE_YOUNG_PAIR(BgL_arg1102z00_845,
																				BNIL);
																			BgL_list1103z00_849 =
																				MAKE_YOUNG_PAIR(BgL_bidzf3zf3_769,
																				BgL_arg1104z00_850);
																		}
																		BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
																			(BgL_list1103z00_849);
																	}
																}
																{	/* Foreign/cenum.scm 91 */
																	obj_t BgL_arg1122z00_851;

																	{	/* Foreign/cenum.scm 91 */
																		obj_t BgL_arg1123z00_852;

																		{	/* Foreign/cenum.scm 91 */
																			obj_t BgL_arg1125z00_853;
																			obj_t BgL_arg1126z00_854;

																			{	/* Foreign/cenum.scm 45 */
																				obj_t BgL_arg1157z00_839;

																				{	/* Foreign/cenum.scm 45 */
																					obj_t BgL_arg1158z00_840;

																					{	/* Foreign/cenum.scm 45 */
																						obj_t BgL_arg1162z00_841;

																						{	/* Foreign/cenum.scm 45 */
																							obj_t BgL_arg1164z00_842;
																							obj_t BgL_arg1166z00_843;

																							{	/* Foreign/cenum.scm 45 */
																								obj_t BgL_arg1171z00_844;

																								BgL_arg1171z00_844 =
																									MAKE_YOUNG_PAIR(BgL_idz00_758,
																									BNIL);
																								BgL_arg1164z00_842 =
																									MAKE_YOUNG_PAIR(CNST_TABLE_REF
																									(18), BgL_arg1171z00_844);
																							}
																							BgL_arg1166z00_843 =
																								MAKE_YOUNG_PAIR
																								(BGl_string1330z00zzforeign_cenumz00,
																								BNIL);
																							BgL_arg1162z00_841 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1164z00_842,
																								BgL_arg1166z00_843);
																						}
																						BgL_arg1158z00_840 =
																							MAKE_YOUNG_PAIR
																							(BgL_idzd2ze3bidz31_761,
																							BgL_arg1162z00_841);
																					}
																					BgL_arg1157z00_839 =
																						MAKE_YOUNG_PAIR(BgL_bidz00_760,
																						BgL_arg1158z00_840);
																				}
																				BgL_arg1125z00_853 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(17),
																					BgL_arg1157z00_839);
																			}
																			{	/* Foreign/cenum.scm 91 */
																				obj_t BgL_tmpz00_970;

																				{	/* Foreign/cenum.scm 48 */
																					obj_t BgL_mnamez00_833;

																					BgL_mnamez00_833 =
																						string_append_3
																						(BGl_string1328z00zzforeign_cenumz00,
																						BgL_namezd2sanszd2z42z42_784,
																						BGl_string1329z00zzforeign_cenumz00);
																					{	/* Foreign/cenum.scm 49 */
																						obj_t BgL_arg1182z00_834;

																						{	/* Foreign/cenum.scm 49 */
																							obj_t BgL_arg1183z00_835;

																							{	/* Foreign/cenum.scm 49 */
																								obj_t BgL_arg1187z00_836;

																								{	/* Foreign/cenum.scm 49 */
																									obj_t BgL_arg1188z00_837;
																									obj_t BgL_arg1189z00_838;

																									BgL_arg1188z00_837 =
																										MAKE_YOUNG_PAIR
																										(BgL_bidz00_760, BNIL);
																									BgL_arg1189z00_838 =
																										MAKE_YOUNG_PAIR
																										(BgL_mnamez00_833, BNIL);
																									BgL_arg1187z00_836 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1188z00_837,
																										BgL_arg1189z00_838);
																								}
																								BgL_arg1183z00_835 =
																									MAKE_YOUNG_PAIR
																									(BgL_bidzd2ze3idz31_765,
																									BgL_arg1187z00_836);
																							}
																							BgL_arg1182z00_834 =
																								MAKE_YOUNG_PAIR(BgL_idz00_758,
																								BgL_arg1183z00_835);
																						}
																						BgL_tmpz00_970 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(17), BgL_arg1182z00_834);
																					}
																				}
																				BgL_arg1126z00_854 =
																					MAKE_YOUNG_PAIR(BgL_tmpz00_970, BNIL);
																			}
																			BgL_arg1123z00_852 =
																				MAKE_YOUNG_PAIR(BgL_arg1125z00_853,
																				BgL_arg1126z00_854);
																		}
																		BgL_arg1122z00_851 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(19),
																			BgL_arg1123z00_852);
																	}
																	BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																		(BgL_arg1122z00_851);
																}
																{	/* Foreign/cenum.scm 94 */
																	obj_t BgL_arg1129z00_855;

																	{	/* Foreign/cenum.scm 94 */
																		obj_t BgL_arg1131z00_856;

																		{	/* Foreign/cenum.scm 94 */
																			obj_t BgL_arg1132z00_857;

																			{	/* Foreign/cenum.scm 94 */
																				obj_t BgL_arg1137z00_858;

																				{	/* Foreign/cenum.scm 94 */
																					obj_t BgL_arg1138z00_859;

																					BgL_arg1138z00_859 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(20),
																						BNIL);
																					BgL_arg1137z00_858 =
																						MAKE_YOUNG_PAIR
																						(BgL_bidzf3zd2boolz21_776,
																						BgL_arg1138z00_859);
																				}
																				BgL_arg1132z00_857 =
																					BGl_makezd2protozd2inlinez00zzforeign_libraryz00
																					(BgL_arg1137z00_858);
																			}
																			BgL_arg1131z00_856 =
																				MAKE_YOUNG_PAIR(BgL_arg1132z00_857,
																				BNIL);
																		}
																		BgL_arg1129z00_855 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(21),
																			BgL_arg1131z00_856);
																	}
																	BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																		(BgL_arg1129z00_855);
																}
																{	/* Foreign/cenum.scm 95 */
																	obj_t BgL_arg1140z00_860;

																	{	/* Foreign/cenum.scm 95 */
																		obj_t BgL_arg1141z00_861;

																		{	/* Foreign/cenum.scm 95 */
																			obj_t BgL_arg1142z00_862;

																			{	/* Foreign/cenum.scm 95 */
																				obj_t BgL_arg1143z00_863;

																				{	/* Foreign/cenum.scm 95 */
																					obj_t BgL_arg1145z00_864;

																					{	/* Foreign/cenum.scm 95 */
																						obj_t BgL_arg1148z00_865;

																						BgL_arg1148z00_865 =
																							MAKE_YOUNG_PAIR(BgL_bidz00_760,
																							BNIL);
																						BgL_arg1145z00_864 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(22), BgL_arg1148z00_865);
																					}
																					BgL_arg1143z00_863 =
																						MAKE_YOUNG_PAIR(BgL_arg1145z00_864,
																						BNIL);
																				}
																				BgL_arg1142z00_862 =
																					MAKE_YOUNG_PAIR(BgL_bidzf3zf3_769,
																					BgL_arg1143z00_863);
																			}
																			BgL_arg1141z00_861 =
																				MAKE_YOUNG_PAIR(BgL_arg1142z00_862,
																				BNIL);
																		}
																		BgL_arg1140z00_860 =
																			MAKE_YOUNG_PAIR(CNST_TABLE_REF(4),
																			BgL_arg1141z00_861);
																	}
																	BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
																		(BgL_arg1140z00_860);
																}
																{	/* Foreign/cenum.scm 98 */
																	obj_t BgL_arg1149z00_866;
																	obj_t BgL_arg1152z00_867;

																	{	/* Foreign/cenum.scm 62 */
																		obj_t BgL_arg1212z00_810;

																		{	/* Foreign/cenum.scm 62 */
																			obj_t BgL_arg1215z00_811;
																			obj_t BgL_arg1216z00_812;

																			{	/* Foreign/cenum.scm 62 */
																				obj_t BgL_list1217z00_813;

																				{	/* Foreign/cenum.scm 62 */
																					obj_t BgL_arg1218z00_814;

																					{	/* Foreign/cenum.scm 62 */
																						obj_t BgL_arg1219z00_815;

																						BgL_arg1219z00_815 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF(5),
																							BNIL);
																						BgL_arg1218z00_814 =
																							MAKE_YOUNG_PAIR(BgL_idz00_758,
																							BgL_arg1219z00_815);
																					}
																					BgL_list1217z00_813 =
																						MAKE_YOUNG_PAIR(CNST_TABLE_REF(6),
																						BgL_arg1218z00_814);
																				}
																				BgL_arg1215z00_811 =
																					BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																					(BgL_list1217z00_813);
																			}
																			{	/* Foreign/cenum.scm 63 */
																				obj_t BgL_arg1220z00_816;
																				obj_t BgL_arg1221z00_817;

																				BgL_arg1220z00_816 =
																					BGl_makezd2typedzd2identz00zzast_identz00
																					(CNST_TABLE_REF(7), BgL_idz00_758);
																				BgL_arg1221z00_817 =
																					MAKE_YOUNG_PAIR
																					(BGl_makezd2typedzd2identz00zzast_identz00
																					(CNST_TABLE_REF(8), BgL_idz00_758),
																					BNIL);
																				BgL_arg1216z00_812 =
																					MAKE_YOUNG_PAIR(BgL_arg1220z00_816,
																					BgL_arg1221z00_817);
																			}
																			BgL_arg1212z00_810 =
																				MAKE_YOUNG_PAIR(BgL_arg1215z00_811,
																				BgL_arg1216z00_812);
																		}
																		BgL_arg1149z00_866 =
																			BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																			(BgL_arg1212z00_810, CNST_TABLE_REF(9));
																	}
																	{	/* Foreign/cenum.scm 98 */
																		obj_t BgL_auxz00_1041;
																		obj_t BgL_tmpz00_1016;

																		{
																			obj_t BgL_literalsz00_792;
																			obj_t BgL_resz00_793;

																			BgL_literalsz00_792 = BgL_literalsz00_785;
																			BgL_resz00_793 = BNIL;
																		BgL_loopz00_791:
																			if (NULLP(BgL_literalsz00_792))
																				{	/* Foreign/cenum.scm 71 */
																					BgL_auxz00_1041 = BgL_resz00_793;
																				}
																			else
																				{	/* Foreign/cenum.scm 73 */
																					obj_t BgL_literalz00_794;

																					BgL_literalz00_794 =
																						CAR(((obj_t) BgL_literalsz00_792));
																					{	/* Foreign/cenum.scm 73 */
																						obj_t BgL_literalzd2idzd2_795;

																						BgL_literalzd2idzd2_795 =
																							CAR(((obj_t) BgL_literalz00_794));
																						{	/* Foreign/cenum.scm 74 */
																							obj_t BgL_literalzd2namezd2_796;

																							{	/* Foreign/cenum.scm 75 */
																								obj_t BgL_pairz00_797;

																								BgL_pairz00_797 =
																									CDR(
																									((obj_t) BgL_literalz00_794));
																								BgL_literalzd2namezd2_796 =
																									CAR(BgL_pairz00_797);
																							}
																							{	/* Foreign/cenum.scm 75 */
																								obj_t BgL_accesszd2idzd2_798;

																								{	/* Foreign/cenum.scm 76 */
																									obj_t BgL_list1235z00_799;

																									{	/* Foreign/cenum.scm 76 */
																										obj_t BgL_arg1236z00_800;

																										{	/* Foreign/cenum.scm 76 */
																											obj_t BgL_arg1238z00_801;

																											BgL_arg1238z00_801 =
																												MAKE_YOUNG_PAIR
																												(BgL_literalzd2idzd2_795,
																												BNIL);
																											BgL_arg1236z00_800 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(3),
																												BgL_arg1238z00_801);
																										}
																										BgL_list1235z00_799 =
																											MAKE_YOUNG_PAIR
																											(BgL_idz00_758,
																											BgL_arg1236z00_800);
																									}
																									BgL_accesszd2idzd2_798 =
																										BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
																										(BgL_list1235z00_799);
																								}
																								{	/* Foreign/cenum.scm 76 */
																									obj_t BgL_accessz00_802;

																									{	/* Foreign/cenum.scm 78 */
																										obj_t BgL_arg1230z00_803;
																										obj_t BgL_arg1231z00_804;

																										BgL_arg1230z00_803 =
																											MAKE_YOUNG_PAIR
																											(BGl_makezd2typedzd2identz00zzast_identz00
																											(BgL_accesszd2idzd2_798,
																												BgL_widz00_759), BNIL);
																										{	/* Foreign/cenum.scm 79 */
																											obj_t BgL_arg1233z00_805;
																											obj_t BgL_arg1234z00_806;

																											BgL_arg1233z00_805 =
																												BGl_makezd2typedzd2identz00zzast_identz00
																												(CNST_TABLE_REF(4),
																												BgL_widz00_759);
																											BgL_arg1234z00_806 =
																												MAKE_YOUNG_PAIR
																												(BgL_literalzd2namezd2_796,
																												BNIL);
																											BgL_arg1231z00_804 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1233z00_805,
																												BgL_arg1234z00_806);
																										}
																										BgL_accessz00_802 =
																											BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																											(BgL_arg1230z00_803,
																											BgL_arg1231z00_804);
																									}
																									{	/* Foreign/cenum.scm 77 */

																										{	/* Foreign/cenum.scm 81 */
																											obj_t BgL_list1227z00_807;

																											BgL_list1227z00_807 =
																												MAKE_YOUNG_PAIR
																												(BgL_accesszd2idzd2_798,
																												BNIL);
																											BGl_registerzd2foreignzd2accesszd2identsz12zc0zzforeign_libraryz00
																												(BgL_list1227z00_807);
																										}
																										{	/* Foreign/cenum.scm 82 */
																											obj_t BgL_arg1228z00_808;
																											obj_t BgL_arg1229z00_809;

																											BgL_arg1228z00_808 =
																												CDR(
																												((obj_t)
																													BgL_literalsz00_792));
																											BgL_arg1229z00_809 =
																												MAKE_YOUNG_PAIR
																												(BgL_accessz00_802,
																												BgL_resz00_793);
																											{
																												obj_t BgL_resz00_1069;
																												obj_t
																													BgL_literalsz00_1068;
																												BgL_literalsz00_1068 =
																													BgL_arg1228z00_808;
																												BgL_resz00_1069 =
																													BgL_arg1229z00_809;
																												BgL_resz00_793 =
																													BgL_resz00_1069;
																												BgL_literalsz00_792 =
																													BgL_literalsz00_1068;
																												goto BgL_loopz00_791;
																											}
																										}
																									}
																								}
																							}
																						}
																					}
																				}
																		}
																		{	/* Foreign/cenum.scm 54 */
																			obj_t BgL_arg1191z00_818;
																			obj_t BgL_arg1193z00_819;

																			{	/* Foreign/cenum.scm 54 */
																				obj_t BgL_arg1194z00_820;

																				BgL_arg1194z00_820 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(10),
																					BNIL);
																				BgL_arg1191z00_818 =
																					MAKE_YOUNG_PAIR
																					(BgL_bidzf3zd2boolz21_776,
																					BgL_arg1194z00_820);
																			}
																			{	/* Foreign/cenum.scm 55 */
																				obj_t BgL_arg1196z00_821;

																				{	/* Foreign/cenum.scm 55 */
																					obj_t BgL_arg1197z00_822;
																					obj_t BgL_arg1198z00_823;

																					{	/* Foreign/cenum.scm 55 */
																						obj_t BgL_arg1199z00_824;

																						BgL_arg1199z00_824 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(11), BNIL);
																						BgL_arg1197z00_822 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(12), BgL_arg1199z00_824);
																					}
																					{	/* Foreign/cenum.scm 56 */
																						obj_t BgL_arg1200z00_825;
																						obj_t BgL_arg1201z00_826;

																						{	/* Foreign/cenum.scm 56 */
																							obj_t BgL_arg1202z00_827;

																							{	/* Foreign/cenum.scm 56 */
																								obj_t BgL_arg1203z00_828;
																								obj_t BgL_arg1206z00_829;

																								{	/* Foreign/cenum.scm 56 */
																									obj_t BgL_arg1208z00_830;

																									BgL_arg1208z00_830 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(11), BNIL);
																									BgL_arg1203z00_828 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(13),
																										BgL_arg1208z00_830);
																								}
																								{	/* Foreign/cenum.scm 56 */
																									obj_t BgL_arg1209z00_831;

																									{	/* Foreign/cenum.scm 56 */
																										obj_t BgL_arg1210z00_832;

																										BgL_arg1210z00_832 =
																											MAKE_YOUNG_PAIR
																											(BgL_bidz00_760, BNIL);
																										BgL_arg1209z00_831 =
																											MAKE_YOUNG_PAIR
																											(CNST_TABLE_REF(14),
																											BgL_arg1210z00_832);
																									}
																									BgL_arg1206z00_829 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1209z00_831, BNIL);
																								}
																								BgL_arg1202z00_827 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1203z00_828,
																									BgL_arg1206z00_829);
																							}
																							BgL_arg1200z00_825 =
																								MAKE_YOUNG_PAIR(CNST_TABLE_REF
																								(15), BgL_arg1202z00_827);
																						}
																						BgL_arg1201z00_826 =
																							MAKE_YOUNG_PAIR(BFALSE, BNIL);
																						BgL_arg1198z00_823 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1200z00_825,
																							BgL_arg1201z00_826);
																					}
																					BgL_arg1196z00_821 =
																						MAKE_YOUNG_PAIR(BgL_arg1197z00_822,
																						BgL_arg1198z00_823);
																				}
																				BgL_arg1193z00_819 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(16),
																					BgL_arg1196z00_821);
																			}
																			BgL_tmpz00_1016 =
																				BGl_makezd2definezd2inlinez00zzforeign_libraryz00
																				(BgL_arg1191z00_818,
																				BgL_arg1193z00_819);
																		}
																		BgL_arg1152z00_867 =
																			MAKE_YOUNG_PAIR(BgL_tmpz00_1016,
																			BgL_auxz00_1041);
																	}
																	return
																		MAKE_YOUNG_PAIR(BgL_arg1149z00_866,
																		BgL_arg1152z00_867);
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzforeign_cenumz00(void)
	{
		{	/* Foreign/cenum.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1331z00zzforeign_cenumz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string1331z00zzforeign_cenumz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1331z00zzforeign_cenumz00));
			BGl_modulezd2initializa7ationz75zzforeign_ctypez00(396356879L,
				BSTRING_TO_STRING(BGl_string1331z00zzforeign_cenumz00));
			BGl_modulezd2initializa7ationz75zzforeign_accessz00(471073666L,
				BSTRING_TO_STRING(BGl_string1331z00zzforeign_cenumz00));
			BGl_modulezd2initializa7ationz75zzforeign_libraryz00(419430976L,
				BSTRING_TO_STRING(BGl_string1331z00zzforeign_cenumz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1331z00zzforeign_cenumz00));
			return
				BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1331z00zzforeign_cenumz00));
		}

	}

#ifdef __cplusplus
}
#endif
