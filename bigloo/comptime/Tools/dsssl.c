/*===========================================================================*/
/*   (Tools/dsssl.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/dsssl.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_DSSSL_TYPE_DEFINITIONS
#define BGL_TOOLS_DSSSL_TYPE_DEFINITIONS
#endif													// BGL_TOOLS_DSSSL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62dssslzd2argsza2zd2ze3argszd2listzf1zztools_dssslz00(obj_t,
		obj_t);
	static obj_t BGl_dssslzd2argumentszd2zztools_dssslz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2keyzd2argszd2sortzd2zztools_dssslz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztools_dssslz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_dssslzd2keyszd2zztools_dssslz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dssslzd2beforezd2dssslz00zztools_dssslz00(obj_t);
	static obj_t BGl_z62dssslzd2keyszb0zztools_dssslz00(obj_t, obj_t);
	static obj_t BGl_z62dssslzd2arityzb0zztools_dssslz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dssslzd2formalszd2zztools_dssslz00(obj_t);
	static obj_t
		BGl_z62dssslzd2keyzd2onlyzd2prototypezf3z43zztools_dssslz00(obj_t, obj_t);
	static obj_t BGl_z62dssslzd2prototypezf3z43zztools_dssslz00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zztools_dssslz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62dssslzd2defaultzd2formalz62zztools_dssslz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zztools_dssslz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2defaultzd2formalz00zztools_dssslz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_dssslzd2keyzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62dssslzd2findzd2firstzd2formalzb0zztools_dssslz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62dssslzd2optionalzd2onlyzd2prototypezf3z43zztools_dssslz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztools_dssslz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00(obj_t);
	BGL_IMPORT obj_t BGl_zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31344ze3ze5zztools_dssslz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_dssslzd2checkzd2prototypezf3zf3zztools_dssslz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t);
	static bool_t BGl_dssslzd2onlyzf3z21zztools_dssslz00(obj_t, obj_t);
	BGL_IMPORT bool_t bigloo_string_lt(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62dssslzd2beforezd2dssslz62zztools_dssslz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zztools_dssslz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_dssslz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztools_dssslz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2argsza2zd2ze3argszd2listz93zztools_dssslz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dssslzd2optionalszd2zztools_dssslz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zztools_dssslz00(void);
	static obj_t BGl_z62dssslzd2optionalszb0zztools_dssslz00(obj_t, obj_t);
	static obj_t BGl_z62dssslzd2formalszb0zztools_dssslz00(obj_t, obj_t);
	static obj_t BGl_z62dssslzd2keyzd2argszd2sortzb0zztools_dssslz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dssslzd2findzd2firstzd2formalzd2zztools_dssslz00(obj_t);
	static obj_t BGl_z62dssslzd2checkzd2prototypezf3z91zztools_dssslz00(obj_t,
		obj_t);
	static obj_t BGl_z62dssslzd2defaultedzd2formalzf3z91zztools_dssslz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dssslzd2arityzd2zztools_dssslz00(obj_t, bool_t);
	BGL_EXPORTED_DECL bool_t BGl_dssslzd2prototypezf3z21zztools_dssslz00(obj_t);
	BGL_IMPORT bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2findzd2firstzd2formalzd2envz00zztools_dssslz00,
		BgL_bgl_za762dssslza7d2findza71381za7,
		BGl_z62dssslzd2findzd2firstzd2formalzb0zztools_dssslz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2optionalzd2onlyzd2prototypezf3zd2envzf3zztools_dssslz00,
		BgL_bgl_za762dssslza7d2optio1382z00,
		BGl_z62dssslzd2optionalzd2onlyzd2prototypezf3z43zztools_dssslz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1375z00zztools_dssslz00,
		BgL_bgl_string1375za700za7za7t1383za7, "dsssl-find-first-formal", 23);
	      DEFINE_STRING(BGl_string1376z00zztools_dssslz00,
		BgL_bgl_string1376za700za7za7t1384za7, "Illegal dsssl formal list", 25);
	      DEFINE_STRING(BGl_string1378z00zztools_dssslz00,
		BgL_bgl_string1378za700za7za7t1385za7, "tools_dsssl", 11);
	      DEFINE_STRING(BGl_string1379z00zztools_dssslz00,
		BgL_bgl_string1379za700za7za7t1386za7, "(#!optional #!key) ", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2checkzd2prototypezf3zd2envz21zztools_dssslz00,
		BgL_bgl_za762dssslza7d2check1387z00,
		BGl_z62dssslzd2checkzd2prototypezf3z91zztools_dssslz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1377z00zztools_dssslz00,
		BgL_bgl_za762za7c3za704anonymo1388za7,
		BGl_z62zc3z04anonymousza31344ze3ze5zztools_dssslz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2keyzd2onlyzd2prototypezf3zd2envzf3zztools_dssslz00,
		BgL_bgl_za762dssslza7d2keyza7d1389za7,
		BGl_z62dssslzd2keyzd2onlyzd2prototypezf3z43zztools_dssslz00, 0L, BUNSPEC,
		1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dssslzd2keyszd2envz00zztools_dssslz00,
		BgL_bgl_za762dssslza7d2keysza71390za7,
		BGl_z62dssslzd2keyszb0zztools_dssslz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2prototypezf3zd2envzf3zztools_dssslz00,
		BgL_bgl_za762dssslza7d2proto1391z00,
		BGl_z62dssslzd2prototypezf3z43zztools_dssslz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2defaultzd2formalzd2envzd2zztools_dssslz00,
		BgL_bgl_za762dssslza7d2defau1392z00,
		BGl_z62dssslzd2defaultzd2formalz62zztools_dssslz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_dssslzd2formalszd2envz00zztools_dssslz00,
		BgL_bgl_za762dssslza7d2forma1393z00,
		BGl_z62dssslzd2formalszb0zztools_dssslz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2argsza2zd2ze3argszd2listzd2envz41zztools_dssslz00,
		BgL_bgl_za762dssslza7d2argsza71394za7,
		BGl_z62dssslzd2argsza2zd2ze3argszd2listzf1zztools_dssslz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2defaultedzd2formalzf3zd2envz21zztools_dssslz00,
		BgL_bgl_za762dssslza7d2defau1395z00,
		BGl_z62dssslzd2defaultedzd2formalzf3z91zztools_dssslz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_dssslzd2optionalszd2envz00zztools_dssslz00,
		BgL_bgl_za762dssslza7d2optio1396z00,
		BGl_z62dssslzd2optionalszb0zztools_dssslz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dssslzd2arityzd2envz00zztools_dssslz00,
		BgL_bgl_za762dssslza7d2arity1397z00,
		BGl_z62dssslzd2arityzb0zztools_dssslz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2keyzd2argszd2sortzd2envz00zztools_dssslz00,
		BgL_bgl_za762dssslza7d2keyza7d1398za7,
		BGl_z62dssslzd2keyzd2argszd2sortzb0zztools_dssslz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2beforezd2dssslzd2envzd2zztools_dssslz00,
		BgL_bgl_za762dssslza7d2befor1399z00,
		BGl_z62dssslzd2beforezd2dssslz62zztools_dssslz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztools_dssslz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long
		BgL_checksumz00_435, char *BgL_fromz00_436)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_dssslz00))
				{
					BGl_requirezd2initializa7ationz75zztools_dssslz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_dssslz00();
					BGl_libraryzd2moduleszd2initz00zztools_dssslz00();
					BGl_cnstzd2initzd2zztools_dssslz00();
					BGl_importedzd2moduleszd2initz00zztools_dssslz00();
					return BGl_methodzd2initzd2zztools_dssslz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_dssslz00(void)
	{
		{	/* Tools/dsssl.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "tools_dsssl");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_dsssl");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_dsssl");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tools_dsssl");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "tools_dsssl");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "tools_dsssl");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "tools_dsssl");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tools_dsssl");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "tools_dsssl");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztools_dssslz00(void)
	{
		{	/* Tools/dsssl.scm 15 */
			{	/* Tools/dsssl.scm 15 */
				obj_t BgL_cportz00_418;

				{	/* Tools/dsssl.scm 15 */
					obj_t BgL_stringz00_425;

					BgL_stringz00_425 = BGl_string1379z00zztools_dssslz00;
					{	/* Tools/dsssl.scm 15 */
						obj_t BgL_startz00_426;

						BgL_startz00_426 = BINT(0L);
						{	/* Tools/dsssl.scm 15 */
							obj_t BgL_endz00_427;

							BgL_endz00_427 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_425)));
							{	/* Tools/dsssl.scm 15 */

								BgL_cportz00_418 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_425, BgL_startz00_426, BgL_endz00_427);
				}}}}
				{
					long BgL_iz00_419;

					BgL_iz00_419 = 0L;
				BgL_loopz00_420:
					if ((BgL_iz00_419 == -1L))
						{	/* Tools/dsssl.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tools/dsssl.scm 15 */
							{	/* Tools/dsssl.scm 15 */
								obj_t BgL_arg1380z00_421;

								{	/* Tools/dsssl.scm 15 */

									{	/* Tools/dsssl.scm 15 */
										obj_t BgL_locationz00_423;

										BgL_locationz00_423 = BBOOL(((bool_t) 0));
										{	/* Tools/dsssl.scm 15 */

											BgL_arg1380z00_421 =
												BGl_readz00zz__readerz00(BgL_cportz00_418,
												BgL_locationz00_423);
										}
									}
								}
								{	/* Tools/dsssl.scm 15 */
									int BgL_tmpz00_463;

									BgL_tmpz00_463 = (int) (BgL_iz00_419);
									CNST_TABLE_SET(BgL_tmpz00_463, BgL_arg1380z00_421);
							}}
							{	/* Tools/dsssl.scm 15 */
								int BgL_auxz00_424;

								BgL_auxz00_424 = (int) ((BgL_iz00_419 - 1L));
								{
									long BgL_iz00_468;

									BgL_iz00_468 = (long) (BgL_auxz00_424);
									BgL_iz00_419 = BgL_iz00_468;
									goto BgL_loopz00_420;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_dssslz00(void)
	{
		{	/* Tools/dsssl.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* dsssl-prototype? */
	BGL_EXPORTED_DEF bool_t BGl_dssslzd2prototypezf3z21zztools_dssslz00(obj_t
		BgL_argsz00_3)
	{
		{	/* Tools/dsssl.scm 37 */
			{
				obj_t BgL_argsz00_32;

				BgL_argsz00_32 = BgL_argsz00_3;
			BgL_zc3z04anonymousza31023ze3z87_33:
				if (NULLP(BgL_argsz00_32))
					{	/* Tools/dsssl.scm 40 */
						return ((bool_t) 0);
					}
				else
					{	/* Tools/dsssl.scm 40 */
						if (PAIRP(BgL_argsz00_32))
							{	/* Tools/dsssl.scm 42 */
								if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
										(BgL_argsz00_32)))
									{	/* Tools/dsssl.scm 44 */
										return ((bool_t) 1);
									}
								else
									{
										obj_t BgL_argsz00_478;

										BgL_argsz00_478 = CDR(BgL_argsz00_32);
										BgL_argsz00_32 = BgL_argsz00_478;
										goto BgL_zc3z04anonymousza31023ze3z87_33;
									}
							}
						else
							{	/* Tools/dsssl.scm 42 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* &dsssl-prototype? */
	obj_t BGl_z62dssslzd2prototypezf3z43zztools_dssslz00(obj_t BgL_envz00_385,
		obj_t BgL_argsz00_386)
	{
		{	/* Tools/dsssl.scm 37 */
			return
				BBOOL(BGl_dssslzd2prototypezf3z21zztools_dssslz00(BgL_argsz00_386));
		}

	}



/* dsssl-only? */
	bool_t BGl_dssslzd2onlyzf3z21zztools_dssslz00(obj_t BgL_dssslz00_4,
		obj_t BgL_argsz00_5)
	{
		{	/* Tools/dsssl.scm 52 */
			{
				obj_t BgL_argsz00_42;
				bool_t BgL_rz00_43;

				BgL_argsz00_42 = BgL_argsz00_5;
				BgL_rz00_43 = ((bool_t) 0);
			BgL_zc3z04anonymousza31030ze3z87_44:
				if (NULLP(BgL_argsz00_42))
					{	/* Tools/dsssl.scm 56 */
						return BgL_rz00_43;
					}
				else
					{	/* Tools/dsssl.scm 56 */
						if (PAIRP(BgL_argsz00_42))
							{	/* Tools/dsssl.scm 58 */
								if ((CAR(BgL_argsz00_42) == BgL_dssslz00_4))
									{
										bool_t BgL_rz00_491;
										obj_t BgL_argsz00_489;

										BgL_argsz00_489 = CDR(BgL_argsz00_42);
										BgL_rz00_491 = ((bool_t) 1);
										BgL_rz00_43 = BgL_rz00_491;
										BgL_argsz00_42 = BgL_argsz00_489;
										goto BgL_zc3z04anonymousza31030ze3z87_44;
									}
								else
									{	/* Tools/dsssl.scm 60 */
										if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
												(BgL_argsz00_42)))
											{	/* Tools/dsssl.scm 62 */
												return ((bool_t) 0);
											}
										else
											{
												obj_t BgL_argsz00_495;

												BgL_argsz00_495 = CDR(BgL_argsz00_42);
												BgL_argsz00_42 = BgL_argsz00_495;
												goto BgL_zc3z04anonymousza31030ze3z87_44;
											}
									}
							}
						else
							{	/* Tools/dsssl.scm 58 */
								return BgL_rz00_43;
							}
					}
			}
		}

	}



/* dsssl-optional-only-prototype? */
	BGL_EXPORTED_DEF bool_t
		BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t
		BgL_argsz00_6)
	{
		{	/* Tools/dsssl.scm 72 */
			return BGl_dssslzd2onlyzf3z21zztools_dssslz00((BOPTIONAL), BgL_argsz00_6);
		}

	}



/* &dsssl-optional-only-prototype? */
	obj_t BGl_z62dssslzd2optionalzd2onlyzd2prototypezf3z43zztools_dssslz00(obj_t
		BgL_envz00_387, obj_t BgL_argsz00_388)
	{
		{	/* Tools/dsssl.scm 72 */
			return
				BBOOL(BGl_dssslzd2optionalzd2onlyzd2prototypezf3z21zztools_dssslz00
				(BgL_argsz00_388));
		}

	}



/* dsssl-key-only-prototype? */
	BGL_EXPORTED_DEF bool_t
		BGl_dssslzd2keyzd2onlyzd2prototypezf3z21zztools_dssslz00(obj_t
		BgL_argsz00_7)
	{
		{	/* Tools/dsssl.scm 78 */
			return BGl_dssslzd2onlyzf3z21zztools_dssslz00((BKEY), BgL_argsz00_7);
		}

	}



/* &dsssl-key-only-prototype? */
	obj_t BGl_z62dssslzd2keyzd2onlyzd2prototypezf3z43zztools_dssslz00(obj_t
		BgL_envz00_389, obj_t BgL_argsz00_390)
	{
		{	/* Tools/dsssl.scm 78 */
			return
				BBOOL(BGl_dssslzd2keyzd2onlyzd2prototypezf3z21zztools_dssslz00
				(BgL_argsz00_390));
		}

	}



/* dsssl-check-prototype? */
	BGL_EXPORTED_DEF bool_t
		BGl_dssslzd2checkzd2prototypezf3zf3zztools_dssslz00(obj_t BgL_argsz00_8)
	{
		{	/* Tools/dsssl.scm 88 */
			{
				obj_t BgL_argsz00_57;

				BgL_argsz00_57 = BgL_argsz00_8;
			BgL_zc3z04anonymousza31041ze3z87_58:
				if (NULLP(BgL_argsz00_57))
					{	/* Tools/dsssl.scm 91 */
						return ((bool_t) 1);
					}
				else
					{	/* Tools/dsssl.scm 91 */
						if (PAIRP(BgL_argsz00_57))
							{	/* Tools/dsssl.scm 93 */
								if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
										(BgL_argsz00_57)))
									{
										obj_t BgL_argsz00_64;

										BgL_argsz00_64 = BgL_argsz00_57;
										if (((BREST) == CAR(BgL_argsz00_64)))
											{	/* Tools/dsssl.scm 103 */
												bool_t BgL_test1415z00_513;

												if (NULLP(CDR(BgL_argsz00_64)))
													{	/* Tools/dsssl.scm 103 */
														BgL_test1415z00_513 = ((bool_t) 1);
													}
												else
													{	/* Tools/dsssl.scm 103 */
														bool_t BgL_test1417z00_517;

														{	/* Tools/dsssl.scm 103 */
															obj_t BgL_tmpz00_518;

															BgL_tmpz00_518 = CAR(CDR(BgL_argsz00_64));
															BgL_test1417z00_517 = SYMBOLP(BgL_tmpz00_518);
														}
														if (BgL_test1417z00_517)
															{	/* Tools/dsssl.scm 103 */
																BgL_test1415z00_513 = ((bool_t) 0);
															}
														else
															{	/* Tools/dsssl.scm 103 */
																BgL_test1415z00_513 = ((bool_t) 1);
															}
													}
												if (BgL_test1415z00_513)
													{	/* Tools/dsssl.scm 103 */
														return ((bool_t) 1);
													}
												else
													{
														obj_t BgL_argsz00_522;

														BgL_argsz00_522 = CDR(CDR(BgL_argsz00_64));
														BgL_argsz00_57 = BgL_argsz00_522;
														goto BgL_zc3z04anonymousza31041ze3z87_58;
													}
											}
										else
											{	/* Tools/dsssl.scm 102 */
												if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
														(BgL_argsz00_64)))
													{
														obj_t BgL_argsz00_528;

														BgL_argsz00_528 = CDR(BgL_argsz00_64);
														BgL_argsz00_57 = BgL_argsz00_528;
														goto BgL_zc3z04anonymousza31041ze3z87_58;
													}
												else
													{	/* Tools/dsssl.scm 108 */
														bool_t BgL_test1419z00_530;

														{	/* Tools/dsssl.scm 108 */
															obj_t BgL_tmpz00_531;

															BgL_tmpz00_531 = CAR(BgL_argsz00_64);
															BgL_test1419z00_530 = SYMBOLP(BgL_tmpz00_531);
														}
														if (BgL_test1419z00_530)
															{
																obj_t BgL_argsz00_534;

																BgL_argsz00_534 = CDR(BgL_argsz00_64);
																BgL_argsz00_57 = BgL_argsz00_534;
																goto BgL_zc3z04anonymousza31041ze3z87_58;
															}
														else
															{	/* Tools/dsssl.scm 110 */
																bool_t BgL_test1420z00_536;

																{	/* Tools/dsssl.scm 110 */
																	bool_t BgL_test1421z00_537;

																	{	/* Tools/dsssl.scm 110 */
																		obj_t BgL_tmpz00_538;

																		BgL_tmpz00_538 = CAR(BgL_argsz00_64);
																		BgL_test1421z00_537 = PAIRP(BgL_tmpz00_538);
																	}
																	if (BgL_test1421z00_537)
																		{	/* Tools/dsssl.scm 111 */
																			bool_t BgL_test1422z00_541;

																			{	/* Tools/dsssl.scm 111 */
																				obj_t BgL_tmpz00_542;

																				BgL_tmpz00_542 = CAR(BgL_argsz00_64);
																				BgL_test1422z00_541 =
																					SYMBOLP(BgL_tmpz00_542);
																			}
																			if (BgL_test1422z00_541)
																				{	/* Tools/dsssl.scm 112 */
																					bool_t BgL_test1423z00_545;

																					{	/* Tools/dsssl.scm 112 */
																						obj_t BgL_tmpz00_546;

																						BgL_tmpz00_546 =
																							CDR(BgL_argsz00_64);
																						BgL_test1423z00_545 =
																							PAIRP(BgL_tmpz00_546);
																					}
																					if (BgL_test1423z00_545)
																						{	/* Tools/dsssl.scm 112 */
																							BgL_test1420z00_536 =
																								NULLP(CDR(CDR(BgL_argsz00_64)));
																						}
																					else
																						{	/* Tools/dsssl.scm 112 */
																							BgL_test1420z00_536 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Tools/dsssl.scm 111 */
																					BgL_test1420z00_536 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Tools/dsssl.scm 110 */
																			BgL_test1420z00_536 = ((bool_t) 0);
																		}
																}
																if (BgL_test1420z00_536)
																	{
																		obj_t BgL_argsz00_552;

																		BgL_argsz00_552 = CDR(BgL_argsz00_64);
																		BgL_argsz00_57 = BgL_argsz00_552;
																		goto BgL_zc3z04anonymousza31041ze3z87_58;
																	}
																else
																	{	/* Tools/dsssl.scm 110 */
																		return ((bool_t) 0);
																	}
															}
													}
											}
									}
								else
									{
										obj_t BgL_argsz00_554;

										BgL_argsz00_554 = CDR(BgL_argsz00_57);
										BgL_argsz00_57 = BgL_argsz00_554;
										goto BgL_zc3z04anonymousza31041ze3z87_58;
									}
							}
						else
							{	/* Tools/dsssl.scm 93 */
								return ((bool_t) 0);
							}
					}
			}
		}

	}



/* &dsssl-check-prototype? */
	obj_t BGl_z62dssslzd2checkzd2prototypezf3z91zztools_dssslz00(obj_t
		BgL_envz00_391, obj_t BgL_argsz00_392)
	{
		{	/* Tools/dsssl.scm 88 */
			return
				BBOOL(BGl_dssslzd2checkzd2prototypezf3zf3zztools_dssslz00
				(BgL_argsz00_392));
		}

	}



/* dsssl-arity */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2arityzd2zztools_dssslz00(obj_t
		BgL_argsz00_9, bool_t BgL_optimz00_10)
	{
		{	/* Tools/dsssl.scm 132 */
			{
				long BgL_iz00_115;
				obj_t BgL_az00_116;

				BgL_iz00_115 = 0L;
				BgL_az00_116 = BgL_argsz00_9;
			BgL_zc3z04anonymousza31116ze3z87_117:
				if (NULLP(BgL_az00_116))
					{	/* Tools/dsssl.scm 137 */
						return BINT(BgL_iz00_115);
					}
				else
					{	/* Tools/dsssl.scm 139 */
						bool_t BgL_test1425z00_561;

						{	/* Tools/dsssl.scm 139 */
							obj_t BgL_tmpz00_562;

							BgL_tmpz00_562 = CAR(((obj_t) BgL_az00_116));
							BgL_test1425z00_561 = SYMBOLP(BgL_tmpz00_562);
						}
						if (BgL_test1425z00_561)
							{	/* Tools/dsssl.scm 140 */
								long BgL_arg1125z00_121;
								obj_t BgL_arg1126z00_122;

								BgL_arg1125z00_121 = (BgL_iz00_115 + 1L);
								BgL_arg1126z00_122 = CDR(((obj_t) BgL_az00_116));
								{
									obj_t BgL_az00_570;
									long BgL_iz00_569;

									BgL_iz00_569 = BgL_arg1125z00_121;
									BgL_az00_570 = BgL_arg1126z00_122;
									BgL_az00_116 = BgL_az00_570;
									BgL_iz00_115 = BgL_iz00_569;
									goto BgL_zc3z04anonymousza31116ze3z87_117;
								}
							}
						else
							{	/* Tools/dsssl.scm 141 */
								bool_t BgL_test1426z00_571;

								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
												((obj_t) BgL_az00_116)), CNST_TABLE_REF(0))))
									{	/* Tools/dsssl.scm 141 */
										BgL_test1426z00_571 = BgL_optimz00_10;
									}
								else
									{	/* Tools/dsssl.scm 141 */
										BgL_test1426z00_571 = ((bool_t) 0);
									}
								if (BgL_test1426z00_571)
									{	/* Tools/dsssl.scm 142 */
										bool_t BgL_test1428z00_578;

										{	/* Tools/dsssl.scm 142 */
											obj_t BgL_g1018z00_139;

											BgL_g1018z00_139 = CDR(((obj_t) BgL_az00_116));
											{
												obj_t BgL_l1016z00_141;

												BgL_l1016z00_141 = BgL_g1018z00_139;
											BgL_zc3z04anonymousza31142ze3z87_142:
												if (NULLP(BgL_l1016z00_141))
													{	/* Tools/dsssl.scm 142 */
														BgL_test1428z00_578 = ((bool_t) 0);
													}
												else
													{	/* Tools/dsssl.scm 142 */
														bool_t BgL__ortest_1019z00_144;

														{	/* Tools/dsssl.scm 142 */
															obj_t BgL_arg1145z00_146;

															BgL_arg1145z00_146 =
																CAR(((obj_t) BgL_l1016z00_141));
															BgL__ortest_1019z00_144 =
																BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
																(BgL_arg1145z00_146);
														}
														if (BgL__ortest_1019z00_144)
															{	/* Tools/dsssl.scm 142 */
																BgL_test1428z00_578 = BgL__ortest_1019z00_144;
															}
														else
															{	/* Tools/dsssl.scm 142 */
																obj_t BgL_arg1143z00_145;

																BgL_arg1143z00_145 =
																	CDR(((obj_t) BgL_l1016z00_141));
																{
																	obj_t BgL_l1016z00_589;

																	BgL_l1016z00_589 = BgL_arg1143z00_145;
																	BgL_l1016z00_141 = BgL_l1016z00_589;
																	goto BgL_zc3z04anonymousza31142ze3z87_142;
																}
															}
													}
											}
										}
										if (BgL_test1428z00_578)
											{	/* Tools/dsssl.scm 142 */
												return
													BGl_zd2zd2zz__r4_numbers_6_5z00(BINT(
														(BgL_iz00_115 + 1L)), BNIL);
											}
										else
											{	/* Tools/dsssl.scm 142 */
												return BINT(BgL_iz00_115);
											}
									}
								else
									{	/* Tools/dsssl.scm 141 */
										return
											BGl_zd2zd2zz__r4_numbers_6_5z00(BINT(
												(BgL_iz00_115 + 1L)), BNIL);
									}
							}
					}
			}
		}

	}



/* &dsssl-arity */
	obj_t BGl_z62dssslzd2arityzb0zztools_dssslz00(obj_t BgL_envz00_393,
		obj_t BgL_argsz00_394, obj_t BgL_optimz00_395)
	{
		{	/* Tools/dsssl.scm 132 */
			return
				BGl_dssslzd2arityzd2zztools_dssslz00(BgL_argsz00_394,
				CBOOL(BgL_optimz00_395));
		}

	}



/* dsssl-defaulted-formal? */
	BGL_EXPORTED_DEF obj_t
		BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00(obj_t BgL_objz00_12)
	{
		{	/* Tools/dsssl.scm 177 */
			if (PAIRP(BgL_objz00_12))
				{	/* Tools/dsssl.scm 178 */
					obj_t BgL_cdrzd2105zd2_198;

					BgL_cdrzd2105zd2_198 = CDR(BgL_objz00_12);
					if (PAIRP(BgL_cdrzd2105zd2_198))
						{	/* Tools/dsssl.scm 178 */
							return BBOOL(NULLP(CDR(BgL_cdrzd2105zd2_198)));
						}
					else
						{	/* Tools/dsssl.scm 178 */
							return BFALSE;
						}
				}
			else
				{	/* Tools/dsssl.scm 178 */
					return BFALSE;
				}
		}

	}



/* &dsssl-defaulted-formal? */
	obj_t BGl_z62dssslzd2defaultedzd2formalzf3z91zztools_dssslz00(obj_t
		BgL_envz00_396, obj_t BgL_objz00_397)
	{
		{	/* Tools/dsssl.scm 177 */
			return
				BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00(BgL_objz00_397);
		}

	}



/* dsssl-default-formal */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2defaultzd2formalz00zztools_dssslz00(obj_t
		BgL_objz00_13)
	{
		{	/* Tools/dsssl.scm 189 */
			return CAR(((obj_t) BgL_objz00_13));
		}

	}



/* &dsssl-default-formal */
	obj_t BGl_z62dssslzd2defaultzd2formalz62zztools_dssslz00(obj_t BgL_envz00_398,
		obj_t BgL_objz00_399)
	{
		{	/* Tools/dsssl.scm 189 */
			return BGl_dssslzd2defaultzd2formalz00zztools_dssslz00(BgL_objz00_399);
		}

	}



/* dsssl-find-first-formal */
	BGL_EXPORTED_DEF obj_t
		BGl_dssslzd2findzd2firstzd2formalzd2zztools_dssslz00(obj_t BgL_argsz00_14)
	{
		{	/* Tools/dsssl.scm 195 */
		BGl_dssslzd2findzd2firstzd2formalzd2zztools_dssslz00:
			if (NULLP(BgL_argsz00_14))
				{	/* Tools/dsssl.scm 197 */
					return BFALSE;
				}
			else
				{	/* Tools/dsssl.scm 197 */
					if (PAIRP(BgL_argsz00_14))
						{	/* Tools/dsssl.scm 199 */
							if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
									(BgL_argsz00_14)))
								{
									obj_t BgL_argsz00_618;

									BgL_argsz00_618 = CDR(BgL_argsz00_14);
									BgL_argsz00_14 = BgL_argsz00_618;
									goto BGl_dssslzd2findzd2firstzd2formalzd2zztools_dssslz00;
								}
							else
								{	/* Tools/dsssl.scm 203 */
									if (CBOOL(BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00
											(CAR(BgL_argsz00_14))))
										{	/* Tools/dsssl.scm 205 */
											return CAR(CAR(BgL_argsz00_14));
										}
									else
										{	/* Tools/dsssl.scm 207 */
											bool_t BgL_test1437z00_626;

											{	/* Tools/dsssl.scm 207 */
												obj_t BgL_tmpz00_627;

												BgL_tmpz00_627 = CAR(BgL_argsz00_14);
												BgL_test1437z00_626 = SYMBOLP(BgL_tmpz00_627);
											}
											if (BgL_test1437z00_626)
												{	/* Tools/dsssl.scm 207 */
													return CAR(BgL_argsz00_14);
												}
											else
												{	/* Tools/dsssl.scm 207 */
													return
														BGl_internalzd2errorzd2zztools_errorz00
														(BGl_string1375z00zztools_dssslz00,
														BGl_string1376z00zztools_dssslz00, BgL_argsz00_14);
												}
										}
								}
						}
					else
						{	/* Tools/dsssl.scm 199 */
							return
								BGl_internalzd2errorzd2zztools_errorz00
								(BGl_string1375z00zztools_dssslz00,
								BGl_string1376z00zztools_dssslz00, BgL_argsz00_14);
						}
				}
		}

	}



/* &dsssl-find-first-formal */
	obj_t BGl_z62dssslzd2findzd2firstzd2formalzb0zztools_dssslz00(obj_t
		BgL_envz00_400, obj_t BgL_argsz00_401)
	{
		{	/* Tools/dsssl.scm 195 */
			return
				BGl_dssslzd2findzd2firstzd2formalzd2zztools_dssslz00(BgL_argsz00_401);
		}

	}



/* dsssl-args*->args-list */
	BGL_EXPORTED_DEF obj_t
		BGl_dssslzd2argsza2zd2ze3argszd2listz93zztools_dssslz00(obj_t BgL_expz00_15)
	{
		{	/* Tools/dsssl.scm 218 */
		BGl_dssslzd2argsza2zd2ze3argszd2listz93zztools_dssslz00:
			if (NULLP(BgL_expz00_15))
				{	/* Tools/dsssl.scm 220 */
					return BNIL;
				}
			else
				{	/* Tools/dsssl.scm 220 */
					if (PAIRP(BgL_expz00_15))
						{	/* Tools/dsssl.scm 222 */
							if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
									(BgL_expz00_15)))
								{	/* Tools/dsssl.scm 225 */
									obj_t BgL_argz00_220;

									BgL_argz00_220 =
										BGl_dssslzd2findzd2firstzd2formalzd2zztools_dssslz00(CDR
										(BgL_expz00_15));
									if (CBOOL(BgL_argz00_220))
										{	/* Tools/dsssl.scm 227 */
											obj_t BgL_list1240z00_221;

											BgL_list1240z00_221 =
												MAKE_YOUNG_PAIR(BgL_argz00_220, BNIL);
											return BgL_list1240z00_221;
										}
									else
										{	/* Tools/dsssl.scm 226 */
											return BNIL;
										}
								}
							else
								{	/* Tools/dsssl.scm 224 */
									if (CBOOL(BGl_dssslzd2defaultedzd2formalzf3zf3zztools_dssslz00
											(CAR(BgL_expz00_15))))
										{
											obj_t BgL_expz00_650;

											BgL_expz00_650 = CDR(BgL_expz00_15);
											BgL_expz00_15 = BgL_expz00_650;
											goto
												BGl_dssslzd2argsza2zd2ze3argszd2listz93zztools_dssslz00;
										}
									else
										{	/* Tools/dsssl.scm 229 */
											return
												MAKE_YOUNG_PAIR(CAR(BgL_expz00_15),
												BGl_dssslzd2argsza2zd2ze3argszd2listz93zztools_dssslz00
												(CDR(BgL_expz00_15)));
										}
								}
						}
					else
						{	/* Tools/dsssl.scm 223 */
							obj_t BgL_list1285z00_231;

							BgL_list1285z00_231 = MAKE_YOUNG_PAIR(BgL_expz00_15, BNIL);
							return BgL_list1285z00_231;
						}
				}
		}

	}



/* &dsssl-args*->args-list */
	obj_t BGl_z62dssslzd2argsza2zd2ze3argszd2listzf1zztools_dssslz00(obj_t
		BgL_envz00_402, obj_t BgL_expz00_403)
	{
		{	/* Tools/dsssl.scm 218 */
			return
				BGl_dssslzd2argsza2zd2ze3argszd2listz93zztools_dssslz00(BgL_expz00_403);
		}

	}



/* dsssl-formals */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2formalszd2zztools_dssslz00(obj_t
		BgL_argsz00_16)
	{
		{	/* Tools/dsssl.scm 237 */
			{
				obj_t BgL_argsz00_233;

				BgL_argsz00_233 = BgL_argsz00_16;
			BgL_zc3z04anonymousza31286ze3z87_234:
				if (PAIRP(BgL_argsz00_233))
					{	/* Tools/dsssl.scm 240 */
						if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
								(BgL_argsz00_233)))
							{	/* Tools/dsssl.scm 242 */
								return BgL_argsz00_233;
							}
						else
							{
								obj_t BgL_argsz00_663;

								BgL_argsz00_663 = CDR(BgL_argsz00_233);
								BgL_argsz00_233 = BgL_argsz00_663;
								goto BgL_zc3z04anonymousza31286ze3z87_234;
							}
					}
				else
					{	/* Tools/dsssl.scm 240 */
						return BNIL;
					}
			}
		}

	}



/* &dsssl-formals */
	obj_t BGl_z62dssslzd2formalszb0zztools_dssslz00(obj_t BgL_envz00_404,
		obj_t BgL_argsz00_405)
	{
		{	/* Tools/dsssl.scm 237 */
			return BGl_dssslzd2formalszd2zztools_dssslz00(BgL_argsz00_405);
		}

	}



/* dsssl-arguments */
	obj_t BGl_dssslzd2argumentszd2zztools_dssslz00(obj_t BgL_keyz00_17,
		obj_t BgL_argsz00_18)
	{
		{	/* Tools/dsssl.scm 250 */
			{
				obj_t BgL_argsz00_242;

				BgL_argsz00_242 = BgL_argsz00_18;
			BgL_zc3z04anonymousza31308ze3z87_243:
				if (PAIRP(BgL_argsz00_242))
					{	/* Tools/dsssl.scm 253 */
						if ((CAR(BgL_argsz00_242) == BgL_keyz00_17))
							{	/* Tools/dsssl.scm 256 */
								obj_t BgL_g1013z00_247;

								BgL_g1013z00_247 = CDR(BgL_argsz00_242);
								{
									obj_t BgL_argsz00_250;
									obj_t BgL_rz00_251;

									BgL_argsz00_250 = BgL_g1013z00_247;
									BgL_rz00_251 = BNIL;
								BgL_zc3z04anonymousza31312ze3z87_252:
									if (NULLP(BgL_argsz00_250))
										{	/* Tools/dsssl.scm 259 */
											return bgl_reverse_bang(BgL_rz00_251);
										}
									else
										{	/* Tools/dsssl.scm 259 */
											if (PAIRP(BgL_argsz00_250))
												{	/* Tools/dsssl.scm 261 */
													if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
															(BgL_argsz00_250)))
														{	/* Tools/dsssl.scm 263 */
															return BNIL;
														}
													else
														{	/* Tools/dsssl.scm 265 */
															bool_t BgL_test1450z00_680;

															{	/* Tools/dsssl.scm 265 */
																obj_t BgL_tmpz00_681;

																BgL_tmpz00_681 = CAR(BgL_argsz00_250);
																BgL_test1450z00_680 = PAIRP(BgL_tmpz00_681);
															}
															if (BgL_test1450z00_680)
																{	/* Tools/dsssl.scm 266 */
																	obj_t BgL_arg1319z00_259;
																	obj_t BgL_arg1320z00_260;

																	BgL_arg1319z00_259 = CDR(BgL_argsz00_250);
																	BgL_arg1320z00_260 =
																		MAKE_YOUNG_PAIR(CAR(BgL_argsz00_250),
																		BgL_rz00_251);
																	{
																		obj_t BgL_rz00_688;
																		obj_t BgL_argsz00_687;

																		BgL_argsz00_687 = BgL_arg1319z00_259;
																		BgL_rz00_688 = BgL_arg1320z00_260;
																		BgL_rz00_251 = BgL_rz00_688;
																		BgL_argsz00_250 = BgL_argsz00_687;
																		goto BgL_zc3z04anonymousza31312ze3z87_252;
																	}
																}
															else
																{	/* Tools/dsssl.scm 268 */
																	obj_t BgL_arg1322z00_262;
																	obj_t BgL_arg1323z00_263;

																	BgL_arg1322z00_262 = CDR(BgL_argsz00_250);
																	{	/* Tools/dsssl.scm 268 */
																		obj_t BgL_arg1325z00_264;

																		{	/* Tools/dsssl.scm 268 */
																			obj_t BgL_arg1326z00_265;

																			BgL_arg1326z00_265 = CAR(BgL_argsz00_250);
																			{	/* Tools/dsssl.scm 268 */
																				obj_t BgL_list1327z00_266;

																				{	/* Tools/dsssl.scm 268 */
																					obj_t BgL_arg1328z00_267;

																					BgL_arg1328z00_267 =
																						MAKE_YOUNG_PAIR(BFALSE, BNIL);
																					BgL_list1327z00_266 =
																						MAKE_YOUNG_PAIR(BgL_arg1326z00_265,
																						BgL_arg1328z00_267);
																				}
																				BgL_arg1325z00_264 =
																					BgL_list1327z00_266;
																			}
																		}
																		BgL_arg1323z00_263 =
																			MAKE_YOUNG_PAIR(BgL_arg1325z00_264,
																			BgL_rz00_251);
																	}
																	{
																		obj_t BgL_rz00_695;
																		obj_t BgL_argsz00_694;

																		BgL_argsz00_694 = BgL_arg1322z00_262;
																		BgL_rz00_695 = BgL_arg1323z00_263;
																		BgL_rz00_251 = BgL_rz00_695;
																		BgL_argsz00_250 = BgL_argsz00_694;
																		goto BgL_zc3z04anonymousza31312ze3z87_252;
																	}
																}
														}
												}
											else
												{	/* Tools/dsssl.scm 261 */
													return BNIL;
												}
										}
								}
							}
						else
							{	/* Tools/dsssl.scm 255 */
								if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
										(BgL_argsz00_242)))
									{	/* Tools/dsssl.scm 269 */
										return BNIL;
									}
								else
									{
										obj_t BgL_argsz00_699;

										BgL_argsz00_699 = CDR(BgL_argsz00_242);
										BgL_argsz00_242 = BgL_argsz00_699;
										goto BgL_zc3z04anonymousza31308ze3z87_243;
									}
							}
					}
				else
					{	/* Tools/dsssl.scm 253 */
						return BNIL;
					}
			}
		}

	}



/* dsssl-optionals */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2optionalszd2zztools_dssslz00(obj_t
		BgL_argsz00_19)
	{
		{	/* Tools/dsssl.scm 277 */
			return
				BGl_dssslzd2argumentszd2zztools_dssslz00((BOPTIONAL), BgL_argsz00_19);
		}

	}



/* &dsssl-optionals */
	obj_t BGl_z62dssslzd2optionalszb0zztools_dssslz00(obj_t BgL_envz00_406,
		obj_t BgL_argsz00_407)
	{
		{	/* Tools/dsssl.scm 277 */
			return BGl_dssslzd2optionalszd2zztools_dssslz00(BgL_argsz00_407);
		}

	}



/* dsssl-keys */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2keyszd2zztools_dssslz00(obj_t
		BgL_argsz00_20)
	{
		{	/* Tools/dsssl.scm 283 */
			return
				BGl_dssslzd2keyzd2argszd2sortzd2zztools_dssslz00
				(BGl_dssslzd2argumentszd2zztools_dssslz00((BKEY), BgL_argsz00_20));
		}

	}



/* &dsssl-keys */
	obj_t BGl_z62dssslzd2keyszb0zztools_dssslz00(obj_t BgL_envz00_408,
		obj_t BgL_argsz00_409)
	{
		{	/* Tools/dsssl.scm 283 */
			return BGl_dssslzd2keyszd2zztools_dssslz00(BgL_argsz00_409);
		}

	}



/* dsssl-key-args-sort */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2keyzd2argszd2sortzd2zztools_dssslz00(obj_t
		BgL_argsz00_21)
	{
		{	/* Tools/dsssl.scm 289 */
			return
				BGl_sortz00zz__r4_vectors_6_8z00(BgL_argsz00_21,
				BGl_proc1377z00zztools_dssslz00);
		}

	}



/* &dsssl-key-args-sort */
	obj_t BGl_z62dssslzd2keyzd2argszd2sortzb0zztools_dssslz00(obj_t
		BgL_envz00_411, obj_t BgL_argsz00_412)
	{
		{	/* Tools/dsssl.scm 289 */
			return BGl_dssslzd2keyzd2argszd2sortzd2zztools_dssslz00(BgL_argsz00_412);
		}

	}



/* &<@anonymous:1344> */
	obj_t BGl_z62zc3z04anonymousza31344ze3ze5zztools_dssslz00(obj_t
		BgL_envz00_413, obj_t BgL_s1z00_414, obj_t BgL_s2z00_415)
	{
		{	/* Tools/dsssl.scm 291 */
			{	/* Tools/dsssl.scm 292 */
				bool_t BgL_tmpz00_708;

				{	/* Tools/dsssl.scm 292 */
					obj_t BgL_arg1346z00_429;
					obj_t BgL_arg1348z00_430;

					{	/* Tools/dsssl.scm 292 */
						obj_t BgL_arg1349z00_431;

						BgL_arg1349z00_431 = CAR(((obj_t) BgL_s1z00_414));
						{	/* Tools/dsssl.scm 292 */
							obj_t BgL_arg1455z00_432;

							BgL_arg1455z00_432 =
								SYMBOL_TO_STRING(((obj_t) BgL_arg1349z00_431));
							BgL_arg1346z00_429 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_432);
						}
					}
					{	/* Tools/dsssl.scm 293 */
						obj_t BgL_arg1351z00_433;

						BgL_arg1351z00_433 = CAR(((obj_t) BgL_s2z00_415));
						{	/* Tools/dsssl.scm 293 */
							obj_t BgL_arg1455z00_434;

							BgL_arg1455z00_434 =
								SYMBOL_TO_STRING(((obj_t) BgL_arg1351z00_433));
							BgL_arg1348z00_430 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_434);
						}
					}
					BgL_tmpz00_708 =
						bigloo_string_lt(BgL_arg1346z00_429, BgL_arg1348z00_430);
				}
				return BBOOL(BgL_tmpz00_708);
			}
		}

	}



/* dsssl-before-dsssl */
	BGL_EXPORTED_DEF obj_t BGl_dssslzd2beforezd2dssslz00zztools_dssslz00(obj_t
		BgL_argsz00_22)
	{
		{	/* Tools/dsssl.scm 301 */
			{
				obj_t BgL_argsz00_289;
				obj_t BgL_resz00_290;

				BgL_argsz00_289 = BgL_argsz00_22;
				BgL_resz00_290 = BNIL;
			BgL_zc3z04anonymousza31352ze3z87_291:
				{	/* Tools/dsssl.scm 305 */
					bool_t BgL_test1452z00_721;

					{	/* Tools/dsssl.scm 305 */
						obj_t BgL_arg1371z00_297;

						BgL_arg1371z00_297 = CAR(((obj_t) BgL_argsz00_289));
						BgL_test1452z00_721 =
							BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00
							(BgL_arg1371z00_297);
					}
					if (BgL_test1452z00_721)
						{	/* Tools/dsssl.scm 305 */
							return bgl_reverse_bang(BgL_resz00_290);
						}
					else
						{	/* Tools/dsssl.scm 308 */
							obj_t BgL_arg1364z00_294;
							obj_t BgL_arg1367z00_295;

							BgL_arg1364z00_294 = CDR(((obj_t) BgL_argsz00_289));
							{	/* Tools/dsssl.scm 308 */
								obj_t BgL_arg1370z00_296;

								BgL_arg1370z00_296 = CAR(((obj_t) BgL_argsz00_289));
								BgL_arg1367z00_295 =
									MAKE_YOUNG_PAIR(BgL_arg1370z00_296, BgL_resz00_290);
							}
							{
								obj_t BgL_resz00_732;
								obj_t BgL_argsz00_731;

								BgL_argsz00_731 = BgL_arg1364z00_294;
								BgL_resz00_732 = BgL_arg1367z00_295;
								BgL_resz00_290 = BgL_resz00_732;
								BgL_argsz00_289 = BgL_argsz00_731;
								goto BgL_zc3z04anonymousza31352ze3z87_291;
							}
						}
				}
			}
		}

	}



/* &dsssl-before-dsssl */
	obj_t BGl_z62dssslzd2beforezd2dssslz62zztools_dssslz00(obj_t BgL_envz00_416,
		obj_t BgL_argsz00_417)
	{
		{	/* Tools/dsssl.scm 301 */
			return BGl_dssslzd2beforezd2dssslz00zztools_dssslz00(BgL_argsz00_417);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_dssslz00(void)
	{
		{	/* Tools/dsssl.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_dssslz00(void)
	{
		{	/* Tools/dsssl.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_dssslz00(void)
	{
		{	/* Tools/dsssl.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztools_dssslz00(void)
	{
		{	/* Tools/dsssl.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1378z00zztools_dssslz00));
		}

	}

#ifdef __cplusplus
}
#endif
