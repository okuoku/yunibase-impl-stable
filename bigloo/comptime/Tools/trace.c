/*===========================================================================*/
/*   (Tools/trace.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/trace.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_TRACE_TYPE_DEFINITIONS
#define BGL_TOOLS_TRACE_TYPE_DEFINITIONS
#endif													// BGL_TOOLS_TRACE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_za2marginsza2z00zztools_tracez00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zztools_tracez00 = BUNSPEC;
	static long BGl_za2levelza2z00zztools_tracez00 = 0L;
	static obj_t BGl_z62tracezd2tabzb0zztools_tracez00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zztools_tracez00(void);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2satisfyzf3z21zztools_tracez00(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zztools_tracez00(void);
	static obj_t BGl_z62startzd2tracezb0zztools_tracez00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zztools_tracez00(void);
	BGL_EXPORTED_DECL obj_t BGl_tracezd2tabzd2zztools_tracez00(int);
	static bool_t BGl_za2tracezd2modeza2zd2zztools_tracez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztools_tracez00(void);
	BGL_EXPORTED_DECL obj_t BGl_printzd2tracezd2zztools_tracez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__datez00(long, char *);
	static obj_t BGl_cnstzd2initzd2zztools_tracez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_tracez00(void);
	static obj_t BGl_za2tracezd2passza2zd2zztools_tracez00 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zztools_tracez00(void);
	static obj_t BGl_gczd2rootszd2initz00zztools_tracez00(void);
	BGL_EXPORTED_DECL obj_t BGl_stopzd2tracezd2zztools_tracez00(void);
	static obj_t BGl_z62printzd2tracezb0zztools_tracez00(obj_t, obj_t);
	static obj_t BGl_z62stopzd2tracezb0zztools_tracez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_startzd2tracezd2zztools_tracez00(obj_t, obj_t);
	static obj_t BGl_z62tracezd2satisfyzf3z43zztools_tracez00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2tracezd2portza2zd2zztools_tracez00 = BUNSPEC;
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stopzd2tracezd2envz00zztools_tracez00,
		BgL_bgl_za762stopza7d2traceza71045za7,
		BGl_z62stopzd2tracezb0zztools_tracez00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_startzd2tracezd2envz00zztools_tracez00,
		BgL_bgl_za762startza7d2trace1046z00,
		BGl_z62startzd2tracezb0zztools_tracez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2tabzd2envz00zztools_tracez00,
		BgL_bgl_za762traceza7d2tabza7b1047za7,
		BGl_z62tracezd2tabzb0zztools_tracez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_printzd2tracezd2envz00zztools_tracez00,
		BgL_bgl_za762printza7d2trace1048z00, va_generic_entry,
		BGl_z62printzd2tracezb0zztools_tracez00, BUNSPEC, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tracezd2satisfyzf3zd2envzf3zztools_tracez00,
		BgL_bgl_za762traceza7d2satis1049z00,
		BGl_z62tracezd2satisfyzf3z43zztools_tracez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1041z00zztools_tracez00,
		BgL_bgl_string1041za700za7za7t1050za7, "tools_trace", 11);
	      DEFINE_STRING(BGl_string1042z00zztools_tracez00,
		BgL_bgl_string1042za700za7za7t1051za7,
		"#(\"\" \" \" \"  \" \"   \" \"    \" \"     \" \"      \" \"       \" \"        \" \"         \" \"          \") none ",
		96);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2marginsza2z00zztools_tracez00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztools_tracez00));
		     ADD_ROOT((void *) (&BGl_za2tracezd2passza2zd2zztools_tracez00));
		     ADD_ROOT((void *) (&BGl_za2tracezd2portza2zd2zztools_tracez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long
		BgL_checksumz00_92, char *BgL_fromz00_93)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_tracez00))
				{
					BGl_requirezd2initializa7ationz75zztools_tracez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_tracez00();
					BGl_libraryzd2moduleszd2initz00zztools_tracez00();
					BGl_cnstzd2initzd2zztools_tracez00();
					BGl_importedzd2moduleszd2initz00zztools_tracez00();
					return BGl_toplevelzd2initzd2zztools_tracez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 15 */
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_trace");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tools_trace");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "tools_trace");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "tools_trace");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_trace");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tools_trace");
			BGl_modulezd2initializa7ationz75zz__datez00(0L, "tools_trace");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"tools_trace");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 15 */
			{	/* Tools/trace.scm 15 */
				obj_t BgL_cportz00_81;

				{	/* Tools/trace.scm 15 */
					obj_t BgL_stringz00_88;

					BgL_stringz00_88 = BGl_string1042z00zztools_tracez00;
					{	/* Tools/trace.scm 15 */
						obj_t BgL_startz00_89;

						BgL_startz00_89 = BINT(0L);
						{	/* Tools/trace.scm 15 */
							obj_t BgL_endz00_90;

							BgL_endz00_90 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_88)));
							{	/* Tools/trace.scm 15 */

								BgL_cportz00_81 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_88, BgL_startz00_89, BgL_endz00_90);
				}}}}
				{
					long BgL_iz00_82;

					BgL_iz00_82 = 1L;
				BgL_loopz00_83:
					if ((BgL_iz00_82 == -1L))
						{	/* Tools/trace.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tools/trace.scm 15 */
							{	/* Tools/trace.scm 15 */
								obj_t BgL_arg1044z00_84;

								{	/* Tools/trace.scm 15 */

									{	/* Tools/trace.scm 15 */
										obj_t BgL_locationz00_86;

										BgL_locationz00_86 = BBOOL(((bool_t) 0));
										{	/* Tools/trace.scm 15 */

											BgL_arg1044z00_84 =
												BGl_readz00zz__readerz00(BgL_cportz00_81,
												BgL_locationz00_86);
										}
									}
								}
								{	/* Tools/trace.scm 15 */
									int BgL_tmpz00_119;

									BgL_tmpz00_119 = (int) (BgL_iz00_82);
									CNST_TABLE_SET(BgL_tmpz00_119, BgL_arg1044z00_84);
							}}
							{	/* Tools/trace.scm 15 */
								int BgL_auxz00_87;

								BgL_auxz00_87 = (int) ((BgL_iz00_82 - 1L));
								{
									long BgL_iz00_124;

									BgL_iz00_124 = (long) (BgL_auxz00_87);
									BgL_iz00_82 = BgL_iz00_124;
									goto BgL_loopz00_83;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 15 */
			BGl_za2tracezd2portza2zd2zztools_tracez00 = BFALSE;
			BGl_za2tracezd2passza2zd2zztools_tracez00 = CNST_TABLE_REF(0);
			BGl_za2levelza2z00zztools_tracez00 = 0L;
			BGl_za2tracezd2modeza2zd2zztools_tracez00 = ((bool_t) 0);
			return (BGl_za2marginsza2z00zztools_tracez00 =
				CNST_TABLE_REF(1), BUNSPEC);
		}

	}



/* start-trace */
	BGL_EXPORTED_DEF obj_t BGl_startzd2tracezd2zztools_tracez00(obj_t
		BgL_levelz00_3, obj_t BgL_passz00_4)
	{
		{	/* Tools/trace.scm 46 */
			return BFALSE;
		}

	}



/* &start-trace */
	obj_t BGl_z62startzd2tracezb0zztools_tracez00(obj_t BgL_envz00_70,
		obj_t BgL_levelz00_71, obj_t BgL_passz00_72)
	{
		{	/* Tools/trace.scm 46 */
			return
				BGl_startzd2tracezd2zztools_tracez00(BgL_levelz00_71, BgL_passz00_72);
		}

	}



/* stop-trace */
	BGL_EXPORTED_DEF obj_t BGl_stopzd2tracezd2zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 66 */
			return BFALSE;
		}

	}



/* &stop-trace */
	obj_t BGl_z62stopzd2tracezb0zztools_tracez00(obj_t BgL_envz00_73)
	{
		{	/* Tools/trace.scm 66 */
			return BGl_stopzd2tracezd2zztools_tracez00();
		}

	}



/* trace-satisfy? */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2satisfyzf3z21zztools_tracez00(obj_t
		BgL_passz00_5, obj_t BgL_levelz00_6)
	{
		{	/* Tools/trace.scm 74 */
			return BFALSE;
		}

	}



/* &trace-satisfy? */
	obj_t BGl_z62tracezd2satisfyzf3z43zztools_tracez00(obj_t BgL_envz00_74,
		obj_t BgL_passz00_75, obj_t BgL_levelz00_76)
	{
		{	/* Tools/trace.scm 74 */
			return
				BGl_tracezd2satisfyzf3z21zztools_tracez00(BgL_passz00_75,
				BgL_levelz00_76);
		}

	}



/* print-trace */
	BGL_EXPORTED_DEF obj_t BGl_printzd2tracezd2zztools_tracez00(obj_t
		BgL_expz00_7)
	{
		{	/* Tools/trace.scm 82 */
			return BFALSE;
		}

	}



/* &print-trace */
	obj_t BGl_z62printzd2tracezb0zztools_tracez00(obj_t BgL_envz00_77,
		obj_t BgL_expz00_78)
	{
		{	/* Tools/trace.scm 82 */
			return BGl_printzd2tracezd2zztools_tracez00(BgL_expz00_78);
		}

	}



/* trace-tab */
	BGL_EXPORTED_DEF obj_t BGl_tracezd2tabzd2zztools_tracez00(int BgL_lenz00_8)
	{
		{	/* Tools/trace.scm 90 */
			return BFALSE;
		}

	}



/* &trace-tab */
	obj_t BGl_z62tracezd2tabzb0zztools_tracez00(obj_t BgL_envz00_79,
		obj_t BgL_lenz00_80)
	{
		{	/* Tools/trace.scm 90 */
			return BGl_tracezd2tabzd2zztools_tracez00(CINT(BgL_lenz00_80));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztools_tracez00(void)
	{
		{	/* Tools/trace.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1041z00zztools_tracez00));
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1041z00zztools_tracez00));
		}

	}

#ifdef __cplusplus
}
#endif
