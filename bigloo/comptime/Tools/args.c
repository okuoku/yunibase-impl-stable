/*===========================================================================*/
/*   (Tools/args.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/args.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_ARGS_TYPE_DEFINITIONS
#define BGL_TOOLS_ARGS_TYPE_DEFINITIONS
#endif													// BGL_TOOLS_ARGS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62argsza2zd2ze3argszd2listz23zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztools_argsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_zb2zd2arityz60zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_makezd2argszd2nameze70ze7zztools_argsz00(long);
	static obj_t BGl_z62foreignzd2arityzb0zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_z62globalzd2arityzb0zztools_argsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2nzd2protoz00zztools_argsz00(obj_t);
	static obj_t BGl_genericzd2initzd2zztools_argsz00(void);
	static obj_t BGl_objectzd2initzd2zztools_argsz00(void);
	BGL_IMPORT bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_localzd2arityzd2zztools_argsz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztools_argsz00(void);
	BGL_EXPORTED_DECL obj_t BGl_argszd2listzd2ze3argsza2z41zztools_argsz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_foreignzd2arityzd2zztools_argsz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_dssslzd2arityzd2za7erozf3z54zztools_argsz00(int,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_soundzd2arityzf3z21zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2arityzb0zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_z62argszd2listzd2ze3argsza2z23zztools_argsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zb2zd2arityz02zztools_argsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__dssslz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62makezd2nzd2protoz62zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_z62makezd2argszd2listz62zztools_argsz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_cnstzd2initzd2zztools_argsz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_argsz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_EXPORTED_DECL long BGl_globalzd2arityzd2zztools_argsz00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zztools_argsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztools_argsz00(void);
	static obj_t BGl_makezd2vazd2protoze70ze7zztools_argsz00(obj_t, long);
	BGL_EXPORTED_DECL obj_t BGl_makezd2argszd2listz00zztools_argsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(obj_t);
	extern obj_t BGl_dssslzd2arityzd2zztools_dssslz00(obj_t, bool_t);
	static obj_t BGl_loopze70ze7zztools_argsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_loopze71ze7zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_z62dssslzd2arityzd2za7erozf3z36zztools_argsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62soundzd2arityzf3z43zztools_argsz00(obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(obj_t);
	static obj_t BGl_makezd2fxzd2protoze70ze7zztools_argsz00(obj_t, long);
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_zb2zd2arityzd2envzb2zztools_argsz00,
		BgL_bgl_za762za7b2za7d2arityza701132z00,
		BGl_z62zb2zd2arityz02zztools_argsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_soundzd2arityzf3zd2envzf3zztools_argsz00,
		BgL_bgl_za762soundza7d2arity1133z00,
		BGl_z62soundzd2arityzf3z43zztools_argsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_argszd2listzd2ze3argsza2zd2envz93zztools_argsz00,
		BgL_bgl_za762argsza7d2listza7d1134za7,
		BGl_z62argszd2listzd2ze3argsza2z23zztools_argsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2nzd2protozd2envzd2zztools_argsz00,
		BgL_bgl_za762makeza7d2nza7d2pr1135za7,
		BGl_z62makezd2nzd2protoz62zztools_argsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2arityzd2envz00zztools_argsz00,
		BgL_bgl_za762localza7d2arity1136z00, BGl_z62localzd2arityzb0zztools_argsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_foreignzd2arityzd2envz00zztools_argsz00,
		BgL_bgl_za762foreignza7d2ari1137z00,
		BGl_z62foreignzd2arityzb0zztools_argsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2arityzd2envz00zztools_argsz00,
		BgL_bgl_za762globalza7d2arit1138z00,
		BGl_z62globalzd2arityzb0zztools_argsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_argsza2zd2ze3argszd2listzd2envz93zztools_argsz00,
		BgL_bgl_za762argsza7a2za7d2za7e31139z00,
		BGl_z62argsza2zd2ze3argszd2listz23zztools_argsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dssslzd2arityzd2za7erozf3zd2envz86zztools_argsz00,
		BgL_bgl_za762dssslza7d2arity1140z00,
		BGl_z62dssslzd2arityzd2za7erozf3z36zztools_argsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2argszd2listzd2envzd2zztools_argsz00,
		BgL_bgl_za762makeza7d2argsza7d1141za7,
		BGl_z62makezd2argszd2listz62zztools_argsz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1127z00zztools_argsz00,
		BgL_bgl_string1127za700za7za7t1142za7,
		"DSSSL arguments not allowed in foreign", 38);
	      DEFINE_STRING(BGl_string1128z00zztools_argsz00,
		BgL_bgl_string1128za700za7za7t1143za7, "A", 1);
	      DEFINE_STRING(BGl_string1129z00zztools_argsz00,
		BgL_bgl_string1129za700za7za7t1144za7, "tools_args", 10);
	      DEFINE_STRING(BGl_string1130z00zztools_argsz00,
		BgL_bgl_string1130za700za7za7t1145za7, "foreign-arity ", 14);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztools_argsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long
		BgL_checksumz00_225, char *BgL_fromz00_226)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_argsz00))
				{
					BGl_requirezd2initializa7ationz75zztools_argsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_argsz00();
					BGl_libraryzd2moduleszd2initz00zztools_argsz00();
					BGl_cnstzd2initzd2zztools_argsz00();
					BGl_importedzd2moduleszd2initz00zztools_argsz00();
					return BGl_methodzd2initzd2zztools_argsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_argsz00(void)
	{
		{	/* Tools/args.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "tools_args");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_args");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tools_args");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "tools_args");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tools_args");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_args");
			BGl_modulezd2initializa7ationz75zz__dssslz00(0L, "tools_args");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "tools_args");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "tools_args");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztools_argsz00(void)
	{
		{	/* Tools/args.scm 15 */
			{	/* Tools/args.scm 15 */
				obj_t BgL_cportz00_214;

				{	/* Tools/args.scm 15 */
					obj_t BgL_stringz00_221;

					BgL_stringz00_221 = BGl_string1130z00zztools_argsz00;
					{	/* Tools/args.scm 15 */
						obj_t BgL_startz00_222;

						BgL_startz00_222 = BINT(0L);
						{	/* Tools/args.scm 15 */
							obj_t BgL_endz00_223;

							BgL_endz00_223 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_221)));
							{	/* Tools/args.scm 15 */

								BgL_cportz00_214 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_221, BgL_startz00_222, BgL_endz00_223);
				}}}}
				{
					long BgL_iz00_215;

					BgL_iz00_215 = 0L;
				BgL_loopz00_216:
					if ((BgL_iz00_215 == -1L))
						{	/* Tools/args.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tools/args.scm 15 */
							{	/* Tools/args.scm 15 */
								obj_t BgL_arg1131z00_217;

								{	/* Tools/args.scm 15 */

									{	/* Tools/args.scm 15 */
										obj_t BgL_locationz00_219;

										BgL_locationz00_219 = BBOOL(((bool_t) 0));
										{	/* Tools/args.scm 15 */

											BgL_arg1131z00_217 =
												BGl_readz00zz__readerz00(BgL_cportz00_214,
												BgL_locationz00_219);
										}
									}
								}
								{	/* Tools/args.scm 15 */
									int BgL_tmpz00_253;

									BgL_tmpz00_253 = (int) (BgL_iz00_215);
									CNST_TABLE_SET(BgL_tmpz00_253, BgL_arg1131z00_217);
							}}
							{	/* Tools/args.scm 15 */
								int BgL_auxz00_220;

								BgL_auxz00_220 = (int) ((BgL_iz00_215 - 1L));
								{
									long BgL_iz00_258;

									BgL_iz00_258 = (long) (BgL_auxz00_220);
									BgL_iz00_215 = BgL_iz00_258;
									goto BgL_loopz00_216;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_argsz00(void)
	{
		{	/* Tools/args.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* global-arity */
	BGL_EXPORTED_DEF long BGl_globalzd2arityzd2zztools_argsz00(obj_t
		BgL_argsz00_3)
	{
		{	/* Tools/args.scm 37 */
			{
				long BgL_iz00_28;
				obj_t BgL_az00_29;

				{	/* Tools/args.scm 38 */
					obj_t BgL_tmpz00_261;

					BgL_iz00_28 = 0L;
					BgL_az00_29 = BgL_argsz00_3;
				BgL_zc3z04anonymousza31016ze3z87_30:
					if (NULLP(BgL_az00_29))
						{	/* Tools/args.scm 41 */
							BgL_tmpz00_261 = BINT(BgL_iz00_28);
						}
					else
						{	/* Tools/args.scm 41 */
							if (PAIRP(BgL_az00_29))
								{	/* Tools/args.scm 43 */
									if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
											(BgL_az00_29)))
										{	/* Tools/args.scm 44 */
											BgL_tmpz00_261 =
												BGl_dssslzd2arityzd2zztools_dssslz00(BgL_argsz00_3,
												((bool_t) 1));
										}
									else
										{
											obj_t BgL_az00_273;
											long BgL_iz00_271;

											BgL_iz00_271 = (BgL_iz00_28 + 1L);
											BgL_az00_273 = CDR(BgL_az00_29);
											BgL_az00_29 = BgL_az00_273;
											BgL_iz00_28 = BgL_iz00_271;
											goto BgL_zc3z04anonymousza31016ze3z87_30;
										}
								}
							else
								{	/* Tools/args.scm 43 */
									BgL_tmpz00_261 = BINT(NEG((BgL_iz00_28 + 1L)));
								}
						}
					return (long) CINT(BgL_tmpz00_261);
		}}}

	}



/* &global-arity */
	obj_t BGl_z62globalzd2arityzb0zztools_argsz00(obj_t BgL_envz00_186,
		obj_t BgL_argsz00_187)
	{
		{	/* Tools/args.scm 37 */
			return BINT(BGl_globalzd2arityzd2zztools_argsz00(BgL_argsz00_187));
		}

	}



/* local-arity */
	BGL_EXPORTED_DEF long BGl_localzd2arityzd2zztools_argsz00(obj_t BgL_argsz00_4)
	{
		{	/* Tools/args.scm 59 */
			{
				long BgL_iz00_41;
				obj_t BgL_az00_42;

				{	/* Tools/args.scm 60 */
					obj_t BgL_tmpz00_281;

					BgL_iz00_41 = 0L;
					BgL_az00_42 = BgL_argsz00_4;
				BgL_zc3z04anonymousza31025ze3z87_43:
					if (NULLP(BgL_az00_42))
						{	/* Tools/args.scm 63 */
							BgL_tmpz00_281 = BINT(BgL_iz00_41);
						}
					else
						{	/* Tools/args.scm 63 */
							if (PAIRP(BgL_az00_42))
								{	/* Tools/args.scm 65 */
									if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
											(BgL_az00_42)))
										{	/* Tools/args.scm 66 */
											BgL_tmpz00_281 =
												BGl_dssslzd2arityzd2zztools_dssslz00(BgL_argsz00_4,
												((bool_t) 0));
										}
									else
										{
											obj_t BgL_az00_293;
											long BgL_iz00_291;

											BgL_iz00_291 = (BgL_iz00_41 + 1L);
											BgL_az00_293 = CDR(BgL_az00_42);
											BgL_az00_42 = BgL_az00_293;
											BgL_iz00_41 = BgL_iz00_291;
											goto BgL_zc3z04anonymousza31025ze3z87_43;
										}
								}
							else
								{	/* Tools/args.scm 65 */
									BgL_tmpz00_281 = BINT(NEG((BgL_iz00_41 + 1L)));
								}
						}
					return (long) CINT(BgL_tmpz00_281);
		}}}

	}



/* &local-arity */
	obj_t BGl_z62localzd2arityzb0zztools_argsz00(obj_t BgL_envz00_188,
		obj_t BgL_argsz00_189)
	{
		{	/* Tools/args.scm 59 */
			return BINT(BGl_localzd2arityzd2zztools_argsz00(BgL_argsz00_189));
		}

	}



/* foreign-arity */
	BGL_EXPORTED_DEF long BGl_foreignzd2arityzd2zztools_argsz00(obj_t
		BgL_argsz00_5)
	{
		{	/* Tools/args.scm 81 */
			{
				long BgL_iz00_54;
				obj_t BgL_az00_55;

				{	/* Tools/args.scm 82 */
					obj_t BgL_tmpz00_301;

					BgL_iz00_54 = 0L;
					BgL_az00_55 = BgL_argsz00_5;
				BgL_zc3z04anonymousza31037ze3z87_56:
					if (NULLP(BgL_az00_55))
						{	/* Tools/args.scm 85 */
							BgL_tmpz00_301 = BINT(BgL_iz00_54);
						}
					else
						{	/* Tools/args.scm 85 */
							if (PAIRP(BgL_az00_55))
								{	/* Tools/args.scm 87 */
									if (BGl_dssslzd2namedzd2constantzf3zf3zz__dssslz00(CAR
											(BgL_az00_55)))
										{	/* Tools/args.scm 88 */
											BgL_tmpz00_301 =
												BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
												BGl_string1127z00zztools_argsz00, BgL_argsz00_5);
										}
									else
										{
											obj_t BgL_az00_314;
											long BgL_iz00_312;

											BgL_iz00_312 = (BgL_iz00_54 + 1L);
											BgL_az00_314 = CDR(BgL_az00_55);
											BgL_az00_55 = BgL_az00_314;
											BgL_iz00_54 = BgL_iz00_312;
											goto BgL_zc3z04anonymousza31037ze3z87_56;
										}
								}
							else
								{	/* Tools/args.scm 87 */
									BgL_tmpz00_301 = BINT(NEG((BgL_iz00_54 + 1L)));
								}
						}
					return (long) CINT(BgL_tmpz00_301);
		}}}

	}



/* &foreign-arity */
	obj_t BGl_z62foreignzd2arityzb0zztools_argsz00(obj_t BgL_envz00_190,
		obj_t BgL_argsz00_191)
	{
		{	/* Tools/args.scm 81 */
			return BINT(BGl_foreignzd2arityzd2zztools_argsz00(BgL_argsz00_191));
		}

	}



/* dsssl-arity-zero? */
	BGL_EXPORTED_DEF bool_t BGl_dssslzd2arityzd2za7erozf3z54zztools_argsz00(int
		BgL_arityz00_6, obj_t BgL_argsz00_7)
	{
		{	/* Tools/args.scm 99 */
			if (((long) (BgL_arityz00_6) < 0L))
				{	/* Tools/args.scm 100 */
					return NULLP(BgL_argsz00_7);
				}
			else
				{	/* Tools/args.scm 100 */
					return ((bool_t) 0);
				}
		}

	}



/* &dsssl-arity-zero? */
	obj_t BGl_z62dssslzd2arityzd2za7erozf3z36zztools_argsz00(obj_t BgL_envz00_192,
		obj_t BgL_arityz00_193, obj_t BgL_argsz00_194)
	{
		{	/* Tools/args.scm 99 */
			return
				BBOOL(BGl_dssslzd2arityzd2za7erozf3z54zztools_argsz00(CINT
					(BgL_arityz00_193), BgL_argsz00_194));
		}

	}



/* args*->args-list */
	BGL_EXPORTED_DEF obj_t BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(obj_t
		BgL_expz00_8)
	{
		{	/* Tools/args.scm 106 */
			if (NULLP(BgL_expz00_8))
				{	/* Tools/args.scm 108 */
					return BNIL;
				}
			else
				{	/* Tools/args.scm 108 */
					if (PAIRP(BgL_expz00_8))
						{	/* Tools/args.scm 110 */
							return
								MAKE_YOUNG_PAIR(CAR(BgL_expz00_8),
								BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(CDR
									(BgL_expz00_8)));
						}
					else
						{	/* Tools/args.scm 111 */
							obj_t BgL_list1052z00_72;

							BgL_list1052z00_72 = MAKE_YOUNG_PAIR(BgL_expz00_8, BNIL);
							return BgL_list1052z00_72;
						}
				}
		}

	}



/* &args*->args-list */
	obj_t BGl_z62argsza2zd2ze3argszd2listz23zztools_argsz00(obj_t BgL_envz00_195,
		obj_t BgL_expz00_196)
	{
		{	/* Tools/args.scm 106 */
			return BGl_argsza2zd2ze3argszd2listz41zztools_argsz00(BgL_expz00_196);
		}

	}



/* args-list->args* */
	BGL_EXPORTED_DEF obj_t BGl_argszd2listzd2ze3argsza2z41zztools_argsz00(obj_t
		BgL_lstz00_9, obj_t BgL_arityz00_10)
	{
		{	/* Tools/args.scm 119 */
			if (((long) CINT(BgL_arityz00_10) >= 0L))
				{	/* Tools/args.scm 121 */
					return BgL_lstz00_9;
				}
			else
				{	/* Tools/args.scm 121 */
					if (((long) CINT(BgL_arityz00_10) == -1L))
						{	/* Tools/args.scm 123 */
							return CAR(((obj_t) BgL_lstz00_9));
						}
					else
						{	/* Tools/args.scm 123 */
							BGL_TAIL return
								BGl_loopze71ze7zztools_argsz00(BgL_lstz00_9, BgL_arityz00_10);
						}
				}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zztools_argsz00(obj_t BgL_lstz00_76,
		obj_t BgL_arityz00_77)
	{
		{	/* Tools/args.scm 126 */
			if (NULLP(CDR(((obj_t) BgL_lstz00_76))))
				{	/* Tools/args.scm 129 */
					bool_t BgL_test1163z00_352;

					if (INTEGERP(BgL_arityz00_77))
						{	/* Tools/args.scm 129 */
							BgL_test1163z00_352 = ((long) CINT(BgL_arityz00_77) < 0L);
						}
					else
						{	/* Tools/args.scm 129 */
							BgL_test1163z00_352 =
								BGl_2zc3zc3zz__r4_numbers_6_5z00(BgL_arityz00_77, BINT(0L));
						}
					if (BgL_test1163z00_352)
						{	/* Tools/args.scm 129 */
							return CAR(((obj_t) BgL_lstz00_76));
						}
					else
						{	/* Tools/args.scm 131 */
							obj_t BgL_list1059z00_82;

							BgL_list1059z00_82 = MAKE_YOUNG_PAIR(BgL_lstz00_76, BNIL);
							return BgL_list1059z00_82;
						}
				}
			else
				{	/* Tools/args.scm 132 */
					obj_t BgL_arg1060z00_83;
					obj_t BgL_arg1062z00_84;

					BgL_arg1060z00_83 = CAR(((obj_t) BgL_lstz00_76));
					{	/* Tools/args.scm 132 */
						obj_t BgL_arg1063z00_85;
						long BgL_arg1065z00_86;

						BgL_arg1063z00_85 = CDR(((obj_t) BgL_lstz00_76));
						BgL_arg1065z00_86 = ((long) CINT(BgL_arityz00_77) + 1L);
						BgL_arg1062z00_84 =
							BGl_loopze71ze7zztools_argsz00(BgL_arg1063z00_85,
							BINT(BgL_arg1065z00_86));
					}
					return MAKE_YOUNG_PAIR(BgL_arg1060z00_83, BgL_arg1062z00_84);
				}
		}

	}



/* &args-list->args* */
	obj_t BGl_z62argszd2listzd2ze3argsza2z23zztools_argsz00(obj_t BgL_envz00_197,
		obj_t BgL_lstz00_198, obj_t BgL_arityz00_199)
	{
		{	/* Tools/args.scm 119 */
			return
				BGl_argszd2listzd2ze3argsza2z41zztools_argsz00(BgL_lstz00_198,
				BgL_arityz00_199);
		}

	}



/* sound-arity? */
	BGL_EXPORTED_DEF bool_t BGl_soundzd2arityzf3z21zztools_argsz00(obj_t
		BgL_arityz00_11, obj_t BgL_argsz00_12)
	{
		{	/* Tools/args.scm 137 */
			{	/* Tools/args.scm 138 */
				long BgL_lenz00_89;

				BgL_lenz00_89 = bgl_list_length(BgL_argsz00_12);
				if (((long) CINT(BgL_arityz00_11) >= 0L))
					{	/* Tools/args.scm 139 */
						return ((long) CINT(BgL_arityz00_11) == BgL_lenz00_89);
					}
				else
					{	/* Tools/args.scm 139 */
						return (NEG((long) CINT(BgL_arityz00_11)) <= (BgL_lenz00_89 + 1L));
		}}}

	}



/* &sound-arity? */
	obj_t BGl_z62soundzd2arityzf3z43zztools_argsz00(obj_t BgL_envz00_200,
		obj_t BgL_arityz00_201, obj_t BgL_argsz00_202)
	{
		{	/* Tools/args.scm 137 */
			return
				BBOOL(BGl_soundzd2arityzf3z21zztools_argsz00(BgL_arityz00_201,
					BgL_argsz00_202));
		}

	}



/* make-args-list */
	BGL_EXPORTED_DEF obj_t BGl_makezd2argszd2listz00zztools_argsz00(obj_t
		BgL_argsz00_13, obj_t BgL_nilz00_14, obj_t BgL_consz00_15)
	{
		{	/* Tools/args.scm 146 */
			BGL_TAIL return
				BGl_loopze70ze7zztools_argsz00(BgL_nilz00_14, BgL_consz00_15,
				BgL_argsz00_13);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zztools_argsz00(obj_t BgL_nilz00_213,
		obj_t BgL_consz00_212, obj_t BgL_argsz00_94)
	{
		{	/* Tools/args.scm 147 */
			if (NULLP(BgL_argsz00_94))
				{	/* Tools/args.scm 148 */
					return BgL_nilz00_213;
				}
			else
				{	/* Tools/args.scm 150 */
					obj_t BgL_arg1078z00_97;

					{	/* Tools/args.scm 150 */
						obj_t BgL_arg1079z00_98;
						obj_t BgL_arg1080z00_99;

						BgL_arg1079z00_98 = CAR(((obj_t) BgL_argsz00_94));
						{	/* Tools/args.scm 150 */
							obj_t BgL_arg1082z00_100;

							{	/* Tools/args.scm 150 */
								obj_t BgL_arg1083z00_101;

								BgL_arg1083z00_101 = CDR(((obj_t) BgL_argsz00_94));
								BgL_arg1082z00_100 =
									BGl_loopze70ze7zztools_argsz00(BgL_nilz00_213,
									BgL_consz00_212, BgL_arg1083z00_101);
							}
							BgL_arg1080z00_99 = MAKE_YOUNG_PAIR(BgL_arg1082z00_100, BNIL);
						}
						BgL_arg1078z00_97 =
							MAKE_YOUNG_PAIR(BgL_arg1079z00_98, BgL_arg1080z00_99);
					}
					return MAKE_YOUNG_PAIR(BgL_consz00_212, BgL_arg1078z00_97);
				}
		}

	}



/* &make-args-list */
	obj_t BGl_z62makezd2argszd2listz62zztools_argsz00(obj_t BgL_envz00_203,
		obj_t BgL_argsz00_204, obj_t BgL_nilz00_205, obj_t BgL_consz00_206)
	{
		{	/* Tools/args.scm 146 */
			return
				BGl_makezd2argszd2listz00zztools_argsz00(BgL_argsz00_204,
				BgL_nilz00_205, BgL_consz00_206);
		}

	}



/* make-n-proto */
	BGL_EXPORTED_DEF obj_t BGl_makezd2nzd2protoz00zztools_argsz00(obj_t
		BgL_nz00_16)
	{
		{	/* Tools/args.scm 157 */
			if (((long) CINT(BgL_nz00_16) < 0L))
				{	/* Tools/args.scm 170 */
					return BGl_makezd2vazd2protoze70ze7zztools_argsz00(BgL_nz00_16, 0L);
				}
			else
				{	/* Tools/args.scm 170 */
					return BGl_makezd2fxzd2protoze70ze7zztools_argsz00(BgL_nz00_16, 0L);
				}
		}

	}



/* make-va-proto~0 */
	obj_t BGl_makezd2vazd2protoze70ze7zztools_argsz00(obj_t BgL_nz00_113,
		long BgL_countz00_114)
	{
		{	/* Tools/args.scm 164 */
			if (((long) CINT(BgL_nz00_113) == -1L))
				{	/* Tools/args.scm 161 */
					return BGl_makezd2argszd2nameze70ze7zztools_argsz00(BgL_countz00_114);
				}
			else
				{	/* Tools/args.scm 163 */
					obj_t BgL_arg1092z00_117;
					obj_t BgL_arg1097z00_118;

					BgL_arg1092z00_117 =
						BGl_makezd2argszd2nameze70ze7zztools_argsz00(BgL_countz00_114);
					{	/* Tools/args.scm 164 */
						long BgL_arg1102z00_119;
						long BgL_arg1103z00_120;

						BgL_arg1102z00_119 = ((long) CINT(BgL_nz00_113) + 1L);
						BgL_arg1103z00_120 = (BgL_countz00_114 + 1L);
						BgL_arg1097z00_118 =
							BGl_makezd2vazd2protoze70ze7zztools_argsz00(BINT
							(BgL_arg1102z00_119), BgL_arg1103z00_120);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1092z00_117, BgL_arg1097z00_118);
				}
		}

	}



/* make-args-name~0 */
	obj_t BGl_makezd2argszd2nameze70ze7zztools_argsz00(long BgL_nz00_107)
	{
		{	/* Tools/args.scm 159 */
			{	/* Tools/args.scm 159 */
				obj_t BgL_arg1087z00_109;

				{	/* Tools/args.scm 159 */
					obj_t BgL_arg1088z00_110;

					{	/* Tools/args.scm 159 */

						BgL_arg1088z00_110 =
							BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
							(BgL_nz00_107, 10L);
					}
					BgL_arg1087z00_109 =
						string_append(BGl_string1128z00zztools_argsz00, BgL_arg1088z00_110);
				}
				return bstring_to_symbol(BgL_arg1087z00_109);
			}
		}

	}



/* make-fx-proto~0 */
	obj_t BGl_makezd2fxzd2protoze70ze7zztools_argsz00(obj_t BgL_nz00_121,
		long BgL_countz00_122)
	{
		{	/* Tools/args.scm 169 */
			if (((long) CINT(BgL_nz00_121) == 0L))
				{	/* Tools/args.scm 166 */
					return BNIL;
				}
			else
				{	/* Tools/args.scm 168 */
					obj_t BgL_arg1114z00_125;
					obj_t BgL_arg1115z00_126;

					BgL_arg1114z00_125 =
						BGl_makezd2argszd2nameze70ze7zztools_argsz00(BgL_countz00_122);
					{	/* Tools/args.scm 169 */
						long BgL_arg1122z00_127;
						long BgL_arg1123z00_128;

						BgL_arg1122z00_127 = ((long) CINT(BgL_nz00_121) - 1L);
						BgL_arg1123z00_128 = (BgL_countz00_122 + 1L);
						BgL_arg1115z00_126 =
							BGl_makezd2fxzd2protoze70ze7zztools_argsz00(BINT
							(BgL_arg1122z00_127), BgL_arg1123z00_128);
					}
					return MAKE_YOUNG_PAIR(BgL_arg1114z00_125, BgL_arg1115z00_126);
				}
		}

	}



/* &make-n-proto */
	obj_t BGl_z62makezd2nzd2protoz62zztools_argsz00(obj_t BgL_envz00_207,
		obj_t BgL_nz00_208)
	{
		{	/* Tools/args.scm 157 */
			return BGl_makezd2nzd2protoz00zztools_argsz00(BgL_nz00_208);
		}

	}



/* +-arity */
	BGL_EXPORTED_DEF obj_t BGl_zb2zd2arityz60zztools_argsz00(obj_t
		BgL_arityz00_17, obj_t BgL_addz00_18)
	{
		{	/* Tools/args.scm 178 */
			if (((long) CINT(BgL_arityz00_17) >= 0L))
				{	/* Tools/args.scm 179 */
					return ADDFX(BgL_addz00_18, BgL_arityz00_17);
				}
			else
				{	/* Tools/args.scm 179 */
					return SUBFX(BgL_arityz00_17, BgL_addz00_18);
				}
		}

	}



/* &+-arity */
	obj_t BGl_z62zb2zd2arityz02zztools_argsz00(obj_t BgL_envz00_209,
		obj_t BgL_arityz00_210, obj_t BgL_addz00_211)
	{
		{	/* Tools/args.scm 178 */
			return
				BGl_zb2zd2arityz60zztools_argsz00(BgL_arityz00_210, BgL_addz00_211);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_argsz00(void)
	{
		{	/* Tools/args.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_argsz00(void)
	{
		{	/* Tools/args.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_argsz00(void)
	{
		{	/* Tools/args.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztools_argsz00(void)
	{
		{	/* Tools/args.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zztools_dssslz00(275867955L,
				BSTRING_TO_STRING(BGl_string1129z00zztools_argsz00));
		}

	}

#ifdef __cplusplus
}
#endif
