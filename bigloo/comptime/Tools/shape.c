/*===========================================================================*/
/*   (Tools/shape.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/shape.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_SHAPE_TYPE_DEFINITIONS
#define BGL_TOOLS_SHAPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;


#endif													// BGL_TOOLS_SHAPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zztools_shapez00 = BUNSPEC;
	extern obj_t BGl_za2accesszd2shapezf3za2z21zzengine_paramz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zztools_shapez00(void);
	BGL_IMPORT obj_t BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_za2typezd2shapezf3za2z21zzengine_paramz00;
	static obj_t BGl_genericzd2initzd2zztools_shapez00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zztools_shapez00(void);
	BGL_EXPORTED_DECL obj_t BGl_shapez00zztools_shapez00(obj_t);
	extern obj_t BGl_za2keyzd2shapezf3za2z21zzengine_paramz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62shapezd2local1227zb0zztools_shapez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t make_vector(long, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2modulezd2shapezf3za2z21zzengine_paramz00;
	static obj_t BGl_z62shape1222z62zztools_shapez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztools_shapez00(void);
	static obj_t BGl_z62shapez62zztools_shapez00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62shapezd2global1225zb0zztools_shapez00(obj_t, obj_t);
	static obj_t BGl_z62shapezd2node1231zb0zztools_shapez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT bool_t BGl_classzf3zf3zz__objectz00(obj_t);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_z62shapezd2type1229zb0zztools_shapez00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	BGL_IMPORT obj_t create_struct(obj_t, int);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zztools_shapez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_shapez00(void);
	extern obj_t BGl_globalzd2bucketzd2positionz00zzast_envz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zztools_shapez00(void);
	static obj_t BGl_gczd2rootszd2initz00zztools_shapez00(void);
	extern obj_t BGl_za2namezd2shapezf3za2z21zzengine_paramz00;
	extern obj_t BGl_za2typenamezd2shapezf3za2z21zzengine_paramz00;
	BGL_IMPORT obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	extern obj_t BGl_za2userzd2shapezf3za2z21zzengine_paramz00;
	BGL_IMPORT obj_t make_struct(obj_t, int, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_loopze70ze7zztools_shapez00(obj_t);
	extern obj_t BGl_nodezd2ze3sexpz31zzast_dumpz00(BgL_nodez00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_GENERIC(BGl_shapezd2envzd2zztools_shapez00,
		BgL_bgl_za762shapeza762za7za7too1799z00, BGl_z62shapez62zztools_shapez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1766z00zztools_shapez00,
		BgL_bgl_string1766za700za7za7t1800za7, "shape1222", 9);
	      DEFINE_STRING(BGl_string1767z00zztools_shapez00,
		BgL_bgl_string1767za700za7za7t1801za7, "#<class:~a>", 11);
	      DEFINE_STRING(BGl_string1769z00zztools_shapez00,
		BgL_bgl_string1769za700za7za7t1802za7, "shape", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1765z00zztools_shapez00,
		BgL_bgl_za762shape1222za762za71803za7, BGl_z62shape1222z62zztools_shapez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1773z00zztools_shapez00,
		BgL_bgl_string1773za700za7za7t1804za7, "-", 1);
	      DEFINE_STRING(BGl_string1774z00zztools_shapez00,
		BgL_bgl_string1774za700za7za7t1805za7, "\"", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1768z00zztools_shapez00,
		BgL_bgl_za762shapeza7d2globa1806z00,
		BGl_z62shapezd2global1225zb0zztools_shapez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1775z00zztools_shapez00,
		BgL_bgl_string1775za700za7za7t1807za7, "::", 2);
	      DEFINE_STRING(BGl_string1776z00zztools_shapez00,
		BgL_bgl_string1776za700za7za7t1808za7, "", 0);
	      DEFINE_STRING(BGl_string1777z00zztools_shapez00,
		BgL_bgl_string1777za700za7za7t1809za7, "-<user>", 7);
	      DEFINE_STRING(BGl_string1778z00zztools_shapez00,
		BgL_bgl_string1778za700za7za7t1810za7, "-<no-user>", 10);
	      DEFINE_STRING(BGl_string1779z00zztools_shapez00,
		BgL_bgl_string1779za700za7za7t1811za7, "{", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1770z00zztools_shapez00,
		BgL_bgl_za762shapeza7d2local1812z00,
		BGl_z62shapezd2local1227zb0zztools_shapez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1771z00zztools_shapez00,
		BgL_bgl_za762shapeza7d2type11813z00,
		BGl_z62shapezd2type1229zb0zztools_shapez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1772z00zztools_shapez00,
		BgL_bgl_za762shapeza7d2node11814z00,
		BGl_z62shapezd2node1231zb0zztools_shapez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1780z00zztools_shapez00,
		BgL_bgl_string1780za700za7za7t1815za7, "}", 1);
	      DEFINE_STRING(BGl_string1781z00zztools_shapez00,
		BgL_bgl_string1781za700za7za7t1816za7, "<noescape>", 10);
	      DEFINE_STRING(BGl_string1782z00zztools_shapez00,
		BgL_bgl_string1782za700za7za7t1817za7, "<escape>", 8);
	      DEFINE_STRING(BGl_string1783z00zztools_shapez00,
		BgL_bgl_string1783za700za7za7t1818za7, "<?>", 3);
	      DEFINE_STRING(BGl_string1784z00zztools_shapez00,
		BgL_bgl_string1784za700za7za7t1819za7, "~a<<~a>>", 8);
	      DEFINE_STRING(BGl_string1785z00zztools_shapez00,
		BgL_bgl_string1785za700za7za7t1820za7, "@", 1);
	      DEFINE_STRING(BGl_string1786z00zztools_shapez00,
		BgL_bgl_string1786za700za7za7t1821za7, "Can't find global anymore -- ", 29);
	      DEFINE_STRING(BGl_string1787z00zztools_shapez00,
		BgL_bgl_string1787za700za7za7t1822za7, "global-shape:", 13);
	      DEFINE_STRING(BGl_string1788z00zztools_shapez00,
		BgL_bgl_string1788za700za7za7t1823za7, "tools_shape", 11);
	      DEFINE_STRING(BGl_string1789z00zztools_shapez00,
		BgL_bgl_string1789za700za7za7t1824za7, "@ _ a-tvector ", 14);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztools_shapez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long
		BgL_checksumz00_1961, char *BgL_fromz00_1962)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_shapez00))
				{
					BGl_requirezd2initializa7ationz75zztools_shapez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_shapez00();
					BGl_libraryzd2moduleszd2initz00zztools_shapez00();
					BGl_cnstzd2initzd2zztools_shapez00();
					BGl_importedzd2moduleszd2initz00zztools_shapez00();
					BGl_genericzd2initzd2zztools_shapez00();
					BGl_methodzd2initzd2zztools_shapez00();
					return BGl_toplevelzd2initzd2zztools_shapez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_shapez00(void)
	{
		{	/* Tools/shape.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"tools_shape");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tools_shape");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_shape");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "tools_shape");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "tools_shape");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztools_shapez00(void)
	{
		{	/* Tools/shape.scm 16 */
			{	/* Tools/shape.scm 16 */
				obj_t BgL_cportz00_1840;

				{	/* Tools/shape.scm 16 */
					obj_t BgL_stringz00_1847;

					BgL_stringz00_1847 = BGl_string1789z00zztools_shapez00;
					{	/* Tools/shape.scm 16 */
						obj_t BgL_startz00_1848;

						BgL_startz00_1848 = BINT(0L);
						{	/* Tools/shape.scm 16 */
							obj_t BgL_endz00_1849;

							BgL_endz00_1849 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1847)));
							{	/* Tools/shape.scm 16 */

								BgL_cportz00_1840 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1847, BgL_startz00_1848, BgL_endz00_1849);
				}}}}
				{
					long BgL_iz00_1841;

					BgL_iz00_1841 = 2L;
				BgL_loopz00_1842:
					if ((BgL_iz00_1841 == -1L))
						{	/* Tools/shape.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Tools/shape.scm 16 */
							{	/* Tools/shape.scm 16 */
								obj_t BgL_arg1798z00_1843;

								{	/* Tools/shape.scm 16 */

									{	/* Tools/shape.scm 16 */
										obj_t BgL_locationz00_1845;

										BgL_locationz00_1845 = BBOOL(((bool_t) 0));
										{	/* Tools/shape.scm 16 */

											BgL_arg1798z00_1843 =
												BGl_readz00zz__readerz00(BgL_cportz00_1840,
												BgL_locationz00_1845);
										}
									}
								}
								{	/* Tools/shape.scm 16 */
									int BgL_tmpz00_1995;

									BgL_tmpz00_1995 = (int) (BgL_iz00_1841);
									CNST_TABLE_SET(BgL_tmpz00_1995, BgL_arg1798z00_1843);
							}}
							{	/* Tools/shape.scm 16 */
								int BgL_auxz00_1846;

								BgL_auxz00_1846 = (int) ((BgL_iz00_1841 - 1L));
								{
									long BgL_iz00_2000;

									BgL_iz00_2000 = (long) (BgL_auxz00_1846);
									BgL_iz00_1841 = BgL_iz00_2000;
									goto BgL_loopz00_1842;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_shapez00(void)
	{
		{	/* Tools/shape.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztools_shapez00(void)
	{
		{	/* Tools/shape.scm 16 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_shapez00(void)
	{
		{	/* Tools/shape.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_shapez00(void)
	{
		{	/* Tools/shape.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_proc1765z00zztools_shapez00,
				BFALSE, BGl_string1766z00zztools_shapez00);
		}

	}



/* &shape1222 */
	obj_t BGl_z62shape1222z62zztools_shapez00(obj_t BgL_envz00_1823,
		obj_t BgL_expz00_1824)
	{
		{	/* Tools/shape.scm 28 */
			if (BGl_classzf3zf3zz__objectz00(BgL_expz00_1824))
				{	/* Tools/shape.scm 31 */
					obj_t BgL_arg1252z00_1851;

					BgL_arg1252z00_1851 =
						BGl_classzd2namezd2zz__objectz00(BgL_expz00_1824);
					{	/* Tools/shape.scm 31 */
						obj_t BgL_list1253z00_1852;

						BgL_list1253z00_1852 = MAKE_YOUNG_PAIR(BgL_arg1252z00_1851, BNIL);
						return
							BGl_formatz00zz__r4_output_6_10_3z00
							(BGl_string1767z00zztools_shapez00, BgL_list1253z00_1852);
					}
				}
			else
				{	/* Tools/shape.scm 30 */
					if (PAIRP(BgL_expz00_1824))
						{	/* Tools/shape.scm 32 */
							return BGl_loopze70ze7zztools_shapez00(BgL_expz00_1824);
						}
					else
						{	/* Tools/shape.scm 32 */
							if (VECTORP(BgL_expz00_1824))
								{	/* Tools/shape.scm 44 */
									obj_t BgL_resz00_1853;

									BgL_resz00_1853 =
										make_vector(VECTOR_LENGTH(BgL_expz00_1824), BNIL);
									{	/* Tools/shape.scm 45 */

										{
											long BgL_indicez00_1855;

											BgL_indicez00_1855 = 0L;
										BgL_loopz00_1854:
											if (
												(BgL_indicez00_1855 == VECTOR_LENGTH(BgL_expz00_1824)))
												{	/* Tools/shape.scm 47 */
													return BgL_resz00_1853;
												}
											else
												{	/* Tools/shape.scm 47 */
													{	/* Tools/shape.scm 52 */
														obj_t BgL_arg1314z00_1856;

														{	/* Tools/shape.scm 52 */
															obj_t BgL_arg1315z00_1857;

															BgL_arg1315z00_1857 =
																VECTOR_REF(
																((obj_t) BgL_expz00_1824), BgL_indicez00_1855);
															BgL_arg1314z00_1856 =
																BGl_shapez00zztools_shapez00
																(BgL_arg1315z00_1857);
														}
														VECTOR_SET(BgL_resz00_1853, BgL_indicez00_1855,
															BgL_arg1314z00_1856);
													}
													{
														long BgL_indicez00_2023;

														BgL_indicez00_2023 = (BgL_indicez00_1855 + 1L);
														BgL_indicez00_1855 = BgL_indicez00_2023;
														goto BgL_loopz00_1854;
													}
												}
										}
									}
								}
							else
								{	/* Tools/shape.scm 54 */
									bool_t BgL_test1832z00_2025;

									if (STRUCTP(BgL_expz00_1824))
										{	/* Tools/shape.scm 54 */
											BgL_test1832z00_2025 =
												(STRUCT_KEY(BgL_expz00_1824) == CNST_TABLE_REF(0));
										}
									else
										{	/* Tools/shape.scm 54 */
											BgL_test1832z00_2025 = ((bool_t) 0);
										}
									if (BgL_test1832z00_2025)
										{	/* Tools/shape.scm 55 */
											obj_t BgL_arg1318z00_1858;
											obj_t BgL_arg1319z00_1859;

											BgL_arg1318z00_1858 =
												BGl_shapez00zztools_shapez00(STRUCT_REF(BgL_expz00_1824,
													(int) (0L)));
											BgL_arg1319z00_1859 =
												BGl_shapez00zztools_shapez00(STRUCT_REF(BgL_expz00_1824,
													(int) (1L)));
											{	/* Tools/shape.scm 55 */
												obj_t BgL_newz00_1860;

												BgL_newz00_1860 =
													create_struct(CNST_TABLE_REF(0), (int) (2L));
												{	/* Tools/shape.scm 55 */
													int BgL_tmpz00_2040;

													BgL_tmpz00_2040 = (int) (1L);
													STRUCT_SET(BgL_newz00_1860, BgL_tmpz00_2040,
														BgL_arg1319z00_1859);
												}
												{	/* Tools/shape.scm 55 */
													int BgL_tmpz00_2043;

													BgL_tmpz00_2043 = (int) (0L);
													STRUCT_SET(BgL_newz00_1860, BgL_tmpz00_2043,
														BgL_arg1318z00_1858);
												}
												return BgL_newz00_1860;
											}
										}
									else
										{	/* Tools/shape.scm 54 */
											if (STRUCTP(BgL_expz00_1824))
												{	/* Tools/shape.scm 57 */
													obj_t BgL_keyz00_1861;

													BgL_keyz00_1861 = STRUCT_KEY(BgL_expz00_1824);
													{	/* Tools/shape.scm 57 */
														int BgL_lenz00_1862;

														BgL_lenz00_1862 = STRUCT_LENGTH(BgL_expz00_1824);
														{	/* Tools/shape.scm 58 */
															obj_t BgL_resz00_1863;

															BgL_resz00_1863 =
																make_struct(BgL_keyz00_1861, BgL_lenz00_1862,
																BNIL);
															{	/* Tools/shape.scm 59 */

																{
																	long BgL_indicez00_1865;

																	BgL_indicez00_1865 = 0L;
																BgL_loopz00_1864:
																	if (
																		(BgL_indicez00_1865 ==
																			(long) (BgL_lenz00_1862)))
																		{	/* Tools/shape.scm 61 */
																			return BgL_resz00_1863;
																		}
																	else
																		{	/* Tools/shape.scm 61 */
																			{	/* Tools/shape.scm 66 */
																				obj_t BgL_arg1325z00_1866;

																				{	/* Tools/shape.scm 66 */
																					obj_t BgL_arg1326z00_1867;

																					BgL_arg1326z00_1867 =
																						STRUCT_REF(
																						((obj_t) BgL_expz00_1824),
																						(int) (BgL_indicez00_1865));
																					BgL_arg1325z00_1866 =
																						BGl_shapez00zztools_shapez00
																						(BgL_arg1326z00_1867);
																				}
																				{	/* Tools/shape.scm 64 */
																					int BgL_tmpz00_2058;

																					BgL_tmpz00_2058 =
																						(int) (BgL_indicez00_1865);
																					STRUCT_SET(BgL_resz00_1863,
																						BgL_tmpz00_2058,
																						BgL_arg1325z00_1866);
																			}}
																			{
																				long BgL_indicez00_2061;

																				BgL_indicez00_2061 =
																					(BgL_indicez00_1865 + 1L);
																				BgL_indicez00_1865 = BgL_indicez00_2061;
																				goto BgL_loopz00_1864;
																			}
																		}
																}
															}
														}
													}
												}
											else
												{	/* Tools/shape.scm 56 */
													return BgL_expz00_1824;
												}
										}
								}
						}
				}
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zztools_shapez00(obj_t BgL_expz00_1386)
	{
		{	/* Tools/shape.scm 33 */
			if (NULLP(BgL_expz00_1386))
				{	/* Tools/shape.scm 35 */
					return BNIL;
				}
			else
				{	/* Tools/shape.scm 35 */
					if (PAIRP(BgL_expz00_1386))
						{	/* Tools/shape.scm 37 */
							if (EPAIRP(BgL_expz00_1386))
								{	/* Tools/shape.scm 40 */
									obj_t BgL_arg1268z00_1391;
									obj_t BgL_arg1272z00_1392;
									obj_t BgL_arg1284z00_1393;

									BgL_arg1268z00_1391 =
										BGl_shapez00zztools_shapez00(CAR(BgL_expz00_1386));
									BgL_arg1272z00_1392 =
										BGl_loopze70ze7zztools_shapez00(CDR(BgL_expz00_1386));
									BgL_arg1284z00_1393 = CER(BgL_expz00_1386);
									{	/* Tools/shape.scm 40 */
										obj_t BgL_res1756z00_1720;

										BgL_res1756z00_1720 =
											MAKE_YOUNG_EPAIR(BgL_arg1268z00_1391, BgL_arg1272z00_1392,
											BgL_arg1284z00_1393);
										return BgL_res1756z00_1720;
									}
								}
							else
								{	/* Tools/shape.scm 39 */
									return
										MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(CAR
											(BgL_expz00_1386)),
										BGl_loopze70ze7zztools_shapez00(CDR(BgL_expz00_1386)));
								}
						}
					else
						{	/* Tools/shape.scm 37 */
							BGL_TAIL return BGl_shapez00zztools_shapez00(BgL_expz00_1386);
						}
				}
		}

	}



/* shape */
	BGL_EXPORTED_DEF obj_t BGl_shapez00zztools_shapez00(obj_t BgL_expz00_13)
	{
		{	/* Tools/shape.scm 28 */
			if (BGL_OBJECTP(BgL_expz00_13))
				{	/* Tools/shape.scm 28 */
					obj_t BgL_method1223z00_1431;

					{	/* Tools/shape.scm 28 */
						obj_t BgL_res1763z00_1782;

						{	/* Tools/shape.scm 28 */
							long BgL_objzd2classzd2numz00_1753;

							BgL_objzd2classzd2numz00_1753 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_expz00_13));
							{	/* Tools/shape.scm 28 */
								obj_t BgL_arg1811z00_1754;

								BgL_arg1811z00_1754 =
									PROCEDURE_REF(BGl_shapezd2envzd2zztools_shapez00, (int) (1L));
								{	/* Tools/shape.scm 28 */
									int BgL_offsetz00_1757;

									BgL_offsetz00_1757 = (int) (BgL_objzd2classzd2numz00_1753);
									{	/* Tools/shape.scm 28 */
										long BgL_offsetz00_1758;

										BgL_offsetz00_1758 =
											((long) (BgL_offsetz00_1757) - OBJECT_TYPE);
										{	/* Tools/shape.scm 28 */
											long BgL_modz00_1759;

											BgL_modz00_1759 =
												(BgL_offsetz00_1758 >> (int) ((long) ((int) (4L))));
											{	/* Tools/shape.scm 28 */
												long BgL_restz00_1761;

												BgL_restz00_1761 =
													(BgL_offsetz00_1758 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* Tools/shape.scm 28 */

													{	/* Tools/shape.scm 28 */
														obj_t BgL_bucketz00_1763;

														BgL_bucketz00_1763 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_1754), BgL_modz00_1759);
														BgL_res1763z00_1782 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_1763), BgL_restz00_1761);
						}}}}}}}}
						BgL_method1223z00_1431 = BgL_res1763z00_1782;
					}
					return BGL_PROCEDURE_CALL1(BgL_method1223z00_1431, BgL_expz00_13);
				}
			else
				{	/* Tools/shape.scm 28 */
					obj_t BgL_fun1329z00_1432;

					BgL_fun1329z00_1432 =
						PROCEDURE_REF(BGl_shapezd2envzd2zztools_shapez00, (int) (0L));
					return BGL_PROCEDURE_CALL1(BgL_fun1329z00_1432, BgL_expz00_13);
				}
		}

	}



/* &shape */
	obj_t BGl_z62shapez62zztools_shapez00(obj_t BgL_envz00_1825,
		obj_t BgL_expz00_1826)
	{
		{	/* Tools/shape.scm 28 */
			return BGl_shapez00zztools_shapez00(BgL_expz00_1826);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_shapez00(void)
	{
		{	/* Tools/shape.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_globalz00zzast_varz00,
				BGl_proc1768z00zztools_shapez00, BGl_string1769z00zztools_shapez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_localz00zzast_varz00,
				BGl_proc1770z00zztools_shapez00, BGl_string1769z00zztools_shapez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_typez00zztype_typez00,
				BGl_proc1771z00zztools_shapez00, BGl_string1769z00zztools_shapez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_nodez00zzast_nodez00,
				BGl_proc1772z00zztools_shapez00, BGl_string1769z00zztools_shapez00);
		}

	}



/* &shape-node1231 */
	obj_t BGl_z62shapezd2node1231zb0zztools_shapez00(obj_t BgL_envz00_1831,
		obj_t BgL_nodez00_1832)
	{
		{	/* Tools/shape.scm 168 */
			return
				BGl_nodezd2ze3sexpz31zzast_dumpz00(
				((BgL_nodez00_bglt) BgL_nodez00_1832));
		}

	}



/* &shape-type1229 */
	obj_t BGl_z62shapezd2type1229zb0zztools_shapez00(obj_t BgL_envz00_1833,
		obj_t BgL_typez00_1834)
	{
		{	/* Tools/shape.scm 150 */
			{	/* Tools/shape.scm 151 */
				BgL_typez00_bglt BgL_typez00_1869;

				BgL_typez00_1869 = ((BgL_typez00_bglt) BgL_typez00_1834);
				{	/* Tools/shape.scm 151 */
					obj_t BgL_spz00_1870;

					{	/* Tools/shape.scm 151 */
						obj_t BgL_arg1613z00_1871;

						BgL_arg1613z00_1871 =
							(((BgL_typez00_bglt) COBJECT(BgL_typez00_1869))->BgL_idz00);
						{	/* Tools/shape.scm 151 */
							obj_t BgL_arg1455z00_1872;

							BgL_arg1455z00_1872 = SYMBOL_TO_STRING(BgL_arg1613z00_1871);
							BgL_spz00_1870 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1872);
						}
					}
					{	/* Tools/shape.scm 151 */
						obj_t BgL_sz00_1873;

						if (CBOOL(BGl_za2keyzd2shapezf3za2z21zzengine_paramz00))
							{	/* Tools/shape.scm 158 */
								obj_t BgL_arg1609z00_1874;

								{	/* Tools/shape.scm 158 */
									long BgL_arg1611z00_1875;

									BgL_arg1611z00_1875 = (long) (BgL_typez00_1869);
									BgL_arg1609z00_1874 =
										BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
										(BgL_arg1611z00_1875, 16L);
								}
								BgL_sz00_1873 =
									string_append_3(BgL_spz00_1870,
									BGl_string1773z00zztools_shapez00, BgL_arg1609z00_1874);
							}
						else
							{	/* Tools/shape.scm 152 */
								BgL_sz00_1873 = BgL_spz00_1870;
							}
						{	/* Tools/shape.scm 152 */

							if (CBOOL(BGl_za2typenamezd2shapezf3za2z21zzengine_paramz00))
								{	/* Tools/shape.scm 162 */
									obj_t BgL_arg1595z00_1876;

									BgL_arg1595z00_1876 =
										(((BgL_typez00_bglt) COBJECT(BgL_typez00_1869))->
										BgL_namez00);
									{	/* Tools/shape.scm 162 */
										obj_t BgL_list1596z00_1877;

										{	/* Tools/shape.scm 162 */
											obj_t BgL_arg1602z00_1878;

											{	/* Tools/shape.scm 162 */
												obj_t BgL_arg1605z00_1879;

												{	/* Tools/shape.scm 162 */
													obj_t BgL_arg1606z00_1880;

													BgL_arg1606z00_1880 =
														MAKE_YOUNG_PAIR(BGl_string1774z00zztools_shapez00,
														BNIL);
													BgL_arg1605z00_1879 =
														MAKE_YOUNG_PAIR(BgL_arg1595z00_1876,
														BgL_arg1606z00_1880);
												}
												BgL_arg1602z00_1878 =
													MAKE_YOUNG_PAIR(BGl_string1774z00zztools_shapez00,
													BgL_arg1605z00_1879);
											}
											BgL_list1596z00_1877 =
												MAKE_YOUNG_PAIR(BgL_sz00_1873, BgL_arg1602z00_1878);
										}
										return
											BGl_stringzd2appendzd2zz__r4_strings_6_7z00
											(BgL_list1596z00_1877);
									}
								}
							else
								{	/* Tools/shape.scm 161 */
									return BgL_sz00_1873;
								}
						}
					}
				}
			}
		}

	}



/* &shape-local1227 */
	obj_t BGl_z62shapezd2local1227zb0zztools_shapez00(obj_t BgL_envz00_1835,
		obj_t BgL_varz00_1836)
	{
		{	/* Tools/shape.scm 115 */
			{	/* Tools/shape.scm 116 */
				obj_t BgL_symz00_1882;

				if (CBOOL(BGl_za2keyzd2shapezf3za2z21zzengine_paramz00))
					{	/* Tools/shape.scm 117 */
						obj_t BgL_arg1576z00_1883;
						obj_t BgL_arg1584z00_1884;

						BgL_arg1576z00_1883 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_varz00_1836))))->BgL_idz00);
						{	/* Tools/shape.scm 120 */
							obj_t BgL_arg1593z00_1885;

							{	/* Tools/shape.scm 120 */

								BgL_arg1593z00_1885 =
									BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
									(((BgL_localz00_bglt) COBJECT(
												((BgL_localz00_bglt) BgL_varz00_1836)))->BgL_keyz00),
									10L);
							}
							BgL_arg1584z00_1884 = bstring_to_symbol(BgL_arg1593z00_1885);
						}
						{	/* Tools/shape.scm 117 */
							obj_t BgL_list1585z00_1886;

							{	/* Tools/shape.scm 117 */
								obj_t BgL_arg1589z00_1887;

								{	/* Tools/shape.scm 117 */
									obj_t BgL_arg1591z00_1888;

									BgL_arg1591z00_1888 =
										MAKE_YOUNG_PAIR(BgL_arg1584z00_1884, BNIL);
									BgL_arg1589z00_1887 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg1591z00_1888);
								}
								BgL_list1585z00_1886 =
									MAKE_YOUNG_PAIR(BgL_arg1576z00_1883, BgL_arg1589z00_1887);
							}
							BgL_symz00_1882 =
								BGl_symbolzd2appendzd2zz__r4_symbols_6_4z00
								(BgL_list1585z00_1886);
						}
					}
				else
					{	/* Tools/shape.scm 116 */
						BgL_symz00_1882 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_varz00_1836))))->BgL_idz00);
					}
				{	/* Tools/shape.scm 116 */
					obj_t BgL_symz00_1889;

					{	/* Tools/shape.scm 122 */
						obj_t BgL_arg1455z00_1890;

						BgL_arg1455z00_1890 = SYMBOL_TO_STRING(BgL_symz00_1882);
						BgL_symz00_1889 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1890);
					}
					{	/* Tools/shape.scm 122 */
						BgL_typez00_bglt BgL_typez00_1891;

						BgL_typez00_1891 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_varz00_1836))))->BgL_typez00);
						{	/* Tools/shape.scm 123 */
							obj_t BgL_tshapez00_1892;

							if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
								{	/* Tools/shape.scm 126 */
									obj_t BgL_arg1575z00_1893;

									BgL_arg1575z00_1893 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_typez00_1891));
									BgL_tshapez00_1892 =
										string_append(BGl_string1775z00zztools_shapez00,
										BgL_arg1575z00_1893);
								}
							else
								{	/* Tools/shape.scm 124 */
									BgL_tshapez00_1892 = BGl_string1776z00zztools_shapez00;
								}
							{	/* Tools/shape.scm 124 */
								obj_t BgL_ushapez00_1894;

								if (CBOOL(BGl_za2userzd2shapezf3za2z21zzengine_paramz00))
									{	/* Tools/shape.scm 127 */
										if (
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_varz00_1836))))->
												BgL_userzf3zf3))
											{	/* Tools/shape.scm 129 */
												BgL_ushapez00_1894 = BGl_string1777z00zztools_shapez00;
											}
										else
											{	/* Tools/shape.scm 129 */
												BgL_ushapez00_1894 = BGl_string1778z00zztools_shapez00;
											}
									}
								else
									{	/* Tools/shape.scm 127 */
										BgL_ushapez00_1894 = BGl_string1776z00zztools_shapez00;
									}
								{	/* Tools/shape.scm 127 */
									obj_t BgL_ashapez00_1895;

									if (CBOOL(BGl_za2accesszd2shapezf3za2z21zzengine_paramz00))
										{	/* Tools/shape.scm 136 */
											obj_t BgL_arg1571z00_1896;

											{	/* Tools/shape.scm 136 */
												obj_t BgL_arg1573z00_1897;

												BgL_arg1573z00_1897 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_varz00_1836))))->
													BgL_accessz00);
												{	/* Tools/shape.scm 136 */
													obj_t BgL_arg1455z00_1898;

													BgL_arg1455z00_1898 =
														SYMBOL_TO_STRING(((obj_t) BgL_arg1573z00_1897));
													BgL_arg1571z00_1896 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1898);
												}
											}
											BgL_ashapez00_1895 =
												string_append_3(BGl_string1779z00zztools_shapez00,
												BgL_arg1571z00_1896, BGl_string1780z00zztools_shapez00);
										}
									else
										{	/* Tools/shape.scm 133 */
											BgL_ashapez00_1895 = BGl_string1776z00zztools_shapez00;
										}
									{	/* Tools/shape.scm 132 */
										obj_t BgL_xshapez00_1899;

										if (CBOOL(BGl_za2accesszd2shapezf3za2z21zzengine_paramz00))
											{	/* Tools/shape.scm 138 */
												if (
													((((BgL_localz00_bglt) COBJECT(
																	((BgL_localz00_bglt) BgL_varz00_1836)))->
															BgL_valzd2noescapezd2) == BTRUE))
													{	/* Tools/shape.scm 141 */
														BgL_xshapez00_1899 =
															BGl_string1781z00zztools_shapez00;
													}
												else
													{	/* Tools/shape.scm 141 */
														if (
															((((BgL_localz00_bglt) COBJECT(
																			((BgL_localz00_bglt) BgL_varz00_1836)))->
																	BgL_valzd2noescapezd2) == BFALSE))
															{	/* Tools/shape.scm 142 */
																BgL_xshapez00_1899 =
																	BGl_string1782z00zztools_shapez00;
															}
														else
															{	/* Tools/shape.scm 142 */
																BgL_xshapez00_1899 =
																	BGl_string1783z00zztools_shapez00;
															}
													}
											}
										else
											{	/* Tools/shape.scm 138 */
												BgL_xshapez00_1899 = BGl_string1776z00zztools_shapez00;
											}
										{	/* Tools/shape.scm 138 */

											{	/* Tools/shape.scm 145 */
												obj_t BgL_arg1516z00_1900;

												{	/* Tools/shape.scm 145 */
													obj_t BgL_list1517z00_1901;

													{	/* Tools/shape.scm 145 */
														obj_t BgL_arg1535z00_1902;

														{	/* Tools/shape.scm 145 */
															obj_t BgL_arg1540z00_1903;

															{	/* Tools/shape.scm 145 */
																obj_t BgL_arg1544z00_1904;

																{	/* Tools/shape.scm 145 */
																	obj_t BgL_arg1546z00_1905;

																	BgL_arg1546z00_1905 =
																		MAKE_YOUNG_PAIR(BgL_xshapez00_1899, BNIL);
																	BgL_arg1544z00_1904 =
																		MAKE_YOUNG_PAIR(BgL_ashapez00_1895,
																		BgL_arg1546z00_1905);
																}
																BgL_arg1540z00_1903 =
																	MAKE_YOUNG_PAIR(BgL_ushapez00_1894,
																	BgL_arg1544z00_1904);
															}
															BgL_arg1535z00_1902 =
																MAKE_YOUNG_PAIR(BgL_tshapez00_1892,
																BgL_arg1540z00_1903);
														}
														BgL_list1517z00_1901 =
															MAKE_YOUNG_PAIR(BgL_symz00_1889,
															BgL_arg1535z00_1902);
													}
													BgL_arg1516z00_1900 =
														BGl_stringzd2appendzd2zz__r4_strings_6_7z00
														(BgL_list1517z00_1901);
												}
												return bstring_to_symbol(BgL_arg1516z00_1900);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &shape-global1225 */
	obj_t BGl_z62shapezd2global1225zb0zztools_shapez00(obj_t BgL_envz00_1837,
		obj_t BgL_varz00_1838)
	{
		{	/* Tools/shape.scm 74 */
			{	/* Tools/shape.scm 75 */
				obj_t BgL_strzd2idzd2_1907;

				if (CBOOL(BGl_za2namezd2shapezf3za2z21zzengine_paramz00))
					{	/* Tools/shape.scm 77 */
						obj_t BgL_arg1502z00_1908;
						obj_t BgL_arg1509z00_1909;

						BgL_arg1502z00_1908 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_1838))))->BgL_idz00);
						BgL_arg1509z00_1909 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_1838))))->BgL_namez00);
						{	/* Tools/shape.scm 77 */
							obj_t BgL_list1510z00_1910;

							{	/* Tools/shape.scm 77 */
								obj_t BgL_arg1513z00_1911;

								BgL_arg1513z00_1911 =
									MAKE_YOUNG_PAIR(BgL_arg1509z00_1909, BNIL);
								BgL_list1510z00_1910 =
									MAKE_YOUNG_PAIR(BgL_arg1502z00_1908, BgL_arg1513z00_1911);
							}
							BgL_strzd2idzd2_1907 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string1784z00zztools_shapez00, BgL_list1510z00_1910);
						}
					}
				else
					{	/* Tools/shape.scm 76 */
						obj_t BgL_arg1514z00_1912;

						BgL_arg1514z00_1912 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_1838))))->BgL_idz00);
						{	/* Tools/shape.scm 76 */
							obj_t BgL_arg1455z00_1913;

							BgL_arg1455z00_1913 = SYMBOL_TO_STRING(BgL_arg1514z00_1912);
							BgL_strzd2idzd2_1907 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1913);
						}
					}
				{	/* Tools/shape.scm 75 */
					obj_t BgL_modulez00_1914;

					{	/* Tools/shape.scm 78 */
						obj_t BgL_arg1489z00_1915;

						BgL_arg1489z00_1915 =
							(((BgL_globalz00_bglt) COBJECT(
									((BgL_globalz00_bglt) BgL_varz00_1838)))->BgL_modulez00);
						{	/* Tools/shape.scm 78 */
							obj_t BgL_arg1455z00_1916;

							BgL_arg1455z00_1916 = SYMBOL_TO_STRING(BgL_arg1489z00_1915);
							BgL_modulez00_1914 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1916);
						}
					}
					{	/* Tools/shape.scm 78 */
						BgL_typez00_bglt BgL_typez00_1917;

						BgL_typez00_1917 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_varz00_1838))))->BgL_typez00);
						{	/* Tools/shape.scm 79 */
							obj_t BgL_tshapez00_1918;

							if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
								{	/* Tools/shape.scm 82 */
									obj_t BgL_arg1485z00_1919;

									BgL_arg1485z00_1919 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_typez00_1917));
									BgL_tshapez00_1918 =
										string_append(BGl_string1775z00zztools_shapez00,
										BgL_arg1485z00_1919);
								}
							else
								{	/* Tools/shape.scm 80 */
									BgL_tshapez00_1918 = BGl_string1776z00zztools_shapez00;
								}
							{	/* Tools/shape.scm 80 */
								obj_t BgL_ushapez00_1920;

								if (CBOOL(BGl_za2userzd2shapezf3za2z21zzengine_paramz00))
									{	/* Tools/shape.scm 83 */
										if (
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_varz00_1838))))->
												BgL_userzf3zf3))
											{	/* Tools/shape.scm 85 */
												BgL_ushapez00_1920 = BGl_string1777z00zztools_shapez00;
											}
										else
											{	/* Tools/shape.scm 85 */
												BgL_ushapez00_1920 = BGl_string1778z00zztools_shapez00;
											}
									}
								else
									{	/* Tools/shape.scm 83 */
										BgL_ushapez00_1920 = BGl_string1776z00zztools_shapez00;
									}
								{	/* Tools/shape.scm 83 */
									obj_t BgL_ashapez00_1921;

									if (CBOOL(BGl_za2accesszd2shapezf3za2z21zzengine_paramz00))
										{	/* Tools/shape.scm 93 */
											obj_t BgL_arg1454z00_1922;

											{	/* Tools/shape.scm 93 */
												obj_t BgL_arg1472z00_1923;

												BgL_arg1472z00_1923 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_varz00_1838))))->
													BgL_accessz00);
												{	/* Tools/shape.scm 93 */
													obj_t BgL_arg1455z00_1924;

													BgL_arg1455z00_1924 =
														SYMBOL_TO_STRING(((obj_t) BgL_arg1472z00_1923));
													BgL_arg1454z00_1922 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_1924);
												}
											}
											BgL_ashapez00_1921 =
												string_append_3(BGl_string1779z00zztools_shapez00,
												BgL_arg1454z00_1922, BGl_string1780z00zztools_shapez00);
										}
									else
										{	/* Tools/shape.scm 89 */
											BgL_ashapez00_1921 = BGl_string1776z00zztools_shapez00;
										}
									{	/* Tools/shape.scm 88 */

										if (CBOOL(BGl_za2modulezd2shapezf3za2z21zzengine_paramz00))
											{	/* Tools/shape.scm 97 */
												obj_t BgL_arg1331z00_1925;

												{	/* Tools/shape.scm 97 */
													obj_t BgL_list1332z00_1926;

													{	/* Tools/shape.scm 97 */
														obj_t BgL_arg1333z00_1927;

														{	/* Tools/shape.scm 97 */
															obj_t BgL_arg1335z00_1928;

															{	/* Tools/shape.scm 97 */
																obj_t BgL_arg1339z00_1929;

																{	/* Tools/shape.scm 97 */
																	obj_t BgL_arg1340z00_1930;

																	{	/* Tools/shape.scm 97 */
																		obj_t BgL_arg1342z00_1931;

																		BgL_arg1342z00_1931 =
																			MAKE_YOUNG_PAIR(BgL_ashapez00_1921, BNIL);
																		BgL_arg1340z00_1930 =
																			MAKE_YOUNG_PAIR(BgL_ushapez00_1920,
																			BgL_arg1342z00_1931);
																	}
																	BgL_arg1339z00_1929 =
																		MAKE_YOUNG_PAIR(BgL_tshapez00_1918,
																		BgL_arg1340z00_1930);
																}
																BgL_arg1335z00_1928 =
																	MAKE_YOUNG_PAIR(BgL_modulez00_1914,
																	BgL_arg1339z00_1929);
															}
															BgL_arg1333z00_1927 =
																MAKE_YOUNG_PAIR
																(BGl_string1785z00zztools_shapez00,
																BgL_arg1335z00_1928);
														}
														BgL_list1332z00_1926 =
															MAKE_YOUNG_PAIR(BgL_strzd2idzd2_1907,
															BgL_arg1333z00_1927);
													}
													BgL_arg1331z00_1925 =
														BGl_stringzd2appendzd2zz__r4_strings_6_7z00
														(BgL_list1332z00_1926);
												}
												return bstring_to_symbol(BgL_arg1331z00_1925);
											}
										else
											{

												{	/* Tools/shape.scm 99 */
													obj_t BgL_aux1103z00_1941;

													{	/* Tools/shape.scm 99 */
														obj_t BgL_arg1380z00_1942;
														obj_t BgL_arg1408z00_1943;

														BgL_arg1380z00_1942 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_varz00_1838))))->
															BgL_idz00);
														BgL_arg1408z00_1943 =
															(((BgL_globalz00_bglt)
																COBJECT(((BgL_globalz00_bglt)
																		BgL_varz00_1838)))->BgL_modulez00);
														BgL_aux1103z00_1941 =
															BGl_globalzd2bucketzd2positionz00zzast_envz00
															(BgL_arg1380z00_1942, BgL_arg1408z00_1943);
													}
													if (INTEGERP(BgL_aux1103z00_1941))
														{	/* Tools/shape.scm 99 */
															switch ((long) CINT(BgL_aux1103z00_1941))
																{
																case -1L:

																	{	/* Tools/shape.scm 103 */
																		obj_t BgL_arg1343z00_1944;

																		{	/* Tools/shape.scm 103 */
																			obj_t BgL_arg1349z00_1945;

																			{	/* Tools/shape.scm 103 */
																				obj_t BgL_arg1351z00_1946;
																				obj_t BgL_arg1352z00_1947;

																				BgL_arg1351z00_1946 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									BgL_varz00_1838))))->
																					BgL_idz00);
																				BgL_arg1352z00_1947 =
																					MAKE_YOUNG_PAIR((((BgL_globalz00_bglt)
																							COBJECT(((BgL_globalz00_bglt)
																									BgL_varz00_1838)))->
																						BgL_modulez00), BNIL);
																				BgL_arg1349z00_1945 =
																					MAKE_YOUNG_PAIR(BgL_arg1351z00_1946,
																					BgL_arg1352z00_1947);
																			}
																			BgL_arg1343z00_1944 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																				BgL_arg1349z00_1945);
																		}
																		{	/* Tools/shape.scm 101 */
																			obj_t BgL_list1344z00_1948;

																			{	/* Tools/shape.scm 101 */
																				obj_t BgL_arg1346z00_1949;

																				{	/* Tools/shape.scm 101 */
																					obj_t BgL_arg1348z00_1950;

																					BgL_arg1348z00_1950 =
																						MAKE_YOUNG_PAIR(BgL_arg1343z00_1944,
																						BNIL);
																					BgL_arg1346z00_1949 =
																						MAKE_YOUNG_PAIR
																						(BGl_string1786z00zztools_shapez00,
																						BgL_arg1348z00_1950);
																				}
																				BgL_list1344z00_1948 =
																					MAKE_YOUNG_PAIR
																					(BGl_string1787z00zztools_shapez00,
																					BgL_arg1346z00_1949);
																			}
																			BGl_warningz00zz__errorz00
																				(BgL_list1344z00_1948);
																		}
																	}
																	{	/* Tools/shape.scm 104 */
																		obj_t BgL_arg1364z00_1951;

																		{	/* Tools/shape.scm 104 */
																			obj_t BgL_list1365z00_1952;

																			{	/* Tools/shape.scm 104 */
																				obj_t BgL_arg1367z00_1953;

																				{	/* Tools/shape.scm 104 */
																					obj_t BgL_arg1370z00_1954;

																					{	/* Tools/shape.scm 104 */
																						obj_t BgL_arg1371z00_1955;

																						BgL_arg1371z00_1955 =
																							MAKE_YOUNG_PAIR
																							(BgL_ashapez00_1921, BNIL);
																						BgL_arg1370z00_1954 =
																							MAKE_YOUNG_PAIR
																							(BgL_ushapez00_1920,
																							BgL_arg1371z00_1955);
																					}
																					BgL_arg1367z00_1953 =
																						MAKE_YOUNG_PAIR(BgL_tshapez00_1918,
																						BgL_arg1370z00_1954);
																				}
																				BgL_list1365z00_1952 =
																					MAKE_YOUNG_PAIR(BgL_strzd2idzd2_1907,
																					BgL_arg1367z00_1953);
																			}
																			BgL_arg1364z00_1951 =
																				BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																				(BgL_list1365z00_1952);
																		}
																		return
																			bstring_to_symbol(BgL_arg1364z00_1951);
																	}
																	break;
																case 0L:

																	{	/* Tools/shape.scm 106 */
																		obj_t BgL_arg1375z00_1956;

																		{	/* Tools/shape.scm 106 */
																			obj_t BgL_list1376z00_1957;

																			{	/* Tools/shape.scm 106 */
																				obj_t BgL_arg1377z00_1958;

																				{	/* Tools/shape.scm 106 */
																					obj_t BgL_arg1378z00_1959;

																					{	/* Tools/shape.scm 106 */
																						obj_t BgL_arg1379z00_1960;

																						BgL_arg1379z00_1960 =
																							MAKE_YOUNG_PAIR
																							(BgL_ashapez00_1921, BNIL);
																						BgL_arg1378z00_1959 =
																							MAKE_YOUNG_PAIR
																							(BgL_ushapez00_1920,
																							BgL_arg1379z00_1960);
																					}
																					BgL_arg1377z00_1958 =
																						MAKE_YOUNG_PAIR(BgL_tshapez00_1918,
																						BgL_arg1378z00_1959);
																				}
																				BgL_list1376z00_1957 =
																					MAKE_YOUNG_PAIR(BgL_strzd2idzd2_1907,
																					BgL_arg1377z00_1958);
																			}
																			BgL_arg1375z00_1956 =
																				BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																				(BgL_list1376z00_1957);
																		}
																		return
																			bstring_to_symbol(BgL_arg1375z00_1956);
																	}
																	break;
																default:
																BgL_case_else1102z00_1932:
																	{	/* Tools/shape.scm 108 */
																		obj_t BgL_symz00_1933;

																		{	/* Tools/shape.scm 109 */
																			obj_t BgL_arg1434z00_1934;

																			{	/* Tools/shape.scm 109 */
																				obj_t BgL_list1435z00_1935;

																				{	/* Tools/shape.scm 109 */
																					obj_t BgL_arg1437z00_1936;

																					{	/* Tools/shape.scm 109 */
																						obj_t BgL_arg1448z00_1937;

																						{	/* Tools/shape.scm 109 */
																							obj_t BgL_arg1453z00_1938;

																							BgL_arg1453z00_1938 =
																								MAKE_YOUNG_PAIR
																								(BgL_ashapez00_1921, BNIL);
																							BgL_arg1448z00_1937 =
																								MAKE_YOUNG_PAIR
																								(BgL_ushapez00_1920,
																								BgL_arg1453z00_1938);
																						}
																						BgL_arg1437z00_1936 =
																							MAKE_YOUNG_PAIR
																							(BgL_tshapez00_1918,
																							BgL_arg1448z00_1937);
																					}
																					BgL_list1435z00_1935 =
																						MAKE_YOUNG_PAIR
																						(BgL_strzd2idzd2_1907,
																						BgL_arg1437z00_1936);
																				}
																				BgL_arg1434z00_1934 =
																					BGl_stringzd2appendzd2zz__r4_strings_6_7z00
																					(BgL_list1435z00_1935);
																			}
																			BgL_symz00_1933 =
																				bstring_to_symbol(BgL_arg1434z00_1934);
																		}
																		{	/* Tools/shape.scm 110 */
																			obj_t BgL_arg1410z00_1939;

																			{	/* Tools/shape.scm 110 */
																				obj_t BgL_arg1421z00_1940;

																				BgL_arg1421z00_1940 =
																					MAKE_YOUNG_PAIR(bstring_to_symbol
																					(BgL_modulez00_1914), BNIL);
																				BgL_arg1410z00_1939 =
																					MAKE_YOUNG_PAIR(BgL_symz00_1933,
																					BgL_arg1421z00_1940);
																			}
																			return
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(2),
																				BgL_arg1410z00_1939);
																		}
																	}
																}
														}
													else
														{	/* Tools/shape.scm 99 */
															goto BgL_case_else1102z00_1932;
														}
												}
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztools_shapez00(void)
	{
		{	/* Tools/shape.scm 16 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1788z00zztools_shapez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1788z00zztools_shapez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1788z00zztools_shapez00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1788z00zztools_shapez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1788z00zztools_shapez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1788z00zztools_shapez00));
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1788z00zztools_shapez00));
		}

	}

#ifdef __cplusplus
}
#endif
