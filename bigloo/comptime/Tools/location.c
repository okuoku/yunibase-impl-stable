/*===========================================================================*/
/*   (Tools/location.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/location.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_LOCATION_TYPE_DEFINITIONS
#define BGL_TOOLS_LOCATION_TYPE_DEFINITIONS
#endif													// BGL_TOOLS_LOCATION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62locationzd2shapezb0zztools_locationz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2libzd2srczd2dirza2z00zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztools_locationz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_dumpzd2locationzd2zztools_locationz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zztools_locationz00(void);
	BGL_EXPORTED_DECL obj_t BGl_locationzd2shapezd2zztools_locationz00(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zztools_locationz00(void);
	extern obj_t BGl_za2locationzd2shapezf3za2z21zzengine_paramz00;
	static obj_t BGl_objectzd2initzd2zztools_locationz00(void);
	static obj_t BGl_z62findzd2locationzb0zztools_locationz00(obj_t, obj_t);
	static obj_t BGl_poszd2ze3linez31zztools_locationz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2filezd2namez00zz__osz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_getzd2filezd2linesz00zztools_locationz00(obj_t);
	static obj_t BGl_methodzd2initzd2zztools_locationz00(void);
	BGL_IMPORT obj_t BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(int,
		obj_t);
	static obj_t BGl_za2filezd2lineszd2tableza2z00zztools_locationz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_locationzd2skipzd2forwardz00zztools_locationz00(obj_t, int);
	static obj_t BGl_z62findzd2locationzf2locz42zztools_locationz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62locationzd2skipzd2forwardz62zztools_locationz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2hashtablezd2zz__hashz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zztools_locationz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__hashz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_IMPORT bool_t fexists(char *);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zztools_locationz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_locationz00(void);
	static obj_t BGl_z62dumpzd2locationzb0zztools_locationz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_filezd2lineszd2zz__r4_input_6_10_2z00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zztools_locationz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztools_locationz00(void);
	BGL_IMPORT obj_t BGl_pwdz00zz__osz00(void);
	static obj_t BGl_z62locationzd2fullzd2fnamez62zztools_locationz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2putz12zc0zz__hashz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_hashtablezd2getzd2zz__hashz00(obj_t, obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1073z00zztools_locationz00,
		BgL_bgl_string1073za700za7za7t1080za7, "~~ ", 3);
	      DEFINE_STRING(BGl_string1074z00zztools_locationz00,
		BgL_bgl_string1074za700za7za7t1081za7, ": ", 2);
	      DEFINE_STRING(BGl_string1075z00zztools_locationz00,
		BgL_bgl_string1075za700za7za7t1082za7, " ", 1);
	      DEFINE_STRING(BGl_string1076z00zztools_locationz00,
		BgL_bgl_string1076za700za7za7t1083za7, "____ ", 5);
	      DEFINE_STRING(BGl_string1077z00zztools_locationz00,
		BgL_bgl_string1077za700za7za7t1084za7, "tools_location", 14);
	      DEFINE_STRING(BGl_string1078z00zztools_locationz00,
		BgL_bgl_string1078za700za7za7t1085za7, "at location ", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_locationzd2shapezd2envz00zztools_locationz00,
		BgL_bgl_za762locationza7d2sh1086z00,
		BGl_z62locationzd2shapezb0zztools_locationz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_locationzd2skipzd2forwardzd2envzd2zztools_locationz00,
		BgL_bgl_za762locationza7d2sk1087z00,
		BGl_z62locationzd2skipzd2forwardz62zztools_locationz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_findzd2locationzd2envz00zztools_locationz00,
		BgL_bgl_za762findza7d2locati1088z00,
		BGl_z62findzd2locationzb0zztools_locationz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpzd2locationzd2envz00zztools_locationz00,
		BgL_bgl_za762dumpza7d2locati1089z00,
		BGl_z62dumpzd2locationzb0zztools_locationz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_findzd2locationzf2loczd2envzf2zztools_locationz00,
		BgL_bgl_za762findza7d2locati1090z00,
		BGl_z62findzd2locationzf2locz42zztools_locationz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_locationzd2fullzd2fnamezd2envzd2zztools_locationz00,
		BgL_bgl_za762locationza7d2fu1091z00,
		BGl_z62locationzd2fullzd2fnamez62zztools_locationz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zztools_locationz00));
		     ADD_ROOT((void
				*) (&BGl_za2filezd2lineszd2tableza2z00zztools_locationz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zztools_locationz00(long
		BgL_checksumz00_230, char *BgL_fromz00_231)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_locationz00))
				{
					BGl_requirezd2initializa7ationz75zztools_locationz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_locationz00();
					BGl_libraryzd2moduleszd2initz00zztools_locationz00();
					BGl_cnstzd2initzd2zztools_locationz00();
					BGl_importedzd2moduleszd2initz00zztools_locationz00();
					return BGl_toplevelzd2initzd2zztools_locationz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_locationz00(void)
	{
		{	/* Tools/location.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"tools_location");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "tools_location");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_location");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"tools_location");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L,
				"tools_location");
			BGl_modulezd2initializa7ationz75zz__hashz00(0L, "tools_location");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"tools_location");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tools_location");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"tools_location");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"tools_location");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_location");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "tools_location");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "tools_location");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztools_locationz00(void)
	{
		{	/* Tools/location.scm 15 */
			{	/* Tools/location.scm 15 */
				obj_t BgL_cportz00_219;

				{	/* Tools/location.scm 15 */
					obj_t BgL_stringz00_226;

					BgL_stringz00_226 = BGl_string1078z00zztools_locationz00;
					{	/* Tools/location.scm 15 */
						obj_t BgL_startz00_227;

						BgL_startz00_227 = BINT(0L);
						{	/* Tools/location.scm 15 */
							obj_t BgL_endz00_228;

							BgL_endz00_228 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_226)));
							{	/* Tools/location.scm 15 */

								BgL_cportz00_219 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_226, BgL_startz00_227, BgL_endz00_228);
				}}}}
				{
					long BgL_iz00_220;

					BgL_iz00_220 = 1L;
				BgL_loopz00_221:
					if ((BgL_iz00_220 == -1L))
						{	/* Tools/location.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tools/location.scm 15 */
							{	/* Tools/location.scm 15 */
								obj_t BgL_arg1079z00_222;

								{	/* Tools/location.scm 15 */

									{	/* Tools/location.scm 15 */
										obj_t BgL_locationz00_224;

										BgL_locationz00_224 = BBOOL(((bool_t) 0));
										{	/* Tools/location.scm 15 */

											BgL_arg1079z00_222 =
												BGl_readz00zz__readerz00(BgL_cportz00_219,
												BgL_locationz00_224);
										}
									}
								}
								{	/* Tools/location.scm 15 */
									int BgL_tmpz00_262;

									BgL_tmpz00_262 = (int) (BgL_iz00_220);
									CNST_TABLE_SET(BgL_tmpz00_262, BgL_arg1079z00_222);
							}}
							{	/* Tools/location.scm 15 */
								int BgL_auxz00_225;

								BgL_auxz00_225 = (int) ((BgL_iz00_220 - 1L));
								{
									long BgL_iz00_267;

									BgL_iz00_267 = (long) (BgL_auxz00_225);
									BgL_iz00_220 = BgL_iz00_267;
									goto BgL_loopz00_221;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_locationz00(void)
	{
		{	/* Tools/location.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztools_locationz00(void)
	{
		{	/* Tools/location.scm 15 */
			return (BGl_za2filezd2lineszd2tableza2z00zztools_locationz00 =
				BGl_makezd2hashtablezd2zz__hashz00(BNIL), BUNSPEC);
		}

	}



/* get-file-lines */
	obj_t BGl_getzd2filezd2linesz00zztools_locationz00(obj_t BgL_filez00_17)
	{
		{	/* Tools/location.scm 33 */
			{	/* Tools/location.scm 34 */
				obj_t BgL_linesz00_51;

				BgL_linesz00_51 =
					BGl_hashtablezd2getzd2zz__hashz00
					(BGl_za2filezd2lineszd2tableza2z00zztools_locationz00,
					BgL_filez00_17);
				if (CBOOL(BgL_linesz00_51))
					{	/* Tools/location.scm 35 */
						return BgL_linesz00_51;
					}
				else
					{	/* Tools/location.scm 36 */
						obj_t BgL_linesz00_52;

						BgL_linesz00_52 =
							BGl_filezd2lineszd2zz__r4_input_6_10_2z00(BgL_filez00_17);
						BGl_hashtablezd2putz12zc0zz__hashz00
							(BGl_za2filezd2lineszd2tableza2z00zztools_locationz00,
							BgL_filez00_17, BgL_linesz00_52);
						return BgL_linesz00_52;
					}
			}
		}

	}



/* pos->line */
	obj_t BGl_poszd2ze3linez31zztools_locationz00(obj_t BgL_filez00_18,
		obj_t BgL_posz00_19)
	{
		{	/* Tools/location.scm 44 */
			{	/* Tools/location.scm 45 */
				obj_t BgL_linesz00_126;

				BgL_linesz00_126 =
					BGl_getzd2filezd2linesz00zztools_locationz00(BgL_filez00_18);
				{	/* Tools/location.scm 46 */
					obj_t BgL__ortest_1012z00_127;

					BgL__ortest_1012z00_127 =
						BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(CINT
						(BgL_posz00_19), BgL_linesz00_126);
					if (CBOOL(BgL__ortest_1012z00_127))
						{	/* Tools/location.scm 46 */
							return BgL__ortest_1012z00_127;
						}
					else
						{	/* Tools/location.scm 46 */
							return BINT(0L);
						}
				}
			}
		}

	}



/* find-location */
	BGL_EXPORTED_DEF obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t
		BgL_expz00_20)
	{
		{	/* Tools/location.scm 51 */
			{
				obj_t BgL_locz00_58;

				if (EPAIRP(BgL_expz00_20))
					{	/* Tools/location.scm 63 */
						obj_t BgL_arg1033z00_57;

						BgL_arg1033z00_57 = CER(((obj_t) BgL_expz00_20));
						BgL_locz00_58 = BgL_arg1033z00_57;
						{	/* Tools/location.scm 53 */
							bool_t BgL_test1097z00_286;

							if (STRUCTP(BgL_locz00_58))
								{	/* Tools/location.scm 53 */
									BgL_test1097z00_286 =
										(STRUCT_KEY(BgL_locz00_58) == CNST_TABLE_REF(0));
								}
							else
								{	/* Tools/location.scm 53 */
									BgL_test1097z00_286 = ((bool_t) 0);
								}
							if (BgL_test1097z00_286)
								{	/* Tools/location.scm 53 */
									return BgL_locz00_58;
								}
							else
								{	/* Tools/location.scm 53 */
									if (PAIRP(BgL_locz00_58))
										{	/* Tools/location.scm 55 */
											obj_t BgL_cdrzd2109zd2_67;

											BgL_cdrzd2109zd2_67 = CDR(((obj_t) BgL_locz00_58));
											if ((CAR(((obj_t) BgL_locz00_58)) == CNST_TABLE_REF(1)))
												{	/* Tools/location.scm 55 */
													if (PAIRP(BgL_cdrzd2109zd2_67))
														{	/* Tools/location.scm 55 */
															obj_t BgL_cdrzd2113zd2_71;

															BgL_cdrzd2113zd2_71 = CDR(BgL_cdrzd2109zd2_67);
															if (PAIRP(BgL_cdrzd2113zd2_71))
																{	/* Tools/location.scm 55 */
																	if (NULLP(CDR(BgL_cdrzd2113zd2_71)))
																		{	/* Tools/location.scm 55 */
																			obj_t BgL_arg1044z00_75;
																			obj_t BgL_arg1045z00_76;

																			BgL_arg1044z00_75 =
																				CAR(BgL_cdrzd2109zd2_67);
																			BgL_arg1045z00_76 =
																				CAR(BgL_cdrzd2113zd2_71);
																			{	/* Tools/location.scm 57 */
																				obj_t BgL_arg1048z00_145;

																				BgL_arg1048z00_145 =
																					BGl_poszd2ze3linez31zztools_locationz00
																					(BgL_arg1044z00_75,
																					BgL_arg1045z00_76);
																				{	/* Tools/location.scm 57 */
																					obj_t BgL_newz00_146;

																					BgL_newz00_146 =
																						create_struct(CNST_TABLE_REF(0),
																						(int) (3L));
																					{	/* Tools/location.scm 57 */
																						int BgL_tmpz00_315;

																						BgL_tmpz00_315 = (int) (2L);
																						STRUCT_SET(BgL_newz00_146,
																							BgL_tmpz00_315,
																							BgL_arg1048z00_145);
																					}
																					{	/* Tools/location.scm 57 */
																						int BgL_tmpz00_318;

																						BgL_tmpz00_318 = (int) (1L);
																						STRUCT_SET(BgL_newz00_146,
																							BgL_tmpz00_318,
																							BgL_arg1045z00_76);
																					}
																					{	/* Tools/location.scm 57 */
																						int BgL_tmpz00_321;

																						BgL_tmpz00_321 = (int) (0L);
																						STRUCT_SET(BgL_newz00_146,
																							BgL_tmpz00_321,
																							BgL_arg1044z00_75);
																					}
																					return BgL_newz00_146;
																				}
																			}
																		}
																	else
																		{	/* Tools/location.scm 55 */
																			return BFALSE;
																		}
																}
															else
																{	/* Tools/location.scm 55 */
																	return BFALSE;
																}
														}
													else
														{	/* Tools/location.scm 55 */
															return BFALSE;
														}
												}
											else
												{	/* Tools/location.scm 55 */
													return BFALSE;
												}
										}
									else
										{	/* Tools/location.scm 55 */
											return BFALSE;
										}
								}
						}
					}
				else
					{	/* Tools/location.scm 61 */
						return BFALSE;
					}
			}
		}

	}



/* &find-location */
	obj_t BGl_z62findzd2locationzb0zztools_locationz00(obj_t BgL_envz00_202,
		obj_t BgL_expz00_203)
	{
		{	/* Tools/location.scm 51 */
			return BGl_findzd2locationzd2zztools_locationz00(BgL_expz00_203);
		}

	}



/* find-location/loc */
	BGL_EXPORTED_DEF obj_t BGl_findzd2locationzf2locz20zztools_locationz00(obj_t
		BgL_expz00_21, obj_t BgL_locz00_22)
	{
		{	/* Tools/location.scm 70 */
			{	/* Tools/location.scm 71 */
				obj_t BgL_newzd2loczd2_151;

				BgL_newzd2loczd2_151 =
					BGl_findzd2locationzd2zztools_locationz00(BgL_expz00_21);
				{	/* Tools/location.scm 72 */
					bool_t BgL_test1104z00_326;

					if (STRUCTP(BgL_newzd2loczd2_151))
						{	/* Tools/location.scm 72 */
							BgL_test1104z00_326 =
								(STRUCT_KEY(BgL_newzd2loczd2_151) == CNST_TABLE_REF(0));
						}
					else
						{	/* Tools/location.scm 72 */
							BgL_test1104z00_326 = ((bool_t) 0);
						}
					if (BgL_test1104z00_326)
						{	/* Tools/location.scm 72 */
							return BgL_newzd2loczd2_151;
						}
					else
						{	/* Tools/location.scm 72 */
							return BgL_locz00_22;
						}
				}
			}
		}

	}



/* &find-location/loc */
	obj_t BGl_z62findzd2locationzf2locz42zztools_locationz00(obj_t BgL_envz00_204,
		obj_t BgL_expz00_205, obj_t BgL_locz00_206)
	{
		{	/* Tools/location.scm 70 */
			return
				BGl_findzd2locationzf2locz20zztools_locationz00(BgL_expz00_205,
				BgL_locz00_206);
		}

	}



/* location-full-fname */
	BGL_EXPORTED_DEF obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t
		BgL_locz00_23)
	{
		{	/* Tools/location.scm 79 */
			{	/* Tools/location.scm 80 */
				obj_t BgL_filezd2namezd2_83;

				BgL_filezd2namezd2_83 = STRUCT_REF(((obj_t) BgL_locz00_23), (int) (0L));
				{	/* Tools/location.scm 80 */
					obj_t BgL_fullzd2namezd2_84;

					BgL_fullzd2namezd2_84 =
						BGl_makezd2filezd2namez00zz__osz00(BGl_pwdz00zz__osz00(),
						BgL_filezd2namezd2_83);
					{	/* Tools/location.scm 81 */

						if (fexists(BSTRING_TO_STRING(BgL_fullzd2namezd2_84)))
							{	/* Tools/location.scm 82 */
								return BgL_fullzd2namezd2_84;
							}
						else
							{	/* Tools/location.scm 84 */
								obj_t BgL_libzd2namezd2_86;

								BgL_libzd2namezd2_86 =
									BGl_makezd2filezd2namez00zz__osz00
									(BGl_za2libzd2srczd2dirza2z00zzengine_paramz00,
									BgL_filezd2namezd2_83);
								if (fexists(BSTRING_TO_STRING(BgL_libzd2namezd2_86)))
									{	/* Tools/location.scm 85 */
										return BgL_libzd2namezd2_86;
									}
								else
									{	/* Tools/location.scm 85 */
										return BgL_filezd2namezd2_83;
									}
							}
					}
				}
			}
		}

	}



/* &location-full-fname */
	obj_t BGl_z62locationzd2fullzd2fnamez62zztools_locationz00(obj_t
		BgL_envz00_207, obj_t BgL_locz00_208)
	{
		{	/* Tools/location.scm 79 */
			return BGl_locationzd2fullzd2fnamez00zztools_locationz00(BgL_locz00_208);
		}

	}



/* location-shape */
	BGL_EXPORTED_DEF obj_t BGl_locationzd2shapezd2zztools_locationz00(obj_t
		BgL_locz00_24, obj_t BgL_lz00_25)
	{
		{	/* Tools/location.scm 92 */
			{	/* Tools/location.scm 93 */
				bool_t BgL_test1108z00_346;

				if (CBOOL(BGl_za2locationzd2shapezf3za2z21zzengine_paramz00))
					{	/* Tools/location.scm 93 */
						if (STRUCTP(BgL_locz00_24))
							{	/* Tools/location.scm 93 */
								BgL_test1108z00_346 =
									(STRUCT_KEY(BgL_locz00_24) == CNST_TABLE_REF(0));
							}
						else
							{	/* Tools/location.scm 93 */
								BgL_test1108z00_346 = ((bool_t) 0);
							}
					}
				else
					{	/* Tools/location.scm 93 */
						BgL_test1108z00_346 = ((bool_t) 0);
					}
				if (BgL_test1108z00_346)
					{	/* Tools/location.scm 94 */
						obj_t BgL_arg1054z00_90;

						{	/* Tools/location.scm 94 */
							obj_t BgL_v1013z00_91;

							BgL_v1013z00_91 = create_vector(3L);
							{	/* Tools/location.scm 94 */
								obj_t BgL_arg1055z00_92;

								{	/* Tools/location.scm 94 */
									obj_t BgL_arg1056z00_93;

									BgL_arg1056z00_93 = STRUCT_REF(BgL_locz00_24, (int) (0L));
									BgL_arg1055z00_92 =
										bstring_to_symbol(((obj_t) BgL_arg1056z00_93));
								}
								VECTOR_SET(BgL_v1013z00_91, 0L, BgL_arg1055z00_92);
							}
							VECTOR_SET(BgL_v1013z00_91, 1L,
								STRUCT_REF(BgL_locz00_24, (int) (1L)));
							VECTOR_SET(BgL_v1013z00_91, 2L,
								STRUCT_REF(BgL_locz00_24, (int) (2L)));
							BgL_arg1054z00_90 = BgL_v1013z00_91;
						}
						return MAKE_YOUNG_PAIR(BgL_arg1054z00_90, BgL_lz00_25);
					}
				else
					{	/* Tools/location.scm 93 */
						return BgL_lz00_25;
					}
			}
		}

	}



/* &location-shape */
	obj_t BGl_z62locationzd2shapezb0zztools_locationz00(obj_t BgL_envz00_209,
		obj_t BgL_locz00_210, obj_t BgL_lz00_211)
	{
		{	/* Tools/location.scm 92 */
			return
				BGl_locationzd2shapezd2zztools_locationz00(BgL_locz00_210,
				BgL_lz00_211);
		}

	}



/* dump-location */
	BGL_EXPORTED_DEF obj_t BGl_dumpzd2locationzd2zztools_locationz00(obj_t
		BgL_fromz00_26, obj_t BgL_exprz00_27)
	{
		{	/* Tools/location.scm 104 */
			{	/* Tools/location.scm 105 */
				obj_t BgL_port1014z00_96;

				{	/* Tools/location.scm 105 */
					obj_t BgL_tmpz00_368;

					BgL_tmpz00_368 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1014z00_96 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_368);
				}
				bgl_display_string(BGl_string1073z00zztools_locationz00,
					BgL_port1014z00_96);
				bgl_display_obj(BgL_fromz00_26, BgL_port1014z00_96);
				bgl_display_string(BGl_string1074z00zztools_locationz00,
					BgL_port1014z00_96);
				bgl_display_obj(BgL_exprz00_27, BgL_port1014z00_96);
				bgl_display_string(BGl_string1075z00zztools_locationz00,
					BgL_port1014z00_96);
				bgl_display_obj(BGl_findzd2locationzd2zztools_locationz00
					(BgL_exprz00_27), BgL_port1014z00_96);
				bgl_display_char(((unsigned char) 10), BgL_port1014z00_96);
			}
			{
				obj_t BgL_exprz00_99;

				{	/* Tools/location.scm 106 */
					bool_t BgL_tmpz00_379;

					BgL_exprz00_99 = BgL_exprz00_27;
				BgL_zc3z04anonymousza31060ze3z87_100:
					if (PAIRP(BgL_exprz00_99))
						{	/* Tools/location.scm 107 */
							{	/* Tools/location.scm 109 */
								obj_t BgL_port1015z00_102;

								{	/* Tools/location.scm 109 */
									obj_t BgL_tmpz00_382;

									BgL_tmpz00_382 = BGL_CURRENT_DYNAMIC_ENV();
									BgL_port1015z00_102 =
										BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_382);
								}
								bgl_display_string(BGl_string1076z00zztools_locationz00,
									BgL_port1015z00_102);
								bgl_display_obj(BgL_fromz00_26, BgL_port1015z00_102);
								bgl_display_string(BGl_string1074z00zztools_locationz00,
									BgL_port1015z00_102);
								bgl_display_obj(BgL_exprz00_99, BgL_port1015z00_102);
								bgl_display_string(BGl_string1075z00zztools_locationz00,
									BgL_port1015z00_102);
								bgl_display_obj(BGl_findzd2locationzd2zztools_locationz00
									(BgL_exprz00_99), BgL_port1015z00_102);
								bgl_display_char(((unsigned char) 10), BgL_port1015z00_102);
							}
							{
								obj_t BgL_exprz00_393;

								BgL_exprz00_393 = CDR(BgL_exprz00_99);
								BgL_exprz00_99 = BgL_exprz00_393;
								goto BgL_zc3z04anonymousza31060ze3z87_100;
							}
						}
					else
						{	/* Tools/location.scm 107 */
							BgL_tmpz00_379 = ((bool_t) 0);
						}
					return BBOOL(BgL_tmpz00_379);
				}
			}
		}

	}



/* &dump-location */
	obj_t BGl_z62dumpzd2locationzb0zztools_locationz00(obj_t BgL_envz00_212,
		obj_t BgL_fromz00_213, obj_t BgL_exprz00_214)
	{
		{	/* Tools/location.scm 104 */
			return
				BGl_dumpzd2locationzd2zztools_locationz00(BgL_fromz00_213,
				BgL_exprz00_214);
		}

	}



/* location-skip-forward */
	BGL_EXPORTED_DEF obj_t
		BGl_locationzd2skipzd2forwardz00zztools_locationz00(obj_t BgL_locz00_28,
		int BgL_skipz00_29)
	{
		{	/* Tools/location.scm 116 */
			{	/* Tools/location.scm 117 */
				bool_t BgL_test1112z00_397;

				if (STRUCTP(BgL_locz00_28))
					{	/* Tools/location.scm 117 */
						BgL_test1112z00_397 =
							(STRUCT_KEY(BgL_locz00_28) == CNST_TABLE_REF(0));
					}
				else
					{	/* Tools/location.scm 117 */
						BgL_test1112z00_397 = ((bool_t) 0);
					}
				if (BgL_test1112z00_397)
					{	/* Tools/location.scm 118 */
						obj_t BgL_fnamez00_107;
						long BgL_nposz00_108;

						BgL_fnamez00_107 = STRUCT_REF(BgL_locz00_28, (int) (0L));
						BgL_nposz00_108 =
							(
							(long) CINT(STRUCT_REF(BgL_locz00_28,
									(int) (1L))) + (long) (BgL_skipz00_29));
						{	/* Tools/location.scm 120 */
							obj_t BgL_arg1065z00_109;

							{	/* Tools/location.scm 45 */
								obj_t BgL_linesz00_196;

								BgL_linesz00_196 =
									BGl_getzd2filezd2linesz00zztools_locationz00
									(BgL_fnamez00_107);
								{	/* Tools/location.scm 46 */
									obj_t BgL__ortest_1012z00_197;

									BgL__ortest_1012z00_197 =
										BGl_filezd2positionzd2ze3lineze3zz__r4_input_6_10_2z00(
										(int) (BgL_nposz00_108), BgL_linesz00_196);
									if (CBOOL(BgL__ortest_1012z00_197))
										{	/* Tools/location.scm 46 */
											BgL_arg1065z00_109 = BgL__ortest_1012z00_197;
										}
									else
										{	/* Tools/location.scm 46 */
											BgL_arg1065z00_109 = BINT(0L);
										}
								}
							}
							{	/* Tools/location.scm 120 */
								obj_t BgL_newz00_198;

								BgL_newz00_198 = create_struct(CNST_TABLE_REF(0), (int) (3L));
								{	/* Tools/location.scm 120 */
									int BgL_tmpz00_419;

									BgL_tmpz00_419 = (int) (2L);
									STRUCT_SET(BgL_newz00_198, BgL_tmpz00_419,
										BgL_arg1065z00_109);
								}
								{	/* Tools/location.scm 120 */
									obj_t BgL_auxz00_424;
									int BgL_tmpz00_422;

									BgL_auxz00_424 = BINT(BgL_nposz00_108);
									BgL_tmpz00_422 = (int) (1L);
									STRUCT_SET(BgL_newz00_198, BgL_tmpz00_422, BgL_auxz00_424);
								}
								{	/* Tools/location.scm 120 */
									int BgL_tmpz00_427;

									BgL_tmpz00_427 = (int) (0L);
									STRUCT_SET(BgL_newz00_198, BgL_tmpz00_427, BgL_fnamez00_107);
								}
								return BgL_newz00_198;
							}
						}
					}
				else
					{	/* Tools/location.scm 117 */
						return BgL_locz00_28;
					}
			}
		}

	}



/* &location-skip-forward */
	obj_t BGl_z62locationzd2skipzd2forwardz62zztools_locationz00(obj_t
		BgL_envz00_215, obj_t BgL_locz00_216, obj_t BgL_skipz00_217)
	{
		{	/* Tools/location.scm 116 */
			return
				BGl_locationzd2skipzd2forwardz00zztools_locationz00(BgL_locz00_216,
				CINT(BgL_skipz00_217));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_locationz00(void)
	{
		{	/* Tools/location.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_locationz00(void)
	{
		{	/* Tools/location.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_locationz00(void)
	{
		{	/* Tools/location.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztools_locationz00(void)
	{
		{	/* Tools/location.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1077z00zztools_locationz00));
		}

	}

#ifdef __cplusplus
}
#endif
