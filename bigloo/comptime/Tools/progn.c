/*===========================================================================*/
/*   (Tools/progn.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/progn.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_PROGN_TYPE_DEFINITIONS
#define BGL_TOOLS_PROGN_TYPE_DEFINITIONS
#endif													// BGL_TOOLS_PROGN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_normaliza7ezd2prognz75zztools_prognz00(obj_t);
	static obj_t BGl_z62normaliza7ezd2prognz17zztools_prognz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztools_prognz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_normaliza7ezd2prognzf2locz87zztools_prognz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zztools_prognz00(void);
	extern obj_t BGl_epairifyz00zztools_miscz00(obj_t, obj_t);
	static obj_t BGl_z62prognzd2firstzd2expressionz62zztools_prognz00(obj_t,
		obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zztools_prognz00(void);
	static obj_t BGl_objectzd2initzd2zztools_prognz00(void);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zztools_prognz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztools_prognz00(void);
	static obj_t BGl_z62emapz62zztools_prognz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_normaliza7ezd2beginz75zztools_prognz00(obj_t);
	static obj_t BGl_z62normaliza7ezd2beginz17zztools_prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__prognz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_emapz00zztools_prognz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prognzd2tailzd2expressionsz00zztools_prognz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zztools_prognz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_prognz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztools_prognz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztools_prognz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_prognzd2firstzd2expressionz00zztools_prognz00(obj_t);
	static obj_t BGl_z62normaliza7ezd2prognzf2locze5zztools_prognz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_loopze70ze7zztools_prognz00(obj_t, obj_t, obj_t);
	static obj_t BGl_loopze71ze7zztools_prognz00(obj_t);
	static obj_t BGl_z62prognzd2tailzd2expressionsz62zztools_prognz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_expandzd2prognzd2zz__prognz00(obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_normaliza7ezd2prognzf2loczd2envz55zztools_prognz00,
		BgL_bgl_za762normaliza7a7eza7d1217za7,
		BGl_z62normaliza7ezd2prognzf2locze5zztools_prognz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prognzd2tailzd2expressionszd2envzd2zztools_prognz00,
		BgL_bgl_za762prognza7d2tailza71218za7,
		BGl_z62prognzd2tailzd2expressionsz62zztools_prognz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_normaliza7ezd2prognzd2envza7zztools_prognz00,
		BgL_bgl_za762normaliza7a7eza7d1219za7,
		BGl_z62normaliza7ezd2prognz17zztools_prognz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_prognzd2firstzd2expressionzd2envzd2zztools_prognz00,
		BgL_bgl_za762prognza7d2first1220z00,
		BGl_z62prognzd2firstzd2expressionz62zztools_prognz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_normaliza7ezd2beginzd2envza7zztools_prognz00,
		BgL_bgl_za762normaliza7a7eza7d1221za7,
		BGl_z62normaliza7ezd2beginz17zztools_prognz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_emapzd2envzd2zztools_prognz00,
		BgL_bgl_za762emapza762za7za7tool1222z00, BGl_z62emapz62zztools_prognz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1210z00zztools_prognz00,
		BgL_bgl_string1210za700za7za7t1223za7, "normalize-progn", 15);
	      DEFINE_STRING(BGl_string1211z00zztools_prognz00,
		BgL_bgl_string1211za700za7za7t1224za7, "Illegal expression", 18);
	      DEFINE_STRING(BGl_string1212z00zztools_prognz00,
		BgL_bgl_string1212za700za7za7t1225za7, "Illegal form", 12);
	      DEFINE_STRING(BGl_string1213z00zztools_prognz00,
		BgL_bgl_string1213za700za7za7t1226za7, "Illegal parameter list", 22);
	      DEFINE_STRING(BGl_string1214z00zztools_prognz00,
		BgL_bgl_string1214za700za7za7t1227za7, "tools_progn", 11);
	      DEFINE_STRING(BGl_string1215z00zztools_prognz00,
		BgL_bgl_string1215za700za7za7t1228za7, "emap normalize-begin begin ", 27);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztools_prognz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztools_prognz00(long
		BgL_checksumz00_304, char *BgL_fromz00_305)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_prognz00))
				{
					BGl_requirezd2initializa7ationz75zztools_prognz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_prognz00();
					BGl_libraryzd2moduleszd2initz00zztools_prognz00();
					BGl_cnstzd2initzd2zztools_prognz00();
					BGl_importedzd2moduleszd2initz00zztools_prognz00();
					return BGl_toplevelzd2initzd2zztools_prognz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_prognz00(void)
	{
		{	/* Tools/progn.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_progn");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_progn");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tools_progn");
			BGl_modulezd2initializa7ationz75zz__prognz00(0L, "tools_progn");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "tools_progn");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "tools_progn");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "tools_progn");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztools_prognz00(void)
	{
		{	/* Tools/progn.scm 15 */
			{	/* Tools/progn.scm 15 */
				obj_t BgL_cportz00_293;

				{	/* Tools/progn.scm 15 */
					obj_t BgL_stringz00_300;

					BgL_stringz00_300 = BGl_string1215z00zztools_prognz00;
					{	/* Tools/progn.scm 15 */
						obj_t BgL_startz00_301;

						BgL_startz00_301 = BINT(0L);
						{	/* Tools/progn.scm 15 */
							obj_t BgL_endz00_302;

							BgL_endz00_302 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_300)));
							{	/* Tools/progn.scm 15 */

								BgL_cportz00_293 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_300, BgL_startz00_301, BgL_endz00_302);
				}}}}
				{
					long BgL_iz00_294;

					BgL_iz00_294 = 2L;
				BgL_loopz00_295:
					if ((BgL_iz00_294 == -1L))
						{	/* Tools/progn.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tools/progn.scm 15 */
							{	/* Tools/progn.scm 15 */
								obj_t BgL_arg1216z00_296;

								{	/* Tools/progn.scm 15 */

									{	/* Tools/progn.scm 15 */
										obj_t BgL_locationz00_298;

										BgL_locationz00_298 = BBOOL(((bool_t) 0));
										{	/* Tools/progn.scm 15 */

											BgL_arg1216z00_296 =
												BGl_readz00zz__readerz00(BgL_cportz00_293,
												BgL_locationz00_298);
										}
									}
								}
								{	/* Tools/progn.scm 15 */
									int BgL_tmpz00_330;

									BgL_tmpz00_330 = (int) (BgL_iz00_294);
									CNST_TABLE_SET(BgL_tmpz00_330, BgL_arg1216z00_296);
							}}
							{	/* Tools/progn.scm 15 */
								int BgL_auxz00_299;

								BgL_auxz00_299 = (int) ((BgL_iz00_294 - 1L));
								{
									long BgL_iz00_335;

									BgL_iz00_335 = (long) (BgL_auxz00_299);
									BgL_iz00_294 = BgL_iz00_335;
									goto BgL_loopz00_295;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_prognz00(void)
	{
		{	/* Tools/progn.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztools_prognz00(void)
	{
		{	/* Tools/progn.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zztools_prognz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_25;

				BgL_headz00_25 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_26;
					obj_t BgL_tailz00_27;

					BgL_prevz00_26 = BgL_headz00_25;
					BgL_tailz00_27 = BgL_l1z00_1;
				BgL_loopz00_28:
					if (PAIRP(BgL_tailz00_27))
						{
							obj_t BgL_newzd2prevzd2_30;

							BgL_newzd2prevzd2_30 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_27), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_26, BgL_newzd2prevzd2_30);
							{
								obj_t BgL_tailz00_345;
								obj_t BgL_prevz00_344;

								BgL_prevz00_344 = BgL_newzd2prevzd2_30;
								BgL_tailz00_345 = CDR(BgL_tailz00_27);
								BgL_tailz00_27 = BgL_tailz00_345;
								BgL_prevz00_26 = BgL_prevz00_344;
								goto BgL_loopz00_28;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_25);
				}
			}
		}

	}



/* normalize-progn */
	BGL_EXPORTED_DEF obj_t BGl_normaliza7ezd2prognz75zztools_prognz00(obj_t
		BgL_bodyza2za2_17)
	{
		{	/* Tools/progn.scm 31 */
		BGl_normaliza7ezd2prognz75zztools_prognz00:
			if (PAIRP(BgL_bodyza2za2_17))
				{	/* Tools/progn.scm 33 */
					if (NULLP(CDR(BgL_bodyza2za2_17)))
						{	/* Tools/progn.scm 36 */
							obj_t BgL_ezd2105zd2_54;

							BgL_ezd2105zd2_54 = CAR(BgL_bodyza2za2_17);
							if (PAIRP(BgL_ezd2105zd2_54))
								{	/* Tools/progn.scm 36 */
									if ((CAR(BgL_ezd2105zd2_54) == CNST_TABLE_REF(0)))
										{	/* Tools/progn.scm 36 */
											if (NULLP(CDR(BgL_ezd2105zd2_54)))
												{	/* Tools/progn.scm 36 */
													return BUNSPEC;
												}
											else
												{	/* Tools/progn.scm 36 */
													obj_t BgL_cdrzd2112zd2_60;

													BgL_cdrzd2112zd2_60 = CDR(BgL_ezd2105zd2_54);
													if (PAIRP(BgL_cdrzd2112zd2_60))
														{	/* Tools/progn.scm 36 */
															if (NULLP(CDR(BgL_cdrzd2112zd2_60)))
																{	/* Tools/progn.scm 36 */
																	return BgL_ezd2105zd2_54;
																}
															else
																{
																	obj_t BgL_bodyza2za2_369;

																	BgL_bodyza2za2_369 = BgL_cdrzd2112zd2_60;
																	BgL_bodyza2za2_17 = BgL_bodyza2za2_369;
																	goto
																		BGl_normaliza7ezd2prognz75zztools_prognz00;
																}
														}
													else
														{
															obj_t BgL_bodyza2za2_370;

															BgL_bodyza2za2_370 = BgL_cdrzd2112zd2_60;
															BgL_bodyza2za2_17 = BgL_bodyza2za2_370;
															goto BGl_normaliza7ezd2prognz75zztools_prognz00;
														}
												}
										}
									else
										{	/* Tools/progn.scm 36 */
											return BgL_ezd2105zd2_54;
										}
								}
							else
								{	/* Tools/progn.scm 36 */
									return BgL_ezd2105zd2_54;
								}
						}
					else
						{	/* Tools/progn.scm 46 */
							obj_t BgL_subz00_70;

							{	/* Tools/progn.scm 46 */
								obj_t BgL_g1012z00_73;

								if ((CAR(BgL_bodyza2za2_17) == CNST_TABLE_REF(0)))
									{	/* Tools/progn.scm 46 */
										BgL_g1012z00_73 = CDR(BgL_bodyza2za2_17);
									}
								else
									{	/* Tools/progn.scm 46 */
										BgL_g1012z00_73 = BgL_bodyza2za2_17;
									}
								BgL_subz00_70 =
									BGl_loopze71ze7zztools_prognz00(BgL_g1012z00_73);
							}
							if (EPAIRP(BgL_bodyza2za2_17))
								{	/* Tools/progn.scm 69 */
									obj_t BgL_arg1048z00_72;

									BgL_arg1048z00_72 = CER(BgL_bodyza2za2_17);
									{	/* Tools/progn.scm 69 */
										obj_t BgL_res1205z00_244;

										{	/* Tools/progn.scm 69 */
											obj_t BgL_obj1z00_243;

											BgL_obj1z00_243 = CNST_TABLE_REF(0);
											BgL_res1205z00_244 =
												MAKE_YOUNG_EPAIR(BgL_obj1z00_243, BgL_subz00_70,
												BgL_arg1048z00_72);
										}
										return BgL_res1205z00_244;
									}
								}
							else
								{	/* Tools/progn.scm 68 */
									return MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_subz00_70);
								}
						}
				}
			else
				{	/* Tools/progn.scm 33 */
					return
						BGl_internalzd2errorzd2zztools_errorz00
						(BGl_string1210z00zztools_prognz00,
						BGl_string1211z00zztools_prognz00, BgL_bodyza2za2_17);
				}
		}

	}



/* loop~1 */
	obj_t BGl_loopze71ze7zztools_prognz00(obj_t BgL_bodyza2za2_75)
	{
		{	/* Tools/progn.scm 46 */
		BGl_loopze71ze7zztools_prognz00:
			if (NULLP(BgL_bodyza2za2_75))
				{	/* Tools/progn.scm 49 */
					return BNIL;
				}
			else
				{	/* Tools/progn.scm 51 */
					obj_t BgL_exprz00_78;

					BgL_exprz00_78 = CAR(((obj_t) BgL_bodyza2za2_75));
					{	/* Tools/progn.scm 52 */
						bool_t BgL_test1245z00_389;

						if (PAIRP(BgL_exprz00_78))
							{	/* Tools/progn.scm 52 */
								BgL_test1245z00_389 =
									(CAR(BgL_exprz00_78) == CNST_TABLE_REF(0));
							}
						else
							{	/* Tools/progn.scm 52 */
								BgL_test1245z00_389 = ((bool_t) 0);
							}
						if (BgL_test1245z00_389)
							{	/* Tools/progn.scm 53 */
								obj_t BgL_arg1054z00_82;
								obj_t BgL_arg1055z00_83;

								BgL_arg1054z00_82 = CDR(BgL_exprz00_78);
								{	/* Tools/progn.scm 53 */
									obj_t BgL_arg1056z00_84;

									BgL_arg1056z00_84 = CDR(((obj_t) BgL_bodyza2za2_75));
									BgL_arg1055z00_83 =
										BGl_loopze71ze7zztools_prognz00(BgL_arg1056z00_84);
								}
								return
									BGl_appendzd221011zd2zztools_prognz00(BgL_arg1054z00_82,
									BgL_arg1055z00_83);
							}
						else
							{	/* Tools/progn.scm 55 */
								bool_t BgL_test1247z00_400;

								if ((BgL_exprz00_78 == BUNSPEC))
									{	/* Tools/progn.scm 56 */
										obj_t BgL_tmpz00_403;

										BgL_tmpz00_403 = CDR(((obj_t) BgL_bodyza2za2_75));
										BgL_test1247z00_400 = PAIRP(BgL_tmpz00_403);
									}
								else
									{	/* Tools/progn.scm 55 */
										BgL_test1247z00_400 = ((bool_t) 0);
									}
								if (BgL_test1247z00_400)
									{	/* Tools/progn.scm 57 */
										obj_t BgL_arg1059z00_87;

										BgL_arg1059z00_87 = CDR(((obj_t) BgL_bodyza2za2_75));
										{
											obj_t BgL_bodyza2za2_409;

											BgL_bodyza2za2_409 = BgL_arg1059z00_87;
											BgL_bodyza2za2_75 = BgL_bodyza2za2_409;
											goto BGl_loopze71ze7zztools_prognz00;
										}
									}
								else
									{	/* Tools/progn.scm 55 */
										if (EPAIRP(BgL_exprz00_78))
											{	/* Tools/progn.scm 60 */
												obj_t BgL_arg1062z00_89;
												obj_t BgL_arg1063z00_90;

												{	/* Tools/progn.scm 60 */
													obj_t BgL_arg1065z00_91;

													BgL_arg1065z00_91 = CDR(((obj_t) BgL_bodyza2za2_75));
													BgL_arg1062z00_89 =
														BGl_loopze71ze7zztools_prognz00(BgL_arg1065z00_91);
												}
												BgL_arg1063z00_90 = CER(((obj_t) BgL_exprz00_78));
												{	/* Tools/progn.scm 59 */
													obj_t BgL_res1203z00_237;

													BgL_res1203z00_237 =
														MAKE_YOUNG_EPAIR(BgL_exprz00_78, BgL_arg1062z00_89,
														BgL_arg1063z00_90);
													return BgL_res1203z00_237;
												}
											}
										else
											{	/* Tools/progn.scm 58 */
												if (EPAIRP(BgL_bodyza2za2_75))
													{	/* Tools/progn.scm 64 */
														obj_t BgL_arg1068z00_93;
														obj_t BgL_arg1074z00_94;

														{	/* Tools/progn.scm 64 */
															obj_t BgL_arg1075z00_95;

															BgL_arg1075z00_95 =
																CDR(((obj_t) BgL_bodyza2za2_75));
															BgL_arg1068z00_93 =
																BGl_loopze71ze7zztools_prognz00
																(BgL_arg1075z00_95);
														}
														BgL_arg1074z00_94 =
															CER(((obj_t) BgL_bodyza2za2_75));
														{	/* Tools/progn.scm 63 */
															obj_t BgL_res1204z00_240;

															BgL_res1204z00_240 =
																MAKE_YOUNG_EPAIR(BgL_exprz00_78,
																BgL_arg1068z00_93, BgL_arg1074z00_94);
															return BgL_res1204z00_240;
														}
													}
												else
													{	/* Tools/progn.scm 67 */
														obj_t BgL_arg1076z00_96;

														{	/* Tools/progn.scm 67 */
															obj_t BgL_arg1078z00_97;

															BgL_arg1078z00_97 =
																CDR(((obj_t) BgL_bodyza2za2_75));
															BgL_arg1076z00_96 =
																BGl_loopze71ze7zztools_prognz00
																(BgL_arg1078z00_97);
														}
														return
															MAKE_YOUNG_PAIR(BgL_exprz00_78,
															BgL_arg1076z00_96);
													}
											}
									}
							}
					}
				}
		}

	}



/* &normalize-progn */
	obj_t BGl_z62normaliza7ezd2prognz17zztools_prognz00(obj_t BgL_envz00_276,
		obj_t BgL_bodyza2za2_277)
	{
		{	/* Tools/progn.scm 31 */
			return BGl_normaliza7ezd2prognz75zztools_prognz00(BgL_bodyza2za2_277);
		}

	}



/* normalize-begin */
	BGL_EXPORTED_DEF obj_t BGl_normaliza7ezd2beginz75zztools_prognz00(obj_t
		BgL_begz00_18)
	{
		{	/* Tools/progn.scm 75 */
		BGl_normaliza7ezd2beginz75zztools_prognz00:
			{	/* Tools/progn.scm 76 */
				obj_t BgL_bodyz00_106;

				BgL_bodyz00_106 = CDR(((obj_t) BgL_begz00_18));
				if (NULLP(BgL_bodyz00_106))
					{	/* Tools/progn.scm 78 */
						return BUNSPEC;
					}
				else
					{	/* Tools/progn.scm 78 */
						if (NULLP(CDR(((obj_t) BgL_bodyz00_106))))
							{	/* Tools/progn.scm 81 */
								obj_t BgL_ez00_110;

								BgL_ez00_110 = CAR(((obj_t) BgL_bodyz00_106));
								if (PAIRP(BgL_ez00_110))
									{	/* Tools/progn.scm 82 */
										if ((CAR(((obj_t) BgL_ez00_110)) == CNST_TABLE_REF(0)))
											{
												obj_t BgL_begz00_448;

												BgL_begz00_448 = BgL_ez00_110;
												BgL_begz00_18 = BgL_begz00_448;
												goto BGl_normaliza7ezd2beginz75zztools_prognz00;
											}
										else
											{	/* Tools/progn.scm 82 */
												return BgL_ez00_110;
											}
									}
								else
									{	/* Tools/progn.scm 82 */
										return BgL_ez00_110;
									}
							}
						else
							{
								obj_t BgL_lz00_122;
								obj_t BgL_resz00_123;

								BgL_lz00_122 = BgL_bodyz00_106;
								BgL_resz00_123 = BNIL;
							BgL_zc3z04anonymousza31098ze3z87_124:
								if (NULLP(BgL_lz00_122))
									{	/* Tools/progn.scm 91 */
										if (NULLP(BgL_resz00_123))
											{	/* Tools/progn.scm 93 */
												return BUNSPEC;
											}
										else
											{	/* Tools/progn.scm 93 */
												if (NULLP(CDR(BgL_resz00_123)))
													{	/* Tools/progn.scm 95 */
														return CAR(BgL_resz00_123);
													}
												else
													{	/* Tools/progn.scm 98 */
														obj_t BgL_arg1104z00_129;

														{	/* Tools/progn.scm 98 */
															obj_t BgL_arg1114z00_130;

															BgL_arg1114z00_130 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																(bgl_reverse_bang(BgL_resz00_123), BNIL);
															BgL_arg1104z00_129 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(0),
																BgL_arg1114z00_130);
														}
														return
															BGl_epairifyz00zztools_miscz00(BgL_arg1104z00_129,
															BgL_begz00_18);
													}
											}
									}
								else
									{	/* Tools/progn.scm 91 */
										if (PAIRP(BgL_lz00_122))
											{	/* Tools/progn.scm 100 */
												obj_t BgL_bz00_134;

												BgL_bz00_134 = CAR(BgL_lz00_122);
												{	/* Tools/progn.scm 101 */
													bool_t BgL_test1261z00_465;

													if (PAIRP(BgL_bz00_134))
														{	/* Tools/progn.scm 101 */
															BgL_test1261z00_465 = ((bool_t) 0);
														}
													else
														{	/* Tools/progn.scm 101 */
															if (SYMBOLP(BgL_bz00_134))
																{	/* Tools/progn.scm 101 */
																	BgL_test1261z00_465 = ((bool_t) 0);
																}
															else
																{	/* Tools/progn.scm 101 */
																	obj_t BgL_tmpz00_470;

																	BgL_tmpz00_470 = CDR(BgL_lz00_122);
																	BgL_test1261z00_465 = PAIRP(BgL_tmpz00_470);
																}
														}
													if (BgL_test1261z00_465)
														{
															obj_t BgL_lz00_473;

															BgL_lz00_473 = CDR(BgL_lz00_122);
															BgL_lz00_122 = BgL_lz00_473;
															goto BgL_zc3z04anonymousza31098ze3z87_124;
														}
													else
														{	/* Tools/progn.scm 103 */
															obj_t BgL_arg1131z00_140;
															obj_t BgL_arg1132z00_141;

															BgL_arg1131z00_140 = CDR(BgL_lz00_122);
															BgL_arg1132z00_141 =
																MAKE_YOUNG_PAIR(BgL_bz00_134, BgL_resz00_123);
															{
																obj_t BgL_resz00_478;
																obj_t BgL_lz00_477;

																BgL_lz00_477 = BgL_arg1131z00_140;
																BgL_resz00_478 = BgL_arg1132z00_141;
																BgL_resz00_123 = BgL_resz00_478;
																BgL_lz00_122 = BgL_lz00_477;
																goto BgL_zc3z04anonymousza31098ze3z87_124;
															}
														}
												}
											}
										else
											{	/* Tools/progn.scm 99 */
												return
													BGl_internalzd2errorzd2zztools_errorz00(CNST_TABLE_REF
													(1), BGl_string1212z00zztools_prognz00,
													BgL_begz00_18);
											}
									}
							}
					}
			}
		}

	}



/* &normalize-begin */
	obj_t BGl_z62normaliza7ezd2beginz17zztools_prognz00(obj_t BgL_envz00_278,
		obj_t BgL_begz00_279)
	{
		{	/* Tools/progn.scm 75 */
			return BGl_normaliza7ezd2beginz75zztools_prognz00(BgL_begz00_279);
		}

	}



/* normalize-progn/loc */
	BGL_EXPORTED_DEF obj_t BGl_normaliza7ezd2prognzf2locz87zztools_prognz00(obj_t
		BgL_bodyza2za2_19, obj_t BgL_locz00_20)
	{
		{	/* Tools/progn.scm 110 */
			{	/* Tools/progn.scm 111 */
				obj_t BgL_nbodyz00_147;

				BgL_nbodyz00_147 = BGl_expandzd2prognzd2zz__prognz00(BgL_bodyza2za2_19);
				if (CBOOL(BgL_locz00_20))
					{	/* Tools/progn.scm 113 */
						if (EPAIRP(BgL_nbodyz00_147))
							{	/* Tools/progn.scm 115 */
								return BgL_nbodyz00_147;
							}
						else
							{	/* Tools/progn.scm 115 */
								if (PAIRP(BgL_nbodyz00_147))
									{	/* Tools/progn.scm 118 */
										obj_t BgL_arg1141z00_150;
										obj_t BgL_arg1142z00_151;

										BgL_arg1141z00_150 = CAR(BgL_nbodyz00_147);
										BgL_arg1142z00_151 = CDR(BgL_nbodyz00_147);
										{	/* Tools/progn.scm 118 */
											obj_t BgL_res1206z00_258;

											BgL_res1206z00_258 =
												MAKE_YOUNG_EPAIR(BgL_arg1141z00_150, BgL_arg1142z00_151,
												BgL_locz00_20);
											return BgL_res1206z00_258;
										}
									}
								else
									{	/* Tools/progn.scm 120 */
										obj_t BgL_arg1143z00_152;

										{	/* Tools/progn.scm 120 */
											obj_t BgL_list1144z00_153;

											BgL_list1144z00_153 =
												MAKE_YOUNG_PAIR(BgL_nbodyz00_147, BNIL);
											BgL_arg1143z00_152 = BgL_list1144z00_153;
										}
										{	/* Tools/progn.scm 120 */
											obj_t BgL_res1208z00_261;

											{	/* Tools/progn.scm 120 */
												obj_t BgL_obj1z00_260;

												BgL_obj1z00_260 = CNST_TABLE_REF(0);
												BgL_res1208z00_261 =
													MAKE_YOUNG_EPAIR(BgL_obj1z00_260, BgL_arg1143z00_152,
													BgL_locz00_20);
											}
											return BgL_res1208z00_261;
										}
									}
							}
					}
				else
					{	/* Tools/progn.scm 113 */
						return BgL_nbodyz00_147;
					}
			}
		}

	}



/* &normalize-progn/loc */
	obj_t BGl_z62normaliza7ezd2prognzf2locze5zztools_prognz00(obj_t
		BgL_envz00_280, obj_t BgL_bodyza2za2_281, obj_t BgL_locz00_282)
	{
		{	/* Tools/progn.scm 110 */
			return
				BGl_normaliza7ezd2prognzf2locz87zztools_prognz00(BgL_bodyza2za2_281,
				BgL_locz00_282);
		}

	}



/* emap */
	BGL_EXPORTED_DEF obj_t BGl_emapz00zztools_prognz00(obj_t BgL_fz00_21,
		obj_t BgL_l0z00_22)
	{
		{	/* Tools/progn.scm 125 */
			return
				BGl_loopze70ze7zztools_prognz00(BgL_fz00_21, BgL_l0z00_22,
				BgL_l0z00_22);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zztools_prognz00(obj_t BgL_fz00_291, obj_t BgL_l0z00_290,
		obj_t BgL_lz00_155)
	{
		{	/* Tools/progn.scm 126 */
			if (NULLP(BgL_lz00_155))
				{	/* Tools/progn.scm 128 */
					return BNIL;
				}
			else
				{	/* Tools/progn.scm 128 */
					if (EPAIRP(BgL_lz00_155))
						{	/* Tools/progn.scm 131 */
							obj_t BgL_arg1148z00_159;
							obj_t BgL_arg1149z00_160;
							obj_t BgL_arg1152z00_161;

							{	/* Tools/progn.scm 131 */
								obj_t BgL_arg1153z00_162;

								BgL_arg1153z00_162 = CAR(((obj_t) BgL_lz00_155));
								BgL_arg1148z00_159 =
									BGL_PROCEDURE_CALL1(BgL_fz00_291, BgL_arg1153z00_162);
							}
							{	/* Tools/progn.scm 131 */
								obj_t BgL_arg1154z00_163;

								BgL_arg1154z00_163 = CDR(((obj_t) BgL_lz00_155));
								BgL_arg1149z00_160 =
									BGl_loopze70ze7zztools_prognz00(BgL_fz00_291, BgL_l0z00_290,
									BgL_arg1154z00_163);
							}
							BgL_arg1152z00_161 = CER(((obj_t) BgL_lz00_155));
							{	/* Tools/progn.scm 131 */
								obj_t BgL_res1209z00_265;

								BgL_res1209z00_265 =
									MAKE_YOUNG_EPAIR(BgL_arg1148z00_159, BgL_arg1149z00_160,
									BgL_arg1152z00_161);
								return BgL_res1209z00_265;
							}
						}
					else
						{	/* Tools/progn.scm 130 */
							if (PAIRP(BgL_lz00_155))
								{	/* Tools/progn.scm 133 */
									obj_t BgL_arg1157z00_165;
									obj_t BgL_arg1158z00_166;

									{	/* Tools/progn.scm 133 */
										obj_t BgL_arg1162z00_167;

										BgL_arg1162z00_167 = CAR(BgL_lz00_155);
										BgL_arg1157z00_165 =
											BGL_PROCEDURE_CALL1(BgL_fz00_291, BgL_arg1162z00_167);
									}
									BgL_arg1158z00_166 =
										BGl_loopze70ze7zztools_prognz00(BgL_fz00_291, BgL_l0z00_290,
										CDR(BgL_lz00_155));
									return
										MAKE_YOUNG_PAIR(BgL_arg1157z00_165, BgL_arg1158z00_166);
								}
							else
								{	/* Tools/progn.scm 132 */
									BGL_TAIL return
										BGl_internalzd2errorzd2zztools_errorz00(CNST_TABLE_REF(2),
										BGl_string1213z00zztools_prognz00, BgL_l0z00_290);
								}
						}
				}
		}

	}



/* &emap */
	obj_t BGl_z62emapz62zztools_prognz00(obj_t BgL_envz00_283, obj_t BgL_fz00_284,
		obj_t BgL_l0z00_285)
	{
		{	/* Tools/progn.scm 125 */
			return BGl_emapz00zztools_prognz00(BgL_fz00_284, BgL_l0z00_285);
		}

	}



/* progn-first-expression */
	BGL_EXPORTED_DEF obj_t BGl_prognzd2firstzd2expressionz00zztools_prognz00(obj_t
		BgL_expz00_23)
	{
		{	/* Tools/progn.scm 142 */
			{
				obj_t BgL_expz00_171;

				BgL_expz00_171 = BgL_expz00_23;
			BgL_zc3z04anonymousza31165ze3z87_172:
				if (PAIRP(BgL_expz00_171))
					{	/* Tools/progn.scm 143 */
						if ((CAR(((obj_t) BgL_expz00_171)) == CNST_TABLE_REF(0)))
							{	/* Tools/progn.scm 143 */
								if (NULLP(CDR(((obj_t) BgL_expz00_171))))
									{	/* Tools/progn.scm 143 */
										return BFALSE;
									}
								else
									{	/* Tools/progn.scm 143 */
										obj_t BgL_cdrzd2153zd2_183;

										BgL_cdrzd2153zd2_183 = CDR(((obj_t) BgL_expz00_171));
										if (PAIRP(BgL_cdrzd2153zd2_183))
											{
												obj_t BgL_expz00_541;

												BgL_expz00_541 = CAR(BgL_cdrzd2153zd2_183);
												BgL_expz00_171 = BgL_expz00_541;
												goto BgL_zc3z04anonymousza31165ze3z87_172;
											}
										else
											{	/* Tools/progn.scm 143 */
												return BgL_expz00_171;
											}
									}
							}
						else
							{	/* Tools/progn.scm 143 */
								return BgL_expz00_171;
							}
					}
				else
					{	/* Tools/progn.scm 143 */
						return BgL_expz00_171;
					}
			}
		}

	}



/* &progn-first-expression */
	obj_t BGl_z62prognzd2firstzd2expressionz62zztools_prognz00(obj_t
		BgL_envz00_286, obj_t BgL_expz00_287)
	{
		{	/* Tools/progn.scm 142 */
			return BGl_prognzd2firstzd2expressionz00zztools_prognz00(BgL_expz00_287);
		}

	}



/* progn-tail-expressions */
	BGL_EXPORTED_DEF obj_t BGl_prognzd2tailzd2expressionsz00zztools_prognz00(obj_t
		BgL_expz00_24)
	{
		{	/* Tools/progn.scm 157 */
			{
				obj_t BgL_expz00_190;

				BgL_expz00_190 = BgL_expz00_24;
				if (PAIRP(BgL_expz00_190))
					{	/* Tools/progn.scm 158 */
						if ((CAR(((obj_t) BgL_expz00_190)) == CNST_TABLE_REF(0)))
							{	/* Tools/progn.scm 158 */
								if (NULLP(CDR(((obj_t) BgL_expz00_190))))
									{	/* Tools/progn.scm 158 */
										return BNIL;
									}
								else
									{	/* Tools/progn.scm 158 */
										obj_t BgL_cdrzd2172zd2_202;

										BgL_cdrzd2172zd2_202 = CDR(((obj_t) BgL_expz00_190));
										if (PAIRP(BgL_cdrzd2172zd2_202))
											{	/* Tools/progn.scm 158 */
												return CDR(BgL_cdrzd2172zd2_202);
											}
										else
											{	/* Tools/progn.scm 158 */
												return BNIL;
											}
									}
							}
						else
							{	/* Tools/progn.scm 158 */
								return BNIL;
							}
					}
				else
					{	/* Tools/progn.scm 158 */
						return BNIL;
					}
			}
		}

	}



/* &progn-tail-expressions */
	obj_t BGl_z62prognzd2tailzd2expressionsz62zztools_prognz00(obj_t
		BgL_envz00_288, obj_t BgL_expz00_289)
	{
		{	/* Tools/progn.scm 157 */
			return BGl_prognzd2tailzd2expressionsz00zztools_prognz00(BgL_expz00_289);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_prognz00(void)
	{
		{	/* Tools/progn.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_prognz00(void)
	{
		{	/* Tools/progn.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_prognz00(void)
	{
		{	/* Tools/progn.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztools_prognz00(void)
	{
		{	/* Tools/progn.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1214z00zztools_prognz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1214z00zztools_prognz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1214z00zztools_prognz00));
		}

	}

#ifdef __cplusplus
}
#endif
