/*===========================================================================*/
/*   (Tools/speek.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/speek.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_SPEEK_TYPE_DEFINITIONS
#define BGL_TOOLS_SPEEK_TYPE_DEFINITIONS
#endif													// BGL_TOOLS_SPEEK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zztools_speekz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zztools_speekz00(void);
	static obj_t BGl_genericzd2initzd2zztools_speekz00(void);
	static obj_t BGl_objectzd2initzd2zztools_speekz00(void);
	static obj_t BGl_methodzd2initzd2zztools_speekz00(void);
	static obj_t BGl_z62verbosez62zztools_speekz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2verboseza2z00zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__pp_circlez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_speekz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztools_speekz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztools_speekz00(void);
	BGL_IMPORT obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_speekz00zztools_speekz00(obj_t, bool_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_za2stdoutza2z00zztools_speekz00 = BUNSPEC;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_verbosezd2envzd2zztools_speekz00,
		BgL_bgl_za762verboseza762za7za7t1023z00, va_generic_entry,
		BGl_z62verbosez62zztools_speekz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string1022z00zztools_speekz00,
		BgL_bgl_string1022za700za7za7t1024za7, "tools_speek", 11);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztools_speekz00));
		     ADD_ROOT((void *) (&BGl_za2stdoutza2z00zztools_speekz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long
		BgL_checksumz00_34, char *BgL_fromz00_35)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_speekz00))
				{
					BGl_requirezd2initializa7ationz75zztools_speekz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_speekz00();
					BGl_libraryzd2moduleszd2initz00zztools_speekz00();
					BGl_importedzd2moduleszd2initz00zztools_speekz00();
					return BGl_toplevelzd2initzd2zztools_speekz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_speekz00(void)
	{
		{	/* Tools/speek.scm 15 */
			BGl_modulezd2initializa7ationz75zz__pp_circlez00(0L, "tools_speek");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "tools_speek");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tools_speek");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_speek");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tools_speek");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_speek");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_speekz00(void)
	{
		{	/* Tools/speek.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztools_speekz00(void)
	{
		{	/* Tools/speek.scm 15 */
			{	/* Tools/speek.scm 22 */
				obj_t BgL_tmpz00_50;

				BgL_tmpz00_50 = BGL_CURRENT_DYNAMIC_ENV();
				return (BGl_za2stdoutza2z00zztools_speekz00 =
					BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_50), BUNSPEC);
			}
		}

	}



/* speek */
	obj_t BGl_speekz00zztools_speekz00(obj_t BgL_portz00_3, bool_t BgL_flagz00_4,
		obj_t BgL_argsz00_5)
	{
		{	/* Tools/speek.scm 27 */
			if (BgL_flagz00_4)
				{	/* Tools/speek.scm 28 */
					{
						obj_t BgL_l1012z00_17;

						BgL_l1012z00_17 = BgL_argsz00_5;
					BgL_zc3z04anonymousza31017ze3z87_18:
						if (PAIRP(BgL_l1012z00_17))
							{	/* Tools/speek.scm 30 */
								BGl_displayzd2circlezd2zz__pp_circlez00(CAR(BgL_l1012z00_17),
									BgL_portz00_3);
								{
									obj_t BgL_l1012z00_58;

									BgL_l1012z00_58 = CDR(BgL_l1012z00_17);
									BgL_l1012z00_17 = BgL_l1012z00_58;
									goto BgL_zc3z04anonymousza31017ze3z87_18;
								}
							}
						else
							{	/* Tools/speek.scm 30 */
								((bool_t) 1);
							}
					}
					return bgl_flush_output_port(BgL_portz00_3);
				}
			else
				{	/* Tools/speek.scm 28 */
					return BFALSE;
				}
		}

	}



/* verbose */
	BGL_EXPORTED_DEF obj_t BGl_verbosez00zztools_speekz00(obj_t BgL_levelz00_6,
		obj_t BgL_argsz00_7)
	{
		{	/* Tools/speek.scm 36 */
			{	/* Tools/speek.scm 37 */
				bool_t BgL_arg1020z00_23;

				{	/* Tools/speek.scm 37 */
					bool_t BgL_test1028z00_61;

					if (INTEGERP(BgL_levelz00_6))
						{	/* Tools/speek.scm 37 */
							BgL_test1028z00_61 =
								INTEGERP(BGl_za2verboseza2z00zzengine_paramz00);
						}
					else
						{	/* Tools/speek.scm 37 */
							BgL_test1028z00_61 = ((bool_t) 0);
						}
					if (BgL_test1028z00_61)
						{	/* Tools/speek.scm 37 */
							BgL_arg1020z00_23 =
								(
								(long) CINT(BgL_levelz00_6) <=
								(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
						}
					else
						{	/* Tools/speek.scm 37 */
							BgL_arg1020z00_23 =
								BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_levelz00_6,
								BGl_za2verboseza2z00zzengine_paramz00);
						}
				}
				return
					BGl_speekz00zztools_speekz00(BGl_za2stdoutza2z00zztools_speekz00,
					BgL_arg1020z00_23, BgL_argsz00_7);
			}
		}

	}



/* &verbose */
	obj_t BGl_z62verbosez62zztools_speekz00(obj_t BgL_envz00_31,
		obj_t BgL_levelz00_32, obj_t BgL_argsz00_33)
	{
		{	/* Tools/speek.scm 36 */
			return BGl_verbosez00zztools_speekz00(BgL_levelz00_32, BgL_argsz00_33);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_speekz00(void)
	{
		{	/* Tools/speek.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_speekz00(void)
	{
		{	/* Tools/speek.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_speekz00(void)
	{
		{	/* Tools/speek.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztools_speekz00(void)
	{
		{	/* Tools/speek.scm 15 */
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1022z00zztools_speekz00));
		}

	}

#ifdef __cplusplus
}
#endif
