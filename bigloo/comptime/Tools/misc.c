/*===========================================================================*/
/*   (Tools/misc.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/misc.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_MISC_TYPE_DEFINITIONS
#define BGL_TOOLS_MISC_TYPE_DEFINITIONS
#endif													// BGL_TOOLS_MISC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62epairifyza2zc0zztools_miscz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_mingwzd2buildzd2pathzd2fromzd2shellzd2variablezd2zztools_miscz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztools_miscz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_epairifyza2za2zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_epairifyz00zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_uncygdrivez00zztools_miscz00(obj_t);
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zztools_miscz00(void);
	static obj_t BGl_z62epairifyzd2reczb0zztools_miscz00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zztools_miscz00(void);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_listzd2ze3stringz31zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62uncygdrivez62zztools_miscz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62replacez12z70zztools_miscz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_unixzd2buildzd2pathzd2fromzd2shellzd2variablezd2zztools_miscz00(obj_t);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62stringzd2splitzd2charz62zztools_miscz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztools_miscz00(void);
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	BGL_IMPORT obj_t bgl_bignum_sub(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_epairifyzd2propagatezd2locz00zztools_miscz00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t BGl_z62epairifyzd2propagatezb0zztools_miscz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t rgc_buffer_substring(obj_t, long, long);
	BGL_IMPORT bool_t bigloo_strncmp(obj_t, obj_t, long);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_replacez12z12zztools_miscz00(obj_t, obj_t);
	static obj_t BGl_ignoreze70ze7zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t
		BGl_z62buildzd2pathzd2fromzd2shellzd2variablez62zztools_miscz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zztools_miscz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_miscz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztools_miscz00(void);
	static obj_t BGl_z62epairifyzd2propagatezd2locz62zztools_miscz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_epairifyzd2reczd2zztools_miscz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31474ze3ze5zztools_miscz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringza2zd2ze3stringz93zztools_miscz00(obj_t);
	static obj_t BGl_z62stringza2zd2ze3stringzf1zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_buildzd2pathzd2fromzd2shellzd2variablez00zztools_miscz00(obj_t);
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	static obj_t BGl_normaliza7ezd2mingwzd2pathze70z40zztools_miscz00(obj_t);
	static obj_t BGl_z62epairifyz62zztools_miscz00(obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_za7erozf3z54zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_loopze70ze7zztools_miscz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_epairifyzd2propagatezd2zztools_miscz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t rgc_fill_buffer(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stringzd2splitzd2charz00zztools_miscz00(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_uncygdrivezd2envzd2zztools_miscz00,
		BgL_bgl_za762uncygdriveza7621576z00, BGl_z62uncygdrivez62zztools_miscz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringza2zd2ze3stringzd2envz41zztools_miscz00,
		BgL_bgl_za762stringza7a2za7d2za71577z00,
		BGl_z62stringza2zd2ze3stringzf1zztools_miscz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_replacez12zd2envzc0zztools_miscz00,
		BgL_bgl_za762replaceza712za7701578za7, BGl_z62replacez12z70zztools_miscz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_buildzd2pathzd2fromzd2shellzd2variablezd2envzd2zztools_miscz00,
		BgL_bgl_za762buildza7d2pathza71579za7,
		BGl_z62buildzd2pathzd2fromzd2shellzd2variablez62zztools_miscz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_epairifyzd2envzd2zztools_miscz00,
		BgL_bgl_za762epairifyza762za7za71580z00, BGl_z62epairifyz62zztools_miscz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stringzd2splitzd2charzd2envzd2zztools_miscz00,
		BgL_bgl_za762stringza7d2spli1581z00,
		BGl_z62stringzd2splitzd2charz62zztools_miscz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1567z00zztools_miscz00,
		BgL_bgl_string1567za700za7za7t1582za7, " ", 1);
	      DEFINE_STRING(BGl_string1568z00zztools_miscz00,
		BgL_bgl_string1568za700za7za7t1583za7, "", 0);
	      DEFINE_STRING(BGl_string1569z00zztools_miscz00,
		BgL_bgl_string1569za700za7za7t1584za7, "mingw", 5);
	      DEFINE_STRING(BGl_string1570z00zztools_miscz00,
		BgL_bgl_string1570za700za7za7t1585za7, ":/", 2);
	      DEFINE_STRING(BGl_string1571z00zztools_miscz00,
		BgL_bgl_string1571za700za7za7t1586za7, "regular-grammar", 15);
	      DEFINE_STRING(BGl_string1572z00zztools_miscz00,
		BgL_bgl_string1572za700za7za7t1587za7, "Illegal match", 13);
	      DEFINE_STRING(BGl_string1573z00zztools_miscz00,
		BgL_bgl_string1573za700za7za7t1588za7, "/cygdrive/", 10);
	      DEFINE_STRING(BGl_string1574z00zztools_miscz00,
		BgL_bgl_string1574za700za7za7t1589za7, "#z1 ", 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_epairifyza2zd2envz70zztools_miscz00,
		BgL_bgl_za762epairifyza7a2za7c1590za7, va_generic_entry,
		BGl_z62epairifyza2zc0zztools_miscz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_epairifyzd2reczd2envz00zztools_miscz00,
		BgL_bgl_za762epairifyza7d2re1591z00,
		BGl_z62epairifyzd2reczb0zztools_miscz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_epairifyzd2propagatezd2loczd2envzd2zztools_miscz00,
		BgL_bgl_za762epairifyza7d2pr1592z00,
		BGl_z62epairifyzd2propagatezd2locz62zztools_miscz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_epairifyzd2propagatezd2envz00zztools_miscz00,
		BgL_bgl_za762epairifyza7d2pr1593z00,
		BGl_z62epairifyzd2propagatezb0zztools_miscz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztools_miscz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long
		BgL_checksumz00_734, char *BgL_fromz00_735)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_miscz00))
				{
					BGl_requirezd2initializa7ationz75zztools_miscz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_miscz00();
					BGl_libraryzd2moduleszd2initz00zztools_miscz00();
					BGl_cnstzd2initzd2zztools_miscz00();
					return BGl_methodzd2initzd2zztools_miscz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_miscz00(void)
	{
		{	/* Tools/misc.scm 15 */
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "tools_misc");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_misc");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "tools_misc");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "tools_misc");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_misc");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tools_misc");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "tools_misc");
			BGl_modulezd2initializa7ationz75zz__rgcz00(0L, "tools_misc");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "tools_misc");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"tools_misc");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tools_misc");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztools_miscz00(void)
	{
		{	/* Tools/misc.scm 15 */
			{	/* Tools/misc.scm 15 */
				obj_t BgL_cportz00_721;

				{	/* Tools/misc.scm 15 */
					obj_t BgL_stringz00_728;

					BgL_stringz00_728 = BGl_string1574z00zztools_miscz00;
					{	/* Tools/misc.scm 15 */
						obj_t BgL_startz00_729;

						BgL_startz00_729 = BINT(0L);
						{	/* Tools/misc.scm 15 */
							obj_t BgL_endz00_730;

							BgL_endz00_730 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_728)));
							{	/* Tools/misc.scm 15 */

								BgL_cportz00_721 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_728, BgL_startz00_729, BgL_endz00_730);
				}}}}
				{
					long BgL_iz00_722;

					BgL_iz00_722 = 0L;
				BgL_loopz00_723:
					if ((BgL_iz00_722 == -1L))
						{	/* Tools/misc.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tools/misc.scm 15 */
							{	/* Tools/misc.scm 15 */
								obj_t BgL_arg1575z00_724;

								{	/* Tools/misc.scm 15 */

									{	/* Tools/misc.scm 15 */
										obj_t BgL_locationz00_726;

										BgL_locationz00_726 = BBOOL(((bool_t) 0));
										{	/* Tools/misc.scm 15 */

											BgL_arg1575z00_724 =
												BGl_readz00zz__readerz00(BgL_cportz00_721,
												BgL_locationz00_726);
										}
									}
								}
								{	/* Tools/misc.scm 15 */
									int BgL_tmpz00_763;

									BgL_tmpz00_763 = (int) (BgL_iz00_722);
									CNST_TABLE_SET(BgL_tmpz00_763, BgL_arg1575z00_724);
							}}
							{	/* Tools/misc.scm 15 */
								int BgL_auxz00_727;

								BgL_auxz00_727 = (int) ((BgL_iz00_722 - 1L));
								{
									long BgL_iz00_768;

									BgL_iz00_768 = (long) (BgL_auxz00_727);
									BgL_iz00_722 = BgL_iz00_768;
									goto BgL_loopz00_723;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_miscz00(void)
	{
		{	/* Tools/misc.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* string*->string */
	BGL_EXPORTED_DEF obj_t BGl_stringza2zd2ze3stringz93zztools_miscz00(obj_t
		BgL_lz00_3)
	{
		{	/* Tools/misc.scm 30 */
			{	/* Tools/misc.scm 31 */
				obj_t BgL_g1012z00_30;

				BgL_g1012z00_30 = bgl_reverse(BgL_lz00_3);
				{
					obj_t BgL_lz00_32;
					obj_t BgL_rz00_33;

					BgL_lz00_32 = BgL_g1012z00_30;
					BgL_rz00_33 = BGl_string1568z00zztools_miscz00;
				BgL_zc3z04anonymousza31038ze3z87_34:
					if (NULLP(BgL_lz00_32))
						{	/* Tools/misc.scm 33 */
							return BgL_rz00_33;
						}
					else
						{	/* Tools/misc.scm 35 */
							obj_t BgL_arg1040z00_36;
							obj_t BgL_arg1041z00_37;

							BgL_arg1040z00_36 = CDR(((obj_t) BgL_lz00_32));
							{	/* Tools/misc.scm 36 */
								obj_t BgL_arg1042z00_38;

								BgL_arg1042z00_38 = CAR(((obj_t) BgL_lz00_32));
								BgL_arg1041z00_37 =
									string_append_3(BgL_arg1042z00_38,
									BGl_string1567z00zztools_miscz00, BgL_rz00_33);
							}
							{
								obj_t BgL_rz00_780;
								obj_t BgL_lz00_779;

								BgL_lz00_779 = BgL_arg1040z00_36;
								BgL_rz00_780 = BgL_arg1041z00_37;
								BgL_rz00_33 = BgL_rz00_780;
								BgL_lz00_32 = BgL_lz00_779;
								goto BgL_zc3z04anonymousza31038ze3z87_34;
							}
						}
				}
			}
		}

	}



/* &string*->string */
	obj_t BGl_z62stringza2zd2ze3stringzf1zztools_miscz00(obj_t BgL_envz00_688,
		obj_t BgL_lz00_689)
	{
		{	/* Tools/misc.scm 30 */
			return BGl_stringza2zd2ze3stringz93zztools_miscz00(BgL_lz00_689);
		}

	}



/* replace! */
	BGL_EXPORTED_DEF obj_t BGl_replacez12z12zztools_miscz00(obj_t BgL_p1z00_4,
		obj_t BgL_p2z00_5)
	{
		{	/* Tools/misc.scm 41 */
			{	/* Tools/misc.scm 42 */
				bool_t BgL_test1598z00_782;

				if (PAIRP(BgL_p1z00_4))
					{	/* Tools/misc.scm 42 */
						if (PAIRP(BgL_p2z00_5))
							{	/* Tools/misc.scm 42 */
								if (EPAIRP(BgL_p2z00_5))
									{	/* Tools/misc.scm 42 */
										BgL_test1598z00_782 = ((bool_t) 0);
									}
								else
									{	/* Tools/misc.scm 42 */
										BgL_test1598z00_782 = ((bool_t) 1);
									}
							}
						else
							{	/* Tools/misc.scm 42 */
								BgL_test1598z00_782 = ((bool_t) 0);
							}
					}
				else
					{	/* Tools/misc.scm 42 */
						BgL_test1598z00_782 = ((bool_t) 0);
					}
				if (BgL_test1598z00_782)
					{	/* Tools/misc.scm 42 */
						{	/* Tools/misc.scm 44 */
							obj_t BgL_tmpz00_789;

							BgL_tmpz00_789 = CAR(BgL_p2z00_5);
							SET_CAR(BgL_p1z00_4, BgL_tmpz00_789);
						}
						{	/* Tools/misc.scm 45 */
							obj_t BgL_tmpz00_792;

							BgL_tmpz00_792 = CDR(BgL_p2z00_5);
							SET_CDR(BgL_p1z00_4, BgL_tmpz00_792);
						}
						return BgL_p1z00_4;
					}
				else
					{	/* Tools/misc.scm 42 */
						return BgL_p2z00_5;
					}
			}
		}

	}



/* &replace! */
	obj_t BGl_z62replacez12z70zztools_miscz00(obj_t BgL_envz00_690,
		obj_t BgL_p1z00_691, obj_t BgL_p2z00_692)
	{
		{	/* Tools/misc.scm 41 */
			return BGl_replacez12z12zztools_miscz00(BgL_p1z00_691, BgL_p2z00_692);
		}

	}



/* string-split-char */
	BGL_EXPORTED_DEF obj_t BGl_stringzd2splitzd2charz00zztools_miscz00(obj_t
		BgL_strz00_6, obj_t BgL_separatorz00_7)
	{
		{	/* Tools/misc.scm 52 */
		BGl_stringzd2splitzd2charz00zztools_miscz00:
			{	/* Tools/misc.scm 53 */
				long BgL_strzd2lengthzd2_49;

				BgL_strzd2lengthzd2_49 = STRING_LENGTH(BgL_strz00_6);
				if ((BgL_strzd2lengthzd2_49 == 0L))
					{	/* Tools/misc.scm 54 */
						return BNIL;
					}
				else
					{
						obj_t BgL_iz00_52;

						BgL_iz00_52 = BINT(0L);
					BgL_zc3z04anonymousza31050ze3z87_53:
						{	/* Tools/misc.scm 57 */
							bool_t BgL_test1603z00_799;

							if (INTEGERP(BgL_iz00_52))
								{	/* Tools/misc.scm 57 */
									BgL_test1603z00_799 =
										((long) CINT(BgL_iz00_52) >= BgL_strzd2lengthzd2_49);
								}
							else
								{	/* Tools/misc.scm 57 */
									BgL_test1603z00_799 =
										BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_iz00_52,
										BINT(BgL_strzd2lengthzd2_49));
								}
							if (BgL_test1603z00_799)
								{	/* Tools/misc.scm 58 */
									obj_t BgL_list1053z00_56;

									BgL_list1053z00_56 = MAKE_YOUNG_PAIR(BgL_strz00_6, BNIL);
									return BgL_list1053z00_56;
								}
							else
								{	/* Tools/misc.scm 57 */
									if (
										(STRING_REF(BgL_strz00_6,
												(long) CINT(BgL_iz00_52)) == CCHAR(BgL_separatorz00_7)))
										{	/* Tools/misc.scm 59 */
											if (BGl_za7erozf3z54zz__r4_numbers_6_5z00(BgL_iz00_52))
												{
													obj_t BgL_strz00_814;

													BgL_strz00_814 =
														c_substring(BgL_strz00_6, 1L,
														BgL_strzd2lengthzd2_49);
													BgL_strz00_6 = BgL_strz00_814;
													goto BGl_stringzd2splitzd2charz00zztools_miscz00;
												}
											else
												{	/* Tools/misc.scm 63 */
													obj_t BgL_arg1058z00_61;
													obj_t BgL_arg1059z00_62;

													BgL_arg1058z00_61 =
														c_substring(BgL_strz00_6, 0L,
														(long) CINT(BgL_iz00_52));
													{	/* Tools/misc.scm 65 */
														obj_t BgL_arg1060z00_63;

														{	/* Tools/misc.scm 65 */
															obj_t BgL_arg1062z00_64;

															if (INTEGERP(BgL_iz00_52))
																{	/* Tools/misc.scm 65 */
																	obj_t BgL_tmpz00_475;

																	BgL_tmpz00_475 = BINT(0L);
																	{	/* Tools/misc.scm 65 */
																		bool_t BgL_test1608z00_821;

																		{	/* Tools/misc.scm 65 */
																			obj_t BgL_tmpz00_822;

																			BgL_tmpz00_822 = BINT(1L);
																			BgL_test1608z00_821 =
																				BGL_ADDFX_OV(BgL_iz00_52,
																				BgL_tmpz00_822, BgL_tmpz00_475);
																		}
																		if (BgL_test1608z00_821)
																			{	/* Tools/misc.scm 65 */
																				BgL_arg1062z00_64 =
																					bgl_bignum_add(bgl_long_to_bignum(
																						(long) CINT(BgL_iz00_52)),
																					CNST_TABLE_REF(0));
																			}
																		else
																			{	/* Tools/misc.scm 65 */
																				BgL_arg1062z00_64 = BgL_tmpz00_475;
																			}
																	}
																}
															else
																{	/* Tools/misc.scm 65 */
																	BgL_arg1062z00_64 =
																		BGl_2zb2zb2zz__r4_numbers_6_5z00
																		(BgL_iz00_52, BINT(1L));
																}
															BgL_arg1060z00_63 =
																c_substring(BgL_strz00_6,
																(long) CINT(BgL_arg1062z00_64),
																BgL_strzd2lengthzd2_49);
														}
														BgL_arg1059z00_62 =
															BGl_stringzd2splitzd2charz00zztools_miscz00
															(BgL_arg1060z00_63, BgL_separatorz00_7);
													}
													return
														MAKE_YOUNG_PAIR(BgL_arg1058z00_61,
														BgL_arg1059z00_62);
												}
										}
									else
										{	/* Tools/misc.scm 68 */
											obj_t BgL_arg1063z00_65;

											if (INTEGERP(BgL_iz00_52))
												{	/* Tools/misc.scm 68 */
													obj_t BgL_tmpz00_488;

													BgL_tmpz00_488 = BINT(0L);
													{	/* Tools/misc.scm 68 */
														bool_t BgL_test1610z00_838;

														{	/* Tools/misc.scm 68 */
															obj_t BgL_tmpz00_839;

															BgL_tmpz00_839 = BINT(1L);
															BgL_test1610z00_838 =
																BGL_ADDFX_OV(BgL_iz00_52, BgL_tmpz00_839,
																BgL_tmpz00_488);
														}
														if (BgL_test1610z00_838)
															{	/* Tools/misc.scm 68 */
																BgL_arg1063z00_65 =
																	bgl_bignum_add(bgl_long_to_bignum(
																		(long) CINT(BgL_iz00_52)),
																	CNST_TABLE_REF(0));
															}
														else
															{	/* Tools/misc.scm 68 */
																BgL_arg1063z00_65 = BgL_tmpz00_488;
															}
													}
												}
											else
												{	/* Tools/misc.scm 68 */
													BgL_arg1063z00_65 =
														BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_iz00_52,
														BINT(1L));
												}
											{
												obj_t BgL_iz00_848;

												BgL_iz00_848 = BgL_arg1063z00_65;
												BgL_iz00_52 = BgL_iz00_848;
												goto BgL_zc3z04anonymousza31050ze3z87_53;
											}
										}
								}
						}
					}
			}
		}

	}



/* &string-split-char */
	obj_t BGl_z62stringzd2splitzd2charz62zztools_miscz00(obj_t BgL_envz00_693,
		obj_t BgL_strz00_694, obj_t BgL_separatorz00_695)
	{
		{	/* Tools/misc.scm 52 */
			return
				BGl_stringzd2splitzd2charz00zztools_miscz00(BgL_strz00_694,
				BgL_separatorz00_695);
		}

	}



/* epairify */
	BGL_EXPORTED_DEF obj_t BGl_epairifyz00zztools_miscz00(obj_t BgL_pairz00_8,
		obj_t BgL_epairz00_9)
	{
		{	/* Tools/misc.scm 78 */
			if (EPAIRP(BgL_epairz00_9))
				{	/* Tools/misc.scm 80 */
					obj_t BgL_arg1068z00_70;
					obj_t BgL_arg1074z00_71;
					obj_t BgL_arg1075z00_72;

					BgL_arg1068z00_70 = CAR(BgL_pairz00_8);
					BgL_arg1074z00_71 = CDR(BgL_pairz00_8);
					BgL_arg1075z00_72 = CER(((obj_t) BgL_epairz00_9));
					{	/* Tools/misc.scm 80 */
						obj_t BgL_res1563z00_499;

						BgL_res1563z00_499 =
							MAKE_YOUNG_EPAIR(BgL_arg1068z00_70, BgL_arg1074z00_71,
							BgL_arg1075z00_72);
						return BgL_res1563z00_499;
					}
				}
			else
				{	/* Tools/misc.scm 79 */
					return BgL_pairz00_8;
				}
		}

	}



/* &epairify */
	obj_t BGl_z62epairifyz62zztools_miscz00(obj_t BgL_envz00_696,
		obj_t BgL_pairz00_697, obj_t BgL_epairz00_698)
	{
		{	/* Tools/misc.scm 78 */
			return BGl_epairifyz00zztools_miscz00(BgL_pairz00_697, BgL_epairz00_698);
		}

	}



/* epairify-rec */
	BGL_EXPORTED_DEF obj_t BGl_epairifyzd2reczd2zztools_miscz00(obj_t BgL_pz00_10,
		obj_t BgL_epz00_11)
	{
		{	/* Tools/misc.scm 88 */
			if (PAIRP(BgL_pz00_10))
				{	/* Tools/misc.scm 90 */
					if (EPAIRP(BgL_epz00_11))
						{	/* Tools/misc.scm 92 */
							if (EPAIRP(BgL_pz00_10))
								{	/* Tools/misc.scm 94 */
									return BgL_pz00_10;
								}
							else
								{	/* Tools/misc.scm 97 */
									obj_t BgL_arg1079z00_76;
									obj_t BgL_arg1080z00_77;
									obj_t BgL_arg1082z00_78;

									{	/* Tools/misc.scm 97 */
										obj_t BgL_arg1083z00_79;
										obj_t BgL_arg1084z00_80;

										BgL_arg1083z00_79 = CAR(BgL_pz00_10);
										BgL_arg1084z00_80 = CAR(((obj_t) BgL_epz00_11));
										BgL_arg1079z00_76 =
											BGl_epairifyzd2reczd2zztools_miscz00(BgL_arg1083z00_79,
											BgL_arg1084z00_80);
									}
									{	/* Tools/misc.scm 98 */
										obj_t BgL_arg1085z00_81;
										obj_t BgL_arg1087z00_82;

										BgL_arg1085z00_81 = CDR(BgL_pz00_10);
										BgL_arg1087z00_82 = CDR(((obj_t) BgL_epz00_11));
										BgL_arg1080z00_77 =
											BGl_epairifyzd2reczd2zztools_miscz00(BgL_arg1085z00_81,
											BgL_arg1087z00_82);
									}
									BgL_arg1082z00_78 = CER(((obj_t) BgL_epz00_11));
									{	/* Tools/misc.scm 97 */
										obj_t BgL_res1564z00_505;

										BgL_res1564z00_505 =
											MAKE_YOUNG_EPAIR(BgL_arg1079z00_76, BgL_arg1080z00_77,
											BgL_arg1082z00_78);
										return BgL_res1564z00_505;
									}
								}
						}
					else
						{	/* Tools/misc.scm 92 */
							return BgL_pz00_10;
						}
				}
			else
				{	/* Tools/misc.scm 90 */
					return BgL_pz00_10;
				}
		}

	}



/* &epairify-rec */
	obj_t BGl_z62epairifyzd2reczb0zztools_miscz00(obj_t BgL_envz00_699,
		obj_t BgL_pz00_700, obj_t BgL_epz00_701)
	{
		{	/* Tools/misc.scm 88 */
			return BGl_epairifyzd2reczd2zztools_miscz00(BgL_pz00_700, BgL_epz00_701);
		}

	}



/* epairify-propagate */
	BGL_EXPORTED_DEF obj_t BGl_epairifyzd2propagatezd2zztools_miscz00(obj_t
		BgL_pz00_12, obj_t BgL_epz00_13)
	{
		{	/* Tools/misc.scm 106 */
			if (EPAIRP(BgL_epz00_13))
				{	/* Tools/misc.scm 109 */
					obj_t BgL_arg1090z00_507;

					BgL_arg1090z00_507 = CER(((obj_t) BgL_epz00_13));
					BGL_TAIL return
						BGl_loopze70ze7zztools_miscz00(BgL_arg1090z00_507, BgL_pz00_12);
				}
			else
				{	/* Tools/misc.scm 107 */
					return BgL_pz00_12;
				}
		}

	}



/* &epairify-propagate */
	obj_t BGl_z62epairifyzd2propagatezb0zztools_miscz00(obj_t BgL_envz00_702,
		obj_t BgL_pz00_703, obj_t BgL_epz00_704)
	{
		{	/* Tools/misc.scm 106 */
			return
				BGl_epairifyzd2propagatezd2zztools_miscz00(BgL_pz00_703, BgL_epz00_704);
		}

	}



/* epairify-propagate-loc */
	BGL_EXPORTED_DEF obj_t BGl_epairifyzd2propagatezd2locz00zztools_miscz00(obj_t
		BgL_pz00_14, obj_t BgL_locz00_15)
	{
		{	/* Tools/misc.scm 116 */
			BGL_TAIL return
				BGl_loopze70ze7zztools_miscz00(BgL_locz00_15, BgL_pz00_14);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zztools_miscz00(obj_t BgL_locz00_720, obj_t BgL_pz00_86)
	{
		{	/* Tools/misc.scm 117 */
			if (PAIRP(BgL_pz00_86))
				{	/* Tools/misc.scm 119 */
					if (EPAIRP(BgL_pz00_86))
						{	/* Tools/misc.scm 121 */
							return BgL_pz00_86;
						}
					else
						{	/* Tools/misc.scm 124 */
							obj_t BgL_arg1097z00_90;
							obj_t BgL_arg1102z00_91;

							BgL_arg1097z00_90 =
								BGl_loopze70ze7zztools_miscz00(BgL_locz00_720,
								CAR(BgL_pz00_86));
							BgL_arg1102z00_91 =
								BGl_loopze70ze7zztools_miscz00(BgL_locz00_720,
								CDR(BgL_pz00_86));
							{	/* Tools/misc.scm 124 */
								obj_t BgL_res1565z00_511;

								BgL_res1565z00_511 =
									MAKE_YOUNG_EPAIR(BgL_arg1097z00_90, BgL_arg1102z00_91,
									BgL_locz00_720);
								return BgL_res1565z00_511;
							}
						}
				}
			else
				{	/* Tools/misc.scm 119 */
					return BgL_pz00_86;
				}
		}

	}



/* &epairify-propagate-loc */
	obj_t BGl_z62epairifyzd2propagatezd2locz62zztools_miscz00(obj_t
		BgL_envz00_705, obj_t BgL_pz00_706, obj_t BgL_locz00_707)
	{
		{	/* Tools/misc.scm 116 */
			return
				BGl_epairifyzd2propagatezd2locz00zztools_miscz00(BgL_pz00_706,
				BgL_locz00_707);
		}

	}



/* epairify* */
	BGL_EXPORTED_DEF obj_t BGl_epairifyza2za2zztools_miscz00(obj_t BgL_defz00_16,
		obj_t BgL_srcsz00_17)
	{
		{	/* Tools/misc.scm 129 */
			{
				obj_t BgL_srcsz00_96;

				BgL_srcsz00_96 = BgL_srcsz00_17;
			BgL_zc3z04anonymousza31105ze3z87_97:
				if (NULLP(BgL_srcsz00_96))
					{	/* Tools/misc.scm 132 */
						return BgL_defz00_16;
					}
				else
					{	/* Tools/misc.scm 134 */
						bool_t BgL_test1621z00_896;

						{	/* Tools/misc.scm 134 */
							obj_t BgL_arg1129z00_106;

							BgL_arg1129z00_106 = CAR(((obj_t) BgL_srcsz00_96));
							BgL_test1621z00_896 = EPAIRP(BgL_arg1129z00_106);
						}
						if (BgL_test1621z00_896)
							{	/* Tools/misc.scm 135 */
								obj_t BgL_arg1122z00_101;
								obj_t BgL_arg1123z00_102;
								obj_t BgL_arg1125z00_103;

								BgL_arg1122z00_101 = CAR(((obj_t) BgL_defz00_16));
								BgL_arg1123z00_102 = CDR(((obj_t) BgL_defz00_16));
								{	/* Tools/misc.scm 135 */
									obj_t BgL_objz00_516;

									BgL_objz00_516 = CAR(((obj_t) BgL_srcsz00_96));
									BgL_arg1125z00_103 = CER(BgL_objz00_516);
								}
								{	/* Tools/misc.scm 135 */
									obj_t BgL_res1566z00_517;

									BgL_res1566z00_517 =
										MAKE_YOUNG_EPAIR(BgL_arg1122z00_101, BgL_arg1123z00_102,
										BgL_arg1125z00_103);
									return BgL_res1566z00_517;
								}
							}
						else
							{	/* Tools/misc.scm 137 */
								obj_t BgL_arg1127z00_105;

								BgL_arg1127z00_105 = CDR(((obj_t) BgL_srcsz00_96));
								{
									obj_t BgL_srcsz00_910;

									BgL_srcsz00_910 = BgL_arg1127z00_105;
									BgL_srcsz00_96 = BgL_srcsz00_910;
									goto BgL_zc3z04anonymousza31105ze3z87_97;
								}
							}
					}
			}
		}

	}



/* &epairify* */
	obj_t BGl_z62epairifyza2zc0zztools_miscz00(obj_t BgL_envz00_708,
		obj_t BgL_defz00_709, obj_t BgL_srcsz00_710)
	{
		{	/* Tools/misc.scm 129 */
			return BGl_epairifyza2za2zztools_miscz00(BgL_defz00_709, BgL_srcsz00_710);
		}

	}



/* build-path-from-shell-variable */
	BGL_EXPORTED_DEF obj_t
		BGl_buildzd2pathzd2fromzd2shellzd2variablez00zztools_miscz00(obj_t
		BgL_varz00_18)
	{
		{	/* Tools/misc.scm 142 */
			{	/* Tools/misc.scm 143 */
				bool_t BgL_test1622z00_912;

				{	/* Tools/misc.scm 143 */
					obj_t BgL_string1z00_519;

					BgL_string1z00_519 = string_to_bstring(OS_CLASS);
					{	/* Tools/misc.scm 143 */
						long BgL_l1z00_521;

						BgL_l1z00_521 = STRING_LENGTH(BgL_string1z00_519);
						if ((BgL_l1z00_521 == 5L))
							{	/* Tools/misc.scm 143 */
								int BgL_arg1282z00_524;

								{	/* Tools/misc.scm 143 */
									char *BgL_auxz00_919;
									char *BgL_tmpz00_917;

									BgL_auxz00_919 =
										BSTRING_TO_STRING(BGl_string1569z00zztools_miscz00);
									BgL_tmpz00_917 = BSTRING_TO_STRING(BgL_string1z00_519);
									BgL_arg1282z00_524 =
										memcmp(BgL_tmpz00_917, BgL_auxz00_919, BgL_l1z00_521);
								}
								BgL_test1622z00_912 = ((long) (BgL_arg1282z00_524) == 0L);
							}
						else
							{	/* Tools/misc.scm 143 */
								BgL_test1622z00_912 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test1622z00_912)
					{	/* Tools/misc.scm 143 */
						BGL_TAIL return
							BGl_mingwzd2buildzd2pathzd2fromzd2shellzd2variablezd2zztools_miscz00
							(BgL_varz00_18);
					}
				else
					{	/* Tools/misc.scm 143 */
						BGL_TAIL return
							BGl_unixzd2buildzd2pathzd2fromzd2shellzd2variablezd2zztools_miscz00
							(BgL_varz00_18);
					}
			}
		}

	}



/* &build-path-from-shell-variable */
	obj_t BGl_z62buildzd2pathzd2fromzd2shellzd2variablez62zztools_miscz00(obj_t
		BgL_envz00_711, obj_t BgL_varz00_712)
	{
		{	/* Tools/misc.scm 142 */
			return
				BGl_buildzd2pathzd2fromzd2shellzd2variablez00zztools_miscz00
				(BgL_varz00_712);
		}

	}



/* mingw-build-path-from-shell-variable */
	obj_t
		BGl_mingwzd2buildzd2pathzd2fromzd2shellzd2variablezd2zztools_miscz00(obj_t
		BgL_varz00_19)
	{
		{	/* Tools/misc.scm 150 */
			{	/* Tools/misc.scm 163 */
				obj_t BgL_valz00_112;
				obj_t BgL_resz00_113;

				BgL_valz00_112 = BGl_getenvz00zz__osz00(BgL_varz00_19);
				BgL_resz00_113 = BNIL;
				if (STRINGP(BgL_valz00_112))
					{	/* Tools/misc.scm 167 */
						long BgL_nz00_115;
						obj_t BgL_kz00_116;

						BgL_nz00_115 = STRING_LENGTH(BgL_valz00_112);
						BgL_kz00_116 = BINT(0L);
						{
							obj_t BgL_iz00_146;

							{
								obj_t BgL_iz00_119;

								BgL_iz00_119 = BINT(0L);
							BgL_zc3z04anonymousza31139ze3z87_120:
								{	/* Tools/misc.scm 177 */
									bool_t BgL_test1625z00_932;

									if (INTEGERP(BgL_iz00_119))
										{	/* Tools/misc.scm 178 */
											BgL_test1625z00_932 =
												((long) CINT(BgL_iz00_119) >= BgL_nz00_115);
										}
									else
										{	/* Tools/misc.scm 178 */
											BgL_test1625z00_932 =
												BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_iz00_119,
												BINT(BgL_nz00_115));
										}
									if (BgL_test1625z00_932)
										{	/* Tools/misc.scm 178 */
											obj_t BgL_l1027z00_123;

											{	/* Tools/misc.scm 179 */
												obj_t BgL_arg1154z00_139;

												{	/* Tools/misc.scm 179 */
													obj_t BgL_arg1157z00_140;

													{	/* Tools/misc.scm 179 */
														long BgL_startz00_589;
														long BgL_endz00_590;

														BgL_startz00_589 = (long) CINT(BgL_kz00_116);
														BgL_endz00_590 = (long) CINT(BgL_iz00_119);
														BgL_arg1157z00_140 =
															c_substring(
															((obj_t) BgL_valz00_112), BgL_startz00_589,
															BgL_endz00_590);
													}
													BgL_arg1154z00_139 =
														MAKE_YOUNG_PAIR(BgL_arg1157z00_140, BgL_resz00_113);
												}
												BgL_l1027z00_123 = bgl_reverse(BgL_arg1154z00_139);
											}
											if (NULLP(BgL_l1027z00_123))
												{	/* Tools/misc.scm 178 */
													return BNIL;
												}
											else
												{	/* Tools/misc.scm 178 */
													obj_t BgL_head1029z00_125;

													BgL_head1029z00_125 =
														MAKE_YOUNG_PAIR
														(BGl_normaliza7ezd2mingwzd2pathze70z40zztools_miscz00
														(CAR(BgL_l1027z00_123)), BNIL);
													{	/* Tools/misc.scm 178 */
														obj_t BgL_g1032z00_126;

														BgL_g1032z00_126 = CDR(BgL_l1027z00_123);
														{
															obj_t BgL_l1027z00_128;
															obj_t BgL_tail1030z00_129;

															BgL_l1027z00_128 = BgL_g1032z00_126;
															BgL_tail1030z00_129 = BgL_head1029z00_125;
														BgL_zc3z04anonymousza31143ze3z87_130:
															if (NULLP(BgL_l1027z00_128))
																{	/* Tools/misc.scm 178 */
																	return BgL_head1029z00_125;
																}
															else
																{	/* Tools/misc.scm 178 */
																	obj_t BgL_newtail1031z00_132;

																	{	/* Tools/misc.scm 178 */
																		obj_t BgL_arg1148z00_134;

																		{	/* Tools/misc.scm 178 */
																			obj_t BgL_arg1149z00_135;

																			BgL_arg1149z00_135 =
																				CAR(((obj_t) BgL_l1027z00_128));
																			BgL_arg1148z00_134 =
																				BGl_normaliza7ezd2mingwzd2pathze70z40zztools_miscz00
																				(BgL_arg1149z00_135);
																		}
																		BgL_newtail1031z00_132 =
																			MAKE_YOUNG_PAIR(BgL_arg1148z00_134, BNIL);
																	}
																	SET_CDR(BgL_tail1030z00_129,
																		BgL_newtail1031z00_132);
																	{	/* Tools/misc.scm 178 */
																		obj_t BgL_arg1145z00_133;

																		BgL_arg1145z00_133 =
																			CDR(((obj_t) BgL_l1027z00_128));
																		{
																			obj_t BgL_tail1030z00_961;
																			obj_t BgL_l1027z00_960;

																			BgL_l1027z00_960 = BgL_arg1145z00_133;
																			BgL_tail1030z00_961 =
																				BgL_newtail1031z00_132;
																			BgL_tail1030z00_129 = BgL_tail1030z00_961;
																			BgL_l1027z00_128 = BgL_l1027z00_960;
																			goto BgL_zc3z04anonymousza31143ze3z87_130;
																		}
																	}
																}
														}
													}
												}
										}
									else
										{	/* Tools/misc.scm 177 */
											{	/* Tools/misc.scm 180 */
												bool_t BgL_test1630z00_962;

												BgL_iz00_146 = BgL_iz00_119;
												if (
													(STRING_REF(
															((obj_t) BgL_valz00_112),
															(long) CINT(BgL_iz00_146)) ==
														((unsigned char) ':')))
													{	/* Tools/misc.scm 171 */
														bool_t BgL_test1632z00_968;

														{	/* Tools/misc.scm 171 */
															obj_t BgL_b1026z00_166;

															{	/* Tools/misc.scm 171 */
																obj_t BgL_za71za7_546;

																BgL_za71za7_546 = BINT(BgL_nz00_115);
																{	/* Tools/misc.scm 171 */
																	obj_t BgL_tmpz00_548;

																	BgL_tmpz00_548 = BINT(0L);
																	{	/* Tools/misc.scm 171 */
																		bool_t BgL_test1633z00_971;

																		{	/* Tools/misc.scm 171 */
																			obj_t BgL_tmpz00_972;

																			BgL_tmpz00_972 = BINT(1L);
																			BgL_test1633z00_971 =
																				BGL_SUBFX_OV(BgL_za71za7_546,
																				BgL_tmpz00_972, BgL_tmpz00_548);
																		}
																		if (BgL_test1633z00_971)
																			{	/* Tools/misc.scm 171 */
																				BgL_b1026z00_166 =
																					bgl_bignum_sub(bgl_long_to_bignum(
																						(long) CINT(BgL_za71za7_546)),
																					CNST_TABLE_REF(0));
																			}
																		else
																			{	/* Tools/misc.scm 171 */
																				BgL_b1026z00_166 = BgL_tmpz00_548;
																			}
																	}
																}
															}
															{	/* Tools/misc.scm 171 */
																bool_t BgL_test1634z00_979;

																if (INTEGERP(BgL_iz00_146))
																	{	/* Tools/misc.scm 171 */
																		BgL_test1634z00_979 =
																			INTEGERP(BgL_b1026z00_166);
																	}
																else
																	{	/* Tools/misc.scm 171 */
																		BgL_test1634z00_979 = ((bool_t) 0);
																	}
																if (BgL_test1634z00_979)
																	{	/* Tools/misc.scm 171 */
																		BgL_test1632z00_968 =
																			(
																			(long) CINT(BgL_iz00_146) <
																			(long) CINT(BgL_b1026z00_166));
																	}
																else
																	{	/* Tools/misc.scm 171 */
																		BgL_test1632z00_968 =
																			BGl_2zc3zc3zz__r4_numbers_6_5z00
																			(BgL_iz00_146, BgL_b1026z00_166);
																	}
															}
														}
														if (BgL_test1632z00_968)
															{	/* Tools/misc.scm 172 */
																bool_t BgL_test1636z00_987;

																{	/* Tools/misc.scm 173 */
																	bool_t BgL_test1637z00_988;

																	{	/* Tools/misc.scm 173 */
																		unsigned char BgL_arg1193z00_164;

																		{	/* Tools/misc.scm 173 */
																			obj_t BgL_arg1194z00_165;

																			if (INTEGERP(BgL_iz00_146))
																				{	/* Tools/misc.scm 173 */
																					obj_t BgL_tmpz00_560;

																					BgL_tmpz00_560 = BINT(0L);
																					{	/* Tools/misc.scm 173 */
																						bool_t BgL_test1639z00_992;

																						{	/* Tools/misc.scm 173 */
																							obj_t BgL_tmpz00_993;

																							BgL_tmpz00_993 = BINT(1L);
																							BgL_test1639z00_992 =
																								BGL_ADDFX_OV(BgL_iz00_146,
																								BgL_tmpz00_993, BgL_tmpz00_560);
																						}
																						if (BgL_test1639z00_992)
																							{	/* Tools/misc.scm 173 */
																								BgL_arg1194z00_165 =
																									bgl_bignum_add
																									(bgl_long_to_bignum((long)
																										CINT(BgL_iz00_146)),
																									CNST_TABLE_REF(0));
																							}
																						else
																							{	/* Tools/misc.scm 173 */
																								BgL_arg1194z00_165 =
																									BgL_tmpz00_560;
																							}
																					}
																				}
																			else
																				{	/* Tools/misc.scm 173 */
																					BgL_arg1194z00_165 =
																						BGl_2zb2zb2zz__r4_numbers_6_5z00
																						(BgL_iz00_146, BINT(1L));
																				}
																			BgL_arg1193z00_164 =
																				STRING_REF(
																				((obj_t) BgL_valz00_112),
																				(long) CINT(BgL_arg1194z00_165));
																		}
																		BgL_test1637z00_988 =
																			(BgL_arg1193z00_164 ==
																			((unsigned char) '/'));
																	}
																	if (BgL_test1637z00_988)
																		{	/* Tools/misc.scm 173 */
																			BgL_test1636z00_987 = ((bool_t) 1);
																		}
																	else
																		{	/* Tools/misc.scm 174 */
																			unsigned char BgL_arg1190z00_162;

																			{	/* Tools/misc.scm 174 */
																				obj_t BgL_arg1191z00_163;

																				if (INTEGERP(BgL_iz00_146))
																					{	/* Tools/misc.scm 174 */
																						obj_t BgL_tmpz00_574;

																						BgL_tmpz00_574 = BINT(0L);
																						{	/* Tools/misc.scm 174 */
																							bool_t BgL_test1641z00_1009;

																							{	/* Tools/misc.scm 174 */
																								obj_t BgL_tmpz00_1010;

																								BgL_tmpz00_1010 = BINT(1L);
																								BgL_test1641z00_1009 =
																									BGL_ADDFX_OV(BgL_iz00_146,
																									BgL_tmpz00_1010,
																									BgL_tmpz00_574);
																							}
																							if (BgL_test1641z00_1009)
																								{	/* Tools/misc.scm 174 */
																									BgL_arg1191z00_163 =
																										bgl_bignum_add
																										(bgl_long_to_bignum((long)
																											CINT(BgL_iz00_146)),
																										CNST_TABLE_REF(0));
																								}
																							else
																								{	/* Tools/misc.scm 174 */
																									BgL_arg1191z00_163 =
																										BgL_tmpz00_574;
																								}
																						}
																					}
																				else
																					{	/* Tools/misc.scm 174 */
																						BgL_arg1191z00_163 =
																							BGl_2zb2zb2zz__r4_numbers_6_5z00
																							(BgL_iz00_146, BINT(1L));
																					}
																				BgL_arg1190z00_162 =
																					STRING_REF(
																					((obj_t) BgL_valz00_112),
																					(long) CINT(BgL_arg1191z00_163));
																			}
																			BgL_test1636z00_987 =
																				(BgL_arg1190z00_162 ==
																				((unsigned char) '\\'));
																}}
																if (BgL_test1636z00_987)
																	{	/* Tools/misc.scm 172 */
																		BgL_test1630z00_962 = ((bool_t) 0);
																	}
																else
																	{	/* Tools/misc.scm 172 */
																		BgL_test1630z00_962 = ((bool_t) 1);
																	}
															}
														else
															{	/* Tools/misc.scm 171 */
																BgL_test1630z00_962 = ((bool_t) 0);
															}
													}
												else
													{	/* Tools/misc.scm 170 */
														BgL_test1630z00_962 = ((bool_t) 0);
													}
												if (BgL_test1630z00_962)
													{	/* Tools/misc.scm 180 */
														{	/* Tools/misc.scm 182 */
															obj_t BgL_arg1162z00_142;

															{	/* Tools/misc.scm 182 */
																long BgL_startz00_597;
																long BgL_endz00_598;

																BgL_startz00_597 = (long) CINT(BgL_kz00_116);
																BgL_endz00_598 = (long) CINT(BgL_iz00_119);
																BgL_arg1162z00_142 =
																	c_substring(
																	((obj_t) BgL_valz00_112), BgL_startz00_597,
																	BgL_endz00_598);
															}
															BgL_resz00_113 =
																MAKE_YOUNG_PAIR(BgL_arg1162z00_142,
																BgL_resz00_113);
														}
														if (INTEGERP(BgL_iz00_119))
															{	/* Tools/misc.scm 183 */
																obj_t BgL_tmpz00_601;

																BgL_tmpz00_601 = BINT(0L);
																{	/* Tools/misc.scm 183 */
																	bool_t BgL_test1643z00_1031;

																	{	/* Tools/misc.scm 183 */
																		obj_t BgL_tmpz00_1032;

																		BgL_tmpz00_1032 = BINT(1L);
																		BgL_test1643z00_1031 =
																			BGL_ADDFX_OV(BgL_iz00_119,
																			BgL_tmpz00_1032, BgL_tmpz00_601);
																	}
																	if (BgL_test1643z00_1031)
																		{	/* Tools/misc.scm 183 */
																			BgL_kz00_116 =
																				bgl_bignum_add(bgl_long_to_bignum(
																					(long) CINT(BgL_iz00_119)),
																				CNST_TABLE_REF(0));
																		}
																	else
																		{	/* Tools/misc.scm 183 */
																			BgL_kz00_116 = BgL_tmpz00_601;
																		}
																}
															}
														else
															{	/* Tools/misc.scm 183 */
																BgL_kz00_116 =
																	BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_iz00_119,
																	BINT(1L));
															}
													}
												else
													{	/* Tools/misc.scm 180 */
														BFALSE;
													}
											}
											{	/* Tools/misc.scm 177 */
												obj_t BgL_arg1164z00_143;

												if (INTEGERP(BgL_iz00_119))
													{	/* Tools/misc.scm 177 */
														obj_t BgL_tmpz00_611;

														BgL_tmpz00_611 = BINT(0L);
														{	/* Tools/misc.scm 177 */
															bool_t BgL_test1645z00_1044;

															{	/* Tools/misc.scm 177 */
																obj_t BgL_tmpz00_1045;

																BgL_tmpz00_1045 = BINT(1L);
																BgL_test1645z00_1044 =
																	BGL_ADDFX_OV(BgL_iz00_119, BgL_tmpz00_1045,
																	BgL_tmpz00_611);
															}
															if (BgL_test1645z00_1044)
																{	/* Tools/misc.scm 177 */
																	BgL_arg1164z00_143 =
																		bgl_bignum_add(bgl_long_to_bignum(
																			(long) CINT(BgL_iz00_119)),
																		CNST_TABLE_REF(0));
																}
															else
																{	/* Tools/misc.scm 177 */
																	BgL_arg1164z00_143 = BgL_tmpz00_611;
																}
														}
													}
												else
													{	/* Tools/misc.scm 177 */
														BgL_arg1164z00_143 =
															BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_iz00_119,
															BINT(1L));
													}
												{
													obj_t BgL_iz00_1054;

													BgL_iz00_1054 = BgL_arg1164z00_143;
													BgL_iz00_119 = BgL_iz00_1054;
													goto BgL_zc3z04anonymousza31139ze3z87_120;
												}
											}
										}
								}
							}
						}
					}
				else
					{	/* Tools/misc.scm 165 */
						return BNIL;
					}
			}
		}

	}



/* normalize-mingw-path~0 */
	obj_t BGl_normaliza7ezd2mingwzd2pathze70z40zztools_miscz00(obj_t
		BgL_pathz00_170)
	{
		{	/* Tools/misc.scm 162 */
			{	/* Tools/misc.scm 152 */
				long BgL_nz00_172;

				BgL_nz00_172 = STRING_LENGTH(((obj_t) BgL_pathz00_170));
				if ((BgL_nz00_172 < 3L))
					{	/* Tools/misc.scm 154 */
						return BgL_pathz00_170;
					}
				else
					{	/* Tools/misc.scm 154 */
						if (
							(STRING_REF(
									((obj_t) BgL_pathz00_170), 0L) == ((unsigned char) '/')))
							{	/* Tools/misc.scm 156 */
								if (
									(STRING_REF(
											((obj_t) BgL_pathz00_170), 2L) == ((unsigned char) '/')))
									{	/* Tools/misc.scm 158 */
										obj_t BgL_arg1203z00_179;
										obj_t BgL_arg1206z00_180;

										{	/* Tools/misc.scm 158 */
											unsigned char BgL_arg1208z00_181;

											BgL_arg1208z00_181 =
												STRING_REF(((obj_t) BgL_pathz00_170), 1L);
											{	/* Tools/misc.scm 158 */
												obj_t BgL_list1209z00_182;

												BgL_list1209z00_182 =
													MAKE_YOUNG_PAIR(BCHAR(BgL_arg1208z00_181), BNIL);
												BgL_arg1203z00_179 =
													BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
													(BgL_list1209z00_182);
										}}
										BgL_arg1206z00_180 =
											c_substring(((obj_t) BgL_pathz00_170), 3L, BgL_nz00_172);
										return
											string_append_3(BgL_arg1203z00_179,
											BGl_string1570z00zztools_miscz00, BgL_arg1206z00_180);
									}
								else
									{	/* Tools/misc.scm 157 */
										return BgL_pathz00_170;
									}
							}
						else
							{	/* Tools/misc.scm 156 */
								return BgL_pathz00_170;
							}
					}
			}
		}

	}



/* unix-build-path-from-shell-variable */
	obj_t
		BGl_unixzd2buildzd2pathzd2fromzd2shellzd2variablezd2zztools_miscz00(obj_t
		BgL_varz00_20)
	{
		{	/* Tools/misc.scm 188 */
			{	/* Tools/misc.scm 189 */
				obj_t BgL_valz00_186;

				BgL_valz00_186 = BGl_getenvz00zz__osz00(BgL_varz00_20);
				if (STRINGP(BgL_valz00_186))
					{	/* Tools/misc.scm 191 */
						obj_t BgL_port1014z00_188;

						{	/* Tools/misc.scm 191 */
							long BgL_endz00_433;

							BgL_endz00_433 = STRING_LENGTH(BgL_valz00_186);
							{	/* Tools/misc.scm 191 */

								BgL_port1014z00_188 =
									BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
									(BgL_valz00_186, BINT(0L), BINT(BgL_endz00_433));
						}}
						{	/* Tools/misc.scm 191 */
							obj_t BgL_exitd1015z00_189;

							BgL_exitd1015z00_189 = BGL_EXITD_TOP_AS_OBJ();
							{	/* Tools/misc.scm 191 */
								obj_t BgL_zc3z04anonymousza31474ze3z87_713;

								BgL_zc3z04anonymousza31474ze3z87_713 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31474ze3ze5zztools_miscz00,
									(int) (0L), (int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31474ze3z87_713, (int) (0L),
									BgL_port1014z00_188);
								{	/* Tools/misc.scm 191 */
									obj_t BgL_arg1828z00_621;

									{	/* Tools/misc.scm 191 */
										obj_t BgL_arg1829z00_622;

										BgL_arg1829z00_622 =
											BGL_EXITD_PROTECT(BgL_exitd1015z00_189);
										BgL_arg1828z00_621 =
											MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31474ze3z87_713,
											BgL_arg1829z00_622);
									}
									BGL_EXITD_PROTECT_SET(BgL_exitd1015z00_189,
										BgL_arg1828z00_621);
									BUNSPEC;
								}
								{	/* Tools/misc.scm 191 */
									obj_t BgL_tmp1017z00_191;

									BgL_tmp1017z00_191 =
										BGl_ignoreze70ze7zztools_miscz00(BgL_port1014z00_188,
										BgL_port1014z00_188);
									{	/* Tools/misc.scm 191 */
										bool_t BgL_test1650z00_1093;

										{	/* Tools/misc.scm 191 */
											obj_t BgL_arg1827z00_670;

											BgL_arg1827z00_670 =
												BGL_EXITD_PROTECT(BgL_exitd1015z00_189);
											BgL_test1650z00_1093 = PAIRP(BgL_arg1827z00_670);
										}
										if (BgL_test1650z00_1093)
											{	/* Tools/misc.scm 191 */
												obj_t BgL_arg1825z00_671;

												{	/* Tools/misc.scm 191 */
													obj_t BgL_arg1826z00_672;

													BgL_arg1826z00_672 =
														BGL_EXITD_PROTECT(BgL_exitd1015z00_189);
													BgL_arg1825z00_671 =
														CDR(((obj_t) BgL_arg1826z00_672));
												}
												BGL_EXITD_PROTECT_SET(BgL_exitd1015z00_189,
													BgL_arg1825z00_671);
												BUNSPEC;
											}
										else
											{	/* Tools/misc.scm 191 */
												BFALSE;
											}
									}
									bgl_close_input_port(BgL_port1014z00_188);
									return BgL_tmp1017z00_191;
								}
							}
						}
					}
				else
					{	/* Tools/misc.scm 190 */
						return BNIL;
					}
			}
		}

	}



/* ignore~0 */
	obj_t BGl_ignoreze70ze7zztools_miscz00(obj_t BgL_port1014z00_719,
		obj_t BgL_iportz00_718)
	{
		{	/* Tools/misc.scm 191 */
		BGl_ignoreze70ze7zztools_miscz00:
			{
				obj_t BgL_iportz00_225;
				long BgL_lastzd2matchzd2_226;
				long BgL_forwardz00_227;
				long BgL_bufposz00_228;
				obj_t BgL_iportz00_238;
				long BgL_lastzd2matchzd2_239;
				long BgL_forwardz00_240;
				long BgL_bufposz00_241;
				obj_t BgL_iportz00_257;
				long BgL_lastzd2matchzd2_258;
				long BgL_forwardz00_259;
				long BgL_bufposz00_260;

				RGC_START_MATCH(BgL_iportz00_718);
				{	/* Tools/misc.scm 191 */
					long BgL_matchz00_389;

					{	/* Tools/misc.scm 191 */
						long BgL_arg1472z00_396;
						long BgL_arg1473z00_397;

						BgL_arg1472z00_396 = RGC_BUFFER_FORWARD(BgL_iportz00_718);
						BgL_arg1473z00_397 = RGC_BUFFER_BUFPOS(BgL_iportz00_718);
						BgL_iportz00_225 = BgL_port1014z00_719;
						BgL_lastzd2matchzd2_226 = 2L;
						BgL_forwardz00_227 = BgL_arg1472z00_396;
						BgL_bufposz00_228 = BgL_arg1473z00_397;
					BgL_zc3z04anonymousza31214ze3z87_229:
						if ((BgL_forwardz00_227 == BgL_bufposz00_228))
							{	/* Tools/misc.scm 191 */
								if (rgc_fill_buffer(BgL_iportz00_225))
									{	/* Tools/misc.scm 191 */
										long BgL_arg1218z00_232;
										long BgL_arg1219z00_233;

										BgL_arg1218z00_232 = RGC_BUFFER_FORWARD(BgL_iportz00_225);
										BgL_arg1219z00_233 = RGC_BUFFER_BUFPOS(BgL_iportz00_225);
										{
											long BgL_bufposz00_1111;
											long BgL_forwardz00_1110;

											BgL_forwardz00_1110 = BgL_arg1218z00_232;
											BgL_bufposz00_1111 = BgL_arg1219z00_233;
											BgL_bufposz00_228 = BgL_bufposz00_1111;
											BgL_forwardz00_227 = BgL_forwardz00_1110;
											goto BgL_zc3z04anonymousza31214ze3z87_229;
										}
									}
								else
									{	/* Tools/misc.scm 191 */
										BgL_matchz00_389 = BgL_lastzd2matchzd2_226;
									}
							}
						else
							{	/* Tools/misc.scm 191 */
								int BgL_curz00_234;

								BgL_curz00_234 =
									RGC_BUFFER_GET_CHAR(BgL_iportz00_225, BgL_forwardz00_227);
								{	/* Tools/misc.scm 191 */

									if (((long) (BgL_curz00_234) == 58L))
										{	/* Tools/misc.scm 191 */
											long BgL_arg1221z00_236;

											BgL_arg1221z00_236 = (1L + BgL_forwardz00_227);
											{	/* Tools/misc.scm 191 */
												long BgL_newzd2matchzd2_632;

												RGC_STOP_MATCH(BgL_iportz00_225, BgL_arg1221z00_236);
												BgL_newzd2matchzd2_632 = 1L;
												BgL_matchz00_389 = BgL_newzd2matchzd2_632;
										}}
									else
										{	/* Tools/misc.scm 191 */
											BgL_iportz00_257 = BgL_iportz00_225;
											BgL_lastzd2matchzd2_258 = BgL_lastzd2matchzd2_226;
											BgL_forwardz00_259 = (1L + BgL_forwardz00_227);
											BgL_bufposz00_260 = BgL_bufposz00_228;
										BgL_zc3z04anonymousza31232ze3z87_261:
											{	/* Tools/misc.scm 191 */
												long BgL_newzd2matchzd2_262;

												RGC_STOP_MATCH(BgL_iportz00_257, BgL_forwardz00_259);
												BgL_newzd2matchzd2_262 = 0L;
												if ((BgL_forwardz00_259 == BgL_bufposz00_260))
													{	/* Tools/misc.scm 191 */
														if (rgc_fill_buffer(BgL_iportz00_257))
															{	/* Tools/misc.scm 191 */
																long BgL_arg1236z00_265;
																long BgL_arg1238z00_266;

																BgL_arg1236z00_265 =
																	RGC_BUFFER_FORWARD(BgL_iportz00_257);
																BgL_arg1238z00_266 =
																	RGC_BUFFER_BUFPOS(BgL_iportz00_257);
																{
																	long BgL_bufposz00_1126;
																	long BgL_forwardz00_1125;

																	BgL_forwardz00_1125 = BgL_arg1236z00_265;
																	BgL_bufposz00_1126 = BgL_arg1238z00_266;
																	BgL_bufposz00_260 = BgL_bufposz00_1126;
																	BgL_forwardz00_259 = BgL_forwardz00_1125;
																	goto BgL_zc3z04anonymousza31232ze3z87_261;
																}
															}
														else
															{	/* Tools/misc.scm 191 */
																BgL_matchz00_389 = BgL_newzd2matchzd2_262;
															}
													}
												else
													{	/* Tools/misc.scm 191 */
														int BgL_curz00_267;

														BgL_curz00_267 =
															RGC_BUFFER_GET_CHAR(BgL_iportz00_257,
															BgL_forwardz00_259);
														{	/* Tools/misc.scm 191 */

															if (((long) (BgL_curz00_267) == 58L))
																{	/* Tools/misc.scm 191 */
																	BgL_matchz00_389 = BgL_newzd2matchzd2_262;
																}
															else
																{	/* Tools/misc.scm 191 */
																	BgL_iportz00_238 = BgL_iportz00_257;
																	BgL_lastzd2matchzd2_239 =
																		BgL_newzd2matchzd2_262;
																	BgL_forwardz00_240 =
																		(1L + BgL_forwardz00_259);
																	BgL_bufposz00_241 = BgL_bufposz00_260;
																BgL_zc3z04anonymousza31224ze3z87_242:
																	{	/* Tools/misc.scm 191 */
																		long BgL_newzd2matchzd2_243;

																		RGC_STOP_MATCH(BgL_iportz00_238,
																			BgL_forwardz00_240);
																		BgL_newzd2matchzd2_243 = 0L;
																		if (
																			(BgL_forwardz00_240 == BgL_bufposz00_241))
																			{	/* Tools/misc.scm 191 */
																				if (rgc_fill_buffer(BgL_iportz00_238))
																					{	/* Tools/misc.scm 191 */
																						long BgL_arg1227z00_246;
																						long BgL_arg1228z00_247;

																						BgL_arg1227z00_246 =
																							RGC_BUFFER_FORWARD
																							(BgL_iportz00_238);
																						BgL_arg1228z00_247 =
																							RGC_BUFFER_BUFPOS
																							(BgL_iportz00_238);
																						{
																							long BgL_bufposz00_1139;
																							long BgL_forwardz00_1138;

																							BgL_forwardz00_1138 =
																								BgL_arg1227z00_246;
																							BgL_bufposz00_1139 =
																								BgL_arg1228z00_247;
																							BgL_bufposz00_241 =
																								BgL_bufposz00_1139;
																							BgL_forwardz00_240 =
																								BgL_forwardz00_1138;
																							goto
																								BgL_zc3z04anonymousza31224ze3z87_242;
																						}
																					}
																				else
																					{	/* Tools/misc.scm 191 */
																						BgL_matchz00_389 =
																							BgL_newzd2matchzd2_243;
																					}
																			}
																		else
																			{	/* Tools/misc.scm 191 */
																				int BgL_curz00_248;

																				BgL_curz00_248 =
																					RGC_BUFFER_GET_CHAR(BgL_iportz00_238,
																					BgL_forwardz00_240);
																				{	/* Tools/misc.scm 191 */

																					if (((long) (BgL_curz00_248) == 58L))
																						{	/* Tools/misc.scm 191 */
																							BgL_matchz00_389 =
																								BgL_newzd2matchzd2_243;
																						}
																					else
																						{
																							long BgL_forwardz00_1145;
																							long BgL_lastzd2matchzd2_1144;

																							BgL_lastzd2matchzd2_1144 =
																								BgL_newzd2matchzd2_243;
																							BgL_forwardz00_1145 =
																								(1L + BgL_forwardz00_240);
																							BgL_forwardz00_240 =
																								BgL_forwardz00_1145;
																							BgL_lastzd2matchzd2_239 =
																								BgL_lastzd2matchzd2_1144;
																							goto
																								BgL_zc3z04anonymousza31224ze3z87_242;
																						}
																				}
																			}
																	}
																}
														}
													}
											}
										}
								}
							}
					}
					RGC_SET_FILEPOS(BgL_iportz00_718);
					{

						switch (BgL_matchz00_389)
							{
							case 2L:

								return BNIL;
								break;
							case 1L:

								{

									goto BGl_ignoreze70ze7zztools_miscz00;
								}
								break;
							case 0L:

								{	/* Tools/misc.scm 193 */
									obj_t BgL_strz00_394;

									{	/* Tools/misc.scm 191 */
										long BgL_arg1249z00_281;

										BgL_arg1249z00_281 =
											RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_718);
										BgL_strz00_394 =
											rgc_buffer_substring(BgL_iportz00_718, 0L,
											BgL_arg1249z00_281);
									}
									{	/* Tools/misc.scm 193 */
										obj_t BgL_resz00_395;

										BgL_resz00_395 =
											BGl_ignoreze70ze7zztools_miscz00(BgL_port1014z00_719,
											BgL_iportz00_718);
										{	/* Tools/misc.scm 194 */

											return MAKE_YOUNG_PAIR(BgL_strz00_394, BgL_resz00_395);
										}
									}
								}
								break;
							default:
								return
									BGl_errorz00zz__errorz00(BGl_string1571z00zztools_miscz00,
									BGl_string1572z00zztools_miscz00, BINT(BgL_matchz00_389));
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1474> */
	obj_t BGl_z62zc3z04anonymousza31474ze3ze5zztools_miscz00(obj_t BgL_envz00_714)
	{
		{	/* Tools/misc.scm 191 */
			{	/* Tools/misc.scm 191 */
				obj_t BgL_port1014z00_715;

				BgL_port1014z00_715 =
					((obj_t) PROCEDURE_REF(BgL_envz00_714, (int) (0L)));
				return bgl_close_input_port(BgL_port1014z00_715);
			}
		}

	}



/* uncygdrive */
	BGL_EXPORTED_DEF obj_t BGl_uncygdrivez00zztools_miscz00(obj_t BgL_strz00_21)
	{
		{	/* Tools/misc.scm 208 */
			if (bigloo_strncmp(BGl_string1573z00zztools_miscz00, BgL_strz00_21, 10L))
				{	/* Tools/misc.scm 210 */
					bool_t BgL_test1663z00_1163;

					if ((STRING_LENGTH(BgL_strz00_21) > 12L))
						{	/* Tools/misc.scm 211 */
							bool_t BgL_test1665z00_1167;

							{	/* Tools/misc.scm 211 */
								unsigned char BgL_tmpz00_1168;

								BgL_tmpz00_1168 = STRING_REF(BgL_strz00_21, 10L);
								BgL_test1665z00_1167 = isalpha(BgL_tmpz00_1168);
							}
							if (BgL_test1665z00_1167)
								{	/* Tools/misc.scm 211 */
									BgL_test1663z00_1163 =
										(STRING_REF(BgL_strz00_21, 11L) == ((unsigned char) '/'));
								}
							else
								{	/* Tools/misc.scm 211 */
									BgL_test1663z00_1163 = ((bool_t) 0);
								}
						}
					else
						{	/* Tools/misc.scm 210 */
							BgL_test1663z00_1163 = ((bool_t) 0);
						}
					if (BgL_test1663z00_1163)
						{	/* Tools/misc.scm 213 */
							obj_t BgL_arg1516z00_443;
							obj_t BgL_arg1535z00_444;

							{	/* Tools/misc.scm 213 */
								unsigned char BgL_arg1540z00_445;

								BgL_arg1540z00_445 = STRING_REF(BgL_strz00_21, 10L);
								{	/* Tools/misc.scm 213 */
									obj_t BgL_list1541z00_446;

									{	/* Tools/misc.scm 213 */
										obj_t BgL_arg1544z00_447;

										{	/* Tools/misc.scm 213 */
											obj_t BgL_arg1546z00_448;

											BgL_arg1546z00_448 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) '/')), BNIL);
											BgL_arg1544z00_447 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) ':')),
												BgL_arg1546z00_448);
										}
										BgL_list1541z00_446 =
											MAKE_YOUNG_PAIR(BCHAR(BgL_arg1540z00_445),
											BgL_arg1544z00_447);
									}
									BgL_arg1516z00_443 =
										BGl_listzd2ze3stringz31zz__r4_strings_6_7z00
										(BgL_list1541z00_446);
							}}
							BgL_arg1535z00_444 =
								c_substring(BgL_strz00_21, 12L, STRING_LENGTH(BgL_strz00_21));
							return string_append(BgL_arg1516z00_443, BgL_arg1535z00_444);
						}
					else
						{	/* Tools/misc.scm 210 */
							return BgL_strz00_21;
						}
				}
			else
				{	/* Tools/misc.scm 209 */
					return BgL_strz00_21;
				}
		}

	}



/* &uncygdrive */
	obj_t BGl_z62uncygdrivez62zztools_miscz00(obj_t BgL_envz00_716,
		obj_t BgL_strz00_717)
	{
		{	/* Tools/misc.scm 208 */
			return BGl_uncygdrivez00zztools_miscz00(BgL_strz00_717);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_miscz00(void)
	{
		{	/* Tools/misc.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_miscz00(void)
	{
		{	/* Tools/misc.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_miscz00(void)
	{
		{	/* Tools/misc.scm 15 */
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
