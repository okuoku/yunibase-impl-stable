/*===========================================================================*/
/*   (Tools/error.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tools/error.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TOOLS_ERROR_TYPE_DEFINITIONS
#define BGL_TOOLS_ERROR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_z62errorz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
		obj_t BgL_procz00;
		obj_t BgL_msgz00;
		obj_t BgL_objz00;
	}                  *BgL_z62errorz62_bglt;

	typedef struct BgL_z62exceptionz62_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_fnamez00;
		obj_t BgL_locationz00;
		obj_t BgL_stackz00;
	}                      *BgL_z62exceptionz62_bglt;


#endif													// BGL_TOOLS_ERROR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62internalzd2errorzb0zztools_errorz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_sharpzd2symbolzd2zztools_errorz00 = BUNSPEC;
	static obj_t BGl_z62userzd2errorzd2notifyz62zztools_errorz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62leavezd2functionzb0zztools_errorz00(obj_t);
	BGL_IMPORT int BGl_bigloozd2warningzd2zz__paramz00(void);
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztools_errorz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2compilerzd2stackzd2debugzf3za2zf3zzengine_paramz00;
	extern obj_t BGl_za2bigloozd2emailza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_za2sfunzd2stackza2zd2zztools_errorz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zztools_errorz00(void);
	BGL_IMPORT obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zztools_errorz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zztools_errorz00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31305ze3ze5zztools_errorz00(obj_t);
	BGL_IMPORT obj_t BGl_errorzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_warningzf2locationzf2zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztools_errorz00(void);
	static obj_t BGl_z62zc3z04anonymousza31324ze3ze5zztools_errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_nozd2warningzd2zztools_errorz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_bigloozd2warningzd2setz12z12zz__paramz00(int);
	BGL_IMPORT obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_z62enterzd2functionzb0zztools_errorz00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_z62nozd2warningzb0zztools_errorz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31343ze3ze5zztools_errorz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31353ze3ze5zztools_errorz00(obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_IMPORT obj_t BGl_errorzd2notifyzd2zz__errorz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31274ze3ze5zztools_errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__pp_circlez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__paramz00(long, char *);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	BGL_IMPORT obj_t BGl_z62errorz62zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_userzd2errorzd2notifyz00zztools_errorz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31349ze3ze5zztools_errorz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zztools_errorz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zztools_errorz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztools_errorz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztools_errorz00(void);
	static obj_t BGl_z62userzd2warningzb0zztools_errorz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04exitza31345ze3ze70z60zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2bigloozd2authorza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_displayzd2circlezd2zz__pp_circlez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_currentzd2functionzd2zztools_errorz00(void);
	static obj_t BGl_withzd2dumpzd2stackz00zztools_errorz00(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_z62userzd2errorzf2locationz42zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t
		BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT int BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00(void);
	BGL_EXPORTED_DECL obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_withzd2exceptionzd2handlerz00zz__errorz00(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(int);
	static obj_t BGl_z62currentzd2functionzb0zztools_errorz00(obj_t);
	extern obj_t BGl_compilerzd2exitzd2zzinit_mainz00(obj_t);
	static obj_t BGl_z62userzd2warningzf2locationz42zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62userzd2errorzb0zztools_errorz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2tracezd2portza2zd2zztools_tracez00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_userzd2warningzf2locationzd2envzf2zztools_errorz00,
		BgL_bgl_za762userza7d2warnin1662z00,
		BGl_z62userzd2warningzf2locationz42zztools_errorz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_userzd2errorzd2notifyzd2envzd2zztools_errorz00,
		BgL_bgl_za762userza7d2errorza71663za7,
		BGl_z62userzd2errorzd2notifyz62zztools_errorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_leavezd2functionzd2envz00zztools_errorz00,
		BgL_bgl_za762leaveza7d2funct1664z00,
		BGl_z62leavezd2functionzb0zztools_errorz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1649z00zztools_errorz00,
		BgL_bgl_string1649za700za7za7t1665za7, "#", 1);
	      DEFINE_STRING(BGl_string1650z00zztools_errorz00,
		BgL_bgl_string1650za700za7za7t1666za7, "*** ERROR: ", 11);
	      DEFINE_STRING(BGl_string1651z00zztools_errorz00,
		BgL_bgl_string1651za700za7za7t1667za7, ":", 1);
	      DEFINE_STRING(BGl_string1652z00zztools_errorz00,
		BgL_bgl_string1652za700za7za7t1668za7, "*** INTERNAL-ERROR in pass: ", 28);
	      DEFINE_STRING(BGl_string1653z00zztools_errorz00,
		BgL_bgl_string1653za700za7za7t1669za7,
		"(Would you, please, send this error report and the source file to", 65);
	      DEFINE_STRING(BGl_string1654z00zztools_errorz00,
		BgL_bgl_string1654za700za7za7t1670za7, " [", 2);
	      DEFINE_STRING(BGl_string1655z00zztools_errorz00,
		BgL_bgl_string1655za700za7za7t1671za7, "], thank you.)", 14);
	      DEFINE_STRING(BGl_string1656z00zztools_errorz00,
		BgL_bgl_string1656za700za7za7t1672za7, " -- ", 4);
	      DEFINE_STRING(BGl_string1657z00zztools_errorz00,
		BgL_bgl_string1657za700za7za7t1673za7, "*** ERROR:", 10);
	      DEFINE_STRING(BGl_string1658z00zztools_errorz00,
		BgL_bgl_string1658za700za7za7t1674za7, " ...", 4);
	      DEFINE_STRING(BGl_string1659z00zztools_errorz00,
		BgL_bgl_string1659za700za7za7t1675za7, "tools_error", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nozd2warningzd2envz00zztools_errorz00,
		BgL_bgl_za762noza7d2warningza71676za7,
		BGl_z62nozd2warningzb0zztools_errorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1660z00zztools_errorz00,
		BgL_bgl_string1660za700za7za7t1677za7, "location (top-level) ", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_currentzd2functionzd2envz00zztools_errorz00,
		BgL_bgl_za762currentza7d2fun1678z00,
		BGl_z62currentzd2functionzb0zztools_errorz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_userzd2warningzd2envz00zztools_errorz00,
		BgL_bgl_za762userza7d2warnin1679z00,
		BGl_z62userzd2warningzb0zztools_errorz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_userzd2errorzd2envz00zztools_errorz00,
		BgL_bgl_za762userza7d2errorza71680za7, va_generic_entry,
		BGl_z62userzd2errorzb0zztools_errorz00, BUNSPEC, -4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_internalzd2errorzd2envz00zztools_errorz00,
		BgL_bgl_za762internalza7d2er1681z00,
		BGl_z62internalzd2errorzb0zztools_errorz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_enterzd2functionzd2envz00zztools_errorz00,
		BgL_bgl_za762enterza7d2funct1682z00,
		BGl_z62enterzd2functionzb0zztools_errorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_userzd2errorzf2locationzd2envzf2zztools_errorz00,
		BgL_bgl_za762userza7d2errorza71683za7, va_generic_entry,
		BGl_z62userzd2errorzf2locationz42zztools_errorz00, BUNSPEC, -5);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_sharpzd2symbolzd2zztools_errorz00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztools_errorz00));
		     ADD_ROOT((void *) (&BGl_za2sfunzd2stackza2zd2zztools_errorz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long
		BgL_checksumz00_1896, char *BgL_fromz00_1897)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztools_errorz00))
				{
					BGl_requirezd2initializa7ationz75zztools_errorz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztools_errorz00();
					BGl_libraryzd2moduleszd2initz00zztools_errorz00();
					BGl_cnstzd2initzd2zztools_errorz00();
					BGl_importedzd2moduleszd2initz00zztools_errorz00();
					return BGl_toplevelzd2initzd2zztools_errorz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztools_errorz00(void)
	{
		{	/* Tools/error.scm 15 */
			BGl_modulezd2initializa7ationz75zz__paramz00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"tools_error");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__pp_circlez00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"tools_error");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tools_error");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tools_error");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "tools_error");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "tools_error");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztools_errorz00(void)
	{
		{	/* Tools/error.scm 15 */
			{	/* Tools/error.scm 15 */
				obj_t BgL_cportz00_1854;

				{	/* Tools/error.scm 15 */
					obj_t BgL_stringz00_1861;

					BgL_stringz00_1861 = BGl_string1660z00zztools_errorz00;
					{	/* Tools/error.scm 15 */
						obj_t BgL_startz00_1862;

						BgL_startz00_1862 = BINT(0L);
						{	/* Tools/error.scm 15 */
							obj_t BgL_endz00_1863;

							BgL_endz00_1863 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1861)));
							{	/* Tools/error.scm 15 */

								BgL_cportz00_1854 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1861, BgL_startz00_1862, BgL_endz00_1863);
				}}}}
				{
					long BgL_iz00_1855;

					BgL_iz00_1855 = 1L;
				BgL_loopz00_1856:
					if ((BgL_iz00_1855 == -1L))
						{	/* Tools/error.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tools/error.scm 15 */
							{	/* Tools/error.scm 15 */
								obj_t BgL_arg1661z00_1857;

								{	/* Tools/error.scm 15 */

									{	/* Tools/error.scm 15 */
										obj_t BgL_locationz00_1859;

										BgL_locationz00_1859 = BBOOL(((bool_t) 0));
										{	/* Tools/error.scm 15 */

											BgL_arg1661z00_1857 =
												BGl_readz00zz__readerz00(BgL_cportz00_1854,
												BgL_locationz00_1859);
										}
									}
								}
								{	/* Tools/error.scm 15 */
									int BgL_tmpz00_1931;

									BgL_tmpz00_1931 = (int) (BgL_iz00_1855);
									CNST_TABLE_SET(BgL_tmpz00_1931, BgL_arg1661z00_1857);
							}}
							{	/* Tools/error.scm 15 */
								int BgL_auxz00_1860;

								BgL_auxz00_1860 = (int) ((BgL_iz00_1855 - 1L));
								{
									long BgL_iz00_1936;

									BgL_iz00_1936 = (long) (BgL_auxz00_1860);
									BgL_iz00_1855 = BgL_iz00_1936;
									goto BgL_loopz00_1856;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztools_errorz00(void)
	{
		{	/* Tools/error.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztools_errorz00(void)
	{
		{	/* Tools/error.scm 15 */
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2sfunzd2stackza2zd2zztools_errorz00 = CNST_TABLE_REF(0);
			return (BGl_sharpzd2symbolzd2zztools_errorz00 =
				bstring_to_symbol(BGl_string1649z00zztools_errorz00), BUNSPEC);
		}

	}



/* internal-error */
	BGL_EXPORTED_DEF obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t
		BgL_procz00_17, obj_t BgL_mesz00_18, obj_t BgL_objz00_19)
	{
		{	/* Tools/error.scm 45 */
			if (OUTPUT_PORTP(BGl_za2tracezd2portza2zd2zztools_tracez00))
				{	/* Tools/error.scm 47 */
					obj_t BgL_port1237z00_1403;

					BgL_port1237z00_1403 = BGl_za2tracezd2portza2zd2zztools_tracez00;
					bgl_display_string(BGl_string1650z00zztools_errorz00,
						BgL_port1237z00_1403);
					bgl_display_obj(BgL_procz00_17, BgL_port1237z00_1403);
					bgl_display_string(BGl_string1651z00zztools_errorz00,
						BgL_port1237z00_1403);
					bgl_display_obj(BgL_mesz00_18, BgL_port1237z00_1403);
					bgl_display_string(BGl_string1651z00zztools_errorz00,
						BgL_port1237z00_1403);
					bgl_display_obj(BgL_objz00_19, BgL_port1237z00_1403);
					bgl_display_char(((unsigned char) 10), BgL_port1237z00_1403);
				}
			else
				{	/* Tools/error.scm 46 */
					BFALSE;
				}
			{	/* Tools/error.scm 48 */
				obj_t BgL_port1238z00_1404;

				{	/* Tools/error.scm 48 */
					obj_t BgL_tmpz00_1951;

					BgL_tmpz00_1951 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1238z00_1404 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1951);
				}
				bgl_display_string(BGl_string1652z00zztools_errorz00,
					BgL_port1238z00_1404);
				bgl_display_obj(BGl_za2currentzd2passza2zd2zzengine_passz00,
					BgL_port1238z00_1404);
				bgl_display_char(((unsigned char) 10), BgL_port1238z00_1404);
			}
			{	/* Tools/error.scm 50 */
				obj_t BgL_port1239z00_1405;

				{	/* Tools/error.scm 50 */
					obj_t BgL_tmpz00_1957;

					BgL_tmpz00_1957 = BGL_CURRENT_DYNAMIC_ENV();
					BgL_port1239z00_1405 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1957);
				}
				bgl_display_string(BGl_string1653z00zztools_errorz00,
					BgL_port1239z00_1405);
				bgl_display_char(((unsigned char) 10), BgL_port1239z00_1405);
				bgl_display_obj(BGl_za2bigloozd2authorza2zd2zzengine_paramz00,
					BgL_port1239z00_1405);
				bgl_display_string(BGl_string1654z00zztools_errorz00,
					BgL_port1239z00_1405);
				bgl_display_obj(BGl_za2bigloozd2emailza2zd2zzengine_paramz00,
					BgL_port1239z00_1405);
				bgl_display_string(BGl_string1655z00zztools_errorz00,
					BgL_port1239z00_1405);
				bgl_display_char(((unsigned char) 10), BgL_port1239z00_1405);
			}
			BGl_errorz00zz__errorz00(BgL_procz00_17, BgL_mesz00_18, BgL_objz00_19);
			return BGl_compilerzd2exitzd2zzinit_mainz00(BINT(1L));
		}

	}



/* &internal-error */
	obj_t BGl_z62internalzd2errorzb0zztools_errorz00(obj_t BgL_envz00_1784,
		obj_t BgL_procz00_1785, obj_t BgL_mesz00_1786, obj_t BgL_objz00_1787)
	{
		{	/* Tools/error.scm 45 */
			return
				BGl_internalzd2errorzd2zztools_errorz00(BgL_procz00_1785,
				BgL_mesz00_1786, BgL_objz00_1787);
		}

	}



/* with-dump-stack */
	obj_t BGl_withzd2dumpzd2stackz00zztools_errorz00(obj_t BgL_thunkz00_20)
	{
		{	/* Tools/error.scm 60 */
			if (CBOOL(BGl_za2compilerzd2stackzd2debugzf3za2zf3zzengine_paramz00))
				{	/* Tools/error.scm 61 */
					return
						((obj_t(*)(obj_t))
						PROCEDURE_L_ENTRY(BgL_thunkz00_20)) (BgL_thunkz00_20);
				}
			else
				{	/* Tools/error.scm 63 */
					int BgL_stz00_1406;

					BgL_stz00_1406 = BGl_bigloozd2tracezd2stackzd2depthzd2zz__paramz00();
					BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(
						(int) (0L));
					{	/* Tools/error.scm 65 */
						obj_t BgL_exitd1102z00_1407;

						BgL_exitd1102z00_1407 = BGL_EXITD_TOP_AS_OBJ();
						{	/* Tools/error.scm 67 */
							obj_t BgL_zc3z04anonymousza31274ze3z87_1788;

							BgL_zc3z04anonymousza31274ze3z87_1788 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31274ze3ze5zztools_errorz00,
								(int) (0L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31274ze3z87_1788, (int) (0L),
								BINT(BgL_stz00_1406));
							{	/* Tools/error.scm 65 */
								obj_t BgL_arg1828z00_1689;

								{	/* Tools/error.scm 65 */
									obj_t BgL_arg1829z00_1690;

									BgL_arg1829z00_1690 =
										BGL_EXITD_PROTECT(BgL_exitd1102z00_1407);
									BgL_arg1828z00_1689 =
										MAKE_YOUNG_PAIR(BgL_zc3z04anonymousza31274ze3z87_1788,
										BgL_arg1829z00_1690);
								}
								BGL_EXITD_PROTECT_SET(BgL_exitd1102z00_1407,
									BgL_arg1828z00_1689);
								BUNSPEC;
							}
							{	/* Tools/error.scm 66 */
								obj_t BgL_tmp1104z00_1409;

								BgL_tmp1104z00_1409 =
									((obj_t(*)(obj_t))
									PROCEDURE_L_ENTRY(BgL_thunkz00_20)) (BgL_thunkz00_20);
								{	/* Tools/error.scm 65 */
									bool_t BgL_test1688z00_1992;

									{	/* Tools/error.scm 65 */
										obj_t BgL_arg1827z00_1692;

										BgL_arg1827z00_1692 =
											BGL_EXITD_PROTECT(BgL_exitd1102z00_1407);
										BgL_test1688z00_1992 = PAIRP(BgL_arg1827z00_1692);
									}
									if (BgL_test1688z00_1992)
										{	/* Tools/error.scm 65 */
											obj_t BgL_arg1825z00_1693;

											{	/* Tools/error.scm 65 */
												obj_t BgL_arg1826z00_1694;

												BgL_arg1826z00_1694 =
													BGL_EXITD_PROTECT(BgL_exitd1102z00_1407);
												BgL_arg1825z00_1693 =
													CDR(((obj_t) BgL_arg1826z00_1694));
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1102z00_1407,
												BgL_arg1825z00_1693);
											BUNSPEC;
										}
									else
										{	/* Tools/error.scm 65 */
											BFALSE;
										}
								}
								BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00
									(BgL_stz00_1406);
								return BgL_tmp1104z00_1409;
							}
						}
					}
				}
		}

	}



/* &<@anonymous:1274> */
	obj_t BGl_z62zc3z04anonymousza31274ze3ze5zztools_errorz00(obj_t
		BgL_envz00_1789)
	{
		{	/* Tools/error.scm 65 */
			return
				BGl_bigloozd2tracezd2stackzd2depthzd2setz12z12zz__paramz00(CINT
				(PROCEDURE_REF(BgL_envz00_1789, (int) (0L))));
		}

	}



/* user-warning/location */
	BGL_EXPORTED_DEF obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t
		BgL_locz00_21, obj_t BgL_procz00_22, obj_t BgL_mesz00_23,
		obj_t BgL_objz00_24)
	{
		{	/* Tools/error.scm 72 */
			{	/* Tools/error.scm 73 */
				bool_t BgL_test1689z00_2004;

				{	/* Tools/error.scm 73 */
					int BgL_arg1320z00_1428;

					BgL_arg1320z00_1428 = BGl_bigloozd2warningzd2zz__paramz00();
					BgL_test1689z00_2004 = ((long) (BgL_arg1320z00_1428) > 0L);
				}
				if (BgL_test1689z00_2004)
					{	/* Tools/error.scm 76 */
						obj_t BgL_zc3z04anonymousza31305ze3z87_1791;

						{
							int BgL_tmpz00_2008;

							BgL_tmpz00_2008 = (int) (4L);
							BgL_zc3z04anonymousza31305ze3z87_1791 =
								MAKE_L_PROCEDURE
								(BGl_z62zc3z04anonymousza31305ze3ze5zztools_errorz00,
								BgL_tmpz00_2008);
						}
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31305ze3z87_1791,
							(int) (0L), BgL_objz00_24);
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31305ze3z87_1791,
							(int) (1L), BgL_mesz00_23);
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31305ze3z87_1791,
							(int) (2L), BgL_procz00_22);
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31305ze3z87_1791,
							(int) (3L), BgL_locz00_21);
						return
							BGl_withzd2dumpzd2stackz00zztools_errorz00
							(BgL_zc3z04anonymousza31305ze3z87_1791);
					}
				else
					{	/* Tools/error.scm 73 */
						return BFALSE;
					}
			}
		}

	}



/* &user-warning/location */
	obj_t BGl_z62userzd2warningzf2locationz42zztools_errorz00(obj_t
		BgL_envz00_1792, obj_t BgL_locz00_1793, obj_t BgL_procz00_1794,
		obj_t BgL_mesz00_1795, obj_t BgL_objz00_1796)
	{
		{	/* Tools/error.scm 72 */
			return
				BGl_userzd2warningzf2locationz20zztools_errorz00(BgL_locz00_1793,
				BgL_procz00_1794, BgL_mesz00_1795, BgL_objz00_1796);
		}

	}



/* &<@anonymous:1305> */
	obj_t BGl_z62zc3z04anonymousza31305ze3ze5zztools_errorz00(obj_t
		BgL_envz00_1797)
	{
		{	/* Tools/error.scm 75 */
			{	/* Tools/error.scm 76 */
				obj_t BgL_objz00_1798;
				obj_t BgL_mesz00_1799;
				obj_t BgL_procz00_1800;
				obj_t BgL_locz00_1801;

				BgL_objz00_1798 = PROCEDURE_L_REF(BgL_envz00_1797, (int) (0L));
				BgL_mesz00_1799 = PROCEDURE_L_REF(BgL_envz00_1797, (int) (1L));
				BgL_procz00_1800 = PROCEDURE_L_REF(BgL_envz00_1797, (int) (2L));
				BgL_locz00_1801 = PROCEDURE_L_REF(BgL_envz00_1797, (int) (3L));
				{	/* Tools/error.scm 76 */
					bool_t BgL_test1690z00_2029;

					if (STRUCTP(BgL_locz00_1801))
						{	/* Tools/error.scm 76 */
							BgL_test1690z00_2029 =
								(STRUCT_KEY(BgL_locz00_1801) == CNST_TABLE_REF(1));
						}
					else
						{	/* Tools/error.scm 76 */
							BgL_test1690z00_2029 = ((bool_t) 0);
						}
					if (BgL_test1690z00_2029)
						{	/* Tools/error.scm 78 */
							obj_t BgL_arg1308z00_1865;
							obj_t BgL_arg1310z00_1866;

							BgL_arg1308z00_1865 =
								BGl_locationzd2fullzd2fnamez00zztools_locationz00
								(BgL_locz00_1801);
							BgL_arg1310z00_1866 = STRUCT_REF(BgL_locz00_1801, (int) (1L));
							{	/* Tools/error.scm 78 */
								obj_t BgL_list1311z00_1867;

								{	/* Tools/error.scm 78 */
									obj_t BgL_arg1312z00_1868;

									{	/* Tools/error.scm 78 */
										obj_t BgL_arg1314z00_1869;

										{	/* Tools/error.scm 78 */
											obj_t BgL_arg1315z00_1870;

											BgL_arg1315z00_1870 =
												MAKE_YOUNG_PAIR(BgL_objz00_1798, BNIL);
											BgL_arg1314z00_1869 =
												MAKE_YOUNG_PAIR(BGl_string1656z00zztools_errorz00,
												BgL_arg1315z00_1870);
										}
										BgL_arg1312z00_1868 =
											MAKE_YOUNG_PAIR(BgL_mesz00_1799, BgL_arg1314z00_1869);
									}
									BgL_list1311z00_1867 =
										MAKE_YOUNG_PAIR(BgL_procz00_1800, BgL_arg1312z00_1868);
								}
								return
									BGl_warningzf2locationzf2zz__errorz00(BgL_arg1308z00_1865,
									BgL_arg1310z00_1866, BgL_list1311z00_1867);
							}
						}
					else
						{	/* Tools/error.scm 77 */
							obj_t BgL_list1316z00_1871;

							{	/* Tools/error.scm 77 */
								obj_t BgL_arg1317z00_1872;

								{	/* Tools/error.scm 77 */
									obj_t BgL_arg1318z00_1873;

									{	/* Tools/error.scm 77 */
										obj_t BgL_arg1319z00_1874;

										BgL_arg1319z00_1874 =
											MAKE_YOUNG_PAIR(BgL_objz00_1798, BNIL);
										BgL_arg1318z00_1873 =
											MAKE_YOUNG_PAIR(BGl_string1656z00zztools_errorz00,
											BgL_arg1319z00_1874);
									}
									BgL_arg1317z00_1872 =
										MAKE_YOUNG_PAIR(BgL_mesz00_1799, BgL_arg1318z00_1873);
								}
								BgL_list1316z00_1871 =
									MAKE_YOUNG_PAIR(BgL_procz00_1800, BgL_arg1317z00_1872);
							}
							return BGl_warningz00zz__errorz00(BgL_list1316z00_1871);
						}
				}
			}
		}

	}



/* user-warning */
	BGL_EXPORTED_DEF obj_t BGl_userzd2warningzd2zztools_errorz00(obj_t
		BgL_procz00_25, obj_t BgL_mesz00_26, obj_t BgL_objz00_27)
	{
		{	/* Tools/error.scm 85 */
			return
				BGl_userzd2warningzf2locationz20zztools_errorz00
				(BGl_findzd2locationzd2zztools_locationz00(BgL_objz00_27),
				BgL_procz00_25, BgL_mesz00_26, BgL_objz00_27);
		}

	}



/* &user-warning */
	obj_t BGl_z62userzd2warningzb0zztools_errorz00(obj_t BgL_envz00_1802,
		obj_t BgL_procz00_1803, obj_t BgL_mesz00_1804, obj_t BgL_objz00_1805)
	{
		{	/* Tools/error.scm 85 */
			return
				BGl_userzd2warningzd2zztools_errorz00(BgL_procz00_1803, BgL_mesz00_1804,
				BgL_objz00_1805);
		}

	}



/* no-warning */
	BGL_EXPORTED_DEF obj_t BGl_nozd2warningzd2zztools_errorz00(obj_t
		BgL_thunkz00_28)
	{
		{	/* Tools/error.scm 91 */
			{	/* Tools/error.scm 92 */
				int BgL_warningz00_1430;

				BgL_warningz00_1430 = BGl_bigloozd2warningzd2zz__paramz00();
				BGl_bigloozd2warningzd2setz12z12zz__paramz00((int) (0L));
				{	/* Tools/error.scm 94 */
					obj_t BgL_valz00_1431;

					BgL_valz00_1431 = BGL_PROCEDURE_CALL0(BgL_thunkz00_28);
					BGl_bigloozd2warningzd2setz12z12zz__paramz00(BgL_warningz00_1430);
					return BgL_valz00_1431;
				}
			}
		}

	}



/* &no-warning */
	obj_t BGl_z62nozd2warningzb0zztools_errorz00(obj_t BgL_envz00_1806,
		obj_t BgL_thunkz00_1807)
	{
		{	/* Tools/error.scm 91 */
			return BGl_nozd2warningzd2zztools_errorz00(BgL_thunkz00_1807);
		}

	}



/* user-error-notify */
	BGL_EXPORTED_DEF obj_t BGl_userzd2errorzd2notifyz00zztools_errorz00(obj_t
		BgL_ez00_29, obj_t BgL_procz00_30)
	{
		{	/* Tools/error.scm 101 */
			{	/* Tools/error.scm 102 */
				bool_t BgL_test1692z00_2059;

				{	/* Tools/error.scm 102 */
					obj_t BgL_classz00_1703;

					BgL_classz00_1703 = BGl_z62errorz62zz__objectz00;
					if (BGL_OBJECTP(BgL_ez00_29))
						{	/* Tools/error.scm 102 */
							BgL_objectz00_bglt BgL_arg1807z00_1705;

							BgL_arg1807z00_1705 = (BgL_objectz00_bglt) (BgL_ez00_29);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tools/error.scm 102 */
									long BgL_idxz00_1711;

									BgL_idxz00_1711 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1705);
									BgL_test1692z00_2059 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_1711 + 3L)) == BgL_classz00_1703);
								}
							else
								{	/* Tools/error.scm 102 */
									bool_t BgL_res1646z00_1736;

									{	/* Tools/error.scm 102 */
										obj_t BgL_oclassz00_1719;

										{	/* Tools/error.scm 102 */
											obj_t BgL_arg1815z00_1727;
											long BgL_arg1816z00_1728;

											BgL_arg1815z00_1727 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tools/error.scm 102 */
												long BgL_arg1817z00_1729;

												BgL_arg1817z00_1729 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1705);
												BgL_arg1816z00_1728 =
													(BgL_arg1817z00_1729 - OBJECT_TYPE);
											}
											BgL_oclassz00_1719 =
												VECTOR_REF(BgL_arg1815z00_1727, BgL_arg1816z00_1728);
										}
										{	/* Tools/error.scm 102 */
											bool_t BgL__ortest_1115z00_1720;

											BgL__ortest_1115z00_1720 =
												(BgL_classz00_1703 == BgL_oclassz00_1719);
											if (BgL__ortest_1115z00_1720)
												{	/* Tools/error.scm 102 */
													BgL_res1646z00_1736 = BgL__ortest_1115z00_1720;
												}
											else
												{	/* Tools/error.scm 102 */
													long BgL_odepthz00_1721;

													{	/* Tools/error.scm 102 */
														obj_t BgL_arg1804z00_1722;

														BgL_arg1804z00_1722 = (BgL_oclassz00_1719);
														BgL_odepthz00_1721 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_1722);
													}
													if ((3L < BgL_odepthz00_1721))
														{	/* Tools/error.scm 102 */
															obj_t BgL_arg1802z00_1724;

															{	/* Tools/error.scm 102 */
																obj_t BgL_arg1803z00_1725;

																BgL_arg1803z00_1725 = (BgL_oclassz00_1719);
																BgL_arg1802z00_1724 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_1725,
																	3L);
															}
															BgL_res1646z00_1736 =
																(BgL_arg1802z00_1724 == BgL_classz00_1703);
														}
													else
														{	/* Tools/error.scm 102 */
															BgL_res1646z00_1736 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1692z00_2059 = BgL_res1646z00_1736;
								}
						}
					else
						{	/* Tools/error.scm 102 */
							BgL_test1692z00_2059 = ((bool_t) 0);
						}
				}
				if (BgL_test1692z00_2059)
					{	/* Tools/error.scm 105 */
						obj_t BgL_zc3z04anonymousza31324ze3z87_1808;

						{
							int BgL_tmpz00_2082;

							BgL_tmpz00_2082 = (int) (2L);
							BgL_zc3z04anonymousza31324ze3z87_1808 =
								MAKE_L_PROCEDURE
								(BGl_z62zc3z04anonymousza31324ze3ze5zztools_errorz00,
								BgL_tmpz00_2082);
						}
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31324ze3z87_1808,
							(int) (0L), BgL_ez00_29);
						PROCEDURE_L_SET(BgL_zc3z04anonymousza31324ze3z87_1808,
							(int) (1L), BgL_procz00_30);
						return
							BGl_withzd2dumpzd2stackz00zztools_errorz00
							(BgL_zc3z04anonymousza31324ze3z87_1808);
					}
				else
					{	/* Tools/error.scm 102 */
						return BFALSE;
					}
			}
		}

	}



/* &user-error-notify */
	obj_t BGl_z62userzd2errorzd2notifyz62zztools_errorz00(obj_t BgL_envz00_1809,
		obj_t BgL_ez00_1810, obj_t BgL_procz00_1811)
	{
		{	/* Tools/error.scm 101 */
			return
				BGl_userzd2errorzd2notifyz00zztools_errorz00(BgL_ez00_1810,
				BgL_procz00_1811);
		}

	}



/* &<@anonymous:1324> */
	obj_t BGl_z62zc3z04anonymousza31324ze3ze5zztools_errorz00(obj_t
		BgL_envz00_1812)
	{
		{	/* Tools/error.scm 104 */
			{	/* Tools/error.scm 105 */
				obj_t BgL_ez00_1813;
				obj_t BgL_procz00_1814;

				BgL_ez00_1813 = PROCEDURE_L_REF(BgL_envz00_1812, (int) (0L));
				BgL_procz00_1814 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_1812, (int) (1L)));
				{	/* Tools/error.scm 106 */
					obj_t BgL_locz00_1875;

					{	/* Tools/error.scm 106 */
						obj_t BgL_arg1329z00_1876;

						BgL_arg1329z00_1876 =
							(((BgL_z62errorz62_bglt) COBJECT(
									((BgL_z62errorz62_bglt) BgL_ez00_1813)))->BgL_objz00);
						BgL_locz00_1875 =
							BGl_findzd2locationzd2zztools_locationz00(BgL_arg1329z00_1876);
					}
					BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 =
						ADDFX(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
						BINT(1L));
					{	/* Tools/error.scm 108 */
						bool_t BgL_test1698z00_2101;

						if (STRUCTP(BgL_locz00_1875))
							{	/* Tools/error.scm 108 */
								BgL_test1698z00_2101 =
									(STRUCT_KEY(BgL_locz00_1875) == CNST_TABLE_REF(1));
							}
						else
							{	/* Tools/error.scm 108 */
								BgL_test1698z00_2101 = ((bool_t) 0);
							}
						if (BgL_test1698z00_2101)
							{	/* Tools/error.scm 109 */
								BgL_z62errorz62_bglt BgL_arg1326z00_1877;

								{	/* Tools/error.scm 109 */
									BgL_z62errorz62_bglt BgL_new1107z00_1878;

									{	/* Tools/error.scm 111 */
										BgL_z62errorz62_bglt BgL_new1113z00_1879;

										BgL_new1113z00_1879 =
											((BgL_z62errorz62_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_z62errorz62_bgl))));
										{	/* Tools/error.scm 111 */
											long BgL_arg1327z00_1880;

											BgL_arg1327z00_1880 =
												BGL_CLASS_NUM(BGl_z62errorz62zz__objectz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1113z00_1879),
												BgL_arg1327z00_1880);
										}
										BgL_new1107z00_1878 = BgL_new1113z00_1879;
									}
									((((BgL_z62exceptionz62_bglt) COBJECT(
													((BgL_z62exceptionz62_bglt) BgL_new1107z00_1878)))->
											BgL_fnamez00) =
										((obj_t)
											BGl_locationzd2fullzd2fnamez00zztools_locationz00
											(BgL_locz00_1875)), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1107z00_1878)))->BgL_locationz00) =
										((obj_t) STRUCT_REF(BgL_locz00_1875, (int) (1L))), BUNSPEC);
									((((BgL_z62exceptionz62_bglt)
												COBJECT(((BgL_z62exceptionz62_bglt)
														BgL_new1107z00_1878)))->BgL_stackz00) =
										((obj_t) (((BgL_z62exceptionz62_bglt)
													COBJECT(((BgL_z62exceptionz62_bglt) BgL_ez00_1813)))->
												BgL_stackz00)), BUNSPEC);
									{
										obj_t BgL_auxz00_2122;

										{	/* Tools/error.scm 110 */
											obj_t BgL__ortest_1114z00_1881;

											BgL__ortest_1114z00_1881 =
												(((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_ez00_1813)))->
												BgL_procz00);
											if (CBOOL(BgL__ortest_1114z00_1881))
												{	/* Tools/error.scm 110 */
													BgL_auxz00_2122 = BgL__ortest_1114z00_1881;
												}
											else
												{	/* Tools/error.scm 110 */
													BgL_auxz00_2122 = BgL_procz00_1814;
												}
										}
										((((BgL_z62errorz62_bglt) COBJECT(BgL_new1107z00_1878))->
												BgL_procz00) = ((obj_t) BgL_auxz00_2122), BUNSPEC);
									}
									((((BgL_z62errorz62_bglt) COBJECT(BgL_new1107z00_1878))->
											BgL_msgz00) =
										((obj_t) (((BgL_z62errorz62_bglt)
													COBJECT(((BgL_z62errorz62_bglt) (
																(BgL_z62exceptionz62_bglt) BgL_ez00_1813))))->
												BgL_msgz00)), BUNSPEC);
									((((BgL_z62errorz62_bglt) COBJECT(BgL_new1107z00_1878))->
											BgL_objz00) =
										((obj_t) (((BgL_z62errorz62_bglt)
													COBJECT(((BgL_z62errorz62_bglt) (
																(BgL_z62exceptionz62_bglt) BgL_ez00_1813))))->
												BgL_objz00)), BUNSPEC);
									BgL_arg1326z00_1877 = BgL_new1107z00_1878;
								}
								return
									BGl_errorzd2notifyzd2zz__errorz00(
									((obj_t) BgL_arg1326z00_1877));
							}
						else
							{	/* Tools/error.scm 108 */
								if (CBOOL(
										(((BgL_z62errorz62_bglt) COBJECT(
													((BgL_z62errorz62_bglt) BgL_ez00_1813)))->
											BgL_procz00)))
									{	/* Tools/error.scm 114 */
										BFALSE;
									}
								else
									{	/* Tools/error.scm 114 */
										((((BgL_z62errorz62_bglt) COBJECT(
														((BgL_z62errorz62_bglt) BgL_ez00_1813)))->
												BgL_procz00) = ((obj_t) BgL_procz00_1814), BUNSPEC);
									}
								return BGl_errorzd2notifyzd2zz__errorz00(BgL_ez00_1813);
							}
					}
				}
			}
		}

	}



/* user-error */
	BGL_EXPORTED_DEF obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t
		BgL_procz00_31, obj_t BgL_mesz00_32, obj_t BgL_objz00_33,
		obj_t BgL_continuez00_34)
	{
		{	/* Tools/error.scm 120 */
			if (PAIRP(BgL_continuez00_34))
				{	/* Tools/error.scm 122 */
					obj_t BgL_arg1331z00_1450;
					obj_t BgL_arg1332z00_1451;
					obj_t BgL_arg1333z00_1452;

					BgL_arg1331z00_1450 =
						BGl_findzd2locationzd2zztools_locationz00(BgL_objz00_33);
					BgL_arg1332z00_1451 = BGl_shapez00zztools_shapez00(BgL_objz00_33);
					BgL_arg1333z00_1452 = CAR(BgL_continuez00_34);
					{	/* Tools/error.scm 122 */
						obj_t BgL_list1334z00_1453;

						BgL_list1334z00_1453 = MAKE_YOUNG_PAIR(BgL_arg1333z00_1452, BNIL);
						return
							BGl_userzd2errorzf2locationz20zztools_errorz00
							(BgL_arg1331z00_1450, BgL_procz00_31, BgL_mesz00_32,
							BgL_arg1332z00_1451, BgL_list1334z00_1453);
					}
				}
			else
				{	/* Tools/error.scm 121 */
					return
						BGl_userzd2errorzf2locationz20zztools_errorz00
						(BGl_findzd2locationzd2zztools_locationz00(BgL_objz00_33),
						BgL_procz00_31, BgL_mesz00_32,
						BGl_shapez00zztools_shapez00(BgL_objz00_33), BNIL);
				}
		}

	}



/* &user-error */
	obj_t BGl_z62userzd2errorzb0zztools_errorz00(obj_t BgL_envz00_1815,
		obj_t BgL_procz00_1816, obj_t BgL_mesz00_1817, obj_t BgL_objz00_1818,
		obj_t BgL_continuez00_1819)
	{
		{	/* Tools/error.scm 120 */
			return
				BGl_userzd2errorzd2zztools_errorz00(BgL_procz00_1816, BgL_mesz00_1817,
				BgL_objz00_1818, BgL_continuez00_1819);
		}

	}



/* user-error/location */
	BGL_EXPORTED_DEF obj_t BGl_userzd2errorzf2locationz20zztools_errorz00(obj_t
		BgL_locz00_35, obj_t BgL_procz00_36, obj_t BgL_msgz00_37,
		obj_t BgL_objz00_38, obj_t BgL_continuez00_39)
	{
		{	/* Tools/error.scm 129 */
			{	/* Tools/error.scm 132 */
				obj_t BgL_zc3z04anonymousza31343ze3z87_1822;

				{
					int BgL_tmpz00_2156;

					BgL_tmpz00_2156 = (int) (5L);
					BgL_zc3z04anonymousza31343ze3z87_1822 =
						MAKE_L_PROCEDURE
						(BGl_z62zc3z04anonymousza31343ze3ze5zztools_errorz00,
						BgL_tmpz00_2156);
				}
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31343ze3z87_1822,
					(int) (0L), BgL_procz00_36);
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31343ze3z87_1822,
					(int) (1L), BgL_msgz00_37);
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31343ze3z87_1822,
					(int) (2L), BgL_objz00_38);
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31343ze3z87_1822,
					(int) (3L), BgL_locz00_35);
				PROCEDURE_L_SET(BgL_zc3z04anonymousza31343ze3z87_1822,
					(int) (4L), BgL_continuez00_39);
				return
					BGl_withzd2dumpzd2stackz00zztools_errorz00
					(BgL_zc3z04anonymousza31343ze3z87_1822);
			}
		}

	}



/* &user-error/location */
	obj_t BGl_z62userzd2errorzf2locationz42zztools_errorz00(obj_t BgL_envz00_1823,
		obj_t BgL_locz00_1824, obj_t BgL_procz00_1825, obj_t BgL_msgz00_1826,
		obj_t BgL_objz00_1827, obj_t BgL_continuez00_1828)
	{
		{	/* Tools/error.scm 129 */
			return
				BGl_userzd2errorzf2locationz20zztools_errorz00(BgL_locz00_1824,
				BgL_procz00_1825, BgL_msgz00_1826, BgL_objz00_1827,
				BgL_continuez00_1828);
		}

	}



/* &<@anonymous:1343> */
	obj_t BGl_z62zc3z04anonymousza31343ze3ze5zztools_errorz00(obj_t
		BgL_envz00_1829)
	{
		{	/* Tools/error.scm 131 */
			{	/* Tools/error.scm 132 */
				obj_t BgL_procz00_1830;
				obj_t BgL_msgz00_1831;
				obj_t BgL_objz00_1832;
				obj_t BgL_locz00_1833;
				obj_t BgL_continuez00_1834;

				BgL_procz00_1830 = PROCEDURE_L_REF(BgL_envz00_1829, (int) (0L));
				BgL_msgz00_1831 = PROCEDURE_L_REF(BgL_envz00_1829, (int) (1L));
				BgL_objz00_1832 = PROCEDURE_L_REF(BgL_envz00_1829, (int) (2L));
				BgL_locz00_1833 = PROCEDURE_L_REF(BgL_envz00_1829, (int) (3L));
				BgL_continuez00_1834 = PROCEDURE_L_REF(BgL_envz00_1829, (int) (4L));
				if (OUTPUT_PORTP(BGl_za2tracezd2portza2zd2zztools_tracez00))
					{	/* Tools/error.scm 133 */
						obj_t BgL_port1240z00_1882;

						BgL_port1240z00_1882 = BGl_za2tracezd2portza2zd2zztools_tracez00;
						bgl_display_string(BGl_string1657z00zztools_errorz00,
							BgL_port1240z00_1882);
						bgl_display_obj(BgL_procz00_1830, BgL_port1240z00_1882);
						bgl_display_string(BGl_string1651z00zztools_errorz00,
							BgL_port1240z00_1882);
						bgl_display_obj(BgL_msgz00_1831, BgL_port1240z00_1882);
						bgl_display_string(BGl_string1651z00zztools_errorz00,
							BgL_port1240z00_1882);
						bgl_display_obj(BgL_objz00_1832, BgL_port1240z00_1882);
						bgl_display_char(((unsigned char) 10), BgL_port1240z00_1882);
					}
				else
					{	/* Tools/error.scm 132 */
						BFALSE;
					}
				BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 =
					ADDFX(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00, BINT(1L));
				{	/* Tools/error.scm 135 */
					obj_t BgL_proczd2stringzd2_1883;

					if (STRINGP(BgL_procz00_1830))
						{	/* Tools/error.scm 136 */
							BgL_proczd2stringzd2_1883 = BgL_procz00_1830;
						}
					else
						{	/* Tools/error.scm 136 */
							if (SYMBOLP(BgL_procz00_1830))
								{	/* Tools/error.scm 139 */
									obj_t BgL_arg1455z00_1884;

									BgL_arg1455z00_1884 = SYMBOL_TO_STRING(BgL_procz00_1830);
									BgL_proczd2stringzd2_1883 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_1884);
								}
							else
								{	/* Tools/error.scm 138 */
									BgL_proczd2stringzd2_1883 = BFALSE;
								}
						}
					{	/* Tools/error.scm 135 */
						obj_t BgL_funzd2stringzd2_1885;

						{	/* Tools/error.scm 142 */
							obj_t BgL_arg1375z00_1886;

							BgL_arg1375z00_1886 =
								CAR(BGl_za2sfunzd2stackza2zd2zztools_errorz00);
							{	/* Tools/error.scm 142 */
								obj_t BgL_arg1455z00_1887;

								BgL_arg1455z00_1887 =
									SYMBOL_TO_STRING(((obj_t) BgL_arg1375z00_1886));
								BgL_funzd2stringzd2_1885 =
									BGl_stringzd2copyzd2zz__r4_strings_6_7z00
									(BgL_arg1455z00_1887);
							}
						}
						{	/* Tools/error.scm 142 */
							obj_t BgL_procz00_1888;

							{	/* Tools/error.scm 143 */
								bool_t BgL_test1706z00_2202;

								if (STRINGP(BgL_proczd2stringzd2_1883))
									{	/* Tools/error.scm 144 */
										bool_t BgL_test1711z00_2205;

										{	/* Tools/error.scm 144 */
											long BgL_l1z00_1889;

											BgL_l1z00_1889 = STRING_LENGTH(BgL_proczd2stringzd2_1883);
											if (
												(BgL_l1z00_1889 ==
													STRING_LENGTH(BgL_funzd2stringzd2_1885)))
												{	/* Tools/error.scm 144 */
													int BgL_arg1282z00_1890;

													{	/* Tools/error.scm 144 */
														char *BgL_auxz00_2212;
														char *BgL_tmpz00_2210;

														BgL_auxz00_2212 =
															BSTRING_TO_STRING(BgL_funzd2stringzd2_1885);
														BgL_tmpz00_2210 =
															BSTRING_TO_STRING(BgL_proczd2stringzd2_1883);
														BgL_arg1282z00_1890 =
															memcmp(BgL_tmpz00_2210, BgL_auxz00_2212,
															BgL_l1z00_1889);
													}
													BgL_test1711z00_2205 =
														((long) (BgL_arg1282z00_1890) == 0L);
												}
											else
												{	/* Tools/error.scm 144 */
													BgL_test1711z00_2205 = ((bool_t) 0);
												}
										}
										if (BgL_test1711z00_2205)
											{	/* Tools/error.scm 144 */
												BgL_test1706z00_2202 = ((bool_t) 0);
											}
										else
											{	/* Tools/error.scm 144 */
												BgL_test1706z00_2202 = ((bool_t) 1);
											}
									}
								else
									{	/* Tools/error.scm 143 */
										BgL_test1706z00_2202 = ((bool_t) 0);
									}
								if (BgL_test1706z00_2202)
									{	/* Tools/error.scm 143 */
										BgL_procz00_1888 =
											string_append_3(BgL_funzd2stringzd2_1885,
											BGl_string1651z00zztools_errorz00,
											BgL_proczd2stringzd2_1883);
									}
								else
									{	/* Tools/error.scm 143 */
										BgL_procz00_1888 = BgL_funzd2stringzd2_1885;
									}
							}
							{	/* Tools/error.scm 143 */
								obj_t BgL_objprnz00_1891;

								{	/* Tools/error.scm 147 */
									obj_t BgL_portz00_1892;

									{	/* Tools/error.scm 147 */

										{	/* Tools/error.scm 147 */

											BgL_portz00_1892 =
												BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00
												(BTRUE);
										}
									}
									BGl_displayzd2circlezd2zz__pp_circlez00(BgL_objz00_1832,
										BgL_portz00_1892);
									{	/* Tools/error.scm 149 */
										obj_t BgL_stringz00_1893;

										BgL_stringz00_1893 =
											bgl_close_output_port(BgL_portz00_1892);
										if ((STRING_LENGTH(((obj_t) BgL_stringz00_1893)) > 45L))
											{	/* Tools/error.scm 151 */
												obj_t BgL_arg1364z00_1894;

												BgL_arg1364z00_1894 =
													c_substring(((obj_t) BgL_stringz00_1893), 0L, 44L);
												BgL_objprnz00_1891 =
													string_append(BgL_arg1364z00_1894,
													BGl_string1658z00zztools_errorz00);
											}
										else
											{	/* Tools/error.scm 150 */
												BgL_objprnz00_1891 = BgL_stringz00_1893;
											}
									}
								}
								{	/* Tools/error.scm 147 */

									return
										BGl_zc3z04exitza31345ze3ze70z60zztools_errorz00
										(BgL_continuez00_1834, BgL_locz00_1833, BgL_objprnz00_1891,
										BgL_msgz00_1831, BgL_procz00_1888);
								}
							}
						}
					}
				}
			}
		}

	}



/* <@exit:1345>~0 */
	obj_t BGl_zc3z04exitza31345ze3ze70z60zztools_errorz00(obj_t
		BgL_continuez00_1852, obj_t BgL_locz00_1851, obj_t BgL_objprnz00_1850,
		obj_t BgL_msgz00_1849, obj_t BgL_procz00_1848)
	{
		{	/* Tools/error.scm 153 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1115z00_1466;

			if (SET_EXIT(BgL_an_exit1115z00_1466))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1115z00_1466 = (void *) jmpbuf;
					{	/* Tools/error.scm 153 */
						obj_t BgL_env1119z00_1467;

						BgL_env1119z00_1467 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1119z00_1467, BgL_an_exit1115z00_1466, 1L);
						{	/* Tools/error.scm 153 */
							obj_t BgL_an_exitd1116z00_1468;

							BgL_an_exitd1116z00_1468 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1119z00_1467);
							{	/* Tools/error.scm 153 */
								obj_t BgL_res1118z00_1471;

								{	/* Tools/error.scm 156 */
									obj_t BgL_zc3z04anonymousza31353ze3z87_1821;
									obj_t BgL_zc3z04anonymousza31349ze3z87_1820;

									BgL_zc3z04anonymousza31353ze3z87_1821 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31353ze3ze5zztools_errorz00,
										(int) (0L), (int) (4L));
									BgL_zc3z04anonymousza31349ze3z87_1820 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza31349ze3ze5zztools_errorz00,
										(int) (1L), (int) (2L));
									PROCEDURE_SET(BgL_zc3z04anonymousza31353ze3z87_1821,
										(int) (0L), BgL_procz00_1848);
									PROCEDURE_SET(BgL_zc3z04anonymousza31353ze3z87_1821,
										(int) (1L), BgL_msgz00_1849);
									PROCEDURE_SET(BgL_zc3z04anonymousza31353ze3z87_1821,
										(int) (2L), BgL_objprnz00_1850);
									PROCEDURE_SET(BgL_zc3z04anonymousza31353ze3z87_1821,
										(int) (3L), BgL_locz00_1851);
									PROCEDURE_SET(BgL_zc3z04anonymousza31349ze3z87_1820,
										(int) (0L), BgL_continuez00_1852);
									PROCEDURE_SET(BgL_zc3z04anonymousza31349ze3z87_1820,
										(int) (1L), BgL_an_exitd1116z00_1468);
									BgL_res1118z00_1471 =
										BGl_withzd2exceptionzd2handlerz00zz__errorz00
										(BgL_zc3z04anonymousza31349ze3z87_1820,
										BgL_zc3z04anonymousza31353ze3z87_1821);
								}
								POP_ENV_EXIT(BgL_env1119z00_1467);
								return BgL_res1118z00_1471;
							}
						}
					}
				}
		}

	}



/* &<@anonymous:1353> */
	obj_t BGl_z62zc3z04anonymousza31353ze3ze5zztools_errorz00(obj_t
		BgL_envz00_1835)
	{
		{	/* Tools/error.scm 160 */
			{	/* Tools/error.scm 161 */
				obj_t BgL_procz00_1836;
				obj_t BgL_msgz00_1837;
				obj_t BgL_objprnz00_1838;
				obj_t BgL_locz00_1839;

				BgL_procz00_1836 = ((obj_t) PROCEDURE_REF(BgL_envz00_1835, (int) (0L)));
				BgL_msgz00_1837 = PROCEDURE_REF(BgL_envz00_1835, (int) (1L));
				BgL_objprnz00_1838 = PROCEDURE_REF(BgL_envz00_1835, (int) (2L));
				BgL_locz00_1839 = PROCEDURE_REF(BgL_envz00_1835, (int) (3L));
				{	/* Tools/error.scm 161 */
					bool_t BgL_test1715z00_2263;

					if (STRUCTP(BgL_locz00_1839))
						{	/* Tools/error.scm 161 */
							BgL_test1715z00_2263 =
								(STRUCT_KEY(BgL_locz00_1839) == CNST_TABLE_REF(1));
						}
					else
						{	/* Tools/error.scm 161 */
							BgL_test1715z00_2263 = ((bool_t) 0);
						}
					if (BgL_test1715z00_2263)
						{	/* Tools/error.scm 161 */
							return
								BGl_errorzf2locationzf2zz__errorz00(BgL_procz00_1836,
								BgL_msgz00_1837, BgL_objprnz00_1838,
								BGl_locationzd2fullzd2fnamez00zztools_locationz00
								(BgL_locz00_1839), STRUCT_REF(BgL_locz00_1839, (int) (1L)));
						}
					else
						{	/* Tools/error.scm 161 */
							return
								BGl_errorz00zz__errorz00(BgL_procz00_1836, BgL_msgz00_1837,
								BgL_objprnz00_1838);
						}
				}
			}
		}

	}



/* &<@anonymous:1349> */
	obj_t BGl_z62zc3z04anonymousza31349ze3ze5zztools_errorz00(obj_t
		BgL_envz00_1840, obj_t BgL_ez00_1843)
	{
		{	/* Tools/error.scm 155 */
			{	/* Tools/error.scm 156 */
				obj_t BgL_continuez00_1841;
				obj_t BgL_an_exitd1116z00_1842;

				BgL_continuez00_1841 = PROCEDURE_REF(BgL_envz00_1840, (int) (0L));
				BgL_an_exitd1116z00_1842 = PROCEDURE_REF(BgL_envz00_1840, (int) (1L));
				BGl_errorzd2notifyzd2zz__errorz00(BgL_ez00_1843);
				if (PAIRP(BgL_continuez00_1841))
					{	/* Tools/error.scm 157 */
						return
							unwind_stack_until(BgL_an_exitd1116z00_1842, BFALSE,
							CAR(BgL_continuez00_1841), BFALSE, BFALSE);
					}
				else
					{	/* Tools/error.scm 159 */
						obj_t BgL_list1352z00_1895;

						BgL_list1352z00_1895 = MAKE_YOUNG_PAIR(BINT(1L), BNIL);
						return BGl_exitz00zz__errorz00(BgL_list1352z00_1895);
					}
			}
		}

	}



/* enter-function */
	BGL_EXPORTED_DEF obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t
		BgL_varz00_40)
	{
		{	/* Tools/error.scm 175 */
			return (BGl_za2sfunzd2stackza2zd2zztools_errorz00 =
				MAKE_YOUNG_PAIR(BgL_varz00_40,
					BGl_za2sfunzd2stackza2zd2zztools_errorz00), BUNSPEC);
		}

	}



/* &enter-function */
	obj_t BGl_z62enterzd2functionzb0zztools_errorz00(obj_t BgL_envz00_1844,
		obj_t BgL_varz00_1845)
	{
		{	/* Tools/error.scm 175 */
			return BGl_enterzd2functionzd2zztools_errorz00(BgL_varz00_1845);
		}

	}



/* leave-function */
	BGL_EXPORTED_DEF obj_t BGl_leavezd2functionzd2zztools_errorz00(void)
	{
		{	/* Tools/error.scm 181 */
			return (BGl_za2sfunzd2stackza2zd2zztools_errorz00 =
				CDR(BGl_za2sfunzd2stackza2zd2zztools_errorz00), BUNSPEC);
		}

	}



/* &leave-function */
	obj_t BGl_z62leavezd2functionzb0zztools_errorz00(obj_t BgL_envz00_1846)
	{
		{	/* Tools/error.scm 181 */
			return BGl_leavezd2functionzd2zztools_errorz00();
		}

	}



/* current-function */
	BGL_EXPORTED_DEF obj_t BGl_currentzd2functionzd2zztools_errorz00(void)
	{
		{	/* Tools/error.scm 187 */
			return CAR(BGl_za2sfunzd2stackza2zd2zztools_errorz00);
		}

	}



/* &current-function */
	obj_t BGl_z62currentzd2functionzb0zztools_errorz00(obj_t BgL_envz00_1847)
	{
		{	/* Tools/error.scm 187 */
			return BGl_currentzd2functionzd2zztools_errorz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztools_errorz00(void)
	{
		{	/* Tools/error.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztools_errorz00(void)
	{
		{	/* Tools/error.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztools_errorz00(void)
	{
		{	/* Tools/error.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztools_errorz00(void)
	{
		{	/* Tools/error.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
			return
				BGl_modulezd2initializa7ationz75zzinit_mainz00(288050968L,
				BSTRING_TO_STRING(BGl_string1659z00zztools_errorz00));
		}

	}

#ifdef __cplusplus
}
#endif
