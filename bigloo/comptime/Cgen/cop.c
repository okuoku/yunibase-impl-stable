/*===========================================================================*/
/*   (Cgen/cop.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cgen/cop.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CGEN_COP_TYPE_DEFINITIONS
#define BGL_CGEN_COP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_copz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}             *BgL_copz00_bglt;

	typedef struct BgL_clabelz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_namez00;
		bool_t BgL_usedzf3zf3;
		obj_t BgL_bodyz00;
	}                *BgL_clabelz00_bglt;

	typedef struct BgL_cgotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_clabelz00_bgl *BgL_labelz00;
	}               *BgL_cgotoz00_bglt;

	typedef struct BgL_cblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                *BgL_cblockz00_bglt;

	typedef struct BgL_creturnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_tailz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                 *BgL_creturnz00_bglt;

	typedef struct BgL_cvoidz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}               *BgL_cvoidz00_bglt;

	typedef struct BgL_catomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_catomz00_bglt;

	typedef struct BgL_varcz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}              *BgL_varcz00_bglt;

	typedef struct BgL_cpragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_formatz00;
		obj_t BgL_argsz00;
	}                 *BgL_cpragmaz00_bglt;

	typedef struct BgL_ccastz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_argz00;
	}               *BgL_ccastz00_bglt;

	typedef struct BgL_csequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_czd2expzf3z21;
		obj_t BgL_copsz00;
	}                   *BgL_csequencez00_bglt;

	typedef struct BgL_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}             *BgL_nopz00_bglt;

	typedef struct BgL_stopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}              *BgL_stopz00_bglt;

	typedef struct BgL_csetqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varcz00_bgl *BgL_varz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}               *BgL_csetqz00_bglt;

	typedef struct BgL_cifz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_testz00;
		struct BgL_copz00_bgl *BgL_truez00;
		struct BgL_copz00_bgl *BgL_falsez00;
	}             *BgL_cifz00_bglt;

	typedef struct BgL_localzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varsz00;
	}                     *BgL_localzd2varzd2_bglt;

	typedef struct BgL_cfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
	}                  *BgL_cfuncallz00_bglt;

	typedef struct BgL_capplyz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		struct BgL_copz00_bgl *BgL_argz00;
	}                *BgL_capplyz00_bglt;

	typedef struct BgL_cappz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}              *BgL_cappz00_bglt;

	typedef struct BgL_cfailz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_procz00;
		struct BgL_copz00_bgl *BgL_msgz00;
		struct BgL_copz00_bgl *BgL_objz00;
	}               *BgL_cfailz00_bglt;

	typedef struct BgL_cswitchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
	}                 *BgL_cswitchz00_bglt;

	typedef struct BgL_cmakezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
		obj_t BgL_stackablez00;
	}                     *BgL_cmakezd2boxzd2_bglt;

	typedef struct BgL_cboxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_varz00;
	}                    *BgL_cboxzd2refzd2_bglt;

	typedef struct BgL_cboxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_varz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                       *BgL_cboxzd2setz12zc0_bglt;

	typedef struct BgL_csetzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_exitz00;
		struct BgL_copz00_bgl *BgL_jumpzd2valuezd2;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                        *BgL_csetzd2exzd2itz00_bglt;

	typedef struct BgL_cjumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_exitz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                         *BgL_cjumpzd2exzd2itz00_bglt;

	typedef struct BgL_sfunzf2czf2_bgl
	{
		struct BgL_clabelz00_bgl *BgL_labelz00;
		bool_t BgL_integratedz00;
	}                  *BgL_sfunzf2czf2_bglt;

	typedef struct BgL_bdbzd2blockzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                     *BgL_bdbzd2blockzd2_bglt;


#endif													// BGL_CGEN_COP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda1766z62zzcgen_copz00(obj_t, obj_t);
	static BgL_cblockz00_bglt BGl_z62lambda1847z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cboxzd2refzd2loczd2setz12zc0zzcgen_copz00(BgL_cboxzd2refzd2_bglt,
		obj_t);
	static obj_t BGl_z62lambda1767z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_cblockz00_bglt BGl_z62lambda1849z62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cfailzf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62sfunzf2Czd2locz42zzcgen_copz00(obj_t, obj_t);
	static BgL_cifz00_bglt BGl_z62makezd2cifzb0zzcgen_copz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2bodyzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62sfunzf2Czd2integratedz42zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2strengthzd2setz12z82zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cfailzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_nopzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_csequencez00_bglt
		BGl_csequencezd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_cgotoz00_bglt BGl_cgotozd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_csetqz00_bglt BGl_csetqzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_varczd2loczd2setz12z12zzcgen_copz00(BgL_varcz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31943ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_variablez00_bglt BGl_z62lambda1932z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1933z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_cfuncallz00_bglt BGl_z62makezd2cfuncallzb0zzcgen_copz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_ccastzd2loczd2zzcgen_copz00(BgL_ccastz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_catomzd2loczd2setz12z12zzcgen_copz00(BgL_catomz00_bglt, obj_t);
	static BgL_copz00_bglt BGl_z62lambda1855z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1856z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1776z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1777z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_cpragmaz00_bglt BGl_z62lambda1939z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_varcz00_bglt BGl_z62makezd2varczb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_clabelzd2usedzf3z21zzcgen_copz00(BgL_clabelz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2argszd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	static BgL_variablez00_bglt BGl_z62varczd2variablezb0zzcgen_copz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Czd2thezd2closurezd2setz12z50zzcgen_copz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32011ze3ze5zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_csequencez00_bglt
		BGl_makezd2csequencezd2zzcgen_copz00(obj_t, BgL_typez00_bglt, bool_t,
		obj_t);
	static obj_t BGl_z62cfuncallzd2argszb0zzcgen_copz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62cboxzd2setz12zf3z51zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62cboxzd2refzd2typez62zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_cpragmaz00_bglt BGl_cpragmazd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2predicatezd2ofzf2zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_creturnz00_bglt
		BGl_makezd2creturnzd2zzcgen_copz00(obj_t, BgL_typez00_bglt, bool_t,
		BgL_copz00_bglt);
	static BgL_clabelz00_bglt BGl_z62cgotozd2labelzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cfailz00_bglt BGl_cfailzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t BGl_cappzd2loczd2zzcgen_copz00(BgL_cappz00_bglt);
	BGL_EXPORTED_DECL BgL_bdbzd2blockzd2_bglt
		BGl_bdbzd2blockzd2nilz00zzcgen_copz00(void);
	static obj_t BGl_requirezd2initializa7ationz75zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_cboxzd2refzf3z21zzcgen_copz00(obj_t);
	static BgL_cpragmaz00_bglt BGl_z62lambda1941z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31928ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_creturnz00_bglt BGl_z62lambda1863z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2sidezd2effectzd2setz12z50zzcgen_copz00(obj_t,
		obj_t, obj_t);
	static BgL_creturnz00_bglt BGl_z62lambda1865z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda1947z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1948z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cappzd2argszd2zzcgen_copz00(BgL_cappz00_bglt);
	static obj_t BGl_z62cpragmazf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2strengthzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2stackablez20zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2thezd2closurezf2zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_csequencezd2loczd2zzcgen_copz00(BgL_csequencez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_cgotozd2loczd2zzcgen_copz00(BgL_cgotoz00_bglt);
	static BgL_copz00_bglt BGl_z62cswitchzd2testzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzf2Czd2bodyz20zzcgen_copz00(BgL_sfunz00_bglt);
	static BgL_cvoidz00_bglt BGl_z62cvoidzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_csetqzd2loczd2zzcgen_copz00(BgL_csetqz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2classz20zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_cvoidzf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda1952z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1953z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1873z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1874z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cfuncallzd2funzd2zzcgen_copz00(BgL_cfuncallz00_bglt);
	static BgL_ccastz00_bglt BGl_z62lambda1959z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda1879z62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cifz00_bglt BGl_cifzd2nilzd2zzcgen_copz00(void);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_cpragmazd2loczd2zzcgen_copz00(BgL_cpragmaz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_cfailzd2loczd2zzcgen_copz00(BgL_cfailz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bdbzd2blockzd2locz00zzcgen_copz00(BgL_bdbzd2blockzd2_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_sfunzf2Czd2topzf3zd3zzcgen_copz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1880z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_ccastz00_bglt BGl_z62lambda1961z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62cjumpzd2exzd2itzf3z91zzcgen_copz00(obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static BgL_copz00_bglt BGl_z62lambda1967z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62clabelzd2bodyzd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_csequencezd2copszd2zzcgen_copz00(BgL_csequencez00_bglt);
	static obj_t BGl_z62lambda1968z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Czd2stackzd2allocatorzd2setz12z50zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_cvoidz00_bglt BGl_z62lambda1888z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cappzd2stackablezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cifzd2typezd2zzcgen_copz00(BgL_cifz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_clabelz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_cappz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62cvoidzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62csetqzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62csequencezd2czd2expzf3z91zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcgen_copz00(void);
	static obj_t BGl_z62sfunzf2Czd2effectz42zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localzd2varzd2_bglt
		BGl_localzd2varzd2nilz00zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_creturnzd2typezd2zzcgen_copz00(BgL_creturnz00_bglt);
	static obj_t BGl_z62cfuncallzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_capplyz00_bglt BGl_makezd2capplyzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_copz00_bglt, BgL_copz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31963ze3ze5zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cboxzd2refzd2zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2argszd2namezf2zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_varcz00_bglt BGl_makezd2varczd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_variablez00_bglt);
	static obj_t BGl_z62nopzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_cvoidz00_bglt BGl_z62lambda1890z62zzcgen_copz00(obj_t);
	static BgL_typez00_bglt BGl_z62cboxzd2setz12zd2typez70zzcgen_copz00(obj_t,
		obj_t);
	static BgL_csequencez00_bglt BGl_z62lambda1974z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_varcz00_bglt BGl_z62csetqzd2varzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_csequencez00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_cgotoz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_cifzd2loczd2zzcgen_copz00(BgL_cifz00_bglt);
	static BgL_csequencez00_bglt BGl_z62lambda1976z62zzcgen_copz00(obj_t);
	static BgL_copz00_bglt BGl_z62lambda1897z62zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cjumpzd2exzd2itzd2exitzb0zzcgen_copz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1898z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cmakezd2boxzd2loczd2setz12za2zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_creturnzf3zf3zzcgen_copz00(obj_t);
	static BgL_cmakezd2boxzd2_bglt
		BGl_z62makezd2cmakezd2boxz62zzcgen_copz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_csetqz00_bglt BGl_makezd2csetqzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_varcz00_bglt, BgL_copz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_stopzd2loczd2setz12z12zzcgen_copz00(BgL_stopz00_bglt, obj_t);
	static obj_t BGl_z62localzd2varzd2loczd2setz12za2zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62csequencezd2typezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_cappz00_bglt BGl_z62makezd2cappzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31875ze3ze5zzcgen_copz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31867ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cfailzd2msgzb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31778ze3ze5zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda1983z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1984z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcgen_copz00(void);
	static obj_t BGl_z62sfunzf2Czd2propertyzd2setz12z82zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1989z62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_csetzd2exzd2itz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_cfailzd2loczd2setz12z12zzcgen_copz00(BgL_cfailz00_bglt, obj_t);
	static obj_t BGl_z62csetqzf3z91zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cvoidzd2valuezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cvoidz00_bglt BGl_cvoidzd2nilzd2zzcgen_copz00(void);
	static obj_t BGl_z62zc3z04anonymousza32121ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2bodyzd2setz12z82zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2varzd2locz00zzcgen_copz00(BgL_localzd2varzd2_bglt);
	static obj_t BGl_z62cfuncallzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_creturnzd2valuezd2zzcgen_copz00(BgL_creturnz00_bglt);
	static obj_t BGl_z62localzd2varzd2varsz62zzcgen_copz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62makezd2copzb0zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cvoidz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31892ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cifzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cifzd2loczd2setz12z12zzcgen_copz00(BgL_cifz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_cfuncallz00_bglt
		BGl_cfuncallzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_nopz00_bglt BGl_makezd2nopzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_objectzd2initzd2zzcgen_copz00(void);
	static obj_t BGl_z62lambda1990z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2keysz42zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cjumpzd2exzd2itzd2valuezd2zzcgen_copz00(BgL_cjumpzd2exzd2itz00_bglt);
	static BgL_nopz00_bglt BGl_z62lambda1996z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_clabelz00_bglt BGl_z62sfunzf2Czd2labelz42zzcgen_copz00(obj_t,
		obj_t);
	static BgL_nopz00_bglt BGl_z62lambda1998z62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_stopzf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62sfunzf2Czd2argszd2setz12z82zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62cswitchzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cblockzd2bodyzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_capplyzd2argzd2zzcgen_copz00(BgL_capplyz00_bglt);
	static BgL_csetqz00_bglt BGl_z62makezd2csetqzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_clabelzd2namezd2zzcgen_copz00(BgL_clabelz00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static BgL_clabelz00_bglt BGl_z62makezd2clabelzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_csetzd2exzd2itzd2jumpzd2valuez00zzcgen_copz00
		(BgL_csetzd2exzd2itz00_bglt);
	static obj_t BGl_z62cswitchzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2propertyzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cswitchzd2loczd2setz12z12zzcgen_copz00(BgL_cswitchz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_csequencezf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cgotozf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_nopzd2typezd2zzcgen_copz00(BgL_nopz00_bglt);
	BGL_EXPORTED_DECL BgL_catomz00_bglt BGl_makezd2catomzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cvoidzd2loczd2zzcgen_copz00(BgL_cvoidz00_bglt);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DECL BgL_cboxzd2refzd2_bglt
		BGl_cboxzd2refzd2nilz00zzcgen_copz00(void);
	static obj_t BGl_z62csequencezd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzf2Czd2argsz20zzcgen_copz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32050ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2stackablez42zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2classzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cifzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfuncallzd2loczd2zzcgen_copz00(BgL_cfuncallz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cblockzd2loczd2setz12z12zzcgen_copz00(BgL_cblockz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_varcz00_bglt
		BGl_csetqzd2varzd2zzcgen_copz00(BgL_csetqz00_bglt);
	static BgL_cboxzd2setz12zc0_bglt
		BGl_z62makezd2cboxzd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62ccastzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cblockzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cpragmazd2argszb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_catomzd2valuezd2zzcgen_copz00(BgL_catomz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2effectzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_capplyz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32027ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62capplyzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cappz00_bglt BGl_makezd2cappzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_copz00_bglt, obj_t, obj_t);
	static BgL_cpragmaz00_bglt BGl_z62cpragmazd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2argszd2retescapezf2zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_copz00_bglt BGl_copzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cfailzd2msgzd2zzcgen_copz00(BgL_cfailz00_bglt);
	static BgL_copz00_bglt BGl_z62capplyzd2argzb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62ccastzd2argzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cpragmaz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_cappzd2stackablezd2zzcgen_copz00(BgL_cappz00_bglt);
	static BgL_cboxzd2setz12zc0_bglt
		BGl_z62cboxzd2setz12zd2nilz70zzcgen_copz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cifz00_bglt BGl_makezd2cifzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_copz00_bglt, BgL_copz00_bglt, BgL_copz00_bglt);
	static obj_t BGl_z62csequencezf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cboxzd2refzd2locz00zzcgen_copz00(BgL_cboxzd2refzd2_bglt);
	static obj_t BGl_z62cfailzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cvoidzd2typezd2zzcgen_copz00(BgL_cvoidz00_bglt);
	static BgL_catomz00_bglt BGl_z62makezd2catomzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_csetqzd2loczd2setz12z12zzcgen_copz00(BgL_csetqz00_bglt, obj_t);
	static BgL_cswitchz00_bglt BGl_z62makezd2cswitchzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_cboxzd2refzd2_bglt BGl_z62makezd2cboxzd2refz62zzcgen_copz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cfuncallzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_cfailz00_bglt BGl_makezd2cfailzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_copz00_bglt, BgL_copz00_bglt, BgL_copz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32214ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_cmakezd2boxzd2_bglt
		BGl_z62cmakezd2boxzd2nilz62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cblockzf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62sfunzf2Czd2bodyz42zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cmakezd2boxzd2stackablez62zzcgen_copz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62bdbzd2blockzd2typez62zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cboxzd2setz12zd2typez12zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt);
	static obj_t BGl_z62zc3z04anonymousza31985ze3ze5zzcgen_copz00(obj_t);
	static obj_t BGl_z62sfunzf2Czd2argszd2namez90zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cmakezd2boxzd2_bglt
		BGl_makezd2cmakezd2boxz00zzcgen_copz00(obj_t, BgL_typez00_bglt,
		BgL_copz00_bglt, obj_t);
	static obj_t BGl_methodzd2initzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bdbzd2blockzd2loczd2setz12zc0zzcgen_copz00(BgL_bdbzd2blockzd2_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_creturnzd2loczd2setz12z12zzcgen_copz00(BgL_creturnz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cpragmazd2typezd2zzcgen_copz00(BgL_cpragmaz00_bglt);
	static obj_t BGl_z62stopzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_csequencezd2typezd2zzcgen_copz00(BgL_csequencez00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cgotozd2typezd2zzcgen_copz00(BgL_cgotoz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cpragmazd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62copzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_copzd2loczd2zzcgen_copz00(BgL_copz00_bglt);
	static BgL_localzd2varzd2_bglt
		BGl_z62makezd2localzd2varz62zzcgen_copz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2topzf3zb1zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2loczd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62cboxzd2setz12zd2locz70zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2loczd2setz12z82zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_ccastz00_bglt BGl_makezd2ccastzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_copz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_cpragmazf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31978ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt
		BGl_z62csetzd2exzd2itzd2jumpzd2valuez62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_nopz00_bglt BGl_nopzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2stackablezd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62cboxzd2refzd2loczd2setz12za2zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfuncallzd2strengthzd2zzcgen_copz00(BgL_cfuncallz00_bglt);
	static obj_t BGl_z62cmakezd2boxzd2locz62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2arityz42zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62cvoidzd2typezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2varzd2varsz00zzcgen_copz00(BgL_localzd2varzd2_bglt);
	BGL_EXPORTED_DEF obj_t BGl_nopz00zzcgen_copz00 = BUNSPEC;
	static BgL_cfailz00_bglt BGl_z62makezd2cfailzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_bdbzd2blockzd2_bglt
		BGl_z62makezd2bdbzd2blockz62zzcgen_copz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cfuncallz00_bglt
		BGl_makezd2cfuncallzd2zzcgen_copz00(obj_t, BgL_typez00_bglt,
		BgL_copz00_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2optionalsz20zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_cmakezd2boxzf3z21zzcgen_copz00(obj_t);
	static BgL_capplyz00_bglt BGl_z62makezd2capplyzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62cvoidzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_creturnzd2tailzd2zzcgen_copz00(BgL_creturnz00_bglt);
	BGL_EXPORTED_DECL BgL_variablez00_bglt
		BGl_varczd2variablezd2zzcgen_copz00(BgL_varcz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfuncallzd2argszd2zzcgen_copz00(BgL_cfuncallz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2dssslzd2keywordszd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cboxzd2refzd2typez00zzcgen_copz00(BgL_cboxzd2refzd2_bglt);
	BGL_EXPORTED_DECL bool_t BGl_sfunzf2Czf3z01zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_csetzd2exzd2itz00_bglt
		BGl_makezd2csetzd2exzd2itzd2zzcgen_copz00(obj_t, BgL_typez00_bglt,
		BgL_copz00_bglt, BgL_copz00_bglt, BgL_copz00_bglt);
	static BgL_typez00_bglt BGl_z62cgotozd2typezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_clabelzd2typezd2zzcgen_copz00(BgL_clabelz00_bglt);
	static BgL_csetzd2exzd2itz00_bglt
		BGl_z62makezd2csetzd2exzd2itzb0zzcgen_copz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_ccastzd2argzd2zzcgen_copz00(BgL_ccastz00_bglt);
	static obj_t BGl_z62sfunzf2Czd2argszd2retescapez90zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_nopzd2loczd2zzcgen_copz00(BgL_nopz00_bglt);
	BGL_EXPORTED_DECL BgL_cjumpzd2exzd2itz00_bglt
		BGl_makezd2cjumpzd2exzd2itzd2zzcgen_copz00(obj_t, BgL_typez00_bglt,
		BgL_copz00_bglt, BgL_copz00_bglt);
	static BgL_ccastz00_bglt BGl_z62makezd2ccastzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cswitchzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_clabelzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_csetqzd2valuezd2zzcgen_copz00(BgL_csetqz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32234ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2predicatezd2ofzd2setz12z50zzcgen_copz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62csetzd2exzd2itzd2loczd2setz12z70zzcgen_copz00(obj_t,
		obj_t, obj_t);
	static BgL_stopz00_bglt BGl_z62lambda2007z62zzcgen_copz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_copz00_bglt BGl_z62cifzd2falsezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cboxzd2setz12zc0_bglt
		BGl_makezd2cboxzd2setz12z12zzcgen_copz00(obj_t, BgL_typez00_bglt,
		BgL_copz00_bglt, BgL_copz00_bglt);
	static BgL_stopz00_bglt BGl_z62lambda2009z62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_ccastzd2loczd2setz12z12zzcgen_copz00(BgL_ccastz00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62localzd2varzd2typez62zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cmakezd2boxzd2zzcgen_copz00 = BUNSPEC;
	static BgL_typez00_bglt BGl_z62cappzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62stopzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cmakezd2boxzf3z43zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2integratedzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL bool_t BGl_cappzf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62clabelzd2usedzf3z43zzcgen_copz00(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Czd2dssslzd2keywordszd2setz12z50zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2dssslzd2keywordszf2zzcgen_copz00(BgL_sfunz00_bglt);
	static BgL_catomz00_bglt BGl_z62catomzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2argszd2retescapezd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cfailzd2typezd2zzcgen_copz00(BgL_cfailz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_capplyzd2loczd2setz12z12zzcgen_copz00(BgL_capplyz00_bglt, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2015z62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_csetqzd2typezd2zzcgen_copz00(BgL_csetqz00_bglt);
	static obj_t BGl_z62lambda2016z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cboxzd2setz12zd2valuez12zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt);
	static obj_t BGl_z62cboxzd2setz12zd2loczd2setz12zb0zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cifz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_csetzd2exzd2itzd2typezd2zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt);
	static obj_t BGl_z62capplyzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_creturnz00_bglt BGl_z62makezd2creturnzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cboxzd2setz12zd2valuez70zzcgen_copz00(obj_t,
		obj_t);
	static obj_t BGl_z62clabelzd2namezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62csetzd2exzd2itzd2typezb0zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2stackzd2allocatorzf2zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_csetzd2exzd2itzf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62sfunzf2Czd2stackablezd2setz12z82zzcgen_copz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cpragmaz00_bglt
		BGl_makezd2cpragmazd2zzcgen_copz00(obj_t, BgL_typez00_bglt, obj_t, obj_t);
	static obj_t BGl_z62cgotozd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2effectz20zzcgen_copz00(BgL_sfunz00_bglt);
	static BgL_copz00_bglt BGl_z62lambda2100z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2101z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cboxzd2refzd2varz00zzcgen_copz00(BgL_cboxzd2refzd2_bglt);
	static BgL_csetqz00_bglt BGl_z62lambda2022z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_ccastzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt BGl_makezd2copzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt);
	static obj_t BGl_z62sfunzf2Czd2argsz42zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2105z62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_bdbzd2blockzd2typez00zzcgen_copz00(BgL_bdbzd2blockzd2_bglt);
	static BgL_csetqz00_bglt BGl_z62lambda2025z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda2106z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2classz42zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62copzd2typezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_capplyzd2typezd2zzcgen_copz00(BgL_capplyz00_bglt);
	BGL_EXPORTED_DECL BgL_cblockz00_bglt BGl_makezd2cblockzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_copz00_bglt);
	static obj_t BGl_z62sfunzf2Czd2sidezd2effectz90zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2thezd2closurezd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt,
		obj_t);
	static BgL_varcz00_bglt BGl_z62varczd2nilzb0zzcgen_copz00(obj_t);
	static BgL_copz00_bglt BGl_z62cfuncallzd2funzb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62catomzd2valuezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cifzd2truezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62catomzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2110z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2111z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localzd2varzd2_bglt
		BGl_makezd2localzd2varz00zzcgen_copz00(obj_t, BgL_typez00_bglt, obj_t);
	static BgL_varcz00_bglt BGl_z62lambda2032z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62creturnzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2033z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cappzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32318ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Czd2argszd2retescapezd2setz12z50zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_capplyz00_bglt BGl_z62lambda2117z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2038z62zzcgen_copz00(obj_t, obj_t);
	static BgL_capplyz00_bglt BGl_z62lambda2119z62zzcgen_copz00(obj_t);
	static BgL_typez00_bglt BGl_z62cfailzd2typezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2sidezd2effectzd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cswitchzf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda2039z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62csetqzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2failsafezd2setz12z82zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_clabelzd2usedzf3zd2setz12ze1zzcgen_copz00(BgL_clabelz00_bglt, bool_t);
	static BgL_typez00_bglt BGl_z62cfuncallzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cappzd2funzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cpragmazd2loczd2setz12z12zzcgen_copz00(BgL_cpragmaz00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62varczd2typezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cgotozf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cmakezd2boxzd2valuez00zzcgen_copz00(BgL_cmakezd2boxzd2_bglt);
	static BgL_copz00_bglt BGl_z62cboxzd2setz12zd2varz70zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cboxzd2setz12zc0zzcgen_copz00 = BUNSPEC;
	static BgL_copz00_bglt BGl_z62cmakezd2boxzd2valuez62zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cifzd2falsezd2zzcgen_copz00(BgL_cifz00_bglt);
	BGL_EXPORTED_DECL BgL_bdbzd2blockzd2_bglt
		BGl_makezd2bdbzd2blockz00zzcgen_copz00(obj_t, BgL_typez00_bglt,
		BgL_copz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_varczf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda2203z62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cmakezd2boxzd2stackablez00zzcgen_copz00(BgL_cmakezd2boxzd2_bglt);
	static obj_t BGl_z62lambda2204z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2125z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2126z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_cifz00_bglt BGl_z62lambda2046z62zzcgen_copz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cappzd2typezd2zzcgen_copz00(BgL_cappz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_catomzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_creturnz00_bglt BGl_creturnzd2nilzd2zzcgen_copz00(void);
	static BgL_cifz00_bglt BGl_z62lambda2048z62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_stopzd2typezd2zzcgen_copz00(BgL_stopz00_bglt);
	static obj_t BGl_z62cpragmazd2formatzb0zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62creturnzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2optionalsz42zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcgen_copz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_cmakezd2boxzd2loczd2setz12zc0zzcgen_copz00(BgL_cmakezd2boxzd2_bglt,
		obj_t);
	static obj_t BGl_z62varczd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cvoidzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cjumpzd2exzd2itzd2valuezb0zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_cjumpzd2exzd2itz00_bglt
		BGl_cjumpzd2exzd2itzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_catomz00_bglt BGl_catomzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2varzd2loczd2setz12zc0zzcgen_copz00(BgL_localzd2varzd2_bglt,
		obj_t);
	static BgL_cifz00_bglt BGl_z62cifzd2nilzb0zzcgen_copz00(obj_t);
	static BgL_cjumpzd2exzd2itz00_bglt
		BGl_z62cjumpzd2exzd2itzd2nilzb0zzcgen_copz00(obj_t);
	static obj_t BGl_z62cappzf3z91zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2thezd2closurezd2globalz42zzcgen_copz00(obj_t,
		obj_t);
	static BgL_cmakezd2boxzd2_bglt BGl_z62lambda2210z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2131z62zzcgen_copz00(obj_t, obj_t);
	static BgL_cmakezd2boxzd2_bglt BGl_z62lambda2212z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda2132z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2dssslzd2keywordsz90zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2failsafezd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2056z62zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2218z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2057z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_cappz00_bglt BGl_z62lambda2138z62zzcgen_copz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2219z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt BGl_makezd2sfunzf2Cz20zzcgen_copz00(long,
		obj_t, obj_t, obj_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		BgL_clabelz00_bglt, bool_t);
	static BgL_stopz00_bglt BGl_z62stopzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cfailzd2proczd2zzcgen_copz00(BgL_cfailz00_bglt);
	static BgL_copz00_bglt BGl_z62bdbzd2blockzd2bodyz62zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sfunzf2Czd2integratedz20zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cswitchzd2testzd2zzcgen_copz00(BgL_cswitchz00_bglt);
	static BgL_cjumpzd2exzd2itz00_bglt
		BGl_z62makezd2cjumpzd2exzd2itzb0zzcgen_copz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_clabelzd2loczd2setz12z12zzcgen_copz00(BgL_clabelz00_bglt, obj_t);
	static obj_t BGl_z62lambda2300z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2thezd2closurez90zzcgen_copz00(obj_t, obj_t);
	static BgL_cappz00_bglt BGl_z62lambda2140z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62clabelzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_creturnzd2loczd2zzcgen_copz00(BgL_creturnz00_bglt);
	static BgL_copz00_bglt BGl_z62lambda2061z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2223z62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_localzd2varzd2typez00zzcgen_copz00(BgL_localzd2varzd2_bglt);
	static obj_t BGl_z62lambda2062z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2224z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2305z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2classzd2setz12z82zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2306z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_csequencez00_bglt BGl_z62csequencezd2nilzb0zzcgen_copz00(obj_t);
	static BgL_copz00_bglt BGl_z62lambda2146z62zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2066z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2147z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2067z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2failsafez20zzcgen_copz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62varczd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2failsafez42zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cifzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_cfuncallz00_bglt BGl_z62cfuncallzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cjumpzd2exzd2itzd2loczd2zzcgen_copz00(BgL_cjumpzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_capplyzd2funzd2zzcgen_copz00(BgL_capplyz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_catomzd2loczd2zzcgen_copz00(BgL_catomz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_csetqz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_localzd2varzd2zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cjumpzd2exzd2itzd2typezd2zzcgen_copz00(BgL_cjumpzd2exzd2itz00_bglt);
	static obj_t BGl_z62cifzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cjumpzd2exzd2itzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cifzd2testzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cswitchz00_bglt BGl_cswitchzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2topzf3zd2setz12z13zzcgen_copz00(BgL_sfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_cgotoz00_bglt BGl_makezd2cgotozd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_clabelz00_bglt);
	static BgL_cboxzd2refzd2_bglt BGl_z62lambda2230z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cblockz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62lambda2151z62zzcgen_copz00(obj_t, obj_t);
	static BgL_cboxzd2refzd2_bglt BGl_z62lambda2232z62zzcgen_copz00(obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda2313z62zzcgen_copz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2152z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32346ze3ze5zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cjumpzd2exzd2itzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cswitchzd2clauseszd2zzcgen_copz00(BgL_cswitchz00_bglt);
	static BgL_sfunz00_bglt BGl_z62lambda2316z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32168ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32249ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_localzd2varzd2_bglt BGl_z62lambda2075z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32079ze3ze5zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cswitchz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62lambda2157z62zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2238z62zzcgen_copz00(obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda2319z62zzcgen_copz00(obj_t, obj_t);
	static BgL_bdbzd2blockzd2_bglt
		BGl_z62bdbzd2blockzd2nilz62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_cboxzd2setz12zc0_bglt
		BGl_cboxzd2setz12zd2nilz12zzcgen_copz00(void);
	static BgL_localzd2varzd2_bglt BGl_z62lambda2077z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda2158z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2239z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62stopzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cswitchzd2clauseszb0zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62clabelzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzcgen_copz00(void);
	static BgL_cappz00_bglt BGl_z62cappzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_varczd2typezd2zzcgen_copz00(BgL_varcz00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cblockzd2typezd2zzcgen_copz00(BgL_cblockz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_bdbzd2blockzd2zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_cboxzd2refzd2_bglt
		BGl_makezd2cboxzd2refz00zzcgen_copz00(obj_t, BgL_typez00_bglt,
		BgL_copz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_csequencezd2loczd2setz12z12zzcgen_copz00(BgL_csequencez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cgotozd2loczd2setz12z12zzcgen_copz00(BgL_cgotoz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_copz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_catomzd2typezd2zzcgen_copz00(BgL_catomz00_bglt);
	static obj_t
		BGl_z62sfunzf2Czd2thezd2closurezd2globalzd2setz12z82zzcgen_copz00(obj_t,
		obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cfailzd2proczb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cmakezd2boxzd2_bglt
		BGl_cmakezd2boxzd2nilz00zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_csetzd2exzd2itzd2loczd2setz12z12zzcgen_copz00
		(BgL_csetzd2exzd2itz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzcgen_copz00(void);
	static BgL_cboxzd2refzd2_bglt BGl_z62cboxzd2refzd2nilz62zzcgen_copz00(obj_t);
	static BgL_typez00_bglt BGl_z62cmakezd2boxzd2typez62zzcgen_copz00(obj_t,
		obj_t);
	static obj_t BGl_z62csequencezd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcgen_copz00(void);
	static BgL_copz00_bglt BGl_z62csetqzd2valuezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62varczf3z91zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32193ze3ze5zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_nopzd2loczd2setz12z12zzcgen_copz00(BgL_nopz00_bglt, obj_t);
	static obj_t BGl_z62lambda2083z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32096ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_cfailz00_bglt BGl_z62lambda2164z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_cboxzd2setz12zc0_bglt BGl_z62lambda2245z62zzcgen_copz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2084z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_clabelz00_bglt BGl_z62lambda2327z62zzcgen_copz00(obj_t, obj_t);
	static BgL_cfailz00_bglt BGl_z62lambda2166z62zzcgen_copz00(obj_t);
	static BgL_cboxzd2setz12zc0_bglt BGl_z62lambda2247z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda2328z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cfuncallzd2loczb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_clabelzd2bodyzd2zzcgen_copz00(BgL_clabelz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2propertyz20zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cswitchzd2loczd2zzcgen_copz00(BgL_cswitchz00_bglt);
	BGL_EXPORTED_DECL BgL_cvoidz00_bglt BGl_makezd2cvoidzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_copz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2thezd2closurezd2globalz20zzcgen_copz00(BgL_sfunz00_bglt);
	static BgL_copz00_bglt BGl_z62capplyzd2funzb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2propertyz42zzcgen_copz00(obj_t, obj_t);
	static BgL_cgotoz00_bglt BGl_z62makezd2cgotozb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_stopzd2valuezd2zzcgen_copz00(BgL_stopz00_bglt);
	static obj_t BGl_z62bdbzd2blockzd2locz62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cboxzd2setz12zd2locz12zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt);
	static BgL_cfuncallz00_bglt BGl_z62lambda2091z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2172z62zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2253z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2334z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2173z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2254z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2335z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_catomz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62cappzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static BgL_cfuncallz00_bglt BGl_z62lambda2094z62zzcgen_copz00(obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static BgL_copz00_bglt BGl_z62lambda2177z62zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2258z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2178z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2259z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_csetzd2exzd2itz00_bglt
		BGl_z62csetzd2exzd2itzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cmakezd2boxzd2locz00zzcgen_copz00(BgL_cmakezd2boxzd2_bglt);
	static obj_t BGl_z62cpragmazd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62cboxzd2refzd2locz62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62csetzd2exzd2itzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2predicatezd2ofzd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62catomzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cfuncallzd2strengthzb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cfailzd2objzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfuncallzd2loczd2setz12z12zzcgen_copz00(BgL_cfuncallz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Czf2zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31911ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_bdbzd2blockzd2_bglt BGl_z62lambda2341z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cpragmazd2formatzd2zzcgen_copz00(BgL_cpragmaz00_bglt);
	static BgL_bdbzd2blockzd2_bglt BGl_z62lambda2343z62zzcgen_copz00(obj_t);
	static BgL_copz00_bglt BGl_z62lambda2182z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2183z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_csetzd2exzd2itz00_bglt BGl_z62lambda2265z62zzcgen_copz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_localzd2varzd2_bglt
		BGl_z62localzd2varzd2nilz62zzcgen_copz00(obj_t);
	static BgL_csetzd2exzd2itz00_bglt BGl_z62lambda2267z62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_capplyz00_bglt BGl_capplyzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_cvoidzd2loczd2setz12z12zzcgen_copz00(BgL_cvoidz00_bglt, obj_t);
	static BgL_cswitchz00_bglt BGl_z62lambda2189z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_clabelz00_bglt
		BGl_sfunzf2Czd2labelz20zzcgen_copz00(BgL_sfunz00_bglt);
	static BgL_typez00_bglt BGl_z62capplyzd2typezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_csetzd2exzd2itzd2bodyzd2zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL BgL_cblockz00_bglt BGl_cblockzd2nilzd2zzcgen_copz00(void);
	static BgL_cblockz00_bglt BGl_z62makezd2cblockzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2integratedzd2setz12z82zzcgen_copz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2argszd2noescapezd2setz12z50zzcgen_copz00(obj_t,
		obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62csetzd2exzd2itzd2bodyzb0zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cswitchzd2typezd2zzcgen_copz00(BgL_cswitchz00_bglt);
	static BgL_cvoidz00_bglt BGl_z62makezd2cvoidzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_capplyzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_clabelz00_bglt
		BGl_cgotozd2labelzd2zzcgen_copz00(BgL_cgotoz00_bglt);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_csetzd2exzd2itzd2exitzd2zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt);
	static BgL_copz00_bglt BGl_z62csetzd2exzd2itzd2exitzb0zzcgen_copz00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62cpragmazd2typezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62stopzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2351z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2352z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31718ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_cswitchz00_bglt BGl_z62lambda2191z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62sfunzf2Czd2predicatezd2ofz90zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62csetzd2exzd2itzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2273z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2274z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32269ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62stopzd2valuezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62copzd2nilzb0zzcgen_copz00(obj_t);
	static BgL_copz00_bglt BGl_z62lambda2278z62zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62lambda2198z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2279z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_bdbzd2blockzd2bodyz00zzcgen_copz00(BgL_bdbzd2blockzd2_bglt);
	static obj_t BGl_z62lambda2199z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62cjumpzd2exzd2itzd2loczd2setz12z70zzcgen_copz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cfailz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62sfunzf2Czd2stackzd2allocatorz90zzcgen_copz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_stopz00zzcgen_copz00 = BUNSPEC;
	static BgL_typez00_bglt BGl_z62cifzd2typezb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_copzd2typezd2zzcgen_copz00(BgL_copz00_bglt);
	static BgL_copz00_bglt BGl_z62lambda1712z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62localzd2varzd2locz62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62ccastzf3z91zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62creturnzd2valuezb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cblockzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_capplyzd2loczd2zzcgen_copz00(BgL_capplyz00_bglt);
	static BgL_copz00_bglt BGl_z62lambda1715z62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cpragmazd2argszd2zzcgen_copz00(BgL_cpragmaz00_bglt);
	static BgL_copz00_bglt BGl_z62lambda2283z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2284z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_creturnz00_bglt BGl_z62creturnzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cblockzd2loczd2zzcgen_copz00(BgL_cblockz00_bglt);
	BGL_EXPORTED_DECL BgL_varcz00_bglt BGl_varczd2nilzd2zzcgen_copz00(void);
	static BgL_capplyz00_bglt BGl_z62capplyzd2nilzb0zzcgen_copz00(obj_t);
	static obj_t BGl_z62catomzd2loczd2setz12z70zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cifzd2truezd2zzcgen_copz00(BgL_cifz00_bglt);
	static obj_t BGl_z62creturnzd2tailzb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_creturnz00zzcgen_copz00 = BUNSPEC;
	static BgL_ccastz00_bglt BGl_z62ccastzd2nilzb0zzcgen_copz00(obj_t);
	static BgL_cblockz00_bglt BGl_z62cblockzd2nilzb0zzcgen_copz00(obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	static obj_t BGl_z62sfunzf2Czd2effectzd2setz12z82zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62cjumpzd2exzd2itzd2typezb0zzcgen_copz00(obj_t,
		obj_t);
	static obj_t BGl_z62creturnzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cfuncallz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62sfunzf2Czd2argszd2noescapez90zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2topzf3zd2setz12z71zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_csequencez00_bglt BGl_z62makezd2csequencezb0zzcgen_copz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_ccastz00zzcgen_copz00 = BUNSPEC;
	static obj_t BGl_z62copzd2loczb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_cjumpzd2exzd2itz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_clabelz00_bglt BGl_clabelzd2nilzd2zzcgen_copz00(void);
	static obj_t BGl_z62zc3z04anonymousza31736ze3ze5zzcgen_copz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzcgen_copz00(obj_t);
	static BgL_cjumpzd2exzd2itz00_bglt BGl_z62lambda2291z62zzcgen_copz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32295ze3ze5zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1807z62zzcgen_copz00(obj_t, obj_t);
	static BgL_cjumpzd2exzd2itz00_bglt BGl_z62lambda2293z62zzcgen_copz00(obj_t);
	static BgL_stopz00_bglt BGl_z62makezd2stopzb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1808z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cjumpzd2exzd2itzd2loczd2setz12z12zzcgen_copz00
		(BgL_cjumpzd2exzd2itz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2stackzd2allocatorzd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cappzd2funzd2zzcgen_copz00(BgL_cappz00_bglt);
	static BgL_copz00_bglt BGl_z62lambda2299z62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_ccastzd2typezd2zzcgen_copz00(BgL_ccastz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2thezd2closurezd2globalzd2setz12ze0zzcgen_copz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt BGl_sfunzf2Czd2nilz20zzcgen_copz00(void);
	static BgL_sfunz00_bglt BGl_z62makezd2sfunzf2Cz42zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_csetqzf3zf3zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_cswitchz00_bglt
		BGl_makezd2cswitchzd2zzcgen_copz00(obj_t, BgL_typez00_bglt, BgL_copz00_bglt,
		obj_t);
	static BgL_nopz00_bglt BGl_z62nopzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cmakezd2boxzd2typez00zzcgen_copz00(BgL_cmakezd2boxzd2_bglt);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cfailzd2objzd2zzcgen_copz00(BgL_cfailz00_bglt);
	static BgL_cgotoz00_bglt BGl_z62cgotozd2nilzb0zzcgen_copz00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62creturnzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1734z62zzcgen_copz00(obj_t, obj_t);
	static BgL_csetqz00_bglt BGl_z62csetqzd2nilzb0zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda1735z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_varczd2loczd2zzcgen_copz00(BgL_varcz00_bglt);
	static obj_t BGl_z62catomzf3z91zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62capplyzd2loczb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2argszd2noescapezd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt,
		obj_t);
	static BgL_nopz00_bglt BGl_z62makezd2nopzb0zzcgen_copz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62ccastzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cblockzd2loczb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bdbzd2blockzf3z21zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_localzd2varzf3z21zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2argszd2noescapezf2zzcgen_copz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62copzf3z91zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_copzd2loczd2setz12z12zzcgen_copz00(BgL_copz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cappzd2loczd2setz12z12zzcgen_copz00(BgL_cappz00_bglt, obj_t);
	static obj_t BGl_z62bdbzd2blockzd2loczd2setz12za2zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_cswitchz00_bglt BGl_z62cswitchzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cboxzd2setz12zd2loczd2setz12zd2zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_clabelzd2loczd2zzcgen_copz00(BgL_clabelz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31851ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1740z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Czf3z63zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_csetzd2exzd2itz00_bglt
		BGl_csetzd2exzd2itzd2nilzd2zzcgen_copz00(void);
	static obj_t BGl_z62lambda1741z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_cfailz00_bglt BGl_z62cfailzd2nilzb0zzcgen_copz00(obj_t);
	static BgL_catomz00_bglt BGl_z62lambda1905z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_stopz00_bglt BGl_stopzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_cfuncallzd2typezd2zzcgen_copz00(BgL_cfuncallz00_bglt);
	static BgL_catomz00_bglt BGl_z62lambda1907z62zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_clabelzd2bodyzd2setz12z12zzcgen_copz00(BgL_clabelz00_bglt, obj_t);
	static BgL_clabelz00_bglt BGl_z62clabelzd2nilzb0zzcgen_copz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzf2Czd2locz20zzcgen_copz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62cboxzd2refzf3z43zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cboxzd2setz12zd2varz12zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt);
	static obj_t BGl_z62nopzd2loczb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cvoidzd2valuezd2zzcgen_copz00(BgL_cvoidz00_bglt);
	static obj_t BGl_z62clabelzd2usedzf3zd2setz12z83zzcgen_copz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62nopzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62ccastzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62cblockzd2typezb0zzcgen_copz00(obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62sfunzf2Czd2nilz42zzcgen_copz00(obj_t);
	static obj_t BGl_z62cappzd2argszb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62csequencezd2copszb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL long BGl_sfunzf2Czd2arityz20zzcgen_copz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32000ze3ze5zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_csequencezd2czd2expzf3zf3zzcgen_copz00(BgL_csequencez00_bglt);
	static obj_t BGl_z62clabelzf3z91zzcgen_copz00(obj_t, obj_t);
	static BgL_cpragmaz00_bglt BGl_z62makezd2cpragmazb0zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62cgotozd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62csetqzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62cboxzd2refzd2varz62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62nopzf3z91zzcgen_copz00(obj_t, obj_t);
	static BgL_clabelz00_bglt BGl_z62lambda1751z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31755ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_cgotoz00_bglt BGl_z62lambda1832z62zzcgen_copz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31836ze3ze5zzcgen_copz00(obj_t, obj_t);
	static BgL_clabelz00_bglt BGl_z62lambda1753z62zzcgen_copz00(obj_t);
	static BgL_cgotoz00_bglt BGl_z62lambda1834z62zzcgen_copz00(obj_t);
	static obj_t BGl_z62lambda1915z62zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_sfunzf2Czd2keysz20zzcgen_copz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1916z62zzcgen_copz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2sidezd2effectzf2zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cjumpzd2exzd2itzd2exitzd2zzcgen_copz00(BgL_cjumpzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL BgL_ccastz00_bglt BGl_ccastzd2nilzd2zzcgen_copz00(void);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cifzd2testzd2zzcgen_copz00(BgL_cifz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_copzf3zf3zzcgen_copz00(obj_t);
	static obj_t BGl_z62clabelzd2bodyzb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62bdbzd2blockzf3z43zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_cboxzd2setz12zf3z33zzcgen_copz00(obj_t);
	static obj_t BGl_z62cswitchzd2loczb0zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62localzd2varzf3z43zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_cblockzd2bodyzd2zzcgen_copz00(BgL_cblockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Czd2strengthz20zzcgen_copz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_varcz00zzcgen_copz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_csetzd2exzd2itzd2loczd2zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt);
	BGL_EXPORTED_DECL BgL_clabelz00_bglt BGl_makezd2clabelzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, obj_t, bool_t, obj_t);
	static obj_t BGl_z62sfunzf2Czd2strengthz42zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62cfailzd2loczb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stopzd2loczd2zzcgen_copz00(BgL_stopz00_bglt);
	BGL_EXPORTED_DECL BgL_stopz00_bglt BGl_makezd2stopzd2zzcgen_copz00(obj_t,
		BgL_typez00_bglt, BgL_copz00_bglt);
	static BgL_clabelz00_bglt BGl_z62lambda1840z62zzcgen_copz00(obj_t, obj_t);
	static obj_t BGl_z62clabelzd2loczb0zzcgen_copz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_cappz00_bglt BGl_cappzd2nilzd2zzcgen_copz00(void);
	static obj_t BGl_z62lambda1841z62zzcgen_copz00(obj_t, obj_t, obj_t);
	static BgL_varcz00_bglt BGl_z62lambda1924z62zzcgen_copz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_varcz00_bglt BGl_z62lambda1926z62zzcgen_copz00(obj_t);
	static obj_t __cnst[62];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2refzd2typezd2envzd2zzcgen_copz00,
		BgL_bgl_za762cboxza7d2refza7d22637za7,
		BGl_z62cboxzd2refzd2typez62zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cappzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cappza7d2locza7d22638za7,
		BGl_z62cappzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetzd2exzd2itzd2exitzd2envz00zzcgen_copz00,
		BgL_bgl_za762csetza7d2exza7d2i2639za7,
		BGl_z62csetzd2exzd2itzd2exitzb0zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2localzd2varzd2envzd2zzcgen_copz00,
		BgL_bgl_za762makeza7d2localza72640za7,
		BGl_z62makezd2localzd2varz62zzcgen_copz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czf3zd2envzd3zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7f3za762641z00, BGl_z62sfunzf2Czf3z63zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bdbzd2blockzd2nilzd2envzd2zzcgen_copz00,
		BgL_bgl_za762bdbza7d2blockza7d2642za7,
		BGl_z62bdbzd2blockzd2nilz62zzcgen_copz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2sidezd2effectzd2setz12zd2envze0zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2si2643za7,
		BGl_z62sfunzf2Czd2sidezd2effectzd2setz12z50zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cswitchzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cswitchza7f3za7912644za7, BGl_z62cswitchzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccastzd2argzd2envz00zzcgen_copz00,
		BgL_bgl_za762ccastza7d2argza7b2645za7, BGl_z62ccastzd2argzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2loczd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2lo2646za7,
		BGl_z62sfunzf2Czd2locz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_varczd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762varcza7d2typeza7b2647za7, BGl_z62varczd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_catomzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762catomza7d2typeza72648za7, BGl_z62catomzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cifzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cifza7d2locza7d2s2649za7,
		BGl_z62cifzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cmakezd2boxzd2loczd2setz12zd2envz12zzcgen_copz00,
		BgL_bgl_za762cmakeza7d2boxza7d2650za7,
		BGl_z62cmakezd2boxzd2loczd2setz12za2zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_copzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762copza7f3za791za7za7cg2651za7, BGl_z62copzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_catomzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762catomza7d2nilza7b2652za7, BGl_z62catomzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cblockzd2bodyzd2envz00zzcgen_copz00,
		BgL_bgl_za762cblockza7d2body2653z00, BGl_z62cblockzd2bodyzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stopzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762stopza7d2locza7b02654za7, BGl_z62stopzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2stopzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2stopza7b2655za7, BGl_z62makezd2stopzb0zzcgen_copz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cappzd2funzd2envz00zzcgen_copz00,
		BgL_bgl_za762cappza7d2funza7b02656za7, BGl_z62cappzd2funzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2catomzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2catomza72657za7, BGl_z62makezd2catomzb0zzcgen_copz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccastzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762ccastza7f3za791za7za72658za7, BGl_z62ccastzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2copzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2copza7b02659za7, BGl_z62makezd2copzb0zzcgen_copz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_varczd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762varcza7d2locza7d22660za7,
		BGl_z62varczd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cmakezd2boxzf3zd2envzf3zzcgen_copz00,
		BgL_bgl_za762cmakeza7d2boxza7f2661za7,
		BGl_z62cmakezd2boxzf3z43zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccastzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762ccastza7d2locza7b2662za7, BGl_z62ccastzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_catomzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762catomza7d2locza7d2663za7,
		BGl_z62catomzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2labelzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2la2664za7,
		BGl_z62sfunzf2Czd2labelz42zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2csetzd2exzd2itzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2csetza7d2665za7,
		BGl_z62makezd2csetzd2exzd2itzb0zzcgen_copz00, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cappzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cappza7b2666za7, BGl_z62makezd2cappzb0zzcgen_copz00,
		0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bdbzd2blockzd2bodyzd2envzd2zzcgen_copz00,
		BgL_bgl_za762bdbza7d2blockza7d2667za7,
		BGl_z62bdbzd2blockzd2bodyz62zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2cjumpzd2exzd2itzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cjumpza72668za7,
		BGl_z62makezd2cjumpzd2exzd2itzb0zzcgen_copz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfuncallzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cfuncallza7f3za792669za7, BGl_z62cfuncallzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2bodyzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2bo2670za7,
		BGl_z62sfunzf2Czd2bodyz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2classzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2cl2671za7,
		BGl_z62sfunzf2Czd2classz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stopzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762stopza7d2typeza7b2672za7, BGl_z62stopzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfuncallzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cfuncallza7d2ni2673z00, BGl_z62cfuncallzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2nopzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2nopza7b02674za7, BGl_z62makezd2nopzb0zzcgen_copz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2sidezd2effectzd2envz20zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2si2675za7,
		BGl_z62sfunzf2Czd2sidezd2effectz90zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cjumpzd2exzd2itzd2exitzd2envz00zzcgen_copz00,
		BgL_bgl_za762cjumpza7d2exza7d22676za7,
		BGl_z62cjumpzd2exzd2itzd2exitzb0zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2thezd2closurezd2globalzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2th2677za7,
		BGl_z62sfunzf2Czd2thezd2closurezd2globalz42zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2strengthzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2st2678za7,
		BGl_z62sfunzf2Czd2strengthz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfailzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cfailza7d2typeza72679za7, BGl_z62cfailzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_capplyzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762capplyza7d2locza72680za7,
		BGl_z62capplyzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_capplyzd2argzd2envz00zzcgen_copz00,
		BgL_bgl_za762capplyza7d2argza72681za7, BGl_z62capplyzd2argzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_clabelzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762clabelza7d2nilza72682za7, BGl_z62clabelzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2stackablezd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2st2683za7,
		BGl_z62sfunzf2Czd2stackablezd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvoidzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cvoidza7d2locza7b2684za7, BGl_z62cvoidzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cifzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cifza7d2typeza7b02685za7, BGl_z62cifzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2500z00zzcgen_copz00,
		BgL_bgl_za762lambda1941za7622686z00, BGl_z62lambda1941z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2501z00zzcgen_copz00,
		BgL_bgl_za762lambda1939za7622687z00, BGl_z62lambda1939z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cpragmazd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cpragm2688z00, BGl_z62makezd2cpragmazb0zzcgen_copz00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2502z00zzcgen_copz00,
		BgL_bgl_za762lambda1968za7622689z00, BGl_z62lambda1968z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2503z00zzcgen_copz00,
		BgL_bgl_za762lambda1967za7622690z00, BGl_z62lambda1967z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2argszd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ar2691za7,
		BGl_z62sfunzf2Czd2argsz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2504z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2692za7,
		BGl_z62zc3z04anonymousza31963ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2505z00zzcgen_copz00,
		BgL_bgl_za762lambda1961za7622693z00, BGl_z62lambda1961z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2506z00zzcgen_copz00,
		BgL_bgl_za762lambda1959za7622694z00, BGl_z62lambda1959z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2507z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2695za7,
		BGl_z62zc3z04anonymousza31985ze3ze5zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2508z00zzcgen_copz00,
		BgL_bgl_za762lambda1984za7622696z00, BGl_z62lambda1984z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2509z00zzcgen_copz00,
		BgL_bgl_za762lambda1983za7622697z00, BGl_z62lambda1983z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2varzd2nilzd2envzd2zzcgen_copz00,
		BgL_bgl_za762localza7d2varza7d2698za7,
		BGl_z62localzd2varzd2nilz62zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_varczd2variablezd2envz00zzcgen_copz00,
		BgL_bgl_za762varcza7d2variab2699z00, BGl_z62varczd2variablezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2argszd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ar2700za7,
		BGl_z62sfunzf2Czd2argszd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2argszd2noescapezd2envz20zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ar2701za7,
		BGl_z62sfunzf2Czd2argszd2noescapez90zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cswitchzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cswitchza7d2typ2702z00, BGl_z62cswitchzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cswitchzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cswitchza7d2nil2703z00, BGl_z62cswitchzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2510z00zzcgen_copz00,
		BgL_bgl_za762lambda1990za7622704z00, BGl_z62lambda1990z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2511z00zzcgen_copz00,
		BgL_bgl_za762lambda1989za7622705z00, BGl_z62lambda1989z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2512z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2706za7,
		BGl_z62zc3z04anonymousza31978ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2513z00zzcgen_copz00,
		BgL_bgl_za762lambda1976za7622707z00, BGl_z62lambda1976z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2514z00zzcgen_copz00,
		BgL_bgl_za762lambda1974za7622708z00, BGl_z62lambda1974z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2515z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2709za7,
		BGl_z62zc3z04anonymousza32000ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cboxzd2setz12zd2loczd2setz12zd2envz00zzcgen_copz00,
		BgL_bgl_za762cboxza7d2setza7122710za7,
		BGl_z62cboxzd2setz12zd2loczd2setz12zb0zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2516z00zzcgen_copz00,
		BgL_bgl_za762lambda1998za7622711z00, BGl_z62lambda1998z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2517z00zzcgen_copz00,
		BgL_bgl_za762lambda1996za7622712z00, BGl_z62lambda1996z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2518z00zzcgen_copz00,
		BgL_bgl_za762lambda2016za7622713z00, BGl_z62lambda2016z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2519z00zzcgen_copz00,
		BgL_bgl_za762lambda2015za7622714z00, BGl_z62lambda2015z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetzd2exzd2itzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762csetza7d2exza7d2i2715za7,
		BGl_z62csetzd2exzd2itzd2nilzb0zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cblockzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cblockza7d2type2716z00, BGl_z62cblockzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2600z00zzcgen_copz00,
		BgL_bgl_za762lambda2258za7622717z00, BGl_z62lambda2258z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2601z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2718za7,
		BGl_z62zc3z04anonymousza32249ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2520z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2719za7,
		BGl_z62zc3z04anonymousza32011ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2602z00zzcgen_copz00,
		BgL_bgl_za762lambda2247za7622720z00, BGl_z62lambda2247z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2521z00zzcgen_copz00,
		BgL_bgl_za762lambda2009za7622721z00, BGl_z62lambda2009z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2603z00zzcgen_copz00,
		BgL_bgl_za762lambda2245za7622722z00, BGl_z62lambda2245z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2522z00zzcgen_copz00,
		BgL_bgl_za762lambda2007za7622723z00, BGl_z62lambda2007z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2604z00zzcgen_copz00,
		BgL_bgl_za762lambda2274za7622724z00, BGl_z62lambda2274z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2523z00zzcgen_copz00,
		BgL_bgl_za762lambda2033za7622725z00, BGl_z62lambda2033z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2605z00zzcgen_copz00,
		BgL_bgl_za762lambda2273za7622726z00, BGl_z62lambda2273z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2524z00zzcgen_copz00,
		BgL_bgl_za762lambda2032za7622727z00, BGl_z62lambda2032z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2443z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2728za7,
		BGl_z62zc3z04anonymousza31736ze3ze5zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2606z00zzcgen_copz00,
		BgL_bgl_za762lambda2279za7622729z00, BGl_z62lambda2279z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2525z00zzcgen_copz00,
		BgL_bgl_za762lambda2039za7622730z00, BGl_z62lambda2039z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2444z00zzcgen_copz00,
		BgL_bgl_za762lambda1735za7622731z00, BGl_z62lambda1735z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2607z00zzcgen_copz00,
		BgL_bgl_za762lambda2278za7622732z00, BGl_z62lambda2278z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2526z00zzcgen_copz00,
		BgL_bgl_za762lambda2038za7622733z00, BGl_z62lambda2038z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2445z00zzcgen_copz00,
		BgL_bgl_za762lambda1734za7622734z00, BGl_z62lambda1734z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2608z00zzcgen_copz00,
		BgL_bgl_za762lambda2284za7622735z00, BGl_z62lambda2284z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2527z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2736za7,
		BGl_z62zc3z04anonymousza32027ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2446z00zzcgen_copz00,
		BgL_bgl_za762lambda1741za7622737z00, BGl_z62lambda1741z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cmakezd2boxzd2stackablezd2envzd2zzcgen_copz00,
		BgL_bgl_za762cmakeza7d2boxza7d2738za7,
		BGl_z62cmakezd2boxzd2stackablez62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2609z00zzcgen_copz00,
		BgL_bgl_za762lambda2283za7622739z00, BGl_z62lambda2283z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2528z00zzcgen_copz00,
		BgL_bgl_za762lambda2025za7622740z00, BGl_z62lambda2025z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2447z00zzcgen_copz00,
		BgL_bgl_za762lambda1740za7622741z00, BGl_z62lambda1740z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2529z00zzcgen_copz00,
		BgL_bgl_za762lambda2022za7622742z00, BGl_z62lambda2022z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2448z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2743za7,
		BGl_z62zc3z04anonymousza31718ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2449z00zzcgen_copz00,
		BgL_bgl_za762lambda1715za7622744z00, BGl_z62lambda1715z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_capplyzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762capplyza7d2locza72745za7, BGl_z62capplyzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_varczd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762varcza7d2nilza7b02746za7, BGl_z62varczd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cifzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cifza7d2nilza7b0za72747z00, BGl_z62cifzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2varzd2loczd2setz12zd2envz12zzcgen_copz00,
		BgL_bgl_za762localza7d2varza7d2748za7,
		BGl_z62localzd2varzd2loczd2setz12za2zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2610z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2749za7,
		BGl_z62zc3z04anonymousza32269ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2611z00zzcgen_copz00,
		BgL_bgl_za762lambda2267za7622750z00, BGl_z62lambda2267z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2530z00zzcgen_copz00,
		BgL_bgl_za762lambda2057za7622751z00, BGl_z62lambda2057z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_creturnzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762creturnza7d2loc2752z00,
		BGl_z62creturnzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2612z00zzcgen_copz00,
		BgL_bgl_za762lambda2265za7622753z00, BGl_z62lambda2265z62zzcgen_copz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2531z00zzcgen_copz00,
		BgL_bgl_za762lambda2056za7622754z00, BGl_z62lambda2056z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2450z00zzcgen_copz00,
		BgL_bgl_za762lambda1712za7622755z00, BGl_z62lambda1712z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2613z00zzcgen_copz00,
		BgL_bgl_za762lambda2300za7622756z00, BGl_z62lambda2300z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2532z00zzcgen_copz00,
		BgL_bgl_za762lambda2062za7622757z00, BGl_z62lambda2062z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2451z00zzcgen_copz00,
		BgL_bgl_za762lambda1767za7622758z00, BGl_z62lambda1767z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfailzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cfailza7d2locza7b2759za7, BGl_z62cfailzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2614z00zzcgen_copz00,
		BgL_bgl_za762lambda2299za7622760z00, BGl_z62lambda2299z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2533z00zzcgen_copz00,
		BgL_bgl_za762lambda2061za7622761z00, BGl_z62lambda2061z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2452z00zzcgen_copz00,
		BgL_bgl_za762lambda1766za7622762z00, BGl_z62lambda1766z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2615z00zzcgen_copz00,
		BgL_bgl_za762lambda2306za7622763z00, BGl_z62lambda2306z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2534z00zzcgen_copz00,
		BgL_bgl_za762lambda2067za7622764z00, BGl_z62lambda2067z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2453z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2765za7,
		BGl_z62zc3z04anonymousza31778ze3ze5zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2616z00zzcgen_copz00,
		BgL_bgl_za762lambda2305za7622766z00, BGl_z62lambda2305z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2535z00zzcgen_copz00,
		BgL_bgl_za762lambda2066za7622767z00, BGl_z62lambda2066z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2454z00zzcgen_copz00,
		BgL_bgl_za762lambda1777za7622768z00, BGl_z62lambda1777z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2617z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2769za7,
		BGl_z62zc3z04anonymousza32295ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2536z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2770za7,
		BGl_z62zc3z04anonymousza32050ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2455z00zzcgen_copz00,
		BgL_bgl_za762lambda1776za7622771z00, BGl_z62lambda1776z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2618z00zzcgen_copz00,
		BgL_bgl_za762lambda2293za7622772z00, BGl_z62lambda2293z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2537z00zzcgen_copz00,
		BgL_bgl_za762lambda2048za7622773z00, BGl_z62lambda2048z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2456z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2774za7,
		BGl_z62zc3z04anonymousza31809ze3ze5zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetqzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762csetqza7d2typeza72775za7, BGl_z62csetqzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2619z00zzcgen_copz00,
		BgL_bgl_za762lambda2291za7622776z00, BGl_z62lambda2291z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2538z00zzcgen_copz00,
		BgL_bgl_za762lambda2046za7622777z00, BGl_z62lambda2046z62zzcgen_copz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2457z00zzcgen_copz00,
		BgL_bgl_za762lambda1808za7622778z00, BGl_z62lambda1808z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2539z00zzcgen_copz00,
		BgL_bgl_za762lambda2084za7622779z00, BGl_z62lambda2084z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2458z00zzcgen_copz00,
		BgL_bgl_za762lambda1807za7622780z00, BGl_z62lambda1807z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_clabelzd2namezd2envz00zzcgen_copz00,
		BgL_bgl_za762clabelza7d2name2781z00, BGl_z62clabelzd2namezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2459z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2782za7,
		BGl_z62zc3z04anonymousza31755ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cboxzd2refzd2loczd2setz12zd2envz12zzcgen_copz00,
		BgL_bgl_za762cboxza7d2refza7d22783za7,
		BGl_z62cboxzd2refzd2loczd2setz12za2zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2stackzd2allocatorzd2envz20zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2st2784za7,
		BGl_z62sfunzf2Czd2stackzd2allocatorz90zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2bodyzd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2bo2785za7,
		BGl_z62sfunzf2Czd2bodyzd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2620z00zzcgen_copz00,
		BgL_bgl_za762lambda2328za7622786z00, BGl_z62lambda2328z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2621z00zzcgen_copz00,
		BgL_bgl_za762lambda2327za7622787z00, BGl_z62lambda2327z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2540z00zzcgen_copz00,
		BgL_bgl_za762lambda2083za7622788z00, BGl_z62lambda2083z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2622z00zzcgen_copz00,
		BgL_bgl_za762lambda2335za7622789z00, BGl_z62lambda2335z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2541z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2790za7,
		BGl_z62zc3z04anonymousza32079ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2460z00zzcgen_copz00,
		BgL_bgl_za762lambda1753za7622791z00, BGl_z62lambda1753z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2623z00zzcgen_copz00,
		BgL_bgl_za762lambda2334za7622792z00, BGl_z62lambda2334z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2542z00zzcgen_copz00,
		BgL_bgl_za762lambda2077za7622793z00, BGl_z62lambda2077z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2461z00zzcgen_copz00,
		BgL_bgl_za762lambda1751za7622794z00, BGl_z62lambda1751z62zzcgen_copz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2624z00zzcgen_copz00,
		BgL_bgl_za762lambda2319za7622795z00, BGl_z62lambda2319z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2543z00zzcgen_copz00,
		BgL_bgl_za762lambda2075za7622796z00, BGl_z62lambda2075z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2462z00zzcgen_copz00,
		BgL_bgl_za762lambda1841za7622797z00, BGl_z62lambda1841z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2625z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2798za7,
		BGl_z62zc3z04anonymousza32318ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2544z00zzcgen_copz00,
		BgL_bgl_za762lambda2101za7622799z00, BGl_z62lambda2101z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2463z00zzcgen_copz00,
		BgL_bgl_za762lambda1840za7622800z00, BGl_z62lambda1840z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfuncallzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cfuncallza7d2lo2801z00,
		BGl_z62cfuncallzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2626z00zzcgen_copz00,
		BgL_bgl_za762lambda2316za7622802z00, BGl_z62lambda2316z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2545z00zzcgen_copz00,
		BgL_bgl_za762lambda2100za7622803z00, BGl_z62lambda2100z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2464z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2804za7,
		BGl_z62zc3z04anonymousza31836ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_creturnzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762creturnza7d2loc2805z00, BGl_z62creturnzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2633z00zzcgen_copz00,
		BgL_bgl_string2633za700za7za7c2806za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2627z00zzcgen_copz00,
		BgL_bgl_za762lambda2313za7622807z00, BGl_z62lambda2313z62zzcgen_copz00, 0L,
		BUNSPEC, 24);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2546z00zzcgen_copz00,
		BgL_bgl_za762lambda2106za7622808z00, BGl_z62lambda2106z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2465z00zzcgen_copz00,
		BgL_bgl_za762lambda1834za7622809z00, BGl_z62lambda1834z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bdbzd2blockzd2typezd2envzd2zzcgen_copz00,
		BgL_bgl_za762bdbza7d2blockza7d2810za7,
		BGl_z62bdbzd2blockzd2typez62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2634z00zzcgen_copz00,
		BgL_bgl_string2634za700za7za7c2811za7, "cgen_cop", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2628z00zzcgen_copz00,
		BgL_bgl_za762lambda2352za7622812z00, BGl_z62lambda2352z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2547z00zzcgen_copz00,
		BgL_bgl_za762lambda2105za7622813z00, BGl_z62lambda2105z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2466z00zzcgen_copz00,
		BgL_bgl_za762lambda1832za7622814z00, BGl_z62lambda1832z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2635z00zzcgen_copz00,
		BgL_bgl_string2635za700za7za7c2815za7,
		"_ bdb-block sfun/C integrated cjump-ex-it cset-ex-it jump-value exit cbox-set! cbox-ref cmake-box cswitch clauses cfail msg proc capp stackable capply cfuncall symbol strength fun local-var vars cif false true test csetq var stop nop csequence cops c-exp? ccast arg cpragma args format varc variable catom cvoid creturn value tail cblock cgoto label clabel body bool used? bstring name cgen_cop cop type obj loc ",
		412);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2629z00zzcgen_copz00,
		BgL_bgl_za762lambda2351za7622816z00, BGl_z62lambda2351z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2548z00zzcgen_copz00,
		BgL_bgl_za762lambda2111za7622817z00, BGl_z62lambda2111z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2467z00zzcgen_copz00,
		BgL_bgl_za762lambda1856za7622818z00, BGl_z62lambda1856z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cifzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cifza7f3za791za7za7cg2819za7, BGl_z62cifzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2549z00zzcgen_copz00,
		BgL_bgl_za762lambda2110za7622820z00, BGl_z62lambda2110z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2468z00zzcgen_copz00,
		BgL_bgl_za762lambda1855za7622821z00, BGl_z62lambda1855z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_creturnzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762creturnza7d2typ2822z00, BGl_z62creturnzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2469z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2823za7,
		BGl_z62zc3z04anonymousza31851ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2failsafezd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2fa2824za7,
		BGl_z62sfunzf2Czd2failsafez42zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cjumpzd2exzd2itzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cjumpza7d2exza7d22825za7,
		BGl_z62cjumpzd2exzd2itzd2loczb0zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_creturnzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762creturnza7f3za7912826za7, BGl_z62creturnzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2630z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2827za7,
		BGl_z62zc3z04anonymousza32346ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2631z00zzcgen_copz00,
		BgL_bgl_za762lambda2343za7622828z00, BGl_z62lambda2343z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2550z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2829za7,
		BGl_z62zc3z04anonymousza32096ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2632z00zzcgen_copz00,
		BgL_bgl_za762lambda2341za7622830z00, BGl_z62lambda2341z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2551z00zzcgen_copz00,
		BgL_bgl_za762lambda2094za7622831z00, BGl_z62lambda2094z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2470z00zzcgen_copz00,
		BgL_bgl_za762lambda1849za7622832z00, BGl_z62lambda1849z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cpragmazd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cpragmaza7d2nil2833z00, BGl_z62cpragmazd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2552z00zzcgen_copz00,
		BgL_bgl_za762lambda2091za7622834z00, BGl_z62lambda2091z62zzcgen_copz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2471z00zzcgen_copz00,
		BgL_bgl_za762lambda1847za7622835z00, BGl_z62lambda1847z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2553z00zzcgen_copz00,
		BgL_bgl_za762lambda2126za7622836z00, BGl_z62lambda2126z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2472z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2837za7,
		BGl_z62zc3z04anonymousza31875ze3ze5zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2554z00zzcgen_copz00,
		BgL_bgl_za762lambda2125za7622838z00, BGl_z62lambda2125z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2473z00zzcgen_copz00,
		BgL_bgl_za762lambda1874za7622839z00, BGl_z62lambda1874z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2555z00zzcgen_copz00,
		BgL_bgl_za762lambda2132za7622840z00, BGl_z62lambda2132z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2474z00zzcgen_copz00,
		BgL_bgl_za762lambda1873za7622841z00, BGl_z62lambda1873z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2556z00zzcgen_copz00,
		BgL_bgl_za762lambda2131za7622842z00, BGl_z62lambda2131z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2475z00zzcgen_copz00,
		BgL_bgl_za762lambda1880za7622843z00, BGl_z62lambda1880z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2557z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2844za7,
		BGl_z62zc3z04anonymousza32121ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2476z00zzcgen_copz00,
		BgL_bgl_za762lambda1879za7622845z00, BGl_z62lambda1879z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2argszd2retescapezd2envz20zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ar2846za7,
		BGl_z62sfunzf2Czd2argszd2retescapez90zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2558z00zzcgen_copz00,
		BgL_bgl_za762lambda2119za7622847z00, BGl_z62lambda2119z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2477z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2848za7,
		BGl_z62zc3z04anonymousza31867ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2559z00zzcgen_copz00,
		BgL_bgl_za762lambda2117za7622849z00, BGl_z62lambda2117z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2478z00zzcgen_copz00,
		BgL_bgl_za762lambda1865za7622850z00, BGl_z62lambda1865z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2479z00zzcgen_copz00,
		BgL_bgl_za762lambda1863za7622851z00, BGl_z62lambda1863z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2setz12zd2nilzd2envzc0zzcgen_copz00,
		BgL_bgl_za762cboxza7d2setza7122852za7,
		BGl_z62cboxzd2setz12zd2nilz70zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nopzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762nopza7d2locza7b0za72853z00, BGl_z62nopzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2setz12zd2varzd2envzc0zzcgen_copz00,
		BgL_bgl_za762cboxza7d2setza7122854za7,
		BGl_z62cboxzd2setz12zd2varz70zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2refzd2loczd2envzd2zzcgen_copz00,
		BgL_bgl_za762cboxza7d2refza7d22855za7,
		BGl_z62cboxzd2refzd2locz62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2arityzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ar2856za7,
		BGl_z62sfunzf2Czd2arityz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2560z00zzcgen_copz00,
		BgL_bgl_za762lambda2147za7622857z00, BGl_z62lambda2147z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetqzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762csetqza7d2locza7b2858za7, BGl_z62csetqzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2561z00zzcgen_copz00,
		BgL_bgl_za762lambda2146za7622859z00, BGl_z62lambda2146z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2480z00zzcgen_copz00,
		BgL_bgl_za762lambda1898za7622860z00, BGl_z62lambda1898z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2562z00zzcgen_copz00,
		BgL_bgl_za762lambda2152za7622861z00, BGl_z62lambda2152z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2481z00zzcgen_copz00,
		BgL_bgl_za762lambda1897za7622862z00, BGl_z62lambda1897z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2563z00zzcgen_copz00,
		BgL_bgl_za762lambda2151za7622863z00, BGl_z62lambda2151z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2482z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2864za7,
		BGl_z62zc3z04anonymousza31892ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2564z00zzcgen_copz00,
		BgL_bgl_za762lambda2158za7622865z00, BGl_z62lambda2158z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2483z00zzcgen_copz00,
		BgL_bgl_za762lambda1890za7622866z00, BGl_z62lambda1890z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2565z00zzcgen_copz00,
		BgL_bgl_za762lambda2157za7622867z00, BGl_z62lambda2157z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2484z00zzcgen_copz00,
		BgL_bgl_za762lambda1888za7622868z00, BGl_z62lambda1888z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2566z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2869za7,
		BGl_z62zc3z04anonymousza32142ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2485z00zzcgen_copz00,
		BgL_bgl_za762lambda1916za7622870z00, BGl_z62lambda1916z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2567z00zzcgen_copz00,
		BgL_bgl_za762lambda2140za7622871z00, BGl_z62lambda2140z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2486z00zzcgen_copz00,
		BgL_bgl_za762lambda1915za7622872z00, BGl_z62lambda1915z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2568z00zzcgen_copz00,
		BgL_bgl_za762lambda2138za7622873z00, BGl_z62lambda2138z62zzcgen_copz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2487z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2874za7,
		BGl_z62zc3z04anonymousza31911ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2569z00zzcgen_copz00,
		BgL_bgl_za762lambda2173za7622875z00, BGl_z62lambda2173z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2488z00zzcgen_copz00,
		BgL_bgl_za762lambda1907za7622876z00, BGl_z62lambda1907z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2489z00zzcgen_copz00,
		BgL_bgl_za762lambda1905za7622877z00, BGl_z62lambda1905z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cpragmazd2argszd2envz00zzcgen_copz00,
		BgL_bgl_za762cpragmaza7d2arg2878z00, BGl_z62cpragmazd2argszb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_clabelzd2bodyzd2envz00zzcgen_copz00,
		BgL_bgl_za762clabelza7d2body2879z00, BGl_z62clabelzd2bodyzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bdbzd2blockzd2loczd2envzd2zzcgen_copz00,
		BgL_bgl_za762bdbza7d2blockza7d2880za7,
		BGl_z62bdbzd2blockzd2locz62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2570z00zzcgen_copz00,
		BgL_bgl_za762lambda2172za7622881z00, BGl_z62lambda2172z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2571z00zzcgen_copz00,
		BgL_bgl_za762lambda2178za7622882z00, BGl_z62lambda2178z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2490z00zzcgen_copz00,
		BgL_bgl_za762lambda1933za7622883z00, BGl_z62lambda1933z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2572z00zzcgen_copz00,
		BgL_bgl_za762lambda2177za7622884z00, BGl_z62lambda2177z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2491z00zzcgen_copz00,
		BgL_bgl_za762lambda1932za7622885z00, BGl_z62lambda1932z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2573z00zzcgen_copz00,
		BgL_bgl_za762lambda2183za7622886z00, BGl_z62lambda2183z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2492z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2887za7,
		BGl_z62zc3z04anonymousza31928ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2574z00zzcgen_copz00,
		BgL_bgl_za762lambda2182za7622888z00, BGl_z62lambda2182z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2493z00zzcgen_copz00,
		BgL_bgl_za762lambda1926za7622889z00, BGl_z62lambda1926z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2setz12zd2valuezd2envzc0zzcgen_copz00,
		BgL_bgl_za762cboxza7d2setza7122890za7,
		BGl_z62cboxzd2setz12zd2valuez70zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2575z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2891za7,
		BGl_z62zc3z04anonymousza32168ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2494z00zzcgen_copz00,
		BgL_bgl_za762lambda1924za7622892z00, BGl_z62lambda1924z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2576z00zzcgen_copz00,
		BgL_bgl_za762lambda2166za7622893z00, BGl_z62lambda2166z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2495z00zzcgen_copz00,
		BgL_bgl_za762lambda1948za7622894z00, BGl_z62lambda1948z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2577z00zzcgen_copz00,
		BgL_bgl_za762lambda2164za7622895z00, BGl_z62lambda2164z62zzcgen_copz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2496z00zzcgen_copz00,
		BgL_bgl_za762lambda1947za7622896z00, BGl_z62lambda1947z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2578z00zzcgen_copz00,
		BgL_bgl_za762lambda2199za7622897z00, BGl_z62lambda2199z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2497z00zzcgen_copz00,
		BgL_bgl_za762lambda1953za7622898z00, BGl_z62lambda1953z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cappzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cappza7d2nilza7b02899za7, BGl_z62cappzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2579z00zzcgen_copz00,
		BgL_bgl_za762lambda2198za7622900z00, BGl_z62lambda2198z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2498z00zzcgen_copz00,
		BgL_bgl_za762lambda1952za7622901z00, BGl_z62lambda1952z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2499z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2902za7,
		BGl_z62zc3z04anonymousza31943ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2ccastzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2ccastza72903za7, BGl_z62makezd2ccastzb0zzcgen_copz00,
		0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2propertyzd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2pr2904za7,
		BGl_z62sfunzf2Czd2propertyzd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cswitchzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cswitchza7d2loc2905z00,
		BGl_z62cswitchzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccastzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762ccastza7d2typeza72906za7, BGl_z62ccastzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2effectzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ef2907za7,
		BGl_z62sfunzf2Czd2effectz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nopzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762nopza7f3za791za7za7cg2908za7, BGl_z62nopzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csequencezd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762csequenceza7d2n2909z00, BGl_z62csequencezd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgotozd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cgotoza7d2nilza7b2910za7, BGl_z62cgotozd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2580z00zzcgen_copz00,
		BgL_bgl_za762lambda2204za7622911z00, BGl_z62lambda2204z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2581z00zzcgen_copz00,
		BgL_bgl_za762lambda2203za7622912z00, BGl_z62lambda2203z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2582z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2913za7,
		BGl_z62zc3z04anonymousza32193ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2583z00zzcgen_copz00,
		BgL_bgl_za762lambda2191za7622914z00, BGl_z62lambda2191z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2584z00zzcgen_copz00,
		BgL_bgl_za762lambda2189za7622915z00, BGl_z62lambda2189z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2585z00zzcgen_copz00,
		BgL_bgl_za762lambda2219za7622916z00, BGl_z62lambda2219z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2586z00zzcgen_copz00,
		BgL_bgl_za762lambda2218za7622917z00, BGl_z62lambda2218z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2587z00zzcgen_copz00,
		BgL_bgl_za762lambda2224za7622918z00, BGl_z62lambda2224z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2588z00zzcgen_copz00,
		BgL_bgl_za762lambda2223za7622919z00, BGl_z62lambda2223z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2589z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2920za7,
		BGl_z62zc3z04anonymousza32214ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_capplyzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762capplyza7d2type2921z00, BGl_z62capplyzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2argszd2noescapezd2setz12zd2envze0zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ar2922za7,
		BGl_z62sfunzf2Czd2argszd2noescapezd2setz12z50zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetzd2exzd2itzd2bodyzd2envz00zzcgen_copz00,
		BgL_bgl_za762csetza7d2exza7d2i2923za7,
		BGl_z62csetzd2exzd2itzd2bodyzb0zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cblockzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cblockza7d2nilza72924za7, BGl_z62cblockzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_capplyzd2funzd2envz00zzcgen_copz00,
		BgL_bgl_za762capplyza7d2funza72925za7, BGl_z62capplyzd2funzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_catomzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762catomza7d2locza7b2926za7, BGl_z62catomzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2thezd2closurezd2setz12zd2envze0zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2th2927za7,
		BGl_z62sfunzf2Czd2thezd2closurezd2setz12z50zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2effectzd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ef2928za7,
		BGl_z62sfunzf2Czd2effectzd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2topzf3zd2setz12zd2envzc1zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2to2929za7,
		BGl_z62sfunzf2Czd2topzf3zd2setz12z71zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cgotozd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cgotoza72930za7, BGl_z62makezd2cgotozb0zzcgen_copz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2csetqzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2csetqza72931za7, BGl_z62makezd2csetqzb0zzcgen_copz00,
		0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_stopzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762stopza7d2locza7d22932za7,
		BGl_z62stopzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2590z00zzcgen_copz00,
		BgL_bgl_za762lambda2212za7622933z00, BGl_z62lambda2212z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2591z00zzcgen_copz00,
		BgL_bgl_za762lambda2210za7622934z00, BGl_z62lambda2210z62zzcgen_copz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2592z00zzcgen_copz00,
		BgL_bgl_za762lambda2239za7622935z00, BGl_z62lambda2239z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2593z00zzcgen_copz00,
		BgL_bgl_za762lambda2238za7622936z00, BGl_z62lambda2238z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2594z00zzcgen_copz00,
		BgL_bgl_za762za7c3za704anonymo2937za7,
		BGl_z62zc3z04anonymousza32234ze3ze5zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_copzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762copza7d2nilza7b0za72938z00, BGl_z62copzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2595z00zzcgen_copz00,
		BgL_bgl_za762lambda2232za7622939z00, BGl_z62lambda2232z62zzcgen_copz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2596z00zzcgen_copz00,
		BgL_bgl_za762lambda2230za7622940z00, BGl_z62lambda2230z62zzcgen_copz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2597z00zzcgen_copz00,
		BgL_bgl_za762lambda2254za7622941z00, BGl_z62lambda2254z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2598z00zzcgen_copz00,
		BgL_bgl_za762lambda2253za7622942z00, BGl_z62lambda2253z62zzcgen_copz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2599z00zzcgen_copz00,
		BgL_bgl_za762lambda2259za7622943z00, BGl_z62lambda2259z62zzcgen_copz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cpragmazd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cpragmaza7d2loc2944z00,
		BGl_z62cpragmazd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2strengthzd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2st2945za7,
		BGl_z62sfunzf2Czd2strengthzd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_csequencezd2czd2expzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762csequenceza7d2c2946z00,
		BGl_z62csequencezd2czd2expzf3z91zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cmakezd2boxzd2nilzd2envzd2zzcgen_copz00,
		BgL_bgl_za762cmakeza7d2boxza7d2947za7,
		BGl_z62cmakezd2boxzd2nilz62zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cfailzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cfailza72948za7, BGl_z62makezd2cfailzb0zzcgen_copz00,
		0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cboxzd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762makeza7d2cboxza7d2949za7,
		BGl_z62makezd2cboxzd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvoidzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cvoidza7f3za791za7za72950za7, BGl_z62cvoidzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cmakezd2boxzd2envzd2zzcgen_copz00,
		BgL_bgl_za762makeza7d2cmakeza72951za7,
		BGl_z62makezd2cmakezd2boxz62zzcgen_copz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2setz12zf3zd2envze1zzcgen_copz00,
		BgL_bgl_za762cboxza7d2setza7122952za7,
		BGl_z62cboxzd2setz12zf3z51zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2dssslzd2keywordszd2envz20zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ds2953za7,
		BGl_z62sfunzf2Czd2dssslzd2keywordsz90zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cpragmazd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cpragmaza7d2typ2954z00, BGl_z62cpragmazd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2failsafezd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2fa2955za7,
		BGl_z62sfunzf2Czd2failsafezd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cpragmazf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cpragmaza7f3za7912956za7, BGl_z62cpragmazf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csequencezf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762csequenceza7f3za72957za7, BGl_z62csequencezf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgotozf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cgotoza7f3za791za7za72958za7, BGl_z62cgotozf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2thezd2closurezd2globalzd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2th2959za7,
		BGl_z62sfunzf2Czd2thezd2closurezd2globalzd2setz12z82zzcgen_copz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2nilzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ni2960za7,
		BGl_z62sfunzf2Czd2nilz42zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfailzd2proczd2envz00zzcgen_copz00,
		BgL_bgl_za762cfailza7d2procza72961za7, BGl_z62cfailzd2proczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cmakezd2boxzd2typezd2envzd2zzcgen_copz00,
		BgL_bgl_za762cmakeza7d2boxza7d2962za7,
		BGl_z62cmakezd2boxzd2typez62zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_clabelzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762clabelza7d2locza72963za7,
		BGl_z62clabelzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2varzd2varszd2envzd2zzcgen_copz00,
		BgL_bgl_za762localza7d2varza7d2964za7,
		BGl_z62localzd2varzd2varsz62zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2classzd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2cl2965za7,
		BGl_z62sfunzf2Czd2classzd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2varzd2typezd2envzd2zzcgen_copz00,
		BgL_bgl_za762localza7d2varza7d2966za7,
		BGl_z62localzd2varzd2typez62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfuncallzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cfuncallza7d2lo2967z00, BGl_z62cfuncallzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cblockzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cblockza7d2locza72968za7,
		BGl_z62cblockzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2capplyzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2capply2969z00, BGl_z62makezd2capplyzb0zzcgen_copz00,
		0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cblockzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cblock2970z00, BGl_z62makezd2cblockzb0zzcgen_copz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_creturnzd2tailzd2envz00zzcgen_copz00,
		BgL_bgl_za762creturnza7d2tai2971z00, BGl_z62creturnzd2tailzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfuncallzd2argszd2envz00zzcgen_copz00,
		BgL_bgl_za762cfuncallza7d2ar2972z00, BGl_z62cfuncallzd2argszb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_clabelzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762clabelza7d2locza72973za7, BGl_z62clabelzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_clabelzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762clabelza7d2type2974z00, BGl_z62clabelzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfailzd2msgzd2envz00zzcgen_copz00,
		BgL_bgl_za762cfailza7d2msgza7b2975za7, BGl_z62cfailzd2msgzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2refzf3zd2envzf3zzcgen_copz00,
		BgL_bgl_za762cboxza7d2refza7f32976za7, BGl_z62cboxzd2refzf3z43zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stopzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762stopza7d2nilza7b02977za7, BGl_z62stopzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cappzd2stackablezd2envz00zzcgen_copz00,
		BgL_bgl_za762cappza7d2stacka2978z00,
		BGl_z62cappzd2stackablezb0zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cmakezd2boxzd2valuezd2envzd2zzcgen_copz00,
		BgL_bgl_za762cmakeza7d2boxza7d2979za7,
		BGl_z62cmakezd2boxzd2valuez62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_clabelzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762clabelza7f3za791za72980z00, BGl_z62clabelzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfailzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cfailza7d2locza7d2981za7,
		BGl_z62cfailzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2stackablezd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2st2982za7,
		BGl_z62sfunzf2Czd2stackablez42zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_csetzd2exzd2itzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762csetza7d2exza7d2i2983za7,
		BGl_z62csetzd2exzd2itzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetqzd2valuezd2envz00zzcgen_copz00,
		BgL_bgl_za762csetqza7d2value2984z00, BGl_z62csetqzd2valuezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2varzd2loczd2envzd2zzcgen_copz00,
		BgL_bgl_za762localza7d2varza7d2985za7,
		BGl_z62localzd2varzd2locz62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccastzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762ccastza7d2nilza7b2986za7, BGl_z62ccastzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cifzd2truezd2envz00zzcgen_copz00,
		BgL_bgl_za762cifza7d2trueza7b02987za7, BGl_z62cifzd2truezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2propertyzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2pr2988za7,
		BGl_z62sfunzf2Czd2propertyz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cswitchzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cswitchza7d2loc2989z00, BGl_z62cswitchzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cvoidzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cvoidza72990za7, BGl_z62makezd2cvoidzb0zzcgen_copz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csequencezd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762csequenceza7d2t2991z00,
		BGl_z62csequencezd2typezb0zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgotozd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cgotoza7d2typeza72992za7, BGl_z62cgotozd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2topzf3zd2envz01zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2to2993za7,
		BGl_z62sfunzf2Czd2topzf3zb1zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetzd2exzd2itzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762csetza7d2exza7d2i2994za7,
		BGl_z62csetzd2exzd2itzd2loczb0zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2clabelzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2clabel2995z00, BGl_z62makezd2clabelzb0zzcgen_copz00,
		0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stopzd2valuezd2envz00zzcgen_copz00,
		BgL_bgl_za762stopza7d2valueza72996za7, BGl_z62stopzd2valuezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetzd2exzd2itzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762csetza7d2exza7d2i2997za7,
		BGl_z62csetzd2exzd2itzd2typezb0zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2loczd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2lo2998za7,
		BGl_z62sfunzf2Czd2loczd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2sfunzf2Czd2envzf2zzcgen_copz00,
		BgL_bgl_za762makeza7d2sfunza7f2999za7,
		BGl_z62makezd2sfunzf2Cz42zzcgen_copz00, 0L, BUNSPEC, 24);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nopzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762nopza7d2typeza7b03000za7, BGl_z62nopzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfailzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cfailza7f3za791za7za73001za7, BGl_z62cfailzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetqzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762csetqza7f3za791za7za73002za7, BGl_z62csetqzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetzd2exzd2itzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762csetza7d2exza7d2i3003za7,
		BGl_z62csetzd2exzd2itzf3z91zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfailzd2objzd2envz00zzcgen_copz00,
		BgL_bgl_za762cfailza7d2objza7b3004za7, BGl_z62cfailzd2objzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2predicatezd2ofzd2setz12zd2envze0zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2pr3005za7,
		BGl_z62sfunzf2Czd2predicatezd2ofzd2setz12z50zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cfuncallzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cfunca3006z00, BGl_z62makezd2cfuncallzb0zzcgen_copz00,
		0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_varczd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762varcza7d2locza7b03007za7, BGl_z62varczd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cifzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cifza7d2locza7b0za73008z00, BGl_z62cifzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bdbzd2blockzf3zd2envzf3zzcgen_copz00,
		BgL_bgl_za762bdbza7d2blockza7f3009za7,
		BGl_z62bdbzd2blockzf3z43zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2dssslzd2keywordszd2setz12zd2envze0zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ds3010za7,
		BGl_z62sfunzf2Czd2dssslzd2keywordszd2setz12z50zzcgen_copz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_copzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762copza7d2locza7d2s3011za7,
		BGl_z62copzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_capplyzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762capplyza7f3za791za73012z00, BGl_z62capplyzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2predicatezd2ofzd2envz20zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2pr3013za7,
		BGl_z62sfunzf2Czd2predicatezd2ofz90zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2creturnzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cretur3014z00, BGl_z62makezd2creturnzb0zzcgen_copz00,
		0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfuncallzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cfuncallza7d2ty3015z00, BGl_z62cfuncallzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_clabelzd2usedzf3zd2setz12zd2envz33zzcgen_copz00,
		BgL_bgl_za762clabelza7d2used3016z00,
		BGl_z62clabelzd2usedzf3zd2setz12z83zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cappzd2argszd2envz00zzcgen_copz00,
		BgL_bgl_za762cappza7d2argsza7b3017za7, BGl_z62cappzd2argszb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvoidzd2valuezd2envz00zzcgen_copz00,
		BgL_bgl_za762cvoidza7d2value3018z00, BGl_z62cvoidzd2valuezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cboxzd2refzd2envzd2zzcgen_copz00,
		BgL_bgl_za762makeza7d2cboxza7d3019za7,
		BGl_z62makezd2cboxzd2refz62zzcgen_copz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_csequencezd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762csequenceza7d2l3020z00,
		BGl_z62csequencezd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgotozd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cgotoza7d2locza7d3021za7,
		BGl_z62cgotozd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvoidzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cvoidza7d2typeza73022za7, BGl_z62cvoidzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvoidzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cvoidza7d2nilza7b3023za7, BGl_z62cvoidzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2thezd2closurezd2envz20zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2th3024za7,
		BGl_z62sfunzf2Czd2thezd2closurez90zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cifzd2falsezd2envz00zzcgen_copz00,
		BgL_bgl_za762cifza7d2falseza7b3025za7, BGl_z62cifzd2falsezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_creturnzd2valuezd2envz00zzcgen_copz00,
		BgL_bgl_za762creturnza7d2val3026z00, BGl_z62creturnzd2valuezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_ccastzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762ccastza7d2locza7d3027za7,
		BGl_z62ccastzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2keyszd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ke3028za7,
		BGl_z62sfunzf2Czd2keysz42zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cjumpzd2exzd2itzd2valuezd2envz00zzcgen_copz00,
		BgL_bgl_za762cjumpza7d2exza7d23029za7,
		BGl_z62cjumpzd2exzd2itzd2valuezb0zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfuncallzd2funzd2envz00zzcgen_copz00,
		BgL_bgl_za762cfuncallza7d2fu3030z00, BGl_z62cfuncallzd2funzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cifzd2testzd2envz00zzcgen_copz00,
		BgL_bgl_za762cifza7d2testza7b03031za7, BGl_z62cifzd2testzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cappzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cappza7f3za791za7za7c3032za7, BGl_z62cappzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_stopzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762stopza7f3za791za7za7c3033za7, BGl_z62stopzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cpragmazd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cpragmaza7d2loc3034z00, BGl_z62cpragmazd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2argszd2retescapezd2setz12zd2envze0zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ar3035za7,
		BGl_z62sfunzf2Czd2argszd2retescapezd2setz12z50zzcgen_copz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2setz12zd2loczd2envzc0zzcgen_copz00,
		BgL_bgl_za762cboxza7d2setza7123036za7,
		BGl_z62cboxzd2setz12zd2locz70zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cjumpzd2exzd2itzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cjumpza7d2exza7d23037za7,
		BGl_z62cjumpzd2exzd2itzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csequencezd2copszd2envz00zzcgen_copz00,
		BgL_bgl_za762csequenceza7d2c3038z00,
		BGl_z62csequencezd2copszb0zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cswitchzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cswitc3039z00, BGl_z62makezd2cswitchzb0zzcgen_copz00,
		0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2integratedzd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2in3040za7,
		BGl_z62sfunzf2Czd2integratedz42zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cswitchzd2testzd2envz00zzcgen_copz00,
		BgL_bgl_za762cswitchza7d2tes3041z00, BGl_z62cswitchzd2testzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfuncallzd2strengthzd2envz00zzcgen_copz00,
		BgL_bgl_za762cfuncallza7d2st3042z00,
		BGl_z62cfuncallzd2strengthzb0zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cpragmazd2formatzd2envz00zzcgen_copz00,
		BgL_bgl_za762cpragmaza7d2for3043z00,
		BGl_z62cpragmazd2formatzb0zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Czd2optionalszd2envzf2zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2op3044za7,
		BGl_z62sfunzf2Czd2optionalsz42zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2argszd2namezd2envz20zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2ar3045za7,
		BGl_z62sfunzf2Czd2argszd2namez90zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_capplyzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762capplyza7d2nilza73046za7, BGl_z62capplyzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2varzf3zd2envzf3zzcgen_copz00,
		BgL_bgl_za762localza7d2varza7f3047za7,
		BGl_z62localzd2varzf3z43zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cjumpzd2exzd2itzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cjumpza7d2exza7d23048za7,
		BGl_z62cjumpzd2exzd2itzd2typezb0zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2csequencezd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cseque3049z00,
		BGl_z62makezd2csequencezb0zzcgen_copz00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgotozd2labelzd2envz00zzcgen_copz00,
		BgL_bgl_za762cgotoza7d2label3050z00, BGl_z62cgotozd2labelzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfailzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cfailza7d2nilza7b3051za7, BGl_z62cfailzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cappzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cappza7d2locza7b03052za7, BGl_z62cappzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cjumpzd2exzd2itzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cjumpza7d2exza7d23053za7,
		BGl_z62cjumpzd2exzd2itzf3z91zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cswitchzd2clauseszd2envz00zzcgen_copz00,
		BgL_bgl_za762cswitchza7d2cla3054z00,
		BGl_z62cswitchzd2clauseszb0zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_clabelzd2bodyzd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762clabelza7d2body3055z00,
		BGl_z62clabelzd2bodyzd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2cifzd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2cifza7b03056za7, BGl_z62makezd2cifzb0zzcgen_copz00,
		0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetqzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762csetqza7d2locza7d3057za7,
		BGl_z62csetqzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csequencezd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762csequenceza7d2l3058z00, BGl_z62csequencezd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgotozd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cgotoza7d2locza7b3059za7, BGl_z62cgotozd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2bdbzd2blockzd2envzd2zzcgen_copz00,
		BgL_bgl_za762makeza7d2bdbza7d23060za7,
		BGl_z62makezd2bdbzd2blockz62zzcgen_copz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_varczf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762varcza7f3za791za7za7c3061za7, BGl_z62varczf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cblockzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762cblockza7f3za791za73062z00, BGl_z62cblockzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_copzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762copza7d2typeza7b03063za7, BGl_z62copzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_nopzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762nopza7d2locza7d2s3064za7,
		BGl_z62nopzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cappzd2typezd2envz00zzcgen_copz00,
		BgL_bgl_za762cappza7d2typeza7b3065za7, BGl_z62cappzd2typezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_catomzf3zd2envz21zzcgen_copz00,
		BgL_bgl_za762catomza7f3za791za7za73066za7, BGl_z62catomzf3z91zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_creturnzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762creturnza7d2nil3067z00, BGl_z62creturnzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2setz12zd2typezd2envzc0zzcgen_copz00,
		BgL_bgl_za762cboxza7d2setza7123068za7,
		BGl_z62cboxzd2setz12zd2typez70zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cblockzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762cblockza7d2locza73069za7, BGl_z62cblockzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2integratedzd2setz12zd2envz32zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2in3070za7,
		BGl_z62sfunzf2Czd2integratedzd2setz12z82zzcgen_copz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cjumpzd2exzd2itzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762cjumpza7d2exza7d23071za7,
		BGl_z62cjumpzd2exzd2itzd2nilzb0zzcgen_copz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bdbzd2blockzd2loczd2setz12zd2envz12zzcgen_copz00,
		BgL_bgl_za762bdbza7d2blockza7d3072za7,
		BGl_z62bdbzd2blockzd2loczd2setz12za2zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_copzd2loczd2envz00zzcgen_copz00,
		BgL_bgl_za762copza7d2locza7b0za73073z00, BGl_z62copzd2loczb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_csetzd2exzd2itzd2jumpzd2valuezd2envzd2zzcgen_copz00,
		BgL_bgl_za762csetza7d2exza7d2i3074za7,
		BGl_z62csetzd2exzd2itzd2jumpzd2valuez62zzcgen_copz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Czd2stackzd2allocatorzd2setz12zd2envze0zzcgen_copz00,
		BgL_bgl_za762sfunza7f2cza7d2st3075za7,
		BGl_z62sfunzf2Czd2stackzd2allocatorzd2setz12z50zzcgen_copz00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_nopzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762nopza7d2nilza7b0za73076z00, BGl_z62nopzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cmakezd2boxzd2loczd2envzd2zzcgen_copz00,
		BgL_bgl_za762cmakeza7d2boxza7d3077za7,
		BGl_z62cmakezd2boxzd2locz62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2refzd2nilzd2envzd2zzcgen_copz00,
		BgL_bgl_za762cboxza7d2refza7d23078za7,
		BGl_z62cboxzd2refzd2nilz62zzcgen_copz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cboxzd2refzd2varzd2envzd2zzcgen_copz00,
		BgL_bgl_za762cboxza7d2refza7d23079za7,
		BGl_z62cboxzd2refzd2varz62zzcgen_copz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetqzd2nilzd2envz00zzcgen_copz00,
		BgL_bgl_za762csetqza7d2nilza7b3080za7, BGl_z62csetqzd2nilzb0zzcgen_copz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_csetqzd2varzd2envz00zzcgen_copz00,
		BgL_bgl_za762csetqza7d2varza7b3081za7, BGl_z62csetqzd2varzb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2varczd2envz00zzcgen_copz00,
		BgL_bgl_za762makeza7d2varcza7b3082za7, BGl_z62makezd2varczb0zzcgen_copz00,
		0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cvoidzd2loczd2setz12zd2envzc0zzcgen_copz00,
		BgL_bgl_za762cvoidza7d2locza7d3083za7,
		BGl_z62cvoidzd2loczd2setz12z70zzcgen_copz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_catomzd2valuezd2envz00zzcgen_copz00,
		BgL_bgl_za762catomza7d2value3084z00, BGl_z62catomzd2valuezb0zzcgen_copz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_clabelzd2usedzf3zd2envzf3zzcgen_copz00,
		BgL_bgl_za762clabelza7d2used3085z00,
		BGl_z62clabelzd2usedzf3z43zzcgen_copz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_clabelz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cappz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cboxzd2refzd2zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_csequencez00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cgotoz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_csetzd2exzd2itz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cvoidz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_capplyz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cpragmaz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_nopz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cmakezd2boxzd2zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cifz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cboxzd2setz12zc0zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_csetqz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_localzd2varzd2zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cblockz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cswitchz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_bdbzd2blockzd2zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_copz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_catomz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_sfunzf2Czf2zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cfailz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_stopz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_creturnz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cfuncallz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_ccastz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_cjumpzd2exzd2itz00zzcgen_copz00));
		     ADD_ROOT((void *) (&BGl_varcz00zzcgen_copz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcgen_copz00(long
		BgL_checksumz00_7194, char *BgL_fromz00_7195)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcgen_copz00))
				{
					BGl_requirezd2initializa7ationz75zzcgen_copz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcgen_copz00();
					BGl_libraryzd2moduleszd2initz00zzcgen_copz00();
					BGl_cnstzd2initzd2zzcgen_copz00();
					BGl_importedzd2moduleszd2initz00zzcgen_copz00();
					BGl_objectzd2initzd2zzcgen_copz00();
					return BGl_toplevelzd2initzd2zzcgen_copz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcgen_copz00(void)
	{
		{	/* Cgen/cop.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cgen_cop");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cgen_cop");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cgen_cop");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cgen_cop");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cgen_cop");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cgen_cop");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cgen_cop");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.scm 15 */
			{	/* Cgen/cop.scm 15 */
				obj_t BgL_cportz00_6174;

				{	/* Cgen/cop.scm 15 */
					obj_t BgL_stringz00_6181;

					BgL_stringz00_6181 = BGl_string2635z00zzcgen_copz00;
					{	/* Cgen/cop.scm 15 */
						obj_t BgL_startz00_6182;

						BgL_startz00_6182 = BINT(0L);
						{	/* Cgen/cop.scm 15 */
							obj_t BgL_endz00_6183;

							BgL_endz00_6183 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6181)));
							{	/* Cgen/cop.scm 15 */

								BgL_cportz00_6174 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6181, BgL_startz00_6182, BgL_endz00_6183);
				}}}}
				{
					long BgL_iz00_6175;

					BgL_iz00_6175 = 61L;
				BgL_loopz00_6176:
					if ((BgL_iz00_6175 == -1L))
						{	/* Cgen/cop.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cgen/cop.scm 15 */
							{	/* Cgen/cop.scm 15 */
								obj_t BgL_arg2636z00_6177;

								{	/* Cgen/cop.scm 15 */

									{	/* Cgen/cop.scm 15 */
										obj_t BgL_locationz00_6179;

										BgL_locationz00_6179 = BBOOL(((bool_t) 0));
										{	/* Cgen/cop.scm 15 */

											BgL_arg2636z00_6177 =
												BGl_readz00zz__readerz00(BgL_cportz00_6174,
												BgL_locationz00_6179);
										}
									}
								}
								{	/* Cgen/cop.scm 15 */
									int BgL_tmpz00_7221;

									BgL_tmpz00_7221 = (int) (BgL_iz00_6175);
									CNST_TABLE_SET(BgL_tmpz00_7221, BgL_arg2636z00_6177);
							}}
							{	/* Cgen/cop.scm 15 */
								int BgL_auxz00_6180;

								BgL_auxz00_6180 = (int) ((BgL_iz00_6175 - 1L));
								{
									long BgL_iz00_7226;

									BgL_iz00_7226 = (long) (BgL_auxz00_6180);
									BgL_iz00_6175 = BgL_iz00_7226;
									goto BgL_loopz00_6176;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcgen_copz00(void)
	{
		{	/* Cgen/cop.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.scm 15 */
			return BUNSPEC;
		}

	}



/* make-cop */
	BGL_EXPORTED_DEF BgL_copz00_bglt BGl_makezd2copzd2zzcgen_copz00(obj_t
		BgL_loc1490z00_3, BgL_typez00_bglt BgL_type1491z00_4)
	{
		{	/* Cgen/cop.sch 382 */
			{	/* Cgen/cop.sch 382 */
				BgL_copz00_bglt BgL_new1319z00_6185;

				{	/* Cgen/cop.sch 382 */
					BgL_copz00_bglt BgL_new1317z00_6186;

					BgL_new1317z00_6186 =
						((BgL_copz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_copz00_bgl))));
					{	/* Cgen/cop.sch 382 */
						long BgL_arg1609z00_6187;

						BgL_arg1609z00_6187 = BGL_CLASS_NUM(BGl_copz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1317z00_6186), BgL_arg1609z00_6187);
					}
					BgL_new1319z00_6185 = BgL_new1317z00_6186;
				}
				((((BgL_copz00_bglt) COBJECT(BgL_new1319z00_6185))->BgL_locz00) =
					((obj_t) BgL_loc1490z00_3), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(BgL_new1319z00_6185))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1491z00_4), BUNSPEC);
				return BgL_new1319z00_6185;
			}
		}

	}



/* &make-cop */
	BgL_copz00_bglt BGl_z62makezd2copzb0zzcgen_copz00(obj_t BgL_envz00_4862,
		obj_t BgL_loc1490z00_4863, obj_t BgL_type1491z00_4864)
	{
		{	/* Cgen/cop.sch 382 */
			return
				BGl_makezd2copzd2zzcgen_copz00(BgL_loc1490z00_4863,
				((BgL_typez00_bglt) BgL_type1491z00_4864));
		}

	}



/* cop? */
	BGL_EXPORTED_DEF bool_t BGl_copzf3zf3zzcgen_copz00(obj_t BgL_objz00_5)
	{
		{	/* Cgen/cop.sch 383 */
			{	/* Cgen/cop.sch 383 */
				obj_t BgL_classz00_6188;

				BgL_classz00_6188 = BGl_copz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_5))
					{	/* Cgen/cop.sch 383 */
						BgL_objectz00_bglt BgL_arg1807z00_6189;

						BgL_arg1807z00_6189 = (BgL_objectz00_bglt) (BgL_objz00_5);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 383 */
								long BgL_idxz00_6190;

								BgL_idxz00_6190 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6189);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6190 + 1L)) == BgL_classz00_6188);
							}
						else
							{	/* Cgen/cop.sch 383 */
								bool_t BgL_res2415z00_6193;

								{	/* Cgen/cop.sch 383 */
									obj_t BgL_oclassz00_6194;

									{	/* Cgen/cop.sch 383 */
										obj_t BgL_arg1815z00_6195;
										long BgL_arg1816z00_6196;

										BgL_arg1815z00_6195 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 383 */
											long BgL_arg1817z00_6197;

											BgL_arg1817z00_6197 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6189);
											BgL_arg1816z00_6196 = (BgL_arg1817z00_6197 - OBJECT_TYPE);
										}
										BgL_oclassz00_6194 =
											VECTOR_REF(BgL_arg1815z00_6195, BgL_arg1816z00_6196);
									}
									{	/* Cgen/cop.sch 383 */
										bool_t BgL__ortest_1115z00_6198;

										BgL__ortest_1115z00_6198 =
											(BgL_classz00_6188 == BgL_oclassz00_6194);
										if (BgL__ortest_1115z00_6198)
											{	/* Cgen/cop.sch 383 */
												BgL_res2415z00_6193 = BgL__ortest_1115z00_6198;
											}
										else
											{	/* Cgen/cop.sch 383 */
												long BgL_odepthz00_6199;

												{	/* Cgen/cop.sch 383 */
													obj_t BgL_arg1804z00_6200;

													BgL_arg1804z00_6200 = (BgL_oclassz00_6194);
													BgL_odepthz00_6199 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6200);
												}
												if ((1L < BgL_odepthz00_6199))
													{	/* Cgen/cop.sch 383 */
														obj_t BgL_arg1802z00_6201;

														{	/* Cgen/cop.sch 383 */
															obj_t BgL_arg1803z00_6202;

															BgL_arg1803z00_6202 = (BgL_oclassz00_6194);
															BgL_arg1802z00_6201 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6202,
																1L);
														}
														BgL_res2415z00_6193 =
															(BgL_arg1802z00_6201 == BgL_classz00_6188);
													}
												else
													{	/* Cgen/cop.sch 383 */
														BgL_res2415z00_6193 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2415z00_6193;
							}
					}
				else
					{	/* Cgen/cop.sch 383 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cop? */
	obj_t BGl_z62copzf3z91zzcgen_copz00(obj_t BgL_envz00_4865,
		obj_t BgL_objz00_4866)
	{
		{	/* Cgen/cop.sch 383 */
			return BBOOL(BGl_copzf3zf3zzcgen_copz00(BgL_objz00_4866));
		}

	}



/* cop-nil */
	BGL_EXPORTED_DEF BgL_copz00_bglt BGl_copzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 384 */
			{	/* Cgen/cop.sch 384 */
				obj_t BgL_classz00_3346;

				BgL_classz00_3346 = BGl_copz00zzcgen_copz00;
				{	/* Cgen/cop.sch 384 */
					obj_t BgL__ortest_1117z00_3347;

					BgL__ortest_1117z00_3347 = BGL_CLASS_NIL(BgL_classz00_3346);
					if (CBOOL(BgL__ortest_1117z00_3347))
						{	/* Cgen/cop.sch 384 */
							return ((BgL_copz00_bglt) BgL__ortest_1117z00_3347);
						}
					else
						{	/* Cgen/cop.sch 384 */
							return
								((BgL_copz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3346));
						}
				}
			}
		}

	}



/* &cop-nil */
	BgL_copz00_bglt BGl_z62copzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_4867)
	{
		{	/* Cgen/cop.sch 384 */
			return BGl_copzd2nilzd2zzcgen_copz00();
		}

	}



/* cop-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_copzd2typezd2zzcgen_copz00(BgL_copz00_bglt BgL_oz00_6)
	{
		{	/* Cgen/cop.sch 385 */
			return (((BgL_copz00_bglt) COBJECT(BgL_oz00_6))->BgL_typez00);
		}

	}



/* &cop-type */
	BgL_typez00_bglt BGl_z62copzd2typezb0zzcgen_copz00(obj_t BgL_envz00_4868,
		obj_t BgL_oz00_4869)
	{
		{	/* Cgen/cop.sch 385 */
			return BGl_copzd2typezd2zzcgen_copz00(((BgL_copz00_bglt) BgL_oz00_4869));
		}

	}



/* cop-loc */
	BGL_EXPORTED_DEF obj_t BGl_copzd2loczd2zzcgen_copz00(BgL_copz00_bglt
		BgL_oz00_9)
	{
		{	/* Cgen/cop.sch 387 */
			return (((BgL_copz00_bglt) COBJECT(BgL_oz00_9))->BgL_locz00);
		}

	}



/* &cop-loc */
	obj_t BGl_z62copzd2loczb0zzcgen_copz00(obj_t BgL_envz00_4870,
		obj_t BgL_oz00_4871)
	{
		{	/* Cgen/cop.sch 387 */
			return BGl_copzd2loczd2zzcgen_copz00(((BgL_copz00_bglt) BgL_oz00_4871));
		}

	}



/* cop-loc-set! */
	BGL_EXPORTED_DEF obj_t BGl_copzd2loczd2setz12z12zzcgen_copz00(BgL_copz00_bglt
		BgL_oz00_10, obj_t BgL_vz00_11)
	{
		{	/* Cgen/cop.sch 388 */
			return
				((((BgL_copz00_bglt) COBJECT(BgL_oz00_10))->BgL_locz00) =
				((obj_t) BgL_vz00_11), BUNSPEC);
		}

	}



/* &cop-loc-set! */
	obj_t BGl_z62copzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4872,
		obj_t BgL_oz00_4873, obj_t BgL_vz00_4874)
	{
		{	/* Cgen/cop.sch 388 */
			return
				BGl_copzd2loczd2setz12z12zzcgen_copz00(
				((BgL_copz00_bglt) BgL_oz00_4873), BgL_vz00_4874);
		}

	}



/* make-clabel */
	BGL_EXPORTED_DEF BgL_clabelz00_bglt BGl_makezd2clabelzd2zzcgen_copz00(obj_t
		BgL_loc1484z00_12, BgL_typez00_bglt BgL_type1485z00_13,
		obj_t BgL_name1486z00_14, bool_t BgL_usedzf31487zf3_15,
		obj_t BgL_body1488z00_16)
	{
		{	/* Cgen/cop.sch 391 */
			{	/* Cgen/cop.sch 391 */
				BgL_clabelz00_bglt BgL_new1321z00_6203;

				{	/* Cgen/cop.sch 391 */
					BgL_clabelz00_bglt BgL_new1320z00_6204;

					BgL_new1320z00_6204 =
						((BgL_clabelz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_clabelz00_bgl))));
					{	/* Cgen/cop.sch 391 */
						long BgL_arg1611z00_6205;

						BgL_arg1611z00_6205 = BGL_CLASS_NUM(BGl_clabelz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1320z00_6204), BgL_arg1611z00_6205);
					}
					BgL_new1321z00_6203 = BgL_new1320z00_6204;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1321z00_6203)))->BgL_locz00) =
					((obj_t) BgL_loc1484z00_12), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1321z00_6203)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1485z00_13), BUNSPEC);
				((((BgL_clabelz00_bglt) COBJECT(BgL_new1321z00_6203))->BgL_namez00) =
					((obj_t) BgL_name1486z00_14), BUNSPEC);
				((((BgL_clabelz00_bglt) COBJECT(BgL_new1321z00_6203))->BgL_usedzf3zf3) =
					((bool_t) BgL_usedzf31487zf3_15), BUNSPEC);
				((((BgL_clabelz00_bglt) COBJECT(BgL_new1321z00_6203))->BgL_bodyz00) =
					((obj_t) BgL_body1488z00_16), BUNSPEC);
				return BgL_new1321z00_6203;
			}
		}

	}



/* &make-clabel */
	BgL_clabelz00_bglt BGl_z62makezd2clabelzb0zzcgen_copz00(obj_t BgL_envz00_4875,
		obj_t BgL_loc1484z00_4876, obj_t BgL_type1485z00_4877,
		obj_t BgL_name1486z00_4878, obj_t BgL_usedzf31487zf3_4879,
		obj_t BgL_body1488z00_4880)
	{
		{	/* Cgen/cop.sch 391 */
			return
				BGl_makezd2clabelzd2zzcgen_copz00(BgL_loc1484z00_4876,
				((BgL_typez00_bglt) BgL_type1485z00_4877), BgL_name1486z00_4878,
				CBOOL(BgL_usedzf31487zf3_4879), BgL_body1488z00_4880);
		}

	}



/* clabel? */
	BGL_EXPORTED_DEF bool_t BGl_clabelzf3zf3zzcgen_copz00(obj_t BgL_objz00_17)
	{
		{	/* Cgen/cop.sch 392 */
			{	/* Cgen/cop.sch 392 */
				obj_t BgL_classz00_6206;

				BgL_classz00_6206 = BGl_clabelz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_17))
					{	/* Cgen/cop.sch 392 */
						BgL_objectz00_bglt BgL_arg1807z00_6207;

						BgL_arg1807z00_6207 = (BgL_objectz00_bglt) (BgL_objz00_17);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 392 */
								long BgL_idxz00_6208;

								BgL_idxz00_6208 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6207);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6208 + 2L)) == BgL_classz00_6206);
							}
						else
							{	/* Cgen/cop.sch 392 */
								bool_t BgL_res2416z00_6211;

								{	/* Cgen/cop.sch 392 */
									obj_t BgL_oclassz00_6212;

									{	/* Cgen/cop.sch 392 */
										obj_t BgL_arg1815z00_6213;
										long BgL_arg1816z00_6214;

										BgL_arg1815z00_6213 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 392 */
											long BgL_arg1817z00_6215;

											BgL_arg1817z00_6215 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6207);
											BgL_arg1816z00_6214 = (BgL_arg1817z00_6215 - OBJECT_TYPE);
										}
										BgL_oclassz00_6212 =
											VECTOR_REF(BgL_arg1815z00_6213, BgL_arg1816z00_6214);
									}
									{	/* Cgen/cop.sch 392 */
										bool_t BgL__ortest_1115z00_6216;

										BgL__ortest_1115z00_6216 =
											(BgL_classz00_6206 == BgL_oclassz00_6212);
										if (BgL__ortest_1115z00_6216)
											{	/* Cgen/cop.sch 392 */
												BgL_res2416z00_6211 = BgL__ortest_1115z00_6216;
											}
										else
											{	/* Cgen/cop.sch 392 */
												long BgL_odepthz00_6217;

												{	/* Cgen/cop.sch 392 */
													obj_t BgL_arg1804z00_6218;

													BgL_arg1804z00_6218 = (BgL_oclassz00_6212);
													BgL_odepthz00_6217 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6218);
												}
												if ((2L < BgL_odepthz00_6217))
													{	/* Cgen/cop.sch 392 */
														obj_t BgL_arg1802z00_6219;

														{	/* Cgen/cop.sch 392 */
															obj_t BgL_arg1803z00_6220;

															BgL_arg1803z00_6220 = (BgL_oclassz00_6212);
															BgL_arg1802z00_6219 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6220,
																2L);
														}
														BgL_res2416z00_6211 =
															(BgL_arg1802z00_6219 == BgL_classz00_6206);
													}
												else
													{	/* Cgen/cop.sch 392 */
														BgL_res2416z00_6211 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2416z00_6211;
							}
					}
				else
					{	/* Cgen/cop.sch 392 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &clabel? */
	obj_t BGl_z62clabelzf3z91zzcgen_copz00(obj_t BgL_envz00_4881,
		obj_t BgL_objz00_4882)
	{
		{	/* Cgen/cop.sch 392 */
			return BBOOL(BGl_clabelzf3zf3zzcgen_copz00(BgL_objz00_4882));
		}

	}



/* clabel-nil */
	BGL_EXPORTED_DEF BgL_clabelz00_bglt BGl_clabelzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 393 */
			{	/* Cgen/cop.sch 393 */
				obj_t BgL_classz00_3388;

				BgL_classz00_3388 = BGl_clabelz00zzcgen_copz00;
				{	/* Cgen/cop.sch 393 */
					obj_t BgL__ortest_1117z00_3389;

					BgL__ortest_1117z00_3389 = BGL_CLASS_NIL(BgL_classz00_3388);
					if (CBOOL(BgL__ortest_1117z00_3389))
						{	/* Cgen/cop.sch 393 */
							return ((BgL_clabelz00_bglt) BgL__ortest_1117z00_3389);
						}
					else
						{	/* Cgen/cop.sch 393 */
							return
								((BgL_clabelz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3388));
						}
				}
			}
		}

	}



/* &clabel-nil */
	BgL_clabelz00_bglt BGl_z62clabelzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_4883)
	{
		{	/* Cgen/cop.sch 393 */
			return BGl_clabelzd2nilzd2zzcgen_copz00();
		}

	}



/* clabel-body */
	BGL_EXPORTED_DEF obj_t BGl_clabelzd2bodyzd2zzcgen_copz00(BgL_clabelz00_bglt
		BgL_oz00_18)
	{
		{	/* Cgen/cop.sch 394 */
			return (((BgL_clabelz00_bglt) COBJECT(BgL_oz00_18))->BgL_bodyz00);
		}

	}



/* &clabel-body */
	obj_t BGl_z62clabelzd2bodyzb0zzcgen_copz00(obj_t BgL_envz00_4884,
		obj_t BgL_oz00_4885)
	{
		{	/* Cgen/cop.sch 394 */
			return
				BGl_clabelzd2bodyzd2zzcgen_copz00(((BgL_clabelz00_bglt) BgL_oz00_4885));
		}

	}



/* clabel-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_clabelzd2bodyzd2setz12z12zzcgen_copz00(BgL_clabelz00_bglt BgL_oz00_19,
		obj_t BgL_vz00_20)
	{
		{	/* Cgen/cop.sch 395 */
			return
				((((BgL_clabelz00_bglt) COBJECT(BgL_oz00_19))->BgL_bodyz00) =
				((obj_t) BgL_vz00_20), BUNSPEC);
		}

	}



/* &clabel-body-set! */
	obj_t BGl_z62clabelzd2bodyzd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4886,
		obj_t BgL_oz00_4887, obj_t BgL_vz00_4888)
	{
		{	/* Cgen/cop.sch 395 */
			return
				BGl_clabelzd2bodyzd2setz12z12zzcgen_copz00(
				((BgL_clabelz00_bglt) BgL_oz00_4887), BgL_vz00_4888);
		}

	}



/* clabel-used? */
	BGL_EXPORTED_DEF bool_t
		BGl_clabelzd2usedzf3z21zzcgen_copz00(BgL_clabelz00_bglt BgL_oz00_21)
	{
		{	/* Cgen/cop.sch 396 */
			return (((BgL_clabelz00_bglt) COBJECT(BgL_oz00_21))->BgL_usedzf3zf3);
		}

	}



/* &clabel-used? */
	obj_t BGl_z62clabelzd2usedzf3z43zzcgen_copz00(obj_t BgL_envz00_4889,
		obj_t BgL_oz00_4890)
	{
		{	/* Cgen/cop.sch 396 */
			return
				BBOOL(BGl_clabelzd2usedzf3z21zzcgen_copz00(
					((BgL_clabelz00_bglt) BgL_oz00_4890)));
		}

	}



/* clabel-used?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_clabelzd2usedzf3zd2setz12ze1zzcgen_copz00(BgL_clabelz00_bglt
		BgL_oz00_22, bool_t BgL_vz00_23)
	{
		{	/* Cgen/cop.sch 397 */
			return
				((((BgL_clabelz00_bglt) COBJECT(BgL_oz00_22))->BgL_usedzf3zf3) =
				((bool_t) BgL_vz00_23), BUNSPEC);
		}

	}



/* &clabel-used?-set! */
	obj_t BGl_z62clabelzd2usedzf3zd2setz12z83zzcgen_copz00(obj_t BgL_envz00_4891,
		obj_t BgL_oz00_4892, obj_t BgL_vz00_4893)
	{
		{	/* Cgen/cop.sch 397 */
			return
				BGl_clabelzd2usedzf3zd2setz12ze1zzcgen_copz00(
				((BgL_clabelz00_bglt) BgL_oz00_4892), CBOOL(BgL_vz00_4893));
		}

	}



/* clabel-name */
	BGL_EXPORTED_DEF obj_t BGl_clabelzd2namezd2zzcgen_copz00(BgL_clabelz00_bglt
		BgL_oz00_24)
	{
		{	/* Cgen/cop.sch 398 */
			return (((BgL_clabelz00_bglt) COBJECT(BgL_oz00_24))->BgL_namez00);
		}

	}



/* &clabel-name */
	obj_t BGl_z62clabelzd2namezb0zzcgen_copz00(obj_t BgL_envz00_4894,
		obj_t BgL_oz00_4895)
	{
		{	/* Cgen/cop.sch 398 */
			return
				BGl_clabelzd2namezd2zzcgen_copz00(((BgL_clabelz00_bglt) BgL_oz00_4895));
		}

	}



/* clabel-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_clabelzd2typezd2zzcgen_copz00(BgL_clabelz00_bglt BgL_oz00_27)
	{
		{	/* Cgen/cop.sch 400 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_27)))->BgL_typez00);
		}

	}



/* &clabel-type */
	BgL_typez00_bglt BGl_z62clabelzd2typezb0zzcgen_copz00(obj_t BgL_envz00_4896,
		obj_t BgL_oz00_4897)
	{
		{	/* Cgen/cop.sch 400 */
			return
				BGl_clabelzd2typezd2zzcgen_copz00(((BgL_clabelz00_bglt) BgL_oz00_4897));
		}

	}



/* clabel-loc */
	BGL_EXPORTED_DEF obj_t BGl_clabelzd2loczd2zzcgen_copz00(BgL_clabelz00_bglt
		BgL_oz00_30)
	{
		{	/* Cgen/cop.sch 402 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_30)))->BgL_locz00);
		}

	}



/* &clabel-loc */
	obj_t BGl_z62clabelzd2loczb0zzcgen_copz00(obj_t BgL_envz00_4898,
		obj_t BgL_oz00_4899)
	{
		{	/* Cgen/cop.sch 402 */
			return
				BGl_clabelzd2loczd2zzcgen_copz00(((BgL_clabelz00_bglt) BgL_oz00_4899));
		}

	}



/* clabel-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_clabelzd2loczd2setz12z12zzcgen_copz00(BgL_clabelz00_bglt BgL_oz00_31,
		obj_t BgL_vz00_32)
	{
		{	/* Cgen/cop.sch 403 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_31)))->BgL_locz00) =
				((obj_t) BgL_vz00_32), BUNSPEC);
		}

	}



/* &clabel-loc-set! */
	obj_t BGl_z62clabelzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4900,
		obj_t BgL_oz00_4901, obj_t BgL_vz00_4902)
	{
		{	/* Cgen/cop.sch 403 */
			return
				BGl_clabelzd2loczd2setz12z12zzcgen_copz00(
				((BgL_clabelz00_bglt) BgL_oz00_4901), BgL_vz00_4902);
		}

	}



/* make-cgoto */
	BGL_EXPORTED_DEF BgL_cgotoz00_bglt BGl_makezd2cgotozd2zzcgen_copz00(obj_t
		BgL_loc1480z00_33, BgL_typez00_bglt BgL_type1481z00_34,
		BgL_clabelz00_bglt BgL_label1482z00_35)
	{
		{	/* Cgen/cop.sch 406 */
			{	/* Cgen/cop.sch 406 */
				BgL_cgotoz00_bglt BgL_new1323z00_6221;

				{	/* Cgen/cop.sch 406 */
					BgL_cgotoz00_bglt BgL_new1322z00_6222;

					BgL_new1322z00_6222 =
						((BgL_cgotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cgotoz00_bgl))));
					{	/* Cgen/cop.sch 406 */
						long BgL_arg1613z00_6223;

						BgL_arg1613z00_6223 = BGL_CLASS_NUM(BGl_cgotoz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1322z00_6222), BgL_arg1613z00_6223);
					}
					BgL_new1323z00_6221 = BgL_new1322z00_6222;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1323z00_6221)))->BgL_locz00) =
					((obj_t) BgL_loc1480z00_33), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1323z00_6221)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1481z00_34), BUNSPEC);
				((((BgL_cgotoz00_bglt) COBJECT(BgL_new1323z00_6221))->BgL_labelz00) =
					((BgL_clabelz00_bglt) BgL_label1482z00_35), BUNSPEC);
				return BgL_new1323z00_6221;
			}
		}

	}



/* &make-cgoto */
	BgL_cgotoz00_bglt BGl_z62makezd2cgotozb0zzcgen_copz00(obj_t BgL_envz00_4903,
		obj_t BgL_loc1480z00_4904, obj_t BgL_type1481z00_4905,
		obj_t BgL_label1482z00_4906)
	{
		{	/* Cgen/cop.sch 406 */
			return
				BGl_makezd2cgotozd2zzcgen_copz00(BgL_loc1480z00_4904,
				((BgL_typez00_bglt) BgL_type1481z00_4905),
				((BgL_clabelz00_bglt) BgL_label1482z00_4906));
		}

	}



/* cgoto? */
	BGL_EXPORTED_DEF bool_t BGl_cgotozf3zf3zzcgen_copz00(obj_t BgL_objz00_36)
	{
		{	/* Cgen/cop.sch 407 */
			{	/* Cgen/cop.sch 407 */
				obj_t BgL_classz00_6224;

				BgL_classz00_6224 = BGl_cgotoz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_36))
					{	/* Cgen/cop.sch 407 */
						BgL_objectz00_bglt BgL_arg1807z00_6225;

						BgL_arg1807z00_6225 = (BgL_objectz00_bglt) (BgL_objz00_36);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 407 */
								long BgL_idxz00_6226;

								BgL_idxz00_6226 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6225);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6226 + 2L)) == BgL_classz00_6224);
							}
						else
							{	/* Cgen/cop.sch 407 */
								bool_t BgL_res2417z00_6229;

								{	/* Cgen/cop.sch 407 */
									obj_t BgL_oclassz00_6230;

									{	/* Cgen/cop.sch 407 */
										obj_t BgL_arg1815z00_6231;
										long BgL_arg1816z00_6232;

										BgL_arg1815z00_6231 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 407 */
											long BgL_arg1817z00_6233;

											BgL_arg1817z00_6233 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6225);
											BgL_arg1816z00_6232 = (BgL_arg1817z00_6233 - OBJECT_TYPE);
										}
										BgL_oclassz00_6230 =
											VECTOR_REF(BgL_arg1815z00_6231, BgL_arg1816z00_6232);
									}
									{	/* Cgen/cop.sch 407 */
										bool_t BgL__ortest_1115z00_6234;

										BgL__ortest_1115z00_6234 =
											(BgL_classz00_6224 == BgL_oclassz00_6230);
										if (BgL__ortest_1115z00_6234)
											{	/* Cgen/cop.sch 407 */
												BgL_res2417z00_6229 = BgL__ortest_1115z00_6234;
											}
										else
											{	/* Cgen/cop.sch 407 */
												long BgL_odepthz00_6235;

												{	/* Cgen/cop.sch 407 */
													obj_t BgL_arg1804z00_6236;

													BgL_arg1804z00_6236 = (BgL_oclassz00_6230);
													BgL_odepthz00_6235 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6236);
												}
												if ((2L < BgL_odepthz00_6235))
													{	/* Cgen/cop.sch 407 */
														obj_t BgL_arg1802z00_6237;

														{	/* Cgen/cop.sch 407 */
															obj_t BgL_arg1803z00_6238;

															BgL_arg1803z00_6238 = (BgL_oclassz00_6230);
															BgL_arg1802z00_6237 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6238,
																2L);
														}
														BgL_res2417z00_6229 =
															(BgL_arg1802z00_6237 == BgL_classz00_6224);
													}
												else
													{	/* Cgen/cop.sch 407 */
														BgL_res2417z00_6229 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2417z00_6229;
							}
					}
				else
					{	/* Cgen/cop.sch 407 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cgoto? */
	obj_t BGl_z62cgotozf3z91zzcgen_copz00(obj_t BgL_envz00_4907,
		obj_t BgL_objz00_4908)
	{
		{	/* Cgen/cop.sch 407 */
			return BBOOL(BGl_cgotozf3zf3zzcgen_copz00(BgL_objz00_4908));
		}

	}



/* cgoto-nil */
	BGL_EXPORTED_DEF BgL_cgotoz00_bglt BGl_cgotozd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 408 */
			{	/* Cgen/cop.sch 408 */
				obj_t BgL_classz00_3430;

				BgL_classz00_3430 = BGl_cgotoz00zzcgen_copz00;
				{	/* Cgen/cop.sch 408 */
					obj_t BgL__ortest_1117z00_3431;

					BgL__ortest_1117z00_3431 = BGL_CLASS_NIL(BgL_classz00_3430);
					if (CBOOL(BgL__ortest_1117z00_3431))
						{	/* Cgen/cop.sch 408 */
							return ((BgL_cgotoz00_bglt) BgL__ortest_1117z00_3431);
						}
					else
						{	/* Cgen/cop.sch 408 */
							return
								((BgL_cgotoz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3430));
						}
				}
			}
		}

	}



/* &cgoto-nil */
	BgL_cgotoz00_bglt BGl_z62cgotozd2nilzb0zzcgen_copz00(obj_t BgL_envz00_4909)
	{
		{	/* Cgen/cop.sch 408 */
			return BGl_cgotozd2nilzd2zzcgen_copz00();
		}

	}



/* cgoto-label */
	BGL_EXPORTED_DEF BgL_clabelz00_bglt
		BGl_cgotozd2labelzd2zzcgen_copz00(BgL_cgotoz00_bglt BgL_oz00_37)
	{
		{	/* Cgen/cop.sch 409 */
			return (((BgL_cgotoz00_bglt) COBJECT(BgL_oz00_37))->BgL_labelz00);
		}

	}



/* &cgoto-label */
	BgL_clabelz00_bglt BGl_z62cgotozd2labelzb0zzcgen_copz00(obj_t BgL_envz00_4910,
		obj_t BgL_oz00_4911)
	{
		{	/* Cgen/cop.sch 409 */
			return
				BGl_cgotozd2labelzd2zzcgen_copz00(((BgL_cgotoz00_bglt) BgL_oz00_4911));
		}

	}



/* cgoto-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cgotozd2typezd2zzcgen_copz00(BgL_cgotoz00_bglt BgL_oz00_40)
	{
		{	/* Cgen/cop.sch 411 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_40)))->BgL_typez00);
		}

	}



/* &cgoto-type */
	BgL_typez00_bglt BGl_z62cgotozd2typezb0zzcgen_copz00(obj_t BgL_envz00_4912,
		obj_t BgL_oz00_4913)
	{
		{	/* Cgen/cop.sch 411 */
			return
				BGl_cgotozd2typezd2zzcgen_copz00(((BgL_cgotoz00_bglt) BgL_oz00_4913));
		}

	}



/* cgoto-loc */
	BGL_EXPORTED_DEF obj_t BGl_cgotozd2loczd2zzcgen_copz00(BgL_cgotoz00_bglt
		BgL_oz00_43)
	{
		{	/* Cgen/cop.sch 413 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_43)))->BgL_locz00);
		}

	}



/* &cgoto-loc */
	obj_t BGl_z62cgotozd2loczb0zzcgen_copz00(obj_t BgL_envz00_4914,
		obj_t BgL_oz00_4915)
	{
		{	/* Cgen/cop.sch 413 */
			return
				BGl_cgotozd2loczd2zzcgen_copz00(((BgL_cgotoz00_bglt) BgL_oz00_4915));
		}

	}



/* cgoto-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cgotozd2loczd2setz12z12zzcgen_copz00(BgL_cgotoz00_bglt BgL_oz00_44,
		obj_t BgL_vz00_45)
	{
		{	/* Cgen/cop.sch 414 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_44)))->BgL_locz00) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &cgoto-loc-set! */
	obj_t BGl_z62cgotozd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4916,
		obj_t BgL_oz00_4917, obj_t BgL_vz00_4918)
	{
		{	/* Cgen/cop.sch 414 */
			return
				BGl_cgotozd2loczd2setz12z12zzcgen_copz00(
				((BgL_cgotoz00_bglt) BgL_oz00_4917), BgL_vz00_4918);
		}

	}



/* make-cblock */
	BGL_EXPORTED_DEF BgL_cblockz00_bglt BGl_makezd2cblockzd2zzcgen_copz00(obj_t
		BgL_loc1476z00_46, BgL_typez00_bglt BgL_type1477z00_47,
		BgL_copz00_bglt BgL_body1478z00_48)
	{
		{	/* Cgen/cop.sch 417 */
			{	/* Cgen/cop.sch 417 */
				BgL_cblockz00_bglt BgL_new1325z00_6239;

				{	/* Cgen/cop.sch 417 */
					BgL_cblockz00_bglt BgL_new1324z00_6240;

					BgL_new1324z00_6240 =
						((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cblockz00_bgl))));
					{	/* Cgen/cop.sch 417 */
						long BgL_arg1615z00_6241;

						BgL_arg1615z00_6241 = BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1324z00_6240), BgL_arg1615z00_6241);
					}
					BgL_new1325z00_6239 = BgL_new1324z00_6240;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1325z00_6239)))->BgL_locz00) =
					((obj_t) BgL_loc1476z00_46), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1325z00_6239)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1477z00_47), BUNSPEC);
				((((BgL_cblockz00_bglt) COBJECT(BgL_new1325z00_6239))->BgL_bodyz00) =
					((BgL_copz00_bglt) BgL_body1478z00_48), BUNSPEC);
				return BgL_new1325z00_6239;
			}
		}

	}



/* &make-cblock */
	BgL_cblockz00_bglt BGl_z62makezd2cblockzb0zzcgen_copz00(obj_t BgL_envz00_4919,
		obj_t BgL_loc1476z00_4920, obj_t BgL_type1477z00_4921,
		obj_t BgL_body1478z00_4922)
	{
		{	/* Cgen/cop.sch 417 */
			return
				BGl_makezd2cblockzd2zzcgen_copz00(BgL_loc1476z00_4920,
				((BgL_typez00_bglt) BgL_type1477z00_4921),
				((BgL_copz00_bglt) BgL_body1478z00_4922));
		}

	}



/* cblock? */
	BGL_EXPORTED_DEF bool_t BGl_cblockzf3zf3zzcgen_copz00(obj_t BgL_objz00_49)
	{
		{	/* Cgen/cop.sch 418 */
			{	/* Cgen/cop.sch 418 */
				obj_t BgL_classz00_6242;

				BgL_classz00_6242 = BGl_cblockz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_49))
					{	/* Cgen/cop.sch 418 */
						BgL_objectz00_bglt BgL_arg1807z00_6243;

						BgL_arg1807z00_6243 = (BgL_objectz00_bglt) (BgL_objz00_49);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 418 */
								long BgL_idxz00_6244;

								BgL_idxz00_6244 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6243);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6244 + 2L)) == BgL_classz00_6242);
							}
						else
							{	/* Cgen/cop.sch 418 */
								bool_t BgL_res2418z00_6247;

								{	/* Cgen/cop.sch 418 */
									obj_t BgL_oclassz00_6248;

									{	/* Cgen/cop.sch 418 */
										obj_t BgL_arg1815z00_6249;
										long BgL_arg1816z00_6250;

										BgL_arg1815z00_6249 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 418 */
											long BgL_arg1817z00_6251;

											BgL_arg1817z00_6251 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6243);
											BgL_arg1816z00_6250 = (BgL_arg1817z00_6251 - OBJECT_TYPE);
										}
										BgL_oclassz00_6248 =
											VECTOR_REF(BgL_arg1815z00_6249, BgL_arg1816z00_6250);
									}
									{	/* Cgen/cop.sch 418 */
										bool_t BgL__ortest_1115z00_6252;

										BgL__ortest_1115z00_6252 =
											(BgL_classz00_6242 == BgL_oclassz00_6248);
										if (BgL__ortest_1115z00_6252)
											{	/* Cgen/cop.sch 418 */
												BgL_res2418z00_6247 = BgL__ortest_1115z00_6252;
											}
										else
											{	/* Cgen/cop.sch 418 */
												long BgL_odepthz00_6253;

												{	/* Cgen/cop.sch 418 */
													obj_t BgL_arg1804z00_6254;

													BgL_arg1804z00_6254 = (BgL_oclassz00_6248);
													BgL_odepthz00_6253 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6254);
												}
												if ((2L < BgL_odepthz00_6253))
													{	/* Cgen/cop.sch 418 */
														obj_t BgL_arg1802z00_6255;

														{	/* Cgen/cop.sch 418 */
															obj_t BgL_arg1803z00_6256;

															BgL_arg1803z00_6256 = (BgL_oclassz00_6248);
															BgL_arg1802z00_6255 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6256,
																2L);
														}
														BgL_res2418z00_6247 =
															(BgL_arg1802z00_6255 == BgL_classz00_6242);
													}
												else
													{	/* Cgen/cop.sch 418 */
														BgL_res2418z00_6247 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2418z00_6247;
							}
					}
				else
					{	/* Cgen/cop.sch 418 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cblock? */
	obj_t BGl_z62cblockzf3z91zzcgen_copz00(obj_t BgL_envz00_4923,
		obj_t BgL_objz00_4924)
	{
		{	/* Cgen/cop.sch 418 */
			return BBOOL(BGl_cblockzf3zf3zzcgen_copz00(BgL_objz00_4924));
		}

	}



/* cblock-nil */
	BGL_EXPORTED_DEF BgL_cblockz00_bglt BGl_cblockzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 419 */
			{	/* Cgen/cop.sch 419 */
				obj_t BgL_classz00_3472;

				BgL_classz00_3472 = BGl_cblockz00zzcgen_copz00;
				{	/* Cgen/cop.sch 419 */
					obj_t BgL__ortest_1117z00_3473;

					BgL__ortest_1117z00_3473 = BGL_CLASS_NIL(BgL_classz00_3472);
					if (CBOOL(BgL__ortest_1117z00_3473))
						{	/* Cgen/cop.sch 419 */
							return ((BgL_cblockz00_bglt) BgL__ortest_1117z00_3473);
						}
					else
						{	/* Cgen/cop.sch 419 */
							return
								((BgL_cblockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3472));
						}
				}
			}
		}

	}



/* &cblock-nil */
	BgL_cblockz00_bglt BGl_z62cblockzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_4925)
	{
		{	/* Cgen/cop.sch 419 */
			return BGl_cblockzd2nilzd2zzcgen_copz00();
		}

	}



/* cblock-body */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cblockzd2bodyzd2zzcgen_copz00(BgL_cblockz00_bglt BgL_oz00_50)
	{
		{	/* Cgen/cop.sch 420 */
			return (((BgL_cblockz00_bglt) COBJECT(BgL_oz00_50))->BgL_bodyz00);
		}

	}



/* &cblock-body */
	BgL_copz00_bglt BGl_z62cblockzd2bodyzb0zzcgen_copz00(obj_t BgL_envz00_4926,
		obj_t BgL_oz00_4927)
	{
		{	/* Cgen/cop.sch 420 */
			return
				BGl_cblockzd2bodyzd2zzcgen_copz00(((BgL_cblockz00_bglt) BgL_oz00_4927));
		}

	}



/* cblock-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cblockzd2typezd2zzcgen_copz00(BgL_cblockz00_bglt BgL_oz00_53)
	{
		{	/* Cgen/cop.sch 422 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_53)))->BgL_typez00);
		}

	}



/* &cblock-type */
	BgL_typez00_bglt BGl_z62cblockzd2typezb0zzcgen_copz00(obj_t BgL_envz00_4928,
		obj_t BgL_oz00_4929)
	{
		{	/* Cgen/cop.sch 422 */
			return
				BGl_cblockzd2typezd2zzcgen_copz00(((BgL_cblockz00_bglt) BgL_oz00_4929));
		}

	}



/* cblock-loc */
	BGL_EXPORTED_DEF obj_t BGl_cblockzd2loczd2zzcgen_copz00(BgL_cblockz00_bglt
		BgL_oz00_56)
	{
		{	/* Cgen/cop.sch 424 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_56)))->BgL_locz00);
		}

	}



/* &cblock-loc */
	obj_t BGl_z62cblockzd2loczb0zzcgen_copz00(obj_t BgL_envz00_4930,
		obj_t BgL_oz00_4931)
	{
		{	/* Cgen/cop.sch 424 */
			return
				BGl_cblockzd2loczd2zzcgen_copz00(((BgL_cblockz00_bglt) BgL_oz00_4931));
		}

	}



/* cblock-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cblockzd2loczd2setz12z12zzcgen_copz00(BgL_cblockz00_bglt BgL_oz00_57,
		obj_t BgL_vz00_58)
	{
		{	/* Cgen/cop.sch 425 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_57)))->BgL_locz00) =
				((obj_t) BgL_vz00_58), BUNSPEC);
		}

	}



/* &cblock-loc-set! */
	obj_t BGl_z62cblockzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4932,
		obj_t BgL_oz00_4933, obj_t BgL_vz00_4934)
	{
		{	/* Cgen/cop.sch 425 */
			return
				BGl_cblockzd2loczd2setz12z12zzcgen_copz00(
				((BgL_cblockz00_bglt) BgL_oz00_4933), BgL_vz00_4934);
		}

	}



/* make-creturn */
	BGL_EXPORTED_DEF BgL_creturnz00_bglt BGl_makezd2creturnzd2zzcgen_copz00(obj_t
		BgL_loc1471z00_59, BgL_typez00_bglt BgL_type1472z00_60,
		bool_t BgL_tail1473z00_61, BgL_copz00_bglt BgL_value1474z00_62)
	{
		{	/* Cgen/cop.sch 428 */
			{	/* Cgen/cop.sch 428 */
				BgL_creturnz00_bglt BgL_new1327z00_6257;

				{	/* Cgen/cop.sch 428 */
					BgL_creturnz00_bglt BgL_new1326z00_6258;

					BgL_new1326z00_6258 =
						((BgL_creturnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_creturnz00_bgl))));
					{	/* Cgen/cop.sch 428 */
						long BgL_arg1616z00_6259;

						BgL_arg1616z00_6259 = BGL_CLASS_NUM(BGl_creturnz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1326z00_6258), BgL_arg1616z00_6259);
					}
					BgL_new1327z00_6257 = BgL_new1326z00_6258;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1327z00_6257)))->BgL_locz00) =
					((obj_t) BgL_loc1471z00_59), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1327z00_6257)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1472z00_60), BUNSPEC);
				((((BgL_creturnz00_bglt) COBJECT(BgL_new1327z00_6257))->BgL_tailz00) =
					((bool_t) BgL_tail1473z00_61), BUNSPEC);
				((((BgL_creturnz00_bglt) COBJECT(BgL_new1327z00_6257))->BgL_valuez00) =
					((BgL_copz00_bglt) BgL_value1474z00_62), BUNSPEC);
				return BgL_new1327z00_6257;
			}
		}

	}



/* &make-creturn */
	BgL_creturnz00_bglt BGl_z62makezd2creturnzb0zzcgen_copz00(obj_t
		BgL_envz00_4935, obj_t BgL_loc1471z00_4936, obj_t BgL_type1472z00_4937,
		obj_t BgL_tail1473z00_4938, obj_t BgL_value1474z00_4939)
	{
		{	/* Cgen/cop.sch 428 */
			return
				BGl_makezd2creturnzd2zzcgen_copz00(BgL_loc1471z00_4936,
				((BgL_typez00_bglt) BgL_type1472z00_4937),
				CBOOL(BgL_tail1473z00_4938), ((BgL_copz00_bglt) BgL_value1474z00_4939));
		}

	}



/* creturn? */
	BGL_EXPORTED_DEF bool_t BGl_creturnzf3zf3zzcgen_copz00(obj_t BgL_objz00_63)
	{
		{	/* Cgen/cop.sch 429 */
			{	/* Cgen/cop.sch 429 */
				obj_t BgL_classz00_6260;

				BgL_classz00_6260 = BGl_creturnz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_63))
					{	/* Cgen/cop.sch 429 */
						BgL_objectz00_bglt BgL_arg1807z00_6261;

						BgL_arg1807z00_6261 = (BgL_objectz00_bglt) (BgL_objz00_63);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 429 */
								long BgL_idxz00_6262;

								BgL_idxz00_6262 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6261);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6262 + 2L)) == BgL_classz00_6260);
							}
						else
							{	/* Cgen/cop.sch 429 */
								bool_t BgL_res2419z00_6265;

								{	/* Cgen/cop.sch 429 */
									obj_t BgL_oclassz00_6266;

									{	/* Cgen/cop.sch 429 */
										obj_t BgL_arg1815z00_6267;
										long BgL_arg1816z00_6268;

										BgL_arg1815z00_6267 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 429 */
											long BgL_arg1817z00_6269;

											BgL_arg1817z00_6269 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6261);
											BgL_arg1816z00_6268 = (BgL_arg1817z00_6269 - OBJECT_TYPE);
										}
										BgL_oclassz00_6266 =
											VECTOR_REF(BgL_arg1815z00_6267, BgL_arg1816z00_6268);
									}
									{	/* Cgen/cop.sch 429 */
										bool_t BgL__ortest_1115z00_6270;

										BgL__ortest_1115z00_6270 =
											(BgL_classz00_6260 == BgL_oclassz00_6266);
										if (BgL__ortest_1115z00_6270)
											{	/* Cgen/cop.sch 429 */
												BgL_res2419z00_6265 = BgL__ortest_1115z00_6270;
											}
										else
											{	/* Cgen/cop.sch 429 */
												long BgL_odepthz00_6271;

												{	/* Cgen/cop.sch 429 */
													obj_t BgL_arg1804z00_6272;

													BgL_arg1804z00_6272 = (BgL_oclassz00_6266);
													BgL_odepthz00_6271 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6272);
												}
												if ((2L < BgL_odepthz00_6271))
													{	/* Cgen/cop.sch 429 */
														obj_t BgL_arg1802z00_6273;

														{	/* Cgen/cop.sch 429 */
															obj_t BgL_arg1803z00_6274;

															BgL_arg1803z00_6274 = (BgL_oclassz00_6266);
															BgL_arg1802z00_6273 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6274,
																2L);
														}
														BgL_res2419z00_6265 =
															(BgL_arg1802z00_6273 == BgL_classz00_6260);
													}
												else
													{	/* Cgen/cop.sch 429 */
														BgL_res2419z00_6265 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2419z00_6265;
							}
					}
				else
					{	/* Cgen/cop.sch 429 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &creturn? */
	obj_t BGl_z62creturnzf3z91zzcgen_copz00(obj_t BgL_envz00_4940,
		obj_t BgL_objz00_4941)
	{
		{	/* Cgen/cop.sch 429 */
			return BBOOL(BGl_creturnzf3zf3zzcgen_copz00(BgL_objz00_4941));
		}

	}



/* creturn-nil */
	BGL_EXPORTED_DEF BgL_creturnz00_bglt BGl_creturnzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 430 */
			{	/* Cgen/cop.sch 430 */
				obj_t BgL_classz00_3514;

				BgL_classz00_3514 = BGl_creturnz00zzcgen_copz00;
				{	/* Cgen/cop.sch 430 */
					obj_t BgL__ortest_1117z00_3515;

					BgL__ortest_1117z00_3515 = BGL_CLASS_NIL(BgL_classz00_3514);
					if (CBOOL(BgL__ortest_1117z00_3515))
						{	/* Cgen/cop.sch 430 */
							return ((BgL_creturnz00_bglt) BgL__ortest_1117z00_3515);
						}
					else
						{	/* Cgen/cop.sch 430 */
							return
								((BgL_creturnz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3514));
						}
				}
			}
		}

	}



/* &creturn-nil */
	BgL_creturnz00_bglt BGl_z62creturnzd2nilzb0zzcgen_copz00(obj_t
		BgL_envz00_4942)
	{
		{	/* Cgen/cop.sch 430 */
			return BGl_creturnzd2nilzd2zzcgen_copz00();
		}

	}



/* creturn-value */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_creturnzd2valuezd2zzcgen_copz00(BgL_creturnz00_bglt BgL_oz00_64)
	{
		{	/* Cgen/cop.sch 431 */
			return (((BgL_creturnz00_bglt) COBJECT(BgL_oz00_64))->BgL_valuez00);
		}

	}



/* &creturn-value */
	BgL_copz00_bglt BGl_z62creturnzd2valuezb0zzcgen_copz00(obj_t BgL_envz00_4943,
		obj_t BgL_oz00_4944)
	{
		{	/* Cgen/cop.sch 431 */
			return
				BGl_creturnzd2valuezd2zzcgen_copz00(
				((BgL_creturnz00_bglt) BgL_oz00_4944));
		}

	}



/* creturn-tail */
	BGL_EXPORTED_DEF bool_t BGl_creturnzd2tailzd2zzcgen_copz00(BgL_creturnz00_bglt
		BgL_oz00_67)
	{
		{	/* Cgen/cop.sch 433 */
			return (((BgL_creturnz00_bglt) COBJECT(BgL_oz00_67))->BgL_tailz00);
		}

	}



/* &creturn-tail */
	obj_t BGl_z62creturnzd2tailzb0zzcgen_copz00(obj_t BgL_envz00_4945,
		obj_t BgL_oz00_4946)
	{
		{	/* Cgen/cop.sch 433 */
			return
				BBOOL(BGl_creturnzd2tailzd2zzcgen_copz00(
					((BgL_creturnz00_bglt) BgL_oz00_4946)));
		}

	}



/* creturn-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_creturnzd2typezd2zzcgen_copz00(BgL_creturnz00_bglt BgL_oz00_70)
	{
		{	/* Cgen/cop.sch 435 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_70)))->BgL_typez00);
		}

	}



/* &creturn-type */
	BgL_typez00_bglt BGl_z62creturnzd2typezb0zzcgen_copz00(obj_t BgL_envz00_4947,
		obj_t BgL_oz00_4948)
	{
		{	/* Cgen/cop.sch 435 */
			return
				BGl_creturnzd2typezd2zzcgen_copz00(
				((BgL_creturnz00_bglt) BgL_oz00_4948));
		}

	}



/* creturn-loc */
	BGL_EXPORTED_DEF obj_t BGl_creturnzd2loczd2zzcgen_copz00(BgL_creturnz00_bglt
		BgL_oz00_73)
	{
		{	/* Cgen/cop.sch 437 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_73)))->BgL_locz00);
		}

	}



/* &creturn-loc */
	obj_t BGl_z62creturnzd2loczb0zzcgen_copz00(obj_t BgL_envz00_4949,
		obj_t BgL_oz00_4950)
	{
		{	/* Cgen/cop.sch 437 */
			return
				BGl_creturnzd2loczd2zzcgen_copz00(
				((BgL_creturnz00_bglt) BgL_oz00_4950));
		}

	}



/* creturn-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_creturnzd2loczd2setz12z12zzcgen_copz00(BgL_creturnz00_bglt BgL_oz00_74,
		obj_t BgL_vz00_75)
	{
		{	/* Cgen/cop.sch 438 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_74)))->BgL_locz00) =
				((obj_t) BgL_vz00_75), BUNSPEC);
		}

	}



/* &creturn-loc-set! */
	obj_t BGl_z62creturnzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4951,
		obj_t BgL_oz00_4952, obj_t BgL_vz00_4953)
	{
		{	/* Cgen/cop.sch 438 */
			return
				BGl_creturnzd2loczd2setz12z12zzcgen_copz00(
				((BgL_creturnz00_bglt) BgL_oz00_4952), BgL_vz00_4953);
		}

	}



/* make-cvoid */
	BGL_EXPORTED_DEF BgL_cvoidz00_bglt BGl_makezd2cvoidzd2zzcgen_copz00(obj_t
		BgL_loc1466z00_76, BgL_typez00_bglt BgL_type1467z00_77,
		BgL_copz00_bglt BgL_value1468z00_78)
	{
		{	/* Cgen/cop.sch 441 */
			{	/* Cgen/cop.sch 441 */
				BgL_cvoidz00_bglt BgL_new1329z00_6275;

				{	/* Cgen/cop.sch 441 */
					BgL_cvoidz00_bglt BgL_new1328z00_6276;

					BgL_new1328z00_6276 =
						((BgL_cvoidz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cvoidz00_bgl))));
					{	/* Cgen/cop.sch 441 */
						long BgL_arg1625z00_6277;

						BgL_arg1625z00_6277 = BGL_CLASS_NUM(BGl_cvoidz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1328z00_6276), BgL_arg1625z00_6277);
					}
					BgL_new1329z00_6275 = BgL_new1328z00_6276;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1329z00_6275)))->BgL_locz00) =
					((obj_t) BgL_loc1466z00_76), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1329z00_6275)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1467z00_77), BUNSPEC);
				((((BgL_cvoidz00_bglt) COBJECT(BgL_new1329z00_6275))->BgL_valuez00) =
					((BgL_copz00_bglt) BgL_value1468z00_78), BUNSPEC);
				return BgL_new1329z00_6275;
			}
		}

	}



/* &make-cvoid */
	BgL_cvoidz00_bglt BGl_z62makezd2cvoidzb0zzcgen_copz00(obj_t BgL_envz00_4954,
		obj_t BgL_loc1466z00_4955, obj_t BgL_type1467z00_4956,
		obj_t BgL_value1468z00_4957)
	{
		{	/* Cgen/cop.sch 441 */
			return
				BGl_makezd2cvoidzd2zzcgen_copz00(BgL_loc1466z00_4955,
				((BgL_typez00_bglt) BgL_type1467z00_4956),
				((BgL_copz00_bglt) BgL_value1468z00_4957));
		}

	}



/* cvoid? */
	BGL_EXPORTED_DEF bool_t BGl_cvoidzf3zf3zzcgen_copz00(obj_t BgL_objz00_79)
	{
		{	/* Cgen/cop.sch 442 */
			{	/* Cgen/cop.sch 442 */
				obj_t BgL_classz00_6278;

				BgL_classz00_6278 = BGl_cvoidz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_79))
					{	/* Cgen/cop.sch 442 */
						BgL_objectz00_bglt BgL_arg1807z00_6279;

						BgL_arg1807z00_6279 = (BgL_objectz00_bglt) (BgL_objz00_79);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 442 */
								long BgL_idxz00_6280;

								BgL_idxz00_6280 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6279);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6280 + 2L)) == BgL_classz00_6278);
							}
						else
							{	/* Cgen/cop.sch 442 */
								bool_t BgL_res2420z00_6283;

								{	/* Cgen/cop.sch 442 */
									obj_t BgL_oclassz00_6284;

									{	/* Cgen/cop.sch 442 */
										obj_t BgL_arg1815z00_6285;
										long BgL_arg1816z00_6286;

										BgL_arg1815z00_6285 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 442 */
											long BgL_arg1817z00_6287;

											BgL_arg1817z00_6287 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6279);
											BgL_arg1816z00_6286 = (BgL_arg1817z00_6287 - OBJECT_TYPE);
										}
										BgL_oclassz00_6284 =
											VECTOR_REF(BgL_arg1815z00_6285, BgL_arg1816z00_6286);
									}
									{	/* Cgen/cop.sch 442 */
										bool_t BgL__ortest_1115z00_6288;

										BgL__ortest_1115z00_6288 =
											(BgL_classz00_6278 == BgL_oclassz00_6284);
										if (BgL__ortest_1115z00_6288)
											{	/* Cgen/cop.sch 442 */
												BgL_res2420z00_6283 = BgL__ortest_1115z00_6288;
											}
										else
											{	/* Cgen/cop.sch 442 */
												long BgL_odepthz00_6289;

												{	/* Cgen/cop.sch 442 */
													obj_t BgL_arg1804z00_6290;

													BgL_arg1804z00_6290 = (BgL_oclassz00_6284);
													BgL_odepthz00_6289 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6290);
												}
												if ((2L < BgL_odepthz00_6289))
													{	/* Cgen/cop.sch 442 */
														obj_t BgL_arg1802z00_6291;

														{	/* Cgen/cop.sch 442 */
															obj_t BgL_arg1803z00_6292;

															BgL_arg1803z00_6292 = (BgL_oclassz00_6284);
															BgL_arg1802z00_6291 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6292,
																2L);
														}
														BgL_res2420z00_6283 =
															(BgL_arg1802z00_6291 == BgL_classz00_6278);
													}
												else
													{	/* Cgen/cop.sch 442 */
														BgL_res2420z00_6283 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2420z00_6283;
							}
					}
				else
					{	/* Cgen/cop.sch 442 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cvoid? */
	obj_t BGl_z62cvoidzf3z91zzcgen_copz00(obj_t BgL_envz00_4958,
		obj_t BgL_objz00_4959)
	{
		{	/* Cgen/cop.sch 442 */
			return BBOOL(BGl_cvoidzf3zf3zzcgen_copz00(BgL_objz00_4959));
		}

	}



/* cvoid-nil */
	BGL_EXPORTED_DEF BgL_cvoidz00_bglt BGl_cvoidzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 443 */
			{	/* Cgen/cop.sch 443 */
				obj_t BgL_classz00_3556;

				BgL_classz00_3556 = BGl_cvoidz00zzcgen_copz00;
				{	/* Cgen/cop.sch 443 */
					obj_t BgL__ortest_1117z00_3557;

					BgL__ortest_1117z00_3557 = BGL_CLASS_NIL(BgL_classz00_3556);
					if (CBOOL(BgL__ortest_1117z00_3557))
						{	/* Cgen/cop.sch 443 */
							return ((BgL_cvoidz00_bglt) BgL__ortest_1117z00_3557);
						}
					else
						{	/* Cgen/cop.sch 443 */
							return
								((BgL_cvoidz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3556));
						}
				}
			}
		}

	}



/* &cvoid-nil */
	BgL_cvoidz00_bglt BGl_z62cvoidzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_4960)
	{
		{	/* Cgen/cop.sch 443 */
			return BGl_cvoidzd2nilzd2zzcgen_copz00();
		}

	}



/* cvoid-value */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cvoidzd2valuezd2zzcgen_copz00(BgL_cvoidz00_bglt BgL_oz00_80)
	{
		{	/* Cgen/cop.sch 444 */
			return (((BgL_cvoidz00_bglt) COBJECT(BgL_oz00_80))->BgL_valuez00);
		}

	}



/* &cvoid-value */
	BgL_copz00_bglt BGl_z62cvoidzd2valuezb0zzcgen_copz00(obj_t BgL_envz00_4961,
		obj_t BgL_oz00_4962)
	{
		{	/* Cgen/cop.sch 444 */
			return
				BGl_cvoidzd2valuezd2zzcgen_copz00(((BgL_cvoidz00_bglt) BgL_oz00_4962));
		}

	}



/* cvoid-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cvoidzd2typezd2zzcgen_copz00(BgL_cvoidz00_bglt BgL_oz00_83)
	{
		{	/* Cgen/cop.sch 446 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_83)))->BgL_typez00);
		}

	}



/* &cvoid-type */
	BgL_typez00_bglt BGl_z62cvoidzd2typezb0zzcgen_copz00(obj_t BgL_envz00_4963,
		obj_t BgL_oz00_4964)
	{
		{	/* Cgen/cop.sch 446 */
			return
				BGl_cvoidzd2typezd2zzcgen_copz00(((BgL_cvoidz00_bglt) BgL_oz00_4964));
		}

	}



/* cvoid-loc */
	BGL_EXPORTED_DEF obj_t BGl_cvoidzd2loczd2zzcgen_copz00(BgL_cvoidz00_bglt
		BgL_oz00_86)
	{
		{	/* Cgen/cop.sch 448 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_86)))->BgL_locz00);
		}

	}



/* &cvoid-loc */
	obj_t BGl_z62cvoidzd2loczb0zzcgen_copz00(obj_t BgL_envz00_4965,
		obj_t BgL_oz00_4966)
	{
		{	/* Cgen/cop.sch 448 */
			return
				BGl_cvoidzd2loczd2zzcgen_copz00(((BgL_cvoidz00_bglt) BgL_oz00_4966));
		}

	}



/* cvoid-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cvoidzd2loczd2setz12z12zzcgen_copz00(BgL_cvoidz00_bglt BgL_oz00_87,
		obj_t BgL_vz00_88)
	{
		{	/* Cgen/cop.sch 449 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_87)))->BgL_locz00) =
				((obj_t) BgL_vz00_88), BUNSPEC);
		}

	}



/* &cvoid-loc-set! */
	obj_t BGl_z62cvoidzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4967,
		obj_t BgL_oz00_4968, obj_t BgL_vz00_4969)
	{
		{	/* Cgen/cop.sch 449 */
			return
				BGl_cvoidzd2loczd2setz12z12zzcgen_copz00(
				((BgL_cvoidz00_bglt) BgL_oz00_4968), BgL_vz00_4969);
		}

	}



/* make-catom */
	BGL_EXPORTED_DEF BgL_catomz00_bglt BGl_makezd2catomzd2zzcgen_copz00(obj_t
		BgL_loc1462z00_89, BgL_typez00_bglt BgL_type1463z00_90,
		obj_t BgL_value1464z00_91)
	{
		{	/* Cgen/cop.sch 452 */
			{	/* Cgen/cop.sch 452 */
				BgL_catomz00_bglt BgL_new1331z00_6293;

				{	/* Cgen/cop.sch 452 */
					BgL_catomz00_bglt BgL_new1330z00_6294;

					BgL_new1330z00_6294 =
						((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_catomz00_bgl))));
					{	/* Cgen/cop.sch 452 */
						long BgL_arg1626z00_6295;

						BgL_arg1626z00_6295 = BGL_CLASS_NUM(BGl_catomz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1330z00_6294), BgL_arg1626z00_6295);
					}
					BgL_new1331z00_6293 = BgL_new1330z00_6294;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1331z00_6293)))->BgL_locz00) =
					((obj_t) BgL_loc1462z00_89), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1331z00_6293)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1463z00_90), BUNSPEC);
				((((BgL_catomz00_bglt) COBJECT(BgL_new1331z00_6293))->BgL_valuez00) =
					((obj_t) BgL_value1464z00_91), BUNSPEC);
				return BgL_new1331z00_6293;
			}
		}

	}



/* &make-catom */
	BgL_catomz00_bglt BGl_z62makezd2catomzb0zzcgen_copz00(obj_t BgL_envz00_4970,
		obj_t BgL_loc1462z00_4971, obj_t BgL_type1463z00_4972,
		obj_t BgL_value1464z00_4973)
	{
		{	/* Cgen/cop.sch 452 */
			return
				BGl_makezd2catomzd2zzcgen_copz00(BgL_loc1462z00_4971,
				((BgL_typez00_bglt) BgL_type1463z00_4972), BgL_value1464z00_4973);
		}

	}



/* catom? */
	BGL_EXPORTED_DEF bool_t BGl_catomzf3zf3zzcgen_copz00(obj_t BgL_objz00_92)
	{
		{	/* Cgen/cop.sch 453 */
			{	/* Cgen/cop.sch 453 */
				obj_t BgL_classz00_6296;

				BgL_classz00_6296 = BGl_catomz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_92))
					{	/* Cgen/cop.sch 453 */
						BgL_objectz00_bglt BgL_arg1807z00_6297;

						BgL_arg1807z00_6297 = (BgL_objectz00_bglt) (BgL_objz00_92);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 453 */
								long BgL_idxz00_6298;

								BgL_idxz00_6298 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6297);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6298 + 2L)) == BgL_classz00_6296);
							}
						else
							{	/* Cgen/cop.sch 453 */
								bool_t BgL_res2421z00_6301;

								{	/* Cgen/cop.sch 453 */
									obj_t BgL_oclassz00_6302;

									{	/* Cgen/cop.sch 453 */
										obj_t BgL_arg1815z00_6303;
										long BgL_arg1816z00_6304;

										BgL_arg1815z00_6303 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 453 */
											long BgL_arg1817z00_6305;

											BgL_arg1817z00_6305 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6297);
											BgL_arg1816z00_6304 = (BgL_arg1817z00_6305 - OBJECT_TYPE);
										}
										BgL_oclassz00_6302 =
											VECTOR_REF(BgL_arg1815z00_6303, BgL_arg1816z00_6304);
									}
									{	/* Cgen/cop.sch 453 */
										bool_t BgL__ortest_1115z00_6306;

										BgL__ortest_1115z00_6306 =
											(BgL_classz00_6296 == BgL_oclassz00_6302);
										if (BgL__ortest_1115z00_6306)
											{	/* Cgen/cop.sch 453 */
												BgL_res2421z00_6301 = BgL__ortest_1115z00_6306;
											}
										else
											{	/* Cgen/cop.sch 453 */
												long BgL_odepthz00_6307;

												{	/* Cgen/cop.sch 453 */
													obj_t BgL_arg1804z00_6308;

													BgL_arg1804z00_6308 = (BgL_oclassz00_6302);
													BgL_odepthz00_6307 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6308);
												}
												if ((2L < BgL_odepthz00_6307))
													{	/* Cgen/cop.sch 453 */
														obj_t BgL_arg1802z00_6309;

														{	/* Cgen/cop.sch 453 */
															obj_t BgL_arg1803z00_6310;

															BgL_arg1803z00_6310 = (BgL_oclassz00_6302);
															BgL_arg1802z00_6309 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6310,
																2L);
														}
														BgL_res2421z00_6301 =
															(BgL_arg1802z00_6309 == BgL_classz00_6296);
													}
												else
													{	/* Cgen/cop.sch 453 */
														BgL_res2421z00_6301 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2421z00_6301;
							}
					}
				else
					{	/* Cgen/cop.sch 453 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &catom? */
	obj_t BGl_z62catomzf3z91zzcgen_copz00(obj_t BgL_envz00_4974,
		obj_t BgL_objz00_4975)
	{
		{	/* Cgen/cop.sch 453 */
			return BBOOL(BGl_catomzf3zf3zzcgen_copz00(BgL_objz00_4975));
		}

	}



/* catom-nil */
	BGL_EXPORTED_DEF BgL_catomz00_bglt BGl_catomzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 454 */
			{	/* Cgen/cop.sch 454 */
				obj_t BgL_classz00_3598;

				BgL_classz00_3598 = BGl_catomz00zzcgen_copz00;
				{	/* Cgen/cop.sch 454 */
					obj_t BgL__ortest_1117z00_3599;

					BgL__ortest_1117z00_3599 = BGL_CLASS_NIL(BgL_classz00_3598);
					if (CBOOL(BgL__ortest_1117z00_3599))
						{	/* Cgen/cop.sch 454 */
							return ((BgL_catomz00_bglt) BgL__ortest_1117z00_3599);
						}
					else
						{	/* Cgen/cop.sch 454 */
							return
								((BgL_catomz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3598));
						}
				}
			}
		}

	}



/* &catom-nil */
	BgL_catomz00_bglt BGl_z62catomzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_4976)
	{
		{	/* Cgen/cop.sch 454 */
			return BGl_catomzd2nilzd2zzcgen_copz00();
		}

	}



/* catom-value */
	BGL_EXPORTED_DEF obj_t BGl_catomzd2valuezd2zzcgen_copz00(BgL_catomz00_bglt
		BgL_oz00_93)
	{
		{	/* Cgen/cop.sch 455 */
			return (((BgL_catomz00_bglt) COBJECT(BgL_oz00_93))->BgL_valuez00);
		}

	}



/* &catom-value */
	obj_t BGl_z62catomzd2valuezb0zzcgen_copz00(obj_t BgL_envz00_4977,
		obj_t BgL_oz00_4978)
	{
		{	/* Cgen/cop.sch 455 */
			return
				BGl_catomzd2valuezd2zzcgen_copz00(((BgL_catomz00_bglt) BgL_oz00_4978));
		}

	}



/* catom-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_catomzd2typezd2zzcgen_copz00(BgL_catomz00_bglt BgL_oz00_96)
	{
		{	/* Cgen/cop.sch 457 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_96)))->BgL_typez00);
		}

	}



/* &catom-type */
	BgL_typez00_bglt BGl_z62catomzd2typezb0zzcgen_copz00(obj_t BgL_envz00_4979,
		obj_t BgL_oz00_4980)
	{
		{	/* Cgen/cop.sch 457 */
			return
				BGl_catomzd2typezd2zzcgen_copz00(((BgL_catomz00_bglt) BgL_oz00_4980));
		}

	}



/* catom-loc */
	BGL_EXPORTED_DEF obj_t BGl_catomzd2loczd2zzcgen_copz00(BgL_catomz00_bglt
		BgL_oz00_99)
	{
		{	/* Cgen/cop.sch 459 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_99)))->BgL_locz00);
		}

	}



/* &catom-loc */
	obj_t BGl_z62catomzd2loczb0zzcgen_copz00(obj_t BgL_envz00_4981,
		obj_t BgL_oz00_4982)
	{
		{	/* Cgen/cop.sch 459 */
			return
				BGl_catomzd2loczd2zzcgen_copz00(((BgL_catomz00_bglt) BgL_oz00_4982));
		}

	}



/* catom-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_catomzd2loczd2setz12z12zzcgen_copz00(BgL_catomz00_bglt BgL_oz00_100,
		obj_t BgL_vz00_101)
	{
		{	/* Cgen/cop.sch 460 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_100)))->BgL_locz00) =
				((obj_t) BgL_vz00_101), BUNSPEC);
		}

	}



/* &catom-loc-set! */
	obj_t BGl_z62catomzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4983,
		obj_t BgL_oz00_4984, obj_t BgL_vz00_4985)
	{
		{	/* Cgen/cop.sch 460 */
			return
				BGl_catomzd2loczd2setz12z12zzcgen_copz00(
				((BgL_catomz00_bglt) BgL_oz00_4984), BgL_vz00_4985);
		}

	}



/* make-varc */
	BGL_EXPORTED_DEF BgL_varcz00_bglt BGl_makezd2varczd2zzcgen_copz00(obj_t
		BgL_loc1458z00_102, BgL_typez00_bglt BgL_type1459z00_103,
		BgL_variablez00_bglt BgL_variable1460z00_104)
	{
		{	/* Cgen/cop.sch 463 */
			{	/* Cgen/cop.sch 463 */
				BgL_varcz00_bglt BgL_new1333z00_6311;

				{	/* Cgen/cop.sch 463 */
					BgL_varcz00_bglt BgL_new1332z00_6312;

					BgL_new1332z00_6312 =
						((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_varcz00_bgl))));
					{	/* Cgen/cop.sch 463 */
						long BgL_arg1627z00_6313;

						BgL_arg1627z00_6313 = BGL_CLASS_NUM(BGl_varcz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1332z00_6312), BgL_arg1627z00_6313);
					}
					BgL_new1333z00_6311 = BgL_new1332z00_6312;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1333z00_6311)))->BgL_locz00) =
					((obj_t) BgL_loc1458z00_102), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1333z00_6311)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1459z00_103), BUNSPEC);
				((((BgL_varcz00_bglt) COBJECT(BgL_new1333z00_6311))->BgL_variablez00) =
					((BgL_variablez00_bglt) BgL_variable1460z00_104), BUNSPEC);
				return BgL_new1333z00_6311;
			}
		}

	}



/* &make-varc */
	BgL_varcz00_bglt BGl_z62makezd2varczb0zzcgen_copz00(obj_t BgL_envz00_4986,
		obj_t BgL_loc1458z00_4987, obj_t BgL_type1459z00_4988,
		obj_t BgL_variable1460z00_4989)
	{
		{	/* Cgen/cop.sch 463 */
			return
				BGl_makezd2varczd2zzcgen_copz00(BgL_loc1458z00_4987,
				((BgL_typez00_bglt) BgL_type1459z00_4988),
				((BgL_variablez00_bglt) BgL_variable1460z00_4989));
		}

	}



/* varc? */
	BGL_EXPORTED_DEF bool_t BGl_varczf3zf3zzcgen_copz00(obj_t BgL_objz00_105)
	{
		{	/* Cgen/cop.sch 464 */
			{	/* Cgen/cop.sch 464 */
				obj_t BgL_classz00_6314;

				BgL_classz00_6314 = BGl_varcz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_105))
					{	/* Cgen/cop.sch 464 */
						BgL_objectz00_bglt BgL_arg1807z00_6315;

						BgL_arg1807z00_6315 = (BgL_objectz00_bglt) (BgL_objz00_105);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 464 */
								long BgL_idxz00_6316;

								BgL_idxz00_6316 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6315);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6316 + 2L)) == BgL_classz00_6314);
							}
						else
							{	/* Cgen/cop.sch 464 */
								bool_t BgL_res2422z00_6319;

								{	/* Cgen/cop.sch 464 */
									obj_t BgL_oclassz00_6320;

									{	/* Cgen/cop.sch 464 */
										obj_t BgL_arg1815z00_6321;
										long BgL_arg1816z00_6322;

										BgL_arg1815z00_6321 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 464 */
											long BgL_arg1817z00_6323;

											BgL_arg1817z00_6323 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6315);
											BgL_arg1816z00_6322 = (BgL_arg1817z00_6323 - OBJECT_TYPE);
										}
										BgL_oclassz00_6320 =
											VECTOR_REF(BgL_arg1815z00_6321, BgL_arg1816z00_6322);
									}
									{	/* Cgen/cop.sch 464 */
										bool_t BgL__ortest_1115z00_6324;

										BgL__ortest_1115z00_6324 =
											(BgL_classz00_6314 == BgL_oclassz00_6320);
										if (BgL__ortest_1115z00_6324)
											{	/* Cgen/cop.sch 464 */
												BgL_res2422z00_6319 = BgL__ortest_1115z00_6324;
											}
										else
											{	/* Cgen/cop.sch 464 */
												long BgL_odepthz00_6325;

												{	/* Cgen/cop.sch 464 */
													obj_t BgL_arg1804z00_6326;

													BgL_arg1804z00_6326 = (BgL_oclassz00_6320);
													BgL_odepthz00_6325 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6326);
												}
												if ((2L < BgL_odepthz00_6325))
													{	/* Cgen/cop.sch 464 */
														obj_t BgL_arg1802z00_6327;

														{	/* Cgen/cop.sch 464 */
															obj_t BgL_arg1803z00_6328;

															BgL_arg1803z00_6328 = (BgL_oclassz00_6320);
															BgL_arg1802z00_6327 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6328,
																2L);
														}
														BgL_res2422z00_6319 =
															(BgL_arg1802z00_6327 == BgL_classz00_6314);
													}
												else
													{	/* Cgen/cop.sch 464 */
														BgL_res2422z00_6319 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2422z00_6319;
							}
					}
				else
					{	/* Cgen/cop.sch 464 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &varc? */
	obj_t BGl_z62varczf3z91zzcgen_copz00(obj_t BgL_envz00_4990,
		obj_t BgL_objz00_4991)
	{
		{	/* Cgen/cop.sch 464 */
			return BBOOL(BGl_varczf3zf3zzcgen_copz00(BgL_objz00_4991));
		}

	}



/* varc-nil */
	BGL_EXPORTED_DEF BgL_varcz00_bglt BGl_varczd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 465 */
			{	/* Cgen/cop.sch 465 */
				obj_t BgL_classz00_3640;

				BgL_classz00_3640 = BGl_varcz00zzcgen_copz00;
				{	/* Cgen/cop.sch 465 */
					obj_t BgL__ortest_1117z00_3641;

					BgL__ortest_1117z00_3641 = BGL_CLASS_NIL(BgL_classz00_3640);
					if (CBOOL(BgL__ortest_1117z00_3641))
						{	/* Cgen/cop.sch 465 */
							return ((BgL_varcz00_bglt) BgL__ortest_1117z00_3641);
						}
					else
						{	/* Cgen/cop.sch 465 */
							return
								((BgL_varcz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3640));
						}
				}
			}
		}

	}



/* &varc-nil */
	BgL_varcz00_bglt BGl_z62varczd2nilzb0zzcgen_copz00(obj_t BgL_envz00_4992)
	{
		{	/* Cgen/cop.sch 465 */
			return BGl_varczd2nilzd2zzcgen_copz00();
		}

	}



/* varc-variable */
	BGL_EXPORTED_DEF BgL_variablez00_bglt
		BGl_varczd2variablezd2zzcgen_copz00(BgL_varcz00_bglt BgL_oz00_106)
	{
		{	/* Cgen/cop.sch 466 */
			return (((BgL_varcz00_bglt) COBJECT(BgL_oz00_106))->BgL_variablez00);
		}

	}



/* &varc-variable */
	BgL_variablez00_bglt BGl_z62varczd2variablezb0zzcgen_copz00(obj_t
		BgL_envz00_4993, obj_t BgL_oz00_4994)
	{
		{	/* Cgen/cop.sch 466 */
			return
				BGl_varczd2variablezd2zzcgen_copz00(((BgL_varcz00_bglt) BgL_oz00_4994));
		}

	}



/* varc-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_varczd2typezd2zzcgen_copz00(BgL_varcz00_bglt BgL_oz00_109)
	{
		{	/* Cgen/cop.sch 468 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_109)))->BgL_typez00);
		}

	}



/* &varc-type */
	BgL_typez00_bglt BGl_z62varczd2typezb0zzcgen_copz00(obj_t BgL_envz00_4995,
		obj_t BgL_oz00_4996)
	{
		{	/* Cgen/cop.sch 468 */
			return
				BGl_varczd2typezd2zzcgen_copz00(((BgL_varcz00_bglt) BgL_oz00_4996));
		}

	}



/* varc-loc */
	BGL_EXPORTED_DEF obj_t BGl_varczd2loczd2zzcgen_copz00(BgL_varcz00_bglt
		BgL_oz00_112)
	{
		{	/* Cgen/cop.sch 470 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_112)))->BgL_locz00);
		}

	}



/* &varc-loc */
	obj_t BGl_z62varczd2loczb0zzcgen_copz00(obj_t BgL_envz00_4997,
		obj_t BgL_oz00_4998)
	{
		{	/* Cgen/cop.sch 470 */
			return BGl_varczd2loczd2zzcgen_copz00(((BgL_varcz00_bglt) BgL_oz00_4998));
		}

	}



/* varc-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_varczd2loczd2setz12z12zzcgen_copz00(BgL_varcz00_bglt BgL_oz00_113,
		obj_t BgL_vz00_114)
	{
		{	/* Cgen/cop.sch 471 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_113)))->BgL_locz00) =
				((obj_t) BgL_vz00_114), BUNSPEC);
		}

	}



/* &varc-loc-set! */
	obj_t BGl_z62varczd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_4999,
		obj_t BgL_oz00_5000, obj_t BgL_vz00_5001)
	{
		{	/* Cgen/cop.sch 471 */
			return
				BGl_varczd2loczd2setz12z12zzcgen_copz00(
				((BgL_varcz00_bglt) BgL_oz00_5000), BgL_vz00_5001);
		}

	}



/* make-cpragma */
	BGL_EXPORTED_DEF BgL_cpragmaz00_bglt BGl_makezd2cpragmazd2zzcgen_copz00(obj_t
		BgL_loc1453z00_115, BgL_typez00_bglt BgL_type1454z00_116,
		obj_t BgL_format1455z00_117, obj_t BgL_args1456z00_118)
	{
		{	/* Cgen/cop.sch 474 */
			{	/* Cgen/cop.sch 474 */
				BgL_cpragmaz00_bglt BgL_new1335z00_6329;

				{	/* Cgen/cop.sch 474 */
					BgL_cpragmaz00_bglt BgL_new1334z00_6330;

					BgL_new1334z00_6330 =
						((BgL_cpragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cpragmaz00_bgl))));
					{	/* Cgen/cop.sch 474 */
						long BgL_arg1629z00_6331;

						BgL_arg1629z00_6331 = BGL_CLASS_NUM(BGl_cpragmaz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1334z00_6330), BgL_arg1629z00_6331);
					}
					BgL_new1335z00_6329 = BgL_new1334z00_6330;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1335z00_6329)))->BgL_locz00) =
					((obj_t) BgL_loc1453z00_115), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1335z00_6329)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1454z00_116), BUNSPEC);
				((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1335z00_6329))->BgL_formatz00) =
					((obj_t) BgL_format1455z00_117), BUNSPEC);
				((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1335z00_6329))->BgL_argsz00) =
					((obj_t) BgL_args1456z00_118), BUNSPEC);
				return BgL_new1335z00_6329;
			}
		}

	}



/* &make-cpragma */
	BgL_cpragmaz00_bglt BGl_z62makezd2cpragmazb0zzcgen_copz00(obj_t
		BgL_envz00_5002, obj_t BgL_loc1453z00_5003, obj_t BgL_type1454z00_5004,
		obj_t BgL_format1455z00_5005, obj_t BgL_args1456z00_5006)
	{
		{	/* Cgen/cop.sch 474 */
			return
				BGl_makezd2cpragmazd2zzcgen_copz00(BgL_loc1453z00_5003,
				((BgL_typez00_bglt) BgL_type1454z00_5004), BgL_format1455z00_5005,
				BgL_args1456z00_5006);
		}

	}



/* cpragma? */
	BGL_EXPORTED_DEF bool_t BGl_cpragmazf3zf3zzcgen_copz00(obj_t BgL_objz00_119)
	{
		{	/* Cgen/cop.sch 475 */
			{	/* Cgen/cop.sch 475 */
				obj_t BgL_classz00_6332;

				BgL_classz00_6332 = BGl_cpragmaz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_119))
					{	/* Cgen/cop.sch 475 */
						BgL_objectz00_bglt BgL_arg1807z00_6333;

						BgL_arg1807z00_6333 = (BgL_objectz00_bglt) (BgL_objz00_119);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 475 */
								long BgL_idxz00_6334;

								BgL_idxz00_6334 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6333);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6334 + 2L)) == BgL_classz00_6332);
							}
						else
							{	/* Cgen/cop.sch 475 */
								bool_t BgL_res2423z00_6337;

								{	/* Cgen/cop.sch 475 */
									obj_t BgL_oclassz00_6338;

									{	/* Cgen/cop.sch 475 */
										obj_t BgL_arg1815z00_6339;
										long BgL_arg1816z00_6340;

										BgL_arg1815z00_6339 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 475 */
											long BgL_arg1817z00_6341;

											BgL_arg1817z00_6341 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6333);
											BgL_arg1816z00_6340 = (BgL_arg1817z00_6341 - OBJECT_TYPE);
										}
										BgL_oclassz00_6338 =
											VECTOR_REF(BgL_arg1815z00_6339, BgL_arg1816z00_6340);
									}
									{	/* Cgen/cop.sch 475 */
										bool_t BgL__ortest_1115z00_6342;

										BgL__ortest_1115z00_6342 =
											(BgL_classz00_6332 == BgL_oclassz00_6338);
										if (BgL__ortest_1115z00_6342)
											{	/* Cgen/cop.sch 475 */
												BgL_res2423z00_6337 = BgL__ortest_1115z00_6342;
											}
										else
											{	/* Cgen/cop.sch 475 */
												long BgL_odepthz00_6343;

												{	/* Cgen/cop.sch 475 */
													obj_t BgL_arg1804z00_6344;

													BgL_arg1804z00_6344 = (BgL_oclassz00_6338);
													BgL_odepthz00_6343 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6344);
												}
												if ((2L < BgL_odepthz00_6343))
													{	/* Cgen/cop.sch 475 */
														obj_t BgL_arg1802z00_6345;

														{	/* Cgen/cop.sch 475 */
															obj_t BgL_arg1803z00_6346;

															BgL_arg1803z00_6346 = (BgL_oclassz00_6338);
															BgL_arg1802z00_6345 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6346,
																2L);
														}
														BgL_res2423z00_6337 =
															(BgL_arg1802z00_6345 == BgL_classz00_6332);
													}
												else
													{	/* Cgen/cop.sch 475 */
														BgL_res2423z00_6337 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2423z00_6337;
							}
					}
				else
					{	/* Cgen/cop.sch 475 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cpragma? */
	obj_t BGl_z62cpragmazf3z91zzcgen_copz00(obj_t BgL_envz00_5007,
		obj_t BgL_objz00_5008)
	{
		{	/* Cgen/cop.sch 475 */
			return BBOOL(BGl_cpragmazf3zf3zzcgen_copz00(BgL_objz00_5008));
		}

	}



/* cpragma-nil */
	BGL_EXPORTED_DEF BgL_cpragmaz00_bglt BGl_cpragmazd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 476 */
			{	/* Cgen/cop.sch 476 */
				obj_t BgL_classz00_3682;

				BgL_classz00_3682 = BGl_cpragmaz00zzcgen_copz00;
				{	/* Cgen/cop.sch 476 */
					obj_t BgL__ortest_1117z00_3683;

					BgL__ortest_1117z00_3683 = BGL_CLASS_NIL(BgL_classz00_3682);
					if (CBOOL(BgL__ortest_1117z00_3683))
						{	/* Cgen/cop.sch 476 */
							return ((BgL_cpragmaz00_bglt) BgL__ortest_1117z00_3683);
						}
					else
						{	/* Cgen/cop.sch 476 */
							return
								((BgL_cpragmaz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3682));
						}
				}
			}
		}

	}



/* &cpragma-nil */
	BgL_cpragmaz00_bglt BGl_z62cpragmazd2nilzb0zzcgen_copz00(obj_t
		BgL_envz00_5009)
	{
		{	/* Cgen/cop.sch 476 */
			return BGl_cpragmazd2nilzd2zzcgen_copz00();
		}

	}



/* cpragma-args */
	BGL_EXPORTED_DEF obj_t BGl_cpragmazd2argszd2zzcgen_copz00(BgL_cpragmaz00_bglt
		BgL_oz00_120)
	{
		{	/* Cgen/cop.sch 477 */
			return (((BgL_cpragmaz00_bglt) COBJECT(BgL_oz00_120))->BgL_argsz00);
		}

	}



/* &cpragma-args */
	obj_t BGl_z62cpragmazd2argszb0zzcgen_copz00(obj_t BgL_envz00_5010,
		obj_t BgL_oz00_5011)
	{
		{	/* Cgen/cop.sch 477 */
			return
				BGl_cpragmazd2argszd2zzcgen_copz00(
				((BgL_cpragmaz00_bglt) BgL_oz00_5011));
		}

	}



/* cpragma-format */
	BGL_EXPORTED_DEF obj_t
		BGl_cpragmazd2formatzd2zzcgen_copz00(BgL_cpragmaz00_bglt BgL_oz00_123)
	{
		{	/* Cgen/cop.sch 479 */
			return (((BgL_cpragmaz00_bglt) COBJECT(BgL_oz00_123))->BgL_formatz00);
		}

	}



/* &cpragma-format */
	obj_t BGl_z62cpragmazd2formatzb0zzcgen_copz00(obj_t BgL_envz00_5012,
		obj_t BgL_oz00_5013)
	{
		{	/* Cgen/cop.sch 479 */
			return
				BGl_cpragmazd2formatzd2zzcgen_copz00(
				((BgL_cpragmaz00_bglt) BgL_oz00_5013));
		}

	}



/* cpragma-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cpragmazd2typezd2zzcgen_copz00(BgL_cpragmaz00_bglt BgL_oz00_126)
	{
		{	/* Cgen/cop.sch 481 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_126)))->BgL_typez00);
		}

	}



/* &cpragma-type */
	BgL_typez00_bglt BGl_z62cpragmazd2typezb0zzcgen_copz00(obj_t BgL_envz00_5014,
		obj_t BgL_oz00_5015)
	{
		{	/* Cgen/cop.sch 481 */
			return
				BGl_cpragmazd2typezd2zzcgen_copz00(
				((BgL_cpragmaz00_bglt) BgL_oz00_5015));
		}

	}



/* cpragma-loc */
	BGL_EXPORTED_DEF obj_t BGl_cpragmazd2loczd2zzcgen_copz00(BgL_cpragmaz00_bglt
		BgL_oz00_129)
	{
		{	/* Cgen/cop.sch 483 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_129)))->BgL_locz00);
		}

	}



/* &cpragma-loc */
	obj_t BGl_z62cpragmazd2loczb0zzcgen_copz00(obj_t BgL_envz00_5016,
		obj_t BgL_oz00_5017)
	{
		{	/* Cgen/cop.sch 483 */
			return
				BGl_cpragmazd2loczd2zzcgen_copz00(
				((BgL_cpragmaz00_bglt) BgL_oz00_5017));
		}

	}



/* cpragma-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cpragmazd2loczd2setz12z12zzcgen_copz00(BgL_cpragmaz00_bglt BgL_oz00_130,
		obj_t BgL_vz00_131)
	{
		{	/* Cgen/cop.sch 484 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_130)))->BgL_locz00) =
				((obj_t) BgL_vz00_131), BUNSPEC);
		}

	}



/* &cpragma-loc-set! */
	obj_t BGl_z62cpragmazd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5018,
		obj_t BgL_oz00_5019, obj_t BgL_vz00_5020)
	{
		{	/* Cgen/cop.sch 484 */
			return
				BGl_cpragmazd2loczd2setz12z12zzcgen_copz00(
				((BgL_cpragmaz00_bglt) BgL_oz00_5019), BgL_vz00_5020);
		}

	}



/* make-ccast */
	BGL_EXPORTED_DEF BgL_ccastz00_bglt BGl_makezd2ccastzd2zzcgen_copz00(obj_t
		BgL_loc1437z00_132, BgL_typez00_bglt BgL_type1438z00_133,
		BgL_copz00_bglt BgL_arg1451z00_134)
	{
		{	/* Cgen/cop.sch 487 */
			{	/* Cgen/cop.sch 487 */
				BgL_ccastz00_bglt BgL_new1337z00_6347;

				{	/* Cgen/cop.sch 487 */
					BgL_ccastz00_bglt BgL_new1336z00_6348;

					BgL_new1336z00_6348 =
						((BgL_ccastz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccastz00_bgl))));
					{	/* Cgen/cop.sch 487 */
						long BgL_arg1630z00_6349;

						BgL_arg1630z00_6349 = BGL_CLASS_NUM(BGl_ccastz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1336z00_6348), BgL_arg1630z00_6349);
					}
					BgL_new1337z00_6347 = BgL_new1336z00_6348;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1337z00_6347)))->BgL_locz00) =
					((obj_t) BgL_loc1437z00_132), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1337z00_6347)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1438z00_133), BUNSPEC);
				((((BgL_ccastz00_bglt) COBJECT(BgL_new1337z00_6347))->BgL_argz00) =
					((BgL_copz00_bglt) BgL_arg1451z00_134), BUNSPEC);
				return BgL_new1337z00_6347;
			}
		}

	}



/* &make-ccast */
	BgL_ccastz00_bglt BGl_z62makezd2ccastzb0zzcgen_copz00(obj_t BgL_envz00_5021,
		obj_t BgL_loc1437z00_5022, obj_t BgL_type1438z00_5023,
		obj_t BgL_arg1451z00_5024)
	{
		{	/* Cgen/cop.sch 487 */
			return
				BGl_makezd2ccastzd2zzcgen_copz00(BgL_loc1437z00_5022,
				((BgL_typez00_bglt) BgL_type1438z00_5023),
				((BgL_copz00_bglt) BgL_arg1451z00_5024));
		}

	}



/* ccast? */
	BGL_EXPORTED_DEF bool_t BGl_ccastzf3zf3zzcgen_copz00(obj_t BgL_objz00_135)
	{
		{	/* Cgen/cop.sch 488 */
			{	/* Cgen/cop.sch 488 */
				obj_t BgL_classz00_6350;

				BgL_classz00_6350 = BGl_ccastz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_135))
					{	/* Cgen/cop.sch 488 */
						BgL_objectz00_bglt BgL_arg1807z00_6351;

						BgL_arg1807z00_6351 = (BgL_objectz00_bglt) (BgL_objz00_135);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 488 */
								long BgL_idxz00_6352;

								BgL_idxz00_6352 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6351);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6352 + 2L)) == BgL_classz00_6350);
							}
						else
							{	/* Cgen/cop.sch 488 */
								bool_t BgL_res2424z00_6355;

								{	/* Cgen/cop.sch 488 */
									obj_t BgL_oclassz00_6356;

									{	/* Cgen/cop.sch 488 */
										obj_t BgL_arg1815z00_6357;
										long BgL_arg1816z00_6358;

										BgL_arg1815z00_6357 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 488 */
											long BgL_arg1817z00_6359;

											BgL_arg1817z00_6359 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6351);
											BgL_arg1816z00_6358 = (BgL_arg1817z00_6359 - OBJECT_TYPE);
										}
										BgL_oclassz00_6356 =
											VECTOR_REF(BgL_arg1815z00_6357, BgL_arg1816z00_6358);
									}
									{	/* Cgen/cop.sch 488 */
										bool_t BgL__ortest_1115z00_6360;

										BgL__ortest_1115z00_6360 =
											(BgL_classz00_6350 == BgL_oclassz00_6356);
										if (BgL__ortest_1115z00_6360)
											{	/* Cgen/cop.sch 488 */
												BgL_res2424z00_6355 = BgL__ortest_1115z00_6360;
											}
										else
											{	/* Cgen/cop.sch 488 */
												long BgL_odepthz00_6361;

												{	/* Cgen/cop.sch 488 */
													obj_t BgL_arg1804z00_6362;

													BgL_arg1804z00_6362 = (BgL_oclassz00_6356);
													BgL_odepthz00_6361 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6362);
												}
												if ((2L < BgL_odepthz00_6361))
													{	/* Cgen/cop.sch 488 */
														obj_t BgL_arg1802z00_6363;

														{	/* Cgen/cop.sch 488 */
															obj_t BgL_arg1803z00_6364;

															BgL_arg1803z00_6364 = (BgL_oclassz00_6356);
															BgL_arg1802z00_6363 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6364,
																2L);
														}
														BgL_res2424z00_6355 =
															(BgL_arg1802z00_6363 == BgL_classz00_6350);
													}
												else
													{	/* Cgen/cop.sch 488 */
														BgL_res2424z00_6355 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2424z00_6355;
							}
					}
				else
					{	/* Cgen/cop.sch 488 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &ccast? */
	obj_t BGl_z62ccastzf3z91zzcgen_copz00(obj_t BgL_envz00_5025,
		obj_t BgL_objz00_5026)
	{
		{	/* Cgen/cop.sch 488 */
			return BBOOL(BGl_ccastzf3zf3zzcgen_copz00(BgL_objz00_5026));
		}

	}



/* ccast-nil */
	BGL_EXPORTED_DEF BgL_ccastz00_bglt BGl_ccastzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 489 */
			{	/* Cgen/cop.sch 489 */
				obj_t BgL_classz00_3724;

				BgL_classz00_3724 = BGl_ccastz00zzcgen_copz00;
				{	/* Cgen/cop.sch 489 */
					obj_t BgL__ortest_1117z00_3725;

					BgL__ortest_1117z00_3725 = BGL_CLASS_NIL(BgL_classz00_3724);
					if (CBOOL(BgL__ortest_1117z00_3725))
						{	/* Cgen/cop.sch 489 */
							return ((BgL_ccastz00_bglt) BgL__ortest_1117z00_3725);
						}
					else
						{	/* Cgen/cop.sch 489 */
							return
								((BgL_ccastz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3724));
						}
				}
			}
		}

	}



/* &ccast-nil */
	BgL_ccastz00_bglt BGl_z62ccastzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_5027)
	{
		{	/* Cgen/cop.sch 489 */
			return BGl_ccastzd2nilzd2zzcgen_copz00();
		}

	}



/* ccast-arg */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_ccastzd2argzd2zzcgen_copz00(BgL_ccastz00_bglt BgL_oz00_136)
	{
		{	/* Cgen/cop.sch 490 */
			return (((BgL_ccastz00_bglt) COBJECT(BgL_oz00_136))->BgL_argz00);
		}

	}



/* &ccast-arg */
	BgL_copz00_bglt BGl_z62ccastzd2argzb0zzcgen_copz00(obj_t BgL_envz00_5028,
		obj_t BgL_oz00_5029)
	{
		{	/* Cgen/cop.sch 490 */
			return
				BGl_ccastzd2argzd2zzcgen_copz00(((BgL_ccastz00_bglt) BgL_oz00_5029));
		}

	}



/* ccast-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_ccastzd2typezd2zzcgen_copz00(BgL_ccastz00_bglt BgL_oz00_139)
	{
		{	/* Cgen/cop.sch 492 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_139)))->BgL_typez00);
		}

	}



/* &ccast-type */
	BgL_typez00_bglt BGl_z62ccastzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5030,
		obj_t BgL_oz00_5031)
	{
		{	/* Cgen/cop.sch 492 */
			return
				BGl_ccastzd2typezd2zzcgen_copz00(((BgL_ccastz00_bglt) BgL_oz00_5031));
		}

	}



/* ccast-loc */
	BGL_EXPORTED_DEF obj_t BGl_ccastzd2loczd2zzcgen_copz00(BgL_ccastz00_bglt
		BgL_oz00_142)
	{
		{	/* Cgen/cop.sch 494 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_142)))->BgL_locz00);
		}

	}



/* &ccast-loc */
	obj_t BGl_z62ccastzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5032,
		obj_t BgL_oz00_5033)
	{
		{	/* Cgen/cop.sch 494 */
			return
				BGl_ccastzd2loczd2zzcgen_copz00(((BgL_ccastz00_bglt) BgL_oz00_5033));
		}

	}



/* ccast-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_ccastzd2loczd2setz12z12zzcgen_copz00(BgL_ccastz00_bglt BgL_oz00_143,
		obj_t BgL_vz00_144)
	{
		{	/* Cgen/cop.sch 495 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_143)))->BgL_locz00) =
				((obj_t) BgL_vz00_144), BUNSPEC);
		}

	}



/* &ccast-loc-set! */
	obj_t BGl_z62ccastzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5034,
		obj_t BgL_oz00_5035, obj_t BgL_vz00_5036)
	{
		{	/* Cgen/cop.sch 495 */
			return
				BGl_ccastzd2loczd2setz12z12zzcgen_copz00(
				((BgL_ccastz00_bglt) BgL_oz00_5035), BgL_vz00_5036);
		}

	}



/* make-csequence */
	BGL_EXPORTED_DEF BgL_csequencez00_bglt
		BGl_makezd2csequencezd2zzcgen_copz00(obj_t BgL_loc1432z00_145,
		BgL_typez00_bglt BgL_type1433z00_146, bool_t BgL_czd2expzf31434z21_147,
		obj_t BgL_cops1435z00_148)
	{
		{	/* Cgen/cop.sch 498 */
			{	/* Cgen/cop.sch 498 */
				BgL_csequencez00_bglt BgL_new1339z00_6365;

				{	/* Cgen/cop.sch 498 */
					BgL_csequencez00_bglt BgL_new1338z00_6366;

					BgL_new1338z00_6366 =
						((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_csequencez00_bgl))));
					{	/* Cgen/cop.sch 498 */
						long BgL_arg1642z00_6367;

						BgL_arg1642z00_6367 = BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1338z00_6366), BgL_arg1642z00_6367);
					}
					BgL_new1339z00_6365 = BgL_new1338z00_6366;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1339z00_6365)))->BgL_locz00) =
					((obj_t) BgL_loc1432z00_145), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1339z00_6365)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1433z00_146), BUNSPEC);
				((((BgL_csequencez00_bglt) COBJECT(BgL_new1339z00_6365))->
						BgL_czd2expzf3z21) = ((bool_t) BgL_czd2expzf31434z21_147), BUNSPEC);
				((((BgL_csequencez00_bglt) COBJECT(BgL_new1339z00_6365))->BgL_copsz00) =
					((obj_t) BgL_cops1435z00_148), BUNSPEC);
				return BgL_new1339z00_6365;
			}
		}

	}



/* &make-csequence */
	BgL_csequencez00_bglt BGl_z62makezd2csequencezb0zzcgen_copz00(obj_t
		BgL_envz00_5037, obj_t BgL_loc1432z00_5038, obj_t BgL_type1433z00_5039,
		obj_t BgL_czd2expzf31434z21_5040, obj_t BgL_cops1435z00_5041)
	{
		{	/* Cgen/cop.sch 498 */
			return
				BGl_makezd2csequencezd2zzcgen_copz00(BgL_loc1432z00_5038,
				((BgL_typez00_bglt) BgL_type1433z00_5039),
				CBOOL(BgL_czd2expzf31434z21_5040), BgL_cops1435z00_5041);
		}

	}



/* csequence? */
	BGL_EXPORTED_DEF bool_t BGl_csequencezf3zf3zzcgen_copz00(obj_t BgL_objz00_149)
	{
		{	/* Cgen/cop.sch 499 */
			{	/* Cgen/cop.sch 499 */
				obj_t BgL_classz00_6368;

				BgL_classz00_6368 = BGl_csequencez00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_149))
					{	/* Cgen/cop.sch 499 */
						BgL_objectz00_bglt BgL_arg1807z00_6369;

						BgL_arg1807z00_6369 = (BgL_objectz00_bglt) (BgL_objz00_149);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 499 */
								long BgL_idxz00_6370;

								BgL_idxz00_6370 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6369);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6370 + 2L)) == BgL_classz00_6368);
							}
						else
							{	/* Cgen/cop.sch 499 */
								bool_t BgL_res2425z00_6373;

								{	/* Cgen/cop.sch 499 */
									obj_t BgL_oclassz00_6374;

									{	/* Cgen/cop.sch 499 */
										obj_t BgL_arg1815z00_6375;
										long BgL_arg1816z00_6376;

										BgL_arg1815z00_6375 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 499 */
											long BgL_arg1817z00_6377;

											BgL_arg1817z00_6377 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6369);
											BgL_arg1816z00_6376 = (BgL_arg1817z00_6377 - OBJECT_TYPE);
										}
										BgL_oclassz00_6374 =
											VECTOR_REF(BgL_arg1815z00_6375, BgL_arg1816z00_6376);
									}
									{	/* Cgen/cop.sch 499 */
										bool_t BgL__ortest_1115z00_6378;

										BgL__ortest_1115z00_6378 =
											(BgL_classz00_6368 == BgL_oclassz00_6374);
										if (BgL__ortest_1115z00_6378)
											{	/* Cgen/cop.sch 499 */
												BgL_res2425z00_6373 = BgL__ortest_1115z00_6378;
											}
										else
											{	/* Cgen/cop.sch 499 */
												long BgL_odepthz00_6379;

												{	/* Cgen/cop.sch 499 */
													obj_t BgL_arg1804z00_6380;

													BgL_arg1804z00_6380 = (BgL_oclassz00_6374);
													BgL_odepthz00_6379 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6380);
												}
												if ((2L < BgL_odepthz00_6379))
													{	/* Cgen/cop.sch 499 */
														obj_t BgL_arg1802z00_6381;

														{	/* Cgen/cop.sch 499 */
															obj_t BgL_arg1803z00_6382;

															BgL_arg1803z00_6382 = (BgL_oclassz00_6374);
															BgL_arg1802z00_6381 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6382,
																2L);
														}
														BgL_res2425z00_6373 =
															(BgL_arg1802z00_6381 == BgL_classz00_6368);
													}
												else
													{	/* Cgen/cop.sch 499 */
														BgL_res2425z00_6373 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2425z00_6373;
							}
					}
				else
					{	/* Cgen/cop.sch 499 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &csequence? */
	obj_t BGl_z62csequencezf3z91zzcgen_copz00(obj_t BgL_envz00_5042,
		obj_t BgL_objz00_5043)
	{
		{	/* Cgen/cop.sch 499 */
			return BBOOL(BGl_csequencezf3zf3zzcgen_copz00(BgL_objz00_5043));
		}

	}



/* csequence-nil */
	BGL_EXPORTED_DEF BgL_csequencez00_bglt
		BGl_csequencezd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 500 */
			{	/* Cgen/cop.sch 500 */
				obj_t BgL_classz00_3766;

				BgL_classz00_3766 = BGl_csequencez00zzcgen_copz00;
				{	/* Cgen/cop.sch 500 */
					obj_t BgL__ortest_1117z00_3767;

					BgL__ortest_1117z00_3767 = BGL_CLASS_NIL(BgL_classz00_3766);
					if (CBOOL(BgL__ortest_1117z00_3767))
						{	/* Cgen/cop.sch 500 */
							return ((BgL_csequencez00_bglt) BgL__ortest_1117z00_3767);
						}
					else
						{	/* Cgen/cop.sch 500 */
							return
								((BgL_csequencez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3766));
						}
				}
			}
		}

	}



/* &csequence-nil */
	BgL_csequencez00_bglt BGl_z62csequencezd2nilzb0zzcgen_copz00(obj_t
		BgL_envz00_5044)
	{
		{	/* Cgen/cop.sch 500 */
			return BGl_csequencezd2nilzd2zzcgen_copz00();
		}

	}



/* csequence-cops */
	BGL_EXPORTED_DEF obj_t
		BGl_csequencezd2copszd2zzcgen_copz00(BgL_csequencez00_bglt BgL_oz00_150)
	{
		{	/* Cgen/cop.sch 501 */
			return (((BgL_csequencez00_bglt) COBJECT(BgL_oz00_150))->BgL_copsz00);
		}

	}



/* &csequence-cops */
	obj_t BGl_z62csequencezd2copszb0zzcgen_copz00(obj_t BgL_envz00_5045,
		obj_t BgL_oz00_5046)
	{
		{	/* Cgen/cop.sch 501 */
			return
				BGl_csequencezd2copszd2zzcgen_copz00(
				((BgL_csequencez00_bglt) BgL_oz00_5046));
		}

	}



/* csequence-c-exp? */
	BGL_EXPORTED_DEF bool_t
		BGl_csequencezd2czd2expzf3zf3zzcgen_copz00(BgL_csequencez00_bglt
		BgL_oz00_153)
	{
		{	/* Cgen/cop.sch 503 */
			return
				(((BgL_csequencez00_bglt) COBJECT(BgL_oz00_153))->BgL_czd2expzf3z21);
		}

	}



/* &csequence-c-exp? */
	obj_t BGl_z62csequencezd2czd2expzf3z91zzcgen_copz00(obj_t BgL_envz00_5047,
		obj_t BgL_oz00_5048)
	{
		{	/* Cgen/cop.sch 503 */
			return
				BBOOL(BGl_csequencezd2czd2expzf3zf3zzcgen_copz00(
					((BgL_csequencez00_bglt) BgL_oz00_5048)));
		}

	}



/* csequence-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_csequencezd2typezd2zzcgen_copz00(BgL_csequencez00_bglt BgL_oz00_156)
	{
		{	/* Cgen/cop.sch 505 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_156)))->BgL_typez00);
		}

	}



/* &csequence-type */
	BgL_typez00_bglt BGl_z62csequencezd2typezb0zzcgen_copz00(obj_t
		BgL_envz00_5049, obj_t BgL_oz00_5050)
	{
		{	/* Cgen/cop.sch 505 */
			return
				BGl_csequencezd2typezd2zzcgen_copz00(
				((BgL_csequencez00_bglt) BgL_oz00_5050));
		}

	}



/* csequence-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_csequencezd2loczd2zzcgen_copz00(BgL_csequencez00_bglt BgL_oz00_159)
	{
		{	/* Cgen/cop.sch 507 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_159)))->BgL_locz00);
		}

	}



/* &csequence-loc */
	obj_t BGl_z62csequencezd2loczb0zzcgen_copz00(obj_t BgL_envz00_5051,
		obj_t BgL_oz00_5052)
	{
		{	/* Cgen/cop.sch 507 */
			return
				BGl_csequencezd2loczd2zzcgen_copz00(
				((BgL_csequencez00_bglt) BgL_oz00_5052));
		}

	}



/* csequence-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_csequencezd2loczd2setz12z12zzcgen_copz00(BgL_csequencez00_bglt
		BgL_oz00_160, obj_t BgL_vz00_161)
	{
		{	/* Cgen/cop.sch 508 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_160)))->BgL_locz00) =
				((obj_t) BgL_vz00_161), BUNSPEC);
		}

	}



/* &csequence-loc-set! */
	obj_t BGl_z62csequencezd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5053,
		obj_t BgL_oz00_5054, obj_t BgL_vz00_5055)
	{
		{	/* Cgen/cop.sch 508 */
			return
				BGl_csequencezd2loczd2setz12z12zzcgen_copz00(
				((BgL_csequencez00_bglt) BgL_oz00_5054), BgL_vz00_5055);
		}

	}



/* make-nop */
	BGL_EXPORTED_DEF BgL_nopz00_bglt BGl_makezd2nopzd2zzcgen_copz00(obj_t
		BgL_loc1429z00_162, BgL_typez00_bglt BgL_type1430z00_163)
	{
		{	/* Cgen/cop.sch 511 */
			{	/* Cgen/cop.sch 511 */
				BgL_nopz00_bglt BgL_new1341z00_6383;

				{	/* Cgen/cop.sch 511 */
					BgL_nopz00_bglt BgL_new1340z00_6384;

					BgL_new1340z00_6384 =
						((BgL_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_nopz00_bgl))));
					{	/* Cgen/cop.sch 511 */
						long BgL_arg1646z00_6385;

						BgL_arg1646z00_6385 = BGL_CLASS_NUM(BGl_nopz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1340z00_6384), BgL_arg1646z00_6385);
					}
					BgL_new1341z00_6383 = BgL_new1340z00_6384;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1341z00_6383)))->BgL_locz00) =
					((obj_t) BgL_loc1429z00_162), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1341z00_6383)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1430z00_163), BUNSPEC);
				return BgL_new1341z00_6383;
			}
		}

	}



/* &make-nop */
	BgL_nopz00_bglt BGl_z62makezd2nopzb0zzcgen_copz00(obj_t BgL_envz00_5056,
		obj_t BgL_loc1429z00_5057, obj_t BgL_type1430z00_5058)
	{
		{	/* Cgen/cop.sch 511 */
			return
				BGl_makezd2nopzd2zzcgen_copz00(BgL_loc1429z00_5057,
				((BgL_typez00_bglt) BgL_type1430z00_5058));
		}

	}



/* nop? */
	BGL_EXPORTED_DEF bool_t BGl_nopzf3zf3zzcgen_copz00(obj_t BgL_objz00_164)
	{
		{	/* Cgen/cop.sch 512 */
			{	/* Cgen/cop.sch 512 */
				obj_t BgL_classz00_6386;

				BgL_classz00_6386 = BGl_nopz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_164))
					{	/* Cgen/cop.sch 512 */
						BgL_objectz00_bglt BgL_arg1807z00_6387;

						BgL_arg1807z00_6387 = (BgL_objectz00_bglt) (BgL_objz00_164);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 512 */
								long BgL_idxz00_6388;

								BgL_idxz00_6388 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6387);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6388 + 2L)) == BgL_classz00_6386);
							}
						else
							{	/* Cgen/cop.sch 512 */
								bool_t BgL_res2426z00_6391;

								{	/* Cgen/cop.sch 512 */
									obj_t BgL_oclassz00_6392;

									{	/* Cgen/cop.sch 512 */
										obj_t BgL_arg1815z00_6393;
										long BgL_arg1816z00_6394;

										BgL_arg1815z00_6393 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 512 */
											long BgL_arg1817z00_6395;

											BgL_arg1817z00_6395 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6387);
											BgL_arg1816z00_6394 = (BgL_arg1817z00_6395 - OBJECT_TYPE);
										}
										BgL_oclassz00_6392 =
											VECTOR_REF(BgL_arg1815z00_6393, BgL_arg1816z00_6394);
									}
									{	/* Cgen/cop.sch 512 */
										bool_t BgL__ortest_1115z00_6396;

										BgL__ortest_1115z00_6396 =
											(BgL_classz00_6386 == BgL_oclassz00_6392);
										if (BgL__ortest_1115z00_6396)
											{	/* Cgen/cop.sch 512 */
												BgL_res2426z00_6391 = BgL__ortest_1115z00_6396;
											}
										else
											{	/* Cgen/cop.sch 512 */
												long BgL_odepthz00_6397;

												{	/* Cgen/cop.sch 512 */
													obj_t BgL_arg1804z00_6398;

													BgL_arg1804z00_6398 = (BgL_oclassz00_6392);
													BgL_odepthz00_6397 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6398);
												}
												if ((2L < BgL_odepthz00_6397))
													{	/* Cgen/cop.sch 512 */
														obj_t BgL_arg1802z00_6399;

														{	/* Cgen/cop.sch 512 */
															obj_t BgL_arg1803z00_6400;

															BgL_arg1803z00_6400 = (BgL_oclassz00_6392);
															BgL_arg1802z00_6399 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6400,
																2L);
														}
														BgL_res2426z00_6391 =
															(BgL_arg1802z00_6399 == BgL_classz00_6386);
													}
												else
													{	/* Cgen/cop.sch 512 */
														BgL_res2426z00_6391 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2426z00_6391;
							}
					}
				else
					{	/* Cgen/cop.sch 512 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &nop? */
	obj_t BGl_z62nopzf3z91zzcgen_copz00(obj_t BgL_envz00_5059,
		obj_t BgL_objz00_5060)
	{
		{	/* Cgen/cop.sch 512 */
			return BBOOL(BGl_nopzf3zf3zzcgen_copz00(BgL_objz00_5060));
		}

	}



/* nop-nil */
	BGL_EXPORTED_DEF BgL_nopz00_bglt BGl_nopzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 513 */
			{	/* Cgen/cop.sch 513 */
				obj_t BgL_classz00_3808;

				BgL_classz00_3808 = BGl_nopz00zzcgen_copz00;
				{	/* Cgen/cop.sch 513 */
					obj_t BgL__ortest_1117z00_3809;

					BgL__ortest_1117z00_3809 = BGL_CLASS_NIL(BgL_classz00_3808);
					if (CBOOL(BgL__ortest_1117z00_3809))
						{	/* Cgen/cop.sch 513 */
							return ((BgL_nopz00_bglt) BgL__ortest_1117z00_3809);
						}
					else
						{	/* Cgen/cop.sch 513 */
							return
								((BgL_nopz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3808));
						}
				}
			}
		}

	}



/* &nop-nil */
	BgL_nopz00_bglt BGl_z62nopzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_5061)
	{
		{	/* Cgen/cop.sch 513 */
			return BGl_nopzd2nilzd2zzcgen_copz00();
		}

	}



/* nop-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_nopzd2typezd2zzcgen_copz00(BgL_nopz00_bglt BgL_oz00_165)
	{
		{	/* Cgen/cop.sch 514 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_165)))->BgL_typez00);
		}

	}



/* &nop-type */
	BgL_typez00_bglt BGl_z62nopzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5062,
		obj_t BgL_oz00_5063)
	{
		{	/* Cgen/cop.sch 514 */
			return BGl_nopzd2typezd2zzcgen_copz00(((BgL_nopz00_bglt) BgL_oz00_5063));
		}

	}



/* nop-loc */
	BGL_EXPORTED_DEF obj_t BGl_nopzd2loczd2zzcgen_copz00(BgL_nopz00_bglt
		BgL_oz00_168)
	{
		{	/* Cgen/cop.sch 516 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_168)))->BgL_locz00);
		}

	}



/* &nop-loc */
	obj_t BGl_z62nopzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5064,
		obj_t BgL_oz00_5065)
	{
		{	/* Cgen/cop.sch 516 */
			return BGl_nopzd2loczd2zzcgen_copz00(((BgL_nopz00_bglt) BgL_oz00_5065));
		}

	}



/* nop-loc-set! */
	BGL_EXPORTED_DEF obj_t BGl_nopzd2loczd2setz12z12zzcgen_copz00(BgL_nopz00_bglt
		BgL_oz00_169, obj_t BgL_vz00_170)
	{
		{	/* Cgen/cop.sch 517 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_169)))->BgL_locz00) =
				((obj_t) BgL_vz00_170), BUNSPEC);
		}

	}



/* &nop-loc-set! */
	obj_t BGl_z62nopzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5066,
		obj_t BgL_oz00_5067, obj_t BgL_vz00_5068)
	{
		{	/* Cgen/cop.sch 517 */
			return
				BGl_nopzd2loczd2setz12z12zzcgen_copz00(
				((BgL_nopz00_bglt) BgL_oz00_5067), BgL_vz00_5068);
		}

	}



/* make-stop */
	BGL_EXPORTED_DEF BgL_stopz00_bglt BGl_makezd2stopzd2zzcgen_copz00(obj_t
		BgL_loc1425z00_171, BgL_typez00_bglt BgL_type1426z00_172,
		BgL_copz00_bglt BgL_value1427z00_173)
	{
		{	/* Cgen/cop.sch 520 */
			{	/* Cgen/cop.sch 520 */
				BgL_stopz00_bglt BgL_new1343z00_6401;

				{	/* Cgen/cop.sch 520 */
					BgL_stopz00_bglt BgL_new1342z00_6402;

					BgL_new1342z00_6402 =
						((BgL_stopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_stopz00_bgl))));
					{	/* Cgen/cop.sch 520 */
						long BgL_arg1650z00_6403;

						BgL_arg1650z00_6403 = BGL_CLASS_NUM(BGl_stopz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1342z00_6402), BgL_arg1650z00_6403);
					}
					BgL_new1343z00_6401 = BgL_new1342z00_6402;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1343z00_6401)))->BgL_locz00) =
					((obj_t) BgL_loc1425z00_171), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1343z00_6401)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1426z00_172), BUNSPEC);
				((((BgL_stopz00_bglt) COBJECT(BgL_new1343z00_6401))->BgL_valuez00) =
					((BgL_copz00_bglt) BgL_value1427z00_173), BUNSPEC);
				return BgL_new1343z00_6401;
			}
		}

	}



/* &make-stop */
	BgL_stopz00_bglt BGl_z62makezd2stopzb0zzcgen_copz00(obj_t BgL_envz00_5069,
		obj_t BgL_loc1425z00_5070, obj_t BgL_type1426z00_5071,
		obj_t BgL_value1427z00_5072)
	{
		{	/* Cgen/cop.sch 520 */
			return
				BGl_makezd2stopzd2zzcgen_copz00(BgL_loc1425z00_5070,
				((BgL_typez00_bglt) BgL_type1426z00_5071),
				((BgL_copz00_bglt) BgL_value1427z00_5072));
		}

	}



/* stop? */
	BGL_EXPORTED_DEF bool_t BGl_stopzf3zf3zzcgen_copz00(obj_t BgL_objz00_174)
	{
		{	/* Cgen/cop.sch 521 */
			{	/* Cgen/cop.sch 521 */
				obj_t BgL_classz00_6404;

				BgL_classz00_6404 = BGl_stopz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_174))
					{	/* Cgen/cop.sch 521 */
						BgL_objectz00_bglt BgL_arg1807z00_6405;

						BgL_arg1807z00_6405 = (BgL_objectz00_bglt) (BgL_objz00_174);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 521 */
								long BgL_idxz00_6406;

								BgL_idxz00_6406 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6405);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6406 + 2L)) == BgL_classz00_6404);
							}
						else
							{	/* Cgen/cop.sch 521 */
								bool_t BgL_res2427z00_6409;

								{	/* Cgen/cop.sch 521 */
									obj_t BgL_oclassz00_6410;

									{	/* Cgen/cop.sch 521 */
										obj_t BgL_arg1815z00_6411;
										long BgL_arg1816z00_6412;

										BgL_arg1815z00_6411 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 521 */
											long BgL_arg1817z00_6413;

											BgL_arg1817z00_6413 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6405);
											BgL_arg1816z00_6412 = (BgL_arg1817z00_6413 - OBJECT_TYPE);
										}
										BgL_oclassz00_6410 =
											VECTOR_REF(BgL_arg1815z00_6411, BgL_arg1816z00_6412);
									}
									{	/* Cgen/cop.sch 521 */
										bool_t BgL__ortest_1115z00_6414;

										BgL__ortest_1115z00_6414 =
											(BgL_classz00_6404 == BgL_oclassz00_6410);
										if (BgL__ortest_1115z00_6414)
											{	/* Cgen/cop.sch 521 */
												BgL_res2427z00_6409 = BgL__ortest_1115z00_6414;
											}
										else
											{	/* Cgen/cop.sch 521 */
												long BgL_odepthz00_6415;

												{	/* Cgen/cop.sch 521 */
													obj_t BgL_arg1804z00_6416;

													BgL_arg1804z00_6416 = (BgL_oclassz00_6410);
													BgL_odepthz00_6415 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6416);
												}
												if ((2L < BgL_odepthz00_6415))
													{	/* Cgen/cop.sch 521 */
														obj_t BgL_arg1802z00_6417;

														{	/* Cgen/cop.sch 521 */
															obj_t BgL_arg1803z00_6418;

															BgL_arg1803z00_6418 = (BgL_oclassz00_6410);
															BgL_arg1802z00_6417 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6418,
																2L);
														}
														BgL_res2427z00_6409 =
															(BgL_arg1802z00_6417 == BgL_classz00_6404);
													}
												else
													{	/* Cgen/cop.sch 521 */
														BgL_res2427z00_6409 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2427z00_6409;
							}
					}
				else
					{	/* Cgen/cop.sch 521 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &stop? */
	obj_t BGl_z62stopzf3z91zzcgen_copz00(obj_t BgL_envz00_5073,
		obj_t BgL_objz00_5074)
	{
		{	/* Cgen/cop.sch 521 */
			return BBOOL(BGl_stopzf3zf3zzcgen_copz00(BgL_objz00_5074));
		}

	}



/* stop-nil */
	BGL_EXPORTED_DEF BgL_stopz00_bglt BGl_stopzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 522 */
			{	/* Cgen/cop.sch 522 */
				obj_t BgL_classz00_3850;

				BgL_classz00_3850 = BGl_stopz00zzcgen_copz00;
				{	/* Cgen/cop.sch 522 */
					obj_t BgL__ortest_1117z00_3851;

					BgL__ortest_1117z00_3851 = BGL_CLASS_NIL(BgL_classz00_3850);
					if (CBOOL(BgL__ortest_1117z00_3851))
						{	/* Cgen/cop.sch 522 */
							return ((BgL_stopz00_bglt) BgL__ortest_1117z00_3851);
						}
					else
						{	/* Cgen/cop.sch 522 */
							return
								((BgL_stopz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3850));
						}
				}
			}
		}

	}



/* &stop-nil */
	BgL_stopz00_bglt BGl_z62stopzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_5075)
	{
		{	/* Cgen/cop.sch 522 */
			return BGl_stopzd2nilzd2zzcgen_copz00();
		}

	}



/* stop-value */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_stopzd2valuezd2zzcgen_copz00(BgL_stopz00_bglt BgL_oz00_175)
	{
		{	/* Cgen/cop.sch 523 */
			return (((BgL_stopz00_bglt) COBJECT(BgL_oz00_175))->BgL_valuez00);
		}

	}



/* &stop-value */
	BgL_copz00_bglt BGl_z62stopzd2valuezb0zzcgen_copz00(obj_t BgL_envz00_5076,
		obj_t BgL_oz00_5077)
	{
		{	/* Cgen/cop.sch 523 */
			return
				BGl_stopzd2valuezd2zzcgen_copz00(((BgL_stopz00_bglt) BgL_oz00_5077));
		}

	}



/* stop-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_stopzd2typezd2zzcgen_copz00(BgL_stopz00_bglt BgL_oz00_178)
	{
		{	/* Cgen/cop.sch 525 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_178)))->BgL_typez00);
		}

	}



/* &stop-type */
	BgL_typez00_bglt BGl_z62stopzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5078,
		obj_t BgL_oz00_5079)
	{
		{	/* Cgen/cop.sch 525 */
			return
				BGl_stopzd2typezd2zzcgen_copz00(((BgL_stopz00_bglt) BgL_oz00_5079));
		}

	}



/* stop-loc */
	BGL_EXPORTED_DEF obj_t BGl_stopzd2loczd2zzcgen_copz00(BgL_stopz00_bglt
		BgL_oz00_181)
	{
		{	/* Cgen/cop.sch 527 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_181)))->BgL_locz00);
		}

	}



/* &stop-loc */
	obj_t BGl_z62stopzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5080,
		obj_t BgL_oz00_5081)
	{
		{	/* Cgen/cop.sch 527 */
			return BGl_stopzd2loczd2zzcgen_copz00(((BgL_stopz00_bglt) BgL_oz00_5081));
		}

	}



/* stop-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_stopzd2loczd2setz12z12zzcgen_copz00(BgL_stopz00_bglt BgL_oz00_182,
		obj_t BgL_vz00_183)
	{
		{	/* Cgen/cop.sch 528 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_182)))->BgL_locz00) =
				((obj_t) BgL_vz00_183), BUNSPEC);
		}

	}



/* &stop-loc-set! */
	obj_t BGl_z62stopzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5082,
		obj_t BgL_oz00_5083, obj_t BgL_vz00_5084)
	{
		{	/* Cgen/cop.sch 528 */
			return
				BGl_stopzd2loczd2setz12z12zzcgen_copz00(
				((BgL_stopz00_bglt) BgL_oz00_5083), BgL_vz00_5084);
		}

	}



/* make-csetq */
	BGL_EXPORTED_DEF BgL_csetqz00_bglt BGl_makezd2csetqzd2zzcgen_copz00(obj_t
		BgL_loc1420z00_184, BgL_typez00_bglt BgL_type1421z00_185,
		BgL_varcz00_bglt BgL_var1422z00_186, BgL_copz00_bglt BgL_value1423z00_187)
	{
		{	/* Cgen/cop.sch 531 */
			{	/* Cgen/cop.sch 531 */
				BgL_csetqz00_bglt BgL_new1346z00_6419;

				{	/* Cgen/cop.sch 531 */
					BgL_csetqz00_bglt BgL_new1345z00_6420;

					BgL_new1345z00_6420 =
						((BgL_csetqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_csetqz00_bgl))));
					{	/* Cgen/cop.sch 531 */
						long BgL_arg1651z00_6421;

						BgL_arg1651z00_6421 = BGL_CLASS_NUM(BGl_csetqz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1345z00_6420), BgL_arg1651z00_6421);
					}
					BgL_new1346z00_6419 = BgL_new1345z00_6420;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1346z00_6419)))->BgL_locz00) =
					((obj_t) BgL_loc1420z00_184), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1346z00_6419)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1421z00_185), BUNSPEC);
				((((BgL_csetqz00_bglt) COBJECT(BgL_new1346z00_6419))->BgL_varz00) =
					((BgL_varcz00_bglt) BgL_var1422z00_186), BUNSPEC);
				((((BgL_csetqz00_bglt) COBJECT(BgL_new1346z00_6419))->BgL_valuez00) =
					((BgL_copz00_bglt) BgL_value1423z00_187), BUNSPEC);
				return BgL_new1346z00_6419;
			}
		}

	}



/* &make-csetq */
	BgL_csetqz00_bglt BGl_z62makezd2csetqzb0zzcgen_copz00(obj_t BgL_envz00_5085,
		obj_t BgL_loc1420z00_5086, obj_t BgL_type1421z00_5087,
		obj_t BgL_var1422z00_5088, obj_t BgL_value1423z00_5089)
	{
		{	/* Cgen/cop.sch 531 */
			return
				BGl_makezd2csetqzd2zzcgen_copz00(BgL_loc1420z00_5086,
				((BgL_typez00_bglt) BgL_type1421z00_5087),
				((BgL_varcz00_bglt) BgL_var1422z00_5088),
				((BgL_copz00_bglt) BgL_value1423z00_5089));
		}

	}



/* csetq? */
	BGL_EXPORTED_DEF bool_t BGl_csetqzf3zf3zzcgen_copz00(obj_t BgL_objz00_188)
	{
		{	/* Cgen/cop.sch 532 */
			{	/* Cgen/cop.sch 532 */
				obj_t BgL_classz00_6422;

				BgL_classz00_6422 = BGl_csetqz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_188))
					{	/* Cgen/cop.sch 532 */
						BgL_objectz00_bglt BgL_arg1807z00_6423;

						BgL_arg1807z00_6423 = (BgL_objectz00_bglt) (BgL_objz00_188);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 532 */
								long BgL_idxz00_6424;

								BgL_idxz00_6424 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6423);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6424 + 2L)) == BgL_classz00_6422);
							}
						else
							{	/* Cgen/cop.sch 532 */
								bool_t BgL_res2428z00_6427;

								{	/* Cgen/cop.sch 532 */
									obj_t BgL_oclassz00_6428;

									{	/* Cgen/cop.sch 532 */
										obj_t BgL_arg1815z00_6429;
										long BgL_arg1816z00_6430;

										BgL_arg1815z00_6429 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 532 */
											long BgL_arg1817z00_6431;

											BgL_arg1817z00_6431 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6423);
											BgL_arg1816z00_6430 = (BgL_arg1817z00_6431 - OBJECT_TYPE);
										}
										BgL_oclassz00_6428 =
											VECTOR_REF(BgL_arg1815z00_6429, BgL_arg1816z00_6430);
									}
									{	/* Cgen/cop.sch 532 */
										bool_t BgL__ortest_1115z00_6432;

										BgL__ortest_1115z00_6432 =
											(BgL_classz00_6422 == BgL_oclassz00_6428);
										if (BgL__ortest_1115z00_6432)
											{	/* Cgen/cop.sch 532 */
												BgL_res2428z00_6427 = BgL__ortest_1115z00_6432;
											}
										else
											{	/* Cgen/cop.sch 532 */
												long BgL_odepthz00_6433;

												{	/* Cgen/cop.sch 532 */
													obj_t BgL_arg1804z00_6434;

													BgL_arg1804z00_6434 = (BgL_oclassz00_6428);
													BgL_odepthz00_6433 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6434);
												}
												if ((2L < BgL_odepthz00_6433))
													{	/* Cgen/cop.sch 532 */
														obj_t BgL_arg1802z00_6435;

														{	/* Cgen/cop.sch 532 */
															obj_t BgL_arg1803z00_6436;

															BgL_arg1803z00_6436 = (BgL_oclassz00_6428);
															BgL_arg1802z00_6435 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6436,
																2L);
														}
														BgL_res2428z00_6427 =
															(BgL_arg1802z00_6435 == BgL_classz00_6422);
													}
												else
													{	/* Cgen/cop.sch 532 */
														BgL_res2428z00_6427 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2428z00_6427;
							}
					}
				else
					{	/* Cgen/cop.sch 532 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &csetq? */
	obj_t BGl_z62csetqzf3z91zzcgen_copz00(obj_t BgL_envz00_5090,
		obj_t BgL_objz00_5091)
	{
		{	/* Cgen/cop.sch 532 */
			return BBOOL(BGl_csetqzf3zf3zzcgen_copz00(BgL_objz00_5091));
		}

	}



/* csetq-nil */
	BGL_EXPORTED_DEF BgL_csetqz00_bglt BGl_csetqzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 533 */
			{	/* Cgen/cop.sch 533 */
				obj_t BgL_classz00_3892;

				BgL_classz00_3892 = BGl_csetqz00zzcgen_copz00;
				{	/* Cgen/cop.sch 533 */
					obj_t BgL__ortest_1117z00_3893;

					BgL__ortest_1117z00_3893 = BGL_CLASS_NIL(BgL_classz00_3892);
					if (CBOOL(BgL__ortest_1117z00_3893))
						{	/* Cgen/cop.sch 533 */
							return ((BgL_csetqz00_bglt) BgL__ortest_1117z00_3893);
						}
					else
						{	/* Cgen/cop.sch 533 */
							return
								((BgL_csetqz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3892));
						}
				}
			}
		}

	}



/* &csetq-nil */
	BgL_csetqz00_bglt BGl_z62csetqzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_5092)
	{
		{	/* Cgen/cop.sch 533 */
			return BGl_csetqzd2nilzd2zzcgen_copz00();
		}

	}



/* csetq-value */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_csetqzd2valuezd2zzcgen_copz00(BgL_csetqz00_bglt BgL_oz00_189)
	{
		{	/* Cgen/cop.sch 534 */
			return (((BgL_csetqz00_bglt) COBJECT(BgL_oz00_189))->BgL_valuez00);
		}

	}



/* &csetq-value */
	BgL_copz00_bglt BGl_z62csetqzd2valuezb0zzcgen_copz00(obj_t BgL_envz00_5093,
		obj_t BgL_oz00_5094)
	{
		{	/* Cgen/cop.sch 534 */
			return
				BGl_csetqzd2valuezd2zzcgen_copz00(((BgL_csetqz00_bglt) BgL_oz00_5094));
		}

	}



/* csetq-var */
	BGL_EXPORTED_DEF BgL_varcz00_bglt
		BGl_csetqzd2varzd2zzcgen_copz00(BgL_csetqz00_bglt BgL_oz00_192)
	{
		{	/* Cgen/cop.sch 536 */
			return (((BgL_csetqz00_bglt) COBJECT(BgL_oz00_192))->BgL_varz00);
		}

	}



/* &csetq-var */
	BgL_varcz00_bglt BGl_z62csetqzd2varzb0zzcgen_copz00(obj_t BgL_envz00_5095,
		obj_t BgL_oz00_5096)
	{
		{	/* Cgen/cop.sch 536 */
			return
				BGl_csetqzd2varzd2zzcgen_copz00(((BgL_csetqz00_bglt) BgL_oz00_5096));
		}

	}



/* csetq-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_csetqzd2typezd2zzcgen_copz00(BgL_csetqz00_bglt BgL_oz00_195)
	{
		{	/* Cgen/cop.sch 538 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_195)))->BgL_typez00);
		}

	}



/* &csetq-type */
	BgL_typez00_bglt BGl_z62csetqzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5097,
		obj_t BgL_oz00_5098)
	{
		{	/* Cgen/cop.sch 538 */
			return
				BGl_csetqzd2typezd2zzcgen_copz00(((BgL_csetqz00_bglt) BgL_oz00_5098));
		}

	}



/* csetq-loc */
	BGL_EXPORTED_DEF obj_t BGl_csetqzd2loczd2zzcgen_copz00(BgL_csetqz00_bglt
		BgL_oz00_198)
	{
		{	/* Cgen/cop.sch 540 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_198)))->BgL_locz00);
		}

	}



/* &csetq-loc */
	obj_t BGl_z62csetqzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5099,
		obj_t BgL_oz00_5100)
	{
		{	/* Cgen/cop.sch 540 */
			return
				BGl_csetqzd2loczd2zzcgen_copz00(((BgL_csetqz00_bglt) BgL_oz00_5100));
		}

	}



/* csetq-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_csetqzd2loczd2setz12z12zzcgen_copz00(BgL_csetqz00_bglt BgL_oz00_199,
		obj_t BgL_vz00_200)
	{
		{	/* Cgen/cop.sch 541 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_199)))->BgL_locz00) =
				((obj_t) BgL_vz00_200), BUNSPEC);
		}

	}



/* &csetq-loc-set! */
	obj_t BGl_z62csetqzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5101,
		obj_t BgL_oz00_5102, obj_t BgL_vz00_5103)
	{
		{	/* Cgen/cop.sch 541 */
			return
				BGl_csetqzd2loczd2setz12z12zzcgen_copz00(
				((BgL_csetqz00_bglt) BgL_oz00_5102), BgL_vz00_5103);
		}

	}



/* make-cif */
	BGL_EXPORTED_DEF BgL_cifz00_bglt BGl_makezd2cifzd2zzcgen_copz00(obj_t
		BgL_loc1414z00_201, BgL_typez00_bglt BgL_type1415z00_202,
		BgL_copz00_bglt BgL_test1416z00_203, BgL_copz00_bglt BgL_true1417z00_204,
		BgL_copz00_bglt BgL_false1418z00_205)
	{
		{	/* Cgen/cop.sch 544 */
			{	/* Cgen/cop.sch 544 */
				BgL_cifz00_bglt BgL_new1348z00_6437;

				{	/* Cgen/cop.sch 544 */
					BgL_cifz00_bglt BgL_new1347z00_6438;

					BgL_new1347z00_6438 =
						((BgL_cifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cifz00_bgl))));
					{	/* Cgen/cop.sch 544 */
						long BgL_arg1654z00_6439;

						BgL_arg1654z00_6439 = BGL_CLASS_NUM(BGl_cifz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1347z00_6438), BgL_arg1654z00_6439);
					}
					BgL_new1348z00_6437 = BgL_new1347z00_6438;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1348z00_6437)))->BgL_locz00) =
					((obj_t) BgL_loc1414z00_201), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1348z00_6437)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1415z00_202), BUNSPEC);
				((((BgL_cifz00_bglt) COBJECT(BgL_new1348z00_6437))->BgL_testz00) =
					((BgL_copz00_bglt) BgL_test1416z00_203), BUNSPEC);
				((((BgL_cifz00_bglt) COBJECT(BgL_new1348z00_6437))->BgL_truez00) =
					((BgL_copz00_bglt) BgL_true1417z00_204), BUNSPEC);
				((((BgL_cifz00_bglt) COBJECT(BgL_new1348z00_6437))->BgL_falsez00) =
					((BgL_copz00_bglt) BgL_false1418z00_205), BUNSPEC);
				return BgL_new1348z00_6437;
			}
		}

	}



/* &make-cif */
	BgL_cifz00_bglt BGl_z62makezd2cifzb0zzcgen_copz00(obj_t BgL_envz00_5104,
		obj_t BgL_loc1414z00_5105, obj_t BgL_type1415z00_5106,
		obj_t BgL_test1416z00_5107, obj_t BgL_true1417z00_5108,
		obj_t BgL_false1418z00_5109)
	{
		{	/* Cgen/cop.sch 544 */
			return
				BGl_makezd2cifzd2zzcgen_copz00(BgL_loc1414z00_5105,
				((BgL_typez00_bglt) BgL_type1415z00_5106),
				((BgL_copz00_bglt) BgL_test1416z00_5107),
				((BgL_copz00_bglt) BgL_true1417z00_5108),
				((BgL_copz00_bglt) BgL_false1418z00_5109));
		}

	}



/* cif? */
	BGL_EXPORTED_DEF bool_t BGl_cifzf3zf3zzcgen_copz00(obj_t BgL_objz00_206)
	{
		{	/* Cgen/cop.sch 545 */
			{	/* Cgen/cop.sch 545 */
				obj_t BgL_classz00_6440;

				BgL_classz00_6440 = BGl_cifz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_206))
					{	/* Cgen/cop.sch 545 */
						BgL_objectz00_bglt BgL_arg1807z00_6441;

						BgL_arg1807z00_6441 = (BgL_objectz00_bglt) (BgL_objz00_206);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 545 */
								long BgL_idxz00_6442;

								BgL_idxz00_6442 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6441);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6442 + 2L)) == BgL_classz00_6440);
							}
						else
							{	/* Cgen/cop.sch 545 */
								bool_t BgL_res2429z00_6445;

								{	/* Cgen/cop.sch 545 */
									obj_t BgL_oclassz00_6446;

									{	/* Cgen/cop.sch 545 */
										obj_t BgL_arg1815z00_6447;
										long BgL_arg1816z00_6448;

										BgL_arg1815z00_6447 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 545 */
											long BgL_arg1817z00_6449;

											BgL_arg1817z00_6449 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6441);
											BgL_arg1816z00_6448 = (BgL_arg1817z00_6449 - OBJECT_TYPE);
										}
										BgL_oclassz00_6446 =
											VECTOR_REF(BgL_arg1815z00_6447, BgL_arg1816z00_6448);
									}
									{	/* Cgen/cop.sch 545 */
										bool_t BgL__ortest_1115z00_6450;

										BgL__ortest_1115z00_6450 =
											(BgL_classz00_6440 == BgL_oclassz00_6446);
										if (BgL__ortest_1115z00_6450)
											{	/* Cgen/cop.sch 545 */
												BgL_res2429z00_6445 = BgL__ortest_1115z00_6450;
											}
										else
											{	/* Cgen/cop.sch 545 */
												long BgL_odepthz00_6451;

												{	/* Cgen/cop.sch 545 */
													obj_t BgL_arg1804z00_6452;

													BgL_arg1804z00_6452 = (BgL_oclassz00_6446);
													BgL_odepthz00_6451 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6452);
												}
												if ((2L < BgL_odepthz00_6451))
													{	/* Cgen/cop.sch 545 */
														obj_t BgL_arg1802z00_6453;

														{	/* Cgen/cop.sch 545 */
															obj_t BgL_arg1803z00_6454;

															BgL_arg1803z00_6454 = (BgL_oclassz00_6446);
															BgL_arg1802z00_6453 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6454,
																2L);
														}
														BgL_res2429z00_6445 =
															(BgL_arg1802z00_6453 == BgL_classz00_6440);
													}
												else
													{	/* Cgen/cop.sch 545 */
														BgL_res2429z00_6445 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2429z00_6445;
							}
					}
				else
					{	/* Cgen/cop.sch 545 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cif? */
	obj_t BGl_z62cifzf3z91zzcgen_copz00(obj_t BgL_envz00_5110,
		obj_t BgL_objz00_5111)
	{
		{	/* Cgen/cop.sch 545 */
			return BBOOL(BGl_cifzf3zf3zzcgen_copz00(BgL_objz00_5111));
		}

	}



/* cif-nil */
	BGL_EXPORTED_DEF BgL_cifz00_bglt BGl_cifzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 546 */
			{	/* Cgen/cop.sch 546 */
				obj_t BgL_classz00_3934;

				BgL_classz00_3934 = BGl_cifz00zzcgen_copz00;
				{	/* Cgen/cop.sch 546 */
					obj_t BgL__ortest_1117z00_3935;

					BgL__ortest_1117z00_3935 = BGL_CLASS_NIL(BgL_classz00_3934);
					if (CBOOL(BgL__ortest_1117z00_3935))
						{	/* Cgen/cop.sch 546 */
							return ((BgL_cifz00_bglt) BgL__ortest_1117z00_3935);
						}
					else
						{	/* Cgen/cop.sch 546 */
							return
								((BgL_cifz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3934));
						}
				}
			}
		}

	}



/* &cif-nil */
	BgL_cifz00_bglt BGl_z62cifzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_5112)
	{
		{	/* Cgen/cop.sch 546 */
			return BGl_cifzd2nilzd2zzcgen_copz00();
		}

	}



/* cif-false */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cifzd2falsezd2zzcgen_copz00(BgL_cifz00_bglt BgL_oz00_207)
	{
		{	/* Cgen/cop.sch 547 */
			return (((BgL_cifz00_bglt) COBJECT(BgL_oz00_207))->BgL_falsez00);
		}

	}



/* &cif-false */
	BgL_copz00_bglt BGl_z62cifzd2falsezb0zzcgen_copz00(obj_t BgL_envz00_5113,
		obj_t BgL_oz00_5114)
	{
		{	/* Cgen/cop.sch 547 */
			return BGl_cifzd2falsezd2zzcgen_copz00(((BgL_cifz00_bglt) BgL_oz00_5114));
		}

	}



/* cif-true */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cifzd2truezd2zzcgen_copz00(BgL_cifz00_bglt BgL_oz00_210)
	{
		{	/* Cgen/cop.sch 549 */
			return (((BgL_cifz00_bglt) COBJECT(BgL_oz00_210))->BgL_truez00);
		}

	}



/* &cif-true */
	BgL_copz00_bglt BGl_z62cifzd2truezb0zzcgen_copz00(obj_t BgL_envz00_5115,
		obj_t BgL_oz00_5116)
	{
		{	/* Cgen/cop.sch 549 */
			return BGl_cifzd2truezd2zzcgen_copz00(((BgL_cifz00_bglt) BgL_oz00_5116));
		}

	}



/* cif-test */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cifzd2testzd2zzcgen_copz00(BgL_cifz00_bglt BgL_oz00_213)
	{
		{	/* Cgen/cop.sch 551 */
			return (((BgL_cifz00_bglt) COBJECT(BgL_oz00_213))->BgL_testz00);
		}

	}



/* &cif-test */
	BgL_copz00_bglt BGl_z62cifzd2testzb0zzcgen_copz00(obj_t BgL_envz00_5117,
		obj_t BgL_oz00_5118)
	{
		{	/* Cgen/cop.sch 551 */
			return BGl_cifzd2testzd2zzcgen_copz00(((BgL_cifz00_bglt) BgL_oz00_5118));
		}

	}



/* cif-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cifzd2typezd2zzcgen_copz00(BgL_cifz00_bglt BgL_oz00_216)
	{
		{	/* Cgen/cop.sch 553 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_216)))->BgL_typez00);
		}

	}



/* &cif-type */
	BgL_typez00_bglt BGl_z62cifzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5119,
		obj_t BgL_oz00_5120)
	{
		{	/* Cgen/cop.sch 553 */
			return BGl_cifzd2typezd2zzcgen_copz00(((BgL_cifz00_bglt) BgL_oz00_5120));
		}

	}



/* cif-loc */
	BGL_EXPORTED_DEF obj_t BGl_cifzd2loczd2zzcgen_copz00(BgL_cifz00_bglt
		BgL_oz00_219)
	{
		{	/* Cgen/cop.sch 555 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_219)))->BgL_locz00);
		}

	}



/* &cif-loc */
	obj_t BGl_z62cifzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5121,
		obj_t BgL_oz00_5122)
	{
		{	/* Cgen/cop.sch 555 */
			return BGl_cifzd2loczd2zzcgen_copz00(((BgL_cifz00_bglt) BgL_oz00_5122));
		}

	}



/* cif-loc-set! */
	BGL_EXPORTED_DEF obj_t BGl_cifzd2loczd2setz12z12zzcgen_copz00(BgL_cifz00_bglt
		BgL_oz00_220, obj_t BgL_vz00_221)
	{
		{	/* Cgen/cop.sch 556 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_220)))->BgL_locz00) =
				((obj_t) BgL_vz00_221), BUNSPEC);
		}

	}



/* &cif-loc-set! */
	obj_t BGl_z62cifzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5123,
		obj_t BgL_oz00_5124, obj_t BgL_vz00_5125)
	{
		{	/* Cgen/cop.sch 556 */
			return
				BGl_cifzd2loczd2setz12z12zzcgen_copz00(
				((BgL_cifz00_bglt) BgL_oz00_5124), BgL_vz00_5125);
		}

	}



/* make-local-var */
	BGL_EXPORTED_DEF BgL_localzd2varzd2_bglt
		BGl_makezd2localzd2varz00zzcgen_copz00(obj_t BgL_loc1410z00_222,
		BgL_typez00_bglt BgL_type1411z00_223, obj_t BgL_vars1412z00_224)
	{
		{	/* Cgen/cop.sch 559 */
			{	/* Cgen/cop.sch 559 */
				BgL_localzd2varzd2_bglt BgL_new1350z00_6455;

				{	/* Cgen/cop.sch 559 */
					BgL_localzd2varzd2_bglt BgL_new1349z00_6456;

					BgL_new1349z00_6456 =
						((BgL_localzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_localzd2varzd2_bgl))));
					{	/* Cgen/cop.sch 559 */
						long BgL_arg1661z00_6457;

						BgL_arg1661z00_6457 =
							BGL_CLASS_NUM(BGl_localzd2varzd2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1349z00_6456), BgL_arg1661z00_6457);
					}
					BgL_new1350z00_6455 = BgL_new1349z00_6456;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1350z00_6455)))->BgL_locz00) =
					((obj_t) BgL_loc1410z00_222), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1350z00_6455)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1411z00_223), BUNSPEC);
				((((BgL_localzd2varzd2_bglt) COBJECT(BgL_new1350z00_6455))->
						BgL_varsz00) = ((obj_t) BgL_vars1412z00_224), BUNSPEC);
				return BgL_new1350z00_6455;
			}
		}

	}



/* &make-local-var */
	BgL_localzd2varzd2_bglt BGl_z62makezd2localzd2varz62zzcgen_copz00(obj_t
		BgL_envz00_5126, obj_t BgL_loc1410z00_5127, obj_t BgL_type1411z00_5128,
		obj_t BgL_vars1412z00_5129)
	{
		{	/* Cgen/cop.sch 559 */
			return
				BGl_makezd2localzd2varz00zzcgen_copz00(BgL_loc1410z00_5127,
				((BgL_typez00_bglt) BgL_type1411z00_5128), BgL_vars1412z00_5129);
		}

	}



/* local-var? */
	BGL_EXPORTED_DEF bool_t BGl_localzd2varzf3z21zzcgen_copz00(obj_t
		BgL_objz00_225)
	{
		{	/* Cgen/cop.sch 560 */
			{	/* Cgen/cop.sch 560 */
				obj_t BgL_classz00_6458;

				BgL_classz00_6458 = BGl_localzd2varzd2zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_225))
					{	/* Cgen/cop.sch 560 */
						BgL_objectz00_bglt BgL_arg1807z00_6459;

						BgL_arg1807z00_6459 = (BgL_objectz00_bglt) (BgL_objz00_225);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 560 */
								long BgL_idxz00_6460;

								BgL_idxz00_6460 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6459);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6460 + 2L)) == BgL_classz00_6458);
							}
						else
							{	/* Cgen/cop.sch 560 */
								bool_t BgL_res2430z00_6463;

								{	/* Cgen/cop.sch 560 */
									obj_t BgL_oclassz00_6464;

									{	/* Cgen/cop.sch 560 */
										obj_t BgL_arg1815z00_6465;
										long BgL_arg1816z00_6466;

										BgL_arg1815z00_6465 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 560 */
											long BgL_arg1817z00_6467;

											BgL_arg1817z00_6467 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6459);
											BgL_arg1816z00_6466 = (BgL_arg1817z00_6467 - OBJECT_TYPE);
										}
										BgL_oclassz00_6464 =
											VECTOR_REF(BgL_arg1815z00_6465, BgL_arg1816z00_6466);
									}
									{	/* Cgen/cop.sch 560 */
										bool_t BgL__ortest_1115z00_6468;

										BgL__ortest_1115z00_6468 =
											(BgL_classz00_6458 == BgL_oclassz00_6464);
										if (BgL__ortest_1115z00_6468)
											{	/* Cgen/cop.sch 560 */
												BgL_res2430z00_6463 = BgL__ortest_1115z00_6468;
											}
										else
											{	/* Cgen/cop.sch 560 */
												long BgL_odepthz00_6469;

												{	/* Cgen/cop.sch 560 */
													obj_t BgL_arg1804z00_6470;

													BgL_arg1804z00_6470 = (BgL_oclassz00_6464);
													BgL_odepthz00_6469 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6470);
												}
												if ((2L < BgL_odepthz00_6469))
													{	/* Cgen/cop.sch 560 */
														obj_t BgL_arg1802z00_6471;

														{	/* Cgen/cop.sch 560 */
															obj_t BgL_arg1803z00_6472;

															BgL_arg1803z00_6472 = (BgL_oclassz00_6464);
															BgL_arg1802z00_6471 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6472,
																2L);
														}
														BgL_res2430z00_6463 =
															(BgL_arg1802z00_6471 == BgL_classz00_6458);
													}
												else
													{	/* Cgen/cop.sch 560 */
														BgL_res2430z00_6463 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2430z00_6463;
							}
					}
				else
					{	/* Cgen/cop.sch 560 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &local-var? */
	obj_t BGl_z62localzd2varzf3z43zzcgen_copz00(obj_t BgL_envz00_5130,
		obj_t BgL_objz00_5131)
	{
		{	/* Cgen/cop.sch 560 */
			return BBOOL(BGl_localzd2varzf3z21zzcgen_copz00(BgL_objz00_5131));
		}

	}



/* local-var-nil */
	BGL_EXPORTED_DEF BgL_localzd2varzd2_bglt
		BGl_localzd2varzd2nilz00zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 561 */
			{	/* Cgen/cop.sch 561 */
				obj_t BgL_classz00_3976;

				BgL_classz00_3976 = BGl_localzd2varzd2zzcgen_copz00;
				{	/* Cgen/cop.sch 561 */
					obj_t BgL__ortest_1117z00_3977;

					BgL__ortest_1117z00_3977 = BGL_CLASS_NIL(BgL_classz00_3976);
					if (CBOOL(BgL__ortest_1117z00_3977))
						{	/* Cgen/cop.sch 561 */
							return ((BgL_localzd2varzd2_bglt) BgL__ortest_1117z00_3977);
						}
					else
						{	/* Cgen/cop.sch 561 */
							return
								((BgL_localzd2varzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3976));
						}
				}
			}
		}

	}



/* &local-var-nil */
	BgL_localzd2varzd2_bglt BGl_z62localzd2varzd2nilz62zzcgen_copz00(obj_t
		BgL_envz00_5132)
	{
		{	/* Cgen/cop.sch 561 */
			return BGl_localzd2varzd2nilz00zzcgen_copz00();
		}

	}



/* local-var-vars */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2varzd2varsz00zzcgen_copz00(BgL_localzd2varzd2_bglt BgL_oz00_226)
	{
		{	/* Cgen/cop.sch 562 */
			return (((BgL_localzd2varzd2_bglt) COBJECT(BgL_oz00_226))->BgL_varsz00);
		}

	}



/* &local-var-vars */
	obj_t BGl_z62localzd2varzd2varsz62zzcgen_copz00(obj_t BgL_envz00_5133,
		obj_t BgL_oz00_5134)
	{
		{	/* Cgen/cop.sch 562 */
			return
				BGl_localzd2varzd2varsz00zzcgen_copz00(
				((BgL_localzd2varzd2_bglt) BgL_oz00_5134));
		}

	}



/* local-var-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_localzd2varzd2typez00zzcgen_copz00(BgL_localzd2varzd2_bglt BgL_oz00_229)
	{
		{	/* Cgen/cop.sch 564 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_229)))->BgL_typez00);
		}

	}



/* &local-var-type */
	BgL_typez00_bglt BGl_z62localzd2varzd2typez62zzcgen_copz00(obj_t
		BgL_envz00_5135, obj_t BgL_oz00_5136)
	{
		{	/* Cgen/cop.sch 564 */
			return
				BGl_localzd2varzd2typez00zzcgen_copz00(
				((BgL_localzd2varzd2_bglt) BgL_oz00_5136));
		}

	}



/* local-var-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2varzd2locz00zzcgen_copz00(BgL_localzd2varzd2_bglt BgL_oz00_232)
	{
		{	/* Cgen/cop.sch 566 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_232)))->BgL_locz00);
		}

	}



/* &local-var-loc */
	obj_t BGl_z62localzd2varzd2locz62zzcgen_copz00(obj_t BgL_envz00_5137,
		obj_t BgL_oz00_5138)
	{
		{	/* Cgen/cop.sch 566 */
			return
				BGl_localzd2varzd2locz00zzcgen_copz00(
				((BgL_localzd2varzd2_bglt) BgL_oz00_5138));
		}

	}



/* local-var-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2varzd2loczd2setz12zc0zzcgen_copz00(BgL_localzd2varzd2_bglt
		BgL_oz00_233, obj_t BgL_vz00_234)
	{
		{	/* Cgen/cop.sch 567 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_233)))->BgL_locz00) =
				((obj_t) BgL_vz00_234), BUNSPEC);
		}

	}



/* &local-var-loc-set! */
	obj_t BGl_z62localzd2varzd2loczd2setz12za2zzcgen_copz00(obj_t BgL_envz00_5139,
		obj_t BgL_oz00_5140, obj_t BgL_vz00_5141)
	{
		{	/* Cgen/cop.sch 567 */
			return
				BGl_localzd2varzd2loczd2setz12zc0zzcgen_copz00(
				((BgL_localzd2varzd2_bglt) BgL_oz00_5140), BgL_vz00_5141);
		}

	}



/* make-cfuncall */
	BGL_EXPORTED_DEF BgL_cfuncallz00_bglt
		BGl_makezd2cfuncallzd2zzcgen_copz00(obj_t BgL_loc1404z00_235,
		BgL_typez00_bglt BgL_type1405z00_236, BgL_copz00_bglt BgL_fun1406z00_237,
		obj_t BgL_args1407z00_238, obj_t BgL_strength1408z00_239)
	{
		{	/* Cgen/cop.sch 570 */
			{	/* Cgen/cop.sch 570 */
				BgL_cfuncallz00_bglt BgL_new1352z00_6473;

				{	/* Cgen/cop.sch 570 */
					BgL_cfuncallz00_bglt BgL_new1351z00_6474;

					BgL_new1351z00_6474 =
						((BgL_cfuncallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cfuncallz00_bgl))));
					{	/* Cgen/cop.sch 570 */
						long BgL_arg1663z00_6475;

						BgL_arg1663z00_6475 = BGL_CLASS_NUM(BGl_cfuncallz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1351z00_6474), BgL_arg1663z00_6475);
					}
					BgL_new1352z00_6473 = BgL_new1351z00_6474;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1352z00_6473)))->BgL_locz00) =
					((obj_t) BgL_loc1404z00_235), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1352z00_6473)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1405z00_236), BUNSPEC);
				((((BgL_cfuncallz00_bglt) COBJECT(BgL_new1352z00_6473))->BgL_funz00) =
					((BgL_copz00_bglt) BgL_fun1406z00_237), BUNSPEC);
				((((BgL_cfuncallz00_bglt) COBJECT(BgL_new1352z00_6473))->BgL_argsz00) =
					((obj_t) BgL_args1407z00_238), BUNSPEC);
				((((BgL_cfuncallz00_bglt) COBJECT(BgL_new1352z00_6473))->
						BgL_strengthz00) = ((obj_t) BgL_strength1408z00_239), BUNSPEC);
				return BgL_new1352z00_6473;
			}
		}

	}



/* &make-cfuncall */
	BgL_cfuncallz00_bglt BGl_z62makezd2cfuncallzb0zzcgen_copz00(obj_t
		BgL_envz00_5142, obj_t BgL_loc1404z00_5143, obj_t BgL_type1405z00_5144,
		obj_t BgL_fun1406z00_5145, obj_t BgL_args1407z00_5146,
		obj_t BgL_strength1408z00_5147)
	{
		{	/* Cgen/cop.sch 570 */
			return
				BGl_makezd2cfuncallzd2zzcgen_copz00(BgL_loc1404z00_5143,
				((BgL_typez00_bglt) BgL_type1405z00_5144),
				((BgL_copz00_bglt) BgL_fun1406z00_5145), BgL_args1407z00_5146,
				BgL_strength1408z00_5147);
		}

	}



/* cfuncall? */
	BGL_EXPORTED_DEF bool_t BGl_cfuncallzf3zf3zzcgen_copz00(obj_t BgL_objz00_240)
	{
		{	/* Cgen/cop.sch 571 */
			{	/* Cgen/cop.sch 571 */
				obj_t BgL_classz00_6476;

				BgL_classz00_6476 = BGl_cfuncallz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_240))
					{	/* Cgen/cop.sch 571 */
						BgL_objectz00_bglt BgL_arg1807z00_6477;

						BgL_arg1807z00_6477 = (BgL_objectz00_bglt) (BgL_objz00_240);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 571 */
								long BgL_idxz00_6478;

								BgL_idxz00_6478 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6477);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6478 + 2L)) == BgL_classz00_6476);
							}
						else
							{	/* Cgen/cop.sch 571 */
								bool_t BgL_res2431z00_6481;

								{	/* Cgen/cop.sch 571 */
									obj_t BgL_oclassz00_6482;

									{	/* Cgen/cop.sch 571 */
										obj_t BgL_arg1815z00_6483;
										long BgL_arg1816z00_6484;

										BgL_arg1815z00_6483 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 571 */
											long BgL_arg1817z00_6485;

											BgL_arg1817z00_6485 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6477);
											BgL_arg1816z00_6484 = (BgL_arg1817z00_6485 - OBJECT_TYPE);
										}
										BgL_oclassz00_6482 =
											VECTOR_REF(BgL_arg1815z00_6483, BgL_arg1816z00_6484);
									}
									{	/* Cgen/cop.sch 571 */
										bool_t BgL__ortest_1115z00_6486;

										BgL__ortest_1115z00_6486 =
											(BgL_classz00_6476 == BgL_oclassz00_6482);
										if (BgL__ortest_1115z00_6486)
											{	/* Cgen/cop.sch 571 */
												BgL_res2431z00_6481 = BgL__ortest_1115z00_6486;
											}
										else
											{	/* Cgen/cop.sch 571 */
												long BgL_odepthz00_6487;

												{	/* Cgen/cop.sch 571 */
													obj_t BgL_arg1804z00_6488;

													BgL_arg1804z00_6488 = (BgL_oclassz00_6482);
													BgL_odepthz00_6487 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6488);
												}
												if ((2L < BgL_odepthz00_6487))
													{	/* Cgen/cop.sch 571 */
														obj_t BgL_arg1802z00_6489;

														{	/* Cgen/cop.sch 571 */
															obj_t BgL_arg1803z00_6490;

															BgL_arg1803z00_6490 = (BgL_oclassz00_6482);
															BgL_arg1802z00_6489 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6490,
																2L);
														}
														BgL_res2431z00_6481 =
															(BgL_arg1802z00_6489 == BgL_classz00_6476);
													}
												else
													{	/* Cgen/cop.sch 571 */
														BgL_res2431z00_6481 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2431z00_6481;
							}
					}
				else
					{	/* Cgen/cop.sch 571 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cfuncall? */
	obj_t BGl_z62cfuncallzf3z91zzcgen_copz00(obj_t BgL_envz00_5148,
		obj_t BgL_objz00_5149)
	{
		{	/* Cgen/cop.sch 571 */
			return BBOOL(BGl_cfuncallzf3zf3zzcgen_copz00(BgL_objz00_5149));
		}

	}



/* cfuncall-nil */
	BGL_EXPORTED_DEF BgL_cfuncallz00_bglt BGl_cfuncallzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 572 */
			{	/* Cgen/cop.sch 572 */
				obj_t BgL_classz00_4018;

				BgL_classz00_4018 = BGl_cfuncallz00zzcgen_copz00;
				{	/* Cgen/cop.sch 572 */
					obj_t BgL__ortest_1117z00_4019;

					BgL__ortest_1117z00_4019 = BGL_CLASS_NIL(BgL_classz00_4018);
					if (CBOOL(BgL__ortest_1117z00_4019))
						{	/* Cgen/cop.sch 572 */
							return ((BgL_cfuncallz00_bglt) BgL__ortest_1117z00_4019);
						}
					else
						{	/* Cgen/cop.sch 572 */
							return
								((BgL_cfuncallz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4018));
						}
				}
			}
		}

	}



/* &cfuncall-nil */
	BgL_cfuncallz00_bglt BGl_z62cfuncallzd2nilzb0zzcgen_copz00(obj_t
		BgL_envz00_5150)
	{
		{	/* Cgen/cop.sch 572 */
			return BGl_cfuncallzd2nilzd2zzcgen_copz00();
		}

	}



/* cfuncall-strength */
	BGL_EXPORTED_DEF obj_t
		BGl_cfuncallzd2strengthzd2zzcgen_copz00(BgL_cfuncallz00_bglt BgL_oz00_241)
	{
		{	/* Cgen/cop.sch 573 */
			return (((BgL_cfuncallz00_bglt) COBJECT(BgL_oz00_241))->BgL_strengthz00);
		}

	}



/* &cfuncall-strength */
	obj_t BGl_z62cfuncallzd2strengthzb0zzcgen_copz00(obj_t BgL_envz00_5151,
		obj_t BgL_oz00_5152)
	{
		{	/* Cgen/cop.sch 573 */
			return
				BGl_cfuncallzd2strengthzd2zzcgen_copz00(
				((BgL_cfuncallz00_bglt) BgL_oz00_5152));
		}

	}



/* cfuncall-args */
	BGL_EXPORTED_DEF obj_t
		BGl_cfuncallzd2argszd2zzcgen_copz00(BgL_cfuncallz00_bglt BgL_oz00_244)
	{
		{	/* Cgen/cop.sch 575 */
			return (((BgL_cfuncallz00_bglt) COBJECT(BgL_oz00_244))->BgL_argsz00);
		}

	}



/* &cfuncall-args */
	obj_t BGl_z62cfuncallzd2argszb0zzcgen_copz00(obj_t BgL_envz00_5153,
		obj_t BgL_oz00_5154)
	{
		{	/* Cgen/cop.sch 575 */
			return
				BGl_cfuncallzd2argszd2zzcgen_copz00(
				((BgL_cfuncallz00_bglt) BgL_oz00_5154));
		}

	}



/* cfuncall-fun */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cfuncallzd2funzd2zzcgen_copz00(BgL_cfuncallz00_bglt BgL_oz00_247)
	{
		{	/* Cgen/cop.sch 577 */
			return (((BgL_cfuncallz00_bglt) COBJECT(BgL_oz00_247))->BgL_funz00);
		}

	}



/* &cfuncall-fun */
	BgL_copz00_bglt BGl_z62cfuncallzd2funzb0zzcgen_copz00(obj_t BgL_envz00_5155,
		obj_t BgL_oz00_5156)
	{
		{	/* Cgen/cop.sch 577 */
			return
				BGl_cfuncallzd2funzd2zzcgen_copz00(
				((BgL_cfuncallz00_bglt) BgL_oz00_5156));
		}

	}



/* cfuncall-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cfuncallzd2typezd2zzcgen_copz00(BgL_cfuncallz00_bglt BgL_oz00_250)
	{
		{	/* Cgen/cop.sch 579 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_250)))->BgL_typez00);
		}

	}



/* &cfuncall-type */
	BgL_typez00_bglt BGl_z62cfuncallzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5157,
		obj_t BgL_oz00_5158)
	{
		{	/* Cgen/cop.sch 579 */
			return
				BGl_cfuncallzd2typezd2zzcgen_copz00(
				((BgL_cfuncallz00_bglt) BgL_oz00_5158));
		}

	}



/* cfuncall-loc */
	BGL_EXPORTED_DEF obj_t BGl_cfuncallzd2loczd2zzcgen_copz00(BgL_cfuncallz00_bglt
		BgL_oz00_253)
	{
		{	/* Cgen/cop.sch 581 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_253)))->BgL_locz00);
		}

	}



/* &cfuncall-loc */
	obj_t BGl_z62cfuncallzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5159,
		obj_t BgL_oz00_5160)
	{
		{	/* Cgen/cop.sch 581 */
			return
				BGl_cfuncallzd2loczd2zzcgen_copz00(
				((BgL_cfuncallz00_bglt) BgL_oz00_5160));
		}

	}



/* cfuncall-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfuncallzd2loczd2setz12z12zzcgen_copz00(BgL_cfuncallz00_bglt
		BgL_oz00_254, obj_t BgL_vz00_255)
	{
		{	/* Cgen/cop.sch 582 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_254)))->BgL_locz00) =
				((obj_t) BgL_vz00_255), BUNSPEC);
		}

	}



/* &cfuncall-loc-set! */
	obj_t BGl_z62cfuncallzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5161,
		obj_t BgL_oz00_5162, obj_t BgL_vz00_5163)
	{
		{	/* Cgen/cop.sch 582 */
			return
				BGl_cfuncallzd2loczd2setz12z12zzcgen_copz00(
				((BgL_cfuncallz00_bglt) BgL_oz00_5162), BgL_vz00_5163);
		}

	}



/* make-capply */
	BGL_EXPORTED_DEF BgL_capplyz00_bglt BGl_makezd2capplyzd2zzcgen_copz00(obj_t
		BgL_loc1399z00_256, BgL_typez00_bglt BgL_type1400z00_257,
		BgL_copz00_bglt BgL_fun1401z00_258, BgL_copz00_bglt BgL_arg1402z00_259)
	{
		{	/* Cgen/cop.sch 585 */
			{	/* Cgen/cop.sch 585 */
				BgL_capplyz00_bglt BgL_new1354z00_6491;

				{	/* Cgen/cop.sch 585 */
					BgL_capplyz00_bglt BgL_new1353z00_6492;

					BgL_new1353z00_6492 =
						((BgL_capplyz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_capplyz00_bgl))));
					{	/* Cgen/cop.sch 585 */
						long BgL_arg1675z00_6493;

						BgL_arg1675z00_6493 = BGL_CLASS_NUM(BGl_capplyz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1353z00_6492), BgL_arg1675z00_6493);
					}
					BgL_new1354z00_6491 = BgL_new1353z00_6492;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1354z00_6491)))->BgL_locz00) =
					((obj_t) BgL_loc1399z00_256), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1354z00_6491)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1400z00_257), BUNSPEC);
				((((BgL_capplyz00_bglt) COBJECT(BgL_new1354z00_6491))->BgL_funz00) =
					((BgL_copz00_bglt) BgL_fun1401z00_258), BUNSPEC);
				((((BgL_capplyz00_bglt) COBJECT(BgL_new1354z00_6491))->BgL_argz00) =
					((BgL_copz00_bglt) BgL_arg1402z00_259), BUNSPEC);
				return BgL_new1354z00_6491;
			}
		}

	}



/* &make-capply */
	BgL_capplyz00_bglt BGl_z62makezd2capplyzb0zzcgen_copz00(obj_t BgL_envz00_5164,
		obj_t BgL_loc1399z00_5165, obj_t BgL_type1400z00_5166,
		obj_t BgL_fun1401z00_5167, obj_t BgL_arg1402z00_5168)
	{
		{	/* Cgen/cop.sch 585 */
			return
				BGl_makezd2capplyzd2zzcgen_copz00(BgL_loc1399z00_5165,
				((BgL_typez00_bglt) BgL_type1400z00_5166),
				((BgL_copz00_bglt) BgL_fun1401z00_5167),
				((BgL_copz00_bglt) BgL_arg1402z00_5168));
		}

	}



/* capply? */
	BGL_EXPORTED_DEF bool_t BGl_capplyzf3zf3zzcgen_copz00(obj_t BgL_objz00_260)
	{
		{	/* Cgen/cop.sch 586 */
			{	/* Cgen/cop.sch 586 */
				obj_t BgL_classz00_6494;

				BgL_classz00_6494 = BGl_capplyz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_260))
					{	/* Cgen/cop.sch 586 */
						BgL_objectz00_bglt BgL_arg1807z00_6495;

						BgL_arg1807z00_6495 = (BgL_objectz00_bglt) (BgL_objz00_260);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 586 */
								long BgL_idxz00_6496;

								BgL_idxz00_6496 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6495);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6496 + 2L)) == BgL_classz00_6494);
							}
						else
							{	/* Cgen/cop.sch 586 */
								bool_t BgL_res2432z00_6499;

								{	/* Cgen/cop.sch 586 */
									obj_t BgL_oclassz00_6500;

									{	/* Cgen/cop.sch 586 */
										obj_t BgL_arg1815z00_6501;
										long BgL_arg1816z00_6502;

										BgL_arg1815z00_6501 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 586 */
											long BgL_arg1817z00_6503;

											BgL_arg1817z00_6503 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6495);
											BgL_arg1816z00_6502 = (BgL_arg1817z00_6503 - OBJECT_TYPE);
										}
										BgL_oclassz00_6500 =
											VECTOR_REF(BgL_arg1815z00_6501, BgL_arg1816z00_6502);
									}
									{	/* Cgen/cop.sch 586 */
										bool_t BgL__ortest_1115z00_6504;

										BgL__ortest_1115z00_6504 =
											(BgL_classz00_6494 == BgL_oclassz00_6500);
										if (BgL__ortest_1115z00_6504)
											{	/* Cgen/cop.sch 586 */
												BgL_res2432z00_6499 = BgL__ortest_1115z00_6504;
											}
										else
											{	/* Cgen/cop.sch 586 */
												long BgL_odepthz00_6505;

												{	/* Cgen/cop.sch 586 */
													obj_t BgL_arg1804z00_6506;

													BgL_arg1804z00_6506 = (BgL_oclassz00_6500);
													BgL_odepthz00_6505 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6506);
												}
												if ((2L < BgL_odepthz00_6505))
													{	/* Cgen/cop.sch 586 */
														obj_t BgL_arg1802z00_6507;

														{	/* Cgen/cop.sch 586 */
															obj_t BgL_arg1803z00_6508;

															BgL_arg1803z00_6508 = (BgL_oclassz00_6500);
															BgL_arg1802z00_6507 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6508,
																2L);
														}
														BgL_res2432z00_6499 =
															(BgL_arg1802z00_6507 == BgL_classz00_6494);
													}
												else
													{	/* Cgen/cop.sch 586 */
														BgL_res2432z00_6499 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2432z00_6499;
							}
					}
				else
					{	/* Cgen/cop.sch 586 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &capply? */
	obj_t BGl_z62capplyzf3z91zzcgen_copz00(obj_t BgL_envz00_5169,
		obj_t BgL_objz00_5170)
	{
		{	/* Cgen/cop.sch 586 */
			return BBOOL(BGl_capplyzf3zf3zzcgen_copz00(BgL_objz00_5170));
		}

	}



/* capply-nil */
	BGL_EXPORTED_DEF BgL_capplyz00_bglt BGl_capplyzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 587 */
			{	/* Cgen/cop.sch 587 */
				obj_t BgL_classz00_4060;

				BgL_classz00_4060 = BGl_capplyz00zzcgen_copz00;
				{	/* Cgen/cop.sch 587 */
					obj_t BgL__ortest_1117z00_4061;

					BgL__ortest_1117z00_4061 = BGL_CLASS_NIL(BgL_classz00_4060);
					if (CBOOL(BgL__ortest_1117z00_4061))
						{	/* Cgen/cop.sch 587 */
							return ((BgL_capplyz00_bglt) BgL__ortest_1117z00_4061);
						}
					else
						{	/* Cgen/cop.sch 587 */
							return
								((BgL_capplyz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4060));
						}
				}
			}
		}

	}



/* &capply-nil */
	BgL_capplyz00_bglt BGl_z62capplyzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_5171)
	{
		{	/* Cgen/cop.sch 587 */
			return BGl_capplyzd2nilzd2zzcgen_copz00();
		}

	}



/* capply-arg */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_capplyzd2argzd2zzcgen_copz00(BgL_capplyz00_bglt BgL_oz00_261)
	{
		{	/* Cgen/cop.sch 588 */
			return (((BgL_capplyz00_bglt) COBJECT(BgL_oz00_261))->BgL_argz00);
		}

	}



/* &capply-arg */
	BgL_copz00_bglt BGl_z62capplyzd2argzb0zzcgen_copz00(obj_t BgL_envz00_5172,
		obj_t BgL_oz00_5173)
	{
		{	/* Cgen/cop.sch 588 */
			return
				BGl_capplyzd2argzd2zzcgen_copz00(((BgL_capplyz00_bglt) BgL_oz00_5173));
		}

	}



/* capply-fun */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_capplyzd2funzd2zzcgen_copz00(BgL_capplyz00_bglt BgL_oz00_264)
	{
		{	/* Cgen/cop.sch 590 */
			return (((BgL_capplyz00_bglt) COBJECT(BgL_oz00_264))->BgL_funz00);
		}

	}



/* &capply-fun */
	BgL_copz00_bglt BGl_z62capplyzd2funzb0zzcgen_copz00(obj_t BgL_envz00_5174,
		obj_t BgL_oz00_5175)
	{
		{	/* Cgen/cop.sch 590 */
			return
				BGl_capplyzd2funzd2zzcgen_copz00(((BgL_capplyz00_bglt) BgL_oz00_5175));
		}

	}



/* capply-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_capplyzd2typezd2zzcgen_copz00(BgL_capplyz00_bglt BgL_oz00_267)
	{
		{	/* Cgen/cop.sch 592 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_267)))->BgL_typez00);
		}

	}



/* &capply-type */
	BgL_typez00_bglt BGl_z62capplyzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5176,
		obj_t BgL_oz00_5177)
	{
		{	/* Cgen/cop.sch 592 */
			return
				BGl_capplyzd2typezd2zzcgen_copz00(((BgL_capplyz00_bglt) BgL_oz00_5177));
		}

	}



/* capply-loc */
	BGL_EXPORTED_DEF obj_t BGl_capplyzd2loczd2zzcgen_copz00(BgL_capplyz00_bglt
		BgL_oz00_270)
	{
		{	/* Cgen/cop.sch 594 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_270)))->BgL_locz00);
		}

	}



/* &capply-loc */
	obj_t BGl_z62capplyzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5178,
		obj_t BgL_oz00_5179)
	{
		{	/* Cgen/cop.sch 594 */
			return
				BGl_capplyzd2loczd2zzcgen_copz00(((BgL_capplyz00_bglt) BgL_oz00_5179));
		}

	}



/* capply-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_capplyzd2loczd2setz12z12zzcgen_copz00(BgL_capplyz00_bglt BgL_oz00_271,
		obj_t BgL_vz00_272)
	{
		{	/* Cgen/cop.sch 595 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_271)))->BgL_locz00) =
				((obj_t) BgL_vz00_272), BUNSPEC);
		}

	}



/* &capply-loc-set! */
	obj_t BGl_z62capplyzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5180,
		obj_t BgL_oz00_5181, obj_t BgL_vz00_5182)
	{
		{	/* Cgen/cop.sch 595 */
			return
				BGl_capplyzd2loczd2setz12z12zzcgen_copz00(
				((BgL_capplyz00_bglt) BgL_oz00_5181), BgL_vz00_5182);
		}

	}



/* make-capp */
	BGL_EXPORTED_DEF BgL_cappz00_bglt BGl_makezd2cappzd2zzcgen_copz00(obj_t
		BgL_loc1393z00_273, BgL_typez00_bglt BgL_type1394z00_274,
		BgL_copz00_bglt BgL_fun1395z00_275, obj_t BgL_args1396z00_276,
		obj_t BgL_stackable1397z00_277)
	{
		{	/* Cgen/cop.sch 598 */
			{	/* Cgen/cop.sch 598 */
				BgL_cappz00_bglt BgL_new1356z00_6509;

				{	/* Cgen/cop.sch 598 */
					BgL_cappz00_bglt BgL_new1355z00_6510;

					BgL_new1355z00_6510 =
						((BgL_cappz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cappz00_bgl))));
					{	/* Cgen/cop.sch 598 */
						long BgL_arg1678z00_6511;

						BgL_arg1678z00_6511 = BGL_CLASS_NUM(BGl_cappz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1355z00_6510), BgL_arg1678z00_6511);
					}
					BgL_new1356z00_6509 = BgL_new1355z00_6510;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1356z00_6509)))->BgL_locz00) =
					((obj_t) BgL_loc1393z00_273), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1356z00_6509)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1394z00_274), BUNSPEC);
				((((BgL_cappz00_bglt) COBJECT(BgL_new1356z00_6509))->BgL_funz00) =
					((BgL_copz00_bglt) BgL_fun1395z00_275), BUNSPEC);
				((((BgL_cappz00_bglt) COBJECT(BgL_new1356z00_6509))->BgL_argsz00) =
					((obj_t) BgL_args1396z00_276), BUNSPEC);
				((((BgL_cappz00_bglt) COBJECT(BgL_new1356z00_6509))->BgL_stackablez00) =
					((obj_t) BgL_stackable1397z00_277), BUNSPEC);
				return BgL_new1356z00_6509;
			}
		}

	}



/* &make-capp */
	BgL_cappz00_bglt BGl_z62makezd2cappzb0zzcgen_copz00(obj_t BgL_envz00_5183,
		obj_t BgL_loc1393z00_5184, obj_t BgL_type1394z00_5185,
		obj_t BgL_fun1395z00_5186, obj_t BgL_args1396z00_5187,
		obj_t BgL_stackable1397z00_5188)
	{
		{	/* Cgen/cop.sch 598 */
			return
				BGl_makezd2cappzd2zzcgen_copz00(BgL_loc1393z00_5184,
				((BgL_typez00_bglt) BgL_type1394z00_5185),
				((BgL_copz00_bglt) BgL_fun1395z00_5186), BgL_args1396z00_5187,
				BgL_stackable1397z00_5188);
		}

	}



/* capp? */
	BGL_EXPORTED_DEF bool_t BGl_cappzf3zf3zzcgen_copz00(obj_t BgL_objz00_278)
	{
		{	/* Cgen/cop.sch 599 */
			{	/* Cgen/cop.sch 599 */
				obj_t BgL_classz00_6512;

				BgL_classz00_6512 = BGl_cappz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_278))
					{	/* Cgen/cop.sch 599 */
						BgL_objectz00_bglt BgL_arg1807z00_6513;

						BgL_arg1807z00_6513 = (BgL_objectz00_bglt) (BgL_objz00_278);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 599 */
								long BgL_idxz00_6514;

								BgL_idxz00_6514 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6513);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6514 + 2L)) == BgL_classz00_6512);
							}
						else
							{	/* Cgen/cop.sch 599 */
								bool_t BgL_res2433z00_6517;

								{	/* Cgen/cop.sch 599 */
									obj_t BgL_oclassz00_6518;

									{	/* Cgen/cop.sch 599 */
										obj_t BgL_arg1815z00_6519;
										long BgL_arg1816z00_6520;

										BgL_arg1815z00_6519 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 599 */
											long BgL_arg1817z00_6521;

											BgL_arg1817z00_6521 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6513);
											BgL_arg1816z00_6520 = (BgL_arg1817z00_6521 - OBJECT_TYPE);
										}
										BgL_oclassz00_6518 =
											VECTOR_REF(BgL_arg1815z00_6519, BgL_arg1816z00_6520);
									}
									{	/* Cgen/cop.sch 599 */
										bool_t BgL__ortest_1115z00_6522;

										BgL__ortest_1115z00_6522 =
											(BgL_classz00_6512 == BgL_oclassz00_6518);
										if (BgL__ortest_1115z00_6522)
											{	/* Cgen/cop.sch 599 */
												BgL_res2433z00_6517 = BgL__ortest_1115z00_6522;
											}
										else
											{	/* Cgen/cop.sch 599 */
												long BgL_odepthz00_6523;

												{	/* Cgen/cop.sch 599 */
													obj_t BgL_arg1804z00_6524;

													BgL_arg1804z00_6524 = (BgL_oclassz00_6518);
													BgL_odepthz00_6523 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6524);
												}
												if ((2L < BgL_odepthz00_6523))
													{	/* Cgen/cop.sch 599 */
														obj_t BgL_arg1802z00_6525;

														{	/* Cgen/cop.sch 599 */
															obj_t BgL_arg1803z00_6526;

															BgL_arg1803z00_6526 = (BgL_oclassz00_6518);
															BgL_arg1802z00_6525 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6526,
																2L);
														}
														BgL_res2433z00_6517 =
															(BgL_arg1802z00_6525 == BgL_classz00_6512);
													}
												else
													{	/* Cgen/cop.sch 599 */
														BgL_res2433z00_6517 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2433z00_6517;
							}
					}
				else
					{	/* Cgen/cop.sch 599 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &capp? */
	obj_t BGl_z62cappzf3z91zzcgen_copz00(obj_t BgL_envz00_5189,
		obj_t BgL_objz00_5190)
	{
		{	/* Cgen/cop.sch 599 */
			return BBOOL(BGl_cappzf3zf3zzcgen_copz00(BgL_objz00_5190));
		}

	}



/* capp-nil */
	BGL_EXPORTED_DEF BgL_cappz00_bglt BGl_cappzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 600 */
			{	/* Cgen/cop.sch 600 */
				obj_t BgL_classz00_4102;

				BgL_classz00_4102 = BGl_cappz00zzcgen_copz00;
				{	/* Cgen/cop.sch 600 */
					obj_t BgL__ortest_1117z00_4103;

					BgL__ortest_1117z00_4103 = BGL_CLASS_NIL(BgL_classz00_4102);
					if (CBOOL(BgL__ortest_1117z00_4103))
						{	/* Cgen/cop.sch 600 */
							return ((BgL_cappz00_bglt) BgL__ortest_1117z00_4103);
						}
					else
						{	/* Cgen/cop.sch 600 */
							return
								((BgL_cappz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4102));
						}
				}
			}
		}

	}



/* &capp-nil */
	BgL_cappz00_bglt BGl_z62cappzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_5191)
	{
		{	/* Cgen/cop.sch 600 */
			return BGl_cappzd2nilzd2zzcgen_copz00();
		}

	}



/* capp-stackable */
	BGL_EXPORTED_DEF obj_t BGl_cappzd2stackablezd2zzcgen_copz00(BgL_cappz00_bglt
		BgL_oz00_279)
	{
		{	/* Cgen/cop.sch 601 */
			return (((BgL_cappz00_bglt) COBJECT(BgL_oz00_279))->BgL_stackablez00);
		}

	}



/* &capp-stackable */
	obj_t BGl_z62cappzd2stackablezb0zzcgen_copz00(obj_t BgL_envz00_5192,
		obj_t BgL_oz00_5193)
	{
		{	/* Cgen/cop.sch 601 */
			return
				BGl_cappzd2stackablezd2zzcgen_copz00(
				((BgL_cappz00_bglt) BgL_oz00_5193));
		}

	}



/* capp-args */
	BGL_EXPORTED_DEF obj_t BGl_cappzd2argszd2zzcgen_copz00(BgL_cappz00_bglt
		BgL_oz00_282)
	{
		{	/* Cgen/cop.sch 603 */
			return (((BgL_cappz00_bglt) COBJECT(BgL_oz00_282))->BgL_argsz00);
		}

	}



/* &capp-args */
	obj_t BGl_z62cappzd2argszb0zzcgen_copz00(obj_t BgL_envz00_5194,
		obj_t BgL_oz00_5195)
	{
		{	/* Cgen/cop.sch 603 */
			return
				BGl_cappzd2argszd2zzcgen_copz00(((BgL_cappz00_bglt) BgL_oz00_5195));
		}

	}



/* capp-fun */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cappzd2funzd2zzcgen_copz00(BgL_cappz00_bglt BgL_oz00_285)
	{
		{	/* Cgen/cop.sch 605 */
			return (((BgL_cappz00_bglt) COBJECT(BgL_oz00_285))->BgL_funz00);
		}

	}



/* &capp-fun */
	BgL_copz00_bglt BGl_z62cappzd2funzb0zzcgen_copz00(obj_t BgL_envz00_5196,
		obj_t BgL_oz00_5197)
	{
		{	/* Cgen/cop.sch 605 */
			return BGl_cappzd2funzd2zzcgen_copz00(((BgL_cappz00_bglt) BgL_oz00_5197));
		}

	}



/* capp-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cappzd2typezd2zzcgen_copz00(BgL_cappz00_bglt BgL_oz00_288)
	{
		{	/* Cgen/cop.sch 607 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_288)))->BgL_typez00);
		}

	}



/* &capp-type */
	BgL_typez00_bglt BGl_z62cappzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5198,
		obj_t BgL_oz00_5199)
	{
		{	/* Cgen/cop.sch 607 */
			return
				BGl_cappzd2typezd2zzcgen_copz00(((BgL_cappz00_bglt) BgL_oz00_5199));
		}

	}



/* capp-loc */
	BGL_EXPORTED_DEF obj_t BGl_cappzd2loczd2zzcgen_copz00(BgL_cappz00_bglt
		BgL_oz00_291)
	{
		{	/* Cgen/cop.sch 609 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_291)))->BgL_locz00);
		}

	}



/* &capp-loc */
	obj_t BGl_z62cappzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5200,
		obj_t BgL_oz00_5201)
	{
		{	/* Cgen/cop.sch 609 */
			return BGl_cappzd2loczd2zzcgen_copz00(((BgL_cappz00_bglt) BgL_oz00_5201));
		}

	}



/* capp-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cappzd2loczd2setz12z12zzcgen_copz00(BgL_cappz00_bglt BgL_oz00_292,
		obj_t BgL_vz00_293)
	{
		{	/* Cgen/cop.sch 610 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_292)))->BgL_locz00) =
				((obj_t) BgL_vz00_293), BUNSPEC);
		}

	}



/* &capp-loc-set! */
	obj_t BGl_z62cappzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5202,
		obj_t BgL_oz00_5203, obj_t BgL_vz00_5204)
	{
		{	/* Cgen/cop.sch 610 */
			return
				BGl_cappzd2loczd2setz12z12zzcgen_copz00(
				((BgL_cappz00_bglt) BgL_oz00_5203), BgL_vz00_5204);
		}

	}



/* make-cfail */
	BGL_EXPORTED_DEF BgL_cfailz00_bglt BGl_makezd2cfailzd2zzcgen_copz00(obj_t
		BgL_loc1387z00_294, BgL_typez00_bglt BgL_type1388z00_295,
		BgL_copz00_bglt BgL_proc1389z00_296, BgL_copz00_bglt BgL_msg1390z00_297,
		BgL_copz00_bglt BgL_obj1391z00_298)
	{
		{	/* Cgen/cop.sch 613 */
			{	/* Cgen/cop.sch 613 */
				BgL_cfailz00_bglt BgL_new1358z00_6527;

				{	/* Cgen/cop.sch 613 */
					BgL_cfailz00_bglt BgL_new1357z00_6528;

					BgL_new1357z00_6528 =
						((BgL_cfailz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cfailz00_bgl))));
					{	/* Cgen/cop.sch 613 */
						long BgL_arg1681z00_6529;

						BgL_arg1681z00_6529 = BGL_CLASS_NUM(BGl_cfailz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1357z00_6528), BgL_arg1681z00_6529);
					}
					BgL_new1358z00_6527 = BgL_new1357z00_6528;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1358z00_6527)))->BgL_locz00) =
					((obj_t) BgL_loc1387z00_294), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1358z00_6527)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1388z00_295), BUNSPEC);
				((((BgL_cfailz00_bglt) COBJECT(BgL_new1358z00_6527))->BgL_procz00) =
					((BgL_copz00_bglt) BgL_proc1389z00_296), BUNSPEC);
				((((BgL_cfailz00_bglt) COBJECT(BgL_new1358z00_6527))->BgL_msgz00) =
					((BgL_copz00_bglt) BgL_msg1390z00_297), BUNSPEC);
				((((BgL_cfailz00_bglt) COBJECT(BgL_new1358z00_6527))->BgL_objz00) =
					((BgL_copz00_bglt) BgL_obj1391z00_298), BUNSPEC);
				return BgL_new1358z00_6527;
			}
		}

	}



/* &make-cfail */
	BgL_cfailz00_bglt BGl_z62makezd2cfailzb0zzcgen_copz00(obj_t BgL_envz00_5205,
		obj_t BgL_loc1387z00_5206, obj_t BgL_type1388z00_5207,
		obj_t BgL_proc1389z00_5208, obj_t BgL_msg1390z00_5209,
		obj_t BgL_obj1391z00_5210)
	{
		{	/* Cgen/cop.sch 613 */
			return
				BGl_makezd2cfailzd2zzcgen_copz00(BgL_loc1387z00_5206,
				((BgL_typez00_bglt) BgL_type1388z00_5207),
				((BgL_copz00_bglt) BgL_proc1389z00_5208),
				((BgL_copz00_bglt) BgL_msg1390z00_5209),
				((BgL_copz00_bglt) BgL_obj1391z00_5210));
		}

	}



/* cfail? */
	BGL_EXPORTED_DEF bool_t BGl_cfailzf3zf3zzcgen_copz00(obj_t BgL_objz00_299)
	{
		{	/* Cgen/cop.sch 614 */
			{	/* Cgen/cop.sch 614 */
				obj_t BgL_classz00_6530;

				BgL_classz00_6530 = BGl_cfailz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_299))
					{	/* Cgen/cop.sch 614 */
						BgL_objectz00_bglt BgL_arg1807z00_6531;

						BgL_arg1807z00_6531 = (BgL_objectz00_bglt) (BgL_objz00_299);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 614 */
								long BgL_idxz00_6532;

								BgL_idxz00_6532 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6531);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6532 + 2L)) == BgL_classz00_6530);
							}
						else
							{	/* Cgen/cop.sch 614 */
								bool_t BgL_res2434z00_6535;

								{	/* Cgen/cop.sch 614 */
									obj_t BgL_oclassz00_6536;

									{	/* Cgen/cop.sch 614 */
										obj_t BgL_arg1815z00_6537;
										long BgL_arg1816z00_6538;

										BgL_arg1815z00_6537 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 614 */
											long BgL_arg1817z00_6539;

											BgL_arg1817z00_6539 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6531);
											BgL_arg1816z00_6538 = (BgL_arg1817z00_6539 - OBJECT_TYPE);
										}
										BgL_oclassz00_6536 =
											VECTOR_REF(BgL_arg1815z00_6537, BgL_arg1816z00_6538);
									}
									{	/* Cgen/cop.sch 614 */
										bool_t BgL__ortest_1115z00_6540;

										BgL__ortest_1115z00_6540 =
											(BgL_classz00_6530 == BgL_oclassz00_6536);
										if (BgL__ortest_1115z00_6540)
											{	/* Cgen/cop.sch 614 */
												BgL_res2434z00_6535 = BgL__ortest_1115z00_6540;
											}
										else
											{	/* Cgen/cop.sch 614 */
												long BgL_odepthz00_6541;

												{	/* Cgen/cop.sch 614 */
													obj_t BgL_arg1804z00_6542;

													BgL_arg1804z00_6542 = (BgL_oclassz00_6536);
													BgL_odepthz00_6541 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6542);
												}
												if ((2L < BgL_odepthz00_6541))
													{	/* Cgen/cop.sch 614 */
														obj_t BgL_arg1802z00_6543;

														{	/* Cgen/cop.sch 614 */
															obj_t BgL_arg1803z00_6544;

															BgL_arg1803z00_6544 = (BgL_oclassz00_6536);
															BgL_arg1802z00_6543 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6544,
																2L);
														}
														BgL_res2434z00_6535 =
															(BgL_arg1802z00_6543 == BgL_classz00_6530);
													}
												else
													{	/* Cgen/cop.sch 614 */
														BgL_res2434z00_6535 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2434z00_6535;
							}
					}
				else
					{	/* Cgen/cop.sch 614 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cfail? */
	obj_t BGl_z62cfailzf3z91zzcgen_copz00(obj_t BgL_envz00_5211,
		obj_t BgL_objz00_5212)
	{
		{	/* Cgen/cop.sch 614 */
			return BBOOL(BGl_cfailzf3zf3zzcgen_copz00(BgL_objz00_5212));
		}

	}



/* cfail-nil */
	BGL_EXPORTED_DEF BgL_cfailz00_bglt BGl_cfailzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 615 */
			{	/* Cgen/cop.sch 615 */
				obj_t BgL_classz00_4144;

				BgL_classz00_4144 = BGl_cfailz00zzcgen_copz00;
				{	/* Cgen/cop.sch 615 */
					obj_t BgL__ortest_1117z00_4145;

					BgL__ortest_1117z00_4145 = BGL_CLASS_NIL(BgL_classz00_4144);
					if (CBOOL(BgL__ortest_1117z00_4145))
						{	/* Cgen/cop.sch 615 */
							return ((BgL_cfailz00_bglt) BgL__ortest_1117z00_4145);
						}
					else
						{	/* Cgen/cop.sch 615 */
							return
								((BgL_cfailz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4144));
						}
				}
			}
		}

	}



/* &cfail-nil */
	BgL_cfailz00_bglt BGl_z62cfailzd2nilzb0zzcgen_copz00(obj_t BgL_envz00_5213)
	{
		{	/* Cgen/cop.sch 615 */
			return BGl_cfailzd2nilzd2zzcgen_copz00();
		}

	}



/* cfail-obj */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cfailzd2objzd2zzcgen_copz00(BgL_cfailz00_bglt BgL_oz00_300)
	{
		{	/* Cgen/cop.sch 616 */
			return (((BgL_cfailz00_bglt) COBJECT(BgL_oz00_300))->BgL_objz00);
		}

	}



/* &cfail-obj */
	BgL_copz00_bglt BGl_z62cfailzd2objzb0zzcgen_copz00(obj_t BgL_envz00_5214,
		obj_t BgL_oz00_5215)
	{
		{	/* Cgen/cop.sch 616 */
			return
				BGl_cfailzd2objzd2zzcgen_copz00(((BgL_cfailz00_bglt) BgL_oz00_5215));
		}

	}



/* cfail-msg */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cfailzd2msgzd2zzcgen_copz00(BgL_cfailz00_bglt BgL_oz00_303)
	{
		{	/* Cgen/cop.sch 618 */
			return (((BgL_cfailz00_bglt) COBJECT(BgL_oz00_303))->BgL_msgz00);
		}

	}



/* &cfail-msg */
	BgL_copz00_bglt BGl_z62cfailzd2msgzb0zzcgen_copz00(obj_t BgL_envz00_5216,
		obj_t BgL_oz00_5217)
	{
		{	/* Cgen/cop.sch 618 */
			return
				BGl_cfailzd2msgzd2zzcgen_copz00(((BgL_cfailz00_bglt) BgL_oz00_5217));
		}

	}



/* cfail-proc */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cfailzd2proczd2zzcgen_copz00(BgL_cfailz00_bglt BgL_oz00_306)
	{
		{	/* Cgen/cop.sch 620 */
			return (((BgL_cfailz00_bglt) COBJECT(BgL_oz00_306))->BgL_procz00);
		}

	}



/* &cfail-proc */
	BgL_copz00_bglt BGl_z62cfailzd2proczb0zzcgen_copz00(obj_t BgL_envz00_5218,
		obj_t BgL_oz00_5219)
	{
		{	/* Cgen/cop.sch 620 */
			return
				BGl_cfailzd2proczd2zzcgen_copz00(((BgL_cfailz00_bglt) BgL_oz00_5219));
		}

	}



/* cfail-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cfailzd2typezd2zzcgen_copz00(BgL_cfailz00_bglt BgL_oz00_309)
	{
		{	/* Cgen/cop.sch 622 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_309)))->BgL_typez00);
		}

	}



/* &cfail-type */
	BgL_typez00_bglt BGl_z62cfailzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5220,
		obj_t BgL_oz00_5221)
	{
		{	/* Cgen/cop.sch 622 */
			return
				BGl_cfailzd2typezd2zzcgen_copz00(((BgL_cfailz00_bglt) BgL_oz00_5221));
		}

	}



/* cfail-loc */
	BGL_EXPORTED_DEF obj_t BGl_cfailzd2loczd2zzcgen_copz00(BgL_cfailz00_bglt
		BgL_oz00_312)
	{
		{	/* Cgen/cop.sch 624 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_312)))->BgL_locz00);
		}

	}



/* &cfail-loc */
	obj_t BGl_z62cfailzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5222,
		obj_t BgL_oz00_5223)
	{
		{	/* Cgen/cop.sch 624 */
			return
				BGl_cfailzd2loczd2zzcgen_copz00(((BgL_cfailz00_bglt) BgL_oz00_5223));
		}

	}



/* cfail-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfailzd2loczd2setz12z12zzcgen_copz00(BgL_cfailz00_bglt BgL_oz00_313,
		obj_t BgL_vz00_314)
	{
		{	/* Cgen/cop.sch 625 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_313)))->BgL_locz00) =
				((obj_t) BgL_vz00_314), BUNSPEC);
		}

	}



/* &cfail-loc-set! */
	obj_t BGl_z62cfailzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5224,
		obj_t BgL_oz00_5225, obj_t BgL_vz00_5226)
	{
		{	/* Cgen/cop.sch 625 */
			return
				BGl_cfailzd2loczd2setz12z12zzcgen_copz00(
				((BgL_cfailz00_bglt) BgL_oz00_5225), BgL_vz00_5226);
		}

	}



/* make-cswitch */
	BGL_EXPORTED_DEF BgL_cswitchz00_bglt BGl_makezd2cswitchzd2zzcgen_copz00(obj_t
		BgL_loc1382z00_315, BgL_typez00_bglt BgL_type1383z00_316,
		BgL_copz00_bglt BgL_test1384z00_317, obj_t BgL_clauses1385z00_318)
	{
		{	/* Cgen/cop.sch 628 */
			{	/* Cgen/cop.sch 628 */
				BgL_cswitchz00_bglt BgL_new1360z00_6545;

				{	/* Cgen/cop.sch 628 */
					BgL_cswitchz00_bglt BgL_new1359z00_6546;

					BgL_new1359z00_6546 =
						((BgL_cswitchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cswitchz00_bgl))));
					{	/* Cgen/cop.sch 628 */
						long BgL_arg1688z00_6547;

						BgL_arg1688z00_6547 = BGL_CLASS_NUM(BGl_cswitchz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1359z00_6546), BgL_arg1688z00_6547);
					}
					BgL_new1360z00_6545 = BgL_new1359z00_6546;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1360z00_6545)))->BgL_locz00) =
					((obj_t) BgL_loc1382z00_315), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1360z00_6545)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1383z00_316), BUNSPEC);
				((((BgL_cswitchz00_bglt) COBJECT(BgL_new1360z00_6545))->BgL_testz00) =
					((BgL_copz00_bglt) BgL_test1384z00_317), BUNSPEC);
				((((BgL_cswitchz00_bglt) COBJECT(BgL_new1360z00_6545))->
						BgL_clausesz00) = ((obj_t) BgL_clauses1385z00_318), BUNSPEC);
				return BgL_new1360z00_6545;
			}
		}

	}



/* &make-cswitch */
	BgL_cswitchz00_bglt BGl_z62makezd2cswitchzb0zzcgen_copz00(obj_t
		BgL_envz00_5227, obj_t BgL_loc1382z00_5228, obj_t BgL_type1383z00_5229,
		obj_t BgL_test1384z00_5230, obj_t BgL_clauses1385z00_5231)
	{
		{	/* Cgen/cop.sch 628 */
			return
				BGl_makezd2cswitchzd2zzcgen_copz00(BgL_loc1382z00_5228,
				((BgL_typez00_bglt) BgL_type1383z00_5229),
				((BgL_copz00_bglt) BgL_test1384z00_5230), BgL_clauses1385z00_5231);
		}

	}



/* cswitch? */
	BGL_EXPORTED_DEF bool_t BGl_cswitchzf3zf3zzcgen_copz00(obj_t BgL_objz00_319)
	{
		{	/* Cgen/cop.sch 629 */
			{	/* Cgen/cop.sch 629 */
				obj_t BgL_classz00_6548;

				BgL_classz00_6548 = BGl_cswitchz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_319))
					{	/* Cgen/cop.sch 629 */
						BgL_objectz00_bglt BgL_arg1807z00_6549;

						BgL_arg1807z00_6549 = (BgL_objectz00_bglt) (BgL_objz00_319);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 629 */
								long BgL_idxz00_6550;

								BgL_idxz00_6550 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6549);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6550 + 2L)) == BgL_classz00_6548);
							}
						else
							{	/* Cgen/cop.sch 629 */
								bool_t BgL_res2435z00_6553;

								{	/* Cgen/cop.sch 629 */
									obj_t BgL_oclassz00_6554;

									{	/* Cgen/cop.sch 629 */
										obj_t BgL_arg1815z00_6555;
										long BgL_arg1816z00_6556;

										BgL_arg1815z00_6555 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 629 */
											long BgL_arg1817z00_6557;

											BgL_arg1817z00_6557 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6549);
											BgL_arg1816z00_6556 = (BgL_arg1817z00_6557 - OBJECT_TYPE);
										}
										BgL_oclassz00_6554 =
											VECTOR_REF(BgL_arg1815z00_6555, BgL_arg1816z00_6556);
									}
									{	/* Cgen/cop.sch 629 */
										bool_t BgL__ortest_1115z00_6558;

										BgL__ortest_1115z00_6558 =
											(BgL_classz00_6548 == BgL_oclassz00_6554);
										if (BgL__ortest_1115z00_6558)
											{	/* Cgen/cop.sch 629 */
												BgL_res2435z00_6553 = BgL__ortest_1115z00_6558;
											}
										else
											{	/* Cgen/cop.sch 629 */
												long BgL_odepthz00_6559;

												{	/* Cgen/cop.sch 629 */
													obj_t BgL_arg1804z00_6560;

													BgL_arg1804z00_6560 = (BgL_oclassz00_6554);
													BgL_odepthz00_6559 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6560);
												}
												if ((2L < BgL_odepthz00_6559))
													{	/* Cgen/cop.sch 629 */
														obj_t BgL_arg1802z00_6561;

														{	/* Cgen/cop.sch 629 */
															obj_t BgL_arg1803z00_6562;

															BgL_arg1803z00_6562 = (BgL_oclassz00_6554);
															BgL_arg1802z00_6561 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6562,
																2L);
														}
														BgL_res2435z00_6553 =
															(BgL_arg1802z00_6561 == BgL_classz00_6548);
													}
												else
													{	/* Cgen/cop.sch 629 */
														BgL_res2435z00_6553 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2435z00_6553;
							}
					}
				else
					{	/* Cgen/cop.sch 629 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cswitch? */
	obj_t BGl_z62cswitchzf3z91zzcgen_copz00(obj_t BgL_envz00_5232,
		obj_t BgL_objz00_5233)
	{
		{	/* Cgen/cop.sch 629 */
			return BBOOL(BGl_cswitchzf3zf3zzcgen_copz00(BgL_objz00_5233));
		}

	}



/* cswitch-nil */
	BGL_EXPORTED_DEF BgL_cswitchz00_bglt BGl_cswitchzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 630 */
			{	/* Cgen/cop.sch 630 */
				obj_t BgL_classz00_4186;

				BgL_classz00_4186 = BGl_cswitchz00zzcgen_copz00;
				{	/* Cgen/cop.sch 630 */
					obj_t BgL__ortest_1117z00_4187;

					BgL__ortest_1117z00_4187 = BGL_CLASS_NIL(BgL_classz00_4186);
					if (CBOOL(BgL__ortest_1117z00_4187))
						{	/* Cgen/cop.sch 630 */
							return ((BgL_cswitchz00_bglt) BgL__ortest_1117z00_4187);
						}
					else
						{	/* Cgen/cop.sch 630 */
							return
								((BgL_cswitchz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4186));
						}
				}
			}
		}

	}



/* &cswitch-nil */
	BgL_cswitchz00_bglt BGl_z62cswitchzd2nilzb0zzcgen_copz00(obj_t
		BgL_envz00_5234)
	{
		{	/* Cgen/cop.sch 630 */
			return BGl_cswitchzd2nilzd2zzcgen_copz00();
		}

	}



/* cswitch-clauses */
	BGL_EXPORTED_DEF obj_t
		BGl_cswitchzd2clauseszd2zzcgen_copz00(BgL_cswitchz00_bglt BgL_oz00_320)
	{
		{	/* Cgen/cop.sch 631 */
			return (((BgL_cswitchz00_bglt) COBJECT(BgL_oz00_320))->BgL_clausesz00);
		}

	}



/* &cswitch-clauses */
	obj_t BGl_z62cswitchzd2clauseszb0zzcgen_copz00(obj_t BgL_envz00_5235,
		obj_t BgL_oz00_5236)
	{
		{	/* Cgen/cop.sch 631 */
			return
				BGl_cswitchzd2clauseszd2zzcgen_copz00(
				((BgL_cswitchz00_bglt) BgL_oz00_5236));
		}

	}



/* cswitch-test */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cswitchzd2testzd2zzcgen_copz00(BgL_cswitchz00_bglt BgL_oz00_323)
	{
		{	/* Cgen/cop.sch 633 */
			return (((BgL_cswitchz00_bglt) COBJECT(BgL_oz00_323))->BgL_testz00);
		}

	}



/* &cswitch-test */
	BgL_copz00_bglt BGl_z62cswitchzd2testzb0zzcgen_copz00(obj_t BgL_envz00_5237,
		obj_t BgL_oz00_5238)
	{
		{	/* Cgen/cop.sch 633 */
			return
				BGl_cswitchzd2testzd2zzcgen_copz00(
				((BgL_cswitchz00_bglt) BgL_oz00_5238));
		}

	}



/* cswitch-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cswitchzd2typezd2zzcgen_copz00(BgL_cswitchz00_bglt BgL_oz00_326)
	{
		{	/* Cgen/cop.sch 635 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_326)))->BgL_typez00);
		}

	}



/* &cswitch-type */
	BgL_typez00_bglt BGl_z62cswitchzd2typezb0zzcgen_copz00(obj_t BgL_envz00_5239,
		obj_t BgL_oz00_5240)
	{
		{	/* Cgen/cop.sch 635 */
			return
				BGl_cswitchzd2typezd2zzcgen_copz00(
				((BgL_cswitchz00_bglt) BgL_oz00_5240));
		}

	}



/* cswitch-loc */
	BGL_EXPORTED_DEF obj_t BGl_cswitchzd2loczd2zzcgen_copz00(BgL_cswitchz00_bglt
		BgL_oz00_329)
	{
		{	/* Cgen/cop.sch 637 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_329)))->BgL_locz00);
		}

	}



/* &cswitch-loc */
	obj_t BGl_z62cswitchzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5241,
		obj_t BgL_oz00_5242)
	{
		{	/* Cgen/cop.sch 637 */
			return
				BGl_cswitchzd2loczd2zzcgen_copz00(
				((BgL_cswitchz00_bglt) BgL_oz00_5242));
		}

	}



/* cswitch-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cswitchzd2loczd2setz12z12zzcgen_copz00(BgL_cswitchz00_bglt BgL_oz00_330,
		obj_t BgL_vz00_331)
	{
		{	/* Cgen/cop.sch 638 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_330)))->BgL_locz00) =
				((obj_t) BgL_vz00_331), BUNSPEC);
		}

	}



/* &cswitch-loc-set! */
	obj_t BGl_z62cswitchzd2loczd2setz12z70zzcgen_copz00(obj_t BgL_envz00_5243,
		obj_t BgL_oz00_5244, obj_t BgL_vz00_5245)
	{
		{	/* Cgen/cop.sch 638 */
			return
				BGl_cswitchzd2loczd2setz12z12zzcgen_copz00(
				((BgL_cswitchz00_bglt) BgL_oz00_5244), BgL_vz00_5245);
		}

	}



/* make-cmake-box */
	BGL_EXPORTED_DEF BgL_cmakezd2boxzd2_bglt
		BGl_makezd2cmakezd2boxz00zzcgen_copz00(obj_t BgL_loc1377z00_332,
		BgL_typez00_bglt BgL_type1378z00_333, BgL_copz00_bglt BgL_value1379z00_334,
		obj_t BgL_stackable1380z00_335)
	{
		{	/* Cgen/cop.sch 641 */
			{	/* Cgen/cop.sch 641 */
				BgL_cmakezd2boxzd2_bglt BgL_new1362z00_6563;

				{	/* Cgen/cop.sch 641 */
					BgL_cmakezd2boxzd2_bglt BgL_new1361z00_6564;

					BgL_new1361z00_6564 =
						((BgL_cmakezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cmakezd2boxzd2_bgl))));
					{	/* Cgen/cop.sch 641 */
						long BgL_arg1689z00_6565;

						BgL_arg1689z00_6565 =
							BGL_CLASS_NUM(BGl_cmakezd2boxzd2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1361z00_6564), BgL_arg1689z00_6565);
					}
					BgL_new1362z00_6563 = BgL_new1361z00_6564;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1362z00_6563)))->BgL_locz00) =
					((obj_t) BgL_loc1377z00_332), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1362z00_6563)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1378z00_333), BUNSPEC);
				((((BgL_cmakezd2boxzd2_bglt) COBJECT(BgL_new1362z00_6563))->
						BgL_valuez00) = ((BgL_copz00_bglt) BgL_value1379z00_334), BUNSPEC);
				((((BgL_cmakezd2boxzd2_bglt) COBJECT(BgL_new1362z00_6563))->
						BgL_stackablez00) = ((obj_t) BgL_stackable1380z00_335), BUNSPEC);
				return BgL_new1362z00_6563;
			}
		}

	}



/* &make-cmake-box */
	BgL_cmakezd2boxzd2_bglt BGl_z62makezd2cmakezd2boxz62zzcgen_copz00(obj_t
		BgL_envz00_5246, obj_t BgL_loc1377z00_5247, obj_t BgL_type1378z00_5248,
		obj_t BgL_value1379z00_5249, obj_t BgL_stackable1380z00_5250)
	{
		{	/* Cgen/cop.sch 641 */
			return
				BGl_makezd2cmakezd2boxz00zzcgen_copz00(BgL_loc1377z00_5247,
				((BgL_typez00_bglt) BgL_type1378z00_5248),
				((BgL_copz00_bglt) BgL_value1379z00_5249), BgL_stackable1380z00_5250);
		}

	}



/* cmake-box? */
	BGL_EXPORTED_DEF bool_t BGl_cmakezd2boxzf3z21zzcgen_copz00(obj_t
		BgL_objz00_336)
	{
		{	/* Cgen/cop.sch 642 */
			{	/* Cgen/cop.sch 642 */
				obj_t BgL_classz00_6566;

				BgL_classz00_6566 = BGl_cmakezd2boxzd2zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_336))
					{	/* Cgen/cop.sch 642 */
						BgL_objectz00_bglt BgL_arg1807z00_6567;

						BgL_arg1807z00_6567 = (BgL_objectz00_bglt) (BgL_objz00_336);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 642 */
								long BgL_idxz00_6568;

								BgL_idxz00_6568 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6567);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6568 + 2L)) == BgL_classz00_6566);
							}
						else
							{	/* Cgen/cop.sch 642 */
								bool_t BgL_res2436z00_6571;

								{	/* Cgen/cop.sch 642 */
									obj_t BgL_oclassz00_6572;

									{	/* Cgen/cop.sch 642 */
										obj_t BgL_arg1815z00_6573;
										long BgL_arg1816z00_6574;

										BgL_arg1815z00_6573 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 642 */
											long BgL_arg1817z00_6575;

											BgL_arg1817z00_6575 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6567);
											BgL_arg1816z00_6574 = (BgL_arg1817z00_6575 - OBJECT_TYPE);
										}
										BgL_oclassz00_6572 =
											VECTOR_REF(BgL_arg1815z00_6573, BgL_arg1816z00_6574);
									}
									{	/* Cgen/cop.sch 642 */
										bool_t BgL__ortest_1115z00_6576;

										BgL__ortest_1115z00_6576 =
											(BgL_classz00_6566 == BgL_oclassz00_6572);
										if (BgL__ortest_1115z00_6576)
											{	/* Cgen/cop.sch 642 */
												BgL_res2436z00_6571 = BgL__ortest_1115z00_6576;
											}
										else
											{	/* Cgen/cop.sch 642 */
												long BgL_odepthz00_6577;

												{	/* Cgen/cop.sch 642 */
													obj_t BgL_arg1804z00_6578;

													BgL_arg1804z00_6578 = (BgL_oclassz00_6572);
													BgL_odepthz00_6577 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6578);
												}
												if ((2L < BgL_odepthz00_6577))
													{	/* Cgen/cop.sch 642 */
														obj_t BgL_arg1802z00_6579;

														{	/* Cgen/cop.sch 642 */
															obj_t BgL_arg1803z00_6580;

															BgL_arg1803z00_6580 = (BgL_oclassz00_6572);
															BgL_arg1802z00_6579 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6580,
																2L);
														}
														BgL_res2436z00_6571 =
															(BgL_arg1802z00_6579 == BgL_classz00_6566);
													}
												else
													{	/* Cgen/cop.sch 642 */
														BgL_res2436z00_6571 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2436z00_6571;
							}
					}
				else
					{	/* Cgen/cop.sch 642 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cmake-box? */
	obj_t BGl_z62cmakezd2boxzf3z43zzcgen_copz00(obj_t BgL_envz00_5251,
		obj_t BgL_objz00_5252)
	{
		{	/* Cgen/cop.sch 642 */
			return BBOOL(BGl_cmakezd2boxzf3z21zzcgen_copz00(BgL_objz00_5252));
		}

	}



/* cmake-box-nil */
	BGL_EXPORTED_DEF BgL_cmakezd2boxzd2_bglt
		BGl_cmakezd2boxzd2nilz00zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 643 */
			{	/* Cgen/cop.sch 643 */
				obj_t BgL_classz00_4228;

				BgL_classz00_4228 = BGl_cmakezd2boxzd2zzcgen_copz00;
				{	/* Cgen/cop.sch 643 */
					obj_t BgL__ortest_1117z00_4229;

					BgL__ortest_1117z00_4229 = BGL_CLASS_NIL(BgL_classz00_4228);
					if (CBOOL(BgL__ortest_1117z00_4229))
						{	/* Cgen/cop.sch 643 */
							return ((BgL_cmakezd2boxzd2_bglt) BgL__ortest_1117z00_4229);
						}
					else
						{	/* Cgen/cop.sch 643 */
							return
								((BgL_cmakezd2boxzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4228));
						}
				}
			}
		}

	}



/* &cmake-box-nil */
	BgL_cmakezd2boxzd2_bglt BGl_z62cmakezd2boxzd2nilz62zzcgen_copz00(obj_t
		BgL_envz00_5253)
	{
		{	/* Cgen/cop.sch 643 */
			return BGl_cmakezd2boxzd2nilz00zzcgen_copz00();
		}

	}



/* cmake-box-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_cmakezd2boxzd2stackablez00zzcgen_copz00(BgL_cmakezd2boxzd2_bglt
		BgL_oz00_337)
	{
		{	/* Cgen/cop.sch 644 */
			return
				(((BgL_cmakezd2boxzd2_bglt) COBJECT(BgL_oz00_337))->BgL_stackablez00);
		}

	}



/* &cmake-box-stackable */
	obj_t BGl_z62cmakezd2boxzd2stackablez62zzcgen_copz00(obj_t BgL_envz00_5254,
		obj_t BgL_oz00_5255)
	{
		{	/* Cgen/cop.sch 644 */
			return
				BGl_cmakezd2boxzd2stackablez00zzcgen_copz00(
				((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5255));
		}

	}



/* cmake-box-value */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cmakezd2boxzd2valuez00zzcgen_copz00(BgL_cmakezd2boxzd2_bglt
		BgL_oz00_340)
	{
		{	/* Cgen/cop.sch 646 */
			return (((BgL_cmakezd2boxzd2_bglt) COBJECT(BgL_oz00_340))->BgL_valuez00);
		}

	}



/* &cmake-box-value */
	BgL_copz00_bglt BGl_z62cmakezd2boxzd2valuez62zzcgen_copz00(obj_t
		BgL_envz00_5256, obj_t BgL_oz00_5257)
	{
		{	/* Cgen/cop.sch 646 */
			return
				BGl_cmakezd2boxzd2valuez00zzcgen_copz00(
				((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5257));
		}

	}



/* cmake-box-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cmakezd2boxzd2typez00zzcgen_copz00(BgL_cmakezd2boxzd2_bglt BgL_oz00_343)
	{
		{	/* Cgen/cop.sch 648 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_343)))->BgL_typez00);
		}

	}



/* &cmake-box-type */
	BgL_typez00_bglt BGl_z62cmakezd2boxzd2typez62zzcgen_copz00(obj_t
		BgL_envz00_5258, obj_t BgL_oz00_5259)
	{
		{	/* Cgen/cop.sch 648 */
			return
				BGl_cmakezd2boxzd2typez00zzcgen_copz00(
				((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5259));
		}

	}



/* cmake-box-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_cmakezd2boxzd2locz00zzcgen_copz00(BgL_cmakezd2boxzd2_bglt BgL_oz00_346)
	{
		{	/* Cgen/cop.sch 650 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_346)))->BgL_locz00);
		}

	}



/* &cmake-box-loc */
	obj_t BGl_z62cmakezd2boxzd2locz62zzcgen_copz00(obj_t BgL_envz00_5260,
		obj_t BgL_oz00_5261)
	{
		{	/* Cgen/cop.sch 650 */
			return
				BGl_cmakezd2boxzd2locz00zzcgen_copz00(
				((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5261));
		}

	}



/* cmake-box-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cmakezd2boxzd2loczd2setz12zc0zzcgen_copz00(BgL_cmakezd2boxzd2_bglt
		BgL_oz00_347, obj_t BgL_vz00_348)
	{
		{	/* Cgen/cop.sch 651 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_347)))->BgL_locz00) =
				((obj_t) BgL_vz00_348), BUNSPEC);
		}

	}



/* &cmake-box-loc-set! */
	obj_t BGl_z62cmakezd2boxzd2loczd2setz12za2zzcgen_copz00(obj_t BgL_envz00_5262,
		obj_t BgL_oz00_5263, obj_t BgL_vz00_5264)
	{
		{	/* Cgen/cop.sch 651 */
			return
				BGl_cmakezd2boxzd2loczd2setz12zc0zzcgen_copz00(
				((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5263), BgL_vz00_5264);
		}

	}



/* make-cbox-ref */
	BGL_EXPORTED_DEF BgL_cboxzd2refzd2_bglt
		BGl_makezd2cboxzd2refz00zzcgen_copz00(obj_t BgL_loc1373z00_349,
		BgL_typez00_bglt BgL_type1374z00_350, BgL_copz00_bglt BgL_var1375z00_351)
	{
		{	/* Cgen/cop.sch 654 */
			{	/* Cgen/cop.sch 654 */
				BgL_cboxzd2refzd2_bglt BgL_new1364z00_6581;

				{	/* Cgen/cop.sch 654 */
					BgL_cboxzd2refzd2_bglt BgL_new1363z00_6582;

					BgL_new1363z00_6582 =
						((BgL_cboxzd2refzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cboxzd2refzd2_bgl))));
					{	/* Cgen/cop.sch 654 */
						long BgL_arg1691z00_6583;

						BgL_arg1691z00_6583 = BGL_CLASS_NUM(BGl_cboxzd2refzd2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1363z00_6582), BgL_arg1691z00_6583);
					}
					BgL_new1364z00_6581 = BgL_new1363z00_6582;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1364z00_6581)))->BgL_locz00) =
					((obj_t) BgL_loc1373z00_349), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1364z00_6581)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1374z00_350), BUNSPEC);
				((((BgL_cboxzd2refzd2_bglt) COBJECT(BgL_new1364z00_6581))->BgL_varz00) =
					((BgL_copz00_bglt) BgL_var1375z00_351), BUNSPEC);
				return BgL_new1364z00_6581;
			}
		}

	}



/* &make-cbox-ref */
	BgL_cboxzd2refzd2_bglt BGl_z62makezd2cboxzd2refz62zzcgen_copz00(obj_t
		BgL_envz00_5265, obj_t BgL_loc1373z00_5266, obj_t BgL_type1374z00_5267,
		obj_t BgL_var1375z00_5268)
	{
		{	/* Cgen/cop.sch 654 */
			return
				BGl_makezd2cboxzd2refz00zzcgen_copz00(BgL_loc1373z00_5266,
				((BgL_typez00_bglt) BgL_type1374z00_5267),
				((BgL_copz00_bglt) BgL_var1375z00_5268));
		}

	}



/* cbox-ref? */
	BGL_EXPORTED_DEF bool_t BGl_cboxzd2refzf3z21zzcgen_copz00(obj_t
		BgL_objz00_352)
	{
		{	/* Cgen/cop.sch 655 */
			{	/* Cgen/cop.sch 655 */
				obj_t BgL_classz00_6584;

				BgL_classz00_6584 = BGl_cboxzd2refzd2zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_352))
					{	/* Cgen/cop.sch 655 */
						BgL_objectz00_bglt BgL_arg1807z00_6585;

						BgL_arg1807z00_6585 = (BgL_objectz00_bglt) (BgL_objz00_352);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 655 */
								long BgL_idxz00_6586;

								BgL_idxz00_6586 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6585);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6586 + 2L)) == BgL_classz00_6584);
							}
						else
							{	/* Cgen/cop.sch 655 */
								bool_t BgL_res2437z00_6589;

								{	/* Cgen/cop.sch 655 */
									obj_t BgL_oclassz00_6590;

									{	/* Cgen/cop.sch 655 */
										obj_t BgL_arg1815z00_6591;
										long BgL_arg1816z00_6592;

										BgL_arg1815z00_6591 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 655 */
											long BgL_arg1817z00_6593;

											BgL_arg1817z00_6593 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6585);
											BgL_arg1816z00_6592 = (BgL_arg1817z00_6593 - OBJECT_TYPE);
										}
										BgL_oclassz00_6590 =
											VECTOR_REF(BgL_arg1815z00_6591, BgL_arg1816z00_6592);
									}
									{	/* Cgen/cop.sch 655 */
										bool_t BgL__ortest_1115z00_6594;

										BgL__ortest_1115z00_6594 =
											(BgL_classz00_6584 == BgL_oclassz00_6590);
										if (BgL__ortest_1115z00_6594)
											{	/* Cgen/cop.sch 655 */
												BgL_res2437z00_6589 = BgL__ortest_1115z00_6594;
											}
										else
											{	/* Cgen/cop.sch 655 */
												long BgL_odepthz00_6595;

												{	/* Cgen/cop.sch 655 */
													obj_t BgL_arg1804z00_6596;

													BgL_arg1804z00_6596 = (BgL_oclassz00_6590);
													BgL_odepthz00_6595 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6596);
												}
												if ((2L < BgL_odepthz00_6595))
													{	/* Cgen/cop.sch 655 */
														obj_t BgL_arg1802z00_6597;

														{	/* Cgen/cop.sch 655 */
															obj_t BgL_arg1803z00_6598;

															BgL_arg1803z00_6598 = (BgL_oclassz00_6590);
															BgL_arg1802z00_6597 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6598,
																2L);
														}
														BgL_res2437z00_6589 =
															(BgL_arg1802z00_6597 == BgL_classz00_6584);
													}
												else
													{	/* Cgen/cop.sch 655 */
														BgL_res2437z00_6589 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2437z00_6589;
							}
					}
				else
					{	/* Cgen/cop.sch 655 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cbox-ref? */
	obj_t BGl_z62cboxzd2refzf3z43zzcgen_copz00(obj_t BgL_envz00_5269,
		obj_t BgL_objz00_5270)
	{
		{	/* Cgen/cop.sch 655 */
			return BBOOL(BGl_cboxzd2refzf3z21zzcgen_copz00(BgL_objz00_5270));
		}

	}



/* cbox-ref-nil */
	BGL_EXPORTED_DEF BgL_cboxzd2refzd2_bglt
		BGl_cboxzd2refzd2nilz00zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 656 */
			{	/* Cgen/cop.sch 656 */
				obj_t BgL_classz00_4270;

				BgL_classz00_4270 = BGl_cboxzd2refzd2zzcgen_copz00;
				{	/* Cgen/cop.sch 656 */
					obj_t BgL__ortest_1117z00_4271;

					BgL__ortest_1117z00_4271 = BGL_CLASS_NIL(BgL_classz00_4270);
					if (CBOOL(BgL__ortest_1117z00_4271))
						{	/* Cgen/cop.sch 656 */
							return ((BgL_cboxzd2refzd2_bglt) BgL__ortest_1117z00_4271);
						}
					else
						{	/* Cgen/cop.sch 656 */
							return
								((BgL_cboxzd2refzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4270));
						}
				}
			}
		}

	}



/* &cbox-ref-nil */
	BgL_cboxzd2refzd2_bglt BGl_z62cboxzd2refzd2nilz62zzcgen_copz00(obj_t
		BgL_envz00_5271)
	{
		{	/* Cgen/cop.sch 656 */
			return BGl_cboxzd2refzd2nilz00zzcgen_copz00();
		}

	}



/* cbox-ref-var */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cboxzd2refzd2varz00zzcgen_copz00(BgL_cboxzd2refzd2_bglt BgL_oz00_353)
	{
		{	/* Cgen/cop.sch 657 */
			return (((BgL_cboxzd2refzd2_bglt) COBJECT(BgL_oz00_353))->BgL_varz00);
		}

	}



/* &cbox-ref-var */
	BgL_copz00_bglt BGl_z62cboxzd2refzd2varz62zzcgen_copz00(obj_t BgL_envz00_5272,
		obj_t BgL_oz00_5273)
	{
		{	/* Cgen/cop.sch 657 */
			return
				BGl_cboxzd2refzd2varz00zzcgen_copz00(
				((BgL_cboxzd2refzd2_bglt) BgL_oz00_5273));
		}

	}



/* cbox-ref-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cboxzd2refzd2typez00zzcgen_copz00(BgL_cboxzd2refzd2_bglt BgL_oz00_356)
	{
		{	/* Cgen/cop.sch 659 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_356)))->BgL_typez00);
		}

	}



/* &cbox-ref-type */
	BgL_typez00_bglt BGl_z62cboxzd2refzd2typez62zzcgen_copz00(obj_t
		BgL_envz00_5274, obj_t BgL_oz00_5275)
	{
		{	/* Cgen/cop.sch 659 */
			return
				BGl_cboxzd2refzd2typez00zzcgen_copz00(
				((BgL_cboxzd2refzd2_bglt) BgL_oz00_5275));
		}

	}



/* cbox-ref-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_cboxzd2refzd2locz00zzcgen_copz00(BgL_cboxzd2refzd2_bglt BgL_oz00_359)
	{
		{	/* Cgen/cop.sch 661 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_359)))->BgL_locz00);
		}

	}



/* &cbox-ref-loc */
	obj_t BGl_z62cboxzd2refzd2locz62zzcgen_copz00(obj_t BgL_envz00_5276,
		obj_t BgL_oz00_5277)
	{
		{	/* Cgen/cop.sch 661 */
			return
				BGl_cboxzd2refzd2locz00zzcgen_copz00(
				((BgL_cboxzd2refzd2_bglt) BgL_oz00_5277));
		}

	}



/* cbox-ref-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cboxzd2refzd2loczd2setz12zc0zzcgen_copz00(BgL_cboxzd2refzd2_bglt
		BgL_oz00_360, obj_t BgL_vz00_361)
	{
		{	/* Cgen/cop.sch 662 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_360)))->BgL_locz00) =
				((obj_t) BgL_vz00_361), BUNSPEC);
		}

	}



/* &cbox-ref-loc-set! */
	obj_t BGl_z62cboxzd2refzd2loczd2setz12za2zzcgen_copz00(obj_t BgL_envz00_5278,
		obj_t BgL_oz00_5279, obj_t BgL_vz00_5280)
	{
		{	/* Cgen/cop.sch 662 */
			return
				BGl_cboxzd2refzd2loczd2setz12zc0zzcgen_copz00(
				((BgL_cboxzd2refzd2_bglt) BgL_oz00_5279), BgL_vz00_5280);
		}

	}



/* make-cbox-set! */
	BGL_EXPORTED_DEF BgL_cboxzd2setz12zc0_bglt
		BGl_makezd2cboxzd2setz12z12zzcgen_copz00(obj_t BgL_loc1368z00_362,
		BgL_typez00_bglt BgL_type1369z00_363, BgL_copz00_bglt BgL_var1370z00_364,
		BgL_copz00_bglt BgL_value1371z00_365)
	{
		{	/* Cgen/cop.sch 665 */
			{	/* Cgen/cop.sch 665 */
				BgL_cboxzd2setz12zc0_bglt BgL_new1366z00_6599;

				{	/* Cgen/cop.sch 665 */
					BgL_cboxzd2setz12zc0_bglt BgL_new1365z00_6600;

					BgL_new1365z00_6600 =
						((BgL_cboxzd2setz12zc0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cboxzd2setz12zc0_bgl))));
					{	/* Cgen/cop.sch 665 */
						long BgL_arg1692z00_6601;

						BgL_arg1692z00_6601 =
							BGL_CLASS_NUM(BGl_cboxzd2setz12zc0zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1365z00_6600), BgL_arg1692z00_6601);
					}
					BgL_new1366z00_6599 = BgL_new1365z00_6600;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1366z00_6599)))->BgL_locz00) =
					((obj_t) BgL_loc1368z00_362), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1366z00_6599)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1369z00_363), BUNSPEC);
				((((BgL_cboxzd2setz12zc0_bglt) COBJECT(BgL_new1366z00_6599))->
						BgL_varz00) = ((BgL_copz00_bglt) BgL_var1370z00_364), BUNSPEC);
				((((BgL_cboxzd2setz12zc0_bglt) COBJECT(BgL_new1366z00_6599))->
						BgL_valuez00) = ((BgL_copz00_bglt) BgL_value1371z00_365), BUNSPEC);
				return BgL_new1366z00_6599;
			}
		}

	}



/* &make-cbox-set! */
	BgL_cboxzd2setz12zc0_bglt BGl_z62makezd2cboxzd2setz12z70zzcgen_copz00(obj_t
		BgL_envz00_5281, obj_t BgL_loc1368z00_5282, obj_t BgL_type1369z00_5283,
		obj_t BgL_var1370z00_5284, obj_t BgL_value1371z00_5285)
	{
		{	/* Cgen/cop.sch 665 */
			return
				BGl_makezd2cboxzd2setz12z12zzcgen_copz00(BgL_loc1368z00_5282,
				((BgL_typez00_bglt) BgL_type1369z00_5283),
				((BgL_copz00_bglt) BgL_var1370z00_5284),
				((BgL_copz00_bglt) BgL_value1371z00_5285));
		}

	}



/* cbox-set!? */
	BGL_EXPORTED_DEF bool_t BGl_cboxzd2setz12zf3z33zzcgen_copz00(obj_t
		BgL_objz00_366)
	{
		{	/* Cgen/cop.sch 666 */
			{	/* Cgen/cop.sch 666 */
				obj_t BgL_classz00_6602;

				BgL_classz00_6602 = BGl_cboxzd2setz12zc0zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_366))
					{	/* Cgen/cop.sch 666 */
						BgL_objectz00_bglt BgL_arg1807z00_6603;

						BgL_arg1807z00_6603 = (BgL_objectz00_bglt) (BgL_objz00_366);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 666 */
								long BgL_idxz00_6604;

								BgL_idxz00_6604 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6603);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6604 + 2L)) == BgL_classz00_6602);
							}
						else
							{	/* Cgen/cop.sch 666 */
								bool_t BgL_res2438z00_6607;

								{	/* Cgen/cop.sch 666 */
									obj_t BgL_oclassz00_6608;

									{	/* Cgen/cop.sch 666 */
										obj_t BgL_arg1815z00_6609;
										long BgL_arg1816z00_6610;

										BgL_arg1815z00_6609 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 666 */
											long BgL_arg1817z00_6611;

											BgL_arg1817z00_6611 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6603);
											BgL_arg1816z00_6610 = (BgL_arg1817z00_6611 - OBJECT_TYPE);
										}
										BgL_oclassz00_6608 =
											VECTOR_REF(BgL_arg1815z00_6609, BgL_arg1816z00_6610);
									}
									{	/* Cgen/cop.sch 666 */
										bool_t BgL__ortest_1115z00_6612;

										BgL__ortest_1115z00_6612 =
											(BgL_classz00_6602 == BgL_oclassz00_6608);
										if (BgL__ortest_1115z00_6612)
											{	/* Cgen/cop.sch 666 */
												BgL_res2438z00_6607 = BgL__ortest_1115z00_6612;
											}
										else
											{	/* Cgen/cop.sch 666 */
												long BgL_odepthz00_6613;

												{	/* Cgen/cop.sch 666 */
													obj_t BgL_arg1804z00_6614;

													BgL_arg1804z00_6614 = (BgL_oclassz00_6608);
													BgL_odepthz00_6613 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6614);
												}
												if ((2L < BgL_odepthz00_6613))
													{	/* Cgen/cop.sch 666 */
														obj_t BgL_arg1802z00_6615;

														{	/* Cgen/cop.sch 666 */
															obj_t BgL_arg1803z00_6616;

															BgL_arg1803z00_6616 = (BgL_oclassz00_6608);
															BgL_arg1802z00_6615 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6616,
																2L);
														}
														BgL_res2438z00_6607 =
															(BgL_arg1802z00_6615 == BgL_classz00_6602);
													}
												else
													{	/* Cgen/cop.sch 666 */
														BgL_res2438z00_6607 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2438z00_6607;
							}
					}
				else
					{	/* Cgen/cop.sch 666 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cbox-set!? */
	obj_t BGl_z62cboxzd2setz12zf3z51zzcgen_copz00(obj_t BgL_envz00_5286,
		obj_t BgL_objz00_5287)
	{
		{	/* Cgen/cop.sch 666 */
			return BBOOL(BGl_cboxzd2setz12zf3z33zzcgen_copz00(BgL_objz00_5287));
		}

	}



/* cbox-set!-nil */
	BGL_EXPORTED_DEF BgL_cboxzd2setz12zc0_bglt
		BGl_cboxzd2setz12zd2nilz12zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 667 */
			{	/* Cgen/cop.sch 667 */
				obj_t BgL_classz00_4312;

				BgL_classz00_4312 = BGl_cboxzd2setz12zc0zzcgen_copz00;
				{	/* Cgen/cop.sch 667 */
					obj_t BgL__ortest_1117z00_4313;

					BgL__ortest_1117z00_4313 = BGL_CLASS_NIL(BgL_classz00_4312);
					if (CBOOL(BgL__ortest_1117z00_4313))
						{	/* Cgen/cop.sch 667 */
							return ((BgL_cboxzd2setz12zc0_bglt) BgL__ortest_1117z00_4313);
						}
					else
						{	/* Cgen/cop.sch 667 */
							return
								((BgL_cboxzd2setz12zc0_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4312));
						}
				}
			}
		}

	}



/* &cbox-set!-nil */
	BgL_cboxzd2setz12zc0_bglt BGl_z62cboxzd2setz12zd2nilz70zzcgen_copz00(obj_t
		BgL_envz00_5288)
	{
		{	/* Cgen/cop.sch 667 */
			return BGl_cboxzd2setz12zd2nilz12zzcgen_copz00();
		}

	}



/* cbox-set!-value */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cboxzd2setz12zd2valuez12zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt
		BgL_oz00_367)
	{
		{	/* Cgen/cop.sch 668 */
			return
				(((BgL_cboxzd2setz12zc0_bglt) COBJECT(BgL_oz00_367))->BgL_valuez00);
		}

	}



/* &cbox-set!-value */
	BgL_copz00_bglt BGl_z62cboxzd2setz12zd2valuez70zzcgen_copz00(obj_t
		BgL_envz00_5289, obj_t BgL_oz00_5290)
	{
		{	/* Cgen/cop.sch 668 */
			return
				BGl_cboxzd2setz12zd2valuez12zzcgen_copz00(
				((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5290));
		}

	}



/* cbox-set!-var */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cboxzd2setz12zd2varz12zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt
		BgL_oz00_370)
	{
		{	/* Cgen/cop.sch 670 */
			return (((BgL_cboxzd2setz12zc0_bglt) COBJECT(BgL_oz00_370))->BgL_varz00);
		}

	}



/* &cbox-set!-var */
	BgL_copz00_bglt BGl_z62cboxzd2setz12zd2varz70zzcgen_copz00(obj_t
		BgL_envz00_5291, obj_t BgL_oz00_5292)
	{
		{	/* Cgen/cop.sch 670 */
			return
				BGl_cboxzd2setz12zd2varz12zzcgen_copz00(
				((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5292));
		}

	}



/* cbox-set!-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cboxzd2setz12zd2typez12zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt
		BgL_oz00_373)
	{
		{	/* Cgen/cop.sch 672 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_373)))->BgL_typez00);
		}

	}



/* &cbox-set!-type */
	BgL_typez00_bglt BGl_z62cboxzd2setz12zd2typez70zzcgen_copz00(obj_t
		BgL_envz00_5293, obj_t BgL_oz00_5294)
	{
		{	/* Cgen/cop.sch 672 */
			return
				BGl_cboxzd2setz12zd2typez12zzcgen_copz00(
				((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5294));
		}

	}



/* cbox-set!-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_cboxzd2setz12zd2locz12zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt
		BgL_oz00_376)
	{
		{	/* Cgen/cop.sch 674 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_376)))->BgL_locz00);
		}

	}



/* &cbox-set!-loc */
	obj_t BGl_z62cboxzd2setz12zd2locz70zzcgen_copz00(obj_t BgL_envz00_5295,
		obj_t BgL_oz00_5296)
	{
		{	/* Cgen/cop.sch 674 */
			return
				BGl_cboxzd2setz12zd2locz12zzcgen_copz00(
				((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5296));
		}

	}



/* cbox-set!-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cboxzd2setz12zd2loczd2setz12zd2zzcgen_copz00(BgL_cboxzd2setz12zc0_bglt
		BgL_oz00_377, obj_t BgL_vz00_378)
	{
		{	/* Cgen/cop.sch 675 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_377)))->BgL_locz00) =
				((obj_t) BgL_vz00_378), BUNSPEC);
		}

	}



/* &cbox-set!-loc-set! */
	obj_t BGl_z62cboxzd2setz12zd2loczd2setz12zb0zzcgen_copz00(obj_t
		BgL_envz00_5297, obj_t BgL_oz00_5298, obj_t BgL_vz00_5299)
	{
		{	/* Cgen/cop.sch 675 */
			return
				BGl_cboxzd2setz12zd2loczd2setz12zd2zzcgen_copz00(
				((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5298), BgL_vz00_5299);
		}

	}



/* make-cset-ex-it */
	BGL_EXPORTED_DEF BgL_csetzd2exzd2itz00_bglt
		BGl_makezd2csetzd2exzd2itzd2zzcgen_copz00(obj_t BgL_loc1362z00_379,
		BgL_typez00_bglt BgL_type1363z00_380, BgL_copz00_bglt BgL_exit1364z00_381,
		BgL_copz00_bglt BgL_jumpzd2value1365zd2_382,
		BgL_copz00_bglt BgL_body1366z00_383)
	{
		{	/* Cgen/cop.sch 678 */
			{	/* Cgen/cop.sch 678 */
				BgL_csetzd2exzd2itz00_bglt BgL_new1368z00_6617;

				{	/* Cgen/cop.sch 678 */
					BgL_csetzd2exzd2itz00_bglt BgL_new1367z00_6618;

					BgL_new1367z00_6618 =
						((BgL_csetzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_csetzd2exzd2itz00_bgl))));
					{	/* Cgen/cop.sch 678 */
						long BgL_arg1699z00_6619;

						BgL_arg1699z00_6619 =
							BGL_CLASS_NUM(BGl_csetzd2exzd2itz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1367z00_6618), BgL_arg1699z00_6619);
					}
					BgL_new1368z00_6617 = BgL_new1367z00_6618;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1368z00_6617)))->BgL_locz00) =
					((obj_t) BgL_loc1362z00_379), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1368z00_6617)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1363z00_380), BUNSPEC);
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_new1368z00_6617))->
						BgL_exitz00) = ((BgL_copz00_bglt) BgL_exit1364z00_381), BUNSPEC);
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_new1368z00_6617))->
						BgL_jumpzd2valuezd2) =
					((BgL_copz00_bglt) BgL_jumpzd2value1365zd2_382), BUNSPEC);
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_new1368z00_6617))->
						BgL_bodyz00) = ((BgL_copz00_bglt) BgL_body1366z00_383), BUNSPEC);
				return BgL_new1368z00_6617;
			}
		}

	}



/* &make-cset-ex-it */
	BgL_csetzd2exzd2itz00_bglt BGl_z62makezd2csetzd2exzd2itzb0zzcgen_copz00(obj_t
		BgL_envz00_5300, obj_t BgL_loc1362z00_5301, obj_t BgL_type1363z00_5302,
		obj_t BgL_exit1364z00_5303, obj_t BgL_jumpzd2value1365zd2_5304,
		obj_t BgL_body1366z00_5305)
	{
		{	/* Cgen/cop.sch 678 */
			return
				BGl_makezd2csetzd2exzd2itzd2zzcgen_copz00(BgL_loc1362z00_5301,
				((BgL_typez00_bglt) BgL_type1363z00_5302),
				((BgL_copz00_bglt) BgL_exit1364z00_5303),
				((BgL_copz00_bglt) BgL_jumpzd2value1365zd2_5304),
				((BgL_copz00_bglt) BgL_body1366z00_5305));
		}

	}



/* cset-ex-it? */
	BGL_EXPORTED_DEF bool_t BGl_csetzd2exzd2itzf3zf3zzcgen_copz00(obj_t
		BgL_objz00_384)
	{
		{	/* Cgen/cop.sch 679 */
			{	/* Cgen/cop.sch 679 */
				obj_t BgL_classz00_6620;

				BgL_classz00_6620 = BGl_csetzd2exzd2itz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_384))
					{	/* Cgen/cop.sch 679 */
						BgL_objectz00_bglt BgL_arg1807z00_6621;

						BgL_arg1807z00_6621 = (BgL_objectz00_bglt) (BgL_objz00_384);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 679 */
								long BgL_idxz00_6622;

								BgL_idxz00_6622 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6621);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6622 + 2L)) == BgL_classz00_6620);
							}
						else
							{	/* Cgen/cop.sch 679 */
								bool_t BgL_res2439z00_6625;

								{	/* Cgen/cop.sch 679 */
									obj_t BgL_oclassz00_6626;

									{	/* Cgen/cop.sch 679 */
										obj_t BgL_arg1815z00_6627;
										long BgL_arg1816z00_6628;

										BgL_arg1815z00_6627 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 679 */
											long BgL_arg1817z00_6629;

											BgL_arg1817z00_6629 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6621);
											BgL_arg1816z00_6628 = (BgL_arg1817z00_6629 - OBJECT_TYPE);
										}
										BgL_oclassz00_6626 =
											VECTOR_REF(BgL_arg1815z00_6627, BgL_arg1816z00_6628);
									}
									{	/* Cgen/cop.sch 679 */
										bool_t BgL__ortest_1115z00_6630;

										BgL__ortest_1115z00_6630 =
											(BgL_classz00_6620 == BgL_oclassz00_6626);
										if (BgL__ortest_1115z00_6630)
											{	/* Cgen/cop.sch 679 */
												BgL_res2439z00_6625 = BgL__ortest_1115z00_6630;
											}
										else
											{	/* Cgen/cop.sch 679 */
												long BgL_odepthz00_6631;

												{	/* Cgen/cop.sch 679 */
													obj_t BgL_arg1804z00_6632;

													BgL_arg1804z00_6632 = (BgL_oclassz00_6626);
													BgL_odepthz00_6631 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6632);
												}
												if ((2L < BgL_odepthz00_6631))
													{	/* Cgen/cop.sch 679 */
														obj_t BgL_arg1802z00_6633;

														{	/* Cgen/cop.sch 679 */
															obj_t BgL_arg1803z00_6634;

															BgL_arg1803z00_6634 = (BgL_oclassz00_6626);
															BgL_arg1802z00_6633 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6634,
																2L);
														}
														BgL_res2439z00_6625 =
															(BgL_arg1802z00_6633 == BgL_classz00_6620);
													}
												else
													{	/* Cgen/cop.sch 679 */
														BgL_res2439z00_6625 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2439z00_6625;
							}
					}
				else
					{	/* Cgen/cop.sch 679 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cset-ex-it? */
	obj_t BGl_z62csetzd2exzd2itzf3z91zzcgen_copz00(obj_t BgL_envz00_5306,
		obj_t BgL_objz00_5307)
	{
		{	/* Cgen/cop.sch 679 */
			return BBOOL(BGl_csetzd2exzd2itzf3zf3zzcgen_copz00(BgL_objz00_5307));
		}

	}



/* cset-ex-it-nil */
	BGL_EXPORTED_DEF BgL_csetzd2exzd2itz00_bglt
		BGl_csetzd2exzd2itzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 680 */
			{	/* Cgen/cop.sch 680 */
				obj_t BgL_classz00_4354;

				BgL_classz00_4354 = BGl_csetzd2exzd2itz00zzcgen_copz00;
				{	/* Cgen/cop.sch 680 */
					obj_t BgL__ortest_1117z00_4355;

					BgL__ortest_1117z00_4355 = BGL_CLASS_NIL(BgL_classz00_4354);
					if (CBOOL(BgL__ortest_1117z00_4355))
						{	/* Cgen/cop.sch 680 */
							return ((BgL_csetzd2exzd2itz00_bglt) BgL__ortest_1117z00_4355);
						}
					else
						{	/* Cgen/cop.sch 680 */
							return
								((BgL_csetzd2exzd2itz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4354));
						}
				}
			}
		}

	}



/* &cset-ex-it-nil */
	BgL_csetzd2exzd2itz00_bglt BGl_z62csetzd2exzd2itzd2nilzb0zzcgen_copz00(obj_t
		BgL_envz00_5308)
	{
		{	/* Cgen/cop.sch 680 */
			return BGl_csetzd2exzd2itzd2nilzd2zzcgen_copz00();
		}

	}



/* cset-ex-it-body */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_csetzd2exzd2itzd2bodyzd2zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt
		BgL_oz00_385)
	{
		{	/* Cgen/cop.sch 681 */
			return
				(((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_oz00_385))->BgL_bodyz00);
		}

	}



/* &cset-ex-it-body */
	BgL_copz00_bglt BGl_z62csetzd2exzd2itzd2bodyzb0zzcgen_copz00(obj_t
		BgL_envz00_5309, obj_t BgL_oz00_5310)
	{
		{	/* Cgen/cop.sch 681 */
			return
				BGl_csetzd2exzd2itzd2bodyzd2zzcgen_copz00(
				((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5310));
		}

	}



/* cset-ex-it-jump-value */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_csetzd2exzd2itzd2jumpzd2valuez00zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt
		BgL_oz00_388)
	{
		{	/* Cgen/cop.sch 683 */
			return
				(((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_oz00_388))->
				BgL_jumpzd2valuezd2);
		}

	}



/* &cset-ex-it-jump-value */
	BgL_copz00_bglt BGl_z62csetzd2exzd2itzd2jumpzd2valuez62zzcgen_copz00(obj_t
		BgL_envz00_5311, obj_t BgL_oz00_5312)
	{
		{	/* Cgen/cop.sch 683 */
			return
				BGl_csetzd2exzd2itzd2jumpzd2valuez00zzcgen_copz00(
				((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5312));
		}

	}



/* cset-ex-it-exit */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_csetzd2exzd2itzd2exitzd2zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt
		BgL_oz00_391)
	{
		{	/* Cgen/cop.sch 685 */
			return
				(((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_oz00_391))->BgL_exitz00);
		}

	}



/* &cset-ex-it-exit */
	BgL_copz00_bglt BGl_z62csetzd2exzd2itzd2exitzb0zzcgen_copz00(obj_t
		BgL_envz00_5313, obj_t BgL_oz00_5314)
	{
		{	/* Cgen/cop.sch 685 */
			return
				BGl_csetzd2exzd2itzd2exitzd2zzcgen_copz00(
				((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5314));
		}

	}



/* cset-ex-it-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_csetzd2exzd2itzd2typezd2zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt
		BgL_oz00_394)
	{
		{	/* Cgen/cop.sch 687 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_394)))->BgL_typez00);
		}

	}



/* &cset-ex-it-type */
	BgL_typez00_bglt BGl_z62csetzd2exzd2itzd2typezb0zzcgen_copz00(obj_t
		BgL_envz00_5315, obj_t BgL_oz00_5316)
	{
		{	/* Cgen/cop.sch 687 */
			return
				BGl_csetzd2exzd2itzd2typezd2zzcgen_copz00(
				((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5316));
		}

	}



/* cset-ex-it-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_csetzd2exzd2itzd2loczd2zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt
		BgL_oz00_397)
	{
		{	/* Cgen/cop.sch 689 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_397)))->BgL_locz00);
		}

	}



/* &cset-ex-it-loc */
	obj_t BGl_z62csetzd2exzd2itzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5317,
		obj_t BgL_oz00_5318)
	{
		{	/* Cgen/cop.sch 689 */
			return
				BGl_csetzd2exzd2itzd2loczd2zzcgen_copz00(
				((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5318));
		}

	}



/* cset-ex-it-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_csetzd2exzd2itzd2loczd2setz12z12zzcgen_copz00(BgL_csetzd2exzd2itz00_bglt
		BgL_oz00_398, obj_t BgL_vz00_399)
	{
		{	/* Cgen/cop.sch 690 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_398)))->BgL_locz00) =
				((obj_t) BgL_vz00_399), BUNSPEC);
		}

	}



/* &cset-ex-it-loc-set! */
	obj_t BGl_z62csetzd2exzd2itzd2loczd2setz12z70zzcgen_copz00(obj_t
		BgL_envz00_5319, obj_t BgL_oz00_5320, obj_t BgL_vz00_5321)
	{
		{	/* Cgen/cop.sch 690 */
			return
				BGl_csetzd2exzd2itzd2loczd2setz12z12zzcgen_copz00(
				((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5320), BgL_vz00_5321);
		}

	}



/* make-cjump-ex-it */
	BGL_EXPORTED_DEF BgL_cjumpzd2exzd2itz00_bglt
		BGl_makezd2cjumpzd2exzd2itzd2zzcgen_copz00(obj_t BgL_loc1357z00_400,
		BgL_typez00_bglt BgL_type1358z00_401, BgL_copz00_bglt BgL_exit1359z00_402,
		BgL_copz00_bglt BgL_value1360z00_403)
	{
		{	/* Cgen/cop.sch 693 */
			{	/* Cgen/cop.sch 693 */
				BgL_cjumpzd2exzd2itz00_bglt BgL_new1370z00_6635;

				{	/* Cgen/cop.sch 693 */
					BgL_cjumpzd2exzd2itz00_bglt BgL_new1369z00_6636;

					BgL_new1369z00_6636 =
						((BgL_cjumpzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cjumpzd2exzd2itz00_bgl))));
					{	/* Cgen/cop.sch 693 */
						long BgL_arg1700z00_6637;

						BgL_arg1700z00_6637 =
							BGL_CLASS_NUM(BGl_cjumpzd2exzd2itz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1369z00_6636), BgL_arg1700z00_6637);
					}
					BgL_new1370z00_6635 = BgL_new1369z00_6636;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1370z00_6635)))->BgL_locz00) =
					((obj_t) BgL_loc1357z00_400), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1370z00_6635)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1358z00_401), BUNSPEC);
				((((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(BgL_new1370z00_6635))->
						BgL_exitz00) = ((BgL_copz00_bglt) BgL_exit1359z00_402), BUNSPEC);
				((((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(BgL_new1370z00_6635))->
						BgL_valuez00) = ((BgL_copz00_bglt) BgL_value1360z00_403), BUNSPEC);
				return BgL_new1370z00_6635;
			}
		}

	}



/* &make-cjump-ex-it */
	BgL_cjumpzd2exzd2itz00_bglt
		BGl_z62makezd2cjumpzd2exzd2itzb0zzcgen_copz00(obj_t BgL_envz00_5322,
		obj_t BgL_loc1357z00_5323, obj_t BgL_type1358z00_5324,
		obj_t BgL_exit1359z00_5325, obj_t BgL_value1360z00_5326)
	{
		{	/* Cgen/cop.sch 693 */
			return
				BGl_makezd2cjumpzd2exzd2itzd2zzcgen_copz00(BgL_loc1357z00_5323,
				((BgL_typez00_bglt) BgL_type1358z00_5324),
				((BgL_copz00_bglt) BgL_exit1359z00_5325),
				((BgL_copz00_bglt) BgL_value1360z00_5326));
		}

	}



/* cjump-ex-it? */
	BGL_EXPORTED_DEF bool_t BGl_cjumpzd2exzd2itzf3zf3zzcgen_copz00(obj_t
		BgL_objz00_404)
	{
		{	/* Cgen/cop.sch 694 */
			{	/* Cgen/cop.sch 694 */
				obj_t BgL_classz00_6638;

				BgL_classz00_6638 = BGl_cjumpzd2exzd2itz00zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_404))
					{	/* Cgen/cop.sch 694 */
						BgL_objectz00_bglt BgL_arg1807z00_6639;

						BgL_arg1807z00_6639 = (BgL_objectz00_bglt) (BgL_objz00_404);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 694 */
								long BgL_idxz00_6640;

								BgL_idxz00_6640 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6639);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6640 + 2L)) == BgL_classz00_6638);
							}
						else
							{	/* Cgen/cop.sch 694 */
								bool_t BgL_res2440z00_6643;

								{	/* Cgen/cop.sch 694 */
									obj_t BgL_oclassz00_6644;

									{	/* Cgen/cop.sch 694 */
										obj_t BgL_arg1815z00_6645;
										long BgL_arg1816z00_6646;

										BgL_arg1815z00_6645 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 694 */
											long BgL_arg1817z00_6647;

											BgL_arg1817z00_6647 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6639);
											BgL_arg1816z00_6646 = (BgL_arg1817z00_6647 - OBJECT_TYPE);
										}
										BgL_oclassz00_6644 =
											VECTOR_REF(BgL_arg1815z00_6645, BgL_arg1816z00_6646);
									}
									{	/* Cgen/cop.sch 694 */
										bool_t BgL__ortest_1115z00_6648;

										BgL__ortest_1115z00_6648 =
											(BgL_classz00_6638 == BgL_oclassz00_6644);
										if (BgL__ortest_1115z00_6648)
											{	/* Cgen/cop.sch 694 */
												BgL_res2440z00_6643 = BgL__ortest_1115z00_6648;
											}
										else
											{	/* Cgen/cop.sch 694 */
												long BgL_odepthz00_6649;

												{	/* Cgen/cop.sch 694 */
													obj_t BgL_arg1804z00_6650;

													BgL_arg1804z00_6650 = (BgL_oclassz00_6644);
													BgL_odepthz00_6649 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6650);
												}
												if ((2L < BgL_odepthz00_6649))
													{	/* Cgen/cop.sch 694 */
														obj_t BgL_arg1802z00_6651;

														{	/* Cgen/cop.sch 694 */
															obj_t BgL_arg1803z00_6652;

															BgL_arg1803z00_6652 = (BgL_oclassz00_6644);
															BgL_arg1802z00_6651 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6652,
																2L);
														}
														BgL_res2440z00_6643 =
															(BgL_arg1802z00_6651 == BgL_classz00_6638);
													}
												else
													{	/* Cgen/cop.sch 694 */
														BgL_res2440z00_6643 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2440z00_6643;
							}
					}
				else
					{	/* Cgen/cop.sch 694 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &cjump-ex-it? */
	obj_t BGl_z62cjumpzd2exzd2itzf3z91zzcgen_copz00(obj_t BgL_envz00_5327,
		obj_t BgL_objz00_5328)
	{
		{	/* Cgen/cop.sch 694 */
			return BBOOL(BGl_cjumpzd2exzd2itzf3zf3zzcgen_copz00(BgL_objz00_5328));
		}

	}



/* cjump-ex-it-nil */
	BGL_EXPORTED_DEF BgL_cjumpzd2exzd2itz00_bglt
		BGl_cjumpzd2exzd2itzd2nilzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 695 */
			{	/* Cgen/cop.sch 695 */
				obj_t BgL_classz00_4396;

				BgL_classz00_4396 = BGl_cjumpzd2exzd2itz00zzcgen_copz00;
				{	/* Cgen/cop.sch 695 */
					obj_t BgL__ortest_1117z00_4397;

					BgL__ortest_1117z00_4397 = BGL_CLASS_NIL(BgL_classz00_4396);
					if (CBOOL(BgL__ortest_1117z00_4397))
						{	/* Cgen/cop.sch 695 */
							return ((BgL_cjumpzd2exzd2itz00_bglt) BgL__ortest_1117z00_4397);
						}
					else
						{	/* Cgen/cop.sch 695 */
							return
								((BgL_cjumpzd2exzd2itz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4396));
						}
				}
			}
		}

	}



/* &cjump-ex-it-nil */
	BgL_cjumpzd2exzd2itz00_bglt BGl_z62cjumpzd2exzd2itzd2nilzb0zzcgen_copz00(obj_t
		BgL_envz00_5329)
	{
		{	/* Cgen/cop.sch 695 */
			return BGl_cjumpzd2exzd2itzd2nilzd2zzcgen_copz00();
		}

	}



/* cjump-ex-it-value */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cjumpzd2exzd2itzd2valuezd2zzcgen_copz00(BgL_cjumpzd2exzd2itz00_bglt
		BgL_oz00_405)
	{
		{	/* Cgen/cop.sch 696 */
			return
				(((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(BgL_oz00_405))->BgL_valuez00);
		}

	}



/* &cjump-ex-it-value */
	BgL_copz00_bglt BGl_z62cjumpzd2exzd2itzd2valuezb0zzcgen_copz00(obj_t
		BgL_envz00_5330, obj_t BgL_oz00_5331)
	{
		{	/* Cgen/cop.sch 696 */
			return
				BGl_cjumpzd2exzd2itzd2valuezd2zzcgen_copz00(
				((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5331));
		}

	}



/* cjump-ex-it-exit */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_cjumpzd2exzd2itzd2exitzd2zzcgen_copz00(BgL_cjumpzd2exzd2itz00_bglt
		BgL_oz00_408)
	{
		{	/* Cgen/cop.sch 698 */
			return
				(((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(BgL_oz00_408))->BgL_exitz00);
		}

	}



/* &cjump-ex-it-exit */
	BgL_copz00_bglt BGl_z62cjumpzd2exzd2itzd2exitzb0zzcgen_copz00(obj_t
		BgL_envz00_5332, obj_t BgL_oz00_5333)
	{
		{	/* Cgen/cop.sch 698 */
			return
				BGl_cjumpzd2exzd2itzd2exitzd2zzcgen_copz00(
				((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5333));
		}

	}



/* cjump-ex-it-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_cjumpzd2exzd2itzd2typezd2zzcgen_copz00(BgL_cjumpzd2exzd2itz00_bglt
		BgL_oz00_411)
	{
		{	/* Cgen/cop.sch 700 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_411)))->BgL_typez00);
		}

	}



/* &cjump-ex-it-type */
	BgL_typez00_bglt BGl_z62cjumpzd2exzd2itzd2typezb0zzcgen_copz00(obj_t
		BgL_envz00_5334, obj_t BgL_oz00_5335)
	{
		{	/* Cgen/cop.sch 700 */
			return
				BGl_cjumpzd2exzd2itzd2typezd2zzcgen_copz00(
				((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5335));
		}

	}



/* cjump-ex-it-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_cjumpzd2exzd2itzd2loczd2zzcgen_copz00(BgL_cjumpzd2exzd2itz00_bglt
		BgL_oz00_414)
	{
		{	/* Cgen/cop.sch 702 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_414)))->BgL_locz00);
		}

	}



/* &cjump-ex-it-loc */
	obj_t BGl_z62cjumpzd2exzd2itzd2loczb0zzcgen_copz00(obj_t BgL_envz00_5336,
		obj_t BgL_oz00_5337)
	{
		{	/* Cgen/cop.sch 702 */
			return
				BGl_cjumpzd2exzd2itzd2loczd2zzcgen_copz00(
				((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5337));
		}

	}



/* cjump-ex-it-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_cjumpzd2exzd2itzd2loczd2setz12z12zzcgen_copz00
		(BgL_cjumpzd2exzd2itz00_bglt BgL_oz00_415, obj_t BgL_vz00_416)
	{
		{	/* Cgen/cop.sch 703 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_415)))->BgL_locz00) =
				((obj_t) BgL_vz00_416), BUNSPEC);
		}

	}



/* &cjump-ex-it-loc-set! */
	obj_t BGl_z62cjumpzd2exzd2itzd2loczd2setz12z70zzcgen_copz00(obj_t
		BgL_envz00_5338, obj_t BgL_oz00_5339, obj_t BgL_vz00_5340)
	{
		{	/* Cgen/cop.sch 703 */
			return
				BGl_cjumpzd2exzd2itzd2loczd2setz12z12zzcgen_copz00(
				((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5339), BgL_vz00_5340);
		}

	}



/* make-sfun/C */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt BGl_makezd2sfunzf2Cz20zzcgen_copz00(long
		BgL_arity1332z00_417, obj_t BgL_sidezd2effect1333zd2_418,
		obj_t BgL_predicatezd2of1334zd2_419, obj_t BgL_stackzd2allocator1335zd2_420,
		bool_t BgL_topzf31336zf3_421, obj_t BgL_thezd2closure1337zd2_422,
		obj_t BgL_effect1338z00_423, obj_t BgL_failsafe1339z00_424,
		obj_t BgL_argszd2noescape1340zd2_425, obj_t BgL_argszd2retescape1341zd2_426,
		obj_t BgL_property1342z00_427, obj_t BgL_args1343z00_428,
		obj_t BgL_argszd2name1344zd2_429, obj_t BgL_body1345z00_430,
		obj_t BgL_class1346z00_431, obj_t BgL_dssslzd2keywords1347zd2_432,
		obj_t BgL_loc1348z00_433, obj_t BgL_optionals1349z00_434,
		obj_t BgL_keys1350z00_435, obj_t BgL_thezd2closurezd2global1351z00_436,
		obj_t BgL_strength1352z00_437, obj_t BgL_stackable1353z00_438,
		BgL_clabelz00_bglt BgL_label1354z00_439, bool_t BgL_integrated1355z00_440)
	{
		{	/* Cgen/cop.sch 706 */
			{	/* Cgen/cop.sch 706 */
				BgL_sfunz00_bglt BgL_new1374z00_6653;

				{	/* Cgen/cop.sch 706 */
					BgL_sfunz00_bglt BgL_tmp1372z00_6654;
					BgL_sfunzf2czf2_bglt BgL_wide1373z00_6655;

					{
						BgL_sfunz00_bglt BgL_auxz00_8824;

						{	/* Cgen/cop.sch 706 */
							BgL_sfunz00_bglt BgL_new1371z00_6656;

							BgL_new1371z00_6656 =
								((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sfunz00_bgl))));
							{	/* Cgen/cop.sch 706 */
								long BgL_arg1702z00_6657;

								BgL_arg1702z00_6657 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1371z00_6656),
									BgL_arg1702z00_6657);
							}
							{	/* Cgen/cop.sch 706 */
								BgL_objectz00_bglt BgL_tmpz00_8829;

								BgL_tmpz00_8829 = ((BgL_objectz00_bglt) BgL_new1371z00_6656);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8829, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1371z00_6656);
							BgL_auxz00_8824 = BgL_new1371z00_6656;
						}
						BgL_tmp1372z00_6654 = ((BgL_sfunz00_bglt) BgL_auxz00_8824);
					}
					BgL_wide1373z00_6655 =
						((BgL_sfunzf2czf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sfunzf2czf2_bgl))));
					{	/* Cgen/cop.sch 706 */
						obj_t BgL_auxz00_8837;
						BgL_objectz00_bglt BgL_tmpz00_8835;

						BgL_auxz00_8837 = ((obj_t) BgL_wide1373z00_6655);
						BgL_tmpz00_8835 = ((BgL_objectz00_bglt) BgL_tmp1372z00_6654);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8835, BgL_auxz00_8837);
					}
					((BgL_objectz00_bglt) BgL_tmp1372z00_6654);
					{	/* Cgen/cop.sch 706 */
						long BgL_arg1701z00_6658;

						BgL_arg1701z00_6658 = BGL_CLASS_NUM(BGl_sfunzf2Czf2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1372z00_6654), BgL_arg1701z00_6658);
					}
					BgL_new1374z00_6653 = ((BgL_sfunz00_bglt) BgL_tmp1372z00_6654);
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1374z00_6653)))->BgL_arityz00) =
					((long) BgL_arity1332z00_417), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1333zd2_418), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1334zd2_419), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1335zd2_420), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31336zf3_421), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1337zd2_422), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_effectz00) = ((obj_t) BgL_effect1338z00_423), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1339z00_424), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1340zd2_425), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1374z00_6653)))->
						BgL_argszd2retescapezd2) =
					((obj_t) BgL_argszd2retescape1341zd2_426), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_propertyz00) =
					((obj_t) BgL_property1342z00_427), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_argsz00) =
					((obj_t) BgL_args1343z00_428), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_argszd2namezd2) =
					((obj_t) BgL_argszd2name1344zd2_429), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_bodyz00) =
					((obj_t) BgL_body1345z00_430), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_classz00) =
					((obj_t) BgL_class1346z00_431), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_dssslzd2keywordszd2) =
					((obj_t) BgL_dssslzd2keywords1347zd2_432), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_locz00) =
					((obj_t) BgL_loc1348z00_433), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_optionalsz00) =
					((obj_t) BgL_optionals1349z00_434), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_keysz00) =
					((obj_t) BgL_keys1350z00_435), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BgL_thezd2closurezd2global1351z00_436), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_strengthz00) =
					((obj_t) BgL_strength1352z00_437), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1374z00_6653)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1353z00_438), BUNSPEC);
				{
					BgL_sfunzf2czf2_bglt BgL_auxz00_8889;

					{
						obj_t BgL_auxz00_8890;

						{	/* Cgen/cop.sch 706 */
							BgL_objectz00_bglt BgL_tmpz00_8891;

							BgL_tmpz00_8891 = ((BgL_objectz00_bglt) BgL_new1374z00_6653);
							BgL_auxz00_8890 = BGL_OBJECT_WIDENING(BgL_tmpz00_8891);
						}
						BgL_auxz00_8889 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_8890);
					}
					((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_8889))->BgL_labelz00) =
						((BgL_clabelz00_bglt) BgL_label1354z00_439), BUNSPEC);
				}
				{
					BgL_sfunzf2czf2_bglt BgL_auxz00_8896;

					{
						obj_t BgL_auxz00_8897;

						{	/* Cgen/cop.sch 706 */
							BgL_objectz00_bglt BgL_tmpz00_8898;

							BgL_tmpz00_8898 = ((BgL_objectz00_bglt) BgL_new1374z00_6653);
							BgL_auxz00_8897 = BGL_OBJECT_WIDENING(BgL_tmpz00_8898);
						}
						BgL_auxz00_8896 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_8897);
					}
					((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_8896))->
							BgL_integratedz00) =
						((bool_t) BgL_integrated1355z00_440), BUNSPEC);
				}
				return BgL_new1374z00_6653;
			}
		}

	}



/* &make-sfun/C */
	BgL_sfunz00_bglt BGl_z62makezd2sfunzf2Cz42zzcgen_copz00(obj_t BgL_envz00_5341,
		obj_t BgL_arity1332z00_5342, obj_t BgL_sidezd2effect1333zd2_5343,
		obj_t BgL_predicatezd2of1334zd2_5344,
		obj_t BgL_stackzd2allocator1335zd2_5345, obj_t BgL_topzf31336zf3_5346,
		obj_t BgL_thezd2closure1337zd2_5347, obj_t BgL_effect1338z00_5348,
		obj_t BgL_failsafe1339z00_5349, obj_t BgL_argszd2noescape1340zd2_5350,
		obj_t BgL_argszd2retescape1341zd2_5351, obj_t BgL_property1342z00_5352,
		obj_t BgL_args1343z00_5353, obj_t BgL_argszd2name1344zd2_5354,
		obj_t BgL_body1345z00_5355, obj_t BgL_class1346z00_5356,
		obj_t BgL_dssslzd2keywords1347zd2_5357, obj_t BgL_loc1348z00_5358,
		obj_t BgL_optionals1349z00_5359, obj_t BgL_keys1350z00_5360,
		obj_t BgL_thezd2closurezd2global1351z00_5361,
		obj_t BgL_strength1352z00_5362, obj_t BgL_stackable1353z00_5363,
		obj_t BgL_label1354z00_5364, obj_t BgL_integrated1355z00_5365)
	{
		{	/* Cgen/cop.sch 706 */
			return
				BGl_makezd2sfunzf2Cz20zzcgen_copz00(
				(long) CINT(BgL_arity1332z00_5342), BgL_sidezd2effect1333zd2_5343,
				BgL_predicatezd2of1334zd2_5344, BgL_stackzd2allocator1335zd2_5345,
				CBOOL(BgL_topzf31336zf3_5346), BgL_thezd2closure1337zd2_5347,
				BgL_effect1338z00_5348, BgL_failsafe1339z00_5349,
				BgL_argszd2noescape1340zd2_5350, BgL_argszd2retescape1341zd2_5351,
				BgL_property1342z00_5352, BgL_args1343z00_5353,
				BgL_argszd2name1344zd2_5354, BgL_body1345z00_5355,
				BgL_class1346z00_5356, BgL_dssslzd2keywords1347zd2_5357,
				BgL_loc1348z00_5358, BgL_optionals1349z00_5359, BgL_keys1350z00_5360,
				BgL_thezd2closurezd2global1351z00_5361, BgL_strength1352z00_5362,
				BgL_stackable1353z00_5363, ((BgL_clabelz00_bglt) BgL_label1354z00_5364),
				CBOOL(BgL_integrated1355z00_5365));
		}

	}



/* sfun/C? */
	BGL_EXPORTED_DEF bool_t BGl_sfunzf2Czf3z01zzcgen_copz00(obj_t BgL_objz00_441)
	{
		{	/* Cgen/cop.sch 707 */
			{	/* Cgen/cop.sch 707 */
				obj_t BgL_classz00_6659;

				BgL_classz00_6659 = BGl_sfunzf2Czf2zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_441))
					{	/* Cgen/cop.sch 707 */
						BgL_objectz00_bglt BgL_arg1807z00_6660;

						BgL_arg1807z00_6660 = (BgL_objectz00_bglt) (BgL_objz00_441);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 707 */
								long BgL_idxz00_6661;

								BgL_idxz00_6661 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6660);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6661 + 4L)) == BgL_classz00_6659);
							}
						else
							{	/* Cgen/cop.sch 707 */
								bool_t BgL_res2441z00_6664;

								{	/* Cgen/cop.sch 707 */
									obj_t BgL_oclassz00_6665;

									{	/* Cgen/cop.sch 707 */
										obj_t BgL_arg1815z00_6666;
										long BgL_arg1816z00_6667;

										BgL_arg1815z00_6666 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 707 */
											long BgL_arg1817z00_6668;

											BgL_arg1817z00_6668 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6660);
											BgL_arg1816z00_6667 = (BgL_arg1817z00_6668 - OBJECT_TYPE);
										}
										BgL_oclassz00_6665 =
											VECTOR_REF(BgL_arg1815z00_6666, BgL_arg1816z00_6667);
									}
									{	/* Cgen/cop.sch 707 */
										bool_t BgL__ortest_1115z00_6669;

										BgL__ortest_1115z00_6669 =
											(BgL_classz00_6659 == BgL_oclassz00_6665);
										if (BgL__ortest_1115z00_6669)
											{	/* Cgen/cop.sch 707 */
												BgL_res2441z00_6664 = BgL__ortest_1115z00_6669;
											}
										else
											{	/* Cgen/cop.sch 707 */
												long BgL_odepthz00_6670;

												{	/* Cgen/cop.sch 707 */
													obj_t BgL_arg1804z00_6671;

													BgL_arg1804z00_6671 = (BgL_oclassz00_6665);
													BgL_odepthz00_6670 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6671);
												}
												if ((4L < BgL_odepthz00_6670))
													{	/* Cgen/cop.sch 707 */
														obj_t BgL_arg1802z00_6672;

														{	/* Cgen/cop.sch 707 */
															obj_t BgL_arg1803z00_6673;

															BgL_arg1803z00_6673 = (BgL_oclassz00_6665);
															BgL_arg1802z00_6672 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6673,
																4L);
														}
														BgL_res2441z00_6664 =
															(BgL_arg1802z00_6672 == BgL_classz00_6659);
													}
												else
													{	/* Cgen/cop.sch 707 */
														BgL_res2441z00_6664 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2441z00_6664;
							}
					}
				else
					{	/* Cgen/cop.sch 707 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sfun/C? */
	obj_t BGl_z62sfunzf2Czf3z63zzcgen_copz00(obj_t BgL_envz00_5366,
		obj_t BgL_objz00_5367)
	{
		{	/* Cgen/cop.sch 707 */
			return BBOOL(BGl_sfunzf2Czf3z01zzcgen_copz00(BgL_objz00_5367));
		}

	}



/* sfun/C-nil */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt BGl_sfunzf2Czd2nilz20zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 708 */
			{	/* Cgen/cop.sch 708 */
				obj_t BgL_classz00_4448;

				BgL_classz00_4448 = BGl_sfunzf2Czf2zzcgen_copz00;
				{	/* Cgen/cop.sch 708 */
					obj_t BgL__ortest_1117z00_4449;

					BgL__ortest_1117z00_4449 = BGL_CLASS_NIL(BgL_classz00_4448);
					if (CBOOL(BgL__ortest_1117z00_4449))
						{	/* Cgen/cop.sch 708 */
							return ((BgL_sfunz00_bglt) BgL__ortest_1117z00_4449);
						}
					else
						{	/* Cgen/cop.sch 708 */
							return
								((BgL_sfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4448));
						}
				}
			}
		}

	}



/* &sfun/C-nil */
	BgL_sfunz00_bglt BGl_z62sfunzf2Czd2nilz42zzcgen_copz00(obj_t BgL_envz00_5368)
	{
		{	/* Cgen/cop.sch 708 */
			return BGl_sfunzf2Czd2nilz20zzcgen_copz00();
		}

	}



/* sfun/C-integrated */
	BGL_EXPORTED_DEF bool_t
		BGl_sfunzf2Czd2integratedz20zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_442)
	{
		{	/* Cgen/cop.sch 709 */
			{
				BgL_sfunzf2czf2_bglt BgL_auxz00_8939;

				{
					obj_t BgL_auxz00_8940;

					{	/* Cgen/cop.sch 709 */
						BgL_objectz00_bglt BgL_tmpz00_8941;

						BgL_tmpz00_8941 = ((BgL_objectz00_bglt) BgL_oz00_442);
						BgL_auxz00_8940 = BGL_OBJECT_WIDENING(BgL_tmpz00_8941);
					}
					BgL_auxz00_8939 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_8940);
				}
				return
					(((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_8939))->
					BgL_integratedz00);
			}
		}

	}



/* &sfun/C-integrated */
	obj_t BGl_z62sfunzf2Czd2integratedz42zzcgen_copz00(obj_t BgL_envz00_5369,
		obj_t BgL_oz00_5370)
	{
		{	/* Cgen/cop.sch 709 */
			return
				BBOOL(BGl_sfunzf2Czd2integratedz20zzcgen_copz00(
					((BgL_sfunz00_bglt) BgL_oz00_5370)));
		}

	}



/* sfun/C-integrated-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2integratedzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_443, bool_t BgL_vz00_444)
	{
		{	/* Cgen/cop.sch 710 */
			{
				BgL_sfunzf2czf2_bglt BgL_auxz00_8949;

				{
					obj_t BgL_auxz00_8950;

					{	/* Cgen/cop.sch 710 */
						BgL_objectz00_bglt BgL_tmpz00_8951;

						BgL_tmpz00_8951 = ((BgL_objectz00_bglt) BgL_oz00_443);
						BgL_auxz00_8950 = BGL_OBJECT_WIDENING(BgL_tmpz00_8951);
					}
					BgL_auxz00_8949 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_8950);
				}
				return
					((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_8949))->
						BgL_integratedz00) = ((bool_t) BgL_vz00_444), BUNSPEC);
			}
		}

	}



/* &sfun/C-integrated-set! */
	obj_t BGl_z62sfunzf2Czd2integratedzd2setz12z82zzcgen_copz00(obj_t
		BgL_envz00_5371, obj_t BgL_oz00_5372, obj_t BgL_vz00_5373)
	{
		{	/* Cgen/cop.sch 710 */
			return
				BGl_sfunzf2Czd2integratedzd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5372), CBOOL(BgL_vz00_5373));
		}

	}



/* sfun/C-label */
	BGL_EXPORTED_DEF BgL_clabelz00_bglt
		BGl_sfunzf2Czd2labelz20zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_445)
	{
		{	/* Cgen/cop.sch 711 */
			{
				BgL_sfunzf2czf2_bglt BgL_auxz00_8959;

				{
					obj_t BgL_auxz00_8960;

					{	/* Cgen/cop.sch 711 */
						BgL_objectz00_bglt BgL_tmpz00_8961;

						BgL_tmpz00_8961 = ((BgL_objectz00_bglt) BgL_oz00_445);
						BgL_auxz00_8960 = BGL_OBJECT_WIDENING(BgL_tmpz00_8961);
					}
					BgL_auxz00_8959 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_8960);
				}
				return
					(((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_8959))->BgL_labelz00);
			}
		}

	}



/* &sfun/C-label */
	BgL_clabelz00_bglt BGl_z62sfunzf2Czd2labelz42zzcgen_copz00(obj_t
		BgL_envz00_5374, obj_t BgL_oz00_5375)
	{
		{	/* Cgen/cop.sch 711 */
			return
				BGl_sfunzf2Czd2labelz20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5375));
		}

	}



/* sfun/C-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2stackablez20zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_448)
	{
		{	/* Cgen/cop.sch 713 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_448)))->BgL_stackablez00);
		}

	}



/* &sfun/C-stackable */
	obj_t BGl_z62sfunzf2Czd2stackablez42zzcgen_copz00(obj_t BgL_envz00_5376,
		obj_t BgL_oz00_5377)
	{
		{	/* Cgen/cop.sch 713 */
			return
				BGl_sfunzf2Czd2stackablez20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5377));
		}

	}



/* sfun/C-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2stackablezd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_449, obj_t BgL_vz00_450)
	{
		{	/* Cgen/cop.sch 714 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_449)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_450), BUNSPEC);
		}

	}



/* &sfun/C-stackable-set! */
	obj_t BGl_z62sfunzf2Czd2stackablezd2setz12z82zzcgen_copz00(obj_t
		BgL_envz00_5378, obj_t BgL_oz00_5379, obj_t BgL_vz00_5380)
	{
		{	/* Cgen/cop.sch 714 */
			return
				BGl_sfunzf2Czd2stackablezd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5379), BgL_vz00_5380);
		}

	}



/* sfun/C-strength */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2strengthz20zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_451)
	{
		{	/* Cgen/cop.sch 715 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_451)))->BgL_strengthz00);
		}

	}



/* &sfun/C-strength */
	obj_t BGl_z62sfunzf2Czd2strengthz42zzcgen_copz00(obj_t BgL_envz00_5381,
		obj_t BgL_oz00_5382)
	{
		{	/* Cgen/cop.sch 715 */
			return
				BGl_sfunzf2Czd2strengthz20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5382));
		}

	}



/* sfun/C-strength-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2strengthzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_452, obj_t BgL_vz00_453)
	{
		{	/* Cgen/cop.sch 716 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_452)))->BgL_strengthz00) =
				((obj_t) BgL_vz00_453), BUNSPEC);
		}

	}



/* &sfun/C-strength-set! */
	obj_t BGl_z62sfunzf2Czd2strengthzd2setz12z82zzcgen_copz00(obj_t
		BgL_envz00_5383, obj_t BgL_oz00_5384, obj_t BgL_vz00_5385)
	{
		{	/* Cgen/cop.sch 716 */
			return
				BGl_sfunzf2Czd2strengthzd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5384), BgL_vz00_5385);
		}

	}



/* sfun/C-the-closure-global */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2thezd2closurezd2globalz20zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_454)
	{
		{	/* Cgen/cop.sch 717 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_454)))->BgL_thezd2closurezd2globalz00);
		}

	}



/* &sfun/C-the-closure-global */
	obj_t BGl_z62sfunzf2Czd2thezd2closurezd2globalz42zzcgen_copz00(obj_t
		BgL_envz00_5386, obj_t BgL_oz00_5387)
	{
		{	/* Cgen/cop.sch 717 */
			return
				BGl_sfunzf2Czd2thezd2closurezd2globalz20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5387));
		}

	}



/* sfun/C-the-closure-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2thezd2closurezd2globalzd2setz12ze0zzcgen_copz00
		(BgL_sfunz00_bglt BgL_oz00_455, obj_t BgL_vz00_456)
	{
		{	/* Cgen/cop.sch 718 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_455)))->
					BgL_thezd2closurezd2globalz00) = ((obj_t) BgL_vz00_456), BUNSPEC);
		}

	}



/* &sfun/C-the-closure-global-set! */
	obj_t BGl_z62sfunzf2Czd2thezd2closurezd2globalzd2setz12z82zzcgen_copz00(obj_t
		BgL_envz00_5388, obj_t BgL_oz00_5389, obj_t BgL_vz00_5390)
	{
		{	/* Cgen/cop.sch 718 */
			return
				BGl_sfunzf2Czd2thezd2closurezd2globalzd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5389), BgL_vz00_5390);
		}

	}



/* sfun/C-keys */
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Czd2keysz20zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_457)
	{
		{	/* Cgen/cop.sch 719 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_457)))->BgL_keysz00);
		}

	}



/* &sfun/C-keys */
	obj_t BGl_z62sfunzf2Czd2keysz42zzcgen_copz00(obj_t BgL_envz00_5391,
		obj_t BgL_oz00_5392)
	{
		{	/* Cgen/cop.sch 719 */
			return
				BGl_sfunzf2Czd2keysz20zzcgen_copz00(((BgL_sfunz00_bglt) BgL_oz00_5392));
		}

	}



/* sfun/C-optionals */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2optionalsz20zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_460)
	{
		{	/* Cgen/cop.sch 721 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_460)))->BgL_optionalsz00);
		}

	}



/* &sfun/C-optionals */
	obj_t BGl_z62sfunzf2Czd2optionalsz42zzcgen_copz00(obj_t BgL_envz00_5393,
		obj_t BgL_oz00_5394)
	{
		{	/* Cgen/cop.sch 721 */
			return
				BGl_sfunzf2Czd2optionalsz20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5394));
		}

	}



/* sfun/C-loc */
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Czd2locz20zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_463)
	{
		{	/* Cgen/cop.sch 723 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_463)))->BgL_locz00);
		}

	}



/* &sfun/C-loc */
	obj_t BGl_z62sfunzf2Czd2locz42zzcgen_copz00(obj_t BgL_envz00_5395,
		obj_t BgL_oz00_5396)
	{
		{	/* Cgen/cop.sch 723 */
			return
				BGl_sfunzf2Czd2locz20zzcgen_copz00(((BgL_sfunz00_bglt) BgL_oz00_5396));
		}

	}



/* sfun/C-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2loczd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_464,
		obj_t BgL_vz00_465)
	{
		{	/* Cgen/cop.sch 724 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_464)))->BgL_locz00) =
				((obj_t) BgL_vz00_465), BUNSPEC);
		}

	}



/* &sfun/C-loc-set! */
	obj_t BGl_z62sfunzf2Czd2loczd2setz12z82zzcgen_copz00(obj_t BgL_envz00_5397,
		obj_t BgL_oz00_5398, obj_t BgL_vz00_5399)
	{
		{	/* Cgen/cop.sch 724 */
			return
				BGl_sfunzf2Czd2loczd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5398), BgL_vz00_5399);
		}

	}



/* sfun/C-dsssl-keywords */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2dssslzd2keywordszf2zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_466)
	{
		{	/* Cgen/cop.sch 725 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_466)))->BgL_dssslzd2keywordszd2);
		}

	}



/* &sfun/C-dsssl-keywords */
	obj_t BGl_z62sfunzf2Czd2dssslzd2keywordsz90zzcgen_copz00(obj_t
		BgL_envz00_5400, obj_t BgL_oz00_5401)
	{
		{	/* Cgen/cop.sch 725 */
			return
				BGl_sfunzf2Czd2dssslzd2keywordszf2zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5401));
		}

	}



/* sfun/C-dsssl-keywords-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2dssslzd2keywordszd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_467, obj_t BgL_vz00_468)
	{
		{	/* Cgen/cop.sch 726 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_467)))->BgL_dssslzd2keywordszd2) =
				((obj_t) BgL_vz00_468), BUNSPEC);
		}

	}



/* &sfun/C-dsssl-keywords-set! */
	obj_t BGl_z62sfunzf2Czd2dssslzd2keywordszd2setz12z50zzcgen_copz00(obj_t
		BgL_envz00_5402, obj_t BgL_oz00_5403, obj_t BgL_vz00_5404)
	{
		{	/* Cgen/cop.sch 726 */
			return
				BGl_sfunzf2Czd2dssslzd2keywordszd2setz12z32zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5403), BgL_vz00_5404);
		}

	}



/* sfun/C-class */
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Czd2classz20zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_469)
	{
		{	/* Cgen/cop.sch 727 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_469)))->BgL_classz00);
		}

	}



/* &sfun/C-class */
	obj_t BGl_z62sfunzf2Czd2classz42zzcgen_copz00(obj_t BgL_envz00_5405,
		obj_t BgL_oz00_5406)
	{
		{	/* Cgen/cop.sch 727 */
			return
				BGl_sfunzf2Czd2classz20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5406));
		}

	}



/* sfun/C-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2classzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_470,
		obj_t BgL_vz00_471)
	{
		{	/* Cgen/cop.sch 728 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_470)))->BgL_classz00) =
				((obj_t) BgL_vz00_471), BUNSPEC);
		}

	}



/* &sfun/C-class-set! */
	obj_t BGl_z62sfunzf2Czd2classzd2setz12z82zzcgen_copz00(obj_t BgL_envz00_5407,
		obj_t BgL_oz00_5408, obj_t BgL_vz00_5409)
	{
		{	/* Cgen/cop.sch 728 */
			return
				BGl_sfunzf2Czd2classzd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5408), BgL_vz00_5409);
		}

	}



/* sfun/C-body */
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Czd2bodyz20zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_472)
	{
		{	/* Cgen/cop.sch 729 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_472)))->BgL_bodyz00);
		}

	}



/* &sfun/C-body */
	obj_t BGl_z62sfunzf2Czd2bodyz42zzcgen_copz00(obj_t BgL_envz00_5410,
		obj_t BgL_oz00_5411)
	{
		{	/* Cgen/cop.sch 729 */
			return
				BGl_sfunzf2Czd2bodyz20zzcgen_copz00(((BgL_sfunz00_bglt) BgL_oz00_5411));
		}

	}



/* sfun/C-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2bodyzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_473,
		obj_t BgL_vz00_474)
	{
		{	/* Cgen/cop.sch 730 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_473)))->BgL_bodyz00) =
				((obj_t) BgL_vz00_474), BUNSPEC);
		}

	}



/* &sfun/C-body-set! */
	obj_t BGl_z62sfunzf2Czd2bodyzd2setz12z82zzcgen_copz00(obj_t BgL_envz00_5412,
		obj_t BgL_oz00_5413, obj_t BgL_vz00_5414)
	{
		{	/* Cgen/cop.sch 730 */
			return
				BGl_sfunzf2Czd2bodyzd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5413), BgL_vz00_5414);
		}

	}



/* sfun/C-args-name */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2argszd2namezf2zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_475)
	{
		{	/* Cgen/cop.sch 731 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_475)))->BgL_argszd2namezd2);
		}

	}



/* &sfun/C-args-name */
	obj_t BGl_z62sfunzf2Czd2argszd2namez90zzcgen_copz00(obj_t BgL_envz00_5415,
		obj_t BgL_oz00_5416)
	{
		{	/* Cgen/cop.sch 731 */
			return
				BGl_sfunzf2Czd2argszd2namezf2zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5416));
		}

	}



/* sfun/C-args */
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Czd2argsz20zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_478)
	{
		{	/* Cgen/cop.sch 733 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_478)))->BgL_argsz00);
		}

	}



/* &sfun/C-args */
	obj_t BGl_z62sfunzf2Czd2argsz42zzcgen_copz00(obj_t BgL_envz00_5417,
		obj_t BgL_oz00_5418)
	{
		{	/* Cgen/cop.sch 733 */
			return
				BGl_sfunzf2Czd2argsz20zzcgen_copz00(((BgL_sfunz00_bglt) BgL_oz00_5418));
		}

	}



/* sfun/C-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2argszd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_479,
		obj_t BgL_vz00_480)
	{
		{	/* Cgen/cop.sch 734 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_479)))->BgL_argsz00) =
				((obj_t) BgL_vz00_480), BUNSPEC);
		}

	}



/* &sfun/C-args-set! */
	obj_t BGl_z62sfunzf2Czd2argszd2setz12z82zzcgen_copz00(obj_t BgL_envz00_5419,
		obj_t BgL_oz00_5420, obj_t BgL_vz00_5421)
	{
		{	/* Cgen/cop.sch 734 */
			return
				BGl_sfunzf2Czd2argszd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5420), BgL_vz00_5421);
		}

	}



/* sfun/C-property */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2propertyz20zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_481)
	{
		{	/* Cgen/cop.sch 735 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_481)))->BgL_propertyz00);
		}

	}



/* &sfun/C-property */
	obj_t BGl_z62sfunzf2Czd2propertyz42zzcgen_copz00(obj_t BgL_envz00_5422,
		obj_t BgL_oz00_5423)
	{
		{	/* Cgen/cop.sch 735 */
			return
				BGl_sfunzf2Czd2propertyz20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5423));
		}

	}



/* sfun/C-property-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2propertyzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_482, obj_t BgL_vz00_483)
	{
		{	/* Cgen/cop.sch 736 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_482)))->BgL_propertyz00) =
				((obj_t) BgL_vz00_483), BUNSPEC);
		}

	}



/* &sfun/C-property-set! */
	obj_t BGl_z62sfunzf2Czd2propertyzd2setz12z82zzcgen_copz00(obj_t
		BgL_envz00_5424, obj_t BgL_oz00_5425, obj_t BgL_vz00_5426)
	{
		{	/* Cgen/cop.sch 736 */
			return
				BGl_sfunzf2Czd2propertyzd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5425), BgL_vz00_5426);
		}

	}



/* sfun/C-args-retescape */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2argszd2retescapezf2zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_484)
	{
		{	/* Cgen/cop.sch 737 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_484)))->BgL_argszd2retescapezd2);
		}

	}



/* &sfun/C-args-retescape */
	obj_t BGl_z62sfunzf2Czd2argszd2retescapez90zzcgen_copz00(obj_t
		BgL_envz00_5427, obj_t BgL_oz00_5428)
	{
		{	/* Cgen/cop.sch 737 */
			return
				BGl_sfunzf2Czd2argszd2retescapezf2zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5428));
		}

	}



/* sfun/C-args-retescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2argszd2retescapezd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_485, obj_t BgL_vz00_486)
	{
		{	/* Cgen/cop.sch 738 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_485)))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_486), BUNSPEC);
		}

	}



/* &sfun/C-args-retescape-set! */
	obj_t BGl_z62sfunzf2Czd2argszd2retescapezd2setz12z50zzcgen_copz00(obj_t
		BgL_envz00_5429, obj_t BgL_oz00_5430, obj_t BgL_vz00_5431)
	{
		{	/* Cgen/cop.sch 738 */
			return
				BGl_sfunzf2Czd2argszd2retescapezd2setz12z32zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5430), BgL_vz00_5431);
		}

	}



/* sfun/C-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2argszd2noescapezf2zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_487)
	{
		{	/* Cgen/cop.sch 739 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_487)))->BgL_argszd2noescapezd2);
		}

	}



/* &sfun/C-args-noescape */
	obj_t BGl_z62sfunzf2Czd2argszd2noescapez90zzcgen_copz00(obj_t BgL_envz00_5432,
		obj_t BgL_oz00_5433)
	{
		{	/* Cgen/cop.sch 739 */
			return
				BGl_sfunzf2Czd2argszd2noescapezf2zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5433));
		}

	}



/* sfun/C-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2argszd2noescapezd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_488, obj_t BgL_vz00_489)
	{
		{	/* Cgen/cop.sch 740 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_488)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_489), BUNSPEC);
		}

	}



/* &sfun/C-args-noescape-set! */
	obj_t BGl_z62sfunzf2Czd2argszd2noescapezd2setz12z50zzcgen_copz00(obj_t
		BgL_envz00_5434, obj_t BgL_oz00_5435, obj_t BgL_vz00_5436)
	{
		{	/* Cgen/cop.sch 740 */
			return
				BGl_sfunzf2Czd2argszd2noescapezd2setz12z32zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5435), BgL_vz00_5436);
		}

	}



/* sfun/C-failsafe */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2failsafez20zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_490)
	{
		{	/* Cgen/cop.sch 741 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_490)))->BgL_failsafez00);
		}

	}



/* &sfun/C-failsafe */
	obj_t BGl_z62sfunzf2Czd2failsafez42zzcgen_copz00(obj_t BgL_envz00_5437,
		obj_t BgL_oz00_5438)
	{
		{	/* Cgen/cop.sch 741 */
			return
				BGl_sfunzf2Czd2failsafez20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5438));
		}

	}



/* sfun/C-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2failsafezd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_491, obj_t BgL_vz00_492)
	{
		{	/* Cgen/cop.sch 742 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_491)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_492), BUNSPEC);
		}

	}



/* &sfun/C-failsafe-set! */
	obj_t BGl_z62sfunzf2Czd2failsafezd2setz12z82zzcgen_copz00(obj_t
		BgL_envz00_5439, obj_t BgL_oz00_5440, obj_t BgL_vz00_5441)
	{
		{	/* Cgen/cop.sch 742 */
			return
				BGl_sfunzf2Czd2failsafezd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5440), BgL_vz00_5441);
		}

	}



/* sfun/C-effect */
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Czd2effectz20zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_493)
	{
		{	/* Cgen/cop.sch 743 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_493)))->BgL_effectz00);
		}

	}



/* &sfun/C-effect */
	obj_t BGl_z62sfunzf2Czd2effectz42zzcgen_copz00(obj_t BgL_envz00_5442,
		obj_t BgL_oz00_5443)
	{
		{	/* Cgen/cop.sch 743 */
			return
				BGl_sfunzf2Czd2effectz20zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5443));
		}

	}



/* sfun/C-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2effectzd2setz12ze0zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_494, obj_t BgL_vz00_495)
	{
		{	/* Cgen/cop.sch 744 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_494)))->BgL_effectz00) =
				((obj_t) BgL_vz00_495), BUNSPEC);
		}

	}



/* &sfun/C-effect-set! */
	obj_t BGl_z62sfunzf2Czd2effectzd2setz12z82zzcgen_copz00(obj_t BgL_envz00_5444,
		obj_t BgL_oz00_5445, obj_t BgL_vz00_5446)
	{
		{	/* Cgen/cop.sch 744 */
			return
				BGl_sfunzf2Czd2effectzd2setz12ze0zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5445), BgL_vz00_5446);
		}

	}



/* sfun/C-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2thezd2closurezf2zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_496)
	{
		{	/* Cgen/cop.sch 745 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_496)))->BgL_thezd2closurezd2);
		}

	}



/* &sfun/C-the-closure */
	obj_t BGl_z62sfunzf2Czd2thezd2closurez90zzcgen_copz00(obj_t BgL_envz00_5447,
		obj_t BgL_oz00_5448)
	{
		{	/* Cgen/cop.sch 745 */
			return
				BGl_sfunzf2Czd2thezd2closurezf2zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5448));
		}

	}



/* sfun/C-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2thezd2closurezd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_497, obj_t BgL_vz00_498)
	{
		{	/* Cgen/cop.sch 746 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_497)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_498), BUNSPEC);
		}

	}



/* &sfun/C-the-closure-set! */
	obj_t BGl_z62sfunzf2Czd2thezd2closurezd2setz12z50zzcgen_copz00(obj_t
		BgL_envz00_5449, obj_t BgL_oz00_5450, obj_t BgL_vz00_5451)
	{
		{	/* Cgen/cop.sch 746 */
			return
				BGl_sfunzf2Czd2thezd2closurezd2setz12z32zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5450), BgL_vz00_5451);
		}

	}



/* sfun/C-top? */
	BGL_EXPORTED_DEF bool_t BGl_sfunzf2Czd2topzf3zd3zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_499)
	{
		{	/* Cgen/cop.sch 747 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_499)))->BgL_topzf3zf3);
		}

	}



/* &sfun/C-top? */
	obj_t BGl_z62sfunzf2Czd2topzf3zb1zzcgen_copz00(obj_t BgL_envz00_5452,
		obj_t BgL_oz00_5453)
	{
		{	/* Cgen/cop.sch 747 */
			return
				BBOOL(BGl_sfunzf2Czd2topzf3zd3zzcgen_copz00(
					((BgL_sfunz00_bglt) BgL_oz00_5453)));
		}

	}



/* sfun/C-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2topzf3zd2setz12z13zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_500, bool_t BgL_vz00_501)
	{
		{	/* Cgen/cop.sch 748 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_500)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_501), BUNSPEC);
		}

	}



/* &sfun/C-top?-set! */
	obj_t BGl_z62sfunzf2Czd2topzf3zd2setz12z71zzcgen_copz00(obj_t BgL_envz00_5454,
		obj_t BgL_oz00_5455, obj_t BgL_vz00_5456)
	{
		{	/* Cgen/cop.sch 748 */
			return
				BGl_sfunzf2Czd2topzf3zd2setz12z13zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5455), CBOOL(BgL_vz00_5456));
		}

	}



/* sfun/C-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2stackzd2allocatorzf2zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_502)
	{
		{	/* Cgen/cop.sch 749 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_502)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &sfun/C-stack-allocator */
	obj_t BGl_z62sfunzf2Czd2stackzd2allocatorz90zzcgen_copz00(obj_t
		BgL_envz00_5457, obj_t BgL_oz00_5458)
	{
		{	/* Cgen/cop.sch 749 */
			return
				BGl_sfunzf2Czd2stackzd2allocatorzf2zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5458));
		}

	}



/* sfun/C-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2stackzd2allocatorzd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_503, obj_t BgL_vz00_504)
	{
		{	/* Cgen/cop.sch 750 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_503)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_504), BUNSPEC);
		}

	}



/* &sfun/C-stack-allocator-set! */
	obj_t BGl_z62sfunzf2Czd2stackzd2allocatorzd2setz12z50zzcgen_copz00(obj_t
		BgL_envz00_5459, obj_t BgL_oz00_5460, obj_t BgL_vz00_5461)
	{
		{	/* Cgen/cop.sch 750 */
			return
				BGl_sfunzf2Czd2stackzd2allocatorzd2setz12z32zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5460), BgL_vz00_5461);
		}

	}



/* sfun/C-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2predicatezd2ofzf2zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_505)
	{
		{	/* Cgen/cop.sch 751 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_505)))->BgL_predicatezd2ofzd2);
		}

	}



/* &sfun/C-predicate-of */
	obj_t BGl_z62sfunzf2Czd2predicatezd2ofz90zzcgen_copz00(obj_t BgL_envz00_5462,
		obj_t BgL_oz00_5463)
	{
		{	/* Cgen/cop.sch 751 */
			return
				BGl_sfunzf2Czd2predicatezd2ofzf2zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5463));
		}

	}



/* sfun/C-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2predicatezd2ofzd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_506, obj_t BgL_vz00_507)
	{
		{	/* Cgen/cop.sch 752 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_506)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_507), BUNSPEC);
		}

	}



/* &sfun/C-predicate-of-set! */
	obj_t BGl_z62sfunzf2Czd2predicatezd2ofzd2setz12z50zzcgen_copz00(obj_t
		BgL_envz00_5464, obj_t BgL_oz00_5465, obj_t BgL_vz00_5466)
	{
		{	/* Cgen/cop.sch 752 */
			return
				BGl_sfunzf2Czd2predicatezd2ofzd2setz12z32zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5465), BgL_vz00_5466);
		}

	}



/* sfun/C-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2sidezd2effectzf2zzcgen_copz00(BgL_sfunz00_bglt BgL_oz00_508)
	{
		{	/* Cgen/cop.sch 753 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_508)))->BgL_sidezd2effectzd2);
		}

	}



/* &sfun/C-side-effect */
	obj_t BGl_z62sfunzf2Czd2sidezd2effectz90zzcgen_copz00(obj_t BgL_envz00_5467,
		obj_t BgL_oz00_5468)
	{
		{	/* Cgen/cop.sch 753 */
			return
				BGl_sfunzf2Czd2sidezd2effectzf2zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5468));
		}

	}



/* sfun/C-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Czd2sidezd2effectzd2setz12z32zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_509, obj_t BgL_vz00_510)
	{
		{	/* Cgen/cop.sch 754 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_509)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_510), BUNSPEC);
		}

	}



/* &sfun/C-side-effect-set! */
	obj_t BGl_z62sfunzf2Czd2sidezd2effectzd2setz12z50zzcgen_copz00(obj_t
		BgL_envz00_5469, obj_t BgL_oz00_5470, obj_t BgL_vz00_5471)
	{
		{	/* Cgen/cop.sch 754 */
			return
				BGl_sfunzf2Czd2sidezd2effectzd2setz12z32zzcgen_copz00(
				((BgL_sfunz00_bglt) BgL_oz00_5470), BgL_vz00_5471);
		}

	}



/* sfun/C-arity */
	BGL_EXPORTED_DEF long BGl_sfunzf2Czd2arityz20zzcgen_copz00(BgL_sfunz00_bglt
		BgL_oz00_511)
	{
		{	/* Cgen/cop.sch 755 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_511)))->BgL_arityz00);
		}

	}



/* &sfun/C-arity */
	obj_t BGl_z62sfunzf2Czd2arityz42zzcgen_copz00(obj_t BgL_envz00_5472,
		obj_t BgL_oz00_5473)
	{
		{	/* Cgen/cop.sch 755 */
			return
				BINT(BGl_sfunzf2Czd2arityz20zzcgen_copz00(
					((BgL_sfunz00_bglt) BgL_oz00_5473)));
		}

	}



/* make-bdb-block */
	BGL_EXPORTED_DEF BgL_bdbzd2blockzd2_bglt
		BGl_makezd2bdbzd2blockz00zzcgen_copz00(obj_t BgL_loc1328z00_514,
		BgL_typez00_bglt BgL_type1329z00_515, BgL_copz00_bglt BgL_body1330z00_516)
	{
		{	/* Cgen/cop.sch 759 */
			{	/* Cgen/cop.sch 759 */
				BgL_bdbzd2blockzd2_bglt BgL_new1376z00_6674;

				{	/* Cgen/cop.sch 759 */
					BgL_bdbzd2blockzd2_bglt BgL_new1375z00_6675;

					BgL_new1375z00_6675 =
						((BgL_bdbzd2blockzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bdbzd2blockzd2_bgl))));
					{	/* Cgen/cop.sch 759 */
						long BgL_arg1703z00_6676;

						BgL_arg1703z00_6676 =
							BGL_CLASS_NUM(BGl_bdbzd2blockzd2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1375z00_6675), BgL_arg1703z00_6676);
					}
					BgL_new1376z00_6674 = BgL_new1375z00_6675;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1376z00_6674)))->BgL_locz00) =
					((obj_t) BgL_loc1328z00_514), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1376z00_6674)))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1329z00_515), BUNSPEC);
				((((BgL_bdbzd2blockzd2_bglt) COBJECT(BgL_new1376z00_6674))->
						BgL_bodyz00) = ((BgL_copz00_bglt) BgL_body1330z00_516), BUNSPEC);
				return BgL_new1376z00_6674;
			}
		}

	}



/* &make-bdb-block */
	BgL_bdbzd2blockzd2_bglt BGl_z62makezd2bdbzd2blockz62zzcgen_copz00(obj_t
		BgL_envz00_5474, obj_t BgL_loc1328z00_5475, obj_t BgL_type1329z00_5476,
		obj_t BgL_body1330z00_5477)
	{
		{	/* Cgen/cop.sch 759 */
			return
				BGl_makezd2bdbzd2blockz00zzcgen_copz00(BgL_loc1328z00_5475,
				((BgL_typez00_bglt) BgL_type1329z00_5476),
				((BgL_copz00_bglt) BgL_body1330z00_5477));
		}

	}



/* bdb-block? */
	BGL_EXPORTED_DEF bool_t BGl_bdbzd2blockzf3z21zzcgen_copz00(obj_t
		BgL_objz00_517)
	{
		{	/* Cgen/cop.sch 760 */
			{	/* Cgen/cop.sch 760 */
				obj_t BgL_classz00_6677;

				BgL_classz00_6677 = BGl_bdbzd2blockzd2zzcgen_copz00;
				if (BGL_OBJECTP(BgL_objz00_517))
					{	/* Cgen/cop.sch 760 */
						BgL_objectz00_bglt BgL_arg1807z00_6678;

						BgL_arg1807z00_6678 = (BgL_objectz00_bglt) (BgL_objz00_517);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cop.sch 760 */
								long BgL_idxz00_6679;

								BgL_idxz00_6679 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6678);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6679 + 2L)) == BgL_classz00_6677);
							}
						else
							{	/* Cgen/cop.sch 760 */
								bool_t BgL_res2442z00_6682;

								{	/* Cgen/cop.sch 760 */
									obj_t BgL_oclassz00_6683;

									{	/* Cgen/cop.sch 760 */
										obj_t BgL_arg1815z00_6684;
										long BgL_arg1816z00_6685;

										BgL_arg1815z00_6684 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cop.sch 760 */
											long BgL_arg1817z00_6686;

											BgL_arg1817z00_6686 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6678);
											BgL_arg1816z00_6685 = (BgL_arg1817z00_6686 - OBJECT_TYPE);
										}
										BgL_oclassz00_6683 =
											VECTOR_REF(BgL_arg1815z00_6684, BgL_arg1816z00_6685);
									}
									{	/* Cgen/cop.sch 760 */
										bool_t BgL__ortest_1115z00_6687;

										BgL__ortest_1115z00_6687 =
											(BgL_classz00_6677 == BgL_oclassz00_6683);
										if (BgL__ortest_1115z00_6687)
											{	/* Cgen/cop.sch 760 */
												BgL_res2442z00_6682 = BgL__ortest_1115z00_6687;
											}
										else
											{	/* Cgen/cop.sch 760 */
												long BgL_odepthz00_6688;

												{	/* Cgen/cop.sch 760 */
													obj_t BgL_arg1804z00_6689;

													BgL_arg1804z00_6689 = (BgL_oclassz00_6683);
													BgL_odepthz00_6688 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6689);
												}
												if ((2L < BgL_odepthz00_6688))
													{	/* Cgen/cop.sch 760 */
														obj_t BgL_arg1802z00_6690;

														{	/* Cgen/cop.sch 760 */
															obj_t BgL_arg1803z00_6691;

															BgL_arg1803z00_6691 = (BgL_oclassz00_6683);
															BgL_arg1802z00_6690 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6691,
																2L);
														}
														BgL_res2442z00_6682 =
															(BgL_arg1802z00_6690 == BgL_classz00_6677);
													}
												else
													{	/* Cgen/cop.sch 760 */
														BgL_res2442z00_6682 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2442z00_6682;
							}
					}
				else
					{	/* Cgen/cop.sch 760 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bdb-block? */
	obj_t BGl_z62bdbzd2blockzf3z43zzcgen_copz00(obj_t BgL_envz00_5478,
		obj_t BgL_objz00_5479)
	{
		{	/* Cgen/cop.sch 760 */
			return BBOOL(BGl_bdbzd2blockzf3z21zzcgen_copz00(BgL_objz00_5479));
		}

	}



/* bdb-block-nil */
	BGL_EXPORTED_DEF BgL_bdbzd2blockzd2_bglt
		BGl_bdbzd2blockzd2nilz00zzcgen_copz00(void)
	{
		{	/* Cgen/cop.sch 761 */
			{	/* Cgen/cop.sch 761 */
				obj_t BgL_classz00_4493;

				BgL_classz00_4493 = BGl_bdbzd2blockzd2zzcgen_copz00;
				{	/* Cgen/cop.sch 761 */
					obj_t BgL__ortest_1117z00_4494;

					BgL__ortest_1117z00_4494 = BGL_CLASS_NIL(BgL_classz00_4493);
					if (CBOOL(BgL__ortest_1117z00_4494))
						{	/* Cgen/cop.sch 761 */
							return ((BgL_bdbzd2blockzd2_bglt) BgL__ortest_1117z00_4494);
						}
					else
						{	/* Cgen/cop.sch 761 */
							return
								((BgL_bdbzd2blockzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4493));
						}
				}
			}
		}

	}



/* &bdb-block-nil */
	BgL_bdbzd2blockzd2_bglt BGl_z62bdbzd2blockzd2nilz62zzcgen_copz00(obj_t
		BgL_envz00_5480)
	{
		{	/* Cgen/cop.sch 761 */
			return BGl_bdbzd2blockzd2nilz00zzcgen_copz00();
		}

	}



/* bdb-block-body */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_bdbzd2blockzd2bodyz00zzcgen_copz00(BgL_bdbzd2blockzd2_bglt BgL_oz00_518)
	{
		{	/* Cgen/cop.sch 762 */
			return (((BgL_bdbzd2blockzd2_bglt) COBJECT(BgL_oz00_518))->BgL_bodyz00);
		}

	}



/* &bdb-block-body */
	BgL_copz00_bglt BGl_z62bdbzd2blockzd2bodyz62zzcgen_copz00(obj_t
		BgL_envz00_5481, obj_t BgL_oz00_5482)
	{
		{	/* Cgen/cop.sch 762 */
			return
				BGl_bdbzd2blockzd2bodyz00zzcgen_copz00(
				((BgL_bdbzd2blockzd2_bglt) BgL_oz00_5482));
		}

	}



/* bdb-block-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_bdbzd2blockzd2typez00zzcgen_copz00(BgL_bdbzd2blockzd2_bglt BgL_oz00_521)
	{
		{	/* Cgen/cop.sch 764 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_521)))->BgL_typez00);
		}

	}



/* &bdb-block-type */
	BgL_typez00_bglt BGl_z62bdbzd2blockzd2typez62zzcgen_copz00(obj_t
		BgL_envz00_5483, obj_t BgL_oz00_5484)
	{
		{	/* Cgen/cop.sch 764 */
			return
				BGl_bdbzd2blockzd2typez00zzcgen_copz00(
				((BgL_bdbzd2blockzd2_bglt) BgL_oz00_5484));
		}

	}



/* bdb-block-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_bdbzd2blockzd2locz00zzcgen_copz00(BgL_bdbzd2blockzd2_bglt BgL_oz00_524)
	{
		{	/* Cgen/cop.sch 766 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_524)))->BgL_locz00);
		}

	}



/* &bdb-block-loc */
	obj_t BGl_z62bdbzd2blockzd2locz62zzcgen_copz00(obj_t BgL_envz00_5485,
		obj_t BgL_oz00_5486)
	{
		{	/* Cgen/cop.sch 766 */
			return
				BGl_bdbzd2blockzd2locz00zzcgen_copz00(
				((BgL_bdbzd2blockzd2_bglt) BgL_oz00_5486));
		}

	}



/* bdb-block-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bdbzd2blockzd2loczd2setz12zc0zzcgen_copz00(BgL_bdbzd2blockzd2_bglt
		BgL_oz00_525, obj_t BgL_vz00_526)
	{
		{	/* Cgen/cop.sch 767 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_525)))->BgL_locz00) =
				((obj_t) BgL_vz00_526), BUNSPEC);
		}

	}



/* &bdb-block-loc-set! */
	obj_t BGl_z62bdbzd2blockzd2loczd2setz12za2zzcgen_copz00(obj_t BgL_envz00_5487,
		obj_t BgL_oz00_5488, obj_t BgL_vz00_5489)
	{
		{	/* Cgen/cop.sch 767 */
			return
				BGl_bdbzd2blockzd2loczd2setz12zc0zzcgen_copz00(
				((BgL_bdbzd2blockzd2_bglt) BgL_oz00_5488), BgL_vz00_5489);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.scm 15 */
			{	/* Cgen/cop.scm 23 */
				obj_t BgL_arg1710z00_1966;
				obj_t BgL_arg1711z00_1967;

				{	/* Cgen/cop.scm 23 */
					obj_t BgL_v1545z00_1979;

					BgL_v1545z00_1979 = create_vector(2L);
					{	/* Cgen/cop.scm 23 */
						obj_t BgL_arg1720z00_1980;

						BgL_arg1720z00_1980 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2445z00zzcgen_copz00, BGl_proc2444z00zzcgen_copz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2443z00zzcgen_copz00,
							CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1545z00_1979, 0L, BgL_arg1720z00_1980);
					}
					{	/* Cgen/cop.scm 23 */
						obj_t BgL_arg1737z00_1993;

						BgL_arg1737z00_1993 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc2447z00zzcgen_copz00, BGl_proc2446z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1545z00_1979, 1L, BgL_arg1737z00_1993);
					}
					BgL_arg1710z00_1966 = BgL_v1545z00_1979;
				}
				{	/* Cgen/cop.scm 23 */
					obj_t BgL_v1546z00_2003;

					BgL_v1546z00_2003 = create_vector(0L);
					BgL_arg1711z00_1967 = BgL_v1546z00_2003;
				}
				BGl_copz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(3),
					CNST_TABLE_REF(4), BGl_objectz00zz__objectz00, 21847L,
					BGl_proc2450z00zzcgen_copz00, BGl_proc2449z00zzcgen_copz00, BFALSE,
					BGl_proc2448z00zzcgen_copz00, BFALSE, BgL_arg1710z00_1966,
					BgL_arg1711z00_1967);
			}
			{	/* Cgen/cop.scm 28 */
				obj_t BgL_arg1749z00_2010;
				obj_t BgL_arg1750z00_2011;

				{	/* Cgen/cop.scm 28 */
					obj_t BgL_v1547z00_2026;

					BgL_v1547z00_2026 = create_vector(3L);
					{	/* Cgen/cop.scm 28 */
						obj_t BgL_arg1761z00_2027;

						BgL_arg1761z00_2027 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc2452z00zzcgen_copz00, BGl_proc2451z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1547z00_2026, 0L, BgL_arg1761z00_2027);
					}
					{	/* Cgen/cop.scm 28 */
						obj_t BgL_arg1770z00_2037;

						BgL_arg1770z00_2037 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc2455z00zzcgen_copz00, BGl_proc2454z00zzcgen_copz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2453z00zzcgen_copz00,
							CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1547z00_2026, 1L, BgL_arg1770z00_2037);
					}
					{	/* Cgen/cop.scm 28 */
						obj_t BgL_arg1798z00_2050;

						BgL_arg1798z00_2050 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2458z00zzcgen_copz00, BGl_proc2457z00zzcgen_copz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc2456z00zzcgen_copz00,
							CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1547z00_2026, 2L, BgL_arg1798z00_2050);
					}
					BgL_arg1749z00_2010 = BgL_v1547z00_2026;
				}
				{	/* Cgen/cop.scm 28 */
					obj_t BgL_v1548z00_2063;

					BgL_v1548z00_2063 = create_vector(0L);
					BgL_arg1750z00_2011 = BgL_v1548z00_2063;
				}
				BGl_clabelz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(10),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 41953L,
					BGl_proc2461z00zzcgen_copz00, BGl_proc2460z00zzcgen_copz00, BFALSE,
					BGl_proc2459z00zzcgen_copz00, BFALSE, BgL_arg1749z00_2010,
					BgL_arg1750z00_2011);
			}
			{	/* Cgen/cop.scm 33 */
				obj_t BgL_arg1823z00_2070;
				obj_t BgL_arg1831z00_2071;

				{	/* Cgen/cop.scm 33 */
					obj_t BgL_v1549z00_2084;

					BgL_v1549z00_2084 = create_vector(1L);
					{	/* Cgen/cop.scm 33 */
						obj_t BgL_arg1837z00_2085;

						BgL_arg1837z00_2085 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc2463z00zzcgen_copz00, BGl_proc2462z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_clabelz00zzcgen_copz00);
						VECTOR_SET(BgL_v1549z00_2084, 0L, BgL_arg1837z00_2085);
					}
					BgL_arg1823z00_2070 = BgL_v1549z00_2084;
				}
				{	/* Cgen/cop.scm 33 */
					obj_t BgL_v1550z00_2095;

					BgL_v1550z00_2095 = create_vector(0L);
					BgL_arg1831z00_2071 = BgL_v1550z00_2095;
				}
				BGl_cgotoz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(12),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 40559L,
					BGl_proc2466z00zzcgen_copz00, BGl_proc2465z00zzcgen_copz00, BFALSE,
					BGl_proc2464z00zzcgen_copz00, BFALSE, BgL_arg1823z00_2070,
					BgL_arg1831z00_2071);
			}
			{	/* Cgen/cop.scm 36 */
				obj_t BgL_arg1845z00_2102;
				obj_t BgL_arg1846z00_2103;

				{	/* Cgen/cop.scm 36 */
					obj_t BgL_v1551z00_2116;

					BgL_v1551z00_2116 = create_vector(1L);
					{	/* Cgen/cop.scm 36 */
						obj_t BgL_arg1852z00_2117;

						BgL_arg1852z00_2117 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2468z00zzcgen_copz00, BGl_proc2467z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1551z00_2116, 0L, BgL_arg1852z00_2117);
					}
					BgL_arg1845z00_2102 = BgL_v1551z00_2116;
				}
				{	/* Cgen/cop.scm 36 */
					obj_t BgL_v1552z00_2127;

					BgL_v1552z00_2127 = create_vector(0L);
					BgL_arg1846z00_2103 = BgL_v1552z00_2127;
				}
				BGl_cblockz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(13),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 31658L,
					BGl_proc2471z00zzcgen_copz00, BGl_proc2470z00zzcgen_copz00, BFALSE,
					BGl_proc2469z00zzcgen_copz00, BFALSE, BgL_arg1845z00_2102,
					BgL_arg1846z00_2103);
			}
			{	/* Cgen/cop.scm 39 */
				obj_t BgL_arg1860z00_2134;
				obj_t BgL_arg1862z00_2135;

				{	/* Cgen/cop.scm 39 */
					obj_t BgL_v1553z00_2149;

					BgL_v1553z00_2149 = create_vector(2L);
					{	/* Cgen/cop.scm 39 */
						obj_t BgL_arg1868z00_2150;

						BgL_arg1868z00_2150 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc2474z00zzcgen_copz00, BGl_proc2473z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BGl_proc2472z00zzcgen_copz00,
							CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1553z00_2149, 0L, BgL_arg1868z00_2150);
					}
					{	/* Cgen/cop.scm 39 */
						obj_t BgL_arg1876z00_2163;

						BgL_arg1876z00_2163 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2476z00zzcgen_copz00, BGl_proc2475z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1553z00_2149, 1L, BgL_arg1876z00_2163);
					}
					BgL_arg1860z00_2134 = BgL_v1553z00_2149;
				}
				{	/* Cgen/cop.scm 39 */
					obj_t BgL_v1554z00_2173;

					BgL_v1554z00_2173 = create_vector(0L);
					BgL_arg1862z00_2135 = BgL_v1554z00_2173;
				}
				BGl_creturnz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(16),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 5403L,
					BGl_proc2479z00zzcgen_copz00, BGl_proc2478z00zzcgen_copz00, BFALSE,
					BGl_proc2477z00zzcgen_copz00, BFALSE, BgL_arg1860z00_2134,
					BgL_arg1862z00_2135);
			}
			{	/* Cgen/cop.scm 43 */
				obj_t BgL_arg1885z00_2180;
				obj_t BgL_arg1887z00_2181;

				{	/* Cgen/cop.scm 43 */
					obj_t BgL_v1555z00_2194;

					BgL_v1555z00_2194 = create_vector(1L);
					{	/* Cgen/cop.scm 43 */
						obj_t BgL_arg1893z00_2195;

						BgL_arg1893z00_2195 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2481z00zzcgen_copz00, BGl_proc2480z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1555z00_2194, 0L, BgL_arg1893z00_2195);
					}
					BgL_arg1885z00_2180 = BgL_v1555z00_2194;
				}
				{	/* Cgen/cop.scm 43 */
					obj_t BgL_v1556z00_2205;

					BgL_v1556z00_2205 = create_vector(0L);
					BgL_arg1887z00_2181 = BgL_v1556z00_2205;
				}
				BGl_cvoidz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(17),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 36994L,
					BGl_proc2484z00zzcgen_copz00, BGl_proc2483z00zzcgen_copz00, BFALSE,
					BGl_proc2482z00zzcgen_copz00, BFALSE, BgL_arg1885z00_2180,
					BgL_arg1887z00_2181);
			}
			{	/* Cgen/cop.scm 46 */
				obj_t BgL_arg1903z00_2212;
				obj_t BgL_arg1904z00_2213;

				{	/* Cgen/cop.scm 46 */
					obj_t BgL_v1557z00_2226;

					BgL_v1557z00_2226 = create_vector(1L);
					{	/* Cgen/cop.scm 46 */
						obj_t BgL_arg1912z00_2227;

						BgL_arg1912z00_2227 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2486z00zzcgen_copz00, BGl_proc2485z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1557z00_2226, 0L, BgL_arg1912z00_2227);
					}
					BgL_arg1903z00_2212 = BgL_v1557z00_2226;
				}
				{	/* Cgen/cop.scm 46 */
					obj_t BgL_v1558z00_2237;

					BgL_v1558z00_2237 = create_vector(0L);
					BgL_arg1904z00_2213 = BgL_v1558z00_2237;
				}
				BGl_catomz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(18),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 15833L,
					BGl_proc2489z00zzcgen_copz00, BGl_proc2488z00zzcgen_copz00, BFALSE,
					BGl_proc2487z00zzcgen_copz00, BFALSE, BgL_arg1903z00_2212,
					BgL_arg1904z00_2213);
			}
			{	/* Cgen/cop.scm 49 */
				obj_t BgL_arg1920z00_2244;
				obj_t BgL_arg1923z00_2245;

				{	/* Cgen/cop.scm 49 */
					obj_t BgL_v1559z00_2258;

					BgL_v1559z00_2258 = create_vector(1L);
					{	/* Cgen/cop.scm 49 */
						obj_t BgL_arg1929z00_2259;

						BgL_arg1929z00_2259 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(19),
							BGl_proc2491z00zzcgen_copz00, BGl_proc2490z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_variablez00zzast_varz00);
						VECTOR_SET(BgL_v1559z00_2258, 0L, BgL_arg1929z00_2259);
					}
					BgL_arg1920z00_2244 = BgL_v1559z00_2258;
				}
				{	/* Cgen/cop.scm 49 */
					obj_t BgL_v1560z00_2269;

					BgL_v1560z00_2269 = create_vector(0L);
					BgL_arg1923z00_2245 = BgL_v1560z00_2269;
				}
				BGl_varcz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(20),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 5316L,
					BGl_proc2494z00zzcgen_copz00, BGl_proc2493z00zzcgen_copz00, BFALSE,
					BGl_proc2492z00zzcgen_copz00, BFALSE, BgL_arg1920z00_2244,
					BgL_arg1923z00_2245);
			}
			{	/* Cgen/cop.scm 52 */
				obj_t BgL_arg1937z00_2276;
				obj_t BgL_arg1938z00_2277;

				{	/* Cgen/cop.scm 52 */
					obj_t BgL_v1561z00_2291;

					BgL_v1561z00_2291 = create_vector(2L);
					{	/* Cgen/cop.scm 52 */
						obj_t BgL_arg1944z00_2292;

						BgL_arg1944z00_2292 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2496z00zzcgen_copz00, BGl_proc2495z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(6));
						VECTOR_SET(BgL_v1561z00_2291, 0L, BgL_arg1944z00_2292);
					}
					{	/* Cgen/cop.scm 52 */
						obj_t BgL_arg1949z00_2302;

						BgL_arg1949z00_2302 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2498z00zzcgen_copz00, BGl_proc2497z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1561z00_2291, 1L, BgL_arg1949z00_2302);
					}
					BgL_arg1937z00_2276 = BgL_v1561z00_2291;
				}
				{	/* Cgen/cop.scm 52 */
					obj_t BgL_v1562z00_2312;

					BgL_v1562z00_2312 = create_vector(0L);
					BgL_arg1938z00_2277 = BgL_v1562z00_2312;
				}
				BGl_cpragmaz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(23),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 59501L,
					BGl_proc2501z00zzcgen_copz00, BGl_proc2500z00zzcgen_copz00, BFALSE,
					BGl_proc2499z00zzcgen_copz00, BFALSE, BgL_arg1937z00_2276,
					BgL_arg1938z00_2277);
			}
			{	/* Cgen/cop.scm 56 */
				obj_t BgL_arg1957z00_2319;
				obj_t BgL_arg1958z00_2320;

				{	/* Cgen/cop.scm 56 */
					obj_t BgL_v1563z00_2333;

					BgL_v1563z00_2333 = create_vector(1L);
					{	/* Cgen/cop.scm 56 */
						obj_t BgL_arg1964z00_2334;

						BgL_arg1964z00_2334 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2503z00zzcgen_copz00, BGl_proc2502z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1563z00_2333, 0L, BgL_arg1964z00_2334);
					}
					BgL_arg1957z00_2319 = BgL_v1563z00_2333;
				}
				{	/* Cgen/cop.scm 56 */
					obj_t BgL_v1564z00_2344;

					BgL_v1564z00_2344 = create_vector(0L);
					BgL_arg1958z00_2320 = BgL_v1564z00_2344;
				}
				BGl_ccastz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(25),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 58454L,
					BGl_proc2506z00zzcgen_copz00, BGl_proc2505z00zzcgen_copz00, BFALSE,
					BGl_proc2504z00zzcgen_copz00, BFALSE, BgL_arg1957z00_2319,
					BgL_arg1958z00_2320);
			}
			{	/* Cgen/cop.scm 59 */
				obj_t BgL_arg1972z00_2351;
				obj_t BgL_arg1973z00_2352;

				{	/* Cgen/cop.scm 59 */
					obj_t BgL_v1565z00_2366;

					BgL_v1565z00_2366 = create_vector(2L);
					{	/* Cgen/cop.scm 59 */
						obj_t BgL_arg1979z00_2367;

						BgL_arg1979z00_2367 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc2509z00zzcgen_copz00, BGl_proc2508z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BGl_proc2507z00zzcgen_copz00,
							CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1565z00_2366, 0L, BgL_arg1979z00_2367);
					}
					{	/* Cgen/cop.scm 59 */
						obj_t BgL_arg1986z00_2380;

						BgL_arg1986z00_2380 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2511z00zzcgen_copz00, BGl_proc2510z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1565z00_2366, 1L, BgL_arg1986z00_2380);
					}
					BgL_arg1972z00_2351 = BgL_v1565z00_2366;
				}
				{	/* Cgen/cop.scm 59 */
					obj_t BgL_v1566z00_2390;

					BgL_v1566z00_2390 = create_vector(0L);
					BgL_arg1973z00_2352 = BgL_v1566z00_2390;
				}
				BGl_csequencez00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(28),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 24844L,
					BGl_proc2514z00zzcgen_copz00, BGl_proc2513z00zzcgen_copz00, BFALSE,
					BGl_proc2512z00zzcgen_copz00, BFALSE, BgL_arg1972z00_2351,
					BgL_arg1973z00_2352);
			}
			{	/* Cgen/cop.scm 63 */
				obj_t BgL_arg1994z00_2397;
				obj_t BgL_arg1995z00_2398;

				{	/* Cgen/cop.scm 63 */
					obj_t BgL_v1567z00_2410;

					BgL_v1567z00_2410 = create_vector(0L);
					BgL_arg1994z00_2397 = BgL_v1567z00_2410;
				}
				{	/* Cgen/cop.scm 63 */
					obj_t BgL_v1568z00_2411;

					BgL_v1568z00_2411 = create_vector(0L);
					BgL_arg1995z00_2398 = BgL_v1568z00_2411;
				}
				BGl_nopz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(29),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 47004L,
					BGl_proc2517z00zzcgen_copz00, BGl_proc2516z00zzcgen_copz00, BFALSE,
					BGl_proc2515z00zzcgen_copz00, BFALSE, BgL_arg1994z00_2397,
					BgL_arg1995z00_2398);
			}
			{	/* Cgen/cop.scm 65 */
				obj_t BgL_arg2004z00_2418;
				obj_t BgL_arg2006z00_2419;

				{	/* Cgen/cop.scm 65 */
					obj_t BgL_v1569z00_2432;

					BgL_v1569z00_2432 = create_vector(1L);
					{	/* Cgen/cop.scm 65 */
						obj_t BgL_arg2012z00_2433;

						BgL_arg2012z00_2433 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2519z00zzcgen_copz00, BGl_proc2518z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1569z00_2432, 0L, BgL_arg2012z00_2433);
					}
					BgL_arg2004z00_2418 = BgL_v1569z00_2432;
				}
				{	/* Cgen/cop.scm 65 */
					obj_t BgL_v1570z00_2443;

					BgL_v1570z00_2443 = create_vector(0L);
					BgL_arg2006z00_2419 = BgL_v1570z00_2443;
				}
				BGl_stopz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(30),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 24049L,
					BGl_proc2522z00zzcgen_copz00, BGl_proc2521z00zzcgen_copz00, BFALSE,
					BGl_proc2520z00zzcgen_copz00, BFALSE, BgL_arg2004z00_2418,
					BgL_arg2006z00_2419);
			}
			{	/* Cgen/cop.scm 68 */
				obj_t BgL_arg2020z00_2450;
				obj_t BgL_arg2021z00_2451;

				{	/* Cgen/cop.scm 68 */
					obj_t BgL_v1571z00_2465;

					BgL_v1571z00_2465 = create_vector(2L);
					{	/* Cgen/cop.scm 68 */
						obj_t BgL_arg2029z00_2466;

						BgL_arg2029z00_2466 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc2524z00zzcgen_copz00, BGl_proc2523z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_varcz00zzcgen_copz00);
						VECTOR_SET(BgL_v1571z00_2465, 0L, BgL_arg2029z00_2466);
					}
					{	/* Cgen/cop.scm 68 */
						obj_t BgL_arg2034z00_2476;

						BgL_arg2034z00_2476 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2526z00zzcgen_copz00, BGl_proc2525z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1571z00_2465, 1L, BgL_arg2034z00_2476);
					}
					BgL_arg2020z00_2450 = BgL_v1571z00_2465;
				}
				{	/* Cgen/cop.scm 68 */
					obj_t BgL_v1572z00_2486;

					BgL_v1572z00_2486 = create_vector(0L);
					BgL_arg2021z00_2451 = BgL_v1572z00_2486;
				}
				BGl_csetqz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(32),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 22879L,
					BGl_proc2529z00zzcgen_copz00, BGl_proc2528z00zzcgen_copz00, BFALSE,
					BGl_proc2527z00zzcgen_copz00, BFALSE, BgL_arg2020z00_2450,
					BgL_arg2021z00_2451);
			}
			{	/* Cgen/cop.scm 72 */
				obj_t BgL_arg2044z00_2493;
				obj_t BgL_arg2045z00_2494;

				{	/* Cgen/cop.scm 72 */
					obj_t BgL_v1573z00_2509;

					BgL_v1573z00_2509 = create_vector(3L);
					{	/* Cgen/cop.scm 72 */
						obj_t BgL_arg2051z00_2510;

						BgL_arg2051z00_2510 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc2531z00zzcgen_copz00, BGl_proc2530z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1573z00_2509, 0L, BgL_arg2051z00_2510);
					}
					{	/* Cgen/cop.scm 72 */
						obj_t BgL_arg2058z00_2520;

						BgL_arg2058z00_2520 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(34),
							BGl_proc2533z00zzcgen_copz00, BGl_proc2532z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1573z00_2509, 1L, BgL_arg2058z00_2520);
					}
					{	/* Cgen/cop.scm 72 */
						obj_t BgL_arg2063z00_2530;

						BgL_arg2063z00_2530 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(35),
							BGl_proc2535z00zzcgen_copz00, BGl_proc2534z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1573z00_2509, 2L, BgL_arg2063z00_2530);
					}
					BgL_arg2044z00_2493 = BgL_v1573z00_2509;
				}
				{	/* Cgen/cop.scm 72 */
					obj_t BgL_v1574z00_2540;

					BgL_v1574z00_2540 = create_vector(0L);
					BgL_arg2045z00_2494 = BgL_v1574z00_2540;
				}
				BGl_cifz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(36),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 63683L,
					BGl_proc2538z00zzcgen_copz00, BGl_proc2537z00zzcgen_copz00, BFALSE,
					BGl_proc2536z00zzcgen_copz00, BFALSE, BgL_arg2044z00_2493,
					BgL_arg2045z00_2494);
			}
			{	/* Cgen/cop.scm 77 */
				obj_t BgL_arg2072z00_2547;
				obj_t BgL_arg2074z00_2548;

				{	/* Cgen/cop.scm 77 */
					obj_t BgL_v1575z00_2561;

					BgL_v1575z00_2561 = create_vector(1L);
					{	/* Cgen/cop.scm 77 */
						obj_t BgL_arg2080z00_2562;

						BgL_arg2080z00_2562 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(37),
							BGl_proc2540z00zzcgen_copz00, BGl_proc2539z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1575z00_2561, 0L, BgL_arg2080z00_2562);
					}
					BgL_arg2072z00_2547 = BgL_v1575z00_2561;
				}
				{	/* Cgen/cop.scm 77 */
					obj_t BgL_v1576z00_2572;

					BgL_v1576z00_2572 = create_vector(0L);
					BgL_arg2074z00_2548 = BgL_v1576z00_2572;
				}
				BGl_localzd2varzd2zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(38),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 30293L,
					BGl_proc2543z00zzcgen_copz00, BGl_proc2542z00zzcgen_copz00, BFALSE,
					BGl_proc2541z00zzcgen_copz00, BFALSE, BgL_arg2072z00_2547,
					BgL_arg2074z00_2548);
			}
			{	/* Cgen/cop.scm 80 */
				obj_t BgL_arg2089z00_2579;
				obj_t BgL_arg2090z00_2580;

				{	/* Cgen/cop.scm 80 */
					obj_t BgL_v1577z00_2595;

					BgL_v1577z00_2595 = create_vector(3L);
					{	/* Cgen/cop.scm 80 */
						obj_t BgL_arg2097z00_2596;

						BgL_arg2097z00_2596 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(39),
							BGl_proc2545z00zzcgen_copz00, BGl_proc2544z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1577z00_2595, 0L, BgL_arg2097z00_2596);
					}
					{	/* Cgen/cop.scm 80 */
						obj_t BgL_arg2102z00_2606;

						BgL_arg2102z00_2606 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2547z00zzcgen_copz00, BGl_proc2546z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1577z00_2595, 1L, BgL_arg2102z00_2606);
					}
					{	/* Cgen/cop.scm 80 */
						obj_t BgL_arg2107z00_2616;

						BgL_arg2107z00_2616 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(40),
							BGl_proc2549z00zzcgen_copz00, BGl_proc2548z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(41));
						VECTOR_SET(BgL_v1577z00_2595, 2L, BgL_arg2107z00_2616);
					}
					BgL_arg2089z00_2579 = BgL_v1577z00_2595;
				}
				{	/* Cgen/cop.scm 80 */
					obj_t BgL_v1578z00_2626;

					BgL_v1578z00_2626 = create_vector(0L);
					BgL_arg2090z00_2580 = BgL_v1578z00_2626;
				}
				BGl_cfuncallz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(42),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 63079L,
					BGl_proc2552z00zzcgen_copz00, BGl_proc2551z00zzcgen_copz00, BFALSE,
					BGl_proc2550z00zzcgen_copz00, BFALSE, BgL_arg2089z00_2579,
					BgL_arg2090z00_2580);
			}
			{	/* Cgen/cop.scm 85 */
				obj_t BgL_arg2115z00_2633;
				obj_t BgL_arg2116z00_2634;

				{	/* Cgen/cop.scm 85 */
					obj_t BgL_v1580z00_2648;

					BgL_v1580z00_2648 = create_vector(2L);
					{	/* Cgen/cop.scm 85 */
						obj_t BgL_arg2122z00_2649;

						BgL_arg2122z00_2649 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(39),
							BGl_proc2554z00zzcgen_copz00, BGl_proc2553z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1580z00_2648, 0L, BgL_arg2122z00_2649);
					}
					{	/* Cgen/cop.scm 85 */
						obj_t BgL_arg2127z00_2659;

						BgL_arg2127z00_2659 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2556z00zzcgen_copz00, BGl_proc2555z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1580z00_2648, 1L, BgL_arg2127z00_2659);
					}
					BgL_arg2115z00_2633 = BgL_v1580z00_2648;
				}
				{	/* Cgen/cop.scm 85 */
					obj_t BgL_v1581z00_2669;

					BgL_v1581z00_2669 = create_vector(0L);
					BgL_arg2116z00_2634 = BgL_v1581z00_2669;
				}
				BGl_capplyz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(43),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 67L,
					BGl_proc2559z00zzcgen_copz00, BGl_proc2558z00zzcgen_copz00, BFALSE,
					BGl_proc2557z00zzcgen_copz00, BFALSE, BgL_arg2115z00_2633,
					BgL_arg2116z00_2634);
			}
			{	/* Cgen/cop.scm 89 */
				obj_t BgL_arg2136z00_2676;
				obj_t BgL_arg2137z00_2677;

				{	/* Cgen/cop.scm 89 */
					obj_t BgL_v1582z00_2692;

					BgL_v1582z00_2692 = create_vector(3L);
					{	/* Cgen/cop.scm 89 */
						obj_t BgL_arg2143z00_2693;

						BgL_arg2143z00_2693 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(39),
							BGl_proc2561z00zzcgen_copz00, BGl_proc2560z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1582z00_2692, 0L, BgL_arg2143z00_2693);
					}
					{	/* Cgen/cop.scm 89 */
						obj_t BgL_arg2148z00_2703;

						BgL_arg2148z00_2703 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2563z00zzcgen_copz00, BGl_proc2562z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1582z00_2692, 1L, BgL_arg2148z00_2703);
					}
					{	/* Cgen/cop.scm 89 */
						obj_t BgL_arg2154z00_2713;

						BgL_arg2154z00_2713 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(44),
							BGl_proc2565z00zzcgen_copz00, BGl_proc2564z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1582z00_2692, 2L, BgL_arg2154z00_2713);
					}
					BgL_arg2136z00_2676 = BgL_v1582z00_2692;
				}
				{	/* Cgen/cop.scm 89 */
					obj_t BgL_v1583z00_2723;

					BgL_v1583z00_2723 = create_vector(0L);
					BgL_arg2137z00_2677 = BgL_v1583z00_2723;
				}
				BGl_cappz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(45),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 2540L,
					BGl_proc2568z00zzcgen_copz00, BGl_proc2567z00zzcgen_copz00, BFALSE,
					BGl_proc2566z00zzcgen_copz00, BFALSE, BgL_arg2136z00_2676,
					BgL_arg2137z00_2677);
			}
			{	/* Cgen/cop.scm 94 */
				obj_t BgL_arg2162z00_2730;
				obj_t BgL_arg2163z00_2731;

				{	/* Cgen/cop.scm 94 */
					obj_t BgL_v1584z00_2746;

					BgL_v1584z00_2746 = create_vector(3L);
					{	/* Cgen/cop.scm 94 */
						obj_t BgL_arg2169z00_2747;

						BgL_arg2169z00_2747 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(46),
							BGl_proc2570z00zzcgen_copz00, BGl_proc2569z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1584z00_2746, 0L, BgL_arg2169z00_2747);
					}
					{	/* Cgen/cop.scm 94 */
						obj_t BgL_arg2174z00_2757;

						BgL_arg2174z00_2757 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(47),
							BGl_proc2572z00zzcgen_copz00, BGl_proc2571z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1584z00_2746, 1L, BgL_arg2174z00_2757);
					}
					{	/* Cgen/cop.scm 94 */
						obj_t BgL_arg2179z00_2767;

						BgL_arg2179z00_2767 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc2574z00zzcgen_copz00, BGl_proc2573z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1584z00_2746, 2L, BgL_arg2179z00_2767);
					}
					BgL_arg2162z00_2730 = BgL_v1584z00_2746;
				}
				{	/* Cgen/cop.scm 94 */
					obj_t BgL_v1585z00_2777;

					BgL_v1585z00_2777 = create_vector(0L);
					BgL_arg2163z00_2731 = BgL_v1585z00_2777;
				}
				BGl_cfailz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(48),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 41205L,
					BGl_proc2577z00zzcgen_copz00, BGl_proc2576z00zzcgen_copz00, BFALSE,
					BGl_proc2575z00zzcgen_copz00, BFALSE, BgL_arg2162z00_2730,
					BgL_arg2163z00_2731);
			}
			{	/* Cgen/cop.scm 99 */
				obj_t BgL_arg2187z00_2784;
				obj_t BgL_arg2188z00_2785;

				{	/* Cgen/cop.scm 99 */
					obj_t BgL_v1586z00_2799;

					BgL_v1586z00_2799 = create_vector(2L);
					{	/* Cgen/cop.scm 99 */
						obj_t BgL_arg2194z00_2800;

						BgL_arg2194z00_2800 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc2579z00zzcgen_copz00, BGl_proc2578z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1586z00_2799, 0L, BgL_arg2194z00_2800);
					}
					{	/* Cgen/cop.scm 99 */
						obj_t BgL_arg2200z00_2810;

						BgL_arg2200z00_2810 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(49),
							BGl_proc2581z00zzcgen_copz00, BGl_proc2580z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1586z00_2799, 1L, BgL_arg2200z00_2810);
					}
					BgL_arg2187z00_2784 = BgL_v1586z00_2799;
				}
				{	/* Cgen/cop.scm 99 */
					obj_t BgL_v1588z00_2820;

					BgL_v1588z00_2820 = create_vector(0L);
					BgL_arg2188z00_2785 = BgL_v1588z00_2820;
				}
				BGl_cswitchz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(50),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 9006L,
					BGl_proc2584z00zzcgen_copz00, BGl_proc2583z00zzcgen_copz00, BFALSE,
					BGl_proc2582z00zzcgen_copz00, BFALSE, BgL_arg2187z00_2784,
					BgL_arg2188z00_2785);
			}
			{	/* Cgen/cop.scm 103 */
				obj_t BgL_arg2208z00_2827;
				obj_t BgL_arg2209z00_2828;

				{	/* Cgen/cop.scm 103 */
					obj_t BgL_v1589z00_2842;

					BgL_v1589z00_2842 = create_vector(2L);
					{	/* Cgen/cop.scm 103 */
						obj_t BgL_arg2215z00_2843;

						BgL_arg2215z00_2843 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2586z00zzcgen_copz00, BGl_proc2585z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1589z00_2842, 0L, BgL_arg2215z00_2843);
					}
					{	/* Cgen/cop.scm 103 */
						obj_t BgL_arg2220z00_2853;

						BgL_arg2220z00_2853 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(44),
							BGl_proc2588z00zzcgen_copz00, BGl_proc2587z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1589z00_2842, 1L, BgL_arg2220z00_2853);
					}
					BgL_arg2208z00_2827 = BgL_v1589z00_2842;
				}
				{	/* Cgen/cop.scm 103 */
					obj_t BgL_v1590z00_2863;

					BgL_v1590z00_2863 = create_vector(0L);
					BgL_arg2209z00_2828 = BgL_v1590z00_2863;
				}
				BGl_cmakezd2boxzd2zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(51),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 30927L,
					BGl_proc2591z00zzcgen_copz00, BGl_proc2590z00zzcgen_copz00, BFALSE,
					BGl_proc2589z00zzcgen_copz00, BFALSE, BgL_arg2208z00_2827,
					BgL_arg2209z00_2828);
			}
			{	/* Cgen/cop.scm 107 */
				obj_t BgL_arg2228z00_2870;
				obj_t BgL_arg2229z00_2871;

				{	/* Cgen/cop.scm 107 */
					obj_t BgL_v1591z00_2884;

					BgL_v1591z00_2884 = create_vector(1L);
					{	/* Cgen/cop.scm 107 */
						obj_t BgL_arg2235z00_2885;

						BgL_arg2235z00_2885 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc2593z00zzcgen_copz00, BGl_proc2592z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1591z00_2884, 0L, BgL_arg2235z00_2885);
					}
					BgL_arg2228z00_2870 = BgL_v1591z00_2884;
				}
				{	/* Cgen/cop.scm 107 */
					obj_t BgL_v1592z00_2895;

					BgL_v1592z00_2895 = create_vector(0L);
					BgL_arg2229z00_2871 = BgL_v1592z00_2895;
				}
				BGl_cboxzd2refzd2zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(52),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 17949L,
					BGl_proc2596z00zzcgen_copz00, BGl_proc2595z00zzcgen_copz00, BFALSE,
					BGl_proc2594z00zzcgen_copz00, BFALSE, BgL_arg2228z00_2870,
					BgL_arg2229z00_2871);
			}
			{	/* Cgen/cop.scm 110 */
				obj_t BgL_arg2243z00_2902;
				obj_t BgL_arg2244z00_2903;

				{	/* Cgen/cop.scm 110 */
					obj_t BgL_v1593z00_2917;

					BgL_v1593z00_2917 = create_vector(2L);
					{	/* Cgen/cop.scm 110 */
						obj_t BgL_arg2250z00_2918;

						BgL_arg2250z00_2918 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc2598z00zzcgen_copz00, BGl_proc2597z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1593z00_2917, 0L, BgL_arg2250z00_2918);
					}
					{	/* Cgen/cop.scm 110 */
						obj_t BgL_arg2255z00_2928;

						BgL_arg2255z00_2928 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2600z00zzcgen_copz00, BGl_proc2599z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1593z00_2917, 1L, BgL_arg2255z00_2928);
					}
					BgL_arg2243z00_2902 = BgL_v1593z00_2917;
				}
				{	/* Cgen/cop.scm 110 */
					obj_t BgL_v1594z00_2938;

					BgL_v1594z00_2938 = create_vector(0L);
					BgL_arg2244z00_2903 = BgL_v1594z00_2938;
				}
				BGl_cboxzd2setz12zc0zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(53),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 33291L,
					BGl_proc2603z00zzcgen_copz00, BGl_proc2602z00zzcgen_copz00, BFALSE,
					BGl_proc2601z00zzcgen_copz00, BFALSE, BgL_arg2243z00_2902,
					BgL_arg2244z00_2903);
			}
			{	/* Cgen/cop.scm 114 */
				obj_t BgL_arg2263z00_2945;
				obj_t BgL_arg2264z00_2946;

				{	/* Cgen/cop.scm 114 */
					obj_t BgL_v1595z00_2961;

					BgL_v1595z00_2961 = create_vector(3L);
					{	/* Cgen/cop.scm 114 */
						obj_t BgL_arg2270z00_2962;

						BgL_arg2270z00_2962 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(54),
							BGl_proc2605z00zzcgen_copz00, BGl_proc2604z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1595z00_2961, 0L, BgL_arg2270z00_2962);
					}
					{	/* Cgen/cop.scm 114 */
						obj_t BgL_arg2275z00_2972;

						BgL_arg2275z00_2972 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(55),
							BGl_proc2607z00zzcgen_copz00, BGl_proc2606z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1595z00_2961, 1L, BgL_arg2275z00_2972);
					}
					{	/* Cgen/cop.scm 114 */
						obj_t BgL_arg2280z00_2982;

						BgL_arg2280z00_2982 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2609z00zzcgen_copz00, BGl_proc2608z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1595z00_2961, 2L, BgL_arg2280z00_2982);
					}
					BgL_arg2263z00_2945 = BgL_v1595z00_2961;
				}
				{	/* Cgen/cop.scm 114 */
					obj_t BgL_v1596z00_2992;

					BgL_v1596z00_2992 = create_vector(0L);
					BgL_arg2264z00_2946 = BgL_v1596z00_2992;
				}
				BGl_csetzd2exzd2itz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(56),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 42091L,
					BGl_proc2612z00zzcgen_copz00, BGl_proc2611z00zzcgen_copz00, BFALSE,
					BGl_proc2610z00zzcgen_copz00, BFALSE, BgL_arg2263z00_2945,
					BgL_arg2264z00_2946);
			}
			{	/* Cgen/cop.scm 119 */
				obj_t BgL_arg2289z00_2999;
				obj_t BgL_arg2290z00_3000;

				{	/* Cgen/cop.scm 119 */
					obj_t BgL_v1597z00_3014;

					BgL_v1597z00_3014 = create_vector(2L);
					{	/* Cgen/cop.scm 119 */
						obj_t BgL_arg2296z00_3015;

						BgL_arg2296z00_3015 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(54),
							BGl_proc2614z00zzcgen_copz00, BGl_proc2613z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1597z00_3014, 0L, BgL_arg2296z00_3015);
					}
					{	/* Cgen/cop.scm 119 */
						obj_t BgL_arg2301z00_3025;

						BgL_arg2301z00_3025 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2616z00zzcgen_copz00, BGl_proc2615z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1597z00_3014, 1L, BgL_arg2301z00_3025);
					}
					BgL_arg2289z00_2999 = BgL_v1597z00_3014;
				}
				{	/* Cgen/cop.scm 119 */
					obj_t BgL_v1598z00_3035;

					BgL_v1598z00_3035 = create_vector(0L);
					BgL_arg2290z00_3000 = BgL_v1598z00_3035;
				}
				BGl_cjumpzd2exzd2itz00zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(57),
					CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 37589L,
					BGl_proc2619z00zzcgen_copz00, BGl_proc2618z00zzcgen_copz00, BFALSE,
					BGl_proc2617z00zzcgen_copz00, BFALSE, BgL_arg2289z00_2999,
					BgL_arg2290z00_3000);
			}
			{	/* Cgen/cop.scm 123 */
				obj_t BgL_arg2311z00_3043;
				obj_t BgL_arg2312z00_3044;

				{	/* Cgen/cop.scm 123 */
					obj_t BgL_v1599z00_3089;

					BgL_v1599z00_3089 = create_vector(2L);
					{	/* Cgen/cop.scm 123 */
						obj_t BgL_arg2324z00_3090;

						BgL_arg2324z00_3090 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc2621z00zzcgen_copz00, BGl_proc2620z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_clabelz00zzcgen_copz00);
						VECTOR_SET(BgL_v1599z00_3089, 0L, BgL_arg2324z00_3090);
					}
					{	/* Cgen/cop.scm 123 */
						obj_t BgL_arg2330z00_3100;

						BgL_arg2330z00_3100 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(58),
							BGl_proc2623z00zzcgen_copz00, BGl_proc2622z00zzcgen_copz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(8));
						VECTOR_SET(BgL_v1599z00_3089, 1L, BgL_arg2330z00_3100);
					}
					BgL_arg2311z00_3043 = BgL_v1599z00_3089;
				}
				{	/* Cgen/cop.scm 123 */
					obj_t BgL_v1600z00_3110;

					BgL_v1600z00_3110 = create_vector(0L);
					BgL_arg2312z00_3044 = BgL_v1600z00_3110;
				}
				BGl_sfunzf2Czf2zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(59),
					CNST_TABLE_REF(4), BGl_sfunz00zzast_varz00, 43966L,
					BGl_proc2627z00zzcgen_copz00, BGl_proc2626z00zzcgen_copz00, BFALSE,
					BGl_proc2625z00zzcgen_copz00, BGl_proc2624z00zzcgen_copz00,
					BgL_arg2311z00_3043, BgL_arg2312z00_3044);
			}
			{	/* Cgen/cop.scm 127 */
				obj_t BgL_arg2339z00_3118;
				obj_t BgL_arg2340z00_3119;

				{	/* Cgen/cop.scm 127 */
					obj_t BgL_v1601z00_3132;

					BgL_v1601z00_3132 = create_vector(1L);
					{	/* Cgen/cop.scm 127 */
						obj_t BgL_arg2348z00_3133;

						BgL_arg2348z00_3133 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2629z00zzcgen_copz00, BGl_proc2628z00zzcgen_copz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE,
							BGl_copz00zzcgen_copz00);
						VECTOR_SET(BgL_v1601z00_3132, 0L, BgL_arg2348z00_3133);
					}
					BgL_arg2339z00_3118 = BgL_v1601z00_3132;
				}
				{	/* Cgen/cop.scm 127 */
					obj_t BgL_v1602z00_3143;

					BgL_v1602z00_3143 = create_vector(0L);
					BgL_arg2340z00_3119 = BgL_v1602z00_3143;
				}
				return (BGl_bdbzd2blockzd2zzcgen_copz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(60),
						CNST_TABLE_REF(4), BGl_copz00zzcgen_copz00, 59132L,
						BGl_proc2632z00zzcgen_copz00, BGl_proc2631z00zzcgen_copz00, BFALSE,
						BGl_proc2630z00zzcgen_copz00, BFALSE, BgL_arg2339z00_3118,
						BgL_arg2340z00_3119), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2346> */
	obj_t BGl_z62zc3z04anonymousza32346ze3ze5zzcgen_copz00(obj_t BgL_envz00_5680,
		obj_t BgL_new1316z00_5681)
	{
		{	/* Cgen/cop.scm 127 */
			{
				BgL_bdbzd2blockzd2_bglt BgL_auxz00_9497;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_bdbzd2blockzd2_bglt) BgL_new1316z00_5681))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9501;

					{	/* Cgen/cop.scm 127 */
						obj_t BgL_classz00_6693;

						BgL_classz00_6693 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 127 */
							obj_t BgL__ortest_1117z00_6694;

							BgL__ortest_1117z00_6694 = BGL_CLASS_NIL(BgL_classz00_6693);
							if (CBOOL(BgL__ortest_1117z00_6694))
								{	/* Cgen/cop.scm 127 */
									BgL_auxz00_9501 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6694);
								}
							else
								{	/* Cgen/cop.scm 127 */
									BgL_auxz00_9501 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6693));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_bdbzd2blockzd2_bglt) BgL_new1316z00_5681))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_9501), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_9511;

					{	/* Cgen/cop.scm 127 */
						obj_t BgL_classz00_6695;

						BgL_classz00_6695 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 127 */
							obj_t BgL__ortest_1117z00_6696;

							BgL__ortest_1117z00_6696 = BGL_CLASS_NIL(BgL_classz00_6695);
							if (CBOOL(BgL__ortest_1117z00_6696))
								{	/* Cgen/cop.scm 127 */
									BgL_auxz00_9511 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6696);
								}
							else
								{	/* Cgen/cop.scm 127 */
									BgL_auxz00_9511 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6695));
								}
						}
					}
					((((BgL_bdbzd2blockzd2_bglt) COBJECT(
									((BgL_bdbzd2blockzd2_bglt) BgL_new1316z00_5681)))->
							BgL_bodyz00) = ((BgL_copz00_bglt) BgL_auxz00_9511), BUNSPEC);
				}
				BgL_auxz00_9497 = ((BgL_bdbzd2blockzd2_bglt) BgL_new1316z00_5681);
				return ((obj_t) BgL_auxz00_9497);
			}
		}

	}



/* &lambda2343 */
	BgL_bdbzd2blockzd2_bglt BGl_z62lambda2343z62zzcgen_copz00(obj_t
		BgL_envz00_5682)
	{
		{	/* Cgen/cop.scm 127 */
			{	/* Cgen/cop.scm 127 */
				BgL_bdbzd2blockzd2_bglt BgL_new1315z00_6697;

				BgL_new1315z00_6697 =
					((BgL_bdbzd2blockzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_bdbzd2blockzd2_bgl))));
				{	/* Cgen/cop.scm 127 */
					long BgL_arg2345z00_6698;

					BgL_arg2345z00_6698 = BGL_CLASS_NUM(BGl_bdbzd2blockzd2zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1315z00_6697), BgL_arg2345z00_6698);
				}
				return BgL_new1315z00_6697;
			}
		}

	}



/* &lambda2341 */
	BgL_bdbzd2blockzd2_bglt BGl_z62lambda2341z62zzcgen_copz00(obj_t
		BgL_envz00_5683, obj_t BgL_loc1312z00_5684, obj_t BgL_type1313z00_5685,
		obj_t BgL_body1314z00_5686)
	{
		{	/* Cgen/cop.scm 127 */
			{	/* Cgen/cop.scm 127 */
				BgL_bdbzd2blockzd2_bglt BgL_new1435z00_6701;

				{	/* Cgen/cop.scm 127 */
					BgL_bdbzd2blockzd2_bglt BgL_new1434z00_6702;

					BgL_new1434z00_6702 =
						((BgL_bdbzd2blockzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bdbzd2blockzd2_bgl))));
					{	/* Cgen/cop.scm 127 */
						long BgL_arg2342z00_6703;

						BgL_arg2342z00_6703 =
							BGL_CLASS_NUM(BGl_bdbzd2blockzd2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1434z00_6702), BgL_arg2342z00_6703);
					}
					BgL_new1435z00_6701 = BgL_new1434z00_6702;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1435z00_6701)))->BgL_locz00) =
					((obj_t) BgL_loc1312z00_5684), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1435z00_6701)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1313z00_5685)),
					BUNSPEC);
				((((BgL_bdbzd2blockzd2_bglt) COBJECT(BgL_new1435z00_6701))->
						BgL_bodyz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_body1314z00_5686)),
					BUNSPEC);
				return BgL_new1435z00_6701;
			}
		}

	}



/* &lambda2352 */
	obj_t BGl_z62lambda2352z62zzcgen_copz00(obj_t BgL_envz00_5687,
		obj_t BgL_oz00_5688, obj_t BgL_vz00_5689)
	{
		{	/* Cgen/cop.scm 127 */
			return
				((((BgL_bdbzd2blockzd2_bglt) COBJECT(
							((BgL_bdbzd2blockzd2_bglt) BgL_oz00_5688)))->BgL_bodyz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5689)), BUNSPEC);
		}

	}



/* &lambda2351 */
	BgL_copz00_bglt BGl_z62lambda2351z62zzcgen_copz00(obj_t BgL_envz00_5690,
		obj_t BgL_oz00_5691)
	{
		{	/* Cgen/cop.scm 127 */
			return
				(((BgL_bdbzd2blockzd2_bglt) COBJECT(
						((BgL_bdbzd2blockzd2_bglt) BgL_oz00_5691)))->BgL_bodyz00);
		}

	}



/* &lambda2319 */
	BgL_sfunz00_bglt BGl_z62lambda2319z62zzcgen_copz00(obj_t BgL_envz00_5692,
		obj_t BgL_o1310z00_5693)
	{
		{	/* Cgen/cop.scm 123 */
			{	/* Cgen/cop.scm 123 */
				long BgL_arg2320z00_6708;

				{	/* Cgen/cop.scm 123 */
					obj_t BgL_arg2321z00_6709;

					{	/* Cgen/cop.scm 123 */
						obj_t BgL_arg2323z00_6710;

						{	/* Cgen/cop.scm 123 */
							obj_t BgL_arg1815z00_6711;
							long BgL_arg1816z00_6712;

							BgL_arg1815z00_6711 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cgen/cop.scm 123 */
								long BgL_arg1817z00_6713;

								BgL_arg1817z00_6713 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_o1310z00_5693)));
								BgL_arg1816z00_6712 = (BgL_arg1817z00_6713 - OBJECT_TYPE);
							}
							BgL_arg2323z00_6710 =
								VECTOR_REF(BgL_arg1815z00_6711, BgL_arg1816z00_6712);
						}
						BgL_arg2321z00_6709 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2323z00_6710);
					}
					{	/* Cgen/cop.scm 123 */
						obj_t BgL_tmpz00_9549;

						BgL_tmpz00_9549 = ((obj_t) BgL_arg2321z00_6709);
						BgL_arg2320z00_6708 = BGL_CLASS_NUM(BgL_tmpz00_9549);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) BgL_o1310z00_5693)), BgL_arg2320z00_6708);
			}
			{	/* Cgen/cop.scm 123 */
				BgL_objectz00_bglt BgL_tmpz00_9555;

				BgL_tmpz00_9555 =
					((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1310z00_5693));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9555, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1310z00_5693));
			return ((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1310z00_5693));
		}

	}



/* &<@anonymous:2318> */
	obj_t BGl_z62zc3z04anonymousza32318ze3ze5zzcgen_copz00(obj_t BgL_envz00_5694,
		obj_t BgL_new1309z00_5695)
	{
		{	/* Cgen/cop.scm 123 */
			{
				BgL_sfunz00_bglt BgL_auxz00_9563;

				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_sfunz00_bglt) BgL_new1309z00_5695))))->BgL_arityz00) =
					((long) 0L), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_predicatezd2ofzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_stackzd2allocatorzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_topzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_thezd2closurezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_failsafez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_argszd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_argszd2retescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_propertyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_argszd2namezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_bodyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_dssslzd2keywordszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_optionalsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_keysz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_strengthz00) =
					((obj_t) CNST_TABLE_REF(61)), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1309z00_5695))))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_clabelz00_bglt BgL_auxz00_9638;
					BgL_sfunzf2czf2_bglt BgL_auxz00_9631;

					{	/* Cgen/cop.scm 123 */
						obj_t BgL_classz00_6715;

						BgL_classz00_6715 = BGl_clabelz00zzcgen_copz00;
						{	/* Cgen/cop.scm 123 */
							obj_t BgL__ortest_1117z00_6716;

							BgL__ortest_1117z00_6716 = BGL_CLASS_NIL(BgL_classz00_6715);
							if (CBOOL(BgL__ortest_1117z00_6716))
								{	/* Cgen/cop.scm 123 */
									BgL_auxz00_9638 =
										((BgL_clabelz00_bglt) BgL__ortest_1117z00_6716);
								}
							else
								{	/* Cgen/cop.scm 123 */
									BgL_auxz00_9638 =
										((BgL_clabelz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6715));
								}
						}
					}
					{
						obj_t BgL_auxz00_9632;

						{	/* Cgen/cop.scm 123 */
							BgL_objectz00_bglt BgL_tmpz00_9633;

							BgL_tmpz00_9633 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1309z00_5695));
							BgL_auxz00_9632 = BGL_OBJECT_WIDENING(BgL_tmpz00_9633);
						}
						BgL_auxz00_9631 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_9632);
					}
					((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_9631))->BgL_labelz00) =
						((BgL_clabelz00_bglt) BgL_auxz00_9638), BUNSPEC);
				}
				{
					BgL_sfunzf2czf2_bglt BgL_auxz00_9646;

					{
						obj_t BgL_auxz00_9647;

						{	/* Cgen/cop.scm 123 */
							BgL_objectz00_bglt BgL_tmpz00_9648;

							BgL_tmpz00_9648 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1309z00_5695));
							BgL_auxz00_9647 = BGL_OBJECT_WIDENING(BgL_tmpz00_9648);
						}
						BgL_auxz00_9646 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_9647);
					}
					((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_9646))->
							BgL_integratedz00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_9563 = ((BgL_sfunz00_bglt) BgL_new1309z00_5695);
				return ((obj_t) BgL_auxz00_9563);
			}
		}

	}



/* &lambda2316 */
	BgL_sfunz00_bglt BGl_z62lambda2316z62zzcgen_copz00(obj_t BgL_envz00_5696,
		obj_t BgL_o1306z00_5697)
	{
		{	/* Cgen/cop.scm 123 */
			{	/* Cgen/cop.scm 123 */
				BgL_sfunzf2czf2_bglt BgL_wide1308z00_6718;

				BgL_wide1308z00_6718 =
					((BgL_sfunzf2czf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sfunzf2czf2_bgl))));
				{	/* Cgen/cop.scm 123 */
					obj_t BgL_auxz00_9661;
					BgL_objectz00_bglt BgL_tmpz00_9657;

					BgL_auxz00_9661 = ((obj_t) BgL_wide1308z00_6718);
					BgL_tmpz00_9657 =
						((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1306z00_5697)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9657, BgL_auxz00_9661);
				}
				((BgL_objectz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1306z00_5697)));
				{	/* Cgen/cop.scm 123 */
					long BgL_arg2317z00_6719;

					BgL_arg2317z00_6719 = BGL_CLASS_NUM(BGl_sfunzf2Czf2zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt)
								((BgL_sfunz00_bglt) BgL_o1306z00_5697))), BgL_arg2317z00_6719);
				}
				return
					((BgL_sfunz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1306z00_5697)));
			}
		}

	}



/* &lambda2313 */
	BgL_sfunz00_bglt BGl_z62lambda2313z62zzcgen_copz00(obj_t BgL_envz00_5698,
		obj_t BgL_arity1282z00_5699, obj_t BgL_sidezd2effect1283zd2_5700,
		obj_t BgL_predicatezd2of1284zd2_5701,
		obj_t BgL_stackzd2allocator1285zd2_5702, obj_t BgL_topzf31286zf3_5703,
		obj_t BgL_thezd2closure1287zd2_5704, obj_t BgL_effect1288z00_5705,
		obj_t BgL_failsafe1289z00_5706, obj_t BgL_argszd2noescape1290zd2_5707,
		obj_t BgL_argszd2retescape1291zd2_5708, obj_t BgL_property1292z00_5709,
		obj_t BgL_args1293z00_5710, obj_t BgL_argszd2name1294zd2_5711,
		obj_t BgL_body1295z00_5712, obj_t BgL_class1296z00_5713,
		obj_t BgL_dssslzd2keywords1297zd2_5714, obj_t BgL_loc1298z00_5715,
		obj_t BgL_optionals1299z00_5716, obj_t BgL_keys1300z00_5717,
		obj_t BgL_thezd2closurezd2global1301z00_5718,
		obj_t BgL_strength1302z00_5719, obj_t BgL_stackable1303z00_5720,
		obj_t BgL_label1304z00_5721, obj_t BgL_integrated1305z00_5722)
	{
		{	/* Cgen/cop.scm 123 */
			{	/* Cgen/cop.scm 123 */
				long BgL_arity1282z00_6720;
				bool_t BgL_topzf31286zf3_6721;
				bool_t BgL_integrated1305z00_6724;

				BgL_arity1282z00_6720 = (long) CINT(BgL_arity1282z00_5699);
				BgL_topzf31286zf3_6721 = CBOOL(BgL_topzf31286zf3_5703);
				BgL_integrated1305z00_6724 = CBOOL(BgL_integrated1305z00_5722);
				{	/* Cgen/cop.scm 123 */
					BgL_sfunz00_bglt BgL_new1432z00_6725;

					{	/* Cgen/cop.scm 123 */
						BgL_sfunz00_bglt BgL_tmp1430z00_6726;
						BgL_sfunzf2czf2_bglt BgL_wide1431z00_6727;

						{
							BgL_sfunz00_bglt BgL_auxz00_9678;

							{	/* Cgen/cop.scm 123 */
								BgL_sfunz00_bglt BgL_new1429z00_6728;

								BgL_new1429z00_6728 =
									((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sfunz00_bgl))));
								{	/* Cgen/cop.scm 123 */
									long BgL_arg2315z00_6729;

									BgL_arg2315z00_6729 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1429z00_6728),
										BgL_arg2315z00_6729);
								}
								{	/* Cgen/cop.scm 123 */
									BgL_objectz00_bglt BgL_tmpz00_9683;

									BgL_tmpz00_9683 = ((BgL_objectz00_bglt) BgL_new1429z00_6728);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9683, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1429z00_6728);
								BgL_auxz00_9678 = BgL_new1429z00_6728;
							}
							BgL_tmp1430z00_6726 = ((BgL_sfunz00_bglt) BgL_auxz00_9678);
						}
						BgL_wide1431z00_6727 =
							((BgL_sfunzf2czf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sfunzf2czf2_bgl))));
						{	/* Cgen/cop.scm 123 */
							obj_t BgL_auxz00_9691;
							BgL_objectz00_bglt BgL_tmpz00_9689;

							BgL_auxz00_9691 = ((obj_t) BgL_wide1431z00_6727);
							BgL_tmpz00_9689 = ((BgL_objectz00_bglt) BgL_tmp1430z00_6726);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9689, BgL_auxz00_9691);
						}
						((BgL_objectz00_bglt) BgL_tmp1430z00_6726);
						{	/* Cgen/cop.scm 123 */
							long BgL_arg2314z00_6730;

							BgL_arg2314z00_6730 = BGL_CLASS_NUM(BGl_sfunzf2Czf2zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1430z00_6726),
								BgL_arg2314z00_6730);
						}
						BgL_new1432z00_6725 = ((BgL_sfunz00_bglt) BgL_tmp1430z00_6726);
					}
					((((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_new1432z00_6725)))->BgL_arityz00) =
						((long) BgL_arity1282z00_6720), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1283zd2_5700), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_predicatezd2ofzd2) =
						((obj_t) BgL_predicatezd2of1284zd2_5701), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_stackzd2allocatorzd2) =
						((obj_t) BgL_stackzd2allocator1285zd2_5702), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_topzf3zf3) =
						((bool_t) BgL_topzf31286zf3_6721), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_thezd2closurezd2) =
						((obj_t) BgL_thezd2closure1287zd2_5704), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_effectz00) =
						((obj_t) BgL_effect1288z00_5705), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_failsafez00) =
						((obj_t) BgL_failsafe1289z00_5706), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_argszd2noescapezd2) =
						((obj_t) BgL_argszd2noescape1290zd2_5707), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1432z00_6725)))->BgL_argszd2retescapezd2) =
						((obj_t) BgL_argszd2retescape1291zd2_5708), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_propertyz00) =
						((obj_t) BgL_property1292z00_5709), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_argsz00) =
						((obj_t) BgL_args1293z00_5710), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_argszd2namezd2) =
						((obj_t) BgL_argszd2name1294zd2_5711), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_bodyz00) =
						((obj_t) BgL_body1295z00_5712), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_classz00) =
						((obj_t) BgL_class1296z00_5713), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_dssslzd2keywordszd2) =
						((obj_t) BgL_dssslzd2keywords1297zd2_5714), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_locz00) =
						((obj_t) BgL_loc1298z00_5715), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_optionalsz00) =
						((obj_t) BgL_optionals1299z00_5716), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_keysz00) =
						((obj_t) BgL_keys1300z00_5717), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_thezd2closurezd2globalz00) =
						((obj_t) BgL_thezd2closurezd2global1301z00_5718), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_strengthz00) =
						((obj_t) ((obj_t) BgL_strength1302z00_5719)), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1432z00_6725)))->BgL_stackablez00) =
						((obj_t) BgL_stackable1303z00_5720), BUNSPEC);
					{
						BgL_sfunzf2czf2_bglt BgL_auxz00_9744;

						{
							obj_t BgL_auxz00_9745;

							{	/* Cgen/cop.scm 123 */
								BgL_objectz00_bglt BgL_tmpz00_9746;

								BgL_tmpz00_9746 = ((BgL_objectz00_bglt) BgL_new1432z00_6725);
								BgL_auxz00_9745 = BGL_OBJECT_WIDENING(BgL_tmpz00_9746);
							}
							BgL_auxz00_9744 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_9745);
						}
						((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_9744))->BgL_labelz00) =
							((BgL_clabelz00_bglt) ((BgL_clabelz00_bglt)
									BgL_label1304z00_5721)), BUNSPEC);
					}
					{
						BgL_sfunzf2czf2_bglt BgL_auxz00_9752;

						{
							obj_t BgL_auxz00_9753;

							{	/* Cgen/cop.scm 123 */
								BgL_objectz00_bglt BgL_tmpz00_9754;

								BgL_tmpz00_9754 = ((BgL_objectz00_bglt) BgL_new1432z00_6725);
								BgL_auxz00_9753 = BGL_OBJECT_WIDENING(BgL_tmpz00_9754);
							}
							BgL_auxz00_9752 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_9753);
						}
						((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_9752))->
								BgL_integratedz00) =
							((bool_t) BgL_integrated1305z00_6724), BUNSPEC);
					}
					return BgL_new1432z00_6725;
				}
			}
		}

	}



/* &lambda2335 */
	obj_t BGl_z62lambda2335z62zzcgen_copz00(obj_t BgL_envz00_5723,
		obj_t BgL_oz00_5724, obj_t BgL_vz00_5725)
	{
		{	/* Cgen/cop.scm 123 */
			{	/* Cgen/cop.scm 123 */
				bool_t BgL_vz00_6732;

				BgL_vz00_6732 = CBOOL(BgL_vz00_5725);
				{
					BgL_sfunzf2czf2_bglt BgL_auxz00_9760;

					{
						obj_t BgL_auxz00_9761;

						{	/* Cgen/cop.scm 123 */
							BgL_objectz00_bglt BgL_tmpz00_9762;

							BgL_tmpz00_9762 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_5724));
							BgL_auxz00_9761 = BGL_OBJECT_WIDENING(BgL_tmpz00_9762);
						}
						BgL_auxz00_9760 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_9761);
					}
					return
						((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_9760))->
							BgL_integratedz00) = ((bool_t) BgL_vz00_6732), BUNSPEC);
				}
			}
		}

	}



/* &lambda2334 */
	obj_t BGl_z62lambda2334z62zzcgen_copz00(obj_t BgL_envz00_5726,
		obj_t BgL_oz00_5727)
	{
		{	/* Cgen/cop.scm 123 */
			{	/* Cgen/cop.scm 123 */
				bool_t BgL_tmpz00_9768;

				{
					BgL_sfunzf2czf2_bglt BgL_auxz00_9769;

					{
						obj_t BgL_auxz00_9770;

						{	/* Cgen/cop.scm 123 */
							BgL_objectz00_bglt BgL_tmpz00_9771;

							BgL_tmpz00_9771 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_5727));
							BgL_auxz00_9770 = BGL_OBJECT_WIDENING(BgL_tmpz00_9771);
						}
						BgL_auxz00_9769 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_9770);
					}
					BgL_tmpz00_9768 =
						(((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_9769))->
						BgL_integratedz00);
				}
				return BBOOL(BgL_tmpz00_9768);
			}
		}

	}



/* &lambda2328 */
	obj_t BGl_z62lambda2328z62zzcgen_copz00(obj_t BgL_envz00_5728,
		obj_t BgL_oz00_5729, obj_t BgL_vz00_5730)
	{
		{	/* Cgen/cop.scm 123 */
			{
				BgL_sfunzf2czf2_bglt BgL_auxz00_9778;

				{
					obj_t BgL_auxz00_9779;

					{	/* Cgen/cop.scm 123 */
						BgL_objectz00_bglt BgL_tmpz00_9780;

						BgL_tmpz00_9780 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_5729));
						BgL_auxz00_9779 = BGL_OBJECT_WIDENING(BgL_tmpz00_9780);
					}
					BgL_auxz00_9778 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_9779);
				}
				return
					((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_9778))->BgL_labelz00) =
					((BgL_clabelz00_bglt) ((BgL_clabelz00_bglt) BgL_vz00_5730)), BUNSPEC);
			}
		}

	}



/* &lambda2327 */
	BgL_clabelz00_bglt BGl_z62lambda2327z62zzcgen_copz00(obj_t BgL_envz00_5731,
		obj_t BgL_oz00_5732)
	{
		{	/* Cgen/cop.scm 123 */
			{
				BgL_sfunzf2czf2_bglt BgL_auxz00_9787;

				{
					obj_t BgL_auxz00_9788;

					{	/* Cgen/cop.scm 123 */
						BgL_objectz00_bglt BgL_tmpz00_9789;

						BgL_tmpz00_9789 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_5732));
						BgL_auxz00_9788 = BGL_OBJECT_WIDENING(BgL_tmpz00_9789);
					}
					BgL_auxz00_9787 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_9788);
				}
				return
					(((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_9787))->BgL_labelz00);
			}
		}

	}



/* &<@anonymous:2295> */
	obj_t BGl_z62zc3z04anonymousza32295ze3ze5zzcgen_copz00(obj_t BgL_envz00_5733,
		obj_t BgL_new1280z00_5734)
	{
		{	/* Cgen/cop.scm 119 */
			{
				BgL_cjumpzd2exzd2itz00_bglt BgL_auxz00_9795;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cjumpzd2exzd2itz00_bglt) BgL_new1280z00_5734))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9799;

					{	/* Cgen/cop.scm 119 */
						obj_t BgL_classz00_6738;

						BgL_classz00_6738 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 119 */
							obj_t BgL__ortest_1117z00_6739;

							BgL__ortest_1117z00_6739 = BGL_CLASS_NIL(BgL_classz00_6738);
							if (CBOOL(BgL__ortest_1117z00_6739))
								{	/* Cgen/cop.scm 119 */
									BgL_auxz00_9799 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6739);
								}
							else
								{	/* Cgen/cop.scm 119 */
									BgL_auxz00_9799 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6738));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cjumpzd2exzd2itz00_bglt) BgL_new1280z00_5734))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_9799), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_9809;

					{	/* Cgen/cop.scm 119 */
						obj_t BgL_classz00_6740;

						BgL_classz00_6740 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 119 */
							obj_t BgL__ortest_1117z00_6741;

							BgL__ortest_1117z00_6741 = BGL_CLASS_NIL(BgL_classz00_6740);
							if (CBOOL(BgL__ortest_1117z00_6741))
								{	/* Cgen/cop.scm 119 */
									BgL_auxz00_9809 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6741);
								}
							else
								{	/* Cgen/cop.scm 119 */
									BgL_auxz00_9809 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6740));
								}
						}
					}
					((((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_cjumpzd2exzd2itz00_bglt) BgL_new1280z00_5734)))->
							BgL_exitz00) = ((BgL_copz00_bglt) BgL_auxz00_9809), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_9818;

					{	/* Cgen/cop.scm 119 */
						obj_t BgL_classz00_6742;

						BgL_classz00_6742 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 119 */
							obj_t BgL__ortest_1117z00_6743;

							BgL__ortest_1117z00_6743 = BGL_CLASS_NIL(BgL_classz00_6742);
							if (CBOOL(BgL__ortest_1117z00_6743))
								{	/* Cgen/cop.scm 119 */
									BgL_auxz00_9818 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6743);
								}
							else
								{	/* Cgen/cop.scm 119 */
									BgL_auxz00_9818 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6742));
								}
						}
					}
					((((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_cjumpzd2exzd2itz00_bglt) BgL_new1280z00_5734)))->
							BgL_valuez00) = ((BgL_copz00_bglt) BgL_auxz00_9818), BUNSPEC);
				}
				BgL_auxz00_9795 = ((BgL_cjumpzd2exzd2itz00_bglt) BgL_new1280z00_5734);
				return ((obj_t) BgL_auxz00_9795);
			}
		}

	}



/* &lambda2293 */
	BgL_cjumpzd2exzd2itz00_bglt BGl_z62lambda2293z62zzcgen_copz00(obj_t
		BgL_envz00_5735)
	{
		{	/* Cgen/cop.scm 119 */
			{	/* Cgen/cop.scm 119 */
				BgL_cjumpzd2exzd2itz00_bglt BgL_new1279z00_6744;

				BgL_new1279z00_6744 =
					((BgL_cjumpzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cjumpzd2exzd2itz00_bgl))));
				{	/* Cgen/cop.scm 119 */
					long BgL_arg2294z00_6745;

					BgL_arg2294z00_6745 =
						BGL_CLASS_NUM(BGl_cjumpzd2exzd2itz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1279z00_6744), BgL_arg2294z00_6745);
				}
				return BgL_new1279z00_6744;
			}
		}

	}



/* &lambda2291 */
	BgL_cjumpzd2exzd2itz00_bglt BGl_z62lambda2291z62zzcgen_copz00(obj_t
		BgL_envz00_5736, obj_t BgL_loc1275z00_5737, obj_t BgL_type1276z00_5738,
		obj_t BgL_exit1277z00_5739, obj_t BgL_value1278z00_5740)
	{
		{	/* Cgen/cop.scm 119 */
			{	/* Cgen/cop.scm 119 */
				BgL_cjumpzd2exzd2itz00_bglt BgL_new1428z00_6749;

				{	/* Cgen/cop.scm 119 */
					BgL_cjumpzd2exzd2itz00_bglt BgL_new1427z00_6750;

					BgL_new1427z00_6750 =
						((BgL_cjumpzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cjumpzd2exzd2itz00_bgl))));
					{	/* Cgen/cop.scm 119 */
						long BgL_arg2292z00_6751;

						BgL_arg2292z00_6751 =
							BGL_CLASS_NUM(BGl_cjumpzd2exzd2itz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1427z00_6750), BgL_arg2292z00_6751);
					}
					BgL_new1428z00_6749 = BgL_new1427z00_6750;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1428z00_6749)))->BgL_locz00) =
					((obj_t) BgL_loc1275z00_5737), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1428z00_6749)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1276z00_5738)),
					BUNSPEC);
				((((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(BgL_new1428z00_6749))->
						BgL_exitz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_exit1277z00_5739)),
					BUNSPEC);
				((((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(BgL_new1428z00_6749))->
						BgL_valuez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_value1278z00_5740)),
					BUNSPEC);
				return BgL_new1428z00_6749;
			}
		}

	}



/* &lambda2306 */
	obj_t BGl_z62lambda2306z62zzcgen_copz00(obj_t BgL_envz00_5741,
		obj_t BgL_oz00_5742, obj_t BgL_vz00_5743)
	{
		{	/* Cgen/cop.scm 119 */
			return
				((((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5742)))->BgL_valuez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5743)), BUNSPEC);
		}

	}



/* &lambda2305 */
	BgL_copz00_bglt BGl_z62lambda2305z62zzcgen_copz00(obj_t BgL_envz00_5744,
		obj_t BgL_oz00_5745)
	{
		{	/* Cgen/cop.scm 119 */
			return
				(((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(
						((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5745)))->BgL_valuez00);
		}

	}



/* &lambda2300 */
	obj_t BGl_z62lambda2300z62zzcgen_copz00(obj_t BgL_envz00_5746,
		obj_t BgL_oz00_5747, obj_t BgL_vz00_5748)
	{
		{	/* Cgen/cop.scm 119 */
			return
				((((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5747)))->BgL_exitz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5748)), BUNSPEC);
		}

	}



/* &lambda2299 */
	BgL_copz00_bglt BGl_z62lambda2299z62zzcgen_copz00(obj_t BgL_envz00_5749,
		obj_t BgL_oz00_5750)
	{
		{	/* Cgen/cop.scm 119 */
			return
				(((BgL_cjumpzd2exzd2itz00_bglt) COBJECT(
						((BgL_cjumpzd2exzd2itz00_bglt) BgL_oz00_5750)))->BgL_exitz00);
		}

	}



/* &<@anonymous:2269> */
	obj_t BGl_z62zc3z04anonymousza32269ze3ze5zzcgen_copz00(obj_t BgL_envz00_5751,
		obj_t BgL_new1273z00_5752)
	{
		{	/* Cgen/cop.scm 114 */
			{
				BgL_csetzd2exzd2itz00_bglt BgL_auxz00_9856;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_csetzd2exzd2itz00_bglt) BgL_new1273z00_5752))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9860;

					{	/* Cgen/cop.scm 114 */
						obj_t BgL_classz00_6759;

						BgL_classz00_6759 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 114 */
							obj_t BgL__ortest_1117z00_6760;

							BgL__ortest_1117z00_6760 = BGL_CLASS_NIL(BgL_classz00_6759);
							if (CBOOL(BgL__ortest_1117z00_6760))
								{	/* Cgen/cop.scm 114 */
									BgL_auxz00_9860 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6760);
								}
							else
								{	/* Cgen/cop.scm 114 */
									BgL_auxz00_9860 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6759));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_csetzd2exzd2itz00_bglt) BgL_new1273z00_5752))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_9860), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_9870;

					{	/* Cgen/cop.scm 114 */
						obj_t BgL_classz00_6761;

						BgL_classz00_6761 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 114 */
							obj_t BgL__ortest_1117z00_6762;

							BgL__ortest_1117z00_6762 = BGL_CLASS_NIL(BgL_classz00_6761);
							if (CBOOL(BgL__ortest_1117z00_6762))
								{	/* Cgen/cop.scm 114 */
									BgL_auxz00_9870 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6762);
								}
							else
								{	/* Cgen/cop.scm 114 */
									BgL_auxz00_9870 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6761));
								}
						}
					}
					((((BgL_csetzd2exzd2itz00_bglt) COBJECT(
									((BgL_csetzd2exzd2itz00_bglt) BgL_new1273z00_5752)))->
							BgL_exitz00) = ((BgL_copz00_bglt) BgL_auxz00_9870), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_9879;

					{	/* Cgen/cop.scm 114 */
						obj_t BgL_classz00_6763;

						BgL_classz00_6763 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 114 */
							obj_t BgL__ortest_1117z00_6764;

							BgL__ortest_1117z00_6764 = BGL_CLASS_NIL(BgL_classz00_6763);
							if (CBOOL(BgL__ortest_1117z00_6764))
								{	/* Cgen/cop.scm 114 */
									BgL_auxz00_9879 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6764);
								}
							else
								{	/* Cgen/cop.scm 114 */
									BgL_auxz00_9879 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6763));
								}
						}
					}
					((((BgL_csetzd2exzd2itz00_bglt) COBJECT(
									((BgL_csetzd2exzd2itz00_bglt) BgL_new1273z00_5752)))->
							BgL_jumpzd2valuezd2) =
						((BgL_copz00_bglt) BgL_auxz00_9879), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_9888;

					{	/* Cgen/cop.scm 114 */
						obj_t BgL_classz00_6765;

						BgL_classz00_6765 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 114 */
							obj_t BgL__ortest_1117z00_6766;

							BgL__ortest_1117z00_6766 = BGL_CLASS_NIL(BgL_classz00_6765);
							if (CBOOL(BgL__ortest_1117z00_6766))
								{	/* Cgen/cop.scm 114 */
									BgL_auxz00_9888 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6766);
								}
							else
								{	/* Cgen/cop.scm 114 */
									BgL_auxz00_9888 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6765));
								}
						}
					}
					((((BgL_csetzd2exzd2itz00_bglt) COBJECT(
									((BgL_csetzd2exzd2itz00_bglt) BgL_new1273z00_5752)))->
							BgL_bodyz00) = ((BgL_copz00_bglt) BgL_auxz00_9888), BUNSPEC);
				}
				BgL_auxz00_9856 = ((BgL_csetzd2exzd2itz00_bglt) BgL_new1273z00_5752);
				return ((obj_t) BgL_auxz00_9856);
			}
		}

	}



/* &lambda2267 */
	BgL_csetzd2exzd2itz00_bglt BGl_z62lambda2267z62zzcgen_copz00(obj_t
		BgL_envz00_5753)
	{
		{	/* Cgen/cop.scm 114 */
			{	/* Cgen/cop.scm 114 */
				BgL_csetzd2exzd2itz00_bglt BgL_new1272z00_6767;

				BgL_new1272z00_6767 =
					((BgL_csetzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_csetzd2exzd2itz00_bgl))));
				{	/* Cgen/cop.scm 114 */
					long BgL_arg2268z00_6768;

					BgL_arg2268z00_6768 =
						BGL_CLASS_NUM(BGl_csetzd2exzd2itz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1272z00_6767), BgL_arg2268z00_6768);
				}
				return BgL_new1272z00_6767;
			}
		}

	}



/* &lambda2265 */
	BgL_csetzd2exzd2itz00_bglt BGl_z62lambda2265z62zzcgen_copz00(obj_t
		BgL_envz00_5754, obj_t BgL_loc1267z00_5755, obj_t BgL_type1268z00_5756,
		obj_t BgL_exit1269z00_5757, obj_t BgL_jumpzd2value1270zd2_5758,
		obj_t BgL_body1271z00_5759)
	{
		{	/* Cgen/cop.scm 114 */
			{	/* Cgen/cop.scm 114 */
				BgL_csetzd2exzd2itz00_bglt BgL_new1426z00_6773;

				{	/* Cgen/cop.scm 114 */
					BgL_csetzd2exzd2itz00_bglt BgL_new1425z00_6774;

					BgL_new1425z00_6774 =
						((BgL_csetzd2exzd2itz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_csetzd2exzd2itz00_bgl))));
					{	/* Cgen/cop.scm 114 */
						long BgL_arg2266z00_6775;

						BgL_arg2266z00_6775 =
							BGL_CLASS_NUM(BGl_csetzd2exzd2itz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1425z00_6774), BgL_arg2266z00_6775);
					}
					BgL_new1426z00_6773 = BgL_new1425z00_6774;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1426z00_6773)))->BgL_locz00) =
					((obj_t) BgL_loc1267z00_5755), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1426z00_6773)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1268z00_5756)),
					BUNSPEC);
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_new1426z00_6773))->
						BgL_exitz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_exit1269z00_5757)),
					BUNSPEC);
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_new1426z00_6773))->
						BgL_jumpzd2valuezd2) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_jumpzd2value1270zd2_5758)),
					BUNSPEC);
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(BgL_new1426z00_6773))->
						BgL_bodyz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_body1271z00_5759)),
					BUNSPEC);
				return BgL_new1426z00_6773;
			}
		}

	}



/* &lambda2284 */
	obj_t BGl_z62lambda2284z62zzcgen_copz00(obj_t BgL_envz00_5760,
		obj_t BgL_oz00_5761, obj_t BgL_vz00_5762)
	{
		{	/* Cgen/cop.scm 114 */
			return
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(
							((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5761)))->BgL_bodyz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5762)), BUNSPEC);
		}

	}



/* &lambda2283 */
	BgL_copz00_bglt BGl_z62lambda2283z62zzcgen_copz00(obj_t BgL_envz00_5763,
		obj_t BgL_oz00_5764)
	{
		{	/* Cgen/cop.scm 114 */
			return
				(((BgL_csetzd2exzd2itz00_bglt) COBJECT(
						((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5764)))->BgL_bodyz00);
		}

	}



/* &lambda2279 */
	obj_t BGl_z62lambda2279z62zzcgen_copz00(obj_t BgL_envz00_5765,
		obj_t BgL_oz00_5766, obj_t BgL_vz00_5767)
	{
		{	/* Cgen/cop.scm 114 */
			return
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(
							((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5766)))->
					BgL_jumpzd2valuezd2) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5767)), BUNSPEC);
		}

	}



/* &lambda2278 */
	BgL_copz00_bglt BGl_z62lambda2278z62zzcgen_copz00(obj_t BgL_envz00_5768,
		obj_t BgL_oz00_5769)
	{
		{	/* Cgen/cop.scm 114 */
			return
				(((BgL_csetzd2exzd2itz00_bglt) COBJECT(
						((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5769)))->
				BgL_jumpzd2valuezd2);
		}

	}



/* &lambda2274 */
	obj_t BGl_z62lambda2274z62zzcgen_copz00(obj_t BgL_envz00_5770,
		obj_t BgL_oz00_5771, obj_t BgL_vz00_5772)
	{
		{	/* Cgen/cop.scm 114 */
			return
				((((BgL_csetzd2exzd2itz00_bglt) COBJECT(
							((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5771)))->BgL_exitz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5772)), BUNSPEC);
		}

	}



/* &lambda2273 */
	BgL_copz00_bglt BGl_z62lambda2273z62zzcgen_copz00(obj_t BgL_envz00_5773,
		obj_t BgL_oz00_5774)
	{
		{	/* Cgen/cop.scm 114 */
			return
				(((BgL_csetzd2exzd2itz00_bglt) COBJECT(
						((BgL_csetzd2exzd2itz00_bglt) BgL_oz00_5774)))->BgL_exitz00);
		}

	}



/* &<@anonymous:2249> */
	obj_t BGl_z62zc3z04anonymousza32249ze3ze5zzcgen_copz00(obj_t BgL_envz00_5775,
		obj_t BgL_new1265z00_5776)
	{
		{	/* Cgen/cop.scm 110 */
			{
				BgL_cboxzd2setz12zc0_bglt BgL_auxz00_9933;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cboxzd2setz12zc0_bglt) BgL_new1265z00_5776))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9937;

					{	/* Cgen/cop.scm 110 */
						obj_t BgL_classz00_6786;

						BgL_classz00_6786 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 110 */
							obj_t BgL__ortest_1117z00_6787;

							BgL__ortest_1117z00_6787 = BGL_CLASS_NIL(BgL_classz00_6786);
							if (CBOOL(BgL__ortest_1117z00_6787))
								{	/* Cgen/cop.scm 110 */
									BgL_auxz00_9937 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6787);
								}
							else
								{	/* Cgen/cop.scm 110 */
									BgL_auxz00_9937 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6786));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cboxzd2setz12zc0_bglt) BgL_new1265z00_5776))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_9937), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_9947;

					{	/* Cgen/cop.scm 110 */
						obj_t BgL_classz00_6788;

						BgL_classz00_6788 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 110 */
							obj_t BgL__ortest_1117z00_6789;

							BgL__ortest_1117z00_6789 = BGL_CLASS_NIL(BgL_classz00_6788);
							if (CBOOL(BgL__ortest_1117z00_6789))
								{	/* Cgen/cop.scm 110 */
									BgL_auxz00_9947 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6789);
								}
							else
								{	/* Cgen/cop.scm 110 */
									BgL_auxz00_9947 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6788));
								}
						}
					}
					((((BgL_cboxzd2setz12zc0_bglt) COBJECT(
									((BgL_cboxzd2setz12zc0_bglt) BgL_new1265z00_5776)))->
							BgL_varz00) = ((BgL_copz00_bglt) BgL_auxz00_9947), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_9956;

					{	/* Cgen/cop.scm 110 */
						obj_t BgL_classz00_6790;

						BgL_classz00_6790 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 110 */
							obj_t BgL__ortest_1117z00_6791;

							BgL__ortest_1117z00_6791 = BGL_CLASS_NIL(BgL_classz00_6790);
							if (CBOOL(BgL__ortest_1117z00_6791))
								{	/* Cgen/cop.scm 110 */
									BgL_auxz00_9956 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6791);
								}
							else
								{	/* Cgen/cop.scm 110 */
									BgL_auxz00_9956 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6790));
								}
						}
					}
					((((BgL_cboxzd2setz12zc0_bglt) COBJECT(
									((BgL_cboxzd2setz12zc0_bglt) BgL_new1265z00_5776)))->
							BgL_valuez00) = ((BgL_copz00_bglt) BgL_auxz00_9956), BUNSPEC);
				}
				BgL_auxz00_9933 = ((BgL_cboxzd2setz12zc0_bglt) BgL_new1265z00_5776);
				return ((obj_t) BgL_auxz00_9933);
			}
		}

	}



/* &lambda2247 */
	BgL_cboxzd2setz12zc0_bglt BGl_z62lambda2247z62zzcgen_copz00(obj_t
		BgL_envz00_5777)
	{
		{	/* Cgen/cop.scm 110 */
			{	/* Cgen/cop.scm 110 */
				BgL_cboxzd2setz12zc0_bglt BgL_new1264z00_6792;

				BgL_new1264z00_6792 =
					((BgL_cboxzd2setz12zc0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cboxzd2setz12zc0_bgl))));
				{	/* Cgen/cop.scm 110 */
					long BgL_arg2248z00_6793;

					BgL_arg2248z00_6793 =
						BGL_CLASS_NUM(BGl_cboxzd2setz12zc0zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1264z00_6792), BgL_arg2248z00_6793);
				}
				return BgL_new1264z00_6792;
			}
		}

	}



/* &lambda2245 */
	BgL_cboxzd2setz12zc0_bglt BGl_z62lambda2245z62zzcgen_copz00(obj_t
		BgL_envz00_5778, obj_t BgL_loc1260z00_5779, obj_t BgL_type1261z00_5780,
		obj_t BgL_var1262z00_5781, obj_t BgL_value1263z00_5782)
	{
		{	/* Cgen/cop.scm 110 */
			{	/* Cgen/cop.scm 110 */
				BgL_cboxzd2setz12zc0_bglt BgL_new1424z00_6797;

				{	/* Cgen/cop.scm 110 */
					BgL_cboxzd2setz12zc0_bglt BgL_new1423z00_6798;

					BgL_new1423z00_6798 =
						((BgL_cboxzd2setz12zc0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cboxzd2setz12zc0_bgl))));
					{	/* Cgen/cop.scm 110 */
						long BgL_arg2246z00_6799;

						BgL_arg2246z00_6799 =
							BGL_CLASS_NUM(BGl_cboxzd2setz12zc0zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1423z00_6798), BgL_arg2246z00_6799);
					}
					BgL_new1424z00_6797 = BgL_new1423z00_6798;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1424z00_6797)))->BgL_locz00) =
					((obj_t) BgL_loc1260z00_5779), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1424z00_6797)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1261z00_5780)),
					BUNSPEC);
				((((BgL_cboxzd2setz12zc0_bglt) COBJECT(BgL_new1424z00_6797))->
						BgL_varz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_var1262z00_5781)), BUNSPEC);
				((((BgL_cboxzd2setz12zc0_bglt) COBJECT(BgL_new1424z00_6797))->
						BgL_valuez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_value1263z00_5782)),
					BUNSPEC);
				return BgL_new1424z00_6797;
			}
		}

	}



/* &lambda2259 */
	obj_t BGl_z62lambda2259z62zzcgen_copz00(obj_t BgL_envz00_5783,
		obj_t BgL_oz00_5784, obj_t BgL_vz00_5785)
	{
		{	/* Cgen/cop.scm 110 */
			return
				((((BgL_cboxzd2setz12zc0_bglt) COBJECT(
							((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5784)))->BgL_valuez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5785)), BUNSPEC);
		}

	}



/* &lambda2258 */
	BgL_copz00_bglt BGl_z62lambda2258z62zzcgen_copz00(obj_t BgL_envz00_5786,
		obj_t BgL_oz00_5787)
	{
		{	/* Cgen/cop.scm 110 */
			return
				(((BgL_cboxzd2setz12zc0_bglt) COBJECT(
						((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5787)))->BgL_valuez00);
		}

	}



/* &lambda2254 */
	obj_t BGl_z62lambda2254z62zzcgen_copz00(obj_t BgL_envz00_5788,
		obj_t BgL_oz00_5789, obj_t BgL_vz00_5790)
	{
		{	/* Cgen/cop.scm 110 */
			return
				((((BgL_cboxzd2setz12zc0_bglt) COBJECT(
							((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5789)))->BgL_varz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5790)), BUNSPEC);
		}

	}



/* &lambda2253 */
	BgL_copz00_bglt BGl_z62lambda2253z62zzcgen_copz00(obj_t BgL_envz00_5791,
		obj_t BgL_oz00_5792)
	{
		{	/* Cgen/cop.scm 110 */
			return
				(((BgL_cboxzd2setz12zc0_bglt) COBJECT(
						((BgL_cboxzd2setz12zc0_bglt) BgL_oz00_5792)))->BgL_varz00);
		}

	}



/* &<@anonymous:2234> */
	obj_t BGl_z62zc3z04anonymousza32234ze3ze5zzcgen_copz00(obj_t BgL_envz00_5793,
		obj_t BgL_new1258z00_5794)
	{
		{	/* Cgen/cop.scm 107 */
			{
				BgL_cboxzd2refzd2_bglt BgL_auxz00_9994;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cboxzd2refzd2_bglt) BgL_new1258z00_5794))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_9998;

					{	/* Cgen/cop.scm 107 */
						obj_t BgL_classz00_6807;

						BgL_classz00_6807 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 107 */
							obj_t BgL__ortest_1117z00_6808;

							BgL__ortest_1117z00_6808 = BGL_CLASS_NIL(BgL_classz00_6807);
							if (CBOOL(BgL__ortest_1117z00_6808))
								{	/* Cgen/cop.scm 107 */
									BgL_auxz00_9998 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6808);
								}
							else
								{	/* Cgen/cop.scm 107 */
									BgL_auxz00_9998 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6807));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cboxzd2refzd2_bglt) BgL_new1258z00_5794))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_9998), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10008;

					{	/* Cgen/cop.scm 107 */
						obj_t BgL_classz00_6809;

						BgL_classz00_6809 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 107 */
							obj_t BgL__ortest_1117z00_6810;

							BgL__ortest_1117z00_6810 = BGL_CLASS_NIL(BgL_classz00_6809);
							if (CBOOL(BgL__ortest_1117z00_6810))
								{	/* Cgen/cop.scm 107 */
									BgL_auxz00_10008 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6810);
								}
							else
								{	/* Cgen/cop.scm 107 */
									BgL_auxz00_10008 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6809));
								}
						}
					}
					((((BgL_cboxzd2refzd2_bglt) COBJECT(
									((BgL_cboxzd2refzd2_bglt) BgL_new1258z00_5794)))->
							BgL_varz00) = ((BgL_copz00_bglt) BgL_auxz00_10008), BUNSPEC);
				}
				BgL_auxz00_9994 = ((BgL_cboxzd2refzd2_bglt) BgL_new1258z00_5794);
				return ((obj_t) BgL_auxz00_9994);
			}
		}

	}



/* &lambda2232 */
	BgL_cboxzd2refzd2_bglt BGl_z62lambda2232z62zzcgen_copz00(obj_t
		BgL_envz00_5795)
	{
		{	/* Cgen/cop.scm 107 */
			{	/* Cgen/cop.scm 107 */
				BgL_cboxzd2refzd2_bglt BgL_new1257z00_6811;

				BgL_new1257z00_6811 =
					((BgL_cboxzd2refzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cboxzd2refzd2_bgl))));
				{	/* Cgen/cop.scm 107 */
					long BgL_arg2233z00_6812;

					BgL_arg2233z00_6812 = BGL_CLASS_NUM(BGl_cboxzd2refzd2zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1257z00_6811), BgL_arg2233z00_6812);
				}
				return BgL_new1257z00_6811;
			}
		}

	}



/* &lambda2230 */
	BgL_cboxzd2refzd2_bglt BGl_z62lambda2230z62zzcgen_copz00(obj_t
		BgL_envz00_5796, obj_t BgL_loc1254z00_5797, obj_t BgL_type1255z00_5798,
		obj_t BgL_var1256z00_5799)
	{
		{	/* Cgen/cop.scm 107 */
			{	/* Cgen/cop.scm 107 */
				BgL_cboxzd2refzd2_bglt BgL_new1422z00_6815;

				{	/* Cgen/cop.scm 107 */
					BgL_cboxzd2refzd2_bglt BgL_new1421z00_6816;

					BgL_new1421z00_6816 =
						((BgL_cboxzd2refzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cboxzd2refzd2_bgl))));
					{	/* Cgen/cop.scm 107 */
						long BgL_arg2231z00_6817;

						BgL_arg2231z00_6817 = BGL_CLASS_NUM(BGl_cboxzd2refzd2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1421z00_6816), BgL_arg2231z00_6817);
					}
					BgL_new1422z00_6815 = BgL_new1421z00_6816;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1422z00_6815)))->BgL_locz00) =
					((obj_t) BgL_loc1254z00_5797), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1422z00_6815)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1255z00_5798)),
					BUNSPEC);
				((((BgL_cboxzd2refzd2_bglt) COBJECT(BgL_new1422z00_6815))->BgL_varz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_var1256z00_5799)), BUNSPEC);
				return BgL_new1422z00_6815;
			}
		}

	}



/* &lambda2239 */
	obj_t BGl_z62lambda2239z62zzcgen_copz00(obj_t BgL_envz00_5800,
		obj_t BgL_oz00_5801, obj_t BgL_vz00_5802)
	{
		{	/* Cgen/cop.scm 107 */
			return
				((((BgL_cboxzd2refzd2_bglt) COBJECT(
							((BgL_cboxzd2refzd2_bglt) BgL_oz00_5801)))->BgL_varz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5802)), BUNSPEC);
		}

	}



/* &lambda2238 */
	BgL_copz00_bglt BGl_z62lambda2238z62zzcgen_copz00(obj_t BgL_envz00_5803,
		obj_t BgL_oz00_5804)
	{
		{	/* Cgen/cop.scm 107 */
			return
				(((BgL_cboxzd2refzd2_bglt) COBJECT(
						((BgL_cboxzd2refzd2_bglt) BgL_oz00_5804)))->BgL_varz00);
		}

	}



/* &<@anonymous:2214> */
	obj_t BGl_z62zc3z04anonymousza32214ze3ze5zzcgen_copz00(obj_t BgL_envz00_5805,
		obj_t BgL_new1252z00_5806)
	{
		{	/* Cgen/cop.scm 103 */
			{
				BgL_cmakezd2boxzd2_bglt BgL_auxz00_10039;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cmakezd2boxzd2_bglt) BgL_new1252z00_5806))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10043;

					{	/* Cgen/cop.scm 103 */
						obj_t BgL_classz00_6822;

						BgL_classz00_6822 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 103 */
							obj_t BgL__ortest_1117z00_6823;

							BgL__ortest_1117z00_6823 = BGL_CLASS_NIL(BgL_classz00_6822);
							if (CBOOL(BgL__ortest_1117z00_6823))
								{	/* Cgen/cop.scm 103 */
									BgL_auxz00_10043 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6823);
								}
							else
								{	/* Cgen/cop.scm 103 */
									BgL_auxz00_10043 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6822));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cmakezd2boxzd2_bglt) BgL_new1252z00_5806))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10043), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10053;

					{	/* Cgen/cop.scm 103 */
						obj_t BgL_classz00_6824;

						BgL_classz00_6824 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 103 */
							obj_t BgL__ortest_1117z00_6825;

							BgL__ortest_1117z00_6825 = BGL_CLASS_NIL(BgL_classz00_6824);
							if (CBOOL(BgL__ortest_1117z00_6825))
								{	/* Cgen/cop.scm 103 */
									BgL_auxz00_10053 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6825);
								}
							else
								{	/* Cgen/cop.scm 103 */
									BgL_auxz00_10053 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6824));
								}
						}
					}
					((((BgL_cmakezd2boxzd2_bglt) COBJECT(
									((BgL_cmakezd2boxzd2_bglt) BgL_new1252z00_5806)))->
							BgL_valuez00) = ((BgL_copz00_bglt) BgL_auxz00_10053), BUNSPEC);
				}
				((((BgL_cmakezd2boxzd2_bglt) COBJECT(
								((BgL_cmakezd2boxzd2_bglt) BgL_new1252z00_5806)))->
						BgL_stackablez00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_10039 = ((BgL_cmakezd2boxzd2_bglt) BgL_new1252z00_5806);
				return ((obj_t) BgL_auxz00_10039);
			}
		}

	}



/* &lambda2212 */
	BgL_cmakezd2boxzd2_bglt BGl_z62lambda2212z62zzcgen_copz00(obj_t
		BgL_envz00_5807)
	{
		{	/* Cgen/cop.scm 103 */
			{	/* Cgen/cop.scm 103 */
				BgL_cmakezd2boxzd2_bglt BgL_new1251z00_6826;

				BgL_new1251z00_6826 =
					((BgL_cmakezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cmakezd2boxzd2_bgl))));
				{	/* Cgen/cop.scm 103 */
					long BgL_arg2213z00_6827;

					BgL_arg2213z00_6827 = BGL_CLASS_NUM(BGl_cmakezd2boxzd2zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1251z00_6826), BgL_arg2213z00_6827);
				}
				return BgL_new1251z00_6826;
			}
		}

	}



/* &lambda2210 */
	BgL_cmakezd2boxzd2_bglt BGl_z62lambda2210z62zzcgen_copz00(obj_t
		BgL_envz00_5808, obj_t BgL_loc1246z00_5809, obj_t BgL_type1247z00_5810,
		obj_t BgL_value1248z00_5811, obj_t BgL_stackable1249z00_5812)
	{
		{	/* Cgen/cop.scm 103 */
			{	/* Cgen/cop.scm 103 */
				BgL_cmakezd2boxzd2_bglt BgL_new1420z00_6830;

				{	/* Cgen/cop.scm 103 */
					BgL_cmakezd2boxzd2_bglt BgL_new1419z00_6831;

					BgL_new1419z00_6831 =
						((BgL_cmakezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cmakezd2boxzd2_bgl))));
					{	/* Cgen/cop.scm 103 */
						long BgL_arg2211z00_6832;

						BgL_arg2211z00_6832 =
							BGL_CLASS_NUM(BGl_cmakezd2boxzd2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1419z00_6831), BgL_arg2211z00_6832);
					}
					BgL_new1420z00_6830 = BgL_new1419z00_6831;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1420z00_6830)))->BgL_locz00) =
					((obj_t) BgL_loc1246z00_5809), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1420z00_6830)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1247z00_5810)),
					BUNSPEC);
				((((BgL_cmakezd2boxzd2_bglt) COBJECT(BgL_new1420z00_6830))->
						BgL_valuez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_value1248z00_5811)),
					BUNSPEC);
				((((BgL_cmakezd2boxzd2_bglt) COBJECT(BgL_new1420z00_6830))->
						BgL_stackablez00) = ((obj_t) BgL_stackable1249z00_5812), BUNSPEC);
				return BgL_new1420z00_6830;
			}
		}

	}



/* &lambda2224 */
	obj_t BGl_z62lambda2224z62zzcgen_copz00(obj_t BgL_envz00_5813,
		obj_t BgL_oz00_5814, obj_t BgL_vz00_5815)
	{
		{	/* Cgen/cop.scm 103 */
			return
				((((BgL_cmakezd2boxzd2_bglt) COBJECT(
							((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5814)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_5815), BUNSPEC);
		}

	}



/* &lambda2223 */
	obj_t BGl_z62lambda2223z62zzcgen_copz00(obj_t BgL_envz00_5816,
		obj_t BgL_oz00_5817)
	{
		{	/* Cgen/cop.scm 103 */
			return
				(((BgL_cmakezd2boxzd2_bglt) COBJECT(
						((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5817)))->BgL_stackablez00);
		}

	}



/* &lambda2219 */
	obj_t BGl_z62lambda2219z62zzcgen_copz00(obj_t BgL_envz00_5818,
		obj_t BgL_oz00_5819, obj_t BgL_vz00_5820)
	{
		{	/* Cgen/cop.scm 103 */
			return
				((((BgL_cmakezd2boxzd2_bglt) COBJECT(
							((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5819)))->BgL_valuez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5820)), BUNSPEC);
		}

	}



/* &lambda2218 */
	BgL_copz00_bglt BGl_z62lambda2218z62zzcgen_copz00(obj_t BgL_envz00_5821,
		obj_t BgL_oz00_5822)
	{
		{	/* Cgen/cop.scm 103 */
			return
				(((BgL_cmakezd2boxzd2_bglt) COBJECT(
						((BgL_cmakezd2boxzd2_bglt) BgL_oz00_5822)))->BgL_valuez00);
		}

	}



/* &<@anonymous:2193> */
	obj_t BGl_z62zc3z04anonymousza32193ze3ze5zzcgen_copz00(obj_t BgL_envz00_5823,
		obj_t BgL_new1244z00_5824)
	{
		{	/* Cgen/cop.scm 99 */
			{
				BgL_cswitchz00_bglt BgL_auxz00_10091;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cswitchz00_bglt) BgL_new1244z00_5824))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10095;

					{	/* Cgen/cop.scm 99 */
						obj_t BgL_classz00_6839;

						BgL_classz00_6839 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 99 */
							obj_t BgL__ortest_1117z00_6840;

							BgL__ortest_1117z00_6840 = BGL_CLASS_NIL(BgL_classz00_6839);
							if (CBOOL(BgL__ortest_1117z00_6840))
								{	/* Cgen/cop.scm 99 */
									BgL_auxz00_10095 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6840);
								}
							else
								{	/* Cgen/cop.scm 99 */
									BgL_auxz00_10095 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6839));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cswitchz00_bglt) BgL_new1244z00_5824))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10095), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10105;

					{	/* Cgen/cop.scm 99 */
						obj_t BgL_classz00_6841;

						BgL_classz00_6841 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 99 */
							obj_t BgL__ortest_1117z00_6842;

							BgL__ortest_1117z00_6842 = BGL_CLASS_NIL(BgL_classz00_6841);
							if (CBOOL(BgL__ortest_1117z00_6842))
								{	/* Cgen/cop.scm 99 */
									BgL_auxz00_10105 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6842);
								}
							else
								{	/* Cgen/cop.scm 99 */
									BgL_auxz00_10105 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6841));
								}
						}
					}
					((((BgL_cswitchz00_bglt) COBJECT(
									((BgL_cswitchz00_bglt) BgL_new1244z00_5824)))->BgL_testz00) =
						((BgL_copz00_bglt) BgL_auxz00_10105), BUNSPEC);
				}
				((((BgL_cswitchz00_bglt) COBJECT(
								((BgL_cswitchz00_bglt) BgL_new1244z00_5824)))->BgL_clausesz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_10091 = ((BgL_cswitchz00_bglt) BgL_new1244z00_5824);
				return ((obj_t) BgL_auxz00_10091);
			}
		}

	}



/* &lambda2191 */
	BgL_cswitchz00_bglt BGl_z62lambda2191z62zzcgen_copz00(obj_t BgL_envz00_5825)
	{
		{	/* Cgen/cop.scm 99 */
			{	/* Cgen/cop.scm 99 */
				BgL_cswitchz00_bglt BgL_new1243z00_6843;

				BgL_new1243z00_6843 =
					((BgL_cswitchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cswitchz00_bgl))));
				{	/* Cgen/cop.scm 99 */
					long BgL_arg2192z00_6844;

					BgL_arg2192z00_6844 = BGL_CLASS_NUM(BGl_cswitchz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1243z00_6843), BgL_arg2192z00_6844);
				}
				return BgL_new1243z00_6843;
			}
		}

	}



/* &lambda2189 */
	BgL_cswitchz00_bglt BGl_z62lambda2189z62zzcgen_copz00(obj_t BgL_envz00_5826,
		obj_t BgL_loc1239z00_5827, obj_t BgL_type1240z00_5828,
		obj_t BgL_test1241z00_5829, obj_t BgL_clauses1242z00_5830)
	{
		{	/* Cgen/cop.scm 99 */
			{	/* Cgen/cop.scm 99 */
				BgL_cswitchz00_bglt BgL_new1418z00_6847;

				{	/* Cgen/cop.scm 99 */
					BgL_cswitchz00_bglt BgL_new1417z00_6848;

					BgL_new1417z00_6848 =
						((BgL_cswitchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cswitchz00_bgl))));
					{	/* Cgen/cop.scm 99 */
						long BgL_arg2190z00_6849;

						BgL_arg2190z00_6849 = BGL_CLASS_NUM(BGl_cswitchz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1417z00_6848), BgL_arg2190z00_6849);
					}
					BgL_new1418z00_6847 = BgL_new1417z00_6848;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1418z00_6847)))->BgL_locz00) =
					((obj_t) BgL_loc1239z00_5827), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1418z00_6847)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1240z00_5828)),
					BUNSPEC);
				((((BgL_cswitchz00_bglt) COBJECT(BgL_new1418z00_6847))->BgL_testz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_test1241z00_5829)),
					BUNSPEC);
				((((BgL_cswitchz00_bglt) COBJECT(BgL_new1418z00_6847))->
						BgL_clausesz00) = ((obj_t) BgL_clauses1242z00_5830), BUNSPEC);
				return BgL_new1418z00_6847;
			}
		}

	}



/* &lambda2204 */
	obj_t BGl_z62lambda2204z62zzcgen_copz00(obj_t BgL_envz00_5831,
		obj_t BgL_oz00_5832, obj_t BgL_vz00_5833)
	{
		{	/* Cgen/cop.scm 99 */
			return
				((((BgL_cswitchz00_bglt) COBJECT(
							((BgL_cswitchz00_bglt) BgL_oz00_5832)))->BgL_clausesz00) =
				((obj_t) BgL_vz00_5833), BUNSPEC);
		}

	}



/* &lambda2203 */
	obj_t BGl_z62lambda2203z62zzcgen_copz00(obj_t BgL_envz00_5834,
		obj_t BgL_oz00_5835)
	{
		{	/* Cgen/cop.scm 99 */
			return
				(((BgL_cswitchz00_bglt) COBJECT(
						((BgL_cswitchz00_bglt) BgL_oz00_5835)))->BgL_clausesz00);
		}

	}



/* &lambda2199 */
	obj_t BGl_z62lambda2199z62zzcgen_copz00(obj_t BgL_envz00_5836,
		obj_t BgL_oz00_5837, obj_t BgL_vz00_5838)
	{
		{	/* Cgen/cop.scm 99 */
			return
				((((BgL_cswitchz00_bglt) COBJECT(
							((BgL_cswitchz00_bglt) BgL_oz00_5837)))->BgL_testz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5838)), BUNSPEC);
		}

	}



/* &lambda2198 */
	BgL_copz00_bglt BGl_z62lambda2198z62zzcgen_copz00(obj_t BgL_envz00_5839,
		obj_t BgL_oz00_5840)
	{
		{	/* Cgen/cop.scm 99 */
			return
				(((BgL_cswitchz00_bglt) COBJECT(
						((BgL_cswitchz00_bglt) BgL_oz00_5840)))->BgL_testz00);
		}

	}



/* &<@anonymous:2168> */
	obj_t BGl_z62zc3z04anonymousza32168ze3ze5zzcgen_copz00(obj_t BgL_envz00_5841,
		obj_t BgL_new1237z00_5842)
	{
		{	/* Cgen/cop.scm 94 */
			{
				BgL_cfailz00_bglt BgL_auxz00_10143;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cfailz00_bglt) BgL_new1237z00_5842))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10147;

					{	/* Cgen/cop.scm 94 */
						obj_t BgL_classz00_6856;

						BgL_classz00_6856 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 94 */
							obj_t BgL__ortest_1117z00_6857;

							BgL__ortest_1117z00_6857 = BGL_CLASS_NIL(BgL_classz00_6856);
							if (CBOOL(BgL__ortest_1117z00_6857))
								{	/* Cgen/cop.scm 94 */
									BgL_auxz00_10147 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6857);
								}
							else
								{	/* Cgen/cop.scm 94 */
									BgL_auxz00_10147 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6856));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cfailz00_bglt) BgL_new1237z00_5842))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10147), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10157;

					{	/* Cgen/cop.scm 94 */
						obj_t BgL_classz00_6858;

						BgL_classz00_6858 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 94 */
							obj_t BgL__ortest_1117z00_6859;

							BgL__ortest_1117z00_6859 = BGL_CLASS_NIL(BgL_classz00_6858);
							if (CBOOL(BgL__ortest_1117z00_6859))
								{	/* Cgen/cop.scm 94 */
									BgL_auxz00_10157 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6859);
								}
							else
								{	/* Cgen/cop.scm 94 */
									BgL_auxz00_10157 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6858));
								}
						}
					}
					((((BgL_cfailz00_bglt) COBJECT(
									((BgL_cfailz00_bglt) BgL_new1237z00_5842)))->BgL_procz00) =
						((BgL_copz00_bglt) BgL_auxz00_10157), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10166;

					{	/* Cgen/cop.scm 94 */
						obj_t BgL_classz00_6860;

						BgL_classz00_6860 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 94 */
							obj_t BgL__ortest_1117z00_6861;

							BgL__ortest_1117z00_6861 = BGL_CLASS_NIL(BgL_classz00_6860);
							if (CBOOL(BgL__ortest_1117z00_6861))
								{	/* Cgen/cop.scm 94 */
									BgL_auxz00_10166 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6861);
								}
							else
								{	/* Cgen/cop.scm 94 */
									BgL_auxz00_10166 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6860));
								}
						}
					}
					((((BgL_cfailz00_bglt) COBJECT(
									((BgL_cfailz00_bglt) BgL_new1237z00_5842)))->BgL_msgz00) =
						((BgL_copz00_bglt) BgL_auxz00_10166), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10175;

					{	/* Cgen/cop.scm 94 */
						obj_t BgL_classz00_6862;

						BgL_classz00_6862 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 94 */
							obj_t BgL__ortest_1117z00_6863;

							BgL__ortest_1117z00_6863 = BGL_CLASS_NIL(BgL_classz00_6862);
							if (CBOOL(BgL__ortest_1117z00_6863))
								{	/* Cgen/cop.scm 94 */
									BgL_auxz00_10175 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6863);
								}
							else
								{	/* Cgen/cop.scm 94 */
									BgL_auxz00_10175 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6862));
								}
						}
					}
					((((BgL_cfailz00_bglt) COBJECT(
									((BgL_cfailz00_bglt) BgL_new1237z00_5842)))->BgL_objz00) =
						((BgL_copz00_bglt) BgL_auxz00_10175), BUNSPEC);
				}
				BgL_auxz00_10143 = ((BgL_cfailz00_bglt) BgL_new1237z00_5842);
				return ((obj_t) BgL_auxz00_10143);
			}
		}

	}



/* &lambda2166 */
	BgL_cfailz00_bglt BGl_z62lambda2166z62zzcgen_copz00(obj_t BgL_envz00_5843)
	{
		{	/* Cgen/cop.scm 94 */
			{	/* Cgen/cop.scm 94 */
				BgL_cfailz00_bglt BgL_new1236z00_6864;

				BgL_new1236z00_6864 =
					((BgL_cfailz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cfailz00_bgl))));
				{	/* Cgen/cop.scm 94 */
					long BgL_arg2167z00_6865;

					BgL_arg2167z00_6865 = BGL_CLASS_NUM(BGl_cfailz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1236z00_6864), BgL_arg2167z00_6865);
				}
				return BgL_new1236z00_6864;
			}
		}

	}



/* &lambda2164 */
	BgL_cfailz00_bglt BGl_z62lambda2164z62zzcgen_copz00(obj_t BgL_envz00_5844,
		obj_t BgL_loc1231z00_5845, obj_t BgL_type1232z00_5846,
		obj_t BgL_proc1233z00_5847, obj_t BgL_msg1234z00_5848,
		obj_t BgL_obj1235z00_5849)
	{
		{	/* Cgen/cop.scm 94 */
			{	/* Cgen/cop.scm 94 */
				BgL_cfailz00_bglt BgL_new1416z00_6870;

				{	/* Cgen/cop.scm 94 */
					BgL_cfailz00_bglt BgL_new1415z00_6871;

					BgL_new1415z00_6871 =
						((BgL_cfailz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cfailz00_bgl))));
					{	/* Cgen/cop.scm 94 */
						long BgL_arg2165z00_6872;

						BgL_arg2165z00_6872 = BGL_CLASS_NUM(BGl_cfailz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1415z00_6871), BgL_arg2165z00_6872);
					}
					BgL_new1416z00_6870 = BgL_new1415z00_6871;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1416z00_6870)))->BgL_locz00) =
					((obj_t) BgL_loc1231z00_5845), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1416z00_6870)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1232z00_5846)),
					BUNSPEC);
				((((BgL_cfailz00_bglt) COBJECT(BgL_new1416z00_6870))->BgL_procz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_proc1233z00_5847)),
					BUNSPEC);
				((((BgL_cfailz00_bglt) COBJECT(BgL_new1416z00_6870))->BgL_msgz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_msg1234z00_5848)), BUNSPEC);
				((((BgL_cfailz00_bglt) COBJECT(BgL_new1416z00_6870))->BgL_objz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_obj1235z00_5849)), BUNSPEC);
				return BgL_new1416z00_6870;
			}
		}

	}



/* &lambda2183 */
	obj_t BGl_z62lambda2183z62zzcgen_copz00(obj_t BgL_envz00_5850,
		obj_t BgL_oz00_5851, obj_t BgL_vz00_5852)
	{
		{	/* Cgen/cop.scm 94 */
			return
				((((BgL_cfailz00_bglt) COBJECT(
							((BgL_cfailz00_bglt) BgL_oz00_5851)))->BgL_objz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5852)), BUNSPEC);
		}

	}



/* &lambda2182 */
	BgL_copz00_bglt BGl_z62lambda2182z62zzcgen_copz00(obj_t BgL_envz00_5853,
		obj_t BgL_oz00_5854)
	{
		{	/* Cgen/cop.scm 94 */
			return
				(((BgL_cfailz00_bglt) COBJECT(
						((BgL_cfailz00_bglt) BgL_oz00_5854)))->BgL_objz00);
		}

	}



/* &lambda2178 */
	obj_t BGl_z62lambda2178z62zzcgen_copz00(obj_t BgL_envz00_5855,
		obj_t BgL_oz00_5856, obj_t BgL_vz00_5857)
	{
		{	/* Cgen/cop.scm 94 */
			return
				((((BgL_cfailz00_bglt) COBJECT(
							((BgL_cfailz00_bglt) BgL_oz00_5856)))->BgL_msgz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5857)), BUNSPEC);
		}

	}



/* &lambda2177 */
	BgL_copz00_bglt BGl_z62lambda2177z62zzcgen_copz00(obj_t BgL_envz00_5858,
		obj_t BgL_oz00_5859)
	{
		{	/* Cgen/cop.scm 94 */
			return
				(((BgL_cfailz00_bglt) COBJECT(
						((BgL_cfailz00_bglt) BgL_oz00_5859)))->BgL_msgz00);
		}

	}



/* &lambda2173 */
	obj_t BGl_z62lambda2173z62zzcgen_copz00(obj_t BgL_envz00_5860,
		obj_t BgL_oz00_5861, obj_t BgL_vz00_5862)
	{
		{	/* Cgen/cop.scm 94 */
			return
				((((BgL_cfailz00_bglt) COBJECT(
							((BgL_cfailz00_bglt) BgL_oz00_5861)))->BgL_procz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5862)), BUNSPEC);
		}

	}



/* &lambda2172 */
	BgL_copz00_bglt BGl_z62lambda2172z62zzcgen_copz00(obj_t BgL_envz00_5863,
		obj_t BgL_oz00_5864)
	{
		{	/* Cgen/cop.scm 94 */
			return
				(((BgL_cfailz00_bglt) COBJECT(
						((BgL_cfailz00_bglt) BgL_oz00_5864)))->BgL_procz00);
		}

	}



/* &<@anonymous:2142> */
	obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzcgen_copz00(obj_t BgL_envz00_5865,
		obj_t BgL_new1229z00_5866)
	{
		{	/* Cgen/cop.scm 89 */
			{
				BgL_cappz00_bglt BgL_auxz00_10220;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cappz00_bglt) BgL_new1229z00_5866))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10224;

					{	/* Cgen/cop.scm 89 */
						obj_t BgL_classz00_6883;

						BgL_classz00_6883 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 89 */
							obj_t BgL__ortest_1117z00_6884;

							BgL__ortest_1117z00_6884 = BGL_CLASS_NIL(BgL_classz00_6883);
							if (CBOOL(BgL__ortest_1117z00_6884))
								{	/* Cgen/cop.scm 89 */
									BgL_auxz00_10224 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6884);
								}
							else
								{	/* Cgen/cop.scm 89 */
									BgL_auxz00_10224 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6883));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cappz00_bglt) BgL_new1229z00_5866))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10224), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10234;

					{	/* Cgen/cop.scm 89 */
						obj_t BgL_classz00_6885;

						BgL_classz00_6885 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 89 */
							obj_t BgL__ortest_1117z00_6886;

							BgL__ortest_1117z00_6886 = BGL_CLASS_NIL(BgL_classz00_6885);
							if (CBOOL(BgL__ortest_1117z00_6886))
								{	/* Cgen/cop.scm 89 */
									BgL_auxz00_10234 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6886);
								}
							else
								{	/* Cgen/cop.scm 89 */
									BgL_auxz00_10234 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6885));
								}
						}
					}
					((((BgL_cappz00_bglt) COBJECT(
									((BgL_cappz00_bglt) BgL_new1229z00_5866)))->BgL_funz00) =
						((BgL_copz00_bglt) BgL_auxz00_10234), BUNSPEC);
				}
				((((BgL_cappz00_bglt) COBJECT(
								((BgL_cappz00_bglt) BgL_new1229z00_5866)))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_cappz00_bglt) COBJECT(((BgL_cappz00_bglt)
									BgL_new1229z00_5866)))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_10220 = ((BgL_cappz00_bglt) BgL_new1229z00_5866);
				return ((obj_t) BgL_auxz00_10220);
			}
		}

	}



/* &lambda2140 */
	BgL_cappz00_bglt BGl_z62lambda2140z62zzcgen_copz00(obj_t BgL_envz00_5867)
	{
		{	/* Cgen/cop.scm 89 */
			{	/* Cgen/cop.scm 89 */
				BgL_cappz00_bglt BgL_new1228z00_6887;

				BgL_new1228z00_6887 =
					((BgL_cappz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cappz00_bgl))));
				{	/* Cgen/cop.scm 89 */
					long BgL_arg2141z00_6888;

					BgL_arg2141z00_6888 = BGL_CLASS_NUM(BGl_cappz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1228z00_6887), BgL_arg2141z00_6888);
				}
				return BgL_new1228z00_6887;
			}
		}

	}



/* &lambda2138 */
	BgL_cappz00_bglt BGl_z62lambda2138z62zzcgen_copz00(obj_t BgL_envz00_5868,
		obj_t BgL_loc1223z00_5869, obj_t BgL_type1224z00_5870,
		obj_t BgL_fun1225z00_5871, obj_t BgL_args1226z00_5872,
		obj_t BgL_stackable1227z00_5873)
	{
		{	/* Cgen/cop.scm 89 */
			{	/* Cgen/cop.scm 89 */
				BgL_cappz00_bglt BgL_new1414z00_6891;

				{	/* Cgen/cop.scm 89 */
					BgL_cappz00_bglt BgL_new1413z00_6892;

					BgL_new1413z00_6892 =
						((BgL_cappz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cappz00_bgl))));
					{	/* Cgen/cop.scm 89 */
						long BgL_arg2139z00_6893;

						BgL_arg2139z00_6893 = BGL_CLASS_NUM(BGl_cappz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1413z00_6892), BgL_arg2139z00_6893);
					}
					BgL_new1414z00_6891 = BgL_new1413z00_6892;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1414z00_6891)))->BgL_locz00) =
					((obj_t) BgL_loc1223z00_5869), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1414z00_6891)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1224z00_5870)),
					BUNSPEC);
				((((BgL_cappz00_bglt) COBJECT(BgL_new1414z00_6891))->BgL_funz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_fun1225z00_5871)), BUNSPEC);
				((((BgL_cappz00_bglt) COBJECT(BgL_new1414z00_6891))->BgL_argsz00) =
					((obj_t) BgL_args1226z00_5872), BUNSPEC);
				((((BgL_cappz00_bglt) COBJECT(BgL_new1414z00_6891))->BgL_stackablez00) =
					((obj_t) BgL_stackable1227z00_5873), BUNSPEC);
				return BgL_new1414z00_6891;
			}
		}

	}



/* &lambda2158 */
	obj_t BGl_z62lambda2158z62zzcgen_copz00(obj_t BgL_envz00_5874,
		obj_t BgL_oz00_5875, obj_t BgL_vz00_5876)
	{
		{	/* Cgen/cop.scm 89 */
			return
				((((BgL_cappz00_bglt) COBJECT(
							((BgL_cappz00_bglt) BgL_oz00_5875)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_5876), BUNSPEC);
		}

	}



/* &lambda2157 */
	obj_t BGl_z62lambda2157z62zzcgen_copz00(obj_t BgL_envz00_5877,
		obj_t BgL_oz00_5878)
	{
		{	/* Cgen/cop.scm 89 */
			return
				(((BgL_cappz00_bglt) COBJECT(
						((BgL_cappz00_bglt) BgL_oz00_5878)))->BgL_stackablez00);
		}

	}



/* &lambda2152 */
	obj_t BGl_z62lambda2152z62zzcgen_copz00(obj_t BgL_envz00_5879,
		obj_t BgL_oz00_5880, obj_t BgL_vz00_5881)
	{
		{	/* Cgen/cop.scm 89 */
			return
				((((BgL_cappz00_bglt) COBJECT(
							((BgL_cappz00_bglt) BgL_oz00_5880)))->BgL_argsz00) =
				((obj_t) BgL_vz00_5881), BUNSPEC);
		}

	}



/* &lambda2151 */
	obj_t BGl_z62lambda2151z62zzcgen_copz00(obj_t BgL_envz00_5882,
		obj_t BgL_oz00_5883)
	{
		{	/* Cgen/cop.scm 89 */
			return
				(((BgL_cappz00_bglt) COBJECT(
						((BgL_cappz00_bglt) BgL_oz00_5883)))->BgL_argsz00);
		}

	}



/* &lambda2147 */
	obj_t BGl_z62lambda2147z62zzcgen_copz00(obj_t BgL_envz00_5884,
		obj_t BgL_oz00_5885, obj_t BgL_vz00_5886)
	{
		{	/* Cgen/cop.scm 89 */
			return
				((((BgL_cappz00_bglt) COBJECT(
							((BgL_cappz00_bglt) BgL_oz00_5885)))->BgL_funz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5886)), BUNSPEC);
		}

	}



/* &lambda2146 */
	BgL_copz00_bglt BGl_z62lambda2146z62zzcgen_copz00(obj_t BgL_envz00_5887,
		obj_t BgL_oz00_5888)
	{
		{	/* Cgen/cop.scm 89 */
			return
				(((BgL_cappz00_bglt) COBJECT(
						((BgL_cappz00_bglt) BgL_oz00_5888)))->BgL_funz00);
		}

	}



/* &<@anonymous:2121> */
	obj_t BGl_z62zc3z04anonymousza32121ze3ze5zzcgen_copz00(obj_t BgL_envz00_5889,
		obj_t BgL_new1221z00_5890)
	{
		{	/* Cgen/cop.scm 85 */
			{
				BgL_capplyz00_bglt BgL_auxz00_10279;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_capplyz00_bglt) BgL_new1221z00_5890))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10283;

					{	/* Cgen/cop.scm 85 */
						obj_t BgL_classz00_6902;

						BgL_classz00_6902 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 85 */
							obj_t BgL__ortest_1117z00_6903;

							BgL__ortest_1117z00_6903 = BGL_CLASS_NIL(BgL_classz00_6902);
							if (CBOOL(BgL__ortest_1117z00_6903))
								{	/* Cgen/cop.scm 85 */
									BgL_auxz00_10283 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6903);
								}
							else
								{	/* Cgen/cop.scm 85 */
									BgL_auxz00_10283 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6902));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_capplyz00_bglt) BgL_new1221z00_5890))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10283), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10293;

					{	/* Cgen/cop.scm 85 */
						obj_t BgL_classz00_6904;

						BgL_classz00_6904 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 85 */
							obj_t BgL__ortest_1117z00_6905;

							BgL__ortest_1117z00_6905 = BGL_CLASS_NIL(BgL_classz00_6904);
							if (CBOOL(BgL__ortest_1117z00_6905))
								{	/* Cgen/cop.scm 85 */
									BgL_auxz00_10293 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6905);
								}
							else
								{	/* Cgen/cop.scm 85 */
									BgL_auxz00_10293 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6904));
								}
						}
					}
					((((BgL_capplyz00_bglt) COBJECT(
									((BgL_capplyz00_bglt) BgL_new1221z00_5890)))->BgL_funz00) =
						((BgL_copz00_bglt) BgL_auxz00_10293), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10302;

					{	/* Cgen/cop.scm 85 */
						obj_t BgL_classz00_6906;

						BgL_classz00_6906 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 85 */
							obj_t BgL__ortest_1117z00_6907;

							BgL__ortest_1117z00_6907 = BGL_CLASS_NIL(BgL_classz00_6906);
							if (CBOOL(BgL__ortest_1117z00_6907))
								{	/* Cgen/cop.scm 85 */
									BgL_auxz00_10302 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6907);
								}
							else
								{	/* Cgen/cop.scm 85 */
									BgL_auxz00_10302 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6906));
								}
						}
					}
					((((BgL_capplyz00_bglt) COBJECT(
									((BgL_capplyz00_bglt) BgL_new1221z00_5890)))->BgL_argz00) =
						((BgL_copz00_bglt) BgL_auxz00_10302), BUNSPEC);
				}
				BgL_auxz00_10279 = ((BgL_capplyz00_bglt) BgL_new1221z00_5890);
				return ((obj_t) BgL_auxz00_10279);
			}
		}

	}



/* &lambda2119 */
	BgL_capplyz00_bglt BGl_z62lambda2119z62zzcgen_copz00(obj_t BgL_envz00_5891)
	{
		{	/* Cgen/cop.scm 85 */
			{	/* Cgen/cop.scm 85 */
				BgL_capplyz00_bglt BgL_new1220z00_6908;

				BgL_new1220z00_6908 =
					((BgL_capplyz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_capplyz00_bgl))));
				{	/* Cgen/cop.scm 85 */
					long BgL_arg2120z00_6909;

					BgL_arg2120z00_6909 = BGL_CLASS_NUM(BGl_capplyz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1220z00_6908), BgL_arg2120z00_6909);
				}
				return BgL_new1220z00_6908;
			}
		}

	}



/* &lambda2117 */
	BgL_capplyz00_bglt BGl_z62lambda2117z62zzcgen_copz00(obj_t BgL_envz00_5892,
		obj_t BgL_loc1216z00_5893, obj_t BgL_type1217z00_5894,
		obj_t BgL_fun1218z00_5895, obj_t BgL_arg1219z00_5896)
	{
		{	/* Cgen/cop.scm 85 */
			{	/* Cgen/cop.scm 85 */
				BgL_capplyz00_bglt BgL_new1412z00_6913;

				{	/* Cgen/cop.scm 85 */
					BgL_capplyz00_bglt BgL_new1411z00_6914;

					BgL_new1411z00_6914 =
						((BgL_capplyz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_capplyz00_bgl))));
					{	/* Cgen/cop.scm 85 */
						long BgL_arg2118z00_6915;

						BgL_arg2118z00_6915 = BGL_CLASS_NUM(BGl_capplyz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1411z00_6914), BgL_arg2118z00_6915);
					}
					BgL_new1412z00_6913 = BgL_new1411z00_6914;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1412z00_6913)))->BgL_locz00) =
					((obj_t) BgL_loc1216z00_5893), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1412z00_6913)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1217z00_5894)),
					BUNSPEC);
				((((BgL_capplyz00_bglt) COBJECT(BgL_new1412z00_6913))->BgL_funz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_fun1218z00_5895)), BUNSPEC);
				((((BgL_capplyz00_bglt) COBJECT(BgL_new1412z00_6913))->BgL_argz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_arg1219z00_5896)), BUNSPEC);
				return BgL_new1412z00_6913;
			}
		}

	}



/* &lambda2132 */
	obj_t BGl_z62lambda2132z62zzcgen_copz00(obj_t BgL_envz00_5897,
		obj_t BgL_oz00_5898, obj_t BgL_vz00_5899)
	{
		{	/* Cgen/cop.scm 85 */
			return
				((((BgL_capplyz00_bglt) COBJECT(
							((BgL_capplyz00_bglt) BgL_oz00_5898)))->BgL_argz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5899)), BUNSPEC);
		}

	}



/* &lambda2131 */
	BgL_copz00_bglt BGl_z62lambda2131z62zzcgen_copz00(obj_t BgL_envz00_5900,
		obj_t BgL_oz00_5901)
	{
		{	/* Cgen/cop.scm 85 */
			return
				(((BgL_capplyz00_bglt) COBJECT(
						((BgL_capplyz00_bglt) BgL_oz00_5901)))->BgL_argz00);
		}

	}



/* &lambda2126 */
	obj_t BGl_z62lambda2126z62zzcgen_copz00(obj_t BgL_envz00_5902,
		obj_t BgL_oz00_5903, obj_t BgL_vz00_5904)
	{
		{	/* Cgen/cop.scm 85 */
			return
				((((BgL_capplyz00_bglt) COBJECT(
							((BgL_capplyz00_bglt) BgL_oz00_5903)))->BgL_funz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5904)), BUNSPEC);
		}

	}



/* &lambda2125 */
	BgL_copz00_bglt BGl_z62lambda2125z62zzcgen_copz00(obj_t BgL_envz00_5905,
		obj_t BgL_oz00_5906)
	{
		{	/* Cgen/cop.scm 85 */
			return
				(((BgL_capplyz00_bglt) COBJECT(
						((BgL_capplyz00_bglt) BgL_oz00_5906)))->BgL_funz00);
		}

	}



/* &<@anonymous:2096> */
	obj_t BGl_z62zc3z04anonymousza32096ze3ze5zzcgen_copz00(obj_t BgL_envz00_5907,
		obj_t BgL_new1214z00_5908)
	{
		{	/* Cgen/cop.scm 80 */
			{
				BgL_cfuncallz00_bglt BgL_auxz00_10340;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cfuncallz00_bglt) BgL_new1214z00_5908))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10344;

					{	/* Cgen/cop.scm 80 */
						obj_t BgL_classz00_6923;

						BgL_classz00_6923 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 80 */
							obj_t BgL__ortest_1117z00_6924;

							BgL__ortest_1117z00_6924 = BGL_CLASS_NIL(BgL_classz00_6923);
							if (CBOOL(BgL__ortest_1117z00_6924))
								{	/* Cgen/cop.scm 80 */
									BgL_auxz00_10344 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6924);
								}
							else
								{	/* Cgen/cop.scm 80 */
									BgL_auxz00_10344 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6923));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cfuncallz00_bglt) BgL_new1214z00_5908))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10344), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10354;

					{	/* Cgen/cop.scm 80 */
						obj_t BgL_classz00_6925;

						BgL_classz00_6925 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 80 */
							obj_t BgL__ortest_1117z00_6926;

							BgL__ortest_1117z00_6926 = BGL_CLASS_NIL(BgL_classz00_6925);
							if (CBOOL(BgL__ortest_1117z00_6926))
								{	/* Cgen/cop.scm 80 */
									BgL_auxz00_10354 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6926);
								}
							else
								{	/* Cgen/cop.scm 80 */
									BgL_auxz00_10354 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6925));
								}
						}
					}
					((((BgL_cfuncallz00_bglt) COBJECT(
									((BgL_cfuncallz00_bglt) BgL_new1214z00_5908)))->BgL_funz00) =
						((BgL_copz00_bglt) BgL_auxz00_10354), BUNSPEC);
				}
				((((BgL_cfuncallz00_bglt) COBJECT(
								((BgL_cfuncallz00_bglt) BgL_new1214z00_5908)))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_cfuncallz00_bglt) COBJECT(((BgL_cfuncallz00_bglt)
									BgL_new1214z00_5908)))->BgL_strengthz00) =
					((obj_t) CNST_TABLE_REF(61)), BUNSPEC);
				BgL_auxz00_10340 = ((BgL_cfuncallz00_bglt) BgL_new1214z00_5908);
				return ((obj_t) BgL_auxz00_10340);
			}
		}

	}



/* &lambda2094 */
	BgL_cfuncallz00_bglt BGl_z62lambda2094z62zzcgen_copz00(obj_t BgL_envz00_5909)
	{
		{	/* Cgen/cop.scm 80 */
			{	/* Cgen/cop.scm 80 */
				BgL_cfuncallz00_bglt BgL_new1213z00_6927;

				BgL_new1213z00_6927 =
					((BgL_cfuncallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cfuncallz00_bgl))));
				{	/* Cgen/cop.scm 80 */
					long BgL_arg2095z00_6928;

					BgL_arg2095z00_6928 = BGL_CLASS_NUM(BGl_cfuncallz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1213z00_6927), BgL_arg2095z00_6928);
				}
				return BgL_new1213z00_6927;
			}
		}

	}



/* &lambda2091 */
	BgL_cfuncallz00_bglt BGl_z62lambda2091z62zzcgen_copz00(obj_t BgL_envz00_5910,
		obj_t BgL_loc1208z00_5911, obj_t BgL_type1209z00_5912,
		obj_t BgL_fun1210z00_5913, obj_t BgL_args1211z00_5914,
		obj_t BgL_strength1212z00_5915)
	{
		{	/* Cgen/cop.scm 80 */
			{	/* Cgen/cop.scm 80 */
				BgL_cfuncallz00_bglt BgL_new1410z00_6932;

				{	/* Cgen/cop.scm 80 */
					BgL_cfuncallz00_bglt BgL_new1409z00_6933;

					BgL_new1409z00_6933 =
						((BgL_cfuncallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cfuncallz00_bgl))));
					{	/* Cgen/cop.scm 80 */
						long BgL_arg2093z00_6934;

						BgL_arg2093z00_6934 = BGL_CLASS_NUM(BGl_cfuncallz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1409z00_6933), BgL_arg2093z00_6934);
					}
					BgL_new1410z00_6932 = BgL_new1409z00_6933;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1410z00_6932)))->BgL_locz00) =
					((obj_t) BgL_loc1208z00_5911), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1410z00_6932)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1209z00_5912)),
					BUNSPEC);
				((((BgL_cfuncallz00_bglt) COBJECT(BgL_new1410z00_6932))->BgL_funz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_fun1210z00_5913)), BUNSPEC);
				((((BgL_cfuncallz00_bglt) COBJECT(BgL_new1410z00_6932))->BgL_argsz00) =
					((obj_t) BgL_args1211z00_5914), BUNSPEC);
				((((BgL_cfuncallz00_bglt) COBJECT(BgL_new1410z00_6932))->
						BgL_strengthz00) =
					((obj_t) ((obj_t) BgL_strength1212z00_5915)), BUNSPEC);
				return BgL_new1410z00_6932;
			}
		}

	}



/* &lambda2111 */
	obj_t BGl_z62lambda2111z62zzcgen_copz00(obj_t BgL_envz00_5916,
		obj_t BgL_oz00_5917, obj_t BgL_vz00_5918)
	{
		{	/* Cgen/cop.scm 80 */
			return
				((((BgL_cfuncallz00_bglt) COBJECT(
							((BgL_cfuncallz00_bglt) BgL_oz00_5917)))->BgL_strengthz00) =
				((obj_t) ((obj_t) BgL_vz00_5918)), BUNSPEC);
		}

	}



/* &lambda2110 */
	obj_t BGl_z62lambda2110z62zzcgen_copz00(obj_t BgL_envz00_5919,
		obj_t BgL_oz00_5920)
	{
		{	/* Cgen/cop.scm 80 */
			return
				(((BgL_cfuncallz00_bglt) COBJECT(
						((BgL_cfuncallz00_bglt) BgL_oz00_5920)))->BgL_strengthz00);
		}

	}



/* &lambda2106 */
	obj_t BGl_z62lambda2106z62zzcgen_copz00(obj_t BgL_envz00_5921,
		obj_t BgL_oz00_5922, obj_t BgL_vz00_5923)
	{
		{	/* Cgen/cop.scm 80 */
			return
				((((BgL_cfuncallz00_bglt) COBJECT(
							((BgL_cfuncallz00_bglt) BgL_oz00_5922)))->BgL_argsz00) =
				((obj_t) BgL_vz00_5923), BUNSPEC);
		}

	}



/* &lambda2105 */
	obj_t BGl_z62lambda2105z62zzcgen_copz00(obj_t BgL_envz00_5924,
		obj_t BgL_oz00_5925)
	{
		{	/* Cgen/cop.scm 80 */
			return
				(((BgL_cfuncallz00_bglt) COBJECT(
						((BgL_cfuncallz00_bglt) BgL_oz00_5925)))->BgL_argsz00);
		}

	}



/* &lambda2101 */
	obj_t BGl_z62lambda2101z62zzcgen_copz00(obj_t BgL_envz00_5926,
		obj_t BgL_oz00_5927, obj_t BgL_vz00_5928)
	{
		{	/* Cgen/cop.scm 80 */
			return
				((((BgL_cfuncallz00_bglt) COBJECT(
							((BgL_cfuncallz00_bglt) BgL_oz00_5927)))->BgL_funz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5928)), BUNSPEC);
		}

	}



/* &lambda2100 */
	BgL_copz00_bglt BGl_z62lambda2100z62zzcgen_copz00(obj_t BgL_envz00_5929,
		obj_t BgL_oz00_5930)
	{
		{	/* Cgen/cop.scm 80 */
			return
				(((BgL_cfuncallz00_bglt) COBJECT(
						((BgL_cfuncallz00_bglt) BgL_oz00_5930)))->BgL_funz00);
		}

	}



/* &<@anonymous:2079> */
	obj_t BGl_z62zc3z04anonymousza32079ze3ze5zzcgen_copz00(obj_t BgL_envz00_5931,
		obj_t BgL_new1206z00_5932)
	{
		{	/* Cgen/cop.scm 77 */
			{
				BgL_localzd2varzd2_bglt BgL_auxz00_10402;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_localzd2varzd2_bglt) BgL_new1206z00_5932))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10406;

					{	/* Cgen/cop.scm 77 */
						obj_t BgL_classz00_6944;

						BgL_classz00_6944 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 77 */
							obj_t BgL__ortest_1117z00_6945;

							BgL__ortest_1117z00_6945 = BGL_CLASS_NIL(BgL_classz00_6944);
							if (CBOOL(BgL__ortest_1117z00_6945))
								{	/* Cgen/cop.scm 77 */
									BgL_auxz00_10406 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6945);
								}
							else
								{	/* Cgen/cop.scm 77 */
									BgL_auxz00_10406 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6944));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_localzd2varzd2_bglt) BgL_new1206z00_5932))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10406), BUNSPEC);
				}
				((((BgL_localzd2varzd2_bglt) COBJECT(
								((BgL_localzd2varzd2_bglt) BgL_new1206z00_5932)))->
						BgL_varsz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_10402 = ((BgL_localzd2varzd2_bglt) BgL_new1206z00_5932);
				return ((obj_t) BgL_auxz00_10402);
			}
		}

	}



/* &lambda2077 */
	BgL_localzd2varzd2_bglt BGl_z62lambda2077z62zzcgen_copz00(obj_t
		BgL_envz00_5933)
	{
		{	/* Cgen/cop.scm 77 */
			{	/* Cgen/cop.scm 77 */
				BgL_localzd2varzd2_bglt BgL_new1205z00_6946;

				BgL_new1205z00_6946 =
					((BgL_localzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzd2varzd2_bgl))));
				{	/* Cgen/cop.scm 77 */
					long BgL_arg2078z00_6947;

					BgL_arg2078z00_6947 = BGL_CLASS_NUM(BGl_localzd2varzd2zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1205z00_6946), BgL_arg2078z00_6947);
				}
				return BgL_new1205z00_6946;
			}
		}

	}



/* &lambda2075 */
	BgL_localzd2varzd2_bglt BGl_z62lambda2075z62zzcgen_copz00(obj_t
		BgL_envz00_5934, obj_t BgL_loc1202z00_5935, obj_t BgL_type1203z00_5936,
		obj_t BgL_vars1204z00_5937)
	{
		{	/* Cgen/cop.scm 77 */
			{	/* Cgen/cop.scm 77 */
				BgL_localzd2varzd2_bglt BgL_new1408z00_6949;

				{	/* Cgen/cop.scm 77 */
					BgL_localzd2varzd2_bglt BgL_new1407z00_6950;

					BgL_new1407z00_6950 =
						((BgL_localzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_localzd2varzd2_bgl))));
					{	/* Cgen/cop.scm 77 */
						long BgL_arg2076z00_6951;

						BgL_arg2076z00_6951 =
							BGL_CLASS_NUM(BGl_localzd2varzd2zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1407z00_6950), BgL_arg2076z00_6951);
					}
					BgL_new1408z00_6949 = BgL_new1407z00_6950;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1408z00_6949)))->BgL_locz00) =
					((obj_t) BgL_loc1202z00_5935), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1408z00_6949)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1203z00_5936)),
					BUNSPEC);
				((((BgL_localzd2varzd2_bglt) COBJECT(BgL_new1408z00_6949))->
						BgL_varsz00) = ((obj_t) BgL_vars1204z00_5937), BUNSPEC);
				return BgL_new1408z00_6949;
			}
		}

	}



/* &lambda2084 */
	obj_t BGl_z62lambda2084z62zzcgen_copz00(obj_t BgL_envz00_5938,
		obj_t BgL_oz00_5939, obj_t BgL_vz00_5940)
	{
		{	/* Cgen/cop.scm 77 */
			return
				((((BgL_localzd2varzd2_bglt) COBJECT(
							((BgL_localzd2varzd2_bglt) BgL_oz00_5939)))->BgL_varsz00) =
				((obj_t) BgL_vz00_5940), BUNSPEC);
		}

	}



/* &lambda2083 */
	obj_t BGl_z62lambda2083z62zzcgen_copz00(obj_t BgL_envz00_5941,
		obj_t BgL_oz00_5942)
	{
		{	/* Cgen/cop.scm 77 */
			return
				(((BgL_localzd2varzd2_bglt) COBJECT(
						((BgL_localzd2varzd2_bglt) BgL_oz00_5942)))->BgL_varsz00);
		}

	}



/* &<@anonymous:2050> */
	obj_t BGl_z62zc3z04anonymousza32050ze3ze5zzcgen_copz00(obj_t BgL_envz00_5943,
		obj_t BgL_new1200z00_5944)
	{
		{	/* Cgen/cop.scm 72 */
			{
				BgL_cifz00_bglt BgL_auxz00_10438;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cifz00_bglt) BgL_new1200z00_5944))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10442;

					{	/* Cgen/cop.scm 72 */
						obj_t BgL_classz00_6955;

						BgL_classz00_6955 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 72 */
							obj_t BgL__ortest_1117z00_6956;

							BgL__ortest_1117z00_6956 = BGL_CLASS_NIL(BgL_classz00_6955);
							if (CBOOL(BgL__ortest_1117z00_6956))
								{	/* Cgen/cop.scm 72 */
									BgL_auxz00_10442 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6956);
								}
							else
								{	/* Cgen/cop.scm 72 */
									BgL_auxz00_10442 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6955));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cifz00_bglt) BgL_new1200z00_5944))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10442), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10452;

					{	/* Cgen/cop.scm 72 */
						obj_t BgL_classz00_6957;

						BgL_classz00_6957 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 72 */
							obj_t BgL__ortest_1117z00_6958;

							BgL__ortest_1117z00_6958 = BGL_CLASS_NIL(BgL_classz00_6957);
							if (CBOOL(BgL__ortest_1117z00_6958))
								{	/* Cgen/cop.scm 72 */
									BgL_auxz00_10452 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6958);
								}
							else
								{	/* Cgen/cop.scm 72 */
									BgL_auxz00_10452 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6957));
								}
						}
					}
					((((BgL_cifz00_bglt) COBJECT(
									((BgL_cifz00_bglt) BgL_new1200z00_5944)))->BgL_testz00) =
						((BgL_copz00_bglt) BgL_auxz00_10452), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10461;

					{	/* Cgen/cop.scm 72 */
						obj_t BgL_classz00_6959;

						BgL_classz00_6959 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 72 */
							obj_t BgL__ortest_1117z00_6960;

							BgL__ortest_1117z00_6960 = BGL_CLASS_NIL(BgL_classz00_6959);
							if (CBOOL(BgL__ortest_1117z00_6960))
								{	/* Cgen/cop.scm 72 */
									BgL_auxz00_10461 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6960);
								}
							else
								{	/* Cgen/cop.scm 72 */
									BgL_auxz00_10461 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6959));
								}
						}
					}
					((((BgL_cifz00_bglt) COBJECT(
									((BgL_cifz00_bglt) BgL_new1200z00_5944)))->BgL_truez00) =
						((BgL_copz00_bglt) BgL_auxz00_10461), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10470;

					{	/* Cgen/cop.scm 72 */
						obj_t BgL_classz00_6961;

						BgL_classz00_6961 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 72 */
							obj_t BgL__ortest_1117z00_6962;

							BgL__ortest_1117z00_6962 = BGL_CLASS_NIL(BgL_classz00_6961);
							if (CBOOL(BgL__ortest_1117z00_6962))
								{	/* Cgen/cop.scm 72 */
									BgL_auxz00_10470 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6962);
								}
							else
								{	/* Cgen/cop.scm 72 */
									BgL_auxz00_10470 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6961));
								}
						}
					}
					((((BgL_cifz00_bglt) COBJECT(
									((BgL_cifz00_bglt) BgL_new1200z00_5944)))->BgL_falsez00) =
						((BgL_copz00_bglt) BgL_auxz00_10470), BUNSPEC);
				}
				BgL_auxz00_10438 = ((BgL_cifz00_bglt) BgL_new1200z00_5944);
				return ((obj_t) BgL_auxz00_10438);
			}
		}

	}



/* &lambda2048 */
	BgL_cifz00_bglt BGl_z62lambda2048z62zzcgen_copz00(obj_t BgL_envz00_5945)
	{
		{	/* Cgen/cop.scm 72 */
			{	/* Cgen/cop.scm 72 */
				BgL_cifz00_bglt BgL_new1198z00_6963;

				BgL_new1198z00_6963 =
					((BgL_cifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_cifz00_bgl))));
				{	/* Cgen/cop.scm 72 */
					long BgL_arg2049z00_6964;

					BgL_arg2049z00_6964 = BGL_CLASS_NUM(BGl_cifz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1198z00_6963), BgL_arg2049z00_6964);
				}
				return BgL_new1198z00_6963;
			}
		}

	}



/* &lambda2046 */
	BgL_cifz00_bglt BGl_z62lambda2046z62zzcgen_copz00(obj_t BgL_envz00_5946,
		obj_t BgL_loc1193z00_5947, obj_t BgL_type1194z00_5948,
		obj_t BgL_test1195z00_5949, obj_t BgL_true1196z00_5950,
		obj_t BgL_false1197z00_5951)
	{
		{	/* Cgen/cop.scm 72 */
			{	/* Cgen/cop.scm 72 */
				BgL_cifz00_bglt BgL_new1406z00_6969;

				{	/* Cgen/cop.scm 72 */
					BgL_cifz00_bglt BgL_new1405z00_6970;

					BgL_new1405z00_6970 =
						((BgL_cifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cifz00_bgl))));
					{	/* Cgen/cop.scm 72 */
						long BgL_arg2047z00_6971;

						BgL_arg2047z00_6971 = BGL_CLASS_NUM(BGl_cifz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1405z00_6970), BgL_arg2047z00_6971);
					}
					BgL_new1406z00_6969 = BgL_new1405z00_6970;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1406z00_6969)))->BgL_locz00) =
					((obj_t) BgL_loc1193z00_5947), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1406z00_6969)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1194z00_5948)),
					BUNSPEC);
				((((BgL_cifz00_bglt) COBJECT(BgL_new1406z00_6969))->BgL_testz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_test1195z00_5949)),
					BUNSPEC);
				((((BgL_cifz00_bglt) COBJECT(BgL_new1406z00_6969))->BgL_truez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_true1196z00_5950)),
					BUNSPEC);
				((((BgL_cifz00_bglt) COBJECT(BgL_new1406z00_6969))->BgL_falsez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_false1197z00_5951)),
					BUNSPEC);
				return BgL_new1406z00_6969;
			}
		}

	}



/* &lambda2067 */
	obj_t BGl_z62lambda2067z62zzcgen_copz00(obj_t BgL_envz00_5952,
		obj_t BgL_oz00_5953, obj_t BgL_vz00_5954)
	{
		{	/* Cgen/cop.scm 72 */
			return
				((((BgL_cifz00_bglt) COBJECT(
							((BgL_cifz00_bglt) BgL_oz00_5953)))->BgL_falsez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5954)), BUNSPEC);
		}

	}



/* &lambda2066 */
	BgL_copz00_bglt BGl_z62lambda2066z62zzcgen_copz00(obj_t BgL_envz00_5955,
		obj_t BgL_oz00_5956)
	{
		{	/* Cgen/cop.scm 72 */
			return
				(((BgL_cifz00_bglt) COBJECT(
						((BgL_cifz00_bglt) BgL_oz00_5956)))->BgL_falsez00);
		}

	}



/* &lambda2062 */
	obj_t BGl_z62lambda2062z62zzcgen_copz00(obj_t BgL_envz00_5957,
		obj_t BgL_oz00_5958, obj_t BgL_vz00_5959)
	{
		{	/* Cgen/cop.scm 72 */
			return
				((((BgL_cifz00_bglt) COBJECT(
							((BgL_cifz00_bglt) BgL_oz00_5958)))->BgL_truez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5959)), BUNSPEC);
		}

	}



/* &lambda2061 */
	BgL_copz00_bglt BGl_z62lambda2061z62zzcgen_copz00(obj_t BgL_envz00_5960,
		obj_t BgL_oz00_5961)
	{
		{	/* Cgen/cop.scm 72 */
			return
				(((BgL_cifz00_bglt) COBJECT(
						((BgL_cifz00_bglt) BgL_oz00_5961)))->BgL_truez00);
		}

	}



/* &lambda2057 */
	obj_t BGl_z62lambda2057z62zzcgen_copz00(obj_t BgL_envz00_5962,
		obj_t BgL_oz00_5963, obj_t BgL_vz00_5964)
	{
		{	/* Cgen/cop.scm 72 */
			return
				((((BgL_cifz00_bglt) COBJECT(
							((BgL_cifz00_bglt) BgL_oz00_5963)))->BgL_testz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5964)), BUNSPEC);
		}

	}



/* &lambda2056 */
	BgL_copz00_bglt BGl_z62lambda2056z62zzcgen_copz00(obj_t BgL_envz00_5965,
		obj_t BgL_oz00_5966)
	{
		{	/* Cgen/cop.scm 72 */
			return
				(((BgL_cifz00_bglt) COBJECT(
						((BgL_cifz00_bglt) BgL_oz00_5966)))->BgL_testz00);
		}

	}



/* &<@anonymous:2027> */
	obj_t BGl_z62zc3z04anonymousza32027ze3ze5zzcgen_copz00(obj_t BgL_envz00_5967,
		obj_t BgL_new1191z00_5968)
	{
		{	/* Cgen/cop.scm 68 */
			{
				BgL_csetqz00_bglt BgL_auxz00_10515;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_csetqz00_bglt) BgL_new1191z00_5968))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10519;

					{	/* Cgen/cop.scm 68 */
						obj_t BgL_classz00_6982;

						BgL_classz00_6982 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 68 */
							obj_t BgL__ortest_1117z00_6983;

							BgL__ortest_1117z00_6983 = BGL_CLASS_NIL(BgL_classz00_6982);
							if (CBOOL(BgL__ortest_1117z00_6983))
								{	/* Cgen/cop.scm 68 */
									BgL_auxz00_10519 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6983);
								}
							else
								{	/* Cgen/cop.scm 68 */
									BgL_auxz00_10519 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6982));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_csetqz00_bglt) BgL_new1191z00_5968))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10519), BUNSPEC);
				}
				{
					BgL_varcz00_bglt BgL_auxz00_10529;

					{	/* Cgen/cop.scm 68 */
						obj_t BgL_classz00_6984;

						BgL_classz00_6984 = BGl_varcz00zzcgen_copz00;
						{	/* Cgen/cop.scm 68 */
							obj_t BgL__ortest_1117z00_6985;

							BgL__ortest_1117z00_6985 = BGL_CLASS_NIL(BgL_classz00_6984);
							if (CBOOL(BgL__ortest_1117z00_6985))
								{	/* Cgen/cop.scm 68 */
									BgL_auxz00_10529 =
										((BgL_varcz00_bglt) BgL__ortest_1117z00_6985);
								}
							else
								{	/* Cgen/cop.scm 68 */
									BgL_auxz00_10529 =
										((BgL_varcz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6984));
								}
						}
					}
					((((BgL_csetqz00_bglt) COBJECT(
									((BgL_csetqz00_bglt) BgL_new1191z00_5968)))->BgL_varz00) =
						((BgL_varcz00_bglt) BgL_auxz00_10529), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10538;

					{	/* Cgen/cop.scm 68 */
						obj_t BgL_classz00_6986;

						BgL_classz00_6986 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 68 */
							obj_t BgL__ortest_1117z00_6987;

							BgL__ortest_1117z00_6987 = BGL_CLASS_NIL(BgL_classz00_6986);
							if (CBOOL(BgL__ortest_1117z00_6987))
								{	/* Cgen/cop.scm 68 */
									BgL_auxz00_10538 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_6987);
								}
							else
								{	/* Cgen/cop.scm 68 */
									BgL_auxz00_10538 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6986));
								}
						}
					}
					((((BgL_csetqz00_bglt) COBJECT(
									((BgL_csetqz00_bglt) BgL_new1191z00_5968)))->BgL_valuez00) =
						((BgL_copz00_bglt) BgL_auxz00_10538), BUNSPEC);
				}
				BgL_auxz00_10515 = ((BgL_csetqz00_bglt) BgL_new1191z00_5968);
				return ((obj_t) BgL_auxz00_10515);
			}
		}

	}



/* &lambda2025 */
	BgL_csetqz00_bglt BGl_z62lambda2025z62zzcgen_copz00(obj_t BgL_envz00_5969)
	{
		{	/* Cgen/cop.scm 68 */
			{	/* Cgen/cop.scm 68 */
				BgL_csetqz00_bglt BgL_new1190z00_6988;

				BgL_new1190z00_6988 =
					((BgL_csetqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_csetqz00_bgl))));
				{	/* Cgen/cop.scm 68 */
					long BgL_arg2026z00_6989;

					BgL_arg2026z00_6989 = BGL_CLASS_NUM(BGl_csetqz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1190z00_6988), BgL_arg2026z00_6989);
				}
				return BgL_new1190z00_6988;
			}
		}

	}



/* &lambda2022 */
	BgL_csetqz00_bglt BGl_z62lambda2022z62zzcgen_copz00(obj_t BgL_envz00_5970,
		obj_t BgL_loc1186z00_5971, obj_t BgL_type1187z00_5972,
		obj_t BgL_var1188z00_5973, obj_t BgL_value1189z00_5974)
	{
		{	/* Cgen/cop.scm 68 */
			{	/* Cgen/cop.scm 68 */
				BgL_csetqz00_bglt BgL_new1404z00_6993;

				{	/* Cgen/cop.scm 68 */
					BgL_csetqz00_bglt BgL_new1403z00_6994;

					BgL_new1403z00_6994 =
						((BgL_csetqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_csetqz00_bgl))));
					{	/* Cgen/cop.scm 68 */
						long BgL_arg2024z00_6995;

						BgL_arg2024z00_6995 = BGL_CLASS_NUM(BGl_csetqz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1403z00_6994), BgL_arg2024z00_6995);
					}
					BgL_new1404z00_6993 = BgL_new1403z00_6994;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1404z00_6993)))->BgL_locz00) =
					((obj_t) BgL_loc1186z00_5971), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1404z00_6993)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1187z00_5972)),
					BUNSPEC);
				((((BgL_csetqz00_bglt) COBJECT(BgL_new1404z00_6993))->BgL_varz00) =
					((BgL_varcz00_bglt) ((BgL_varcz00_bglt) BgL_var1188z00_5973)),
					BUNSPEC);
				((((BgL_csetqz00_bglt) COBJECT(BgL_new1404z00_6993))->BgL_valuez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_value1189z00_5974)),
					BUNSPEC);
				return BgL_new1404z00_6993;
			}
		}

	}



/* &lambda2039 */
	obj_t BGl_z62lambda2039z62zzcgen_copz00(obj_t BgL_envz00_5975,
		obj_t BgL_oz00_5976, obj_t BgL_vz00_5977)
	{
		{	/* Cgen/cop.scm 68 */
			return
				((((BgL_csetqz00_bglt) COBJECT(
							((BgL_csetqz00_bglt) BgL_oz00_5976)))->BgL_valuez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5977)), BUNSPEC);
		}

	}



/* &lambda2038 */
	BgL_copz00_bglt BGl_z62lambda2038z62zzcgen_copz00(obj_t BgL_envz00_5978,
		obj_t BgL_oz00_5979)
	{
		{	/* Cgen/cop.scm 68 */
			return
				(((BgL_csetqz00_bglt) COBJECT(
						((BgL_csetqz00_bglt) BgL_oz00_5979)))->BgL_valuez00);
		}

	}



/* &lambda2033 */
	obj_t BGl_z62lambda2033z62zzcgen_copz00(obj_t BgL_envz00_5980,
		obj_t BgL_oz00_5981, obj_t BgL_vz00_5982)
	{
		{	/* Cgen/cop.scm 68 */
			return
				((((BgL_csetqz00_bglt) COBJECT(
							((BgL_csetqz00_bglt) BgL_oz00_5981)))->BgL_varz00) =
				((BgL_varcz00_bglt) ((BgL_varcz00_bglt) BgL_vz00_5982)), BUNSPEC);
		}

	}



/* &lambda2032 */
	BgL_varcz00_bglt BGl_z62lambda2032z62zzcgen_copz00(obj_t BgL_envz00_5983,
		obj_t BgL_oz00_5984)
	{
		{	/* Cgen/cop.scm 68 */
			return
				(((BgL_csetqz00_bglt) COBJECT(
						((BgL_csetqz00_bglt) BgL_oz00_5984)))->BgL_varz00);
		}

	}



/* &<@anonymous:2011> */
	obj_t BGl_z62zc3z04anonymousza32011ze3ze5zzcgen_copz00(obj_t BgL_envz00_5985,
		obj_t BgL_new1184z00_5986)
	{
		{	/* Cgen/cop.scm 65 */
			{
				BgL_stopz00_bglt BgL_auxz00_10576;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_stopz00_bglt) BgL_new1184z00_5986))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10580;

					{	/* Cgen/cop.scm 65 */
						obj_t BgL_classz00_7003;

						BgL_classz00_7003 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 65 */
							obj_t BgL__ortest_1117z00_7004;

							BgL__ortest_1117z00_7004 = BGL_CLASS_NIL(BgL_classz00_7003);
							if (CBOOL(BgL__ortest_1117z00_7004))
								{	/* Cgen/cop.scm 65 */
									BgL_auxz00_10580 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7004);
								}
							else
								{	/* Cgen/cop.scm 65 */
									BgL_auxz00_10580 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7003));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_stopz00_bglt) BgL_new1184z00_5986))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10580), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10590;

					{	/* Cgen/cop.scm 65 */
						obj_t BgL_classz00_7005;

						BgL_classz00_7005 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 65 */
							obj_t BgL__ortest_1117z00_7006;

							BgL__ortest_1117z00_7006 = BGL_CLASS_NIL(BgL_classz00_7005);
							if (CBOOL(BgL__ortest_1117z00_7006))
								{	/* Cgen/cop.scm 65 */
									BgL_auxz00_10590 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_7006);
								}
							else
								{	/* Cgen/cop.scm 65 */
									BgL_auxz00_10590 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7005));
								}
						}
					}
					((((BgL_stopz00_bglt) COBJECT(
									((BgL_stopz00_bglt) BgL_new1184z00_5986)))->BgL_valuez00) =
						((BgL_copz00_bglt) BgL_auxz00_10590), BUNSPEC);
				}
				BgL_auxz00_10576 = ((BgL_stopz00_bglt) BgL_new1184z00_5986);
				return ((obj_t) BgL_auxz00_10576);
			}
		}

	}



/* &lambda2009 */
	BgL_stopz00_bglt BGl_z62lambda2009z62zzcgen_copz00(obj_t BgL_envz00_5987)
	{
		{	/* Cgen/cop.scm 65 */
			{	/* Cgen/cop.scm 65 */
				BgL_stopz00_bglt BgL_new1183z00_7007;

				BgL_new1183z00_7007 =
					((BgL_stopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_stopz00_bgl))));
				{	/* Cgen/cop.scm 65 */
					long BgL_arg2010z00_7008;

					BgL_arg2010z00_7008 = BGL_CLASS_NUM(BGl_stopz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1183z00_7007), BgL_arg2010z00_7008);
				}
				return BgL_new1183z00_7007;
			}
		}

	}



/* &lambda2007 */
	BgL_stopz00_bglt BGl_z62lambda2007z62zzcgen_copz00(obj_t BgL_envz00_5988,
		obj_t BgL_loc1180z00_5989, obj_t BgL_type1181z00_5990,
		obj_t BgL_value1182z00_5991)
	{
		{	/* Cgen/cop.scm 65 */
			{	/* Cgen/cop.scm 65 */
				BgL_stopz00_bglt BgL_new1402z00_7011;

				{	/* Cgen/cop.scm 65 */
					BgL_stopz00_bglt BgL_new1401z00_7012;

					BgL_new1401z00_7012 =
						((BgL_stopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_stopz00_bgl))));
					{	/* Cgen/cop.scm 65 */
						long BgL_arg2008z00_7013;

						BgL_arg2008z00_7013 = BGL_CLASS_NUM(BGl_stopz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1401z00_7012), BgL_arg2008z00_7013);
					}
					BgL_new1402z00_7011 = BgL_new1401z00_7012;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1402z00_7011)))->BgL_locz00) =
					((obj_t) BgL_loc1180z00_5989), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1402z00_7011)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1181z00_5990)),
					BUNSPEC);
				((((BgL_stopz00_bglt) COBJECT(BgL_new1402z00_7011))->BgL_valuez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_value1182z00_5991)),
					BUNSPEC);
				return BgL_new1402z00_7011;
			}
		}

	}



/* &lambda2016 */
	obj_t BGl_z62lambda2016z62zzcgen_copz00(obj_t BgL_envz00_5992,
		obj_t BgL_oz00_5993, obj_t BgL_vz00_5994)
	{
		{	/* Cgen/cop.scm 65 */
			return
				((((BgL_stopz00_bglt) COBJECT(
							((BgL_stopz00_bglt) BgL_oz00_5993)))->BgL_valuez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_5994)), BUNSPEC);
		}

	}



/* &lambda2015 */
	BgL_copz00_bglt BGl_z62lambda2015z62zzcgen_copz00(obj_t BgL_envz00_5995,
		obj_t BgL_oz00_5996)
	{
		{	/* Cgen/cop.scm 65 */
			return
				(((BgL_stopz00_bglt) COBJECT(
						((BgL_stopz00_bglt) BgL_oz00_5996)))->BgL_valuez00);
		}

	}



/* &<@anonymous:2000> */
	obj_t BGl_z62zc3z04anonymousza32000ze3ze5zzcgen_copz00(obj_t BgL_envz00_5997,
		obj_t BgL_new1178z00_5998)
	{
		{	/* Cgen/cop.scm 63 */
			{
				BgL_nopz00_bglt BgL_auxz00_10621;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_nopz00_bglt) BgL_new1178z00_5998))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10625;

					{	/* Cgen/cop.scm 63 */
						obj_t BgL_classz00_7018;

						BgL_classz00_7018 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 63 */
							obj_t BgL__ortest_1117z00_7019;

							BgL__ortest_1117z00_7019 = BGL_CLASS_NIL(BgL_classz00_7018);
							if (CBOOL(BgL__ortest_1117z00_7019))
								{	/* Cgen/cop.scm 63 */
									BgL_auxz00_10625 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7019);
								}
							else
								{	/* Cgen/cop.scm 63 */
									BgL_auxz00_10625 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7018));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_nopz00_bglt) BgL_new1178z00_5998))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10625), BUNSPEC);
				}
				BgL_auxz00_10621 = ((BgL_nopz00_bglt) BgL_new1178z00_5998);
				return ((obj_t) BgL_auxz00_10621);
			}
		}

	}



/* &lambda1998 */
	BgL_nopz00_bglt BGl_z62lambda1998z62zzcgen_copz00(obj_t BgL_envz00_5999)
	{
		{	/* Cgen/cop.scm 63 */
			{	/* Cgen/cop.scm 63 */
				BgL_nopz00_bglt BgL_new1177z00_7020;

				BgL_new1177z00_7020 =
					((BgL_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_nopz00_bgl))));
				{	/* Cgen/cop.scm 63 */
					long BgL_arg1999z00_7021;

					BgL_arg1999z00_7021 = BGL_CLASS_NUM(BGl_nopz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1177z00_7020), BgL_arg1999z00_7021);
				}
				return BgL_new1177z00_7020;
			}
		}

	}



/* &lambda1996 */
	BgL_nopz00_bglt BGl_z62lambda1996z62zzcgen_copz00(obj_t BgL_envz00_6000,
		obj_t BgL_loc1175z00_6001, obj_t BgL_type1176z00_6002)
	{
		{	/* Cgen/cop.scm 63 */
			{	/* Cgen/cop.scm 63 */
				BgL_nopz00_bglt BgL_new1400z00_7023;

				{	/* Cgen/cop.scm 63 */
					BgL_nopz00_bglt BgL_new1399z00_7024;

					BgL_new1399z00_7024 =
						((BgL_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_nopz00_bgl))));
					{	/* Cgen/cop.scm 63 */
						long BgL_arg1997z00_7025;

						BgL_arg1997z00_7025 = BGL_CLASS_NUM(BGl_nopz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1399z00_7024), BgL_arg1997z00_7025);
					}
					BgL_new1400z00_7023 = BgL_new1399z00_7024;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1400z00_7023)))->BgL_locz00) =
					((obj_t) BgL_loc1175z00_6001), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1400z00_7023)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1176z00_6002)),
					BUNSPEC);
				return BgL_new1400z00_7023;
			}
		}

	}



/* &<@anonymous:1978> */
	obj_t BGl_z62zc3z04anonymousza31978ze3ze5zzcgen_copz00(obj_t BgL_envz00_6003,
		obj_t BgL_new1173z00_6004)
	{
		{	/* Cgen/cop.scm 59 */
			{
				BgL_csequencez00_bglt BgL_auxz00_10650;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_csequencez00_bglt) BgL_new1173z00_6004))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10654;

					{	/* Cgen/cop.scm 59 */
						obj_t BgL_classz00_7027;

						BgL_classz00_7027 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 59 */
							obj_t BgL__ortest_1117z00_7028;

							BgL__ortest_1117z00_7028 = BGL_CLASS_NIL(BgL_classz00_7027);
							if (CBOOL(BgL__ortest_1117z00_7028))
								{	/* Cgen/cop.scm 59 */
									BgL_auxz00_10654 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7028);
								}
							else
								{	/* Cgen/cop.scm 59 */
									BgL_auxz00_10654 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7027));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_csequencez00_bglt) BgL_new1173z00_6004))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10654), BUNSPEC);
				}
				((((BgL_csequencez00_bglt) COBJECT(
								((BgL_csequencez00_bglt) BgL_new1173z00_6004)))->
						BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_csequencez00_bglt) COBJECT(((BgL_csequencez00_bglt)
									BgL_new1173z00_6004)))->BgL_copsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_10650 = ((BgL_csequencez00_bglt) BgL_new1173z00_6004);
				return ((obj_t) BgL_auxz00_10650);
			}
		}

	}



/* &lambda1976 */
	BgL_csequencez00_bglt BGl_z62lambda1976z62zzcgen_copz00(obj_t BgL_envz00_6005)
	{
		{	/* Cgen/cop.scm 59 */
			{	/* Cgen/cop.scm 59 */
				BgL_csequencez00_bglt BgL_new1172z00_7029;

				BgL_new1172z00_7029 =
					((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_csequencez00_bgl))));
				{	/* Cgen/cop.scm 59 */
					long BgL_arg1977z00_7030;

					BgL_arg1977z00_7030 = BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1172z00_7029), BgL_arg1977z00_7030);
				}
				return BgL_new1172z00_7029;
			}
		}

	}



/* &lambda1974 */
	BgL_csequencez00_bglt BGl_z62lambda1974z62zzcgen_copz00(obj_t BgL_envz00_6006,
		obj_t BgL_loc1168z00_6007, obj_t BgL_type1169z00_6008,
		obj_t BgL_czd2expzf31170z21_6009, obj_t BgL_cops1171z00_6010)
	{
		{	/* Cgen/cop.scm 59 */
			{	/* Cgen/cop.scm 59 */
				bool_t BgL_czd2expzf31170z21_7032;

				BgL_czd2expzf31170z21_7032 = CBOOL(BgL_czd2expzf31170z21_6009);
				{	/* Cgen/cop.scm 59 */
					BgL_csequencez00_bglt BgL_new1398z00_7033;

					{	/* Cgen/cop.scm 59 */
						BgL_csequencez00_bglt BgL_new1397z00_7034;

						BgL_new1397z00_7034 =
							((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_csequencez00_bgl))));
						{	/* Cgen/cop.scm 59 */
							long BgL_arg1975z00_7035;

							BgL_arg1975z00_7035 =
								BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1397z00_7034),
								BgL_arg1975z00_7035);
						}
						BgL_new1398z00_7033 = BgL_new1397z00_7034;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1398z00_7033)))->BgL_locz00) =
						((obj_t) BgL_loc1168z00_6007), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1398z00_7033)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1169z00_6008)),
						BUNSPEC);
					((((BgL_csequencez00_bglt) COBJECT(BgL_new1398z00_7033))->
							BgL_czd2expzf3z21) =
						((bool_t) BgL_czd2expzf31170z21_7032), BUNSPEC);
					((((BgL_csequencez00_bglt) COBJECT(BgL_new1398z00_7033))->
							BgL_copsz00) = ((obj_t) BgL_cops1171z00_6010), BUNSPEC);
					return BgL_new1398z00_7033;
				}
			}
		}

	}



/* &lambda1990 */
	obj_t BGl_z62lambda1990z62zzcgen_copz00(obj_t BgL_envz00_6011,
		obj_t BgL_oz00_6012, obj_t BgL_vz00_6013)
	{
		{	/* Cgen/cop.scm 59 */
			return
				((((BgL_csequencez00_bglt) COBJECT(
							((BgL_csequencez00_bglt) BgL_oz00_6012)))->BgL_copsz00) =
				((obj_t) BgL_vz00_6013), BUNSPEC);
		}

	}



/* &lambda1989 */
	obj_t BGl_z62lambda1989z62zzcgen_copz00(obj_t BgL_envz00_6014,
		obj_t BgL_oz00_6015)
	{
		{	/* Cgen/cop.scm 59 */
			return
				(((BgL_csequencez00_bglt) COBJECT(
						((BgL_csequencez00_bglt) BgL_oz00_6015)))->BgL_copsz00);
		}

	}



/* &<@anonymous:1985> */
	obj_t BGl_z62zc3z04anonymousza31985ze3ze5zzcgen_copz00(obj_t BgL_envz00_6016)
	{
		{	/* Cgen/cop.scm 59 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1984 */
	obj_t BGl_z62lambda1984z62zzcgen_copz00(obj_t BgL_envz00_6017,
		obj_t BgL_oz00_6018, obj_t BgL_vz00_6019)
	{
		{	/* Cgen/cop.scm 59 */
			{	/* Cgen/cop.scm 59 */
				bool_t BgL_vz00_7039;

				BgL_vz00_7039 = CBOOL(BgL_vz00_6019);
				return
					((((BgL_csequencez00_bglt) COBJECT(
								((BgL_csequencez00_bglt) BgL_oz00_6018)))->BgL_czd2expzf3z21) =
					((bool_t) BgL_vz00_7039), BUNSPEC);
			}
		}

	}



/* &lambda1983 */
	obj_t BGl_z62lambda1983z62zzcgen_copz00(obj_t BgL_envz00_6020,
		obj_t BgL_oz00_6021)
	{
		{	/* Cgen/cop.scm 59 */
			return
				BBOOL(
				(((BgL_csequencez00_bglt) COBJECT(
							((BgL_csequencez00_bglt) BgL_oz00_6021)))->BgL_czd2expzf3z21));
		}

	}



/* &<@anonymous:1963> */
	obj_t BGl_z62zc3z04anonymousza31963ze3ze5zzcgen_copz00(obj_t BgL_envz00_6022,
		obj_t BgL_new1166z00_6023)
	{
		{	/* Cgen/cop.scm 56 */
			{
				BgL_ccastz00_bglt BgL_auxz00_10697;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_ccastz00_bglt) BgL_new1166z00_6023))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10701;

					{	/* Cgen/cop.scm 56 */
						obj_t BgL_classz00_7042;

						BgL_classz00_7042 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 56 */
							obj_t BgL__ortest_1117z00_7043;

							BgL__ortest_1117z00_7043 = BGL_CLASS_NIL(BgL_classz00_7042);
							if (CBOOL(BgL__ortest_1117z00_7043))
								{	/* Cgen/cop.scm 56 */
									BgL_auxz00_10701 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7043);
								}
							else
								{	/* Cgen/cop.scm 56 */
									BgL_auxz00_10701 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7042));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_ccastz00_bglt) BgL_new1166z00_6023))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10701), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10711;

					{	/* Cgen/cop.scm 56 */
						obj_t BgL_classz00_7044;

						BgL_classz00_7044 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 56 */
							obj_t BgL__ortest_1117z00_7045;

							BgL__ortest_1117z00_7045 = BGL_CLASS_NIL(BgL_classz00_7044);
							if (CBOOL(BgL__ortest_1117z00_7045))
								{	/* Cgen/cop.scm 56 */
									BgL_auxz00_10711 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_7045);
								}
							else
								{	/* Cgen/cop.scm 56 */
									BgL_auxz00_10711 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7044));
								}
						}
					}
					((((BgL_ccastz00_bglt) COBJECT(
									((BgL_ccastz00_bglt) BgL_new1166z00_6023)))->BgL_argz00) =
						((BgL_copz00_bglt) BgL_auxz00_10711), BUNSPEC);
				}
				BgL_auxz00_10697 = ((BgL_ccastz00_bglt) BgL_new1166z00_6023);
				return ((obj_t) BgL_auxz00_10697);
			}
		}

	}



/* &lambda1961 */
	BgL_ccastz00_bglt BGl_z62lambda1961z62zzcgen_copz00(obj_t BgL_envz00_6024)
	{
		{	/* Cgen/cop.scm 56 */
			{	/* Cgen/cop.scm 56 */
				BgL_ccastz00_bglt BgL_new1165z00_7046;

				BgL_new1165z00_7046 =
					((BgL_ccastz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ccastz00_bgl))));
				{	/* Cgen/cop.scm 56 */
					long BgL_arg1962z00_7047;

					BgL_arg1962z00_7047 = BGL_CLASS_NUM(BGl_ccastz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1165z00_7046), BgL_arg1962z00_7047);
				}
				return BgL_new1165z00_7046;
			}
		}

	}



/* &lambda1959 */
	BgL_ccastz00_bglt BGl_z62lambda1959z62zzcgen_copz00(obj_t BgL_envz00_6025,
		obj_t BgL_loc1161z00_6026, obj_t BgL_type1162z00_6027,
		obj_t BgL_arg1164z00_6028)
	{
		{	/* Cgen/cop.scm 56 */
			{	/* Cgen/cop.scm 56 */
				BgL_ccastz00_bglt BgL_new1396z00_7050;

				{	/* Cgen/cop.scm 56 */
					BgL_ccastz00_bglt BgL_new1395z00_7051;

					BgL_new1395z00_7051 =
						((BgL_ccastz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_ccastz00_bgl))));
					{	/* Cgen/cop.scm 56 */
						long BgL_arg1960z00_7052;

						BgL_arg1960z00_7052 = BGL_CLASS_NUM(BGl_ccastz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1395z00_7051), BgL_arg1960z00_7052);
					}
					BgL_new1396z00_7050 = BgL_new1395z00_7051;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1396z00_7050)))->BgL_locz00) =
					((obj_t) BgL_loc1161z00_6026), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1396z00_7050)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1162z00_6027)),
					BUNSPEC);
				((((BgL_ccastz00_bglt) COBJECT(BgL_new1396z00_7050))->BgL_argz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_arg1164z00_6028)), BUNSPEC);
				return BgL_new1396z00_7050;
			}
		}

	}



/* &lambda1968 */
	obj_t BGl_z62lambda1968z62zzcgen_copz00(obj_t BgL_envz00_6029,
		obj_t BgL_oz00_6030, obj_t BgL_vz00_6031)
	{
		{	/* Cgen/cop.scm 56 */
			return
				((((BgL_ccastz00_bglt) COBJECT(
							((BgL_ccastz00_bglt) BgL_oz00_6030)))->BgL_argz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_6031)), BUNSPEC);
		}

	}



/* &lambda1967 */
	BgL_copz00_bglt BGl_z62lambda1967z62zzcgen_copz00(obj_t BgL_envz00_6032,
		obj_t BgL_oz00_6033)
	{
		{	/* Cgen/cop.scm 56 */
			return
				(((BgL_ccastz00_bglt) COBJECT(
						((BgL_ccastz00_bglt) BgL_oz00_6033)))->BgL_argz00);
		}

	}



/* &<@anonymous:1943> */
	obj_t BGl_z62zc3z04anonymousza31943ze3ze5zzcgen_copz00(obj_t BgL_envz00_6034,
		obj_t BgL_new1159z00_6035)
	{
		{	/* Cgen/cop.scm 52 */
			{
				BgL_cpragmaz00_bglt BgL_auxz00_10742;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cpragmaz00_bglt) BgL_new1159z00_6035))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10746;

					{	/* Cgen/cop.scm 52 */
						obj_t BgL_classz00_7057;

						BgL_classz00_7057 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 52 */
							obj_t BgL__ortest_1117z00_7058;

							BgL__ortest_1117z00_7058 = BGL_CLASS_NIL(BgL_classz00_7057);
							if (CBOOL(BgL__ortest_1117z00_7058))
								{	/* Cgen/cop.scm 52 */
									BgL_auxz00_10746 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7058);
								}
							else
								{	/* Cgen/cop.scm 52 */
									BgL_auxz00_10746 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7057));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cpragmaz00_bglt) BgL_new1159z00_6035))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10746), BUNSPEC);
				}
				((((BgL_cpragmaz00_bglt) COBJECT(
								((BgL_cpragmaz00_bglt) BgL_new1159z00_6035)))->BgL_formatz00) =
					((obj_t) BGl_string2633z00zzcgen_copz00), BUNSPEC);
				((((BgL_cpragmaz00_bglt) COBJECT(((BgL_cpragmaz00_bglt)
									BgL_new1159z00_6035)))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_10742 = ((BgL_cpragmaz00_bglt) BgL_new1159z00_6035);
				return ((obj_t) BgL_auxz00_10742);
			}
		}

	}



/* &lambda1941 */
	BgL_cpragmaz00_bglt BGl_z62lambda1941z62zzcgen_copz00(obj_t BgL_envz00_6036)
	{
		{	/* Cgen/cop.scm 52 */
			{	/* Cgen/cop.scm 52 */
				BgL_cpragmaz00_bglt BgL_new1158z00_7059;

				BgL_new1158z00_7059 =
					((BgL_cpragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cpragmaz00_bgl))));
				{	/* Cgen/cop.scm 52 */
					long BgL_arg1942z00_7060;

					BgL_arg1942z00_7060 = BGL_CLASS_NUM(BGl_cpragmaz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1158z00_7059), BgL_arg1942z00_7060);
				}
				return BgL_new1158z00_7059;
			}
		}

	}



/* &lambda1939 */
	BgL_cpragmaz00_bglt BGl_z62lambda1939z62zzcgen_copz00(obj_t BgL_envz00_6037,
		obj_t BgL_loc1154z00_6038, obj_t BgL_type1155z00_6039,
		obj_t BgL_format1156z00_6040, obj_t BgL_args1157z00_6041)
	{
		{	/* Cgen/cop.scm 52 */
			{	/* Cgen/cop.scm 52 */
				BgL_cpragmaz00_bglt BgL_new1394z00_7063;

				{	/* Cgen/cop.scm 52 */
					BgL_cpragmaz00_bglt BgL_new1393z00_7064;

					BgL_new1393z00_7064 =
						((BgL_cpragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cpragmaz00_bgl))));
					{	/* Cgen/cop.scm 52 */
						long BgL_arg1940z00_7065;

						BgL_arg1940z00_7065 = BGL_CLASS_NUM(BGl_cpragmaz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1393z00_7064), BgL_arg1940z00_7065);
					}
					BgL_new1394z00_7063 = BgL_new1393z00_7064;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1394z00_7063)))->BgL_locz00) =
					((obj_t) BgL_loc1154z00_6038), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1394z00_7063)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1155z00_6039)),
					BUNSPEC);
				((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1394z00_7063))->BgL_formatz00) =
					((obj_t) ((obj_t) BgL_format1156z00_6040)), BUNSPEC);
				((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1394z00_7063))->BgL_argsz00) =
					((obj_t) BgL_args1157z00_6041), BUNSPEC);
				return BgL_new1394z00_7063;
			}
		}

	}



/* &lambda1953 */
	obj_t BGl_z62lambda1953z62zzcgen_copz00(obj_t BgL_envz00_6042,
		obj_t BgL_oz00_6043, obj_t BgL_vz00_6044)
	{
		{	/* Cgen/cop.scm 52 */
			return
				((((BgL_cpragmaz00_bglt) COBJECT(
							((BgL_cpragmaz00_bglt) BgL_oz00_6043)))->BgL_argsz00) =
				((obj_t) BgL_vz00_6044), BUNSPEC);
		}

	}



/* &lambda1952 */
	obj_t BGl_z62lambda1952z62zzcgen_copz00(obj_t BgL_envz00_6045,
		obj_t BgL_oz00_6046)
	{
		{	/* Cgen/cop.scm 52 */
			return
				(((BgL_cpragmaz00_bglt) COBJECT(
						((BgL_cpragmaz00_bglt) BgL_oz00_6046)))->BgL_argsz00);
		}

	}



/* &lambda1948 */
	obj_t BGl_z62lambda1948z62zzcgen_copz00(obj_t BgL_envz00_6047,
		obj_t BgL_oz00_6048, obj_t BgL_vz00_6049)
	{
		{	/* Cgen/cop.scm 52 */
			return
				((((BgL_cpragmaz00_bglt) COBJECT(
							((BgL_cpragmaz00_bglt) BgL_oz00_6048)))->BgL_formatz00) = ((obj_t)
					((obj_t) BgL_vz00_6049)), BUNSPEC);
		}

	}



/* &lambda1947 */
	obj_t BGl_z62lambda1947z62zzcgen_copz00(obj_t BgL_envz00_6050,
		obj_t BgL_oz00_6051)
	{
		{	/* Cgen/cop.scm 52 */
			return
				(((BgL_cpragmaz00_bglt) COBJECT(
						((BgL_cpragmaz00_bglt) BgL_oz00_6051)))->BgL_formatz00);
		}

	}



/* &<@anonymous:1928> */
	obj_t BGl_z62zc3z04anonymousza31928ze3ze5zzcgen_copz00(obj_t BgL_envz00_6052,
		obj_t BgL_new1152z00_6053)
	{
		{	/* Cgen/cop.scm 49 */
			{
				BgL_varcz00_bglt BgL_auxz00_10787;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_varcz00_bglt) BgL_new1152z00_6053))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10791;

					{	/* Cgen/cop.scm 49 */
						obj_t BgL_classz00_7072;

						BgL_classz00_7072 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 49 */
							obj_t BgL__ortest_1117z00_7073;

							BgL__ortest_1117z00_7073 = BGL_CLASS_NIL(BgL_classz00_7072);
							if (CBOOL(BgL__ortest_1117z00_7073))
								{	/* Cgen/cop.scm 49 */
									BgL_auxz00_10791 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7073);
								}
							else
								{	/* Cgen/cop.scm 49 */
									BgL_auxz00_10791 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7072));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_varcz00_bglt) BgL_new1152z00_6053))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10791), BUNSPEC);
				}
				{
					BgL_variablez00_bglt BgL_auxz00_10801;

					{	/* Cgen/cop.scm 49 */
						obj_t BgL_classz00_7074;

						BgL_classz00_7074 = BGl_variablez00zzast_varz00;
						{	/* Cgen/cop.scm 49 */
							obj_t BgL__ortest_1117z00_7075;

							BgL__ortest_1117z00_7075 = BGL_CLASS_NIL(BgL_classz00_7074);
							if (CBOOL(BgL__ortest_1117z00_7075))
								{	/* Cgen/cop.scm 49 */
									BgL_auxz00_10801 =
										((BgL_variablez00_bglt) BgL__ortest_1117z00_7075);
								}
							else
								{	/* Cgen/cop.scm 49 */
									BgL_auxz00_10801 =
										((BgL_variablez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7074));
								}
						}
					}
					((((BgL_varcz00_bglt) COBJECT(
									((BgL_varcz00_bglt) BgL_new1152z00_6053)))->BgL_variablez00) =
						((BgL_variablez00_bglt) BgL_auxz00_10801), BUNSPEC);
				}
				BgL_auxz00_10787 = ((BgL_varcz00_bglt) BgL_new1152z00_6053);
				return ((obj_t) BgL_auxz00_10787);
			}
		}

	}



/* &lambda1926 */
	BgL_varcz00_bglt BGl_z62lambda1926z62zzcgen_copz00(obj_t BgL_envz00_6054)
	{
		{	/* Cgen/cop.scm 49 */
			{	/* Cgen/cop.scm 49 */
				BgL_varcz00_bglt BgL_new1151z00_7076;

				BgL_new1151z00_7076 =
					((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_varcz00_bgl))));
				{	/* Cgen/cop.scm 49 */
					long BgL_arg1927z00_7077;

					BgL_arg1927z00_7077 = BGL_CLASS_NUM(BGl_varcz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1151z00_7076), BgL_arg1927z00_7077);
				}
				return BgL_new1151z00_7076;
			}
		}

	}



/* &lambda1924 */
	BgL_varcz00_bglt BGl_z62lambda1924z62zzcgen_copz00(obj_t BgL_envz00_6055,
		obj_t BgL_loc1148z00_6056, obj_t BgL_type1149z00_6057,
		obj_t BgL_variable1150z00_6058)
	{
		{	/* Cgen/cop.scm 49 */
			{	/* Cgen/cop.scm 49 */
				BgL_varcz00_bglt BgL_new1392z00_7080;

				{	/* Cgen/cop.scm 49 */
					BgL_varcz00_bglt BgL_new1391z00_7081;

					BgL_new1391z00_7081 =
						((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_varcz00_bgl))));
					{	/* Cgen/cop.scm 49 */
						long BgL_arg1925z00_7082;

						BgL_arg1925z00_7082 = BGL_CLASS_NUM(BGl_varcz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1391z00_7081), BgL_arg1925z00_7082);
					}
					BgL_new1392z00_7080 = BgL_new1391z00_7081;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1392z00_7080)))->BgL_locz00) =
					((obj_t) BgL_loc1148z00_6056), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1392z00_7080)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1149z00_6057)),
					BUNSPEC);
				((((BgL_varcz00_bglt) COBJECT(BgL_new1392z00_7080))->BgL_variablez00) =
					((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
							BgL_variable1150z00_6058)), BUNSPEC);
				return BgL_new1392z00_7080;
			}
		}

	}



/* &lambda1933 */
	obj_t BGl_z62lambda1933z62zzcgen_copz00(obj_t BgL_envz00_6059,
		obj_t BgL_oz00_6060, obj_t BgL_vz00_6061)
	{
		{	/* Cgen/cop.scm 49 */
			return
				((((BgL_varcz00_bglt) COBJECT(
							((BgL_varcz00_bglt) BgL_oz00_6060)))->BgL_variablez00) =
				((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_vz00_6061)),
				BUNSPEC);
		}

	}



/* &lambda1932 */
	BgL_variablez00_bglt BGl_z62lambda1932z62zzcgen_copz00(obj_t BgL_envz00_6062,
		obj_t BgL_oz00_6063)
	{
		{	/* Cgen/cop.scm 49 */
			return
				(((BgL_varcz00_bglt) COBJECT(
						((BgL_varcz00_bglt) BgL_oz00_6063)))->BgL_variablez00);
		}

	}



/* &<@anonymous:1911> */
	obj_t BGl_z62zc3z04anonymousza31911ze3ze5zzcgen_copz00(obj_t BgL_envz00_6064,
		obj_t BgL_new1146z00_6065)
	{
		{	/* Cgen/cop.scm 46 */
			{
				BgL_catomz00_bglt BgL_auxz00_10832;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_catomz00_bglt) BgL_new1146z00_6065))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10836;

					{	/* Cgen/cop.scm 46 */
						obj_t BgL_classz00_7087;

						BgL_classz00_7087 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 46 */
							obj_t BgL__ortest_1117z00_7088;

							BgL__ortest_1117z00_7088 = BGL_CLASS_NIL(BgL_classz00_7087);
							if (CBOOL(BgL__ortest_1117z00_7088))
								{	/* Cgen/cop.scm 46 */
									BgL_auxz00_10836 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7088);
								}
							else
								{	/* Cgen/cop.scm 46 */
									BgL_auxz00_10836 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7087));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_catomz00_bglt) BgL_new1146z00_6065))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10836), BUNSPEC);
				}
				((((BgL_catomz00_bglt) COBJECT(
								((BgL_catomz00_bglt) BgL_new1146z00_6065)))->BgL_valuez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_10832 = ((BgL_catomz00_bglt) BgL_new1146z00_6065);
				return ((obj_t) BgL_auxz00_10832);
			}
		}

	}



/* &lambda1907 */
	BgL_catomz00_bglt BGl_z62lambda1907z62zzcgen_copz00(obj_t BgL_envz00_6066)
	{
		{	/* Cgen/cop.scm 46 */
			{	/* Cgen/cop.scm 46 */
				BgL_catomz00_bglt BgL_new1145z00_7089;

				BgL_new1145z00_7089 =
					((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_catomz00_bgl))));
				{	/* Cgen/cop.scm 46 */
					long BgL_arg1910z00_7090;

					BgL_arg1910z00_7090 = BGL_CLASS_NUM(BGl_catomz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1145z00_7089), BgL_arg1910z00_7090);
				}
				return BgL_new1145z00_7089;
			}
		}

	}



/* &lambda1905 */
	BgL_catomz00_bglt BGl_z62lambda1905z62zzcgen_copz00(obj_t BgL_envz00_6067,
		obj_t BgL_loc1142z00_6068, obj_t BgL_type1143z00_6069,
		obj_t BgL_value1144z00_6070)
	{
		{	/* Cgen/cop.scm 46 */
			{	/* Cgen/cop.scm 46 */
				BgL_catomz00_bglt BgL_new1390z00_7092;

				{	/* Cgen/cop.scm 46 */
					BgL_catomz00_bglt BgL_new1389z00_7093;

					BgL_new1389z00_7093 =
						((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_catomz00_bgl))));
					{	/* Cgen/cop.scm 46 */
						long BgL_arg1906z00_7094;

						BgL_arg1906z00_7094 = BGL_CLASS_NUM(BGl_catomz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1389z00_7093), BgL_arg1906z00_7094);
					}
					BgL_new1390z00_7092 = BgL_new1389z00_7093;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1390z00_7092)))->BgL_locz00) =
					((obj_t) BgL_loc1142z00_6068), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1390z00_7092)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1143z00_6069)),
					BUNSPEC);
				((((BgL_catomz00_bglt) COBJECT(BgL_new1390z00_7092))->BgL_valuez00) =
					((obj_t) BgL_value1144z00_6070), BUNSPEC);
				return BgL_new1390z00_7092;
			}
		}

	}



/* &lambda1916 */
	obj_t BGl_z62lambda1916z62zzcgen_copz00(obj_t BgL_envz00_6071,
		obj_t BgL_oz00_6072, obj_t BgL_vz00_6073)
	{
		{	/* Cgen/cop.scm 46 */
			return
				((((BgL_catomz00_bglt) COBJECT(
							((BgL_catomz00_bglt) BgL_oz00_6072)))->BgL_valuez00) =
				((obj_t) BgL_vz00_6073), BUNSPEC);
		}

	}



/* &lambda1915 */
	obj_t BGl_z62lambda1915z62zzcgen_copz00(obj_t BgL_envz00_6074,
		obj_t BgL_oz00_6075)
	{
		{	/* Cgen/cop.scm 46 */
			return
				(((BgL_catomz00_bglt) COBJECT(
						((BgL_catomz00_bglt) BgL_oz00_6075)))->BgL_valuez00);
		}

	}



/* &<@anonymous:1892> */
	obj_t BGl_z62zc3z04anonymousza31892ze3ze5zzcgen_copz00(obj_t BgL_envz00_6076,
		obj_t BgL_new1140z00_6077)
	{
		{	/* Cgen/cop.scm 43 */
			{
				BgL_cvoidz00_bglt BgL_auxz00_10868;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cvoidz00_bglt) BgL_new1140z00_6077))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10872;

					{	/* Cgen/cop.scm 43 */
						obj_t BgL_classz00_7098;

						BgL_classz00_7098 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 43 */
							obj_t BgL__ortest_1117z00_7099;

							BgL__ortest_1117z00_7099 = BGL_CLASS_NIL(BgL_classz00_7098);
							if (CBOOL(BgL__ortest_1117z00_7099))
								{	/* Cgen/cop.scm 43 */
									BgL_auxz00_10872 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7099);
								}
							else
								{	/* Cgen/cop.scm 43 */
									BgL_auxz00_10872 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7098));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cvoidz00_bglt) BgL_new1140z00_6077))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_10872), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10882;

					{	/* Cgen/cop.scm 43 */
						obj_t BgL_classz00_7100;

						BgL_classz00_7100 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 43 */
							obj_t BgL__ortest_1117z00_7101;

							BgL__ortest_1117z00_7101 = BGL_CLASS_NIL(BgL_classz00_7100);
							if (CBOOL(BgL__ortest_1117z00_7101))
								{	/* Cgen/cop.scm 43 */
									BgL_auxz00_10882 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_7101);
								}
							else
								{	/* Cgen/cop.scm 43 */
									BgL_auxz00_10882 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7100));
								}
						}
					}
					((((BgL_cvoidz00_bglt) COBJECT(
									((BgL_cvoidz00_bglt) BgL_new1140z00_6077)))->BgL_valuez00) =
						((BgL_copz00_bglt) BgL_auxz00_10882), BUNSPEC);
				}
				BgL_auxz00_10868 = ((BgL_cvoidz00_bglt) BgL_new1140z00_6077);
				return ((obj_t) BgL_auxz00_10868);
			}
		}

	}



/* &lambda1890 */
	BgL_cvoidz00_bglt BGl_z62lambda1890z62zzcgen_copz00(obj_t BgL_envz00_6078)
	{
		{	/* Cgen/cop.scm 43 */
			{	/* Cgen/cop.scm 43 */
				BgL_cvoidz00_bglt BgL_new1139z00_7102;

				BgL_new1139z00_7102 =
					((BgL_cvoidz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cvoidz00_bgl))));
				{	/* Cgen/cop.scm 43 */
					long BgL_arg1891z00_7103;

					BgL_arg1891z00_7103 = BGL_CLASS_NUM(BGl_cvoidz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1139z00_7102), BgL_arg1891z00_7103);
				}
				return BgL_new1139z00_7102;
			}
		}

	}



/* &lambda1888 */
	BgL_cvoidz00_bglt BGl_z62lambda1888z62zzcgen_copz00(obj_t BgL_envz00_6079,
		obj_t BgL_loc1136z00_6080, obj_t BgL_type1137z00_6081,
		obj_t BgL_value1138z00_6082)
	{
		{	/* Cgen/cop.scm 43 */
			{	/* Cgen/cop.scm 43 */
				BgL_cvoidz00_bglt BgL_new1388z00_7106;

				{	/* Cgen/cop.scm 43 */
					BgL_cvoidz00_bglt BgL_new1387z00_7107;

					BgL_new1387z00_7107 =
						((BgL_cvoidz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cvoidz00_bgl))));
					{	/* Cgen/cop.scm 43 */
						long BgL_arg1889z00_7108;

						BgL_arg1889z00_7108 = BGL_CLASS_NUM(BGl_cvoidz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1387z00_7107), BgL_arg1889z00_7108);
					}
					BgL_new1388z00_7106 = BgL_new1387z00_7107;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1388z00_7106)))->BgL_locz00) =
					((obj_t) BgL_loc1136z00_6080), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1388z00_7106)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1137z00_6081)),
					BUNSPEC);
				((((BgL_cvoidz00_bglt) COBJECT(BgL_new1388z00_7106))->BgL_valuez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_value1138z00_6082)),
					BUNSPEC);
				return BgL_new1388z00_7106;
			}
		}

	}



/* &lambda1898 */
	obj_t BGl_z62lambda1898z62zzcgen_copz00(obj_t BgL_envz00_6083,
		obj_t BgL_oz00_6084, obj_t BgL_vz00_6085)
	{
		{	/* Cgen/cop.scm 43 */
			return
				((((BgL_cvoidz00_bglt) COBJECT(
							((BgL_cvoidz00_bglt) BgL_oz00_6084)))->BgL_valuez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_6085)), BUNSPEC);
		}

	}



/* &lambda1897 */
	BgL_copz00_bglt BGl_z62lambda1897z62zzcgen_copz00(obj_t BgL_envz00_6086,
		obj_t BgL_oz00_6087)
	{
		{	/* Cgen/cop.scm 43 */
			return
				(((BgL_cvoidz00_bglt) COBJECT(
						((BgL_cvoidz00_bglt) BgL_oz00_6087)))->BgL_valuez00);
		}

	}



/* &<@anonymous:1867> */
	obj_t BGl_z62zc3z04anonymousza31867ze3ze5zzcgen_copz00(obj_t BgL_envz00_6088,
		obj_t BgL_new1134z00_6089)
	{
		{	/* Cgen/cop.scm 39 */
			{
				BgL_creturnz00_bglt BgL_auxz00_10913;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_creturnz00_bglt) BgL_new1134z00_6089))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10917;

					{	/* Cgen/cop.scm 39 */
						obj_t BgL_classz00_7113;

						BgL_classz00_7113 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 39 */
							obj_t BgL__ortest_1117z00_7114;

							BgL__ortest_1117z00_7114 = BGL_CLASS_NIL(BgL_classz00_7113);
							if (CBOOL(BgL__ortest_1117z00_7114))
								{	/* Cgen/cop.scm 39 */
									BgL_auxz00_10917 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7114);
								}
							else
								{	/* Cgen/cop.scm 39 */
									BgL_auxz00_10917 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7113));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_creturnz00_bglt) BgL_new1134z00_6089))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10917), BUNSPEC);
				}
				((((BgL_creturnz00_bglt) COBJECT(
								((BgL_creturnz00_bglt) BgL_new1134z00_6089)))->BgL_tailz00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_copz00_bglt BgL_auxz00_10929;

					{	/* Cgen/cop.scm 39 */
						obj_t BgL_classz00_7115;

						BgL_classz00_7115 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 39 */
							obj_t BgL__ortest_1117z00_7116;

							BgL__ortest_1117z00_7116 = BGL_CLASS_NIL(BgL_classz00_7115);
							if (CBOOL(BgL__ortest_1117z00_7116))
								{	/* Cgen/cop.scm 39 */
									BgL_auxz00_10929 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_7116);
								}
							else
								{	/* Cgen/cop.scm 39 */
									BgL_auxz00_10929 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7115));
								}
						}
					}
					((((BgL_creturnz00_bglt) COBJECT(
									((BgL_creturnz00_bglt) BgL_new1134z00_6089)))->BgL_valuez00) =
						((BgL_copz00_bglt) BgL_auxz00_10929), BUNSPEC);
				}
				BgL_auxz00_10913 = ((BgL_creturnz00_bglt) BgL_new1134z00_6089);
				return ((obj_t) BgL_auxz00_10913);
			}
		}

	}



/* &lambda1865 */
	BgL_creturnz00_bglt BGl_z62lambda1865z62zzcgen_copz00(obj_t BgL_envz00_6090)
	{
		{	/* Cgen/cop.scm 39 */
			{	/* Cgen/cop.scm 39 */
				BgL_creturnz00_bglt BgL_new1133z00_7117;

				BgL_new1133z00_7117 =
					((BgL_creturnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_creturnz00_bgl))));
				{	/* Cgen/cop.scm 39 */
					long BgL_arg1866z00_7118;

					BgL_arg1866z00_7118 = BGL_CLASS_NUM(BGl_creturnz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1133z00_7117), BgL_arg1866z00_7118);
				}
				return BgL_new1133z00_7117;
			}
		}

	}



/* &lambda1863 */
	BgL_creturnz00_bglt BGl_z62lambda1863z62zzcgen_copz00(obj_t BgL_envz00_6091,
		obj_t BgL_loc1129z00_6092, obj_t BgL_type1130z00_6093,
		obj_t BgL_tail1131z00_6094, obj_t BgL_value1132z00_6095)
	{
		{	/* Cgen/cop.scm 39 */
			{	/* Cgen/cop.scm 39 */
				bool_t BgL_tail1131z00_7120;

				BgL_tail1131z00_7120 = CBOOL(BgL_tail1131z00_6094);
				{	/* Cgen/cop.scm 39 */
					BgL_creturnz00_bglt BgL_new1386z00_7122;

					{	/* Cgen/cop.scm 39 */
						BgL_creturnz00_bglt BgL_new1385z00_7123;

						BgL_new1385z00_7123 =
							((BgL_creturnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_creturnz00_bgl))));
						{	/* Cgen/cop.scm 39 */
							long BgL_arg1864z00_7124;

							BgL_arg1864z00_7124 = BGL_CLASS_NUM(BGl_creturnz00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1385z00_7123),
								BgL_arg1864z00_7124);
						}
						BgL_new1386z00_7122 = BgL_new1385z00_7123;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1386z00_7122)))->BgL_locz00) =
						((obj_t) BgL_loc1129z00_6092), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1386z00_7122)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1130z00_6093)),
						BUNSPEC);
					((((BgL_creturnz00_bglt) COBJECT(BgL_new1386z00_7122))->BgL_tailz00) =
						((bool_t) BgL_tail1131z00_7120), BUNSPEC);
					((((BgL_creturnz00_bglt) COBJECT(BgL_new1386z00_7122))->
							BgL_valuez00) =
						((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_value1132z00_6095)),
						BUNSPEC);
					return BgL_new1386z00_7122;
				}
			}
		}

	}



/* &lambda1880 */
	obj_t BGl_z62lambda1880z62zzcgen_copz00(obj_t BgL_envz00_6096,
		obj_t BgL_oz00_6097, obj_t BgL_vz00_6098)
	{
		{	/* Cgen/cop.scm 39 */
			return
				((((BgL_creturnz00_bglt) COBJECT(
							((BgL_creturnz00_bglt) BgL_oz00_6097)))->BgL_valuez00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_6098)), BUNSPEC);
		}

	}



/* &lambda1879 */
	BgL_copz00_bglt BGl_z62lambda1879z62zzcgen_copz00(obj_t BgL_envz00_6099,
		obj_t BgL_oz00_6100)
	{
		{	/* Cgen/cop.scm 39 */
			return
				(((BgL_creturnz00_bglt) COBJECT(
						((BgL_creturnz00_bglt) BgL_oz00_6100)))->BgL_valuez00);
		}

	}



/* &<@anonymous:1875> */
	obj_t BGl_z62zc3z04anonymousza31875ze3ze5zzcgen_copz00(obj_t BgL_envz00_6101)
	{
		{	/* Cgen/cop.scm 39 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1874 */
	obj_t BGl_z62lambda1874z62zzcgen_copz00(obj_t BgL_envz00_6102,
		obj_t BgL_oz00_6103, obj_t BgL_vz00_6104)
	{
		{	/* Cgen/cop.scm 39 */
			{	/* Cgen/cop.scm 39 */
				bool_t BgL_vz00_7129;

				BgL_vz00_7129 = CBOOL(BgL_vz00_6104);
				return
					((((BgL_creturnz00_bglt) COBJECT(
								((BgL_creturnz00_bglt) BgL_oz00_6103)))->BgL_tailz00) =
					((bool_t) BgL_vz00_7129), BUNSPEC);
			}
		}

	}



/* &lambda1873 */
	obj_t BGl_z62lambda1873z62zzcgen_copz00(obj_t BgL_envz00_6105,
		obj_t BgL_oz00_6106)
	{
		{	/* Cgen/cop.scm 39 */
			return
				BBOOL(
				(((BgL_creturnz00_bglt) COBJECT(
							((BgL_creturnz00_bglt) BgL_oz00_6106)))->BgL_tailz00));
		}

	}



/* &<@anonymous:1851> */
	obj_t BGl_z62zc3z04anonymousza31851ze3ze5zzcgen_copz00(obj_t BgL_envz00_6107,
		obj_t BgL_new1127z00_6108)
	{
		{	/* Cgen/cop.scm 36 */
			{
				BgL_cblockz00_bglt BgL_auxz00_10969;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cblockz00_bglt) BgL_new1127z00_6108))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_10973;

					{	/* Cgen/cop.scm 36 */
						obj_t BgL_classz00_7132;

						BgL_classz00_7132 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 36 */
							obj_t BgL__ortest_1117z00_7133;

							BgL__ortest_1117z00_7133 = BGL_CLASS_NIL(BgL_classz00_7132);
							if (CBOOL(BgL__ortest_1117z00_7133))
								{	/* Cgen/cop.scm 36 */
									BgL_auxz00_10973 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7133);
								}
							else
								{	/* Cgen/cop.scm 36 */
									BgL_auxz00_10973 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7132));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cblockz00_bglt) BgL_new1127z00_6108))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_10973), BUNSPEC);
				}
				{
					BgL_copz00_bglt BgL_auxz00_10983;

					{	/* Cgen/cop.scm 36 */
						obj_t BgL_classz00_7134;

						BgL_classz00_7134 = BGl_copz00zzcgen_copz00;
						{	/* Cgen/cop.scm 36 */
							obj_t BgL__ortest_1117z00_7135;

							BgL__ortest_1117z00_7135 = BGL_CLASS_NIL(BgL_classz00_7134);
							if (CBOOL(BgL__ortest_1117z00_7135))
								{	/* Cgen/cop.scm 36 */
									BgL_auxz00_10983 =
										((BgL_copz00_bglt) BgL__ortest_1117z00_7135);
								}
							else
								{	/* Cgen/cop.scm 36 */
									BgL_auxz00_10983 =
										((BgL_copz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7134));
								}
						}
					}
					((((BgL_cblockz00_bglt) COBJECT(
									((BgL_cblockz00_bglt) BgL_new1127z00_6108)))->BgL_bodyz00) =
						((BgL_copz00_bglt) BgL_auxz00_10983), BUNSPEC);
				}
				BgL_auxz00_10969 = ((BgL_cblockz00_bglt) BgL_new1127z00_6108);
				return ((obj_t) BgL_auxz00_10969);
			}
		}

	}



/* &lambda1849 */
	BgL_cblockz00_bglt BGl_z62lambda1849z62zzcgen_copz00(obj_t BgL_envz00_6109)
	{
		{	/* Cgen/cop.scm 36 */
			{	/* Cgen/cop.scm 36 */
				BgL_cblockz00_bglt BgL_new1125z00_7136;

				BgL_new1125z00_7136 =
					((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cblockz00_bgl))));
				{	/* Cgen/cop.scm 36 */
					long BgL_arg1850z00_7137;

					BgL_arg1850z00_7137 = BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1125z00_7136), BgL_arg1850z00_7137);
				}
				return BgL_new1125z00_7136;
			}
		}

	}



/* &lambda1847 */
	BgL_cblockz00_bglt BGl_z62lambda1847z62zzcgen_copz00(obj_t BgL_envz00_6110,
		obj_t BgL_loc1122z00_6111, obj_t BgL_type1123z00_6112,
		obj_t BgL_body1124z00_6113)
	{
		{	/* Cgen/cop.scm 36 */
			{	/* Cgen/cop.scm 36 */
				BgL_cblockz00_bglt BgL_new1384z00_7140;

				{	/* Cgen/cop.scm 36 */
					BgL_cblockz00_bglt BgL_new1383z00_7141;

					BgL_new1383z00_7141 =
						((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cblockz00_bgl))));
					{	/* Cgen/cop.scm 36 */
						long BgL_arg1848z00_7142;

						BgL_arg1848z00_7142 = BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1383z00_7141), BgL_arg1848z00_7142);
					}
					BgL_new1384z00_7140 = BgL_new1383z00_7141;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1384z00_7140)))->BgL_locz00) =
					((obj_t) BgL_loc1122z00_6111), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1384z00_7140)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1123z00_6112)),
					BUNSPEC);
				((((BgL_cblockz00_bglt) COBJECT(BgL_new1384z00_7140))->BgL_bodyz00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_body1124z00_6113)),
					BUNSPEC);
				return BgL_new1384z00_7140;
			}
		}

	}



/* &lambda1856 */
	obj_t BGl_z62lambda1856z62zzcgen_copz00(obj_t BgL_envz00_6114,
		obj_t BgL_oz00_6115, obj_t BgL_vz00_6116)
	{
		{	/* Cgen/cop.scm 36 */
			return
				((((BgL_cblockz00_bglt) COBJECT(
							((BgL_cblockz00_bglt) BgL_oz00_6115)))->BgL_bodyz00) =
				((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_6116)), BUNSPEC);
		}

	}



/* &lambda1855 */
	BgL_copz00_bglt BGl_z62lambda1855z62zzcgen_copz00(obj_t BgL_envz00_6117,
		obj_t BgL_oz00_6118)
	{
		{	/* Cgen/cop.scm 36 */
			return
				(((BgL_cblockz00_bglt) COBJECT(
						((BgL_cblockz00_bglt) BgL_oz00_6118)))->BgL_bodyz00);
		}

	}



/* &<@anonymous:1836> */
	obj_t BGl_z62zc3z04anonymousza31836ze3ze5zzcgen_copz00(obj_t BgL_envz00_6119,
		obj_t BgL_new1120z00_6120)
	{
		{	/* Cgen/cop.scm 33 */
			{
				BgL_cgotoz00_bglt BgL_auxz00_11014;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cgotoz00_bglt) BgL_new1120z00_6120))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11018;

					{	/* Cgen/cop.scm 33 */
						obj_t BgL_classz00_7147;

						BgL_classz00_7147 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 33 */
							obj_t BgL__ortest_1117z00_7148;

							BgL__ortest_1117z00_7148 = BGL_CLASS_NIL(BgL_classz00_7147);
							if (CBOOL(BgL__ortest_1117z00_7148))
								{	/* Cgen/cop.scm 33 */
									BgL_auxz00_11018 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7148);
								}
							else
								{	/* Cgen/cop.scm 33 */
									BgL_auxz00_11018 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7147));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cgotoz00_bglt) BgL_new1120z00_6120))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_11018), BUNSPEC);
				}
				{
					BgL_clabelz00_bglt BgL_auxz00_11028;

					{	/* Cgen/cop.scm 33 */
						obj_t BgL_classz00_7149;

						BgL_classz00_7149 = BGl_clabelz00zzcgen_copz00;
						{	/* Cgen/cop.scm 33 */
							obj_t BgL__ortest_1117z00_7150;

							BgL__ortest_1117z00_7150 = BGL_CLASS_NIL(BgL_classz00_7149);
							if (CBOOL(BgL__ortest_1117z00_7150))
								{	/* Cgen/cop.scm 33 */
									BgL_auxz00_11028 =
										((BgL_clabelz00_bglt) BgL__ortest_1117z00_7150);
								}
							else
								{	/* Cgen/cop.scm 33 */
									BgL_auxz00_11028 =
										((BgL_clabelz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7149));
								}
						}
					}
					((((BgL_cgotoz00_bglt) COBJECT(
									((BgL_cgotoz00_bglt) BgL_new1120z00_6120)))->BgL_labelz00) =
						((BgL_clabelz00_bglt) BgL_auxz00_11028), BUNSPEC);
				}
				BgL_auxz00_11014 = ((BgL_cgotoz00_bglt) BgL_new1120z00_6120);
				return ((obj_t) BgL_auxz00_11014);
			}
		}

	}



/* &lambda1834 */
	BgL_cgotoz00_bglt BGl_z62lambda1834z62zzcgen_copz00(obj_t BgL_envz00_6121)
	{
		{	/* Cgen/cop.scm 33 */
			{	/* Cgen/cop.scm 33 */
				BgL_cgotoz00_bglt BgL_new1119z00_7151;

				BgL_new1119z00_7151 =
					((BgL_cgotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cgotoz00_bgl))));
				{	/* Cgen/cop.scm 33 */
					long BgL_arg1835z00_7152;

					BgL_arg1835z00_7152 = BGL_CLASS_NUM(BGl_cgotoz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1119z00_7151), BgL_arg1835z00_7152);
				}
				return BgL_new1119z00_7151;
			}
		}

	}



/* &lambda1832 */
	BgL_cgotoz00_bglt BGl_z62lambda1832z62zzcgen_copz00(obj_t BgL_envz00_6122,
		obj_t BgL_loc1116z00_6123, obj_t BgL_type1117z00_6124,
		obj_t BgL_label1118z00_6125)
	{
		{	/* Cgen/cop.scm 33 */
			{	/* Cgen/cop.scm 33 */
				BgL_cgotoz00_bglt BgL_new1382z00_7155;

				{	/* Cgen/cop.scm 33 */
					BgL_cgotoz00_bglt BgL_new1381z00_7156;

					BgL_new1381z00_7156 =
						((BgL_cgotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cgotoz00_bgl))));
					{	/* Cgen/cop.scm 33 */
						long BgL_arg1833z00_7157;

						BgL_arg1833z00_7157 = BGL_CLASS_NUM(BGl_cgotoz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1381z00_7156), BgL_arg1833z00_7157);
					}
					BgL_new1382z00_7155 = BgL_new1381z00_7156;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1382z00_7155)))->BgL_locz00) =
					((obj_t) BgL_loc1116z00_6123), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1382z00_7155)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1117z00_6124)),
					BUNSPEC);
				((((BgL_cgotoz00_bglt) COBJECT(BgL_new1382z00_7155))->BgL_labelz00) =
					((BgL_clabelz00_bglt) ((BgL_clabelz00_bglt) BgL_label1118z00_6125)),
					BUNSPEC);
				return BgL_new1382z00_7155;
			}
		}

	}



/* &lambda1841 */
	obj_t BGl_z62lambda1841z62zzcgen_copz00(obj_t BgL_envz00_6126,
		obj_t BgL_oz00_6127, obj_t BgL_vz00_6128)
	{
		{	/* Cgen/cop.scm 33 */
			return
				((((BgL_cgotoz00_bglt) COBJECT(
							((BgL_cgotoz00_bglt) BgL_oz00_6127)))->BgL_labelz00) =
				((BgL_clabelz00_bglt) ((BgL_clabelz00_bglt) BgL_vz00_6128)), BUNSPEC);
		}

	}



/* &lambda1840 */
	BgL_clabelz00_bglt BGl_z62lambda1840z62zzcgen_copz00(obj_t BgL_envz00_6129,
		obj_t BgL_oz00_6130)
	{
		{	/* Cgen/cop.scm 33 */
			return
				(((BgL_cgotoz00_bglt) COBJECT(
						((BgL_cgotoz00_bglt) BgL_oz00_6130)))->BgL_labelz00);
		}

	}



/* &<@anonymous:1755> */
	obj_t BGl_z62zc3z04anonymousza31755ze3ze5zzcgen_copz00(obj_t BgL_envz00_6131,
		obj_t BgL_new1114z00_6132)
	{
		{	/* Cgen/cop.scm 28 */
			{
				BgL_clabelz00_bglt BgL_auxz00_11059;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_clabelz00_bglt) BgL_new1114z00_6132))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11063;

					{	/* Cgen/cop.scm 28 */
						obj_t BgL_classz00_7162;

						BgL_classz00_7162 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 28 */
							obj_t BgL__ortest_1117z00_7163;

							BgL__ortest_1117z00_7163 = BGL_CLASS_NIL(BgL_classz00_7162);
							if (CBOOL(BgL__ortest_1117z00_7163))
								{	/* Cgen/cop.scm 28 */
									BgL_auxz00_11063 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7163);
								}
							else
								{	/* Cgen/cop.scm 28 */
									BgL_auxz00_11063 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7162));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_clabelz00_bglt) BgL_new1114z00_6132))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_11063), BUNSPEC);
				}
				((((BgL_clabelz00_bglt) COBJECT(
								((BgL_clabelz00_bglt) BgL_new1114z00_6132)))->BgL_namez00) =
					((obj_t) BGl_string2633z00zzcgen_copz00), BUNSPEC);
				((((BgL_clabelz00_bglt) COBJECT(((BgL_clabelz00_bglt)
									BgL_new1114z00_6132)))->BgL_usedzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_clabelz00_bglt) COBJECT(((BgL_clabelz00_bglt)
									BgL_new1114z00_6132)))->BgL_bodyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_11059 = ((BgL_clabelz00_bglt) BgL_new1114z00_6132);
				return ((obj_t) BgL_auxz00_11059);
			}
		}

	}



/* &lambda1753 */
	BgL_clabelz00_bglt BGl_z62lambda1753z62zzcgen_copz00(obj_t BgL_envz00_6133)
	{
		{	/* Cgen/cop.scm 28 */
			{	/* Cgen/cop.scm 28 */
				BgL_clabelz00_bglt BgL_new1113z00_7164;

				BgL_new1113z00_7164 =
					((BgL_clabelz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_clabelz00_bgl))));
				{	/* Cgen/cop.scm 28 */
					long BgL_arg1754z00_7165;

					BgL_arg1754z00_7165 = BGL_CLASS_NUM(BGl_clabelz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1113z00_7164), BgL_arg1754z00_7165);
				}
				return BgL_new1113z00_7164;
			}
		}

	}



/* &lambda1751 */
	BgL_clabelz00_bglt BGl_z62lambda1751z62zzcgen_copz00(obj_t BgL_envz00_6134,
		obj_t BgL_loc1108z00_6135, obj_t BgL_type1109z00_6136,
		obj_t BgL_name1110z00_6137, obj_t BgL_usedzf31111zf3_6138,
		obj_t BgL_body1112z00_6139)
	{
		{	/* Cgen/cop.scm 28 */
			{	/* Cgen/cop.scm 28 */
				bool_t BgL_usedzf31111zf3_7168;

				BgL_usedzf31111zf3_7168 = CBOOL(BgL_usedzf31111zf3_6138);
				{	/* Cgen/cop.scm 28 */
					BgL_clabelz00_bglt BgL_new1380z00_7169;

					{	/* Cgen/cop.scm 28 */
						BgL_clabelz00_bglt BgL_new1379z00_7170;

						BgL_new1379z00_7170 =
							((BgL_clabelz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_clabelz00_bgl))));
						{	/* Cgen/cop.scm 28 */
							long BgL_arg1752z00_7171;

							BgL_arg1752z00_7171 = BGL_CLASS_NUM(BGl_clabelz00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1379z00_7170),
								BgL_arg1752z00_7171);
						}
						BgL_new1380z00_7169 = BgL_new1379z00_7170;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1380z00_7169)))->BgL_locz00) =
						((obj_t) BgL_loc1108z00_6135), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1380z00_7169)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1109z00_6136)),
						BUNSPEC);
					((((BgL_clabelz00_bglt) COBJECT(BgL_new1380z00_7169))->BgL_namez00) =
						((obj_t) ((obj_t) BgL_name1110z00_6137)), BUNSPEC);
					((((BgL_clabelz00_bglt) COBJECT(BgL_new1380z00_7169))->
							BgL_usedzf3zf3) = ((bool_t) BgL_usedzf31111zf3_7168), BUNSPEC);
					((((BgL_clabelz00_bglt) COBJECT(BgL_new1380z00_7169))->BgL_bodyz00) =
						((obj_t) BgL_body1112z00_6139), BUNSPEC);
					return BgL_new1380z00_7169;
				}
			}
		}

	}



/* &<@anonymous:1809> */
	obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzcgen_copz00(obj_t BgL_envz00_6140)
	{
		{	/* Cgen/cop.scm 28 */
			return BUNSPEC;
		}

	}



/* &lambda1808 */
	obj_t BGl_z62lambda1808z62zzcgen_copz00(obj_t BgL_envz00_6141,
		obj_t BgL_oz00_6142, obj_t BgL_vz00_6143)
	{
		{	/* Cgen/cop.scm 28 */
			return
				((((BgL_clabelz00_bglt) COBJECT(
							((BgL_clabelz00_bglt) BgL_oz00_6142)))->BgL_bodyz00) =
				((obj_t) BgL_vz00_6143), BUNSPEC);
		}

	}



/* &lambda1807 */
	obj_t BGl_z62lambda1807z62zzcgen_copz00(obj_t BgL_envz00_6144,
		obj_t BgL_oz00_6145)
	{
		{	/* Cgen/cop.scm 28 */
			return
				(((BgL_clabelz00_bglt) COBJECT(
						((BgL_clabelz00_bglt) BgL_oz00_6145)))->BgL_bodyz00);
		}

	}



/* &<@anonymous:1778> */
	obj_t BGl_z62zc3z04anonymousza31778ze3ze5zzcgen_copz00(obj_t BgL_envz00_6146)
	{
		{	/* Cgen/cop.scm 28 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1777 */
	obj_t BGl_z62lambda1777z62zzcgen_copz00(obj_t BgL_envz00_6147,
		obj_t BgL_oz00_6148, obj_t BgL_vz00_6149)
	{
		{	/* Cgen/cop.scm 28 */
			{	/* Cgen/cop.scm 28 */
				bool_t BgL_vz00_7175;

				BgL_vz00_7175 = CBOOL(BgL_vz00_6149);
				return
					((((BgL_clabelz00_bglt) COBJECT(
								((BgL_clabelz00_bglt) BgL_oz00_6148)))->BgL_usedzf3zf3) =
					((bool_t) BgL_vz00_7175), BUNSPEC);
			}
		}

	}



/* &lambda1776 */
	obj_t BGl_z62lambda1776z62zzcgen_copz00(obj_t BgL_envz00_6150,
		obj_t BgL_oz00_6151)
	{
		{	/* Cgen/cop.scm 28 */
			return
				BBOOL(
				(((BgL_clabelz00_bglt) COBJECT(
							((BgL_clabelz00_bglt) BgL_oz00_6151)))->BgL_usedzf3zf3));
		}

	}



/* &lambda1767 */
	obj_t BGl_z62lambda1767z62zzcgen_copz00(obj_t BgL_envz00_6152,
		obj_t BgL_oz00_6153, obj_t BgL_vz00_6154)
	{
		{	/* Cgen/cop.scm 28 */
			return
				((((BgL_clabelz00_bglt) COBJECT(
							((BgL_clabelz00_bglt) BgL_oz00_6153)))->BgL_namez00) = ((obj_t)
					((obj_t) BgL_vz00_6154)), BUNSPEC);
		}

	}



/* &lambda1766 */
	obj_t BGl_z62lambda1766z62zzcgen_copz00(obj_t BgL_envz00_6155,
		obj_t BgL_oz00_6156)
	{
		{	/* Cgen/cop.scm 28 */
			return
				(((BgL_clabelz00_bglt) COBJECT(
						((BgL_clabelz00_bglt) BgL_oz00_6156)))->BgL_namez00);
		}

	}



/* &<@anonymous:1718> */
	obj_t BGl_z62zc3z04anonymousza31718ze3ze5zzcgen_copz00(obj_t BgL_envz00_6157,
		obj_t BgL_new1106z00_6158)
	{
		{	/* Cgen/cop.scm 23 */
			{
				BgL_copz00_bglt BgL_auxz00_11115;

				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1106z00_6158)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_11118;

					{	/* Cgen/cop.scm 23 */
						obj_t BgL_classz00_7181;

						BgL_classz00_7181 = BGl_typez00zztype_typez00;
						{	/* Cgen/cop.scm 23 */
							obj_t BgL__ortest_1117z00_7182;

							BgL__ortest_1117z00_7182 = BGL_CLASS_NIL(BgL_classz00_7181);
							if (CBOOL(BgL__ortest_1117z00_7182))
								{	/* Cgen/cop.scm 23 */
									BgL_auxz00_11118 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_7182);
								}
							else
								{	/* Cgen/cop.scm 23 */
									BgL_auxz00_11118 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_7181));
								}
						}
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1106z00_6158)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_11118), BUNSPEC);
				}
				BgL_auxz00_11115 = ((BgL_copz00_bglt) BgL_new1106z00_6158);
				return ((obj_t) BgL_auxz00_11115);
			}
		}

	}



/* &lambda1715 */
	BgL_copz00_bglt BGl_z62lambda1715z62zzcgen_copz00(obj_t BgL_envz00_6159)
	{
		{	/* Cgen/cop.scm 23 */
			{	/* Cgen/cop.scm 23 */
				BgL_copz00_bglt BgL_new1105z00_7183;

				BgL_new1105z00_7183 =
					((BgL_copz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_copz00_bgl))));
				{	/* Cgen/cop.scm 23 */
					long BgL_arg1717z00_7184;

					BgL_arg1717z00_7184 = BGL_CLASS_NUM(BGl_copz00zzcgen_copz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1105z00_7183), BgL_arg1717z00_7184);
				}
				return BgL_new1105z00_7183;
			}
		}

	}



/* &lambda1712 */
	BgL_copz00_bglt BGl_z62lambda1712z62zzcgen_copz00(obj_t BgL_envz00_6160,
		obj_t BgL_loc1103z00_6161, obj_t BgL_type1104z00_6162)
	{
		{	/* Cgen/cop.scm 23 */
			{	/* Cgen/cop.scm 23 */
				BgL_copz00_bglt BgL_new1378z00_7186;

				{	/* Cgen/cop.scm 23 */
					BgL_copz00_bglt BgL_new1377z00_7187;

					BgL_new1377z00_7187 =
						((BgL_copz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_copz00_bgl))));
					{	/* Cgen/cop.scm 23 */
						long BgL_arg1714z00_7188;

						BgL_arg1714z00_7188 = BGL_CLASS_NUM(BGl_copz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1377z00_7187), BgL_arg1714z00_7188);
					}
					BgL_new1378z00_7186 = BgL_new1377z00_7187;
				}
				((((BgL_copz00_bglt) COBJECT(BgL_new1378z00_7186))->BgL_locz00) =
					((obj_t) BgL_loc1103z00_6161), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(BgL_new1378z00_7186))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1104z00_6162)),
					BUNSPEC);
				return BgL_new1378z00_7186;
			}
		}

	}



/* &lambda1741 */
	obj_t BGl_z62lambda1741z62zzcgen_copz00(obj_t BgL_envz00_6163,
		obj_t BgL_oz00_6164, obj_t BgL_vz00_6165)
	{
		{	/* Cgen/cop.scm 23 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_6164)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_6165)), BUNSPEC);
		}

	}



/* &lambda1740 */
	BgL_typez00_bglt BGl_z62lambda1740z62zzcgen_copz00(obj_t BgL_envz00_6166,
		obj_t BgL_oz00_6167)
	{
		{	/* Cgen/cop.scm 23 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_6167)))->BgL_typez00);
		}

	}



/* &<@anonymous:1736> */
	obj_t BGl_z62zc3z04anonymousza31736ze3ze5zzcgen_copz00(obj_t BgL_envz00_6168)
	{
		{	/* Cgen/cop.scm 23 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1735 */
	obj_t BGl_z62lambda1735z62zzcgen_copz00(obj_t BgL_envz00_6169,
		obj_t BgL_oz00_6170, obj_t BgL_vz00_6171)
	{
		{	/* Cgen/cop.scm 23 */
			return
				((((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt) BgL_oz00_6170)))->BgL_locz00) =
				((obj_t) BgL_vz00_6171), BUNSPEC);
		}

	}



/* &lambda1734 */
	obj_t BGl_z62lambda1734z62zzcgen_copz00(obj_t BgL_envz00_6172,
		obj_t BgL_oz00_6173)
	{
		{	/* Cgen/cop.scm 23 */
			return
				(((BgL_copz00_bglt) COBJECT(
						((BgL_copz00_bglt) BgL_oz00_6173)))->BgL_locz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcgen_copz00(void)
	{
		{	/* Cgen/cop.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcgen_copz00(void)
	{
		{	/* Cgen/cop.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2634z00zzcgen_copz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2634z00zzcgen_copz00));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2634z00zzcgen_copz00));
		}

	}

#ifdef __cplusplus
}
#endif
