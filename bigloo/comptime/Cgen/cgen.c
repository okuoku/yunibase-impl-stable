/*===========================================================================*/
/*   (Cgen/cgen.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cgen/cgen.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CGEN_CGEN_TYPE_DEFINITIONS
#define BGL_CGEN_CGEN_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_genpatchidz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		long BgL_indexz00;
		long BgL_rindexz00;
	}                    *BgL_genpatchidz00_bglt;

	typedef struct BgL_privatez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                 *BgL_privatez00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_copz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}             *BgL_copz00_bglt;

	typedef struct BgL_clabelz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_namez00;
		bool_t BgL_usedzf3zf3;
		obj_t BgL_bodyz00;
	}                *BgL_clabelz00_bglt;

	typedef struct BgL_cgotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_clabelz00_bgl *BgL_labelz00;
	}               *BgL_cgotoz00_bglt;

	typedef struct BgL_cblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                *BgL_cblockz00_bglt;

	typedef struct BgL_creturnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_tailz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                 *BgL_creturnz00_bglt;

	typedef struct BgL_cvoidz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}               *BgL_cvoidz00_bglt;

	typedef struct BgL_catomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_catomz00_bglt;

	typedef struct BgL_varcz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}              *BgL_varcz00_bglt;

	typedef struct BgL_cpragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_formatz00;
		obj_t BgL_argsz00;
	}                 *BgL_cpragmaz00_bglt;

	typedef struct BgL_ccastz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_argz00;
	}               *BgL_ccastz00_bglt;

	typedef struct BgL_csequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_czd2expzf3z21;
		obj_t BgL_copsz00;
	}                   *BgL_csequencez00_bglt;

	typedef struct BgL_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}             *BgL_nopz00_bglt;

	typedef struct BgL_stopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}              *BgL_stopz00_bglt;

	typedef struct BgL_csetqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varcz00_bgl *BgL_varz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}               *BgL_csetqz00_bglt;

	typedef struct BgL_cifz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_testz00;
		struct BgL_copz00_bgl *BgL_truez00;
		struct BgL_copz00_bgl *BgL_falsez00;
	}             *BgL_cifz00_bglt;

	typedef struct BgL_localzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varsz00;
	}                     *BgL_localzd2varzd2_bglt;

	typedef struct BgL_cfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
	}                  *BgL_cfuncallz00_bglt;

	typedef struct BgL_capplyz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		struct BgL_copz00_bgl *BgL_argz00;
	}                *BgL_capplyz00_bglt;

	typedef struct BgL_cappz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}              *BgL_cappz00_bglt;

	typedef struct BgL_cfailz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_procz00;
		struct BgL_copz00_bgl *BgL_msgz00;
		struct BgL_copz00_bgl *BgL_objz00;
	}               *BgL_cfailz00_bglt;

	typedef struct BgL_cswitchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
	}                 *BgL_cswitchz00_bglt;

	typedef struct BgL_cmakezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
		obj_t BgL_stackablez00;
	}                     *BgL_cmakezd2boxzd2_bglt;

	typedef struct BgL_cboxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_varz00;
	}                    *BgL_cboxzd2refzd2_bglt;

	typedef struct BgL_cboxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_varz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                       *BgL_cboxzd2setz12zc0_bglt;

	typedef struct BgL_csetzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_exitz00;
		struct BgL_copz00_bgl *BgL_jumpzd2valuezd2;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                        *BgL_csetzd2exzd2itz00_bglt;

	typedef struct BgL_cjumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_exitz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                         *BgL_cjumpzd2exzd2itz00_bglt;

	typedef struct BgL_sfunzf2czf2_bgl
	{
		struct BgL_clabelz00_bgl *BgL_labelz00;
		bool_t BgL_integratedz00;
	}                  *BgL_sfunzf2czf2_bglt;

	typedef struct BgL_bdbzd2blockzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                     *BgL_bdbzd2blockzd2_bglt;

	typedef struct BgL_retblockzf2gotozf2_bgl
	{
		obj_t BgL_localz00;
		obj_t BgL_labelz00;
	}                         *BgL_retblockzf2gotozf2_bglt;


#endif													// BGL_CGEN_CGEN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static bool_t BGl_cappzd2tailcallablezf3z21zzcgen_cgenz00(BgL_cappz00_bglt);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2switch1759z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62blockzd2kontzb0zzcgen_cgenz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2conditiona1755z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_returnz00zzast_nodez00;
	BGL_EXPORTED_DEF obj_t BGl_za2stopzd2kontza2zd2zzcgen_cgenz00 = BUNSPEC;
	extern obj_t
		BGl_makezd2typedzd2declarationz00zztype_toolsz00(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static BgL_cvoidz00_bglt BGl_za2voidzd2kontza2zd2zzcgen_cgenz00(obj_t);
	static obj_t BGl_nozd2bdbzd2newlinez00zzcgen_cgenz00(void);
	static obj_t BGl_requirezd2initializa7ationz75zzcgen_cgenz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32681ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	static bool_t BGl_simplezd2exprzf3ze70zc6zzcgen_cgenz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00 =
		BUNSPEC;
	static obj_t
		BGl_z62zc3z04za2returnzd2kontza2za31779ze3z37zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static BgL_setqz00_bglt BGl_z62nodezd2setqzb0zzcgen_cgenz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_externzd2ze3copz31zzcgen_cgenz00(obj_t, bool_t,
		BgL_externz00_bglt, obj_t, obj_t);
	extern obj_t BGl_za2voidza2z00zztype_cachez00;
	extern obj_t BGl_za2czd2portza2zd2zzbackend_c_emitz00;
	extern obj_t BGl_stringzd2sanszd2z42z42zztype_toolsz00(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza32675ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static BgL_copz00_bglt BGl_z62nodezd2ze3copzd2cast1751z81zzcgen_cgenz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_clabelz00zzcgen_copz00;
	extern obj_t BGl_resetzd2bdbzd2locz12z12zzcgen_emitzd2copzd2(void);
	extern obj_t BGl_cappz00zzcgen_copz00;
	static obj_t BGl_toplevelzd2initzd2zzcgen_cgenz00(void);
	static obj_t BGl_z62zc3z04anonymousza31963ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_cboxzd2refzd2zzcgen_copz00;
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_csequencez00zzcgen_copz00;
	extern obj_t BGl_cgotoz00zzcgen_copz00;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_pregexp1685z00zzcgen_cgenz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04za2stopzd2kontza2za31838ze3z37zzcgen_cgenz00(obj_t,
		obj_t);
	static BgL_copz00_bglt BGl_z62nodezd2ze3copzd2setq1753z81zzcgen_cgenz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzcgen_cgenz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	extern obj_t BGl_csetzd2exzd2itz00zzcgen_copz00;
	static obj_t BGl_nodezd2argszd2ze3copze3zzcgen_cgenz00(BgL_typez00_bglt,
		obj_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_setqz00_bglt
		BGl_nodezd2setqzd2zzcgen_cgenz00(BgL_variablez00_bglt, BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62nodezd2ze3copzd2sync1745z81zzcgen_cgenz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2cellza2z00zztype_cachez00;
	extern obj_t BGl_cvoidz00zzcgen_copz00;
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2private1749z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_copz00_bglt BGl_z62nodezd2ze3copzd2fail1757z81zzcgen_cgenz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62cgenzd2functionzb0zzcgen_cgenz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzcgen_cgenz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_bdbzd2letzd2varz00zzcgen_cgenz00(BgL_copz00_bglt, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza31982ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04za2idzd2kontza2za31837ze3z37zzcgen_cgenz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_pregexpz00zz__regexpz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static bool_t BGl_allocazd2letzd2varze70ze7zzcgen_cgenz00(BgL_nodez00_bglt);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2boxzd2setz121777z41zzcgen_cgenz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_capplyz00zzcgen_copz00;
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2makezd2box1773z53zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2genpatchid1735z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2letzd2fun1761z53zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62za2voidzd2kontza2zb0zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_cpragmaz00zzcgen_copz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	static obj_t BGl_z62nodezd2ze3cop1727z53zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzcgen_cgenz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcgen_cgenz00(void);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2jumpzd2exzd2it1767z81zzcgen_cgenz00(obj_t, obj_t,
		obj_t, obj_t);
	static bool_t
		BGl_capplyzd2tailcallablezf3z21zzcgen_cgenz00(BgL_capplyz00_bglt);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2pragma1747z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_localz00_bglt
		BGl_z62makezd2localzd2svarzf2namez90zzcgen_cgenz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_nopz00zzcgen_copz00;
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(obj_t, BgL_typez00_bglt);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static BgL_cfailz00_bglt
		BGl_z62zc3z04anonymousza32242ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	static obj_t BGl_z62zc3z04anonymousza32056ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_cmakezd2boxzd2zzcgen_copz00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static bool_t
		BGl_cfuncallzd2tailcallablezf3z21zzcgen_cgenz00(BgL_cfuncallz00_bglt);
	extern obj_t BGl_cifz00zzcgen_copz00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static BgL_copz00_bglt BGl_z62bdbzd2letzd2varz62zzcgen_cgenz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2literal1730z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2sequence1743z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_getzd2currentzd2bdbzd2loczd2zzcgen_emitzd2copzd2(void);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	static obj_t BGl_retblockzf2gotozf2zzcgen_cgenz00 = BUNSPEC;
	static obj_t BGl_argzd2typezd2zzcgen_cgenz00(obj_t);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2kwote1737z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	extern obj_t BGl_cboxzd2setz12zc0zzcgen_copz00;
	BGL_EXPORTED_DECL obj_t
		BGl_cgenzd2functionzd2zzcgen_cgenz00(BgL_globalz00_bglt);
	static BgL_retblockz00_bglt BGl_z62lambda2049z62zzcgen_cgenz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2setzd2exzd2it1765z81zzcgen_cgenz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcgen_cgenz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_cappz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_emitzd2copzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_copz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsync_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__regexpz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	extern bool_t BGl_emitzd2copzd2zzcgen_emitzd2copzd2(BgL_copz00_bglt);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza32611ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_pregexpzd2replaceza2z70zz__regexpz00(obj_t, obj_t,
		obj_t);
	static BgL_retblockz00_bglt BGl_z62lambda2052z62zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static BgL_retblockz00_bglt BGl_z62lambda2057z62zzcgen_cgenz00(obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_synczd2ze3sequencez31zzsync_nodez00(BgL_syncz00_bglt);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2patch1732z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2064z62zzcgen_cgenz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2065z62zzcgen_cgenz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_csetqz00zzcgen_copz00;
	extern obj_t BGl_localzd2varzd2zzcgen_copz00;
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_cblockz00zzcgen_copz00;
	static obj_t BGl_z62lambda2070z62zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62lambda2071z62zzcgen_cgenz00(obj_t, obj_t, obj_t);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2letzd2var1763z53zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_cswitchz00zzcgen_copz00;
	static obj_t BGl_cnstzd2initzd2zzcgen_cgenz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcgen_cgenz00(void);
	extern bool_t
		BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(BgL_globalz00_bglt);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	extern obj_t BGl_bdbzd2blockzd2zzcgen_copz00;
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzcgen_cgenz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcgen_cgenz00(void);
	static obj_t BGl_z62zc3z04anonymousza32177ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	extern bool_t BGl_sidezd2effectzf3z21zzeffect_effectz00(BgL_nodez00_bglt);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2closure1741z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_makezd2setqzd2kontz00zzcgen_cgenz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_catomz00zzcgen_copz00;
	extern obj_t BGl_retblockz00zzast_nodez00;
	BGL_EXPORTED_DEF obj_t BGl_za2returnzd2kontza2zd2zzcgen_cgenz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2boxzd2ref1775z53zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sfunzf2Czf2zzcgen_copz00;
	static BgL_copz00_bglt BGl_z62nodezd2ze3copz53zzcgen_cgenz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_privatez00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza32625ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2retblock1769z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_zc3z04anonymousza32313ze3ze70z60zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_cfailz00zzcgen_copz00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern obj_t BGl_stopz00zzcgen_copz00;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static bool_t BGl_stackablezf3ze70z14zzcgen_cgenz00(BgL_appz00_bglt);
	extern obj_t BGl_genpatchidz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_copz00_bglt BGl_z62nodezd2ze3copzd2var1739z81zzcgen_cgenz00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_creturnz00zzcgen_copz00;
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_cfuncallz00zzcgen_copz00;
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_ccastz00zzcgen_copz00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_cjumpzd2exzd2itz00zzcgen_copz00;
	static obj_t BGl_loopze70ze7zzcgen_cgenz00(obj_t);
	extern obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL BgL_copz00_bglt
		BGl_nodezd2ze3copz31zzcgen_cgenz00(BgL_nodez00_bglt, obj_t, bool_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62zc3z04anonymousza32637ze3ze5zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_za2czd2tailzd2callza2z00zzengine_paramz00;
	BGL_EXPORTED_DEF obj_t BGl_za2idzd2kontza2zd2zzcgen_cgenz00 = BUNSPEC;
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t BGl_blockzd2kontzd2zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_globalzd2ze3cz31zzcgen_cgenz00(BgL_globalz00_bglt);
	extern obj_t BGl_varcz00zzcgen_copz00;
	static bool_t BGl_iszd2pushzd2exitzf3zf3zzcgen_cgenz00(BgL_nodez00_bglt);
	extern obj_t BGl_patchz00zzast_nodez00;
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2return1771z81zzcgen_cgenz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t __cnst[19];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_nodezd2setqzd2envz00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2setqza7b2974za7, BGl_z62nodezd2setqzb0zzcgen_cgenz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bdbzd2letzd2varzd2envzd2zzcgen_cgenz00,
		BgL_bgl_za762bdbza7d2letza7d2v2975za7,
		BGl_z62bdbzd2letzd2varz62zzcgen_cgenz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop2976za7,
		BGl_z62nodezd2ze3copz53zzcgen_cgenz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2localzd2svarzf2namezd2envz20zzcgen_cgenz00,
		BgL_bgl_za762makeza7d2localza72977za7,
		BGl_z62makezd2localzd2svarzf2namez90zzcgen_cgenz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockzd2kontzd2envz00zzcgen_cgenz00,
		BgL_bgl_za762blockza7d2kontza72978za7, BGl_z62blockzd2kontzb0zzcgen_cgenz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2900z00zzcgen_cgenz00,
		BgL_bgl_string2900za700za7za7c2979za7, "[*]/", 4);
	      DEFINE_STRING(BGl_string2904z00zzcgen_cgenz00,
		BgL_bgl_string2904za700za7za7c2980za7, "/* ", 3);
	      DEFINE_STRING(BGl_string2905z00zzcgen_cgenz00,
		BgL_bgl_string2905za700za7za7c2981za7, "*|", 2);
	      DEFINE_STRING(BGl_string2906z00zzcgen_cgenz00,
		BgL_bgl_string2906za700za7za7c2982za7, " */", 3);
	      DEFINE_STRING(BGl_string2907z00zzcgen_cgenz00,
		BgL_bgl_string2907za700za7za7c2983za7, "{", 1);
	      DEFINE_STRING(BGl_string2908z00zzcgen_cgenz00,
		BgL_bgl_string2908za700za7za7c2984za7,
		"{ obj_t ___ = BUNSPEC; } /* bdb dummy init stmt */", 50);
	      DEFINE_STRING(BGl_string2909z00zzcgen_cgenz00,
		BgL_bgl_string2909za700za7za7c2985za7, "\n}", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2901z00zzcgen_cgenz00,
		BgL_bgl_za762za7c3za704za7a2retu2986z00,
		BGl_z62zc3z04za2returnzd2kontza2za31779ze3z37zzcgen_cgenz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2902z00zzcgen_cgenz00,
		BgL_bgl_za762za7c3za704za7a2idza7d2987za7,
		BGl_z62zc3z04za2idzd2kontza2za31837ze3z37zzcgen_cgenz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2903z00zzcgen_cgenz00,
		BgL_bgl_za762za7c3za704za7a2stop2988z00,
		BGl_z62zc3z04za2stopzd2kontza2za31838ze3z37zzcgen_cgenz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2910z00zzcgen_cgenz00,
		BgL_bgl_string2910za700za7za7c2989za7, "BGL_EXPORTED_DEF ", 17);
	      DEFINE_STRING(BGl_string2911z00zzcgen_cgenz00,
		BgL_bgl_string2911za700za7za7c2990za7, "(void)", 6);
	      DEFINE_STRING(BGl_string2912z00zzcgen_cgenz00,
		BgL_bgl_string2912za700za7za7c2991za7, "(", 1);
	      DEFINE_STRING(BGl_string2913z00zzcgen_cgenz00,
		BgL_bgl_string2913za700za7za7c2992za7, ")", 1);
	      DEFINE_STRING(BGl_string2914z00zzcgen_cgenz00,
		BgL_bgl_string2914za700za7za7c2993za7, ", ", 2);
	      DEFINE_STRING(BGl_string2915z00zzcgen_cgenz00,
		BgL_bgl_string2915za700za7za7c2994za7, "make-local-svar/name", 20);
	      DEFINE_STRING(BGl_string2916z00zzcgen_cgenz00,
		BgL_bgl_string2916za700za7za7c2995za7, "Illegal local name", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2917z00zzcgen_cgenz00,
		BgL_bgl_za762lambda2065za7622996z00, BGl_z62lambda2065z62zzcgen_cgenz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2918z00zzcgen_cgenz00,
		BgL_bgl_za762lambda2064za7622997z00, BGl_z62lambda2064z62zzcgen_cgenz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2919z00zzcgen_cgenz00,
		BgL_bgl_za762lambda2071za7622998z00, BGl_z62lambda2071z62zzcgen_cgenz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2926z00zzcgen_cgenz00,
		BgL_bgl_string2926za700za7za7c2999za7, "node->cop1727", 13);
	      DEFINE_STRING(BGl_string2927z00zzcgen_cgenz00,
		BgL_bgl_string2927za700za7za7c3000za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2929z00zzcgen_cgenz00,
		BgL_bgl_string2929za700za7za7c3001za7, "node->cop", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2920z00zzcgen_cgenz00,
		BgL_bgl_za762lambda2070za7623002z00, BGl_z62lambda2070z62zzcgen_cgenz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2921z00zzcgen_cgenz00,
		BgL_bgl_za762lambda2057za7623003z00, BGl_z62lambda2057z62zzcgen_cgenz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2922z00zzcgen_cgenz00,
		BgL_bgl_za762za7c3za704anonymo3004za7,
		BGl_z62zc3z04anonymousza32056ze3ze5zzcgen_cgenz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2923z00zzcgen_cgenz00,
		BgL_bgl_za762lambda2052za7623005z00, BGl_z62lambda2052z62zzcgen_cgenz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2924z00zzcgen_cgenz00,
		BgL_bgl_za762lambda2049za7623006z00, BGl_z62lambda2049z62zzcgen_cgenz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2925z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3007za7,
		BGl_z62nodezd2ze3cop1727z53zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cgenzd2functionzd2envz00zzcgen_cgenz00,
		BgL_bgl_za762cgenza7d2functi3008z00,
		BGl_z62cgenzd2functionzb0zzcgen_cgenz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2928z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3009za7,
		BGl_z62nodezd2ze3copzd2literal1730z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2930z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3010za7,
		BGl_z62nodezd2ze3copzd2patch1732z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2931z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3011za7,
		BGl_z62nodezd2ze3copzd2genpatchid1735z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2932z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3012za7,
		BGl_z62nodezd2ze3copzd2kwote1737z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2933z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3013za7,
		BGl_z62nodezd2ze3copzd2var1739z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2934z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3014za7,
		BGl_z62nodezd2ze3copzd2closure1741z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2935z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3015za7,
		BGl_z62nodezd2ze3copzd2sequence1743z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2936z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3016za7,
		BGl_z62nodezd2ze3copzd2sync1745z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2937z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3017za7,
		BGl_z62nodezd2ze3copzd2pragma1747z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2938z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3018za7,
		BGl_z62nodezd2ze3copzd2private1749z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2939z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3019za7,
		BGl_z62nodezd2ze3copzd2cast1751z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2940z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3020za7,
		BGl_z62nodezd2ze3copzd2setq1753z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2941z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3021za7,
		BGl_z62nodezd2ze3copzd2conditiona1755z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2942z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3022za7,
		BGl_z62nodezd2ze3copzd2fail1757z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2943z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3023za7,
		BGl_z62nodezd2ze3copzd2switch1759z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2944z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3024za7,
		BGl_z62nodezd2ze3copzd2letzd2fun1761z53zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2945z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3025za7,
		BGl_z62nodezd2ze3copzd2letzd2var1763z53zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2946z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3026za7,
		BGl_z62nodezd2ze3copzd2setzd2exzd2it1765z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2947z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3027za7,
		BGl_z62nodezd2ze3copzd2jumpzd2exzd2it1767z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2954z00zzcgen_cgenz00,
		BgL_bgl_string2954za700za7za7c3028za7, "jmp_buf_t jmpbuf", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2948z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3029za7,
		BGl_z62nodezd2ze3copzd2retblock1769z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2955z00zzcgen_cgenz00,
		BgL_bgl_string2955za700za7za7c3030za7, ")jmpbuf", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2949z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3031za7,
		BGl_z62nodezd2ze3copzd2return1771z81zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string2956z00zzcgen_cgenz00,
		BgL_bgl_string2956za700za7za7c3032za7, "\n", 1);
	      DEFINE_STRING(BGl_string2957z00zzcgen_cgenz00,
		BgL_bgl_string2957za700za7za7c3033za7, " ", 1);
	      DEFINE_STRING(BGl_string2958z00zzcgen_cgenz00,
		BgL_bgl_string2958za700za7za7c3034za7, "      stack allocation \"", 24);
	      DEFINE_STRING(BGl_string2959z00zzcgen_cgenz00,
		BgL_bgl_string2959za700za7za7c3035za7,
		"      stack allocation \"make-cell\" ", 35);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2950z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3036za7,
		BGl_z62nodezd2ze3copzd2makezd2box1773z53zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2951z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3037za7,
		BGl_z62nodezd2ze3copzd2boxzd2ref1775z53zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2952z00zzcgen_cgenz00,
		BgL_bgl_za762nodeza7d2za7e3cop3038za7,
		BGl_z62nodezd2ze3copzd2boxzd2setz121777z41zzcgen_cgenz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2953z00zzcgen_cgenz00,
		BgL_bgl_za762za7c3za704anonymo3039za7,
		BGl_z62zc3z04anonymousza32611ze3ze5zzcgen_cgenz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2960z00zzcgen_cgenz00,
		BgL_bgl_string2960za700za7za7c3040za7, "struct bgl_cell ~a", 18);
	      DEFINE_STRING(BGl_string2961z00zzcgen_cgenz00,
		BgL_bgl_string2961za700za7za7c3041za7, "############## IGNORING PRAGMA ",
		31);
	      DEFINE_STRING(BGl_string2962z00zzcgen_cgenz00,
		BgL_bgl_string2962za700za7za7c3042za7, ":", 1);
	      DEFINE_STRING(BGl_string2963z00zzcgen_cgenz00,
		BgL_bgl_string2963za700za7za7c3043za7, ",", 1);
	      DEFINE_STRING(BGl_string2964z00zzcgen_cgenz00,
		BgL_bgl_string2964za700za7za7c3044za7, "Cgen/cgen.scm", 13);
	      DEFINE_STRING(BGl_string2965z00zzcgen_cgenz00,
		BgL_bgl_string2965za700za7za7c3045za7, "/tmp/bigloo/comptime/Cgen/cgen.scm",
		34);
	      DEFINE_STRING(BGl_string2966z00zzcgen_cgenz00,
		BgL_bgl_string2966za700za7za7c3046za7, "node->cop-pragma1747", 20);
	      DEFINE_STRING(BGl_string2967z00zzcgen_cgenz00,
		BgL_bgl_string2967za700za7za7c3047za7, "type", 4);
	      DEFINE_STRING(BGl_string2968z00zzcgen_cgenz00,
		BgL_bgl_string2968za700za7za7c3048za7, "Unexpected `closure' node", 25);
	      DEFINE_STRING(BGl_string2969z00zzcgen_cgenz00,
		BgL_bgl_string2969za700za7za7c3049za7, "Unexpected `kwote' node", 23);
	      DEFINE_STRING(BGl_string2970z00zzcgen_cgenz00,
		BgL_bgl_string2970za700za7za7c3050za7,
		"BGL_PATCHABLE_CONSTANT_~a($1, $2, $3)", 37);
	      DEFINE_STRING(BGl_string2971z00zzcgen_cgenz00,
		BgL_bgl_string2971za700za7za7c3051za7, "cgen_cgen", 9);
	      DEFINE_STRING(BGl_string2972z00zzcgen_cgenz00,
		BgL_bgl_string2972za700za7za7c3052za7,
		"elong-size test box have-c99-stack-alloc bigloo-c exit __return __retval cellval node->cop1727 cgen_cgen retblock/goto label obj local aux push-exit! export location ",
		166);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_za2voidzd2kontza2zd2envz00zzcgen_cgenz00,
		BgL_bgl_za762za7a2voidza7d2kon3053za7,
		BGl_z62za2voidzd2kontza2zb0zzcgen_cgenz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2stopzd2kontza2zd2zzcgen_cgenz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcgen_cgenz00));
		   
			 ADD_ROOT((void *) (&BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00));
		     ADD_ROOT((void *) (&BGl_pregexp1685z00zzcgen_cgenz00));
		     ADD_ROOT((void *) (&BGl_retblockzf2gotozf2zzcgen_cgenz00));
		     ADD_ROOT((void *) (&BGl_za2returnzd2kontza2zd2zzcgen_cgenz00));
		     ADD_ROOT((void *) (&BGl_za2idzd2kontza2zd2zzcgen_cgenz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcgen_cgenz00(long
		BgL_checksumz00_7661, char *BgL_fromz00_7662)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcgen_cgenz00))
				{
					BGl_requirezd2initializa7ationz75zzcgen_cgenz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcgen_cgenz00();
					BGl_libraryzd2moduleszd2initz00zzcgen_cgenz00();
					BGl_cnstzd2initzd2zzcgen_cgenz00();
					BGl_importedzd2moduleszd2initz00zzcgen_cgenz00();
					BGl_objectzd2initzd2zzcgen_cgenz00();
					BGl_genericzd2initzd2zzcgen_cgenz00();
					BGl_methodzd2initzd2zzcgen_cgenz00();
					return BGl_toplevelzd2initzd2zzcgen_cgenz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__regexpz00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cgen_cgen");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cgen_cgen");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 15 */
			{	/* Cgen/cgen.scm 15 */
				obj_t BgL_cportz00_6722;

				{	/* Cgen/cgen.scm 15 */
					obj_t BgL_stringz00_6729;

					BgL_stringz00_6729 = BGl_string2972z00zzcgen_cgenz00;
					{	/* Cgen/cgen.scm 15 */
						obj_t BgL_startz00_6730;

						BgL_startz00_6730 = BINT(0L);
						{	/* Cgen/cgen.scm 15 */
							obj_t BgL_endz00_6731;

							BgL_endz00_6731 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6729)));
							{	/* Cgen/cgen.scm 15 */

								BgL_cportz00_6722 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6729, BgL_startz00_6730, BgL_endz00_6731);
				}}}}
				{
					long BgL_iz00_6723;

					BgL_iz00_6723 = 18L;
				BgL_loopz00_6724:
					if ((BgL_iz00_6723 == -1L))
						{	/* Cgen/cgen.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cgen/cgen.scm 15 */
							{	/* Cgen/cgen.scm 15 */
								obj_t BgL_arg2973z00_6725;

								{	/* Cgen/cgen.scm 15 */

									{	/* Cgen/cgen.scm 15 */
										obj_t BgL_locationz00_6727;

										BgL_locationz00_6727 = BBOOL(((bool_t) 0));
										{	/* Cgen/cgen.scm 15 */

											BgL_arg2973z00_6725 =
												BGl_readz00zz__readerz00(BgL_cportz00_6722,
												BgL_locationz00_6727);
										}
									}
								}
								{	/* Cgen/cgen.scm 15 */
									int BgL_tmpz00_7700;

									BgL_tmpz00_7700 = (int) (BgL_iz00_6723);
									CNST_TABLE_SET(BgL_tmpz00_7700, BgL_arg2973z00_6725);
							}}
							{	/* Cgen/cgen.scm 15 */
								int BgL_auxz00_6728;

								BgL_auxz00_6728 = (int) ((BgL_iz00_6723 - 1L));
								{
									long BgL_iz00_7705;

									BgL_iz00_7705 = (long) (BgL_auxz00_6728);
									BgL_iz00_6723 = BgL_iz00_7705;
									goto BgL_loopz00_6724;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 15 */
			BGl_pregexp1685z00zzcgen_cgenz00 =
				BGl_pregexpz00zz__regexpz00(BGl_string2900z00zzcgen_cgenz00, BNIL);
			BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00 = BUNSPEC;
			BGl_za2returnzd2kontza2zd2zzcgen_cgenz00 = BGl_proc2901z00zzcgen_cgenz00;
			BGl_za2idzd2kontza2zd2zzcgen_cgenz00 = BGl_proc2902z00zzcgen_cgenz00;
			BGl_za2stopzd2kontza2zd2zzcgen_cgenz00 = BGl_proc2903z00zzcgen_cgenz00;
			return BUNSPEC;
		}

	}



/* &<@*stop-kont*:1838> */
	obj_t BGl_z62zc3z04za2stopzd2kontza2za31838ze3z37zzcgen_cgenz00(obj_t
		BgL_envz00_6494, obj_t BgL_copz00_6495)
	{
		{	/* Cgen/cgen.scm 282 */
			{
				BgL_stopz00_bglt BgL_auxz00_7709;

				{	/* Cgen/cgen.scm 284 */
					BgL_stopz00_bglt BgL_new1186z00_6733;

					{	/* Cgen/cgen.scm 284 */
						BgL_stopz00_bglt BgL_new1185z00_6734;

						BgL_new1185z00_6734 =
							((BgL_stopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_stopz00_bgl))));
						{	/* Cgen/cgen.scm 284 */
							long BgL_arg1839z00_6735;

							BgL_arg1839z00_6735 = BGL_CLASS_NUM(BGl_stopz00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1185z00_6734),
								BgL_arg1839z00_6735);
						}
						BgL_new1186z00_6733 = BgL_new1185z00_6734;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1186z00_6733)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1186z00_6733)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
					((((BgL_stopz00_bglt) COBJECT(BgL_new1186z00_6733))->BgL_valuez00) =
						((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_copz00_6495)), BUNSPEC);
					BgL_auxz00_7709 = BgL_new1186z00_6733;
				}
				return ((obj_t) BgL_auxz00_7709);
			}
		}

	}



/* &<@*id-kont*:1837> */
	obj_t BGl_z62zc3z04za2idzd2kontza2za31837ze3z37zzcgen_cgenz00(obj_t
		BgL_envz00_6496, obj_t BgL_copz00_6497)
	{
		{	/* Cgen/cgen.scm 268 */
			return BgL_copz00_6497;
		}

	}



/* &<@*return-kont*:1779> */
	obj_t BGl_z62zc3z04za2returnzd2kontza2za31779ze3z37zzcgen_cgenz00(obj_t
		BgL_envz00_6498, obj_t BgL_copz00_6499)
	{
		{	/* Cgen/cgen.scm 237 */
			{
				BgL_creturnz00_bglt BgL_auxz00_7722;

				{	/* Cgen/cgen.scm 239 */
					BgL_creturnz00_bglt BgL_new1173z00_6736;

					{	/* Cgen/cgen.scm 241 */
						BgL_creturnz00_bglt BgL_new1172z00_6737;

						BgL_new1172z00_6737 =
							((BgL_creturnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_creturnz00_bgl))));
						{	/* Cgen/cgen.scm 241 */
							long BgL_arg1836z00_6738;

							BgL_arg1836z00_6738 = BGL_CLASS_NUM(BGl_creturnz00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1172z00_6737),
								BgL_arg1836z00_6738);
						}
						BgL_new1173z00_6736 = BgL_new1172z00_6737;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1173z00_6736)))->BgL_locz00) =
						((obj_t) (((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_copz00_6499)))->BgL_locz00)), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1173z00_6736)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
					{
						bool_t BgL_auxz00_7734;

						if (CBOOL(BGl_za2czd2tailzd2callza2z00zzengine_paramz00))
							{	/* Cgen/cgen.scm 243 */
								bool_t BgL__ortest_1174z00_6739;

								{	/* Cgen/cgen.scm 243 */
									bool_t BgL_test3057z00_7737;

									{	/* Cgen/cgen.scm 243 */
										obj_t BgL_classz00_6740;

										BgL_classz00_6740 = BGl_capplyz00zzcgen_copz00;
										if (BGL_OBJECTP(BgL_copz00_6499))
											{	/* Cgen/cgen.scm 243 */
												BgL_objectz00_bglt BgL_arg1807z00_6741;

												BgL_arg1807z00_6741 =
													(BgL_objectz00_bglt) (BgL_copz00_6499);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cgen/cgen.scm 243 */
														long BgL_idxz00_6742;

														BgL_idxz00_6742 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6741);
														{	/* Cgen/cgen.scm 243 */
															obj_t BgL_arg1800z00_6743;

															{	/* Cgen/cgen.scm 243 */
																long BgL_arg1801z00_6744;

																BgL_arg1801z00_6744 = (BgL_idxz00_6742 + 2L);
																{	/* Cgen/cgen.scm 243 */
																	obj_t BgL_vectorz00_6745;

																	BgL_vectorz00_6745 =
																		BGl_za2inheritancesza2z00zz__objectz00;
																	BgL_arg1800z00_6743 =
																		VECTOR_REF(BgL_vectorz00_6745,
																		BgL_arg1801z00_6744);
															}}
															BgL_test3057z00_7737 =
																(BgL_arg1800z00_6743 == BgL_classz00_6740);
													}}
												else
													{	/* Cgen/cgen.scm 243 */
														bool_t BgL_res2801z00_6746;

														{	/* Cgen/cgen.scm 243 */
															obj_t BgL_oclassz00_6747;

															{	/* Cgen/cgen.scm 243 */
																obj_t BgL_arg1815z00_6748;
																long BgL_arg1816z00_6749;

																BgL_arg1815z00_6748 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cgen/cgen.scm 243 */
																	long BgL_arg1817z00_6750;

																	BgL_arg1817z00_6750 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6741);
																	BgL_arg1816z00_6749 =
																		(BgL_arg1817z00_6750 - OBJECT_TYPE);
																}
																BgL_oclassz00_6747 =
																	VECTOR_REF(BgL_arg1815z00_6748,
																	BgL_arg1816z00_6749);
															}
															{	/* Cgen/cgen.scm 243 */
																bool_t BgL__ortest_1115z00_6751;

																BgL__ortest_1115z00_6751 =
																	(BgL_classz00_6740 == BgL_oclassz00_6747);
																if (BgL__ortest_1115z00_6751)
																	{	/* Cgen/cgen.scm 243 */
																		BgL_res2801z00_6746 =
																			BgL__ortest_1115z00_6751;
																	}
																else
																	{	/* Cgen/cgen.scm 243 */
																		long BgL_odepthz00_6752;

																		{	/* Cgen/cgen.scm 243 */
																			obj_t BgL_arg1804z00_6753;

																			BgL_arg1804z00_6753 =
																				(BgL_oclassz00_6747);
																			BgL_odepthz00_6752 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6753);
																		}
																		if ((2L < BgL_odepthz00_6752))
																			{	/* Cgen/cgen.scm 243 */
																				obj_t BgL_arg1802z00_6754;

																				{	/* Cgen/cgen.scm 243 */
																					obj_t BgL_arg1803z00_6755;

																					BgL_arg1803z00_6755 =
																						(BgL_oclassz00_6747);
																					BgL_arg1802z00_6754 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6755, 2L);
																				}
																				BgL_res2801z00_6746 =
																					(BgL_arg1802z00_6754 ==
																					BgL_classz00_6740);
																			}
																		else
																			{	/* Cgen/cgen.scm 243 */
																				BgL_res2801z00_6746 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3057z00_7737 = BgL_res2801z00_6746;
													}
											}
										else
											{	/* Cgen/cgen.scm 243 */
												BgL_test3057z00_7737 = ((bool_t) 0);
											}
									}
									if (BgL_test3057z00_7737)
										{	/* Cgen/cgen.scm 243 */
											BgL__ortest_1174z00_6739 =
												BGl_capplyzd2tailcallablezf3z21zzcgen_cgenz00(
												((BgL_capplyz00_bglt) BgL_copz00_6499));
										}
									else
										{	/* Cgen/cgen.scm 243 */
											BgL__ortest_1174z00_6739 = ((bool_t) 0);
										}
								}
								if (BgL__ortest_1174z00_6739)
									{	/* Cgen/cgen.scm 243 */
										BgL_auxz00_7734 = BgL__ortest_1174z00_6739;
									}
								else
									{	/* Cgen/cgen.scm 244 */
										bool_t BgL__ortest_1175z00_6756;

										{	/* Cgen/cgen.scm 244 */
											bool_t BgL_test3063z00_7763;

											{	/* Cgen/cgen.scm 244 */
												obj_t BgL_classz00_6757;

												BgL_classz00_6757 = BGl_cfuncallz00zzcgen_copz00;
												if (BGL_OBJECTP(BgL_copz00_6499))
													{	/* Cgen/cgen.scm 244 */
														BgL_objectz00_bglt BgL_arg1807z00_6758;

														BgL_arg1807z00_6758 =
															(BgL_objectz00_bglt) (BgL_copz00_6499);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Cgen/cgen.scm 244 */
																long BgL_idxz00_6759;

																BgL_idxz00_6759 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_6758);
																{	/* Cgen/cgen.scm 244 */
																	obj_t BgL_arg1800z00_6760;

																	{	/* Cgen/cgen.scm 244 */
																		long BgL_arg1801z00_6761;

																		BgL_arg1801z00_6761 =
																			(BgL_idxz00_6759 + 2L);
																		{	/* Cgen/cgen.scm 244 */
																			obj_t BgL_vectorz00_6762;

																			BgL_vectorz00_6762 =
																				BGl_za2inheritancesza2z00zz__objectz00;
																			BgL_arg1800z00_6760 =
																				VECTOR_REF(BgL_vectorz00_6762,
																				BgL_arg1801z00_6761);
																	}}
																	BgL_test3063z00_7763 =
																		(BgL_arg1800z00_6760 == BgL_classz00_6757);
															}}
														else
															{	/* Cgen/cgen.scm 244 */
																bool_t BgL_res2802z00_6763;

																{	/* Cgen/cgen.scm 244 */
																	obj_t BgL_oclassz00_6764;

																	{	/* Cgen/cgen.scm 244 */
																		obj_t BgL_arg1815z00_6765;
																		long BgL_arg1816z00_6766;

																		BgL_arg1815z00_6765 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Cgen/cgen.scm 244 */
																			long BgL_arg1817z00_6767;

																			BgL_arg1817z00_6767 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_6758);
																			BgL_arg1816z00_6766 =
																				(BgL_arg1817z00_6767 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_6764 =
																			VECTOR_REF(BgL_arg1815z00_6765,
																			BgL_arg1816z00_6766);
																	}
																	{	/* Cgen/cgen.scm 244 */
																		bool_t BgL__ortest_1115z00_6768;

																		BgL__ortest_1115z00_6768 =
																			(BgL_classz00_6757 == BgL_oclassz00_6764);
																		if (BgL__ortest_1115z00_6768)
																			{	/* Cgen/cgen.scm 244 */
																				BgL_res2802z00_6763 =
																					BgL__ortest_1115z00_6768;
																			}
																		else
																			{	/* Cgen/cgen.scm 244 */
																				long BgL_odepthz00_6769;

																				{	/* Cgen/cgen.scm 244 */
																					obj_t BgL_arg1804z00_6770;

																					BgL_arg1804z00_6770 =
																						(BgL_oclassz00_6764);
																					BgL_odepthz00_6769 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_6770);
																				}
																				if ((2L < BgL_odepthz00_6769))
																					{	/* Cgen/cgen.scm 244 */
																						obj_t BgL_arg1802z00_6771;

																						{	/* Cgen/cgen.scm 244 */
																							obj_t BgL_arg1803z00_6772;

																							BgL_arg1803z00_6772 =
																								(BgL_oclassz00_6764);
																							BgL_arg1802z00_6771 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_6772, 2L);
																						}
																						BgL_res2802z00_6763 =
																							(BgL_arg1802z00_6771 ==
																							BgL_classz00_6757);
																					}
																				else
																					{	/* Cgen/cgen.scm 244 */
																						BgL_res2802z00_6763 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3063z00_7763 = BgL_res2802z00_6763;
															}
													}
												else
													{	/* Cgen/cgen.scm 244 */
														BgL_test3063z00_7763 = ((bool_t) 0);
													}
											}
											if (BgL_test3063z00_7763)
												{	/* Cgen/cgen.scm 244 */
													BgL__ortest_1175z00_6756 =
														BGl_cfuncallzd2tailcallablezf3z21zzcgen_cgenz00(
														((BgL_cfuncallz00_bglt) BgL_copz00_6499));
												}
											else
												{	/* Cgen/cgen.scm 244 */
													BgL__ortest_1175z00_6756 = ((bool_t) 0);
												}
										}
										if (BgL__ortest_1175z00_6756)
											{	/* Cgen/cgen.scm 244 */
												BgL_auxz00_7734 = BgL__ortest_1175z00_6756;
											}
										else
											{	/* Cgen/cgen.scm 245 */
												bool_t BgL_test3069z00_7789;

												{	/* Cgen/cgen.scm 245 */
													bool_t BgL_test3070z00_7790;

													{	/* Cgen/cgen.scm 245 */
														obj_t BgL_classz00_6773;

														BgL_classz00_6773 = BGl_cappz00zzcgen_copz00;
														if (BGL_OBJECTP(BgL_copz00_6499))
															{	/* Cgen/cgen.scm 245 */
																BgL_objectz00_bglt BgL_arg1807z00_6774;

																BgL_arg1807z00_6774 =
																	(BgL_objectz00_bglt) (BgL_copz00_6499);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Cgen/cgen.scm 245 */
																		long BgL_idxz00_6775;

																		BgL_idxz00_6775 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_6774);
																		{	/* Cgen/cgen.scm 245 */
																			obj_t BgL_arg1800z00_6776;

																			{	/* Cgen/cgen.scm 245 */
																				long BgL_arg1801z00_6777;

																				BgL_arg1801z00_6777 =
																					(BgL_idxz00_6775 + 2L);
																				{	/* Cgen/cgen.scm 245 */
																					obj_t BgL_vectorz00_6778;

																					BgL_vectorz00_6778 =
																						BGl_za2inheritancesza2z00zz__objectz00;
																					BgL_arg1800z00_6776 =
																						VECTOR_REF(BgL_vectorz00_6778,
																						BgL_arg1801z00_6777);
																			}}
																			BgL_test3070z00_7790 =
																				(BgL_arg1800z00_6776 ==
																				BgL_classz00_6773);
																	}}
																else
																	{	/* Cgen/cgen.scm 245 */
																		bool_t BgL_res2803z00_6779;

																		{	/* Cgen/cgen.scm 245 */
																			obj_t BgL_oclassz00_6780;

																			{	/* Cgen/cgen.scm 245 */
																				obj_t BgL_arg1815z00_6781;
																				long BgL_arg1816z00_6782;

																				BgL_arg1815z00_6781 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Cgen/cgen.scm 245 */
																					long BgL_arg1817z00_6783;

																					BgL_arg1817z00_6783 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_6774);
																					BgL_arg1816z00_6782 =
																						(BgL_arg1817z00_6783 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_6780 =
																					VECTOR_REF(BgL_arg1815z00_6781,
																					BgL_arg1816z00_6782);
																			}
																			{	/* Cgen/cgen.scm 245 */
																				bool_t BgL__ortest_1115z00_6784;

																				BgL__ortest_1115z00_6784 =
																					(BgL_classz00_6773 ==
																					BgL_oclassz00_6780);
																				if (BgL__ortest_1115z00_6784)
																					{	/* Cgen/cgen.scm 245 */
																						BgL_res2803z00_6779 =
																							BgL__ortest_1115z00_6784;
																					}
																				else
																					{	/* Cgen/cgen.scm 245 */
																						long BgL_odepthz00_6785;

																						{	/* Cgen/cgen.scm 245 */
																							obj_t BgL_arg1804z00_6786;

																							BgL_arg1804z00_6786 =
																								(BgL_oclassz00_6780);
																							BgL_odepthz00_6785 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_6786);
																						}
																						if ((2L < BgL_odepthz00_6785))
																							{	/* Cgen/cgen.scm 245 */
																								obj_t BgL_arg1802z00_6787;

																								{	/* Cgen/cgen.scm 245 */
																									obj_t BgL_arg1803z00_6788;

																									BgL_arg1803z00_6788 =
																										(BgL_oclassz00_6780);
																									BgL_arg1802z00_6787 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_6788, 2L);
																								}
																								BgL_res2803z00_6779 =
																									(BgL_arg1802z00_6787 ==
																									BgL_classz00_6773);
																							}
																						else
																							{	/* Cgen/cgen.scm 245 */
																								BgL_res2803z00_6779 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test3070z00_7790 = BgL_res2803z00_6779;
																	}
															}
														else
															{	/* Cgen/cgen.scm 245 */
																BgL_test3070z00_7790 = ((bool_t) 0);
															}
													}
													if (BgL_test3070z00_7790)
														{	/* Cgen/cgen.scm 245 */
															BgL_test3069z00_7789 =
																BGl_cappzd2tailcallablezf3z21zzcgen_cgenz00(
																((BgL_cappz00_bglt) BgL_copz00_6499));
														}
													else
														{	/* Cgen/cgen.scm 245 */
															BgL_test3069z00_7789 = ((bool_t) 0);
														}
												}
												if (BgL_test3069z00_7789)
													{	/* Cgen/cgen.scm 246 */
														BgL_variablez00_bglt BgL_funz00_6789;

														BgL_funz00_6789 =
															(((BgL_varcz00_bglt) COBJECT(
																	((BgL_varcz00_bglt)
																		(((BgL_cappz00_bglt) COBJECT(
																					((BgL_cappz00_bglt)
																						BgL_copz00_6499)))->BgL_funz00))))->
															BgL_variablez00);
														{	/* Cgen/cgen.scm 248 */
															bool_t BgL_test3075z00_7819;

															{	/* Cgen/cgen.scm 248 */
																BgL_valuez00_bglt BgL_arg1822z00_6790;

																BgL_arg1822z00_6790 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_funz00_6789))))->BgL_valuez00);
																{	/* Cgen/cgen.scm 248 */
																	obj_t BgL_classz00_6791;

																	BgL_classz00_6791 = BGl_cfunz00zzast_varz00;
																	{	/* Cgen/cgen.scm 248 */
																		BgL_objectz00_bglt BgL_arg1807z00_6792;

																		{	/* Cgen/cgen.scm 248 */
																			obj_t BgL_tmpz00_7823;

																			BgL_tmpz00_7823 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_arg1822z00_6790));
																			BgL_arg1807z00_6792 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_7823);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Cgen/cgen.scm 248 */
																				long BgL_idxz00_6793;

																				BgL_idxz00_6793 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_6792);
																				{	/* Cgen/cgen.scm 248 */
																					obj_t BgL_arg1800z00_6794;

																					{	/* Cgen/cgen.scm 248 */
																						long BgL_arg1801z00_6795;

																						BgL_arg1801z00_6795 =
																							(BgL_idxz00_6793 + 3L);
																						{	/* Cgen/cgen.scm 248 */
																							obj_t BgL_vectorz00_6796;

																							BgL_vectorz00_6796 =
																								BGl_za2inheritancesza2z00zz__objectz00;
																							BgL_arg1800z00_6794 =
																								VECTOR_REF(BgL_vectorz00_6796,
																								BgL_arg1801z00_6795);
																					}}
																					BgL_test3075z00_7819 =
																						(BgL_arg1800z00_6794 ==
																						BgL_classz00_6791);
																			}}
																		else
																			{	/* Cgen/cgen.scm 248 */
																				bool_t BgL_res2804z00_6797;

																				{	/* Cgen/cgen.scm 248 */
																					obj_t BgL_oclassz00_6798;

																					{	/* Cgen/cgen.scm 248 */
																						obj_t BgL_arg1815z00_6799;
																						long BgL_arg1816z00_6800;

																						BgL_arg1815z00_6799 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Cgen/cgen.scm 248 */
																							long BgL_arg1817z00_6801;

																							BgL_arg1817z00_6801 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_6792);
																							BgL_arg1816z00_6800 =
																								(BgL_arg1817z00_6801 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_6798 =
																							VECTOR_REF(BgL_arg1815z00_6799,
																							BgL_arg1816z00_6800);
																					}
																					{	/* Cgen/cgen.scm 248 */
																						bool_t BgL__ortest_1115z00_6802;

																						BgL__ortest_1115z00_6802 =
																							(BgL_classz00_6791 ==
																							BgL_oclassz00_6798);
																						if (BgL__ortest_1115z00_6802)
																							{	/* Cgen/cgen.scm 248 */
																								BgL_res2804z00_6797 =
																									BgL__ortest_1115z00_6802;
																							}
																						else
																							{	/* Cgen/cgen.scm 248 */
																								long BgL_odepthz00_6803;

																								{	/* Cgen/cgen.scm 248 */
																									obj_t BgL_arg1804z00_6804;

																									BgL_arg1804z00_6804 =
																										(BgL_oclassz00_6798);
																									BgL_odepthz00_6803 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_6804);
																								}
																								if ((3L < BgL_odepthz00_6803))
																									{	/* Cgen/cgen.scm 248 */
																										obj_t BgL_arg1802z00_6805;

																										{	/* Cgen/cgen.scm 248 */
																											obj_t BgL_arg1803z00_6806;

																											BgL_arg1803z00_6806 =
																												(BgL_oclassz00_6798);
																											BgL_arg1802z00_6805 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_6806,
																												3L);
																										}
																										BgL_res2804z00_6797 =
																											(BgL_arg1802z00_6805 ==
																											BgL_classz00_6791);
																									}
																								else
																									{	/* Cgen/cgen.scm 248 */
																										BgL_res2804z00_6797 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test3075z00_7819 =
																					BgL_res2804z00_6797;
																			}
																	}
																}
															}
															if (BgL_test3075z00_7819)
																{	/* Cgen/cgen.scm 248 */
																	if (
																		(((BgL_cfunz00_bglt) COBJECT(
																					((BgL_cfunz00_bglt)
																						(((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										((BgL_globalz00_bglt)
																											BgL_funz00_6789))))->
																							BgL_valuez00))))->
																			BgL_infixzf3zf3))
																		{	/* Cgen/cgen.scm 249 */
																			BgL_auxz00_7734 = ((bool_t) 0);
																		}
																	else
																		{	/* Cgen/cgen.scm 249 */
																			if (
																				(((BgL_cfunz00_bglt) COBJECT(
																							((BgL_cfunz00_bglt)
																								(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt) (
																													(BgL_globalz00_bglt)
																													BgL_funz00_6789))))->
																									BgL_valuez00))))->
																					BgL_macrozf3zf3))
																				{	/* Cgen/cgen.scm 250 */
																					BgL_auxz00_7734 = ((bool_t) 0);
																				}
																			else
																				{	/* Cgen/cgen.scm 250 */
																					BgL_auxz00_7734 = ((bool_t) 1);
																				}
																		}
																}
															else
																{	/* Cgen/cgen.scm 248 */
																	BgL_auxz00_7734 = ((bool_t) 1);
																}
														}
													}
												else
													{	/* Cgen/cgen.scm 245 */
														BgL_auxz00_7734 = ((bool_t) 0);
													}
											}
									}
							}
						else
							{	/* Cgen/cgen.scm 242 */
								BgL_auxz00_7734 = ((bool_t) 0);
							}
						((((BgL_creturnz00_bglt) COBJECT(BgL_new1173z00_6736))->
								BgL_tailz00) = ((bool_t) BgL_auxz00_7734), BUNSPEC);
					}
					{
						BgL_copz00_bglt BgL_auxz00_7859;

						{	/* Cgen/cgen.scm 253 */
							bool_t BgL_test3081z00_7860;

							{	/* Cgen/cgen.scm 253 */
								obj_t BgL_classz00_6807;

								BgL_classz00_6807 = BGl_csetqz00zzcgen_copz00;
								if (BGL_OBJECTP(BgL_copz00_6499))
									{	/* Cgen/cgen.scm 253 */
										BgL_objectz00_bglt BgL_arg1807z00_6808;

										BgL_arg1807z00_6808 =
											(BgL_objectz00_bglt) (BgL_copz00_6499);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cgen/cgen.scm 253 */
												long BgL_idxz00_6809;

												BgL_idxz00_6809 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6808);
												{	/* Cgen/cgen.scm 253 */
													obj_t BgL_arg1800z00_6810;

													{	/* Cgen/cgen.scm 253 */
														long BgL_arg1801z00_6811;

														BgL_arg1801z00_6811 = (BgL_idxz00_6809 + 2L);
														{	/* Cgen/cgen.scm 253 */
															obj_t BgL_vectorz00_6812;

															BgL_vectorz00_6812 =
																BGl_za2inheritancesza2z00zz__objectz00;
															BgL_arg1800z00_6810 =
																VECTOR_REF(BgL_vectorz00_6812,
																BgL_arg1801z00_6811);
													}}
													BgL_test3081z00_7860 =
														(BgL_arg1800z00_6810 == BgL_classz00_6807);
											}}
										else
											{	/* Cgen/cgen.scm 253 */
												bool_t BgL_res2805z00_6813;

												{	/* Cgen/cgen.scm 253 */
													obj_t BgL_oclassz00_6814;

													{	/* Cgen/cgen.scm 253 */
														obj_t BgL_arg1815z00_6815;
														long BgL_arg1816z00_6816;

														BgL_arg1815z00_6815 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cgen/cgen.scm 253 */
															long BgL_arg1817z00_6817;

															BgL_arg1817z00_6817 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6808);
															BgL_arg1816z00_6816 =
																(BgL_arg1817z00_6817 - OBJECT_TYPE);
														}
														BgL_oclassz00_6814 =
															VECTOR_REF(BgL_arg1815z00_6815,
															BgL_arg1816z00_6816);
													}
													{	/* Cgen/cgen.scm 253 */
														bool_t BgL__ortest_1115z00_6818;

														BgL__ortest_1115z00_6818 =
															(BgL_classz00_6807 == BgL_oclassz00_6814);
														if (BgL__ortest_1115z00_6818)
															{	/* Cgen/cgen.scm 253 */
																BgL_res2805z00_6813 = BgL__ortest_1115z00_6818;
															}
														else
															{	/* Cgen/cgen.scm 253 */
																long BgL_odepthz00_6819;

																{	/* Cgen/cgen.scm 253 */
																	obj_t BgL_arg1804z00_6820;

																	BgL_arg1804z00_6820 = (BgL_oclassz00_6814);
																	BgL_odepthz00_6819 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_6820);
																}
																if ((2L < BgL_odepthz00_6819))
																	{	/* Cgen/cgen.scm 253 */
																		obj_t BgL_arg1802z00_6821;

																		{	/* Cgen/cgen.scm 253 */
																			obj_t BgL_arg1803z00_6822;

																			BgL_arg1803z00_6822 =
																				(BgL_oclassz00_6814);
																			BgL_arg1802z00_6821 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_6822, 2L);
																		}
																		BgL_res2805z00_6813 =
																			(BgL_arg1802z00_6821 ==
																			BgL_classz00_6807);
																	}
																else
																	{	/* Cgen/cgen.scm 253 */
																		BgL_res2805z00_6813 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3081z00_7860 = BgL_res2805z00_6813;
											}
									}
								else
									{	/* Cgen/cgen.scm 253 */
										BgL_test3081z00_7860 = ((bool_t) 0);
									}
							}
							if (BgL_test3081z00_7860)
								{	/* Cgen/cgen.scm 254 */
									BgL_csequencez00_bglt BgL_new1179z00_6823;

									{	/* Cgen/cgen.scm 256 */
										BgL_csequencez00_bglt BgL_new1178z00_6824;

										BgL_new1178z00_6824 =
											((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_csequencez00_bgl))));
										{	/* Cgen/cgen.scm 256 */
											long BgL_arg1835z00_6825;

											{	/* Cgen/cgen.scm 256 */
												obj_t BgL_classz00_6826;

												BgL_classz00_6826 = BGl_csequencez00zzcgen_copz00;
												BgL_arg1835z00_6825 = BGL_CLASS_NUM(BgL_classz00_6826);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1178z00_6824),
												BgL_arg1835z00_6825);
										}
										BgL_new1179z00_6823 = BgL_new1178z00_6824;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1179z00_6823)))->
											BgL_locz00) =
										((obj_t) (((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_copz00_6499)))->BgL_locz00)), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1179z00_6823)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
									((((BgL_csequencez00_bglt) COBJECT(BgL_new1179z00_6823))->
											BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 1)), BUNSPEC);
									{
										obj_t BgL_auxz00_7895;

										{	/* Cgen/cgen.scm 259 */
											BgL_catomz00_bglt BgL_arg1831z00_6827;

											{	/* Cgen/cgen.scm 259 */
												BgL_catomz00_bglt BgL_new1182z00_6828;

												{	/* Cgen/cgen.scm 259 */
													BgL_catomz00_bglt BgL_new1180z00_6829;

													BgL_new1180z00_6829 =
														((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_catomz00_bgl))));
													{	/* Cgen/cgen.scm 259 */
														long BgL_arg1834z00_6830;

														{	/* Cgen/cgen.scm 259 */
															obj_t BgL_classz00_6831;

															BgL_classz00_6831 = BGl_catomz00zzcgen_copz00;
															BgL_arg1834z00_6830 =
																BGL_CLASS_NUM(BgL_classz00_6831);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1180z00_6829),
															BgL_arg1834z00_6830);
													}
													BgL_new1182z00_6828 = BgL_new1180z00_6829;
												}
												((((BgL_copz00_bglt) COBJECT(
																((BgL_copz00_bglt) BgL_new1182z00_6828)))->
														BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
												((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																	BgL_new1182z00_6828)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
												((((BgL_catomz00_bglt) COBJECT(BgL_new1182z00_6828))->
														BgL_valuez00) = ((obj_t) BUNSPEC), BUNSPEC);
												BgL_arg1831z00_6827 = BgL_new1182z00_6828;
											}
											{	/* Cgen/cgen.scm 258 */
												obj_t BgL_list1832z00_6832;

												{	/* Cgen/cgen.scm 258 */
													obj_t BgL_arg1833z00_6833;

													BgL_arg1833z00_6833 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1831z00_6827), BNIL);
													BgL_list1832z00_6832 =
														MAKE_YOUNG_PAIR(BgL_copz00_6499,
														BgL_arg1833z00_6833);
												}
												BgL_auxz00_7895 = BgL_list1832z00_6832;
										}}
										((((BgL_csequencez00_bglt) COBJECT(BgL_new1179z00_6823))->
												BgL_copsz00) = ((obj_t) BgL_auxz00_7895), BUNSPEC);
									}
									BgL_auxz00_7859 = ((BgL_copz00_bglt) BgL_new1179z00_6823);
								}
							else
								{	/* Cgen/cgen.scm 253 */
									BgL_auxz00_7859 = ((BgL_copz00_bglt) BgL_copz00_6499);
								}
						}
						((((BgL_creturnz00_bglt) COBJECT(BgL_new1173z00_6736))->
								BgL_valuez00) = ((BgL_copz00_bglt) BgL_auxz00_7859), BUNSPEC);
					}
					BgL_auxz00_7722 = BgL_new1173z00_6736;
				}
				return ((obj_t) BgL_auxz00_7722);
			}
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzcgen_cgenz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2233;

				BgL_headz00_2233 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2234;
					obj_t BgL_tailz00_2235;

					BgL_prevz00_2234 = BgL_headz00_2233;
					BgL_tailz00_2235 = BgL_l1z00_1;
				BgL_loopz00_2236:
					if (PAIRP(BgL_tailz00_2235))
						{
							obj_t BgL_newzd2prevzd2_2238;

							BgL_newzd2prevzd2_2238 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2235), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2234, BgL_newzd2prevzd2_2238);
							{
								obj_t BgL_tailz00_7921;
								obj_t BgL_prevz00_7920;

								BgL_prevz00_7920 = BgL_newzd2prevzd2_2238;
								BgL_tailz00_7921 = CDR(BgL_tailz00_2235);
								BgL_tailz00_2235 = BgL_tailz00_7921;
								BgL_prevz00_2234 = BgL_prevz00_7920;
								goto BgL_loopz00_2236;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2233);
				}
			}
		}

	}



/* cgen-function */
	BGL_EXPORTED_DEF obj_t BGl_cgenzd2functionzd2zzcgen_cgenz00(BgL_globalz00_bglt
		BgL_globalz00_17)
	{
		{	/* Cgen/cgen.scm 64 */
			if (BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00
				(BgL_globalz00_17))
				{	/* Cgen/cgen.scm 65 */
					{	/* Cgen/cgen.scm 66 */
						obj_t BgL_arg1856z00_2254;

						BgL_arg1856z00_2254 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_globalz00_17)))->BgL_idz00);
						BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1856z00_2254);
					}
					{	/* Cgen/cgen.scm 70 */
						obj_t BgL_globz00_2255;

						BgL_globz00_2255 =
							BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00;
						BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00 =
							((obj_t) BgL_globalz00_17);
						{	/* Cgen/cgen.scm 72 */
							obj_t BgL_shz00_2256;

							BgL_shz00_2256 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_globalz00_17));
							BUNSPEC;
						}
						{	/* Cgen/cgen.scm 74 */
							BgL_sfunz00_bglt BgL_sfunz00_2257;

							{	/* Cgen/cgen.scm 74 */
								BgL_sfunz00_bglt BgL_tmp1150z00_2276;

								BgL_tmp1150z00_2276 =
									((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_globalz00_17)))->
										BgL_valuez00));
								{	/* Cgen/cgen.scm 74 */
									BgL_sfunzf2czf2_bglt BgL_wide1152z00_2278;

									BgL_wide1152z00_2278 =
										((BgL_sfunzf2czf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_sfunzf2czf2_bgl))));
									{	/* Cgen/cgen.scm 74 */
										obj_t BgL_auxz00_7939;
										BgL_objectz00_bglt BgL_tmpz00_7936;

										BgL_auxz00_7939 = ((obj_t) BgL_wide1152z00_2278);
										BgL_tmpz00_7936 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt) BgL_tmp1150z00_2276));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7936, BgL_auxz00_7939);
									}
									((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_tmp1150z00_2276));
									{	/* Cgen/cgen.scm 74 */
										long BgL_arg1873z00_2279;

										BgL_arg1873z00_2279 =
											BGL_CLASS_NUM(BGl_sfunzf2Czf2zzcgen_copz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_tmp1150z00_2276)),
											BgL_arg1873z00_2279);
									}
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1150z00_2276));
								}
								{
									BgL_clabelz00_bglt BgL_auxz00_7957;
									BgL_sfunzf2czf2_bglt BgL_auxz00_7950;

									{	/* Cgen/cgen.scm 75 */
										BgL_clabelz00_bglt BgL_new1156z00_2281;

										{	/* Cgen/cgen.scm 78 */
											BgL_clabelz00_bglt BgL_new1154z00_2283;

											BgL_new1154z00_2283 =
												((BgL_clabelz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_clabelz00_bgl))));
											{	/* Cgen/cgen.scm 78 */
												long BgL_arg1875z00_2284;

												{	/* Cgen/cgen.scm 78 */
													obj_t BgL_classz00_4171;

													BgL_classz00_4171 = BGl_clabelz00zzcgen_copz00;
													BgL_arg1875z00_2284 =
														BGL_CLASS_NUM(BgL_classz00_4171);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1154z00_2283),
													BgL_arg1875z00_2284);
											}
											BgL_new1156z00_2281 = BgL_new1154z00_2283;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1156z00_2281)))->
												BgL_locz00) =
											((obj_t) (((BgL_sfunz00_bglt)
														COBJECT(((BgL_sfunz00_bglt) (((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_globalz00_17)))->BgL_valuez00))))->
													BgL_locz00)), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1156z00_2281)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
										((((BgL_clabelz00_bglt) COBJECT(BgL_new1156z00_2281))->
												BgL_namez00) =
											((obj_t) (((BgL_variablez00_bglt)
														COBJECT(((BgL_variablez00_bglt)
																BgL_globalz00_17)))->BgL_namez00)), BUNSPEC);
										((((BgL_clabelz00_bglt) COBJECT(BgL_new1156z00_2281))->
												BgL_usedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										((((BgL_clabelz00_bglt) COBJECT(BgL_new1156z00_2281))->
												BgL_bodyz00) = ((obj_t) BUNSPEC), BUNSPEC);
										BgL_auxz00_7957 = BgL_new1156z00_2281;
									}
									{
										obj_t BgL_auxz00_7951;

										{	/* Cgen/cgen.scm 75 */
											BgL_objectz00_bglt BgL_tmpz00_7952;

											BgL_tmpz00_7952 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_tmp1150z00_2276));
											BgL_auxz00_7951 = BGL_OBJECT_WIDENING(BgL_tmpz00_7952);
										}
										BgL_auxz00_7950 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_7951);
									}
									((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_7950))->
											BgL_labelz00) =
										((BgL_clabelz00_bglt) BgL_auxz00_7957), BUNSPEC);
								}
								{
									BgL_sfunzf2czf2_bglt BgL_auxz00_7977;

									{
										obj_t BgL_auxz00_7978;

										{	/* Cgen/cgen.scm 79 */
											BgL_objectz00_bglt BgL_tmpz00_7979;

											BgL_tmpz00_7979 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_tmp1150z00_2276));
											BgL_auxz00_7978 = BGL_OBJECT_WIDENING(BgL_tmpz00_7979);
										}
										BgL_auxz00_7977 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_7978);
									}
									((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_7977))->
											BgL_integratedz00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
								}
								BgL_sfunz00_2257 = ((BgL_sfunz00_bglt) BgL_tmp1150z00_2276);
							}
							{	/* Cgen/cgen.scm 74 */
								obj_t BgL_locz00_2258;

								BgL_locz00_2258 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_sfunz00_2257)))->BgL_locz00);
								{	/* Cgen/cgen.scm 80 */
									BgL_copz00_bglt BgL_copz00_2259;

									{	/* Cgen/cgen.scm 81 */
										obj_t BgL_arg1866z00_2271;
										obj_t BgL_arg1868z00_2272;

										BgL_arg1866z00_2271 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_sfunz00_2257)))->BgL_bodyz00);
										{	/* Cgen/cgen.scm 82 */
											bool_t BgL_test3088z00_7990;

											{	/* Cgen/cgen.scm 82 */
												BgL_typez00_bglt BgL_arg1872z00_2275;

												BgL_arg1872z00_2275 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_globalz00_17)))->
													BgL_typez00);
												BgL_test3088z00_7990 =
													(((obj_t) BgL_arg1872z00_2275) ==
													BGl_za2voidza2z00zztype_cachez00);
											}
											if (BgL_test3088z00_7990)
												{	/* Cgen/cgen.scm 82 */
													BgL_arg1868z00_2272 =
														BGl_za2voidzd2kontza2zd2envz00zzcgen_cgenz00;
												}
											else
												{	/* Cgen/cgen.scm 82 */
													BgL_arg1868z00_2272 =
														BGl_za2returnzd2kontza2zd2zzcgen_cgenz00;
												}
										}
										BgL_copz00_2259 =
											BGl_nodezd2ze3copz31zzcgen_cgenz00(
											((BgL_nodez00_bglt) BgL_arg1866z00_2271),
											BgL_arg1868z00_2272, ((bool_t) 0));
									}
									{	/* Cgen/cgen.scm 81 */

										BGl_resetzd2bdbzd2locz12z12zzcgen_emitzd2copzd2();
										bgl_display_char(((unsigned char) 10),
											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										bgl_display_char(((unsigned char) 10),
											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										bgl_display_string(BGl_string2904z00zzcgen_cgenz00,
											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										{	/* Cgen/cgen.scm 91 */
											obj_t BgL_arg1857z00_2260;

											{	/* Cgen/cgen.scm 91 */
												obj_t BgL_arg1858z00_2261;

												{	/* Cgen/cgen.scm 91 */
													obj_t BgL_arg1859z00_2262;

													BgL_arg1859z00_2262 =
														BGl_shapez00zztools_shapez00(
														((obj_t) BgL_globalz00_17));
													BgL_arg1858z00_2261 =
														SYMBOL_TO_STRING(((obj_t) BgL_arg1859z00_2262));
												}
												BgL_arg1857z00_2260 =
													BGl_pregexpzd2replaceza2z70zz__regexpz00
													(BGl_pregexp1685z00zzcgen_cgenz00,
													BgL_arg1858z00_2261, BGl_string2905z00zzcgen_cgenz00);
											}
											bgl_display_obj(BgL_arg1857z00_2260,
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										}
										bgl_display_string(BGl_string2906z00zzcgen_cgenz00,
											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BFALSE);
										BGl_resetzd2bdbzd2locz12z12zzcgen_emitzd2copzd2();
										{	/* Cgen/cgen.scm 102 */
											BgL_clabelz00_bglt BgL_arg1860z00_2263;

											{
												BgL_sfunzf2czf2_bglt BgL_auxz00_8010;

												{
													obj_t BgL_auxz00_8011;

													{	/* Cgen/cgen.scm 102 */
														BgL_objectz00_bglt BgL_tmpz00_8012;

														BgL_tmpz00_8012 =
															((BgL_objectz00_bglt) BgL_sfunz00_2257);
														BgL_auxz00_8011 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_8012);
													}
													BgL_auxz00_8010 =
														((BgL_sfunzf2czf2_bglt) BgL_auxz00_8011);
												}
												BgL_arg1860z00_2263 =
													(((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_8010))->
													BgL_labelz00);
											}
											((((BgL_clabelz00_bglt) COBJECT(BgL_arg1860z00_2263))->
													BgL_bodyz00) =
												((obj_t) ((obj_t) BgL_copz00_2259)), BUNSPEC);
										}
										BGl_globalzd2ze3cz31zzcgen_cgenz00(BgL_globalz00_17);
										{	/* Cgen/cgen.scm 104 */
											obj_t BgL_copz00_2264;

											{	/* Cgen/cgen.scm 104 */
												BgL_clabelz00_bglt BgL_arg1864z00_2270;

												{
													BgL_sfunzf2czf2_bglt BgL_auxz00_8020;

													{
														obj_t BgL_auxz00_8021;

														{	/* Cgen/cgen.scm 104 */
															BgL_objectz00_bglt BgL_tmpz00_8022;

															BgL_tmpz00_8022 =
																((BgL_objectz00_bglt) BgL_sfunz00_2257);
															BgL_auxz00_8021 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_8022);
														}
														BgL_auxz00_8020 =
															((BgL_sfunzf2czf2_bglt) BgL_auxz00_8021);
													}
													BgL_arg1864z00_2270 =
														(((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_8020))->
														BgL_labelz00);
												}
												BgL_copz00_2264 =
													BGl_blockzd2kontzd2zzcgen_cgenz00(
													((obj_t) BgL_arg1864z00_2270), BgL_locz00_2258);
											}
											bgl_display_string(BGl_string2907z00zzcgen_cgenz00,
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
											{	/* Cgen/cgen.scm 109 */
												bool_t BgL_test3089z00_8030;

												{	/* Cgen/cgen.scm 109 */
													bool_t BgL_test3090z00_8031;

													if (INTEGERP
														(BGl_za2bdbzd2debugza2zd2zzengine_paramz00))
														{	/* Cgen/cgen.scm 109 */
															BgL_test3090z00_8031 =
																(
																(long)
																CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00)
																> 0L);
														}
													else
														{	/* Cgen/cgen.scm 109 */
															BgL_test3090z00_8031 =
																BGl_2ze3ze3zz__r4_numbers_6_5z00
																(BGl_za2bdbzd2debugza2zd2zzengine_paramz00,
																BINT(0L));
														}
													if (BgL_test3090z00_8031)
														{	/* Cgen/cgen.scm 109 */
															if (STRUCTP(BgL_locz00_2258))
																{	/* Cgen/cgen.scm 109 */
																	BgL_test3089z00_8030 =
																		(STRUCT_KEY(BgL_locz00_2258) ==
																		CNST_TABLE_REF(0));
																}
															else
																{	/* Cgen/cgen.scm 109 */
																	BgL_test3089z00_8030 = ((bool_t) 0);
																}
														}
													else
														{	/* Cgen/cgen.scm 109 */
															BgL_test3089z00_8030 = ((bool_t) 0);
														}
												}
												if (BgL_test3089z00_8030)
													{	/* Cgen/cgen.scm 109 */
														BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2
															(BgL_locz00_2258);
														bgl_display_string(BGl_string2908z00zzcgen_cgenz00,
															BGl_za2czd2portza2zd2zzbackend_c_emitz00);
													}
												else
													{	/* Cgen/cgen.scm 109 */
														BFALSE;
													}
											}
											BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
												((BgL_copz00_bglt) BgL_copz00_2264));
											BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2
												(BGl_getzd2currentzd2bdbzd2loczd2zzcgen_emitzd2copzd2
												());
											{	/* Cgen/cgen.scm 118 */
												obj_t BgL_port1686z00_2269;

												BgL_port1686z00_2269 =
													BGl_za2czd2portza2zd2zzbackend_c_emitz00;
												{	/* Cgen/cgen.scm 118 */
													obj_t BgL_tmpz00_8049;

													BgL_tmpz00_8049 = ((obj_t) BgL_port1686z00_2269);
													bgl_display_string(BGl_string2909z00zzcgen_cgenz00,
														BgL_tmpz00_8049);
												}
												{	/* Cgen/cgen.scm 118 */
													obj_t BgL_tmpz00_8052;

													BgL_tmpz00_8052 = ((obj_t) BgL_port1686z00_2269);
													bgl_display_char(((unsigned char) 10),
														BgL_tmpz00_8052);
										}}}
										BGl_nozd2bdbzd2newlinez00zzcgen_cgenz00();
										BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00 =
											BgL_globz00_2255;
										return BGl_leavezd2functionzd2zztools_errorz00();
									}
								}
							}
						}
					}
				}
			else
				{	/* Cgen/cgen.scm 65 */
					return BFALSE;
				}
		}

	}



/* &cgen-function */
	obj_t BGl_z62cgenzd2functionzb0zzcgen_cgenz00(obj_t BgL_envz00_6500,
		obj_t BgL_globalz00_6501)
	{
		{	/* Cgen/cgen.scm 64 */
			return
				BGl_cgenzd2functionzd2zzcgen_cgenz00(
				((BgL_globalz00_bglt) BgL_globalz00_6501));
		}

	}



/* global->c */
	obj_t BGl_globalzd2ze3cz31zzcgen_cgenz00(BgL_globalz00_bglt BgL_gz00_18)
	{
		{	/* Cgen/cgen.scm 133 */
			if (
				((((BgL_globalz00_bglt) COBJECT(BgL_gz00_18))->BgL_importz00) ==
					CNST_TABLE_REF(1)))
				{	/* Cgen/cgen.scm 135 */
					bgl_display_string(BGl_string2910z00zzcgen_cgenz00,
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
			else
				{	/* Cgen/cgen.scm 135 */
					BFALSE;
				}
			{	/* Cgen/cgen.scm 136 */
				obj_t BgL_arg1879z00_2289;

				{	/* Cgen/cgen.scm 136 */
					BgL_typez00_bglt BgL_arg1880z00_2290;
					obj_t BgL_arg1882z00_2291;

					BgL_arg1880z00_2290 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_gz00_18)))->BgL_typez00);
					{	/* Cgen/cgen.scm 138 */
						obj_t BgL_arg1883z00_2292;
						obj_t BgL_arg1884z00_2293;

						BgL_arg1883z00_2292 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_gz00_18)))->BgL_namez00);
						if (NULLP(
								(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_gz00_18)))->
													BgL_valuez00))))->BgL_argsz00)))
							{	/* Cgen/cgen.scm 140 */
								BgL_arg1884z00_2293 = BGl_string2911z00zzcgen_cgenz00;
							}
						else
							{	/* Cgen/cgen.scm 144 */
								obj_t BgL_arg1889z00_2297;

								{	/* Cgen/cgen.scm 144 */
									obj_t BgL_g1159z00_2298;

									BgL_g1159z00_2298 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_gz00_18)))->
														BgL_valuez00))))->BgL_argsz00);
									BgL_arg1889z00_2297 =
										BGl_loopze70ze7zzcgen_cgenz00(BgL_g1159z00_2298);
								}
								BgL_arg1884z00_2293 =
									string_append(BGl_string2912z00zzcgen_cgenz00,
									BgL_arg1889z00_2297);
							}
						BgL_arg1882z00_2291 =
							string_append(BgL_arg1883z00_2292, BgL_arg1884z00_2293);
					}
					BgL_arg1879z00_2289 =
						BGl_makezd2typedzd2declarationz00zztype_toolsz00
						(BgL_arg1880z00_2290, BgL_arg1882z00_2291);
				}
				bgl_display_obj(BgL_arg1879z00_2289,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			}
			return BGl_nozd2bdbzd2newlinez00zzcgen_cgenz00();
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzcgen_cgenz00(obj_t BgL_argsz00_2300)
	{
		{	/* Cgen/cgen.scm 144 */
			if (NULLP(CDR(((obj_t) BgL_argsz00_2300))))
				{	/* Cgen/cgen.scm 146 */
					BgL_localz00_bglt BgL_i1160z00_2304;

					BgL_i1160z00_2304 =
						((BgL_localz00_bglt) CAR(((obj_t) BgL_argsz00_2300)));
					{	/* Cgen/cgen.scm 148 */
						obj_t BgL_arg1893z00_2305;

						{	/* Cgen/cgen.scm 148 */
							BgL_typez00_bglt BgL_arg1894z00_2306;
							obj_t BgL_arg1896z00_2307;

							BgL_arg1894z00_2306 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_i1160z00_2304)))->BgL_typez00);
							BgL_arg1896z00_2307 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_i1160z00_2304)))->BgL_namez00);
							BgL_arg1893z00_2305 =
								BGl_makezd2typedzd2declarationz00zztype_toolsz00
								(BgL_arg1894z00_2306, BgL_arg1896z00_2307);
						}
						return
							string_append(BgL_arg1893z00_2305,
							BGl_string2913z00zzcgen_cgenz00);
					}
				}
			else
				{	/* Cgen/cgen.scm 149 */
					BgL_localz00_bglt BgL_i1161z00_2308;

					BgL_i1161z00_2308 =
						((BgL_localz00_bglt) CAR(((obj_t) BgL_argsz00_2300)));
					{	/* Cgen/cgen.scm 151 */
						obj_t BgL_arg1897z00_2309;
						obj_t BgL_arg1898z00_2310;

						{	/* Cgen/cgen.scm 151 */
							BgL_typez00_bglt BgL_arg1899z00_2311;
							obj_t BgL_arg1901z00_2312;

							BgL_arg1899z00_2311 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_i1161z00_2308)))->BgL_typez00);
							BgL_arg1901z00_2312 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_i1161z00_2308)))->BgL_namez00);
							BgL_arg1897z00_2309 =
								BGl_makezd2typedzd2declarationz00zztype_toolsz00
								(BgL_arg1899z00_2311, BgL_arg1901z00_2312);
						}
						{	/* Cgen/cgen.scm 153 */
							obj_t BgL_arg1902z00_2313;

							BgL_arg1902z00_2313 = CDR(((obj_t) BgL_argsz00_2300));
							BgL_arg1898z00_2310 =
								BGl_loopze70ze7zzcgen_cgenz00(BgL_arg1902z00_2313);
						}
						return
							string_append_3(BgL_arg1897z00_2309,
							BGl_string2914z00zzcgen_cgenz00, BgL_arg1898z00_2310);
					}
				}
		}

	}



/* arg-type */
	obj_t BGl_argzd2typezd2zzcgen_cgenz00(obj_t BgL_az00_19)
	{
		{	/* Cgen/cgen.scm 160 */
			{	/* Cgen/cgen.scm 162 */
				bool_t BgL_test3096z00_8109;

				{	/* Cgen/cgen.scm 162 */
					obj_t BgL_classz00_4213;

					BgL_classz00_4213 = BGl_typez00zztype_typez00;
					if (BGL_OBJECTP(BgL_az00_19))
						{	/* Cgen/cgen.scm 162 */
							BgL_objectz00_bglt BgL_arg1807z00_4215;

							BgL_arg1807z00_4215 = (BgL_objectz00_bglt) (BgL_az00_19);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cgen/cgen.scm 162 */
									long BgL_idxz00_4221;

									BgL_idxz00_4221 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4215);
									BgL_test3096z00_8109 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4221 + 1L)) == BgL_classz00_4213);
								}
							else
								{	/* Cgen/cgen.scm 162 */
									bool_t BgL_res2809z00_4246;

									{	/* Cgen/cgen.scm 162 */
										obj_t BgL_oclassz00_4229;

										{	/* Cgen/cgen.scm 162 */
											obj_t BgL_arg1815z00_4237;
											long BgL_arg1816z00_4238;

											BgL_arg1815z00_4237 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cgen/cgen.scm 162 */
												long BgL_arg1817z00_4239;

												BgL_arg1817z00_4239 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4215);
												BgL_arg1816z00_4238 =
													(BgL_arg1817z00_4239 - OBJECT_TYPE);
											}
											BgL_oclassz00_4229 =
												VECTOR_REF(BgL_arg1815z00_4237, BgL_arg1816z00_4238);
										}
										{	/* Cgen/cgen.scm 162 */
											bool_t BgL__ortest_1115z00_4230;

											BgL__ortest_1115z00_4230 =
												(BgL_classz00_4213 == BgL_oclassz00_4229);
											if (BgL__ortest_1115z00_4230)
												{	/* Cgen/cgen.scm 162 */
													BgL_res2809z00_4246 = BgL__ortest_1115z00_4230;
												}
											else
												{	/* Cgen/cgen.scm 162 */
													long BgL_odepthz00_4231;

													{	/* Cgen/cgen.scm 162 */
														obj_t BgL_arg1804z00_4232;

														BgL_arg1804z00_4232 = (BgL_oclassz00_4229);
														BgL_odepthz00_4231 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4232);
													}
													if ((1L < BgL_odepthz00_4231))
														{	/* Cgen/cgen.scm 162 */
															obj_t BgL_arg1802z00_4234;

															{	/* Cgen/cgen.scm 162 */
																obj_t BgL_arg1803z00_4235;

																BgL_arg1803z00_4235 = (BgL_oclassz00_4229);
																BgL_arg1802z00_4234 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4235,
																	1L);
															}
															BgL_res2809z00_4246 =
																(BgL_arg1802z00_4234 == BgL_classz00_4213);
														}
													else
														{	/* Cgen/cgen.scm 162 */
															BgL_res2809z00_4246 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3096z00_8109 = BgL_res2809z00_4246;
								}
						}
					else
						{	/* Cgen/cgen.scm 162 */
							BgL_test3096z00_8109 = ((bool_t) 0);
						}
				}
				if (BgL_test3096z00_8109)
					{	/* Cgen/cgen.scm 162 */
						return BgL_az00_19;
					}
				else
					{	/* Cgen/cgen.scm 163 */
						bool_t BgL_test3101z00_8132;

						{	/* Cgen/cgen.scm 163 */
							obj_t BgL_classz00_4247;

							BgL_classz00_4247 = BGl_variablez00zzast_varz00;
							if (BGL_OBJECTP(BgL_az00_19))
								{	/* Cgen/cgen.scm 163 */
									BgL_objectz00_bglt BgL_arg1807z00_4249;

									BgL_arg1807z00_4249 = (BgL_objectz00_bglt) (BgL_az00_19);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cgen/cgen.scm 163 */
											long BgL_idxz00_4255;

											BgL_idxz00_4255 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4249);
											BgL_test3101z00_8132 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4255 + 1L)) == BgL_classz00_4247);
										}
									else
										{	/* Cgen/cgen.scm 163 */
											bool_t BgL_res2810z00_4280;

											{	/* Cgen/cgen.scm 163 */
												obj_t BgL_oclassz00_4263;

												{	/* Cgen/cgen.scm 163 */
													obj_t BgL_arg1815z00_4271;
													long BgL_arg1816z00_4272;

													BgL_arg1815z00_4271 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cgen/cgen.scm 163 */
														long BgL_arg1817z00_4273;

														BgL_arg1817z00_4273 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4249);
														BgL_arg1816z00_4272 =
															(BgL_arg1817z00_4273 - OBJECT_TYPE);
													}
													BgL_oclassz00_4263 =
														VECTOR_REF(BgL_arg1815z00_4271,
														BgL_arg1816z00_4272);
												}
												{	/* Cgen/cgen.scm 163 */
													bool_t BgL__ortest_1115z00_4264;

													BgL__ortest_1115z00_4264 =
														(BgL_classz00_4247 == BgL_oclassz00_4263);
													if (BgL__ortest_1115z00_4264)
														{	/* Cgen/cgen.scm 163 */
															BgL_res2810z00_4280 = BgL__ortest_1115z00_4264;
														}
													else
														{	/* Cgen/cgen.scm 163 */
															long BgL_odepthz00_4265;

															{	/* Cgen/cgen.scm 163 */
																obj_t BgL_arg1804z00_4266;

																BgL_arg1804z00_4266 = (BgL_oclassz00_4263);
																BgL_odepthz00_4265 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4266);
															}
															if ((1L < BgL_odepthz00_4265))
																{	/* Cgen/cgen.scm 163 */
																	obj_t BgL_arg1802z00_4268;

																	{	/* Cgen/cgen.scm 163 */
																		obj_t BgL_arg1803z00_4269;

																		BgL_arg1803z00_4269 = (BgL_oclassz00_4263);
																		BgL_arg1802z00_4268 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4269, 1L);
																	}
																	BgL_res2810z00_4280 =
																		(BgL_arg1802z00_4268 == BgL_classz00_4247);
																}
															else
																{	/* Cgen/cgen.scm 163 */
																	BgL_res2810z00_4280 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3101z00_8132 = BgL_res2810z00_4280;
										}
								}
							else
								{	/* Cgen/cgen.scm 163 */
									BgL_test3101z00_8132 = ((bool_t) 0);
								}
						}
						if (BgL_test3101z00_8132)
							{	/* Cgen/cgen.scm 163 */
								return
									((obj_t)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_az00_19)))->BgL_typez00));
							}
						else
							{	/* Cgen/cgen.scm 163 */
								BGL_TAIL return BGl_shapez00zztools_shapez00(BgL_az00_19);
							}
					}
			}
		}

	}



/* capply-tailcallable? */
	bool_t BGl_capplyzd2tailcallablezf3z21zzcgen_cgenz00(BgL_capplyz00_bglt
		BgL_copz00_20)
	{
		{	/* Cgen/cgen.scm 172 */
			{	/* Cgen/cgen.scm 173 */
				BgL_valuez00_bglt BgL_callerz00_2321;

				{	/* Cgen/cgen.scm 173 */
					BgL_globalz00_bglt BgL_oz00_4282;

					BgL_oz00_4282 =
						((BgL_globalz00_bglt)
						BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00);
					BgL_callerz00_2321 =
						(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_oz00_4282)))->BgL_valuez00);
				}
				{	/* Cgen/cgen.scm 173 */
					obj_t BgL_formalsz00_2322;

					BgL_formalsz00_2322 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_callerz00_2321)))->BgL_argsz00);
					{	/* Cgen/cgen.scm 174 */

						if ((bgl_list_length(BgL_formalsz00_2322) == 2L))
							{	/* Cgen/cgen.scm 176 */
								bool_t BgL_test3107z00_8167;

								{	/* Cgen/cgen.scm 176 */
									obj_t BgL_arg1918z00_2328;

									{	/* Cgen/cgen.scm 176 */
										obj_t BgL_arg1919z00_2329;

										BgL_arg1919z00_2329 = CAR(((obj_t) BgL_formalsz00_2322));
										BgL_arg1918z00_2328 =
											BGl_argzd2typezd2zzcgen_cgenz00(BgL_arg1919z00_2329);
									}
									BgL_test3107z00_8167 =
										(BgL_arg1918z00_2328 == BGl_za2objza2z00zztype_cachez00);
								}
								if (BgL_test3107z00_8167)
									{	/* Cgen/cgen.scm 177 */
										obj_t BgL_arg1916z00_2326;

										{	/* Cgen/cgen.scm 177 */
											obj_t BgL_arg1917z00_2327;

											{	/* Cgen/cgen.scm 177 */
												obj_t BgL_pairz00_4289;

												BgL_pairz00_4289 = CDR(((obj_t) BgL_formalsz00_2322));
												BgL_arg1917z00_2327 = CAR(BgL_pairz00_4289);
											}
											BgL_arg1916z00_2326 =
												BGl_argzd2typezd2zzcgen_cgenz00(BgL_arg1917z00_2327);
										}
										return
											(BgL_arg1916z00_2326 == BGl_za2objza2z00zztype_cachez00);
									}
								else
									{	/* Cgen/cgen.scm 176 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Cgen/cgen.scm 175 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* cfuncall-tailcallable? */
	bool_t BGl_cfuncallzd2tailcallablezf3z21zzcgen_cgenz00(BgL_cfuncallz00_bglt
		BgL_copz00_21)
	{
		{	/* Cgen/cgen.scm 182 */
			{
				BgL_valuez00_bglt BgL_calleez00_2358;
				obj_t BgL_actualsz00_2359;

				{	/* Cgen/cgen.scm 191 */
					BgL_valuez00_bglt BgL_callerz00_2333;
					BgL_valuez00_bglt BgL_calleez00_2334;

					{	/* Cgen/cgen.scm 191 */
						BgL_globalz00_bglt BgL_oz00_4360;

						BgL_oz00_4360 =
							((BgL_globalz00_bglt)
							BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00);
						BgL_callerz00_2333 =
							(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_oz00_4360)))->BgL_valuez00);
					}
					BgL_calleez00_2334 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varcz00_bglt) COBJECT(
											((BgL_varcz00_bglt)
												(((BgL_cfuncallz00_bglt) COBJECT(BgL_copz00_21))->
													BgL_funz00))))->BgL_variablez00)))->BgL_valuez00);
					{	/* Cgen/cgen.scm 193 */
						obj_t BgL_g1166z00_2335;
						obj_t BgL_g1167z00_2336;

						BgL_calleez00_2358 = BgL_calleez00_2334;
						BgL_actualsz00_2359 =
							(((BgL_cfuncallz00_bglt) COBJECT(BgL_copz00_21))->BgL_argsz00);
						{	/* Cgen/cgen.scm 186 */
							bool_t BgL_test3108z00_8184;

							{	/* Cgen/cgen.scm 186 */
								obj_t BgL_classz00_4290;

								BgL_classz00_4290 = BGl_sfunz00zzast_varz00;
								{	/* Cgen/cgen.scm 186 */
									BgL_objectz00_bglt BgL_arg1807z00_4292;

									{	/* Cgen/cgen.scm 186 */
										obj_t BgL_tmpz00_8185;

										BgL_tmpz00_8185 =
											((obj_t) ((BgL_objectz00_bglt) BgL_calleez00_2358));
										BgL_arg1807z00_4292 =
											(BgL_objectz00_bglt) (BgL_tmpz00_8185);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cgen/cgen.scm 186 */
											long BgL_idxz00_4298;

											BgL_idxz00_4298 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4292);
											BgL_test3108z00_8184 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4298 + 3L)) == BgL_classz00_4290);
										}
									else
										{	/* Cgen/cgen.scm 186 */
											bool_t BgL_res2811z00_4323;

											{	/* Cgen/cgen.scm 186 */
												obj_t BgL_oclassz00_4306;

												{	/* Cgen/cgen.scm 186 */
													obj_t BgL_arg1815z00_4314;
													long BgL_arg1816z00_4315;

													BgL_arg1815z00_4314 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cgen/cgen.scm 186 */
														long BgL_arg1817z00_4316;

														BgL_arg1817z00_4316 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4292);
														BgL_arg1816z00_4315 =
															(BgL_arg1817z00_4316 - OBJECT_TYPE);
													}
													BgL_oclassz00_4306 =
														VECTOR_REF(BgL_arg1815z00_4314,
														BgL_arg1816z00_4315);
												}
												{	/* Cgen/cgen.scm 186 */
													bool_t BgL__ortest_1115z00_4307;

													BgL__ortest_1115z00_4307 =
														(BgL_classz00_4290 == BgL_oclassz00_4306);
													if (BgL__ortest_1115z00_4307)
														{	/* Cgen/cgen.scm 186 */
															BgL_res2811z00_4323 = BgL__ortest_1115z00_4307;
														}
													else
														{	/* Cgen/cgen.scm 186 */
															long BgL_odepthz00_4308;

															{	/* Cgen/cgen.scm 186 */
																obj_t BgL_arg1804z00_4309;

																BgL_arg1804z00_4309 = (BgL_oclassz00_4306);
																BgL_odepthz00_4308 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4309);
															}
															if ((3L < BgL_odepthz00_4308))
																{	/* Cgen/cgen.scm 186 */
																	obj_t BgL_arg1802z00_4311;

																	{	/* Cgen/cgen.scm 186 */
																		obj_t BgL_arg1803z00_4312;

																		BgL_arg1803z00_4312 = (BgL_oclassz00_4306);
																		BgL_arg1802z00_4311 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4312, 3L);
																	}
																	BgL_res2811z00_4323 =
																		(BgL_arg1802z00_4311 == BgL_classz00_4290);
																}
															else
																{	/* Cgen/cgen.scm 186 */
																	BgL_res2811z00_4323 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3108z00_8184 = BgL_res2811z00_4323;
										}
								}
							}
							if (BgL_test3108z00_8184)
								{	/* Cgen/cgen.scm 186 */
									BgL_g1166z00_2335 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_calleez00_2358)))->BgL_argsz00);
								}
							else
								{	/* Cgen/cgen.scm 187 */
									bool_t BgL_test3112z00_8210;

									{	/* Cgen/cgen.scm 187 */
										obj_t BgL_classz00_4325;

										BgL_classz00_4325 = BGl_cfunz00zzast_varz00;
										{	/* Cgen/cgen.scm 187 */
											BgL_objectz00_bglt BgL_arg1807z00_4327;

											{	/* Cgen/cgen.scm 187 */
												obj_t BgL_tmpz00_8211;

												BgL_tmpz00_8211 =
													((obj_t) ((BgL_objectz00_bglt) BgL_calleez00_2358));
												BgL_arg1807z00_4327 =
													(BgL_objectz00_bglt) (BgL_tmpz00_8211);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cgen/cgen.scm 187 */
													long BgL_idxz00_4333;

													BgL_idxz00_4333 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4327);
													BgL_test3112z00_8210 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4333 + 3L)) == BgL_classz00_4325);
												}
											else
												{	/* Cgen/cgen.scm 187 */
													bool_t BgL_res2812z00_4358;

													{	/* Cgen/cgen.scm 187 */
														obj_t BgL_oclassz00_4341;

														{	/* Cgen/cgen.scm 187 */
															obj_t BgL_arg1815z00_4349;
															long BgL_arg1816z00_4350;

															BgL_arg1815z00_4349 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cgen/cgen.scm 187 */
																long BgL_arg1817z00_4351;

																BgL_arg1817z00_4351 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4327);
																BgL_arg1816z00_4350 =
																	(BgL_arg1817z00_4351 - OBJECT_TYPE);
															}
															BgL_oclassz00_4341 =
																VECTOR_REF(BgL_arg1815z00_4349,
																BgL_arg1816z00_4350);
														}
														{	/* Cgen/cgen.scm 187 */
															bool_t BgL__ortest_1115z00_4342;

															BgL__ortest_1115z00_4342 =
																(BgL_classz00_4325 == BgL_oclassz00_4341);
															if (BgL__ortest_1115z00_4342)
																{	/* Cgen/cgen.scm 187 */
																	BgL_res2812z00_4358 =
																		BgL__ortest_1115z00_4342;
																}
															else
																{	/* Cgen/cgen.scm 187 */
																	long BgL_odepthz00_4343;

																	{	/* Cgen/cgen.scm 187 */
																		obj_t BgL_arg1804z00_4344;

																		BgL_arg1804z00_4344 = (BgL_oclassz00_4341);
																		BgL_odepthz00_4343 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4344);
																	}
																	if ((3L < BgL_odepthz00_4343))
																		{	/* Cgen/cgen.scm 187 */
																			obj_t BgL_arg1802z00_4346;

																			{	/* Cgen/cgen.scm 187 */
																				obj_t BgL_arg1803z00_4347;

																				BgL_arg1803z00_4347 =
																					(BgL_oclassz00_4341);
																				BgL_arg1802z00_4346 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4347, 3L);
																			}
																			BgL_res2812z00_4358 =
																				(BgL_arg1802z00_4346 ==
																				BgL_classz00_4325);
																		}
																	else
																		{	/* Cgen/cgen.scm 187 */
																			BgL_res2812z00_4358 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3112z00_8210 = BgL_res2812z00_4358;
												}
										}
									}
									if (BgL_test3112z00_8210)
										{	/* Cgen/cgen.scm 187 */
											BgL_g1166z00_2335 =
												(((BgL_cfunz00_bglt) COBJECT(
														((BgL_cfunz00_bglt) BgL_calleez00_2358)))->
												BgL_argszd2typezd2);
										}
									else
										{	/* Cgen/cgen.scm 187 */
											BgL_g1166z00_2335 = BgL_actualsz00_2359;
										}
								}
						}
						BgL_g1167z00_2336 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_callerz00_2333)))->BgL_argsz00);
						{
							obj_t BgL_actualsz00_2338;
							obj_t BgL_formalsz00_2339;

							BgL_actualsz00_2338 = BgL_g1166z00_2335;
							BgL_formalsz00_2339 = BgL_g1167z00_2336;
						BgL_zc3z04anonymousza31921ze3z87_2340:
							if (NULLP(BgL_actualsz00_2338))
								{	/* Cgen/cgen.scm 196 */
									return NULLP(BgL_formalsz00_2339);
								}
							else
								{	/* Cgen/cgen.scm 196 */
									if (NULLP(BgL_formalsz00_2339))
										{	/* Cgen/cgen.scm 198 */
											return ((bool_t) 0);
										}
									else
										{	/* Cgen/cgen.scm 200 */
											bool_t BgL_test3118z00_8244;

											{	/* Cgen/cgen.scm 200 */
												obj_t BgL_arg1931z00_2350;
												obj_t BgL_arg1932z00_2351;

												{	/* Cgen/cgen.scm 200 */
													obj_t BgL_arg1933z00_2352;

													BgL_arg1933z00_2352 =
														CAR(((obj_t) BgL_actualsz00_2338));
													BgL_arg1931z00_2350 =
														BGl_argzd2typezd2zzcgen_cgenz00
														(BgL_arg1933z00_2352);
												}
												{	/* Cgen/cgen.scm 200 */
													obj_t BgL_arg1934z00_2353;

													BgL_arg1934z00_2353 =
														CAR(((obj_t) BgL_formalsz00_2339));
													BgL_arg1932z00_2351 =
														BGl_argzd2typezd2zzcgen_cgenz00
														(BgL_arg1934z00_2353);
												}
												BgL_test3118z00_8244 =
													(BgL_arg1931z00_2350 == BgL_arg1932z00_2351);
											}
											if (BgL_test3118z00_8244)
												{	/* Cgen/cgen.scm 201 */
													obj_t BgL_arg1929z00_2348;
													obj_t BgL_arg1930z00_2349;

													BgL_arg1929z00_2348 =
														CDR(((obj_t) BgL_actualsz00_2338));
													BgL_arg1930z00_2349 =
														CDR(((obj_t) BgL_formalsz00_2339));
													{
														obj_t BgL_formalsz00_8257;
														obj_t BgL_actualsz00_8256;

														BgL_actualsz00_8256 = BgL_arg1929z00_2348;
														BgL_formalsz00_8257 = BgL_arg1930z00_2349;
														BgL_formalsz00_2339 = BgL_formalsz00_8257;
														BgL_actualsz00_2338 = BgL_actualsz00_8256;
														goto BgL_zc3z04anonymousza31921ze3z87_2340;
													}
												}
											else
												{	/* Cgen/cgen.scm 200 */
													return ((bool_t) 0);
												}
										}
								}
						}
					}
				}
			}
		}

	}



/* capp-tailcallable? */
	bool_t BGl_cappzd2tailcallablezf3z21zzcgen_cgenz00(BgL_cappz00_bglt
		BgL_copz00_22)
	{
		{	/* Cgen/cgen.scm 211 */
			{
				BgL_valuez00_bglt BgL_calleez00_2390;

				{	/* Cgen/cgen.scm 220 */
					BgL_valuez00_bglt BgL_callerz00_2366;
					BgL_valuez00_bglt BgL_calleez00_2367;

					{	/* Cgen/cgen.scm 220 */
						BgL_globalz00_bglt BgL_oz00_4438;

						BgL_oz00_4438 =
							((BgL_globalz00_bglt)
							BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00);
						BgL_callerz00_2366 =
							(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_oz00_4438)))->BgL_valuez00);
					}
					BgL_calleez00_2367 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varcz00_bglt) COBJECT(
											((BgL_varcz00_bglt)
												(((BgL_cappz00_bglt) COBJECT(BgL_copz00_22))->
													BgL_funz00))))->BgL_variablez00)))->BgL_valuez00);
					{	/* Cgen/cgen.scm 222 */
						obj_t BgL_g1169z00_2368;
						obj_t BgL_g1171z00_2369;

						BgL_calleez00_2390 = BgL_calleez00_2367;
						{	/* Cgen/cgen.scm 215 */
							bool_t BgL_test3119z00_8265;

							{	/* Cgen/cgen.scm 215 */
								obj_t BgL_classz00_4368;

								BgL_classz00_4368 = BGl_sfunz00zzast_varz00;
								{	/* Cgen/cgen.scm 215 */
									BgL_objectz00_bglt BgL_arg1807z00_4370;

									{	/* Cgen/cgen.scm 215 */
										obj_t BgL_tmpz00_8266;

										BgL_tmpz00_8266 =
											((obj_t) ((BgL_objectz00_bglt) BgL_calleez00_2390));
										BgL_arg1807z00_4370 =
											(BgL_objectz00_bglt) (BgL_tmpz00_8266);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cgen/cgen.scm 215 */
											long BgL_idxz00_4376;

											BgL_idxz00_4376 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4370);
											BgL_test3119z00_8265 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4376 + 3L)) == BgL_classz00_4368);
										}
									else
										{	/* Cgen/cgen.scm 215 */
											bool_t BgL_res2813z00_4401;

											{	/* Cgen/cgen.scm 215 */
												obj_t BgL_oclassz00_4384;

												{	/* Cgen/cgen.scm 215 */
													obj_t BgL_arg1815z00_4392;
													long BgL_arg1816z00_4393;

													BgL_arg1815z00_4392 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cgen/cgen.scm 215 */
														long BgL_arg1817z00_4394;

														BgL_arg1817z00_4394 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4370);
														BgL_arg1816z00_4393 =
															(BgL_arg1817z00_4394 - OBJECT_TYPE);
													}
													BgL_oclassz00_4384 =
														VECTOR_REF(BgL_arg1815z00_4392,
														BgL_arg1816z00_4393);
												}
												{	/* Cgen/cgen.scm 215 */
													bool_t BgL__ortest_1115z00_4385;

													BgL__ortest_1115z00_4385 =
														(BgL_classz00_4368 == BgL_oclassz00_4384);
													if (BgL__ortest_1115z00_4385)
														{	/* Cgen/cgen.scm 215 */
															BgL_res2813z00_4401 = BgL__ortest_1115z00_4385;
														}
													else
														{	/* Cgen/cgen.scm 215 */
															long BgL_odepthz00_4386;

															{	/* Cgen/cgen.scm 215 */
																obj_t BgL_arg1804z00_4387;

																BgL_arg1804z00_4387 = (BgL_oclassz00_4384);
																BgL_odepthz00_4386 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4387);
															}
															if ((3L < BgL_odepthz00_4386))
																{	/* Cgen/cgen.scm 215 */
																	obj_t BgL_arg1802z00_4389;

																	{	/* Cgen/cgen.scm 215 */
																		obj_t BgL_arg1803z00_4390;

																		BgL_arg1803z00_4390 = (BgL_oclassz00_4384);
																		BgL_arg1802z00_4389 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4390, 3L);
																	}
																	BgL_res2813z00_4401 =
																		(BgL_arg1802z00_4389 == BgL_classz00_4368);
																}
															else
																{	/* Cgen/cgen.scm 215 */
																	BgL_res2813z00_4401 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3119z00_8265 = BgL_res2813z00_4401;
										}
								}
							}
							if (BgL_test3119z00_8265)
								{	/* Cgen/cgen.scm 215 */
									BgL_g1169z00_2368 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_calleez00_2390)))->BgL_argsz00);
								}
							else
								{	/* Cgen/cgen.scm 216 */
									bool_t BgL_test3123z00_8291;

									{	/* Cgen/cgen.scm 216 */
										obj_t BgL_classz00_4403;

										BgL_classz00_4403 = BGl_cfunz00zzast_varz00;
										{	/* Cgen/cgen.scm 216 */
											BgL_objectz00_bglt BgL_arg1807z00_4405;

											{	/* Cgen/cgen.scm 216 */
												obj_t BgL_tmpz00_8292;

												BgL_tmpz00_8292 =
													((obj_t) ((BgL_objectz00_bglt) BgL_calleez00_2390));
												BgL_arg1807z00_4405 =
													(BgL_objectz00_bglt) (BgL_tmpz00_8292);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cgen/cgen.scm 216 */
													long BgL_idxz00_4411;

													BgL_idxz00_4411 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4405);
													BgL_test3123z00_8291 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4411 + 3L)) == BgL_classz00_4403);
												}
											else
												{	/* Cgen/cgen.scm 216 */
													bool_t BgL_res2814z00_4436;

													{	/* Cgen/cgen.scm 216 */
														obj_t BgL_oclassz00_4419;

														{	/* Cgen/cgen.scm 216 */
															obj_t BgL_arg1815z00_4427;
															long BgL_arg1816z00_4428;

															BgL_arg1815z00_4427 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cgen/cgen.scm 216 */
																long BgL_arg1817z00_4429;

																BgL_arg1817z00_4429 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4405);
																BgL_arg1816z00_4428 =
																	(BgL_arg1817z00_4429 - OBJECT_TYPE);
															}
															BgL_oclassz00_4419 =
																VECTOR_REF(BgL_arg1815z00_4427,
																BgL_arg1816z00_4428);
														}
														{	/* Cgen/cgen.scm 216 */
															bool_t BgL__ortest_1115z00_4420;

															BgL__ortest_1115z00_4420 =
																(BgL_classz00_4403 == BgL_oclassz00_4419);
															if (BgL__ortest_1115z00_4420)
																{	/* Cgen/cgen.scm 216 */
																	BgL_res2814z00_4436 =
																		BgL__ortest_1115z00_4420;
																}
															else
																{	/* Cgen/cgen.scm 216 */
																	long BgL_odepthz00_4421;

																	{	/* Cgen/cgen.scm 216 */
																		obj_t BgL_arg1804z00_4422;

																		BgL_arg1804z00_4422 = (BgL_oclassz00_4419);
																		BgL_odepthz00_4421 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4422);
																	}
																	if ((3L < BgL_odepthz00_4421))
																		{	/* Cgen/cgen.scm 216 */
																			obj_t BgL_arg1802z00_4424;

																			{	/* Cgen/cgen.scm 216 */
																				obj_t BgL_arg1803z00_4425;

																				BgL_arg1803z00_4425 =
																					(BgL_oclassz00_4419);
																				BgL_arg1802z00_4424 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4425, 3L);
																			}
																			BgL_res2814z00_4436 =
																				(BgL_arg1802z00_4424 ==
																				BgL_classz00_4403);
																		}
																	else
																		{	/* Cgen/cgen.scm 216 */
																			BgL_res2814z00_4436 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3123z00_8291 = BgL_res2814z00_4436;
												}
										}
									}
									if (BgL_test3123z00_8291)
										{	/* Cgen/cgen.scm 216 */
											BgL_g1169z00_2368 =
												(((BgL_cfunz00_bglt) COBJECT(
														((BgL_cfunz00_bglt) BgL_calleez00_2390)))->
												BgL_argszd2typezd2);
										}
									else
										{	/* Cgen/cgen.scm 216 */
											BgL_g1169z00_2368 = BNIL;
										}
								}
						}
						BgL_g1171z00_2369 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_callerz00_2366)))->BgL_argsz00);
						{
							obj_t BgL_actualsz00_2371;
							obj_t BgL_formalsz00_2372;

							BgL_actualsz00_2371 = BgL_g1169z00_2368;
							BgL_formalsz00_2372 = BgL_g1171z00_2369;
						BgL_zc3z04anonymousza31941ze3z87_2373:
							if (NULLP(BgL_actualsz00_2371))
								{	/* Cgen/cgen.scm 225 */
									return NULLP(BgL_formalsz00_2372);
								}
							else
								{	/* Cgen/cgen.scm 225 */
									if (NULLP(BgL_formalsz00_2372))
										{	/* Cgen/cgen.scm 227 */
											return ((bool_t) 0);
										}
									else
										{	/* Cgen/cgen.scm 229 */
											bool_t BgL_test3129z00_8324;

											{	/* Cgen/cgen.scm 229 */
												obj_t BgL_arg1951z00_2383;
												obj_t BgL_arg1952z00_2384;

												{	/* Cgen/cgen.scm 229 */
													obj_t BgL_arg1953z00_2385;

													BgL_arg1953z00_2385 =
														CAR(((obj_t) BgL_actualsz00_2371));
													BgL_arg1951z00_2383 =
														BGl_argzd2typezd2zzcgen_cgenz00
														(BgL_arg1953z00_2385);
												}
												{	/* Cgen/cgen.scm 229 */
													obj_t BgL_arg1954z00_2386;

													BgL_arg1954z00_2386 =
														CAR(((obj_t) BgL_formalsz00_2372));
													BgL_arg1952z00_2384 =
														BGl_argzd2typezd2zzcgen_cgenz00
														(BgL_arg1954z00_2386);
												}
												BgL_test3129z00_8324 =
													(BgL_arg1951z00_2383 == BgL_arg1952z00_2384);
											}
											if (BgL_test3129z00_8324)
												{	/* Cgen/cgen.scm 230 */
													obj_t BgL_arg1949z00_2381;
													obj_t BgL_arg1950z00_2382;

													BgL_arg1949z00_2381 =
														CDR(((obj_t) BgL_actualsz00_2371));
													BgL_arg1950z00_2382 =
														CDR(((obj_t) BgL_formalsz00_2372));
													{
														obj_t BgL_formalsz00_8337;
														obj_t BgL_actualsz00_8336;

														BgL_actualsz00_8336 = BgL_arg1949z00_2381;
														BgL_formalsz00_8337 = BgL_arg1950z00_2382;
														BgL_formalsz00_2372 = BgL_formalsz00_8337;
														BgL_actualsz00_2371 = BgL_actualsz00_8336;
														goto BgL_zc3z04anonymousza31941ze3z87_2373;
													}
												}
											else
												{	/* Cgen/cgen.scm 229 */
													return ((bool_t) 0);
												}
										}
								}
						}
					}
				}
			}
		}

	}



/* *void-kont* */
	BgL_cvoidz00_bglt BGl_za2voidzd2kontza2zd2zzcgen_cgenz00(obj_t BgL_copz00_23)
	{
		{	/* Cgen/cgen.scm 273 */
			{	/* Cgen/cgen.scm 275 */
				BgL_cvoidz00_bglt BgL_new1184z00_2395;

				{	/* Cgen/cgen.scm 275 */
					BgL_cvoidz00_bglt BgL_new1183z00_2396;

					BgL_new1183z00_2396 =
						((BgL_cvoidz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cvoidz00_bgl))));
					{	/* Cgen/cgen.scm 275 */
						long BgL_arg1960z00_2397;

						BgL_arg1960z00_2397 = BGL_CLASS_NUM(BGl_cvoidz00zzcgen_copz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1183z00_2396), BgL_arg1960z00_2397);
					}
					BgL_new1184z00_2395 = BgL_new1183z00_2396;
				}
				((((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt) BgL_new1184z00_2395)))->BgL_locz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) BgL_new1184z00_2395)))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt)
							BGl_za2voidza2z00zztype_cachez00)), BUNSPEC);
				((((BgL_cvoidz00_bglt) COBJECT(BgL_new1184z00_2395))->BgL_valuez00) =
					((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_copz00_23)), BUNSPEC);
				return BgL_new1184z00_2395;
			}
		}

	}



/* &*void-kont* */
	obj_t BGl_z62za2voidzd2kontza2zb0zzcgen_cgenz00(obj_t BgL_envz00_6502,
		obj_t BgL_copz00_6503)
	{
		{	/* Cgen/cgen.scm 273 */
			return ((obj_t) BGl_za2voidzd2kontza2zd2zzcgen_cgenz00(BgL_copz00_6503));
		}

	}



/* block-kont */
	BGL_EXPORTED_DEF obj_t BGl_blockzd2kontzd2zzcgen_cgenz00(obj_t BgL_copz00_24,
		obj_t BgL_locz00_25)
	{
		{	/* Cgen/cgen.scm 291 */
			{	/* Cgen/cgen.scm 293 */
				bool_t BgL_test3130z00_8351;

				{	/* Cgen/cgen.scm 293 */
					obj_t BgL_classz00_4449;

					BgL_classz00_4449 = BGl_cblockz00zzcgen_copz00;
					if (BGL_OBJECTP(BgL_copz00_24))
						{	/* Cgen/cgen.scm 293 */
							BgL_objectz00_bglt BgL_arg1807z00_4451;

							BgL_arg1807z00_4451 = (BgL_objectz00_bglt) (BgL_copz00_24);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cgen/cgen.scm 293 */
									long BgL_idxz00_4457;

									BgL_idxz00_4457 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4451);
									BgL_test3130z00_8351 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4457 + 2L)) == BgL_classz00_4449);
								}
							else
								{	/* Cgen/cgen.scm 293 */
									bool_t BgL_res2815z00_4482;

									{	/* Cgen/cgen.scm 293 */
										obj_t BgL_oclassz00_4465;

										{	/* Cgen/cgen.scm 293 */
											obj_t BgL_arg1815z00_4473;
											long BgL_arg1816z00_4474;

											BgL_arg1815z00_4473 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cgen/cgen.scm 293 */
												long BgL_arg1817z00_4475;

												BgL_arg1817z00_4475 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4451);
												BgL_arg1816z00_4474 =
													(BgL_arg1817z00_4475 - OBJECT_TYPE);
											}
											BgL_oclassz00_4465 =
												VECTOR_REF(BgL_arg1815z00_4473, BgL_arg1816z00_4474);
										}
										{	/* Cgen/cgen.scm 293 */
											bool_t BgL__ortest_1115z00_4466;

											BgL__ortest_1115z00_4466 =
												(BgL_classz00_4449 == BgL_oclassz00_4465);
											if (BgL__ortest_1115z00_4466)
												{	/* Cgen/cgen.scm 293 */
													BgL_res2815z00_4482 = BgL__ortest_1115z00_4466;
												}
											else
												{	/* Cgen/cgen.scm 293 */
													long BgL_odepthz00_4467;

													{	/* Cgen/cgen.scm 293 */
														obj_t BgL_arg1804z00_4468;

														BgL_arg1804z00_4468 = (BgL_oclassz00_4465);
														BgL_odepthz00_4467 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4468);
													}
													if ((2L < BgL_odepthz00_4467))
														{	/* Cgen/cgen.scm 293 */
															obj_t BgL_arg1802z00_4470;

															{	/* Cgen/cgen.scm 293 */
																obj_t BgL_arg1803z00_4471;

																BgL_arg1803z00_4471 = (BgL_oclassz00_4465);
																BgL_arg1802z00_4470 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4471,
																	2L);
															}
															BgL_res2815z00_4482 =
																(BgL_arg1802z00_4470 == BgL_classz00_4449);
														}
													else
														{	/* Cgen/cgen.scm 293 */
															BgL_res2815z00_4482 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3130z00_8351 = BgL_res2815z00_4482;
								}
						}
					else
						{	/* Cgen/cgen.scm 293 */
							BgL_test3130z00_8351 = ((bool_t) 0);
						}
				}
				if (BgL_test3130z00_8351)
					{	/* Cgen/cgen.scm 293 */
						return BgL_copz00_24;
					}
				else
					{	/* Cgen/cgen.scm 296 */
						BgL_cblockz00_bglt BgL_new1189z00_2399;

						{	/* Cgen/cgen.scm 299 */
							BgL_cblockz00_bglt BgL_new1187z00_2400;

							BgL_new1187z00_2400 =
								((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cblockz00_bgl))));
							{	/* Cgen/cgen.scm 299 */
								long BgL_arg1962z00_2401;

								BgL_arg1962z00_2401 = BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1187z00_2400),
									BgL_arg1962z00_2401);
							}
							BgL_new1189z00_2399 = BgL_new1187z00_2400;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1189z00_2399)))->BgL_locz00) =
							((obj_t) BgL_locz00_25), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1189z00_2399)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
												BgL_copz00_24)))->BgL_typez00)), BUNSPEC);
						((((BgL_cblockz00_bglt) COBJECT(BgL_new1189z00_2399))->
								BgL_bodyz00) =
							((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_copz00_24)), BUNSPEC);
						return ((obj_t) BgL_new1189z00_2399);
					}
			}
		}

	}



/* &block-kont */
	obj_t BGl_z62blockzd2kontzb0zzcgen_cgenz00(obj_t BgL_envz00_6504,
		obj_t BgL_copz00_6505, obj_t BgL_locz00_6506)
	{
		{	/* Cgen/cgen.scm 291 */
			return
				BGl_blockzd2kontzd2zzcgen_cgenz00(BgL_copz00_6505, BgL_locz00_6506);
		}

	}



/* make-setq-kont */
	obj_t BGl_makezd2setqzd2kontz00zzcgen_cgenz00(obj_t BgL_varz00_28,
		obj_t BgL_locz00_29, obj_t BgL_kontz00_30)
	{
		{	/* Cgen/cgen.scm 314 */
			{	/* Cgen/cgen.scm 315 */
				obj_t BgL_zc3z04anonymousza31963ze3z87_6507;

				BgL_zc3z04anonymousza31963ze3z87_6507 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31963ze3ze5zzcgen_cgenz00,
					(int) (1L), (int) (3L));
				PROCEDURE_SET(BgL_zc3z04anonymousza31963ze3z87_6507,
					(int) (0L), BgL_locz00_29);
				PROCEDURE_SET(BgL_zc3z04anonymousza31963ze3z87_6507,
					(int) (1L), BgL_varz00_28);
				PROCEDURE_SET(BgL_zc3z04anonymousza31963ze3z87_6507,
					(int) (2L), BgL_kontz00_30);
				return BgL_zc3z04anonymousza31963ze3z87_6507;
			}
		}

	}



/* &<@anonymous:1963> */
	obj_t BGl_z62zc3z04anonymousza31963ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6508,
		obj_t BgL_copz00_6512)
	{
		{	/* Cgen/cgen.scm 315 */
			{	/* Cgen/cgen.scm 316 */
				obj_t BgL_locz00_6509;
				obj_t BgL_varz00_6510;
				obj_t BgL_kontz00_6511;

				BgL_locz00_6509 = PROCEDURE_REF(BgL_envz00_6508, (int) (0L));
				BgL_varz00_6510 = PROCEDURE_REF(BgL_envz00_6508, (int) (1L));
				BgL_kontz00_6511 = PROCEDURE_REF(BgL_envz00_6508, (int) (2L));
				{	/* Cgen/cgen.scm 316 */
					bool_t BgL_test3135z00_8403;

					{	/* Cgen/cgen.scm 316 */
						obj_t BgL_classz00_6834;

						BgL_classz00_6834 = BGl_cfailz00zzcgen_copz00;
						if (BGL_OBJECTP(BgL_copz00_6512))
							{	/* Cgen/cgen.scm 316 */
								BgL_objectz00_bglt BgL_arg1807z00_6835;

								BgL_arg1807z00_6835 = (BgL_objectz00_bglt) (BgL_copz00_6512);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cgen/cgen.scm 316 */
										long BgL_idxz00_6836;

										BgL_idxz00_6836 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6835);
										BgL_test3135z00_8403 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6836 + 2L)) == BgL_classz00_6834);
									}
								else
									{	/* Cgen/cgen.scm 316 */
										bool_t BgL_res2816z00_6839;

										{	/* Cgen/cgen.scm 316 */
											obj_t BgL_oclassz00_6840;

											{	/* Cgen/cgen.scm 316 */
												obj_t BgL_arg1815z00_6841;
												long BgL_arg1816z00_6842;

												BgL_arg1815z00_6841 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cgen/cgen.scm 316 */
													long BgL_arg1817z00_6843;

													BgL_arg1817z00_6843 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6835);
													BgL_arg1816z00_6842 =
														(BgL_arg1817z00_6843 - OBJECT_TYPE);
												}
												BgL_oclassz00_6840 =
													VECTOR_REF(BgL_arg1815z00_6841, BgL_arg1816z00_6842);
											}
											{	/* Cgen/cgen.scm 316 */
												bool_t BgL__ortest_1115z00_6844;

												BgL__ortest_1115z00_6844 =
													(BgL_classz00_6834 == BgL_oclassz00_6840);
												if (BgL__ortest_1115z00_6844)
													{	/* Cgen/cgen.scm 316 */
														BgL_res2816z00_6839 = BgL__ortest_1115z00_6844;
													}
												else
													{	/* Cgen/cgen.scm 316 */
														long BgL_odepthz00_6845;

														{	/* Cgen/cgen.scm 316 */
															obj_t BgL_arg1804z00_6846;

															BgL_arg1804z00_6846 = (BgL_oclassz00_6840);
															BgL_odepthz00_6845 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_6846);
														}
														if ((2L < BgL_odepthz00_6845))
															{	/* Cgen/cgen.scm 316 */
																obj_t BgL_arg1802z00_6847;

																{	/* Cgen/cgen.scm 316 */
																	obj_t BgL_arg1803z00_6848;

																	BgL_arg1803z00_6848 = (BgL_oclassz00_6840);
																	BgL_arg1802z00_6847 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6848,
																		2L);
																}
																BgL_res2816z00_6839 =
																	(BgL_arg1802z00_6847 == BgL_classz00_6834);
															}
														else
															{	/* Cgen/cgen.scm 316 */
																BgL_res2816z00_6839 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3135z00_8403 = BgL_res2816z00_6839;
									}
							}
						else
							{	/* Cgen/cgen.scm 316 */
								BgL_test3135z00_8403 = ((bool_t) 0);
							}
					}
					if (BgL_test3135z00_8403)
						{	/* Cgen/cgen.scm 316 */
							return BgL_copz00_6512;
						}
					else
						{	/* Cgen/cgen.scm 318 */
							BgL_csetqz00_bglt BgL_arg1965z00_6849;

							{	/* Cgen/cgen.scm 318 */
								BgL_csetqz00_bglt BgL_new1191z00_6850;

								{	/* Cgen/cgen.scm 336 */
									BgL_csetqz00_bglt BgL_new1190z00_6851;

									BgL_new1190z00_6851 =
										((BgL_csetqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_csetqz00_bgl))));
									{	/* Cgen/cgen.scm 336 */
										long BgL_arg1973z00_6852;

										BgL_arg1973z00_6852 =
											BGL_CLASS_NUM(BGl_csetqz00zzcgen_copz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1190z00_6851),
											BgL_arg1973z00_6852);
									}
									BgL_new1191z00_6850 = BgL_new1190z00_6851;
								}
								((((BgL_copz00_bglt) COBJECT(
												((BgL_copz00_bglt) BgL_new1191z00_6850)))->BgL_locz00) =
									((obj_t) BgL_locz00_6509), BUNSPEC);
								((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
													BgL_new1191z00_6850)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_variablez00_bglt)
												COBJECT(((BgL_variablez00_bglt) BgL_varz00_6510)))->
											BgL_typez00)), BUNSPEC);
								{
									BgL_varcz00_bglt BgL_auxz00_8436;

									{	/* Cgen/cgen.scm 320 */
										BgL_varcz00_bglt BgL_new1193z00_6853;

										{	/* Cgen/cgen.scm 320 */
											BgL_varcz00_bglt BgL_new1192z00_6854;

											BgL_new1192z00_6854 =
												((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_varcz00_bgl))));
											{	/* Cgen/cgen.scm 320 */
												long BgL_arg1966z00_6855;

												{	/* Cgen/cgen.scm 320 */
													obj_t BgL_classz00_6856;

													BgL_classz00_6856 = BGl_varcz00zzcgen_copz00;
													BgL_arg1966z00_6855 =
														BGL_CLASS_NUM(BgL_classz00_6856);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1192z00_6854),
													BgL_arg1966z00_6855);
											}
											BgL_new1193z00_6853 = BgL_new1192z00_6854;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1193z00_6853)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1193z00_6853)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_variablez00_bglt)
														COBJECT(((BgL_variablez00_bglt) BgL_varz00_6510)))->
													BgL_typez00)), BUNSPEC);
										((((BgL_varcz00_bglt) COBJECT(BgL_new1193z00_6853))->
												BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													BgL_varz00_6510)), BUNSPEC);
										BgL_auxz00_8436 = BgL_new1193z00_6853;
									}
									((((BgL_csetqz00_bglt) COBJECT(BgL_new1191z00_6850))->
											BgL_varz00) =
										((BgL_varcz00_bglt) BgL_auxz00_8436), BUNSPEC);
								}
								{
									BgL_copz00_bglt BgL_auxz00_8450;

									{	/* Cgen/cgen.scm 324 */
										bool_t BgL_test3140z00_8451;

										{	/* Cgen/cgen.scm 324 */
											obj_t BgL_classz00_6857;

											BgL_classz00_6857 = BGl_csetqz00zzcgen_copz00;
											if (BGL_OBJECTP(BgL_copz00_6512))
												{	/* Cgen/cgen.scm 324 */
													BgL_objectz00_bglt BgL_arg1807z00_6858;

													BgL_arg1807z00_6858 =
														(BgL_objectz00_bglt) (BgL_copz00_6512);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cgen/cgen.scm 324 */
															long BgL_idxz00_6859;

															BgL_idxz00_6859 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6858);
															{	/* Cgen/cgen.scm 324 */
																obj_t BgL_arg1800z00_6860;

																{	/* Cgen/cgen.scm 324 */
																	long BgL_arg1801z00_6861;

																	BgL_arg1801z00_6861 = (BgL_idxz00_6859 + 2L);
																	{	/* Cgen/cgen.scm 324 */
																		obj_t BgL_vectorz00_6862;

																		BgL_vectorz00_6862 =
																			BGl_za2inheritancesza2z00zz__objectz00;
																		BgL_arg1800z00_6860 =
																			VECTOR_REF(BgL_vectorz00_6862,
																			BgL_arg1801z00_6861);
																}}
																BgL_test3140z00_8451 =
																	(BgL_arg1800z00_6860 == BgL_classz00_6857);
														}}
													else
														{	/* Cgen/cgen.scm 324 */
															bool_t BgL_res2817z00_6863;

															{	/* Cgen/cgen.scm 324 */
																obj_t BgL_oclassz00_6864;

																{	/* Cgen/cgen.scm 324 */
																	obj_t BgL_arg1815z00_6865;
																	long BgL_arg1816z00_6866;

																	BgL_arg1815z00_6865 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cgen/cgen.scm 324 */
																		long BgL_arg1817z00_6867;

																		BgL_arg1817z00_6867 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6858);
																		BgL_arg1816z00_6866 =
																			(BgL_arg1817z00_6867 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_6864 =
																		VECTOR_REF(BgL_arg1815z00_6865,
																		BgL_arg1816z00_6866);
																}
																{	/* Cgen/cgen.scm 324 */
																	bool_t BgL__ortest_1115z00_6868;

																	BgL__ortest_1115z00_6868 =
																		(BgL_classz00_6857 == BgL_oclassz00_6864);
																	if (BgL__ortest_1115z00_6868)
																		{	/* Cgen/cgen.scm 324 */
																			BgL_res2817z00_6863 =
																				BgL__ortest_1115z00_6868;
																		}
																	else
																		{	/* Cgen/cgen.scm 324 */
																			long BgL_odepthz00_6869;

																			{	/* Cgen/cgen.scm 324 */
																				obj_t BgL_arg1804z00_6870;

																				BgL_arg1804z00_6870 =
																					(BgL_oclassz00_6864);
																				BgL_odepthz00_6869 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_6870);
																			}
																			if ((2L < BgL_odepthz00_6869))
																				{	/* Cgen/cgen.scm 324 */
																					obj_t BgL_arg1802z00_6871;

																					{	/* Cgen/cgen.scm 324 */
																						obj_t BgL_arg1803z00_6872;

																						BgL_arg1803z00_6872 =
																							(BgL_oclassz00_6864);
																						BgL_arg1802z00_6871 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_6872, 2L);
																					}
																					BgL_res2817z00_6863 =
																						(BgL_arg1802z00_6871 ==
																						BgL_classz00_6857);
																				}
																			else
																				{	/* Cgen/cgen.scm 324 */
																					BgL_res2817z00_6863 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3140z00_8451 = BgL_res2817z00_6863;
														}
												}
											else
												{	/* Cgen/cgen.scm 324 */
													BgL_test3140z00_8451 = ((bool_t) 0);
												}
										}
										if (BgL_test3140z00_8451)
											{	/* Cgen/cgen.scm 325 */
												BgL_csequencez00_bglt BgL_new1195z00_6873;

												{	/* Cgen/cgen.scm 327 */
													BgL_csequencez00_bglt BgL_new1194z00_6874;

													BgL_new1194z00_6874 =
														((BgL_csequencez00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_csequencez00_bgl))));
													{	/* Cgen/cgen.scm 327 */
														long BgL_arg1972z00_6875;

														{	/* Cgen/cgen.scm 327 */
															obj_t BgL_classz00_6876;

															BgL_classz00_6876 = BGl_csequencez00zzcgen_copz00;
															BgL_arg1972z00_6875 =
																BGL_CLASS_NUM(BgL_classz00_6876);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1194z00_6874),
															BgL_arg1972z00_6875);
													}
													BgL_new1195z00_6873 = BgL_new1194z00_6874;
												}
												((((BgL_copz00_bglt) COBJECT(
																((BgL_copz00_bglt) BgL_new1195z00_6873)))->
														BgL_locz00) =
													((obj_t) (((BgL_copz00_bglt)
																COBJECT(((BgL_copz00_bglt) BgL_copz00_6512)))->
															BgL_locz00)), BUNSPEC);
												((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																	BgL_new1195z00_6873)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
												((((BgL_csequencez00_bglt)
															COBJECT(BgL_new1195z00_6873))->
														BgL_czd2expzf3z21) =
													((bool_t) ((bool_t) 1)), BUNSPEC);
												{
													obj_t BgL_auxz00_8486;

													{	/* Cgen/cgen.scm 330 */
														BgL_catomz00_bglt BgL_arg1968z00_6877;

														{	/* Cgen/cgen.scm 330 */
															BgL_catomz00_bglt BgL_new1197z00_6878;

															{	/* Cgen/cgen.scm 332 */
																BgL_catomz00_bglt BgL_new1196z00_6879;

																BgL_new1196z00_6879 =
																	((BgL_catomz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_catomz00_bgl))));
																{	/* Cgen/cgen.scm 332 */
																	long BgL_arg1971z00_6880;

																	{	/* Cgen/cgen.scm 332 */
																		obj_t BgL_classz00_6881;

																		BgL_classz00_6881 =
																			BGl_catomz00zzcgen_copz00;
																		BgL_arg1971z00_6880 =
																			BGL_CLASS_NUM(BgL_classz00_6881);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1196z00_6879),
																		BgL_arg1971z00_6880);
																}
																BgL_new1197z00_6878 = BgL_new1196z00_6879;
															}
															((((BgL_copz00_bglt) COBJECT(
																			((BgL_copz00_bglt)
																				BgL_new1197z00_6878)))->BgL_locz00) =
																((obj_t) (((BgL_copz00_bglt)
																			COBJECT(((BgL_copz00_bglt)
																					BgL_copz00_6512)))->BgL_locz00)),
																BUNSPEC);
															((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																				BgL_new1197z00_6878)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BGl_za2unspecza2z00zztype_cachez00)),
																BUNSPEC);
															((((BgL_catomz00_bglt)
																		COBJECT(BgL_new1197z00_6878))->
																	BgL_valuez00) = ((obj_t) BUNSPEC), BUNSPEC);
															BgL_arg1968z00_6877 = BgL_new1197z00_6878;
														}
														{	/* Cgen/cgen.scm 329 */
															obj_t BgL_list1969z00_6882;

															{	/* Cgen/cgen.scm 329 */
																obj_t BgL_arg1970z00_6883;

																BgL_arg1970z00_6883 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1968z00_6877), BNIL);
																BgL_list1969z00_6882 =
																	MAKE_YOUNG_PAIR(BgL_copz00_6512,
																	BgL_arg1970z00_6883);
															}
															BgL_auxz00_8486 = BgL_list1969z00_6882;
													}}
													((((BgL_csequencez00_bglt)
																COBJECT(BgL_new1195z00_6873))->BgL_copsz00) =
														((obj_t) BgL_auxz00_8486), BUNSPEC);
												}
												BgL_auxz00_8450 =
													((BgL_copz00_bglt) BgL_new1195z00_6873);
											}
										else
											{	/* Cgen/cgen.scm 324 */
												BgL_auxz00_8450 = ((BgL_copz00_bglt) BgL_copz00_6512);
											}
									}
									((((BgL_csetqz00_bglt) COBJECT(BgL_new1191z00_6850))->
											BgL_valuez00) =
										((BgL_copz00_bglt) BgL_auxz00_8450), BUNSPEC);
								}
								BgL_arg1965z00_6849 = BgL_new1191z00_6850;
							}
							return
								BGL_PROCEDURE_CALL1(BgL_kontz00_6511,
								((obj_t) BgL_arg1965z00_6849));
						}
				}
			}
		}

	}



/* is-push-exit? */
	bool_t BGl_iszd2pushzd2exitzf3zf3zzcgen_cgenz00(BgL_nodez00_bglt
		BgL_nodez00_55)
	{
		{	/* Cgen/cgen.scm 478 */
			{	/* Cgen/cgen.scm 479 */
				bool_t BgL_test3145z00_8511;

				{	/* Cgen/cgen.scm 479 */
					obj_t BgL_classz00_4572;

					BgL_classz00_4572 = BGl_appz00zzast_nodez00;
					{	/* Cgen/cgen.scm 479 */
						BgL_objectz00_bglt BgL_arg1807z00_4574;

						{	/* Cgen/cgen.scm 479 */
							obj_t BgL_tmpz00_8512;

							BgL_tmpz00_8512 = ((obj_t) BgL_nodez00_55);
							BgL_arg1807z00_4574 = (BgL_objectz00_bglt) (BgL_tmpz00_8512);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cgen.scm 479 */
								long BgL_idxz00_4580;

								BgL_idxz00_4580 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4574);
								BgL_test3145z00_8511 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4580 + 3L)) == BgL_classz00_4572);
							}
						else
							{	/* Cgen/cgen.scm 479 */
								bool_t BgL_res2819z00_4605;

								{	/* Cgen/cgen.scm 479 */
									obj_t BgL_oclassz00_4588;

									{	/* Cgen/cgen.scm 479 */
										obj_t BgL_arg1815z00_4596;
										long BgL_arg1816z00_4597;

										BgL_arg1815z00_4596 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cgen.scm 479 */
											long BgL_arg1817z00_4598;

											BgL_arg1817z00_4598 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4574);
											BgL_arg1816z00_4597 = (BgL_arg1817z00_4598 - OBJECT_TYPE);
										}
										BgL_oclassz00_4588 =
											VECTOR_REF(BgL_arg1815z00_4596, BgL_arg1816z00_4597);
									}
									{	/* Cgen/cgen.scm 479 */
										bool_t BgL__ortest_1115z00_4589;

										BgL__ortest_1115z00_4589 =
											(BgL_classz00_4572 == BgL_oclassz00_4588);
										if (BgL__ortest_1115z00_4589)
											{	/* Cgen/cgen.scm 479 */
												BgL_res2819z00_4605 = BgL__ortest_1115z00_4589;
											}
										else
											{	/* Cgen/cgen.scm 479 */
												long BgL_odepthz00_4590;

												{	/* Cgen/cgen.scm 479 */
													obj_t BgL_arg1804z00_4591;

													BgL_arg1804z00_4591 = (BgL_oclassz00_4588);
													BgL_odepthz00_4590 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4591);
												}
												if ((3L < BgL_odepthz00_4590))
													{	/* Cgen/cgen.scm 479 */
														obj_t BgL_arg1802z00_4593;

														{	/* Cgen/cgen.scm 479 */
															obj_t BgL_arg1803z00_4594;

															BgL_arg1803z00_4594 = (BgL_oclassz00_4588);
															BgL_arg1802z00_4593 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4594,
																3L);
														}
														BgL_res2819z00_4605 =
															(BgL_arg1802z00_4593 == BgL_classz00_4572);
													}
												else
													{	/* Cgen/cgen.scm 479 */
														BgL_res2819z00_4605 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test3145z00_8511 = BgL_res2819z00_4605;
							}
					}
				}
				if (BgL_test3145z00_8511)
					{	/* Cgen/cgen.scm 481 */
						BgL_variablez00_bglt BgL_varz00_2424;

						BgL_varz00_2424 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_nodez00_55)))->BgL_funz00)))->
							BgL_variablez00);
						{	/* Cgen/cgen.scm 482 */
							bool_t BgL_test3149z00_8537;

							{	/* Cgen/cgen.scm 482 */
								obj_t BgL_classz00_4607;

								BgL_classz00_4607 = BGl_globalz00zzast_varz00;
								{	/* Cgen/cgen.scm 482 */
									BgL_objectz00_bglt BgL_arg1807z00_4609;

									{	/* Cgen/cgen.scm 482 */
										obj_t BgL_tmpz00_8538;

										BgL_tmpz00_8538 =
											((obj_t) ((BgL_objectz00_bglt) BgL_varz00_2424));
										BgL_arg1807z00_4609 =
											(BgL_objectz00_bglt) (BgL_tmpz00_8538);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cgen/cgen.scm 482 */
											long BgL_idxz00_4615;

											BgL_idxz00_4615 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4609);
											BgL_test3149z00_8537 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4615 + 2L)) == BgL_classz00_4607);
										}
									else
										{	/* Cgen/cgen.scm 482 */
											bool_t BgL_res2820z00_4640;

											{	/* Cgen/cgen.scm 482 */
												obj_t BgL_oclassz00_4623;

												{	/* Cgen/cgen.scm 482 */
													obj_t BgL_arg1815z00_4631;
													long BgL_arg1816z00_4632;

													BgL_arg1815z00_4631 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cgen/cgen.scm 482 */
														long BgL_arg1817z00_4633;

														BgL_arg1817z00_4633 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4609);
														BgL_arg1816z00_4632 =
															(BgL_arg1817z00_4633 - OBJECT_TYPE);
													}
													BgL_oclassz00_4623 =
														VECTOR_REF(BgL_arg1815z00_4631,
														BgL_arg1816z00_4632);
												}
												{	/* Cgen/cgen.scm 482 */
													bool_t BgL__ortest_1115z00_4624;

													BgL__ortest_1115z00_4624 =
														(BgL_classz00_4607 == BgL_oclassz00_4623);
													if (BgL__ortest_1115z00_4624)
														{	/* Cgen/cgen.scm 482 */
															BgL_res2820z00_4640 = BgL__ortest_1115z00_4624;
														}
													else
														{	/* Cgen/cgen.scm 482 */
															long BgL_odepthz00_4625;

															{	/* Cgen/cgen.scm 482 */
																obj_t BgL_arg1804z00_4626;

																BgL_arg1804z00_4626 = (BgL_oclassz00_4623);
																BgL_odepthz00_4625 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4626);
															}
															if ((2L < BgL_odepthz00_4625))
																{	/* Cgen/cgen.scm 482 */
																	obj_t BgL_arg1802z00_4628;

																	{	/* Cgen/cgen.scm 482 */
																		obj_t BgL_arg1803z00_4629;

																		BgL_arg1803z00_4629 = (BgL_oclassz00_4623);
																		BgL_arg1802z00_4628 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4629, 2L);
																	}
																	BgL_res2820z00_4640 =
																		(BgL_arg1802z00_4628 == BgL_classz00_4607);
																}
															else
																{	/* Cgen/cgen.scm 482 */
																	BgL_res2820z00_4640 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3149z00_8537 = BgL_res2820z00_4640;
										}
								}
							}
							if (BgL_test3149z00_8537)
								{	/* Cgen/cgen.scm 482 */
									return
										(
										(((BgL_variablez00_bglt) COBJECT(BgL_varz00_2424))->
											BgL_idz00) == CNST_TABLE_REF(2));
								}
							else
								{	/* Cgen/cgen.scm 482 */
									return ((bool_t) 0);
								}
						}
					}
				else
					{	/* Cgen/cgen.scm 479 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* extern->cop */
	obj_t BGl_externzd2ze3copz31zzcgen_cgenz00(obj_t BgL_formatz00_59,
		bool_t BgL_argszd2safezd2_60, BgL_externz00_bglt BgL_nodez00_61,
		obj_t BgL_kontz00_62, obj_t BgL_inpushexitz00_63)
	{
		{	/* Cgen/cgen.scm 494 */
			{	/* Cgen/cgen.scm 499 */
				BgL_typez00_bglt BgL_arg1978z00_2429;
				obj_t BgL_arg1979z00_2430;
				obj_t BgL_arg1980z00_2431;

				BgL_arg1978z00_2429 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_nodez00_61)))->BgL_typez00);
				BgL_arg1979z00_2430 =
					(((BgL_externz00_bglt) COBJECT(BgL_nodez00_61))->BgL_exprza2za2);
				BgL_arg1980z00_2431 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt) BgL_nodez00_61)))->BgL_locz00);
				{	/* Cgen/cgen.scm 503 */
					obj_t BgL_zc3z04anonymousza31982ze3z87_6513;

					{
						int BgL_tmpz00_8569;

						BgL_tmpz00_8569 = (int) (3L);
						BgL_zc3z04anonymousza31982ze3z87_6513 =
							MAKE_L_PROCEDURE
							(BGl_z62zc3z04anonymousza31982ze3ze5zzcgen_cgenz00,
							BgL_tmpz00_8569);
					}
					PROCEDURE_L_SET(BgL_zc3z04anonymousza31982ze3z87_6513,
						(int) (0L), ((obj_t) BgL_nodez00_61));
					PROCEDURE_L_SET(BgL_zc3z04anonymousza31982ze3z87_6513,
						(int) (1L), BgL_formatz00_59);
					PROCEDURE_L_SET(BgL_zc3z04anonymousza31982ze3z87_6513,
						(int) (2L), BgL_kontz00_62);
					return
						BGl_nodezd2argszd2ze3copze3zzcgen_cgenz00(BgL_arg1978z00_2429,
						BgL_arg1979z00_2430, BgL_argszd2safezd2_60, BgL_arg1980z00_2431,
						BgL_zc3z04anonymousza31982ze3z87_6513, BgL_inpushexitz00_63);
				}
			}
		}

	}



/* &<@anonymous:1982> */
	obj_t BGl_z62zc3z04anonymousza31982ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6514,
		obj_t BgL_newzd2argszd2_6518)
	{
		{	/* Cgen/cgen.scm 502 */
			{	/* Cgen/cgen.scm 503 */
				BgL_externz00_bglt BgL_i1231z00_6515;
				obj_t BgL_formatz00_6516;
				obj_t BgL_kontz00_6517;

				BgL_i1231z00_6515 =
					((BgL_externz00_bglt) PROCEDURE_L_REF(BgL_envz00_6514, (int) (0L)));
				BgL_formatz00_6516 =
					((obj_t) PROCEDURE_L_REF(BgL_envz00_6514, (int) (1L)));
				BgL_kontz00_6517 = PROCEDURE_L_REF(BgL_envz00_6514, (int) (2L));
				{	/* Cgen/cgen.scm 503 */
					BgL_cpragmaz00_bglt BgL_arg1983z00_6885;

					{	/* Cgen/cgen.scm 503 */
						BgL_cpragmaz00_bglt BgL_new1233z00_6886;

						{	/* Cgen/cgen.scm 505 */
							BgL_cpragmaz00_bglt BgL_new1232z00_6887;

							BgL_new1232z00_6887 =
								((BgL_cpragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cpragmaz00_bgl))));
							{	/* Cgen/cgen.scm 505 */
								long BgL_arg1984z00_6888;

								BgL_arg1984z00_6888 =
									BGL_CLASS_NUM(BGl_cpragmaz00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1232z00_6887),
									BgL_arg1984z00_6888);
							}
							BgL_new1233z00_6886 = BgL_new1232z00_6887;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1233z00_6886)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_i1231z00_6515)))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1233z00_6886)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) BgL_i1231z00_6515)))->
									BgL_typez00)), BUNSPEC);
						((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1233z00_6886))->
								BgL_formatz00) = ((obj_t) BgL_formatz00_6516), BUNSPEC);
						((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1233z00_6886))->
								BgL_argsz00) = ((obj_t) BgL_newzd2argszd2_6518), BUNSPEC);
						BgL_arg1983z00_6885 = BgL_new1233z00_6886;
					}
					return
						BGL_PROCEDURE_CALL1(BgL_kontz00_6517,
						((obj_t) BgL_arg1983z00_6885));
				}
			}
		}

	}



/* bdb-let-var */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_bdbzd2letzd2varz00zzcgen_cgenz00(BgL_copz00_bglt BgL_copz00_91,
		obj_t BgL_locz00_92)
	{
		{	/* Cgen/cgen.scm 866 */
			{	/* Cgen/cgen.scm 867 */
				bool_t BgL_test3153z00_8607;

				if (((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) > 0L))
					{	/* Cgen/cgen.scm 867 */
						if (STRUCTP(BgL_locz00_92))
							{	/* Cgen/cgen.scm 867 */
								BgL_test3153z00_8607 =
									(STRUCT_KEY(BgL_locz00_92) == CNST_TABLE_REF(0));
							}
						else
							{	/* Cgen/cgen.scm 867 */
								BgL_test3153z00_8607 = ((bool_t) 0);
							}
					}
				else
					{	/* Cgen/cgen.scm 867 */
						BgL_test3153z00_8607 = ((bool_t) 0);
					}
				if (BgL_test3153z00_8607)
					{	/* Cgen/cgen.scm 868 */
						BgL_bdbzd2blockzd2_bglt BgL_new1373z00_2442;

						{	/* Cgen/cgen.scm 871 */
							BgL_bdbzd2blockzd2_bglt BgL_new1372z00_2443;

							BgL_new1372z00_2443 =
								((BgL_bdbzd2blockzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_bdbzd2blockzd2_bgl))));
							{	/* Cgen/cgen.scm 871 */
								long BgL_arg1987z00_2444;

								BgL_arg1987z00_2444 =
									BGL_CLASS_NUM(BGl_bdbzd2blockzd2zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1372z00_2443),
									BgL_arg1987z00_2444);
							}
							BgL_new1373z00_2442 = BgL_new1372z00_2443;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1373z00_2442)))->BgL_locz00) =
							((obj_t) BgL_locz00_92), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1373z00_2442)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_copz00_bglt) COBJECT(BgL_copz00_91))->
									BgL_typez00)), BUNSPEC);
						((((BgL_bdbzd2blockzd2_bglt) COBJECT(BgL_new1373z00_2442))->
								BgL_bodyz00) = ((BgL_copz00_bglt) BgL_copz00_91), BUNSPEC);
						return ((BgL_copz00_bglt) BgL_new1373z00_2442);
					}
				else
					{	/* Cgen/cgen.scm 867 */
						return BgL_copz00_91;
					}
			}
		}

	}



/* &bdb-let-var */
	BgL_copz00_bglt BGl_z62bdbzd2letzd2varz62zzcgen_cgenz00(obj_t BgL_envz00_6519,
		obj_t BgL_copz00_6520, obj_t BgL_locz00_6521)
	{
		{	/* Cgen/cgen.scm 866 */
			return
				BGl_bdbzd2letzd2varz00zzcgen_cgenz00(
				((BgL_copz00_bglt) BgL_copz00_6520), BgL_locz00_6521);
		}

	}



/* node-setq */
	BGL_EXPORTED_DEF BgL_setqz00_bglt
		BGl_nodezd2setqzd2zzcgen_cgenz00(BgL_variablez00_bglt BgL_variablez00_114,
		BgL_nodez00_bglt BgL_valuez00_115)
	{
		{	/* Cgen/cgen.scm 1169 */
			{	/* Cgen/cgen.scm 1170 */
				BgL_setqz00_bglt BgL_new1480z00_2446;

				{	/* Cgen/cgen.scm 1172 */
					BgL_setqz00_bglt BgL_new1479z00_2450;

					BgL_new1479z00_2450 =
						((BgL_setqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_setqz00_bgl))));
					{	/* Cgen/cgen.scm 1172 */
						long BgL_arg1989z00_2451;

						BgL_arg1989z00_2451 = BGL_CLASS_NUM(BGl_setqz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1479z00_2450), BgL_arg1989z00_2451);
					}
					{	/* Cgen/cgen.scm 1172 */
						BgL_objectz00_bglt BgL_tmpz00_8633;

						BgL_tmpz00_8633 = ((BgL_objectz00_bglt) BgL_new1479z00_2450);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8633, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1479z00_2450);
					BgL_new1480z00_2446 = BgL_new1479z00_2450;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1480z00_2446)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_valuez00_115))->
							BgL_locz00)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1480z00_2446)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt)
							BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_8643;

					{	/* Cgen/cgen.scm 1173 */
						BgL_refz00_bglt BgL_new1483z00_2447;

						{	/* Cgen/cgen.scm 1174 */
							BgL_refz00_bglt BgL_new1482z00_2448;

							BgL_new1482z00_2448 =
								((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_refz00_bgl))));
							{	/* Cgen/cgen.scm 1174 */
								long BgL_arg1988z00_2449;

								{	/* Cgen/cgen.scm 1174 */
									obj_t BgL_classz00_4659;

									BgL_classz00_4659 = BGl_refz00zzast_nodez00;
									BgL_arg1988z00_2449 = BGL_CLASS_NUM(BgL_classz00_4659);
								}
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1482z00_2448),
									BgL_arg1988z00_2449);
							}
							{	/* Cgen/cgen.scm 1174 */
								BgL_objectz00_bglt BgL_tmpz00_8648;

								BgL_tmpz00_8648 = ((BgL_objectz00_bglt) BgL_new1482z00_2448);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8648, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1482z00_2448);
							BgL_new1483z00_2447 = BgL_new1482z00_2448;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1483z00_2447)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1483z00_2447)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_variablez00_bglt)
										COBJECT(BgL_variablez00_114))->BgL_typez00)), BUNSPEC);
						((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
											BgL_new1483z00_2447)))->BgL_variablez00) =
							((BgL_variablez00_bglt) BgL_variablez00_114), BUNSPEC);
						BgL_auxz00_8643 = ((BgL_varz00_bglt) BgL_new1483z00_2447);
					}
					((((BgL_setqz00_bglt) COBJECT(BgL_new1480z00_2446))->BgL_varz00) =
						((BgL_varz00_bglt) BgL_auxz00_8643), BUNSPEC);
				}
				((((BgL_setqz00_bglt) COBJECT(BgL_new1480z00_2446))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_valuez00_115), BUNSPEC);
				return BgL_new1480z00_2446;
			}
		}

	}



/* &node-setq */
	BgL_setqz00_bglt BGl_z62nodezd2setqzb0zzcgen_cgenz00(obj_t BgL_envz00_6522,
		obj_t BgL_variablez00_6523, obj_t BgL_valuez00_6524)
	{
		{	/* Cgen/cgen.scm 1169 */
			return
				BGl_nodezd2setqzd2zzcgen_cgenz00(
				((BgL_variablez00_bglt) BgL_variablez00_6523),
				((BgL_nodez00_bglt) BgL_valuez00_6524));
		}

	}



/* make-local-svar/name */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(obj_t BgL_idz00_116,
		BgL_typez00_bglt BgL_typez00_117)
	{
		{	/* Cgen/cgen.scm 1182 */
			{	/* Cgen/cgen.scm 1183 */
				BgL_localz00_bglt BgL_localz00_2452;

				BgL_localz00_2452 =
					BGl_makezd2localzd2svarz00zzast_localz00(BgL_idz00_116,
					BgL_typez00_117);
				{	/* Cgen/cgen.scm 1184 */
					bool_t BgL_test3156z00_8666;

					{	/* Cgen/cgen.scm 1184 */
						obj_t BgL_tmpz00_8667;

						BgL_tmpz00_8667 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_localz00_2452)))->BgL_namez00);
						BgL_test3156z00_8666 = STRINGP(BgL_tmpz00_8667);
					}
					if (BgL_test3156z00_8666)
						{	/* Cgen/cgen.scm 1184 */
							BFALSE;
						}
					else
						{	/* Cgen/cgen.scm 1184 */
							BGl_errorz00zz__errorz00(BGl_string2915z00zzcgen_cgenz00,
								BGl_string2916z00zzcgen_cgenz00, ((obj_t) BgL_localz00_2452));
						}
				}
				return BgL_localz00_2452;
			}
		}

	}



/* &make-local-svar/name */
	BgL_localz00_bglt BGl_z62makezd2localzd2svarzf2namez90zzcgen_cgenz00(obj_t
		BgL_envz00_6525, obj_t BgL_idz00_6526, obj_t BgL_typez00_6527)
	{
		{	/* Cgen/cgen.scm 1182 */
			return
				BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(BgL_idz00_6526,
				((BgL_typez00_bglt) BgL_typez00_6527));
		}

	}



/* no-bdb-newline */
	obj_t BGl_nozd2bdbzd2newlinez00zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 1193 */
			if ((BGl_za2bdbzd2debugza2zd2zzengine_paramz00 == BINT(0L)))
				{	/* Cgen/cgen.scm 1194 */
					return
						bgl_display_char(((unsigned char) 10),
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
			else
				{	/* Cgen/cgen.scm 1194 */
					return BFALSE;
				}
		}

	}



/* node-args->cop */
	obj_t BGl_nodezd2argszd2ze3copze3zzcgen_cgenz00(BgL_typez00_bglt
		BgL_typez00_118, obj_t BgL_argsz00_119, bool_t BgL_argszd2safezd2_120,
		obj_t BgL_locz00_121, obj_t BgL_kontz00_122, obj_t BgL_inpushexitz00_123)
	{
		{	/* Cgen/cgen.scm 1200 */
			{	/* Cgen/cgen.scm 1201 */
				BgL_localz00_bglt BgL_g1485z00_2457;

				{	/* Cgen/cgen.scm 1203 */
					BgL_localz00_bglt BgL_res2822z00_4672;

					{	/* Cgen/cgen.scm 1203 */
						obj_t BgL_idz00_4666;
						BgL_typez00_bglt BgL_typez00_4667;

						BgL_idz00_4666 = CNST_TABLE_REF(3);
						BgL_typez00_4667 =
							((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
						{	/* Cgen/cgen.scm 1183 */
							BgL_localz00_bglt BgL_localz00_4668;

							BgL_localz00_4668 =
								BGl_makezd2localzd2svarz00zzast_localz00(BgL_idz00_4666,
								BgL_typez00_4667);
							{	/* Cgen/cgen.scm 1184 */
								bool_t BgL_test3158z00_8682;

								{	/* Cgen/cgen.scm 1184 */
									obj_t BgL_tmpz00_8683;

									BgL_tmpz00_8683 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_localz00_4668)))->
										BgL_namez00);
									BgL_test3158z00_8682 = STRINGP(BgL_tmpz00_8683);
								}
								if (BgL_test3158z00_8682)
									{	/* Cgen/cgen.scm 1184 */
										BFALSE;
									}
								else
									{	/* Cgen/cgen.scm 1184 */
										BGl_errorz00zz__errorz00(BGl_string2915z00zzcgen_cgenz00,
											BGl_string2916z00zzcgen_cgenz00,
											((obj_t) BgL_localz00_4668));
									}
							}
							BgL_res2822z00_4672 = BgL_localz00_4668;
						}
					}
					BgL_g1485z00_2457 = BgL_res2822z00_4672;
				}
				{
					obj_t BgL_oldzd2actualszd2_2461;
					obj_t BgL_newzd2actualszd2_2462;
					BgL_localz00_bglt BgL_auxz00_2463;
					obj_t BgL_auxsz00_2464;
					obj_t BgL_expsz00_2465;

					BgL_oldzd2actualszd2_2461 = BgL_argsz00_119;
					BgL_newzd2actualszd2_2462 = BNIL;
					BgL_auxz00_2463 = BgL_g1485z00_2457;
					BgL_auxsz00_2464 = BNIL;
					BgL_expsz00_2465 = BNIL;
				BgL_zc3z04anonymousza31993ze3z87_2466:
					if (NULLP(BgL_oldzd2actualszd2_2461))
						{	/* Cgen/cgen.scm 1206 */
							if (NULLP(BgL_auxsz00_2464))
								{	/* Cgen/cgen.scm 1208 */
									obj_t BgL_arg1996z00_2469;

									BgL_arg1996z00_2469 =
										bgl_reverse_bang(BgL_newzd2actualszd2_2462);
									return
										((obj_t(*)(obj_t,
												obj_t))
										PROCEDURE_L_ENTRY(BgL_kontz00_122)) (BgL_kontz00_122,
										BgL_arg1996z00_2469);
								}
							else
								{	/* Cgen/cgen.scm 1209 */
									BgL_cblockz00_bglt BgL_new1489z00_2470;

									{	/* Cgen/cgen.scm 1209 */
										BgL_cblockz00_bglt BgL_new1488z00_2487;

										BgL_new1488z00_2487 =
											((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_cblockz00_bgl))));
										{	/* Cgen/cgen.scm 1209 */
											long BgL_arg2008z00_2488;

											BgL_arg2008z00_2488 =
												BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1488z00_2487),
												BgL_arg2008z00_2488);
										}
										BgL_new1489z00_2470 = BgL_new1488z00_2487;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1489z00_2470)))->
											BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1489z00_2470)))->BgL_typez00) =
										((BgL_typez00_bglt) BgL_typez00_118), BUNSPEC);
									{
										BgL_copz00_bglt BgL_auxz00_8706;

										{	/* Cgen/cgen.scm 1211 */
											BgL_csequencez00_bglt BgL_new1491z00_2471;

											{	/* Cgen/cgen.scm 1213 */
												BgL_csequencez00_bglt BgL_new1490z00_2485;

												BgL_new1490z00_2485 =
													((BgL_csequencez00_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_csequencez00_bgl))));
												{	/* Cgen/cgen.scm 1213 */
													long BgL_arg2007z00_2486;

													{	/* Cgen/cgen.scm 1213 */
														obj_t BgL_classz00_4676;

														BgL_classz00_4676 = BGl_csequencez00zzcgen_copz00;
														BgL_arg2007z00_2486 =
															BGL_CLASS_NUM(BgL_classz00_4676);
													}
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1490z00_2485),
														BgL_arg2007z00_2486);
												}
												BgL_new1491z00_2471 = BgL_new1490z00_2485;
											}
											((((BgL_copz00_bglt) COBJECT(
															((BgL_copz00_bglt) BgL_new1491z00_2471)))->
													BgL_locz00) = ((obj_t) BgL_locz00_121), BUNSPEC);
											((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																BgL_new1491z00_2471)))->BgL_typez00) =
												((BgL_typez00_bglt) BgL_typez00_118), BUNSPEC);
											((((BgL_csequencez00_bglt) COBJECT(BgL_new1491z00_2471))->
													BgL_czd2expzf3z21) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
											{
												obj_t BgL_auxz00_8716;

												{	/* Cgen/cgen.scm 1215 */
													BgL_localzd2varzd2_bglt BgL_arg1997z00_2472;
													BgL_csequencez00_bglt BgL_arg1998z00_2473;
													obj_t BgL_arg1999z00_2474;

													{	/* Cgen/cgen.scm 1215 */
														BgL_localzd2varzd2_bglt BgL_new1493z00_2478;

														{	/* Cgen/cgen.scm 1218 */
															BgL_localzd2varzd2_bglt BgL_new1492z00_2479;

															BgL_new1492z00_2479 =
																((BgL_localzd2varzd2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_localzd2varzd2_bgl))));
															{	/* Cgen/cgen.scm 1218 */
																long BgL_arg2003z00_2480;

																{	/* Cgen/cgen.scm 1218 */
																	obj_t BgL_classz00_4679;

																	BgL_classz00_4679 =
																		BGl_localzd2varzd2zzcgen_copz00;
																	BgL_arg2003z00_2480 =
																		BGL_CLASS_NUM(BgL_classz00_4679);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1492z00_2479),
																	BgL_arg2003z00_2480);
															}
															BgL_new1493z00_2478 = BgL_new1492z00_2479;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1493z00_2478)))->
																BgL_locz00) =
															((obj_t) BgL_locz00_121), BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1493z00_2478)))->BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
														((((BgL_localzd2varzd2_bglt)
																	COBJECT(BgL_new1493z00_2478))->BgL_varsz00) =
															((obj_t) BgL_auxsz00_2464), BUNSPEC);
														BgL_arg1997z00_2472 = BgL_new1493z00_2478;
													}
													{	/* Cgen/cgen.scm 1219 */
														BgL_csequencez00_bglt BgL_new1495z00_2481;

														{	/* Cgen/cgen.scm 1219 */
															BgL_csequencez00_bglt BgL_new1494z00_2482;

															BgL_new1494z00_2482 =
																((BgL_csequencez00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_csequencez00_bgl))));
															{	/* Cgen/cgen.scm 1219 */
																long BgL_arg2004z00_2483;

																{	/* Cgen/cgen.scm 1219 */
																	obj_t BgL_classz00_4682;

																	BgL_classz00_4682 =
																		BGl_csequencez00zzcgen_copz00;
																	BgL_arg2004z00_2483 =
																		BGL_CLASS_NUM(BgL_classz00_4682);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1494z00_2482),
																	BgL_arg2004z00_2483);
															}
															BgL_new1495z00_2481 = BgL_new1494z00_2482;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1495z00_2481)))->
																BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1495z00_2481)))->BgL_typez00) =
															((BgL_typez00_bglt) BgL_typez00_118), BUNSPEC);
														((((BgL_csequencez00_bglt)
																	COBJECT(BgL_new1495z00_2481))->
																BgL_czd2expzf3z21) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
														((((BgL_csequencez00_bglt)
																	COBJECT(BgL_new1495z00_2481))->BgL_copsz00) =
															((obj_t) BgL_expsz00_2465), BUNSPEC);
														BgL_arg1998z00_2473 = BgL_new1495z00_2481;
													}
													{	/* Cgen/cgen.scm 1222 */
														obj_t BgL_arg2006z00_2484;

														BgL_arg2006z00_2484 =
															bgl_reverse_bang(BgL_newzd2actualszd2_2462);
														BgL_arg1999z00_2474 =
															((obj_t(*)(obj_t,
																	obj_t))
															PROCEDURE_L_ENTRY(BgL_kontz00_122))
															(BgL_kontz00_122, BgL_arg2006z00_2484);
													}
													{	/* Cgen/cgen.scm 1214 */
														obj_t BgL_list2000z00_2475;

														{	/* Cgen/cgen.scm 1214 */
															obj_t BgL_arg2001z00_2476;

															{	/* Cgen/cgen.scm 1214 */
																obj_t BgL_arg2002z00_2477;

																BgL_arg2002z00_2477 =
																	MAKE_YOUNG_PAIR(BgL_arg1999z00_2474, BNIL);
																BgL_arg2001z00_2476 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1998z00_2473),
																	BgL_arg2002z00_2477);
															}
															BgL_list2000z00_2475 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1997z00_2472),
																BgL_arg2001z00_2476);
														}
														BgL_auxz00_8716 = BgL_list2000z00_2475;
												}}
												((((BgL_csequencez00_bglt)
															COBJECT(BgL_new1491z00_2471))->BgL_copsz00) =
													((obj_t) BgL_auxz00_8716), BUNSPEC);
											}
											BgL_auxz00_8706 = ((BgL_copz00_bglt) BgL_new1491z00_2471);
										}
										((((BgL_cblockz00_bglt) COBJECT(BgL_new1489z00_2470))->
												BgL_bodyz00) =
											((BgL_copz00_bglt) BgL_auxz00_8706), BUNSPEC);
									}
									return ((obj_t) BgL_new1489z00_2470);
								}
						}
					else
						{	/* Cgen/cgen.scm 1223 */
							BgL_copz00_bglt BgL_copz00_2489;

							{	/* Cgen/cgen.scm 1223 */
								BgL_setqz00_bglt BgL_arg2040z00_2527;

								{	/* Cgen/cgen.scm 1223 */
									obj_t BgL_arg2041z00_2528;

									BgL_arg2041z00_2528 =
										CAR(((obj_t) BgL_oldzd2actualszd2_2461));
									BgL_arg2040z00_2527 =
										BGl_nodezd2setqzd2zzcgen_cgenz00(
										((BgL_variablez00_bglt) BgL_auxz00_2463),
										((BgL_nodez00_bglt) BgL_arg2041z00_2528));
								}
								BgL_copz00_2489 =
									BGl_nodezd2ze3copz31zzcgen_cgenz00(
									((BgL_nodez00_bglt) BgL_arg2040z00_2527),
									BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
									CBOOL(BgL_inpushexitz00_123));
							}
							{	/* Cgen/cgen.scm 1225 */
								bool_t BgL_test3161z00_8759;

								{	/* Cgen/cgen.scm 1225 */
									bool_t BgL_test3162z00_8760;

									{	/* Cgen/cgen.scm 1225 */
										obj_t BgL_classz00_4687;

										BgL_classz00_4687 = BGl_csetqz00zzcgen_copz00;
										{	/* Cgen/cgen.scm 1225 */
											BgL_objectz00_bglt BgL_arg1807z00_4689;

											{	/* Cgen/cgen.scm 1225 */
												obj_t BgL_tmpz00_8761;

												BgL_tmpz00_8761 =
													((obj_t) ((BgL_objectz00_bglt) BgL_copz00_2489));
												BgL_arg1807z00_4689 =
													(BgL_objectz00_bglt) (BgL_tmpz00_8761);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cgen/cgen.scm 1225 */
													long BgL_idxz00_4695;

													BgL_idxz00_4695 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4689);
													BgL_test3162z00_8760 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4695 + 2L)) == BgL_classz00_4687);
												}
											else
												{	/* Cgen/cgen.scm 1225 */
													bool_t BgL_res2824z00_4720;

													{	/* Cgen/cgen.scm 1225 */
														obj_t BgL_oclassz00_4703;

														{	/* Cgen/cgen.scm 1225 */
															obj_t BgL_arg1815z00_4711;
															long BgL_arg1816z00_4712;

															BgL_arg1815z00_4711 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cgen/cgen.scm 1225 */
																long BgL_arg1817z00_4713;

																BgL_arg1817z00_4713 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4689);
																BgL_arg1816z00_4712 =
																	(BgL_arg1817z00_4713 - OBJECT_TYPE);
															}
															BgL_oclassz00_4703 =
																VECTOR_REF(BgL_arg1815z00_4711,
																BgL_arg1816z00_4712);
														}
														{	/* Cgen/cgen.scm 1225 */
															bool_t BgL__ortest_1115z00_4704;

															BgL__ortest_1115z00_4704 =
																(BgL_classz00_4687 == BgL_oclassz00_4703);
															if (BgL__ortest_1115z00_4704)
																{	/* Cgen/cgen.scm 1225 */
																	BgL_res2824z00_4720 =
																		BgL__ortest_1115z00_4704;
																}
															else
																{	/* Cgen/cgen.scm 1225 */
																	long BgL_odepthz00_4705;

																	{	/* Cgen/cgen.scm 1225 */
																		obj_t BgL_arg1804z00_4706;

																		BgL_arg1804z00_4706 = (BgL_oclassz00_4703);
																		BgL_odepthz00_4705 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4706);
																	}
																	if ((2L < BgL_odepthz00_4705))
																		{	/* Cgen/cgen.scm 1225 */
																			obj_t BgL_arg1802z00_4708;

																			{	/* Cgen/cgen.scm 1225 */
																				obj_t BgL_arg1803z00_4709;

																				BgL_arg1803z00_4709 =
																					(BgL_oclassz00_4703);
																				BgL_arg1802z00_4708 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4709, 2L);
																			}
																			BgL_res2824z00_4720 =
																				(BgL_arg1802z00_4708 ==
																				BgL_classz00_4687);
																		}
																	else
																		{	/* Cgen/cgen.scm 1225 */
																			BgL_res2824z00_4720 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3162z00_8760 = BgL_res2824z00_4720;
												}
										}
									}
									if (BgL_test3162z00_8760)
										{	/* Cgen/cgen.scm 1225 */
											if (
												(((obj_t)
														(((BgL_varcz00_bglt) COBJECT(
																	(((BgL_csetqz00_bglt) COBJECT(
																				((BgL_csetqz00_bglt)
																					BgL_copz00_2489)))->BgL_varz00)))->
															BgL_variablez00)) == ((obj_t) BgL_auxz00_2463)))
												{	/* Cgen/cgen.scm 1226 */
													if (BgL_argszd2safezd2_120)
														{	/* Cgen/cgen.scm 1227 */
															BgL_test3161z00_8759 = BgL_argszd2safezd2_120;
														}
													else
														{	/* Cgen/cgen.scm 1228 */
															bool_t BgL__ortest_1497z00_2520;

															{	/* Cgen/cgen.scm 1228 */
																BgL_copz00_bglt BgL_arg2037z00_2524;

																BgL_arg2037z00_2524 =
																	(((BgL_csetqz00_bglt) COBJECT(
																			((BgL_csetqz00_bglt) BgL_copz00_2489)))->
																	BgL_valuez00);
																{	/* Cgen/cgen.scm 1228 */
																	obj_t BgL_classz00_4724;

																	BgL_classz00_4724 = BGl_catomz00zzcgen_copz00;
																	{	/* Cgen/cgen.scm 1228 */
																		BgL_objectz00_bglt BgL_arg1807z00_4726;

																		{	/* Cgen/cgen.scm 1228 */
																			obj_t BgL_tmpz00_8794;

																			BgL_tmpz00_8794 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_arg2037z00_2524));
																			BgL_arg1807z00_4726 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_8794);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Cgen/cgen.scm 1228 */
																				long BgL_idxz00_4732;

																				BgL_idxz00_4732 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_4726);
																				BgL__ortest_1497z00_2520 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_4732 + 2L)) ==
																					BgL_classz00_4724);
																			}
																		else
																			{	/* Cgen/cgen.scm 1228 */
																				bool_t BgL_res2825z00_4757;

																				{	/* Cgen/cgen.scm 1228 */
																					obj_t BgL_oclassz00_4740;

																					{	/* Cgen/cgen.scm 1228 */
																						obj_t BgL_arg1815z00_4748;
																						long BgL_arg1816z00_4749;

																						BgL_arg1815z00_4748 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Cgen/cgen.scm 1228 */
																							long BgL_arg1817z00_4750;

																							BgL_arg1817z00_4750 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_4726);
																							BgL_arg1816z00_4749 =
																								(BgL_arg1817z00_4750 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_4740 =
																							VECTOR_REF(BgL_arg1815z00_4748,
																							BgL_arg1816z00_4749);
																					}
																					{	/* Cgen/cgen.scm 1228 */
																						bool_t BgL__ortest_1115z00_4741;

																						BgL__ortest_1115z00_4741 =
																							(BgL_classz00_4724 ==
																							BgL_oclassz00_4740);
																						if (BgL__ortest_1115z00_4741)
																							{	/* Cgen/cgen.scm 1228 */
																								BgL_res2825z00_4757 =
																									BgL__ortest_1115z00_4741;
																							}
																						else
																							{	/* Cgen/cgen.scm 1228 */
																								long BgL_odepthz00_4742;

																								{	/* Cgen/cgen.scm 1228 */
																									obj_t BgL_arg1804z00_4743;

																									BgL_arg1804z00_4743 =
																										(BgL_oclassz00_4740);
																									BgL_odepthz00_4742 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_4743);
																								}
																								if ((2L < BgL_odepthz00_4742))
																									{	/* Cgen/cgen.scm 1228 */
																										obj_t BgL_arg1802z00_4745;

																										{	/* Cgen/cgen.scm 1228 */
																											obj_t BgL_arg1803z00_4746;

																											BgL_arg1803z00_4746 =
																												(BgL_oclassz00_4740);
																											BgL_arg1802z00_4745 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_4746,
																												2L);
																										}
																										BgL_res2825z00_4757 =
																											(BgL_arg1802z00_4745 ==
																											BgL_classz00_4724);
																									}
																								else
																									{	/* Cgen/cgen.scm 1228 */
																										BgL_res2825z00_4757 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL__ortest_1497z00_2520 =
																					BgL_res2825z00_4757;
																			}
																	}
																}
															}
															if (BgL__ortest_1497z00_2520)
																{	/* Cgen/cgen.scm 1228 */
																	BgL_test3161z00_8759 =
																		BgL__ortest_1497z00_2520;
																}
															else
																{	/* Cgen/cgen.scm 1229 */
																	bool_t BgL__ortest_1498z00_2521;

																	{	/* Cgen/cgen.scm 1229 */
																		BgL_copz00_bglt BgL_arg2036z00_2523;

																		BgL_arg2036z00_2523 =
																			(((BgL_csetqz00_bglt) COBJECT(
																					((BgL_csetqz00_bglt)
																						BgL_copz00_2489)))->BgL_valuez00);
																		{	/* Cgen/cgen.scm 1229 */
																			obj_t BgL_classz00_4759;

																			BgL_classz00_4759 =
																				BGl_varcz00zzcgen_copz00;
																			{	/* Cgen/cgen.scm 1229 */
																				BgL_objectz00_bglt BgL_arg1807z00_4761;

																				{	/* Cgen/cgen.scm 1229 */
																					obj_t BgL_tmpz00_8820;

																					BgL_tmpz00_8820 =
																						((obj_t)
																						((BgL_objectz00_bglt)
																							BgL_arg2036z00_2523));
																					BgL_arg1807z00_4761 =
																						(BgL_objectz00_bglt)
																						(BgL_tmpz00_8820);
																				}
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Cgen/cgen.scm 1229 */
																						long BgL_idxz00_4767;

																						BgL_idxz00_4767 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_4761);
																						BgL__ortest_1498z00_2521 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_4767 + 2L)) ==
																							BgL_classz00_4759);
																					}
																				else
																					{	/* Cgen/cgen.scm 1229 */
																						bool_t BgL_res2826z00_4792;

																						{	/* Cgen/cgen.scm 1229 */
																							obj_t BgL_oclassz00_4775;

																							{	/* Cgen/cgen.scm 1229 */
																								obj_t BgL_arg1815z00_4783;
																								long BgL_arg1816z00_4784;

																								BgL_arg1815z00_4783 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Cgen/cgen.scm 1229 */
																									long BgL_arg1817z00_4785;

																									BgL_arg1817z00_4785 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_4761);
																									BgL_arg1816z00_4784 =
																										(BgL_arg1817z00_4785 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_4775 =
																									VECTOR_REF
																									(BgL_arg1815z00_4783,
																									BgL_arg1816z00_4784);
																							}
																							{	/* Cgen/cgen.scm 1229 */
																								bool_t BgL__ortest_1115z00_4776;

																								BgL__ortest_1115z00_4776 =
																									(BgL_classz00_4759 ==
																									BgL_oclassz00_4775);
																								if (BgL__ortest_1115z00_4776)
																									{	/* Cgen/cgen.scm 1229 */
																										BgL_res2826z00_4792 =
																											BgL__ortest_1115z00_4776;
																									}
																								else
																									{	/* Cgen/cgen.scm 1229 */
																										long BgL_odepthz00_4777;

																										{	/* Cgen/cgen.scm 1229 */
																											obj_t BgL_arg1804z00_4778;

																											BgL_arg1804z00_4778 =
																												(BgL_oclassz00_4775);
																											BgL_odepthz00_4777 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_4778);
																										}
																										if (
																											(2L < BgL_odepthz00_4777))
																											{	/* Cgen/cgen.scm 1229 */
																												obj_t
																													BgL_arg1802z00_4780;
																												{	/* Cgen/cgen.scm 1229 */
																													obj_t
																														BgL_arg1803z00_4781;
																													BgL_arg1803z00_4781 =
																														(BgL_oclassz00_4775);
																													BgL_arg1802z00_4780 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_4781,
																														2L);
																												}
																												BgL_res2826z00_4792 =
																													(BgL_arg1802z00_4780
																													== BgL_classz00_4759);
																											}
																										else
																											{	/* Cgen/cgen.scm 1229 */
																												BgL_res2826z00_4792 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL__ortest_1498z00_2521 =
																							BgL_res2826z00_4792;
																					}
																			}
																		}
																	}
																	if (BgL__ortest_1498z00_2521)
																		{	/* Cgen/cgen.scm 1229 */
																			BgL_test3161z00_8759 =
																				BgL__ortest_1498z00_2521;
																		}
																	else
																		{	/* Cgen/cgen.scm 1230 */
																			BgL_copz00_bglt BgL_arg2034z00_2522;

																			BgL_arg2034z00_2522 =
																				(((BgL_csetqz00_bglt) COBJECT(
																						((BgL_csetqz00_bglt)
																							BgL_copz00_2489)))->BgL_valuez00);
																			{	/* Cgen/cgen.scm 1230 */
																				obj_t BgL_classz00_4794;

																				BgL_classz00_4794 =
																					BGl_cpragmaz00zzcgen_copz00;
																				{	/* Cgen/cgen.scm 1230 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_4796;
																					{	/* Cgen/cgen.scm 1230 */
																						obj_t BgL_tmpz00_8846;

																						BgL_tmpz00_8846 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_arg2034z00_2522));
																						BgL_arg1807z00_4796 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_8846);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Cgen/cgen.scm 1230 */
																							long BgL_idxz00_4802;

																							BgL_idxz00_4802 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_4796);
																							BgL_test3161z00_8759 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_4802 + 2L)) ==
																								BgL_classz00_4794);
																						}
																					else
																						{	/* Cgen/cgen.scm 1230 */
																							bool_t BgL_res2827z00_4827;

																							{	/* Cgen/cgen.scm 1230 */
																								obj_t BgL_oclassz00_4810;

																								{	/* Cgen/cgen.scm 1230 */
																									obj_t BgL_arg1815z00_4818;
																									long BgL_arg1816z00_4819;

																									BgL_arg1815z00_4818 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Cgen/cgen.scm 1230 */
																										long BgL_arg1817z00_4820;

																										BgL_arg1817z00_4820 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_4796);
																										BgL_arg1816z00_4819 =
																											(BgL_arg1817z00_4820 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_4810 =
																										VECTOR_REF
																										(BgL_arg1815z00_4818,
																										BgL_arg1816z00_4819);
																								}
																								{	/* Cgen/cgen.scm 1230 */
																									bool_t
																										BgL__ortest_1115z00_4811;
																									BgL__ortest_1115z00_4811 =
																										(BgL_classz00_4794 ==
																										BgL_oclassz00_4810);
																									if (BgL__ortest_1115z00_4811)
																										{	/* Cgen/cgen.scm 1230 */
																											BgL_res2827z00_4827 =
																												BgL__ortest_1115z00_4811;
																										}
																									else
																										{	/* Cgen/cgen.scm 1230 */
																											long BgL_odepthz00_4812;

																											{	/* Cgen/cgen.scm 1230 */
																												obj_t
																													BgL_arg1804z00_4813;
																												BgL_arg1804z00_4813 =
																													(BgL_oclassz00_4810);
																												BgL_odepthz00_4812 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_4813);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_4812))
																												{	/* Cgen/cgen.scm 1230 */
																													obj_t
																														BgL_arg1802z00_4815;
																													{	/* Cgen/cgen.scm 1230 */
																														obj_t
																															BgL_arg1803z00_4816;
																														BgL_arg1803z00_4816
																															=
																															(BgL_oclassz00_4810);
																														BgL_arg1802z00_4815
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_4816,
																															2L);
																													}
																													BgL_res2827z00_4827 =
																														(BgL_arg1802z00_4815
																														==
																														BgL_classz00_4794);
																												}
																											else
																												{	/* Cgen/cgen.scm 1230 */
																													BgL_res2827z00_4827 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test3161z00_8759 =
																								BgL_res2827z00_4827;
																						}
																				}
																			}
																		}
																}
														}
												}
											else
												{	/* Cgen/cgen.scm 1226 */
													BgL_test3161z00_8759 = ((bool_t) 0);
												}
										}
									else
										{	/* Cgen/cgen.scm 1225 */
											BgL_test3161z00_8759 = ((bool_t) 0);
										}
								}
								if (BgL_test3161z00_8759)
									{	/* Cgen/cgen.scm 1231 */
										obj_t BgL_arg2019z00_2503;
										obj_t BgL_arg2020z00_2504;

										BgL_arg2019z00_2503 =
											CDR(((obj_t) BgL_oldzd2actualszd2_2461));
										{	/* Cgen/cgen.scm 1232 */
											BgL_copz00_bglt BgL_arg2021z00_2505;

											BgL_arg2021z00_2505 =
												(((BgL_csetqz00_bglt) COBJECT(
														((BgL_csetqz00_bglt) BgL_copz00_2489)))->
												BgL_valuez00);
											BgL_arg2020z00_2504 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2021z00_2505),
												BgL_newzd2actualszd2_2462);
										}
										{
											obj_t BgL_newzd2actualszd2_8876;
											obj_t BgL_oldzd2actualszd2_8875;

											BgL_oldzd2actualszd2_8875 = BgL_arg2019z00_2503;
											BgL_newzd2actualszd2_8876 = BgL_arg2020z00_2504;
											BgL_newzd2actualszd2_2462 = BgL_newzd2actualszd2_8876;
											BgL_oldzd2actualszd2_2461 = BgL_oldzd2actualszd2_8875;
											goto BgL_zc3z04anonymousza31993ze3z87_2466;
										}
									}
								else
									{	/* Cgen/cgen.scm 1225 */
										{	/* Cgen/cgen.scm 1237 */
											BgL_typez00_bglt BgL_arg2022z00_2506;

											{	/* Cgen/cgen.scm 1237 */
												obj_t BgL_arg2024z00_2507;

												BgL_arg2024z00_2507 =
													CAR(((obj_t) BgL_oldzd2actualszd2_2461));
												BgL_arg2022z00_2506 =
													BGl_getzd2typezd2zztype_typeofz00(
													((BgL_nodez00_bglt) BgL_arg2024z00_2507),
													((bool_t) 0));
											}
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_auxz00_2463)))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_arg2022z00_2506), BUNSPEC);
										}
										{	/* Cgen/cgen.scm 1238 */
											obj_t BgL_arg2025z00_2508;
											obj_t BgL_arg2026z00_2509;
											BgL_localz00_bglt BgL_arg2027z00_2510;
											obj_t BgL_arg2029z00_2511;
											obj_t BgL_arg2030z00_2512;

											BgL_arg2025z00_2508 =
												CDR(((obj_t) BgL_oldzd2actualszd2_2461));
											{	/* Cgen/cgen.scm 1239 */
												BgL_varcz00_bglt BgL_arg2031z00_2513;

												{	/* Cgen/cgen.scm 1239 */
													BgL_varcz00_bglt BgL_new1500z00_2514;

													{	/* Cgen/cgen.scm 1241 */
														BgL_varcz00_bglt BgL_new1499z00_2515;

														BgL_new1499z00_2515 =
															((BgL_varcz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_varcz00_bgl))));
														{	/* Cgen/cgen.scm 1241 */
															long BgL_arg2033z00_2516;

															BgL_arg2033z00_2516 =
																BGL_CLASS_NUM(BGl_varcz00zzcgen_copz00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1499z00_2515),
																BgL_arg2033z00_2516);
														}
														BgL_new1500z00_2514 = BgL_new1499z00_2515;
													}
													((((BgL_copz00_bglt) COBJECT(
																	((BgL_copz00_bglt) BgL_new1500z00_2514)))->
															BgL_locz00) = ((obj_t) BgL_locz00_121), BUNSPEC);
													((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																		BgL_new1500z00_2514)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																	COBJECT(((BgL_variablez00_bglt)
																			BgL_auxz00_2463)))->BgL_typez00)),
														BUNSPEC);
													((((BgL_varcz00_bglt) COBJECT(BgL_new1500z00_2514))->
															BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BgL_auxz00_2463)), BUNSPEC);
													BgL_arg2031z00_2513 = BgL_new1500z00_2514;
												}
												BgL_arg2026z00_2509 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_arg2031z00_2513),
													BgL_newzd2actualszd2_2462);
											}
											{	/* Cgen/cgen.scm 1244 */
												BgL_localz00_bglt BgL_res2828z00_4844;

												{	/* Cgen/cgen.scm 1244 */
													obj_t BgL_idz00_4838;
													BgL_typez00_bglt BgL_typez00_4839;

													BgL_idz00_4838 = CNST_TABLE_REF(3);
													BgL_typez00_4839 =
														((BgL_typez00_bglt)
														BGl_za2objza2z00zztype_cachez00);
													{	/* Cgen/cgen.scm 1183 */
														BgL_localz00_bglt BgL_localz00_4840;

														BgL_localz00_4840 =
															BGl_makezd2localzd2svarz00zzast_localz00
															(BgL_idz00_4838, BgL_typez00_4839);
														{	/* Cgen/cgen.scm 1184 */
															bool_t BgL_test3179z00_8902;

															{	/* Cgen/cgen.scm 1184 */
																obj_t BgL_tmpz00_8903;

																BgL_tmpz00_8903 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_localz00_4840)))->BgL_namez00);
																BgL_test3179z00_8902 = STRINGP(BgL_tmpz00_8903);
															}
															if (BgL_test3179z00_8902)
																{	/* Cgen/cgen.scm 1184 */
																	BFALSE;
																}
															else
																{	/* Cgen/cgen.scm 1184 */
																	BGl_errorz00zz__errorz00
																		(BGl_string2915z00zzcgen_cgenz00,
																		BGl_string2916z00zzcgen_cgenz00,
																		((obj_t) BgL_localz00_4840));
																}
														}
														BgL_res2828z00_4844 = BgL_localz00_4840;
													}
												}
												BgL_arg2027z00_2510 = BgL_res2828z00_4844;
											}
											BgL_arg2029z00_2511 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_auxz00_2463), BgL_auxsz00_2464);
											BgL_arg2030z00_2512 =
												MAKE_YOUNG_PAIR(
												((obj_t) BgL_copz00_2489), BgL_expsz00_2465);
											{
												obj_t BgL_expsz00_8917;
												obj_t BgL_auxsz00_8916;
												BgL_localz00_bglt BgL_auxz00_8915;
												obj_t BgL_newzd2actualszd2_8914;
												obj_t BgL_oldzd2actualszd2_8913;

												BgL_oldzd2actualszd2_8913 = BgL_arg2025z00_2508;
												BgL_newzd2actualszd2_8914 = BgL_arg2026z00_2509;
												BgL_auxz00_8915 = BgL_arg2027z00_2510;
												BgL_auxsz00_8916 = BgL_arg2029z00_2511;
												BgL_expsz00_8917 = BgL_arg2030z00_2512;
												BgL_expsz00_2465 = BgL_expsz00_8917;
												BgL_auxsz00_2464 = BgL_auxsz00_8916;
												BgL_auxz00_2463 = BgL_auxz00_8915;
												BgL_newzd2actualszd2_2462 = BgL_newzd2actualszd2_8914;
												BgL_oldzd2actualszd2_2461 = BgL_oldzd2actualszd2_8913;
												goto BgL_zc3z04anonymousza31993ze3z87_2466;
											}
										}
									}
							}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 15 */
			{	/* Cgen/cgen.scm 46 */
				obj_t BgL_arg2047z00_2534;
				obj_t BgL_arg2048z00_2535;

				{	/* Cgen/cgen.scm 46 */
					obj_t BgL_v1725z00_2561;

					BgL_v1725z00_2561 = create_vector(2L);
					{	/* Cgen/cgen.scm 46 */
						obj_t BgL_arg2061z00_2562;

						BgL_arg2061z00_2562 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc2918z00zzcgen_cgenz00, BGl_proc2917z00zzcgen_cgenz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1725z00_2561, 0L, BgL_arg2061z00_2562);
					}
					{	/* Cgen/cgen.scm 46 */
						obj_t BgL_arg2067z00_2572;

						BgL_arg2067z00_2572 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc2920z00zzcgen_cgenz00, BGl_proc2919z00zzcgen_cgenz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1725z00_2561, 1L, BgL_arg2067z00_2572);
					}
					BgL_arg2047z00_2534 = BgL_v1725z00_2561;
				}
				{	/* Cgen/cgen.scm 46 */
					obj_t BgL_v1726z00_2582;

					BgL_v1726z00_2582 = create_vector(0L);
					BgL_arg2048z00_2535 = BgL_v1726z00_2582;
				}
				return (BGl_retblockzf2gotozf2zzcgen_cgenz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(7),
						CNST_TABLE_REF(8), BGl_retblockz00zzast_nodez00, 22818L,
						BGl_proc2924z00zzcgen_cgenz00, BGl_proc2923z00zzcgen_cgenz00,
						BFALSE, BGl_proc2922z00zzcgen_cgenz00,
						BGl_proc2921z00zzcgen_cgenz00, BgL_arg2047z00_2534,
						BgL_arg2048z00_2535), BUNSPEC);
			}
		}

	}



/* &lambda2057 */
	BgL_retblockz00_bglt BGl_z62lambda2057z62zzcgen_cgenz00(obj_t BgL_envz00_6536,
		obj_t BgL_o1145z00_6537)
	{
		{	/* Cgen/cgen.scm 46 */
			{	/* Cgen/cgen.scm 46 */
				long BgL_arg2058z00_6890;

				{	/* Cgen/cgen.scm 46 */
					obj_t BgL_arg2059z00_6891;

					{	/* Cgen/cgen.scm 46 */
						obj_t BgL_arg2060z00_6892;

						{	/* Cgen/cgen.scm 46 */
							obj_t BgL_arg1815z00_6893;
							long BgL_arg1816z00_6894;

							BgL_arg1815z00_6893 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cgen/cgen.scm 46 */
								long BgL_arg1817z00_6895;

								BgL_arg1817z00_6895 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt) BgL_o1145z00_6537)));
								BgL_arg1816z00_6894 = (BgL_arg1817z00_6895 - OBJECT_TYPE);
							}
							BgL_arg2060z00_6892 =
								VECTOR_REF(BgL_arg1815z00_6893, BgL_arg1816z00_6894);
						}
						BgL_arg2059z00_6891 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2060z00_6892);
					}
					{	/* Cgen/cgen.scm 46 */
						obj_t BgL_tmpz00_8938;

						BgL_tmpz00_8938 = ((obj_t) BgL_arg2059z00_6891);
						BgL_arg2058z00_6890 = BGL_CLASS_NUM(BgL_tmpz00_8938);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt) BgL_o1145z00_6537)), BgL_arg2058z00_6890);
			}
			{	/* Cgen/cgen.scm 46 */
				BgL_objectz00_bglt BgL_tmpz00_8944;

				BgL_tmpz00_8944 =
					((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_o1145z00_6537));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8944, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_o1145z00_6537));
			return
				((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1145z00_6537));
		}

	}



/* &<@anonymous:2056> */
	obj_t BGl_z62zc3z04anonymousza32056ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6538,
		obj_t BgL_new1144z00_6539)
	{
		{	/* Cgen/cgen.scm 46 */
			{
				BgL_retblockz00_bglt BgL_auxz00_8952;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_retblockz00_bglt) BgL_new1144z00_6539))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_8956;

					{	/* Cgen/cgen.scm 46 */
						obj_t BgL_classz00_6897;

						BgL_classz00_6897 = BGl_typez00zztype_typez00;
						{	/* Cgen/cgen.scm 46 */
							obj_t BgL__ortest_1117z00_6898;

							BgL__ortest_1117z00_6898 = BGL_CLASS_NIL(BgL_classz00_6897);
							if (CBOOL(BgL__ortest_1117z00_6898))
								{	/* Cgen/cgen.scm 46 */
									BgL_auxz00_8956 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6898);
								}
							else
								{	/* Cgen/cgen.scm 46 */
									BgL_auxz00_8956 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6897));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_retblockz00_bglt) BgL_new1144z00_6539))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_8956), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8966;

					{	/* Cgen/cgen.scm 46 */
						obj_t BgL_classz00_6899;

						BgL_classz00_6899 = BGl_nodez00zzast_nodez00;
						{	/* Cgen/cgen.scm 46 */
							obj_t BgL__ortest_1117z00_6900;

							BgL__ortest_1117z00_6900 = BGL_CLASS_NIL(BgL_classz00_6899);
							if (CBOOL(BgL__ortest_1117z00_6900))
								{	/* Cgen/cgen.scm 46 */
									BgL_auxz00_8966 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_6900);
								}
							else
								{	/* Cgen/cgen.scm 46 */
									BgL_auxz00_8966 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6899));
								}
						}
					}
					((((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_new1144z00_6539))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_8966), BUNSPEC);
				}
				{
					BgL_retblockzf2gotozf2_bglt BgL_auxz00_8976;

					{
						obj_t BgL_auxz00_8977;

						{	/* Cgen/cgen.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_8978;

							BgL_tmpz00_8978 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1144z00_6539));
							BgL_auxz00_8977 = BGL_OBJECT_WIDENING(BgL_tmpz00_8978);
						}
						BgL_auxz00_8976 = ((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_8977);
					}
					((((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_8976))->
							BgL_localz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_retblockzf2gotozf2_bglt BgL_auxz00_8984;

					{
						obj_t BgL_auxz00_8985;

						{	/* Cgen/cgen.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_8986;

							BgL_tmpz00_8986 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1144z00_6539));
							BgL_auxz00_8985 = BGL_OBJECT_WIDENING(BgL_tmpz00_8986);
						}
						BgL_auxz00_8984 = ((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_8985);
					}
					((((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_8984))->
							BgL_labelz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_8952 = ((BgL_retblockz00_bglt) BgL_new1144z00_6539);
				return ((obj_t) BgL_auxz00_8952);
			}
		}

	}



/* &lambda2052 */
	BgL_retblockz00_bglt BGl_z62lambda2052z62zzcgen_cgenz00(obj_t BgL_envz00_6540,
		obj_t BgL_o1141z00_6541)
	{
		{	/* Cgen/cgen.scm 46 */
			{	/* Cgen/cgen.scm 46 */
				BgL_retblockzf2gotozf2_bglt BgL_wide1143z00_6902;

				BgL_wide1143z00_6902 =
					((BgL_retblockzf2gotozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_retblockzf2gotozf2_bgl))));
				{	/* Cgen/cgen.scm 46 */
					obj_t BgL_auxz00_8999;
					BgL_objectz00_bglt BgL_tmpz00_8995;

					BgL_auxz00_8999 = ((obj_t) BgL_wide1143z00_6902);
					BgL_tmpz00_8995 =
						((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt)
							((BgL_retblockz00_bglt) BgL_o1141z00_6541)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8995, BgL_auxz00_8999);
				}
				((BgL_objectz00_bglt)
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1141z00_6541)));
				{	/* Cgen/cgen.scm 46 */
					long BgL_arg2055z00_6903;

					BgL_arg2055z00_6903 =
						BGL_CLASS_NUM(BGl_retblockzf2gotozf2zzcgen_cgenz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_retblockz00_bglt)
								((BgL_retblockz00_bglt) BgL_o1141z00_6541))),
						BgL_arg2055z00_6903);
				}
				return
					((BgL_retblockz00_bglt)
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1141z00_6541)));
			}
		}

	}



/* &lambda2049 */
	BgL_retblockz00_bglt BGl_z62lambda2049z62zzcgen_cgenz00(obj_t BgL_envz00_6542,
		obj_t BgL_loc1136z00_6543, obj_t BgL_type1137z00_6544,
		obj_t BgL_body1138z00_6545, obj_t BgL_local1139z00_6546,
		obj_t BgL_label1140z00_6547)
	{
		{	/* Cgen/cgen.scm 46 */
			{	/* Cgen/cgen.scm 46 */
				BgL_retblockz00_bglt BgL_new1504z00_6906;

				{	/* Cgen/cgen.scm 46 */
					BgL_retblockz00_bglt BgL_tmp1502z00_6907;
					BgL_retblockzf2gotozf2_bglt BgL_wide1503z00_6908;

					{
						BgL_retblockz00_bglt BgL_auxz00_9013;

						{	/* Cgen/cgen.scm 46 */
							BgL_retblockz00_bglt BgL_new1501z00_6909;

							BgL_new1501z00_6909 =
								((BgL_retblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_retblockz00_bgl))));
							{	/* Cgen/cgen.scm 46 */
								long BgL_arg2051z00_6910;

								BgL_arg2051z00_6910 =
									BGL_CLASS_NUM(BGl_retblockz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1501z00_6909),
									BgL_arg2051z00_6910);
							}
							{	/* Cgen/cgen.scm 46 */
								BgL_objectz00_bglt BgL_tmpz00_9018;

								BgL_tmpz00_9018 = ((BgL_objectz00_bglt) BgL_new1501z00_6909);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9018, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1501z00_6909);
							BgL_auxz00_9013 = BgL_new1501z00_6909;
						}
						BgL_tmp1502z00_6907 = ((BgL_retblockz00_bglt) BgL_auxz00_9013);
					}
					BgL_wide1503z00_6908 =
						((BgL_retblockzf2gotozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_retblockzf2gotozf2_bgl))));
					{	/* Cgen/cgen.scm 46 */
						obj_t BgL_auxz00_9026;
						BgL_objectz00_bglt BgL_tmpz00_9024;

						BgL_auxz00_9026 = ((obj_t) BgL_wide1503z00_6908);
						BgL_tmpz00_9024 = ((BgL_objectz00_bglt) BgL_tmp1502z00_6907);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9024, BgL_auxz00_9026);
					}
					((BgL_objectz00_bglt) BgL_tmp1502z00_6907);
					{	/* Cgen/cgen.scm 46 */
						long BgL_arg2050z00_6911;

						BgL_arg2050z00_6911 =
							BGL_CLASS_NUM(BGl_retblockzf2gotozf2zzcgen_cgenz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1502z00_6907), BgL_arg2050z00_6911);
					}
					BgL_new1504z00_6906 = ((BgL_retblockz00_bglt) BgL_tmp1502z00_6907);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1504z00_6906)))->BgL_locz00) =
					((obj_t) BgL_loc1136z00_6543), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1504z00_6906)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1137z00_6544)),
					BUNSPEC);
				((((BgL_retblockz00_bglt) COBJECT(((BgL_retblockz00_bglt)
									BgL_new1504z00_6906)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1138z00_6545)),
					BUNSPEC);
				{
					BgL_retblockzf2gotozf2_bglt BgL_auxz00_9042;

					{
						obj_t BgL_auxz00_9043;

						{	/* Cgen/cgen.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_9044;

							BgL_tmpz00_9044 = ((BgL_objectz00_bglt) BgL_new1504z00_6906);
							BgL_auxz00_9043 = BGL_OBJECT_WIDENING(BgL_tmpz00_9044);
						}
						BgL_auxz00_9042 = ((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9043);
					}
					((((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9042))->
							BgL_localz00) = ((obj_t) BgL_local1139z00_6546), BUNSPEC);
				}
				{
					BgL_retblockzf2gotozf2_bglt BgL_auxz00_9049;

					{
						obj_t BgL_auxz00_9050;

						{	/* Cgen/cgen.scm 46 */
							BgL_objectz00_bglt BgL_tmpz00_9051;

							BgL_tmpz00_9051 = ((BgL_objectz00_bglt) BgL_new1504z00_6906);
							BgL_auxz00_9050 = BGL_OBJECT_WIDENING(BgL_tmpz00_9051);
						}
						BgL_auxz00_9049 = ((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9050);
					}
					((((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9049))->
							BgL_labelz00) = ((obj_t) BgL_label1140z00_6547), BUNSPEC);
				}
				return BgL_new1504z00_6906;
			}
		}

	}



/* &lambda2071 */
	obj_t BGl_z62lambda2071z62zzcgen_cgenz00(obj_t BgL_envz00_6548,
		obj_t BgL_oz00_6549, obj_t BgL_vz00_6550)
	{
		{	/* Cgen/cgen.scm 46 */
			{
				BgL_retblockzf2gotozf2_bglt BgL_auxz00_9056;

				{
					obj_t BgL_auxz00_9057;

					{	/* Cgen/cgen.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_9058;

						BgL_tmpz00_9058 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_6549));
						BgL_auxz00_9057 = BGL_OBJECT_WIDENING(BgL_tmpz00_9058);
					}
					BgL_auxz00_9056 = ((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9057);
				}
				return
					((((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9056))->
						BgL_labelz00) = ((obj_t) BgL_vz00_6550), BUNSPEC);
			}
		}

	}



/* &lambda2070 */
	obj_t BGl_z62lambda2070z62zzcgen_cgenz00(obj_t BgL_envz00_6551,
		obj_t BgL_oz00_6552)
	{
		{	/* Cgen/cgen.scm 46 */
			{
				BgL_retblockzf2gotozf2_bglt BgL_auxz00_9064;

				{
					obj_t BgL_auxz00_9065;

					{	/* Cgen/cgen.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_9066;

						BgL_tmpz00_9066 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_6552));
						BgL_auxz00_9065 = BGL_OBJECT_WIDENING(BgL_tmpz00_9066);
					}
					BgL_auxz00_9064 = ((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9065);
				}
				return
					(((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9064))->
					BgL_labelz00);
			}
		}

	}



/* &lambda2065 */
	obj_t BGl_z62lambda2065z62zzcgen_cgenz00(obj_t BgL_envz00_6553,
		obj_t BgL_oz00_6554, obj_t BgL_vz00_6555)
	{
		{	/* Cgen/cgen.scm 46 */
			{
				BgL_retblockzf2gotozf2_bglt BgL_auxz00_9072;

				{
					obj_t BgL_auxz00_9073;

					{	/* Cgen/cgen.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_9074;

						BgL_tmpz00_9074 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_6554));
						BgL_auxz00_9073 = BGL_OBJECT_WIDENING(BgL_tmpz00_9074);
					}
					BgL_auxz00_9072 = ((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9073);
				}
				return
					((((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9072))->
						BgL_localz00) = ((obj_t) BgL_vz00_6555), BUNSPEC);
			}
		}

	}



/* &lambda2064 */
	obj_t BGl_z62lambda2064z62zzcgen_cgenz00(obj_t BgL_envz00_6556,
		obj_t BgL_oz00_6557)
	{
		{	/* Cgen/cgen.scm 46 */
			{
				BgL_retblockzf2gotozf2_bglt BgL_auxz00_9080;

				{
					obj_t BgL_auxz00_9081;

					{	/* Cgen/cgen.scm 46 */
						BgL_objectz00_bglt BgL_tmpz00_9082;

						BgL_tmpz00_9082 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_6557));
						BgL_auxz00_9081 = BGL_OBJECT_WIDENING(BgL_tmpz00_9082);
					}
					BgL_auxz00_9080 = ((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9081);
				}
				return
					(((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9080))->
					BgL_localz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_proc2925z00zzcgen_cgenz00, BGl_nodez00zzast_nodez00,
				BGl_string2926z00zzcgen_cgenz00);
		}

	}



/* &node->cop1727 */
	obj_t BGl_z62nodezd2ze3cop1727z53zzcgen_cgenz00(obj_t BgL_envz00_6559,
		obj_t BgL_nodez00_6560, obj_t BgL_kontz00_6561,
		obj_t BgL_inpushexitz00_6562)
	{
		{	/* Cgen/cgen.scm 341 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(9),
				BGl_string2927z00zzcgen_cgenz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_6560)));
		}

	}



/* node->cop */
	BGL_EXPORTED_DEF BgL_copz00_bglt
		BGl_nodezd2ze3copz31zzcgen_cgenz00(BgL_nodez00_bglt BgL_nodez00_31,
		obj_t BgL_kontz00_32, bool_t BgL_inpushexitz00_33)
	{
		{	/* Cgen/cgen.scm 341 */
			{	/* Cgen/cgen.scm 341 */
				obj_t BgL_method1728z00_2593;

				{	/* Cgen/cgen.scm 341 */
					obj_t BgL_res2833z00_4914;

					{	/* Cgen/cgen.scm 341 */
						long BgL_objzd2classzd2numz00_4885;

						BgL_objzd2classzd2numz00_4885 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_31));
						{	/* Cgen/cgen.scm 341 */
							obj_t BgL_arg1811z00_4886;

							BgL_arg1811z00_4886 =
								PROCEDURE_REF(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
								(int) (1L));
							{	/* Cgen/cgen.scm 341 */
								int BgL_offsetz00_4889;

								BgL_offsetz00_4889 = (int) (BgL_objzd2classzd2numz00_4885);
								{	/* Cgen/cgen.scm 341 */
									long BgL_offsetz00_4890;

									BgL_offsetz00_4890 =
										((long) (BgL_offsetz00_4889) - OBJECT_TYPE);
									{	/* Cgen/cgen.scm 341 */
										long BgL_modz00_4891;

										BgL_modz00_4891 =
											(BgL_offsetz00_4890 >> (int) ((long) ((int) (4L))));
										{	/* Cgen/cgen.scm 341 */
											long BgL_restz00_4893;

											BgL_restz00_4893 =
												(BgL_offsetz00_4890 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cgen/cgen.scm 341 */

												{	/* Cgen/cgen.scm 341 */
													obj_t BgL_bucketz00_4895;

													BgL_bucketz00_4895 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4886), BgL_modz00_4891);
													BgL_res2833z00_4914 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4895), BgL_restz00_4893);
					}}}}}}}}
					BgL_method1728z00_2593 = BgL_res2833z00_4914;
				}
				return
					((BgL_copz00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1728z00_2593,
						((obj_t) BgL_nodez00_31), BgL_kontz00_32,
						BBOOL(BgL_inpushexitz00_33)));
			}
		}

	}



/* &node->cop */
	BgL_copz00_bglt BGl_z62nodezd2ze3copz53zzcgen_cgenz00(obj_t BgL_envz00_6563,
		obj_t BgL_nodez00_6564, obj_t BgL_kontz00_6565,
		obj_t BgL_inpushexitz00_6566)
	{
		{	/* Cgen/cgen.scm 341 */
			return
				BGl_nodezd2ze3copz31zzcgen_cgenz00(
				((BgL_nodez00_bglt) BgL_nodez00_6564), BgL_kontz00_6565,
				CBOOL(BgL_inpushexitz00_6566));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_literalz00zzast_nodez00,
				BGl_proc2928z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_patchz00zzast_nodez00,
				BGl_proc2930z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_genpatchidz00zzast_nodez00, BGl_proc2931z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_kwotez00zzast_nodez00,
				BGl_proc2932z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_varz00zzast_nodez00,
				BGl_proc2933z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_closurez00zzast_nodez00,
				BGl_proc2934z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_sequencez00zzast_nodez00,
				BGl_proc2935z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_syncz00zzast_nodez00,
				BGl_proc2936z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_pragmaz00zzast_nodez00,
				BGl_proc2937z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_privatez00zzast_nodez00,
				BGl_proc2938z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_castz00zzast_nodez00,
				BGl_proc2939z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_setqz00zzast_nodez00,
				BGl_proc2940z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2941z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_failz00zzast_nodez00,
				BGl_proc2942z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_switchz00zzast_nodez00,
				BGl_proc2943z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2944z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2945z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2946z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2947z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_retblockz00zzast_nodez00,
				BGl_proc2948z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_returnz00zzast_nodez00,
				BGl_proc2949z00zzcgen_cgenz00, BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2950z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2951z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2952z00zzcgen_cgenz00,
				BGl_string2929z00zzcgen_cgenz00);
		}

	}



/* &node->cop-box-set!1777 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2boxzd2setz121777z41zzcgen_cgenz00(obj_t
		BgL_envz00_6598, obj_t BgL_nodez00_6599, obj_t BgL_kontz00_6600,
		obj_t BgL_inpushexitz00_6601)
	{
		{	/* Cgen/cgen.scm 1149 */
			{	/* Tools/trace.sch 53 */
				BgL_variablez00_bglt BgL_vz00_6920;

				BgL_vz00_6920 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6599)))->
								BgL_varz00)))->BgL_variablez00);
				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg2678z00_6921;

					BgL_arg2678z00_6921 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6599)))->BgL_valuez00);
					{	/* Tools/trace.sch 53 */
						obj_t BgL_zc3z04anonymousza32681ze3z87_6922;

						BgL_zc3z04anonymousza32681ze3z87_6922 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza32681ze3ze5zzcgen_cgenz00, (int) (1L),
							(int) (3L));
						PROCEDURE_SET(BgL_zc3z04anonymousza32681ze3z87_6922, (int) (0L),
							((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6599)));
						PROCEDURE_SET(BgL_zc3z04anonymousza32681ze3z87_6922, (int) (1L),
							((obj_t) BgL_vz00_6920));
						PROCEDURE_SET(BgL_zc3z04anonymousza32681ze3z87_6922, (int) (2L),
							BgL_kontz00_6600);
						return BGl_nodezd2ze3copz31zzcgen_cgenz00(BgL_arg2678z00_6921,
							BgL_zc3z04anonymousza32681ze3z87_6922,
							CBOOL(BgL_inpushexitz00_6601));
					}
				}
			}
		}

	}



/* &<@anonymous:2681> */
	obj_t BGl_z62zc3z04anonymousza32681ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6602,
		obj_t BgL_vlz00_6606)
	{
		{	/* Cgen/cgen.scm 1156 */
			{	/* Cgen/cgen.scm 1156 */
				BgL_boxzd2setz12zc0_bglt BgL_i1474z00_6603;
				BgL_variablez00_bglt BgL_vz00_6604;
				obj_t BgL_kontz00_6605;

				BgL_i1474z00_6603 =
					((BgL_boxzd2setz12zc0_bglt)
					PROCEDURE_REF(BgL_envz00_6602, (int) (0L)));
				BgL_vz00_6604 =
					((BgL_variablez00_bglt) PROCEDURE_REF(BgL_envz00_6602, (int) (1L)));
				BgL_kontz00_6605 = PROCEDURE_REF(BgL_envz00_6602, (int) (2L));
				{	/* Cgen/cgen.scm 1156 */
					BgL_cboxzd2setz12zc0_bglt BgL_arg2682z00_6923;

					{	/* Cgen/cgen.scm 1156 */
						BgL_cboxzd2setz12zc0_bglt BgL_new1476z00_6924;

						{	/* Cgen/cgen.scm 1158 */
							BgL_cboxzd2setz12zc0_bglt BgL_new1475z00_6925;

							BgL_new1475z00_6925 =
								((BgL_cboxzd2setz12zc0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cboxzd2setz12zc0_bgl))));
							{	/* Cgen/cgen.scm 1158 */
								long BgL_arg2685z00_6926;

								BgL_arg2685z00_6926 =
									BGL_CLASS_NUM(BGl_cboxzd2setz12zc0zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1475z00_6925),
									BgL_arg2685z00_6926);
							}
							BgL_new1476z00_6924 = BgL_new1475z00_6925;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1476z00_6924)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_i1474z00_6603)))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1476z00_6924)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) BgL_i1474z00_6603)))->
									BgL_typez00)), BUNSPEC);
						{
							BgL_copz00_bglt BgL_auxz00_9193;

							{	/* Cgen/cgen.scm 1159 */
								BgL_varcz00_bglt BgL_new1478z00_6927;

								{	/* Cgen/cgen.scm 1161 */
									BgL_varcz00_bglt BgL_new1477z00_6928;

									BgL_new1477z00_6928 =
										((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_varcz00_bgl))));
									{	/* Cgen/cgen.scm 1161 */
										long BgL_arg2684z00_6929;

										{	/* Cgen/cgen.scm 1161 */
											obj_t BgL_classz00_6930;

											BgL_classz00_6930 = BGl_varcz00zzcgen_copz00;
											BgL_arg2684z00_6929 = BGL_CLASS_NUM(BgL_classz00_6930);
										}
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1477z00_6928),
											BgL_arg2684z00_6929);
									}
									BgL_new1478z00_6927 = BgL_new1477z00_6928;
								}
								((((BgL_copz00_bglt) COBJECT(
												((BgL_copz00_bglt) BgL_new1478z00_6927)))->BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_i1474z00_6603)))->BgL_locz00)), BUNSPEC);
								((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
													BgL_new1478z00_6927)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_variablez00_bglt)
												COBJECT(BgL_vz00_6604))->BgL_typez00)), BUNSPEC);
								((((BgL_varcz00_bglt) COBJECT(BgL_new1478z00_6927))->
										BgL_variablez00) =
									((BgL_variablez00_bglt) BgL_vz00_6604), BUNSPEC);
								BgL_auxz00_9193 = ((BgL_copz00_bglt) BgL_new1478z00_6927);
							}
							((((BgL_cboxzd2setz12zc0_bglt) COBJECT(BgL_new1476z00_6924))->
									BgL_varz00) = ((BgL_copz00_bglt) BgL_auxz00_9193), BUNSPEC);
						}
						((((BgL_cboxzd2setz12zc0_bglt) COBJECT(BgL_new1476z00_6924))->
								BgL_valuez00) =
							((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vlz00_6606)), BUNSPEC);
						BgL_arg2682z00_6923 = BgL_new1476z00_6924;
					}
					return
						BGL_PROCEDURE_CALL1(BgL_kontz00_6605,
						((obj_t) BgL_arg2682z00_6923));
				}
			}
		}

	}



/* &node->cop-box-ref1775 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2boxzd2ref1775z53zzcgen_cgenz00(obj_t
		BgL_envz00_6607, obj_t BgL_nodez00_6608, obj_t BgL_kontz00_6609,
		obj_t BgL_inpushexitz00_6610)
	{
		{	/* Cgen/cgen.scm 1134 */
			{	/* Tools/trace.sch 53 */
				BgL_copz00_bglt BgL_arg2672z00_6932;

				{	/* Tools/trace.sch 53 */
					BgL_varz00_bglt BgL_arg2673z00_6933;

					BgL_arg2673z00_6933 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_6608)))->BgL_varz00);
					{	/* Tools/trace.sch 53 */
						obj_t BgL_zc3z04anonymousza32675ze3z87_6934;

						BgL_zc3z04anonymousza32675ze3z87_6934 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza32675ze3ze5zzcgen_cgenz00, (int) (1L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza32675ze3z87_6934, (int) (0L),
							((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_6608)));
						BgL_arg2672z00_6932 =
							BGl_nodezd2ze3copz31zzcgen_cgenz00(((BgL_nodez00_bglt)
								BgL_arg2673z00_6933), BgL_zc3z04anonymousza32675ze3z87_6934,
							CBOOL(BgL_inpushexitz00_6610));
				}}
				return
					((BgL_copz00_bglt)
					BGL_PROCEDURE_CALL1(BgL_kontz00_6609, ((obj_t) BgL_arg2672z00_6932)));
			}
		}

	}



/* &<@anonymous:2675> */
	obj_t BGl_z62zc3z04anonymousza32675ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6611,
		obj_t BgL_vz00_6613)
	{
		{	/* Cgen/cgen.scm 1140 */
			{	/* Cgen/cgen.scm 1140 */
				BgL_boxzd2refzd2_bglt BgL_i1471z00_6612;

				BgL_i1471z00_6612 =
					((BgL_boxzd2refzd2_bglt) PROCEDURE_REF(BgL_envz00_6611, (int) (0L)));
				{
					BgL_cboxzd2refzd2_bglt BgL_auxz00_9236;

					{	/* Cgen/cgen.scm 1140 */
						BgL_cboxzd2refzd2_bglt BgL_new1473z00_6935;

						{	/* Cgen/cgen.scm 1142 */
							BgL_cboxzd2refzd2_bglt BgL_new1472z00_6936;

							BgL_new1472z00_6936 =
								((BgL_cboxzd2refzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cboxzd2refzd2_bgl))));
							{	/* Cgen/cgen.scm 1142 */
								long BgL_arg2676z00_6937;

								BgL_arg2676z00_6937 =
									BGL_CLASS_NUM(BGl_cboxzd2refzd2zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1472z00_6936),
									BgL_arg2676z00_6937);
							}
							BgL_new1473z00_6935 = BgL_new1472z00_6936;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1473z00_6935)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_i1471z00_6612)))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1473z00_6935)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) BgL_i1471z00_6612)))->
									BgL_typez00)), BUNSPEC);
						((((BgL_cboxzd2refzd2_bglt) COBJECT(BgL_new1473z00_6935))->
								BgL_varz00) =
							((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_6613)), BUNSPEC);
						BgL_auxz00_9236 = BgL_new1473z00_6935;
					}
					return ((obj_t) BgL_auxz00_9236);
				}
			}
		}

	}



/* &node->cop-make-box1773 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2makezd2box1773z53zzcgen_cgenz00(obj_t
		BgL_envz00_6614, obj_t BgL_nodez00_6615, obj_t BgL_kontz00_6616,
		obj_t BgL_inpushexitz00_6617)
	{
		{	/* Cgen/cgen.scm 1080 */
			{	/* Cgen/cgen.scm 1097 */
				bool_t BgL_test3182z00_9252;

				{	/* Cgen/cgen.scm 1097 */
					BgL_nodez00_bglt BgL_arg2657z00_6939;

					BgL_arg2657z00_6939 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_6615)))->BgL_valuez00);
					BgL_test3182z00_9252 =
						BGl_simplezd2exprzf3ze70zc6zzcgen_cgenz00(
						((obj_t) BgL_arg2657z00_6939));
				}
				if (BgL_test3182z00_9252)
					{	/* Cgen/cgen.scm 1098 */
						BgL_nodez00_bglt BgL_arg2635z00_6940;

						BgL_arg2635z00_6940 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_6615)))->BgL_valuez00);
						{	/* Cgen/cgen.scm 1101 */
							obj_t BgL_zc3z04anonymousza32637ze3z87_6941;

							BgL_zc3z04anonymousza32637ze3z87_6941 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza32637ze3ze5zzcgen_cgenz00, (int) (1L),
								(int) (2L));
							PROCEDURE_SET(BgL_zc3z04anonymousza32637ze3z87_6941, (int) (0L),
								((obj_t) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_6615)));
							PROCEDURE_SET(BgL_zc3z04anonymousza32637ze3z87_6941, (int) (1L),
								BgL_kontz00_6616);
							return BGl_nodezd2ze3copz31zzcgen_cgenz00(BgL_arg2635z00_6940,
								BgL_zc3z04anonymousza32637ze3z87_6941,
								CBOOL(BgL_inpushexitz00_6617));
						}
					}
				else
					{	/* Cgen/cgen.scm 1107 */
						BgL_localz00_bglt BgL_auxz00_6942;

						{	/* Cgen/cgen.scm 1107 */
							BgL_localz00_bglt BgL_res2882z00_6943;

							{	/* Cgen/cgen.scm 1107 */
								obj_t BgL_idz00_6944;
								BgL_typez00_bglt BgL_typez00_6945;

								BgL_idz00_6944 = CNST_TABLE_REF(10);
								BgL_typez00_6945 =
									((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
								{	/* Cgen/cgen.scm 1183 */
									BgL_localz00_bglt BgL_localz00_6946;

									BgL_localz00_6946 =
										BGl_makezd2localzd2svarz00zzast_localz00(BgL_idz00_6944,
										BgL_typez00_6945);
									{	/* Cgen/cgen.scm 1184 */
										bool_t BgL_test3183z00_9273;

										{	/* Cgen/cgen.scm 1184 */
											obj_t BgL_tmpz00_9274;

											BgL_tmpz00_9274 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_localz00_6946)))->
												BgL_namez00);
											BgL_test3183z00_9273 = STRINGP(BgL_tmpz00_9274);
										}
										if (BgL_test3183z00_9273)
											{	/* Cgen/cgen.scm 1184 */
												BFALSE;
											}
										else
											{	/* Cgen/cgen.scm 1184 */
												BGl_errorz00zz__errorz00
													(BGl_string2915z00zzcgen_cgenz00,
													BGl_string2916z00zzcgen_cgenz00,
													((obj_t) BgL_localz00_6946));
											}
									}
									BgL_res2882z00_6943 = BgL_localz00_6946;
								}
							}
							BgL_auxz00_6942 = BgL_res2882z00_6943;
						}
						{	/* Cgen/cgen.scm 1107 */
							BgL_copz00_bglt BgL_cvalz00_6947;

							{	/* Cgen/cgen.scm 1108 */
								BgL_setqz00_bglt BgL_arg2654z00_6948;

								{	/* Cgen/cgen.scm 1108 */
									BgL_nodez00_bglt BgL_arg2656z00_6949;

									BgL_arg2656z00_6949 =
										(((BgL_makezd2boxzd2_bglt) COBJECT(
												((BgL_makezd2boxzd2_bglt) BgL_nodez00_6615)))->
										BgL_valuez00);
									BgL_arg2654z00_6948 =
										BGl_nodezd2setqzd2zzcgen_cgenz00(((BgL_variablez00_bglt)
											BgL_auxz00_6942), BgL_arg2656z00_6949);
								}
								BgL_cvalz00_6947 =
									BGl_nodezd2ze3copz31zzcgen_cgenz00(
									((BgL_nodez00_bglt) BgL_arg2654z00_6948),
									BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
									CBOOL(BgL_inpushexitz00_6617));
							}
							{	/* Cgen/cgen.scm 1108 */

								{	/* Cgen/cgen.scm 1109 */
									BgL_cblockz00_bglt BgL_new1462z00_6950;

									{	/* Cgen/cgen.scm 1111 */
										BgL_cblockz00_bglt BgL_new1461z00_6951;

										BgL_new1461z00_6951 =
											((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_cblockz00_bgl))));
										{	/* Cgen/cgen.scm 1111 */
											long BgL_arg2653z00_6952;

											BgL_arg2653z00_6952 =
												BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1461z00_6951),
												BgL_arg2653z00_6952);
										}
										BgL_new1462z00_6950 = BgL_new1461z00_6951;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1462z00_6950)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt)
																BgL_nodez00_6615))))->BgL_locz00)), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1462z00_6950)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2cellza2z00zztype_cachez00)), BUNSPEC);
									{
										BgL_copz00_bglt BgL_auxz00_9299;

										{	/* Cgen/cgen.scm 1112 */
											BgL_csequencez00_bglt BgL_new1464z00_6953;

											{	/* Cgen/cgen.scm 1114 */
												BgL_csequencez00_bglt BgL_new1463z00_6954;

												BgL_new1463z00_6954 =
													((BgL_csequencez00_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_csequencez00_bgl))));
												{	/* Cgen/cgen.scm 1114 */
													long BgL_arg2651z00_6955;

													{	/* Cgen/cgen.scm 1114 */
														obj_t BgL_classz00_6956;

														BgL_classz00_6956 = BGl_csequencez00zzcgen_copz00;
														BgL_arg2651z00_6955 =
															BGL_CLASS_NUM(BgL_classz00_6956);
													}
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1463z00_6954),
														BgL_arg2651z00_6955);
												}
												BgL_new1464z00_6953 = BgL_new1463z00_6954;
											}
											((((BgL_copz00_bglt) COBJECT(
															((BgL_copz00_bglt) BgL_new1464z00_6953)))->
													BgL_locz00) =
												((obj_t) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) (
																		(BgL_makezd2boxzd2_bglt)
																		BgL_nodez00_6615))))->BgL_locz00)),
												BUNSPEC);
											((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																BgL_new1464z00_6953)))->BgL_typez00) =
												((BgL_typez00_bglt) ((BgL_typez00_bglt)
														BGl_za2cellza2z00zztype_cachez00)), BUNSPEC);
											((((BgL_csequencez00_bglt) COBJECT(BgL_new1464z00_6953))->
													BgL_czd2expzf3z21) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
											{
												obj_t BgL_auxz00_9313;

												{	/* Cgen/cgen.scm 1116 */
													BgL_localzd2varzd2_bglt BgL_arg2640z00_6957;
													obj_t BgL_arg2641z00_6958;

													{	/* Cgen/cgen.scm 1116 */
														BgL_localzd2varzd2_bglt BgL_new1466z00_6959;

														{	/* Cgen/cgen.scm 1118 */
															BgL_localzd2varzd2_bglt BgL_new1465z00_6960;

															BgL_new1465z00_6960 =
																((BgL_localzd2varzd2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_localzd2varzd2_bgl))));
															{	/* Cgen/cgen.scm 1118 */
																long BgL_arg2647z00_6961;

																{	/* Cgen/cgen.scm 1118 */
																	obj_t BgL_classz00_6962;

																	BgL_classz00_6962 =
																		BGl_localzd2varzd2zzcgen_copz00;
																	BgL_arg2647z00_6961 =
																		BGL_CLASS_NUM(BgL_classz00_6962);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1465z00_6960),
																	BgL_arg2647z00_6961);
															}
															BgL_new1466z00_6959 = BgL_new1465z00_6960;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1466z00_6959)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_makezd2boxzd2_bglt)
																					BgL_nodez00_6615))))->BgL_locz00)),
															BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1466z00_6959)))->BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
														{
															obj_t BgL_auxz00_9326;

															{	/* Cgen/cgen.scm 1119 */
																obj_t BgL_list2645z00_6963;

																BgL_list2645z00_6963 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_auxz00_6942), BNIL);
																BgL_auxz00_9326 = BgL_list2645z00_6963;
															}
															((((BgL_localzd2varzd2_bglt)
																		COBJECT(BgL_new1466z00_6959))->
																	BgL_varsz00) =
																((obj_t) BgL_auxz00_9326), BUNSPEC);
														}
														BgL_arg2640z00_6957 = BgL_new1466z00_6959;
													}
													{	/* Cgen/cgen.scm 1122 */
														BgL_cmakezd2boxzd2_bglt BgL_arg2648z00_6964;

														{	/* Cgen/cgen.scm 1122 */
															BgL_cmakezd2boxzd2_bglt BgL_new1468z00_6965;

															{	/* Cgen/cgen.scm 1124 */
																BgL_cmakezd2boxzd2_bglt BgL_new1467z00_6966;

																BgL_new1467z00_6966 =
																	((BgL_cmakezd2boxzd2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_cmakezd2boxzd2_bgl))));
																{	/* Cgen/cgen.scm 1124 */
																	long BgL_arg2650z00_6967;

																	{	/* Cgen/cgen.scm 1124 */
																		obj_t BgL_classz00_6968;

																		BgL_classz00_6968 =
																			BGl_cmakezd2boxzd2zzcgen_copz00;
																		BgL_arg2650z00_6967 =
																			BGL_CLASS_NUM(BgL_classz00_6968);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1467z00_6966),
																		BgL_arg2650z00_6967);
																}
																BgL_new1468z00_6965 = BgL_new1467z00_6966;
															}
															((((BgL_copz00_bglt) COBJECT(
																			((BgL_copz00_bglt)
																				BgL_new1468z00_6965)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_makezd2boxzd2_bglt)
																						BgL_nodez00_6615))))->BgL_locz00)),
																BUNSPEC);
															((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																				BgL_new1468z00_6965)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BGl_za2cellza2z00zztype_cachez00)),
																BUNSPEC);
															{
																BgL_copz00_bglt BgL_auxz00_9342;

																{	/* Cgen/cgen.scm 1125 */
																	BgL_varcz00_bglt BgL_new1470z00_6969;

																	{	/* Cgen/cgen.scm 1127 */
																		BgL_varcz00_bglt BgL_new1469z00_6970;

																		BgL_new1469z00_6970 =
																			((BgL_varcz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_varcz00_bgl))));
																		{	/* Cgen/cgen.scm 1127 */
																			long BgL_arg2649z00_6971;

																			{	/* Cgen/cgen.scm 1127 */
																				obj_t BgL_classz00_6972;

																				BgL_classz00_6972 =
																					BGl_varcz00zzcgen_copz00;
																				BgL_arg2649z00_6971 =
																					BGL_CLASS_NUM(BgL_classz00_6972);
																			}
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1469z00_6970),
																				BgL_arg2649z00_6971);
																		}
																		BgL_new1470z00_6969 = BgL_new1469z00_6970;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1470z00_6969)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_makezd2boxzd2_bglt)
																								BgL_nodez00_6615))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1470z00_6969)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt)
																							BgL_auxz00_6942)))->BgL_typez00)),
																		BUNSPEC);
																	((((BgL_varcz00_bglt)
																				COBJECT(BgL_new1470z00_6969))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_auxz00_6942)), BUNSPEC);
																	BgL_auxz00_9342 =
																		((BgL_copz00_bglt) BgL_new1470z00_6969);
																}
																((((BgL_cmakezd2boxzd2_bglt)
																			COBJECT(BgL_new1468z00_6965))->
																		BgL_valuez00) =
																	((BgL_copz00_bglt) BgL_auxz00_9342), BUNSPEC);
															}
															((((BgL_cmakezd2boxzd2_bglt)
																		COBJECT(BgL_new1468z00_6965))->
																	BgL_stackablez00) =
																((obj_t) (((BgL_makezd2boxzd2_bglt)
																			COBJECT(((BgL_makezd2boxzd2_bglt)
																					BgL_nodez00_6615)))->
																		BgL_stackablez00)), BUNSPEC);
															BgL_arg2648z00_6964 = BgL_new1468z00_6965;
														}
														BgL_arg2641z00_6958 =
															BGL_PROCEDURE_CALL1(BgL_kontz00_6616,
															((obj_t) BgL_arg2648z00_6964));
													}
													{	/* Cgen/cgen.scm 1115 */
														obj_t BgL_list2642z00_6973;

														{	/* Cgen/cgen.scm 1115 */
															obj_t BgL_arg2643z00_6974;

															{	/* Cgen/cgen.scm 1115 */
																obj_t BgL_arg2644z00_6975;

																BgL_arg2644z00_6975 =
																	MAKE_YOUNG_PAIR(BgL_arg2641z00_6958, BNIL);
																BgL_arg2643z00_6974 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_cvalz00_6947),
																	BgL_arg2644z00_6975);
															}
															BgL_list2642z00_6973 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg2640z00_6957),
																BgL_arg2643z00_6974);
														}
														BgL_auxz00_9313 = BgL_list2642z00_6973;
												}}
												((((BgL_csequencez00_bglt)
															COBJECT(BgL_new1464z00_6953))->BgL_copsz00) =
													((obj_t) BgL_auxz00_9313), BUNSPEC);
											}
											BgL_auxz00_9299 = ((BgL_copz00_bglt) BgL_new1464z00_6953);
										}
										((((BgL_cblockz00_bglt) COBJECT(BgL_new1462z00_6950))->
												BgL_bodyz00) =
											((BgL_copz00_bglt) BgL_auxz00_9299), BUNSPEC);
									}
									return ((BgL_copz00_bglt) BgL_new1462z00_6950);
								}
							}
						}
					}
			}
		}

	}



/* simple-expr?~0 */
	bool_t BGl_simplezd2exprzf3ze70zc6zzcgen_cgenz00(obj_t BgL_valuez00_3651)
	{
		{	/* Cgen/cgen.scm 1094 */
			{
				obj_t BgL_valuez00_3622;

				{	/* Cgen/cgen.scm 1094 */
					bool_t BgL__ortest_1454z00_3653;

					{	/* Cgen/cgen.scm 1094 */
						obj_t BgL_classz00_5873;

						BgL_classz00_5873 = BGl_varz00zzast_nodez00;
						if (BGL_OBJECTP(BgL_valuez00_3651))
							{	/* Cgen/cgen.scm 1094 */
								BgL_objectz00_bglt BgL_arg1807z00_5875;

								BgL_arg1807z00_5875 = (BgL_objectz00_bglt) (BgL_valuez00_3651);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cgen/cgen.scm 1094 */
										long BgL_idxz00_5881;

										BgL_idxz00_5881 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5875);
										BgL__ortest_1454z00_3653 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5881 + 2L)) == BgL_classz00_5873);
									}
								else
									{	/* Cgen/cgen.scm 1094 */
										bool_t BgL_res2879z00_5906;

										{	/* Cgen/cgen.scm 1094 */
											obj_t BgL_oclassz00_5889;

											{	/* Cgen/cgen.scm 1094 */
												obj_t BgL_arg1815z00_5897;
												long BgL_arg1816z00_5898;

												BgL_arg1815z00_5897 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cgen/cgen.scm 1094 */
													long BgL_arg1817z00_5899;

													BgL_arg1817z00_5899 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5875);
													BgL_arg1816z00_5898 =
														(BgL_arg1817z00_5899 - OBJECT_TYPE);
												}
												BgL_oclassz00_5889 =
													VECTOR_REF(BgL_arg1815z00_5897, BgL_arg1816z00_5898);
											}
											{	/* Cgen/cgen.scm 1094 */
												bool_t BgL__ortest_1115z00_5890;

												BgL__ortest_1115z00_5890 =
													(BgL_classz00_5873 == BgL_oclassz00_5889);
												if (BgL__ortest_1115z00_5890)
													{	/* Cgen/cgen.scm 1094 */
														BgL_res2879z00_5906 = BgL__ortest_1115z00_5890;
													}
												else
													{	/* Cgen/cgen.scm 1094 */
														long BgL_odepthz00_5891;

														{	/* Cgen/cgen.scm 1094 */
															obj_t BgL_arg1804z00_5892;

															BgL_arg1804z00_5892 = (BgL_oclassz00_5889);
															BgL_odepthz00_5891 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5892);
														}
														if ((2L < BgL_odepthz00_5891))
															{	/* Cgen/cgen.scm 1094 */
																obj_t BgL_arg1802z00_5894;

																{	/* Cgen/cgen.scm 1094 */
																	obj_t BgL_arg1803z00_5895;

																	BgL_arg1803z00_5895 = (BgL_oclassz00_5889);
																	BgL_arg1802z00_5894 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5895,
																		2L);
																}
																BgL_res2879z00_5906 =
																	(BgL_arg1802z00_5894 == BgL_classz00_5873);
															}
														else
															{	/* Cgen/cgen.scm 1094 */
																BgL_res2879z00_5906 = ((bool_t) 0);
															}
													}
											}
										}
										BgL__ortest_1454z00_3653 = BgL_res2879z00_5906;
									}
							}
						else
							{	/* Cgen/cgen.scm 1094 */
								BgL__ortest_1454z00_3653 = ((bool_t) 0);
							}
					}
					if (BgL__ortest_1454z00_3653)
						{	/* Cgen/cgen.scm 1094 */
							return BgL__ortest_1454z00_3653;
						}
					else
						{	/* Cgen/cgen.scm 1094 */
							bool_t BgL__ortest_1455z00_3654;

							{	/* Cgen/cgen.scm 1094 */
								obj_t BgL_classz00_5907;

								BgL_classz00_5907 = BGl_atomz00zzast_nodez00;
								if (BGL_OBJECTP(BgL_valuez00_3651))
									{	/* Cgen/cgen.scm 1094 */
										BgL_objectz00_bglt BgL_arg1807z00_5909;

										BgL_arg1807z00_5909 =
											(BgL_objectz00_bglt) (BgL_valuez00_3651);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cgen/cgen.scm 1094 */
												long BgL_idxz00_5915;

												BgL_idxz00_5915 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5909);
												BgL__ortest_1455z00_3654 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5915 + 2L)) == BgL_classz00_5907);
											}
										else
											{	/* Cgen/cgen.scm 1094 */
												bool_t BgL_res2880z00_5940;

												{	/* Cgen/cgen.scm 1094 */
													obj_t BgL_oclassz00_5923;

													{	/* Cgen/cgen.scm 1094 */
														obj_t BgL_arg1815z00_5931;
														long BgL_arg1816z00_5932;

														BgL_arg1815z00_5931 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cgen/cgen.scm 1094 */
															long BgL_arg1817z00_5933;

															BgL_arg1817z00_5933 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5909);
															BgL_arg1816z00_5932 =
																(BgL_arg1817z00_5933 - OBJECT_TYPE);
														}
														BgL_oclassz00_5923 =
															VECTOR_REF(BgL_arg1815z00_5931,
															BgL_arg1816z00_5932);
													}
													{	/* Cgen/cgen.scm 1094 */
														bool_t BgL__ortest_1115z00_5924;

														BgL__ortest_1115z00_5924 =
															(BgL_classz00_5907 == BgL_oclassz00_5923);
														if (BgL__ortest_1115z00_5924)
															{	/* Cgen/cgen.scm 1094 */
																BgL_res2880z00_5940 = BgL__ortest_1115z00_5924;
															}
														else
															{	/* Cgen/cgen.scm 1094 */
																long BgL_odepthz00_5925;

																{	/* Cgen/cgen.scm 1094 */
																	obj_t BgL_arg1804z00_5926;

																	BgL_arg1804z00_5926 = (BgL_oclassz00_5923);
																	BgL_odepthz00_5925 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5926);
																}
																if ((2L < BgL_odepthz00_5925))
																	{	/* Cgen/cgen.scm 1094 */
																		obj_t BgL_arg1802z00_5928;

																		{	/* Cgen/cgen.scm 1094 */
																			obj_t BgL_arg1803z00_5929;

																			BgL_arg1803z00_5929 =
																				(BgL_oclassz00_5923);
																			BgL_arg1802z00_5928 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5929, 2L);
																		}
																		BgL_res2880z00_5940 =
																			(BgL_arg1802z00_5928 ==
																			BgL_classz00_5907);
																	}
																else
																	{	/* Cgen/cgen.scm 1094 */
																		BgL_res2880z00_5940 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL__ortest_1455z00_3654 = BgL_res2880z00_5940;
											}
									}
								else
									{	/* Cgen/cgen.scm 1094 */
										BgL__ortest_1455z00_3654 = ((bool_t) 0);
									}
							}
							if (BgL__ortest_1455z00_3654)
								{	/* Cgen/cgen.scm 1094 */
									return BgL__ortest_1455z00_3654;
								}
							else
								{	/* Cgen/cgen.scm 1094 */
									bool_t BgL__ortest_1456z00_3655;

									{	/* Cgen/cgen.scm 1094 */
										obj_t BgL_classz00_5941;

										BgL_classz00_5941 = BGl_kwotez00zzast_nodez00;
										if (BGL_OBJECTP(BgL_valuez00_3651))
											{	/* Cgen/cgen.scm 1094 */
												BgL_objectz00_bglt BgL_arg1807z00_5943;

												BgL_arg1807z00_5943 =
													(BgL_objectz00_bglt) (BgL_valuez00_3651);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cgen/cgen.scm 1094 */
														long BgL_idxz00_5949;

														BgL_idxz00_5949 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5943);
														BgL__ortest_1456z00_3655 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5949 + 2L)) == BgL_classz00_5941);
													}
												else
													{	/* Cgen/cgen.scm 1094 */
														bool_t BgL_res2881z00_5974;

														{	/* Cgen/cgen.scm 1094 */
															obj_t BgL_oclassz00_5957;

															{	/* Cgen/cgen.scm 1094 */
																obj_t BgL_arg1815z00_5965;
																long BgL_arg1816z00_5966;

																BgL_arg1815z00_5965 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cgen/cgen.scm 1094 */
																	long BgL_arg1817z00_5967;

																	BgL_arg1817z00_5967 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5943);
																	BgL_arg1816z00_5966 =
																		(BgL_arg1817z00_5967 - OBJECT_TYPE);
																}
																BgL_oclassz00_5957 =
																	VECTOR_REF(BgL_arg1815z00_5965,
																	BgL_arg1816z00_5966);
															}
															{	/* Cgen/cgen.scm 1094 */
																bool_t BgL__ortest_1115z00_5958;

																BgL__ortest_1115z00_5958 =
																	(BgL_classz00_5941 == BgL_oclassz00_5957);
																if (BgL__ortest_1115z00_5958)
																	{	/* Cgen/cgen.scm 1094 */
																		BgL_res2881z00_5974 =
																			BgL__ortest_1115z00_5958;
																	}
																else
																	{	/* Cgen/cgen.scm 1094 */
																		long BgL_odepthz00_5959;

																		{	/* Cgen/cgen.scm 1094 */
																			obj_t BgL_arg1804z00_5960;

																			BgL_arg1804z00_5960 =
																				(BgL_oclassz00_5957);
																			BgL_odepthz00_5959 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5960);
																		}
																		if ((2L < BgL_odepthz00_5959))
																			{	/* Cgen/cgen.scm 1094 */
																				obj_t BgL_arg1802z00_5962;

																				{	/* Cgen/cgen.scm 1094 */
																					obj_t BgL_arg1803z00_5963;

																					BgL_arg1803z00_5963 =
																						(BgL_oclassz00_5957);
																					BgL_arg1802z00_5962 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5963, 2L);
																				}
																				BgL_res2881z00_5974 =
																					(BgL_arg1802z00_5962 ==
																					BgL_classz00_5941);
																			}
																		else
																			{	/* Cgen/cgen.scm 1094 */
																				BgL_res2881z00_5974 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL__ortest_1456z00_3655 = BgL_res2881z00_5974;
													}
											}
										else
											{	/* Cgen/cgen.scm 1094 */
												BgL__ortest_1456z00_3655 = ((bool_t) 0);
											}
									}
									if (BgL__ortest_1456z00_3655)
										{	/* Cgen/cgen.scm 1094 */
											return BgL__ortest_1456z00_3655;
										}
									else
										{	/* Cgen/cgen.scm 1094 */
											BgL_valuez00_3622 = BgL_valuez00_3651;
											{	/* Cgen/cgen.scm 1086 */
												bool_t BgL_test3199z00_9446;

												{	/* Cgen/cgen.scm 1086 */
													obj_t BgL_classz00_5836;

													BgL_classz00_5836 = BGl_appz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_valuez00_3622))
														{	/* Cgen/cgen.scm 1086 */
															BgL_objectz00_bglt BgL_arg1807z00_5838;

															BgL_arg1807z00_5838 =
																(BgL_objectz00_bglt) (BgL_valuez00_3622);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Cgen/cgen.scm 1086 */
																	long BgL_idxz00_5844;

																	BgL_idxz00_5844 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_5838);
																	BgL_test3199z00_9446 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_5844 + 3L)) ==
																		BgL_classz00_5836);
																}
															else
																{	/* Cgen/cgen.scm 1086 */
																	bool_t BgL_res2878z00_5869;

																	{	/* Cgen/cgen.scm 1086 */
																		obj_t BgL_oclassz00_5852;

																		{	/* Cgen/cgen.scm 1086 */
																			obj_t BgL_arg1815z00_5860;
																			long BgL_arg1816z00_5861;

																			BgL_arg1815z00_5860 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Cgen/cgen.scm 1086 */
																				long BgL_arg1817z00_5862;

																				BgL_arg1817z00_5862 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_5838);
																				BgL_arg1816z00_5861 =
																					(BgL_arg1817z00_5862 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_5852 =
																				VECTOR_REF(BgL_arg1815z00_5860,
																				BgL_arg1816z00_5861);
																		}
																		{	/* Cgen/cgen.scm 1086 */
																			bool_t BgL__ortest_1115z00_5853;

																			BgL__ortest_1115z00_5853 =
																				(BgL_classz00_5836 ==
																				BgL_oclassz00_5852);
																			if (BgL__ortest_1115z00_5853)
																				{	/* Cgen/cgen.scm 1086 */
																					BgL_res2878z00_5869 =
																						BgL__ortest_1115z00_5853;
																				}
																			else
																				{	/* Cgen/cgen.scm 1086 */
																					long BgL_odepthz00_5854;

																					{	/* Cgen/cgen.scm 1086 */
																						obj_t BgL_arg1804z00_5855;

																						BgL_arg1804z00_5855 =
																							(BgL_oclassz00_5852);
																						BgL_odepthz00_5854 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_5855);
																					}
																					if ((3L < BgL_odepthz00_5854))
																						{	/* Cgen/cgen.scm 1086 */
																							obj_t BgL_arg1802z00_5857;

																							{	/* Cgen/cgen.scm 1086 */
																								obj_t BgL_arg1803z00_5858;

																								BgL_arg1803z00_5858 =
																									(BgL_oclassz00_5852);
																								BgL_arg1802z00_5857 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_5858, 3L);
																							}
																							BgL_res2878z00_5869 =
																								(BgL_arg1802z00_5857 ==
																								BgL_classz00_5836);
																						}
																					else
																						{	/* Cgen/cgen.scm 1086 */
																							BgL_res2878z00_5869 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test3199z00_9446 = BgL_res2878z00_5869;
																}
														}
													else
														{	/* Cgen/cgen.scm 1086 */
															BgL_test3199z00_9446 = ((bool_t) 0);
														}
												}
												if (BgL_test3199z00_9446)
													{	/* Cgen/cgen.scm 1088 */
														bool_t BgL_test3204z00_9469;

														{	/* Cgen/cgen.scm 1088 */
															obj_t BgL_g1723z00_3641;

															BgL_g1723z00_3641 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt) BgL_valuez00_3622)))->
																BgL_argsz00);
															{
																obj_t BgL_l1721z00_3643;

																BgL_l1721z00_3643 = BgL_g1723z00_3641;
															BgL_zc3z04anonymousza32668ze3z87_3644:
																if (NULLP(BgL_l1721z00_3643))
																	{	/* Cgen/cgen.scm 1088 */
																		BgL_test3204z00_9469 = ((bool_t) 1);
																	}
																else
																	{	/* Cgen/cgen.scm 1088 */
																		bool_t BgL_test3206z00_9474;

																		{	/* Cgen/cgen.scm 1088 */
																			obj_t BgL_arg2670z00_3649;

																			BgL_arg2670z00_3649 =
																				CAR(((obj_t) BgL_l1721z00_3643));
																			BgL_test3206z00_9474 =
																				BGl_simplezd2exprzf3ze70zc6zzcgen_cgenz00
																				(BgL_arg2670z00_3649);
																		}
																		if (BgL_test3206z00_9474)
																			{	/* Cgen/cgen.scm 1088 */
																				obj_t BgL_arg2669z00_3648;

																				BgL_arg2669z00_3648 =
																					CDR(((obj_t) BgL_l1721z00_3643));
																				{
																					obj_t BgL_l1721z00_9480;

																					BgL_l1721z00_9480 =
																						BgL_arg2669z00_3648;
																					BgL_l1721z00_3643 = BgL_l1721z00_9480;
																					goto
																						BgL_zc3z04anonymousza32668ze3z87_3644;
																				}
																			}
																		else
																			{	/* Cgen/cgen.scm 1088 */
																				BgL_test3204z00_9469 = ((bool_t) 0);
																			}
																	}
															}
														}
														if (BgL_test3204z00_9469)
															{	/* Cgen/cgen.scm 1088 */
																if (CBOOL(
																		(((BgL_funz00_bglt) COBJECT(
																					((BgL_funz00_bglt)
																						(((BgL_variablez00_bglt) COBJECT(
																									(((BgL_varz00_bglt) COBJECT(
																												(((BgL_appz00_bglt)
																														COBJECT((
																																(BgL_appz00_bglt)
																																BgL_valuez00_3622)))->
																													BgL_funz00)))->
																										BgL_variablez00)))->
																							BgL_valuez00))))->
																			BgL_sidezd2effectzd2)))
																	{	/* Cgen/cgen.scm 1091 */
																		return ((bool_t) 0);
																	}
																else
																	{	/* Cgen/cgen.scm 1091 */
																		return ((bool_t) 1);
																	}
															}
														else
															{	/* Cgen/cgen.scm 1088 */
																return ((bool_t) 0);
															}
													}
												else
													{	/* Cgen/cgen.scm 1086 */
														return ((bool_t) 0);
													}
											}
										}
								}
						}
				}
			}
		}

	}



/* &<@anonymous:2637> */
	obj_t BGl_z62zc3z04anonymousza32637ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6618,
		obj_t BgL_vz00_6621)
	{
		{	/* Cgen/cgen.scm 1099 */
			{	/* Cgen/cgen.scm 1101 */
				BgL_makezd2boxzd2_bglt BgL_i1457z00_6619;
				obj_t BgL_kontz00_6620;

				BgL_i1457z00_6619 =
					((BgL_makezd2boxzd2_bglt) PROCEDURE_REF(BgL_envz00_6618, (int) (0L)));
				BgL_kontz00_6620 = PROCEDURE_REF(BgL_envz00_6618, (int) (1L));
				{	/* Cgen/cgen.scm 1101 */
					BgL_cmakezd2boxzd2_bglt BgL_arg2638z00_6976;

					{	/* Cgen/cgen.scm 1101 */
						BgL_cmakezd2boxzd2_bglt BgL_new1460z00_6977;

						{	/* Cgen/cgen.scm 1103 */
							BgL_cmakezd2boxzd2_bglt BgL_new1458z00_6978;

							BgL_new1458z00_6978 =
								((BgL_cmakezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cmakezd2boxzd2_bgl))));
							{	/* Cgen/cgen.scm 1103 */
								long BgL_arg2639z00_6979;

								BgL_arg2639z00_6979 =
									BGL_CLASS_NUM(BGl_cmakezd2boxzd2zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1458z00_6978),
									BgL_arg2639z00_6979);
							}
							BgL_new1460z00_6977 = BgL_new1458z00_6978;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1460z00_6977)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_i1457z00_6619)))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1460z00_6977)))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_za2cellza2z00zztype_cachez00)), BUNSPEC);
						((((BgL_cmakezd2boxzd2_bglt) COBJECT(BgL_new1460z00_6977))->
								BgL_valuez00) =
							((BgL_copz00_bglt) ((BgL_copz00_bglt) BgL_vz00_6621)), BUNSPEC);
						((((BgL_cmakezd2boxzd2_bglt) COBJECT(BgL_new1460z00_6977))->
								BgL_stackablez00) =
							((obj_t) (((BgL_makezd2boxzd2_bglt) COBJECT(BgL_i1457z00_6619))->
									BgL_stackablez00)), BUNSPEC);
						BgL_arg2638z00_6976 = BgL_new1460z00_6977;
					}
					return
						BGL_PROCEDURE_CALL1(BgL_kontz00_6620,
						((obj_t) BgL_arg2638z00_6976));
				}
			}
		}

	}



/* &node->cop-return1771 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2return1771z81zzcgen_cgenz00(obj_t
		BgL_envz00_6622, obj_t BgL_nodez00_6623, obj_t BgL_kontz00_6624,
		obj_t BgL_inpushexitz00_6625)
	{
		{	/* Cgen/cgen.scm 1060 */
			{	/* Cgen/cgen.scm 1062 */
				obj_t BgL_kontz00_6981;

				{	/* Cgen/cgen.scm 1062 */
					bool_t BgL_test3208z00_9514;

					{	/* Cgen/cgen.scm 1062 */
						BgL_retblockz00_bglt BgL_arg2631z00_6982;

						BgL_arg2631z00_6982 =
							(((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_nodez00_6623)))->BgL_blockz00);
						{	/* Cgen/cgen.scm 1062 */
							obj_t BgL_classz00_6983;

							BgL_classz00_6983 = BGl_retblockzf2gotozf2zzcgen_cgenz00;
							{	/* Cgen/cgen.scm 1062 */
								obj_t BgL_oclassz00_6984;

								{	/* Cgen/cgen.scm 1062 */
									obj_t BgL_arg1815z00_6985;
									long BgL_arg1816z00_6986;

									BgL_arg1815z00_6985 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Cgen/cgen.scm 1062 */
										long BgL_arg1817z00_6987;

										BgL_arg1817z00_6987 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt) BgL_arg2631z00_6982));
										BgL_arg1816z00_6986 = (BgL_arg1817z00_6987 - OBJECT_TYPE);
									}
									BgL_oclassz00_6984 =
										VECTOR_REF(BgL_arg1815z00_6985, BgL_arg1816z00_6986);
								}
								BgL_test3208z00_9514 =
									(BgL_oclassz00_6984 == BgL_classz00_6983);
					}}}
					if (BgL_test3208z00_9514)
						{	/* Cgen/cgen.scm 1063 */
							BgL_retblockz00_bglt BgL_i1446z00_6988;

							BgL_i1446z00_6988 =
								((BgL_retblockz00_bglt)
								(((BgL_returnz00_bglt) COBJECT(
											((BgL_returnz00_bglt) BgL_nodez00_6623)))->BgL_blockz00));
							{	/* Cgen/cgen.scm 1064 */
								obj_t BgL_arg2622z00_6989;
								obj_t BgL_arg2623z00_6990;

								{
									BgL_retblockzf2gotozf2_bglt BgL_auxz00_9526;

									{
										obj_t BgL_auxz00_9527;

										{	/* Cgen/cgen.scm 1064 */
											BgL_objectz00_bglt BgL_tmpz00_9528;

											BgL_tmpz00_9528 =
												((BgL_objectz00_bglt) BgL_i1446z00_6988);
											BgL_auxz00_9527 = BGL_OBJECT_WIDENING(BgL_tmpz00_9528);
										}
										BgL_auxz00_9526 =
											((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9527);
									}
									BgL_arg2622z00_6989 =
										(((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9526))->
										BgL_localz00);
								}
								BgL_arg2623z00_6990 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_returnz00_bglt) BgL_nodez00_6623))))->BgL_locz00);
								{	/* Cgen/cgen.scm 1066 */
									obj_t BgL_zc3z04anonymousza32625ze3z87_6991;

									BgL_zc3z04anonymousza32625ze3z87_6991 =
										MAKE_FX_PROCEDURE
										(BGl_z62zc3z04anonymousza32625ze3ze5zzcgen_cgenz00,
										(int) (1L), (int) (2L));
									PROCEDURE_SET(BgL_zc3z04anonymousza32625ze3z87_6991,
										(int) (0L),
										((obj_t) ((BgL_returnz00_bglt) BgL_nodez00_6623)));
									PROCEDURE_SET(BgL_zc3z04anonymousza32625ze3z87_6991,
										(int) (1L), ((obj_t) BgL_i1446z00_6988));
									BgL_kontz00_6981 =
										BGl_makezd2setqzd2kontz00zzcgen_cgenz00(BgL_arg2622z00_6989,
										BgL_arg2623z00_6990, BgL_zc3z04anonymousza32625ze3z87_6991);
						}}}
					else
						{	/* Cgen/cgen.scm 1062 */
							BgL_kontz00_6981 = BgL_kontz00_6624;
						}
				}
				return
					BGl_nodezd2ze3copz31zzcgen_cgenz00(
					(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_nodez00_6623)))->BgL_valuez00),
					BgL_kontz00_6981, CBOOL(BgL_inpushexitz00_6625));
			}
		}

	}



/* &<@anonymous:2625> */
	obj_t BGl_z62zc3z04anonymousza32625ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6626,
		obj_t BgL_cz00_6629)
	{
		{	/* Cgen/cgen.scm 1065 */
			{	/* Cgen/cgen.scm 1066 */
				BgL_returnz00_bglt BgL_i1445z00_6627;
				BgL_retblockz00_bglt BgL_i1446z00_6628;

				BgL_i1445z00_6627 =
					((BgL_returnz00_bglt) PROCEDURE_REF(BgL_envz00_6626, (int) (0L)));
				BgL_i1446z00_6628 =
					((BgL_retblockz00_bglt) PROCEDURE_REF(BgL_envz00_6626, (int) (1L)));
				{
					BgL_csequencez00_bglt BgL_auxz00_9557;

					{	/* Cgen/cgen.scm 1066 */
						BgL_csequencez00_bglt BgL_new1448z00_6992;

						{	/* Cgen/cgen.scm 1068 */
							BgL_csequencez00_bglt BgL_new1447z00_6993;

							BgL_new1447z00_6993 =
								((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_csequencez00_bgl))));
							{	/* Cgen/cgen.scm 1068 */
								long BgL_arg2630z00_6994;

								BgL_arg2630z00_6994 =
									BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1447z00_6993),
									BgL_arg2630z00_6994);
							}
							BgL_new1448z00_6992 = BgL_new1447z00_6993;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1448z00_6992)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_i1445z00_6627)))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1448z00_6992)))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_za2voidza2z00zztype_cachez00)), BUNSPEC);
						((((BgL_csequencez00_bglt) COBJECT(BgL_new1448z00_6992))->
								BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
						{
							obj_t BgL_auxz00_9570;

							{	/* Cgen/cgen.scm 1070 */
								BgL_cgotoz00_bglt BgL_arg2626z00_6995;

								{	/* Cgen/cgen.scm 1070 */
									BgL_cgotoz00_bglt BgL_new1450z00_6996;

									{	/* Cgen/cgen.scm 1072 */
										BgL_cgotoz00_bglt BgL_new1449z00_6997;

										BgL_new1449z00_6997 =
											((BgL_cgotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_cgotoz00_bgl))));
										{	/* Cgen/cgen.scm 1072 */
											long BgL_arg2629z00_6998;

											{	/* Cgen/cgen.scm 1072 */
												obj_t BgL_classz00_6999;

												BgL_classz00_6999 = BGl_cgotoz00zzcgen_copz00;
												BgL_arg2629z00_6998 = BGL_CLASS_NUM(BgL_classz00_6999);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1449z00_6997),
												BgL_arg2629z00_6998);
										}
										BgL_new1450z00_6996 = BgL_new1449z00_6997;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1450z00_6996)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_i1445z00_6627)))->BgL_locz00)), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1450z00_6996)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2voidza2z00zztype_cachez00)), BUNSPEC);
									{
										BgL_clabelz00_bglt BgL_auxz00_9582;

										{
											obj_t BgL_auxz00_9583;

											{
												BgL_retblockzf2gotozf2_bglt BgL_auxz00_9584;

												{
													obj_t BgL_auxz00_9585;

													{	/* Cgen/cgen.scm 1073 */
														BgL_objectz00_bglt BgL_tmpz00_9586;

														BgL_tmpz00_9586 =
															((BgL_objectz00_bglt) BgL_i1446z00_6628);
														BgL_auxz00_9585 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_9586);
													}
													BgL_auxz00_9584 =
														((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9585);
												}
												BgL_auxz00_9583 =
													(((BgL_retblockzf2gotozf2_bglt)
														COBJECT(BgL_auxz00_9584))->BgL_labelz00);
											}
											BgL_auxz00_9582 = ((BgL_clabelz00_bglt) BgL_auxz00_9583);
										}
										((((BgL_cgotoz00_bglt) COBJECT(BgL_new1450z00_6996))->
												BgL_labelz00) =
											((BgL_clabelz00_bglt) BgL_auxz00_9582), BUNSPEC);
									}
									BgL_arg2626z00_6995 = BgL_new1450z00_6996;
								}
								{	/* Cgen/cgen.scm 1069 */
									obj_t BgL_list2627z00_7000;

									{	/* Cgen/cgen.scm 1069 */
										obj_t BgL_arg2628z00_7001;

										BgL_arg2628z00_7001 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg2626z00_6995), BNIL);
										BgL_list2627z00_7000 =
											MAKE_YOUNG_PAIR(BgL_cz00_6629, BgL_arg2628z00_7001);
									}
									BgL_auxz00_9570 = BgL_list2627z00_7000;
							}}
							((((BgL_csequencez00_bglt) COBJECT(BgL_new1448z00_6992))->
									BgL_copsz00) = ((obj_t) BgL_auxz00_9570), BUNSPEC);
						}
						BgL_auxz00_9557 = BgL_new1448z00_6992;
					}
					return ((obj_t) BgL_auxz00_9557);
				}
			}
		}

	}



/* &node->cop-retblock1769 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2retblock1769z81zzcgen_cgenz00(obj_t
		BgL_envz00_6630, obj_t BgL_nodez00_6631, obj_t BgL_kontz00_6632,
		obj_t BgL_inpushexitz00_6633)
	{
		{	/* Cgen/cgen.scm 1027 */
			{	/* Cgen/cgen.scm 1029 */
				BgL_localz00_bglt BgL_localz00_7003;

				{	/* Cgen/cgen.scm 1029 */
					obj_t BgL_arg2617z00_7004;
					BgL_typez00_bglt BgL_arg2618z00_7005;

					BgL_arg2617z00_7004 =
						BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(11));
					BgL_arg2618z00_7005 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_retblockz00_bglt) BgL_nodez00_6631))))->BgL_typez00);
					BgL_localz00_7003 =
						BGl_makezd2localzd2svarz00zzast_localz00(BgL_arg2617z00_7004,
						BgL_arg2618z00_7005);
				}
				{	/* Cgen/cgen.scm 1029 */
					BgL_clabelz00_bglt BgL_labelz00_7006;

					{	/* Cgen/cgen.scm 1030 */
						BgL_clabelz00_bglt BgL_new1432z00_7007;

						{	/* Cgen/cgen.scm 1032 */
							BgL_clabelz00_bglt BgL_new1431z00_7008;

							BgL_new1431z00_7008 =
								((BgL_clabelz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_clabelz00_bgl))));
							{	/* Cgen/cgen.scm 1032 */
								long BgL_arg2615z00_7009;

								BgL_arg2615z00_7009 = BGL_CLASS_NUM(BGl_clabelz00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1431z00_7008),
									BgL_arg2615z00_7009);
							}
							BgL_new1432z00_7007 = BgL_new1431z00_7008;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1432z00_7007)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_retblockz00_bglt)
													BgL_nodez00_6631))))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1432z00_7007)))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_za2voidza2z00zztype_cachez00)), BUNSPEC);
						{
							obj_t BgL_auxz00_9616;

							{	/* Cgen/cgen.scm 1034 */
								obj_t BgL_arg2612z00_7010;

								BgL_arg2612z00_7010 =
									BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(12));
								{	/* Cgen/cgen.scm 1034 */
									obj_t BgL_arg1455z00_7011;

									BgL_arg1455z00_7011 = SYMBOL_TO_STRING(BgL_arg2612z00_7010);
									BgL_auxz00_9616 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_7011);
							}}
							((((BgL_clabelz00_bglt) COBJECT(BgL_new1432z00_7007))->
									BgL_namez00) = ((obj_t) BgL_auxz00_9616), BUNSPEC);
						}
						((((BgL_clabelz00_bglt) COBJECT(BgL_new1432z00_7007))->
								BgL_usedzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
						{
							obj_t BgL_auxz00_9623;

							{	/* Cgen/cgen.scm 1035 */
								BgL_varcz00_bglt BgL_arg2613z00_7012;

								{	/* Cgen/cgen.scm 1035 */
									BgL_varcz00_bglt BgL_new1434z00_7013;

									{	/* Cgen/cgen.scm 1037 */
										BgL_varcz00_bglt BgL_new1433z00_7014;

										BgL_new1433z00_7014 =
											((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_varcz00_bgl))));
										{	/* Cgen/cgen.scm 1037 */
											long BgL_arg2614z00_7015;

											{	/* Cgen/cgen.scm 1037 */
												obj_t BgL_classz00_7016;

												BgL_classz00_7016 = BGl_varcz00zzcgen_copz00;
												BgL_arg2614z00_7015 = BGL_CLASS_NUM(BgL_classz00_7016);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1433z00_7014),
												BgL_arg2614z00_7015);
										}
										BgL_new1434z00_7013 = BgL_new1433z00_7014;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1434z00_7013)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_retblockz00_bglt)
																BgL_nodez00_6631))))->BgL_locz00)), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1434z00_7013)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) BgL_localz00_7003)))->
												BgL_typez00)), BUNSPEC);
									((((BgL_varcz00_bglt) COBJECT(BgL_new1434z00_7013))->
											BgL_variablez00) =
										((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
												BgL_localz00_7003)), BUNSPEC);
									BgL_arg2613z00_7012 = BgL_new1434z00_7013;
								}
								BgL_auxz00_9623 =
									BGL_PROCEDURE_CALL1(BgL_kontz00_6632,
									((obj_t) BgL_arg2613z00_7012));
							}
							((((BgL_clabelz00_bglt) COBJECT(BgL_new1432z00_7007))->
									BgL_bodyz00) = ((obj_t) BgL_auxz00_9623), BUNSPEC);
						}
						BgL_labelz00_7006 = BgL_new1432z00_7007;
					}
					{	/* Cgen/cgen.scm 1030 */
						obj_t BgL_retkontz00_7017;

						{	/* Cgen/cgen.scm 1039 */
							obj_t BgL_arg2609z00_7018;

							BgL_arg2609z00_7018 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_retblockz00_bglt) BgL_nodez00_6631))))->BgL_locz00);
							BgL_retkontz00_7017 =
								BGl_makezd2setqzd2kontz00zzcgen_cgenz00(
								((obj_t) BgL_localz00_7003), BgL_arg2609z00_7018,
								BGl_proc2953z00zzcgen_cgenz00);
						}
						{	/* Cgen/cgen.scm 1039 */

							{	/* Cgen/cgen.scm 1040 */
								BgL_retblockzf2gotozf2_bglt BgL_wide1437z00_7020;

								BgL_wide1437z00_7020 =
									((BgL_retblockzf2gotozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_retblockzf2gotozf2_bgl))));
								{	/* Cgen/cgen.scm 1040 */
									obj_t BgL_auxz00_9655;
									BgL_objectz00_bglt BgL_tmpz00_9651;

									BgL_auxz00_9655 = ((obj_t) BgL_wide1437z00_7020);
									BgL_tmpz00_9651 =
										((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt)
											((BgL_retblockz00_bglt) BgL_nodez00_6631)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9651, BgL_auxz00_9655);
								}
								((BgL_objectz00_bglt)
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_nodez00_6631)));
								{	/* Cgen/cgen.scm 1040 */
									long BgL_arg2595z00_7021;

									BgL_arg2595z00_7021 =
										BGL_CLASS_NUM(BGl_retblockzf2gotozf2zzcgen_cgenz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_retblockz00_bglt)
												((BgL_retblockz00_bglt) BgL_nodez00_6631))),
										BgL_arg2595z00_7021);
								}
								((BgL_retblockz00_bglt)
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_nodez00_6631)));
							}
							{
								BgL_retblockzf2gotozf2_bglt BgL_auxz00_9669;

								{
									obj_t BgL_auxz00_9670;

									{	/* Cgen/cgen.scm 1041 */
										BgL_objectz00_bglt BgL_tmpz00_9671;

										BgL_tmpz00_9671 =
											((BgL_objectz00_bglt)
											((BgL_retblockz00_bglt)
												((BgL_retblockz00_bglt) BgL_nodez00_6631)));
										BgL_auxz00_9670 = BGL_OBJECT_WIDENING(BgL_tmpz00_9671);
									}
									BgL_auxz00_9669 =
										((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9670);
								}
								((((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9669))->
										BgL_localz00) =
									((obj_t) ((obj_t) BgL_localz00_7003)), BUNSPEC);
							}
							{
								BgL_retblockzf2gotozf2_bglt BgL_auxz00_9679;

								{
									obj_t BgL_auxz00_9680;

									{	/* Cgen/cgen.scm 1042 */
										BgL_objectz00_bglt BgL_tmpz00_9681;

										BgL_tmpz00_9681 =
											((BgL_objectz00_bglt)
											((BgL_retblockz00_bglt)
												((BgL_retblockz00_bglt) BgL_nodez00_6631)));
										BgL_auxz00_9680 = BGL_OBJECT_WIDENING(BgL_tmpz00_9681);
									}
									BgL_auxz00_9679 =
										((BgL_retblockzf2gotozf2_bglt) BgL_auxz00_9680);
								}
								((((BgL_retblockzf2gotozf2_bglt) COBJECT(BgL_auxz00_9679))->
										BgL_labelz00) =
									((obj_t) ((obj_t) BgL_labelz00_7006)), BUNSPEC);
							}
							((BgL_retblockz00_bglt)
								((BgL_retblockz00_bglt) BgL_nodez00_6631));
							{	/* Cgen/cgen.scm 1043 */
								BgL_cblockz00_bglt BgL_new1440z00_7022;

								{	/* Cgen/cgen.scm 1045 */
									BgL_cblockz00_bglt BgL_new1439z00_7023;

									BgL_new1439z00_7023 =
										((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_cblockz00_bgl))));
									{	/* Cgen/cgen.scm 1045 */
										long BgL_arg2608z00_7024;

										BgL_arg2608z00_7024 =
											BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1439z00_7023),
											BgL_arg2608z00_7024);
									}
									BgL_new1440z00_7022 = BgL_new1439z00_7023;
								}
								((((BgL_copz00_bglt) COBJECT(
												((BgL_copz00_bglt) BgL_new1440z00_7022)))->BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) ((BgL_retblockz00_bglt)
															BgL_nodez00_6631))))->BgL_locz00)), BUNSPEC);
								((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
													BgL_new1440z00_7022)))->BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt)
											BGl_za2voidza2z00zztype_cachez00)), BUNSPEC);
								{
									BgL_copz00_bglt BgL_auxz00_9703;

									{	/* Cgen/cgen.scm 1046 */
										BgL_csequencez00_bglt BgL_new1442z00_7025;

										{	/* Cgen/cgen.scm 1048 */
											BgL_csequencez00_bglt BgL_new1441z00_7026;

											BgL_new1441z00_7026 =
												((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_csequencez00_bgl))));
											{	/* Cgen/cgen.scm 1048 */
												long BgL_arg2607z00_7027;

												{	/* Cgen/cgen.scm 1048 */
													obj_t BgL_classz00_7028;

													BgL_classz00_7028 = BGl_csequencez00zzcgen_copz00;
													BgL_arg2607z00_7027 =
														BGL_CLASS_NUM(BgL_classz00_7028);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1441z00_7026),
													BgL_arg2607z00_7027);
											}
											BgL_new1442z00_7025 = BgL_new1441z00_7026;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1442z00_7025)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_retblockz00_bglt)
																	BgL_nodez00_6631))))->BgL_locz00)), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1442z00_7025)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2voidza2z00zztype_cachez00)), BUNSPEC);
										((((BgL_csequencez00_bglt) COBJECT(BgL_new1442z00_7025))->
												BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										{
											obj_t BgL_auxz00_9717;

											{	/* Cgen/cgen.scm 1050 */
												BgL_localzd2varzd2_bglt BgL_arg2596z00_7029;
												BgL_copz00_bglt BgL_arg2597z00_7030;

												{	/* Cgen/cgen.scm 1050 */
													BgL_localzd2varzd2_bglt BgL_new1444z00_7031;

													{	/* Cgen/cgen.scm 1052 */
														BgL_localzd2varzd2_bglt BgL_new1443z00_7032;

														BgL_new1443z00_7032 =
															((BgL_localzd2varzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_localzd2varzd2_bgl))));
														{	/* Cgen/cgen.scm 1052 */
															long BgL_arg2604z00_7033;

															{	/* Cgen/cgen.scm 1052 */
																obj_t BgL_classz00_7034;

																BgL_classz00_7034 =
																	BGl_localzd2varzd2zzcgen_copz00;
																BgL_arg2604z00_7033 =
																	BGL_CLASS_NUM(BgL_classz00_7034);
															}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1443z00_7032),
																BgL_arg2604z00_7033);
														}
														BgL_new1444z00_7031 = BgL_new1443z00_7032;
													}
													((((BgL_copz00_bglt) COBJECT(
																	((BgL_copz00_bglt) BgL_new1444z00_7031)))->
															BgL_locz00) =
														((obj_t) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) (
																				(BgL_retblockz00_bglt)
																				BgL_nodez00_6631))))->BgL_locz00)),
														BUNSPEC);
													((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																		BgL_new1444z00_7031)))->BgL_typez00) =
														((BgL_typez00_bglt) ((BgL_typez00_bglt)
																BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
													{
														obj_t BgL_auxz00_9730;

														{	/* Cgen/cgen.scm 1053 */
															obj_t BgL_list2601z00_7035;

															BgL_list2601z00_7035 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_localz00_7003), BNIL);
															BgL_auxz00_9730 = BgL_list2601z00_7035;
														}
														((((BgL_localzd2varzd2_bglt)
																	COBJECT(BgL_new1444z00_7031))->BgL_varsz00) =
															((obj_t) BgL_auxz00_9730), BUNSPEC);
													}
													BgL_arg2596z00_7029 = BgL_new1444z00_7031;
												}
												{	/* Cgen/cgen.scm 1054 */
													BgL_nodez00_bglt BgL_arg2605z00_7036;

													BgL_arg2605z00_7036 =
														(((BgL_retblockz00_bglt) COBJECT(
																((BgL_retblockz00_bglt) BgL_nodez00_6631)))->
														BgL_bodyz00);
													BgL_arg2597z00_7030 =
														BGl_nodezd2ze3copz31zzcgen_cgenz00
														(BgL_arg2605z00_7036, BgL_retkontz00_7017,
														CBOOL(BgL_inpushexitz00_6633));
												}
												{	/* Cgen/cgen.scm 1049 */
													obj_t BgL_list2598z00_7037;

													{	/* Cgen/cgen.scm 1049 */
														obj_t BgL_arg2599z00_7038;

														{	/* Cgen/cgen.scm 1049 */
															obj_t BgL_arg2600z00_7039;

															BgL_arg2600z00_7039 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_labelz00_7006), BNIL);
															BgL_arg2599z00_7038 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg2597z00_7030),
																BgL_arg2600z00_7039);
														}
														BgL_list2598z00_7037 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2596z00_7029),
															BgL_arg2599z00_7038);
													}
													BgL_auxz00_9717 = BgL_list2598z00_7037;
											}}
											((((BgL_csequencez00_bglt) COBJECT(BgL_new1442z00_7025))->
													BgL_copsz00) = ((obj_t) BgL_auxz00_9717), BUNSPEC);
										}
										BgL_auxz00_9703 = ((BgL_copz00_bglt) BgL_new1442z00_7025);
									}
									((((BgL_cblockz00_bglt) COBJECT(BgL_new1440z00_7022))->
											BgL_bodyz00) =
										((BgL_copz00_bglt) BgL_auxz00_9703), BUNSPEC);
								}
								return ((BgL_copz00_bglt) BgL_new1440z00_7022);
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:2611> */
	obj_t BGl_z62zc3z04anonymousza32611ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6634,
		obj_t BgL_cz00_6635)
	{
		{	/* Cgen/cgen.scm 1039 */
			return BgL_cz00_6635;
		}

	}



/* &node->cop-jump-ex-it1767 */
	BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2jumpzd2exzd2it1767z81zzcgen_cgenz00(obj_t
		BgL_envz00_6636, obj_t BgL_nodez00_6637, obj_t BgL_kontz00_6638,
		obj_t BgL_inpushexitz00_6639)
	{
		{	/* Cgen/cgen.scm 929 */
			{	/* Tools/trace.sch 53 */
				BgL_localz00_bglt BgL_vauxz00_7041;

				{	/* Tools/trace.sch 53 */
					BgL_localz00_bglt BgL_res2860z00_7042;

					{	/* Tools/trace.sch 53 */
						obj_t BgL_idz00_7043;
						BgL_typez00_bglt BgL_typez00_7044;

						BgL_idz00_7043 = CNST_TABLE_REF(3);
						BgL_typez00_7044 =
							((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
						{	/* Tools/trace.sch 53 */
							BgL_localz00_bglt BgL_localz00_7045;

							BgL_localz00_7045 =
								BGl_makezd2localzd2svarz00zzast_localz00(BgL_idz00_7043,
								BgL_typez00_7044);
							{	/* Tools/trace.sch 53 */
								bool_t BgL_test3209z00_9751;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_tmpz00_9752;

									BgL_tmpz00_9752 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_localz00_7045)))->
										BgL_namez00);
									BgL_test3209z00_9751 = STRINGP(BgL_tmpz00_9752);
								}
								if (BgL_test3209z00_9751)
									{	/* Tools/trace.sch 53 */
										BFALSE;
									}
								else
									{	/* Tools/trace.sch 53 */
										BGl_errorz00zz__errorz00(BGl_string2915z00zzcgen_cgenz00,
											BGl_string2916z00zzcgen_cgenz00,
											((obj_t) BgL_localz00_7045));
									}
							}
							BgL_res2860z00_7042 = BgL_localz00_7045;
						}
					}
					BgL_vauxz00_7041 = BgL_res2860z00_7042;
				}
				{	/* Tools/trace.sch 53 */
					BgL_copz00_bglt BgL_vcopz00_7046;

					{	/* Tools/trace.sch 53 */
						BgL_setqz00_bglt BgL_arg2592z00_7047;

						{	/* Tools/trace.sch 53 */
							BgL_nodez00_bglt BgL_arg2594z00_7048;

							BgL_arg2594z00_7048 =
								(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
										((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6637)))->
								BgL_valuez00);
							BgL_arg2592z00_7047 =
								BGl_nodezd2setqzd2zzcgen_cgenz00(((BgL_variablez00_bglt)
									BgL_vauxz00_7041), BgL_arg2594z00_7048);
						}
						BgL_vcopz00_7046 =
							BGl_nodezd2ze3copz31zzcgen_cgenz00(
							((BgL_nodez00_bglt) BgL_arg2592z00_7047),
							BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
							CBOOL(BgL_inpushexitz00_6639));
					}
					{	/* Tools/trace.sch 53 */
						BgL_nodez00_bglt BgL_exitz00_7049;

						BgL_exitz00_7049 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6637)))->
							BgL_exitz00);
						{	/* Tools/trace.sch 53 */
							BgL_localz00_bglt BgL_eauxz00_7050;

							{	/* Tools/trace.sch 53 */
								BgL_localz00_bglt BgL_res2861z00_7051;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_idz00_7052;
									BgL_typez00_bglt BgL_typez00_7053;

									BgL_idz00_7052 = CNST_TABLE_REF(13);
									BgL_typez00_7053 =
										((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00);
									{	/* Tools/trace.sch 53 */
										BgL_localz00_bglt BgL_localz00_7054;

										BgL_localz00_7054 =
											BGl_makezd2localzd2svarz00zzast_localz00(BgL_idz00_7052,
											BgL_typez00_7053);
										{	/* Tools/trace.sch 53 */
											bool_t BgL_test3210z00_9770;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_tmpz00_9771;

												BgL_tmpz00_9771 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_localz00_7054)))->
													BgL_namez00);
												BgL_test3210z00_9770 = STRINGP(BgL_tmpz00_9771);
											}
											if (BgL_test3210z00_9770)
												{	/* Tools/trace.sch 53 */
													BFALSE;
												}
											else
												{	/* Tools/trace.sch 53 */
													BGl_errorz00zz__errorz00
														(BGl_string2915z00zzcgen_cgenz00,
														BGl_string2916z00zzcgen_cgenz00,
														((obj_t) BgL_localz00_7054));
												}
										}
										BgL_res2861z00_7051 = BgL_localz00_7054;
									}
								}
								BgL_eauxz00_7050 = BgL_res2861z00_7051;
							}
							{	/* Tools/trace.sch 53 */
								BgL_copz00_bglt BgL_ecopz00_7055;

								{	/* Tools/trace.sch 53 */
									BgL_setqz00_bglt BgL_arg2591z00_7056;

									BgL_arg2591z00_7056 =
										BGl_nodezd2setqzd2zzcgen_cgenz00(
										((BgL_variablez00_bglt) BgL_eauxz00_7050),
										BgL_exitz00_7049);
									BgL_ecopz00_7055 =
										BGl_nodezd2ze3copz31zzcgen_cgenz00(((BgL_nodez00_bglt)
											BgL_arg2591z00_7056),
										BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
										CBOOL(BgL_inpushexitz00_6639));
								}
								{	/* Tools/trace.sch 53 */

									{	/* Tools/trace.sch 53 */
										bool_t BgL_test3211z00_9782;

										{	/* Tools/trace.sch 53 */
											bool_t BgL_test3212z00_9783;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_classz00_7057;

												BgL_classz00_7057 = BGl_csetqz00zzcgen_copz00;
												{	/* Tools/trace.sch 53 */
													BgL_objectz00_bglt BgL_arg1807z00_7058;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_tmpz00_9784;

														BgL_tmpz00_9784 =
															((obj_t) ((BgL_objectz00_bglt) BgL_vcopz00_7046));
														BgL_arg1807z00_7058 =
															(BgL_objectz00_bglt) (BgL_tmpz00_9784);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Tools/trace.sch 53 */
															long BgL_idxz00_7059;

															BgL_idxz00_7059 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_7058);
															BgL_test3212z00_9783 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_7059 + 2L)) == BgL_classz00_7057);
														}
													else
														{	/* Tools/trace.sch 53 */
															bool_t BgL_res2862z00_7062;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_oclassz00_7063;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1815z00_7064;
																	long BgL_arg1816z00_7065;

																	BgL_arg1815z00_7064 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg1817z00_7066;

																		BgL_arg1817z00_7066 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_7058);
																		BgL_arg1816z00_7065 =
																			(BgL_arg1817z00_7066 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_7063 =
																		VECTOR_REF(BgL_arg1815z00_7064,
																		BgL_arg1816z00_7065);
																}
																{	/* Tools/trace.sch 53 */
																	bool_t BgL__ortest_1115z00_7067;

																	BgL__ortest_1115z00_7067 =
																		(BgL_classz00_7057 == BgL_oclassz00_7063);
																	if (BgL__ortest_1115z00_7067)
																		{	/* Tools/trace.sch 53 */
																			BgL_res2862z00_7062 =
																				BgL__ortest_1115z00_7067;
																		}
																	else
																		{	/* Tools/trace.sch 53 */
																			long BgL_odepthz00_7068;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1804z00_7069;

																				BgL_arg1804z00_7069 =
																					(BgL_oclassz00_7063);
																				BgL_odepthz00_7068 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_7069);
																			}
																			if ((2L < BgL_odepthz00_7068))
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1802z00_7070;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1803z00_7071;

																						BgL_arg1803z00_7071 =
																							(BgL_oclassz00_7063);
																						BgL_arg1802z00_7070 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_7071, 2L);
																					}
																					BgL_res2862z00_7062 =
																						(BgL_arg1802z00_7070 ==
																						BgL_classz00_7057);
																				}
																			else
																				{	/* Tools/trace.sch 53 */
																					BgL_res2862z00_7062 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3212z00_9783 = BgL_res2862z00_7062;
														}
												}
											}
											if (BgL_test3212z00_9783)
												{	/* Tools/trace.sch 53 */
													if (
														(((obj_t)
																(((BgL_varcz00_bglt) COBJECT(
																			(((BgL_csetqz00_bglt) COBJECT(
																						((BgL_csetqz00_bglt)
																							BgL_vcopz00_7046)))->
																				BgL_varz00)))->BgL_variablez00)) ==
															((obj_t) BgL_vauxz00_7041)))
														{	/* Tools/trace.sch 53 */
															bool_t BgL_test3217z00_9814;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_classz00_7072;

																BgL_classz00_7072 = BGl_csetqz00zzcgen_copz00;
																{	/* Tools/trace.sch 53 */
																	BgL_objectz00_bglt BgL_arg1807z00_7073;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_tmpz00_9815;

																		BgL_tmpz00_9815 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_ecopz00_7055));
																		BgL_arg1807z00_7073 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_9815);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Tools/trace.sch 53 */
																			long BgL_idxz00_7074;

																			BgL_idxz00_7074 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_7073);
																			BgL_test3217z00_9814 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_7074 + 2L)) ==
																				BgL_classz00_7072);
																		}
																	else
																		{	/* Tools/trace.sch 53 */
																			bool_t BgL_res2863z00_7077;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_oclassz00_7078;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1815z00_7079;
																					long BgL_arg1816z00_7080;

																					BgL_arg1815z00_7079 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Tools/trace.sch 53 */
																						long BgL_arg1817z00_7081;

																						BgL_arg1817z00_7081 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_7073);
																						BgL_arg1816z00_7080 =
																							(BgL_arg1817z00_7081 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_7078 =
																						VECTOR_REF(BgL_arg1815z00_7079,
																						BgL_arg1816z00_7080);
																				}
																				{	/* Tools/trace.sch 53 */
																					bool_t BgL__ortest_1115z00_7082;

																					BgL__ortest_1115z00_7082 =
																						(BgL_classz00_7072 ==
																						BgL_oclassz00_7078);
																					if (BgL__ortest_1115z00_7082)
																						{	/* Tools/trace.sch 53 */
																							BgL_res2863z00_7077 =
																								BgL__ortest_1115z00_7082;
																						}
																					else
																						{	/* Tools/trace.sch 53 */
																							long BgL_odepthz00_7083;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1804z00_7084;

																								BgL_arg1804z00_7084 =
																									(BgL_oclassz00_7078);
																								BgL_odepthz00_7083 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_7084);
																							}
																							if ((2L < BgL_odepthz00_7083))
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1802z00_7085;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1803z00_7086;

																										BgL_arg1803z00_7086 =
																											(BgL_oclassz00_7078);
																										BgL_arg1802z00_7085 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_7086, 2L);
																									}
																									BgL_res2863z00_7077 =
																										(BgL_arg1802z00_7085 ==
																										BgL_classz00_7072);
																								}
																							else
																								{	/* Tools/trace.sch 53 */
																									BgL_res2863z00_7077 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test3217z00_9814 =
																				BgL_res2863z00_7077;
																		}
																}
															}
															if (BgL_test3217z00_9814)
																{	/* Tools/trace.sch 53 */
																	BgL_test3211z00_9782 =
																		(
																		((obj_t)
																			(((BgL_varcz00_bglt) COBJECT(
																						(((BgL_csetqz00_bglt) COBJECT(
																									((BgL_csetqz00_bglt)
																										BgL_ecopz00_7055)))->
																							BgL_varz00)))->
																				BgL_variablez00)) ==
																		((obj_t) BgL_eauxz00_7050));
																}
															else
																{	/* Tools/trace.sch 53 */
																	BgL_test3211z00_9782 = ((bool_t) 0);
																}
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_test3211z00_9782 = ((bool_t) 0);
														}
												}
											else
												{	/* Tools/trace.sch 53 */
													BgL_test3211z00_9782 = ((bool_t) 0);
												}
										}
										if (BgL_test3211z00_9782)
											{	/* Tools/trace.sch 53 */
												BgL_cjumpzd2exzd2itz00_bglt BgL_arg2495z00_7087;

												{	/* Tools/trace.sch 53 */
													BgL_cjumpzd2exzd2itz00_bglt BgL_new1391z00_7088;

													{	/* Tools/trace.sch 53 */
														BgL_cjumpzd2exzd2itz00_bglt BgL_new1390z00_7089;

														BgL_new1390z00_7089 =
															((BgL_cjumpzd2exzd2itz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_cjumpzd2exzd2itz00_bgl))));
														{	/* Tools/trace.sch 53 */
															long BgL_arg2497z00_7090;

															BgL_arg2497z00_7090 =
																BGL_CLASS_NUM
																(BGl_cjumpzd2exzd2itz00zzcgen_copz00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1390z00_7089), BgL_arg2497z00_7090);
														}
														BgL_new1391z00_7088 = BgL_new1390z00_7089;
													}
													((((BgL_copz00_bglt) COBJECT(
																	((BgL_copz00_bglt) BgL_new1391z00_7088)))->
															BgL_locz00) =
														((obj_t) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) (
																				(BgL_jumpzd2exzd2itz00_bglt)
																				BgL_nodez00_6637))))->BgL_locz00)),
														BUNSPEC);
													((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																		BgL_new1391z00_7088)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) (
																				(BgL_jumpzd2exzd2itz00_bglt)
																				BgL_nodez00_6637))))->BgL_typez00)),
														BUNSPEC);
													((((BgL_cjumpzd2exzd2itz00_bglt)
																COBJECT(BgL_new1391z00_7088))->BgL_exitz00) =
														((BgL_copz00_bglt) (((BgL_csetqz00_bglt)
																	COBJECT(((BgL_csetqz00_bglt)
																			BgL_ecopz00_7055)))->BgL_valuez00)),
														BUNSPEC);
													((((BgL_cjumpzd2exzd2itz00_bglt)
																COBJECT(BgL_new1391z00_7088))->BgL_valuez00) =
														((BgL_copz00_bglt) (((BgL_csetqz00_bglt)
																	COBJECT(((BgL_csetqz00_bglt)
																			BgL_vcopz00_7046)))->BgL_valuez00)),
														BUNSPEC);
													BgL_arg2495z00_7087 = BgL_new1391z00_7088;
												}
												return ((BgL_copz00_bglt) BgL_arg2495z00_7087);
											}
										else
											{	/* Tools/trace.sch 53 */
												bool_t BgL_test3221z00_9865;

												{	/* Tools/trace.sch 53 */
													bool_t BgL_test3222z00_9866;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_classz00_7091;

														BgL_classz00_7091 = BGl_csetqz00zzcgen_copz00;
														{	/* Tools/trace.sch 53 */
															BgL_objectz00_bglt BgL_arg1807z00_7092;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_tmpz00_9867;

																BgL_tmpz00_9867 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_vcopz00_7046));
																BgL_arg1807z00_7092 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_9867);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Tools/trace.sch 53 */
																	long BgL_idxz00_7093;

																	BgL_idxz00_7093 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_7092);
																	BgL_test3222z00_9866 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_7093 + 2L)) ==
																		BgL_classz00_7091);
																}
															else
																{	/* Tools/trace.sch 53 */
																	bool_t BgL_res2864z00_7096;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_oclassz00_7097;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1815z00_7098;
																			long BgL_arg1816z00_7099;

																			BgL_arg1815z00_7098 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Tools/trace.sch 53 */
																				long BgL_arg1817z00_7100;

																				BgL_arg1817z00_7100 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_7092);
																				BgL_arg1816z00_7099 =
																					(BgL_arg1817z00_7100 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_7097 =
																				VECTOR_REF(BgL_arg1815z00_7098,
																				BgL_arg1816z00_7099);
																		}
																		{	/* Tools/trace.sch 53 */
																			bool_t BgL__ortest_1115z00_7101;

																			BgL__ortest_1115z00_7101 =
																				(BgL_classz00_7091 ==
																				BgL_oclassz00_7097);
																			if (BgL__ortest_1115z00_7101)
																				{	/* Tools/trace.sch 53 */
																					BgL_res2864z00_7096 =
																						BgL__ortest_1115z00_7101;
																				}
																			else
																				{	/* Tools/trace.sch 53 */
																					long BgL_odepthz00_7102;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1804z00_7103;

																						BgL_arg1804z00_7103 =
																							(BgL_oclassz00_7097);
																						BgL_odepthz00_7102 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_7103);
																					}
																					if ((2L < BgL_odepthz00_7102))
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1802z00_7104;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1803z00_7105;

																								BgL_arg1803z00_7105 =
																									(BgL_oclassz00_7097);
																								BgL_arg1802z00_7104 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_7105, 2L);
																							}
																							BgL_res2864z00_7096 =
																								(BgL_arg1802z00_7104 ==
																								BgL_classz00_7091);
																						}
																					else
																						{	/* Tools/trace.sch 53 */
																							BgL_res2864z00_7096 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test3222z00_9866 = BgL_res2864z00_7096;
																}
														}
													}
													if (BgL_test3222z00_9866)
														{	/* Tools/trace.sch 53 */
															BgL_test3221z00_9865 =
																(
																((obj_t)
																	(((BgL_varcz00_bglt) COBJECT(
																				(((BgL_csetqz00_bglt) COBJECT(
																							((BgL_csetqz00_bglt)
																								BgL_vcopz00_7046)))->
																					BgL_varz00)))->BgL_variablez00)) ==
																((obj_t) BgL_vauxz00_7041));
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_test3221z00_9865 = ((bool_t) 0);
														}
												}
												if (BgL_test3221z00_9865)
													{	/* Tools/trace.sch 53 */
														BgL_cblockz00_bglt BgL_new1393z00_7106;

														{	/* Tools/trace.sch 53 */
															BgL_cblockz00_bglt BgL_new1392z00_7107;

															BgL_new1392z00_7107 =
																((BgL_cblockz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_cblockz00_bgl))));
															{	/* Tools/trace.sch 53 */
																long BgL_arg2519z00_7108;

																BgL_arg2519z00_7108 =
																	BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1392z00_7107),
																	BgL_arg2519z00_7108);
															}
															BgL_new1393z00_7106 = BgL_new1392z00_7107;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1393z00_7106)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_jumpzd2exzd2itz00_bglt)
																					BgL_nodez00_6637))))->BgL_locz00)),
															BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1393z00_7106)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_jumpzd2exzd2itz00_bglt)
																					BgL_nodez00_6637))))->BgL_typez00)),
															BUNSPEC);
														{
															BgL_copz00_bglt BgL_auxz00_9910;

															{	/* Tools/trace.sch 53 */
																BgL_csequencez00_bglt BgL_new1395z00_7109;

																{	/* Tools/trace.sch 53 */
																	BgL_csequencez00_bglt BgL_new1394z00_7110;

																	BgL_new1394z00_7110 =
																		((BgL_csequencez00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_csequencez00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg2518z00_7111;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_classz00_7112;

																			BgL_classz00_7112 =
																				BGl_csequencez00zzcgen_copz00;
																			BgL_arg2518z00_7111 =
																				BGL_CLASS_NUM(BgL_classz00_7112);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1394z00_7110),
																			BgL_arg2518z00_7111);
																	}
																	BgL_new1395z00_7109 = BgL_new1394z00_7110;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1395z00_7109)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_jumpzd2exzd2itz00_bglt)
																							BgL_nodez00_6637))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1395z00_7109)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_jumpzd2exzd2itz00_bglt)
																							BgL_nodez00_6637))))->
																			BgL_typez00)), BUNSPEC);
																((((BgL_csequencez00_bglt)
																			COBJECT(BgL_new1395z00_7109))->
																		BgL_czd2expzf3z21) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
																{
																	obj_t BgL_auxz00_9926;

																	{	/* Tools/trace.sch 53 */
																		BgL_localzd2varzd2_bglt BgL_arg2503z00_7113;
																		BgL_csequencez00_bglt BgL_arg2505z00_7114;
																		BgL_cjumpzd2exzd2itz00_bglt
																			BgL_arg2506z00_7115;
																		{	/* Tools/trace.sch 53 */
																			BgL_localzd2varzd2_bglt
																				BgL_new1397z00_7116;
																			{	/* Tools/trace.sch 53 */
																				BgL_localzd2varzd2_bglt
																					BgL_new1396z00_7117;
																				BgL_new1396z00_7117 =
																					((BgL_localzd2varzd2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_localzd2varzd2_bgl))));
																				{	/* Tools/trace.sch 53 */
																					long BgL_arg2511z00_7118;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_classz00_7119;

																						BgL_classz00_7119 =
																							BGl_localzd2varzd2zzcgen_copz00;
																						BgL_arg2511z00_7118 =
																							BGL_CLASS_NUM(BgL_classz00_7119);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1396z00_7117),
																						BgL_arg2511z00_7118);
																				}
																				BgL_new1397z00_7116 =
																					BgL_new1396z00_7117;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1397z00_7116)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt) (
																										(BgL_jumpzd2exzd2itz00_bglt)
																										BgL_nodez00_6637))))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1397z00_7116)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00)),
																				BUNSPEC);
																			{
																				obj_t BgL_auxz00_9939;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_list2510z00_7120;

																					BgL_list2510z00_7120 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_eauxz00_7050), BNIL);
																					BgL_auxz00_9939 =
																						BgL_list2510z00_7120;
																				}
																				((((BgL_localzd2varzd2_bglt)
																							COBJECT(BgL_new1397z00_7116))->
																						BgL_varsz00) =
																					((obj_t) BgL_auxz00_9939), BUNSPEC);
																			}
																			BgL_arg2503z00_7113 = BgL_new1397z00_7116;
																		}
																		{	/* Tools/trace.sch 53 */
																			BgL_csequencez00_bglt BgL_new1399z00_7121;

																			{	/* Tools/trace.sch 53 */
																				BgL_csequencez00_bglt
																					BgL_new1398z00_7122;
																				BgL_new1398z00_7122 =
																					((BgL_csequencez00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_csequencez00_bgl))));
																				{	/* Tools/trace.sch 53 */
																					long BgL_arg2513z00_7123;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_classz00_7124;

																						BgL_classz00_7124 =
																							BGl_csequencez00zzcgen_copz00;
																						BgL_arg2513z00_7123 =
																							BGL_CLASS_NUM(BgL_classz00_7124);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1398z00_7122),
																						BgL_arg2513z00_7123);
																				}
																				BgL_new1399z00_7121 =
																					BgL_new1398z00_7122;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1399z00_7121)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt) (
																										(BgL_jumpzd2exzd2itz00_bglt)
																										BgL_nodez00_6637))))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1399z00_7121)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1399z00_7121))->
																					BgL_czd2expzf3z21) =
																				((bool_t) ((bool_t) 0)), BUNSPEC);
																			{
																				obj_t BgL_auxz00_9956;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_list2512z00_7125;

																					BgL_list2512z00_7125 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_ecopz00_7055), BNIL);
																					BgL_auxz00_9956 =
																						BgL_list2512z00_7125;
																				}
																				((((BgL_csequencez00_bglt)
																							COBJECT(BgL_new1399z00_7121))->
																						BgL_copsz00) =
																					((obj_t) BgL_auxz00_9956), BUNSPEC);
																			}
																			BgL_arg2505z00_7114 = BgL_new1399z00_7121;
																		}
																		{	/* Tools/trace.sch 53 */
																			BgL_cjumpzd2exzd2itz00_bglt
																				BgL_arg2514z00_7126;
																			{	/* Tools/trace.sch 53 */
																				BgL_cjumpzd2exzd2itz00_bglt
																					BgL_new1401z00_7127;
																				{	/* Tools/trace.sch 53 */
																					BgL_cjumpzd2exzd2itz00_bglt
																						BgL_new1400z00_7128;
																					BgL_new1400z00_7128 =
																						((BgL_cjumpzd2exzd2itz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_cjumpzd2exzd2itz00_bgl))));
																					{	/* Tools/trace.sch 53 */
																						long BgL_arg2516z00_7129;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_classz00_7130;

																							BgL_classz00_7130 =
																								BGl_cjumpzd2exzd2itz00zzcgen_copz00;
																							BgL_arg2516z00_7129 =
																								BGL_CLASS_NUM
																								(BgL_classz00_7130);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1400z00_7128),
																							BgL_arg2516z00_7129);
																					}
																					BgL_new1401z00_7127 =
																						BgL_new1400z00_7128;
																				}
																				((((BgL_copz00_bglt) COBJECT(
																								((BgL_copz00_bglt)
																									BgL_new1401z00_7127)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt) (
																											(BgL_jumpzd2exzd2itz00_bglt)
																											BgL_nodez00_6637))))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_copz00_bglt)
																							COBJECT(((BgL_copz00_bglt)
																									BgL_new1401z00_7127)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt) (
																											(BgL_jumpzd2exzd2itz00_bglt)
																											BgL_nodez00_6637))))->
																							BgL_typez00)), BUNSPEC);
																				{
																					BgL_copz00_bglt BgL_auxz00_9974;

																					{	/* Tools/trace.sch 53 */
																						BgL_varcz00_bglt
																							BgL_new1403z00_7131;
																						{	/* Tools/trace.sch 53 */
																							BgL_varcz00_bglt
																								BgL_new1402z00_7132;
																							BgL_new1402z00_7132 =
																								((BgL_varcz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_varcz00_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg2515z00_7133;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_7134;

																									BgL_classz00_7134 =
																										BGl_varcz00zzcgen_copz00;
																									BgL_arg2515z00_7133 =
																										BGL_CLASS_NUM
																										(BgL_classz00_7134);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1402z00_7132),
																									BgL_arg2515z00_7133);
																							}
																							BgL_new1403z00_7131 =
																								BgL_new1402z00_7132;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1403z00_7131)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6637))))->BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1403z00_7131)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) ((
																										(BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt)
																												BgL_eauxz00_7050)))->
																									BgL_typez00)), BUNSPEC);
																						((((BgL_varcz00_bglt)
																									COBJECT
																									(BgL_new1403z00_7131))->
																								BgL_variablez00) =
																							((BgL_variablez00_bglt) (
																									(BgL_variablez00_bglt)
																									BgL_eauxz00_7050)), BUNSPEC);
																						BgL_auxz00_9974 =
																							((BgL_copz00_bglt)
																							BgL_new1403z00_7131);
																					}
																					((((BgL_cjumpzd2exzd2itz00_bglt)
																								COBJECT(BgL_new1401z00_7127))->
																							BgL_exitz00) =
																						((BgL_copz00_bglt) BgL_auxz00_9974),
																						BUNSPEC);
																				}
																				((((BgL_cjumpzd2exzd2itz00_bglt)
																							COBJECT(BgL_new1401z00_7127))->
																						BgL_valuez00) =
																					((BgL_copz00_bglt) ((
																								(BgL_csetqz00_bglt)
																								COBJECT(((BgL_csetqz00_bglt)
																										BgL_vcopz00_7046)))->
																							BgL_valuez00)), BUNSPEC);
																				BgL_arg2514z00_7126 =
																					BgL_new1401z00_7127;
																			}
																			BgL_arg2506z00_7115 = BgL_arg2514z00_7126;
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_list2507z00_7135;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg2508z00_7136;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg2509z00_7137;

																					BgL_arg2509z00_7137 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_arg2506z00_7115),
																						BNIL);
																					BgL_arg2508z00_7136 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_arg2505z00_7114),
																						BgL_arg2509z00_7137);
																				}
																				BgL_list2507z00_7135 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg2503z00_7113),
																					BgL_arg2508z00_7136);
																			}
																			BgL_auxz00_9926 = BgL_list2507z00_7135;
																	}}
																	((((BgL_csequencez00_bglt)
																				COBJECT(BgL_new1395z00_7109))->
																			BgL_copsz00) =
																		((obj_t) BgL_auxz00_9926), BUNSPEC);
																}
																BgL_auxz00_9910 =
																	((BgL_copz00_bglt) BgL_new1395z00_7109);
															}
															((((BgL_cblockz00_bglt)
																		COBJECT(BgL_new1393z00_7106))->
																	BgL_bodyz00) =
																((BgL_copz00_bglt) BgL_auxz00_9910), BUNSPEC);
														}
														return ((BgL_copz00_bglt) BgL_new1393z00_7106);
													}
												else
													{	/* Tools/trace.sch 53 */
														bool_t BgL_test3226z00_10005;

														{	/* Tools/trace.sch 53 */
															bool_t BgL_test3227z00_10006;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_classz00_7138;

																BgL_classz00_7138 = BGl_csetqz00zzcgen_copz00;
																{	/* Tools/trace.sch 53 */
																	BgL_objectz00_bglt BgL_arg1807z00_7139;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_tmpz00_10007;

																		BgL_tmpz00_10007 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_ecopz00_7055));
																		BgL_arg1807z00_7139 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_10007);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Tools/trace.sch 53 */
																			long BgL_idxz00_7140;

																			BgL_idxz00_7140 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_7139);
																			BgL_test3227z00_10006 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_7140 + 2L)) ==
																				BgL_classz00_7138);
																		}
																	else
																		{	/* Tools/trace.sch 53 */
																			bool_t BgL_res2868z00_7143;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_oclassz00_7144;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1815z00_7145;
																					long BgL_arg1816z00_7146;

																					BgL_arg1815z00_7145 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Tools/trace.sch 53 */
																						long BgL_arg1817z00_7147;

																						BgL_arg1817z00_7147 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_7139);
																						BgL_arg1816z00_7146 =
																							(BgL_arg1817z00_7147 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_7144 =
																						VECTOR_REF(BgL_arg1815z00_7145,
																						BgL_arg1816z00_7146);
																				}
																				{	/* Tools/trace.sch 53 */
																					bool_t BgL__ortest_1115z00_7148;

																					BgL__ortest_1115z00_7148 =
																						(BgL_classz00_7138 ==
																						BgL_oclassz00_7144);
																					if (BgL__ortest_1115z00_7148)
																						{	/* Tools/trace.sch 53 */
																							BgL_res2868z00_7143 =
																								BgL__ortest_1115z00_7148;
																						}
																					else
																						{	/* Tools/trace.sch 53 */
																							long BgL_odepthz00_7149;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1804z00_7150;

																								BgL_arg1804z00_7150 =
																									(BgL_oclassz00_7144);
																								BgL_odepthz00_7149 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_7150);
																							}
																							if ((2L < BgL_odepthz00_7149))
																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1802z00_7151;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1803z00_7152;

																										BgL_arg1803z00_7152 =
																											(BgL_oclassz00_7144);
																										BgL_arg1802z00_7151 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_7152, 2L);
																									}
																									BgL_res2868z00_7143 =
																										(BgL_arg1802z00_7151 ==
																										BgL_classz00_7138);
																								}
																							else
																								{	/* Tools/trace.sch 53 */
																									BgL_res2868z00_7143 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test3227z00_10006 =
																				BgL_res2868z00_7143;
																		}
																}
															}
															if (BgL_test3227z00_10006)
																{	/* Tools/trace.sch 53 */
																	BgL_test3226z00_10005 =
																		(
																		((obj_t)
																			(((BgL_varcz00_bglt) COBJECT(
																						(((BgL_csetqz00_bglt) COBJECT(
																									((BgL_csetqz00_bglt)
																										BgL_ecopz00_7055)))->
																							BgL_varz00)))->
																				BgL_variablez00)) ==
																		((obj_t) BgL_eauxz00_7050));
																}
															else
																{	/* Tools/trace.sch 53 */
																	BgL_test3226z00_10005 = ((bool_t) 0);
																}
														}
														if (BgL_test3226z00_10005)
															{	/* Tools/trace.sch 53 */
																BgL_cblockz00_bglt BgL_new1405z00_7153;

																{	/* Tools/trace.sch 53 */
																	BgL_cblockz00_bglt BgL_new1404z00_7154;

																	BgL_new1404z00_7154 =
																		((BgL_cblockz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_cblockz00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg2548z00_7155;

																		BgL_arg2548z00_7155 =
																			BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1404z00_7154),
																			BgL_arg2548z00_7155);
																	}
																	BgL_new1405z00_7153 = BgL_new1404z00_7154;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1405z00_7153)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_jumpzd2exzd2itz00_bglt)
																							BgL_nodez00_6637))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1405z00_7153)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_jumpzd2exzd2itz00_bglt)
																							BgL_nodez00_6637))))->
																			BgL_typez00)), BUNSPEC);
																{
																	BgL_copz00_bglt BgL_auxz00_10050;

																	{	/* Tools/trace.sch 53 */
																		BgL_csequencez00_bglt BgL_new1407z00_7156;

																		{	/* Tools/trace.sch 53 */
																			BgL_csequencez00_bglt BgL_new1406z00_7157;

																			BgL_new1406z00_7157 =
																				((BgL_csequencez00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_csequencez00_bgl))));
																			{	/* Tools/trace.sch 53 */
																				long BgL_arg2546z00_7158;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_classz00_7159;

																					BgL_classz00_7159 =
																						BGl_csequencez00zzcgen_copz00;
																					BgL_arg2546z00_7158 =
																						BGL_CLASS_NUM(BgL_classz00_7159);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1406z00_7157),
																					BgL_arg2546z00_7158);
																			}
																			BgL_new1407z00_7156 = BgL_new1406z00_7157;
																		}
																		((((BgL_copz00_bglt) COBJECT(
																						((BgL_copz00_bglt)
																							BgL_new1407z00_7156)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_jumpzd2exzd2itz00_bglt)
																									BgL_nodez00_6637))))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_copz00_bglt)
																					COBJECT(((BgL_copz00_bglt)
																							BgL_new1407z00_7156)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_jumpzd2exzd2itz00_bglt)
																									BgL_nodez00_6637))))->
																					BgL_typez00)), BUNSPEC);
																		((((BgL_csequencez00_bglt)
																					COBJECT(BgL_new1407z00_7156))->
																				BgL_czd2expzf3z21) =
																			((bool_t) ((bool_t) 0)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_10066;

																			{	/* Tools/trace.sch 53 */
																				BgL_localzd2varzd2_bglt
																					BgL_arg2526z00_7160;
																				BgL_csequencez00_bglt
																					BgL_arg2527z00_7161;
																				BgL_cjumpzd2exzd2itz00_bglt
																					BgL_arg2528z00_7162;
																				{	/* Tools/trace.sch 53 */
																					BgL_localzd2varzd2_bglt
																						BgL_new1409z00_7163;
																					{	/* Tools/trace.sch 53 */
																						BgL_localzd2varzd2_bglt
																							BgL_new1408z00_7164;
																						BgL_new1408z00_7164 =
																							((BgL_localzd2varzd2_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_localzd2varzd2_bgl))));
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg2538z00_7165;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_classz00_7166;

																								BgL_classz00_7166 =
																									BGl_localzd2varzd2zzcgen_copz00;
																								BgL_arg2538z00_7165 =
																									BGL_CLASS_NUM
																									(BgL_classz00_7166);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1408z00_7164),
																								BgL_arg2538z00_7165);
																						}
																						BgL_new1409z00_7163 =
																							BgL_new1408z00_7164;
																					}
																					((((BgL_copz00_bglt) COBJECT(
																									((BgL_copz00_bglt)
																										BgL_new1409z00_7163)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt) (
																												(BgL_jumpzd2exzd2itz00_bglt)
																												BgL_nodez00_6637))))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_copz00_bglt)
																								COBJECT(((BgL_copz00_bglt)
																										BgL_new1409z00_7163)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2objza2z00zztype_cachez00)),
																						BUNSPEC);
																					{
																						obj_t BgL_auxz00_10079;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_list2537z00_7167;

																							BgL_list2537z00_7167 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_vauxz00_7041),
																								BNIL);
																							BgL_auxz00_10079 =
																								BgL_list2537z00_7167;
																						}
																						((((BgL_localzd2varzd2_bglt)
																									COBJECT
																									(BgL_new1409z00_7163))->
																								BgL_varsz00) =
																							((obj_t) BgL_auxz00_10079),
																							BUNSPEC);
																					}
																					BgL_arg2526z00_7160 =
																						BgL_new1409z00_7163;
																				}
																				{	/* Tools/trace.sch 53 */
																					BgL_csequencez00_bglt
																						BgL_new1411z00_7168;
																					{	/* Tools/trace.sch 53 */
																						BgL_csequencez00_bglt
																							BgL_new1410z00_7169;
																						BgL_new1410z00_7169 =
																							((BgL_csequencez00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_csequencez00_bgl))));
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg2540z00_7170;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_classz00_7171;

																								BgL_classz00_7171 =
																									BGl_csequencez00zzcgen_copz00;
																								BgL_arg2540z00_7170 =
																									BGL_CLASS_NUM
																									(BgL_classz00_7171);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1410z00_7169),
																								BgL_arg2540z00_7170);
																						}
																						BgL_new1411z00_7168 =
																							BgL_new1410z00_7169;
																					}
																					((((BgL_copz00_bglt) COBJECT(
																									((BgL_copz00_bglt)
																										BgL_new1411z00_7168)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt) (
																												(BgL_jumpzd2exzd2itz00_bglt)
																												BgL_nodez00_6637))))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_copz00_bglt)
																								COBJECT(((BgL_copz00_bglt)
																										BgL_new1411z00_7168)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2objza2z00zztype_cachez00)),
																						BUNSPEC);
																					((((BgL_csequencez00_bglt)
																								COBJECT(BgL_new1411z00_7168))->
																							BgL_czd2expzf3z21) =
																						((bool_t) ((bool_t) 0)), BUNSPEC);
																					{
																						obj_t BgL_auxz00_10096;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_list2539z00_7172;

																							BgL_list2539z00_7172 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_vcopz00_7046),
																								BNIL);
																							BgL_auxz00_10096 =
																								BgL_list2539z00_7172;
																						}
																						((((BgL_csequencez00_bglt)
																									COBJECT
																									(BgL_new1411z00_7168))->
																								BgL_copsz00) =
																							((obj_t) BgL_auxz00_10096),
																							BUNSPEC);
																					}
																					BgL_arg2527z00_7161 =
																						BgL_new1411z00_7168;
																				}
																				{	/* Tools/trace.sch 53 */
																					BgL_cjumpzd2exzd2itz00_bglt
																						BgL_arg2542z00_7173;
																					{	/* Tools/trace.sch 53 */
																						BgL_cjumpzd2exzd2itz00_bglt
																							BgL_new1413z00_7174;
																						{	/* Tools/trace.sch 53 */
																							BgL_cjumpzd2exzd2itz00_bglt
																								BgL_new1412z00_7175;
																							BgL_new1412z00_7175 =
																								((BgL_cjumpzd2exzd2itz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_cjumpzd2exzd2itz00_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg2545z00_7176;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_7177;

																									BgL_classz00_7177 =
																										BGl_cjumpzd2exzd2itz00zzcgen_copz00;
																									BgL_arg2545z00_7176 =
																										BGL_CLASS_NUM
																										(BgL_classz00_7177);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1412z00_7175),
																									BgL_arg2545z00_7176);
																							}
																							BgL_new1413z00_7174 =
																								BgL_new1412z00_7175;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1413z00_7174)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6637))))->BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1413z00_7174)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) ((
																										(BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6637))))->BgL_typez00)), BUNSPEC);
																						((((BgL_cjumpzd2exzd2itz00_bglt)
																									COBJECT
																									(BgL_new1413z00_7174))->
																								BgL_exitz00) =
																							((BgL_copz00_bglt) ((
																										(BgL_csetqz00_bglt)
																										COBJECT(((BgL_csetqz00_bglt)
																												BgL_ecopz00_7055)))->
																									BgL_valuez00)), BUNSPEC);
																						{
																							BgL_copz00_bglt BgL_auxz00_10117;

																							{	/* Tools/trace.sch 53 */
																								BgL_varcz00_bglt
																									BgL_new1415z00_7178;
																								{	/* Tools/trace.sch 53 */
																									BgL_varcz00_bglt
																										BgL_new1414z00_7179;
																									BgL_new1414z00_7179 =
																										((BgL_varcz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_varcz00_bgl))));
																									{	/* Tools/trace.sch 53 */
																										long BgL_arg2543z00_7180;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_classz00_7181;

																											BgL_classz00_7181 =
																												BGl_varcz00zzcgen_copz00;
																											BgL_arg2543z00_7180 =
																												BGL_CLASS_NUM
																												(BgL_classz00_7181);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1414z00_7179),
																											BgL_arg2543z00_7180);
																									}
																									BgL_new1415z00_7178 =
																										BgL_new1414z00_7179;
																								}
																								((((BgL_copz00_bglt) COBJECT(
																												((BgL_copz00_bglt)
																													BgL_new1415z00_7178)))->
																										BgL_locz00) =
																									((obj_t) (((BgL_nodez00_bglt)
																												COBJECT((
																														(BgL_nodez00_bglt) (
																															(BgL_jumpzd2exzd2itz00_bglt)
																															BgL_nodez00_6637))))->
																											BgL_locz00)), BUNSPEC);
																								((((BgL_copz00_bglt)
																											COBJECT(((BgL_copz00_bglt)
																													BgL_new1415z00_7178)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt) ((
																												(BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														BgL_vauxz00_7041)))->
																											BgL_typez00)), BUNSPEC);
																								((((BgL_varcz00_bglt)
																											COBJECT
																											(BgL_new1415z00_7178))->
																										BgL_variablez00) =
																									((BgL_variablez00_bglt) (
																											(BgL_variablez00_bglt)
																											BgL_vauxz00_7041)),
																									BUNSPEC);
																								BgL_auxz00_10117 =
																									((BgL_copz00_bglt)
																									BgL_new1415z00_7178);
																							}
																							((((BgL_cjumpzd2exzd2itz00_bglt)
																										COBJECT
																										(BgL_new1413z00_7174))->
																									BgL_valuez00) =
																								((BgL_copz00_bglt)
																									BgL_auxz00_10117), BUNSPEC);
																						}
																						BgL_arg2542z00_7173 =
																							BgL_new1413z00_7174;
																					}
																					BgL_arg2528z00_7162 =
																						BgL_arg2542z00_7173;
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_list2529z00_7182;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg2534z00_7183;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg2536z00_7184;

																							BgL_arg2536z00_7184 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_arg2528z00_7162),
																								BNIL);
																							BgL_arg2534z00_7183 =
																								MAKE_YOUNG_PAIR(((obj_t)
																									BgL_arg2527z00_7161),
																								BgL_arg2536z00_7184);
																						}
																						BgL_list2529z00_7182 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_arg2526z00_7160),
																							BgL_arg2534z00_7183);
																					}
																					BgL_auxz00_10066 =
																						BgL_list2529z00_7182;
																			}}
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1407z00_7156))->
																					BgL_copsz00) =
																				((obj_t) BgL_auxz00_10066), BUNSPEC);
																		}
																		BgL_auxz00_10050 =
																			((BgL_copz00_bglt) BgL_new1407z00_7156);
																	}
																	((((BgL_cblockz00_bglt)
																				COBJECT(BgL_new1405z00_7153))->
																			BgL_bodyz00) =
																		((BgL_copz00_bglt) BgL_auxz00_10050),
																		BUNSPEC);
																}
																return ((BgL_copz00_bglt) BgL_new1405z00_7153);
															}
														else
															{	/* Tools/trace.sch 53 */
																BgL_cblockz00_bglt BgL_new1417z00_7185;

																{	/* Tools/trace.sch 53 */
																	BgL_cblockz00_bglt BgL_new1416z00_7186;

																	BgL_new1416z00_7186 =
																		((BgL_cblockz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_cblockz00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg2572z00_7187;

																		BgL_arg2572z00_7187 =
																			BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1416z00_7186),
																			BgL_arg2572z00_7187);
																	}
																	BgL_new1417z00_7185 = BgL_new1416z00_7186;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1417z00_7185)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_jumpzd2exzd2itz00_bglt)
																							BgL_nodez00_6637))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1417z00_7185)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_jumpzd2exzd2itz00_bglt)
																							BgL_nodez00_6637))))->
																			BgL_typez00)), BUNSPEC);
																{
																	BgL_copz00_bglt BgL_auxz00_10159;

																	{	/* Tools/trace.sch 53 */
																		BgL_csequencez00_bglt BgL_new1419z00_7188;

																		{	/* Tools/trace.sch 53 */
																			BgL_csequencez00_bglt BgL_new1418z00_7189;

																			BgL_new1418z00_7189 =
																				((BgL_csequencez00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_csequencez00_bgl))));
																			{	/* Tools/trace.sch 53 */
																				long BgL_arg2568z00_7190;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_classz00_7191;

																					BgL_classz00_7191 =
																						BGl_csequencez00zzcgen_copz00;
																					BgL_arg2568z00_7190 =
																						BGL_CLASS_NUM(BgL_classz00_7191);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1418z00_7189),
																					BgL_arg2568z00_7190);
																			}
																			BgL_new1419z00_7188 = BgL_new1418z00_7189;
																		}
																		((((BgL_copz00_bglt) COBJECT(
																						((BgL_copz00_bglt)
																							BgL_new1419z00_7188)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_jumpzd2exzd2itz00_bglt)
																									BgL_nodez00_6637))))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_copz00_bglt)
																					COBJECT(((BgL_copz00_bglt)
																							BgL_new1419z00_7188)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_jumpzd2exzd2itz00_bglt)
																									BgL_nodez00_6637))))->
																					BgL_typez00)), BUNSPEC);
																		((((BgL_csequencez00_bglt)
																					COBJECT(BgL_new1419z00_7188))->
																				BgL_czd2expzf3z21) =
																			((bool_t) ((bool_t) 0)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_10175;

																			{	/* Tools/trace.sch 53 */
																				BgL_localzd2varzd2_bglt
																					BgL_arg2549z00_7192;
																				BgL_csequencez00_bglt
																					BgL_arg2551z00_7193;
																				BgL_cjumpzd2exzd2itz00_bglt
																					BgL_arg2552z00_7194;
																				{	/* Tools/trace.sch 53 */
																					BgL_localzd2varzd2_bglt
																						BgL_new1421z00_7195;
																					{	/* Tools/trace.sch 53 */
																						BgL_localzd2varzd2_bglt
																							BgL_new1420z00_7196;
																						BgL_new1420z00_7196 =
																							((BgL_localzd2varzd2_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_localzd2varzd2_bgl))));
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg2560z00_7197;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_classz00_7198;

																								BgL_classz00_7198 =
																									BGl_localzd2varzd2zzcgen_copz00;
																								BgL_arg2560z00_7197 =
																									BGL_CLASS_NUM
																									(BgL_classz00_7198);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1420z00_7196),
																								BgL_arg2560z00_7197);
																						}
																						BgL_new1421z00_7195 =
																							BgL_new1420z00_7196;
																					}
																					((((BgL_copz00_bglt) COBJECT(
																									((BgL_copz00_bglt)
																										BgL_new1421z00_7195)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt) (
																												(BgL_jumpzd2exzd2itz00_bglt)
																												BgL_nodez00_6637))))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_copz00_bglt)
																								COBJECT(((BgL_copz00_bglt)
																										BgL_new1421z00_7195)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2objza2z00zztype_cachez00)),
																						BUNSPEC);
																					{
																						obj_t BgL_auxz00_10188;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_list2556z00_7199;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg2557z00_7200;

																								BgL_arg2557z00_7200 =
																									MAKE_YOUNG_PAIR(
																									((obj_t) BgL_vauxz00_7041),
																									BNIL);
																								BgL_list2556z00_7199 =
																									MAKE_YOUNG_PAIR(((obj_t)
																										BgL_eauxz00_7050),
																									BgL_arg2557z00_7200);
																							}
																							BgL_auxz00_10188 =
																								BgL_list2556z00_7199;
																						}
																						((((BgL_localzd2varzd2_bglt)
																									COBJECT
																									(BgL_new1421z00_7195))->
																								BgL_varsz00) =
																							((obj_t) BgL_auxz00_10188),
																							BUNSPEC);
																					}
																					BgL_arg2549z00_7192 =
																						BgL_new1421z00_7195;
																				}
																				{	/* Tools/trace.sch 53 */
																					BgL_csequencez00_bglt
																						BgL_new1423z00_7201;
																					{	/* Tools/trace.sch 53 */
																						BgL_csequencez00_bglt
																							BgL_new1422z00_7202;
																						BgL_new1422z00_7202 =
																							((BgL_csequencez00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_csequencez00_bgl))));
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg2563z00_7203;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_classz00_7204;

																								BgL_classz00_7204 =
																									BGl_csequencez00zzcgen_copz00;
																								BgL_arg2563z00_7203 =
																									BGL_CLASS_NUM
																									(BgL_classz00_7204);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1422z00_7202),
																								BgL_arg2563z00_7203);
																						}
																						BgL_new1423z00_7201 =
																							BgL_new1422z00_7202;
																					}
																					((((BgL_copz00_bglt) COBJECT(
																									((BgL_copz00_bglt)
																										BgL_new1423z00_7201)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt) (
																												(BgL_jumpzd2exzd2itz00_bglt)
																												BgL_nodez00_6637))))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_copz00_bglt)
																								COBJECT(((BgL_copz00_bglt)
																										BgL_new1423z00_7201)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2objza2z00zztype_cachez00)),
																						BUNSPEC);
																					((((BgL_csequencez00_bglt)
																								COBJECT(BgL_new1423z00_7201))->
																							BgL_czd2expzf3z21) =
																						((bool_t) ((bool_t) 0)), BUNSPEC);
																					{
																						obj_t BgL_auxz00_10207;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_list2561z00_7205;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg2562z00_7206;

																								BgL_arg2562z00_7206 =
																									MAKE_YOUNG_PAIR(
																									((obj_t) BgL_vcopz00_7046),
																									BNIL);
																								BgL_list2561z00_7205 =
																									MAKE_YOUNG_PAIR(((obj_t)
																										BgL_ecopz00_7055),
																									BgL_arg2562z00_7206);
																							}
																							BgL_auxz00_10207 =
																								BgL_list2561z00_7205;
																						}
																						((((BgL_csequencez00_bglt)
																									COBJECT
																									(BgL_new1423z00_7201))->
																								BgL_copsz00) =
																							((obj_t) BgL_auxz00_10207),
																							BUNSPEC);
																					}
																					BgL_arg2551z00_7193 =
																						BgL_new1423z00_7201;
																				}
																				{	/* Tools/trace.sch 53 */
																					BgL_cjumpzd2exzd2itz00_bglt
																						BgL_arg2564z00_7207;
																					{	/* Tools/trace.sch 53 */
																						BgL_cjumpzd2exzd2itz00_bglt
																							BgL_new1425z00_7208;
																						{	/* Tools/trace.sch 53 */
																							BgL_cjumpzd2exzd2itz00_bglt
																								BgL_new1424z00_7209;
																							BgL_new1424z00_7209 =
																								((BgL_cjumpzd2exzd2itz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_cjumpzd2exzd2itz00_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg2567z00_7210;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_7211;

																									BgL_classz00_7211 =
																										BGl_cjumpzd2exzd2itz00zzcgen_copz00;
																									BgL_arg2567z00_7210 =
																										BGL_CLASS_NUM
																										(BgL_classz00_7211);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1424z00_7209),
																									BgL_arg2567z00_7210);
																							}
																							BgL_new1425z00_7208 =
																								BgL_new1424z00_7209;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1425z00_7208)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6637))))->BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1425z00_7208)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) ((
																										(BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6637))))->BgL_typez00)), BUNSPEC);
																						{
																							BgL_copz00_bglt BgL_auxz00_10227;

																							{	/* Tools/trace.sch 53 */
																								BgL_varcz00_bglt
																									BgL_new1427z00_7212;
																								{	/* Tools/trace.sch 53 */
																									BgL_varcz00_bglt
																										BgL_new1426z00_7213;
																									BgL_new1426z00_7213 =
																										((BgL_varcz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_varcz00_bgl))));
																									{	/* Tools/trace.sch 53 */
																										long BgL_arg2565z00_7214;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_classz00_7215;

																											BgL_classz00_7215 =
																												BGl_varcz00zzcgen_copz00;
																											BgL_arg2565z00_7214 =
																												BGL_CLASS_NUM
																												(BgL_classz00_7215);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1426z00_7213),
																											BgL_arg2565z00_7214);
																									}
																									BgL_new1427z00_7212 =
																										BgL_new1426z00_7213;
																								}
																								((((BgL_copz00_bglt) COBJECT(
																												((BgL_copz00_bglt)
																													BgL_new1427z00_7212)))->
																										BgL_locz00) =
																									((obj_t) BFALSE), BUNSPEC);
																								((((BgL_copz00_bglt)
																											COBJECT(((BgL_copz00_bglt)
																													BgL_new1427z00_7212)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt) ((
																												(BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														BgL_eauxz00_7050)))->
																											BgL_typez00)), BUNSPEC);
																								((((BgL_varcz00_bglt)
																											COBJECT
																											(BgL_new1427z00_7212))->
																										BgL_variablez00) =
																									((BgL_variablez00_bglt) (
																											(BgL_variablez00_bglt)
																											BgL_eauxz00_7050)),
																									BUNSPEC);
																								BgL_auxz00_10227 =
																									((BgL_copz00_bglt)
																									BgL_new1427z00_7212);
																							}
																							((((BgL_cjumpzd2exzd2itz00_bglt)
																										COBJECT
																										(BgL_new1425z00_7208))->
																									BgL_exitz00) =
																								((BgL_copz00_bglt)
																									BgL_auxz00_10227), BUNSPEC);
																						}
																						{
																							BgL_copz00_bglt BgL_auxz00_10242;

																							{	/* Tools/trace.sch 53 */
																								BgL_varcz00_bglt
																									BgL_new1429z00_7216;
																								{	/* Tools/trace.sch 53 */
																									BgL_varcz00_bglt
																										BgL_new1428z00_7217;
																									BgL_new1428z00_7217 =
																										((BgL_varcz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_varcz00_bgl))));
																									{	/* Tools/trace.sch 53 */
																										long BgL_arg2566z00_7218;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_classz00_7219;

																											BgL_classz00_7219 =
																												BGl_varcz00zzcgen_copz00;
																											BgL_arg2566z00_7218 =
																												BGL_CLASS_NUM
																												(BgL_classz00_7219);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1428z00_7217),
																											BgL_arg2566z00_7218);
																									}
																									BgL_new1429z00_7216 =
																										BgL_new1428z00_7217;
																								}
																								((((BgL_copz00_bglt) COBJECT(
																												((BgL_copz00_bglt)
																													BgL_new1429z00_7216)))->
																										BgL_locz00) =
																									((obj_t) BFALSE), BUNSPEC);
																								((((BgL_copz00_bglt)
																											COBJECT(((BgL_copz00_bglt)
																													BgL_new1429z00_7216)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt) ((
																												(BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														BgL_vauxz00_7041)))->
																											BgL_typez00)), BUNSPEC);
																								((((BgL_varcz00_bglt)
																											COBJECT
																											(BgL_new1429z00_7216))->
																										BgL_variablez00) =
																									((BgL_variablez00_bglt) (
																											(BgL_variablez00_bglt)
																											BgL_vauxz00_7041)),
																									BUNSPEC);
																								BgL_auxz00_10242 =
																									((BgL_copz00_bglt)
																									BgL_new1429z00_7216);
																							}
																							((((BgL_cjumpzd2exzd2itz00_bglt)
																										COBJECT
																										(BgL_new1425z00_7208))->
																									BgL_valuez00) =
																								((BgL_copz00_bglt)
																									BgL_auxz00_10242), BUNSPEC);
																						}
																						BgL_arg2564z00_7207 =
																							BgL_new1425z00_7208;
																					}
																					BgL_arg2552z00_7194 =
																						BgL_arg2564z00_7207;
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_list2553z00_7220;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg2554z00_7221;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg2555z00_7222;

																							BgL_arg2555z00_7222 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_arg2552z00_7194),
																								BNIL);
																							BgL_arg2554z00_7221 =
																								MAKE_YOUNG_PAIR(((obj_t)
																									BgL_arg2551z00_7193),
																								BgL_arg2555z00_7222);
																						}
																						BgL_list2553z00_7220 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_arg2549z00_7192),
																							BgL_arg2554z00_7221);
																					}
																					BgL_auxz00_10175 =
																						BgL_list2553z00_7220;
																			}}
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1419z00_7188))->
																					BgL_copsz00) =
																				((obj_t) BgL_auxz00_10175), BUNSPEC);
																		}
																		BgL_auxz00_10159 =
																			((BgL_copz00_bglt) BgL_new1419z00_7188);
																	}
																	((((BgL_cblockz00_bglt)
																				COBJECT(BgL_new1417z00_7185))->
																			BgL_bodyz00) =
																		((BgL_copz00_bglt) BgL_auxz00_10159),
																		BUNSPEC);
																}
																return ((BgL_copz00_bglt) BgL_new1417z00_7185);
															}
													}
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &node->cop-set-ex-it1765 */
	BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2setzd2exzd2it1765z81zzcgen_cgenz00(obj_t
		BgL_envz00_6640, obj_t BgL_nodez00_6641, obj_t BgL_kontz00_6642,
		obj_t BgL_inpushexitz00_6643)
	{
		{	/* Cgen/cgen.scm 877 */
			{	/* Tools/trace.sch 53 */
				BgL_variablez00_bglt BgL_exitz00_7224;

				BgL_exitz00_7224 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6641)))->
								BgL_varz00)))->BgL_variablez00);
				BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_exitz00_7224);
				{	/* Tools/trace.sch 53 */
					BgL_csequencez00_bglt BgL_new1376z00_7225;

					{	/* Tools/trace.sch 53 */
						BgL_csequencez00_bglt BgL_new1375z00_7226;

						BgL_new1375z00_7226 =
							((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_csequencez00_bgl))));
						{	/* Tools/trace.sch 53 */
							long BgL_arg2482z00_7227;

							BgL_arg2482z00_7227 =
								BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1375z00_7226),
								BgL_arg2482z00_7227);
						}
						BgL_new1376z00_7225 = BgL_new1375z00_7226;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1376z00_7225)))->BgL_locz00) =
						((obj_t) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
												BgL_nodez00_6641))))->BgL_locz00)), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1376z00_7225)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
												BgL_nodez00_6641))))->BgL_typez00)), BUNSPEC);
					((((BgL_csequencez00_bglt) COBJECT(BgL_new1376z00_7225))->
							BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
					{
						obj_t BgL_auxz00_10286;

						{	/* Tools/trace.sch 53 */
							BgL_cpragmaz00_bglt BgL_arg2449z00_7228;
							BgL_localzd2varzd2_bglt BgL_arg2450z00_7229;
							BgL_csetzd2exzd2itz00_bglt BgL_arg2451z00_7230;

							{	/* Tools/trace.sch 53 */
								BgL_cpragmaz00_bglt BgL_new1378z00_7231;

								{	/* Tools/trace.sch 53 */
									BgL_cpragmaz00_bglt BgL_new1377z00_7232;

									BgL_new1377z00_7232 =
										((BgL_cpragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_cpragmaz00_bgl))));
									{	/* Tools/trace.sch 53 */
										long BgL_arg2456z00_7233;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_classz00_7234;

											BgL_classz00_7234 = BGl_cpragmaz00zzcgen_copz00;
											BgL_arg2456z00_7233 = BGL_CLASS_NUM(BgL_classz00_7234);
										}
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1377z00_7232),
											BgL_arg2456z00_7233);
									}
									BgL_new1378z00_7231 = BgL_new1377z00_7232;
								}
								((((BgL_copz00_bglt) COBJECT(
												((BgL_copz00_bglt) BgL_new1378z00_7231)))->BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
															BgL_nodez00_6641))))->BgL_locz00)), BUNSPEC);
								((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
													BgL_new1378z00_7231)))->BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt)
											BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
								((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1378z00_7231))->
										BgL_formatz00) =
									((obj_t) BGl_string2954z00zzcgen_cgenz00), BUNSPEC);
								((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1378z00_7231))->
										BgL_argsz00) = ((obj_t) BNIL), BUNSPEC);
								BgL_arg2449z00_7228 = BgL_new1378z00_7231;
							}
							{	/* Tools/trace.sch 53 */
								BgL_localzd2varzd2_bglt BgL_new1380z00_7235;

								{	/* Tools/trace.sch 53 */
									BgL_localzd2varzd2_bglt BgL_new1379z00_7236;

									BgL_new1379z00_7236 =
										((BgL_localzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_localzd2varzd2_bgl))));
									{	/* Tools/trace.sch 53 */
										long BgL_arg2460z00_7237;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_classz00_7238;

											BgL_classz00_7238 = BGl_localzd2varzd2zzcgen_copz00;
											BgL_arg2460z00_7237 = BGL_CLASS_NUM(BgL_classz00_7238);
										}
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1379z00_7236),
											BgL_arg2460z00_7237);
									}
									BgL_new1380z00_7235 = BgL_new1379z00_7236;
								}
								((((BgL_copz00_bglt) COBJECT(
												((BgL_copz00_bglt) BgL_new1380z00_7235)))->BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
															BgL_nodez00_6641))))->BgL_locz00)), BUNSPEC);
								((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
													BgL_new1380z00_7235)))->BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt)
											BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
								{
									obj_t BgL_auxz00_10313;

									{	/* Tools/trace.sch 53 */
										BgL_variablez00_bglt BgL_arg2457z00_7239;

										BgL_arg2457z00_7239 =
											(((BgL_varz00_bglt) COBJECT(
													(((BgL_setzd2exzd2itz00_bglt) COBJECT(
																((BgL_setzd2exzd2itz00_bglt)
																	BgL_nodez00_6641)))->BgL_varz00)))->
											BgL_variablez00);
										{	/* Tools/trace.sch 53 */
											obj_t BgL_list2458z00_7240;

											BgL_list2458z00_7240 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2457z00_7239), BNIL);
											BgL_auxz00_10313 = BgL_list2458z00_7240;
									}}
									((((BgL_localzd2varzd2_bglt) COBJECT(BgL_new1380z00_7235))->
											BgL_varsz00) = ((obj_t) BgL_auxz00_10313), BUNSPEC);
								}
								BgL_arg2450z00_7229 = BgL_new1380z00_7235;
							}
							{	/* Tools/trace.sch 53 */
								BgL_csetzd2exzd2itz00_bglt BgL_new1382z00_7241;

								{	/* Tools/trace.sch 53 */
									BgL_csetzd2exzd2itz00_bglt BgL_new1381z00_7242;

									BgL_new1381z00_7242 =
										((BgL_csetzd2exzd2itz00_bglt)
										BOBJECT(GC_MALLOC(sizeof(struct
													BgL_csetzd2exzd2itz00_bgl))));
									{	/* Tools/trace.sch 53 */
										long BgL_arg2481z00_7243;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_classz00_7244;

											BgL_classz00_7244 = BGl_csetzd2exzd2itz00zzcgen_copz00;
											BgL_arg2481z00_7243 = BGL_CLASS_NUM(BgL_classz00_7244);
										}
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1381z00_7242),
											BgL_arg2481z00_7243);
									}
									BgL_new1382z00_7241 = BgL_new1381z00_7242;
								}
								((((BgL_copz00_bglt) COBJECT(
												((BgL_copz00_bglt) BgL_new1382z00_7241)))->BgL_locz00) =
									((obj_t) (((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
															BgL_nodez00_6641))))->BgL_locz00)), BUNSPEC);
								((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
													BgL_new1382z00_7241)))->BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
															BgL_nodez00_6641))))->BgL_typez00)), BUNSPEC);
								{
									BgL_copz00_bglt BgL_auxz00_10334;

									{	/* Tools/trace.sch 53 */
										BgL_varcz00_bglt BgL_new1384z00_7245;

										{	/* Tools/trace.sch 53 */
											BgL_varcz00_bglt BgL_new1383z00_7246;

											BgL_new1383z00_7246 =
												((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_varcz00_bgl))));
											{	/* Tools/trace.sch 53 */
												long BgL_arg2461z00_7247;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_classz00_7248;

													BgL_classz00_7248 = BGl_varcz00zzcgen_copz00;
													BgL_arg2461z00_7247 =
														BGL_CLASS_NUM(BgL_classz00_7248);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1383z00_7246),
													BgL_arg2461z00_7247);
											}
											BgL_new1384z00_7245 = BgL_new1383z00_7246;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1384z00_7245)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) (
																	(BgL_setzd2exzd2itz00_bglt)
																	BgL_nodez00_6641))))->BgL_locz00)), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1384z00_7245)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_variablez00_bglt)
														COBJECT(BgL_exitz00_7224))->BgL_typez00)), BUNSPEC);
										((((BgL_varcz00_bglt) COBJECT(BgL_new1384z00_7245))->
												BgL_variablez00) =
											((BgL_variablez00_bglt) BgL_exitz00_7224), BUNSPEC);
										BgL_auxz00_10334 = ((BgL_copz00_bglt) BgL_new1384z00_7245);
									}
									((((BgL_csetzd2exzd2itz00_bglt)
												COBJECT(BgL_new1382z00_7241))->BgL_exitz00) =
										((BgL_copz00_bglt) BgL_auxz00_10334), BUNSPEC);
								}
								{
									BgL_copz00_bglt BgL_auxz00_10350;

									{	/* Tools/trace.sch 53 */
										BgL_nodez00_bglt BgL_arg2462z00_7249;

										BgL_arg2462z00_7249 =
											(((BgL_setzd2exzd2itz00_bglt) COBJECT(
													((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6641)))->
											BgL_onexitz00);
										BgL_auxz00_10350 =
											BGl_nodezd2ze3copz31zzcgen_cgenz00(BgL_arg2462z00_7249,
											BgL_kontz00_6642, CBOOL(BgL_inpushexitz00_6643));
									}
									((((BgL_csetzd2exzd2itz00_bglt)
												COBJECT(BgL_new1382z00_7241))->BgL_jumpzd2valuezd2) =
										((BgL_copz00_bglt) BgL_auxz00_10350), BUNSPEC);
								}
								{
									BgL_copz00_bglt BgL_auxz00_10356;

									{	/* Tools/trace.sch 53 */
										BgL_csequencez00_bglt BgL_new1386z00_7250;

										{	/* Tools/trace.sch 53 */
											BgL_csequencez00_bglt BgL_new1385z00_7251;

											BgL_new1385z00_7251 =
												((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_csequencez00_bgl))));
											{	/* Tools/trace.sch 53 */
												long BgL_arg2480z00_7252;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_classz00_7253;

													BgL_classz00_7253 = BGl_csequencez00zzcgen_copz00;
													BgL_arg2480z00_7252 =
														BGL_CLASS_NUM(BgL_classz00_7253);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1385z00_7251),
													BgL_arg2480z00_7252);
											}
											BgL_new1386z00_7250 = BgL_new1385z00_7251;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1386z00_7250)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) (
																	(BgL_setzd2exzd2itz00_bglt)
																	BgL_nodez00_6641))))->BgL_locz00)), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1386z00_7250)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_nodez00_bglt)
														COBJECT((((BgL_setzd2exzd2itz00_bglt)
																	COBJECT(((BgL_setzd2exzd2itz00_bglt)
																			BgL_nodez00_6641)))->BgL_bodyz00)))->
													BgL_typez00)), BUNSPEC);
										((((BgL_csequencez00_bglt) COBJECT(BgL_new1386z00_7250))->
												BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										{
											obj_t BgL_auxz00_10372;

											{	/* Tools/trace.sch 53 */
												BgL_copz00_bglt BgL_arg2465z00_7254;
												BgL_copz00_bglt BgL_arg2466z00_7255;

												{	/* Tools/trace.sch 53 */
													BgL_setqz00_bglt BgL_arg2470z00_7256;

													{	/* Tools/trace.sch 53 */
														BgL_pragmaz00_bglt BgL_arg2471z00_7257;

														{	/* Tools/trace.sch 53 */
															BgL_pragmaz00_bglt BgL_new1388z00_7258;

															{	/* Tools/trace.sch 53 */
																BgL_pragmaz00_bglt BgL_new1387z00_7259;

																BgL_new1387z00_7259 =
																	((BgL_pragmaz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_pragmaz00_bgl))));
																{	/* Tools/trace.sch 53 */
																	long BgL_arg2476z00_7260;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_classz00_7261;

																		BgL_classz00_7261 =
																			BGl_pragmaz00zzast_nodez00;
																		BgL_arg2476z00_7260 =
																			BGL_CLASS_NUM(BgL_classz00_7261);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1387z00_7259),
																		BgL_arg2476z00_7260);
																}
																{	/* Tools/trace.sch 53 */
																	BgL_objectz00_bglt BgL_tmpz00_10377;

																	BgL_tmpz00_10377 =
																		((BgL_objectz00_bglt) BgL_new1387z00_7259);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10377,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1387z00_7259);
																BgL_new1388z00_7258 = BgL_new1387z00_7259;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1388z00_7258)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_setzd2exzd2itz00_bglt)
																						BgL_nodez00_6641))))->BgL_locz00)),
																BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1388z00_7258)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1388z00_7258)))->
																	BgL_sidezd2effectzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1388z00_7258)))->BgL_keyz00) =
																((obj_t) BINT(-1L)), BUNSPEC);
															((((BgL_externz00_bglt)
																		COBJECT(((BgL_externz00_bglt)
																				BgL_new1388z00_7258)))->
																	BgL_exprza2za2) = ((obj_t) BNIL), BUNSPEC);
															((((BgL_externz00_bglt)
																		COBJECT(((BgL_externz00_bglt)
																				BgL_new1388z00_7258)))->BgL_effectz00) =
																((obj_t) BUNSPEC), BUNSPEC);
															{
																obj_t BgL_auxz00_10398;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg2473z00_7262;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg2474z00_7263;

																		BgL_arg2474z00_7263 =
																			(((BgL_typez00_bglt) COBJECT(
																					(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_localz00_bglt)
																										BgL_exitz00_7224))))->
																						BgL_typez00)))->BgL_namez00);
																		BgL_arg2473z00_7262 =
																			BGl_stringzd2sanszd2z42z42zztype_toolsz00
																			(BgL_arg2474z00_7263);
																	}
																	BgL_auxz00_10398 =
																		string_append_3
																		(BGl_string2912z00zzcgen_cgenz00,
																		BgL_arg2473z00_7262,
																		BGl_string2955z00zzcgen_cgenz00);
																}
																((((BgL_pragmaz00_bglt)
																			COBJECT(BgL_new1388z00_7258))->
																		BgL_formatz00) =
																	((obj_t) BgL_auxz00_10398), BUNSPEC);
															}
															((((BgL_pragmaz00_bglt)
																		COBJECT(BgL_new1388z00_7258))->
																	BgL_srfi0z00) =
																((obj_t) CNST_TABLE_REF(14)), BUNSPEC);
															BgL_arg2471z00_7257 = BgL_new1388z00_7258;
														}
														BgL_arg2470z00_7256 =
															BGl_nodezd2setqzd2zzcgen_cgenz00(BgL_exitz00_7224,
															((BgL_nodez00_bglt) BgL_arg2471z00_7257));
													}
													BgL_arg2465z00_7254 =
														BGl_nodezd2ze3copz31zzcgen_cgenz00(
														((BgL_nodez00_bglt) BgL_arg2470z00_7256),
														BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
														CBOOL(BgL_inpushexitz00_6643));
												}
												{	/* Tools/trace.sch 53 */
													BgL_nodez00_bglt BgL_arg2479z00_7264;

													BgL_arg2479z00_7264 =
														(((BgL_setzd2exzd2itz00_bglt) COBJECT(
																((BgL_setzd2exzd2itz00_bglt)
																	BgL_nodez00_6641)))->BgL_bodyz00);
													BgL_arg2466z00_7255 =
														BGl_nodezd2ze3copz31zzcgen_cgenz00
														(BgL_arg2479z00_7264, BgL_kontz00_6642,
														CBOOL(BgL_inpushexitz00_6643));
												}
												{	/* Tools/trace.sch 53 */
													obj_t BgL_list2467z00_7265;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg2469z00_7266;

														BgL_arg2469z00_7266 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2466z00_7255), BNIL);
														BgL_list2467z00_7265 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2465z00_7254),
															BgL_arg2469z00_7266);
													}
													BgL_auxz00_10372 = BgL_list2467z00_7265;
											}}
											((((BgL_csequencez00_bglt) COBJECT(BgL_new1386z00_7250))->
													BgL_copsz00) = ((obj_t) BgL_auxz00_10372), BUNSPEC);
										}
										BgL_auxz00_10356 = ((BgL_copz00_bglt) BgL_new1386z00_7250);
									}
									((((BgL_csetzd2exzd2itz00_bglt)
												COBJECT(BgL_new1382z00_7241))->BgL_bodyz00) =
										((BgL_copz00_bglt) BgL_auxz00_10356), BUNSPEC);
								}
								BgL_arg2451z00_7230 = BgL_new1382z00_7241;
							}
							{	/* Tools/trace.sch 53 */
								obj_t BgL_list2452z00_7267;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg2453z00_7268;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg2455z00_7269;

										BgL_arg2455z00_7269 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg2451z00_7230), BNIL);
										BgL_arg2453z00_7268 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_arg2450z00_7229), BgL_arg2455z00_7269);
									}
									BgL_list2452z00_7267 =
										MAKE_YOUNG_PAIR(
										((obj_t) BgL_arg2449z00_7228), BgL_arg2453z00_7268);
								}
								BgL_auxz00_10286 = BgL_list2452z00_7267;
						}}
						((((BgL_csequencez00_bglt) COBJECT(BgL_new1376z00_7225))->
								BgL_copsz00) = ((obj_t) BgL_auxz00_10286), BUNSPEC);
					}
					return ((BgL_copz00_bglt) BgL_new1376z00_7225);
				}
			}
		}

	}



/* &node->cop-let-var1763 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2letzd2var1763z53zzcgen_cgenz00(obj_t
		BgL_envz00_6644, obj_t BgL_nodez00_6645, obj_t BgL_kontz00_6646,
		obj_t BgL_inpushexitz00_6647)
	{
		{	/* Cgen/cgen.scm 728 */
			{	/* Cgen/cgen.scm 840 */
				obj_t BgL_g1706z00_7271;

				BgL_g1706z00_7271 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_6645)))->BgL_bindingsz00);
				{
					obj_t BgL_l1704z00_7273;

					BgL_l1704z00_7273 = BgL_g1706z00_7271;
				BgL_zc3z04anonymousza32298ze3z87_7272:
					if (PAIRP(BgL_l1704z00_7273))
						{	/* Cgen/cgen.scm 840 */
							{	/* Cgen/cgen.scm 840 */
								obj_t BgL_xz00_7274;

								BgL_xz00_7274 = CAR(BgL_l1704z00_7273);
								{	/* Cgen/cgen.scm 840 */
									obj_t BgL_arg2301z00_7275;

									BgL_arg2301z00_7275 = CAR(((obj_t) BgL_xz00_7274));
									BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
										((BgL_variablez00_bglt) BgL_arg2301z00_7275));
								}
							}
							{
								obj_t BgL_l1704z00_10441;

								BgL_l1704z00_10441 = CDR(BgL_l1704z00_7273);
								BgL_l1704z00_7273 = BgL_l1704z00_10441;
								goto BgL_zc3z04anonymousza32298ze3z87_7272;
							}
						}
					else
						{	/* Cgen/cgen.scm 840 */
							((bool_t) 1);
						}
				}
			}
			{	/* Cgen/cgen.scm 841 */
				obj_t BgL_allocaz00_7276;
				BgL_localzd2varzd2_bglt BgL_declsz00_7277;
				obj_t BgL_setsz00_7278;
				BgL_stopz00_bglt BgL_bodyz00_7279;

				BgL_allocaz00_7276 =
					BGl_zc3z04anonymousza32313ze3ze70z60zzcgen_cgenz00
					(BgL_inpushexitz00_6647,
					(((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
									BgL_nodez00_6645)))->BgL_bindingsz00));
				{	/* Cgen/cgen.scm 842 */
					BgL_localzd2varzd2_bglt BgL_new1367z00_7280;

					{	/* Cgen/cgen.scm 844 */
						BgL_localzd2varzd2_bglt BgL_new1366z00_7281;

						BgL_new1366z00_7281 =
							((BgL_localzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzd2varzd2_bgl))));
						{	/* Cgen/cgen.scm 844 */
							long BgL_arg2328z00_7282;

							BgL_arg2328z00_7282 =
								BGL_CLASS_NUM(BGl_localzd2varzd2zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1366z00_7281),
								BgL_arg2328z00_7282);
						}
						BgL_new1367z00_7280 = BgL_new1366z00_7281;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1367z00_7280)))->BgL_locz00) =
						((obj_t) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
												BgL_nodez00_6645))))->BgL_locz00)), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1367z00_7280)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
					{
						obj_t BgL_auxz00_10458;

						{	/* Cgen/cgen.scm 845 */
							obj_t BgL_l1710z00_7283;

							BgL_l1710z00_7283 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_6645)))->
								BgL_bindingsz00);
							if (NULLP(BgL_l1710z00_7283))
								{	/* Cgen/cgen.scm 845 */
									BgL_auxz00_10458 = BNIL;
								}
							else
								{	/* Cgen/cgen.scm 845 */
									obj_t BgL_head1712z00_7284;

									{	/* Cgen/cgen.scm 845 */
										obj_t BgL_arg2326z00_7285;

										{	/* Cgen/cgen.scm 845 */
											obj_t BgL_pairz00_7286;

											BgL_pairz00_7286 = CAR(((obj_t) BgL_l1710z00_7283));
											BgL_arg2326z00_7285 = CAR(BgL_pairz00_7286);
										}
										BgL_head1712z00_7284 =
											MAKE_YOUNG_PAIR(BgL_arg2326z00_7285, BNIL);
									}
									{	/* Cgen/cgen.scm 845 */
										obj_t BgL_g1715z00_7287;

										BgL_g1715z00_7287 = CDR(((obj_t) BgL_l1710z00_7283));
										{
											obj_t BgL_l1710z00_7289;
											obj_t BgL_tail1713z00_7290;

											BgL_l1710z00_7289 = BgL_g1715z00_7287;
											BgL_tail1713z00_7290 = BgL_head1712z00_7284;
										BgL_zc3z04anonymousza32320ze3z87_7288:
											if (NULLP(BgL_l1710z00_7289))
												{	/* Cgen/cgen.scm 845 */
													BgL_auxz00_10458 = BgL_head1712z00_7284;
												}
											else
												{	/* Cgen/cgen.scm 845 */
													obj_t BgL_newtail1714z00_7291;

													{	/* Cgen/cgen.scm 845 */
														obj_t BgL_arg2324z00_7292;

														{	/* Cgen/cgen.scm 845 */
															obj_t BgL_pairz00_7293;

															BgL_pairz00_7293 =
																CAR(((obj_t) BgL_l1710z00_7289));
															BgL_arg2324z00_7292 = CAR(BgL_pairz00_7293);
														}
														BgL_newtail1714z00_7291 =
															MAKE_YOUNG_PAIR(BgL_arg2324z00_7292, BNIL);
													}
													SET_CDR(BgL_tail1713z00_7290,
														BgL_newtail1714z00_7291);
													{	/* Cgen/cgen.scm 845 */
														obj_t BgL_arg2323z00_7294;

														BgL_arg2323z00_7294 =
															CDR(((obj_t) BgL_l1710z00_7289));
														{
															obj_t BgL_tail1713z00_10479;
															obj_t BgL_l1710z00_10478;

															BgL_l1710z00_10478 = BgL_arg2323z00_7294;
															BgL_tail1713z00_10479 = BgL_newtail1714z00_7291;
															BgL_tail1713z00_7290 = BgL_tail1713z00_10479;
															BgL_l1710z00_7289 = BgL_l1710z00_10478;
															goto BgL_zc3z04anonymousza32320ze3z87_7288;
														}
													}
												}
										}
									}
								}
						}
						((((BgL_localzd2varzd2_bglt) COBJECT(BgL_new1367z00_7280))->
								BgL_varsz00) = ((obj_t) BgL_auxz00_10458), BUNSPEC);
					}
					BgL_declsz00_7277 = BgL_new1367z00_7280;
				}
				{	/* Cgen/cgen.scm 846 */
					obj_t BgL_l1716z00_7295;

					BgL_l1716z00_7295 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_6645)))->BgL_bindingsz00);
					if (NULLP(BgL_l1716z00_7295))
						{	/* Cgen/cgen.scm 846 */
							BgL_setsz00_7278 = BNIL;
						}
					else
						{	/* Cgen/cgen.scm 846 */
							obj_t BgL_head1718z00_7296;

							BgL_head1718z00_7296 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1716z00_7298;
								obj_t BgL_tail1719z00_7299;

								BgL_l1716z00_7298 = BgL_l1716z00_7295;
								BgL_tail1719z00_7299 = BgL_head1718z00_7296;
							BgL_zc3z04anonymousza32330ze3z87_7297:
								if (NULLP(BgL_l1716z00_7298))
									{	/* Cgen/cgen.scm 846 */
										BgL_setsz00_7278 = CDR(BgL_head1718z00_7296);
									}
								else
									{	/* Cgen/cgen.scm 846 */
										obj_t BgL_newtail1720z00_7300;

										{	/* Cgen/cgen.scm 846 */
											BgL_copz00_bglt BgL_arg2335z00_7301;

											{	/* Cgen/cgen.scm 846 */
												obj_t BgL_xz00_7302;

												BgL_xz00_7302 = CAR(((obj_t) BgL_l1716z00_7298));
												{	/* Cgen/cgen.scm 847 */
													BgL_setqz00_bglt BgL_arg2336z00_7303;

													{	/* Cgen/cgen.scm 847 */
														obj_t BgL_arg2337z00_7304;
														obj_t BgL_arg2338z00_7305;

														BgL_arg2337z00_7304 = CAR(((obj_t) BgL_xz00_7302));
														BgL_arg2338z00_7305 = CDR(((obj_t) BgL_xz00_7302));
														BgL_arg2336z00_7303 =
															BGl_nodezd2setqzd2zzcgen_cgenz00(
															((BgL_variablez00_bglt) BgL_arg2337z00_7304),
															((BgL_nodez00_bglt) BgL_arg2338z00_7305));
													}
													BgL_arg2335z00_7301 =
														BGl_nodezd2ze3copz31zzcgen_cgenz00(
														((BgL_nodez00_bglt) BgL_arg2336z00_7303),
														BGl_za2stopzd2kontza2zd2zzcgen_cgenz00,
														CBOOL(BgL_inpushexitz00_6647));
												}
											}
											BgL_newtail1720z00_7300 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2335z00_7301), BNIL);
										}
										SET_CDR(BgL_tail1719z00_7299, BgL_newtail1720z00_7300);
										{	/* Cgen/cgen.scm 846 */
											obj_t BgL_arg2333z00_7306;

											BgL_arg2333z00_7306 = CDR(((obj_t) BgL_l1716z00_7298));
											{
												obj_t BgL_tail1719z00_10507;
												obj_t BgL_l1716z00_10506;

												BgL_l1716z00_10506 = BgL_arg2333z00_7306;
												BgL_tail1719z00_10507 = BgL_newtail1720z00_7300;
												BgL_tail1719z00_7299 = BgL_tail1719z00_10507;
												BgL_l1716z00_7298 = BgL_l1716z00_10506;
												goto BgL_zc3z04anonymousza32330ze3z87_7297;
											}
										}
									}
							}
						}
				}
				{	/* Cgen/cgen.scm 850 */
					BgL_copz00_bglt BgL_copz00_7307;

					BgL_copz00_7307 =
						BGl_nodezd2ze3copz31zzcgen_cgenz00(
						(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_6645)))->BgL_bodyz00),
						BgL_kontz00_6646, CBOOL(BgL_inpushexitz00_6647));
					{	/* Cgen/cgen.scm 851 */
						BgL_stopz00_bglt BgL_new1369z00_7308;

						{	/* Cgen/cgen.scm 851 */
							BgL_stopz00_bglt BgL_new1368z00_7309;

							BgL_new1368z00_7309 =
								((BgL_stopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_stopz00_bgl))));
							{	/* Cgen/cgen.scm 851 */
								long BgL_arg2339z00_7310;

								BgL_arg2339z00_7310 = BGL_CLASS_NUM(BGl_stopz00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1368z00_7309),
									BgL_arg2339z00_7310);
							}
							BgL_new1369z00_7308 = BgL_new1368z00_7309;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1369z00_7308)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1369z00_7308)))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
						((((BgL_stopz00_bglt) COBJECT(BgL_new1369z00_7308))->BgL_valuez00) =
							((BgL_copz00_bglt) BgL_copz00_7307), BUNSPEC);
						BgL_bodyz00_7279 = BgL_new1369z00_7308;
				}}
				{	/* Cgen/cgen.scm 856 */
					BgL_copz00_bglt BgL_arg2304z00_7311;
					obj_t BgL_arg2305z00_7312;

					{	/* Cgen/cgen.scm 856 */
						BgL_csequencez00_bglt BgL_arg2306z00_7313;
						obj_t BgL_arg2307z00_7314;

						{	/* Cgen/cgen.scm 856 */
							BgL_csequencez00_bglt BgL_new1371z00_7315;

							{	/* Cgen/cgen.scm 858 */
								BgL_csequencez00_bglt BgL_new1370z00_7316;

								BgL_new1370z00_7316 =
									((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_csequencez00_bgl))));
								{	/* Cgen/cgen.scm 858 */
									long BgL_arg2312z00_7317;

									BgL_arg2312z00_7317 =
										BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1370z00_7316),
										BgL_arg2312z00_7317);
								}
								BgL_new1371z00_7315 = BgL_new1370z00_7316;
							}
							((((BgL_copz00_bglt) COBJECT(
											((BgL_copz00_bglt) BgL_new1371z00_7315)))->BgL_locz00) =
								((obj_t) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
														BgL_nodez00_6645))))->BgL_locz00)), BUNSPEC);
							((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
												BgL_new1371z00_7315)))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_copz00_bglt)
											COBJECT(((BgL_copz00_bglt) BgL_bodyz00_7279)))->
										BgL_typez00)), BUNSPEC);
							((((BgL_csequencez00_bglt) COBJECT(BgL_new1371z00_7315))->
									BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							{
								obj_t BgL_auxz00_10536;

								{	/* Cgen/cgen.scm 859 */
									obj_t BgL_arg2308z00_7318;

									{	/* Cgen/cgen.scm 859 */
										obj_t BgL_arg2309z00_7319;

										{	/* Cgen/cgen.scm 859 */
											obj_t BgL_arg2310z00_7320;

											{	/* Cgen/cgen.scm 859 */
												obj_t BgL_list2311z00_7321;

												BgL_list2311z00_7321 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_bodyz00_7279), BNIL);
												BgL_arg2310z00_7320 = BgL_list2311z00_7321;
											}
											BgL_arg2309z00_7319 =
												BGl_appendzd221011zd2zzcgen_cgenz00(BgL_setsz00_7278,
												BgL_arg2310z00_7320);
										}
										BgL_arg2308z00_7318 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_declsz00_7277), BgL_arg2309z00_7319);
									}
									BgL_auxz00_10536 =
										BGl_appendzd221011zd2zzcgen_cgenz00(BgL_allocaz00_7276,
										BgL_arg2308z00_7318);
								}
								((((BgL_csequencez00_bglt) COBJECT(BgL_new1371z00_7315))->
										BgL_copsz00) = ((obj_t) BgL_auxz00_10536), BUNSPEC);
							}
							BgL_arg2306z00_7313 = BgL_new1371z00_7315;
						}
						BgL_arg2307z00_7314 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_nodez00_6645))))->BgL_locz00);
						BgL_arg2304z00_7311 =
							BGl_bdbzd2letzd2varz00zzcgen_cgenz00(
							((BgL_copz00_bglt) BgL_arg2306z00_7313), BgL_arg2307z00_7314);
					}
					BgL_arg2305z00_7312 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_6645))))->BgL_locz00);
					return
						((BgL_copz00_bglt)
						BGl_blockzd2kontzd2zzcgen_cgenz00(
							((obj_t) BgL_arg2304z00_7311), BgL_arg2305z00_7312));
				}
			}
		}

	}



/* stackable?~0 */
	bool_t BGl_stackablezf3ze70z14zzcgen_cgenz00(BgL_appz00_bglt BgL_nodez00_3113)
	{
		{	/* Cgen/cgen.scm 738 */
			if (CBOOL(
					(((BgL_appz00_bglt) COBJECT(BgL_nodez00_3113))->BgL_stackablez00)))
				{	/* Cgen/cgen.scm 736 */
					BgL_variablez00_bglt BgL_vz00_3117;

					BgL_vz00_3117 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(BgL_nodez00_3113))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Cgen/cgen.scm 737 */
						bool_t BgL_test3237z00_10560;

						{	/* Cgen/cgen.scm 737 */
							obj_t BgL_classz00_5228;

							BgL_classz00_5228 = BGl_globalz00zzast_varz00;
							{	/* Cgen/cgen.scm 737 */
								BgL_objectz00_bglt BgL_arg1807z00_5230;

								{	/* Cgen/cgen.scm 737 */
									obj_t BgL_tmpz00_10561;

									BgL_tmpz00_10561 =
										((obj_t) ((BgL_objectz00_bglt) BgL_vz00_3117));
									BgL_arg1807z00_5230 = (BgL_objectz00_bglt) (BgL_tmpz00_10561);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cgen/cgen.scm 737 */
										long BgL_idxz00_5236;

										BgL_idxz00_5236 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5230);
										BgL_test3237z00_10560 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5236 + 2L)) == BgL_classz00_5228);
									}
								else
									{	/* Cgen/cgen.scm 737 */
										bool_t BgL_res2848z00_5261;

										{	/* Cgen/cgen.scm 737 */
											obj_t BgL_oclassz00_5244;

											{	/* Cgen/cgen.scm 737 */
												obj_t BgL_arg1815z00_5252;
												long BgL_arg1816z00_5253;

												BgL_arg1815z00_5252 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cgen/cgen.scm 737 */
													long BgL_arg1817z00_5254;

													BgL_arg1817z00_5254 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5230);
													BgL_arg1816z00_5253 =
														(BgL_arg1817z00_5254 - OBJECT_TYPE);
												}
												BgL_oclassz00_5244 =
													VECTOR_REF(BgL_arg1815z00_5252, BgL_arg1816z00_5253);
											}
											{	/* Cgen/cgen.scm 737 */
												bool_t BgL__ortest_1115z00_5245;

												BgL__ortest_1115z00_5245 =
													(BgL_classz00_5228 == BgL_oclassz00_5244);
												if (BgL__ortest_1115z00_5245)
													{	/* Cgen/cgen.scm 737 */
														BgL_res2848z00_5261 = BgL__ortest_1115z00_5245;
													}
												else
													{	/* Cgen/cgen.scm 737 */
														long BgL_odepthz00_5246;

														{	/* Cgen/cgen.scm 737 */
															obj_t BgL_arg1804z00_5247;

															BgL_arg1804z00_5247 = (BgL_oclassz00_5244);
															BgL_odepthz00_5246 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5247);
														}
														if ((2L < BgL_odepthz00_5246))
															{	/* Cgen/cgen.scm 737 */
																obj_t BgL_arg1802z00_5249;

																{	/* Cgen/cgen.scm 737 */
																	obj_t BgL_arg1803z00_5250;

																	BgL_arg1803z00_5250 = (BgL_oclassz00_5244);
																	BgL_arg1802z00_5249 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5250,
																		2L);
																}
																BgL_res2848z00_5261 =
																	(BgL_arg1802z00_5249 == BgL_classz00_5228);
															}
														else
															{	/* Cgen/cgen.scm 737 */
																BgL_res2848z00_5261 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3237z00_10560 = BgL_res2848z00_5261;
									}
							}
						}
						if (BgL_test3237z00_10560)
							{	/* Cgen/cgen.scm 738 */
								obj_t BgL_tmpz00_10584;

								BgL_tmpz00_10584 =
									(((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_vz00_3117))))->
													BgL_valuez00))))->BgL_stackzd2allocatorzd2);
								return PAIRP(BgL_tmpz00_10584);
							}
						else
							{	/* Cgen/cgen.scm 737 */
								return ((bool_t) 0);
							}
					}
				}
			else
				{	/* Cgen/cgen.scm 735 */
					return ((bool_t) 0);
				}
		}

	}



/* alloca-let-var~0 */
	bool_t BGl_allocazd2letzd2varze70ze7zzcgen_cgenz00(BgL_nodez00_bglt
		BgL_nz00_3122)
	{
		{	/* Cgen/cgen.scm 743 */
			{	/* Cgen/cgen.scm 741 */
				bool_t BgL_test3241z00_10591;

				{	/* Cgen/cgen.scm 741 */
					obj_t BgL_classz00_5264;

					BgL_classz00_5264 = BGl_letzd2varzd2zzast_nodez00;
					{	/* Cgen/cgen.scm 741 */
						BgL_objectz00_bglt BgL_arg1807z00_5266;

						{	/* Cgen/cgen.scm 741 */
							obj_t BgL_tmpz00_10592;

							BgL_tmpz00_10592 = ((obj_t) BgL_nz00_3122);
							BgL_arg1807z00_5266 = (BgL_objectz00_bglt) (BgL_tmpz00_10592);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Cgen/cgen.scm 741 */
								long BgL_idxz00_5272;

								BgL_idxz00_5272 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5266);
								BgL_test3241z00_10591 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5272 + 3L)) == BgL_classz00_5264);
							}
						else
							{	/* Cgen/cgen.scm 741 */
								bool_t BgL_res2849z00_5297;

								{	/* Cgen/cgen.scm 741 */
									obj_t BgL_oclassz00_5280;

									{	/* Cgen/cgen.scm 741 */
										obj_t BgL_arg1815z00_5288;
										long BgL_arg1816z00_5289;

										BgL_arg1815z00_5288 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Cgen/cgen.scm 741 */
											long BgL_arg1817z00_5290;

											BgL_arg1817z00_5290 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5266);
											BgL_arg1816z00_5289 = (BgL_arg1817z00_5290 - OBJECT_TYPE);
										}
										BgL_oclassz00_5280 =
											VECTOR_REF(BgL_arg1815z00_5288, BgL_arg1816z00_5289);
									}
									{	/* Cgen/cgen.scm 741 */
										bool_t BgL__ortest_1115z00_5281;

										BgL__ortest_1115z00_5281 =
											(BgL_classz00_5264 == BgL_oclassz00_5280);
										if (BgL__ortest_1115z00_5281)
											{	/* Cgen/cgen.scm 741 */
												BgL_res2849z00_5297 = BgL__ortest_1115z00_5281;
											}
										else
											{	/* Cgen/cgen.scm 741 */
												long BgL_odepthz00_5282;

												{	/* Cgen/cgen.scm 741 */
													obj_t BgL_arg1804z00_5283;

													BgL_arg1804z00_5283 = (BgL_oclassz00_5280);
													BgL_odepthz00_5282 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5283);
												}
												if ((3L < BgL_odepthz00_5282))
													{	/* Cgen/cgen.scm 741 */
														obj_t BgL_arg1802z00_5285;

														{	/* Cgen/cgen.scm 741 */
															obj_t BgL_arg1803z00_5286;

															BgL_arg1803z00_5286 = (BgL_oclassz00_5280);
															BgL_arg1802z00_5285 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5286,
																3L);
														}
														BgL_res2849z00_5297 =
															(BgL_arg1802z00_5285 == BgL_classz00_5264);
													}
												else
													{	/* Cgen/cgen.scm 741 */
														BgL_res2849z00_5297 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test3241z00_10591 = BgL_res2849z00_5297;
							}
					}
				}
				if (BgL_test3241z00_10591)
					{	/* Cgen/cgen.scm 742 */
						bool_t BgL_test3245z00_10614;

						{	/* Cgen/cgen.scm 742 */
							BgL_nodez00_bglt BgL_arg2351z00_3127;

							BgL_arg2351z00_3127 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nz00_3122)))->BgL_bodyz00);
							{	/* Cgen/cgen.scm 742 */
								obj_t BgL_classz00_5299;

								BgL_classz00_5299 = BGl_appz00zzast_nodez00;
								{	/* Cgen/cgen.scm 742 */
									BgL_objectz00_bglt BgL_arg1807z00_5301;

									{	/* Cgen/cgen.scm 742 */
										obj_t BgL_tmpz00_10617;

										BgL_tmpz00_10617 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2351z00_3127));
										BgL_arg1807z00_5301 =
											(BgL_objectz00_bglt) (BgL_tmpz00_10617);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cgen/cgen.scm 742 */
											long BgL_idxz00_5307;

											BgL_idxz00_5307 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5301);
											BgL_test3245z00_10614 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5307 + 3L)) == BgL_classz00_5299);
										}
									else
										{	/* Cgen/cgen.scm 742 */
											bool_t BgL_res2850z00_5332;

											{	/* Cgen/cgen.scm 742 */
												obj_t BgL_oclassz00_5315;

												{	/* Cgen/cgen.scm 742 */
													obj_t BgL_arg1815z00_5323;
													long BgL_arg1816z00_5324;

													BgL_arg1815z00_5323 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cgen/cgen.scm 742 */
														long BgL_arg1817z00_5325;

														BgL_arg1817z00_5325 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5301);
														BgL_arg1816z00_5324 =
															(BgL_arg1817z00_5325 - OBJECT_TYPE);
													}
													BgL_oclassz00_5315 =
														VECTOR_REF(BgL_arg1815z00_5323,
														BgL_arg1816z00_5324);
												}
												{	/* Cgen/cgen.scm 742 */
													bool_t BgL__ortest_1115z00_5316;

													BgL__ortest_1115z00_5316 =
														(BgL_classz00_5299 == BgL_oclassz00_5315);
													if (BgL__ortest_1115z00_5316)
														{	/* Cgen/cgen.scm 742 */
															BgL_res2850z00_5332 = BgL__ortest_1115z00_5316;
														}
													else
														{	/* Cgen/cgen.scm 742 */
															long BgL_odepthz00_5317;

															{	/* Cgen/cgen.scm 742 */
																obj_t BgL_arg1804z00_5318;

																BgL_arg1804z00_5318 = (BgL_oclassz00_5315);
																BgL_odepthz00_5317 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5318);
															}
															if ((3L < BgL_odepthz00_5317))
																{	/* Cgen/cgen.scm 742 */
																	obj_t BgL_arg1802z00_5320;

																	{	/* Cgen/cgen.scm 742 */
																		obj_t BgL_arg1803z00_5321;

																		BgL_arg1803z00_5321 = (BgL_oclassz00_5315);
																		BgL_arg1802z00_5320 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5321, 3L);
																	}
																	BgL_res2850z00_5332 =
																		(BgL_arg1802z00_5320 == BgL_classz00_5299);
																}
															else
																{	/* Cgen/cgen.scm 742 */
																	BgL_res2850z00_5332 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3245z00_10614 = BgL_res2850z00_5332;
										}
								}
							}
						}
						if (BgL_test3245z00_10614)
							{	/* Cgen/cgen.scm 743 */
								BgL_nodez00_bglt BgL_arg2350z00_3126;

								BgL_arg2350z00_3126 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nz00_3122)))->BgL_bodyz00);
								return
									BGl_stackablezf3ze70z14zzcgen_cgenz00(
									((BgL_appz00_bglt) BgL_arg2350z00_3126));
							}
						else
							{	/* Cgen/cgen.scm 742 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Cgen/cgen.scm 741 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* <@anonymous:2313>~0 */
	obj_t BGl_zc3z04anonymousza32313ze3ze70z60zzcgen_cgenz00(obj_t
		BgL_inpushexitz00_6719, obj_t BgL_l1708z00_3065)
	{
		{	/* Cgen/cgen.scm 841 */
			{
				obj_t BgL_xz00_3128;

				if (NULLP(BgL_l1708z00_3065))
					{	/* Cgen/cgen.scm 841 */
						return BNIL;
					}
				else
					{	/* Cgen/cgen.scm 841 */
						obj_t BgL_arg2315z00_3068;
						obj_t BgL_arg2316z00_3069;

						{	/* Cgen/cgen.scm 841 */
							obj_t BgL_arg2317z00_3070;

							BgL_arg2317z00_3070 = CAR(((obj_t) BgL_l1708z00_3065));
							BgL_xz00_3128 = BgL_arg2317z00_3070;
							{	/* Cgen/cgen.scm 749 */
								bool_t BgL_test3250z00_10648;

								{	/* Cgen/cgen.scm 749 */
									bool_t BgL_test3251z00_10649;

									{	/* Cgen/cgen.scm 749 */
										obj_t BgL_arg2447z00_3310;

										BgL_arg2447z00_3310 = CDR(((obj_t) BgL_xz00_3128));
										{	/* Cgen/cgen.scm 749 */
											obj_t BgL_classz00_5335;

											BgL_classz00_5335 = BGl_appz00zzast_nodez00;
											if (BGL_OBJECTP(BgL_arg2447z00_3310))
												{	/* Cgen/cgen.scm 749 */
													BgL_objectz00_bglt BgL_arg1807z00_5337;

													BgL_arg1807z00_5337 =
														(BgL_objectz00_bglt) (BgL_arg2447z00_3310);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cgen/cgen.scm 749 */
															long BgL_idxz00_5343;

															BgL_idxz00_5343 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5337);
															BgL_test3251z00_10649 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5343 + 3L)) == BgL_classz00_5335);
														}
													else
														{	/* Cgen/cgen.scm 749 */
															bool_t BgL_res2851z00_5368;

															{	/* Cgen/cgen.scm 749 */
																obj_t BgL_oclassz00_5351;

																{	/* Cgen/cgen.scm 749 */
																	obj_t BgL_arg1815z00_5359;
																	long BgL_arg1816z00_5360;

																	BgL_arg1815z00_5359 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cgen/cgen.scm 749 */
																		long BgL_arg1817z00_5361;

																		BgL_arg1817z00_5361 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5337);
																		BgL_arg1816z00_5360 =
																			(BgL_arg1817z00_5361 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5351 =
																		VECTOR_REF(BgL_arg1815z00_5359,
																		BgL_arg1816z00_5360);
																}
																{	/* Cgen/cgen.scm 749 */
																	bool_t BgL__ortest_1115z00_5352;

																	BgL__ortest_1115z00_5352 =
																		(BgL_classz00_5335 == BgL_oclassz00_5351);
																	if (BgL__ortest_1115z00_5352)
																		{	/* Cgen/cgen.scm 749 */
																			BgL_res2851z00_5368 =
																				BgL__ortest_1115z00_5352;
																		}
																	else
																		{	/* Cgen/cgen.scm 749 */
																			long BgL_odepthz00_5353;

																			{	/* Cgen/cgen.scm 749 */
																				obj_t BgL_arg1804z00_5354;

																				BgL_arg1804z00_5354 =
																					(BgL_oclassz00_5351);
																				BgL_odepthz00_5353 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5354);
																			}
																			if ((3L < BgL_odepthz00_5353))
																				{	/* Cgen/cgen.scm 749 */
																					obj_t BgL_arg1802z00_5356;

																					{	/* Cgen/cgen.scm 749 */
																						obj_t BgL_arg1803z00_5357;

																						BgL_arg1803z00_5357 =
																							(BgL_oclassz00_5351);
																						BgL_arg1802z00_5356 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5357, 3L);
																					}
																					BgL_res2851z00_5368 =
																						(BgL_arg1802z00_5356 ==
																						BgL_classz00_5335);
																				}
																			else
																				{	/* Cgen/cgen.scm 749 */
																					BgL_res2851z00_5368 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3251z00_10649 = BgL_res2851z00_5368;
														}
												}
											else
												{	/* Cgen/cgen.scm 749 */
													BgL_test3251z00_10649 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test3251z00_10649)
										{	/* Cgen/cgen.scm 750 */
											bool_t BgL_test3256z00_10674;

											{	/* Cgen/cgen.scm 750 */
												obj_t BgL_arg2446z00_3309;

												BgL_arg2446z00_3309 = CDR(((obj_t) BgL_xz00_3128));
												BgL_test3256z00_10674 =
													BGl_stackablezf3ze70z14zzcgen_cgenz00(
													((BgL_appz00_bglt) BgL_arg2446z00_3309));
											}
											if (BgL_test3256z00_10674)
												{	/* Cgen/cgen.scm 750 */
													BgL_test3250z00_10648 =
														CBOOL(BGl_bigloozd2configzd2zz__configurez00
														(CNST_TABLE_REF(15)));
												}
											else
												{	/* Cgen/cgen.scm 750 */
													BgL_test3250z00_10648 = ((bool_t) 0);
												}
										}
									else
										{	/* Cgen/cgen.scm 749 */
											BgL_test3250z00_10648 = ((bool_t) 0);
										}
								}
								if (BgL_test3250z00_10648)
									{	/* Cgen/cgen.scm 753 */
										BgL_appz00_bglt BgL_i1293z00_3137;

										BgL_i1293z00_3137 =
											((BgL_appz00_bglt) CDR(((obj_t) BgL_xz00_3128)));
										{	/* Cgen/cgen.scm 754 */
											BgL_variablez00_bglt BgL_vz00_3138;

											BgL_vz00_3138 =
												(((BgL_varz00_bglt) COBJECT(
														(((BgL_appz00_bglt) COBJECT(BgL_i1293z00_3137))->
															BgL_funz00)))->BgL_variablez00);
											{	/* Cgen/cgen.scm 754 */
												obj_t BgL_saz00_3139;

												BgL_saz00_3139 =
													(((BgL_funz00_bglt) COBJECT(
															((BgL_funz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_vz00_3138))))->BgL_valuez00))))->
													BgL_stackzd2allocatorzd2);
												{	/* Cgen/cgen.scm 755 */

													{	/* Cgen/cgen.scm 756 */
														obj_t BgL_arg2361z00_3140;
														obj_t BgL_arg2363z00_3141;

														BgL_arg2361z00_3140 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_vz00_3138))))->
															BgL_namez00);
														BgL_arg2363z00_3141 =
															(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_i1293z00_3137)))->BgL_locz00);
														{	/* Cgen/cgen.scm 756 */
															obj_t BgL_list2364z00_3142;

															{	/* Cgen/cgen.scm 756 */
																obj_t BgL_arg2365z00_3143;

																{	/* Cgen/cgen.scm 756 */
																	obj_t BgL_arg2366z00_3144;

																	{	/* Cgen/cgen.scm 756 */
																		obj_t BgL_arg2367z00_3145;

																		{	/* Cgen/cgen.scm 756 */
																			obj_t BgL_arg2368z00_3146;

																			BgL_arg2368z00_3146 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2956z00zzcgen_cgenz00, BNIL);
																			BgL_arg2367z00_3145 =
																				MAKE_YOUNG_PAIR(BgL_arg2363z00_3141,
																				BgL_arg2368z00_3146);
																		}
																		BgL_arg2366z00_3144 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2957z00zzcgen_cgenz00,
																			BgL_arg2367z00_3145);
																	}
																	BgL_arg2365z00_3143 =
																		MAKE_YOUNG_PAIR(BgL_arg2361z00_3140,
																		BgL_arg2366z00_3144);
																}
																BgL_list2364z00_3142 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2958z00zzcgen_cgenz00,
																	BgL_arg2365z00_3143);
															}
															BGl_verbosez00zztools_speekz00(BINT(3L),
																BgL_list2364z00_3142);
														}
													}
													{	/* Cgen/cgen.scm 759 */
														obj_t BgL_idz00_3147;

														BgL_idz00_3147 =
															BGl_gensymz00zz__r4_symbols_6_4z00(
															(((BgL_variablez00_bglt) COBJECT(BgL_vz00_3138))->
																BgL_idz00));
														{	/* Cgen/cgen.scm 759 */
															BgL_localz00_bglt BgL_declz00_3148;

															{	/* Cgen/cgen.scm 760 */
																BgL_localz00_bglt BgL_dz00_3193;

																{	/* Cgen/cgen.scm 760 */
																	BgL_variablez00_bglt
																		BgL_duplicated1296z00_3194;
																	BgL_localz00_bglt BgL_new1294z00_3195;

																	BgL_duplicated1296z00_3194 =
																		((BgL_variablez00_bglt)
																		CAR(((obj_t) BgL_xz00_3128)));
																	{	/* Cgen/cgen.scm 761 */
																		BgL_localz00_bglt BgL_new1300z00_3199;

																		BgL_new1300z00_3199 =
																			((BgL_localz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_localz00_bgl))));
																		{	/* Cgen/cgen.scm 761 */
																			long BgL_arg2385z00_3200;

																			BgL_arg2385z00_3200 =
																				BGL_CLASS_NUM(BGl_localz00zzast_varz00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1300z00_3199),
																				BgL_arg2385z00_3200);
																		}
																		{	/* Cgen/cgen.scm 761 */
																			BgL_objectz00_bglt BgL_tmpz00_10713;

																			BgL_tmpz00_10713 =
																				((BgL_objectz00_bglt)
																				BgL_new1300z00_3199);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10713,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1300z00_3199);
																		BgL_new1294z00_3195 = BgL_new1300z00_3199;
																	}
																	((((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->BgL_idz00) =
																		((obj_t) BgL_idz00_3147), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_namez00) = ((obj_t) BFALSE), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2objza2z00zztype_cachez00)),
																		BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_valuez00) =
																		((BgL_valuez00_bglt) ((
																					(BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1296z00_3194))->
																				BgL_valuez00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_accessz00) =
																		((obj_t) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1296z00_3194))->
																				BgL_accessz00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_fastzd2alphazd2) =
																		((obj_t) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1296z00_3194))->
																				BgL_fastzd2alphazd2)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_removablez00) =
																		((obj_t) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1296z00_3194))->
																				BgL_removablez00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_occurrencez00) =
																		((long) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1296z00_3194))->
																				BgL_occurrencez00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_occurrencewz00) =
																		((long) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1296z00_3194))->
																				BgL_occurrencewz00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1294z00_3195)))->
																			BgL_userzf3zf3) =
																		((bool_t) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1296z00_3194))->
																				BgL_userzf3zf3)), BUNSPEC);
																	((((BgL_localz00_bglt)
																				COBJECT(BgL_new1294z00_3195))->
																			BgL_keyz00) =
																		((long) (((BgL_localz00_bglt)
																					COBJECT(((BgL_localz00_bglt)
																							BgL_duplicated1296z00_3194)))->
																				BgL_keyz00)), BUNSPEC);
																	((((BgL_localz00_bglt)
																				COBJECT(BgL_new1294z00_3195))->
																			BgL_valzd2noescapezd2) =
																		((obj_t) (((BgL_localz00_bglt)
																					COBJECT(((BgL_localz00_bglt)
																							BgL_duplicated1296z00_3194)))->
																				BgL_valzd2noescapezd2)), BUNSPEC);
																	((((BgL_localz00_bglt)
																				COBJECT(BgL_new1294z00_3195))->
																			BgL_volatilez00) =
																		((bool_t) (((BgL_localz00_bglt)
																					COBJECT(((BgL_localz00_bglt)
																							BgL_duplicated1296z00_3194)))->
																				BgL_volatilez00)), BUNSPEC);
																	BgL_dz00_3193 = BgL_new1294z00_3195;
																}
																BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
																	(((BgL_variablez00_bglt) BgL_dz00_3193));
																BgL_declz00_3148 = BgL_dz00_3193;
															}
															{	/* Cgen/cgen.scm 760 */
																BgL_cpragmaz00_bglt BgL_allocz00_3149;

																{	/* Cgen/cgen.scm 766 */
																	BgL_cpragmaz00_bglt BgL_new1303z00_3174;

																	{	/* Cgen/cgen.scm 768 */
																		BgL_cpragmaz00_bglt BgL_new1301z00_3191;

																		BgL_new1301z00_3191 =
																			((BgL_cpragmaz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_cpragmaz00_bgl))));
																		{	/* Cgen/cgen.scm 768 */
																			long BgL_arg2384z00_3192;

																			BgL_arg2384z00_3192 =
																				BGL_CLASS_NUM
																				(BGl_cpragmaz00zzcgen_copz00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1301z00_3191),
																				BgL_arg2384z00_3192);
																		}
																		BgL_new1303z00_3174 = BgL_new1301z00_3191;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1303z00_3174)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_i1293z00_3137)))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1303z00_3174)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00)),
																		BUNSPEC);
																	{
																		obj_t BgL_auxz00_10767;

																		{	/* Cgen/cgen.scm 769 */
																			obj_t BgL_arg2376z00_3175;
																			obj_t BgL_arg2377z00_3176;

																			BgL_arg2376z00_3175 =
																				CAR(((obj_t) BgL_saz00_3139));
																			BgL_arg2377z00_3176 =
																				(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_declz00_3148)))->BgL_namez00);
																			{	/* Cgen/cgen.scm 769 */
																				obj_t BgL_list2378z00_3177;

																				BgL_list2378z00_3177 =
																					MAKE_YOUNG_PAIR(BgL_arg2377z00_3176,
																					BNIL);
																				BgL_auxz00_10767 =
																					BGl_formatz00zz__r4_output_6_10_3z00
																					(BgL_arg2376z00_3175,
																					BgL_list2378z00_3177);
																		}}
																		((((BgL_cpragmaz00_bglt)
																					COBJECT(BgL_new1303z00_3174))->
																				BgL_formatz00) =
																			((obj_t) BgL_auxz00_10767), BUNSPEC);
																	}
																	{
																		obj_t BgL_auxz00_10775;

																		{	/* Cgen/cgen.scm 770 */
																			obj_t BgL_l1694z00_3178;

																			BgL_l1694z00_3178 =
																				(((BgL_appz00_bglt)
																					COBJECT(BgL_i1293z00_3137))->
																				BgL_argsz00);
																			if (NULLP(BgL_l1694z00_3178))
																				{	/* Cgen/cgen.scm 770 */
																					BgL_auxz00_10775 = BNIL;
																				}
																			else
																				{	/* Cgen/cgen.scm 770 */
																					obj_t BgL_head1696z00_3180;

																					BgL_head1696z00_3180 =
																						MAKE_YOUNG_PAIR(BNIL, BNIL);
																					{
																						obj_t BgL_l1694z00_3182;
																						obj_t BgL_tail1697z00_3183;

																						BgL_l1694z00_3182 =
																							BgL_l1694z00_3178;
																						BgL_tail1697z00_3183 =
																							BgL_head1696z00_3180;
																					BgL_zc3z04anonymousza32380ze3z87_3184:
																						if (NULLP
																							(BgL_l1694z00_3182))
																							{	/* Cgen/cgen.scm 770 */
																								BgL_auxz00_10775 =
																									CDR(BgL_head1696z00_3180);
																							}
																						else
																							{	/* Cgen/cgen.scm 770 */
																								obj_t BgL_newtail1698z00_3186;

																								{	/* Cgen/cgen.scm 770 */
																									BgL_copz00_bglt
																										BgL_arg2383z00_3188;
																									{	/* Cgen/cgen.scm 770 */
																										obj_t BgL_az00_3189;

																										BgL_az00_3189 =
																											CAR(
																											((obj_t)
																												BgL_l1694z00_3182));
																										BgL_arg2383z00_3188 =
																											BGl_nodezd2ze3copz31zzcgen_cgenz00
																											(((BgL_nodez00_bglt)
																												BgL_az00_3189),
																											BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
																											CBOOL
																											(BgL_inpushexitz00_6719));
																									}
																									BgL_newtail1698z00_3186 =
																										MAKE_YOUNG_PAIR(
																										((obj_t)
																											BgL_arg2383z00_3188),
																										BNIL);
																								}
																								SET_CDR(BgL_tail1697z00_3183,
																									BgL_newtail1698z00_3186);
																								{	/* Cgen/cgen.scm 770 */
																									obj_t BgL_arg2382z00_3187;

																									BgL_arg2382z00_3187 =
																										CDR(
																										((obj_t)
																											BgL_l1694z00_3182));
																									{
																										obj_t BgL_tail1697z00_10794;
																										obj_t BgL_l1694z00_10793;

																										BgL_l1694z00_10793 =
																											BgL_arg2382z00_3187;
																										BgL_tail1697z00_10794 =
																											BgL_newtail1698z00_3186;
																										BgL_tail1697z00_3183 =
																											BgL_tail1697z00_10794;
																										BgL_l1694z00_3182 =
																											BgL_l1694z00_10793;
																										goto
																											BgL_zc3z04anonymousza32380ze3z87_3184;
																									}
																								}
																							}
																					}
																				}
																		}
																		((((BgL_cpragmaz00_bglt)
																					COBJECT(BgL_new1303z00_3174))->
																				BgL_argsz00) =
																			((obj_t) BgL_auxz00_10775), BUNSPEC);
																	}
																	BgL_allocz00_3149 = BgL_new1303z00_3174;
																}
																{	/* Cgen/cgen.scm 766 */

																	{
																		BgL_varz00_bglt BgL_auxz00_10796;

																		{	/* Cgen/cgen.scm 774 */
																			BgL_nodez00_bglt
																				BgL_duplicated1306z00_3150;
																			BgL_refz00_bglt BgL_new1304z00_3151;

																			BgL_duplicated1306z00_3150 =
																				((BgL_nodez00_bglt)
																				(((BgL_appz00_bglt)
																						COBJECT(BgL_i1293z00_3137))->
																					BgL_funz00));
																			{	/* Cgen/cgen.scm 775 */
																				BgL_refz00_bglt BgL_new1307z00_3166;

																				BgL_new1307z00_3166 =
																					((BgL_refz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_refz00_bgl))));
																				{	/* Cgen/cgen.scm 775 */
																					long BgL_arg2370z00_3167;

																					{	/* Cgen/cgen.scm 775 */
																						obj_t BgL_classz00_5390;

																						BgL_classz00_5390 =
																							BGl_refz00zzast_nodez00;
																						BgL_arg2370z00_3167 =
																							BGL_CLASS_NUM(BgL_classz00_5390);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1307z00_3166),
																						BgL_arg2370z00_3167);
																				}
																				{	/* Cgen/cgen.scm 775 */
																					BgL_objectz00_bglt BgL_tmpz00_10803;

																					BgL_tmpz00_10803 =
																						((BgL_objectz00_bglt)
																						BgL_new1307z00_3166);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_10803, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1307z00_3166);
																				BgL_new1304z00_3151 =
																					BgL_new1307z00_3166;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1304z00_3151)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT
																							(BgL_duplicated1306z00_3150))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1304z00_3151)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																							COBJECT
																							(BgL_duplicated1306z00_3150))->
																						BgL_typez00)), BUNSPEC);
																			{
																				BgL_variablez00_bglt BgL_auxz00_10813;

																				{	/* Cgen/cgen.scm 775 */
																					BgL_globalz00_bglt
																						BgL_new1308z00_3153;
																					{	/* Cgen/cgen.scm 776 */
																						BgL_globalz00_bglt
																							BgL_new1321z00_3164;
																						BgL_new1321z00_3164 =
																							((BgL_globalz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_globalz00_bgl))));
																						{	/* Cgen/cgen.scm 776 */
																							long BgL_arg2369z00_3165;

																							{	/* Cgen/cgen.scm 776 */
																								obj_t BgL_classz00_5394;

																								BgL_classz00_5394 =
																									BGl_globalz00zzast_varz00;
																								BgL_arg2369z00_3165 =
																									BGL_CLASS_NUM
																									(BgL_classz00_5394);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1321z00_3164),
																								BgL_arg2369z00_3165);
																						}
																						{	/* Cgen/cgen.scm 776 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_10819;
																							BgL_tmpz00_10819 =
																								((BgL_objectz00_bglt)
																								BgL_new1321z00_3164);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_10819, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1321z00_3164);
																						BgL_new1308z00_3153 =
																							BgL_new1321z00_3164;
																					}
																					((((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_idz00) =
																						((obj_t) (((BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_idz00)), BUNSPEC);
																					{
																						obj_t BgL_auxz00_10826;

																						{	/* Cgen/cgen.scm 776 */
																							obj_t BgL_pairz00_5401;

																							BgL_pairz00_5401 =
																								CDR(((obj_t) BgL_saz00_3139));
																							BgL_auxz00_10826 =
																								CAR(BgL_pairz00_5401);
																						}
																						((((BgL_variablez00_bglt) COBJECT(
																										((BgL_variablez00_bglt)
																											BgL_new1308z00_3153)))->
																								BgL_namez00) =
																							((obj_t) BgL_auxz00_10826),
																							BUNSPEC);
																					}
																					((((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) ((
																									(BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_typez00)), BUNSPEC);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_valuez00) =
																						((BgL_valuez00_bglt) ((
																									(BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_valuez00)), BUNSPEC);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_accessz00) =
																						((obj_t) (((BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_accessz00)), BUNSPEC);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_fastzd2alphazd2) =
																						((obj_t) (((BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_fastzd2alphazd2)), BUNSPEC);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_removablez00) =
																						((obj_t) (((BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_removablez00)), BUNSPEC);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_occurrencez00) =
																						((long) (((BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_occurrencez00)), BUNSPEC);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_occurrencewz00) =
																						((long) (((BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_occurrencewz00)), BUNSPEC);
																					((((BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_new1308z00_3153)))->
																							BgL_userzf3zf3) =
																						((bool_t) (((BgL_variablez00_bglt)
																									COBJECT(BgL_vz00_3138))->
																								BgL_userzf3zf3)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_modulez00) =
																						((obj_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_modulez00)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_importz00) =
																						((obj_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_importz00)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_evaluablezf3zf3) =
																						((bool_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_evaluablezf3zf3)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_evalzf3zf3) =
																						((bool_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_evalzf3zf3)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_libraryz00) =
																						((obj_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_libraryz00)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_pragmaz00) =
																						((obj_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_pragmaz00)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_srcz00) =
																						((obj_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_srcz00)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_jvmzd2typezd2namez00) =
																						((obj_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_jvmzd2typezd2namez00)),
																						BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_initz00) =
																						((obj_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_initz00)), BUNSPEC);
																					((((BgL_globalz00_bglt)
																								COBJECT(BgL_new1308z00_3153))->
																							BgL_aliasz00) =
																						((obj_t) (((BgL_globalz00_bglt)
																									COBJECT(((BgL_globalz00_bglt)
																											BgL_vz00_3138)))->
																								BgL_aliasz00)), BUNSPEC);
																					BgL_auxz00_10813 =
																						((BgL_variablez00_bglt)
																						BgL_new1308z00_3153);
																				}
																				((((BgL_varz00_bglt) COBJECT(
																								((BgL_varz00_bglt)
																									BgL_new1304z00_3151)))->
																						BgL_variablez00) =
																					((BgL_variablez00_bglt)
																						BgL_auxz00_10813), BUNSPEC);
																			}
																			BgL_auxz00_10796 =
																				((BgL_varz00_bglt) BgL_new1304z00_3151);
																		}
																		((((BgL_appz00_bglt)
																					COBJECT(BgL_i1293z00_3137))->
																				BgL_funz00) =
																			((BgL_varz00_bglt) BgL_auxz00_10796),
																			BUNSPEC);
																	}
																	{
																		obj_t BgL_auxz00_10890;

																		{	/* Cgen/cgen.scm 777 */
																			BgL_refz00_bglt BgL_arg2371z00_3168;
																			obj_t BgL_arg2373z00_3169;

																			{	/* Cgen/cgen.scm 777 */
																				BgL_refz00_bglt BgL_new1323z00_3170;

																				{	/* Cgen/cgen.scm 780 */
																					BgL_refz00_bglt BgL_new1322z00_3171;

																					BgL_new1322z00_3171 =
																						((BgL_refz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_refz00_bgl))));
																					{	/* Cgen/cgen.scm 780 */
																						long BgL_arg2374z00_3172;

																						{	/* Cgen/cgen.scm 780 */
																							obj_t BgL_classz00_5402;

																							BgL_classz00_5402 =
																								BGl_refz00zzast_nodez00;
																							BgL_arg2374z00_3172 =
																								BGL_CLASS_NUM
																								(BgL_classz00_5402);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1322z00_3171),
																							BgL_arg2374z00_3172);
																					}
																					{	/* Cgen/cgen.scm 780 */
																						BgL_objectz00_bglt BgL_tmpz00_10895;

																						BgL_tmpz00_10895 =
																							((BgL_objectz00_bglt)
																							BgL_new1322z00_3171);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_10895, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1322z00_3171);
																					BgL_new1323z00_3170 =
																						BgL_new1322z00_3171;
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1323z00_3170)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_i1293z00_3137)))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt)
																									BgL_new1323z00_3170)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) (
																							(BgL_typez00_bglt)
																							BGl_za2objza2z00zztype_cachez00)),
																					BUNSPEC);
																				((((BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									BgL_new1323z00_3170)))->
																						BgL_variablez00) =
																					((BgL_variablez00_bglt) (
																							(BgL_variablez00_bglt)
																							BgL_declz00_3148)), BUNSPEC);
																				BgL_arg2371z00_3168 =
																					BgL_new1323z00_3170;
																			}
																			BgL_arg2373z00_3169 =
																				(((BgL_appz00_bglt)
																					COBJECT(BgL_i1293z00_3137))->
																				BgL_argsz00);
																			BgL_auxz00_10890 =
																				MAKE_YOUNG_PAIR(((obj_t)
																					BgL_arg2371z00_3168),
																				BgL_arg2373z00_3169);
																		}
																		((((BgL_appz00_bglt)
																					COBJECT(BgL_i1293z00_3137))->
																				BgL_argsz00) =
																			((obj_t) BgL_auxz00_10890), BUNSPEC);
																	}
																	{	/* Cgen/cgen.scm 782 */
																		obj_t BgL_list2375z00_3173;

																		BgL_list2375z00_3173 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_allocz00_3149), BNIL);
																		BgL_arg2315z00_3068 = BgL_list2375z00_3173;
									}}}}}}}}}
								else
									{	/* Cgen/cgen.scm 783 */
										bool_t BgL_test3259z00_10915;

										{	/* Cgen/cgen.scm 783 */
											bool_t BgL_test3260z00_10916;

											{	/* Cgen/cgen.scm 783 */
												obj_t BgL_arg2445z00_3306;

												BgL_arg2445z00_3306 = CDR(((obj_t) BgL_xz00_3128));
												BgL_test3260z00_10916 =
													BGl_allocazd2letzd2varze70ze7zzcgen_cgenz00(
													((BgL_nodez00_bglt) BgL_arg2445z00_3306));
											}
											if (BgL_test3260z00_10916)
												{	/* Cgen/cgen.scm 783 */
													BgL_test3259z00_10915 =
														CBOOL(BGl_bigloozd2configzd2zz__configurez00
														(CNST_TABLE_REF(15)));
												}
											else
												{	/* Cgen/cgen.scm 783 */
													BgL_test3259z00_10915 = ((bool_t) 0);
												}
										}
										if (BgL_test3259z00_10915)
											{	/* Cgen/cgen.scm 786 */
												BgL_letzd2varzd2_bglt BgL_i1324z00_3208;

												BgL_i1324z00_3208 =
													((BgL_letzd2varzd2_bglt)
													CDR(((obj_t) BgL_xz00_3128)));
												{	/* Cgen/cgen.scm 787 */
													BgL_appz00_bglt BgL_i1325z00_3209;

													BgL_i1325z00_3209 =
														((BgL_appz00_bglt)
														(((BgL_letzd2varzd2_bglt)
																COBJECT(BgL_i1324z00_3208))->BgL_bodyz00));
													{	/* Cgen/cgen.scm 788 */
														BgL_variablez00_bglt BgL_vz00_3210;

														BgL_vz00_3210 =
															(((BgL_varz00_bglt) COBJECT(
																	(((BgL_appz00_bglt)
																			COBJECT(BgL_i1325z00_3209))->
																		BgL_funz00)))->BgL_variablez00);
														{	/* Cgen/cgen.scm 788 */
															obj_t BgL_saz00_3211;

															BgL_saz00_3211 =
																(((BgL_funz00_bglt) COBJECT(
																		((BgL_funz00_bglt)
																			(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_globalz00_bglt)
																								BgL_vz00_3210))))->
																				BgL_valuez00))))->
																BgL_stackzd2allocatorzd2);
															{	/* Cgen/cgen.scm 789 */

																{	/* Cgen/cgen.scm 790 */
																	obj_t BgL_arg2393z00_3212;
																	obj_t BgL_arg2395z00_3213;

																	BgL_arg2393z00_3212 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_vz00_3210))))->BgL_namez00);
																	BgL_arg2395z00_3213 =
																		(((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_i1325z00_3209)))->BgL_locz00);
																	{	/* Cgen/cgen.scm 790 */
																		obj_t BgL_list2396z00_3214;

																		{	/* Cgen/cgen.scm 790 */
																			obj_t BgL_arg2397z00_3215;

																			{	/* Cgen/cgen.scm 790 */
																				obj_t BgL_arg2398z00_3216;

																				{	/* Cgen/cgen.scm 790 */
																					obj_t BgL_arg2399z00_3217;

																					{	/* Cgen/cgen.scm 790 */
																						obj_t BgL_arg2401z00_3218;

																						BgL_arg2401z00_3218 =
																							MAKE_YOUNG_PAIR
																							(BGl_string2956z00zzcgen_cgenz00,
																							BNIL);
																						BgL_arg2399z00_3217 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2395z00_3213,
																							BgL_arg2401z00_3218);
																					}
																					BgL_arg2398z00_3216 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2957z00zzcgen_cgenz00,
																						BgL_arg2399z00_3217);
																				}
																				BgL_arg2397z00_3215 =
																					MAKE_YOUNG_PAIR(BgL_arg2393z00_3212,
																					BgL_arg2398z00_3216);
																			}
																			BgL_list2396z00_3214 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2958z00zzcgen_cgenz00,
																				BgL_arg2397z00_3215);
																		}
																		BGl_verbosez00zztools_speekz00(BINT(3L),
																			BgL_list2396z00_3214);
																	}
																}
																{	/* Cgen/cgen.scm 793 */
																	obj_t BgL_idz00_3219;

																	BgL_idz00_3219 =
																		BGl_gensymz00zz__r4_symbols_6_4z00(
																		(((BgL_variablez00_bglt)
																				COBJECT(BgL_vz00_3210))->BgL_idz00));
																	{	/* Cgen/cgen.scm 793 */
																		BgL_localz00_bglt BgL_declz00_3220;

																		{	/* Cgen/cgen.scm 794 */
																			BgL_localz00_bglt BgL_dz00_3265;

																			{	/* Cgen/cgen.scm 794 */
																				BgL_variablez00_bglt
																					BgL_duplicated1328z00_3266;
																				BgL_localz00_bglt BgL_new1326z00_3267;

																				BgL_duplicated1328z00_3266 =
																					((BgL_variablez00_bglt)
																					CAR(((obj_t) BgL_xz00_3128)));
																				{	/* Cgen/cgen.scm 795 */
																					BgL_localz00_bglt BgL_new1332z00_3271;

																					BgL_new1332z00_3271 =
																						((BgL_localz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_localz00_bgl))));
																					{	/* Cgen/cgen.scm 795 */
																						long BgL_arg2420z00_3272;

																						BgL_arg2420z00_3272 =
																							BGL_CLASS_NUM
																							(BGl_localz00zzast_varz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1332z00_3271),
																							BgL_arg2420z00_3272);
																					}
																					{	/* Cgen/cgen.scm 795 */
																						BgL_objectz00_bglt BgL_tmpz00_10957;

																						BgL_tmpz00_10957 =
																							((BgL_objectz00_bglt)
																							BgL_new1332z00_3271);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_10957, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1332z00_3271);
																					BgL_new1326z00_3267 =
																						BgL_new1332z00_3271;
																				}
																				((((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_idz00) =
																					((obj_t) BgL_idz00_3219), BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_namez00) =
																					((obj_t) BFALSE), BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) (
																							(BgL_typez00_bglt)
																							BGl_za2objza2z00zztype_cachez00)),
																					BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_valuez00) =
																					((BgL_valuez00_bglt) ((
																								(BgL_variablez00_bglt)
																								COBJECT
																								(BgL_duplicated1328z00_3266))->
																							BgL_valuez00)), BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_accessz00) =
																					((obj_t) (((BgL_variablez00_bglt)
																								COBJECT
																								(BgL_duplicated1328z00_3266))->
																							BgL_accessz00)), BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_fastzd2alphazd2) =
																					((obj_t) (((BgL_variablez00_bglt)
																								COBJECT
																								(BgL_duplicated1328z00_3266))->
																							BgL_fastzd2alphazd2)), BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_removablez00) =
																					((obj_t) (((BgL_variablez00_bglt)
																								COBJECT
																								(BgL_duplicated1328z00_3266))->
																							BgL_removablez00)), BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_occurrencez00) =
																					((long) (((BgL_variablez00_bglt)
																								COBJECT
																								(BgL_duplicated1328z00_3266))->
																							BgL_occurrencez00)), BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_occurrencewz00) =
																					((long) (((BgL_variablez00_bglt)
																								COBJECT
																								(BgL_duplicated1328z00_3266))->
																							BgL_occurrencewz00)), BUNSPEC);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_new1326z00_3267)))->
																						BgL_userzf3zf3) =
																					((bool_t) (((BgL_variablez00_bglt)
																								COBJECT
																								(BgL_duplicated1328z00_3266))->
																							BgL_userzf3zf3)), BUNSPEC);
																				((((BgL_localz00_bglt)
																							COBJECT(BgL_new1326z00_3267))->
																						BgL_keyz00) =
																					((long) (((BgL_localz00_bglt)
																								COBJECT(((BgL_localz00_bglt)
																										BgL_duplicated1328z00_3266)))->
																							BgL_keyz00)), BUNSPEC);
																				((((BgL_localz00_bglt)
																							COBJECT(BgL_new1326z00_3267))->
																						BgL_valzd2noescapezd2) =
																					((obj_t) (((BgL_localz00_bglt)
																								COBJECT(((BgL_localz00_bglt)
																										BgL_duplicated1328z00_3266)))->
																							BgL_valzd2noescapezd2)), BUNSPEC);
																				((((BgL_localz00_bglt)
																							COBJECT(BgL_new1326z00_3267))->
																						BgL_volatilez00) =
																					((bool_t) (((BgL_localz00_bglt)
																								COBJECT(((BgL_localz00_bglt)
																										BgL_duplicated1328z00_3266)))->
																							BgL_volatilez00)), BUNSPEC);
																				BgL_dz00_3265 = BgL_new1326z00_3267;
																			}
																			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
																				(((BgL_variablez00_bglt)
																					BgL_dz00_3265));
																			BgL_declz00_3220 = BgL_dz00_3265;
																		}
																		{	/* Cgen/cgen.scm 794 */
																			BgL_cpragmaz00_bglt BgL_allocz00_3221;

																			{	/* Cgen/cgen.scm 800 */
																				BgL_cpragmaz00_bglt BgL_new1334z00_3246;

																				{	/* Cgen/cgen.scm 802 */
																					BgL_cpragmaz00_bglt
																						BgL_new1333z00_3263;
																					BgL_new1333z00_3263 =
																						((BgL_cpragmaz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_cpragmaz00_bgl))));
																					{	/* Cgen/cgen.scm 802 */
																						long BgL_arg2419z00_3264;

																						BgL_arg2419z00_3264 =
																							BGL_CLASS_NUM
																							(BGl_cpragmaz00zzcgen_copz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1333z00_3263),
																							BgL_arg2419z00_3264);
																					}
																					BgL_new1334z00_3246 =
																						BgL_new1333z00_3263;
																				}
																				((((BgL_copz00_bglt) COBJECT(
																								((BgL_copz00_bglt)
																									BgL_new1334z00_3246)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_i1325z00_3209)))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_copz00_bglt)
																							COBJECT(((BgL_copz00_bglt)
																									BgL_new1334z00_3246)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) (
																							(BgL_typez00_bglt)
																							BGl_za2_za2z00zztype_cachez00)),
																					BUNSPEC);
																				{
																					obj_t BgL_auxz00_11011;

																					{	/* Cgen/cgen.scm 803 */
																						obj_t BgL_arg2410z00_3247;
																						obj_t BgL_arg2411z00_3248;

																						BgL_arg2410z00_3247 =
																							CAR(((obj_t) BgL_saz00_3211));
																						BgL_arg2411z00_3248 =
																							(((BgL_variablez00_bglt) COBJECT(
																									((BgL_variablez00_bglt)
																										BgL_declz00_3220)))->
																							BgL_namez00);
																						{	/* Cgen/cgen.scm 803 */
																							obj_t BgL_list2412z00_3249;

																							BgL_list2412z00_3249 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2411z00_3248, BNIL);
																							BgL_auxz00_11011 =
																								BGl_formatz00zz__r4_output_6_10_3z00
																								(BgL_arg2410z00_3247,
																								BgL_list2412z00_3249);
																					}}
																					((((BgL_cpragmaz00_bglt)
																								COBJECT(BgL_new1334z00_3246))->
																							BgL_formatz00) =
																						((obj_t) BgL_auxz00_11011),
																						BUNSPEC);
																				}
																				{
																					obj_t BgL_auxz00_11019;

																					{	/* Cgen/cgen.scm 804 */
																						obj_t BgL_l1699z00_3250;

																						BgL_l1699z00_3250 =
																							(((BgL_appz00_bglt)
																								COBJECT(BgL_i1325z00_3209))->
																							BgL_argsz00);
																						if (NULLP(BgL_l1699z00_3250))
																							{	/* Cgen/cgen.scm 804 */
																								BgL_auxz00_11019 = BNIL;
																							}
																						else
																							{	/* Cgen/cgen.scm 804 */
																								obj_t BgL_head1701z00_3252;

																								BgL_head1701z00_3252 =
																									MAKE_YOUNG_PAIR(BNIL, BNIL);
																								{
																									obj_t BgL_l1699z00_3254;
																									obj_t BgL_tail1702z00_3255;

																									BgL_l1699z00_3254 =
																										BgL_l1699z00_3250;
																									BgL_tail1702z00_3255 =
																										BgL_head1701z00_3252;
																								BgL_zc3z04anonymousza32414ze3z87_3256:
																									if (NULLP
																										(BgL_l1699z00_3254))
																										{	/* Cgen/cgen.scm 804 */
																											BgL_auxz00_11019 =
																												CDR
																												(BgL_head1701z00_3252);
																										}
																									else
																										{	/* Cgen/cgen.scm 804 */
																											obj_t
																												BgL_newtail1703z00_3258;
																											{	/* Cgen/cgen.scm 804 */
																												BgL_copz00_bglt
																													BgL_arg2418z00_3260;
																												{	/* Cgen/cgen.scm 804 */
																													obj_t BgL_az00_3261;

																													BgL_az00_3261 =
																														CAR(
																														((obj_t)
																															BgL_l1699z00_3254));
																													BgL_arg2418z00_3260 =
																														BGl_nodezd2ze3copz31zzcgen_cgenz00
																														(((BgL_nodez00_bglt)
																															BgL_az00_3261),
																														BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
																														CBOOL
																														(BgL_inpushexitz00_6719));
																												}
																												BgL_newtail1703z00_3258
																													=
																													MAKE_YOUNG_PAIR((
																														(obj_t)
																														BgL_arg2418z00_3260),
																													BNIL);
																											}
																											SET_CDR
																												(BgL_tail1702z00_3255,
																												BgL_newtail1703z00_3258);
																											{	/* Cgen/cgen.scm 804 */
																												obj_t
																													BgL_arg2417z00_3259;
																												BgL_arg2417z00_3259 =
																													CDR(((obj_t)
																														BgL_l1699z00_3254));
																												{
																													obj_t
																														BgL_tail1702z00_11038;
																													obj_t
																														BgL_l1699z00_11037;
																													BgL_l1699z00_11037 =
																														BgL_arg2417z00_3259;
																													BgL_tail1702z00_11038
																														=
																														BgL_newtail1703z00_3258;
																													BgL_tail1702z00_3255 =
																														BgL_tail1702z00_11038;
																													BgL_l1699z00_3254 =
																														BgL_l1699z00_11037;
																													goto
																														BgL_zc3z04anonymousza32414ze3z87_3256;
																												}
																											}
																										}
																								}
																							}
																					}
																					((((BgL_cpragmaz00_bglt)
																								COBJECT(BgL_new1334z00_3246))->
																							BgL_argsz00) =
																						((obj_t) BgL_auxz00_11019),
																						BUNSPEC);
																				}
																				BgL_allocz00_3221 = BgL_new1334z00_3246;
																			}
																			{	/* Cgen/cgen.scm 800 */

																				{
																					BgL_varz00_bglt BgL_auxz00_11040;

																					{	/* Cgen/cgen.scm 808 */
																						BgL_nodez00_bglt
																							BgL_duplicated1337z00_3222;
																						BgL_refz00_bglt BgL_new1335z00_3223;

																						BgL_duplicated1337z00_3222 =
																							((BgL_nodez00_bglt)
																							(((BgL_appz00_bglt)
																									COBJECT(BgL_i1325z00_3209))->
																								BgL_funz00));
																						{	/* Cgen/cgen.scm 809 */
																							BgL_refz00_bglt
																								BgL_new1338z00_3238;
																							BgL_new1338z00_3238 =
																								((BgL_refz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_refz00_bgl))));
																							{	/* Cgen/cgen.scm 809 */
																								long BgL_arg2403z00_3239;

																								{	/* Cgen/cgen.scm 809 */
																									obj_t BgL_classz00_5428;

																									BgL_classz00_5428 =
																										BGl_refz00zzast_nodez00;
																									BgL_arg2403z00_3239 =
																										BGL_CLASS_NUM
																										(BgL_classz00_5428);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1338z00_3238),
																									BgL_arg2403z00_3239);
																							}
																							{	/* Cgen/cgen.scm 809 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_11047;
																								BgL_tmpz00_11047 =
																									((BgL_objectz00_bglt)
																									BgL_new1338z00_3238);
																								BGL_OBJECT_WIDENING_SET
																									(BgL_tmpz00_11047, BFALSE);
																							}
																							((BgL_objectz00_bglt)
																								BgL_new1338z00_3238);
																							BgL_new1335z00_3223 =
																								BgL_new1338z00_3238;
																						}
																						((((BgL_nodez00_bglt) COBJECT(
																										((BgL_nodez00_bglt)
																											BgL_new1335z00_3223)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT
																										(BgL_duplicated1337z00_3222))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt)
																											BgL_new1335z00_3223)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) ((
																										(BgL_nodez00_bglt)
																										COBJECT
																										(BgL_duplicated1337z00_3222))->
																									BgL_typez00)), BUNSPEC);
																						{
																							BgL_variablez00_bglt
																								BgL_auxz00_11057;
																							{	/* Cgen/cgen.scm 809 */
																								BgL_globalz00_bglt
																									BgL_new1339z00_3225;
																								{	/* Cgen/cgen.scm 810 */
																									BgL_globalz00_bglt
																										BgL_new1352z00_3236;
																									BgL_new1352z00_3236 =
																										((BgL_globalz00_bglt)
																										BOBJECT(GC_MALLOC(sizeof
																												(struct
																													BgL_globalz00_bgl))));
																									{	/* Cgen/cgen.scm 810 */
																										long BgL_arg2402z00_3237;

																										{	/* Cgen/cgen.scm 810 */
																											obj_t BgL_classz00_5432;

																											BgL_classz00_5432 =
																												BGl_globalz00zzast_varz00;
																											BgL_arg2402z00_3237 =
																												BGL_CLASS_NUM
																												(BgL_classz00_5432);
																										}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_new1352z00_3236),
																											BgL_arg2402z00_3237);
																									}
																									{	/* Cgen/cgen.scm 810 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_11063;
																										BgL_tmpz00_11063 =
																											((BgL_objectz00_bglt)
																											BgL_new1352z00_3236);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_11063,
																											BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_new1352z00_3236);
																									BgL_new1339z00_3225 =
																										BgL_new1352z00_3236;
																								}
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_idz00) =
																									((obj_t) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_idz00)), BUNSPEC);
																								{
																									obj_t BgL_auxz00_11070;

																									{	/* Cgen/cgen.scm 810 */
																										obj_t BgL_pairz00_5439;

																										BgL_pairz00_5439 =
																											CDR(
																											((obj_t) BgL_saz00_3211));
																										BgL_auxz00_11070 =
																											CAR(BgL_pairz00_5439);
																									}
																									((((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														BgL_new1339z00_3225)))->
																											BgL_namez00) =
																										((obj_t) BgL_auxz00_11070),
																										BUNSPEC);
																								}
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_typez00) =
																									((BgL_typez00_bglt) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_typez00)), BUNSPEC);
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_valuez00) =
																									((BgL_valuez00_bglt) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_valuez00)), BUNSPEC);
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_accessz00) =
																									((obj_t) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_accessz00)), BUNSPEC);
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_fastzd2alphazd2) =
																									((obj_t) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_fastzd2alphazd2)),
																									BUNSPEC);
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_removablez00) =
																									((obj_t) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_removablez00)),
																									BUNSPEC);
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_occurrencez00) =
																									((long) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_occurrencez00)),
																									BUNSPEC);
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_occurrencewz00) =
																									((long) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_occurrencewz00)),
																									BUNSPEC);
																								((((BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_new1339z00_3225)))->
																										BgL_userzf3zf3) =
																									((bool_t) ((
																												(BgL_variablez00_bglt)
																												COBJECT
																												(BgL_vz00_3210))->
																											BgL_userzf3zf3)),
																									BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_modulez00) =
																									((obj_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_modulez00)), BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_importz00) =
																									((obj_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_importz00)), BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_evaluablezf3zf3) =
																									((bool_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_evaluablezf3zf3)),
																									BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_evalzf3zf3) =
																									((bool_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_evalzf3zf3)),
																									BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_libraryz00) =
																									((obj_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_libraryz00)),
																									BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_pragmaz00) =
																									((obj_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_pragmaz00)), BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_srcz00) =
																									((obj_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_srcz00)), BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_jvmzd2typezd2namez00) =
																									((obj_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_jvmzd2typezd2namez00)),
																									BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_initz00) =
																									((obj_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_initz00)), BUNSPEC);
																								((((BgL_globalz00_bglt)
																											COBJECT
																											(BgL_new1339z00_3225))->
																										BgL_aliasz00) =
																									((obj_t) ((
																												(BgL_globalz00_bglt)
																												COBJECT((
																														(BgL_globalz00_bglt)
																														BgL_vz00_3210)))->
																											BgL_aliasz00)), BUNSPEC);
																								BgL_auxz00_11057 =
																									((BgL_variablez00_bglt)
																									BgL_new1339z00_3225);
																							}
																							((((BgL_varz00_bglt) COBJECT(
																											((BgL_varz00_bglt)
																												BgL_new1335z00_3223)))->
																									BgL_variablez00) =
																								((BgL_variablez00_bglt)
																									BgL_auxz00_11057), BUNSPEC);
																						}
																						BgL_auxz00_11040 =
																							((BgL_varz00_bglt)
																							BgL_new1335z00_3223);
																					}
																					((((BgL_appz00_bglt)
																								COBJECT(BgL_i1325z00_3209))->
																							BgL_funz00) =
																						((BgL_varz00_bglt)
																							BgL_auxz00_11040), BUNSPEC);
																				}
																				{
																					obj_t BgL_auxz00_11134;

																					{	/* Cgen/cgen.scm 811 */
																						BgL_refz00_bglt BgL_arg2404z00_3240;
																						obj_t BgL_arg2405z00_3241;

																						{	/* Cgen/cgen.scm 811 */
																							BgL_refz00_bglt
																								BgL_new1354z00_3242;
																							{	/* Cgen/cgen.scm 814 */
																								BgL_refz00_bglt
																									BgL_new1353z00_3243;
																								BgL_new1353z00_3243 =
																									((BgL_refz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_refz00_bgl))));
																								{	/* Cgen/cgen.scm 814 */
																									long BgL_arg2407z00_3244;

																									{	/* Cgen/cgen.scm 814 */
																										obj_t BgL_classz00_5440;

																										BgL_classz00_5440 =
																											BGl_refz00zzast_nodez00;
																										BgL_arg2407z00_3244 =
																											BGL_CLASS_NUM
																											(BgL_classz00_5440);
																									}
																									BGL_OBJECT_CLASS_NUM_SET(
																										((BgL_objectz00_bglt)
																											BgL_new1353z00_3243),
																										BgL_arg2407z00_3244);
																								}
																								{	/* Cgen/cgen.scm 814 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_11139;
																									BgL_tmpz00_11139 =
																										((BgL_objectz00_bglt)
																										BgL_new1353z00_3243);
																									BGL_OBJECT_WIDENING_SET
																										(BgL_tmpz00_11139, BFALSE);
																								}
																								((BgL_objectz00_bglt)
																									BgL_new1353z00_3243);
																								BgL_new1354z00_3242 =
																									BgL_new1353z00_3243;
																							}
																							((((BgL_nodez00_bglt) COBJECT(
																											((BgL_nodez00_bglt)
																												BgL_new1354z00_3242)))->
																									BgL_locz00) =
																								((obj_t) (((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt)
																													BgL_i1325z00_3209)))->
																										BgL_locz00)), BUNSPEC);
																							((((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												BgL_new1354z00_3242)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt) (
																										(BgL_typez00_bglt)
																										BGl_za2objza2z00zztype_cachez00)),
																								BUNSPEC);
																							((((BgL_varz00_bglt)
																										COBJECT(((BgL_varz00_bglt)
																												BgL_new1354z00_3242)))->
																									BgL_variablez00) =
																								((BgL_variablez00_bglt) (
																										(BgL_variablez00_bglt)
																										BgL_declz00_3220)),
																								BUNSPEC);
																							BgL_arg2404z00_3240 =
																								BgL_new1354z00_3242;
																						}
																						BgL_arg2405z00_3241 =
																							(((BgL_appz00_bglt)
																								COBJECT(BgL_i1325z00_3209))->
																							BgL_argsz00);
																						BgL_auxz00_11134 =
																							MAKE_YOUNG_PAIR(((obj_t)
																								BgL_arg2404z00_3240),
																							BgL_arg2405z00_3241);
																					}
																					((((BgL_appz00_bglt)
																								COBJECT(BgL_i1325z00_3209))->
																							BgL_argsz00) =
																						((obj_t) BgL_auxz00_11134),
																						BUNSPEC);
																				}
																				{	/* Cgen/cgen.scm 816 */
																					obj_t BgL_list2408z00_3245;

																					BgL_list2408z00_3245 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_allocz00_3221), BNIL);
																					BgL_arg2315z00_3068 =
																						BgL_list2408z00_3245;
											}}}}}}}}}}
										else
											{	/* Cgen/cgen.scm 817 */
												bool_t BgL_test3263z00_11159;

												{	/* Cgen/cgen.scm 817 */
													bool_t BgL_test3264z00_11160;

													{	/* Cgen/cgen.scm 817 */
														obj_t BgL_arg2444z00_3304;

														BgL_arg2444z00_3304 = CDR(((obj_t) BgL_xz00_3128));
														{	/* Cgen/cgen.scm 817 */
															obj_t BgL_classz00_5446;

															BgL_classz00_5446 =
																BGl_makezd2boxzd2zzast_nodez00;
															if (BGL_OBJECTP(BgL_arg2444z00_3304))
																{	/* Cgen/cgen.scm 817 */
																	BgL_objectz00_bglt BgL_arg1807z00_5448;

																	BgL_arg1807z00_5448 =
																		(BgL_objectz00_bglt) (BgL_arg2444z00_3304);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Cgen/cgen.scm 817 */
																			long BgL_idxz00_5454;

																			BgL_idxz00_5454 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_5448);
																			BgL_test3264z00_11160 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_5454 + 3L)) ==
																				BgL_classz00_5446);
																		}
																	else
																		{	/* Cgen/cgen.scm 817 */
																			bool_t BgL_res2854z00_5479;

																			{	/* Cgen/cgen.scm 817 */
																				obj_t BgL_oclassz00_5462;

																				{	/* Cgen/cgen.scm 817 */
																					obj_t BgL_arg1815z00_5470;
																					long BgL_arg1816z00_5471;

																					BgL_arg1815z00_5470 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Cgen/cgen.scm 817 */
																						long BgL_arg1817z00_5472;

																						BgL_arg1817z00_5472 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_5448);
																						BgL_arg1816z00_5471 =
																							(BgL_arg1817z00_5472 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_5462 =
																						VECTOR_REF(BgL_arg1815z00_5470,
																						BgL_arg1816z00_5471);
																				}
																				{	/* Cgen/cgen.scm 817 */
																					bool_t BgL__ortest_1115z00_5463;

																					BgL__ortest_1115z00_5463 =
																						(BgL_classz00_5446 ==
																						BgL_oclassz00_5462);
																					if (BgL__ortest_1115z00_5463)
																						{	/* Cgen/cgen.scm 817 */
																							BgL_res2854z00_5479 =
																								BgL__ortest_1115z00_5463;
																						}
																					else
																						{	/* Cgen/cgen.scm 817 */
																							long BgL_odepthz00_5464;

																							{	/* Cgen/cgen.scm 817 */
																								obj_t BgL_arg1804z00_5465;

																								BgL_arg1804z00_5465 =
																									(BgL_oclassz00_5462);
																								BgL_odepthz00_5464 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_5465);
																							}
																							if ((3L < BgL_odepthz00_5464))
																								{	/* Cgen/cgen.scm 817 */
																									obj_t BgL_arg1802z00_5467;

																									{	/* Cgen/cgen.scm 817 */
																										obj_t BgL_arg1803z00_5468;

																										BgL_arg1803z00_5468 =
																											(BgL_oclassz00_5462);
																										BgL_arg1802z00_5467 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_5468, 3L);
																									}
																									BgL_res2854z00_5479 =
																										(BgL_arg1802z00_5467 ==
																										BgL_classz00_5446);
																								}
																							else
																								{	/* Cgen/cgen.scm 817 */
																									BgL_res2854z00_5479 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test3264z00_11160 =
																				BgL_res2854z00_5479;
																		}
																}
															else
																{	/* Cgen/cgen.scm 817 */
																	BgL_test3264z00_11160 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test3264z00_11160)
														{	/* Cgen/cgen.scm 817 */
															BgL_test3263z00_11159 =
																CBOOL(
																(((BgL_makezd2boxzd2_bglt) COBJECT(
																			((BgL_makezd2boxzd2_bglt)
																				CDR(
																					((obj_t) BgL_xz00_3128)))))->
																	BgL_stackablez00));
														}
													else
														{	/* Cgen/cgen.scm 817 */
															BgL_test3263z00_11159 = ((bool_t) 0);
														}
												}
												if (BgL_test3263z00_11159)
													{	/* Cgen/cgen.scm 820 */
														BgL_makezd2boxzd2_bglt BgL_i1355z00_3281;

														BgL_i1355z00_3281 =
															((BgL_makezd2boxzd2_bglt)
															CDR(((obj_t) BgL_xz00_3128)));
														{	/* Cgen/cgen.scm 821 */
															obj_t BgL_arg2431z00_3282;

															BgL_arg2431z00_3282 =
																(((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_i1355z00_3281)))->
																BgL_locz00);
															{	/* Cgen/cgen.scm 821 */
																obj_t BgL_list2432z00_3283;

																{	/* Cgen/cgen.scm 821 */
																	obj_t BgL_arg2434z00_3284;

																	{	/* Cgen/cgen.scm 821 */
																		obj_t BgL_arg2435z00_3285;

																		BgL_arg2435z00_3285 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2956z00zzcgen_cgenz00, BNIL);
																		BgL_arg2434z00_3284 =
																			MAKE_YOUNG_PAIR(BgL_arg2431z00_3282,
																			BgL_arg2435z00_3285);
																	}
																	BgL_list2432z00_3283 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2959z00zzcgen_cgenz00,
																		BgL_arg2434z00_3284);
																}
																BGl_verbosez00zztools_speekz00(BINT(3L),
																	BgL_list2432z00_3283);
															}
														}
														{	/* Cgen/cgen.scm 822 */
															BgL_localz00_bglt BgL_declz00_3286;

															{	/* Cgen/cgen.scm 822 */
																BgL_localz00_bglt BgL_dz00_3294;

																{	/* Cgen/cgen.scm 822 */
																	BgL_variablez00_bglt
																		BgL_duplicated1358z00_3295;
																	BgL_localz00_bglt BgL_new1356z00_3296;

																	BgL_duplicated1358z00_3295 =
																		((BgL_variablez00_bglt)
																		CAR(((obj_t) BgL_xz00_3128)));
																	{	/* Cgen/cgen.scm 823 */
																		BgL_localz00_bglt BgL_new1362z00_3300;

																		BgL_new1362z00_3300 =
																			((BgL_localz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_localz00_bgl))));
																		{	/* Cgen/cgen.scm 823 */
																			long BgL_arg2442z00_3301;

																			BgL_arg2442z00_3301 =
																				BGL_CLASS_NUM(BGl_localz00zzast_varz00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1362z00_3300),
																				BgL_arg2442z00_3301);
																		}
																		{	/* Cgen/cgen.scm 823 */
																			BgL_objectz00_bglt BgL_tmpz00_11207;

																			BgL_tmpz00_11207 =
																				((BgL_objectz00_bglt)
																				BgL_new1362z00_3300);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11207,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1362z00_3300);
																		BgL_new1356z00_3296 = BgL_new1362z00_3300;
																	}
																	((((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->BgL_idz00) =
																		((obj_t)
																			BGl_gensymz00zz__r4_symbols_6_4z00
																			(CNST_TABLE_REF(16))), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_namez00) = ((obj_t) BFALSE), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2cellza2z00zztype_cachez00)),
																		BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_valuez00) =
																		((BgL_valuez00_bglt) ((
																					(BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1358z00_3295))->
																				BgL_valuez00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_accessz00) =
																		((obj_t) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1358z00_3295))->
																				BgL_accessz00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_fastzd2alphazd2) =
																		((obj_t) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1358z00_3295))->
																				BgL_fastzd2alphazd2)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_removablez00) =
																		((obj_t) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1358z00_3295))->
																				BgL_removablez00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_occurrencez00) =
																		((long) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1358z00_3295))->
																				BgL_occurrencez00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_occurrencewz00) =
																		((long) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1358z00_3295))->
																				BgL_occurrencewz00)), BUNSPEC);
																	((((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_new1356z00_3296)))->
																			BgL_userzf3zf3) =
																		((bool_t) (((BgL_variablez00_bglt)
																					COBJECT(BgL_duplicated1358z00_3295))->
																				BgL_userzf3zf3)), BUNSPEC);
																	((((BgL_localz00_bglt)
																				COBJECT(BgL_new1356z00_3296))->
																			BgL_keyz00) =
																		((long) (((BgL_localz00_bglt)
																					COBJECT(((BgL_localz00_bglt)
																							BgL_duplicated1358z00_3295)))->
																				BgL_keyz00)), BUNSPEC);
																	((((BgL_localz00_bglt)
																				COBJECT(BgL_new1356z00_3296))->
																			BgL_valzd2noescapezd2) =
																		((obj_t) (((BgL_localz00_bglt)
																					COBJECT(((BgL_localz00_bglt)
																							BgL_duplicated1358z00_3295)))->
																				BgL_valzd2noescapezd2)), BUNSPEC);
																	((((BgL_localz00_bglt)
																				COBJECT(BgL_new1356z00_3296))->
																			BgL_volatilez00) =
																		((bool_t) (((BgL_localz00_bglt)
																					COBJECT(((BgL_localz00_bglt)
																							BgL_duplicated1358z00_3295)))->
																				BgL_volatilez00)), BUNSPEC);
																	BgL_dz00_3294 = BgL_new1356z00_3296;
																}
																BGl_setzd2variablezd2namez12z12zzbackend_cplibz00
																	(((BgL_variablez00_bglt) BgL_dz00_3294));
																BgL_declz00_3286 = BgL_dz00_3294;
															}
															{	/* Cgen/cgen.scm 822 */
																BgL_cpragmaz00_bglt BgL_allocz00_3287;

																{	/* Cgen/cgen.scm 828 */
																	BgL_cpragmaz00_bglt BgL_new1364z00_3289;

																	{	/* Cgen/cgen.scm 830 */
																		BgL_cpragmaz00_bglt BgL_new1363z00_3292;

																		BgL_new1363z00_3292 =
																			((BgL_cpragmaz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_cpragmaz00_bgl))));
																		{	/* Cgen/cgen.scm 830 */
																			long BgL_arg2439z00_3293;

																			BgL_arg2439z00_3293 =
																				BGL_CLASS_NUM
																				(BGl_cpragmaz00zzcgen_copz00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1363z00_3292),
																				BgL_arg2439z00_3293);
																		}
																		BgL_new1364z00_3289 = BgL_new1363z00_3292;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1364z00_3289)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_i1355z00_3281)))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1364z00_3289)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2_za2z00zztype_cachez00)),
																		BUNSPEC);
																	{
																		obj_t BgL_auxz00_11263;

																		{	/* Cgen/cgen.scm 832 */
																			obj_t BgL_arg2437z00_3290;

																			BgL_arg2437z00_3290 =
																				(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_declz00_3286)))->BgL_namez00);
																			{	/* Cgen/cgen.scm 831 */
																				obj_t BgL_list2438z00_3291;

																				BgL_list2438z00_3291 =
																					MAKE_YOUNG_PAIR(BgL_arg2437z00_3290,
																					BNIL);
																				BgL_auxz00_11263 =
																					BGl_formatz00zz__r4_output_6_10_3z00
																					(BGl_string2960z00zzcgen_cgenz00,
																					BgL_list2438z00_3291);
																		}}
																		((((BgL_cpragmaz00_bglt)
																					COBJECT(BgL_new1364z00_3289))->
																				BgL_formatz00) =
																			((obj_t) BgL_auxz00_11263), BUNSPEC);
																	}
																	((((BgL_cpragmaz00_bglt)
																				COBJECT(BgL_new1364z00_3289))->
																			BgL_argsz00) = ((obj_t) BNIL), BUNSPEC);
																	BgL_allocz00_3287 = BgL_new1364z00_3289;
																}
																{	/* Cgen/cgen.scm 828 */

																	((((BgL_makezd2boxzd2_bglt)
																				COBJECT(BgL_i1355z00_3281))->
																			BgL_stackablez00) =
																		((obj_t) ((obj_t) BgL_declz00_3286)),
																		BUNSPEC);
																	{	/* Cgen/cgen.scm 835 */
																		obj_t BgL_list2436z00_3288;

																		BgL_list2436z00_3288 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_allocz00_3287), BNIL);
																		BgL_arg2315z00_3068 = BgL_list2436z00_3288;
													}}}}}
												else
													{	/* Cgen/cgen.scm 817 */
														BgL_arg2315z00_3068 = BNIL;
													}
											}
									}
							}
						}
						{	/* Cgen/cgen.scm 841 */
							obj_t BgL_arg2318z00_3071;

							BgL_arg2318z00_3071 = CDR(((obj_t) BgL_l1708z00_3065));
							BgL_arg2316z00_3069 =
								BGl_zc3z04anonymousza32313ze3ze70z60zzcgen_cgenz00
								(BgL_inpushexitz00_6719, BgL_arg2318z00_3071);
						}
						return bgl_append2(BgL_arg2315z00_3068, BgL_arg2316z00_3069);
					}
			}
		}

	}



/* &node->cop-let-fun1761 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2letzd2fun1761z53zzcgen_cgenz00(obj_t
		BgL_envz00_6648, obj_t BgL_nodez00_6649, obj_t BgL_kontz00_6650,
		obj_t BgL_inpushexitz00_6651)
	{
		{	/* Cgen/cgen.scm 688 */
			{
				obj_t BgL_localsz00_7324;
				obj_t BgL_allzd2formalszd2_7325;

				{	/* Tools/trace.sch 53 */
					obj_t BgL_arg2274z00_7356;

					BgL_arg2274z00_7356 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_6649)))->BgL_localsz00);
					{
						obj_t BgL_auxz00_11280;

						BgL_localsz00_7324 = BgL_arg2274z00_7356;
						BgL_allzd2formalszd2_7325 = BNIL;
					BgL_loopz00_7323:
						if (NULLP(BgL_localsz00_7324))
							{	/* Tools/trace.sch 53 */
								BgL_copz00_bglt BgL_arg2277z00_7326;

								{	/* Tools/trace.sch 53 */
									BgL_csequencez00_bglt BgL_arg2279z00_7327;
									obj_t BgL_arg2280z00_7328;

									{	/* Tools/trace.sch 53 */
										BgL_csequencez00_bglt BgL_new1281z00_7329;

										{	/* Tools/trace.sch 53 */
											BgL_csequencez00_bglt BgL_new1280z00_7330;

											BgL_new1280z00_7330 =
												((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_csequencez00_bgl))));
											{	/* Tools/trace.sch 53 */
												long BgL_arg2288z00_7331;

												BgL_arg2288z00_7331 =
													BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1280z00_7330),
													BgL_arg2288z00_7331);
											}
											BgL_new1281z00_7329 = BgL_new1280z00_7330;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1281z00_7329)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
																	BgL_nodez00_6649))))->BgL_locz00)), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1281z00_7329)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
																	BgL_nodez00_6649))))->BgL_typez00)), BUNSPEC);
										((((BgL_csequencez00_bglt) COBJECT(BgL_new1281z00_7329))->
												BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										{
											obj_t BgL_auxz00_11298;

											{	/* Tools/trace.sch 53 */
												BgL_localzd2varzd2_bglt BgL_arg2281z00_7332;
												BgL_copz00_bglt BgL_arg2282z00_7333;

												{	/* Tools/trace.sch 53 */
													BgL_localzd2varzd2_bglt BgL_new1283z00_7334;

													{	/* Tools/trace.sch 53 */
														BgL_localzd2varzd2_bglt BgL_new1282z00_7335;

														BgL_new1282z00_7335 =
															((BgL_localzd2varzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_localzd2varzd2_bgl))));
														{	/* Tools/trace.sch 53 */
															long BgL_arg2286z00_7336;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_classz00_7337;

																BgL_classz00_7337 =
																	BGl_localzd2varzd2zzcgen_copz00;
																BgL_arg2286z00_7336 =
																	BGL_CLASS_NUM(BgL_classz00_7337);
															}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1282z00_7335),
																BgL_arg2286z00_7336);
														}
														BgL_new1283z00_7334 = BgL_new1282z00_7335;
													}
													((((BgL_copz00_bglt) COBJECT(
																	((BgL_copz00_bglt) BgL_new1283z00_7334)))->
															BgL_locz00) =
														((obj_t) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt) (
																				(BgL_letzd2funzd2_bglt)
																				BgL_nodez00_6649))))->BgL_locz00)),
														BUNSPEC);
													((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																		BgL_new1283z00_7334)))->BgL_typez00) =
														((BgL_typez00_bglt) ((BgL_typez00_bglt)
																BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
													((((BgL_localzd2varzd2_bglt)
																COBJECT(BgL_new1283z00_7334))->BgL_varsz00) =
														((obj_t) BgL_allzd2formalszd2_7325), BUNSPEC);
													BgL_arg2281z00_7332 = BgL_new1283z00_7334;
												}
												{	/* Tools/trace.sch 53 */
													BgL_nodez00_bglt BgL_arg2287z00_7338;

													BgL_arg2287z00_7338 =
														(((BgL_letzd2funzd2_bglt) COBJECT(
																((BgL_letzd2funzd2_bglt) BgL_nodez00_6649)))->
														BgL_bodyz00);
													BgL_arg2282z00_7333 =
														BGl_nodezd2ze3copz31zzcgen_cgenz00
														(BgL_arg2287z00_7338, BgL_kontz00_6650,
														CBOOL(BgL_inpushexitz00_6651));
												}
												{	/* Tools/trace.sch 53 */
													obj_t BgL_list2283z00_7339;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg2284z00_7340;

														BgL_arg2284z00_7340 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2282z00_7333), BNIL);
														BgL_list2283z00_7339 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2281z00_7332),
															BgL_arg2284z00_7340);
													}
													BgL_auxz00_11298 = BgL_list2283z00_7339;
											}}
											((((BgL_csequencez00_bglt) COBJECT(BgL_new1281z00_7329))->
													BgL_copsz00) = ((obj_t) BgL_auxz00_11298), BUNSPEC);
										}
										BgL_arg2279z00_7327 = BgL_new1281z00_7329;
									}
									BgL_arg2280z00_7328 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_letzd2funzd2_bglt) BgL_nodez00_6649))))->
										BgL_locz00);
									BgL_arg2277z00_7326 =
										BGl_bdbzd2letzd2varz00zzcgen_cgenz00(((BgL_copz00_bglt)
											BgL_arg2279z00_7327), BgL_arg2280z00_7328);
								}
								BgL_auxz00_11280 =
									BGl_blockzd2kontzd2zzcgen_cgenz00(
									((obj_t) BgL_arg2277z00_7326), BFALSE);
							}
						else
							{	/* Tools/trace.sch 53 */
								obj_t BgL_localz00_7341;

								BgL_localz00_7341 = CAR(((obj_t) BgL_localsz00_7324));
								BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
									((BgL_variablez00_bglt) BgL_localz00_7341));
								{	/* Tools/trace.sch 53 */
									BgL_sfunz00_bglt BgL_funz00_7342;

									{	/* Tools/trace.sch 53 */
										BgL_sfunz00_bglt BgL_tmp1284z00_7343;

										BgL_tmp1284z00_7343 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_localz00_7341))))->
												BgL_valuez00));
										{	/* Tools/trace.sch 53 */
											BgL_sfunzf2czf2_bglt BgL_wide1286z00_7344;

											BgL_wide1286z00_7344 =
												((BgL_sfunzf2czf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_sfunzf2czf2_bgl))));
											{	/* Tools/trace.sch 53 */
												obj_t BgL_auxz00_11340;
												BgL_objectz00_bglt BgL_tmpz00_11337;

												BgL_auxz00_11340 = ((obj_t) BgL_wide1286z00_7344);
												BgL_tmpz00_11337 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_tmp1284z00_7343));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_11337,
													BgL_auxz00_11340);
											}
											((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_tmp1284z00_7343));
											{	/* Tools/trace.sch 53 */
												long BgL_arg2295z00_7345;

												BgL_arg2295z00_7345 =
													BGL_CLASS_NUM(BGl_sfunzf2Czf2zzcgen_copz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_tmp1284z00_7343)),
													BgL_arg2295z00_7345);
											}
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_tmp1284z00_7343));
										}
										{
											BgL_clabelz00_bglt BgL_auxz00_11358;
											BgL_sfunzf2czf2_bglt BgL_auxz00_11351;

											{	/* Tools/trace.sch 53 */
												BgL_clabelz00_bglt BgL_new1289z00_7346;

												{	/* Tools/trace.sch 53 */
													BgL_clabelz00_bglt BgL_new1288z00_7347;

													BgL_new1288z00_7347 =
														((BgL_clabelz00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_clabelz00_bgl))));
													{	/* Tools/trace.sch 53 */
														long BgL_arg2297z00_7348;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_classz00_7349;

															BgL_classz00_7349 = BGl_clabelz00zzcgen_copz00;
															BgL_arg2297z00_7348 =
																BGL_CLASS_NUM(BgL_classz00_7349);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1288z00_7347),
															BgL_arg2297z00_7348);
													}
													BgL_new1289z00_7346 = BgL_new1288z00_7347;
												}
												((((BgL_copz00_bglt) COBJECT(
																((BgL_copz00_bglt) BgL_new1289z00_7346)))->
														BgL_locz00) =
													((obj_t) (((BgL_sfunz00_bglt)
																COBJECT(((BgL_sfunz00_bglt) ((
																				(BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt) (
																							(BgL_localz00_bglt)
																							BgL_localz00_7341))))->
																			BgL_valuez00))))->BgL_locz00)), BUNSPEC);
												((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																	BgL_new1289z00_7346)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BGl_za2voidza2z00zztype_cachez00)), BUNSPEC);
												((((BgL_clabelz00_bglt) COBJECT(BgL_new1289z00_7346))->
														BgL_namez00) =
													((obj_t) (((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt) (
																			(BgL_localz00_bglt)
																			BgL_localz00_7341))))->BgL_namez00)),
													BUNSPEC);
												((((BgL_clabelz00_bglt) COBJECT(BgL_new1289z00_7346))->
														BgL_usedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
												((((BgL_clabelz00_bglt) COBJECT(BgL_new1289z00_7346))->
														BgL_bodyz00) = ((obj_t) BUNSPEC), BUNSPEC);
												BgL_auxz00_11358 = BgL_new1289z00_7346;
											}
											{
												obj_t BgL_auxz00_11352;

												{	/* Tools/trace.sch 53 */
													BgL_objectz00_bglt BgL_tmpz00_11353;

													BgL_tmpz00_11353 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_tmp1284z00_7343));
													BgL_auxz00_11352 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_11353);
												}
												BgL_auxz00_11351 =
													((BgL_sfunzf2czf2_bglt) BgL_auxz00_11352);
											}
											((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_11351))->
													BgL_labelz00) =
												((BgL_clabelz00_bglt) BgL_auxz00_11358), BUNSPEC);
										}
										{
											BgL_sfunzf2czf2_bglt BgL_auxz00_11380;

											{
												obj_t BgL_auxz00_11381;

												{	/* Tools/trace.sch 53 */
													BgL_objectz00_bglt BgL_tmpz00_11382;

													BgL_tmpz00_11382 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_tmp1284z00_7343));
													BgL_auxz00_11381 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_11382);
												}
												BgL_auxz00_11380 =
													((BgL_sfunzf2czf2_bglt) BgL_auxz00_11381);
											}
											((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_11380))->
													BgL_integratedz00) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										BgL_funz00_7342 = ((BgL_sfunz00_bglt) BgL_tmp1284z00_7343);
									}
									{	/* Tools/trace.sch 53 */
										obj_t BgL_formalsz00_7350;

										BgL_formalsz00_7350 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_7342)))->BgL_argsz00);
										{	/* Tools/trace.sch 53 */

											{
												obj_t BgL_l1692z00_7352;

												BgL_l1692z00_7352 = BgL_formalsz00_7350;
											BgL_zc3z04anonymousza32289ze3z87_7351:
												if (PAIRP(BgL_l1692z00_7352))
													{	/* Tools/trace.sch 53 */
														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg2291z00_7353;

															BgL_arg2291z00_7353 = CAR(BgL_l1692z00_7352);
															BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
																((BgL_variablez00_bglt) BgL_arg2291z00_7353));
														}
														{
															obj_t BgL_l1692z00_11396;

															BgL_l1692z00_11396 = CDR(BgL_l1692z00_7352);
															BgL_l1692z00_7352 = BgL_l1692z00_11396;
															goto BgL_zc3z04anonymousza32289ze3z87_7351;
														}
													}
												else
													{	/* Tools/trace.sch 53 */
														((bool_t) 1);
													}
											}
											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg2293z00_7354;
												obj_t BgL_arg2294z00_7355;

												BgL_arg2293z00_7354 = CDR(((obj_t) BgL_localsz00_7324));
												BgL_arg2294z00_7355 =
													BGl_appendzd221011zd2zzcgen_cgenz00
													(BgL_formalsz00_7350, BgL_allzd2formalszd2_7325);
												{
													obj_t BgL_allzd2formalszd2_11402;
													obj_t BgL_localsz00_11401;

													BgL_localsz00_11401 = BgL_arg2293z00_7354;
													BgL_allzd2formalszd2_11402 = BgL_arg2294z00_7355;
													BgL_allzd2formalszd2_7325 =
														BgL_allzd2formalszd2_11402;
													BgL_localsz00_7324 = BgL_localsz00_11401;
													goto BgL_loopz00_7323;
												}
											}
										}
									}
								}
							}
						return ((BgL_copz00_bglt) BgL_auxz00_11280);
					}
				}
			}
		}

	}



/* &node->cop-switch1759 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2switch1759z81zzcgen_cgenz00(obj_t
		BgL_envz00_6652, obj_t BgL_nodez00_6653, obj_t BgL_kontz00_6654,
		obj_t BgL_inpushexitz00_6655)
	{
		{	/* Cgen/cgen.scm 648 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_cclausesz00_7358;

				{	/* Tools/trace.sch 53 */
					obj_t BgL_l1687z00_7359;

					BgL_l1687z00_7359 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_6653)))->BgL_clausesz00);
					if (NULLP(BgL_l1687z00_7359))
						{	/* Tools/trace.sch 53 */
							BgL_cclausesz00_7358 = BNIL;
						}
					else
						{	/* Tools/trace.sch 53 */
							obj_t BgL_head1689z00_7360;

							BgL_head1689z00_7360 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1687z00_7362;
								obj_t BgL_tail1690z00_7363;

								BgL_l1687z00_7362 = BgL_l1687z00_7359;
								BgL_tail1690z00_7363 = BgL_head1689z00_7360;
							BgL_zc3z04anonymousza32267ze3z87_7361:
								if (NULLP(BgL_l1687z00_7362))
									{	/* Tools/trace.sch 53 */
										BgL_cclausesz00_7358 = CDR(BgL_head1689z00_7360);
									}
								else
									{	/* Tools/trace.sch 53 */
										obj_t BgL_newtail1691z00_7364;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg2270z00_7365;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_clausez00_7366;

												BgL_clausez00_7366 = CAR(((obj_t) BgL_l1687z00_7362));
												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg2271z00_7367;
													BgL_copz00_bglt BgL_arg2272z00_7368;

													BgL_arg2271z00_7367 =
														CAR(((obj_t) BgL_clausez00_7366));
													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg2273z00_7369;

														BgL_arg2273z00_7369 =
															CDR(((obj_t) BgL_clausez00_7366));
														BgL_arg2272z00_7368 =
															BGl_nodezd2ze3copz31zzcgen_cgenz00(
															((BgL_nodez00_bglt) BgL_arg2273z00_7369),
															BgL_kontz00_6654, CBOOL(BgL_inpushexitz00_6655));
													}
													BgL_arg2270z00_7365 =
														MAKE_YOUNG_PAIR(BgL_arg2271z00_7367,
														((obj_t) BgL_arg2272z00_7368));
												}
											}
											BgL_newtail1691z00_7364 =
												MAKE_YOUNG_PAIR(BgL_arg2270z00_7365, BNIL);
										}
										SET_CDR(BgL_tail1690z00_7363, BgL_newtail1691z00_7364);
										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg2269z00_7370;

											BgL_arg2269z00_7370 = CDR(((obj_t) BgL_l1687z00_7362));
											{
												obj_t BgL_tail1690z00_11428;
												obj_t BgL_l1687z00_11427;

												BgL_l1687z00_11427 = BgL_arg2269z00_7370;
												BgL_tail1690z00_11428 = BgL_newtail1691z00_7364;
												BgL_tail1690z00_7363 = BgL_tail1690z00_11428;
												BgL_l1687z00_7362 = BgL_l1687z00_11427;
												goto BgL_zc3z04anonymousza32267ze3z87_7361;
											}
										}
									}
							}
						}
				}
				{	/* Tools/trace.sch 53 */
					BgL_localz00_bglt BgL_auxz00_7371;

					{	/* Tools/trace.sch 53 */
						BgL_typez00_bglt BgL_arg2265z00_7372;

						BgL_arg2265z00_7372 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_6653)))->
							BgL_itemzd2typezd2);
						{	/* Tools/trace.sch 53 */
							BgL_localz00_bglt BgL_res2843z00_7373;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_idz00_7374;

								BgL_idz00_7374 = CNST_TABLE_REF(3);
								{	/* Tools/trace.sch 53 */
									BgL_localz00_bglt BgL_localz00_7375;

									BgL_localz00_7375 =
										BGl_makezd2localzd2svarz00zzast_localz00(BgL_idz00_7374,
										BgL_arg2265z00_7372);
									{	/* Tools/trace.sch 53 */
										bool_t BgL_test3273z00_11433;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_tmpz00_11434;

											BgL_tmpz00_11434 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_localz00_7375)))->
												BgL_namez00);
											BgL_test3273z00_11433 = STRINGP(BgL_tmpz00_11434);
										}
										if (BgL_test3273z00_11433)
											{	/* Tools/trace.sch 53 */
												BFALSE;
											}
										else
											{	/* Tools/trace.sch 53 */
												BGl_errorz00zz__errorz00
													(BGl_string2915z00zzcgen_cgenz00,
													BGl_string2916z00zzcgen_cgenz00,
													((obj_t) BgL_localz00_7375));
											}
									}
									BgL_res2843z00_7373 = BgL_localz00_7375;
								}
							}
							BgL_auxz00_7371 = BgL_res2843z00_7373;
						}
					}
					{	/* Tools/trace.sch 53 */
						BgL_copz00_bglt BgL_copz00_7376;

						{	/* Tools/trace.sch 53 */
							BgL_setqz00_bglt BgL_arg2263z00_7377;

							{	/* Tools/trace.sch 53 */
								BgL_nodez00_bglt BgL_arg2264z00_7378;

								BgL_arg2264z00_7378 =
									(((BgL_switchz00_bglt) COBJECT(
											((BgL_switchz00_bglt) BgL_nodez00_6653)))->BgL_testz00);
								BgL_arg2263z00_7377 =
									BGl_nodezd2setqzd2zzcgen_cgenz00(
									((BgL_variablez00_bglt) BgL_auxz00_7371),
									BgL_arg2264z00_7378);
							}
							BgL_copz00_7376 =
								BGl_nodezd2ze3copz31zzcgen_cgenz00(
								((BgL_nodez00_bglt) BgL_arg2263z00_7377),
								BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
								CBOOL(BgL_inpushexitz00_6655));
						}
						{	/* Tools/trace.sch 53 */

							{	/* Tools/trace.sch 53 */
								bool_t BgL_test3274z00_11447;

								{	/* Tools/trace.sch 53 */
									bool_t BgL_test3275z00_11448;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_classz00_7379;

										BgL_classz00_7379 = BGl_csetqz00zzcgen_copz00;
										{	/* Tools/trace.sch 53 */
											BgL_objectz00_bglt BgL_arg1807z00_7380;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_tmpz00_11449;

												BgL_tmpz00_11449 =
													((obj_t) ((BgL_objectz00_bglt) BgL_copz00_7376));
												BgL_arg1807z00_7380 =
													(BgL_objectz00_bglt) (BgL_tmpz00_11449);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Tools/trace.sch 53 */
													long BgL_idxz00_7381;

													BgL_idxz00_7381 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_7380);
													BgL_test3275z00_11448 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_7381 + 2L)) == BgL_classz00_7379);
												}
											else
												{	/* Tools/trace.sch 53 */
													bool_t BgL_res2844z00_7384;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_oclassz00_7385;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1815z00_7386;
															long BgL_arg1816z00_7387;

															BgL_arg1815z00_7386 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Tools/trace.sch 53 */
																long BgL_arg1817z00_7388;

																BgL_arg1817z00_7388 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_7380);
																BgL_arg1816z00_7387 =
																	(BgL_arg1817z00_7388 - OBJECT_TYPE);
															}
															BgL_oclassz00_7385 =
																VECTOR_REF(BgL_arg1815z00_7386,
																BgL_arg1816z00_7387);
														}
														{	/* Tools/trace.sch 53 */
															bool_t BgL__ortest_1115z00_7389;

															BgL__ortest_1115z00_7389 =
																(BgL_classz00_7379 == BgL_oclassz00_7385);
															if (BgL__ortest_1115z00_7389)
																{	/* Tools/trace.sch 53 */
																	BgL_res2844z00_7384 =
																		BgL__ortest_1115z00_7389;
																}
															else
																{	/* Tools/trace.sch 53 */
																	long BgL_odepthz00_7390;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1804z00_7391;

																		BgL_arg1804z00_7391 = (BgL_oclassz00_7385);
																		BgL_odepthz00_7390 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_7391);
																	}
																	if ((2L < BgL_odepthz00_7390))
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1802z00_7392;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1803z00_7393;

																				BgL_arg1803z00_7393 =
																					(BgL_oclassz00_7385);
																				BgL_arg1802z00_7392 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_7393, 2L);
																			}
																			BgL_res2844z00_7384 =
																				(BgL_arg1802z00_7392 ==
																				BgL_classz00_7379);
																		}
																	else
																		{	/* Tools/trace.sch 53 */
																			BgL_res2844z00_7384 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3275z00_11448 = BgL_res2844z00_7384;
												}
										}
									}
									if (BgL_test3275z00_11448)
										{	/* Tools/trace.sch 53 */
											BgL_test3274z00_11447 =
												(
												((obj_t)
													(((BgL_varcz00_bglt) COBJECT(
																(((BgL_csetqz00_bglt) COBJECT(
																			((BgL_csetqz00_bglt) BgL_copz00_7376)))->
																	BgL_varz00)))->BgL_variablez00)) ==
												((obj_t) BgL_auxz00_7371));
										}
									else
										{	/* Tools/trace.sch 53 */
											BgL_test3274z00_11447 = ((bool_t) 0);
										}
								}
								if (BgL_test3274z00_11447)
									{	/* Tools/trace.sch 53 */
										BgL_cswitchz00_bglt BgL_new1267z00_7394;

										{	/* Tools/trace.sch 53 */
											BgL_cswitchz00_bglt BgL_new1266z00_7395;

											BgL_new1266z00_7395 =
												((BgL_cswitchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_cswitchz00_bgl))));
											{	/* Tools/trace.sch 53 */
												long BgL_arg2249z00_7396;

												BgL_arg2249z00_7396 =
													BGL_CLASS_NUM(BGl_cswitchz00zzcgen_copz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1266z00_7395),
													BgL_arg2249z00_7396);
											}
											BgL_new1267z00_7394 = BgL_new1266z00_7395;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1267z00_7394)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_switchz00_bglt)
																	BgL_nodez00_6653))))->BgL_locz00)), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1267z00_7394)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_switchz00_bglt)
																	BgL_nodez00_6653))))->BgL_typez00)), BUNSPEC);
										((((BgL_cswitchz00_bglt) COBJECT(BgL_new1267z00_7394))->
												BgL_testz00) =
											((BgL_copz00_bglt) (((BgL_csetqz00_bglt)
														COBJECT(((BgL_csetqz00_bglt) BgL_copz00_7376)))->
													BgL_valuez00)), BUNSPEC);
										((((BgL_cswitchz00_bglt) COBJECT(BgL_new1267z00_7394))->
												BgL_clausesz00) =
											((obj_t) BgL_cclausesz00_7358), BUNSPEC);
										return ((BgL_copz00_bglt) BgL_new1267z00_7394);
									}
								else
									{	/* Tools/trace.sch 53 */
										BgL_cblockz00_bglt BgL_new1269z00_7397;

										{	/* Tools/trace.sch 53 */
											BgL_cblockz00_bglt BgL_new1268z00_7398;

											BgL_new1268z00_7398 =
												((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_cblockz00_bgl))));
											{	/* Tools/trace.sch 53 */
												long BgL_arg2260z00_7399;

												BgL_arg2260z00_7399 =
													BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1268z00_7398),
													BgL_arg2260z00_7399);
											}
											BgL_new1269z00_7397 = BgL_new1268z00_7398;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1269z00_7397)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_switchz00_bglt)
																	BgL_nodez00_6653))))->BgL_locz00)), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1269z00_7397)))->BgL_typez00) =
											((BgL_typez00_bglt) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_switchz00_bglt)
																	BgL_nodez00_6653))))->BgL_typez00)), BUNSPEC);
										{
											BgL_copz00_bglt BgL_auxz00_11511;

											{	/* Tools/trace.sch 53 */
												BgL_csequencez00_bglt BgL_new1271z00_7400;

												{	/* Tools/trace.sch 53 */
													BgL_csequencez00_bglt BgL_new1270z00_7401;

													BgL_new1270z00_7401 =
														((BgL_csequencez00_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_csequencez00_bgl))));
													{	/* Tools/trace.sch 53 */
														long BgL_arg2259z00_7402;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_classz00_7403;

															BgL_classz00_7403 = BGl_csequencez00zzcgen_copz00;
															BgL_arg2259z00_7402 =
																BGL_CLASS_NUM(BgL_classz00_7403);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1270z00_7401),
															BgL_arg2259z00_7402);
													}
													BgL_new1271z00_7400 = BgL_new1270z00_7401;
												}
												((((BgL_copz00_bglt) COBJECT(
																((BgL_copz00_bglt) BgL_new1271z00_7400)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) (
																			(BgL_switchz00_bglt)
																			BgL_nodez00_6653))))->BgL_locz00)),
													BUNSPEC);
												((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																	BgL_new1271z00_7400)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) (
																			(BgL_switchz00_bglt)
																			BgL_nodez00_6653))))->BgL_typez00)),
													BUNSPEC);
												((((BgL_csequencez00_bglt)
															COBJECT(BgL_new1271z00_7400))->
														BgL_czd2expzf3z21) =
													((bool_t) ((bool_t) 0)), BUNSPEC);
												{
													obj_t BgL_auxz00_11527;

													{	/* Tools/trace.sch 53 */
														BgL_localzd2varzd2_bglt BgL_arg2250z00_7404;
														BgL_cswitchz00_bglt BgL_arg2251z00_7405;

														{	/* Tools/trace.sch 53 */
															BgL_localzd2varzd2_bglt BgL_new1273z00_7406;

															{	/* Tools/trace.sch 53 */
																BgL_localzd2varzd2_bglt BgL_new1272z00_7407;

																BgL_new1272z00_7407 =
																	((BgL_localzd2varzd2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_localzd2varzd2_bgl))));
																{	/* Tools/trace.sch 53 */
																	long BgL_arg2256z00_7408;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_classz00_7409;

																		BgL_classz00_7409 =
																			BGl_localzd2varzd2zzcgen_copz00;
																		BgL_arg2256z00_7408 =
																			BGL_CLASS_NUM(BgL_classz00_7409);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1272z00_7407),
																		BgL_arg2256z00_7408);
																}
																BgL_new1273z00_7406 = BgL_new1272z00_7407;
															}
															((((BgL_copz00_bglt) COBJECT(
																			((BgL_copz00_bglt)
																				BgL_new1273z00_7406)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_switchz00_bglt)
																						BgL_nodez00_6653))))->BgL_locz00)),
																BUNSPEC);
															((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																				BgL_new1273z00_7406)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
															{
																obj_t BgL_auxz00_11540;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_list2255z00_7410;

																	BgL_list2255z00_7410 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_auxz00_7371), BNIL);
																	BgL_auxz00_11540 = BgL_list2255z00_7410;
																}
																((((BgL_localzd2varzd2_bglt)
																			COBJECT(BgL_new1273z00_7406))->
																		BgL_varsz00) =
																	((obj_t) BgL_auxz00_11540), BUNSPEC);
															}
															BgL_arg2250z00_7404 = BgL_new1273z00_7406;
														}
														{	/* Tools/trace.sch 53 */
															BgL_cswitchz00_bglt BgL_new1275z00_7411;

															{	/* Tools/trace.sch 53 */
																BgL_cswitchz00_bglt BgL_new1274z00_7412;

																BgL_new1274z00_7412 =
																	((BgL_cswitchz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_cswitchz00_bgl))));
																{	/* Tools/trace.sch 53 */
																	long BgL_arg2258z00_7413;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_classz00_7414;

																		BgL_classz00_7414 =
																			BGl_cswitchz00zzcgen_copz00;
																		BgL_arg2258z00_7413 =
																			BGL_CLASS_NUM(BgL_classz00_7414);
																	}
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1274z00_7412),
																		BgL_arg2258z00_7413);
																}
																BgL_new1275z00_7411 = BgL_new1274z00_7412;
															}
															((((BgL_copz00_bglt) COBJECT(
																			((BgL_copz00_bglt)
																				BgL_new1275z00_7411)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_switchz00_bglt)
																						BgL_nodez00_6653))))->BgL_locz00)),
																BUNSPEC);
															((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																				BgL_new1275z00_7411)))->BgL_typez00) =
																((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_switchz00_bglt)
																						BgL_nodez00_6653))))->BgL_typez00)),
																BUNSPEC);
															{
																BgL_copz00_bglt BgL_auxz00_11558;

																{	/* Tools/trace.sch 53 */
																	BgL_varcz00_bglt BgL_new1277z00_7415;

																	{	/* Tools/trace.sch 53 */
																		BgL_varcz00_bglt BgL_new1276z00_7416;

																		BgL_new1276z00_7416 =
																			((BgL_varcz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_varcz00_bgl))));
																		{	/* Tools/trace.sch 53 */
																			long BgL_arg2257z00_7417;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_classz00_7418;

																				BgL_classz00_7418 =
																					BGl_varcz00zzcgen_copz00;
																				BgL_arg2257z00_7417 =
																					BGL_CLASS_NUM(BgL_classz00_7418);
																			}
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1276z00_7416),
																				BgL_arg2257z00_7417);
																		}
																		BgL_new1277z00_7415 = BgL_new1276z00_7416;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1277z00_7415)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_switchz00_bglt)
																								BgL_nodez00_6653))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1277z00_7415)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt)
																							BgL_auxz00_7371)))->BgL_typez00)),
																		BUNSPEC);
																	((((BgL_varcz00_bglt)
																				COBJECT(BgL_new1277z00_7415))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_auxz00_7371)), BUNSPEC);
																	BgL_auxz00_11558 =
																		((BgL_copz00_bglt) BgL_new1277z00_7415);
																}
																((((BgL_cswitchz00_bglt)
																			COBJECT(BgL_new1275z00_7411))->
																		BgL_testz00) =
																	((BgL_copz00_bglt) BgL_auxz00_11558),
																	BUNSPEC);
															}
															((((BgL_cswitchz00_bglt)
																		COBJECT(BgL_new1275z00_7411))->
																	BgL_clausesz00) =
																((obj_t) BgL_cclausesz00_7358), BUNSPEC);
															BgL_arg2251z00_7405 = BgL_new1275z00_7411;
														}
														{	/* Tools/trace.sch 53 */
															obj_t BgL_list2252z00_7419;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg2253z00_7420;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg2254z00_7421;

																	BgL_arg2254z00_7421 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg2251z00_7405), BNIL);
																	BgL_arg2253z00_7420 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_copz00_7376),
																		BgL_arg2254z00_7421);
																}
																BgL_list2252z00_7419 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg2250z00_7404),
																	BgL_arg2253z00_7420);
															}
															BgL_auxz00_11527 = BgL_list2252z00_7419;
													}}
													((((BgL_csequencez00_bglt)
																COBJECT(BgL_new1271z00_7400))->BgL_copsz00) =
														((obj_t) BgL_auxz00_11527), BUNSPEC);
												}
												BgL_auxz00_11511 =
													((BgL_copz00_bglt) BgL_new1271z00_7400);
											}
											((((BgL_cblockz00_bglt) COBJECT(BgL_new1269z00_7397))->
													BgL_bodyz00) =
												((BgL_copz00_bglt) BgL_auxz00_11511), BUNSPEC);
										}
										return ((BgL_copz00_bglt) BgL_new1269z00_7397);
									}
							}
						}
					}
				}
			}
		}

	}



/* &node->cop-fail1757 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2fail1757z81zzcgen_cgenz00(obj_t
		BgL_envz00_6656, obj_t BgL_nodez00_6657, obj_t BgL_kontz00_6658,
		obj_t BgL_inpushexitz00_6659)
	{
		{	/* Cgen/cgen.scm 627 */
			{	/* Tools/trace.sch 53 */
				BgL_typez00_bglt BgL_arg2232z00_7423;
				obj_t BgL_arg2233z00_7424;
				obj_t BgL_arg2234z00_7425;

				BgL_arg2232z00_7423 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_failz00_bglt) BgL_nodez00_6657))))->BgL_typez00);
				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg2236z00_7426;
					BgL_nodez00_bglt BgL_arg2237z00_7427;
					BgL_nodez00_bglt BgL_arg2238z00_7428;

					BgL_arg2236z00_7426 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6657)))->BgL_procz00);
					BgL_arg2237z00_7427 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6657)))->BgL_msgz00);
					BgL_arg2238z00_7428 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6657)))->BgL_objz00);
					{	/* Tools/trace.sch 53 */
						obj_t BgL_list2239z00_7429;

						{	/* Tools/trace.sch 53 */
							obj_t BgL_arg2240z00_7430;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_arg2241z00_7431;

								BgL_arg2241z00_7431 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg2238z00_7428), BNIL);
								BgL_arg2240z00_7430 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg2237z00_7427), BgL_arg2241z00_7431);
							}
							BgL_list2239z00_7429 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_arg2236z00_7426), BgL_arg2240z00_7430);
						}
						BgL_arg2233z00_7424 = BgL_list2239z00_7429;
					}
				}
				BgL_arg2234z00_7425 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_failz00_bglt) BgL_nodez00_6657))))->BgL_locz00);
				{	/* Tools/trace.sch 53 */
					obj_t BgL_zc3z04anonymousza32242ze3z87_7432;

					{	/* Tools/trace.sch 53 */
						int BgL_tmpz00_11605;

						BgL_tmpz00_11605 = (int) (1L);
						BgL_zc3z04anonymousza32242ze3z87_7432 =
							MAKE_L_PROCEDURE
							(BGl_z62zc3z04anonymousza32242ze3ze5zzcgen_cgenz00,
							BgL_tmpz00_11605);
					}
					PROCEDURE_L_SET(BgL_zc3z04anonymousza32242ze3z87_7432,
						(int) (0L), ((obj_t) ((BgL_failz00_bglt) BgL_nodez00_6657)));
					return
						((BgL_copz00_bglt)
						BGl_nodezd2argszd2ze3copze3zzcgen_cgenz00(BgL_arg2232z00_7423,
							BgL_arg2233z00_7424, ((bool_t) 0), BgL_arg2234z00_7425,
							BgL_zc3z04anonymousza32242ze3z87_7432, BgL_inpushexitz00_6659));
				}
			}
		}

	}



/* &<@anonymous:2242> */
	BgL_cfailz00_bglt BGl_z62zc3z04anonymousza32242ze3ze5zzcgen_cgenz00(obj_t
		BgL_envz00_6660, obj_t BgL_newzd2argszd2_6662)
	{
		{	/* Cgen/cgen.scm 635 */
			{	/* Cgen/cgen.scm 637 */
				BgL_failz00_bglt BgL_i1262z00_6661;

				BgL_i1262z00_6661 =
					((BgL_failz00_bglt) PROCEDURE_L_REF(BgL_envz00_6660, (int) (0L)));
				{	/* Cgen/cgen.scm 637 */
					BgL_cfailz00_bglt BgL_arg2243z00_7434;

					{	/* Cgen/cgen.scm 637 */
						BgL_cfailz00_bglt BgL_new1264z00_7435;

						{	/* Cgen/cgen.scm 639 */
							BgL_cfailz00_bglt BgL_new1263z00_7436;

							BgL_new1263z00_7436 =
								((BgL_cfailz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cfailz00_bgl))));
							{	/* Cgen/cgen.scm 639 */
								long BgL_arg2244z00_7437;

								BgL_arg2244z00_7437 = BGL_CLASS_NUM(BGl_cfailz00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1263z00_7436),
									BgL_arg2244z00_7437);
							}
							BgL_new1264z00_7435 = BgL_new1263z00_7436;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1264z00_7435)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_i1262z00_6661)))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1264z00_7435)))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
						((((BgL_cfailz00_bglt) COBJECT(BgL_new1264z00_7435))->BgL_procz00) =
							((BgL_copz00_bglt) ((BgL_copz00_bglt) CAR(((obj_t)
											BgL_newzd2argszd2_6662)))), BUNSPEC);
						{
							BgL_copz00_bglt BgL_auxz00_11632;

							{	/* Cgen/cgen.scm 641 */
								obj_t BgL_pairz00_7438;

								BgL_pairz00_7438 = CDR(((obj_t) BgL_newzd2argszd2_6662));
								BgL_auxz00_11632 = ((BgL_copz00_bglt) CAR(BgL_pairz00_7438));
							}
							((((BgL_cfailz00_bglt) COBJECT(BgL_new1264z00_7435))->
									BgL_msgz00) = ((BgL_copz00_bglt) BgL_auxz00_11632), BUNSPEC);
						}
						{
							BgL_copz00_bglt BgL_auxz00_11638;

							{	/* Cgen/cgen.scm 642 */
								obj_t BgL_pairz00_7439;

								{	/* Cgen/cgen.scm 642 */
									obj_t BgL_pairz00_7440;

									BgL_pairz00_7440 = CDR(((obj_t) BgL_newzd2argszd2_6662));
									BgL_pairz00_7439 = CDR(BgL_pairz00_7440);
								}
								BgL_auxz00_11638 = ((BgL_copz00_bglt) CAR(BgL_pairz00_7439));
							}
							((((BgL_cfailz00_bglt) COBJECT(BgL_new1264z00_7435))->
									BgL_objz00) = ((BgL_copz00_bglt) BgL_auxz00_11638), BUNSPEC);
						}
						BgL_arg2243z00_7434 = BgL_new1264z00_7435;
					}
					return BgL_arg2243z00_7434;
				}
			}
		}

	}



/* &node->cop-conditiona1755 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2conditiona1755z81zzcgen_cgenz00(obj_t
		BgL_envz00_6663, obj_t BgL_nodez00_6664, obj_t BgL_kontz00_6665,
		obj_t BgL_inpushexitz00_6666)
	{
		{	/* Cgen/cgen.scm 581 */
			{	/* Tools/trace.sch 53 */
				BgL_localz00_bglt BgL_auxz00_7442;

				{	/* Tools/trace.sch 53 */
					obj_t BgL_arg2231z00_7443;

					BgL_arg2231z00_7443 =
						BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(17));
					BgL_auxz00_7442 =
						BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(BgL_arg2231z00_7443,
						((BgL_typez00_bglt) BGl_za2boolza2z00zztype_cachez00));
				}
				{	/* Tools/trace.sch 53 */
					BgL_copz00_bglt BgL_ctestz00_7444;

					{	/* Tools/trace.sch 53 */
						BgL_setqz00_bglt BgL_arg2229z00_7445;

						{	/* Tools/trace.sch 53 */
							BgL_nodez00_bglt BgL_arg2230z00_7446;

							BgL_arg2230z00_7446 =
								(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_nodez00_6664)))->
								BgL_testz00);
							BgL_arg2229z00_7445 =
								BGl_nodezd2setqzd2zzcgen_cgenz00(((BgL_variablez00_bglt)
									BgL_auxz00_7442), BgL_arg2230z00_7446);
						}
						BgL_ctestz00_7444 =
							BGl_nodezd2ze3copz31zzcgen_cgenz00(
							((BgL_nodez00_bglt) BgL_arg2229z00_7445),
							BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
							CBOOL(BgL_inpushexitz00_6666));
					}
					{	/* Tools/trace.sch 53 */

						{	/* Tools/trace.sch 53 */
							bool_t BgL_test3279z00_11656;

							{	/* Tools/trace.sch 53 */
								bool_t BgL_test3280z00_11657;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_classz00_7447;

									BgL_classz00_7447 = BGl_csetqz00zzcgen_copz00;
									{	/* Tools/trace.sch 53 */
										BgL_objectz00_bglt BgL_arg1807z00_7448;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_tmpz00_11658;

											BgL_tmpz00_11658 =
												((obj_t) ((BgL_objectz00_bglt) BgL_ctestz00_7444));
											BgL_arg1807z00_7448 =
												(BgL_objectz00_bglt) (BgL_tmpz00_11658);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Tools/trace.sch 53 */
												long BgL_idxz00_7449;

												BgL_idxz00_7449 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_7448);
												BgL_test3280z00_11657 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_7449 + 2L)) == BgL_classz00_7447);
											}
										else
											{	/* Tools/trace.sch 53 */
												bool_t BgL_res2839z00_7452;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_oclassz00_7453;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg1815z00_7454;
														long BgL_arg1816z00_7455;

														BgL_arg1815z00_7454 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Tools/trace.sch 53 */
															long BgL_arg1817z00_7456;

															BgL_arg1817z00_7456 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_7448);
															BgL_arg1816z00_7455 =
																(BgL_arg1817z00_7456 - OBJECT_TYPE);
														}
														BgL_oclassz00_7453 =
															VECTOR_REF(BgL_arg1815z00_7454,
															BgL_arg1816z00_7455);
													}
													{	/* Tools/trace.sch 53 */
														bool_t BgL__ortest_1115z00_7457;

														BgL__ortest_1115z00_7457 =
															(BgL_classz00_7447 == BgL_oclassz00_7453);
														if (BgL__ortest_1115z00_7457)
															{	/* Tools/trace.sch 53 */
																BgL_res2839z00_7452 = BgL__ortest_1115z00_7457;
															}
														else
															{	/* Tools/trace.sch 53 */
																long BgL_odepthz00_7458;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1804z00_7459;

																	BgL_arg1804z00_7459 = (BgL_oclassz00_7453);
																	BgL_odepthz00_7458 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_7459);
																}
																if ((2L < BgL_odepthz00_7458))
																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1802z00_7460;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1803z00_7461;

																			BgL_arg1803z00_7461 =
																				(BgL_oclassz00_7453);
																			BgL_arg1802z00_7460 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_7461, 2L);
																		}
																		BgL_res2839z00_7452 =
																			(BgL_arg1802z00_7460 ==
																			BgL_classz00_7447);
																	}
																else
																	{	/* Tools/trace.sch 53 */
																		BgL_res2839z00_7452 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3280z00_11657 = BgL_res2839z00_7452;
											}
									}
								}
								if (BgL_test3280z00_11657)
									{	/* Tools/trace.sch 53 */
										BgL_test3279z00_11656 =
											(
											((obj_t)
												(((BgL_varcz00_bglt) COBJECT(
															(((BgL_csetqz00_bglt) COBJECT(
																		((BgL_csetqz00_bglt) BgL_ctestz00_7444)))->
																BgL_varz00)))->BgL_variablez00)) ==
											((obj_t) BgL_auxz00_7442));
									}
								else
									{	/* Tools/trace.sch 53 */
										BgL_test3279z00_11656 = ((bool_t) 0);
									}
							}
							if (BgL_test3279z00_11656)
								{	/* Tools/trace.sch 53 */
									BgL_cifz00_bglt BgL_new1249z00_7462;

									{	/* Tools/trace.sch 53 */
										BgL_cifz00_bglt BgL_new1248z00_7463;

										BgL_new1248z00_7463 =
											((BgL_cifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_cifz00_bgl))));
										{	/* Tools/trace.sch 53 */
											long BgL_arg2207z00_7464;

											BgL_arg2207z00_7464 =
												BGL_CLASS_NUM(BGl_cifz00zzcgen_copz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1248z00_7463),
												BgL_arg2207z00_7464);
										}
										BgL_new1249z00_7462 = BgL_new1248z00_7463;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1249z00_7462)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt)
																BgL_nodez00_6664))))->BgL_locz00)), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1249z00_7462)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt)
																BgL_nodez00_6664))))->BgL_typez00)), BUNSPEC);
									((((BgL_cifz00_bglt) COBJECT(BgL_new1249z00_7462))->
											BgL_testz00) =
										((BgL_copz00_bglt) (((BgL_csetqz00_bglt)
													COBJECT(((BgL_csetqz00_bglt) BgL_ctestz00_7444)))->
												BgL_valuez00)), BUNSPEC);
									{
										BgL_copz00_bglt BgL_auxz00_11704;

										{	/* Tools/trace.sch 53 */
											BgL_copz00_bglt BgL_arg2201z00_7465;
											obj_t BgL_arg2202z00_7466;

											{	/* Tools/trace.sch 53 */
												BgL_nodez00_bglt BgL_arg2203z00_7467;

												BgL_arg2203z00_7467 =
													(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt) BgL_nodez00_6664)))->
													BgL_truez00);
												BgL_arg2201z00_7465 =
													BGl_nodezd2ze3copz31zzcgen_cgenz00
													(BgL_arg2203z00_7467, BgL_kontz00_6665,
													CBOOL(BgL_inpushexitz00_6666));
											}
											BgL_arg2202z00_7466 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_conditionalz00_bglt) BgL_nodez00_6664))))->
												BgL_locz00);
											BgL_auxz00_11704 =
												((BgL_copz00_bglt)
												BGl_blockzd2kontzd2zzcgen_cgenz00(((obj_t)
														BgL_arg2201z00_7465), BgL_arg2202z00_7466));
										}
										((((BgL_cifz00_bglt) COBJECT(BgL_new1249z00_7462))->
												BgL_truez00) =
											((BgL_copz00_bglt) BgL_auxz00_11704), BUNSPEC);
									}
									{
										BgL_copz00_bglt BgL_auxz00_11716;

										{	/* Tools/trace.sch 53 */
											BgL_copz00_bglt BgL_arg2204z00_7468;
											obj_t BgL_arg2205z00_7469;

											{	/* Tools/trace.sch 53 */
												BgL_nodez00_bglt BgL_arg2206z00_7470;

												BgL_arg2206z00_7470 =
													(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt) BgL_nodez00_6664)))->
													BgL_falsez00);
												BgL_arg2204z00_7468 =
													BGl_nodezd2ze3copz31zzcgen_cgenz00
													(BgL_arg2206z00_7470, BgL_kontz00_6665,
													CBOOL(BgL_inpushexitz00_6666));
											}
											BgL_arg2205z00_7469 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_conditionalz00_bglt) BgL_nodez00_6664))))->
												BgL_locz00);
											BgL_auxz00_11716 =
												((BgL_copz00_bglt)
												BGl_blockzd2kontzd2zzcgen_cgenz00(((obj_t)
														BgL_arg2204z00_7468), BgL_arg2205z00_7469));
										}
										((((BgL_cifz00_bglt) COBJECT(BgL_new1249z00_7462))->
												BgL_falsez00) =
											((BgL_copz00_bglt) BgL_auxz00_11716), BUNSPEC);
									}
									return ((BgL_copz00_bglt) BgL_new1249z00_7462);
								}
							else
								{	/* Tools/trace.sch 53 */
									BgL_cblockz00_bglt BgL_new1252z00_7471;

									{	/* Tools/trace.sch 53 */
										BgL_cblockz00_bglt BgL_new1251z00_7472;

										BgL_new1251z00_7472 =
											((BgL_cblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_cblockz00_bgl))));
										{	/* Tools/trace.sch 53 */
											long BgL_arg2226z00_7473;

											BgL_arg2226z00_7473 =
												BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1251z00_7472),
												BgL_arg2226z00_7473);
										}
										BgL_new1252z00_7471 = BgL_new1251z00_7472;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1252z00_7471)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt)
																BgL_nodez00_6664))))->BgL_locz00)), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1252z00_7471)))->BgL_typez00) =
										((BgL_typez00_bglt) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt)
																BgL_nodez00_6664))))->BgL_typez00)), BUNSPEC);
									{
										BgL_copz00_bglt BgL_auxz00_11743;

										{	/* Tools/trace.sch 53 */
											BgL_csequencez00_bglt BgL_new1254z00_7474;

											{	/* Tools/trace.sch 53 */
												BgL_csequencez00_bglt BgL_new1253z00_7475;

												BgL_new1253z00_7475 =
													((BgL_csequencez00_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_csequencez00_bgl))));
												{	/* Tools/trace.sch 53 */
													long BgL_arg2225z00_7476;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_classz00_7477;

														BgL_classz00_7477 = BGl_csequencez00zzcgen_copz00;
														BgL_arg2225z00_7476 =
															BGL_CLASS_NUM(BgL_classz00_7477);
													}
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1253z00_7475),
														BgL_arg2225z00_7476);
												}
												BgL_new1254z00_7474 = BgL_new1253z00_7475;
											}
											((((BgL_copz00_bglt) COBJECT(
															((BgL_copz00_bglt) BgL_new1254z00_7474)))->
													BgL_locz00) =
												((obj_t) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) (
																		(BgL_conditionalz00_bglt)
																		BgL_nodez00_6664))))->BgL_locz00)),
												BUNSPEC);
											((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																BgL_new1254z00_7474)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) (
																		(BgL_conditionalz00_bglt)
																		BgL_nodez00_6664))))->BgL_typez00)),
												BUNSPEC);
											((((BgL_csequencez00_bglt) COBJECT(BgL_new1254z00_7474))->
													BgL_czd2expzf3z21) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
											{
												obj_t BgL_auxz00_11759;

												{	/* Tools/trace.sch 53 */
													BgL_localzd2varzd2_bglt BgL_arg2208z00_7478;
													BgL_cifz00_bglt BgL_arg2209z00_7479;

													{	/* Tools/trace.sch 53 */
														BgL_localzd2varzd2_bglt BgL_new1256z00_7480;

														{	/* Tools/trace.sch 53 */
															BgL_localzd2varzd2_bglt BgL_new1255z00_7481;

															BgL_new1255z00_7481 =
																((BgL_localzd2varzd2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_localzd2varzd2_bgl))));
															{	/* Tools/trace.sch 53 */
																long BgL_arg2216z00_7482;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_classz00_7483;

																	BgL_classz00_7483 =
																		BGl_localzd2varzd2zzcgen_copz00;
																	BgL_arg2216z00_7482 =
																		BGL_CLASS_NUM(BgL_classz00_7483);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1255z00_7481),
																	BgL_arg2216z00_7482);
															}
															BgL_new1256z00_7480 = BgL_new1255z00_7481;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1256z00_7480)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_conditionalz00_bglt)
																					BgL_nodez00_6664))))->BgL_locz00)),
															BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1256z00_7480)))->BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
														{
															obj_t BgL_auxz00_11772;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_list2215z00_7484;

																BgL_list2215z00_7484 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_auxz00_7442), BNIL);
																BgL_auxz00_11772 = BgL_list2215z00_7484;
															}
															((((BgL_localzd2varzd2_bglt)
																		COBJECT(BgL_new1256z00_7480))->
																	BgL_varsz00) =
																((obj_t) BgL_auxz00_11772), BUNSPEC);
														}
														BgL_arg2208z00_7478 = BgL_new1256z00_7480;
													}
													{	/* Tools/trace.sch 53 */
														BgL_cifz00_bglt BgL_new1258z00_7485;

														{	/* Tools/trace.sch 53 */
															BgL_cifz00_bglt BgL_new1257z00_7486;

															BgL_new1257z00_7486 =
																((BgL_cifz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_cifz00_bgl))));
															{	/* Tools/trace.sch 53 */
																long BgL_arg2224z00_7487;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_classz00_7488;

																	BgL_classz00_7488 = BGl_cifz00zzcgen_copz00;
																	BgL_arg2224z00_7487 =
																		BGL_CLASS_NUM(BgL_classz00_7488);
																}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1257z00_7486),
																	BgL_arg2224z00_7487);
															}
															BgL_new1258z00_7485 = BgL_new1257z00_7486;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1258z00_7485)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_conditionalz00_bglt)
																					BgL_nodez00_6664))))->BgL_locz00)),
															BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1258z00_7485)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_conditionalz00_bglt)
																					BgL_nodez00_6664))))->BgL_typez00)),
															BUNSPEC);
														{
															BgL_copz00_bglt BgL_auxz00_11790;

															{	/* Tools/trace.sch 53 */
																BgL_varcz00_bglt BgL_new1261z00_7489;

																{	/* Tools/trace.sch 53 */
																	BgL_varcz00_bglt BgL_new1260z00_7490;

																	BgL_new1260z00_7490 =
																		((BgL_varcz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_varcz00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg2217z00_7491;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_classz00_7492;

																			BgL_classz00_7492 =
																				BGl_varcz00zzcgen_copz00;
																			BgL_arg2217z00_7491 =
																				BGL_CLASS_NUM(BgL_classz00_7492);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1260z00_7490),
																			BgL_arg2217z00_7491);
																	}
																	BgL_new1261z00_7489 = BgL_new1260z00_7490;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1261z00_7489)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_conditionalz00_bglt)
																							BgL_nodez00_6664))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1261z00_7489)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_auxz00_7442)))->BgL_typez00)),
																	BUNSPEC);
																((((BgL_varcz00_bglt)
																			COBJECT(BgL_new1261z00_7489))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt) BgL_auxz00_7442)),
																	BUNSPEC);
																BgL_auxz00_11790 =
																	((BgL_copz00_bglt) BgL_new1261z00_7489);
															}
															((((BgL_cifz00_bglt)
																		COBJECT(BgL_new1258z00_7485))->
																	BgL_testz00) =
																((BgL_copz00_bglt) BgL_auxz00_11790), BUNSPEC);
														}
														{
															BgL_copz00_bglt BgL_auxz00_11808;

															{	/* Tools/trace.sch 53 */
																BgL_copz00_bglt BgL_arg2218z00_7493;
																obj_t BgL_arg2219z00_7494;

																{	/* Tools/trace.sch 53 */
																	BgL_nodez00_bglt BgL_arg2220z00_7495;

																	BgL_arg2220z00_7495 =
																		(((BgL_conditionalz00_bglt) COBJECT(
																				((BgL_conditionalz00_bglt)
																					BgL_nodez00_6664)))->BgL_truez00);
																	BgL_arg2218z00_7493 =
																		BGl_nodezd2ze3copz31zzcgen_cgenz00
																		(BgL_arg2220z00_7495, BgL_kontz00_6665,
																		CBOOL(BgL_inpushexitz00_6666));
																}
																BgL_arg2219z00_7494 =
																	(((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				((BgL_conditionalz00_bglt)
																					BgL_nodez00_6664))))->BgL_locz00);
																BgL_auxz00_11808 =
																	((BgL_copz00_bglt)
																	BGl_blockzd2kontzd2zzcgen_cgenz00(((obj_t)
																			BgL_arg2218z00_7493),
																		BgL_arg2219z00_7494));
															}
															((((BgL_cifz00_bglt)
																		COBJECT(BgL_new1258z00_7485))->
																	BgL_truez00) =
																((BgL_copz00_bglt) BgL_auxz00_11808), BUNSPEC);
														}
														{
															BgL_copz00_bglt BgL_auxz00_11820;

															{	/* Tools/trace.sch 53 */
																BgL_copz00_bglt BgL_arg2221z00_7496;
																obj_t BgL_arg2222z00_7497;

																{	/* Tools/trace.sch 53 */
																	BgL_nodez00_bglt BgL_arg2223z00_7498;

																	BgL_arg2223z00_7498 =
																		(((BgL_conditionalz00_bglt) COBJECT(
																				((BgL_conditionalz00_bglt)
																					BgL_nodez00_6664)))->BgL_falsez00);
																	BgL_arg2221z00_7496 =
																		BGl_nodezd2ze3copz31zzcgen_cgenz00
																		(BgL_arg2223z00_7498, BgL_kontz00_6665,
																		CBOOL(BgL_inpushexitz00_6666));
																}
																BgL_arg2222z00_7497 =
																	(((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				((BgL_conditionalz00_bglt)
																					BgL_nodez00_6664))))->BgL_locz00);
																BgL_auxz00_11820 =
																	((BgL_copz00_bglt)
																	BGl_blockzd2kontzd2zzcgen_cgenz00(((obj_t)
																			BgL_arg2221z00_7496),
																		BgL_arg2222z00_7497));
															}
															((((BgL_cifz00_bglt)
																		COBJECT(BgL_new1258z00_7485))->
																	BgL_falsez00) =
																((BgL_copz00_bglt) BgL_auxz00_11820), BUNSPEC);
														}
														BgL_arg2209z00_7479 = BgL_new1258z00_7485;
													}
													{	/* Tools/trace.sch 53 */
														obj_t BgL_list2210z00_7499;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg2211z00_7500;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg2212z00_7501;

																BgL_arg2212z00_7501 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg2209z00_7479), BNIL);
																BgL_arg2211z00_7500 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_ctestz00_7444),
																	BgL_arg2212z00_7501);
															}
															BgL_list2210z00_7499 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg2208z00_7478),
																BgL_arg2211z00_7500);
														}
														BgL_auxz00_11759 = BgL_list2210z00_7499;
												}}
												((((BgL_csequencez00_bglt)
															COBJECT(BgL_new1254z00_7474))->BgL_copsz00) =
													((obj_t) BgL_auxz00_11759), BUNSPEC);
											}
											BgL_auxz00_11743 =
												((BgL_copz00_bglt) BgL_new1254z00_7474);
										}
										((((BgL_cblockz00_bglt) COBJECT(BgL_new1252z00_7471))->
												BgL_bodyz00) =
											((BgL_copz00_bglt) BgL_auxz00_11743), BUNSPEC);
									}
									return ((BgL_copz00_bglt) BgL_new1252z00_7471);
								}
						}
					}
				}
			}
		}

	}



/* &node->cop-setq1753 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2setq1753z81zzcgen_cgenz00(obj_t
		BgL_envz00_6667, obj_t BgL_nodez00_6668, obj_t BgL_kontz00_6669,
		obj_t BgL_inpushexitz00_6670)
	{
		{	/* Cgen/cgen.scm 565 */
			{	/* Tools/trace.sch 53 */
				BgL_variablez00_bglt BgL_varz00_7503;

				BgL_varz00_7503 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_6668)))->BgL_varz00)))->
					BgL_variablez00);
				{	/* Tools/trace.sch 53 */
					bool_t BgL_test3284z00_11845;

					{	/* Tools/trace.sch 53 */
						bool_t BgL_test3285z00_11846;

						{	/* Tools/trace.sch 53 */
							BgL_nodez00_bglt BgL_arg2194z00_7504;

							BgL_arg2194z00_7504 =
								(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_6668)))->BgL_valuez00);
							{	/* Tools/trace.sch 53 */
								obj_t BgL_classz00_7505;

								BgL_classz00_7505 = BGl_varz00zzast_nodez00;
								{	/* Tools/trace.sch 53 */
									BgL_objectz00_bglt BgL_arg1807z00_7506;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_tmpz00_11849;

										BgL_tmpz00_11849 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2194z00_7504));
										BgL_arg1807z00_7506 =
											(BgL_objectz00_bglt) (BgL_tmpz00_11849);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Tools/trace.sch 53 */
											long BgL_idxz00_7507;

											BgL_idxz00_7507 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_7506);
											BgL_test3285z00_11846 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_7507 + 2L)) == BgL_classz00_7505);
										}
									else
										{	/* Tools/trace.sch 53 */
											bool_t BgL_res2838z00_7510;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_oclassz00_7511;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1815z00_7512;
													long BgL_arg1816z00_7513;

													BgL_arg1815z00_7512 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Tools/trace.sch 53 */
														long BgL_arg1817z00_7514;

														BgL_arg1817z00_7514 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_7506);
														BgL_arg1816z00_7513 =
															(BgL_arg1817z00_7514 - OBJECT_TYPE);
													}
													BgL_oclassz00_7511 =
														VECTOR_REF(BgL_arg1815z00_7512,
														BgL_arg1816z00_7513);
												}
												{	/* Tools/trace.sch 53 */
													bool_t BgL__ortest_1115z00_7515;

													BgL__ortest_1115z00_7515 =
														(BgL_classz00_7505 == BgL_oclassz00_7511);
													if (BgL__ortest_1115z00_7515)
														{	/* Tools/trace.sch 53 */
															BgL_res2838z00_7510 = BgL__ortest_1115z00_7515;
														}
													else
														{	/* Tools/trace.sch 53 */
															long BgL_odepthz00_7516;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1804z00_7517;

																BgL_arg1804z00_7517 = (BgL_oclassz00_7511);
																BgL_odepthz00_7516 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_7517);
															}
															if ((2L < BgL_odepthz00_7516))
																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1802z00_7518;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1803z00_7519;

																		BgL_arg1803z00_7519 = (BgL_oclassz00_7511);
																		BgL_arg1802z00_7518 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_7519, 2L);
																	}
																	BgL_res2838z00_7510 =
																		(BgL_arg1802z00_7518 == BgL_classz00_7505);
																}
															else
																{	/* Tools/trace.sch 53 */
																	BgL_res2838z00_7510 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3285z00_11846 = BgL_res2838z00_7510;
										}
								}
							}
						}
						if (BgL_test3285z00_11846)
							{	/* Tools/trace.sch 53 */
								BgL_test3284z00_11845 =
									(
									((obj_t) BgL_varz00_7503) ==
									((obj_t)
										(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt)
														(((BgL_setqz00_bglt) COBJECT(
																	((BgL_setqz00_bglt) BgL_nodez00_6668)))->
															BgL_valuez00))))->BgL_variablez00)));
							}
						else
							{	/* Tools/trace.sch 53 */
								BgL_test3284z00_11845 = ((bool_t) 0);
							}
					}
					if (BgL_test3284z00_11845)
						{	/* Tools/trace.sch 53 */
							BgL_cvoidz00_bglt BgL_arg2186z00_7520;

							{	/* Tools/trace.sch 53 */
								BgL_catomz00_bglt BgL_arg2187z00_7521;

								{	/* Tools/trace.sch 53 */
									BgL_catomz00_bglt BgL_new1246z00_7522;

									{	/* Tools/trace.sch 53 */
										BgL_catomz00_bglt BgL_new1245z00_7523;

										BgL_new1245z00_7523 =
											((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_catomz00_bgl))));
										{	/* Tools/trace.sch 53 */
											long BgL_arg2188z00_7524;

											BgL_arg2188z00_7524 =
												BGL_CLASS_NUM(BGl_catomz00zzcgen_copz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1245z00_7523),
												BgL_arg2188z00_7524);
										}
										BgL_new1246z00_7522 = BgL_new1245z00_7523;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1246z00_7522)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_setqz00_bglt)
																BgL_nodez00_6668))))->BgL_locz00)), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1246z00_7522)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
									((((BgL_catomz00_bglt) COBJECT(BgL_new1246z00_7522))->
											BgL_valuez00) = ((obj_t) BUNSPEC), BUNSPEC);
									BgL_arg2187z00_7521 = BgL_new1246z00_7522;
								}
								BgL_arg2186z00_7520 =
									BGl_za2voidzd2kontza2zd2zzcgen_cgenz00(
									((obj_t) BgL_arg2187z00_7521));
							}
							return
								((BgL_copz00_bglt)
								BGL_PROCEDURE_CALL1(BgL_kontz00_6669,
									((obj_t) BgL_arg2186z00_7520)));
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_nodez00_bglt BgL_arg2189z00_7525;
							obj_t BgL_arg2190z00_7526;

							BgL_arg2189z00_7525 =
								(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_6668)))->BgL_valuez00);
							BgL_arg2190z00_7526 =
								BGl_makezd2setqzd2kontz00zzcgen_cgenz00(
								((obj_t) BgL_varz00_7503),
								(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_setqz00_bglt) BgL_nodez00_6668))))->BgL_locz00),
								BgL_kontz00_6669);
							return BGl_nodezd2ze3copz31zzcgen_cgenz00(BgL_arg2189z00_7525,
								BgL_arg2190z00_7526, CBOOL(BgL_inpushexitz00_6670));
						}
				}
			}
		}

	}



/* &node->cop-cast1751 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2cast1751z81zzcgen_cgenz00(obj_t
		BgL_envz00_6671, obj_t BgL_nodez00_6672, obj_t BgL_kontz00_6673,
		obj_t BgL_inpushexitz00_6674)
	{
		{	/* Cgen/cgen.scm 547 */
			{	/* Tools/trace.sch 53 */
				BgL_typez00_bglt BgL_arg2171z00_7528;
				obj_t BgL_arg2172z00_7529;
				obj_t BgL_arg2173z00_7530;

				BgL_arg2171z00_7528 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_castz00_bglt) BgL_nodez00_6672))))->BgL_typez00);
				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg2175z00_7531;

					BgL_arg2175z00_7531 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_6672)))->BgL_argz00);
					{	/* Tools/trace.sch 53 */
						obj_t BgL_list2176z00_7532;

						BgL_list2176z00_7532 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg2175z00_7531), BNIL);
						BgL_arg2172z00_7529 = BgL_list2176z00_7532;
					}
				}
				BgL_arg2173z00_7530 =
					(((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_castz00_bglt) BgL_nodez00_6672))))->BgL_locz00);
				{	/* Tools/trace.sch 53 */
					obj_t BgL_zc3z04anonymousza32177ze3z87_7533;

					{	/* Tools/trace.sch 53 */
						int BgL_tmpz00_11919;

						BgL_tmpz00_11919 = (int) (2L);
						BgL_zc3z04anonymousza32177ze3z87_7533 =
							MAKE_L_PROCEDURE
							(BGl_z62zc3z04anonymousza32177ze3ze5zzcgen_cgenz00,
							BgL_tmpz00_11919);
					}
					PROCEDURE_L_SET(BgL_zc3z04anonymousza32177ze3z87_7533,
						(int) (0L), ((obj_t) ((BgL_castz00_bglt) BgL_nodez00_6672)));
					PROCEDURE_L_SET(BgL_zc3z04anonymousza32177ze3z87_7533,
						(int) (1L), BgL_kontz00_6673);
					return
						((BgL_copz00_bglt)
						BGl_nodezd2argszd2ze3copze3zzcgen_cgenz00(BgL_arg2171z00_7528,
							BgL_arg2172z00_7529, ((bool_t) 1), BgL_arg2173z00_7530,
							BgL_zc3z04anonymousza32177ze3z87_7533, BgL_inpushexitz00_6674));
				}
			}
		}

	}



/* &<@anonymous:2177> */
	obj_t BGl_z62zc3z04anonymousza32177ze3ze5zzcgen_cgenz00(obj_t BgL_envz00_6675,
		obj_t BgL_newzd2argszd2_6678)
	{
		{	/* Cgen/cgen.scm 555 */
			{	/* Cgen/cgen.scm 556 */
				BgL_castz00_bglt BgL_i1240z00_6676;
				obj_t BgL_kontz00_6677;

				BgL_i1240z00_6676 =
					((BgL_castz00_bglt) PROCEDURE_L_REF(BgL_envz00_6675, (int) (0L)));
				BgL_kontz00_6677 = PROCEDURE_L_REF(BgL_envz00_6675, (int) (1L));
				{	/* Cgen/cgen.scm 556 */
					BgL_ccastz00_bglt BgL_arg2178z00_7535;

					{	/* Cgen/cgen.scm 556 */
						BgL_ccastz00_bglt BgL_new1243z00_7536;

						{	/* Cgen/cgen.scm 558 */
							BgL_ccastz00_bglt BgL_new1242z00_7537;

							BgL_new1242z00_7537 =
								((BgL_ccastz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_ccastz00_bgl))));
							{	/* Cgen/cgen.scm 558 */
								long BgL_arg2179z00_7538;

								BgL_arg2179z00_7538 = BGL_CLASS_NUM(BGl_ccastz00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1242z00_7537),
									BgL_arg2179z00_7538);
							}
							BgL_new1243z00_7536 = BgL_new1242z00_7537;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1243z00_7536)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
												BgL_i1240z00_6676)))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1243z00_7536)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) BgL_i1240z00_6676)))->
									BgL_typez00)), BUNSPEC);
						((((BgL_ccastz00_bglt) COBJECT(BgL_new1243z00_7536))->BgL_argz00) =
							((BgL_copz00_bglt) ((BgL_copz00_bglt) CAR(((obj_t)
											BgL_newzd2argszd2_6678)))), BUNSPEC);
						BgL_arg2178z00_7535 = BgL_new1243z00_7536;
					}
					return
						BGL_PROCEDURE_CALL1(BgL_kontz00_6677,
						((obj_t) BgL_arg2178z00_7535));
				}
			}
		}

	}



/* &node->cop-private1749 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2private1749z81zzcgen_cgenz00(obj_t
		BgL_envz00_6679, obj_t BgL_nodez00_6680, obj_t BgL_kontz00_6681,
		obj_t BgL_inpushexitz00_6682)
	{
		{	/* Cgen/cgen.scm 537 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_arg2170z00_7540;

				BgL_arg2170z00_7540 =
					(((BgL_privatez00_bglt) COBJECT(
							((BgL_privatez00_bglt) BgL_nodez00_6680)))->BgL_czd2formatzd2);
				return
					((BgL_copz00_bglt)
					BGl_externzd2ze3copz31zzcgen_cgenz00(BgL_arg2170z00_7540,
						((bool_t) 1),
						((BgL_externz00_bglt) ((BgL_privatez00_bglt) BgL_nodez00_6680)),
						BgL_kontz00_6681, BgL_inpushexitz00_6682));
			}
		}

	}



/* &node->cop-pragma1747 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2pragma1747z81zzcgen_cgenz00(obj_t
		BgL_envz00_6683, obj_t BgL_nodez00_6684, obj_t BgL_kontz00_6685,
		obj_t BgL_inpushexitz00_6686)
	{
		{	/* Cgen/cgen.scm 513 */
			if (
				((((BgL_pragmaz00_bglt) COBJECT(
								((BgL_pragmaz00_bglt) BgL_nodez00_6684)))->BgL_srfi0z00) ==
					CNST_TABLE_REF(14)))
				{	/* Tools/trace.sch 53 */
					bool_t BgL_test3290z00_11967;

					if (
						(STRING_LENGTH(
								(((BgL_pragmaz00_bglt) COBJECT(
											((BgL_pragmaz00_bglt) BgL_nodez00_6684)))->
									BgL_formatz00)) == 0L))
						{	/* Tools/trace.sch 53 */
							if (NULLP(
									(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_pragmaz00_bglt) BgL_nodez00_6684))))->
										BgL_exprza2za2)))
								{	/* Tools/trace.sch 53 */
									BgL_test3290z00_11967 = ((bool_t) 0);
								}
							else
								{	/* Tools/trace.sch 53 */
									bool_t BgL_test3293z00_11978;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_tmpz00_11979;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_pairz00_7542;

											BgL_pairz00_7542 =
												(((BgL_externz00_bglt) COBJECT(
														((BgL_externz00_bglt)
															((BgL_pragmaz00_bglt) BgL_nodez00_6684))))->
												BgL_exprza2za2);
											BgL_tmpz00_11979 = CDR(BgL_pairz00_7542);
										}
										BgL_test3293z00_11978 = NULLP(BgL_tmpz00_11979);
									}
									if (BgL_test3293z00_11978)
										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg2154z00_7543;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_pairz00_7544;

												BgL_pairz00_7544 =
													(((BgL_externz00_bglt) COBJECT(
															((BgL_externz00_bglt)
																((BgL_pragmaz00_bglt) BgL_nodez00_6684))))->
													BgL_exprza2za2);
												BgL_arg2154z00_7543 = CAR(BgL_pairz00_7544);
											}
											{	/* Tools/trace.sch 53 */
												obj_t BgL_classz00_7545;

												BgL_classz00_7545 = BGl_varz00zzast_nodez00;
												if (BGL_OBJECTP(BgL_arg2154z00_7543))
													{	/* Tools/trace.sch 53 */
														BgL_objectz00_bglt BgL_arg1807z00_7546;

														BgL_arg1807z00_7546 =
															(BgL_objectz00_bglt) (BgL_arg2154z00_7543);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Tools/trace.sch 53 */
																long BgL_idxz00_7547;

																BgL_idxz00_7547 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_7546);
																BgL_test3290z00_11967 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_7547 + 2L)) ==
																	BgL_classz00_7545);
															}
														else
															{	/* Tools/trace.sch 53 */
																bool_t BgL_res2836z00_7550;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_oclassz00_7551;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1815z00_7552;
																		long BgL_arg1816z00_7553;

																		BgL_arg1815z00_7552 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Tools/trace.sch 53 */
																			long BgL_arg1817z00_7554;

																			BgL_arg1817z00_7554 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_7546);
																			BgL_arg1816z00_7553 =
																				(BgL_arg1817z00_7554 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_7551 =
																			VECTOR_REF(BgL_arg1815z00_7552,
																			BgL_arg1816z00_7553);
																	}
																	{	/* Tools/trace.sch 53 */
																		bool_t BgL__ortest_1115z00_7555;

																		BgL__ortest_1115z00_7555 =
																			(BgL_classz00_7545 == BgL_oclassz00_7551);
																		if (BgL__ortest_1115z00_7555)
																			{	/* Tools/trace.sch 53 */
																				BgL_res2836z00_7550 =
																					BgL__ortest_1115z00_7555;
																			}
																		else
																			{	/* Tools/trace.sch 53 */
																				long BgL_odepthz00_7556;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1804z00_7557;

																					BgL_arg1804z00_7557 =
																						(BgL_oclassz00_7551);
																					BgL_odepthz00_7556 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_7557);
																				}
																				if ((2L < BgL_odepthz00_7556))
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1802z00_7558;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1803z00_7559;

																							BgL_arg1803z00_7559 =
																								(BgL_oclassz00_7551);
																							BgL_arg1802z00_7558 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_7559, 2L);
																						}
																						BgL_res2836z00_7550 =
																							(BgL_arg1802z00_7558 ==
																							BgL_classz00_7545);
																					}
																				else
																					{	/* Tools/trace.sch 53 */
																						BgL_res2836z00_7550 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3290z00_11967 = BgL_res2836z00_7550;
															}
													}
												else
													{	/* Tools/trace.sch 53 */
														BgL_test3290z00_11967 = ((bool_t) 0);
													}
											}
										}
									else
										{	/* Tools/trace.sch 53 */
											BgL_test3290z00_11967 = ((bool_t) 0);
										}
								}
						}
					else
						{	/* Tools/trace.sch 53 */
							BgL_test3290z00_11967 = ((bool_t) 0);
						}
					if (BgL_test3290z00_11967)
						{	/* Tools/trace.sch 53 */
							BgL_varz00_bglt BgL_i1235z00_7560;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_pairz00_7561;

								BgL_pairz00_7561 =
									(((BgL_externz00_bglt) COBJECT(
											((BgL_externz00_bglt)
												((BgL_pragmaz00_bglt) BgL_nodez00_6684))))->
									BgL_exprza2za2);
								BgL_i1235z00_7560 = ((BgL_varz00_bglt) CAR(BgL_pairz00_7561));
							}
							{	/* Tools/trace.sch 53 */
								BgL_variablez00_bglt BgL_i1236z00_7562;

								BgL_i1236z00_7562 =
									(((BgL_varz00_bglt) COBJECT(BgL_i1235z00_7560))->
									BgL_variablez00);
								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg2150z00_7563;

									BgL_arg2150z00_7563 =
										(((BgL_variablez00_bglt) COBJECT(BgL_i1236z00_7562))->
										BgL_namez00);
									return ((BgL_copz00_bglt)
										BGl_externzd2ze3copz31zzcgen_cgenz00(BgL_arg2150z00_7563,
											((bool_t) 0),
											((BgL_externz00_bglt) ((BgL_pragmaz00_bglt)
													BgL_nodez00_6684)), BgL_kontz00_6685,
											BgL_inpushexitz00_6686));
								}
							}
						}
					else
						{	/* Tools/trace.sch 53 */
							obj_t BgL_arg2152z00_7564;

							BgL_arg2152z00_7564 =
								(((BgL_pragmaz00_bglt) COBJECT(
										((BgL_pragmaz00_bglt) BgL_nodez00_6684)))->BgL_formatz00);
							return
								((BgL_copz00_bglt)
								BGl_externzd2ze3copz31zzcgen_cgenz00(BgL_arg2152z00_7564,
									((bool_t) 0),
									((BgL_externz00_bglt) ((BgL_pragmaz00_bglt)
											BgL_nodez00_6684)), BgL_kontz00_6685,
									BgL_inpushexitz00_6686));
						}
				}
			else
				{	/* Tools/trace.sch 53 */
					{	/* Tools/trace.sch 53 */
						obj_t BgL_arg2160z00_7565;
						obj_t BgL_arg2161z00_7566;

						{	/* Tools/trace.sch 53 */
							obj_t BgL_tmpz00_12028;

							BgL_tmpz00_12028 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2160z00_7565 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_12028);
						}
						BgL_arg2161z00_7566 =
							(((BgL_pragmaz00_bglt) COBJECT(
									((BgL_pragmaz00_bglt) BgL_nodez00_6684)))->BgL_srfi0z00);
						{	/* Tools/trace.sch 53 */
							obj_t BgL_list2162z00_7567;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_arg2163z00_7568;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg2164z00_7569;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg2165z00_7570;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg2166z00_7571;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg2167z00_7572;

												BgL_arg2167z00_7572 =
													MAKE_YOUNG_PAIR(BgL_arg2161z00_7566, BNIL);
												BgL_arg2166z00_7571 =
													MAKE_YOUNG_PAIR(BGl_string2961z00zzcgen_cgenz00,
													BgL_arg2167z00_7572);
											}
											BgL_arg2165z00_7570 =
												MAKE_YOUNG_PAIR(BGl_string2962z00zzcgen_cgenz00,
												BgL_arg2166z00_7571);
										}
										BgL_arg2164z00_7569 =
											MAKE_YOUNG_PAIR(BINT(528L), BgL_arg2165z00_7570);
									}
									BgL_arg2163z00_7568 =
										MAKE_YOUNG_PAIR(BGl_string2963z00zzcgen_cgenz00,
										BgL_arg2164z00_7569);
								}
								BgL_list2162z00_7567 =
									MAKE_YOUNG_PAIR(BGl_string2964z00zzcgen_cgenz00,
									BgL_arg2163z00_7568);
							}
							BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg2160z00_7565,
								BgL_list2162z00_7567);
						}
					}
					{	/* Tools/trace.sch 53 */
						BgL_catomz00_bglt BgL_new1238z00_7573;

						{	/* Tools/trace.sch 53 */
							BgL_catomz00_bglt BgL_new1237z00_7574;

							BgL_new1237z00_7574 =
								((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_catomz00_bgl))));
							{	/* Tools/trace.sch 53 */
								long BgL_arg2168z00_7575;

								BgL_arg2168z00_7575 = BGL_CLASS_NUM(BGl_catomz00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1237z00_7574),
									BgL_arg2168z00_7575);
							}
							BgL_new1238z00_7573 = BgL_new1237z00_7574;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1238z00_7573)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_pragmaz00_bglt)
													BgL_nodez00_6684))))->BgL_locz00)), BUNSPEC);
						{
							obj_t BgL_auxz00_12050;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_aux2899z00_7576;

								BgL_aux2899z00_7576 = BGl_typez00zztype_typez00;
								{
									obj_t BgL_auxz00_12052;

									BgL_auxz00_12052 =
										BGl_typezd2errorzd2zz__errorz00
										(BGl_string2965z00zzcgen_cgenz00, BINT(20252L),
										BGl_string2966z00zzcgen_cgenz00,
										BGl_string2967z00zzcgen_cgenz00, BgL_aux2899z00_7576);
									FAILURE(BgL_auxz00_12052, BFALSE, BFALSE);
							}}
							((((BgL_copz00_bglt) COBJECT(
											((BgL_copz00_bglt) BgL_new1238z00_7573)))->BgL_typez00) =
								((BgL_typez00_bglt) BgL_auxz00_12050), BUNSPEC);
						}
						((((BgL_catomz00_bglt) COBJECT(BgL_new1238z00_7573))->
								BgL_valuez00) = ((obj_t) BINT(0L)), BUNSPEC);
						return ((BgL_copz00_bglt) BgL_new1238z00_7573);
					}
				}
		}

	}



/* &node->cop-sync1745 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2sync1745z81zzcgen_cgenz00(obj_t
		BgL_envz00_6687, obj_t BgL_nodez00_6688, obj_t BgL_kontz00_6689,
		obj_t BgL_inpushexitz00_6690)
	{
		{	/* Cgen/cgen.scm 488 */
			return
				BGl_nodezd2ze3copz31zzcgen_cgenz00
				(BGl_synczd2ze3sequencez31zzsync_nodez00(((BgL_syncz00_bglt)
						BgL_nodez00_6688)), BgL_kontz00_6689,
				CBOOL(BgL_inpushexitz00_6690));
		}

	}



/* &node->cop-sequence1743 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2sequence1743z81zzcgen_cgenz00(obj_t
		BgL_envz00_6691, obj_t BgL_nodez00_6692, obj_t BgL_kontz00_6693,
		obj_t BgL_inpushexitz00_6694)
	{
		{	/* Cgen/cgen.scm 443 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_expz00_7579;

				BgL_expz00_7579 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_6692)))->BgL_nodesz00);
				if (NULLP(BgL_expz00_7579))
					{	/* Tools/trace.sch 53 */
						BgL_nopz00_bglt BgL_arg2108z00_7580;

						{	/* Tools/trace.sch 53 */
							BgL_nopz00_bglt BgL_new1223z00_7581;

							{	/* Tools/trace.sch 53 */
								BgL_nopz00_bglt BgL_new1222z00_7582;

								BgL_new1222z00_7582 =
									((BgL_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_nopz00_bgl))));
								{	/* Tools/trace.sch 53 */
									long BgL_arg2109z00_7583;

									BgL_arg2109z00_7583 = BGL_CLASS_NUM(BGl_nopz00zzcgen_copz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1222z00_7582),
										BgL_arg2109z00_7583);
								}
								BgL_new1223z00_7581 = BgL_new1222z00_7582;
							}
							((((BgL_copz00_bglt) COBJECT(
											((BgL_copz00_bglt) BgL_new1223z00_7581)))->BgL_locz00) =
								((obj_t) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_sequencez00_bglt)
														BgL_nodez00_6692))))->BgL_locz00)), BUNSPEC);
							((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
												BgL_new1223z00_7581)))->BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2voidza2z00zztype_cachez00)), BUNSPEC);
							BgL_arg2108z00_7580 = BgL_new1223z00_7581;
						}
						return
							((BgL_copz00_bglt)
							BGL_PROCEDURE_CALL1(BgL_kontz00_6693,
								((obj_t) BgL_arg2108z00_7580)));
					}
				else
					{	/* Tools/trace.sch 53 */
						if (NULLP(CDR(BgL_expz00_7579)))
							{	/* Tools/trace.sch 53 */
								BgL_copz00_bglt BgL_copz00_7584;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg2113z00_7585;

									BgL_arg2113z00_7585 = CAR(BgL_expz00_7579);
									BgL_copz00_7584 =
										BGl_nodezd2ze3copz31zzcgen_cgenz00(
										((BgL_nodez00_bglt) BgL_arg2113z00_7585), BgL_kontz00_6693,
										CBOOL(BgL_inpushexitz00_6694));
								}
								{	/* Tools/trace.sch 53 */
									BgL_stopz00_bglt BgL_new1225z00_7586;

									{	/* Tools/trace.sch 53 */
										BgL_stopz00_bglt BgL_new1224z00_7587;

										BgL_new1224z00_7587 =
											((BgL_stopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_stopz00_bgl))));
										{	/* Tools/trace.sch 53 */
											long BgL_arg2112z00_7588;

											BgL_arg2112z00_7588 =
												BGL_CLASS_NUM(BGl_stopz00zzcgen_copz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1224z00_7587),
												BgL_arg2112z00_7588);
										}
										BgL_new1225z00_7586 = BgL_new1224z00_7587;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1225z00_7586)))->
											BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1225z00_7586)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
									((((BgL_stopz00_bglt) COBJECT(BgL_new1225z00_7586))->
											BgL_valuez00) =
										((BgL_copz00_bglt) BgL_copz00_7584), BUNSPEC);
									return ((BgL_copz00_bglt) BgL_new1225z00_7586);
								}
							}
						else
							{	/* Tools/trace.sch 53 */
								obj_t BgL_inpushexitz00_7589;

								if (CBOOL(BgL_inpushexitz00_6694))
									{	/* Tools/trace.sch 53 */
										BgL_inpushexitz00_7589 = BgL_inpushexitz00_6694;
									}
								else
									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg2130z00_7590;

										BgL_arg2130z00_7590 = CAR(BgL_expz00_7579);
										BgL_inpushexitz00_7589 =
											BBOOL(BGl_iszd2pushzd2exitzf3zf3zzcgen_cgenz00(
												((BgL_nodez00_bglt) BgL_arg2130z00_7590)));
									}
								{
									obj_t BgL_expz00_7592;
									obj_t BgL_newz00_7593;

									{
										BgL_csequencez00_bglt BgL_auxz00_12110;

										BgL_expz00_7592 = BgL_expz00_7579;
										BgL_newz00_7593 = BNIL;
									BgL_loopz00_7591:
										if (NULLP(CDR(((obj_t) BgL_expz00_7592))))
											{	/* Tools/trace.sch 53 */
												BgL_copz00_bglt BgL_copz00_7594;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg2119z00_7595;

													BgL_arg2119z00_7595 = CAR(((obj_t) BgL_expz00_7592));
													BgL_copz00_7594 =
														BGl_nodezd2ze3copz31zzcgen_cgenz00(
														((BgL_nodez00_bglt) BgL_arg2119z00_7595),
														BgL_kontz00_6693, CBOOL(BgL_inpushexitz00_7589));
												}
												{	/* Tools/trace.sch 53 */
													BgL_csequencez00_bglt BgL_new1229z00_7596;

													{	/* Tools/trace.sch 53 */
														BgL_csequencez00_bglt BgL_new1228z00_7597;

														BgL_new1228z00_7597 =
															((BgL_csequencez00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_csequencez00_bgl))));
														{	/* Tools/trace.sch 53 */
															long BgL_arg2118z00_7598;

															BgL_arg2118z00_7598 =
																BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1228z00_7597),
																BgL_arg2118z00_7598);
														}
														BgL_new1229z00_7596 = BgL_new1228z00_7597;
													}
													((((BgL_copz00_bglt) COBJECT(
																	((BgL_copz00_bglt) BgL_new1229z00_7596)))->
															BgL_locz00) =
														((obj_t) (((BgL_copz00_bglt)
																	COBJECT(BgL_copz00_7594))->BgL_locz00)),
														BUNSPEC);
													((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																		BgL_new1229z00_7596)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_copz00_bglt)
																	COBJECT(BgL_copz00_7594))->BgL_typez00)),
														BUNSPEC);
													((((BgL_csequencez00_bglt)
																COBJECT(BgL_new1229z00_7596))->
															BgL_czd2expzf3z21) =
														((bool_t) ((bool_t) 0)), BUNSPEC);
													{
														obj_t BgL_auxz00_12131;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg2117z00_7599;

															BgL_arg2117z00_7599 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_copz00_7594), BgL_newz00_7593);
															BgL_auxz00_12131 =
																bgl_reverse_bang(BgL_arg2117z00_7599);
														}
														((((BgL_csequencez00_bglt)
																	COBJECT(BgL_new1229z00_7596))->BgL_copsz00) =
															((obj_t) BgL_auxz00_12131), BUNSPEC);
													}
													BgL_auxz00_12110 = BgL_new1229z00_7596;
											}}
										else
											{	/* Tools/trace.sch 53 */
												bool_t BgL_test3302z00_12136;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg2127z00_7600;

													BgL_arg2127z00_7600 = CAR(((obj_t) BgL_expz00_7592));
													BgL_test3302z00_12136 =
														BGl_sidezd2effectzf3z21zzeffect_effectz00(
														((BgL_nodez00_bglt) BgL_arg2127z00_7600));
												}
												if (BgL_test3302z00_12136)
													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg2122z00_7601;
														obj_t BgL_arg2123z00_7602;

														BgL_arg2122z00_7601 =
															CDR(((obj_t) BgL_expz00_7592));
														{	/* Tools/trace.sch 53 */
															BgL_copz00_bglt BgL_arg2124z00_7603;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg2125z00_7604;

																BgL_arg2125z00_7604 =
																	CAR(((obj_t) BgL_expz00_7592));
																BgL_arg2124z00_7603 =
																	BGl_nodezd2ze3copz31zzcgen_cgenz00(
																	((BgL_nodez00_bglt) BgL_arg2125z00_7604),
																	BGl_za2stopzd2kontza2zd2zzcgen_cgenz00,
																	CBOOL(BgL_inpushexitz00_7589));
															}
															BgL_arg2123z00_7602 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg2124z00_7603), BgL_newz00_7593);
														}
														{
															obj_t BgL_newz00_12151;
															obj_t BgL_expz00_12150;

															BgL_expz00_12150 = BgL_arg2122z00_7601;
															BgL_newz00_12151 = BgL_arg2123z00_7602;
															BgL_newz00_7593 = BgL_newz00_12151;
															BgL_expz00_7592 = BgL_expz00_12150;
															goto BgL_loopz00_7591;
														}
													}
												else
													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg2126z00_7605;

														BgL_arg2126z00_7605 =
															CDR(((obj_t) BgL_expz00_7592));
														{
															obj_t BgL_expz00_12154;

															BgL_expz00_12154 = BgL_arg2126z00_7605;
															BgL_expz00_7592 = BgL_expz00_12154;
															goto BgL_loopz00_7591;
														}
													}
											}
										return ((BgL_copz00_bglt) BgL_auxz00_12110);
									}
								}
							}
					}
			}
		}

	}



/* &node->cop-closure1741 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2closure1741z81zzcgen_cgenz00(obj_t
		BgL_envz00_6695, obj_t BgL_nodez00_6696, obj_t BgL_kontz00_6697,
		obj_t BgL_inpushexitz00_6698)
	{
		{	/* Cgen/cgen.scm 434 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_arg2106z00_7607;

				BgL_arg2106z00_7607 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_6696)));
				return
					((BgL_copz00_bglt)
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2929z00zzcgen_cgenz00, BGl_string2968z00zzcgen_cgenz00,
						BgL_arg2106z00_7607));
			}
		}

	}



/* &node->cop-var1739 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2var1739z81zzcgen_cgenz00(obj_t
		BgL_envz00_6699, obj_t BgL_nodez00_6700, obj_t BgL_kontz00_6701,
		obj_t BgL_inpushexitz00_6702)
	{
		{	/* Cgen/cgen.scm 418 */
			{	/* Tools/trace.sch 53 */
				BgL_varcz00_bglt BgL_arg2103z00_7609;

				{	/* Tools/trace.sch 53 */
					BgL_varcz00_bglt BgL_new1220z00_7610;

					{	/* Tools/trace.sch 53 */
						BgL_varcz00_bglt BgL_new1219z00_7611;

						BgL_new1219z00_7611 =
							((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_varcz00_bgl))));
						{	/* Tools/trace.sch 53 */
							long BgL_arg2105z00_7612;

							BgL_arg2105z00_7612 = BGL_CLASS_NUM(BGl_varcz00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1219z00_7611),
								BgL_arg2105z00_7612);
						}
						BgL_new1220z00_7610 = BgL_new1219z00_7611;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1220z00_7610)))->BgL_locz00) =
						((obj_t) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
												BgL_nodez00_6700))))->BgL_locz00)), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1220z00_7610)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_variablez00_bglt)
									COBJECT((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
														BgL_nodez00_6700)))->BgL_variablez00)))->
								BgL_typez00)), BUNSPEC);
					((((BgL_varcz00_bglt) COBJECT(BgL_new1220z00_7610))->
							BgL_variablez00) =
						((BgL_variablez00_bglt) (((BgL_varz00_bglt)
									COBJECT(((BgL_varz00_bglt) BgL_nodez00_6700)))->
								BgL_variablez00)), BUNSPEC);
					BgL_arg2103z00_7609 = BgL_new1220z00_7610;
				}
				return
					((BgL_copz00_bglt)
					BGL_PROCEDURE_CALL1(BgL_kontz00_6701, ((obj_t) BgL_arg2103z00_7609)));
			}
		}

	}



/* &node->cop-kwote1737 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2kwote1737z81zzcgen_cgenz00(obj_t
		BgL_envz00_6703, obj_t BgL_nodez00_6704, obj_t BgL_kontz00_6705,
		obj_t BgL_inpushexitz00_6706)
	{
		{	/* Cgen/cgen.scm 409 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_arg2102z00_7614;

				BgL_arg2102z00_7614 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_kwotez00_bglt) BgL_nodez00_6704)));
				return
					((BgL_copz00_bglt)
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2929z00zzcgen_cgenz00, BGl_string2969z00zzcgen_cgenz00,
						BgL_arg2102z00_7614));
			}
		}

	}



/* &node->cop-genpatchid1735 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2genpatchid1735z81zzcgen_cgenz00(obj_t
		BgL_envz00_6707, obj_t BgL_nodez00_6708, obj_t BgL_kontz00_6709,
		obj_t BgL_inpushexitz00_6710)
	{
		{	/* Cgen/cgen.scm 395 */
			{	/* Tools/trace.sch 53 */
				BgL_catomz00_bglt BgL_arg2100z00_7616;

				{	/* Tools/trace.sch 53 */
					BgL_catomz00_bglt BgL_new1217z00_7617;

					{	/* Tools/trace.sch 53 */
						BgL_catomz00_bglt BgL_new1216z00_7618;

						BgL_new1216z00_7618 =
							((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_catomz00_bgl))));
						{	/* Tools/trace.sch 53 */
							long BgL_arg2101z00_7619;

							BgL_arg2101z00_7619 = BGL_CLASS_NUM(BGl_catomz00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1216z00_7618),
								BgL_arg2101z00_7619);
						}
						BgL_new1217z00_7617 = BgL_new1216z00_7618;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1217z00_7617)))->BgL_locz00) =
						((obj_t) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_genpatchidz00_bglt)
												BgL_nodez00_6708))))->BgL_locz00)), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1217z00_7617)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BGl_za2longza2z00zztype_cachez00)), BUNSPEC);
					((((BgL_catomz00_bglt) COBJECT(BgL_new1217z00_7617))->BgL_valuez00) =
						((obj_t) BINT((((BgL_genpatchidz00_bglt)
										COBJECT(((BgL_genpatchidz00_bglt) BgL_nodez00_6708)))->
									BgL_indexz00))), BUNSPEC);
					BgL_arg2100z00_7616 = BgL_new1217z00_7617;
				}
				return
					((BgL_copz00_bglt)
					BGL_PROCEDURE_CALL1(BgL_kontz00_6709, ((obj_t) BgL_arg2100z00_7616)));
			}
		}

	}



/* &node->cop-patch1732 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2patch1732z81zzcgen_cgenz00(obj_t
		BgL_envz00_6711, obj_t BgL_nodez00_6712, obj_t BgL_kontz00_6713,
		obj_t BgL_inpushexitz00_6714)
	{
		{	/* Cgen/cgen.scm 361 */
			{	/* Tools/trace.sch 53 */
				BgL_genpatchidz00_bglt BgL_i1203z00_7621;

				BgL_i1203z00_7621 =
					((BgL_genpatchidz00_bglt)
					(((BgL_patchz00_bglt) COBJECT(
								((BgL_patchz00_bglt) BgL_nodez00_6712)))->BgL_patchidz00));
				{	/* Tools/trace.sch 53 */
					BgL_varz00_bglt BgL_i1204z00_7622;

					BgL_i1204z00_7622 =
						((BgL_varz00_bglt)
						(((BgL_atomz00_bglt) COBJECT(
									((BgL_atomz00_bglt)
										((BgL_patchz00_bglt) BgL_nodez00_6712))))->BgL_valuez00));
					{	/* Tools/trace.sch 53 */
						BgL_csequencez00_bglt BgL_new1206z00_7623;

						{	/* Tools/trace.sch 53 */
							BgL_csequencez00_bglt BgL_new1205z00_7624;

							BgL_new1205z00_7624 =
								((BgL_csequencez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_csequencez00_bgl))));
							{	/* Tools/trace.sch 53 */
								long BgL_arg2099z00_7625;

								BgL_arg2099z00_7625 =
									BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1205z00_7624),
									BgL_arg2099z00_7625);
							}
							BgL_new1206z00_7623 = BgL_new1205z00_7624;
						}
						((((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt) BgL_new1206z00_7623)))->BgL_locz00) =
							((obj_t) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_patchz00_bglt)
													BgL_nodez00_6712))))->BgL_locz00)), BUNSPEC);
						((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
											BgL_new1206z00_7623)))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
						((((BgL_csequencez00_bglt) COBJECT(BgL_new1206z00_7623))->
								BgL_czd2expzf3z21) = ((bool_t) ((bool_t) 0)), BUNSPEC);
						{
							obj_t BgL_auxz00_12231;

							{	/* Tools/trace.sch 53 */
								BgL_cpragmaz00_bglt BgL_arg2076z00_7626;
								BgL_copz00_bglt BgL_arg2077z00_7627;

								{	/* Tools/trace.sch 53 */
									BgL_cpragmaz00_bglt BgL_new1208z00_7628;

									{	/* Tools/trace.sch 53 */
										BgL_cpragmaz00_bglt BgL_new1207z00_7629;

										BgL_new1207z00_7629 =
											((BgL_cpragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_cpragmaz00_bgl))));
										{	/* Tools/trace.sch 53 */
											long BgL_arg2097z00_7630;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_classz00_7631;

												BgL_classz00_7631 = BGl_cpragmaz00zzcgen_copz00;
												BgL_arg2097z00_7630 = BGL_CLASS_NUM(BgL_classz00_7631);
											}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1207z00_7629),
												BgL_arg2097z00_7630);
										}
										BgL_new1208z00_7628 = BgL_new1207z00_7629;
									}
									((((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_new1208z00_7628)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt)
													COBJECT(((BgL_nodez00_bglt) ((BgL_patchz00_bglt)
																BgL_nodez00_6712))))->BgL_locz00)), BUNSPEC);
									((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
														BgL_new1208z00_7628)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
									{
										obj_t BgL_auxz00_12244;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg2080z00_7632;

											{	/* Tools/trace.sch 53 */
												bool_t BgL_test3303z00_12245;

												{	/* Tools/trace.sch 53 */
													BgL_typez00_bglt BgL_arg2084z00_7633;

													BgL_arg2084z00_7633 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_patchz00_bglt) BgL_nodez00_6712))))->
														BgL_typez00);
													BgL_test3303z00_12245 =
														(((obj_t) BgL_arg2084z00_7633) ==
														BGl_za2objza2z00zztype_cachez00);
												}
												if (BgL_test3303z00_12245)
													{	/* Tools/trace.sch 53 */
														BgL_arg2080z00_7632 =
															BGl_bigloozd2configzd2zz__configurez00
															(CNST_TABLE_REF(18));
													}
												else
													{	/* Tools/trace.sch 53 */
														BgL_arg2080z00_7632 = BINT(32L);
													}
											}
											{	/* Tools/trace.sch 53 */
												obj_t BgL_list2081z00_7634;

												BgL_list2081z00_7634 =
													MAKE_YOUNG_PAIR(BgL_arg2080z00_7632, BNIL);
												BgL_auxz00_12244 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string2970z00zzcgen_cgenz00,
													BgL_list2081z00_7634);
											}
										}
										((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1208z00_7628))->
												BgL_formatz00) = ((obj_t) BgL_auxz00_12244), BUNSPEC);
									}
									{
										obj_t BgL_auxz00_12257;

										{	/* Tools/trace.sch 53 */
											BgL_catomz00_bglt BgL_arg2086z00_7635;
											BgL_catomz00_bglt BgL_arg2087z00_7636;
											BgL_varcz00_bglt BgL_arg2088z00_7637;

											{	/* Tools/trace.sch 53 */
												BgL_catomz00_bglt BgL_new1210z00_7638;

												{	/* Tools/trace.sch 53 */
													BgL_catomz00_bglt BgL_new1209z00_7639;

													BgL_new1209z00_7639 =
														((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_catomz00_bgl))));
													{	/* Tools/trace.sch 53 */
														long BgL_arg2093z00_7640;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_classz00_7641;

															BgL_classz00_7641 = BGl_catomz00zzcgen_copz00;
															BgL_arg2093z00_7640 =
																BGL_CLASS_NUM(BgL_classz00_7641);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1209z00_7639),
															BgL_arg2093z00_7640);
													}
													BgL_new1210z00_7638 = BgL_new1209z00_7639;
												}
												((((BgL_copz00_bglt) COBJECT(
																((BgL_copz00_bglt) BgL_new1210z00_7638)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_patchz00_bglt)
																			BgL_nodez00_6712))))->BgL_locz00)),
													BUNSPEC);
												((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																	BgL_new1210z00_7638)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BGl_za2longza2z00zztype_cachez00)), BUNSPEC);
												((((BgL_catomz00_bglt) COBJECT(BgL_new1210z00_7638))->
														BgL_valuez00) =
													((obj_t) BINT((((BgL_patchz00_bglt)
																	COBJECT(((BgL_patchz00_bglt)
																			BgL_nodez00_6712)))->BgL_indexz00))),
													BUNSPEC);
												BgL_arg2086z00_7635 = BgL_new1210z00_7638;
											}
											{	/* Tools/trace.sch 53 */
												BgL_catomz00_bglt BgL_new1212z00_7642;

												{	/* Tools/trace.sch 53 */
													BgL_catomz00_bglt BgL_new1211z00_7643;

													BgL_new1211z00_7643 =
														((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_catomz00_bgl))));
													{	/* Tools/trace.sch 53 */
														long BgL_arg2094z00_7644;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_classz00_7645;

															BgL_classz00_7645 = BGl_catomz00zzcgen_copz00;
															BgL_arg2094z00_7644 =
																BGL_CLASS_NUM(BgL_classz00_7645);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1211z00_7643),
															BgL_arg2094z00_7644);
													}
													BgL_new1212z00_7642 = BgL_new1211z00_7643;
												}
												((((BgL_copz00_bglt) COBJECT(
																((BgL_copz00_bglt) BgL_new1212z00_7642)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_patchz00_bglt)
																			BgL_nodez00_6712))))->BgL_locz00)),
													BUNSPEC);
												((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																	BgL_new1212z00_7642)))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BGl_za2longza2z00zztype_cachez00)), BUNSPEC);
												((((BgL_catomz00_bglt) COBJECT(BgL_new1212z00_7642))->
														BgL_valuez00) =
													((obj_t) BINT((((BgL_genpatchidz00_bglt)
																	COBJECT(BgL_i1203z00_7621))->BgL_indexz00))),
													BUNSPEC);
												BgL_arg2087z00_7636 = BgL_new1212z00_7642;
											}
											{	/* Tools/trace.sch 53 */
												BgL_varcz00_bglt BgL_new1214z00_7646;

												{	/* Tools/trace.sch 53 */
													BgL_varcz00_bglt BgL_new1213z00_7647;

													BgL_new1213z00_7647 =
														((BgL_varcz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_varcz00_bgl))));
													{	/* Tools/trace.sch 53 */
														long BgL_arg2096z00_7648;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_classz00_7649;

															BgL_classz00_7649 = BGl_varcz00zzcgen_copz00;
															BgL_arg2096z00_7648 =
																BGL_CLASS_NUM(BgL_classz00_7649);
														}
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1213z00_7647),
															BgL_arg2096z00_7648);
													}
													BgL_new1214z00_7646 = BgL_new1213z00_7647;
												}
												((((BgL_copz00_bglt) COBJECT(
																((BgL_copz00_bglt) BgL_new1214z00_7646)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_patchz00_bglt)
																			BgL_nodez00_6712))))->BgL_locz00)),
													BUNSPEC);
												((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																	BgL_new1214z00_7646)))->BgL_typez00) =
													((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																COBJECT((((BgL_varz00_bglt)
																			COBJECT(BgL_i1204z00_7622))->
																		BgL_variablez00)))->BgL_typez00)), BUNSPEC);
												((((BgL_varcz00_bglt) COBJECT(BgL_new1214z00_7646))->
														BgL_variablez00) =
													((BgL_variablez00_bglt) (((BgL_varz00_bglt)
																COBJECT(BgL_i1204z00_7622))->BgL_variablez00)),
													BUNSPEC);
												BgL_arg2088z00_7637 = BgL_new1214z00_7646;
											}
											{	/* Tools/trace.sch 53 */
												obj_t BgL_list2089z00_7650;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg2090z00_7651;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg2091z00_7652;

														BgL_arg2091z00_7652 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2088z00_7637), BNIL);
														BgL_arg2090z00_7651 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2087z00_7636),
															BgL_arg2091z00_7652);
													}
													BgL_list2089z00_7650 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg2086z00_7635), BgL_arg2090z00_7651);
												}
												BgL_auxz00_12257 = BgL_list2089z00_7650;
										}}
										((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1208z00_7628))->
												BgL_argsz00) = ((obj_t) BgL_auxz00_12257), BUNSPEC);
									}
									BgL_arg2076z00_7626 = BgL_new1208z00_7628;
								}
								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg2098z00_7653;

									BgL_arg2098z00_7653 =
										(((BgL_atomz00_bglt) COBJECT(
												((BgL_atomz00_bglt)
													((BgL_patchz00_bglt) BgL_nodez00_6712))))->
										BgL_valuez00);
									BgL_arg2077z00_7627 =
										BGl_nodezd2ze3copz31zzcgen_cgenz00(((BgL_nodez00_bglt)
											BgL_arg2098z00_7653), BgL_kontz00_6713,
										CBOOL(BgL_inpushexitz00_6714));
								}
								{	/* Tools/trace.sch 53 */
									obj_t BgL_list2078z00_7654;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg2079z00_7655;

										BgL_arg2079z00_7655 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg2077z00_7627), BNIL);
										BgL_list2078z00_7654 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_arg2076z00_7626), BgL_arg2079z00_7655);
									}
									BgL_auxz00_12231 = BgL_list2078z00_7654;
							}}
							((((BgL_csequencez00_bglt) COBJECT(BgL_new1206z00_7623))->
									BgL_copsz00) = ((obj_t) BgL_auxz00_12231), BUNSPEC);
						}
						return ((BgL_copz00_bglt) BgL_new1206z00_7623);
					}
				}
			}
		}

	}



/* &node->cop-literal1730 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2literal1730z81zzcgen_cgenz00(obj_t
		BgL_envz00_6715, obj_t BgL_nodez00_6716, obj_t BgL_kontz00_6717,
		obj_t BgL_inpushexitz00_6718)
	{
		{	/* Cgen/cgen.scm 346 */
			{	/* Tools/trace.sch 53 */
				BgL_catomz00_bglt BgL_arg2074z00_7657;

				{	/* Tools/trace.sch 53 */
					BgL_catomz00_bglt BgL_new1201z00_7658;

					{	/* Tools/trace.sch 53 */
						BgL_catomz00_bglt BgL_new1200z00_7659;

						BgL_new1200z00_7659 =
							((BgL_catomz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_catomz00_bgl))));
						{	/* Tools/trace.sch 53 */
							long BgL_arg2075z00_7660;

							BgL_arg2075z00_7660 = BGL_CLASS_NUM(BGl_catomz00zzcgen_copz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1200z00_7659),
								BgL_arg2075z00_7660);
						}
						BgL_new1201z00_7658 = BgL_new1200z00_7659;
					}
					((((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_new1201z00_7658)))->BgL_locz00) =
						((obj_t) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_atomz00_bglt) (
													(BgL_literalz00_bglt) BgL_nodez00_6716)))))->
								BgL_locz00)), BUNSPEC);
					((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
										BgL_new1201z00_7658)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_atomz00_bglt) (
													(BgL_literalz00_bglt) BgL_nodez00_6716)))))->
								BgL_typez00)), BUNSPEC);
					((((BgL_catomz00_bglt) COBJECT(BgL_new1201z00_7658))->BgL_valuez00) =
						((obj_t) (((BgL_atomz00_bglt)
									COBJECT(((BgL_atomz00_bglt) ((BgL_literalz00_bglt)
												BgL_nodez00_6716))))->BgL_valuez00)), BUNSPEC);
					BgL_arg2074z00_7657 = BgL_new1201z00_7658;
				}
				return
					((BgL_copz00_bglt)
					BGL_PROCEDURE_CALL1(BgL_kontz00_6717, ((obj_t) BgL_arg2074z00_7657)));
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcgen_cgenz00(void)
	{
		{	/* Cgen/cgen.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzast_appz00(449859288L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzsync_nodez00(421078394L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzcgen_copz00(529595144L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzcgen_emitzd2copzd2(371489745L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzcgen_cappz00(426639521L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(474089076L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(364917963L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string2971z00zzcgen_cgenz00));
		}

	}

#ifdef __cplusplus
}
#endif
