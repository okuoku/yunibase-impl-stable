/*===========================================================================*/
/*   (Cgen/emit_cop.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cgen/emit_cop.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_CGEN_EMITzd2COPzd2_TYPE_DEFINITIONS
#define BGL_BgL_CGEN_EMITzd2COPzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_copz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}             *BgL_copz00_bglt;

	typedef struct BgL_clabelz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_namez00;
		bool_t BgL_usedzf3zf3;
		obj_t BgL_bodyz00;
	}                *BgL_clabelz00_bglt;

	typedef struct BgL_cgotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_clabelz00_bgl *BgL_labelz00;
	}               *BgL_cgotoz00_bglt;

	typedef struct BgL_cblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                *BgL_cblockz00_bglt;

	typedef struct BgL_creturnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_tailz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                 *BgL_creturnz00_bglt;

	typedef struct BgL_cvoidz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}               *BgL_cvoidz00_bglt;

	typedef struct BgL_catomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_catomz00_bglt;

	typedef struct BgL_varcz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}              *BgL_varcz00_bglt;

	typedef struct BgL_cpragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_formatz00;
		obj_t BgL_argsz00;
	}                 *BgL_cpragmaz00_bglt;

	typedef struct BgL_ccastz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_argz00;
	}               *BgL_ccastz00_bglt;

	typedef struct BgL_csequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_czd2expzf3z21;
		obj_t BgL_copsz00;
	}                   *BgL_csequencez00_bglt;

	typedef struct BgL_stopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}              *BgL_stopz00_bglt;

	typedef struct BgL_csetqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varcz00_bgl *BgL_varz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}               *BgL_csetqz00_bglt;

	typedef struct BgL_cifz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_testz00;
		struct BgL_copz00_bgl *BgL_truez00;
		struct BgL_copz00_bgl *BgL_falsez00;
	}             *BgL_cifz00_bglt;

	typedef struct BgL_localzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varsz00;
	}                     *BgL_localzd2varzd2_bglt;

	typedef struct BgL_cfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
	}                  *BgL_cfuncallz00_bglt;

	typedef struct BgL_capplyz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		struct BgL_copz00_bgl *BgL_argz00;
	}                *BgL_capplyz00_bglt;

	typedef struct BgL_cappz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}              *BgL_cappz00_bglt;

	typedef struct BgL_cfailz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_procz00;
		struct BgL_copz00_bgl *BgL_msgz00;
		struct BgL_copz00_bgl *BgL_objz00;
	}               *BgL_cfailz00_bglt;

	typedef struct BgL_cswitchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
	}                 *BgL_cswitchz00_bglt;

	typedef struct BgL_cmakezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
		obj_t BgL_stackablez00;
	}                     *BgL_cmakezd2boxzd2_bglt;

	typedef struct BgL_cboxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_varz00;
	}                    *BgL_cboxzd2refzd2_bglt;

	typedef struct BgL_cboxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_varz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                       *BgL_cboxzd2setz12zc0_bglt;

	typedef struct BgL_csetzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_exitz00;
		struct BgL_copz00_bgl *BgL_jumpzd2valuezd2;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                        *BgL_csetzd2exzd2itz00_bglt;

	typedef struct BgL_cjumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_exitz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}                         *BgL_cjumpzd2exzd2itz00_bglt;

	typedef struct BgL_bdbzd2blockzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                     *BgL_bdbzd2blockzd2_bglt;


#endif													// BGL_BgL_CGEN_EMITzd2COPzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62emitzd2copzd2csetq1414z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	static obj_t BGl_z62emitzd2copzd2cif1416z62zzcgen_emitzd2copzd2(obj_t, obj_t);
	extern obj_t
		BGl_makezd2typedzd2declarationz00zztype_toolsz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcgen_emitzd2copzd2 = BUNSPEC;
	static obj_t BGl_z62emitzd2copzd2cpragma1401z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(obj_t);
	extern obj_t BGl_getzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	static obj_t BGl_z62emitzd2copzd2csequence1406z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	static obj_t BGl_z62emitzd2copzd2cfail1430z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2czd2portza2zd2zzbackend_c_emitz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00(obj_t,
		BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	extern obj_t BGl_clabelz00zzcgen_copz00;
	extern obj_t BGl_cappz00zzcgen_copz00;
	BGL_EXPORTED_DECL obj_t BGl_resetzd2bdbzd2locz12z12zzcgen_emitzd2copzd2(void);
	static obj_t BGl_toplevelzd2initzd2zzcgen_emitzd2copzd2(void);
	static obj_t BGl_z62emitzd2cop1382zb0zzcgen_emitzd2copzd2(obj_t, obj_t);
	extern obj_t BGl_cboxzd2refzd2zzcgen_copz00;
	extern obj_t BGl_csequencez00zzcgen_copz00;
	extern obj_t BGl_cgotoz00zzcgen_copz00;
	static obj_t BGl_z62emitzd2copzd2cgoto1388z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	static obj_t BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2 = BUNSPEC;
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_cfuncallzd2castszd2zzcgen_emitzd2copzd2 = BUNSPEC;
	static obj_t BGl_z62emitzd2copzd2bdbzd2block1420zb0zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzcgen_emitzd2copzd2(void);
	extern obj_t BGl_csetzd2exzd2itz00zzcgen_copz00;
	static obj_t BGl_emitzd2bdbzd2loczd2commentzd2zzcgen_emitzd2copzd2(obj_t);
	extern obj_t BGl_cvoidz00zzcgen_copz00;
	static obj_t BGl_objectzd2initzd2zzcgen_emitzd2copzd2(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_z62emitzd2copzd2localzd2var1418zb0zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	static obj_t BGl_z62emitzd2copzd2nop1408z62zzcgen_emitzd2copzd2(obj_t, obj_t);
	extern obj_t BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00;
	static obj_t BGl_z62getzd2currentzd2bdbzd2loczb0zzcgen_emitzd2copzd2(obj_t);
	static obj_t BGl_za2bfalseza2z00zzcgen_emitzd2copzd2 = BUNSPEC;
	static obj_t BGl_z62emitzd2copzb0zzcgen_emitzd2copzd2(obj_t, obj_t);
	extern obj_t BGl_capplyz00zzcgen_copz00;
	static obj_t BGl_z62funzd2castzd2vazd2strictzb0zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	extern obj_t BGl_cpragmaz00zzcgen_copz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t make_vector(long, obj_t);
	static obj_t BGl_z62emitzd2copzd2capply1426z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzcgen_emitzd2copzd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcgen_emitzd2copzd2(void);
	static obj_t BGl_z62emitzd2copzd2cvoid1396z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	static obj_t BGl_z62emitzd2copzd2stop1412z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	extern obj_t BGl_nopz00zzcgen_copz00;
	BGL_IMPORT obj_t bgl_close_input_port(obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	static obj_t BGl_outzd2callze70z35zzcgen_emitzd2copzd2(BgL_cfuncallz00_bglt,
		obj_t, obj_t, obj_t, bool_t);
	BGL_IMPORT obj_t rgc_buffer_substring(obj_t, long, long);
	extern obj_t BGl_cmakezd2boxzd2zzcgen_copz00;
	static obj_t BGl_z62emitzd2copzd2capp1428z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62emitzd2copzd2cblock1390z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	extern obj_t BGl_cifz00zzcgen_copz00;
	static obj_t BGl_z62emitzd2copzd2varc1399z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2currentzd2bdbzd2loczd2zzcgen_emitzd2copzd2(void);
	static obj_t BGl_z62emitzd2bdbzd2locz62zzcgen_emitzd2copzd2(obj_t, obj_t);
	static obj_t BGl_z62emitzd2copzd2clabel1386z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_cboxzd2setz12zc0zzcgen_copz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcgen_emitzd2copzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_copz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__rgcz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL bool_t
		BGl_emitzd2copzd2zzcgen_emitzd2copzd2(BgL_copz00_bglt);
	static bool_t
		BGl_emitzd2prefixzd2cappze70ze7zzcgen_emitzd2copzd2(BgL_cappz00_bglt);
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t
		BGl_z62emitzd2copzd2csetzd2exzd2it1440z62zzcgen_emitzd2copzd2(obj_t, obj_t);
	extern obj_t BGl_csetqz00zzcgen_copz00;
	extern obj_t BGl_localzd2varzd2zzcgen_copz00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_cblockz00zzcgen_copz00;
	extern obj_t BGl_cswitchz00zzcgen_copz00;
	static obj_t BGl_cnstzd2initzd2zzcgen_emitzd2copzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcgen_emitzd2copzd2(void);
	static obj_t BGl_z62funzd2lzd2castz62zzcgen_emitzd2copzd2(obj_t, obj_t);
	extern obj_t BGl_bdbzd2blockzd2zzcgen_copz00;
	extern obj_t BGl_copz00zzcgen_copz00;
	BGL_IMPORT long bgl_list_length(obj_t);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzcgen_emitzd2copzd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzcgen_emitzd2copzd2(void);
	BGL_IMPORT obj_t bgl_display_fixnum(obj_t, obj_t);
	extern obj_t BGl_za2modulezd2locationza2zd2zzmodule_modulez00;
	extern obj_t BGl_catomz00zzcgen_copz00;
	BGL_IMPORT obj_t BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(int, obj_t);
	static obj_t BGl_z62emitzd2copzd2cswitch1432z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	static obj_t BGl_z62emitzd2copzd2catom1394z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	extern obj_t BGl_cfailz00zzcgen_copz00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static obj_t BGl_z62emitzd2copzd2creturn1392z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	static obj_t BGl_z62emitzd2copzd2cfuncall1423z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	extern obj_t BGl_stopz00zzcgen_copz00;
	static obj_t BGl_callzd2castze70z35zzcgen_emitzd2copzd2(BgL_cfuncallz00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_creturnz00zzcgen_copz00;
	static obj_t
		BGl_z62emitzd2copzd2cboxzd2setz121438za2zzcgen_emitzd2copzd2(obj_t, obj_t);
	extern obj_t BGl_cfuncallz00zzcgen_copz00;
	extern obj_t BGl_ccastz00zzcgen_copz00;
	static obj_t
		BGl_z62emitzd2copzd2cjumpzd2exzd2it1442z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	static obj_t BGl_thezd2stringze70z35zzcgen_emitzd2copzd2(obj_t);
	extern obj_t BGl_cjumpzd2exzd2itz00zzcgen_copz00;
	static obj_t BGl_z62emitzd2copzd2cmakezd2box1434zb0zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	extern obj_t
		BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00;
	static obj_t BGl_z62emitzd2copzd2cboxzd2ref1436zb0zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	BGL_IMPORT bool_t rgc_fill_buffer(obj_t);
	static obj_t BGl_z62emitzd2copzd2ccast1403z62zzcgen_emitzd2copzd2(obj_t,
		obj_t);
	static obj_t BGl_z62resetzd2bdbzd2locz12z70zzcgen_emitzd2copzd2(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62funzd2castzb0zzcgen_emitzd2copzd2(obj_t, obj_t);
	extern obj_t BGl_varcz00zzcgen_copz00;
	static obj_t __cnst[9];


	   
		 
		DEFINE_STRING(BGl_string2400z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2400za700za7za7c2470za7, "JUMP_EXIT( ", 11);
	      DEFINE_STRING(BGl_string2401z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2401za700za7za7c2471za7, "if( SET_EXIT(", 13);
	      DEFINE_STRING(BGl_string2402z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2402za700za7za7c2472za7, " ) ) { ", 7);
	      DEFINE_STRING(BGl_string2403z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2403za700za7za7c2473za7, ";", 1);
	      DEFINE_STRING(BGl_string2404z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2404za700za7za7c2474za7, "} else {\n", 9);
	      DEFINE_STRING(BGl_string2405z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2405za700za7za7c2475za7, "#if( SIGSETJMP_SAVESIGS == 0 )\n",
		31);
	      DEFINE_STRING(BGl_string2406z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2406za700za7za7c2476za7,
		"  // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c\n", 53);
	      DEFINE_STRING(BGl_string2407z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2407za700za7za7c2477za7,
		"  // bgl_restore_signal_handlers();\n", 36);
	      DEFINE_STRING(BGl_string2408z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2408za700za7za7c2478za7, "#endif\n", 7);
	      DEFINE_STRING(BGl_string2409z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2409za700za7za7c2479za7, "}", 1);
	      DEFINE_STRING(BGl_string2410z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2410za700za7za7c2480za7, "CELL_SET(", 9);
	      DEFINE_STRING(BGl_string2411z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2411za700za7za7c2481za7, ", ", 2);
	      DEFINE_STRING(BGl_string2412z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2412za700za7za7c2482za7, "((", 2);
	      DEFINE_STRING(BGl_string2413z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2413za700za7za7c2483za7, ")", 1);
	      DEFINE_STRING(BGl_string2414z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2414za700za7za7c2484za7, "CELL_REF(", 9);
	      DEFINE_STRING(BGl_string2415z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2415za700za7za7c2485za7, "))", 2);
	      DEFINE_STRING(BGl_string2416z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2416za700za7za7c2486za7, "MAKE_CELL_STACK(", 16);
	      DEFINE_STRING(BGl_string2417z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2417za700za7za7c2487za7, "MAKE_CELL(", 10);
	      DEFINE_STRING(BGl_string2418z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2418za700za7za7c2488za7, "switch( ", 8);
	      DEFINE_STRING(BGl_string2419z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2419za700za7za7c2489za7, ") { ", 4);
	      DEFINE_STRING(BGl_string2420z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2420za700za7za7c2490za7, "default: ", 9);
	      DEFINE_STRING(BGl_string2421z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2421za700za7za7c2491za7, "; ", 2);
	      DEFINE_STRING(BGl_string2422z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2422za700za7za7c2492za7, "} ", 2);
	      DEFINE_STRING(BGl_string2423z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2423za700za7za7c2493za7, "case ", 5);
	      DEFINE_STRING(BGl_string2424z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2424za700za7za7c2494za7, " : ", 3);
	      DEFINE_STRING(BGl_string2425z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2425za700za7za7c2495za7, "break;", 6);
	      DEFINE_STRING(BGl_string2426z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2426za700za7za7c2496za7, "exit( -1 );", 11);
	      DEFINE_STRING(BGl_string2427z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2427za700za7za7c2497za7, "FAILURE(", 8);
	      DEFINE_STRING(BGl_string2428z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2428za700za7za7c2498za7, ");", 2);
	      DEFINE_STRING(BGl_string2429z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2429za700za7za7c2499za7, "the_failure(", 12);
	      DEFINE_STRING(BGl_string2430z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2430za700za7za7c2500za7, "), exit( -1 );", 14);
	      DEFINE_STRING(BGl_string2431z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2431za700za7za7c2501za7, "Illegal infix macro", 19);
	      DEFINE_STRING(BGl_string2432z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2432za700za7za7c2502za7, "apply(", 6);
	      DEFINE_STRING(BGl_string2436z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2436za700za7za7c2503za7, "PROCEDURE_L_ENTRY", 17);
	      DEFINE_STRING(BGl_string2437z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2437za700za7za7c2504za7, "PROCEDURE_ENTRY", 15);
	      DEFINE_STRING(BGl_string2438z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2438za700za7za7c2505za7, "()", 2);
	      DEFINE_STRING(BGl_string2439z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2439za700za7za7c2506za7, "(VA_PROCEDUREP( ", 16);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2433z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762funza7d2castza7b02507za7,
		BGl_z62funzd2castzb0zzcgen_emitzd2copzd2);
	      DEFINE_STRING(BGl_string2440z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2440za700za7za7c2508za7, " ) ? ", 5);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2434z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762funza7d2castza7d22509za7,
		BGl_z62funzd2castzd2vazd2strictzb0zzcgen_emitzd2copzd2);
	      DEFINE_STRING(BGl_string2441z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2441za700za7za7c2510za7, " )", 2);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2435z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762funza7d2lza7d2cas2511za7,
		BGl_z62funzd2lzd2castz62zzcgen_emitzd2copzd2);
	      DEFINE_STRING(BGl_string2442z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2442za700za7za7c2512za7, "BGL_PROCEDURE_CALL", 18);
	      DEFINE_STRING(BGl_string2443z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2443za700za7za7c2513za7, "(", 1);
	      DEFINE_STRING(BGl_string2444z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2444za700za7za7c2514za7, "obj_t", 5);
	      DEFINE_STRING(BGl_string2445z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2445za700za7za7c2515za7, "))(", 3);
	      DEFINE_STRING(BGl_string2446z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2446za700za7za7c2516za7, "(*)(~(, )))", 11);
	      DEFINE_STRING(BGl_string2365z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2365za700za7za7c2517za7, "\n#line ", 7);
	      DEFINE_STRING(BGl_string2447z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2447za700za7za7c2518za7, "(obj_t (*)(obj_t, ...))", 23);
	      DEFINE_STRING(BGl_string2366z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2366za700za7za7c2519za7, " \"", 2);
	      DEFINE_STRING(BGl_string2448z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2448za700za7za7c2520za7, "(obj_t (*)(~(, )))", 18);
	      DEFINE_STRING(BGl_string2367z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2367za700za7za7c2521za7, "/* ", 3);
	      DEFINE_STRING(BGl_string2449z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2449za700za7za7c2522za7,
		"int bigloo_dummy_bdb; bigloo_dummy_bdb = 0; { ", 46);
	      DEFINE_STRING(BGl_string2368z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2368za700za7za7c2523za7, " ", 1);
	      DEFINE_STRING(BGl_string2369z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2369za700za7za7c2524za7, " */", 3);
	      DEFINE_STRING(BGl_string2450z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2450za700za7za7c2525za7, "volatile ", 9);
	      DEFINE_STRING(BGl_string2451z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2451za700za7za7c2526za7, "", 0);
	      DEFINE_STRING(BGl_string2452z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2452za700za7za7c2527za7, " = ((", 5);
	      DEFINE_STRING(BGl_string2371z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2371za700za7za7c2528za7, "emit-cop1382", 12);
	      DEFINE_STRING(BGl_string2453z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2453za700za7za7c2529za7, ")BUNSPEC)", 9);
	      DEFINE_STRING(BGl_string2372z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2372za700za7za7c2530za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2454z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2454za700za7za7c2531za7, "if(", 3);
	      DEFINE_STRING(BGl_string2455z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2455za700za7za7c2532za7, " else ", 6);
	      DEFINE_STRING(BGl_string2374z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2374za700za7za7c2533za7, "emit-cop", 8);
	      DEFINE_STRING(BGl_string2456z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2456za700za7za7c2534za7, " = ", 3);
	      DEFINE_STRING(BGl_string2457z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2457za700za7za7c2535za7, "( ", 2);
	      DEFINE_STRING(BGl_string2458z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2458za700za7za7c2536za7, ") ", 2);
	      DEFINE_STRING(BGl_string2459z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2459za700za7za7c2537za7, "regular-grammar", 15);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7b02538za7,
		BGl_z62emitzd2copzb0zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2370z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2cop1382539z00,
		BGl_z62emitzd2cop1382zb0zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2460z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2460za700za7za7c2540za7, "Illegal match", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2373z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22541za7,
		BGl_z62emitzd2copzd2clabel1386z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2461z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2461za700za7za7c2542za7, "$", 1);
	      DEFINE_STRING(BGl_string2462z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2462za700za7za7c2543za7, "BGL_REAL_CNST( ", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2375z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22544za7,
		BGl_z62emitzd2copzd2cgoto1388z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2463z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2463za700za7za7c2545za7, "BGL_TAIL ", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2376z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22546za7,
		BGl_z62emitzd2copzd2cblock1390z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2464z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2464za700za7za7c2547za7, "return ", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2377z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22548za7,
		BGl_z62emitzd2copzd2creturn1392z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2465z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2465za700za7za7c2549za7, "{ ", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2378z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22550za7,
		BGl_z62emitzd2copzd2catom1394z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2466z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2466za700za7za7c2551za7, "goto ", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2379z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22552za7,
		BGl_z62emitzd2copzd2cvoid1396z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2467z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2467za700za7za7c2553za7, "cgen_emit-cop", 13);
	      DEFINE_STRING(BGl_string2468z00zzcgen_emitzd2copzd2,
		BgL_bgl_string2468za700za7za7c2554za7,
		"sreal bigloo light elight foreign bfalse else emit-cop1382 location ", 68);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2380z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22555za7,
		BGl_z62emitzd2copzd2varc1399z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2381z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22556za7,
		BGl_z62emitzd2copzd2cpragma1401z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2382z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22557za7,
		BGl_z62emitzd2copzd2ccast1403z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2383z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22558za7,
		BGl_z62emitzd2copzd2csequence1406z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2384z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22559za7,
		BGl_z62emitzd2copzd2nop1408z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2385z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22560za7,
		BGl_z62emitzd2copzd2stop1412z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2386z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22561za7,
		BGl_z62emitzd2copzd2csetq1414z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2387z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22562za7,
		BGl_z62emitzd2copzd2cif1416z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2388z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22563za7,
		BGl_z62emitzd2copzd2localzd2var1418zb0zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2389z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22564za7,
		BGl_z62emitzd2copzd2bdbzd2block1420zb0zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2390z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22565za7,
		BGl_z62emitzd2copzd2cfuncall1423z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2391z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22566za7,
		BGl_z62emitzd2copzd2capply1426z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2392z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22567za7,
		BGl_z62emitzd2copzd2capp1428z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2393z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22568za7,
		BGl_z62emitzd2copzd2cfail1430z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2394z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22569za7,
		BGl_z62emitzd2copzd2cswitch1432z62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2395z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22570za7,
		BGl_z62emitzd2copzd2cmakezd2box1434zb0zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2396z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22571za7,
		BGl_z62emitzd2copzd2cboxzd2ref1436zb0zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2397z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22572za7,
		BGl_z62emitzd2copzd2cboxzd2setz121438za2zzcgen_emitzd2copzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2398z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22573za7,
		BGl_z62emitzd2copzd2csetzd2exzd2it1440z62zzcgen_emitzd2copzd2, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2399z00zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2copza7d22574za7,
		BGl_z62emitzd2copzd2cjumpzd2exzd2it1442z62zzcgen_emitzd2copzd2, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2currentzd2bdbzd2loczd2envz00zzcgen_emitzd2copzd2,
		BgL_bgl_za762getza7d2current2575z00,
		BGl_z62getzd2currentzd2bdbzd2loczb0zzcgen_emitzd2copzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_resetzd2bdbzd2locz12zd2envzc0zzcgen_emitzd2copzd2,
		BgL_bgl_za762resetza7d2bdbza7d2576za7,
		BGl_z62resetzd2bdbzd2locz12z70zzcgen_emitzd2copzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2bdbzd2loczd2envzd2zzcgen_emitzd2copzd2,
		BgL_bgl_za762emitza7d2bdbza7d22577za7,
		BGl_z62emitzd2bdbzd2locz62zzcgen_emitzd2copzd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcgen_emitzd2copzd2));
		     ADD_ROOT((void *) (&BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2));
		     ADD_ROOT((void *) (&BGl_cfuncallzd2castszd2zzcgen_emitzd2copzd2));
		     ADD_ROOT((void *) (&BGl_za2bfalseza2z00zzcgen_emitzd2copzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzcgen_emitzd2copzd2(long
		BgL_checksumz00_4692, char *BgL_fromz00_4693)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcgen_emitzd2copzd2))
				{
					BGl_requirezd2initializa7ationz75zzcgen_emitzd2copzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcgen_emitzd2copzd2();
					BGl_libraryzd2moduleszd2initz00zzcgen_emitzd2copzd2();
					BGl_cnstzd2initzd2zzcgen_emitzd2copzd2();
					BGl_importedzd2moduleszd2initz00zzcgen_emitzd2copzd2();
					BGl_genericzd2initzd2zzcgen_emitzd2copzd2();
					BGl_methodzd2initzd2zzcgen_emitzd2copzd2();
					return BGl_toplevelzd2initzd2zzcgen_emitzd2copzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__r4_input_6_10_2z00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__rgcz00(0L, "cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cgen_emit-cop");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cgen_emit-cop");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 15 */
			{	/* Cgen/emit_cop.scm 15 */
				obj_t BgL_cportz00_4216;

				{	/* Cgen/emit_cop.scm 15 */
					obj_t BgL_stringz00_4223;

					BgL_stringz00_4223 = BGl_string2468z00zzcgen_emitzd2copzd2;
					{	/* Cgen/emit_cop.scm 15 */
						obj_t BgL_startz00_4224;

						BgL_startz00_4224 = BINT(0L);
						{	/* Cgen/emit_cop.scm 15 */
							obj_t BgL_endz00_4225;

							BgL_endz00_4225 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4223)));
							{	/* Cgen/emit_cop.scm 15 */

								BgL_cportz00_4216 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4223, BgL_startz00_4224, BgL_endz00_4225);
				}}}}
				{
					long BgL_iz00_4217;

					BgL_iz00_4217 = 8L;
				BgL_loopz00_4218:
					if ((BgL_iz00_4217 == -1L))
						{	/* Cgen/emit_cop.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cgen/emit_cop.scm 15 */
							{	/* Cgen/emit_cop.scm 15 */
								obj_t BgL_arg2469z00_4219;

								{	/* Cgen/emit_cop.scm 15 */

									{	/* Cgen/emit_cop.scm 15 */
										obj_t BgL_locationz00_4221;

										BgL_locationz00_4221 = BBOOL(((bool_t) 0));
										{	/* Cgen/emit_cop.scm 15 */

											BgL_arg2469z00_4219 =
												BGl_readz00zz__readerz00(BgL_cportz00_4216,
												BgL_locationz00_4221);
										}
									}
								}
								{	/* Cgen/emit_cop.scm 15 */
									int BgL_tmpz00_4729;

									BgL_tmpz00_4729 = (int) (BgL_iz00_4217);
									CNST_TABLE_SET(BgL_tmpz00_4729, BgL_arg2469z00_4219);
							}}
							{	/* Cgen/emit_cop.scm 15 */
								int BgL_auxz00_4222;

								BgL_auxz00_4222 = (int) ((BgL_iz00_4217 - 1L));
								{
									long BgL_iz00_4734;

									BgL_iz00_4734 = (long) (BgL_auxz00_4222);
									BgL_iz00_4217 = BgL_iz00_4734;
									goto BgL_loopz00_4218;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 15 */
			BGl_cfuncallzd2castszd2zzcgen_emitzd2copzd2 = make_vector(32L, BFALSE);
			BGl_za2bfalseza2z00zzcgen_emitzd2copzd2 = BFALSE;
			return (BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2 = BUNSPEC, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzcgen_emitzd2copzd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1759;

				BgL_headz00_1759 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1760;
					obj_t BgL_tailz00_1761;

					BgL_prevz00_1760 = BgL_headz00_1759;
					BgL_tailz00_1761 = BgL_l1z00_1;
				BgL_loopz00_1762:
					if (PAIRP(BgL_tailz00_1761))
						{
							obj_t BgL_newzd2prevzd2_1764;

							BgL_newzd2prevzd2_1764 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1761), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1760, BgL_newzd2prevzd2_1764);
							{
								obj_t BgL_tailz00_4745;
								obj_t BgL_prevz00_4744;

								BgL_prevz00_4744 = BgL_newzd2prevzd2_1764;
								BgL_tailz00_4745 = CDR(BgL_tailz00_1761);
								BgL_tailz00_1761 = BgL_tailz00_4745;
								BgL_prevz00_1760 = BgL_prevz00_4744;
								goto BgL_loopz00_1762;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1759);
				}
			}
		}

	}



/* reset-bdb-loc! */
	BGL_EXPORTED_DEF obj_t BGl_resetzd2bdbzd2locz12z12zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 751 */
			return (BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2 = BUNSPEC, BUNSPEC);
		}

	}



/* &reset-bdb-loc! */
	obj_t BGl_z62resetzd2bdbzd2locz12z70zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4116)
	{
		{	/* Cgen/emit_cop.scm 751 */
			return BGl_resetzd2bdbzd2locz12z12zzcgen_emitzd2copzd2();
		}

	}



/* get-current-bdb-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2currentzd2bdbzd2loczd2zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 757 */
			return BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2;
		}

	}



/* &get-current-bdb-loc */
	obj_t BGl_z62getzd2currentzd2bdbzd2loczb0zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4117)
	{
		{	/* Cgen/emit_cop.scm 757 */
			return BGl_getzd2currentzd2bdbzd2loczd2zzcgen_emitzd2copzd2();
		}

	}



/* emit-bdb-loc */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(obj_t
		BgL_curzd2loczd2_44)
	{
		{	/* Cgen/emit_cop.scm 768 */
		BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2:
			{	/* Cgen/emit_cop.scm 770 */
				bool_t BgL_test2581z00_4750;

				if (CBOOL(BGl_za2czd2debugzd2lineszd2infoza2zd2zzengine_paramz00))
					{	/* Cgen/emit_cop.scm 770 */
						BgL_test2581z00_4750 =
							CBOOL
							(BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00);
					}
				else
					{	/* Cgen/emit_cop.scm 770 */
						BgL_test2581z00_4750 = ((bool_t) 1);
					}
				if (BgL_test2581z00_4750)
					{	/* Cgen/emit_cop.scm 770 */
						return
							bgl_display_char(((unsigned char) 10),
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* Cgen/emit_cop.scm 772 */
						bool_t BgL_test2583z00_4755;

						if (STRUCTP(BgL_curzd2loczd2_44))
							{	/* Cgen/emit_cop.scm 772 */
								BgL_test2583z00_4755 =
									(STRUCT_KEY(BgL_curzd2loczd2_44) == CNST_TABLE_REF(0));
							}
						else
							{	/* Cgen/emit_cop.scm 772 */
								BgL_test2583z00_4755 = ((bool_t) 0);
							}
						if (BgL_test2583z00_4755)
							{	/* Cgen/emit_cop.scm 785 */
								obj_t BgL_curzd2fnamezd2_1781;
								obj_t BgL_curzd2linezd2_1782;

								BgL_curzd2fnamezd2_1781 =
									STRUCT_REF(BgL_curzd2loczd2_44, (int) (0L));
								BgL_curzd2linezd2_1782 =
									STRUCT_REF(BgL_curzd2loczd2_44, (int) (2L));
								{	/* Cgen/emit_cop.scm 787 */
									bool_t BgL_test2585z00_4765;

									if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
										(BgL_curzd2linezd2_1782))
										{	/* Cgen/emit_cop.scm 787 */
											BgL_test2585z00_4765 = STRINGP(BgL_curzd2fnamezd2_1781);
										}
									else
										{	/* Cgen/emit_cop.scm 787 */
											BgL_test2585z00_4765 = ((bool_t) 0);
										}
									if (BgL_test2585z00_4765)
										{	/* Cgen/emit_cop.scm 788 */
											obj_t BgL_port1381z00_1785;

											BgL_port1381z00_1785 =
												BGl_za2czd2portza2zd2zzbackend_c_emitz00;
											{	/* Cgen/emit_cop.scm 788 */
												obj_t BgL_tmpz00_4769;

												BgL_tmpz00_4769 = ((obj_t) BgL_port1381z00_1785);
												bgl_display_string
													(BGl_string2365z00zzcgen_emitzd2copzd2,
													BgL_tmpz00_4769);
											}
											bgl_display_obj(BgL_curzd2linezd2_1782,
												BgL_port1381z00_1785);
											{	/* Cgen/emit_cop.scm 788 */
												obj_t BgL_tmpz00_4773;

												BgL_tmpz00_4773 = ((obj_t) BgL_port1381z00_1785);
												bgl_display_string
													(BGl_string2366z00zzcgen_emitzd2copzd2,
													BgL_tmpz00_4773);
											}
											bgl_display_obj(BgL_curzd2fnamezd2_1781,
												BgL_port1381z00_1785);
											{	/* Cgen/emit_cop.scm 788 */
												obj_t BgL_tmpz00_4777;

												BgL_tmpz00_4777 = ((obj_t) BgL_port1381z00_1785);
												bgl_display_char(((unsigned char) '"'),
													BgL_tmpz00_4777);
											}
											{	/* Cgen/emit_cop.scm 788 */
												obj_t BgL_tmpz00_4780;

												BgL_tmpz00_4780 = ((obj_t) BgL_port1381z00_1785);
												bgl_display_char(((unsigned char) 10), BgL_tmpz00_4780);
										}}
									else
										{	/* Cgen/emit_cop.scm 787 */
											BFALSE;
										}
								}
								return (BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2 =
									BgL_curzd2loczd2_44, BUNSPEC);
							}
						else
							{	/* Cgen/emit_cop.scm 774 */
								bool_t BgL_test2587z00_4783;

								{	/* Cgen/emit_cop.scm 774 */
									obj_t BgL_oz00_3030;

									BgL_oz00_3030 = BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2;
									if (STRUCTP(BgL_oz00_3030))
										{	/* Cgen/emit_cop.scm 774 */
											BgL_test2587z00_4783 =
												(STRUCT_KEY(BgL_oz00_3030) == CNST_TABLE_REF(0));
										}
									else
										{	/* Cgen/emit_cop.scm 774 */
											BgL_test2587z00_4783 = ((bool_t) 0);
										}
								}
								if (BgL_test2587z00_4783)
									{
										obj_t BgL_curzd2loczd2_4789;

										BgL_curzd2loczd2_4789 =
											BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2;
										BgL_curzd2loczd2_44 = BgL_curzd2loczd2_4789;
										goto BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2;
									}
								else
									{	/* Cgen/emit_cop.scm 776 */
										bool_t BgL_test2589z00_4790;

										{	/* Cgen/emit_cop.scm 776 */
											obj_t BgL_oz00_3035;

											BgL_oz00_3035 =
												BGl_za2modulezd2locationza2zd2zzmodule_modulez00;
											if (STRUCTP(BgL_oz00_3035))
												{	/* Cgen/emit_cop.scm 776 */
													BgL_test2589z00_4790 =
														(STRUCT_KEY(BgL_oz00_3035) == CNST_TABLE_REF(0));
												}
											else
												{	/* Cgen/emit_cop.scm 776 */
													BgL_test2589z00_4790 = ((bool_t) 0);
												}
										}
										if (BgL_test2589z00_4790)
											{
												obj_t BgL_curzd2loczd2_4796;

												BgL_curzd2loczd2_4796 =
													BGl_za2modulezd2locationza2zd2zzmodule_modulez00;
												BgL_curzd2loczd2_44 = BgL_curzd2loczd2_4796;
												goto BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2;
											}
										else
											{	/* Cgen/emit_cop.scm 776 */
												if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
													{	/* Cgen/emit_cop.scm 783 */
														obj_t BgL_port1380z00_1790;

														BgL_port1380z00_1790 =
															BGl_za2czd2portza2zd2zzbackend_c_emitz00;
														{	/* Cgen/emit_cop.scm 783 */
															obj_t BgL_tmpz00_4799;

															BgL_tmpz00_4799 = ((obj_t) BgL_port1380z00_1790);
															bgl_display_string
																(BGl_string2365z00zzcgen_emitzd2copzd2,
																BgL_tmpz00_4799);
														}
														bgl_display_fixnum(BINT(1L),
															((obj_t) BgL_port1380z00_1790));
														{	/* Cgen/emit_cop.scm 783 */
															obj_t BgL_tmpz00_4805;

															BgL_tmpz00_4805 = ((obj_t) BgL_port1380z00_1790);
															bgl_display_string
																(BGl_string2366z00zzcgen_emitzd2copzd2,
																BgL_tmpz00_4805);
														}
														bgl_display_obj(CAR
															(BGl_za2srczd2filesza2zd2zzengine_paramz00),
															BgL_port1380z00_1790);
														{	/* Cgen/emit_cop.scm 783 */
															obj_t BgL_tmpz00_4810;

															BgL_tmpz00_4810 = ((obj_t) BgL_port1380z00_1790);
															bgl_display_char(((unsigned char) '"'),
																BgL_tmpz00_4810);
														}
														{	/* Cgen/emit_cop.scm 783 */
															obj_t BgL_tmpz00_4813;

															BgL_tmpz00_4813 = ((obj_t) BgL_port1380z00_1790);
															return
																bgl_display_char(((unsigned char) 10),
																BgL_tmpz00_4813);
													}}
												else
													{	/* Cgen/emit_cop.scm 780 */
														return BFALSE;
													}
											}
									}
							}
					}
			}
		}

	}



/* &emit-bdb-loc */
	obj_t BGl_z62emitzd2bdbzd2locz62zzcgen_emitzd2copzd2(obj_t BgL_envz00_4118,
		obj_t BgL_curzd2loczd2_4119)
	{
		{	/* Cgen/emit_cop.scm 768 */
			return BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_curzd2loczd2_4119);
		}

	}



/* emit-bdb-loc-comment */
	obj_t BGl_emitzd2bdbzd2loczd2commentzd2zzcgen_emitzd2copzd2(obj_t
		BgL_curzd2loczd2_45)
	{
		{	/* Cgen/emit_cop.scm 797 */
			{	/* Cgen/emit_cop.scm 798 */
				bool_t BgL_test2592z00_4817;

				if (STRUCTP(BgL_curzd2loczd2_45))
					{	/* Cgen/emit_cop.scm 798 */
						BgL_test2592z00_4817 =
							(STRUCT_KEY(BgL_curzd2loczd2_45) == CNST_TABLE_REF(0));
					}
				else
					{	/* Cgen/emit_cop.scm 798 */
						BgL_test2592z00_4817 = ((bool_t) 0);
					}
				if (BgL_test2592z00_4817)
					{	/* Cgen/emit_cop.scm 798 */
						bgl_display_string(BGl_string2367z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						{	/* Cgen/emit_cop.scm 801 */
							obj_t BgL_arg1540z00_1793;

							BgL_arg1540z00_1793 = STRUCT_REF(BgL_curzd2loczd2_45, (int) (0L));
							bgl_display_obj(BgL_arg1540z00_1793,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
						bgl_display_string(BGl_string2368z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						{	/* Cgen/emit_cop.scm 803 */
							obj_t BgL_arg1544z00_1794;

							BgL_arg1544z00_1794 = STRUCT_REF(BgL_curzd2loczd2_45, (int) (2L));
							bgl_display_obj(BgL_arg1544z00_1794,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
						return
							bgl_display_string(BGl_string2369z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* Cgen/emit_cop.scm 798 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_proc2370z00zzcgen_emitzd2copzd2, BGl_copz00zzcgen_copz00,
				BGl_string2371z00zzcgen_emitzd2copzd2);
		}

	}



/* &emit-cop1382 */
	obj_t BGl_z62emitzd2cop1382zb0zzcgen_emitzd2copzd2(obj_t BgL_envz00_4121,
		obj_t BgL_copz00_4122)
	{
		{	/* Cgen/emit_cop.scm 45 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string2372z00zzcgen_emitzd2copzd2,
				((obj_t) ((BgL_copz00_bglt) BgL_copz00_4122)));
		}

	}



/* emit-cop */
	BGL_EXPORTED_DEF bool_t BGl_emitzd2copzd2zzcgen_emitzd2copzd2(BgL_copz00_bglt
		BgL_copz00_17)
	{
		{	/* Cgen/emit_cop.scm 45 */
			{	/* Cgen/emit_cop.scm 45 */
				obj_t BgL_method1383z00_1799;

				{	/* Cgen/emit_cop.scm 45 */
					obj_t BgL_res2347z00_3093;

					{	/* Cgen/emit_cop.scm 45 */
						long BgL_objzd2classzd2numz00_3064;

						BgL_objzd2classzd2numz00_3064 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_copz00_17));
						{	/* Cgen/emit_cop.scm 45 */
							obj_t BgL_arg1811z00_3065;

							BgL_arg1811z00_3065 =
								PROCEDURE_REF(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
								(int) (1L));
							{	/* Cgen/emit_cop.scm 45 */
								int BgL_offsetz00_3068;

								BgL_offsetz00_3068 = (int) (BgL_objzd2classzd2numz00_3064);
								{	/* Cgen/emit_cop.scm 45 */
									long BgL_offsetz00_3069;

									BgL_offsetz00_3069 =
										((long) (BgL_offsetz00_3068) - OBJECT_TYPE);
									{	/* Cgen/emit_cop.scm 45 */
										long BgL_modz00_3070;

										BgL_modz00_3070 =
											(BgL_offsetz00_3069 >> (int) ((long) ((int) (4L))));
										{	/* Cgen/emit_cop.scm 45 */
											long BgL_restz00_3072;

											BgL_restz00_3072 =
												(BgL_offsetz00_3069 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cgen/emit_cop.scm 45 */

												{	/* Cgen/emit_cop.scm 45 */
													obj_t BgL_bucketz00_3074;

													BgL_bucketz00_3074 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3065), BgL_modz00_3070);
													BgL_res2347z00_3093 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3074), BgL_restz00_3072);
					}}}}}}}}
					BgL_method1383z00_1799 = BgL_res2347z00_3093;
				}
				return
					CBOOL(BGL_PROCEDURE_CALL1(BgL_method1383z00_1799,
						((obj_t) BgL_copz00_17)));
			}
		}

	}



/* &emit-cop */
	obj_t BGl_z62emitzd2copzb0zzcgen_emitzd2copzd2(obj_t BgL_envz00_4123,
		obj_t BgL_copz00_4124)
	{
		{	/* Cgen/emit_cop.scm 45 */
			return
				BBOOL(BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
					((BgL_copz00_bglt) BgL_copz00_4124)));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_clabelz00zzcgen_copz00, BGl_proc2373z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_cgotoz00zzcgen_copz00,
				BGl_proc2375z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_cblockz00zzcgen_copz00, BGl_proc2376z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_creturnz00zzcgen_copz00, BGl_proc2377z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_catomz00zzcgen_copz00,
				BGl_proc2378z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_cvoidz00zzcgen_copz00,
				BGl_proc2379z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_varcz00zzcgen_copz00,
				BGl_proc2380z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_cpragmaz00zzcgen_copz00, BGl_proc2381z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_ccastz00zzcgen_copz00,
				BGl_proc2382z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_csequencez00zzcgen_copz00, BGl_proc2383z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_nopz00zzcgen_copz00,
				BGl_proc2384z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_stopz00zzcgen_copz00,
				BGl_proc2385z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_csetqz00zzcgen_copz00,
				BGl_proc2386z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_cifz00zzcgen_copz00,
				BGl_proc2387z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_localzd2varzd2zzcgen_copz00, BGl_proc2388z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_bdbzd2blockzd2zzcgen_copz00, BGl_proc2389z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_cfuncallz00zzcgen_copz00, BGl_proc2390z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_capplyz00zzcgen_copz00, BGl_proc2391z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_cappz00zzcgen_copz00,
				BGl_proc2392z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2, BGl_cfailz00zzcgen_copz00,
				BGl_proc2393z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_cswitchz00zzcgen_copz00, BGl_proc2394z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_cmakezd2boxzd2zzcgen_copz00, BGl_proc2395z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_cboxzd2refzd2zzcgen_copz00, BGl_proc2396z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_cboxzd2setz12zc0zzcgen_copz00, BGl_proc2397z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_csetzd2exzd2itz00zzcgen_copz00, BGl_proc2398z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_emitzd2copzd2envz00zzcgen_emitzd2copzd2,
				BGl_cjumpzd2exzd2itz00zzcgen_copz00,
				BGl_proc2399z00zzcgen_emitzd2copzd2,
				BGl_string2374z00zzcgen_emitzd2copzd2);
		}

	}



/* &emit-cop-cjump-ex-it1442 */
	obj_t BGl_z62emitzd2copzd2cjumpzd2exzd2it1442z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4154, obj_t BgL_copz00_4155)
	{
		{	/* Cgen/emit_cop.scm 731 */
			{	/* Cgen/emit_cop.scm 732 */
				bool_t BgL_tmpz00_4897;

				{	/* Cgen/emit_cop.scm 733 */
					obj_t BgL_arg2244z00_4229;

					BgL_arg2244z00_4229 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cjumpzd2exzd2itz00_bglt) BgL_copz00_4155))))->
						BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2244z00_4229);
				}
				bgl_display_string(BGl_string2400z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cjumpzd2exzd2itz00_bglt)
							COBJECT(((BgL_cjumpzd2exzd2itz00_bglt) BgL_copz00_4155)))->
						BgL_exitz00));
				bgl_display_char(((unsigned char) ','),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cjumpzd2exzd2itz00_bglt)
							COBJECT(((BgL_cjumpzd2exzd2itz00_bglt) BgL_copz00_4155)))->
						BgL_valuez00));
				bgl_display_char(((unsigned char) ')'),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BgL_tmpz00_4897 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_4897);
			}
		}

	}



/* &emit-cop-cset-ex-it1440 */
	obj_t BGl_z62emitzd2copzd2csetzd2exzd2it1440z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4156, obj_t BgL_copz00_4157)
	{
		{	/* Cgen/emit_cop.scm 708 */
			{	/* Cgen/emit_cop.scm 709 */
				bool_t BgL_tmpz00_4912;

				{	/* Cgen/emit_cop.scm 710 */
					obj_t BgL_arg2236z00_4231;

					BgL_arg2236z00_4231 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_csetzd2exzd2itz00_bglt) BgL_copz00_4157))))->
						BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2236z00_4231);
				}
				bgl_display_string(BGl_string2401z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_csetzd2exzd2itz00_bglt)
							COBJECT(((BgL_csetzd2exzd2itz00_bglt) BgL_copz00_4157)))->
						BgL_exitz00));
				bgl_display_string(BGl_string2402z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				if (BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_csetzd2exzd2itz00_bglt)
								COBJECT(((BgL_csetzd2exzd2itz00_bglt) BgL_copz00_4157)))->
							BgL_jumpzd2valuezd2)))
					{	/* Cgen/emit_cop.scm 715 */
						bgl_display_string(BGl_string2403z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* Cgen/emit_cop.scm 715 */
						BFALSE;
					}
				{	/* Cgen/emit_cop.scm 716 */
					obj_t BgL_arg2241z00_4232;

					BgL_arg2241z00_4232 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_csetzd2exzd2itz00_bglt) BgL_copz00_4157))))->
						BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2241z00_4232);
				}
				bgl_display_string(BGl_string2404z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				bgl_display_string(BGl_string2405z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				bgl_display_string(BGl_string2406z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				bgl_display_string(BGl_string2407z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				bgl_display_string(BGl_string2408z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_csetzd2exzd2itz00_bglt)
							COBJECT(((BgL_csetzd2exzd2itz00_bglt) BgL_copz00_4157)))->
						BgL_bodyz00));
				{	/* Cgen/emit_cop.scm 723 */
					obj_t BgL_arg2243z00_4233;

					BgL_arg2243z00_4233 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_csetzd2exzd2itz00_bglt) BgL_copz00_4157))))->
						BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2243z00_4233);
				}
				bgl_display_string(BGl_string2409z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BgL_tmpz00_4912 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_4912);
			}
		}

	}



/* &emit-cop-cbox-set!1438 */
	obj_t BGl_z62emitzd2copzd2cboxzd2setz121438za2zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4158, obj_t BgL_copz00_4159)
	{
		{	/* Cgen/emit_cop.scm 695 */
			{	/* Cgen/emit_cop.scm 696 */
				bool_t BgL_tmpz00_4945;

				{	/* Cgen/emit_cop.scm 697 */
					obj_t BgL_arg2233z00_4235;

					BgL_arg2233z00_4235 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cboxzd2setz12zc0_bglt) BgL_copz00_4159))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2233z00_4235);
				}
				bgl_display_string(BGl_string2410z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cboxzd2setz12zc0_bglt)
							COBJECT(((BgL_cboxzd2setz12zc0_bglt) BgL_copz00_4159)))->
						BgL_varz00));
				bgl_display_string(BGl_string2411z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cboxzd2setz12zc0_bglt)
							COBJECT(((BgL_cboxzd2setz12zc0_bglt) BgL_copz00_4159)))->
						BgL_valuez00));
				bgl_display_char(((unsigned char) ')'),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BgL_tmpz00_4945 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_4945);
			}
		}

	}



/* &emit-cop-cbox-ref1436 */
	obj_t BGl_z62emitzd2copzd2cboxzd2ref1436zb0zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4160, obj_t BgL_copz00_4161)
	{
		{	/* Cgen/emit_cop.scm 678 */
			{	/* Cgen/emit_cop.scm 679 */
				bool_t BgL_tmpz00_4960;

				{	/* Cgen/emit_cop.scm 680 */
					obj_t BgL_arg2223z00_4237;

					BgL_arg2223z00_4237 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cboxzd2refzd2_bglt) BgL_copz00_4161))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2223z00_4237);
				}
				{	/* Cgen/emit_cop.scm 681 */
					bool_t BgL_test2595z00_4965;

					{	/* Cgen/emit_cop.scm 681 */
						BgL_typez00_bglt BgL_arg2228z00_4238;

						BgL_arg2228z00_4238 =
							(((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cboxzd2refzd2_bglt) BgL_copz00_4161))))->BgL_typez00);
						BgL_test2595z00_4965 =
							(
							((obj_t) BgL_arg2228z00_4238) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2595z00_4965)
						{	/* Cgen/emit_cop.scm 681 */
							BFALSE;
						}
					else
						{	/* Cgen/emit_cop.scm 681 */
							bgl_display_string(BGl_string2412z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							{	/* Cgen/emit_cop.scm 683 */
								obj_t BgL_arg2226z00_4239;

								BgL_arg2226z00_4239 =
									(((BgL_typez00_bglt) COBJECT(
											(((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt)
															((BgL_cboxzd2refzd2_bglt) BgL_copz00_4161))))->
												BgL_typez00)))->BgL_namez00);
								bgl_display_obj(BgL_arg2226z00_4239,
									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							}
							bgl_display_string(BGl_string2413z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
				}
				bgl_display_string(BGl_string2414z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cboxzd2refzd2_bglt)
							COBJECT(((BgL_cboxzd2refzd2_bglt) BgL_copz00_4161)))->
						BgL_varz00));
				{	/* Cgen/emit_cop.scm 687 */
					bool_t BgL_test2596z00_4982;

					{	/* Cgen/emit_cop.scm 687 */
						BgL_typez00_bglt BgL_arg2232z00_4240;

						BgL_arg2232z00_4240 =
							(((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt)
										((BgL_cboxzd2refzd2_bglt) BgL_copz00_4161))))->BgL_typez00);
						BgL_test2596z00_4982 =
							(
							((obj_t) BgL_arg2232z00_4240) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2596z00_4982)
						{	/* Cgen/emit_cop.scm 687 */
							bgl_display_char(((unsigned char) ')'),
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
					else
						{	/* Cgen/emit_cop.scm 687 */
							bgl_display_string(BGl_string2415z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
				}
				BgL_tmpz00_4960 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_4960);
			}
		}

	}



/* &emit-cop-cmake-box1434 */
	obj_t BGl_z62emitzd2copzd2cmakezd2box1434zb0zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4162, obj_t BgL_copz00_4163)
	{
		{	/* Cgen/emit_cop.scm 659 */
			{	/* Cgen/emit_cop.scm 660 */
				bool_t BgL_tmpz00_4991;

				{	/* Cgen/emit_cop.scm 661 */
					obj_t BgL_arg2215z00_4242;

					BgL_arg2215z00_4242 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cmakezd2boxzd2_bglt) BgL_copz00_4163))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2215z00_4242);
				}
				{	/* Cgen/emit_cop.scm 662 */
					bool_t BgL_test2597z00_4996;

					{	/* Cgen/emit_cop.scm 662 */
						obj_t BgL_arg2222z00_4243;

						BgL_arg2222z00_4243 =
							(((BgL_cmakezd2boxzd2_bglt) COBJECT(
									((BgL_cmakezd2boxzd2_bglt) BgL_copz00_4163)))->
							BgL_stackablez00);
						{	/* Cgen/emit_cop.scm 662 */
							obj_t BgL_classz00_4244;

							BgL_classz00_4244 = BGl_localz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg2222z00_4243))
								{	/* Cgen/emit_cop.scm 662 */
									BgL_objectz00_bglt BgL_arg1807z00_4245;

									BgL_arg1807z00_4245 =
										(BgL_objectz00_bglt) (BgL_arg2222z00_4243);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cgen/emit_cop.scm 662 */
											long BgL_idxz00_4246;

											BgL_idxz00_4246 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4245);
											BgL_test2597z00_4996 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4246 + 2L)) == BgL_classz00_4244);
										}
									else
										{	/* Cgen/emit_cop.scm 662 */
											bool_t BgL_res2360z00_4249;

											{	/* Cgen/emit_cop.scm 662 */
												obj_t BgL_oclassz00_4250;

												{	/* Cgen/emit_cop.scm 662 */
													obj_t BgL_arg1815z00_4251;
													long BgL_arg1816z00_4252;

													BgL_arg1815z00_4251 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cgen/emit_cop.scm 662 */
														long BgL_arg1817z00_4253;

														BgL_arg1817z00_4253 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4245);
														BgL_arg1816z00_4252 =
															(BgL_arg1817z00_4253 - OBJECT_TYPE);
													}
													BgL_oclassz00_4250 =
														VECTOR_REF(BgL_arg1815z00_4251,
														BgL_arg1816z00_4252);
												}
												{	/* Cgen/emit_cop.scm 662 */
													bool_t BgL__ortest_1115z00_4254;

													BgL__ortest_1115z00_4254 =
														(BgL_classz00_4244 == BgL_oclassz00_4250);
													if (BgL__ortest_1115z00_4254)
														{	/* Cgen/emit_cop.scm 662 */
															BgL_res2360z00_4249 = BgL__ortest_1115z00_4254;
														}
													else
														{	/* Cgen/emit_cop.scm 662 */
															long BgL_odepthz00_4255;

															{	/* Cgen/emit_cop.scm 662 */
																obj_t BgL_arg1804z00_4256;

																BgL_arg1804z00_4256 = (BgL_oclassz00_4250);
																BgL_odepthz00_4255 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4256);
															}
															if ((2L < BgL_odepthz00_4255))
																{	/* Cgen/emit_cop.scm 662 */
																	obj_t BgL_arg1802z00_4257;

																	{	/* Cgen/emit_cop.scm 662 */
																		obj_t BgL_arg1803z00_4258;

																		BgL_arg1803z00_4258 = (BgL_oclassz00_4250);
																		BgL_arg1802z00_4257 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4258, 2L);
																	}
																	BgL_res2360z00_4249 =
																		(BgL_arg1802z00_4257 == BgL_classz00_4244);
																}
															else
																{	/* Cgen/emit_cop.scm 662 */
																	BgL_res2360z00_4249 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2597z00_4996 = BgL_res2360z00_4249;
										}
								}
							else
								{	/* Cgen/emit_cop.scm 662 */
									BgL_test2597z00_4996 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2597z00_4996)
						{	/* Cgen/emit_cop.scm 662 */
							bgl_display_string(BGl_string2416z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cmakezd2boxzd2_bglt)
										COBJECT(((BgL_cmakezd2boxzd2_bglt) BgL_copz00_4163)))->
									BgL_valuez00));
							bgl_display_char(((unsigned char) ','),
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							{	/* Cgen/emit_cop.scm 667 */
								obj_t BgL_arg2219z00_4259;

								BgL_arg2219z00_4259 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												(((BgL_cmakezd2boxzd2_bglt) COBJECT(
															((BgL_cmakezd2boxzd2_bglt) BgL_copz00_4163)))->
													BgL_stackablez00))))->BgL_namez00);
								bgl_display_obj(BgL_arg2219z00_4259,
									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							}
							bgl_display_char(((unsigned char) ')'),
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
					else
						{	/* Cgen/emit_cop.scm 662 */
							bgl_display_string(BGl_string2417z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cmakezd2boxzd2_bglt)
										COBJECT(((BgL_cmakezd2boxzd2_bglt) BgL_copz00_4163)))->
									BgL_valuez00));
							bgl_display_char(((unsigned char) ')'),
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}}
				BgL_tmpz00_4991 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_4991);
			}
		}

	}



/* &emit-cop-cswitch1432 */
	obj_t BGl_z62emitzd2copzd2cswitch1432z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4164, obj_t BgL_copz00_4165)
	{
		{	/* Cgen/emit_cop.scm 615 */
			{	/* Cgen/emit_cop.scm 616 */
				bool_t BgL_tmpz00_5038;

				{	/* Cgen/emit_cop.scm 617 */
					obj_t BgL_arg2185z00_4261;

					BgL_arg2185z00_4261 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cswitchz00_bglt) BgL_copz00_4165))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2185z00_4261);
				}
				bgl_display_string(BGl_string2418z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cswitchz00_bglt)
							COBJECT(((BgL_cswitchz00_bglt) BgL_copz00_4165)))->BgL_testz00));
				bgl_display_string(BGl_string2419z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				{
					obj_t BgL_clausesz00_4263;
					obj_t BgL_seenz00_4264;

					BgL_clausesz00_4263 =
						(((BgL_cswitchz00_bglt) COBJECT(
								((BgL_cswitchz00_bglt) BgL_copz00_4165)))->BgL_clausesz00);
					BgL_seenz00_4264 = BNIL;
				BgL_loopz00_4262:
					{	/* Cgen/emit_cop.scm 624 */
						obj_t BgL_clausez00_4265;

						BgL_clausez00_4265 = CAR(((obj_t) BgL_clausesz00_4263));
						if ((CAR(((obj_t) BgL_clausez00_4265)) == CNST_TABLE_REF(2)))
							{	/* Cgen/emit_cop.scm 627 */
								obj_t BgL_locz00_4266;

								BgL_locz00_4266 =
									(((BgL_copz00_bglt) COBJECT(
											((BgL_copz00_bglt)
												CDR(((obj_t) BgL_clausez00_4265)))))->BgL_locz00);
								BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_locz00_4266);
								bgl_display_string(BGl_string2420z00zzcgen_emitzd2copzd2,
									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
								{	/* Cgen/emit_cop.scm 630 */
									bool_t BgL_test2603z00_5061;

									{	/* Cgen/emit_cop.scm 630 */
										obj_t BgL_arg2193z00_4267;

										BgL_arg2193z00_4267 = CDR(((obj_t) BgL_clausez00_4265));
										BgL_test2603z00_5061 =
											BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
											((BgL_copz00_bglt) BgL_arg2193z00_4267));
									}
									if (BgL_test2603z00_5061)
										{	/* Cgen/emit_cop.scm 630 */
											bgl_display_string(BGl_string2421z00zzcgen_emitzd2copzd2,
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
											BNIL;
										}
									else
										{	/* Cgen/emit_cop.scm 630 */
											BFALSE;
										}
								}
								bgl_display_string(BGl_string2422z00zzcgen_emitzd2copzd2,
									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
								BgL_tmpz00_5038 = ((bool_t) 0);
							}
						else
							{	/* Cgen/emit_cop.scm 638 */
								bool_t BgL_test2604z00_5068;

								{
									obj_t BgL_l1373z00_4269;

									BgL_l1373z00_4269 = CAR(((obj_t) BgL_clausez00_4265));
								BgL_zc3z04anonymousza32212ze3z87_4268:
									if (NULLP(BgL_l1373z00_4269))
										{	/* Cgen/emit_cop.scm 638 */
											BgL_test2604z00_5068 = ((bool_t) 1);
										}
									else
										{	/* Cgen/emit_cop.scm 638 */
											if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
															((obj_t) BgL_l1373z00_4269)), BgL_seenz00_4264)))
												{
													obj_t BgL_l1373z00_5076;

													BgL_l1373z00_5076 = CDR(((obj_t) BgL_l1373z00_4269));
													BgL_l1373z00_4269 = BgL_l1373z00_5076;
													goto BgL_zc3z04anonymousza32212ze3z87_4268;
												}
											else
												{	/* Cgen/emit_cop.scm 638 */
													BgL_test2604z00_5068 = ((bool_t) 0);
												}
										}
								}
								if (BgL_test2604z00_5068)
									{	/* Cgen/emit_cop.scm 639 */
										obj_t BgL_arg2199z00_4270;

										BgL_arg2199z00_4270 = CDR(((obj_t) BgL_clausesz00_4263));
										{
											obj_t BgL_clausesz00_5083;

											BgL_clausesz00_5083 = BgL_arg2199z00_4270;
											BgL_clausesz00_4263 = BgL_clausesz00_5083;
											goto BgL_loopz00_4262;
										}
									}
								else
									{	/* Cgen/emit_cop.scm 638 */
										{	/* Cgen/emit_cop.scm 641 */
											obj_t BgL_g1379z00_4271;

											BgL_g1379z00_4271 = CAR(((obj_t) BgL_clausez00_4265));
											{
												obj_t BgL_l1377z00_4273;

												BgL_l1377z00_4273 = BgL_g1379z00_4271;
											BgL_zc3z04anonymousza32200ze3z87_4272:
												if (PAIRP(BgL_l1377z00_4273))
													{	/* Cgen/emit_cop.scm 647 */
														{	/* Cgen/emit_cop.scm 642 */
															obj_t BgL_tz00_4274;

															BgL_tz00_4274 = CAR(BgL_l1377z00_4273);
															if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																	(BgL_tz00_4274, BgL_seenz00_4264)))
																{	/* Cgen/emit_cop.scm 642 */
																	BFALSE;
																}
															else
																{	/* Cgen/emit_cop.scm 642 */
																	bgl_display_string
																		(BGl_string2423z00zzcgen_emitzd2copzd2,
																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																	BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00
																		(BgL_tz00_4274,
																		(((BgL_copz00_bglt)
																				COBJECT((((BgL_cswitchz00_bglt)
																							COBJECT(((BgL_cswitchz00_bglt)
																									BgL_copz00_4165)))->
																						BgL_testz00)))->BgL_typez00));
																	bgl_display_string
																		(BGl_string2424z00zzcgen_emitzd2copzd2,
																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																	bgl_display_char(((unsigned char) 10),
																		BGl_za2czd2portza2zd2zzbackend_c_emitz00);
														}}
														{
															obj_t BgL_l1377z00_5099;

															BgL_l1377z00_5099 = CDR(BgL_l1377z00_4273);
															BgL_l1377z00_4273 = BgL_l1377z00_5099;
															goto BgL_zc3z04anonymousza32200ze3z87_4272;
														}
													}
												else
													{	/* Cgen/emit_cop.scm 647 */
														((bool_t) 1);
													}
											}
										}
										{	/* Cgen/emit_cop.scm 648 */
											bool_t BgL_test2609z00_5101;

											{	/* Cgen/emit_cop.scm 648 */
												obj_t BgL_arg2208z00_4275;

												BgL_arg2208z00_4275 = CDR(((obj_t) BgL_clausez00_4265));
												BgL_test2609z00_5101 =
													BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
													((BgL_copz00_bglt) BgL_arg2208z00_4275));
											}
											if (BgL_test2609z00_5101)
												{	/* Cgen/emit_cop.scm 648 */
													bgl_display_string
														(BGl_string2421z00zzcgen_emitzd2copzd2,
														BGl_za2czd2portza2zd2zzbackend_c_emitz00);
													BNIL;
												}
											else
												{	/* Cgen/emit_cop.scm 648 */
													BFALSE;
												}
										}
										bgl_display_string(BGl_string2425z00zzcgen_emitzd2copzd2,
											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										{	/* Cgen/emit_cop.scm 654 */
											obj_t BgL_arg2209z00_4276;
											obj_t BgL_arg2210z00_4277;

											BgL_arg2209z00_4276 = CDR(((obj_t) BgL_clausesz00_4263));
											{	/* Cgen/emit_cop.scm 654 */
												obj_t BgL_arg2211z00_4278;

												BgL_arg2211z00_4278 = CAR(((obj_t) BgL_clausez00_4265));
												BgL_arg2210z00_4277 =
													BGl_appendzd221011zd2zzcgen_emitzd2copzd2
													(BgL_arg2211z00_4278, BgL_seenz00_4264);
											}
											{
												obj_t BgL_seenz00_5114;
												obj_t BgL_clausesz00_5113;

												BgL_clausesz00_5113 = BgL_arg2209z00_4276;
												BgL_seenz00_5114 = BgL_arg2210z00_4277;
												BgL_seenz00_4264 = BgL_seenz00_5114;
												BgL_clausesz00_4263 = BgL_clausesz00_5113;
												goto BgL_loopz00_4262;
											}
										}
									}
							}
					}
				}
				return BBOOL(BgL_tmpz00_5038);
			}
		}

	}



/* &emit-cop-cfail1430 */
	obj_t BGl_z62emitzd2copzd2cfail1430z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4166, obj_t BgL_copz00_4167)
	{
		{	/* Cgen/emit_cop.scm 584 */
			{	/* Cgen/emit_cop.scm 585 */
				bool_t BgL_tmpz00_5118;

				{	/* Cgen/emit_cop.scm 586 */
					obj_t BgL_arg2146z00_4280;

					BgL_arg2146z00_4280 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cfailz00_bglt) BgL_copz00_4167))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2146z00_4280);
				}
				if (CBOOL(BGl_za2bfalseza2z00zzcgen_emitzd2copzd2))
					{	/* Cgen/emit_cop.scm 587 */
						BFALSE;
					}
				else
					{	/* Cgen/emit_cop.scm 587 */
						BGl_za2bfalseza2z00zzcgen_emitzd2copzd2 =
							BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(3),
							CNST_TABLE_REF(4));
					}
				{	/* Cgen/emit_cop.scm 590 */
					bool_t BgL_test2611z00_5128;

					{	/* Cgen/emit_cop.scm 590 */
						bool_t BgL_test2612z00_5129;

						{	/* Cgen/emit_cop.scm 590 */
							BgL_copz00_bglt BgL_arg2184z00_4281;

							BgL_arg2184z00_4281 =
								(((BgL_cfailz00_bglt) COBJECT(
										((BgL_cfailz00_bglt) BgL_copz00_4167)))->BgL_procz00);
							{	/* Cgen/emit_cop.scm 590 */
								obj_t BgL_classz00_4282;

								BgL_classz00_4282 = BGl_varcz00zzcgen_copz00;
								{	/* Cgen/emit_cop.scm 590 */
									BgL_objectz00_bglt BgL_arg1807z00_4283;

									{	/* Cgen/emit_cop.scm 590 */
										obj_t BgL_tmpz00_5132;

										BgL_tmpz00_5132 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2184z00_4281));
										BgL_arg1807z00_4283 =
											(BgL_objectz00_bglt) (BgL_tmpz00_5132);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cgen/emit_cop.scm 590 */
											long BgL_idxz00_4284;

											BgL_idxz00_4284 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4283);
											BgL_test2612z00_5129 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4284 + 2L)) == BgL_classz00_4282);
										}
									else
										{	/* Cgen/emit_cop.scm 590 */
											bool_t BgL_res2357z00_4287;

											{	/* Cgen/emit_cop.scm 590 */
												obj_t BgL_oclassz00_4288;

												{	/* Cgen/emit_cop.scm 590 */
													obj_t BgL_arg1815z00_4289;
													long BgL_arg1816z00_4290;

													BgL_arg1815z00_4289 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cgen/emit_cop.scm 590 */
														long BgL_arg1817z00_4291;

														BgL_arg1817z00_4291 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4283);
														BgL_arg1816z00_4290 =
															(BgL_arg1817z00_4291 - OBJECT_TYPE);
													}
													BgL_oclassz00_4288 =
														VECTOR_REF(BgL_arg1815z00_4289,
														BgL_arg1816z00_4290);
												}
												{	/* Cgen/emit_cop.scm 590 */
													bool_t BgL__ortest_1115z00_4292;

													BgL__ortest_1115z00_4292 =
														(BgL_classz00_4282 == BgL_oclassz00_4288);
													if (BgL__ortest_1115z00_4292)
														{	/* Cgen/emit_cop.scm 590 */
															BgL_res2357z00_4287 = BgL__ortest_1115z00_4292;
														}
													else
														{	/* Cgen/emit_cop.scm 590 */
															long BgL_odepthz00_4293;

															{	/* Cgen/emit_cop.scm 590 */
																obj_t BgL_arg1804z00_4294;

																BgL_arg1804z00_4294 = (BgL_oclassz00_4288);
																BgL_odepthz00_4293 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4294);
															}
															if ((2L < BgL_odepthz00_4293))
																{	/* Cgen/emit_cop.scm 590 */
																	obj_t BgL_arg1802z00_4295;

																	{	/* Cgen/emit_cop.scm 590 */
																		obj_t BgL_arg1803z00_4296;

																		BgL_arg1803z00_4296 = (BgL_oclassz00_4288);
																		BgL_arg1802z00_4295 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4296, 2L);
																	}
																	BgL_res2357z00_4287 =
																		(BgL_arg1802z00_4295 == BgL_classz00_4282);
																}
															else
																{	/* Cgen/emit_cop.scm 590 */
																	BgL_res2357z00_4287 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2612z00_5129 = BgL_res2357z00_4287;
										}
								}
							}
						}
						if (BgL_test2612z00_5129)
							{	/* Cgen/emit_cop.scm 590 */
								bool_t BgL_test2616z00_5155;

								{	/* Cgen/emit_cop.scm 590 */
									BgL_variablez00_bglt BgL_arg2182z00_4297;

									BgL_arg2182z00_4297 =
										(((BgL_varcz00_bglt) COBJECT(
												((BgL_varcz00_bglt)
													(((BgL_cfailz00_bglt) COBJECT(
																((BgL_cfailz00_bglt) BgL_copz00_4167)))->
														BgL_procz00))))->BgL_variablez00);
									BgL_test2616z00_5155 =
										(((obj_t) BgL_arg2182z00_4297) ==
										BGl_za2bfalseza2z00zzcgen_emitzd2copzd2);
								}
								if (BgL_test2616z00_5155)
									{	/* Cgen/emit_cop.scm 591 */
										bool_t BgL_test2617z00_5162;

										{	/* Cgen/emit_cop.scm 591 */
											BgL_copz00_bglt BgL_arg2181z00_4298;

											BgL_arg2181z00_4298 =
												(((BgL_cfailz00_bglt) COBJECT(
														((BgL_cfailz00_bglt) BgL_copz00_4167)))->
												BgL_msgz00);
											{	/* Cgen/emit_cop.scm 591 */
												obj_t BgL_classz00_4299;

												BgL_classz00_4299 = BGl_varcz00zzcgen_copz00;
												{	/* Cgen/emit_cop.scm 591 */
													BgL_objectz00_bglt BgL_arg1807z00_4300;

													{	/* Cgen/emit_cop.scm 591 */
														obj_t BgL_tmpz00_5165;

														BgL_tmpz00_5165 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg2181z00_4298));
														BgL_arg1807z00_4300 =
															(BgL_objectz00_bglt) (BgL_tmpz00_5165);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cgen/emit_cop.scm 591 */
															long BgL_idxz00_4301;

															BgL_idxz00_4301 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4300);
															BgL_test2617z00_5162 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4301 + 2L)) == BgL_classz00_4299);
														}
													else
														{	/* Cgen/emit_cop.scm 591 */
															bool_t BgL_res2358z00_4304;

															{	/* Cgen/emit_cop.scm 591 */
																obj_t BgL_oclassz00_4305;

																{	/* Cgen/emit_cop.scm 591 */
																	obj_t BgL_arg1815z00_4306;
																	long BgL_arg1816z00_4307;

																	BgL_arg1815z00_4306 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cgen/emit_cop.scm 591 */
																		long BgL_arg1817z00_4308;

																		BgL_arg1817z00_4308 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4300);
																		BgL_arg1816z00_4307 =
																			(BgL_arg1817z00_4308 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4305 =
																		VECTOR_REF(BgL_arg1815z00_4306,
																		BgL_arg1816z00_4307);
																}
																{	/* Cgen/emit_cop.scm 591 */
																	bool_t BgL__ortest_1115z00_4309;

																	BgL__ortest_1115z00_4309 =
																		(BgL_classz00_4299 == BgL_oclassz00_4305);
																	if (BgL__ortest_1115z00_4309)
																		{	/* Cgen/emit_cop.scm 591 */
																			BgL_res2358z00_4304 =
																				BgL__ortest_1115z00_4309;
																		}
																	else
																		{	/* Cgen/emit_cop.scm 591 */
																			long BgL_odepthz00_4310;

																			{	/* Cgen/emit_cop.scm 591 */
																				obj_t BgL_arg1804z00_4311;

																				BgL_arg1804z00_4311 =
																					(BgL_oclassz00_4305);
																				BgL_odepthz00_4310 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4311);
																			}
																			if ((2L < BgL_odepthz00_4310))
																				{	/* Cgen/emit_cop.scm 591 */
																					obj_t BgL_arg1802z00_4312;

																					{	/* Cgen/emit_cop.scm 591 */
																						obj_t BgL_arg1803z00_4313;

																						BgL_arg1803z00_4313 =
																							(BgL_oclassz00_4305);
																						BgL_arg1802z00_4312 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4313, 2L);
																					}
																					BgL_res2358z00_4304 =
																						(BgL_arg1802z00_4312 ==
																						BgL_classz00_4299);
																				}
																			else
																				{	/* Cgen/emit_cop.scm 591 */
																					BgL_res2358z00_4304 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2617z00_5162 = BgL_res2358z00_4304;
														}
												}
											}
										}
										if (BgL_test2617z00_5162)
											{	/* Cgen/emit_cop.scm 591 */
												bool_t BgL_test2621z00_5188;

												{	/* Cgen/emit_cop.scm 591 */
													BgL_variablez00_bglt BgL_arg2179z00_4314;

													BgL_arg2179z00_4314 =
														(((BgL_varcz00_bglt) COBJECT(
																((BgL_varcz00_bglt)
																	(((BgL_cfailz00_bglt) COBJECT(
																				((BgL_cfailz00_bglt)
																					BgL_copz00_4167)))->BgL_msgz00))))->
														BgL_variablez00);
													BgL_test2621z00_5188 =
														(((obj_t) BgL_arg2179z00_4314) ==
														BGl_za2bfalseza2z00zzcgen_emitzd2copzd2);
												}
												if (BgL_test2621z00_5188)
													{	/* Cgen/emit_cop.scm 592 */
														bool_t BgL_test2622z00_5195;

														{	/* Cgen/emit_cop.scm 592 */
															BgL_copz00_bglt BgL_arg2178z00_4315;

															BgL_arg2178z00_4315 =
																(((BgL_cfailz00_bglt) COBJECT(
																		((BgL_cfailz00_bglt) BgL_copz00_4167)))->
																BgL_objz00);
															{	/* Cgen/emit_cop.scm 592 */
																obj_t BgL_classz00_4316;

																BgL_classz00_4316 = BGl_varcz00zzcgen_copz00;
																{	/* Cgen/emit_cop.scm 592 */
																	BgL_objectz00_bglt BgL_arg1807z00_4317;

																	{	/* Cgen/emit_cop.scm 592 */
																		obj_t BgL_tmpz00_5198;

																		BgL_tmpz00_5198 =
																			((obj_t)
																			((BgL_objectz00_bglt)
																				BgL_arg2178z00_4315));
																		BgL_arg1807z00_4317 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_5198);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Cgen/emit_cop.scm 592 */
																			long BgL_idxz00_4318;

																			BgL_idxz00_4318 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_4317);
																			BgL_test2622z00_5195 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_4318 + 2L)) ==
																				BgL_classz00_4316);
																		}
																	else
																		{	/* Cgen/emit_cop.scm 592 */
																			bool_t BgL_res2359z00_4321;

																			{	/* Cgen/emit_cop.scm 592 */
																				obj_t BgL_oclassz00_4322;

																				{	/* Cgen/emit_cop.scm 592 */
																					obj_t BgL_arg1815z00_4323;
																					long BgL_arg1816z00_4324;

																					BgL_arg1815z00_4323 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Cgen/emit_cop.scm 592 */
																						long BgL_arg1817z00_4325;

																						BgL_arg1817z00_4325 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_4317);
																						BgL_arg1816z00_4324 =
																							(BgL_arg1817z00_4325 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_4322 =
																						VECTOR_REF(BgL_arg1815z00_4323,
																						BgL_arg1816z00_4324);
																				}
																				{	/* Cgen/emit_cop.scm 592 */
																					bool_t BgL__ortest_1115z00_4326;

																					BgL__ortest_1115z00_4326 =
																						(BgL_classz00_4316 ==
																						BgL_oclassz00_4322);
																					if (BgL__ortest_1115z00_4326)
																						{	/* Cgen/emit_cop.scm 592 */
																							BgL_res2359z00_4321 =
																								BgL__ortest_1115z00_4326;
																						}
																					else
																						{	/* Cgen/emit_cop.scm 592 */
																							long BgL_odepthz00_4327;

																							{	/* Cgen/emit_cop.scm 592 */
																								obj_t BgL_arg1804z00_4328;

																								BgL_arg1804z00_4328 =
																									(BgL_oclassz00_4322);
																								BgL_odepthz00_4327 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_4328);
																							}
																							if ((2L < BgL_odepthz00_4327))
																								{	/* Cgen/emit_cop.scm 592 */
																									obj_t BgL_arg1802z00_4329;

																									{	/* Cgen/emit_cop.scm 592 */
																										obj_t BgL_arg1803z00_4330;

																										BgL_arg1803z00_4330 =
																											(BgL_oclassz00_4322);
																										BgL_arg1802z00_4329 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_4330, 2L);
																									}
																									BgL_res2359z00_4321 =
																										(BgL_arg1802z00_4329 ==
																										BgL_classz00_4316);
																								}
																							else
																								{	/* Cgen/emit_cop.scm 592 */
																									BgL_res2359z00_4321 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2622z00_5195 =
																				BgL_res2359z00_4321;
																		}
																}
															}
														}
														if (BgL_test2622z00_5195)
															{	/* Cgen/emit_cop.scm 592 */
																BgL_variablez00_bglt BgL_arg2176z00_4331;

																BgL_arg2176z00_4331 =
																	(((BgL_varcz00_bglt) COBJECT(
																			((BgL_varcz00_bglt)
																				(((BgL_cfailz00_bglt) COBJECT(
																							((BgL_cfailz00_bglt)
																								BgL_copz00_4167)))->
																					BgL_objz00))))->BgL_variablez00);
																BgL_test2611z00_5128 =
																	(((obj_t) BgL_arg2176z00_4331) ==
																	BGl_za2bfalseza2z00zzcgen_emitzd2copzd2);
															}
														else
															{	/* Cgen/emit_cop.scm 592 */
																BgL_test2611z00_5128 = ((bool_t) 0);
															}
													}
												else
													{	/* Cgen/emit_cop.scm 591 */
														BgL_test2611z00_5128 = ((bool_t) 0);
													}
											}
										else
											{	/* Cgen/emit_cop.scm 591 */
												BgL_test2611z00_5128 = ((bool_t) 0);
											}
									}
								else
									{	/* Cgen/emit_cop.scm 590 */
										BgL_test2611z00_5128 = ((bool_t) 0);
									}
							}
						else
							{	/* Cgen/emit_cop.scm 590 */
								BgL_test2611z00_5128 = ((bool_t) 0);
							}
					}
					if (BgL_test2611z00_5128)
						{	/* Cgen/emit_cop.scm 590 */
							bgl_display_string(BGl_string2426z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
					else
						{	/* Cgen/emit_cop.scm 590 */
							if (
								((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) <= 0L))
								{	/* Cgen/emit_cop.scm 594 */
									bgl_display_string(BGl_string2427z00zzcgen_emitzd2copzd2,
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cfailz00_bglt)
												COBJECT(((BgL_cfailz00_bglt) BgL_copz00_4167)))->
											BgL_procz00));
									bgl_display_char(((unsigned char) ','),
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cfailz00_bglt)
												COBJECT(((BgL_cfailz00_bglt) BgL_copz00_4167)))->
											BgL_msgz00));
									bgl_display_char(((unsigned char) ','),
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cfailz00_bglt)
												COBJECT(((BgL_cfailz00_bglt) BgL_copz00_4167)))->
											BgL_objz00));
									bgl_display_string(BGl_string2428z00zzcgen_emitzd2copzd2,
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
								}
							else
								{	/* Cgen/emit_cop.scm 594 */
									bgl_display_string(BGl_string2429z00zzcgen_emitzd2copzd2,
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cfailz00_bglt)
												COBJECT(((BgL_cfailz00_bglt) BgL_copz00_4167)))->
											BgL_procz00));
									bgl_display_char(((unsigned char) ','),
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cfailz00_bglt)
												COBJECT(((BgL_cfailz00_bglt) BgL_copz00_4167)))->
											BgL_msgz00));
									bgl_display_char(((unsigned char) ','),
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cfailz00_bglt)
												COBJECT(((BgL_cfailz00_bglt) BgL_copz00_4167)))->
											BgL_objz00));
									bgl_display_string(BGl_string2430z00zzcgen_emitzd2copzd2,
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}}}
				BgL_tmpz00_5118 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_5118);
			}
		}

	}



/* &emit-cop-capp1428 */
	obj_t BGl_z62emitzd2copzd2capp1428z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4168, obj_t BgL_copz00_4169)
	{
		{	/* Cgen/emit_cop.scm 518 */
			{

				{	/* Cgen/emit_cop.scm 563 */
					BgL_variablez00_bglt BgL_funz00_4346;
					obj_t BgL_locz00_4347;

					BgL_funz00_4346 =
						(((BgL_varcz00_bglt) COBJECT(
								((BgL_varcz00_bglt)
									(((BgL_cappz00_bglt) COBJECT(
												((BgL_cappz00_bglt) BgL_copz00_4169)))->BgL_funz00))))->
						BgL_variablez00);
					BgL_locz00_4347 =
						(((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt) ((BgL_cappz00_bglt)
										BgL_copz00_4169))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_locz00_4347);
					{	/* Cgen/emit_cop.scm 567 */
						bool_t BgL_test2627z00_5266;

						{	/* Cgen/emit_cop.scm 567 */
							bool_t BgL_test2628z00_5267;

							{	/* Cgen/emit_cop.scm 567 */
								BgL_valuez00_bglt BgL_arg2115z00_4348;

								BgL_arg2115z00_4348 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_funz00_4346))))->
									BgL_valuez00);
								{	/* Cgen/emit_cop.scm 567 */
									obj_t BgL_classz00_4349;

									BgL_classz00_4349 = BGl_cfunz00zzast_varz00;
									{	/* Cgen/emit_cop.scm 567 */
										BgL_objectz00_bglt BgL_arg1807z00_4350;

										{	/* Cgen/emit_cop.scm 567 */
											obj_t BgL_tmpz00_5271;

											BgL_tmpz00_5271 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg2115z00_4348));
											BgL_arg1807z00_4350 =
												(BgL_objectz00_bglt) (BgL_tmpz00_5271);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cgen/emit_cop.scm 567 */
												long BgL_idxz00_4351;

												BgL_idxz00_4351 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4350);
												BgL_test2628z00_5267 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4351 + 3L)) == BgL_classz00_4349);
											}
										else
											{	/* Cgen/emit_cop.scm 567 */
												bool_t BgL_res2355z00_4354;

												{	/* Cgen/emit_cop.scm 567 */
													obj_t BgL_oclassz00_4355;

													{	/* Cgen/emit_cop.scm 567 */
														obj_t BgL_arg1815z00_4356;
														long BgL_arg1816z00_4357;

														BgL_arg1815z00_4356 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cgen/emit_cop.scm 567 */
															long BgL_arg1817z00_4358;

															BgL_arg1817z00_4358 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4350);
															BgL_arg1816z00_4357 =
																(BgL_arg1817z00_4358 - OBJECT_TYPE);
														}
														BgL_oclassz00_4355 =
															VECTOR_REF(BgL_arg1815z00_4356,
															BgL_arg1816z00_4357);
													}
													{	/* Cgen/emit_cop.scm 567 */
														bool_t BgL__ortest_1115z00_4359;

														BgL__ortest_1115z00_4359 =
															(BgL_classz00_4349 == BgL_oclassz00_4355);
														if (BgL__ortest_1115z00_4359)
															{	/* Cgen/emit_cop.scm 567 */
																BgL_res2355z00_4354 = BgL__ortest_1115z00_4359;
															}
														else
															{	/* Cgen/emit_cop.scm 567 */
																long BgL_odepthz00_4360;

																{	/* Cgen/emit_cop.scm 567 */
																	obj_t BgL_arg1804z00_4361;

																	BgL_arg1804z00_4361 = (BgL_oclassz00_4355);
																	BgL_odepthz00_4360 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4361);
																}
																if ((3L < BgL_odepthz00_4360))
																	{	/* Cgen/emit_cop.scm 567 */
																		obj_t BgL_arg1802z00_4362;

																		{	/* Cgen/emit_cop.scm 567 */
																			obj_t BgL_arg1803z00_4363;

																			BgL_arg1803z00_4363 =
																				(BgL_oclassz00_4355);
																			BgL_arg1802z00_4362 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4363, 3L);
																		}
																		BgL_res2355z00_4354 =
																			(BgL_arg1802z00_4362 ==
																			BgL_classz00_4349);
																	}
																else
																	{	/* Cgen/emit_cop.scm 567 */
																		BgL_res2355z00_4354 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2628z00_5267 = BgL_res2355z00_4354;
											}
									}
								}
							}
							if (BgL_test2628z00_5267)
								{	/* Cgen/emit_cop.scm 567 */
									BgL_test2627z00_5266 =
										(((BgL_cfunz00_bglt) COBJECT(
												((BgL_cfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_globalz00_bglt) BgL_funz00_4346))))->
														BgL_valuez00))))->BgL_infixzf3zf3);
								}
							else
								{	/* Cgen/emit_cop.scm 567 */
									BgL_test2627z00_5266 = ((bool_t) 0);
								}
						}
						if (BgL_test2627z00_5266)
							{	/* Cgen/emit_cop.scm 568 */
								bool_t BgL_tmpz00_5299;

								{	/* Cgen/emit_cop.scm 521 */
									obj_t BgL_actualsz00_4335;

									BgL_actualsz00_4335 =
										(((BgL_cappz00_bglt) COBJECT(
												((BgL_cappz00_bglt) BgL_copz00_4169)))->BgL_argsz00);
									bgl_display_char(((unsigned char) '('),
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									if (NULLP(BgL_actualsz00_4335))
										{	/* Cgen/emit_cop.scm 525 */
											BgL_copz00_bglt BgL_arg2119z00_4336;

											BgL_arg2119z00_4336 =
												(((BgL_cappz00_bglt) COBJECT(
														((BgL_cappz00_bglt) BgL_copz00_4169)))->BgL_funz00);
											BBOOL(BGl_emitzd2copzd2zzcgen_emitzd2copzd2
												(BgL_arg2119z00_4336));
										}
									else
										{	/* Cgen/emit_cop.scm 524 */
											if (NULLP(CDR(((obj_t) BgL_actualsz00_4335))))
												{	/* Cgen/emit_cop.scm 526 */
													{	/* Cgen/emit_cop.scm 527 */
														obj_t BgL_arg2122z00_4337;

														BgL_arg2122z00_4337 =
															CAR(((obj_t) BgL_actualsz00_4335));
														BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
															((BgL_copz00_bglt) BgL_arg2122z00_4337));
													}
													{	/* Cgen/emit_cop.scm 528 */
														BgL_copz00_bglt BgL_arg2123z00_4338;

														BgL_arg2123z00_4338 =
															(((BgL_cappz00_bglt) COBJECT(
																	((BgL_cappz00_bglt) BgL_copz00_4169)))->
															BgL_funz00);
														BBOOL(BGl_emitzd2copzd2zzcgen_emitzd2copzd2
															(BgL_arg2123z00_4338));
													}
												}
											else
												{	/* Cgen/emit_cop.scm 529 */
													bool_t BgL_test2634z00_5321;

													{	/* Cgen/emit_cop.scm 529 */
														obj_t BgL_tmpz00_5322;

														{	/* Cgen/emit_cop.scm 529 */
															obj_t BgL_pairz00_4339;

															BgL_pairz00_4339 =
																CDR(((obj_t) BgL_actualsz00_4335));
															BgL_tmpz00_5322 = CDR(BgL_pairz00_4339);
														}
														BgL_test2634z00_5321 = NULLP(BgL_tmpz00_5322);
													}
													if (BgL_test2634z00_5321)
														{	/* Cgen/emit_cop.scm 529 */
															{	/* Cgen/emit_cop.scm 530 */
																obj_t BgL_arg2126z00_4340;

																BgL_arg2126z00_4340 =
																	CAR(((obj_t) BgL_actualsz00_4335));
																BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
																	((BgL_copz00_bglt) BgL_arg2126z00_4340));
															}
															BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
																(((BgL_cappz00_bglt) COBJECT(
																			((BgL_cappz00_bglt) BgL_copz00_4169)))->
																	BgL_funz00));
															{	/* Cgen/emit_cop.scm 532 */
																obj_t BgL_arg2129z00_4341;

																{	/* Cgen/emit_cop.scm 532 */
																	obj_t BgL_pairz00_4342;

																	BgL_pairz00_4342 =
																		CDR(((obj_t) BgL_actualsz00_4335));
																	BgL_arg2129z00_4341 = CAR(BgL_pairz00_4342);
																}
																BBOOL(BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
																		((BgL_copz00_bglt) BgL_arg2129z00_4341)));
															}
														}
													else
														{	/* Cgen/emit_cop.scm 535 */
															obj_t BgL_arg2130z00_4343;

															{	/* Cgen/emit_cop.scm 535 */
																BgL_variablez00_bglt BgL_arg2131z00_4344;

																BgL_arg2131z00_4344 =
																	(((BgL_varcz00_bglt) COBJECT(
																			((BgL_varcz00_bglt)
																				(((BgL_cappz00_bglt) COBJECT(
																							((BgL_cappz00_bglt)
																								BgL_copz00_4169)))->
																					BgL_funz00))))->BgL_variablez00);
																BgL_arg2130z00_4343 =
																	BGl_shapez00zztools_shapez00(((obj_t)
																		BgL_arg2131z00_4344));
															}
															BGl_errorz00zz__errorz00
																(BGl_string2374z00zzcgen_emitzd2copzd2,
																BGl_string2431z00zzcgen_emitzd2copzd2,
																BgL_arg2130z00_4343);
														}
												}
										}
									bgl_display_char(((unsigned char) ')'),
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									BgL_tmpz00_5299 = ((bool_t) 1);
								}
								return BBOOL(BgL_tmpz00_5299);
							}
						else
							{	/* Cgen/emit_cop.scm 569 */
								bool_t BgL_test2635z00_5349;

								{	/* Cgen/emit_cop.scm 569 */
									bool_t BgL_test2636z00_5350;

									{	/* Cgen/emit_cop.scm 569 */
										BgL_valuez00_bglt BgL_arg2113z00_4364;

										BgL_arg2113z00_4364 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_funz00_4346))))->
											BgL_valuez00);
										{	/* Cgen/emit_cop.scm 569 */
											obj_t BgL_classz00_4365;

											BgL_classz00_4365 = BGl_cfunz00zzast_varz00;
											{	/* Cgen/emit_cop.scm 569 */
												BgL_objectz00_bglt BgL_arg1807z00_4366;

												{	/* Cgen/emit_cop.scm 569 */
													obj_t BgL_tmpz00_5354;

													BgL_tmpz00_5354 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg2113z00_4364));
													BgL_arg1807z00_4366 =
														(BgL_objectz00_bglt) (BgL_tmpz00_5354);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cgen/emit_cop.scm 569 */
														long BgL_idxz00_4367;

														BgL_idxz00_4367 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4366);
														BgL_test2636z00_5350 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_4367 + 3L)) == BgL_classz00_4365);
													}
												else
													{	/* Cgen/emit_cop.scm 569 */
														bool_t BgL_res2356z00_4370;

														{	/* Cgen/emit_cop.scm 569 */
															obj_t BgL_oclassz00_4371;

															{	/* Cgen/emit_cop.scm 569 */
																obj_t BgL_arg1815z00_4372;
																long BgL_arg1816z00_4373;

																BgL_arg1815z00_4372 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cgen/emit_cop.scm 569 */
																	long BgL_arg1817z00_4374;

																	BgL_arg1817z00_4374 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4366);
																	BgL_arg1816z00_4373 =
																		(BgL_arg1817z00_4374 - OBJECT_TYPE);
																}
																BgL_oclassz00_4371 =
																	VECTOR_REF(BgL_arg1815z00_4372,
																	BgL_arg1816z00_4373);
															}
															{	/* Cgen/emit_cop.scm 569 */
																bool_t BgL__ortest_1115z00_4375;

																BgL__ortest_1115z00_4375 =
																	(BgL_classz00_4365 == BgL_oclassz00_4371);
																if (BgL__ortest_1115z00_4375)
																	{	/* Cgen/emit_cop.scm 569 */
																		BgL_res2356z00_4370 =
																			BgL__ortest_1115z00_4375;
																	}
																else
																	{	/* Cgen/emit_cop.scm 569 */
																		long BgL_odepthz00_4376;

																		{	/* Cgen/emit_cop.scm 569 */
																			obj_t BgL_arg1804z00_4377;

																			BgL_arg1804z00_4377 =
																				(BgL_oclassz00_4371);
																			BgL_odepthz00_4376 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_4377);
																		}
																		if ((3L < BgL_odepthz00_4376))
																			{	/* Cgen/emit_cop.scm 569 */
																				obj_t BgL_arg1802z00_4378;

																				{	/* Cgen/emit_cop.scm 569 */
																					obj_t BgL_arg1803z00_4379;

																					BgL_arg1803z00_4379 =
																						(BgL_oclassz00_4371);
																					BgL_arg1802z00_4378 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_4379, 3L);
																				}
																				BgL_res2356z00_4370 =
																					(BgL_arg1802z00_4378 ==
																					BgL_classz00_4365);
																			}
																		else
																			{	/* Cgen/emit_cop.scm 569 */
																				BgL_res2356z00_4370 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2636z00_5350 = BgL_res2356z00_4370;
													}
											}
										}
									}
									if (BgL_test2636z00_5350)
										{	/* Cgen/emit_cop.scm 569 */
											BgL_test2635z00_5349 =
												(((BgL_cfunz00_bglt) COBJECT(
														((BgL_cfunz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_globalz00_bglt)
																				BgL_funz00_4346))))->BgL_valuez00))))->
												BgL_macrozf3zf3);
										}
									else
										{	/* Cgen/emit_cop.scm 569 */
											BgL_test2635z00_5349 = ((bool_t) 0);
										}
								}
								if (BgL_test2635z00_5349)
									{	/* Cgen/emit_cop.scm 569 */
										{	/* Cgen/emit_cop.scm 559 */
											obj_t BgL_oz00_4345;

											BgL_oz00_4345 =
												BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00;
											BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00
												= BTRUE;
											BGl_emitzd2prefixzd2cappze70ze7zzcgen_emitzd2copzd2((
													(BgL_cappz00_bglt) BgL_copz00_4169));
											return
												(BGl_za2bdbzd2debugzd2nozd2linezd2directiveszf3za2zf3zzengine_paramz00
												= BgL_oz00_4345, BUNSPEC);
										}
									}
								else
									{	/* Cgen/emit_cop.scm 569 */
										return
											BBOOL(BGl_emitzd2prefixzd2cappze70ze7zzcgen_emitzd2copzd2(
												((BgL_cappz00_bglt) BgL_copz00_4169)));
									}
							}
					}
				}
			}
		}

	}



/* emit-prefix-capp~0 */
	bool_t BGl_emitzd2prefixzd2cappze70ze7zzcgen_emitzd2copzd2(BgL_cappz00_bglt
		BgL_copz00_4214)
	{
		{	/* Cgen/emit_cop.scm 556 */
			{	/* Cgen/emit_cop.scm 540 */
				obj_t BgL_actualsz00_2572;

				BgL_actualsz00_2572 =
					(((BgL_cappz00_bglt) COBJECT(BgL_copz00_4214))->BgL_argsz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
					(((BgL_cappz00_bglt) COBJECT(BgL_copz00_4214))->BgL_funz00));
				bgl_display_char(((unsigned char) '('),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				if (NULLP(BgL_actualsz00_2572))
					{	/* Cgen/emit_cop.scm 543 */
						bgl_display_char(((unsigned char) ')'),
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						return ((bool_t) 1);
					}
				else
					{
						obj_t BgL_actualsz00_2576;

						BgL_actualsz00_2576 = BgL_actualsz00_2572;
					BgL_zc3z04anonymousza32138ze3z87_2577:
						if (NULLP(CDR(((obj_t) BgL_actualsz00_2576))))
							{	/* Cgen/emit_cop.scm 548 */
								{	/* Cgen/emit_cop.scm 550 */
									obj_t BgL_arg2141z00_2580;

									BgL_arg2141z00_2580 = CAR(((obj_t) BgL_actualsz00_2576));
									BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
										((BgL_copz00_bglt) BgL_arg2141z00_2580));
								}
								bgl_display_char(((unsigned char) ')'),
									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
								return ((bool_t) 1);
							}
						else
							{	/* Cgen/emit_cop.scm 548 */
								{	/* Cgen/emit_cop.scm 554 */
									obj_t BgL_arg2142z00_2581;

									BgL_arg2142z00_2581 = CAR(((obj_t) BgL_actualsz00_2576));
									BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
										((BgL_copz00_bglt) BgL_arg2142z00_2581));
								}
								bgl_display_string(BGl_string2411z00zzcgen_emitzd2copzd2,
									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
								{	/* Cgen/emit_cop.scm 556 */
									obj_t BgL_arg2143z00_2582;

									BgL_arg2143z00_2582 = CDR(((obj_t) BgL_actualsz00_2576));
									{
										obj_t BgL_actualsz00_5410;

										BgL_actualsz00_5410 = BgL_arg2143z00_2582;
										BgL_actualsz00_2576 = BgL_actualsz00_5410;
										goto BgL_zc3z04anonymousza32138ze3z87_2577;
									}
								}
							}
					}
			}
		}

	}



/* &emit-cop-capply1426 */
	obj_t BGl_z62emitzd2copzd2capply1426z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4170, obj_t BgL_copz00_4171)
	{
		{	/* Cgen/emit_cop.scm 505 */
			{	/* Cgen/emit_cop.scm 506 */
				bool_t BgL_tmpz00_5411;

				{	/* Cgen/emit_cop.scm 507 */
					obj_t BgL_arg2099z00_4381;

					BgL_arg2099z00_4381 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_capplyz00_bglt) BgL_copz00_4171))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg2099z00_4381);
				}
				bgl_display_string(BGl_string2432z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_capplyz00_bglt)
							COBJECT(((BgL_capplyz00_bglt) BgL_copz00_4171)))->BgL_funz00));
				bgl_display_string(BGl_string2411z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_capplyz00_bglt)
							COBJECT(((BgL_capplyz00_bglt) BgL_copz00_4171)))->BgL_argz00));
				bgl_display_char(((unsigned char) ')'),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BgL_tmpz00_5411 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_5411);
			}
		}

	}



/* &emit-cop-cfuncall1423 */
	obj_t BGl_z62emitzd2copzd2cfuncall1423z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4172, obj_t BgL_copz00_4173)
	{
		{	/* Cgen/emit_cop.scm 308 */
			{	/* Cgen/emit_cop.scm 311 */
				bool_t BgL_tmpz00_5426;

				{
					BgL_copz00_bglt BgL_xz00_4387;
					obj_t BgL_yz00_4388;

					{
						BgL_cfuncallz00_bglt BgL_copz00_4454;
						BgL_cfuncallz00_bglt BgL_copz00_4447;
						BgL_cfuncallz00_bglt BgL_copz00_4440;
						BgL_cfuncallz00_bglt BgL_copz00_4432;
						BgL_cfuncallz00_bglt BgL_copz00_4424;

						{	/* Cgen/emit_cop.scm 491 */
							obj_t BgL_arg1962z00_4464;

							BgL_arg1962z00_4464 =
								(((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt)
											((BgL_cfuncallz00_bglt) BgL_copz00_4173))))->BgL_locz00);
							BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1962z00_4464);
						}
						{	/* Cgen/emit_cop.scm 492 */
							obj_t BgL_casezd2valuezd2_4465;

							BgL_casezd2valuezd2_4465 =
								(((BgL_cfuncallz00_bglt) COBJECT(
										((BgL_cfuncallz00_bglt) BgL_copz00_4173)))->
								BgL_strengthz00);
							if ((BgL_casezd2valuezd2_4465 == CNST_TABLE_REF(5)))
								{	/* Cgen/emit_cop.scm 492 */
									BgL_copz00_4424 = ((BgL_cfuncallz00_bglt) BgL_copz00_4173);
									{	/* Cgen/emit_cop.scm 369 */
										obj_t BgL_actualsz00_4425;

										BgL_actualsz00_4425 =
											(((BgL_cfuncallz00_bglt) COBJECT(BgL_copz00_4424))->
											BgL_argsz00);
										BGl_emitzd2copzd2zzcgen_emitzd2copzd2(((
													(BgL_cfuncallz00_bglt) COBJECT(BgL_copz00_4424))->
												BgL_funz00));
										bgl_display_char(((unsigned char) '('),
											BGl_za2czd2portza2zd2zzbackend_c_emitz00);
										{
											obj_t BgL_actualsz00_4427;

											BgL_actualsz00_4427 = BgL_actualsz00_4425;
										BgL_loopz00_4426:
											{	/* Cgen/emit_cop.scm 375 */
												bool_t BgL_test2643z00_5440;

												{	/* Cgen/emit_cop.scm 375 */
													obj_t BgL_tmpz00_5441;

													{	/* Cgen/emit_cop.scm 375 */
														obj_t BgL_pairz00_4428;

														BgL_pairz00_4428 =
															CDR(((obj_t) BgL_actualsz00_4427));
														BgL_tmpz00_5441 = CDR(BgL_pairz00_4428);
													}
													BgL_test2643z00_5440 = NULLP(BgL_tmpz00_5441);
												}
												if (BgL_test2643z00_5440)
													{	/* Cgen/emit_cop.scm 375 */
														{	/* Cgen/emit_cop.scm 377 */
															obj_t BgL_arg1969z00_4429;

															BgL_arg1969z00_4429 =
																CAR(((obj_t) BgL_actualsz00_4427));
															BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
																((BgL_copz00_bglt) BgL_arg1969z00_4429));
														}
														bgl_display_char(((unsigned char) ')'),
															BGl_za2czd2portza2zd2zzbackend_c_emitz00);
														BgL_tmpz00_5426 = ((bool_t) 1);
													}
												else
													{	/* Cgen/emit_cop.scm 375 */
														{	/* Cgen/emit_cop.scm 381 */
															obj_t BgL_arg1970z00_4430;

															BgL_arg1970z00_4430 =
																CAR(((obj_t) BgL_actualsz00_4427));
															BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
																((BgL_copz00_bglt) BgL_arg1970z00_4430));
														}
														bgl_display_string
															(BGl_string2411z00zzcgen_emitzd2copzd2,
															BGl_za2czd2portza2zd2zzbackend_c_emitz00);
														{	/* Cgen/emit_cop.scm 383 */
															obj_t BgL_arg1971z00_4431;

															BgL_arg1971z00_4431 =
																CDR(((obj_t) BgL_actualsz00_4427));
															{
																obj_t BgL_actualsz00_5458;

																BgL_actualsz00_5458 = BgL_arg1971z00_4431;
																BgL_actualsz00_4427 = BgL_actualsz00_5458;
																goto BgL_loopz00_4426;
															}
														}
													}
											}
										}
									}
								}
							else
								{	/* Cgen/emit_cop.scm 492 */
									if ((BgL_casezd2valuezd2_4465 == CNST_TABLE_REF(6)))
										{	/* Cgen/emit_cop.scm 492 */
											BgL_copz00_4432 =
												((BgL_cfuncallz00_bglt) BgL_copz00_4173);
											{	/* Cgen/emit_cop.scm 385 */
												obj_t BgL_actualsz00_4433;

												BgL_actualsz00_4433 =
													(((BgL_cfuncallz00_bglt) COBJECT(BgL_copz00_4432))->
													BgL_argsz00);
												BGl_outzd2callze70z35zzcgen_emitzd2copzd2((
														(BgL_cfuncallz00_bglt) BgL_copz00_4173),
													BGl_string2436z00zzcgen_emitzd2copzd2,
													BGl_proc2435z00zzcgen_emitzd2copzd2,
													BgL_actualsz00_4433, ((bool_t) 0));
												{
													obj_t BgL_actualsz00_4435;

													BgL_actualsz00_4435 = BgL_actualsz00_4433;
												BgL_loopz00_4434:
													{	/* Cgen/emit_cop.scm 390 */
														bool_t BgL_test2645z00_5466;

														{	/* Cgen/emit_cop.scm 390 */
															obj_t BgL_tmpz00_5467;

															{	/* Cgen/emit_cop.scm 390 */
																obj_t BgL_pairz00_4436;

																BgL_pairz00_4436 =
																	CDR(((obj_t) BgL_actualsz00_4435));
																BgL_tmpz00_5467 = CDR(BgL_pairz00_4436);
															}
															BgL_test2645z00_5466 = NULLP(BgL_tmpz00_5467);
														}
														if (BgL_test2645z00_5466)
															{	/* Cgen/emit_cop.scm 390 */
																{	/* Cgen/emit_cop.scm 392 */
																	obj_t BgL_arg1976z00_4437;

																	BgL_arg1976z00_4437 =
																		CAR(((obj_t) BgL_actualsz00_4435));
																	BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
																		((BgL_copz00_bglt) BgL_arg1976z00_4437));
																}
																bgl_display_string
																	(BGl_string2413z00zzcgen_emitzd2copzd2,
																	BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																BgL_tmpz00_5426 = ((bool_t) 1);
															}
														else
															{	/* Cgen/emit_cop.scm 390 */
																{	/* Cgen/emit_cop.scm 396 */
																	obj_t BgL_arg1977z00_4438;

																	BgL_arg1977z00_4438 =
																		CAR(((obj_t) BgL_actualsz00_4435));
																	BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
																		((BgL_copz00_bglt) BgL_arg1977z00_4438));
																}
																bgl_display_string
																	(BGl_string2411z00zzcgen_emitzd2copzd2,
																	BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																{	/* Cgen/emit_cop.scm 398 */
																	obj_t BgL_arg1978z00_4439;

																	BgL_arg1978z00_4439 =
																		CDR(((obj_t) BgL_actualsz00_4435));
																	{
																		obj_t BgL_actualsz00_5484;

																		BgL_actualsz00_5484 = BgL_arg1978z00_4439;
																		BgL_actualsz00_4435 = BgL_actualsz00_5484;
																		goto BgL_loopz00_4434;
																	}
																}
															}
													}
												}
											}
										}
									else
										{	/* Cgen/emit_cop.scm 492 */
											BgL_copz00_4454 =
												((BgL_cfuncallz00_bglt) BgL_copz00_4173);
											{	/* Cgen/emit_cop.scm 460 */
												obj_t BgL_actualsz00_4455;

												BgL_actualsz00_4455 =
													(((BgL_cfuncallz00_bglt) COBJECT(BgL_copz00_4454))->
													BgL_argsz00);
												{	/* Cgen/emit_cop.scm 460 */
													BgL_copz00_bglt BgL_funz00_4456;

													BgL_funz00_4456 =
														(((BgL_cfuncallz00_bglt) COBJECT(BgL_copz00_4454))->
														BgL_funz00);
													{	/* Cgen/emit_cop.scm 461 */
														long BgL_lenz00_4457;

														BgL_lenz00_4457 =
															bgl_list_length(BgL_actualsz00_4455);
														{	/* Cgen/emit_cop.scm 462 */

															{	/* Cgen/emit_cop.scm 463 */
																bool_t BgL_test2646z00_5489;

																if ((BgL_lenz00_4457 > 32L))
																	{	/* Cgen/emit_cop.scm 463 */
																		BgL_test2646z00_5489 = ((bool_t) 1);
																	}
																else
																	{	/* Cgen/emit_cop.scm 463 */
																		bool_t BgL_test2648z00_5492;

																		BgL_xz00_4387 = BgL_funz00_4456;
																		BgL_yz00_4388 =
																			CAR(((obj_t) BgL_actualsz00_4455));
																		{	/* Cgen/emit_cop.scm 363 */
																			bool_t BgL_test2649z00_5493;

																			{	/* Cgen/emit_cop.scm 363 */
																				bool_t BgL_test2650z00_5494;

																				{	/* Cgen/emit_cop.scm 363 */
																					obj_t BgL_classz00_4389;

																					BgL_classz00_4389 =
																						BGl_varcz00zzcgen_copz00;
																					{	/* Cgen/emit_cop.scm 363 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_4390;
																						{	/* Cgen/emit_cop.scm 363 */
																							obj_t BgL_tmpz00_5495;

																							BgL_tmpz00_5495 =
																								((obj_t)
																								((BgL_objectz00_bglt)
																									BgL_xz00_4387));
																							BgL_arg1807z00_4390 =
																								(BgL_objectz00_bglt)
																								(BgL_tmpz00_5495);
																						}
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Cgen/emit_cop.scm 363 */
																								long BgL_idxz00_4391;

																								BgL_idxz00_4391 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_4390);
																								BgL_test2650z00_5494 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_4391 + 2L)) ==
																									BgL_classz00_4389);
																							}
																						else
																							{	/* Cgen/emit_cop.scm 363 */
																								bool_t BgL_res2353z00_4394;

																								{	/* Cgen/emit_cop.scm 363 */
																									obj_t BgL_oclassz00_4395;

																									{	/* Cgen/emit_cop.scm 363 */
																										obj_t BgL_arg1815z00_4396;
																										long BgL_arg1816z00_4397;

																										BgL_arg1815z00_4396 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Cgen/emit_cop.scm 363 */
																											long BgL_arg1817z00_4398;

																											BgL_arg1817z00_4398 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_4390);
																											BgL_arg1816z00_4397 =
																												(BgL_arg1817z00_4398 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_4395 =
																											VECTOR_REF
																											(BgL_arg1815z00_4396,
																											BgL_arg1816z00_4397);
																									}
																									{	/* Cgen/emit_cop.scm 363 */
																										bool_t
																											BgL__ortest_1115z00_4399;
																										BgL__ortest_1115z00_4399 =
																											(BgL_classz00_4389 ==
																											BgL_oclassz00_4395);
																										if (BgL__ortest_1115z00_4399)
																											{	/* Cgen/emit_cop.scm 363 */
																												BgL_res2353z00_4394 =
																													BgL__ortest_1115z00_4399;
																											}
																										else
																											{	/* Cgen/emit_cop.scm 363 */
																												long BgL_odepthz00_4400;

																												{	/* Cgen/emit_cop.scm 363 */
																													obj_t
																														BgL_arg1804z00_4401;
																													BgL_arg1804z00_4401 =
																														(BgL_oclassz00_4395);
																													BgL_odepthz00_4400 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_4401);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_4400))
																													{	/* Cgen/emit_cop.scm 363 */
																														obj_t
																															BgL_arg1802z00_4402;
																														{	/* Cgen/emit_cop.scm 363 */
																															obj_t
																																BgL_arg1803z00_4403;
																															BgL_arg1803z00_4403
																																=
																																(BgL_oclassz00_4395);
																															BgL_arg1802z00_4402
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_4403,
																																2L);
																														}
																														BgL_res2353z00_4394
																															=
																															(BgL_arg1802z00_4402
																															==
																															BgL_classz00_4389);
																													}
																												else
																													{	/* Cgen/emit_cop.scm 363 */
																														BgL_res2353z00_4394
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2650z00_5494 =
																									BgL_res2353z00_4394;
																							}
																					}
																				}
																				if (BgL_test2650z00_5494)
																					{	/* Cgen/emit_cop.scm 363 */
																						obj_t BgL_classz00_4404;

																						BgL_classz00_4404 =
																							BGl_varcz00zzcgen_copz00;
																						if (BGL_OBJECTP(BgL_yz00_4388))
																							{	/* Cgen/emit_cop.scm 363 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_4405;
																								BgL_arg1807z00_4405 =
																									(BgL_objectz00_bglt)
																									(BgL_yz00_4388);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* Cgen/emit_cop.scm 363 */
																										long BgL_idxz00_4406;

																										BgL_idxz00_4406 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_4405);
																										BgL_test2649z00_5493 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_4406 +
																													2L)) ==
																											BgL_classz00_4404);
																									}
																								else
																									{	/* Cgen/emit_cop.scm 363 */
																										bool_t BgL_res2354z00_4409;

																										{	/* Cgen/emit_cop.scm 363 */
																											obj_t BgL_oclassz00_4410;

																											{	/* Cgen/emit_cop.scm 363 */
																												obj_t
																													BgL_arg1815z00_4411;
																												long
																													BgL_arg1816z00_4412;
																												BgL_arg1815z00_4411 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* Cgen/emit_cop.scm 363 */
																													long
																														BgL_arg1817z00_4413;
																													BgL_arg1817z00_4413 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_4405);
																													BgL_arg1816z00_4412 =
																														(BgL_arg1817z00_4413
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_4410 =
																													VECTOR_REF
																													(BgL_arg1815z00_4411,
																													BgL_arg1816z00_4412);
																											}
																											{	/* Cgen/emit_cop.scm 363 */
																												bool_t
																													BgL__ortest_1115z00_4414;
																												BgL__ortest_1115z00_4414
																													=
																													(BgL_classz00_4404 ==
																													BgL_oclassz00_4410);
																												if (BgL__ortest_1115z00_4414)
																													{	/* Cgen/emit_cop.scm 363 */
																														BgL_res2354z00_4409
																															=
																															BgL__ortest_1115z00_4414;
																													}
																												else
																													{	/* Cgen/emit_cop.scm 363 */
																														long
																															BgL_odepthz00_4415;
																														{	/* Cgen/emit_cop.scm 363 */
																															obj_t
																																BgL_arg1804z00_4416;
																															BgL_arg1804z00_4416
																																=
																																(BgL_oclassz00_4410);
																															BgL_odepthz00_4415
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_4416);
																														}
																														if (
																															(2L <
																																BgL_odepthz00_4415))
																															{	/* Cgen/emit_cop.scm 363 */
																																obj_t
																																	BgL_arg1802z00_4417;
																																{	/* Cgen/emit_cop.scm 363 */
																																	obj_t
																																		BgL_arg1803z00_4418;
																																	BgL_arg1803z00_4418
																																		=
																																		(BgL_oclassz00_4410);
																																	BgL_arg1802z00_4417
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_4418,
																																		2L);
																																}
																																BgL_res2354z00_4409
																																	=
																																	(BgL_arg1802z00_4417
																																	==
																																	BgL_classz00_4404);
																															}
																														else
																															{	/* Cgen/emit_cop.scm 363 */
																																BgL_res2354z00_4409
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test2649z00_5493 =
																											BgL_res2354z00_4409;
																									}
																							}
																						else
																							{	/* Cgen/emit_cop.scm 363 */
																								BgL_test2649z00_5493 =
																									((bool_t) 0);
																							}
																					}
																				else
																					{	/* Cgen/emit_cop.scm 363 */
																						BgL_test2649z00_5493 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2649z00_5493)
																				{	/* Cgen/emit_cop.scm 363 */
																					BgL_test2648z00_5492 =
																						(
																						((obj_t)
																							(((BgL_varcz00_bglt) COBJECT(
																										((BgL_varcz00_bglt)
																											BgL_xz00_4387)))->
																								BgL_variablez00)) ==
																						((obj_t) (((BgL_varcz00_bglt)
																									COBJECT(((BgL_varcz00_bglt)
																											BgL_yz00_4388)))->
																								BgL_variablez00)));
																				}
																			else
																				{	/* Cgen/emit_cop.scm 363 */
																					BgL_test2648z00_5492 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test2648z00_5492)
																			{	/* Cgen/emit_cop.scm 463 */
																				BgL_test2646z00_5489 = ((bool_t) 0);
																			}
																		else
																			{	/* Cgen/emit_cop.scm 463 */
																				BgL_test2646z00_5489 = ((bool_t) 1);
																			}
																	}
																if (BgL_test2646z00_5489)
																	{	/* Cgen/emit_cop.scm 463 */
																		BGl_callzd2castze70z35zzcgen_emitzd2copzd2
																			(BgL_copz00_4454);
																		bgl_display_string
																			(BGl_string2439z00zzcgen_emitzd2copzd2,
																			BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																		BGl_emitzd2copzd2zzcgen_emitzd2copzd2(((
																					(BgL_cfuncallz00_bglt)
																					COBJECT(BgL_copz00_4454))->
																				BgL_funz00));
																		bgl_display_string
																			(BGl_string2440z00zzcgen_emitzd2copzd2,
																			BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																		BgL_copz00_4447 = BgL_copz00_4454;
																		{	/* Cgen/emit_cop.scm 430 */
																			obj_t BgL_actualsz00_4448;

																			BgL_actualsz00_4448 =
																				(((BgL_cfuncallz00_bglt)
																					COBJECT(BgL_copz00_4447))->
																				BgL_argsz00);
																			BGl_outzd2callze70z35zzcgen_emitzd2copzd2(
																				((BgL_cfuncallz00_bglt)
																					BgL_copz00_4173),
																				BGl_string2437z00zzcgen_emitzd2copzd2,
																				BGl_proc2434z00zzcgen_emitzd2copzd2,
																				BgL_actualsz00_4448, ((bool_t) 1));
																			{
																				obj_t BgL_actualsz00_4450;

																				BgL_actualsz00_4450 =
																					BgL_actualsz00_4448;
																			BgL_loopz00_4449:
																				if (NULLP(CDR(
																							((obj_t) BgL_actualsz00_4450))))
																					{	/* Cgen/emit_cop.scm 435 */
																						{	/* Cgen/emit_cop.scm 437 */
																							obj_t BgL_arg2001z00_4451;

																							BgL_arg2001z00_4451 =
																								CAR(
																								((obj_t) BgL_actualsz00_4450));
																							BGl_emitzd2copzd2zzcgen_emitzd2copzd2
																								(((BgL_copz00_bglt)
																									BgL_arg2001z00_4451));
																						}
																						bgl_display_string
																							(BGl_string2413z00zzcgen_emitzd2copzd2,
																							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																						((bool_t) 1);
																					}
																				else
																					{	/* Cgen/emit_cop.scm 435 */
																						{	/* Cgen/emit_cop.scm 441 */
																							obj_t BgL_arg2002z00_4452;

																							BgL_arg2002z00_4452 =
																								CAR(
																								((obj_t) BgL_actualsz00_4450));
																							BGl_emitzd2copzd2zzcgen_emitzd2copzd2
																								(((BgL_copz00_bglt)
																									BgL_arg2002z00_4452));
																						}
																						bgl_display_string
																							(BGl_string2411z00zzcgen_emitzd2copzd2,
																							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																						{	/* Cgen/emit_cop.scm 443 */
																							obj_t BgL_arg2003z00_4453;

																							BgL_arg2003z00_4453 =
																								CDR(
																								((obj_t) BgL_actualsz00_4450));
																							{
																								obj_t BgL_actualsz00_5573;

																								BgL_actualsz00_5573 =
																									BgL_arg2003z00_4453;
																								BgL_actualsz00_4450 =
																									BgL_actualsz00_5573;
																								goto BgL_loopz00_4449;
																							}
																						}
																					}
																			}
																		}
																		bgl_display_string
																			(BGl_string2424z00zzcgen_emitzd2copzd2,
																			BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																		BgL_copz00_4440 = BgL_copz00_4454;
																		{	/* Cgen/emit_cop.scm 415 */
																			obj_t BgL_actualsz00_4441;

																			BgL_actualsz00_4441 =
																				bgl_reverse_bang(CDR(bgl_reverse(
																						(((BgL_cfuncallz00_bglt)
																								COBJECT(BgL_copz00_4440))->
																							BgL_argsz00))));
																			BGl_outzd2callze70z35zzcgen_emitzd2copzd2(
																				((BgL_cfuncallz00_bglt)
																					BgL_copz00_4173),
																				BGl_string2437z00zzcgen_emitzd2copzd2,
																				BGl_proc2433z00zzcgen_emitzd2copzd2,
																				BgL_actualsz00_4441, ((bool_t) 0));
																			if (NULLP(BgL_actualsz00_4441))
																				{	/* Cgen/emit_cop.scm 417 */
																					bgl_display_string
																						(BGl_string2438z00zzcgen_emitzd2copzd2,
																						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																				}
																			else
																				{
																					obj_t BgL_actualsz00_4443;

																					{	/* Cgen/emit_cop.scm 419 */
																						bool_t BgL_tmpz00_5584;

																						BgL_actualsz00_4443 =
																							BgL_actualsz00_4441;
																					BgL_loopz00_4442:
																						if (NULLP(CDR(
																									((obj_t)
																										BgL_actualsz00_4443))))
																							{	/* Cgen/emit_cop.scm 420 */
																								{	/* Cgen/emit_cop.scm 422 */
																									obj_t BgL_arg1991z00_4444;

																									BgL_arg1991z00_4444 =
																										CAR(
																										((obj_t)
																											BgL_actualsz00_4443));
																									BGl_emitzd2copzd2zzcgen_emitzd2copzd2
																										(((BgL_copz00_bglt)
																											BgL_arg1991z00_4444));
																								}
																								bgl_display_string
																									(BGl_string2413z00zzcgen_emitzd2copzd2,
																									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																								BgL_tmpz00_5584 = ((bool_t) 1);
																							}
																						else
																							{	/* Cgen/emit_cop.scm 420 */
																								{	/* Cgen/emit_cop.scm 426 */
																									obj_t BgL_arg1992z00_4445;

																									BgL_arg1992z00_4445 =
																										CAR(
																										((obj_t)
																											BgL_actualsz00_4443));
																									BGl_emitzd2copzd2zzcgen_emitzd2copzd2
																										(((BgL_copz00_bglt)
																											BgL_arg1992z00_4445));
																								}
																								bgl_display_string
																									(BGl_string2411z00zzcgen_emitzd2copzd2,
																									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																								{	/* Cgen/emit_cop.scm 428 */
																									obj_t BgL_arg1993z00_4446;

																									BgL_arg1993z00_4446 =
																										CDR(
																										((obj_t)
																											BgL_actualsz00_4443));
																									{
																										obj_t BgL_actualsz00_5601;

																										BgL_actualsz00_5601 =
																											BgL_arg1993z00_4446;
																										BgL_actualsz00_4443 =
																											BgL_actualsz00_5601;
																										goto BgL_loopz00_4442;
																									}
																								}
																							}
																						BBOOL(BgL_tmpz00_5584);
																					}
																				}
																		}
																		bgl_display_string
																			(BGl_string2441z00zzcgen_emitzd2copzd2,
																			BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																		BgL_tmpz00_5426 = ((bool_t) 1);
																	}
																else
																	{	/* Cgen/emit_cop.scm 463 */
																		BGl_callzd2castze70z35zzcgen_emitzd2copzd2
																			(BgL_copz00_4454);
																		bgl_display_string
																			(BGl_string2442z00zzcgen_emitzd2copzd2,
																			BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																		{	/* Cgen/emit_cop.scm 477 */
																			long BgL_arg2018z00_4458;

																			BgL_arg2018z00_4458 =
																				(BgL_lenz00_4457 - 2L);
																			bgl_display_obj(BINT(BgL_arg2018z00_4458),
																				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																		}
																		bgl_display_string
																			(BGl_string2443z00zzcgen_emitzd2copzd2,
																			BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																		BGl_emitzd2copzd2zzcgen_emitzd2copzd2
																			(BgL_funz00_4456);
																		{	/* Cgen/emit_cop.scm 482 */
																			obj_t BgL_g1171z00_4459;

																			BgL_g1171z00_4459 =
																				CDR(((obj_t) BgL_actualsz00_4455));
																			{
																				obj_t BgL_argsz00_4461;

																				BgL_argsz00_4461 = BgL_g1171z00_4459;
																			BgL_loopz00_4460:
																				if (NULLP(CDR(
																							((obj_t) BgL_argsz00_4461))))
																					{	/* Cgen/emit_cop.scm 483 */
																						bgl_display_string
																							(BGl_string2413z00zzcgen_emitzd2copzd2,
																							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																						BgL_tmpz00_5426 = ((bool_t) 1);
																					}
																				else
																					{	/* Cgen/emit_cop.scm 483 */
																						bgl_display_string
																							(BGl_string2411z00zzcgen_emitzd2copzd2,
																							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
																						{	/* Cgen/emit_cop.scm 489 */
																							obj_t BgL_arg2022z00_4462;

																							BgL_arg2022z00_4462 =
																								CAR(((obj_t) BgL_argsz00_4461));
																							BGl_emitzd2copzd2zzcgen_emitzd2copzd2
																								(((BgL_copz00_bglt)
																									BgL_arg2022z00_4462));
																						}
																						{	/* Cgen/emit_cop.scm 490 */
																							obj_t BgL_arg2024z00_4463;

																							BgL_arg2024z00_4463 =
																								CDR(((obj_t) BgL_argsz00_4461));
																							{
																								obj_t BgL_argsz00_5625;

																								BgL_argsz00_5625 =
																									BgL_arg2024z00_4463;
																								BgL_argsz00_4461 =
																									BgL_argsz00_5625;
																								goto BgL_loopz00_4460;
																							}
																						}
																					}
																			}
																		}
																	}
															}
														}
													}
												}
											}
										}
								}
						}
					}
				}
				return BBOOL(BgL_tmpz00_5426);
			}
		}

	}



/* call-cast~0 */
	obj_t BGl_callzd2castze70z35zzcgen_emitzd2copzd2(BgL_cfuncallz00_bglt
		BgL_copz00_2489)
	{
		{	/* Cgen/emit_cop.scm 360 */
			{	/* Cgen/emit_cop.scm 356 */
				bool_t BgL_test2662z00_5628;

				{	/* Cgen/emit_cop.scm 356 */
					bool_t BgL_test2663z00_5629;

					{	/* Cgen/emit_cop.scm 356 */
						BgL_typez00_bglt BgL_arg2093z00_2502;

						BgL_arg2093z00_2502 =
							(((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_copz00_2489)))->BgL_typez00);
						BgL_test2663z00_5629 =
							(
							((obj_t) BgL_arg2093z00_2502) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2663z00_5629)
						{	/* Cgen/emit_cop.scm 356 */
							BgL_test2662z00_5628 = ((bool_t) 1);
						}
					else
						{	/* Cgen/emit_cop.scm 356 */
							BgL_test2662z00_5628 =
								BGl_equalzf3zf3zz__r4_equivalence_6_2z00
								(BGl_string2444z00zzcgen_emitzd2copzd2,
								(((BgL_typez00_bglt) COBJECT((((BgL_copz00_bglt)
													COBJECT(((BgL_copz00_bglt) BgL_copz00_2489)))->
												BgL_typez00)))->BgL_namez00));
						}
				}
				if (BgL_test2662z00_5628)
					{	/* Cgen/emit_cop.scm 356 */
						return BFALSE;
					}
				else
					{	/* Cgen/emit_cop.scm 356 */
						bgl_display_string(BGl_string2443z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						{	/* Cgen/emit_cop.scm 359 */
							obj_t BgL_arg2088z00_2497;

							BgL_arg2088z00_2497 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_copz00_2489)))->
											BgL_typez00)))->BgL_namez00);
							bgl_display_obj(BgL_arg2088z00_2497,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
						return
							bgl_display_string(BGl_string2413z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
			}
		}

	}



/* out-call~0 */
	obj_t BGl_outzd2callze70z35zzcgen_emitzd2copzd2(BgL_cfuncallz00_bglt
		BgL_copz00_4213, obj_t BgL_opz00_2450, obj_t BgL_castz00_2451,
		obj_t BgL_actualsz00_2452, bool_t BgL_eoazf3zf3_2453)
	{
		{	/* Cgen/emit_cop.scm 353 */
			{	/* Cgen/emit_cop.scm 332 */
				bool_t BgL_test2664z00_5644;

				{	/* Cgen/emit_cop.scm 332 */
					bool_t BgL_test2665z00_5645;

					{	/* Cgen/emit_cop.scm 332 */
						BgL_typez00_bglt BgL_arg2079z00_2488;

						BgL_arg2079z00_2488 =
							(((BgL_copz00_bglt) COBJECT(
									((BgL_copz00_bglt) BgL_copz00_4213)))->BgL_typez00);
						BgL_test2665z00_5645 =
							(
							((obj_t) BgL_arg2079z00_2488) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2665z00_5645)
						{	/* Cgen/emit_cop.scm 332 */
							BgL_test2664z00_5644 = ((bool_t) 1);
						}
					else
						{	/* Cgen/emit_cop.scm 332 */
							BgL_test2664z00_5644 =
								BGl_equalzf3zf3zz__r4_equivalence_6_2z00
								(BGl_string2444z00zzcgen_emitzd2copzd2,
								(((BgL_typez00_bglt) COBJECT((((BgL_copz00_bglt)
													COBJECT(((BgL_copz00_bglt) BgL_copz00_4213)))->
												BgL_typez00)))->BgL_namez00));
						}
				}
				if (BgL_test2664z00_5644)
					{	/* Cgen/emit_cop.scm 332 */
						bgl_display_string(BGl_string2443z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						{	/* Cgen/emit_cop.scm 336 */
							obj_t BgL_arg2059z00_2461;

							BgL_arg2059z00_2461 =
								((obj_t(*)(obj_t,
										obj_t))
								PROCEDURE_L_ENTRY(BgL_castz00_2451)) (BgL_castz00_2451,
								BgL_actualsz00_2452);
							bgl_display_obj(BgL_arg2059z00_2461,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
						bgl_display_obj(BgL_opz00_2450,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						bgl_display_string(BGl_string2443z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cfuncallz00_bglt)
									COBJECT(BgL_copz00_4213))->BgL_funz00));
						return bgl_display_string(BGl_string2445z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* Cgen/emit_cop.scm 332 */
						bgl_display_string(BGl_string2412z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						{	/* Cgen/emit_cop.scm 343 */
							obj_t BgL_arg2061z00_2463;

							BgL_arg2061z00_2463 =
								(((BgL_typez00_bglt) COBJECT(
										(((BgL_copz00_bglt) COBJECT(
													((BgL_copz00_bglt) BgL_copz00_4213)))->
											BgL_typez00)))->BgL_namez00);
							bgl_display_obj(BgL_arg2061z00_2463,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
						{	/* Cgen/emit_cop.scm 345 */
							obj_t BgL_arg2063z00_2465;

							{	/* Cgen/emit_cop.scm 345 */
								obj_t BgL_arg2064z00_2466;

								{	/* Cgen/emit_cop.scm 345 */
									obj_t BgL_l1368z00_2468;

									if (BgL_eoazf3zf3_2453)
										{	/* Cgen/emit_cop.scm 346 */
											BgL_l1368z00_2468 = BgL_actualsz00_2452;
										}
									else
										{	/* Cgen/emit_cop.scm 346 */
											BgL_l1368z00_2468 =
												bgl_reverse_bang(CDR(bgl_reverse(BgL_actualsz00_2452)));
										}
									if (NULLP(BgL_l1368z00_2468))
										{	/* Cgen/emit_cop.scm 345 */
											BgL_arg2064z00_2466 = BNIL;
										}
									else
										{	/* Cgen/emit_cop.scm 345 */
											obj_t BgL_head1370z00_2470;

											BgL_head1370z00_2470 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1368z00_2472;
												obj_t BgL_tail1371z00_2473;

												BgL_l1368z00_2472 = BgL_l1368z00_2468;
												BgL_tail1371z00_2473 = BgL_head1370z00_2470;
											BgL_zc3z04anonymousza32067ze3z87_2474:
												if (NULLP(BgL_l1368z00_2472))
													{	/* Cgen/emit_cop.scm 345 */
														BgL_arg2064z00_2466 = CDR(BgL_head1370z00_2470);
													}
												else
													{	/* Cgen/emit_cop.scm 345 */
														obj_t BgL_newtail1372z00_2476;

														{	/* Cgen/emit_cop.scm 345 */
															obj_t BgL_arg2070z00_2478;

															BgL_arg2070z00_2478 =
																(((BgL_typez00_bglt) COBJECT(
																		(((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						CAR(
																							((obj_t) BgL_l1368z00_2472)))))->
																			BgL_typez00)))->BgL_namez00);
															BgL_newtail1372z00_2476 =
																MAKE_YOUNG_PAIR(BgL_arg2070z00_2478, BNIL);
														}
														SET_CDR(BgL_tail1371z00_2473,
															BgL_newtail1372z00_2476);
														{	/* Cgen/emit_cop.scm 345 */
															obj_t BgL_arg2069z00_2477;

															BgL_arg2069z00_2477 =
																CDR(((obj_t) BgL_l1368z00_2472));
															{
																obj_t BgL_tail1371z00_5690;
																obj_t BgL_l1368z00_5689;

																BgL_l1368z00_5689 = BgL_arg2069z00_2477;
																BgL_tail1371z00_5690 = BgL_newtail1372z00_2476;
																BgL_tail1371z00_2473 = BgL_tail1371z00_5690;
																BgL_l1368z00_2472 = BgL_l1368z00_5689;
																goto BgL_zc3z04anonymousza32067ze3z87_2474;
															}
														}
													}
											}
										}
								}
								{	/* Cgen/emit_cop.scm 344 */
									obj_t BgL_list2065z00_2467;

									BgL_list2065z00_2467 =
										MAKE_YOUNG_PAIR(BgL_arg2064z00_2466, BNIL);
									BgL_arg2063z00_2465 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string2446z00zzcgen_emitzd2copzd2,
										BgL_list2065z00_2467);
								}
							}
							bgl_display_obj(BgL_arg2063z00_2465,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
						bgl_display_obj(BgL_opz00_2450,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						bgl_display_string(BGl_string2443z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cfuncallz00_bglt)
									COBJECT(BgL_copz00_4213))->BgL_funz00));
						return bgl_display_string(BGl_string2445z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
			}
		}

	}



/* &fun-cast-va-strict */
	obj_t BGl_z62funzd2castzd2vazd2strictzb0zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4174, obj_t BgL_actualsz00_4175)
	{
		{	/* Cgen/emit_cop.scm 324 */
			return BGl_string2447z00zzcgen_emitzd2copzd2;
		}

	}



/* &fun-cast */
	obj_t BGl_z62funzd2castzb0zzcgen_emitzd2copzd2(obj_t BgL_envz00_4176,
		obj_t BgL_actualsz00_4177)
	{
		{	/* Cgen/emit_cop.scm 321 */
			{	/* Cgen/emit_cop.scm 311 */
				long BgL_largsz00_4466;

				BgL_largsz00_4466 = bgl_list_length(BgL_actualsz00_4177);
				{	/* Cgen/emit_cop.scm 313 */
					bool_t BgL_test2669z00_5700;

					{	/* Cgen/emit_cop.scm 313 */
						long BgL_arg2037z00_4467;

						BgL_arg2037z00_4467 =
							VECTOR_LENGTH(BGl_cfuncallzd2castszd2zzcgen_emitzd2copzd2);
						BgL_test2669z00_5700 = (BgL_largsz00_4466 >= BgL_arg2037z00_4467);
					}
					if (BgL_test2669z00_5700)
						{	/* Cgen/emit_cop.scm 314 */
							obj_t BgL_arg2031z00_4468;

							{	/* Cgen/emit_cop.scm 314 */
								obj_t BgL_list2033z00_4469;

								BgL_list2033z00_4469 =
									MAKE_YOUNG_PAIR(BGl_string2444z00zzcgen_emitzd2copzd2, BNIL);
								BgL_arg2031z00_4468 =
									BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00(
									(int) (BgL_largsz00_4466), BgL_list2033z00_4469);
							}
							{	/* Cgen/emit_cop.scm 314 */
								obj_t BgL_list2032z00_4470;

								BgL_list2032z00_4470 =
									MAKE_YOUNG_PAIR(BgL_arg2031z00_4468, BNIL);
								return
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string2448z00zzcgen_emitzd2copzd2, BgL_list2032z00_4470);
							}
						}
					else
						{	/* Cgen/emit_cop.scm 315 */
							obj_t BgL_g1167z00_4471;

							BgL_g1167z00_4471 =
								VECTOR_REF(BGl_cfuncallzd2castszd2zzcgen_emitzd2copzd2,
								BgL_largsz00_4466);
							if (CBOOL(BgL_g1167z00_4471))
								{	/* Cgen/emit_cop.scm 315 */
									return BgL_g1167z00_4471;
								}
							else
								{	/* Cgen/emit_cop.scm 319 */
									obj_t BgL_pz00_4472;

									{	/* Cgen/emit_cop.scm 319 */
										obj_t BgL_arg2034z00_4473;

										{	/* Cgen/emit_cop.scm 319 */
											obj_t BgL_list2036z00_4474;

											BgL_list2036z00_4474 =
												MAKE_YOUNG_PAIR(BGl_string2444z00zzcgen_emitzd2copzd2,
												BNIL);
											BgL_arg2034z00_4473 =
												BGl_makezd2listzd2zz__r4_pairs_and_lists_6_3z00((int)
												(BgL_largsz00_4466), BgL_list2036z00_4474);
										}
										{	/* Cgen/emit_cop.scm 319 */
											obj_t BgL_list2035z00_4475;

											BgL_list2035z00_4475 =
												MAKE_YOUNG_PAIR(BgL_arg2034z00_4473, BNIL);
											BgL_pz00_4472 =
												BGl_formatz00zz__r4_output_6_10_3z00
												(BGl_string2448z00zzcgen_emitzd2copzd2,
												BgL_list2035z00_4475);
									}}
									VECTOR_SET(BGl_cfuncallzd2castszd2zzcgen_emitzd2copzd2,
										BgL_largsz00_4466, BgL_pz00_4472);
									return BgL_pz00_4472;
								}
						}
				}
			}
		}

	}



/* &fun-l-cast */
	obj_t BGl_z62funzd2lzd2castz62zzcgen_emitzd2copzd2(obj_t BgL_envz00_4178,
		obj_t BgL_actualsz00_4179)
	{
		{	/* Cgen/emit_cop.scm 329 */
			{	/* Cgen/emit_cop.scm 328 */
				obj_t BgL_arg2040z00_4476;

				{	/* Cgen/emit_cop.scm 328 */
					obj_t BgL_l1363z00_4477;

					BgL_l1363z00_4477 =
						bgl_reverse_bang(CDR(bgl_reverse(BgL_actualsz00_4179)));
					if (NULLP(BgL_l1363z00_4477))
						{	/* Cgen/emit_cop.scm 328 */
							BgL_arg2040z00_4476 = BNIL;
						}
					else
						{	/* Cgen/emit_cop.scm 328 */
							obj_t BgL_head1365z00_4478;

							BgL_head1365z00_4478 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1363z00_4480;
								obj_t BgL_tail1366z00_4481;

								BgL_l1363z00_4480 = BgL_l1363z00_4477;
								BgL_tail1366z00_4481 = BgL_head1365z00_4478;
							BgL_zc3z04anonymousza32043ze3z87_4479:
								if (NULLP(BgL_l1363z00_4480))
									{	/* Cgen/emit_cop.scm 328 */
										BgL_arg2040z00_4476 = CDR(BgL_head1365z00_4478);
									}
								else
									{	/* Cgen/emit_cop.scm 328 */
										obj_t BgL_newtail1367z00_4482;

										{	/* Cgen/emit_cop.scm 328 */
											obj_t BgL_arg2046z00_4483;

											BgL_arg2046z00_4483 =
												(((BgL_typez00_bglt) COBJECT(
														(((BgL_copz00_bglt) COBJECT(
																	((BgL_copz00_bglt)
																		CAR(
																			((obj_t) BgL_l1363z00_4480)))))->
															BgL_typez00)))->BgL_namez00);
											BgL_newtail1367z00_4482 =
												MAKE_YOUNG_PAIR(BgL_arg2046z00_4483, BNIL);
										}
										SET_CDR(BgL_tail1366z00_4481, BgL_newtail1367z00_4482);
										{	/* Cgen/emit_cop.scm 328 */
											obj_t BgL_arg2045z00_4484;

											BgL_arg2045z00_4484 = CDR(((obj_t) BgL_l1363z00_4480));
											{
												obj_t BgL_tail1366z00_5736;
												obj_t BgL_l1363z00_5735;

												BgL_l1363z00_5735 = BgL_arg2045z00_4484;
												BgL_tail1366z00_5736 = BgL_newtail1367z00_4482;
												BgL_tail1366z00_4481 = BgL_tail1366z00_5736;
												BgL_l1363z00_4480 = BgL_l1363z00_5735;
												goto BgL_zc3z04anonymousza32043ze3z87_4479;
											}
										}
									}
							}
						}
				}
				{	/* Cgen/emit_cop.scm 327 */
					obj_t BgL_list2041z00_4485;

					BgL_list2041z00_4485 = MAKE_YOUNG_PAIR(BgL_arg2040z00_4476, BNIL);
					return
						BGl_formatz00zz__r4_output_6_10_3z00
						(BGl_string2448z00zzcgen_emitzd2copzd2, BgL_list2041z00_4485);
				}
			}
		}

	}



/* &emit-cop-bdb-block1420 */
	obj_t BGl_z62emitzd2copzd2bdbzd2block1420zb0zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4180, obj_t BgL_copz00_4181)
	{
		{	/* Cgen/emit_cop.scm 291 */
			{	/* Cgen/emit_cop.scm 293 */
				obj_t BgL_arg1960z00_4487;

				BgL_arg1960z00_4487 =
					(((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt)
								((BgL_bdbzd2blockzd2_bglt) BgL_copz00_4181))))->BgL_locz00);
				BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1960z00_4487);
			}
			{	/* Cgen/emit_cop.scm 294 */
				obj_t BgL_port1166z00_4488;

				BgL_port1166z00_4488 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
				{	/* Cgen/emit_cop.scm 294 */
					obj_t BgL_tmpz00_5743;

					BgL_tmpz00_5743 = ((obj_t) BgL_port1166z00_4488);
					bgl_display_string(BGl_string2449z00zzcgen_emitzd2copzd2,
						BgL_tmpz00_5743);
				}
			}
			BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
				(((BgL_bdbzd2blockzd2_bglt) COBJECT(
							((BgL_bdbzd2blockzd2_bglt) BgL_copz00_4181)))->BgL_bodyz00));
			bgl_display_string(BGl_string2422z00zzcgen_emitzd2copzd2,
				BGl_za2czd2portza2zd2zzbackend_c_emitz00);
			return BNIL;
		}

	}



/* &emit-cop-local-var1418 */
	obj_t BGl_z62emitzd2copzd2localzd2var1418zb0zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4182, obj_t BgL_copz00_4183)
	{
		{	/* Cgen/emit_cop.scm 269 */
			{	/* Cgen/emit_cop.scm 270 */
				bool_t BgL_tmpz00_5750;

				{	/* Cgen/emit_cop.scm 271 */
					obj_t BgL_arg1942z00_4490;

					BgL_arg1942z00_4490 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_localzd2varzd2_bglt) BgL_copz00_4183))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1942z00_4490);
				}
				{	/* Cgen/emit_cop.scm 272 */
					obj_t BgL_g1362z00_4491;

					BgL_g1362z00_4491 =
						(((BgL_localzd2varzd2_bglt) COBJECT(
								((BgL_localzd2varzd2_bglt) BgL_copz00_4183)))->BgL_varsz00);
					{
						obj_t BgL_l1360z00_4493;

						BgL_l1360z00_4493 = BgL_g1362z00_4491;
					BgL_zc3z04anonymousza31943ze3z87_4492:
						if (PAIRP(BgL_l1360z00_4493))
							{	/* Cgen/emit_cop.scm 272 */
								{	/* Cgen/emit_cop.scm 273 */
									obj_t BgL_localz00_4494;

									BgL_localz00_4494 = CAR(BgL_l1360z00_4493);
									{	/* Cgen/emit_cop.scm 275 */
										obj_t BgL_port1164z00_4495;

										BgL_port1164z00_4495 =
											BGl_za2czd2portza2zd2zzbackend_c_emitz00;
										{	/* Cgen/emit_cop.scm 276 */
											obj_t BgL_arg1945z00_4496;

											if (
												(((BgL_localz00_bglt) COBJECT(
															((BgL_localz00_bglt) BgL_localz00_4494)))->
													BgL_volatilez00))
												{	/* Cgen/emit_cop.scm 276 */
													BgL_arg1945z00_4496 =
														BGl_string2450z00zzcgen_emitzd2copzd2;
												}
											else
												{	/* Cgen/emit_cop.scm 276 */
													BgL_arg1945z00_4496 =
														BGl_string2368z00zzcgen_emitzd2copzd2;
												}
											bgl_display_obj(BgL_arg1945z00_4496,
												BgL_port1164z00_4495);
										}
										{	/* Cgen/emit_cop.scm 277 */
											obj_t BgL_arg1947z00_4497;

											{	/* Cgen/emit_cop.scm 277 */
												BgL_typez00_bglt BgL_arg1948z00_4498;
												obj_t BgL_arg1949z00_4499;

												BgL_arg1948z00_4498 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_localz00_4494))))->
													BgL_typez00);
												BgL_arg1949z00_4499 =
													(((BgL_variablez00_bglt)
														COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
																	BgL_localz00_4494))))->BgL_namez00);
												BgL_arg1947z00_4497 =
													BGl_makezd2typedzd2declarationz00zztype_toolsz00
													(BgL_arg1948z00_4498, BgL_arg1949z00_4499);
											}
											bgl_display_obj(BgL_arg1947z00_4497,
												BgL_port1164z00_4495);
										}
										{	/* Cgen/emit_cop.scm 278 */
											obj_t BgL_arg1950z00_4500;

											{	/* Cgen/emit_cop.scm 278 */
												bool_t BgL_test2675z00_5772;

												if (
													((long)
														CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >
														0L))
													{	/* Cgen/emit_cop.scm 278 */
														BgL_test2675z00_5772 =
															(
															(((BgL_typez00_bglt) COBJECT(
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_localz00_4494))))->
																			BgL_typez00)))->BgL_classz00) ==
															CNST_TABLE_REF(7));
													}
												else
													{	/* Cgen/emit_cop.scm 278 */
														BgL_test2675z00_5772 = ((bool_t) 0);
													}
												if (BgL_test2675z00_5772)
													{	/* Cgen/emit_cop.scm 281 */
														obj_t BgL_arg1955z00_4501;

														{	/* Cgen/emit_cop.scm 281 */
															BgL_typez00_bglt BgL_arg1956z00_4502;

															BgL_arg1956z00_4502 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				BgL_localz00_4494))))->BgL_typez00);
															BgL_arg1955z00_4501 =
																BGl_makezd2typedzd2declarationz00zztype_toolsz00
																(BgL_arg1956z00_4502,
																BGl_string2451z00zzcgen_emitzd2copzd2);
														}
														BgL_arg1950z00_4500 =
															string_append_3
															(BGl_string2452z00zzcgen_emitzd2copzd2,
															BgL_arg1955z00_4501,
															BGl_string2453z00zzcgen_emitzd2copzd2);
													}
												else
													{	/* Cgen/emit_cop.scm 278 */
														BgL_arg1950z00_4500 =
															BGl_string2451z00zzcgen_emitzd2copzd2;
													}
											}
											bgl_display_obj(BgL_arg1950z00_4500,
												BgL_port1164z00_4495);
										}
										{	/* Cgen/emit_cop.scm 275 */
											obj_t BgL_tmpz00_5788;

											BgL_tmpz00_5788 = ((obj_t) BgL_port1164z00_4495);
											bgl_display_char(((unsigned char) ';'), BgL_tmpz00_5788);
								}}}
								{
									obj_t BgL_l1360z00_5791;

									BgL_l1360z00_5791 = CDR(BgL_l1360z00_4493);
									BgL_l1360z00_4493 = BgL_l1360z00_5791;
									goto BgL_zc3z04anonymousza31943ze3z87_4492;
								}
							}
						else
							{	/* Cgen/emit_cop.scm 272 */
								((bool_t) 1);
							}
					}
				}
				BgL_tmpz00_5750 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_5750);
			}
		}

	}



/* &emit-cop-cif1416 */
	obj_t BGl_z62emitzd2copzd2cif1416z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4184, obj_t BgL_copz00_4185)
	{
		{	/* Cgen/emit_cop.scm 256 */
			{	/* Cgen/emit_cop.scm 257 */
				bool_t BgL_tmpz00_5794;

				{	/* Cgen/emit_cop.scm 258 */
					obj_t BgL_arg1938z00_4504;

					BgL_arg1938z00_4504 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cifz00_bglt) BgL_copz00_4185))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1938z00_4504);
				}
				bgl_display_string(BGl_string2454z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cifz00_bglt)
							COBJECT(((BgL_cifz00_bglt) BgL_copz00_4185)))->BgL_testz00));
				bgl_display_char(((unsigned char) ')'),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cifz00_bglt)
							COBJECT(((BgL_cifz00_bglt) BgL_copz00_4185)))->BgL_truez00));
				bgl_display_string(BGl_string2455z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BgL_tmpz00_5794 =
					BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_cifz00_bglt)
							COBJECT(((BgL_cifz00_bglt) BgL_copz00_4185)))->BgL_falsez00));
				return BBOOL(BgL_tmpz00_5794);
			}
		}

	}



/* &emit-cop-csetq1414 */
	obj_t BGl_z62emitzd2copzd2csetq1414z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4186, obj_t BgL_copz00_4187)
	{
		{	/* Cgen/emit_cop.scm 245 */
			{	/* Cgen/emit_cop.scm 246 */
				bool_t BgL_tmpz00_5812;

				{	/* Cgen/emit_cop.scm 247 */
					obj_t BgL_arg1935z00_4506;

					BgL_arg1935z00_4506 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_csetqz00_bglt) BgL_copz00_4187))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1935z00_4506);
				}
				{	/* Cgen/emit_cop.scm 248 */
					BgL_varcz00_bglt BgL_arg1936z00_4507;

					BgL_arg1936z00_4507 =
						(((BgL_csetqz00_bglt) COBJECT(
								((BgL_csetqz00_bglt) BgL_copz00_4187)))->BgL_varz00);
					BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
						((BgL_copz00_bglt) BgL_arg1936z00_4507));
				}
				bgl_display_string(BGl_string2456z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_csetqz00_bglt)
							COBJECT(((BgL_csetqz00_bglt) BgL_copz00_4187)))->BgL_valuez00));
				BgL_tmpz00_5812 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_5812);
			}
		}

	}



/* &emit-cop-stop1412 */
	obj_t BGl_z62emitzd2copzd2stop1412z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4188, obj_t BgL_copz00_4189)
	{
		{	/* Cgen/emit_cop.scm 232 */
			{	/* Cgen/emit_cop.scm 233 */
				bool_t BgL_tmpz00_5826;

				if (BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
						(((BgL_stopz00_bglt) COBJECT(
									((BgL_stopz00_bglt) BgL_copz00_4189)))->BgL_valuez00)))
					{	/* Cgen/emit_cop.scm 234 */
						bgl_display_string(BGl_string2421z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						BNIL;
					}
				else
					{	/* Cgen/emit_cop.scm 234 */
						BFALSE;
					}
				BgL_tmpz00_5826 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_5826);
			}
		}

	}



/* &emit-cop-nop1408 */
	obj_t BGl_z62emitzd2copzd2nop1408z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4190, obj_t BgL_copz00_4191)
	{
		{	/* Cgen/emit_cop.scm 223 */
			{	/* Cgen/emit_cop.scm 225 */
				bool_t BgL_tmpz00_5833;

				bgl_display_string(BGl_string2421z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BgL_tmpz00_5833 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_5833);
			}
		}

	}



/* &emit-cop-csequence1406 */
	obj_t BGl_z62emitzd2copzd2csequence1406z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4192, obj_t BgL_copz00_4193)
	{
		{	/* Cgen/emit_cop.scm 180 */
			if (
				(((BgL_csequencez00_bglt) COBJECT(
							((BgL_csequencez00_bglt) BgL_copz00_4193)))->BgL_czd2expzf3z21))
				{	/* Cgen/emit_cop.scm 182 */
					if (NULLP(
							(((BgL_csequencez00_bglt) COBJECT(
										((BgL_csequencez00_bglt) BgL_copz00_4193)))->BgL_copsz00)))
						{	/* Cgen/emit_cop.scm 184 */
							return
								BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00(BUNSPEC,
								((BgL_typez00_bglt) BGl_za2unspecza2z00zztype_cachez00));
						}
					else
						{	/* Cgen/emit_cop.scm 184 */
							bgl_display_string(BGl_string2457z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							{
								obj_t BgL_expz00_4512;

								{	/* Cgen/emit_cop.scm 189 */
									obj_t BgL_arg1912z00_4532;

									BgL_arg1912z00_4532 =
										(((BgL_csequencez00_bglt) COBJECT(
												((BgL_csequencez00_bglt) BgL_copz00_4193)))->
										BgL_copsz00);
									{	/* Cgen/emit_cop.scm 189 */
										bool_t BgL_tmpz00_5848;

										BgL_expz00_4512 = BgL_arg1912z00_4532;
									BgL_liipz00_4511:
										if (NULLP(CDR(((obj_t) BgL_expz00_4512))))
											{	/* Cgen/emit_cop.scm 190 */
												{	/* Cgen/emit_cop.scm 192 */
													obj_t BgL_arg1916z00_4513;

													BgL_arg1916z00_4513 = CAR(((obj_t) BgL_expz00_4512));
													BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
														((BgL_copz00_bglt) BgL_arg1916z00_4513));
												}
												bgl_display_string
													(BGl_string2458z00zzcgen_emitzd2copzd2,
													BGl_za2czd2portza2zd2zzbackend_c_emitz00);
												BgL_tmpz00_5848 = ((bool_t) 1);
											}
										else
											{	/* Cgen/emit_cop.scm 190 */
												{	/* Cgen/emit_cop.scm 197 */
													obj_t BgL_arg1917z00_4514;

													BgL_arg1917z00_4514 = CAR(((obj_t) BgL_expz00_4512));
													BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
														((BgL_copz00_bglt) BgL_arg1917z00_4514));
												}
												{	/* Cgen/emit_cop.scm 198 */
													bool_t BgL_test2681z00_5862;

													{	/* Cgen/emit_cop.scm 198 */
														obj_t BgL_arg1923z00_4515;

														BgL_arg1923z00_4515 =
															CAR(((obj_t) BgL_expz00_4512));
														{	/* Cgen/emit_cop.scm 198 */
															obj_t BgL_classz00_4516;

															BgL_classz00_4516 = BGl_cfailz00zzcgen_copz00;
															if (BGL_OBJECTP(BgL_arg1923z00_4515))
																{	/* Cgen/emit_cop.scm 198 */
																	BgL_objectz00_bglt BgL_arg1807z00_4517;

																	BgL_arg1807z00_4517 =
																		(BgL_objectz00_bglt) (BgL_arg1923z00_4515);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Cgen/emit_cop.scm 198 */
																			long BgL_idxz00_4518;

																			BgL_idxz00_4518 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_4517);
																			BgL_test2681z00_5862 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_4518 + 2L)) ==
																				BgL_classz00_4516);
																		}
																	else
																		{	/* Cgen/emit_cop.scm 198 */
																			bool_t BgL_res2351z00_4521;

																			{	/* Cgen/emit_cop.scm 198 */
																				obj_t BgL_oclassz00_4522;

																				{	/* Cgen/emit_cop.scm 198 */
																					obj_t BgL_arg1815z00_4523;
																					long BgL_arg1816z00_4524;

																					BgL_arg1815z00_4523 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Cgen/emit_cop.scm 198 */
																						long BgL_arg1817z00_4525;

																						BgL_arg1817z00_4525 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_4517);
																						BgL_arg1816z00_4524 =
																							(BgL_arg1817z00_4525 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_4522 =
																						VECTOR_REF(BgL_arg1815z00_4523,
																						BgL_arg1816z00_4524);
																				}
																				{	/* Cgen/emit_cop.scm 198 */
																					bool_t BgL__ortest_1115z00_4526;

																					BgL__ortest_1115z00_4526 =
																						(BgL_classz00_4516 ==
																						BgL_oclassz00_4522);
																					if (BgL__ortest_1115z00_4526)
																						{	/* Cgen/emit_cop.scm 198 */
																							BgL_res2351z00_4521 =
																								BgL__ortest_1115z00_4526;
																						}
																					else
																						{	/* Cgen/emit_cop.scm 198 */
																							long BgL_odepthz00_4527;

																							{	/* Cgen/emit_cop.scm 198 */
																								obj_t BgL_arg1804z00_4528;

																								BgL_arg1804z00_4528 =
																									(BgL_oclassz00_4522);
																								BgL_odepthz00_4527 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_4528);
																							}
																							if ((2L < BgL_odepthz00_4527))
																								{	/* Cgen/emit_cop.scm 198 */
																									obj_t BgL_arg1802z00_4529;

																									{	/* Cgen/emit_cop.scm 198 */
																										obj_t BgL_arg1803z00_4530;

																										BgL_arg1803z00_4530 =
																											(BgL_oclassz00_4522);
																										BgL_arg1802z00_4529 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_4530, 2L);
																									}
																									BgL_res2351z00_4521 =
																										(BgL_arg1802z00_4529 ==
																										BgL_classz00_4516);
																								}
																							else
																								{	/* Cgen/emit_cop.scm 198 */
																									BgL_res2351z00_4521 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2681z00_5862 =
																				BgL_res2351z00_4521;
																		}
																}
															else
																{	/* Cgen/emit_cop.scm 198 */
																	BgL_test2681z00_5862 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2681z00_5862)
														{	/* Cgen/emit_cop.scm 198 */
															bgl_display_string
																(BGl_string2458z00zzcgen_emitzd2copzd2,
																BGl_za2czd2portza2zd2zzbackend_c_emitz00);
															BgL_tmpz00_5848 = ((bool_t) 1);
														}
													else
														{	/* Cgen/emit_cop.scm 198 */
															bgl_display_string
																(BGl_string2411z00zzcgen_emitzd2copzd2,
																BGl_za2czd2portza2zd2zzbackend_c_emitz00);
															{	/* Cgen/emit_cop.scm 206 */
																obj_t BgL_arg1920z00_4531;

																BgL_arg1920z00_4531 =
																	CDR(((obj_t) BgL_expz00_4512));
																{
																	obj_t BgL_expz00_5891;

																	BgL_expz00_5891 = BgL_arg1920z00_4531;
																	BgL_expz00_4512 = BgL_expz00_5891;
																	goto BgL_liipz00_4511;
																}
															}
														}
												}
											}
										return BBOOL(BgL_tmpz00_5848);
									}
								}
							}
						}
				}
			else
				{
					obj_t BgL_expz00_4534;

					{	/* Cgen/emit_cop.scm 207 */
						obj_t BgL_arg1926z00_4552;

						BgL_arg1926z00_4552 =
							(((BgL_csequencez00_bglt) COBJECT(
									((BgL_csequencez00_bglt) BgL_copz00_4193)))->BgL_copsz00);
						{	/* Cgen/emit_cop.scm 207 */
							bool_t BgL_tmpz00_5895;

							BgL_expz00_4534 = BgL_arg1926z00_4552;
						BgL_liipz00_4533:
							if (NULLP(BgL_expz00_4534))
								{	/* Cgen/emit_cop.scm 208 */
									BgL_tmpz00_5895 = ((bool_t) 0);
								}
							else
								{	/* Cgen/emit_cop.scm 210 */
									obj_t BgL_ez00_4535;

									BgL_ez00_4535 = CAR(((obj_t) BgL_expz00_4534));
									if (BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
											((BgL_copz00_bglt) BgL_ez00_4535)))
										{	/* Cgen/emit_cop.scm 211 */
											bgl_display_string(BGl_string2421z00zzcgen_emitzd2copzd2,
												BGl_za2czd2portza2zd2zzbackend_c_emitz00);
											BNIL;
										}
									else
										{	/* Cgen/emit_cop.scm 211 */
											BFALSE;
										}
									{	/* Cgen/emit_cop.scm 216 */
										bool_t BgL_test2688z00_5904;

										{	/* Cgen/emit_cop.scm 216 */
											obj_t BgL_classz00_4536;

											BgL_classz00_4536 = BGl_cfailz00zzcgen_copz00;
											if (BGL_OBJECTP(BgL_ez00_4535))
												{	/* Cgen/emit_cop.scm 216 */
													BgL_objectz00_bglt BgL_arg1807z00_4537;

													BgL_arg1807z00_4537 =
														(BgL_objectz00_bglt) (BgL_ez00_4535);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cgen/emit_cop.scm 216 */
															long BgL_idxz00_4538;

															BgL_idxz00_4538 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4537);
															BgL_test2688z00_5904 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4538 + 2L)) == BgL_classz00_4536);
														}
													else
														{	/* Cgen/emit_cop.scm 216 */
															bool_t BgL_res2352z00_4541;

															{	/* Cgen/emit_cop.scm 216 */
																obj_t BgL_oclassz00_4542;

																{	/* Cgen/emit_cop.scm 216 */
																	obj_t BgL_arg1815z00_4543;
																	long BgL_arg1816z00_4544;

																	BgL_arg1815z00_4543 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cgen/emit_cop.scm 216 */
																		long BgL_arg1817z00_4545;

																		BgL_arg1817z00_4545 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4537);
																		BgL_arg1816z00_4544 =
																			(BgL_arg1817z00_4545 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4542 =
																		VECTOR_REF(BgL_arg1815z00_4543,
																		BgL_arg1816z00_4544);
																}
																{	/* Cgen/emit_cop.scm 216 */
																	bool_t BgL__ortest_1115z00_4546;

																	BgL__ortest_1115z00_4546 =
																		(BgL_classz00_4536 == BgL_oclassz00_4542);
																	if (BgL__ortest_1115z00_4546)
																		{	/* Cgen/emit_cop.scm 216 */
																			BgL_res2352z00_4541 =
																				BgL__ortest_1115z00_4546;
																		}
																	else
																		{	/* Cgen/emit_cop.scm 216 */
																			long BgL_odepthz00_4547;

																			{	/* Cgen/emit_cop.scm 216 */
																				obj_t BgL_arg1804z00_4548;

																				BgL_arg1804z00_4548 =
																					(BgL_oclassz00_4542);
																				BgL_odepthz00_4547 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4548);
																			}
																			if ((2L < BgL_odepthz00_4547))
																				{	/* Cgen/emit_cop.scm 216 */
																					obj_t BgL_arg1802z00_4549;

																					{	/* Cgen/emit_cop.scm 216 */
																						obj_t BgL_arg1803z00_4550;

																						BgL_arg1803z00_4550 =
																							(BgL_oclassz00_4542);
																						BgL_arg1802z00_4549 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4550, 2L);
																					}
																					BgL_res2352z00_4541 =
																						(BgL_arg1802z00_4549 ==
																						BgL_classz00_4536);
																				}
																			else
																				{	/* Cgen/emit_cop.scm 216 */
																					BgL_res2352z00_4541 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2688z00_5904 = BgL_res2352z00_4541;
														}
												}
											else
												{	/* Cgen/emit_cop.scm 216 */
													BgL_test2688z00_5904 = ((bool_t) 0);
												}
										}
										if (BgL_test2688z00_5904)
											{
												obj_t BgL_expz00_5927;

												BgL_expz00_5927 = BNIL;
												BgL_expz00_4534 = BgL_expz00_5927;
												goto BgL_liipz00_4533;
											}
										else
											{	/* Cgen/emit_cop.scm 218 */
												obj_t BgL_arg1931z00_4551;

												BgL_arg1931z00_4551 = CDR(((obj_t) BgL_expz00_4534));
												{
													obj_t BgL_expz00_5930;

													BgL_expz00_5930 = BgL_arg1931z00_4551;
													BgL_expz00_4534 = BgL_expz00_5930;
													goto BgL_liipz00_4533;
												}
											}
									}
								}
							return BBOOL(BgL_tmpz00_5895);
						}
					}
				}
		}

	}



/* &emit-cop-ccast1403 */
	obj_t BGl_z62emitzd2copzd2ccast1403z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4194, obj_t BgL_copz00_4195)
	{
		{	/* Cgen/emit_cop.scm 167 */
			{	/* Cgen/emit_cop.scm 168 */
				bool_t BgL_tmpz00_5932;

				{	/* Cgen/emit_cop.scm 169 */
					obj_t BgL_arg1902z00_4554;

					BgL_arg1902z00_4554 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_ccastz00_bglt) BgL_copz00_4195))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1902z00_4554);
				}
				bgl_display_string(BGl_string2412z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				{	/* Cgen/emit_cop.scm 171 */
					obj_t BgL_arg1903z00_4555;

					BgL_arg1903z00_4555 =
						(((BgL_typez00_bglt) COBJECT(
								(((BgL_copz00_bglt) COBJECT(
											((BgL_copz00_bglt)
												((BgL_ccastz00_bglt) BgL_copz00_4195))))->
									BgL_typez00)))->BgL_namez00);
					bgl_display_obj(BgL_arg1903z00_4555,
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
				bgl_display_char(((unsigned char) ')'),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_ccastz00_bglt)
							COBJECT(((BgL_ccastz00_bglt) BgL_copz00_4195)))->BgL_argz00));
				bgl_display_char(((unsigned char) ')'),
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				BgL_tmpz00_5932 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_5932);
			}
		}

	}



/* &emit-cop-cpragma1401 */
	obj_t BGl_z62emitzd2copzd2cpragma1401z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4196, obj_t BgL_copz00_4197)
	{
		{	/* Cgen/emit_cop.scm 137 */
			{	/* Cgen/emit_cop.scm 139 */
				obj_t BgL_arg1661z00_4557;

				BgL_arg1661z00_4557 =
					(((BgL_copz00_bglt) COBJECT(
							((BgL_copz00_bglt)
								((BgL_cpragmaz00_bglt) BgL_copz00_4197))))->BgL_locz00);
				BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1661z00_4557);
			}
			if (NULLP(
					(((BgL_cpragmaz00_bglt) COBJECT(
								((BgL_cpragmaz00_bglt) BgL_copz00_4197)))->BgL_argsz00)))
				{	/* Cgen/emit_cop.scm 141 */
					obj_t BgL_arg1675z00_4558;

					BgL_arg1675z00_4558 =
						(((BgL_cpragmaz00_bglt) COBJECT(
								((BgL_cpragmaz00_bglt) BgL_copz00_4197)))->BgL_formatz00);
					return
						bgl_display_obj(BgL_arg1675z00_4558,
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				}
			else
				{	/* Cgen/emit_cop.scm 142 */
					obj_t BgL_sportz00_4559;

					{	/* Cgen/emit_cop.scm 142 */
						obj_t BgL_arg1899z00_4560;

						BgL_arg1899z00_4560 =
							(((BgL_cpragmaz00_bglt) COBJECT(
									((BgL_cpragmaz00_bglt) BgL_copz00_4197)))->BgL_formatz00);
						{	/* Cgen/emit_cop.scm 142 */
							long BgL_endz00_4561;

							BgL_endz00_4561 = STRING_LENGTH(BgL_arg1899z00_4560);
							{	/* Cgen/emit_cop.scm 142 */

								BgL_sportz00_4559 =
									BGl_openzd2inputzd2stringz00zz__r4_ports_6_10_1z00
									(BgL_arg1899z00_4560, BINT(0L), BINT(BgL_endz00_4561));
					}}}
					{	/* Cgen/emit_cop.scm 142 */
						obj_t BgL_argsz00_4562;

						BgL_argsz00_4562 =
							BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(
							(((BgL_cpragmaz00_bglt) COBJECT(
										((BgL_cpragmaz00_bglt) BgL_copz00_4197)))->BgL_argsz00));
						{
							obj_t BgL_iportz00_4564;

							{	/* Cgen/emit_cop.scm 144 */

								BgL_iportz00_4564 = BgL_sportz00_4559;
								{
									obj_t BgL_iportz00_4616;
									long BgL_lastzd2matchzd2_4617;
									long BgL_forwardz00_4618;
									long BgL_bufposz00_4619;
									obj_t BgL_iportz00_4606;
									long BgL_lastzd2matchzd2_4607;
									long BgL_forwardz00_4608;
									long BgL_bufposz00_4609;
									obj_t BgL_iportz00_4598;
									long BgL_lastzd2matchzd2_4599;
									long BgL_forwardz00_4600;
									long BgL_bufposz00_4601;
									obj_t BgL_iportz00_4591;
									long BgL_lastzd2matchzd2_4592;
									long BgL_forwardz00_4593;
									long BgL_bufposz00_4594;
									obj_t BgL_iportz00_4583;
									long BgL_lastzd2matchzd2_4584;
									long BgL_forwardz00_4585;
									long BgL_bufposz00_4586;

								BgL_ignorez00_4565:
									RGC_START_MATCH(BgL_iportz00_4564);
									{	/* Cgen/emit_cop.scm 144 */
										long BgL_matchz00_4572;

										{	/* Cgen/emit_cop.scm 144 */
											long BgL_arg1896z00_4573;
											long BgL_arg1897z00_4574;

											BgL_arg1896z00_4573 =
												RGC_BUFFER_FORWARD(BgL_iportz00_4564);
											BgL_arg1897z00_4574 =
												RGC_BUFFER_BUFPOS(BgL_iportz00_4564);
											BgL_iportz00_4591 = BgL_iportz00_4564;
											BgL_lastzd2matchzd2_4592 = 3L;
											BgL_forwardz00_4593 = BgL_arg1896z00_4573;
											BgL_bufposz00_4594 = BgL_arg1897z00_4574;
										BgL_statezd20zd21147z00_4568:
											if ((BgL_forwardz00_4593 == BgL_bufposz00_4594))
												{	/* Cgen/emit_cop.scm 144 */
													if (rgc_fill_buffer(BgL_iportz00_4591))
														{	/* Cgen/emit_cop.scm 144 */
															long BgL_arg1733z00_4595;
															long BgL_arg1734z00_4596;

															BgL_arg1733z00_4595 =
																RGC_BUFFER_FORWARD(BgL_iportz00_4591);
															BgL_arg1734z00_4596 =
																RGC_BUFFER_BUFPOS(BgL_iportz00_4591);
															{
																long BgL_bufposz00_5979;
																long BgL_forwardz00_5978;

																BgL_forwardz00_5978 = BgL_arg1733z00_4595;
																BgL_bufposz00_5979 = BgL_arg1734z00_4596;
																BgL_bufposz00_4594 = BgL_bufposz00_5979;
																BgL_forwardz00_4593 = BgL_forwardz00_5978;
																goto BgL_statezd20zd21147z00_4568;
															}
														}
													else
														{	/* Cgen/emit_cop.scm 144 */
															BgL_matchz00_4572 = BgL_lastzd2matchzd2_4592;
														}
												}
											else
												{	/* Cgen/emit_cop.scm 144 */
													int BgL_curz00_4597;

													BgL_curz00_4597 =
														RGC_BUFFER_GET_CHAR(BgL_iportz00_4591,
														BgL_forwardz00_4593);
													{	/* Cgen/emit_cop.scm 144 */

														if (((long) (BgL_curz00_4597) == 36L))
															{	/* Cgen/emit_cop.scm 144 */
																BgL_iportz00_4606 = BgL_iportz00_4591;
																BgL_lastzd2matchzd2_4607 =
																	BgL_lastzd2matchzd2_4592;
																BgL_forwardz00_4608 =
																	(1L + BgL_forwardz00_4593);
																BgL_bufposz00_4609 = BgL_bufposz00_4594;
															BgL_statezd22zd21149z00_4570:
																{	/* Cgen/emit_cop.scm 144 */
																	long BgL_newzd2matchzd2_4610;

																	RGC_STOP_MATCH(BgL_iportz00_4606,
																		BgL_forwardz00_4608);
																	BgL_newzd2matchzd2_4610 = 3L;
																	if (
																		(BgL_forwardz00_4608 == BgL_bufposz00_4609))
																		{	/* Cgen/emit_cop.scm 144 */
																			if (rgc_fill_buffer(BgL_iportz00_4606))
																				{	/* Cgen/emit_cop.scm 144 */
																					long BgL_arg1699z00_4611;
																					long BgL_arg1700z00_4612;

																					BgL_arg1699z00_4611 =
																						RGC_BUFFER_FORWARD
																						(BgL_iportz00_4606);
																					BgL_arg1700z00_4612 =
																						RGC_BUFFER_BUFPOS
																						(BgL_iportz00_4606);
																					{
																						long BgL_bufposz00_5992;
																						long BgL_forwardz00_5991;

																						BgL_forwardz00_5991 =
																							BgL_arg1699z00_4611;
																						BgL_bufposz00_5992 =
																							BgL_arg1700z00_4612;
																						BgL_bufposz00_4609 =
																							BgL_bufposz00_5992;
																						BgL_forwardz00_4608 =
																							BgL_forwardz00_5991;
																						goto BgL_statezd22zd21149z00_4570;
																					}
																				}
																			else
																				{	/* Cgen/emit_cop.scm 144 */
																					BgL_matchz00_4572 =
																						BgL_newzd2matchzd2_4610;
																				}
																		}
																	else
																		{	/* Cgen/emit_cop.scm 144 */
																			int BgL_curz00_4613;

																			BgL_curz00_4613 =
																				RGC_BUFFER_GET_CHAR(BgL_iportz00_4606,
																				BgL_forwardz00_4608);
																			{	/* Cgen/emit_cop.scm 144 */

																				{	/* Cgen/emit_cop.scm 144 */
																					bool_t BgL_test2699z00_5994;

																					if (((long) (BgL_curz00_4613) >= 48L))
																						{	/* Cgen/emit_cop.scm 144 */
																							BgL_test2699z00_5994 =
																								(
																								(long) (BgL_curz00_4613) < 58L);
																						}
																					else
																						{	/* Cgen/emit_cop.scm 144 */
																							BgL_test2699z00_5994 =
																								((bool_t) 0);
																						}
																					if (BgL_test2699z00_5994)
																						{	/* Cgen/emit_cop.scm 144 */
																							BgL_iportz00_4583 =
																								BgL_iportz00_4606;
																							BgL_lastzd2matchzd2_4584 =
																								BgL_newzd2matchzd2_4610;
																							BgL_forwardz00_4585 =
																								(1L + BgL_forwardz00_4608);
																							BgL_bufposz00_4586 =
																								BgL_bufposz00_4609;
																						BgL_statezd24zd21151z00_4567:
																							{	/* Cgen/emit_cop.scm 144 */
																								long BgL_newzd2matchzd2_4587;

																								RGC_STOP_MATCH
																									(BgL_iportz00_4583,
																									BgL_forwardz00_4585);
																								BgL_newzd2matchzd2_4587 = 0L;
																								if (
																									(BgL_forwardz00_4585 ==
																										BgL_bufposz00_4586))
																									{	/* Cgen/emit_cop.scm 144 */
																										if (rgc_fill_buffer
																											(BgL_iportz00_4583))
																											{	/* Cgen/emit_cop.scm 144 */
																												long
																													BgL_arg1746z00_4588;
																												long
																													BgL_arg1747z00_4589;
																												BgL_arg1746z00_4588 =
																													RGC_BUFFER_FORWARD
																													(BgL_iportz00_4583);
																												BgL_arg1747z00_4589 =
																													RGC_BUFFER_BUFPOS
																													(BgL_iportz00_4583);
																												{
																													long
																														BgL_bufposz00_6008;
																													long
																														BgL_forwardz00_6007;
																													BgL_forwardz00_6007 =
																														BgL_arg1746z00_4588;
																													BgL_bufposz00_6008 =
																														BgL_arg1747z00_4589;
																													BgL_bufposz00_4586 =
																														BgL_bufposz00_6008;
																													BgL_forwardz00_4585 =
																														BgL_forwardz00_6007;
																													goto
																														BgL_statezd24zd21151z00_4567;
																												}
																											}
																										else
																											{	/* Cgen/emit_cop.scm 144 */
																												BgL_matchz00_4572 =
																													BgL_newzd2matchzd2_4587;
																											}
																									}
																								else
																									{	/* Cgen/emit_cop.scm 144 */
																										int BgL_curz00_4590;

																										BgL_curz00_4590 =
																											RGC_BUFFER_GET_CHAR
																											(BgL_iportz00_4583,
																											BgL_forwardz00_4585);
																										{	/* Cgen/emit_cop.scm 144 */

																											{	/* Cgen/emit_cop.scm 144 */
																												bool_t
																													BgL_test2703z00_6010;
																												if (((long)
																														(BgL_curz00_4590) >=
																														48L))
																													{	/* Cgen/emit_cop.scm 144 */
																														BgL_test2703z00_6010
																															=
																															((long)
																															(BgL_curz00_4590)
																															< 58L);
																													}
																												else
																													{	/* Cgen/emit_cop.scm 144 */
																														BgL_test2703z00_6010
																															= ((bool_t) 0);
																													}
																												if (BgL_test2703z00_6010)
																													{
																														long
																															BgL_forwardz00_6017;
																														long
																															BgL_lastzd2matchzd2_6016;
																														BgL_lastzd2matchzd2_6016
																															=
																															BgL_newzd2matchzd2_4587;
																														BgL_forwardz00_6017
																															=
																															(1L +
																															BgL_forwardz00_4585);
																														BgL_forwardz00_4585
																															=
																															BgL_forwardz00_6017;
																														BgL_lastzd2matchzd2_4584
																															=
																															BgL_lastzd2matchzd2_6016;
																														goto
																															BgL_statezd24zd21151z00_4567;
																													}
																												else
																													{	/* Cgen/emit_cop.scm 144 */
																														BgL_matchz00_4572 =
																															BgL_newzd2matchzd2_4587;
																													}
																											}
																										}
																									}
																							}
																						}
																					else
																						{	/* Cgen/emit_cop.scm 144 */
																							if (
																								((long) (BgL_curz00_4613) ==
																									36L))
																								{	/* Cgen/emit_cop.scm 144 */
																									long BgL_arg1705z00_4614;

																									BgL_arg1705z00_4614 =
																										(1L + BgL_forwardz00_4608);
																									{	/* Cgen/emit_cop.scm 144 */
																										long
																											BgL_newzd2matchzd2_4615;
																										RGC_STOP_MATCH
																											(BgL_iportz00_4606,
																											BgL_arg1705z00_4614);
																										BgL_newzd2matchzd2_4615 =
																											1L;
																										BgL_matchz00_4572 =
																											BgL_newzd2matchzd2_4615;
																								}}
																							else
																								{	/* Cgen/emit_cop.scm 144 */
																									BgL_matchz00_4572 =
																										BgL_newzd2matchzd2_4610;
																								}
																						}
																				}
																			}
																		}
																}
															}
														else
															{	/* Cgen/emit_cop.scm 144 */
																BgL_iportz00_4598 = BgL_iportz00_4591;
																BgL_lastzd2matchzd2_4599 =
																	BgL_lastzd2matchzd2_4592;
																BgL_forwardz00_4600 =
																	(1L + BgL_forwardz00_4593);
																BgL_bufposz00_4601 = BgL_bufposz00_4594;
															BgL_statezd21zd21148z00_4569:
																{	/* Cgen/emit_cop.scm 144 */
																	long BgL_newzd2matchzd2_4602;

																	RGC_STOP_MATCH(BgL_iportz00_4598,
																		BgL_forwardz00_4600);
																	BgL_newzd2matchzd2_4602 = 2L;
																	if (
																		(BgL_forwardz00_4600 == BgL_bufposz00_4601))
																		{	/* Cgen/emit_cop.scm 144 */
																			if (rgc_fill_buffer(BgL_iportz00_4598))
																				{	/* Cgen/emit_cop.scm 144 */
																					long BgL_arg1714z00_4603;
																					long BgL_arg1717z00_4604;

																					BgL_arg1714z00_4603 =
																						RGC_BUFFER_FORWARD
																						(BgL_iportz00_4598);
																					BgL_arg1717z00_4604 =
																						RGC_BUFFER_BUFPOS
																						(BgL_iportz00_4598);
																					{
																						long BgL_bufposz00_6034;
																						long BgL_forwardz00_6033;

																						BgL_forwardz00_6033 =
																							BgL_arg1714z00_4603;
																						BgL_bufposz00_6034 =
																							BgL_arg1717z00_4604;
																						BgL_bufposz00_4601 =
																							BgL_bufposz00_6034;
																						BgL_forwardz00_4600 =
																							BgL_forwardz00_6033;
																						goto BgL_statezd21zd21148z00_4569;
																					}
																				}
																			else
																				{	/* Cgen/emit_cop.scm 144 */
																					BgL_matchz00_4572 =
																						BgL_newzd2matchzd2_4602;
																				}
																		}
																	else
																		{	/* Cgen/emit_cop.scm 144 */
																			int BgL_curz00_4605;

																			BgL_curz00_4605 =
																				RGC_BUFFER_GET_CHAR(BgL_iportz00_4598,
																				BgL_forwardz00_4600);
																			{	/* Cgen/emit_cop.scm 144 */

																				if (((long) (BgL_curz00_4605) == 36L))
																					{	/* Cgen/emit_cop.scm 144 */
																						BgL_matchz00_4572 =
																							BgL_newzd2matchzd2_4602;
																					}
																				else
																					{	/* Cgen/emit_cop.scm 144 */
																						BgL_iportz00_4616 =
																							BgL_iportz00_4598;
																						BgL_lastzd2matchzd2_4617 =
																							BgL_newzd2matchzd2_4602;
																						BgL_forwardz00_4618 =
																							(1L + BgL_forwardz00_4600);
																						BgL_bufposz00_4619 =
																							BgL_bufposz00_4601;
																					BgL_statezd26zd21153z00_4571:
																						{	/* Cgen/emit_cop.scm 144 */
																							long BgL_newzd2matchzd2_4620;

																							RGC_STOP_MATCH(BgL_iportz00_4616,
																								BgL_forwardz00_4618);
																							BgL_newzd2matchzd2_4620 = 2L;
																							if (
																								(BgL_forwardz00_4618 ==
																									BgL_bufposz00_4619))
																								{	/* Cgen/emit_cop.scm 144 */
																									if (rgc_fill_buffer
																										(BgL_iportz00_4616))
																										{	/* Cgen/emit_cop.scm 144 */
																											long BgL_arg1688z00_4621;
																											long BgL_arg1689z00_4622;

																											BgL_arg1688z00_4621 =
																												RGC_BUFFER_FORWARD
																												(BgL_iportz00_4616);
																											BgL_arg1689z00_4622 =
																												RGC_BUFFER_BUFPOS
																												(BgL_iportz00_4616);
																											{
																												long BgL_bufposz00_6047;
																												long
																													BgL_forwardz00_6046;
																												BgL_forwardz00_6046 =
																													BgL_arg1688z00_4621;
																												BgL_bufposz00_6047 =
																													BgL_arg1689z00_4622;
																												BgL_bufposz00_4619 =
																													BgL_bufposz00_6047;
																												BgL_forwardz00_4618 =
																													BgL_forwardz00_6046;
																												goto
																													BgL_statezd26zd21153z00_4571;
																											}
																										}
																									else
																										{	/* Cgen/emit_cop.scm 144 */
																											BgL_matchz00_4572 =
																												BgL_newzd2matchzd2_4620;
																										}
																								}
																							else
																								{	/* Cgen/emit_cop.scm 144 */
																									int BgL_curz00_4623;

																									BgL_curz00_4623 =
																										RGC_BUFFER_GET_CHAR
																										(BgL_iportz00_4616,
																										BgL_forwardz00_4618);
																									{	/* Cgen/emit_cop.scm 144 */

																										if (
																											((long) (BgL_curz00_4623)
																												== 36L))
																											{	/* Cgen/emit_cop.scm 144 */
																												BgL_matchz00_4572 =
																													BgL_newzd2matchzd2_4620;
																											}
																										else
																											{
																												long
																													BgL_forwardz00_6053;
																												long
																													BgL_lastzd2matchzd2_6052;
																												BgL_lastzd2matchzd2_6052
																													=
																													BgL_newzd2matchzd2_4620;
																												BgL_forwardz00_6053 =
																													(1L +
																													BgL_forwardz00_4618);
																												BgL_forwardz00_4618 =
																													BgL_forwardz00_6053;
																												BgL_lastzd2matchzd2_4617
																													=
																													BgL_lastzd2matchzd2_6052;
																												goto
																													BgL_statezd26zd21153z00_4571;
																											}
																									}
																								}
																						}
																					}
																			}
																		}
																}
															}
													}
												}
										}
										RGC_SET_FILEPOS(BgL_iportz00_4564);
										{

											switch (BgL_matchz00_4572)
												{
												case 3L:

													{	/* Cgen/emit_cop.scm 144 */
														bool_t BgL_test2712z00_6058;

														{	/* Cgen/emit_cop.scm 144 */
															long BgL_arg1884z00_4582;

															BgL_arg1884z00_4582 =
																RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4564);
															BgL_test2712z00_6058 =
																(BgL_arg1884z00_4582 == 0L);
														}
														if (BgL_test2712z00_6058)
															{	/* Cgen/emit_cop.scm 144 */
																BEOF;
															}
														else
															{	/* Cgen/emit_cop.scm 144 */
																BCHAR(RGC_BUFFER_CHARACTER(BgL_iportz00_4564));
															}
													}
													break;
												case 2L:

													{	/* Cgen/emit_cop.scm 156 */
														obj_t BgL_arg1891z00_4576;

														BgL_arg1891z00_4576 =
															BGl_thezd2stringze70z35zzcgen_emitzd2copzd2
															(BgL_iportz00_4564);
														bgl_display_obj(BgL_arg1891z00_4576,
															BGl_za2czd2portza2zd2zzbackend_c_emitz00);
													}
													goto BgL_ignorez00_4565;
													break;
												case 1L:

													bgl_display_string
														(BGl_string2461z00zzcgen_emitzd2copzd2,
														BGl_za2czd2portza2zd2zzbackend_c_emitz00);
													goto BgL_ignorez00_4565;
													break;
												case 0L:

													{	/* Cgen/emit_cop.scm 146 */
														obj_t BgL_strz00_4577;

														BgL_strz00_4577 =
															BGl_thezd2stringze70z35zzcgen_emitzd2copzd2
															(BgL_iportz00_4564);
														{	/* Cgen/emit_cop.scm 146 */
															long BgL_lenz00_4578;

															BgL_lenz00_4578 =
																RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4564);
															{	/* Cgen/emit_cop.scm 147 */
																obj_t BgL_indexz00_4579;

																{	/* Cgen/emit_cop.scm 149 */
																	obj_t BgL_arg1894z00_4580;

																	BgL_arg1894z00_4580 =
																		c_substring(BgL_strz00_4577, 1L,
																		BgL_lenz00_4578);
																	{	/* Cgen/emit_cop.scm 148 */

																		BgL_indexz00_4579 =
																			BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00
																			(BgL_arg1894z00_4580, BINT(10L));
																}}
																{	/* Cgen/emit_cop.scm 148 */

																	{	/* Cgen/emit_cop.scm 150 */
																		obj_t BgL_arg1892z00_4581;

																		BgL_arg1892z00_4581 =
																			VECTOR_REF(BgL_argsz00_4562,
																			((long) CINT(BgL_indexz00_4579) - 1L));
																		BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
																			((BgL_copz00_bglt) BgL_arg1892z00_4581));
																	}
																	goto BgL_ignorez00_4565;
																}
															}
														}
													}
													break;
												default:
													BGl_errorz00zz__errorz00
														(BGl_string2459z00zzcgen_emitzd2copzd2,
														BGl_string2460z00zzcgen_emitzd2copzd2,
														BINT(BgL_matchz00_4572));
												}
										}
									}
								}
								bgl_close_input_port(BgL_sportz00_4559);
								return BTRUE;
							}
						}
					}
				}
		}

	}



/* the-string~0 */
	obj_t BGl_thezd2stringze70z35zzcgen_emitzd2copzd2(obj_t BgL_iportz00_4212)
	{
		{	/* Cgen/emit_cop.scm 144 */
			{	/* Cgen/emit_cop.scm 144 */
				long BgL_arg1761z00_2017;

				BgL_arg1761z00_2017 = RGC_BUFFER_MATCH_LENGTH(BgL_iportz00_4212);
				return rgc_buffer_substring(BgL_iportz00_4212, 0L, BgL_arg1761z00_2017);
			}
		}

	}



/* &emit-cop-varc1399 */
	obj_t BGl_z62emitzd2copzd2varc1399z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4198, obj_t BgL_copz00_4199)
	{
		{	/* Cgen/emit_cop.scm 120 */
			{	/* Cgen/emit_cop.scm 121 */
				bool_t BgL_tmpz00_6082;

				{	/* Cgen/emit_cop.scm 122 */
					bool_t BgL_test2713z00_6083;

					{	/* Cgen/emit_cop.scm 122 */
						bool_t BgL_test2714z00_6084;

						{	/* Cgen/emit_cop.scm 122 */
							BgL_variablez00_bglt BgL_arg1654z00_4625;

							BgL_arg1654z00_4625 =
								(((BgL_varcz00_bglt) COBJECT(
										((BgL_varcz00_bglt) BgL_copz00_4199)))->BgL_variablez00);
							{	/* Cgen/emit_cop.scm 122 */
								obj_t BgL_classz00_4626;

								BgL_classz00_4626 = BGl_globalz00zzast_varz00;
								{	/* Cgen/emit_cop.scm 122 */
									BgL_objectz00_bglt BgL_arg1807z00_4627;

									{	/* Cgen/emit_cop.scm 122 */
										obj_t BgL_tmpz00_6087;

										BgL_tmpz00_6087 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1654z00_4625));
										BgL_arg1807z00_4627 =
											(BgL_objectz00_bglt) (BgL_tmpz00_6087);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cgen/emit_cop.scm 122 */
											long BgL_idxz00_4628;

											BgL_idxz00_4628 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4627);
											BgL_test2714z00_6084 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4628 + 2L)) == BgL_classz00_4626);
										}
									else
										{	/* Cgen/emit_cop.scm 122 */
											bool_t BgL_res2349z00_4631;

											{	/* Cgen/emit_cop.scm 122 */
												obj_t BgL_oclassz00_4632;

												{	/* Cgen/emit_cop.scm 122 */
													obj_t BgL_arg1815z00_4633;
													long BgL_arg1816z00_4634;

													BgL_arg1815z00_4633 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cgen/emit_cop.scm 122 */
														long BgL_arg1817z00_4635;

														BgL_arg1817z00_4635 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4627);
														BgL_arg1816z00_4634 =
															(BgL_arg1817z00_4635 - OBJECT_TYPE);
													}
													BgL_oclassz00_4632 =
														VECTOR_REF(BgL_arg1815z00_4633,
														BgL_arg1816z00_4634);
												}
												{	/* Cgen/emit_cop.scm 122 */
													bool_t BgL__ortest_1115z00_4636;

													BgL__ortest_1115z00_4636 =
														(BgL_classz00_4626 == BgL_oclassz00_4632);
													if (BgL__ortest_1115z00_4636)
														{	/* Cgen/emit_cop.scm 122 */
															BgL_res2349z00_4631 = BgL__ortest_1115z00_4636;
														}
													else
														{	/* Cgen/emit_cop.scm 122 */
															long BgL_odepthz00_4637;

															{	/* Cgen/emit_cop.scm 122 */
																obj_t BgL_arg1804z00_4638;

																BgL_arg1804z00_4638 = (BgL_oclassz00_4632);
																BgL_odepthz00_4637 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4638);
															}
															if ((2L < BgL_odepthz00_4637))
																{	/* Cgen/emit_cop.scm 122 */
																	obj_t BgL_arg1802z00_4639;

																	{	/* Cgen/emit_cop.scm 122 */
																		obj_t BgL_arg1803z00_4640;

																		BgL_arg1803z00_4640 = (BgL_oclassz00_4632);
																		BgL_arg1802z00_4639 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4640, 2L);
																	}
																	BgL_res2349z00_4631 =
																		(BgL_arg1802z00_4639 == BgL_classz00_4626);
																}
															else
																{	/* Cgen/emit_cop.scm 122 */
																	BgL_res2349z00_4631 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2714z00_6084 = BgL_res2349z00_4631;
										}
								}
							}
						}
						if (BgL_test2714z00_6084)
							{	/* Cgen/emit_cop.scm 123 */
								BgL_globalz00_bglt BgL_i1144z00_4641;

								BgL_i1144z00_4641 =
									((BgL_globalz00_bglt)
									(((BgL_varcz00_bglt) COBJECT(
												((BgL_varcz00_bglt) BgL_copz00_4199)))->
										BgL_variablez00));
								{	/* Cgen/emit_cop.scm 124 */
									bool_t BgL_test2718z00_6113;

									{	/* Cgen/emit_cop.scm 124 */
										BgL_valuez00_bglt BgL_arg1651z00_4642;

										BgL_arg1651z00_4642 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_i1144z00_4641)))->
											BgL_valuez00);
										{	/* Cgen/emit_cop.scm 124 */
											obj_t BgL_classz00_4643;

											BgL_classz00_4643 = BGl_scnstz00zzast_varz00;
											{	/* Cgen/emit_cop.scm 124 */
												BgL_objectz00_bglt BgL_arg1807z00_4644;

												{	/* Cgen/emit_cop.scm 124 */
													obj_t BgL_tmpz00_6116;

													BgL_tmpz00_6116 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1651z00_4642));
													BgL_arg1807z00_4644 =
														(BgL_objectz00_bglt) (BgL_tmpz00_6116);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cgen/emit_cop.scm 124 */
														long BgL_idxz00_4645;

														BgL_idxz00_4645 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4644);
														BgL_test2718z00_6113 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_4645 + 2L)) == BgL_classz00_4643);
													}
												else
													{	/* Cgen/emit_cop.scm 124 */
														bool_t BgL_res2350z00_4648;

														{	/* Cgen/emit_cop.scm 124 */
															obj_t BgL_oclassz00_4649;

															{	/* Cgen/emit_cop.scm 124 */
																obj_t BgL_arg1815z00_4650;
																long BgL_arg1816z00_4651;

																BgL_arg1815z00_4650 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cgen/emit_cop.scm 124 */
																	long BgL_arg1817z00_4652;

																	BgL_arg1817z00_4652 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4644);
																	BgL_arg1816z00_4651 =
																		(BgL_arg1817z00_4652 - OBJECT_TYPE);
																}
																BgL_oclassz00_4649 =
																	VECTOR_REF(BgL_arg1815z00_4650,
																	BgL_arg1816z00_4651);
															}
															{	/* Cgen/emit_cop.scm 124 */
																bool_t BgL__ortest_1115z00_4653;

																BgL__ortest_1115z00_4653 =
																	(BgL_classz00_4643 == BgL_oclassz00_4649);
																if (BgL__ortest_1115z00_4653)
																	{	/* Cgen/emit_cop.scm 124 */
																		BgL_res2350z00_4648 =
																			BgL__ortest_1115z00_4653;
																	}
																else
																	{	/* Cgen/emit_cop.scm 124 */
																		long BgL_odepthz00_4654;

																		{	/* Cgen/emit_cop.scm 124 */
																			obj_t BgL_arg1804z00_4655;

																			BgL_arg1804z00_4655 =
																				(BgL_oclassz00_4649);
																			BgL_odepthz00_4654 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_4655);
																		}
																		if ((2L < BgL_odepthz00_4654))
																			{	/* Cgen/emit_cop.scm 124 */
																				obj_t BgL_arg1802z00_4656;

																				{	/* Cgen/emit_cop.scm 124 */
																					obj_t BgL_arg1803z00_4657;

																					BgL_arg1803z00_4657 =
																						(BgL_oclassz00_4649);
																					BgL_arg1802z00_4656 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_4657, 2L);
																				}
																				BgL_res2350z00_4648 =
																					(BgL_arg1802z00_4656 ==
																					BgL_classz00_4643);
																			}
																		else
																			{	/* Cgen/emit_cop.scm 124 */
																				BgL_res2350z00_4648 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2718z00_6113 = BgL_res2350z00_4648;
													}
											}
										}
									}
									if (BgL_test2718z00_6113)
										{	/* Cgen/emit_cop.scm 124 */
											BgL_test2713z00_6083 =
												(
												(((BgL_scnstz00_bglt) COBJECT(
															((BgL_scnstz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_i1144z00_4641)))->BgL_valuez00))))->
													BgL_classz00) == CNST_TABLE_REF(8));
										}
									else
										{	/* Cgen/emit_cop.scm 124 */
											BgL_test2713z00_6083 = ((bool_t) 0);
										}
								}
							}
						else
							{	/* Cgen/emit_cop.scm 122 */
								BgL_test2713z00_6083 = ((bool_t) 0);
							}
					}
					if (BgL_test2713z00_6083)
						{	/* Cgen/emit_cop.scm 122 */
							bgl_display_string(BGl_string2462z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							{	/* Cgen/emit_cop.scm 129 */
								obj_t BgL_arg1629z00_4658;

								BgL_arg1629z00_4658 =
									(((BgL_variablez00_bglt) COBJECT(
											(((BgL_varcz00_bglt) COBJECT(
														((BgL_varcz00_bglt) BgL_copz00_4199)))->
												BgL_variablez00)))->BgL_namez00);
								bgl_display_obj(BgL_arg1629z00_4658,
									BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							}
							bgl_display_string(BGl_string2413z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
					else
						{	/* Cgen/emit_cop.scm 131 */
							obj_t BgL_arg1642z00_4659;

							BgL_arg1642z00_4659 =
								(((BgL_variablez00_bglt) COBJECT(
										(((BgL_varcz00_bglt) COBJECT(
													((BgL_varcz00_bglt) BgL_copz00_4199)))->
											BgL_variablez00)))->BgL_namez00);
							bgl_display_obj(BgL_arg1642z00_4659,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
				}
				BgL_tmpz00_6082 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_6082);
			}
		}

	}



/* &emit-cop-cvoid1396 */
	obj_t BGl_z62emitzd2copzd2cvoid1396z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4200, obj_t BgL_copz00_4201)
	{
		{	/* Cgen/emit_cop.scm 113 */
			return
				BBOOL(BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
					(((BgL_cvoidz00_bglt) COBJECT(
								((BgL_cvoidz00_bglt) BgL_copz00_4201)))->BgL_valuez00)));
		}

	}



/* &emit-cop-catom1394 */
	obj_t BGl_z62emitzd2copzd2catom1394z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4202, obj_t BgL_copz00_4203)
	{
		{	/* Cgen/emit_cop.scm 105 */
			{	/* Cgen/emit_cop.scm 106 */
				bool_t BgL_tmpz00_6160;

				{	/* Cgen/emit_cop.scm 107 */
					obj_t BgL_arg1606z00_4662;
					BgL_typez00_bglt BgL_arg1609z00_4663;

					BgL_arg1606z00_4662 =
						(((BgL_catomz00_bglt) COBJECT(
								((BgL_catomz00_bglt) BgL_copz00_4203)))->BgL_valuez00);
					BgL_arg1609z00_4663 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_catomz00_bglt) BgL_copz00_4203))))->BgL_typez00);
					BGl_emitzd2atomzd2valuez00zzbackend_c_emitz00(BgL_arg1606z00_4662,
						BgL_arg1609z00_4663);
				}
				BgL_tmpz00_6160 = ((bool_t) 1);
				return BBOOL(BgL_tmpz00_6160);
			}
		}

	}



/* &emit-cop-creturn1392 */
	obj_t BGl_z62emitzd2copzd2creturn1392z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4204, obj_t BgL_copz00_4205)
	{
		{	/* Cgen/emit_cop.scm 92 */
			{	/* Cgen/emit_cop.scm 93 */
				bool_t BgL_tmpz00_6168;

				{	/* Cgen/emit_cop.scm 94 */
					obj_t BgL_arg1594z00_4665;

					BgL_arg1594z00_4665 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_creturnz00_bglt) BgL_copz00_4205))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1594z00_4665);
				}
				if (
					(((BgL_creturnz00_bglt) COBJECT(
								((BgL_creturnz00_bglt) BgL_copz00_4205)))->BgL_tailz00))
					{	/* Cgen/emit_cop.scm 95 */
						bgl_display_string(BGl_string2463z00zzcgen_emitzd2copzd2,
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* Cgen/emit_cop.scm 95 */
						BFALSE;
					}
				bgl_display_string(BGl_string2464z00zzcgen_emitzd2copzd2,
					BGl_za2czd2portza2zd2zzbackend_c_emitz00);
				if (BGl_emitzd2copzd2zzcgen_emitzd2copzd2((((BgL_creturnz00_bglt)
								COBJECT(((BgL_creturnz00_bglt) BgL_copz00_4205)))->
							BgL_valuez00)))
					{	/* Cgen/emit_cop.scm 98 */
						bgl_display_char(((unsigned char) ';'),
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* Cgen/emit_cop.scm 98 */
						BFALSE;
					}
				BgL_tmpz00_6168 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_6168);
			}
		}

	}



/* &emit-cop-cblock1390 */
	obj_t BGl_z62emitzd2copzd2cblock1390z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4206, obj_t BgL_copz00_4207)
	{
		{	/* Cgen/emit_cop.scm 71 */
			{	/* Cgen/emit_cop.scm 72 */
				bool_t BgL_tmpz00_6184;

				{	/* Cgen/emit_cop.scm 73 */
					obj_t BgL_arg1571z00_4667;

					BgL_arg1571z00_4667 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cblockz00_bglt) BgL_copz00_4207))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1571z00_4667);
				}
				{	/* Cgen/emit_cop.scm 74 */
					bool_t BgL_test2724z00_6189;

					{	/* Cgen/emit_cop.scm 74 */
						BgL_copz00_bglt BgL_arg1593z00_4668;

						BgL_arg1593z00_4668 =
							(((BgL_cblockz00_bglt) COBJECT(
									((BgL_cblockz00_bglt) BgL_copz00_4207)))->BgL_bodyz00);
						{	/* Cgen/emit_cop.scm 74 */
							obj_t BgL_classz00_4669;

							BgL_classz00_4669 = BGl_cblockz00zzcgen_copz00;
							{	/* Cgen/emit_cop.scm 74 */
								BgL_objectz00_bglt BgL_arg1807z00_4670;

								{	/* Cgen/emit_cop.scm 74 */
									obj_t BgL_tmpz00_6192;

									BgL_tmpz00_6192 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1593z00_4668));
									BgL_arg1807z00_4670 = (BgL_objectz00_bglt) (BgL_tmpz00_6192);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cgen/emit_cop.scm 74 */
										long BgL_idxz00_4671;

										BgL_idxz00_4671 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4670);
										BgL_test2724z00_6189 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4671 + 2L)) == BgL_classz00_4669);
									}
								else
									{	/* Cgen/emit_cop.scm 74 */
										bool_t BgL_res2348z00_4674;

										{	/* Cgen/emit_cop.scm 74 */
											obj_t BgL_oclassz00_4675;

											{	/* Cgen/emit_cop.scm 74 */
												obj_t BgL_arg1815z00_4676;
												long BgL_arg1816z00_4677;

												BgL_arg1815z00_4676 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cgen/emit_cop.scm 74 */
													long BgL_arg1817z00_4678;

													BgL_arg1817z00_4678 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4670);
													BgL_arg1816z00_4677 =
														(BgL_arg1817z00_4678 - OBJECT_TYPE);
												}
												BgL_oclassz00_4675 =
													VECTOR_REF(BgL_arg1815z00_4676, BgL_arg1816z00_4677);
											}
											{	/* Cgen/emit_cop.scm 74 */
												bool_t BgL__ortest_1115z00_4679;

												BgL__ortest_1115z00_4679 =
													(BgL_classz00_4669 == BgL_oclassz00_4675);
												if (BgL__ortest_1115z00_4679)
													{	/* Cgen/emit_cop.scm 74 */
														BgL_res2348z00_4674 = BgL__ortest_1115z00_4679;
													}
												else
													{	/* Cgen/emit_cop.scm 74 */
														long BgL_odepthz00_4680;

														{	/* Cgen/emit_cop.scm 74 */
															obj_t BgL_arg1804z00_4681;

															BgL_arg1804z00_4681 = (BgL_oclassz00_4675);
															BgL_odepthz00_4680 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4681);
														}
														if ((2L < BgL_odepthz00_4680))
															{	/* Cgen/emit_cop.scm 74 */
																obj_t BgL_arg1802z00_4682;

																{	/* Cgen/emit_cop.scm 74 */
																	obj_t BgL_arg1803z00_4683;

																	BgL_arg1803z00_4683 = (BgL_oclassz00_4675);
																	BgL_arg1802z00_4682 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4683,
																		2L);
																}
																BgL_res2348z00_4674 =
																	(BgL_arg1802z00_4682 == BgL_classz00_4669);
															}
														else
															{	/* Cgen/emit_cop.scm 74 */
																BgL_res2348z00_4674 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2724z00_6189 = BgL_res2348z00_4674;
									}
							}
						}
					}
					if (BgL_test2724z00_6189)
						{	/* Cgen/emit_cop.scm 74 */
							BgL_tmpz00_6184 =
								BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
								(((BgL_cblockz00_bglt) COBJECT(
											((BgL_cblockz00_bglt) BgL_copz00_4207)))->BgL_bodyz00));
						}
					else
						{	/* Cgen/emit_cop.scm 74 */
							bgl_display_string(BGl_string2465z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							{	/* Cgen/emit_cop.scm 79 */
								obj_t BgL_arg1576z00_4684;

								BgL_arg1576z00_4684 =
									(((BgL_copz00_bglt) COBJECT(
											((BgL_copz00_bglt)
												((BgL_cblockz00_bglt) BgL_copz00_4207))))->BgL_locz00);
								BGl_emitzd2bdbzd2loczd2commentzd2zzcgen_emitzd2copzd2
									(BgL_arg1576z00_4684);
							}
							if (BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
									(((BgL_cblockz00_bglt) COBJECT(
												((BgL_cblockz00_bglt) BgL_copz00_4207)))->BgL_bodyz00)))
								{	/* Cgen/emit_cop.scm 80 */
									BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2
										(BGl_za2bdbzd2locza2zd2zzcgen_emitzd2copzd2);
									bgl_display_string(BGl_string2421z00zzcgen_emitzd2copzd2,
										BGl_za2czd2portza2zd2zzbackend_c_emitz00);
									BNIL;
								}
							else
								{	/* Cgen/emit_cop.scm 80 */
									BFALSE;
								}
							bgl_display_string(BGl_string2422z00zzcgen_emitzd2copzd2,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
							BgL_tmpz00_6184 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_6184);
			}
		}

	}



/* &emit-cop-cgoto1388 */
	obj_t BGl_z62emitzd2copzd2cgoto1388z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4208, obj_t BgL_copz00_4209)
	{
		{	/* Cgen/emit_cop.scm 62 */
			{	/* Cgen/emit_cop.scm 63 */
				bool_t BgL_tmpz00_6231;

				{	/* Cgen/emit_cop.scm 64 */
					obj_t BgL_arg1561z00_4686;

					BgL_arg1561z00_4686 =
						(((BgL_copz00_bglt) COBJECT(
								((BgL_copz00_bglt)
									((BgL_cgotoz00_bglt) BgL_copz00_4209))))->BgL_locz00);
					BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1561z00_4686);
				}
				{	/* Cgen/emit_cop.scm 65 */
					obj_t BgL_port1138z00_4687;

					BgL_port1138z00_4687 = BGl_za2czd2portza2zd2zzbackend_c_emitz00;
					{	/* Cgen/emit_cop.scm 65 */
						obj_t BgL_tmpz00_6236;

						BgL_tmpz00_6236 = ((obj_t) BgL_port1138z00_4687);
						bgl_display_string(BGl_string2466z00zzcgen_emitzd2copzd2,
							BgL_tmpz00_6236);
					}
					bgl_display_obj(
						(((BgL_clabelz00_bglt) COBJECT(
									(((BgL_cgotoz00_bglt) COBJECT(
												((BgL_cgotoz00_bglt) BgL_copz00_4209)))->
										BgL_labelz00)))->BgL_namez00), BgL_port1138z00_4687);
					{	/* Cgen/emit_cop.scm 65 */
						obj_t BgL_tmpz00_6243;

						BgL_tmpz00_6243 = ((obj_t) BgL_port1138z00_4687);
						bgl_display_char(((unsigned char) ';'), BgL_tmpz00_6243);
				}}
				BgL_tmpz00_6231 = ((bool_t) 0);
				return BBOOL(BgL_tmpz00_6231);
			}
		}

	}



/* &emit-cop-clabel1386 */
	obj_t BGl_z62emitzd2copzd2clabel1386z62zzcgen_emitzd2copzd2(obj_t
		BgL_envz00_4210, obj_t BgL_copz00_4211)
	{
		{	/* Cgen/emit_cop.scm 50 */
			{	/* Cgen/emit_cop.scm 51 */
				bool_t BgL_tmpz00_6247;

				if (
					(((BgL_clabelz00_bglt) COBJECT(
								((BgL_clabelz00_bglt) BgL_copz00_4211)))->BgL_usedzf3zf3))
					{	/* Cgen/emit_cop.scm 52 */
						{	/* Cgen/emit_cop.scm 54 */
							obj_t BgL_arg1552z00_4689;

							BgL_arg1552z00_4689 =
								(((BgL_copz00_bglt) COBJECT(
										((BgL_copz00_bglt)
											((BgL_clabelz00_bglt) BgL_copz00_4211))))->BgL_locz00);
							BGl_emitzd2bdbzd2locz00zzcgen_emitzd2copzd2(BgL_arg1552z00_4689);
						}
						{	/* Cgen/emit_cop.scm 55 */
							obj_t BgL_arg1553z00_4690;

							BgL_arg1553z00_4690 =
								(((BgL_clabelz00_bglt) COBJECT(
										((BgL_clabelz00_bglt) BgL_copz00_4211)))->BgL_namez00);
							bgl_display_obj(BgL_arg1553z00_4690,
								BGl_za2czd2portza2zd2zzbackend_c_emitz00);
						}
						bgl_display_char(((unsigned char) ':'),
							BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					}
				else
					{	/* Cgen/emit_cop.scm 52 */
						BFALSE;
					}
				{	/* Cgen/emit_cop.scm 57 */
					obj_t BgL_arg1559z00_4691;

					BgL_arg1559z00_4691 =
						(((BgL_clabelz00_bglt) COBJECT(
								((BgL_clabelz00_bglt) BgL_copz00_4211)))->BgL_bodyz00);
					BgL_tmpz00_6247 =
						BGl_emitzd2copzd2zzcgen_emitzd2copzd2(
						((BgL_copz00_bglt) BgL_arg1559z00_4691));
				}
				return BBOOL(BgL_tmpz00_6247);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcgen_emitzd2copzd2(void)
	{
		{	/* Cgen/emit_cop.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(474089076L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			BGl_modulezd2initializa7ationz75zzcgen_copz00(529595144L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
			return
				BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string2467z00zzcgen_emitzd2copzd2));
		}

	}

#ifdef __cplusplus
}
#endif
