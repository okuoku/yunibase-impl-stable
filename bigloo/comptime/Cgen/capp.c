/*===========================================================================*/
/*   (Cgen/capp.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cgen/capp.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CGEN_CAPP_TYPE_DEFINITIONS
#define BGL_CGEN_CAPP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_copz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}             *BgL_copz00_bglt;

	typedef struct BgL_clabelz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_namez00;
		bool_t BgL_usedzf3zf3;
		obj_t BgL_bodyz00;
	}                *BgL_clabelz00_bglt;

	typedef struct BgL_cgotoz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_clabelz00_bgl *BgL_labelz00;
	}               *BgL_cgotoz00_bglt;

	typedef struct BgL_cblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_bodyz00;
	}                *BgL_cblockz00_bglt;

	typedef struct BgL_varcz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}              *BgL_varcz00_bglt;

	typedef struct BgL_cpragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_formatz00;
		obj_t BgL_argsz00;
	}                 *BgL_cpragmaz00_bglt;

	typedef struct BgL_csequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_czd2expzf3z21;
		obj_t BgL_copsz00;
	}                   *BgL_csequencez00_bglt;

	typedef struct BgL_stopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}              *BgL_stopz00_bglt;

	typedef struct BgL_csetqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varcz00_bgl *BgL_varz00;
		struct BgL_copz00_bgl *BgL_valuez00;
	}               *BgL_csetqz00_bglt;

	typedef struct BgL_localzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varsz00;
	}                     *BgL_localzd2varzd2_bglt;

	typedef struct BgL_cfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
	}                  *BgL_cfuncallz00_bglt;

	typedef struct BgL_capplyz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		struct BgL_copz00_bgl *BgL_argz00;
	}                *BgL_capplyz00_bglt;

	typedef struct BgL_cappz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_copz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}              *BgL_cappz00_bglt;

	typedef struct BgL_sfunzf2czf2_bgl
	{
		struct BgL_clabelz00_bgl *BgL_labelz00;
		bool_t BgL_integratedz00;
	}                  *BgL_sfunzf2czf2_bglt;


#endif													// BGL_CGEN_CAPP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2stopzd2kontza2zd2zzcgen_cgenz00;
	static obj_t BGl_requirezd2initializa7ationz75zzcgen_cappz00 = BUNSPEC;
	extern obj_t BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00;
	static obj_t
		BGl_nodezd2cfunzd2nonzd2tailzd2appzd2ze3copz31zzcgen_cappz00
		(BgL_variablez00_bglt, BgL_appz00_bglt, obj_t, obj_t);
	static obj_t
		BGl_nodezd2tailzd2appzd2ze3copz31zzcgen_cappz00(BgL_variablez00_bglt,
		BgL_appz00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_cappz00zzcgen_copz00;
	static obj_t BGl_toplevelzd2initzd2zzcgen_cappz00(void);
	extern obj_t BGl_csequencez00zzcgen_copz00;
	extern obj_t BGl_cgotoz00zzcgen_copz00;
	static obj_t BGl_genericzd2initzd2zzcgen_cappz00(void);
	extern BgL_setqz00_bglt BGl_nodezd2setqzd2zzcgen_cgenz00(BgL_variablez00_bglt,
		BgL_nodez00_bglt);
	static obj_t BGl_objectzd2initzd2zzcgen_cappz00(void);
	extern BgL_copz00_bglt BGl_bdbzd2letzd2varz00zzcgen_cgenz00(BgL_copz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern obj_t BGl_capplyz00zzcgen_copz00;
	extern obj_t BGl_cpragmaz00zzcgen_copz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcgen_cappz00(void);
	extern BgL_localz00_bglt
		BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(obj_t, BgL_typez00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern bool_t BGl_globalzd2argszd2safezf3zf3zzast_varz00(BgL_globalz00_bglt);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2funcall1464z81zzcgen_cappz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t
		BGl_nodezd2sfunzd2nonzd2tailzd2appzd2ze3copz31zzcgen_cappz00
		(BgL_variablez00_bglt, BgL_appz00_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcgen_cappz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_cgenz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcgen_copz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzeffect_effectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_toolsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_csetqz00zzcgen_copz00;
	extern obj_t BGl_localzd2varzd2zzcgen_copz00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_cblockz00zzcgen_copz00;
	static obj_t BGl_cnstzd2initzd2zzcgen_cappz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcgen_cappz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcgen_cappz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcgen_cappz00(void);
	static BgL_copz00_bglt
		BGl_z62nodezd2ze3copzd2appzd2ly1462z53zzcgen_cappz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_catomz00zzcgen_copz00;
	extern obj_t BGl_za2returnzd2kontza2zd2zzcgen_cgenz00;
	extern obj_t BGl_stopz00zzcgen_copz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_cfuncallz00zzcgen_copz00;
	extern BgL_copz00_bglt BGl_nodezd2ze3copz31zzcgen_cgenz00(BgL_nodez00_bglt,
		obj_t, bool_t);
	static BgL_copz00_bglt BGl_z62nodezd2ze3copzd2app1466z81zzcgen_cappz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static bool_t
		BGl_iszd2getzd2exitdzd2topzf3z21zzcgen_cappz00(BgL_variablez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_za2idzd2kontza2zd2zzcgen_cgenz00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_blockzd2kontzd2zzcgen_cgenz00(obj_t, obj_t);
	extern obj_t BGl_varcz00zzcgen_copz00;
	static obj_t __cnst[7];


	   
		 
		DEFINE_STRING(BGl_string2155z00zzcgen_cappz00,
		BgL_bgl_string2155za700za7za7c2163za7, "((obj_t)(&exitd))", 17);
	      DEFINE_STRING(BGl_string2157z00zzcgen_cappz00,
		BgL_bgl_string2157za700za7za7c2164za7, "node->cop", 9);
	      DEFINE_STRING(BGl_string2160z00zzcgen_cappz00,
		BgL_bgl_string2160za700za7za7c2165za7, "cgen_capp", 9);
	      DEFINE_STRING(BGl_string2161z00zzcgen_cappz00,
		BgL_bgl_string2161za700za7za7c2166za7,
		"fun a tmpfun tmp $env-get-exitd-top $get-exitd-top aux ", 55);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzcgen_cappz00,
		BgL_bgl_za762nodeza7d2za7e3cop2167za7,
		BGl_z62nodezd2ze3copzd2appzd2ly1462z53zzcgen_cappz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzcgen_cappz00,
		BgL_bgl_za762nodeza7d2za7e3cop2168za7,
		BGl_z62nodezd2ze3copzd2funcall1464z81zzcgen_cappz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzcgen_cappz00,
		BgL_bgl_za762nodeza7d2za7e3cop2169za7,
		BGl_z62nodezd2ze3copzd2app1466z81zzcgen_cappz00, 0L, BUNSPEC, 3);
	extern obj_t BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcgen_cappz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcgen_cappz00(long
		BgL_checksumz00_4037, char *BgL_fromz00_4038)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcgen_cappz00))
				{
					BGl_requirezd2initializa7ationz75zzcgen_cappz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcgen_cappz00();
					BGl_libraryzd2moduleszd2initz00zzcgen_cappz00();
					BGl_cnstzd2initzd2zzcgen_cappz00();
					BGl_importedzd2moduleszd2initz00zzcgen_cappz00();
					BGl_methodzd2initzd2zzcgen_cappz00();
					return BGl_toplevelzd2initzd2zzcgen_cappz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcgen_cappz00(void)
	{
		{	/* Cgen/capp.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cgen_capp");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cgen_capp");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cgen_capp");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cgen_capp");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cgen_capp");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cgen_capp");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cgen_capp");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cgen_capp");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcgen_cappz00(void)
	{
		{	/* Cgen/capp.scm 15 */
			{	/* Cgen/capp.scm 15 */
				obj_t BgL_cportz00_3682;

				{	/* Cgen/capp.scm 15 */
					obj_t BgL_stringz00_3689;

					BgL_stringz00_3689 = BGl_string2161z00zzcgen_cappz00;
					{	/* Cgen/capp.scm 15 */
						obj_t BgL_startz00_3690;

						BgL_startz00_3690 = BINT(0L);
						{	/* Cgen/capp.scm 15 */
							obj_t BgL_endz00_3691;

							BgL_endz00_3691 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3689)));
							{	/* Cgen/capp.scm 15 */

								BgL_cportz00_3682 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3689, BgL_startz00_3690, BgL_endz00_3691);
				}}}}
				{
					long BgL_iz00_3683;

					BgL_iz00_3683 = 6L;
				BgL_loopz00_3684:
					if ((BgL_iz00_3683 == -1L))
						{	/* Cgen/capp.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cgen/capp.scm 15 */
							{	/* Cgen/capp.scm 15 */
								obj_t BgL_arg2162z00_3685;

								{	/* Cgen/capp.scm 15 */

									{	/* Cgen/capp.scm 15 */
										obj_t BgL_locationz00_3687;

										BgL_locationz00_3687 = BBOOL(((bool_t) 0));
										{	/* Cgen/capp.scm 15 */

											BgL_arg2162z00_3685 =
												BGl_readz00zz__readerz00(BgL_cportz00_3682,
												BgL_locationz00_3687);
										}
									}
								}
								{	/* Cgen/capp.scm 15 */
									int BgL_tmpz00_4065;

									BgL_tmpz00_4065 = (int) (BgL_iz00_3683);
									CNST_TABLE_SET(BgL_tmpz00_4065, BgL_arg2162z00_3685);
							}}
							{	/* Cgen/capp.scm 15 */
								int BgL_auxz00_3688;

								BgL_auxz00_3688 = (int) ((BgL_iz00_3683 - 1L));
								{
									long BgL_iz00_4070;

									BgL_iz00_4070 = (long) (BgL_auxz00_3688);
									BgL_iz00_3683 = BgL_iz00_4070;
									goto BgL_loopz00_3684;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcgen_cappz00(void)
	{
		{	/* Cgen/capp.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcgen_cappz00(void)
	{
		{	/* Cgen/capp.scm 15 */
			return BUNSPEC;
		}

	}



/* node-sfun-non-tail-app->cop */
	obj_t
		BGl_nodezd2sfunzd2nonzd2tailzd2appzd2ze3copz31zzcgen_cappz00
		(BgL_variablez00_bglt BgL_varz00_16, BgL_appz00_bglt BgL_nodez00_17,
		obj_t BgL_kontz00_18, obj_t BgL_inpushexitz00_19)
	{
		{	/* Cgen/capp.scm 259 */
			{	/* Cgen/capp.scm 260 */
				obj_t BgL_argsz00_1753;

				BgL_argsz00_1753 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(BgL_varz00_16))->
									BgL_valuez00))))->BgL_argsz00);
				{	/* Cgen/capp.scm 260 */
					obj_t BgL_argszd2typezd2_1754;

					if (NULLP(BgL_argsz00_1753))
						{	/* Cgen/capp.scm 261 */
							BgL_argszd2typezd2_1754 = BNIL;
						}
					else
						{	/* Cgen/capp.scm 261 */
							obj_t BgL_head1458z00_1831;

							BgL_head1458z00_1831 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1456z00_1833;
								obj_t BgL_tail1459z00_1834;

								BgL_l1456z00_1833 = BgL_argsz00_1753;
								BgL_tail1459z00_1834 = BgL_head1458z00_1831;
							BgL_zc3z04anonymousza31618ze3z87_1835:
								if (NULLP(BgL_l1456z00_1833))
									{	/* Cgen/capp.scm 261 */
										BgL_argszd2typezd2_1754 = CDR(BgL_head1458z00_1831);
									}
								else
									{	/* Cgen/capp.scm 261 */
										obj_t BgL_newtail1460z00_1837;

										{	/* Cgen/capp.scm 261 */
											obj_t BgL_arg1626z00_1839;

											{	/* Cgen/capp.scm 261 */
												obj_t BgL_xz00_1840;

												BgL_xz00_1840 = CAR(((obj_t) BgL_l1456z00_1833));
												{	/* Cgen/capp.scm 261 */
													bool_t BgL_test2174z00_4084;

													{	/* Cgen/capp.scm 261 */
														obj_t BgL_classz00_2588;

														BgL_classz00_2588 = BGl_localz00zzast_varz00;
														if (BGL_OBJECTP(BgL_xz00_1840))
															{	/* Cgen/capp.scm 261 */
																BgL_objectz00_bglt BgL_arg1807z00_2590;

																BgL_arg1807z00_2590 =
																	(BgL_objectz00_bglt) (BgL_xz00_1840);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Cgen/capp.scm 261 */
																		long BgL_idxz00_2596;

																		BgL_idxz00_2596 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2590);
																		BgL_test2174z00_4084 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2596 + 2L)) ==
																			BgL_classz00_2588);
																	}
																else
																	{	/* Cgen/capp.scm 261 */
																		bool_t BgL_res2116z00_2621;

																		{	/* Cgen/capp.scm 261 */
																			obj_t BgL_oclassz00_2604;

																			{	/* Cgen/capp.scm 261 */
																				obj_t BgL_arg1815z00_2612;
																				long BgL_arg1816z00_2613;

																				BgL_arg1815z00_2612 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Cgen/capp.scm 261 */
																					long BgL_arg1817z00_2614;

																					BgL_arg1817z00_2614 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2590);
																					BgL_arg1816z00_2613 =
																						(BgL_arg1817z00_2614 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2604 =
																					VECTOR_REF(BgL_arg1815z00_2612,
																					BgL_arg1816z00_2613);
																			}
																			{	/* Cgen/capp.scm 261 */
																				bool_t BgL__ortest_1115z00_2605;

																				BgL__ortest_1115z00_2605 =
																					(BgL_classz00_2588 ==
																					BgL_oclassz00_2604);
																				if (BgL__ortest_1115z00_2605)
																					{	/* Cgen/capp.scm 261 */
																						BgL_res2116z00_2621 =
																							BgL__ortest_1115z00_2605;
																					}
																				else
																					{	/* Cgen/capp.scm 261 */
																						long BgL_odepthz00_2606;

																						{	/* Cgen/capp.scm 261 */
																							obj_t BgL_arg1804z00_2607;

																							BgL_arg1804z00_2607 =
																								(BgL_oclassz00_2604);
																							BgL_odepthz00_2606 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2607);
																						}
																						if ((2L < BgL_odepthz00_2606))
																							{	/* Cgen/capp.scm 261 */
																								obj_t BgL_arg1802z00_2609;

																								{	/* Cgen/capp.scm 261 */
																									obj_t BgL_arg1803z00_2610;

																									BgL_arg1803z00_2610 =
																										(BgL_oclassz00_2604);
																									BgL_arg1802z00_2609 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2610, 2L);
																								}
																								BgL_res2116z00_2621 =
																									(BgL_arg1802z00_2609 ==
																									BgL_classz00_2588);
																							}
																						else
																							{	/* Cgen/capp.scm 261 */
																								BgL_res2116z00_2621 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2174z00_4084 = BgL_res2116z00_2621;
																	}
															}
														else
															{	/* Cgen/capp.scm 261 */
																BgL_test2174z00_4084 = ((bool_t) 0);
															}
													}
													if (BgL_test2174z00_4084)
														{	/* Cgen/capp.scm 261 */
															BgL_arg1626z00_1839 =
																((obj_t)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt) BgL_xz00_1840))))->
																	BgL_typez00));
														}
													else
														{	/* Cgen/capp.scm 261 */
															BgL_arg1626z00_1839 = BgL_xz00_1840;
														}
												}
											}
											BgL_newtail1460z00_1837 =
												MAKE_YOUNG_PAIR(BgL_arg1626z00_1839, BNIL);
										}
										SET_CDR(BgL_tail1459z00_1834, BgL_newtail1460z00_1837);
										{	/* Cgen/capp.scm 261 */
											obj_t BgL_arg1625z00_1838;

											BgL_arg1625z00_1838 = CDR(((obj_t) BgL_l1456z00_1833));
											{
												obj_t BgL_tail1459z00_4116;
												obj_t BgL_l1456z00_4115;

												BgL_l1456z00_4115 = BgL_arg1625z00_1838;
												BgL_tail1459z00_4116 = BgL_newtail1460z00_1837;
												BgL_tail1459z00_1834 = BgL_tail1459z00_4116;
												BgL_l1456z00_1833 = BgL_l1456z00_4115;
												goto BgL_zc3z04anonymousza31618ze3z87_1835;
											}
										}
									}
							}
						}
					{
						BgL_copz00_bglt BgL_copz00_1822;
						BgL_localz00_bglt BgL_auxz00_1823;

						{	/* Cgen/capp.scm 262 */

							{	/* Cgen/capp.scm 265 */
								obj_t BgL_g1214z00_1756;
								BgL_localz00_bglt BgL_g1216z00_1758;

								BgL_g1214z00_1756 =
									(((BgL_appz00_bglt) COBJECT(BgL_nodez00_17))->BgL_argsz00);
								BgL_g1216z00_1758 =
									BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(CNST_TABLE_REF
									(0), ((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
								{
									obj_t BgL_oldzd2actualszd2_1762;
									obj_t BgL_argszd2typezd2_1763;
									obj_t BgL_newzd2actualszd2_1764;
									BgL_localz00_bglt BgL_auxz00_1765;
									obj_t BgL_auxsz00_1766;
									obj_t BgL_expsz00_1767;

									BgL_oldzd2actualszd2_1762 = BgL_g1214z00_1756;
									BgL_argszd2typezd2_1763 = BgL_argszd2typezd2_1754;
									BgL_newzd2actualszd2_1764 = BNIL;
									BgL_auxz00_1765 = BgL_g1216z00_1758;
									BgL_auxsz00_1766 = BNIL;
									BgL_expsz00_1767 = BNIL;
								BgL_zc3z04anonymousza31490ze3z87_1768:
									if (NULLP(BgL_oldzd2actualszd2_1762))
										{	/* Cgen/capp.scm 271 */
											if (NULLP(BgL_auxsz00_1766))
												{	/* Cgen/capp.scm 274 */
													BgL_cappz00_bglt BgL_arg1502z00_1771;

													{	/* Cgen/capp.scm 274 */
														BgL_cappz00_bglt BgL_new1220z00_1772;

														{	/* Cgen/capp.scm 276 */
															BgL_cappz00_bglt BgL_new1219z00_1774;

															BgL_new1219z00_1774 =
																((BgL_cappz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_cappz00_bgl))));
															{	/* Cgen/capp.scm 276 */
																long BgL_arg1513z00_1775;

																BgL_arg1513z00_1775 =
																	BGL_CLASS_NUM(BGl_cappz00zzcgen_copz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1219z00_1774),
																	BgL_arg1513z00_1775);
															}
															BgL_new1220z00_1772 = BgL_new1219z00_1774;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1220z00_1772)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_17)))->BgL_locz00)),
															BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1220z00_1772)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_17)))->BgL_typez00)),
															BUNSPEC);
														{
															BgL_copz00_bglt BgL_auxz00_4137;

															{	/* Cgen/capp.scm 277 */
																BgL_varz00_bglt BgL_arg1509z00_1773;

																BgL_arg1509z00_1773 =
																	(((BgL_appz00_bglt) COBJECT(BgL_nodez00_17))->
																	BgL_funz00);
																BgL_auxz00_4137 =
																	BGl_nodezd2ze3copz31zzcgen_cgenz00((
																		(BgL_nodez00_bglt) BgL_arg1509z00_1773),
																	BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
																	CBOOL(BgL_inpushexitz00_19));
															}
															((((BgL_cappz00_bglt)
																		COBJECT(BgL_new1220z00_1772))->BgL_funz00) =
																((BgL_copz00_bglt) BgL_auxz00_4137), BUNSPEC);
														}
														((((BgL_cappz00_bglt)
																	COBJECT(BgL_new1220z00_1772))->BgL_argsz00) =
															((obj_t)
																bgl_reverse_bang(BgL_newzd2actualszd2_1764)),
															BUNSPEC);
														((((BgL_cappz00_bglt)
																	COBJECT(BgL_new1220z00_1772))->
																BgL_stackablez00) =
															((obj_t) (((BgL_appz00_bglt)
																		COBJECT(BgL_nodez00_17))->
																	BgL_stackablez00)), BUNSPEC);
														BgL_arg1502z00_1771 = BgL_new1220z00_1772;
													}
													return
														BGL_PROCEDURE_CALL1(BgL_kontz00_18,
														((obj_t) BgL_arg1502z00_1771));
												}
											else
												{	/* Cgen/capp.scm 283 */
													obj_t BgL_locz00_1776;

													BgL_locz00_1776 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_nodez00_17)))->
														BgL_locz00);
													{	/* Cgen/capp.scm 284 */
														BgL_cblockz00_bglt BgL_new1222z00_1777;

														{	/* Cgen/capp.scm 286 */
															BgL_cblockz00_bglt BgL_new1221z00_1798;

															BgL_new1221z00_1798 =
																((BgL_cblockz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_cblockz00_bgl))));
															{	/* Cgen/capp.scm 286 */
																long BgL_arg1565z00_1799;

																BgL_arg1565z00_1799 =
																	BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1221z00_1798),
																	BgL_arg1565z00_1799);
															}
															BgL_new1222z00_1777 = BgL_new1221z00_1798;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1222z00_1777)))->
																BgL_locz00) =
															((obj_t) BgL_locz00_1776), BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1222z00_1777)))->BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
														{
															BgL_copz00_bglt BgL_auxz00_4163;

															{	/* Cgen/capp.scm 287 */
																BgL_csequencez00_bglt BgL_new1224z00_1778;

																{	/* Cgen/capp.scm 289 */
																	BgL_csequencez00_bglt BgL_new1223z00_1796;

																	BgL_new1223z00_1796 =
																		((BgL_csequencez00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_csequencez00_bgl))));
																	{	/* Cgen/capp.scm 289 */
																		long BgL_arg1564z00_1797;

																		{	/* Cgen/capp.scm 289 */
																			obj_t BgL_classz00_2673;

																			BgL_classz00_2673 =
																				BGl_csequencez00zzcgen_copz00;
																			BgL_arg1564z00_1797 =
																				BGL_CLASS_NUM(BgL_classz00_2673);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1223z00_1796),
																			BgL_arg1564z00_1797);
																	}
																	BgL_new1224z00_1778 = BgL_new1223z00_1796;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1224z00_1778)))->BgL_locz00) =
																	((obj_t) BgL_locz00_1776), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1224z00_1778)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_nodez00_17)))->BgL_typez00)),
																	BUNSPEC);
																((((BgL_csequencez00_bglt)
																			COBJECT(BgL_new1224z00_1778))->
																		BgL_czd2expzf3z21) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
																{
																	obj_t BgL_auxz00_4175;

																	{	/* Cgen/capp.scm 292 */
																		BgL_localzd2varzd2_bglt BgL_arg1514z00_1779;
																		BgL_csequencez00_bglt BgL_arg1516z00_1780;
																		obj_t BgL_arg1535z00_1781;

																		{	/* Cgen/capp.scm 292 */
																			BgL_localzd2varzd2_bglt
																				BgL_new1226z00_1785;
																			{	/* Cgen/capp.scm 294 */
																				BgL_localzd2varzd2_bglt
																					BgL_new1225z00_1786;
																				BgL_new1225z00_1786 =
																					((BgL_localzd2varzd2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_localzd2varzd2_bgl))));
																				{	/* Cgen/capp.scm 294 */
																					long BgL_arg1546z00_1787;

																					{	/* Cgen/capp.scm 294 */
																						obj_t BgL_classz00_2677;

																						BgL_classz00_2677 =
																							BGl_localzd2varzd2zzcgen_copz00;
																						BgL_arg1546z00_1787 =
																							BGL_CLASS_NUM(BgL_classz00_2677);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1225z00_1786),
																						BgL_arg1546z00_1787);
																				}
																				BgL_new1226z00_1785 =
																					BgL_new1225z00_1786;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1226z00_1785)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_1776), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1226z00_1785)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_localzd2varzd2_bglt)
																						COBJECT(BgL_new1226z00_1785))->
																					BgL_varsz00) =
																				((obj_t) BgL_auxsz00_1766), BUNSPEC);
																			BgL_arg1514z00_1779 = BgL_new1226z00_1785;
																		}
																		{	/* Cgen/capp.scm 296 */
																			BgL_csequencez00_bglt BgL_new1229z00_1788;

																			{	/* Cgen/capp.scm 298 */
																				BgL_csequencez00_bglt
																					BgL_new1228z00_1789;
																				BgL_new1228z00_1789 =
																					((BgL_csequencez00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_csequencez00_bgl))));
																				{	/* Cgen/capp.scm 298 */
																					long BgL_arg1552z00_1790;

																					{	/* Cgen/capp.scm 298 */
																						obj_t BgL_classz00_2680;

																						BgL_classz00_2680 =
																							BGl_csequencez00zzcgen_copz00;
																						BgL_arg1552z00_1790 =
																							BGL_CLASS_NUM(BgL_classz00_2680);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1228z00_1789),
																						BgL_arg1552z00_1790);
																				}
																				BgL_new1229z00_1788 =
																					BgL_new1228z00_1789;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1229z00_1788)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_1776), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1229z00_1788)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1229z00_1788))->
																					BgL_czd2expzf3z21) =
																				((bool_t) ((bool_t) 0)), BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1229z00_1788))->
																					BgL_copsz00) =
																				((obj_t) BgL_expsz00_1767), BUNSPEC);
																			BgL_arg1516z00_1780 = BgL_new1229z00_1788;
																		}
																		{	/* Cgen/capp.scm 301 */
																			BgL_cappz00_bglt BgL_arg1553z00_1791;

																			{	/* Cgen/capp.scm 301 */
																				BgL_cappz00_bglt BgL_new1231z00_1792;

																				{	/* Cgen/capp.scm 303 */
																					BgL_cappz00_bglt BgL_new1230z00_1794;

																					BgL_new1230z00_1794 =
																						((BgL_cappz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_cappz00_bgl))));
																					{	/* Cgen/capp.scm 303 */
																						long BgL_arg1561z00_1795;

																						{	/* Cgen/capp.scm 303 */
																							obj_t BgL_classz00_2683;

																							BgL_classz00_2683 =
																								BGl_cappz00zzcgen_copz00;
																							BgL_arg1561z00_1795 =
																								BGL_CLASS_NUM
																								(BgL_classz00_2683);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1230z00_1794),
																							BgL_arg1561z00_1795);
																					}
																					BgL_new1231z00_1792 =
																						BgL_new1230z00_1794;
																				}
																				((((BgL_copz00_bglt) COBJECT(
																								((BgL_copz00_bglt)
																									BgL_new1231z00_1792)))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_1776), BUNSPEC);
																				((((BgL_copz00_bglt)
																							COBJECT(((BgL_copz00_bglt)
																									BgL_new1231z00_1792)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_nodez00_17)))->
																							BgL_typez00)), BUNSPEC);
																				{
																					BgL_copz00_bglt BgL_auxz00_4207;

																					{	/* Cgen/capp.scm 304 */
																						BgL_varz00_bglt BgL_arg1559z00_1793;

																						BgL_arg1559z00_1793 =
																							(((BgL_appz00_bglt)
																								COBJECT(BgL_nodez00_17))->
																							BgL_funz00);
																						BgL_auxz00_4207 =
																							BGl_nodezd2ze3copz31zzcgen_cgenz00
																							(((BgL_nodez00_bglt)
																								BgL_arg1559z00_1793),
																							BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
																							CBOOL(BgL_inpushexitz00_19));
																					}
																					((((BgL_cappz00_bglt)
																								COBJECT(BgL_new1231z00_1792))->
																							BgL_funz00) =
																						((BgL_copz00_bglt) BgL_auxz00_4207),
																						BUNSPEC);
																				}
																				((((BgL_cappz00_bglt)
																							COBJECT(BgL_new1231z00_1792))->
																						BgL_argsz00) =
																					((obj_t)
																						bgl_reverse_bang
																						(BgL_newzd2actualszd2_1764)),
																					BUNSPEC);
																				((((BgL_cappz00_bglt)
																							COBJECT(BgL_new1231z00_1792))->
																						BgL_stackablez00) =
																					((obj_t) (((BgL_appz00_bglt)
																								COBJECT(BgL_nodez00_17))->
																							BgL_stackablez00)), BUNSPEC);
																				BgL_arg1553z00_1791 =
																					BgL_new1231z00_1792;
																			}
																			BgL_arg1535z00_1781 =
																				BGL_PROCEDURE_CALL1(BgL_kontz00_18,
																				((obj_t) BgL_arg1553z00_1791));
																		}
																		{	/* Cgen/capp.scm 291 */
																			obj_t BgL_list1536z00_1782;

																			{	/* Cgen/capp.scm 291 */
																				obj_t BgL_arg1540z00_1783;

																				{	/* Cgen/capp.scm 291 */
																					obj_t BgL_arg1544z00_1784;

																					BgL_arg1544z00_1784 =
																						MAKE_YOUNG_PAIR(BgL_arg1535z00_1781,
																						BNIL);
																					BgL_arg1540z00_1783 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_arg1516z00_1780),
																						BgL_arg1544z00_1784);
																				}
																				BgL_list1536z00_1782 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg1514z00_1779),
																					BgL_arg1540z00_1783);
																			}
																			BgL_auxz00_4175 = BgL_list1536z00_1782;
																	}}
																	((((BgL_csequencez00_bglt)
																				COBJECT(BgL_new1224z00_1778))->
																			BgL_copsz00) =
																		((obj_t) BgL_auxz00_4175), BUNSPEC);
																}
																BgL_auxz00_4163 =
																	((BgL_copz00_bglt) BgL_new1224z00_1778);
															}
															((((BgL_cblockz00_bglt)
																		COBJECT(BgL_new1222z00_1777))->
																	BgL_bodyz00) =
																((BgL_copz00_bglt) BgL_auxz00_4163), BUNSPEC);
														}
														return ((obj_t) BgL_new1222z00_1777);
													}
												}
										}
									else
										{	/* Cgen/capp.scm 308 */
											BgL_copz00_bglt BgL_copz00_1800;

											{	/* Cgen/capp.scm 308 */
												BgL_setqz00_bglt BgL_arg1611z00_1819;

												{	/* Cgen/capp.scm 308 */
													obj_t BgL_arg1613z00_1820;

													BgL_arg1613z00_1820 =
														CAR(((obj_t) BgL_oldzd2actualszd2_1762));
													BgL_arg1611z00_1819 =
														BGl_nodezd2setqzd2zzcgen_cgenz00(
														((BgL_variablez00_bglt) BgL_auxz00_1765),
														((BgL_nodez00_bglt) BgL_arg1613z00_1820));
												}
												BgL_copz00_1800 =
													BGl_nodezd2ze3copz31zzcgen_cgenz00(
													((BgL_nodez00_bglt) BgL_arg1611z00_1819),
													BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
													CBOOL(BgL_inpushexitz00_19));
											}
											{	/* Cgen/capp.scm 310 */
												bool_t BgL_test2181z00_4239;

												BgL_copz00_1822 = BgL_copz00_1800;
												BgL_auxz00_1823 = BgL_auxz00_1765;
												{	/* Cgen/capp.scm 263 */
													bool_t BgL_test2182z00_4240;

													{	/* Cgen/capp.scm 263 */
														obj_t BgL_classz00_2625;

														BgL_classz00_2625 = BGl_csetqz00zzcgen_copz00;
														{	/* Cgen/capp.scm 263 */
															BgL_objectz00_bglt BgL_arg1807z00_2627;

															{	/* Cgen/capp.scm 263 */
																obj_t BgL_tmpz00_4241;

																BgL_tmpz00_4241 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_copz00_1822));
																BgL_arg1807z00_2627 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4241);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Cgen/capp.scm 263 */
																	long BgL_idxz00_2633;

																	BgL_idxz00_2633 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2627);
																	BgL_test2182z00_4240 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2633 + 2L)) ==
																		BgL_classz00_2625);
																}
															else
																{	/* Cgen/capp.scm 263 */
																	bool_t BgL_res2117z00_2658;

																	{	/* Cgen/capp.scm 263 */
																		obj_t BgL_oclassz00_2641;

																		{	/* Cgen/capp.scm 263 */
																			obj_t BgL_arg1815z00_2649;
																			long BgL_arg1816z00_2650;

																			BgL_arg1815z00_2649 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Cgen/capp.scm 263 */
																				long BgL_arg1817z00_2651;

																				BgL_arg1817z00_2651 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2627);
																				BgL_arg1816z00_2650 =
																					(BgL_arg1817z00_2651 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2641 =
																				VECTOR_REF(BgL_arg1815z00_2649,
																				BgL_arg1816z00_2650);
																		}
																		{	/* Cgen/capp.scm 263 */
																			bool_t BgL__ortest_1115z00_2642;

																			BgL__ortest_1115z00_2642 =
																				(BgL_classz00_2625 ==
																				BgL_oclassz00_2641);
																			if (BgL__ortest_1115z00_2642)
																				{	/* Cgen/capp.scm 263 */
																					BgL_res2117z00_2658 =
																						BgL__ortest_1115z00_2642;
																				}
																			else
																				{	/* Cgen/capp.scm 263 */
																					long BgL_odepthz00_2643;

																					{	/* Cgen/capp.scm 263 */
																						obj_t BgL_arg1804z00_2644;

																						BgL_arg1804z00_2644 =
																							(BgL_oclassz00_2641);
																						BgL_odepthz00_2643 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2644);
																					}
																					if ((2L < BgL_odepthz00_2643))
																						{	/* Cgen/capp.scm 263 */
																							obj_t BgL_arg1802z00_2646;

																							{	/* Cgen/capp.scm 263 */
																								obj_t BgL_arg1803z00_2647;

																								BgL_arg1803z00_2647 =
																									(BgL_oclassz00_2641);
																								BgL_arg1802z00_2646 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2647, 2L);
																							}
																							BgL_res2117z00_2658 =
																								(BgL_arg1802z00_2646 ==
																								BgL_classz00_2625);
																						}
																					else
																						{	/* Cgen/capp.scm 263 */
																							BgL_res2117z00_2658 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2182z00_4240 = BgL_res2117z00_2658;
																}
														}
													}
													if (BgL_test2182z00_4240)
														{	/* Cgen/capp.scm 263 */
															BgL_test2181z00_4239 =
																(
																((obj_t)
																	(((BgL_varcz00_bglt) COBJECT(
																				(((BgL_csetqz00_bglt) COBJECT(
																							((BgL_csetqz00_bglt)
																								BgL_copz00_1822)))->
																					BgL_varz00)))->BgL_variablez00)) ==
																((obj_t) BgL_auxz00_1823));
														}
													else
														{	/* Cgen/capp.scm 263 */
															BgL_test2181z00_4239 = ((bool_t) 0);
														}
												}
												if (BgL_test2181z00_4239)
													{	/* Cgen/capp.scm 311 */
														obj_t BgL_arg1571z00_1802;
														obj_t BgL_arg1573z00_1803;
														obj_t BgL_arg1575z00_1804;

														BgL_arg1571z00_1802 =
															CDR(((obj_t) BgL_oldzd2actualszd2_1762));
														BgL_arg1573z00_1803 =
															CDR(((obj_t) BgL_argszd2typezd2_1763));
														{	/* Cgen/capp.scm 313 */
															BgL_copz00_bglt BgL_arg1576z00_1805;

															BgL_arg1576z00_1805 =
																(((BgL_csetqz00_bglt) COBJECT(
																		((BgL_csetqz00_bglt) BgL_copz00_1800)))->
																BgL_valuez00);
															BgL_arg1575z00_1804 =
																MAKE_YOUNG_PAIR(((obj_t) BgL_arg1576z00_1805),
																BgL_newzd2actualszd2_1764);
														}
														{
															obj_t BgL_newzd2actualszd2_4280;
															obj_t BgL_argszd2typezd2_4279;
															obj_t BgL_oldzd2actualszd2_4278;

															BgL_oldzd2actualszd2_4278 = BgL_arg1571z00_1802;
															BgL_argszd2typezd2_4279 = BgL_arg1573z00_1803;
															BgL_newzd2actualszd2_4280 = BgL_arg1575z00_1804;
															BgL_newzd2actualszd2_1764 =
																BgL_newzd2actualszd2_4280;
															BgL_argszd2typezd2_1763 = BgL_argszd2typezd2_4279;
															BgL_oldzd2actualszd2_1762 =
																BgL_oldzd2actualszd2_4278;
															goto BgL_zc3z04anonymousza31490ze3z87_1768;
														}
													}
												else
													{	/* Cgen/capp.scm 310 */
														{	/* Cgen/capp.scm 318 */
															obj_t BgL_arg1584z00_1806;

															BgL_arg1584z00_1806 =
																CAR(((obj_t) BgL_argszd2typezd2_1763));
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_auxz00_1765)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BgL_arg1584z00_1806)), BUNSPEC);
														}
														{	/* Cgen/capp.scm 319 */
															obj_t BgL_arg1585z00_1807;
															obj_t BgL_arg1589z00_1808;
															obj_t BgL_arg1591z00_1809;
															BgL_localz00_bglt BgL_arg1593z00_1810;
															obj_t BgL_arg1594z00_1811;
															obj_t BgL_arg1595z00_1812;

															BgL_arg1585z00_1807 =
																CDR(((obj_t) BgL_oldzd2actualszd2_1762));
															BgL_arg1589z00_1808 =
																CDR(((obj_t) BgL_argszd2typezd2_1763));
															{	/* Cgen/capp.scm 321 */
																BgL_varcz00_bglt BgL_arg1602z00_1813;

																{	/* Cgen/capp.scm 321 */
																	BgL_varcz00_bglt BgL_new1233z00_1814;

																	{	/* Cgen/capp.scm 324 */
																		BgL_varcz00_bglt BgL_new1232z00_1816;

																		BgL_new1232z00_1816 =
																			((BgL_varcz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_varcz00_bgl))));
																		{	/* Cgen/capp.scm 324 */
																			long BgL_arg1606z00_1817;

																			BgL_arg1606z00_1817 =
																				BGL_CLASS_NUM(BGl_varcz00zzcgen_copz00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1232z00_1816),
																				BgL_arg1606z00_1817);
																		}
																		BgL_new1233z00_1814 = BgL_new1232z00_1816;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1233z00_1814)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							CAR(((obj_t)
																									BgL_oldzd2actualszd2_1762)))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1233z00_1814)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt)
																							BgL_auxz00_1765)))->BgL_typez00)),
																		BUNSPEC);
																	((((BgL_varcz00_bglt)
																				COBJECT(BgL_new1233z00_1814))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_auxz00_1765)), BUNSPEC);
																	BgL_arg1602z00_1813 = BgL_new1233z00_1814;
																}
																BgL_arg1591z00_1809 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1602z00_1813),
																	BgL_newzd2actualszd2_1764);
															}
															{	/* Cgen/capp.scm 326 */
																obj_t BgL_arg1609z00_1818;

																BgL_arg1609z00_1818 =
																	CAR(((obj_t) BgL_argszd2typezd2_1763));
																BgL_arg1593z00_1810 =
																	BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00
																	(CNST_TABLE_REF(0),
																	((BgL_typez00_bglt) BgL_arg1609z00_1818));
															}
															BgL_arg1594z00_1811 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_auxz00_1765), BgL_auxsz00_1766);
															BgL_arg1595z00_1812 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_copz00_1800), BgL_expsz00_1767);
															{
																obj_t BgL_expsz00_4322;
																obj_t BgL_auxsz00_4321;
																BgL_localz00_bglt BgL_auxz00_4320;
																obj_t BgL_newzd2actualszd2_4319;
																obj_t BgL_argszd2typezd2_4318;
																obj_t BgL_oldzd2actualszd2_4317;

																BgL_oldzd2actualszd2_4317 = BgL_arg1585z00_1807;
																BgL_argszd2typezd2_4318 = BgL_arg1589z00_1808;
																BgL_newzd2actualszd2_4319 = BgL_arg1591z00_1809;
																BgL_auxz00_4320 = BgL_arg1593z00_1810;
																BgL_auxsz00_4321 = BgL_arg1594z00_1811;
																BgL_expsz00_4322 = BgL_arg1595z00_1812;
																BgL_expsz00_1767 = BgL_expsz00_4322;
																BgL_auxsz00_1766 = BgL_auxsz00_4321;
																BgL_auxz00_1765 = BgL_auxz00_4320;
																BgL_newzd2actualszd2_1764 =
																	BgL_newzd2actualszd2_4319;
																BgL_argszd2typezd2_1763 =
																	BgL_argszd2typezd2_4318;
																BgL_oldzd2actualszd2_1762 =
																	BgL_oldzd2actualszd2_4317;
																goto BgL_zc3z04anonymousza31490ze3z87_1768;
															}
														}
													}
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* is-get-exitd-top? */
	bool_t BGl_iszd2getzd2exitdzd2topzf3z21zzcgen_cappz00(BgL_variablez00_bglt
		BgL_varz00_20)
	{
		{	/* Cgen/capp.scm 335 */
			{	/* Cgen/capp.scm 336 */
				bool_t BgL__ortest_1234z00_1844;

				BgL__ortest_1234z00_1844 =
					(
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_20))->BgL_idz00) ==
					CNST_TABLE_REF(1));
				if (BgL__ortest_1234z00_1844)
					{	/* Cgen/capp.scm 336 */
						return BgL__ortest_1234z00_1844;
					}
				else
					{	/* Cgen/capp.scm 336 */
						return
							(
							(((BgL_variablez00_bglt) COBJECT(BgL_varz00_20))->BgL_idz00) ==
							CNST_TABLE_REF(2));
					}
			}
		}

	}



/* node-cfun-non-tail-app->cop */
	obj_t
		BGl_nodezd2cfunzd2nonzd2tailzd2appzd2ze3copz31zzcgen_cappz00
		(BgL_variablez00_bglt BgL_varz00_21, BgL_appz00_bglt BgL_nodez00_22,
		obj_t BgL_kontz00_23, obj_t BgL_inpushexitz00_24)
	{
		{	/* Cgen/capp.scm 345 */
			{	/* Cgen/capp.scm 346 */
				bool_t BgL_test2187z00_4330;

				if (CBOOL(BgL_inpushexitz00_24))
					{	/* Cgen/capp.scm 346 */
						BgL_test2187z00_4330 =
							BGl_iszd2getzd2exitdzd2topzf3z21zzcgen_cappz00(BgL_varz00_21);
					}
				else
					{	/* Cgen/capp.scm 346 */
						BgL_test2187z00_4330 = ((bool_t) 0);
					}
				if (BgL_test2187z00_4330)
					{	/* Cgen/capp.scm 347 */
						BgL_cpragmaz00_bglt BgL_arg1646z00_1848;

						{	/* Cgen/capp.scm 347 */
							BgL_cpragmaz00_bglt BgL_new1236z00_1849;

							{	/* Cgen/capp.scm 349 */
								BgL_cpragmaz00_bglt BgL_new1235z00_1850;

								BgL_new1235z00_1850 =
									((BgL_cpragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_cpragmaz00_bgl))));
								{	/* Cgen/capp.scm 349 */
									long BgL_arg1650z00_1851;

									BgL_arg1650z00_1851 =
										BGL_CLASS_NUM(BGl_cpragmaz00zzcgen_copz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1235z00_1850),
										BgL_arg1650z00_1851);
								}
								BgL_new1236z00_1849 = BgL_new1235z00_1850;
							}
							((((BgL_copz00_bglt) COBJECT(
											((BgL_copz00_bglt) BgL_new1236z00_1849)))->BgL_locz00) =
								((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
													BgL_nodez00_22)))->BgL_locz00)), BUNSPEC);
							((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
												BgL_new1236z00_1849)))->BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
							((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1236z00_1849))->
									BgL_formatz00) =
								((obj_t) BGl_string2155z00zzcgen_cappz00), BUNSPEC);
							((((BgL_cpragmaz00_bglt) COBJECT(BgL_new1236z00_1849))->
									BgL_argsz00) = ((obj_t) BNIL), BUNSPEC);
							BgL_arg1646z00_1848 = BgL_new1236z00_1849;
						}
						return
							BGL_PROCEDURE_CALL1(BgL_kontz00_23,
							((obj_t) BgL_arg1646z00_1848));
					}
				else
					{	/* Cgen/capp.scm 352 */
						obj_t BgL_argszd2typezd2_1852;

						BgL_argszd2typezd2_1852 =
							(((BgL_cfunz00_bglt) COBJECT(
									((BgL_cfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(BgL_varz00_21))->
											BgL_valuez00))))->BgL_argszd2typezd2);
						{
							BgL_copz00_bglt BgL_copz00_1927;
							BgL_localz00_bglt BgL_auxz00_1928;

							{	/* Cgen/capp.scm 359 */
								obj_t BgL_g1241z00_1854;
								BgL_localz00_bglt BgL_g1243z00_1856;

								BgL_g1241z00_1854 =
									(((BgL_appz00_bglt) COBJECT(BgL_nodez00_22))->BgL_argsz00);
								BgL_g1243z00_1856 =
									BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(CNST_TABLE_REF
									(3), ((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
								{
									obj_t BgL_oldzd2actualszd2_1860;
									obj_t BgL_argszd2typezd2_1861;
									obj_t BgL_newzd2actualszd2_1862;
									BgL_localz00_bglt BgL_auxz00_1863;
									obj_t BgL_auxsz00_1864;
									obj_t BgL_expsz00_1865;

									BgL_oldzd2actualszd2_1860 = BgL_g1241z00_1854;
									BgL_argszd2typezd2_1861 = BgL_argszd2typezd2_1852;
									BgL_newzd2actualszd2_1862 = BNIL;
									BgL_auxz00_1863 = BgL_g1243z00_1856;
									BgL_auxsz00_1864 = BNIL;
									BgL_expsz00_1865 = BNIL;
								BgL_zc3z04anonymousza31651ze3z87_1866:
									if (NULLP(BgL_oldzd2actualszd2_1860))
										{	/* Cgen/capp.scm 365 */
											if (NULLP(BgL_auxsz00_1864))
												{	/* Cgen/capp.scm 368 */
													BgL_cappz00_bglt BgL_arg1654z00_1869;

													{	/* Cgen/capp.scm 368 */
														BgL_cappz00_bglt BgL_new1247z00_1870;

														{	/* Cgen/capp.scm 370 */
															BgL_cappz00_bglt BgL_new1246z00_1872;

															BgL_new1246z00_1872 =
																((BgL_cappz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_cappz00_bgl))));
															{	/* Cgen/capp.scm 370 */
																long BgL_arg1663z00_1873;

																BgL_arg1663z00_1873 =
																	BGL_CLASS_NUM(BGl_cappz00zzcgen_copz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1246z00_1872),
																	BgL_arg1663z00_1873);
															}
															BgL_new1247z00_1870 = BgL_new1246z00_1872;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1247z00_1870)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_22)))->BgL_locz00)),
															BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1247z00_1870)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_22)))->BgL_typez00)),
															BUNSPEC);
														{
															BgL_copz00_bglt BgL_auxz00_4375;

															{	/* Cgen/capp.scm 371 */
																BgL_varz00_bglt BgL_arg1661z00_1871;

																BgL_arg1661z00_1871 =
																	(((BgL_appz00_bglt) COBJECT(BgL_nodez00_22))->
																	BgL_funz00);
																BgL_auxz00_4375 =
																	BGl_nodezd2ze3copz31zzcgen_cgenz00((
																		(BgL_nodez00_bglt) BgL_arg1661z00_1871),
																	BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
																	CBOOL(BgL_inpushexitz00_24));
															}
															((((BgL_cappz00_bglt)
																		COBJECT(BgL_new1247z00_1870))->BgL_funz00) =
																((BgL_copz00_bglt) BgL_auxz00_4375), BUNSPEC);
														}
														((((BgL_cappz00_bglt)
																	COBJECT(BgL_new1247z00_1870))->BgL_argsz00) =
															((obj_t)
																bgl_reverse_bang(BgL_newzd2actualszd2_1862)),
															BUNSPEC);
														((((BgL_cappz00_bglt)
																	COBJECT(BgL_new1247z00_1870))->
																BgL_stackablez00) =
															((obj_t) (((BgL_appz00_bglt)
																		COBJECT(BgL_nodez00_22))->
																	BgL_stackablez00)), BUNSPEC);
														BgL_arg1654z00_1869 = BgL_new1247z00_1870;
													}
													return
														BGL_PROCEDURE_CALL1(BgL_kontz00_23,
														((obj_t) BgL_arg1654z00_1869));
												}
											else
												{	/* Cgen/capp.scm 379 */
													obj_t BgL_locz00_1874;

													BgL_locz00_1874 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_nodez00_22)))->
														BgL_locz00);
													{	/* Cgen/capp.scm 380 */
														BgL_cblockz00_bglt BgL_new1249z00_1875;

														{	/* Cgen/capp.scm 382 */
															BgL_cblockz00_bglt BgL_new1248z00_1896;

															BgL_new1248z00_1896 =
																((BgL_cblockz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_cblockz00_bgl))));
															{	/* Cgen/capp.scm 382 */
																long BgL_arg1703z00_1897;

																BgL_arg1703z00_1897 =
																	BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1248z00_1896),
																	BgL_arg1703z00_1897);
															}
															BgL_new1249z00_1875 = BgL_new1248z00_1896;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1249z00_1875)))->
																BgL_locz00) =
															((obj_t) BgL_locz00_1874), BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1249z00_1875)))->BgL_typez00) =
															((BgL_typez00_bglt) ((BgL_typez00_bglt)
																	BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
														{
															BgL_copz00_bglt BgL_auxz00_4401;

															{	/* Cgen/capp.scm 383 */
																BgL_csequencez00_bglt BgL_new1252z00_1876;

																{	/* Cgen/capp.scm 385 */
																	BgL_csequencez00_bglt BgL_new1251z00_1894;

																	BgL_new1251z00_1894 =
																		((BgL_csequencez00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_csequencez00_bgl))));
																	{	/* Cgen/capp.scm 385 */
																		long BgL_arg1702z00_1895;

																		{	/* Cgen/capp.scm 385 */
																			obj_t BgL_classz00_2832;

																			BgL_classz00_2832 =
																				BGl_csequencez00zzcgen_copz00;
																			BgL_arg1702z00_1895 =
																				BGL_CLASS_NUM(BgL_classz00_2832);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1251z00_1894),
																			BgL_arg1702z00_1895);
																	}
																	BgL_new1252z00_1876 = BgL_new1251z00_1894;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1252z00_1876)))->BgL_locz00) =
																	((obj_t) BgL_locz00_1874), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1252z00_1876)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_nodez00_22)))->BgL_typez00)),
																	BUNSPEC);
																((((BgL_csequencez00_bglt)
																			COBJECT(BgL_new1252z00_1876))->
																		BgL_czd2expzf3z21) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
																{
																	obj_t BgL_auxz00_4413;

																	{	/* Cgen/capp.scm 388 */
																		BgL_localzd2varzd2_bglt BgL_arg1675z00_1877;
																		BgL_csequencez00_bglt BgL_arg1678z00_1878;
																		obj_t BgL_arg1681z00_1879;

																		{	/* Cgen/capp.scm 388 */
																			BgL_localzd2varzd2_bglt
																				BgL_new1254z00_1883;
																			{	/* Cgen/capp.scm 390 */
																				BgL_localzd2varzd2_bglt
																					BgL_new1253z00_1884;
																				BgL_new1253z00_1884 =
																					((BgL_localzd2varzd2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_localzd2varzd2_bgl))));
																				{	/* Cgen/capp.scm 390 */
																					long BgL_arg1691z00_1885;

																					{	/* Cgen/capp.scm 390 */
																						obj_t BgL_classz00_2836;

																						BgL_classz00_2836 =
																							BGl_localzd2varzd2zzcgen_copz00;
																						BgL_arg1691z00_1885 =
																							BGL_CLASS_NUM(BgL_classz00_2836);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1253z00_1884),
																						BgL_arg1691z00_1885);
																				}
																				BgL_new1254z00_1883 =
																					BgL_new1253z00_1884;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1254z00_1883)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_1874), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1254z00_1883)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_localzd2varzd2_bglt)
																						COBJECT(BgL_new1254z00_1883))->
																					BgL_varsz00) =
																				((obj_t) BgL_auxsz00_1864), BUNSPEC);
																			BgL_arg1675z00_1877 = BgL_new1254z00_1883;
																		}
																		{	/* Cgen/capp.scm 392 */
																			BgL_csequencez00_bglt BgL_new1256z00_1886;

																			{	/* Cgen/capp.scm 394 */
																				BgL_csequencez00_bglt
																					BgL_new1255z00_1887;
																				BgL_new1255z00_1887 =
																					((BgL_csequencez00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_csequencez00_bgl))));
																				{	/* Cgen/capp.scm 394 */
																					long BgL_arg1692z00_1888;

																					{	/* Cgen/capp.scm 394 */
																						obj_t BgL_classz00_2839;

																						BgL_classz00_2839 =
																							BGl_csequencez00zzcgen_copz00;
																						BgL_arg1692z00_1888 =
																							BGL_CLASS_NUM(BgL_classz00_2839);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1255z00_1887),
																						BgL_arg1692z00_1888);
																				}
																				BgL_new1256z00_1886 =
																					BgL_new1255z00_1887;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1256z00_1886)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_1874), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1256z00_1886)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1256z00_1886))->
																					BgL_czd2expzf3z21) =
																				((bool_t) ((bool_t) 0)), BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1256z00_1886))->
																					BgL_copsz00) =
																				((obj_t) BgL_expsz00_1865), BUNSPEC);
																			BgL_arg1678z00_1878 = BgL_new1256z00_1886;
																		}
																		{	/* Cgen/capp.scm 397 */
																			BgL_cappz00_bglt BgL_arg1699z00_1889;

																			{	/* Cgen/capp.scm 397 */
																				BgL_cappz00_bglt BgL_new1258z00_1890;

																				{	/* Cgen/capp.scm 399 */
																					BgL_cappz00_bglt BgL_new1257z00_1892;

																					BgL_new1257z00_1892 =
																						((BgL_cappz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_cappz00_bgl))));
																					{	/* Cgen/capp.scm 399 */
																						long BgL_arg1701z00_1893;

																						{	/* Cgen/capp.scm 399 */
																							obj_t BgL_classz00_2842;

																							BgL_classz00_2842 =
																								BGl_cappz00zzcgen_copz00;
																							BgL_arg1701z00_1893 =
																								BGL_CLASS_NUM
																								(BgL_classz00_2842);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1257z00_1892),
																							BgL_arg1701z00_1893);
																					}
																					BgL_new1258z00_1890 =
																						BgL_new1257z00_1892;
																				}
																				((((BgL_copz00_bglt) COBJECT(
																								((BgL_copz00_bglt)
																									BgL_new1258z00_1890)))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_1874), BUNSPEC);
																				((((BgL_copz00_bglt)
																							COBJECT(((BgL_copz00_bglt)
																									BgL_new1258z00_1890)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt)
																										BgL_nodez00_22)))->
																							BgL_typez00)), BUNSPEC);
																				{
																					BgL_copz00_bglt BgL_auxz00_4445;

																					{	/* Cgen/capp.scm 400 */
																						BgL_varz00_bglt BgL_arg1700z00_1891;

																						BgL_arg1700z00_1891 =
																							(((BgL_appz00_bglt)
																								COBJECT(BgL_nodez00_22))->
																							BgL_funz00);
																						BgL_auxz00_4445 =
																							BGl_nodezd2ze3copz31zzcgen_cgenz00
																							(((BgL_nodez00_bglt)
																								BgL_arg1700z00_1891),
																							BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
																							CBOOL(BgL_inpushexitz00_24));
																					}
																					((((BgL_cappz00_bglt)
																								COBJECT(BgL_new1258z00_1890))->
																							BgL_funz00) =
																						((BgL_copz00_bglt) BgL_auxz00_4445),
																						BUNSPEC);
																				}
																				((((BgL_cappz00_bglt)
																							COBJECT(BgL_new1258z00_1890))->
																						BgL_argsz00) =
																					((obj_t)
																						bgl_reverse_bang
																						(BgL_newzd2actualszd2_1862)),
																					BUNSPEC);
																				((((BgL_cappz00_bglt)
																							COBJECT(BgL_new1258z00_1890))->
																						BgL_stackablez00) =
																					((obj_t) (((BgL_appz00_bglt)
																								COBJECT(BgL_nodez00_22))->
																							BgL_stackablez00)), BUNSPEC);
																				BgL_arg1699z00_1889 =
																					BgL_new1258z00_1890;
																			}
																			BgL_arg1681z00_1879 =
																				BGL_PROCEDURE_CALL1(BgL_kontz00_23,
																				((obj_t) BgL_arg1699z00_1889));
																		}
																		{	/* Cgen/capp.scm 387 */
																			obj_t BgL_list1682z00_1880;

																			{	/* Cgen/capp.scm 387 */
																				obj_t BgL_arg1688z00_1881;

																				{	/* Cgen/capp.scm 387 */
																					obj_t BgL_arg1689z00_1882;

																					BgL_arg1689z00_1882 =
																						MAKE_YOUNG_PAIR(BgL_arg1681z00_1879,
																						BNIL);
																					BgL_arg1688z00_1881 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_arg1678z00_1878),
																						BgL_arg1689z00_1882);
																				}
																				BgL_list1682z00_1880 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg1675z00_1877),
																					BgL_arg1688z00_1881);
																			}
																			BgL_auxz00_4413 = BgL_list1682z00_1880;
																	}}
																	((((BgL_csequencez00_bglt)
																				COBJECT(BgL_new1252z00_1876))->
																			BgL_copsz00) =
																		((obj_t) BgL_auxz00_4413), BUNSPEC);
																}
																BgL_auxz00_4401 =
																	((BgL_copz00_bglt) BgL_new1252z00_1876);
															}
															((((BgL_cblockz00_bglt)
																		COBJECT(BgL_new1249z00_1875))->
																	BgL_bodyz00) =
																((BgL_copz00_bglt) BgL_auxz00_4401), BUNSPEC);
														}
														return ((obj_t) BgL_new1249z00_1875);
													}
												}
										}
									else
										{	/* Cgen/capp.scm 405 */
											BgL_copz00_bglt BgL_copz00_1898;

											{	/* Cgen/capp.scm 405 */
												BgL_setqz00_bglt BgL_arg1748z00_1923;

												{	/* Cgen/capp.scm 405 */
													obj_t BgL_arg1749z00_1924;

													BgL_arg1749z00_1924 =
														CAR(((obj_t) BgL_oldzd2actualszd2_1860));
													BgL_arg1748z00_1923 =
														BGl_nodezd2setqzd2zzcgen_cgenz00(
														((BgL_variablez00_bglt) BgL_auxz00_1863),
														((BgL_nodez00_bglt) BgL_arg1749z00_1924));
												}
												BgL_copz00_1898 =
													BGl_nodezd2ze3copz31zzcgen_cgenz00(
													((BgL_nodez00_bglt) BgL_arg1748z00_1923),
													BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
													CBOOL(BgL_inpushexitz00_24));
											}
											{	/* Cgen/capp.scm 407 */
												bool_t BgL_test2191z00_4477;

												BgL_copz00_1927 = BgL_copz00_1898;
												BgL_auxz00_1928 = BgL_auxz00_1863;
												{	/* Cgen/capp.scm 354 */
													bool_t BgL_test2192z00_4478;

													{	/* Cgen/capp.scm 354 */
														obj_t BgL_classz00_2714;

														BgL_classz00_2714 = BGl_csetqz00zzcgen_copz00;
														{	/* Cgen/capp.scm 354 */
															BgL_objectz00_bglt BgL_arg1807z00_2716;

															{	/* Cgen/capp.scm 354 */
																obj_t BgL_tmpz00_4479;

																BgL_tmpz00_4479 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_copz00_1927));
																BgL_arg1807z00_2716 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4479);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Cgen/capp.scm 354 */
																	long BgL_idxz00_2722;

																	BgL_idxz00_2722 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2716);
																	BgL_test2192z00_4478 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2722 + 2L)) ==
																		BgL_classz00_2714);
																}
															else
																{	/* Cgen/capp.scm 354 */
																	bool_t BgL_res2119z00_2747;

																	{	/* Cgen/capp.scm 354 */
																		obj_t BgL_oclassz00_2730;

																		{	/* Cgen/capp.scm 354 */
																			obj_t BgL_arg1815z00_2738;
																			long BgL_arg1816z00_2739;

																			BgL_arg1815z00_2738 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Cgen/capp.scm 354 */
																				long BgL_arg1817z00_2740;

																				BgL_arg1817z00_2740 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2716);
																				BgL_arg1816z00_2739 =
																					(BgL_arg1817z00_2740 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2730 =
																				VECTOR_REF(BgL_arg1815z00_2738,
																				BgL_arg1816z00_2739);
																		}
																		{	/* Cgen/capp.scm 354 */
																			bool_t BgL__ortest_1115z00_2731;

																			BgL__ortest_1115z00_2731 =
																				(BgL_classz00_2714 ==
																				BgL_oclassz00_2730);
																			if (BgL__ortest_1115z00_2731)
																				{	/* Cgen/capp.scm 354 */
																					BgL_res2119z00_2747 =
																						BgL__ortest_1115z00_2731;
																				}
																			else
																				{	/* Cgen/capp.scm 354 */
																					long BgL_odepthz00_2732;

																					{	/* Cgen/capp.scm 354 */
																						obj_t BgL_arg1804z00_2733;

																						BgL_arg1804z00_2733 =
																							(BgL_oclassz00_2730);
																						BgL_odepthz00_2732 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2733);
																					}
																					if ((2L < BgL_odepthz00_2732))
																						{	/* Cgen/capp.scm 354 */
																							obj_t BgL_arg1802z00_2735;

																							{	/* Cgen/capp.scm 354 */
																								obj_t BgL_arg1803z00_2736;

																								BgL_arg1803z00_2736 =
																									(BgL_oclassz00_2730);
																								BgL_arg1802z00_2735 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2736, 2L);
																							}
																							BgL_res2119z00_2747 =
																								(BgL_arg1802z00_2735 ==
																								BgL_classz00_2714);
																						}
																					else
																						{	/* Cgen/capp.scm 354 */
																							BgL_res2119z00_2747 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2192z00_4478 = BgL_res2119z00_2747;
																}
														}
													}
													if (BgL_test2192z00_4478)
														{	/* Cgen/capp.scm 354 */
															if (
																(((obj_t)
																		(((BgL_varcz00_bglt) COBJECT(
																					(((BgL_csetqz00_bglt) COBJECT(
																								((BgL_csetqz00_bglt)
																									BgL_copz00_1927)))->
																						BgL_varz00)))->BgL_variablez00)) ==
																	((obj_t) BgL_auxz00_1928)))
																{	/* Cgen/capp.scm 356 */
																	bool_t BgL__ortest_1239z00_1932;

																	BgL__ortest_1239z00_1932 =
																		BGl_globalzd2argszd2safezf3zf3zzast_varz00(
																		((BgL_globalz00_bglt) BgL_varz00_21));
																	if (BgL__ortest_1239z00_1932)
																		{	/* Cgen/capp.scm 356 */
																			BgL_test2191z00_4477 =
																				BgL__ortest_1239z00_1932;
																		}
																	else
																		{	/* Cgen/capp.scm 357 */
																			bool_t BgL__ortest_1240z00_1933;

																			{	/* Cgen/capp.scm 357 */
																				BgL_copz00_bglt BgL_arg1753z00_1935;

																				BgL_arg1753z00_1935 =
																					(((BgL_csetqz00_bglt) COBJECT(
																							((BgL_csetqz00_bglt)
																								BgL_copz00_1927)))->
																					BgL_valuez00);
																				{	/* Cgen/capp.scm 357 */
																					obj_t BgL_classz00_2751;

																					BgL_classz00_2751 =
																						BGl_catomz00zzcgen_copz00;
																					{	/* Cgen/capp.scm 357 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2753;
																						{	/* Cgen/capp.scm 357 */
																							obj_t BgL_tmpz00_4514;

																							BgL_tmpz00_4514 =
																								((obj_t)
																								((BgL_objectz00_bglt)
																									BgL_arg1753z00_1935));
																							BgL_arg1807z00_2753 =
																								(BgL_objectz00_bglt)
																								(BgL_tmpz00_4514);
																						}
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Cgen/capp.scm 357 */
																								long BgL_idxz00_2759;

																								BgL_idxz00_2759 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2753);
																								BgL__ortest_1240z00_1933 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2759 + 2L)) ==
																									BgL_classz00_2751);
																							}
																						else
																							{	/* Cgen/capp.scm 357 */
																								bool_t BgL_res2120z00_2784;

																								{	/* Cgen/capp.scm 357 */
																									obj_t BgL_oclassz00_2767;

																									{	/* Cgen/capp.scm 357 */
																										obj_t BgL_arg1815z00_2775;
																										long BgL_arg1816z00_2776;

																										BgL_arg1815z00_2775 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Cgen/capp.scm 357 */
																											long BgL_arg1817z00_2777;

																											BgL_arg1817z00_2777 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2753);
																											BgL_arg1816z00_2776 =
																												(BgL_arg1817z00_2777 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2767 =
																											VECTOR_REF
																											(BgL_arg1815z00_2775,
																											BgL_arg1816z00_2776);
																									}
																									{	/* Cgen/capp.scm 357 */
																										bool_t
																											BgL__ortest_1115z00_2768;
																										BgL__ortest_1115z00_2768 =
																											(BgL_classz00_2751 ==
																											BgL_oclassz00_2767);
																										if (BgL__ortest_1115z00_2768)
																											{	/* Cgen/capp.scm 357 */
																												BgL_res2120z00_2784 =
																													BgL__ortest_1115z00_2768;
																											}
																										else
																											{	/* Cgen/capp.scm 357 */
																												long BgL_odepthz00_2769;

																												{	/* Cgen/capp.scm 357 */
																													obj_t
																														BgL_arg1804z00_2770;
																													BgL_arg1804z00_2770 =
																														(BgL_oclassz00_2767);
																													BgL_odepthz00_2769 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2770);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_2769))
																													{	/* Cgen/capp.scm 357 */
																														obj_t
																															BgL_arg1802z00_2772;
																														{	/* Cgen/capp.scm 357 */
																															obj_t
																																BgL_arg1803z00_2773;
																															BgL_arg1803z00_2773
																																=
																																(BgL_oclassz00_2767);
																															BgL_arg1802z00_2772
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2773,
																																2L);
																														}
																														BgL_res2120z00_2784
																															=
																															(BgL_arg1802z00_2772
																															==
																															BgL_classz00_2751);
																													}
																												else
																													{	/* Cgen/capp.scm 357 */
																														BgL_res2120z00_2784
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL__ortest_1240z00_1933 =
																									BgL_res2120z00_2784;
																							}
																					}
																				}
																			}
																			if (BgL__ortest_1240z00_1933)
																				{	/* Cgen/capp.scm 357 */
																					BgL_test2191z00_4477 =
																						BgL__ortest_1240z00_1933;
																				}
																			else
																				{	/* Cgen/capp.scm 358 */
																					BgL_copz00_bglt BgL_arg1752z00_1934;

																					BgL_arg1752z00_1934 =
																						(((BgL_csetqz00_bglt) COBJECT(
																								((BgL_csetqz00_bglt)
																									BgL_copz00_1927)))->
																						BgL_valuez00);
																					{	/* Cgen/capp.scm 358 */
																						obj_t BgL_classz00_2786;

																						BgL_classz00_2786 =
																							BGl_varcz00zzcgen_copz00;
																						{	/* Cgen/capp.scm 358 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_2788;
																							{	/* Cgen/capp.scm 358 */
																								obj_t BgL_tmpz00_4540;

																								BgL_tmpz00_4540 =
																									((obj_t)
																									((BgL_objectz00_bglt)
																										BgL_arg1752z00_1934));
																								BgL_arg1807z00_2788 =
																									(BgL_objectz00_bglt)
																									(BgL_tmpz00_4540);
																							}
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* Cgen/capp.scm 358 */
																									long BgL_idxz00_2794;

																									BgL_idxz00_2794 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_2788);
																									BgL_test2191z00_4477 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_2794 + 2L)) ==
																										BgL_classz00_2786);
																								}
																							else
																								{	/* Cgen/capp.scm 358 */
																									bool_t BgL_res2121z00_2819;

																									{	/* Cgen/capp.scm 358 */
																										obj_t BgL_oclassz00_2802;

																										{	/* Cgen/capp.scm 358 */
																											obj_t BgL_arg1815z00_2810;
																											long BgL_arg1816z00_2811;

																											BgL_arg1815z00_2810 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* Cgen/capp.scm 358 */
																												long
																													BgL_arg1817z00_2812;
																												BgL_arg1817z00_2812 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_2788);
																												BgL_arg1816z00_2811 =
																													(BgL_arg1817z00_2812 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_2802 =
																												VECTOR_REF
																												(BgL_arg1815z00_2810,
																												BgL_arg1816z00_2811);
																										}
																										{	/* Cgen/capp.scm 358 */
																											bool_t
																												BgL__ortest_1115z00_2803;
																											BgL__ortest_1115z00_2803 =
																												(BgL_classz00_2786 ==
																												BgL_oclassz00_2802);
																											if (BgL__ortest_1115z00_2803)
																												{	/* Cgen/capp.scm 358 */
																													BgL_res2121z00_2819 =
																														BgL__ortest_1115z00_2803;
																												}
																											else
																												{	/* Cgen/capp.scm 358 */
																													long
																														BgL_odepthz00_2804;
																													{	/* Cgen/capp.scm 358 */
																														obj_t
																															BgL_arg1804z00_2805;
																														BgL_arg1804z00_2805
																															=
																															(BgL_oclassz00_2802);
																														BgL_odepthz00_2804 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_2805);
																													}
																													if (
																														(2L <
																															BgL_odepthz00_2804))
																														{	/* Cgen/capp.scm 358 */
																															obj_t
																																BgL_arg1802z00_2807;
																															{	/* Cgen/capp.scm 358 */
																																obj_t
																																	BgL_arg1803z00_2808;
																																BgL_arg1803z00_2808
																																	=
																																	(BgL_oclassz00_2802);
																																BgL_arg1802z00_2807
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_2808,
																																	2L);
																															}
																															BgL_res2121z00_2819
																																=
																																(BgL_arg1802z00_2807
																																==
																																BgL_classz00_2786);
																														}
																													else
																														{	/* Cgen/capp.scm 358 */
																															BgL_res2121z00_2819
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test2191z00_4477 =
																										BgL_res2121z00_2819;
																								}
																						}
																					}
																				}
																		}
																}
															else
																{	/* Cgen/capp.scm 355 */
																	BgL_test2191z00_4477 = ((bool_t) 0);
																}
														}
													else
														{	/* Cgen/capp.scm 354 */
															BgL_test2191z00_4477 = ((bool_t) 0);
														}
												}
												if (BgL_test2191z00_4477)
													{	/* Cgen/capp.scm 408 */
														obj_t BgL_arg1705z00_1900;
														obj_t BgL_arg1708z00_1901;
														obj_t BgL_arg1709z00_1902;

														BgL_arg1705z00_1900 =
															CDR(((obj_t) BgL_oldzd2actualszd2_1860));
														if (NULLP(CDR(((obj_t) BgL_argszd2typezd2_1861))))
															{	/* Cgen/capp.scm 409 */
																BgL_arg1708z00_1901 = BgL_argszd2typezd2_1861;
															}
														else
															{	/* Cgen/capp.scm 409 */
																BgL_arg1708z00_1901 =
																	CDR(((obj_t) BgL_argszd2typezd2_1861));
															}
														{	/* Cgen/capp.scm 412 */
															BgL_copz00_bglt BgL_arg1717z00_1906;

															BgL_arg1717z00_1906 =
																(((BgL_csetqz00_bglt) COBJECT(
																		((BgL_csetqz00_bglt) BgL_copz00_1898)))->
																BgL_valuez00);
															BgL_arg1709z00_1902 =
																MAKE_YOUNG_PAIR(((obj_t) BgL_arg1717z00_1906),
																BgL_newzd2actualszd2_1862);
														}
														{
															obj_t BgL_newzd2actualszd2_4577;
															obj_t BgL_argszd2typezd2_4576;
															obj_t BgL_oldzd2actualszd2_4575;

															BgL_oldzd2actualszd2_4575 = BgL_arg1705z00_1900;
															BgL_argszd2typezd2_4576 = BgL_arg1708z00_1901;
															BgL_newzd2actualszd2_4577 = BgL_arg1709z00_1902;
															BgL_newzd2actualszd2_1862 =
																BgL_newzd2actualszd2_4577;
															BgL_argszd2typezd2_1861 = BgL_argszd2typezd2_4576;
															BgL_oldzd2actualszd2_1860 =
																BgL_oldzd2actualszd2_4575;
															goto BgL_zc3z04anonymousza31651ze3z87_1866;
														}
													}
												else
													{	/* Cgen/capp.scm 407 */
														{	/* Cgen/capp.scm 417 */
															obj_t BgL_arg1718z00_1907;

															BgL_arg1718z00_1907 =
																CAR(((obj_t) BgL_argszd2typezd2_1861));
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_auxz00_1863)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BgL_arg1718z00_1907)), BUNSPEC);
														}
														{	/* Cgen/capp.scm 418 */
															obj_t BgL_arg1720z00_1908;
															obj_t BgL_arg1722z00_1909;
															obj_t BgL_arg1724z00_1910;
															BgL_localz00_bglt BgL_arg1733z00_1911;
															obj_t BgL_arg1734z00_1912;
															obj_t BgL_arg1735z00_1913;

															BgL_arg1720z00_1908 =
																CDR(((obj_t) BgL_oldzd2actualszd2_1860));
															if (NULLP(CDR(((obj_t) BgL_argszd2typezd2_1861))))
																{	/* Cgen/capp.scm 419 */
																	BgL_arg1722z00_1909 = BgL_argszd2typezd2_1861;
																}
															else
																{	/* Cgen/capp.scm 419 */
																	BgL_arg1722z00_1909 =
																		CDR(((obj_t) BgL_argszd2typezd2_1861));
																}
															{	/* Cgen/capp.scm 422 */
																BgL_varcz00_bglt BgL_arg1739z00_1917;

																{	/* Cgen/capp.scm 422 */
																	BgL_varcz00_bglt BgL_new1261z00_1918;

																	{	/* Cgen/capp.scm 425 */
																		BgL_varcz00_bglt BgL_new1260z00_1920;

																		BgL_new1260z00_1920 =
																			((BgL_varcz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_varcz00_bgl))));
																		{	/* Cgen/capp.scm 425 */
																			long BgL_arg1746z00_1921;

																			BgL_arg1746z00_1921 =
																				BGL_CLASS_NUM(BGl_varcz00zzcgen_copz00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1260z00_1920),
																				BgL_arg1746z00_1921);
																		}
																		BgL_new1261z00_1918 = BgL_new1260z00_1920;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1261z00_1918)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							CAR(((obj_t)
																									BgL_oldzd2actualszd2_1860)))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1261z00_1918)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt)
																							BgL_auxz00_1863)))->BgL_typez00)),
																		BUNSPEC);
																	((((BgL_varcz00_bglt)
																				COBJECT(BgL_new1261z00_1918))->
																			BgL_variablez00) =
																		((BgL_variablez00_bglt) (
																				(BgL_variablez00_bglt)
																				BgL_auxz00_1863)), BUNSPEC);
																	BgL_arg1739z00_1917 = BgL_new1261z00_1918;
																}
																BgL_arg1724z00_1910 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1739z00_1917),
																	BgL_newzd2actualszd2_1862);
															}
															{	/* Cgen/capp.scm 427 */
																obj_t BgL_arg1747z00_1922;

																BgL_arg1747z00_1922 =
																	CAR(((obj_t) BgL_argszd2typezd2_1861));
																BgL_arg1733z00_1911 =
																	BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00
																	(CNST_TABLE_REF(0),
																	((BgL_typez00_bglt) BgL_arg1747z00_1922));
															}
															BgL_arg1734z00_1912 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_auxz00_1863), BgL_auxsz00_1864);
															BgL_arg1735z00_1913 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_copz00_1898), BgL_expsz00_1865);
															{
																obj_t BgL_expsz00_4623;
																obj_t BgL_auxsz00_4622;
																BgL_localz00_bglt BgL_auxz00_4621;
																obj_t BgL_newzd2actualszd2_4620;
																obj_t BgL_argszd2typezd2_4619;
																obj_t BgL_oldzd2actualszd2_4618;

																BgL_oldzd2actualszd2_4618 = BgL_arg1720z00_1908;
																BgL_argszd2typezd2_4619 = BgL_arg1722z00_1909;
																BgL_newzd2actualszd2_4620 = BgL_arg1724z00_1910;
																BgL_auxz00_4621 = BgL_arg1733z00_1911;
																BgL_auxsz00_4622 = BgL_arg1734z00_1912;
																BgL_expsz00_4623 = BgL_arg1735z00_1913;
																BgL_expsz00_1865 = BgL_expsz00_4623;
																BgL_auxsz00_1864 = BgL_auxsz00_4622;
																BgL_auxz00_1863 = BgL_auxz00_4621;
																BgL_newzd2actualszd2_1862 =
																	BgL_newzd2actualszd2_4620;
																BgL_argszd2typezd2_1861 =
																	BgL_argszd2typezd2_4619;
																BgL_oldzd2actualszd2_1860 =
																	BgL_oldzd2actualszd2_4618;
																goto BgL_zc3z04anonymousza31651ze3z87_1866;
															}
														}
													}
											}
										}
								}
							}
						}
					}
			}
		}

	}



/* node-tail-app->cop */
	obj_t BGl_nodezd2tailzd2appzd2ze3copz31zzcgen_cappz00(BgL_variablez00_bglt
		BgL_varz00_25, BgL_appz00_bglt BgL_nodez00_26, obj_t BgL_kontz00_27,
		obj_t BgL_inpushexitz00_28)
	{
		{	/* Cgen/capp.scm 439 */
			{	/* Cgen/capp.scm 445 */
				BgL_clabelz00_bglt BgL_labelz00_1939;
				obj_t BgL_argsz00_1940;
				obj_t BgL_locz00_1941;

				{	/* Cgen/capp.scm 445 */
					BgL_sfunz00_bglt BgL_oz00_2868;

					BgL_oz00_2868 =
						((BgL_sfunz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_25))->BgL_valuez00));
					{
						BgL_sfunzf2czf2_bglt BgL_auxz00_4626;

						{
							obj_t BgL_auxz00_4627;

							{	/* Cgen/capp.scm 445 */
								BgL_objectz00_bglt BgL_tmpz00_4628;

								BgL_tmpz00_4628 = ((BgL_objectz00_bglt) BgL_oz00_2868);
								BgL_auxz00_4627 = BGL_OBJECT_WIDENING(BgL_tmpz00_4628);
							}
							BgL_auxz00_4626 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_4627);
						}
						BgL_labelz00_1939 =
							(((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_4626))->BgL_labelz00);
					}
				}
				BgL_argsz00_1940 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(BgL_varz00_25))->
									BgL_valuez00))))->BgL_argsz00);
				BgL_locz00_1941 =
					(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) BgL_nodez00_26)))->
					BgL_locz00);
				{	/* Cgen/capp.scm 448 */
					bool_t BgL_test2207z00_4638;

					{	/* Cgen/capp.scm 448 */
						BgL_sfunz00_bglt BgL_oz00_2874;

						BgL_oz00_2874 =
							((BgL_sfunz00_bglt)
							(((BgL_variablez00_bglt) COBJECT(BgL_varz00_25))->BgL_valuez00));
						{
							BgL_sfunzf2czf2_bglt BgL_auxz00_4641;

							{
								obj_t BgL_auxz00_4642;

								{	/* Cgen/capp.scm 448 */
									BgL_objectz00_bglt BgL_tmpz00_4643;

									BgL_tmpz00_4643 = ((BgL_objectz00_bglt) BgL_oz00_2874);
									BgL_auxz00_4642 = BGL_OBJECT_WIDENING(BgL_tmpz00_4643);
								}
								BgL_auxz00_4641 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_4642);
							}
							BgL_test2207z00_4638 =
								(((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_4641))->
								BgL_integratedz00);
						}
					}
					if (BgL_test2207z00_4638)
						{	/* Cgen/capp.scm 448 */
							if (NULLP(BgL_argsz00_1940))
								{	/* Cgen/capp.scm 472 */
									((((BgL_clabelz00_bglt) COBJECT(BgL_labelz00_1939))->
											BgL_usedzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
									{	/* Cgen/capp.scm 475 */
										BgL_cgotoz00_bglt BgL_new1267z00_1945;

										{	/* Cgen/capp.scm 477 */
											BgL_cgotoz00_bglt BgL_new1266z00_1946;

											BgL_new1266z00_1946 =
												((BgL_cgotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_cgotoz00_bgl))));
											{	/* Cgen/capp.scm 477 */
												long BgL_arg1765z00_1947;

												BgL_arg1765z00_1947 =
													BGL_CLASS_NUM(BGl_cgotoz00zzcgen_copz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1266z00_1946),
													BgL_arg1765z00_1947);
											}
											BgL_new1267z00_1945 = BgL_new1266z00_1946;
										}
										((((BgL_copz00_bglt) COBJECT(
														((BgL_copz00_bglt) BgL_new1267z00_1945)))->
												BgL_locz00) = ((obj_t) BgL_locz00_1941), BUNSPEC);
										((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
															BgL_new1267z00_1945)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
										((((BgL_cgotoz00_bglt) COBJECT(BgL_new1267z00_1945))->
												BgL_labelz00) =
											((BgL_clabelz00_bglt) BgL_labelz00_1939), BUNSPEC);
										return ((obj_t) BgL_new1267z00_1945);
									}
								}
							else
								{	/* Cgen/capp.scm 479 */
									obj_t BgL_g1270z00_1948;
									obj_t BgL_g1273z00_1951;

									BgL_g1270z00_1948 =
										(((BgL_appz00_bglt) COBJECT(BgL_nodez00_26))->BgL_argsz00);
									{	/* Cgen/capp.scm 484 */
										BgL_cgotoz00_bglt BgL_arg1844z00_2004;

										((((BgL_clabelz00_bglt) COBJECT(BgL_labelz00_1939))->
												BgL_usedzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
										{	/* Cgen/capp.scm 485 */
											BgL_cgotoz00_bglt BgL_new1269z00_2006;

											{	/* Cgen/capp.scm 487 */
												BgL_cgotoz00_bglt BgL_new1268z00_2007;

												BgL_new1268z00_2007 =
													((BgL_cgotoz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_cgotoz00_bgl))));
												{	/* Cgen/capp.scm 487 */
													long BgL_arg1846z00_2008;

													BgL_arg1846z00_2008 =
														BGL_CLASS_NUM(BGl_cgotoz00zzcgen_copz00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1268z00_2007),
														BgL_arg1846z00_2008);
												}
												BgL_new1269z00_2006 = BgL_new1268z00_2007;
											}
											((((BgL_copz00_bglt) COBJECT(
															((BgL_copz00_bglt) BgL_new1269z00_2006)))->
													BgL_locz00) = ((obj_t) BgL_locz00_1941), BUNSPEC);
											((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																BgL_new1269z00_2006)))->BgL_typez00) =
												((BgL_typez00_bglt) ((BgL_typez00_bglt)
														BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
											((((BgL_cgotoz00_bglt) COBJECT(BgL_new1269z00_2006))->
													BgL_labelz00) =
												((BgL_clabelz00_bglt) BgL_labelz00_1939), BUNSPEC);
											BgL_arg1844z00_2004 = BgL_new1269z00_2006;
										}
										{	/* Cgen/capp.scm 483 */
											obj_t BgL_list1845z00_2005;

											BgL_list1845z00_2005 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1844z00_2004), BNIL);
											BgL_g1273z00_1951 = BgL_list1845z00_2005;
									}}
									{
										obj_t BgL_argsz00_1953;
										obj_t BgL_actualsz00_1954;
										obj_t BgL_auxsz00_1955;
										obj_t BgL_seq1z00_1956;
										obj_t BgL_seq2z00_1957;

										BgL_argsz00_1953 = BgL_argsz00_1940;
										BgL_actualsz00_1954 = BgL_g1270z00_1948;
										BgL_auxsz00_1955 = BNIL;
										BgL_seq1z00_1956 = BNIL;
										BgL_seq2z00_1957 = BgL_g1273z00_1951;
									BgL_zc3z04anonymousza31766ze3z87_1958:
										if (NULLP(BgL_argsz00_1953))
											{	/* Cgen/capp.scm 489 */
												if (NULLP(BgL_seq1z00_1956))
													{	/* Cgen/capp.scm 491 */
														BgL_seq1z00_1956 = BgL_seq2z00_1957;
													}
												else
													{	/* Cgen/capp.scm 491 */
														BgL_seq1z00_1956 =
															bgl_reverse_bang(BgL_seq1z00_1956);
														{	/* Cgen/capp.scm 495 */
															obj_t BgL_tmpz00_4681;

															BgL_tmpz00_4681 =
																BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																(BgL_seq1z00_1956);
															SET_CDR(BgL_tmpz00_4681, BgL_seq2z00_1957);
														}
													}
												{	/* Cgen/capp.scm 498 */
													BgL_copz00_bglt BgL_arg1771z00_1962;

													{	/* Cgen/capp.scm 498 */
														BgL_csequencez00_bglt BgL_arg1773z00_1963;

														{	/* Cgen/capp.scm 498 */
															BgL_csequencez00_bglt BgL_new1275z00_1964;

															{	/* Cgen/capp.scm 500 */
																BgL_csequencez00_bglt BgL_new1274z00_1969;

																BgL_new1274z00_1969 =
																	((BgL_csequencez00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_csequencez00_bgl))));
																{	/* Cgen/capp.scm 500 */
																	long BgL_arg1799z00_1970;

																	BgL_arg1799z00_1970 =
																		BGL_CLASS_NUM
																		(BGl_csequencez00zzcgen_copz00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			BgL_new1274z00_1969),
																		BgL_arg1799z00_1970);
																}
																BgL_new1275z00_1964 = BgL_new1274z00_1969;
															}
															((((BgL_copz00_bglt) COBJECT(
																			((BgL_copz00_bglt)
																				BgL_new1275z00_1964)))->BgL_locz00) =
																((obj_t) BgL_locz00_1941), BUNSPEC);
															((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																				BgL_new1275z00_1964)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
															((((BgL_csequencez00_bglt)
																		COBJECT(BgL_new1275z00_1964))->
																	BgL_czd2expzf3z21) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
															{
																obj_t BgL_auxz00_4694;

																{	/* Cgen/capp.scm 501 */
																	BgL_localzd2varzd2_bglt BgL_arg1775z00_1965;

																	{	/* Cgen/capp.scm 501 */
																		BgL_localzd2varzd2_bglt BgL_new1277z00_1966;

																		{	/* Cgen/capp.scm 504 */
																			BgL_localzd2varzd2_bglt
																				BgL_new1276z00_1967;
																			BgL_new1276z00_1967 =
																				((BgL_localzd2varzd2_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_localzd2varzd2_bgl))));
																			{	/* Cgen/capp.scm 504 */
																				long BgL_arg1798z00_1968;

																				{	/* Cgen/capp.scm 504 */
																					obj_t BgL_classz00_2893;

																					BgL_classz00_2893 =
																						BGl_localzd2varzd2zzcgen_copz00;
																					BgL_arg1798z00_1968 =
																						BGL_CLASS_NUM(BgL_classz00_2893);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1276z00_1967),
																					BgL_arg1798z00_1968);
																			}
																			BgL_new1277z00_1966 = BgL_new1276z00_1967;
																		}
																		((((BgL_copz00_bglt) COBJECT(
																						((BgL_copz00_bglt)
																							BgL_new1277z00_1966)))->
																				BgL_locz00) =
																			((obj_t) BgL_locz00_1941), BUNSPEC);
																		((((BgL_copz00_bglt)
																					COBJECT(((BgL_copz00_bglt)
																							BgL_new1277z00_1966)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2objza2z00zztype_cachez00)),
																			BUNSPEC);
																		((((BgL_localzd2varzd2_bglt)
																					COBJECT(BgL_new1277z00_1966))->
																				BgL_varsz00) =
																			((obj_t) BgL_auxsz00_1955), BUNSPEC);
																		BgL_arg1775z00_1965 = BgL_new1277z00_1966;
																	}
																	BgL_auxz00_4694 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1775z00_1965),
																		BgL_seq1z00_1956);
																}
																((((BgL_csequencez00_bglt)
																			COBJECT(BgL_new1275z00_1964))->
																		BgL_copsz00) =
																	((obj_t) BgL_auxz00_4694), BUNSPEC);
															}
															BgL_arg1773z00_1963 = BgL_new1275z00_1964;
														}
														BgL_arg1771z00_1962 =
															BGl_bdbzd2letzd2varz00zzcgen_cgenz00(
															((BgL_copz00_bglt) BgL_arg1773z00_1963),
															BgL_locz00_1941);
													}
													return
														BGl_blockzd2kontzd2zzcgen_cgenz00(
														((obj_t) BgL_arg1771z00_1962), BFALSE);
												}
											}
										else
											{	/* Cgen/capp.scm 508 */
												obj_t BgL_argz00_1971;
												obj_t BgL_actz00_1972;

												BgL_argz00_1971 = CAR(((obj_t) BgL_argsz00_1953));
												BgL_actz00_1972 = CAR(((obj_t) BgL_actualsz00_1954));
												{	/* Cgen/capp.scm 513 */
													bool_t BgL_test2211z00_4716;

													{	/* Cgen/capp.scm 513 */
														bool_t BgL_test2212z00_4717;

														{	/* Cgen/capp.scm 513 */
															obj_t BgL_classz00_2898;

															BgL_classz00_2898 = BGl_varz00zzast_nodez00;
															if (BGL_OBJECTP(BgL_actz00_1972))
																{	/* Cgen/capp.scm 513 */
																	BgL_objectz00_bglt BgL_arg1807z00_2900;

																	BgL_arg1807z00_2900 =
																		(BgL_objectz00_bglt) (BgL_actz00_1972);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Cgen/capp.scm 513 */
																			long BgL_idxz00_2906;

																			BgL_idxz00_2906 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2900);
																			BgL_test2212z00_4717 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2906 + 2L)) ==
																				BgL_classz00_2898);
																		}
																	else
																		{	/* Cgen/capp.scm 513 */
																			bool_t BgL_res2124z00_2931;

																			{	/* Cgen/capp.scm 513 */
																				obj_t BgL_oclassz00_2914;

																				{	/* Cgen/capp.scm 513 */
																					obj_t BgL_arg1815z00_2922;
																					long BgL_arg1816z00_2923;

																					BgL_arg1815z00_2922 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Cgen/capp.scm 513 */
																						long BgL_arg1817z00_2924;

																						BgL_arg1817z00_2924 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2900);
																						BgL_arg1816z00_2923 =
																							(BgL_arg1817z00_2924 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2914 =
																						VECTOR_REF(BgL_arg1815z00_2922,
																						BgL_arg1816z00_2923);
																				}
																				{	/* Cgen/capp.scm 513 */
																					bool_t BgL__ortest_1115z00_2915;

																					BgL__ortest_1115z00_2915 =
																						(BgL_classz00_2898 ==
																						BgL_oclassz00_2914);
																					if (BgL__ortest_1115z00_2915)
																						{	/* Cgen/capp.scm 513 */
																							BgL_res2124z00_2931 =
																								BgL__ortest_1115z00_2915;
																						}
																					else
																						{	/* Cgen/capp.scm 513 */
																							long BgL_odepthz00_2916;

																							{	/* Cgen/capp.scm 513 */
																								obj_t BgL_arg1804z00_2917;

																								BgL_arg1804z00_2917 =
																									(BgL_oclassz00_2914);
																								BgL_odepthz00_2916 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2917);
																							}
																							if ((2L < BgL_odepthz00_2916))
																								{	/* Cgen/capp.scm 513 */
																									obj_t BgL_arg1802z00_2919;

																									{	/* Cgen/capp.scm 513 */
																										obj_t BgL_arg1803z00_2920;

																										BgL_arg1803z00_2920 =
																											(BgL_oclassz00_2914);
																										BgL_arg1802z00_2919 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2920, 2L);
																									}
																									BgL_res2124z00_2931 =
																										(BgL_arg1802z00_2919 ==
																										BgL_classz00_2898);
																								}
																							else
																								{	/* Cgen/capp.scm 513 */
																									BgL_res2124z00_2931 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2212z00_4717 =
																				BgL_res2124z00_2931;
																		}
																}
															else
																{	/* Cgen/capp.scm 513 */
																	BgL_test2212z00_4717 = ((bool_t) 0);
																}
														}
														if (BgL_test2212z00_4717)
															{	/* Cgen/capp.scm 513 */
																BgL_test2211z00_4716 =
																	(BgL_argz00_1971 ==
																	((obj_t)
																		(((BgL_varz00_bglt) COBJECT(
																					((BgL_varz00_bglt)
																						BgL_actz00_1972)))->
																			BgL_variablez00)));
															}
														else
															{	/* Cgen/capp.scm 513 */
																BgL_test2211z00_4716 = ((bool_t) 0);
															}
													}
													if (BgL_test2211z00_4716)
														{	/* Cgen/capp.scm 514 */
															obj_t BgL_arg1808z00_1976;
															obj_t BgL_arg1812z00_1977;

															BgL_arg1808z00_1976 =
																CDR(((obj_t) BgL_argsz00_1953));
															BgL_arg1812z00_1977 =
																CDR(((obj_t) BgL_actualsz00_1954));
															{
																obj_t BgL_actualsz00_4749;
																obj_t BgL_argsz00_4748;

																BgL_argsz00_4748 = BgL_arg1808z00_1976;
																BgL_actualsz00_4749 = BgL_arg1812z00_1977;
																BgL_actualsz00_1954 = BgL_actualsz00_4749;
																BgL_argsz00_1953 = BgL_argsz00_4748;
																goto BgL_zc3z04anonymousza31766ze3z87_1958;
															}
														}
													else
														{	/* Cgen/capp.scm 519 */
															BgL_localz00_bglt BgL_auxz00_1978;

															{	/* Cgen/capp.scm 520 */
																obj_t BgL_arg1840z00_1999;
																BgL_typez00_bglt BgL_arg1842z00_2000;

																BgL_arg1840z00_1999 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_argz00_1971))))->BgL_idz00);
																BgL_arg1842z00_2000 =
																	(((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt) (
																					(BgL_localz00_bglt)
																					BgL_argz00_1971))))->BgL_typez00);
																BgL_auxz00_1978 =
																	BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00
																	(BgL_arg1840z00_1999, BgL_arg1842z00_2000);
															}
															{	/* Cgen/capp.scm 522 */
																obj_t BgL_arg1820z00_1979;
																obj_t BgL_arg1822z00_1980;
																obj_t BgL_arg1823z00_1981;
																obj_t BgL_arg1831z00_1982;
																obj_t BgL_arg1832z00_1983;

																BgL_arg1820z00_1979 =
																	CDR(((obj_t) BgL_argsz00_1953));
																BgL_arg1822z00_1980 =
																	CDR(((obj_t) BgL_actualsz00_1954));
																BgL_arg1823z00_1981 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_auxz00_1978), BgL_auxsz00_1955);
																{	/* Cgen/capp.scm 525 */
																	BgL_copz00_bglt BgL_arg1833z00_1984;

																	{	/* Cgen/capp.scm 525 */
																		BgL_setqz00_bglt BgL_arg1834z00_1985;

																		BgL_arg1834z00_1985 =
																			BGl_nodezd2setqzd2zzcgen_cgenz00(
																			((BgL_variablez00_bglt) BgL_auxz00_1978),
																			((BgL_nodez00_bglt) BgL_actz00_1972));
																		BgL_arg1833z00_1984 =
																			BGl_nodezd2ze3copz31zzcgen_cgenz00(
																			((BgL_nodez00_bglt) BgL_arg1834z00_1985),
																			BGl_za2stopzd2kontza2zd2zzcgen_cgenz00,
																			CBOOL(BgL_inpushexitz00_28));
																	}
																	BgL_arg1831z00_1982 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1833z00_1984),
																		BgL_seq1z00_1956);
																}
																{	/* Cgen/capp.scm 529 */
																	BgL_stopz00_bglt BgL_arg1835z00_1986;

																	{	/* Cgen/capp.scm 529 */
																		BgL_stopz00_bglt BgL_new1280z00_1987;

																		{	/* Cgen/capp.scm 533 */
																			BgL_stopz00_bglt BgL_new1279z00_1997;

																			BgL_new1279z00_1997 =
																				((BgL_stopz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_stopz00_bgl))));
																			{	/* Cgen/capp.scm 533 */
																				long BgL_arg1839z00_1998;

																				BgL_arg1839z00_1998 =
																					BGL_CLASS_NUM
																					(BGl_stopz00zzcgen_copz00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1279z00_1997),
																					BgL_arg1839z00_1998);
																			}
																			BgL_new1280z00_1987 = BgL_new1279z00_1997;
																		}
																		((((BgL_copz00_bglt) COBJECT(
																						((BgL_copz00_bglt)
																							BgL_new1280z00_1987)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_actz00_1972)))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_copz00_bglt)
																					COBJECT(((BgL_copz00_bglt)
																							BgL_new1280z00_1987)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((BgL_typez00_bglt)
																					BGl_za2_za2z00zztype_cachez00)),
																			BUNSPEC);
																		{
																			BgL_copz00_bglt BgL_auxz00_4782;

																			{	/* Cgen/capp.scm 533 */
																				BgL_csetqz00_bglt BgL_new1282z00_1988;

																				{	/* Cgen/capp.scm 539 */
																					BgL_csetqz00_bglt BgL_new1281z00_1995;

																					BgL_new1281z00_1995 =
																						((BgL_csetqz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_csetqz00_bgl))));
																					{	/* Cgen/capp.scm 539 */
																						long BgL_arg1838z00_1996;

																						{	/* Cgen/capp.scm 539 */
																							obj_t BgL_classz00_2943;

																							BgL_classz00_2943 =
																								BGl_csetqz00zzcgen_copz00;
																							BgL_arg1838z00_1996 =
																								BGL_CLASS_NUM
																								(BgL_classz00_2943);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1281z00_1995),
																							BgL_arg1838z00_1996);
																					}
																					BgL_new1282z00_1988 =
																						BgL_new1281z00_1995;
																				}
																				((((BgL_copz00_bglt) COBJECT(
																								((BgL_copz00_bglt)
																									BgL_new1282z00_1988)))->
																						BgL_locz00) =
																					((obj_t) BgL_locz00_1941), BUNSPEC);
																				((((BgL_copz00_bglt)
																							COBJECT(((BgL_copz00_bglt)
																									BgL_new1282z00_1988)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) ((
																								(BgL_variablez00_bglt)
																								COBJECT(((BgL_variablez00_bglt)
																										BgL_auxz00_1978)))->
																							BgL_typez00)), BUNSPEC);
																				{
																					BgL_varcz00_bglt BgL_auxz00_4793;

																					{	/* Cgen/capp.scm 536 */
																						BgL_varcz00_bglt
																							BgL_new1284z00_1989;
																						{	/* Cgen/capp.scm 538 */
																							BgL_varcz00_bglt
																								BgL_new1283z00_1990;
																							BgL_new1283z00_1990 =
																								((BgL_varcz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_varcz00_bgl))));
																							{	/* Cgen/capp.scm 538 */
																								long BgL_arg1836z00_1991;

																								{	/* Cgen/capp.scm 538 */
																									obj_t BgL_classz00_2947;

																									BgL_classz00_2947 =
																										BGl_varcz00zzcgen_copz00;
																									BgL_arg1836z00_1991 =
																										BGL_CLASS_NUM
																										(BgL_classz00_2947);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1283z00_1990),
																									BgL_arg1836z00_1991);
																							}
																							BgL_new1284z00_1989 =
																								BgL_new1283z00_1990;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1284z00_1989)))->
																								BgL_locz00) =
																							((obj_t) BFALSE), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1284z00_1989)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) ((
																										(BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt)
																												BgL_argz00_1971)))->
																									BgL_typez00)), BUNSPEC);
																						((((BgL_varcz00_bglt)
																									COBJECT
																									(BgL_new1284z00_1989))->
																								BgL_variablez00) =
																							((BgL_variablez00_bglt) (
																									(BgL_variablez00_bglt)
																									BgL_argz00_1971)), BUNSPEC);
																						BgL_auxz00_4793 =
																							BgL_new1284z00_1989;
																					}
																					((((BgL_csetqz00_bglt)
																								COBJECT(BgL_new1282z00_1988))->
																							BgL_varz00) =
																						((BgL_varcz00_bglt)
																							BgL_auxz00_4793), BUNSPEC);
																				}
																				{
																					BgL_copz00_bglt BgL_auxz00_4807;

																					{	/* Cgen/capp.scm 539 */
																						BgL_varcz00_bglt
																							BgL_new1286z00_1992;
																						{	/* Cgen/capp.scm 542 */
																							BgL_varcz00_bglt
																								BgL_new1285z00_1993;
																							BgL_new1285z00_1993 =
																								((BgL_varcz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_varcz00_bgl))));
																							{	/* Cgen/capp.scm 542 */
																								long BgL_arg1837z00_1994;

																								{	/* Cgen/capp.scm 542 */
																									obj_t BgL_classz00_2951;

																									BgL_classz00_2951 =
																										BGl_varcz00zzcgen_copz00;
																									BgL_arg1837z00_1994 =
																										BGL_CLASS_NUM
																										(BgL_classz00_2951);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1285z00_1993),
																									BgL_arg1837z00_1994);
																							}
																							BgL_new1286z00_1992 =
																								BgL_new1285z00_1993;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1286z00_1992)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												BgL_actz00_1972)))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1286z00_1992)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) ((
																										(BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt)
																												BgL_auxz00_1978)))->
																									BgL_typez00)), BUNSPEC);
																						((((BgL_varcz00_bglt)
																									COBJECT
																									(BgL_new1286z00_1992))->
																								BgL_variablez00) =
																							((BgL_variablez00_bglt) (
																									(BgL_variablez00_bglt)
																									BgL_auxz00_1978)), BUNSPEC);
																						BgL_auxz00_4807 =
																							((BgL_copz00_bglt)
																							BgL_new1286z00_1992);
																					}
																					((((BgL_csetqz00_bglt)
																								COBJECT(BgL_new1282z00_1988))->
																							BgL_valuez00) =
																						((BgL_copz00_bglt) BgL_auxz00_4807),
																						BUNSPEC);
																				}
																				BgL_auxz00_4782 =
																					((BgL_copz00_bglt)
																					BgL_new1282z00_1988);
																			}
																			((((BgL_stopz00_bglt)
																						COBJECT(BgL_new1280z00_1987))->
																					BgL_valuez00) =
																				((BgL_copz00_bglt) BgL_auxz00_4782),
																				BUNSPEC);
																		}
																		BgL_arg1835z00_1986 = BgL_new1280z00_1987;
																	}
																	BgL_arg1832z00_1983 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1835z00_1986),
																		BgL_seq2z00_1957);
																}
																{
																	obj_t BgL_seq2z00_4832;
																	obj_t BgL_seq1z00_4831;
																	obj_t BgL_auxsz00_4830;
																	obj_t BgL_actualsz00_4829;
																	obj_t BgL_argsz00_4828;

																	BgL_argsz00_4828 = BgL_arg1820z00_1979;
																	BgL_actualsz00_4829 = BgL_arg1822z00_1980;
																	BgL_auxsz00_4830 = BgL_arg1823z00_1981;
																	BgL_seq1z00_4831 = BgL_arg1831z00_1982;
																	BgL_seq2z00_4832 = BgL_arg1832z00_1983;
																	BgL_seq2z00_1957 = BgL_seq2z00_4832;
																	BgL_seq1z00_1956 = BgL_seq1z00_4831;
																	BgL_auxsz00_1955 = BgL_auxsz00_4830;
																	BgL_actualsz00_1954 = BgL_actualsz00_4829;
																	BgL_argsz00_1953 = BgL_argsz00_4828;
																	goto BgL_zc3z04anonymousza31766ze3z87_1958;
																}
															}
														}
												}
											}
									}
								}
						}
					else
						{	/* Cgen/capp.scm 448 */
							{	/* Cgen/capp.scm 450 */
								BgL_valuez00_bglt BgL_arg1847z00_2009;

								BgL_arg1847z00_2009 =
									(((BgL_variablez00_bglt) COBJECT(BgL_varz00_25))->
									BgL_valuez00);
								{
									BgL_sfunzf2czf2_bglt BgL_auxz00_4834;

									{
										obj_t BgL_auxz00_4835;

										{	/* Cgen/capp.scm 450 */
											BgL_objectz00_bglt BgL_tmpz00_4836;

											BgL_tmpz00_4836 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_arg1847z00_2009));
											BgL_auxz00_4835 = BGL_OBJECT_WIDENING(BgL_tmpz00_4836);
										}
										BgL_auxz00_4834 = ((BgL_sfunzf2czf2_bglt) BgL_auxz00_4835);
									}
									((((BgL_sfunzf2czf2_bglt) COBJECT(BgL_auxz00_4834))->
											BgL_integratedz00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
								}
							}
							{	/* Cgen/capp.scm 451 */
								BgL_copz00_bglt BgL_bodyz00_2010;

								{	/* Cgen/capp.scm 451 */
									obj_t BgL_arg1862z00_2032;

									BgL_arg1862z00_2032 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_varz00_25))))->
														BgL_valuez00))))->BgL_bodyz00);
									BgL_bodyz00_2010 =
										BGl_nodezd2ze3copz31zzcgen_cgenz00(((BgL_nodez00_bglt)
											BgL_arg1862z00_2032), BgL_kontz00_27,
										CBOOL(BgL_inpushexitz00_28));
								}
								((((BgL_clabelz00_bglt) COBJECT(BgL_labelz00_1939))->
										BgL_bodyz00) =
									((obj_t) ((obj_t) BgL_bodyz00_2010)), BUNSPEC);
								if (NULLP(BgL_argsz00_1940))
									{	/* Cgen/capp.scm 453 */
										return ((obj_t) BgL_labelz00_1939);
									}
								else
									{	/* Cgen/capp.scm 455 */
										obj_t BgL_g1262z00_2012;

										BgL_g1262z00_2012 =
											(((BgL_appz00_bglt) COBJECT(BgL_nodez00_26))->
											BgL_argsz00);
										{
											obj_t BgL_formalsz00_2015;
											obj_t BgL_actualsz00_2016;
											obj_t BgL_seqz00_2017;

											{
												BgL_csequencez00_bglt BgL_auxz00_4856;

												BgL_formalsz00_2015 = BgL_argsz00_1940;
												BgL_actualsz00_2016 = BgL_g1262z00_2012;
												BgL_seqz00_2017 = BNIL;
											BgL_zc3z04anonymousza31849ze3z87_2018:
												if (NULLP(BgL_formalsz00_2015))
													{	/* Cgen/capp.scm 459 */
														BgL_csequencez00_bglt BgL_new1265z00_2020;

														{	/* Cgen/capp.scm 461 */
															BgL_csequencez00_bglt BgL_new1264z00_2022;

															BgL_new1264z00_2022 =
																((BgL_csequencez00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_csequencez00_bgl))));
															{	/* Cgen/capp.scm 461 */
																long BgL_arg1852z00_2023;

																BgL_arg1852z00_2023 =
																	BGL_CLASS_NUM(BGl_csequencez00zzcgen_copz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1264z00_2022),
																	BgL_arg1852z00_2023);
															}
															BgL_new1265z00_2020 = BgL_new1264z00_2022;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1265z00_2020)))->
																BgL_locz00) =
															((obj_t) BgL_locz00_1941), BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1265z00_2020)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_26)))->BgL_typez00)),
															BUNSPEC);
														((((BgL_csequencez00_bglt)
																	COBJECT(BgL_new1265z00_2020))->
																BgL_czd2expzf3z21) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
														{
															obj_t BgL_auxz00_4870;

															{	/* Cgen/capp.scm 462 */
																obj_t BgL_arg1851z00_2021;

																BgL_arg1851z00_2021 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_labelz00_1939), BgL_seqz00_2017);
																BgL_auxz00_4870 =
																	bgl_reverse_bang(BgL_arg1851z00_2021);
															}
															((((BgL_csequencez00_bglt)
																		COBJECT(BgL_new1265z00_2020))->
																	BgL_copsz00) =
																((obj_t) BgL_auxz00_4870), BUNSPEC);
														}
														BgL_auxz00_4856 = BgL_new1265z00_2020;
													}
												else
													{	/* Cgen/capp.scm 463 */
														obj_t BgL_arg1853z00_2024;
														obj_t BgL_arg1854z00_2025;
														obj_t BgL_arg1856z00_2026;

														BgL_arg1853z00_2024 =
															CDR(((obj_t) BgL_formalsz00_2015));
														BgL_arg1854z00_2025 =
															CDR(((obj_t) BgL_actualsz00_2016));
														{	/* Cgen/capp.scm 467 */
															BgL_copz00_bglt BgL_arg1857z00_2027;

															{	/* Cgen/capp.scm 467 */
																BgL_setqz00_bglt BgL_arg1858z00_2028;

																{	/* Cgen/capp.scm 467 */
																	obj_t BgL_arg1859z00_2029;
																	obj_t BgL_arg1860z00_2030;

																	BgL_arg1859z00_2029 =
																		CAR(((obj_t) BgL_formalsz00_2015));
																	BgL_arg1860z00_2030 =
																		CAR(((obj_t) BgL_actualsz00_2016));
																	BgL_arg1858z00_2028 =
																		BGl_nodezd2setqzd2zzcgen_cgenz00(
																		((BgL_variablez00_bglt)
																			BgL_arg1859z00_2029),
																		((BgL_nodez00_bglt) BgL_arg1860z00_2030));
																}
																BgL_arg1857z00_2027 =
																	BGl_nodezd2ze3copz31zzcgen_cgenz00(
																	((BgL_nodez00_bglt) BgL_arg1858z00_2028),
																	BGl_za2stopzd2kontza2zd2zzcgen_cgenz00,
																	CBOOL(BgL_inpushexitz00_28));
															}
															BgL_arg1856z00_2026 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1857z00_2027), BgL_seqz00_2017);
														}
														{
															obj_t BgL_seqz00_4893;
															obj_t BgL_actualsz00_4892;
															obj_t BgL_formalsz00_4891;

															BgL_formalsz00_4891 = BgL_arg1853z00_2024;
															BgL_actualsz00_4892 = BgL_arg1854z00_2025;
															BgL_seqz00_4893 = BgL_arg1856z00_2026;
															BgL_seqz00_2017 = BgL_seqz00_4893;
															BgL_actualsz00_2016 = BgL_actualsz00_4892;
															BgL_formalsz00_2015 = BgL_formalsz00_4891;
															goto BgL_zc3z04anonymousza31849ze3z87_2018;
														}
													}
												return ((obj_t) BgL_auxz00_4856);
											}
										}
									}
							}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcgen_cappz00(void)
	{
		{	/* Cgen/capp.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcgen_cappz00(void)
	{
		{	/* Cgen/capp.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcgen_cappz00(void)
	{
		{	/* Cgen/capp.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2156z00zzcgen_cappz00, BGl_string2157z00zzcgen_cappz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_funcallz00zzast_nodez00,
				BGl_proc2158z00zzcgen_cappz00, BGl_string2157z00zzcgen_cappz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3copzd2envze3zzcgen_cgenz00, BGl_appz00zzast_nodez00,
				BGl_proc2159z00zzcgen_cappz00, BGl_string2157z00zzcgen_cappz00);
		}

	}



/* &node->cop-app1466 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2app1466z81zzcgen_cappz00(obj_t
		BgL_envz00_3666, obj_t BgL_nodez00_3667, obj_t BgL_kontz00_3668,
		obj_t BgL_inpushexitz00_3669)
	{
		{	/* Cgen/capp.scm 232 */
			{	/* Tools/trace.sch 53 */
				BgL_variablez00_bglt BgL_varz00_3694;

				BgL_varz00_3694 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3667)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Tools/trace.sch 53 */
					bool_t BgL_test2219z00_4901;

					{	/* Tools/trace.sch 53 */
						bool_t BgL_test2220z00_4902;

						{	/* Tools/trace.sch 53 */
							obj_t BgL_classz00_3695;

							BgL_classz00_3695 = BGl_globalz00zzast_varz00;
							{	/* Tools/trace.sch 53 */
								BgL_objectz00_bglt BgL_arg1807z00_3696;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_tmpz00_4903;

									BgL_tmpz00_4903 =
										((obj_t) ((BgL_objectz00_bglt) BgL_varz00_3694));
									BgL_arg1807z00_3696 = (BgL_objectz00_bglt) (BgL_tmpz00_4903);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Tools/trace.sch 53 */
										long BgL_idxz00_3697;

										BgL_idxz00_3697 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3696);
										BgL_test2220z00_4902 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3697 + 2L)) == BgL_classz00_3695);
									}
								else
									{	/* Tools/trace.sch 53 */
										bool_t BgL_res2143z00_3700;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_oclassz00_3701;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1815z00_3702;
												long BgL_arg1816z00_3703;

												BgL_arg1815z00_3702 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Tools/trace.sch 53 */
													long BgL_arg1817z00_3704;

													BgL_arg1817z00_3704 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3696);
													BgL_arg1816z00_3703 =
														(BgL_arg1817z00_3704 - OBJECT_TYPE);
												}
												BgL_oclassz00_3701 =
													VECTOR_REF(BgL_arg1815z00_3702, BgL_arg1816z00_3703);
											}
											{	/* Tools/trace.sch 53 */
												bool_t BgL__ortest_1115z00_3705;

												BgL__ortest_1115z00_3705 =
													(BgL_classz00_3695 == BgL_oclassz00_3701);
												if (BgL__ortest_1115z00_3705)
													{	/* Tools/trace.sch 53 */
														BgL_res2143z00_3700 = BgL__ortest_1115z00_3705;
													}
												else
													{	/* Tools/trace.sch 53 */
														long BgL_odepthz00_3706;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1804z00_3707;

															BgL_arg1804z00_3707 = (BgL_oclassz00_3701);
															BgL_odepthz00_3706 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3707);
														}
														if ((2L < BgL_odepthz00_3706))
															{	/* Tools/trace.sch 53 */
																obj_t BgL_arg1802z00_3708;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_arg1803z00_3709;

																	BgL_arg1803z00_3709 = (BgL_oclassz00_3701);
																	BgL_arg1802z00_3708 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3709,
																		2L);
																}
																BgL_res2143z00_3700 =
																	(BgL_arg1802z00_3708 == BgL_classz00_3695);
															}
														else
															{	/* Tools/trace.sch 53 */
																BgL_res2143z00_3700 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2220z00_4902 = BgL_res2143z00_3700;
									}
							}
						}
						if (BgL_test2220z00_4902)
							{	/* Tools/trace.sch 53 */
								bool_t BgL__ortest_1211z00_3710;

								if (
									(((obj_t) BgL_varz00_3694) ==
										BGl_za2thezd2currentzd2globalza2z00zzcgen_cgenz00))
									{	/* Tools/trace.sch 53 */
										BgL__ortest_1211z00_3710 = ((bool_t) 0);
									}
								else
									{	/* Tools/trace.sch 53 */
										BgL__ortest_1211z00_3710 = ((bool_t) 1);
									}
								if (BgL__ortest_1211z00_3710)
									{	/* Tools/trace.sch 53 */
										BgL_test2219z00_4901 = BgL__ortest_1211z00_3710;
									}
								else
									{	/* Tools/trace.sch 53 */
										if (
											(BgL_kontz00_3668 ==
												BGl_za2returnzd2kontza2zd2zzcgen_cgenz00))
											{	/* Tools/trace.sch 53 */
												BgL_test2219z00_4901 = ((bool_t) 0);
											}
										else
											{	/* Tools/trace.sch 53 */
												BgL_test2219z00_4901 = ((bool_t) 1);
											}
									}
							}
						else
							{	/* Tools/trace.sch 53 */
								BgL_test2219z00_4901 = ((bool_t) 0);
							}
					}
					if (BgL_test2219z00_4901)
						{	/* Tools/trace.sch 53 */
							{	/* Tools/trace.sch 53 */
								bool_t BgL_test2227z00_4932;

								{	/* Tools/trace.sch 53 */
									BgL_valuez00_bglt BgL_arg1489z00_3711;

									BgL_arg1489z00_3711 =
										(((BgL_variablez00_bglt) COBJECT(BgL_varz00_3694))->
										BgL_valuez00);
									{	/* Tools/trace.sch 53 */
										obj_t BgL_classz00_3712;

										BgL_classz00_3712 = BGl_sfunz00zzast_varz00;
										{	/* Tools/trace.sch 53 */
											BgL_objectz00_bglt BgL_arg1807z00_3713;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_tmpz00_4934;

												BgL_tmpz00_4934 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1489z00_3711));
												BgL_arg1807z00_3713 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4934);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Tools/trace.sch 53 */
													long BgL_idxz00_3714;

													BgL_idxz00_3714 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3713);
													BgL_test2227z00_4932 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3714 + 3L)) == BgL_classz00_3712);
												}
											else
												{	/* Tools/trace.sch 53 */
													bool_t BgL_res2144z00_3717;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_oclassz00_3718;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_arg1815z00_3719;
															long BgL_arg1816z00_3720;

															BgL_arg1815z00_3719 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Tools/trace.sch 53 */
																long BgL_arg1817z00_3721;

																BgL_arg1817z00_3721 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3713);
																BgL_arg1816z00_3720 =
																	(BgL_arg1817z00_3721 - OBJECT_TYPE);
															}
															BgL_oclassz00_3718 =
																VECTOR_REF(BgL_arg1815z00_3719,
																BgL_arg1816z00_3720);
														}
														{	/* Tools/trace.sch 53 */
															bool_t BgL__ortest_1115z00_3722;

															BgL__ortest_1115z00_3722 =
																(BgL_classz00_3712 == BgL_oclassz00_3718);
															if (BgL__ortest_1115z00_3722)
																{	/* Tools/trace.sch 53 */
																	BgL_res2144z00_3717 =
																		BgL__ortest_1115z00_3722;
																}
															else
																{	/* Tools/trace.sch 53 */
																	long BgL_odepthz00_3723;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1804z00_3724;

																		BgL_arg1804z00_3724 = (BgL_oclassz00_3718);
																		BgL_odepthz00_3723 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3724);
																	}
																	if ((3L < BgL_odepthz00_3723))
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1802z00_3725;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1803z00_3726;

																				BgL_arg1803z00_3726 =
																					(BgL_oclassz00_3718);
																				BgL_arg1802z00_3725 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3726, 3L);
																			}
																			BgL_res2144z00_3717 =
																				(BgL_arg1802z00_3725 ==
																				BgL_classz00_3712);
																		}
																	else
																		{	/* Tools/trace.sch 53 */
																			BgL_res2144z00_3717 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2227z00_4932 = BgL_res2144z00_3717;
												}
										}
									}
								}
								if (BgL_test2227z00_4932)
									{	/* Tools/trace.sch 53 */
										return
											((BgL_copz00_bglt)
											BGl_nodezd2sfunzd2nonzd2tailzd2appzd2ze3copz31zzcgen_cappz00
											(BgL_varz00_3694, ((BgL_appz00_bglt) BgL_nodez00_3667),
												BgL_kontz00_3668, BgL_inpushexitz00_3669));
									}
								else
									{	/* Tools/trace.sch 53 */
										return
											((BgL_copz00_bglt)
											BGl_nodezd2cfunzd2nonzd2tailzd2appzd2ze3copz31zzcgen_cappz00
											(BgL_varz00_3694, ((BgL_appz00_bglt) BgL_nodez00_3667),
												BgL_kontz00_3668, BgL_inpushexitz00_3669));
									}
							}
						}
					else
						{	/* Tools/trace.sch 53 */
							return
								((BgL_copz00_bglt)
								BGl_nodezd2tailzd2appzd2ze3copz31zzcgen_cappz00(BgL_varz00_3694,
									((BgL_appz00_bglt) BgL_nodez00_3667), BgL_kontz00_3668,
									BgL_inpushexitz00_3669));
						}
				}
			}
		}

	}



/* &node->cop-funcall1464 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2funcall1464z81zzcgen_cappz00(obj_t
		BgL_envz00_3670, obj_t BgL_nodez00_3671, obj_t BgL_kontz00_3672,
		obj_t BgL_inpushexitz00_3673)
	{
		{	/* Cgen/capp.scm 138 */
			{
				obj_t BgL_oldzd2actualszd2_3729;
				obj_t BgL_newzd2actualszd2_3730;
				obj_t BgL_auxsz00_3731;
				obj_t BgL_expsz00_3732;

				{	/* Tools/trace.sch 53 */
					obj_t BgL_arg1956z00_3861;

					BgL_arg1956z00_3861 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3671)))->BgL_argsz00);
					{
						obj_t BgL_auxz00_4968;

						BgL_oldzd2actualszd2_3729 = BgL_arg1956z00_3861;
						BgL_newzd2actualszd2_3730 = BNIL;
						BgL_auxsz00_3731 = BNIL;
						BgL_expsz00_3732 = BNIL;
					BgL_loopz00_3728:
						if (NULLP(BgL_oldzd2actualszd2_3729))
							{	/* Tools/trace.sch 53 */
								BgL_typez00_bglt BgL_typez00_3733;

								BgL_typez00_3733 =
									BGl_getzd2typezd2zztype_typeofz00(
									((BgL_nodez00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_3671)), ((bool_t) 0));
								{	/* Tools/trace.sch 53 */
									BgL_localz00_bglt BgL_auxz00_3734;

									BgL_auxz00_3734 =
										BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00
										(CNST_TABLE_REF(4),
										((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00));
									{	/* Tools/trace.sch 53 */
										BgL_copz00_bglt BgL_copz00_3735;

										{	/* Tools/trace.sch 53 */
											BgL_setqz00_bglt BgL_arg1997z00_3736;

											{	/* Tools/trace.sch 53 */
												BgL_nodez00_bglt BgL_arg1998z00_3737;

												BgL_arg1998z00_3737 =
													(((BgL_funcallz00_bglt) COBJECT(
															((BgL_funcallz00_bglt) BgL_nodez00_3671)))->
													BgL_funz00);
												BgL_arg1997z00_3736 =
													BGl_nodezd2setqzd2zzcgen_cgenz00((
														(BgL_variablez00_bglt) BgL_auxz00_3734),
													BgL_arg1998z00_3737);
											}
											BgL_copz00_3735 =
												BGl_nodezd2ze3copz31zzcgen_cgenz00(
												((BgL_nodez00_bglt) BgL_arg1997z00_3736),
												BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
												CBOOL(BgL_inpushexitz00_3673));
										}
										{	/* Tools/trace.sch 53 */

											{	/* Tools/trace.sch 53 */
												bool_t BgL_test2232z00_4984;

												{	/* Tools/trace.sch 53 */
													bool_t BgL_test2233z00_4985;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_classz00_3738;

														BgL_classz00_3738 = BGl_csetqz00zzcgen_copz00;
														{	/* Tools/trace.sch 53 */
															BgL_objectz00_bglt BgL_arg1807z00_3739;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_tmpz00_4986;

																BgL_tmpz00_4986 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_copz00_3735));
																BgL_arg1807z00_3739 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4986);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Tools/trace.sch 53 */
																	long BgL_idxz00_3740;

																	BgL_idxz00_3740 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3739);
																	BgL_test2233z00_4985 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3740 + 2L)) ==
																		BgL_classz00_3738);
																}
															else
																{	/* Tools/trace.sch 53 */
																	bool_t BgL_res2138z00_3743;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_oclassz00_3744;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1815z00_3745;
																			long BgL_arg1816z00_3746;

																			BgL_arg1815z00_3745 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Tools/trace.sch 53 */
																				long BgL_arg1817z00_3747;

																				BgL_arg1817z00_3747 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3739);
																				BgL_arg1816z00_3746 =
																					(BgL_arg1817z00_3747 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3744 =
																				VECTOR_REF(BgL_arg1815z00_3745,
																				BgL_arg1816z00_3746);
																		}
																		{	/* Tools/trace.sch 53 */
																			bool_t BgL__ortest_1115z00_3748;

																			BgL__ortest_1115z00_3748 =
																				(BgL_classz00_3738 ==
																				BgL_oclassz00_3744);
																			if (BgL__ortest_1115z00_3748)
																				{	/* Tools/trace.sch 53 */
																					BgL_res2138z00_3743 =
																						BgL__ortest_1115z00_3748;
																				}
																			else
																				{	/* Tools/trace.sch 53 */
																					long BgL_odepthz00_3749;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1804z00_3750;

																						BgL_arg1804z00_3750 =
																							(BgL_oclassz00_3744);
																						BgL_odepthz00_3749 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3750);
																					}
																					if ((2L < BgL_odepthz00_3749))
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1802z00_3751;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1803z00_3752;

																								BgL_arg1803z00_3752 =
																									(BgL_oclassz00_3744);
																								BgL_arg1802z00_3751 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3752, 2L);
																							}
																							BgL_res2138z00_3743 =
																								(BgL_arg1802z00_3751 ==
																								BgL_classz00_3738);
																						}
																					else
																						{	/* Tools/trace.sch 53 */
																							BgL_res2138z00_3743 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2233z00_4985 = BgL_res2138z00_3743;
																}
														}
													}
													if (BgL_test2233z00_4985)
														{	/* Tools/trace.sch 53 */
															bool_t BgL_test2237z00_5009;

															{	/* Tools/trace.sch 53 */
																BgL_nodez00_bglt BgL_arg1996z00_3753;

																BgL_arg1996z00_3753 =
																	(((BgL_funcallz00_bglt) COBJECT(
																			((BgL_funcallz00_bglt)
																				BgL_nodez00_3671)))->BgL_funz00);
																{	/* Tools/trace.sch 53 */
																	obj_t BgL_classz00_3754;

																	BgL_classz00_3754 = BGl_varz00zzast_nodez00;
																	{	/* Tools/trace.sch 53 */
																		BgL_objectz00_bglt BgL_arg1807z00_3755;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_tmpz00_5012;

																			BgL_tmpz00_5012 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_arg1996z00_3753));
																			BgL_arg1807z00_3755 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_5012);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Tools/trace.sch 53 */
																				long BgL_idxz00_3756;

																				BgL_idxz00_3756 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_3755);
																				BgL_test2237z00_5009 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_3756 + 2L)) ==
																					BgL_classz00_3754);
																			}
																		else
																			{	/* Tools/trace.sch 53 */
																				bool_t BgL_res2139z00_3759;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_oclassz00_3760;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1815z00_3761;
																						long BgL_arg1816z00_3762;

																						BgL_arg1815z00_3761 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg1817z00_3763;

																							BgL_arg1817z00_3763 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_3755);
																							BgL_arg1816z00_3762 =
																								(BgL_arg1817z00_3763 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_3760 =
																							VECTOR_REF(BgL_arg1815z00_3761,
																							BgL_arg1816z00_3762);
																					}
																					{	/* Tools/trace.sch 53 */
																						bool_t BgL__ortest_1115z00_3764;

																						BgL__ortest_1115z00_3764 =
																							(BgL_classz00_3754 ==
																							BgL_oclassz00_3760);
																						if (BgL__ortest_1115z00_3764)
																							{	/* Tools/trace.sch 53 */
																								BgL_res2139z00_3759 =
																									BgL__ortest_1115z00_3764;
																							}
																						else
																							{	/* Tools/trace.sch 53 */
																								long BgL_odepthz00_3765;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1804z00_3766;

																									BgL_arg1804z00_3766 =
																										(BgL_oclassz00_3760);
																									BgL_odepthz00_3765 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_3766);
																								}
																								if ((2L < BgL_odepthz00_3765))
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1802z00_3767;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1803z00_3768;

																											BgL_arg1803z00_3768 =
																												(BgL_oclassz00_3760);
																											BgL_arg1802z00_3767 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_3768,
																												2L);
																										}
																										BgL_res2139z00_3759 =
																											(BgL_arg1802z00_3767 ==
																											BgL_classz00_3754);
																									}
																								else
																									{	/* Tools/trace.sch 53 */
																										BgL_res2139z00_3759 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2237z00_5009 =
																					BgL_res2139z00_3759;
																			}
																	}
																}
															}
															if (BgL_test2237z00_5009)
																{	/* Tools/trace.sch 53 */
																	BgL_test2232z00_4984 =
																		(
																		((obj_t)
																			(((BgL_varcz00_bglt) COBJECT(
																						(((BgL_csetqz00_bglt) COBJECT(
																									((BgL_csetqz00_bglt)
																										BgL_copz00_3735)))->
																							BgL_varz00)))->
																				BgL_variablez00)) ==
																		((obj_t) BgL_auxz00_3734));
																}
															else
																{	/* Tools/trace.sch 53 */
																	BgL_test2232z00_4984 = ((bool_t) 0);
																}
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_test2232z00_4984 = ((bool_t) 0);
														}
												}
												if (BgL_test2232z00_4984)
													{	/* Tools/trace.sch 53 */
														BgL_copz00_bglt BgL_cfunz00_3769;

														BgL_cfunz00_3769 =
															(((BgL_csetqz00_bglt) COBJECT(
																	((BgL_csetqz00_bglt) BgL_copz00_3735)))->
															BgL_valuez00);
														if (NULLP(BgL_auxsz00_3731))
															{	/* Tools/trace.sch 53 */
																BgL_cfuncallz00_bglt BgL_arg1967z00_3770;

																{	/* Tools/trace.sch 53 */
																	BgL_cfuncallz00_bglt BgL_new1182z00_3771;

																	{	/* Tools/trace.sch 53 */
																		BgL_cfuncallz00_bglt BgL_new1180z00_3772;

																		BgL_new1180z00_3772 =
																			((BgL_cfuncallz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_cfuncallz00_bgl))));
																		{	/* Tools/trace.sch 53 */
																			long BgL_arg1968z00_3773;

																			BgL_arg1968z00_3773 =
																				BGL_CLASS_NUM
																				(BGl_cfuncallz00zzcgen_copz00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1180z00_3772),
																				BgL_arg1968z00_3773);
																		}
																		BgL_new1182z00_3771 = BgL_new1180z00_3772;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1182z00_3771)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_funcallz00_bglt)
																								BgL_nodez00_3671))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1182z00_3771)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) BgL_typez00_3733),
																		BUNSPEC);
																	((((BgL_cfuncallz00_bglt)
																				COBJECT(BgL_new1182z00_3771))->
																			BgL_funz00) =
																		((BgL_copz00_bglt) BgL_cfunz00_3769),
																		BUNSPEC);
																	((((BgL_cfuncallz00_bglt)
																				COBJECT(BgL_new1182z00_3771))->
																			BgL_argsz00) =
																		((obj_t)
																			bgl_reverse_bang
																			(BgL_newzd2actualszd2_3730)), BUNSPEC);
																	((((BgL_cfuncallz00_bglt)
																				COBJECT(BgL_new1182z00_3771))->
																			BgL_strengthz00) =
																		((obj_t) (((BgL_funcallz00_bglt)
																					COBJECT(((BgL_funcallz00_bglt)
																							BgL_nodez00_3671)))->
																				BgL_strengthz00)), BUNSPEC);
																	BgL_arg1967z00_3770 = BgL_new1182z00_3771;
																}
																BgL_auxz00_4968 =
																	BGL_PROCEDURE_CALL1(BgL_kontz00_3672,
																	((obj_t) BgL_arg1967z00_3770));
															}
														else
															{	/* Tools/trace.sch 53 */
																BgL_cblockz00_bglt BgL_new1184z00_3774;

																{	/* Tools/trace.sch 53 */
																	BgL_cblockz00_bglt BgL_new1183z00_3775;

																	BgL_new1183z00_3775 =
																		((BgL_cblockz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_cblockz00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg1980z00_3776;

																		BgL_arg1980z00_3776 =
																			BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1183z00_3775),
																			BgL_arg1980z00_3776);
																	}
																	BgL_new1184z00_3774 = BgL_new1183z00_3775;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1184z00_3774)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_funcallz00_bglt)
																							BgL_nodez00_3671))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1184z00_3774)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_typez00_3733),
																	BUNSPEC);
																{
																	BgL_copz00_bglt BgL_auxz00_5078;

																	{	/* Tools/trace.sch 53 */
																		BgL_csequencez00_bglt BgL_new1186z00_3777;

																		{	/* Tools/trace.sch 53 */
																			BgL_csequencez00_bglt BgL_new1185z00_3778;

																			BgL_new1185z00_3778 =
																				((BgL_csequencez00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_csequencez00_bgl))));
																			{	/* Tools/trace.sch 53 */
																				long BgL_arg1979z00_3779;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_classz00_3780;

																					BgL_classz00_3780 =
																						BGl_csequencez00zzcgen_copz00;
																					BgL_arg1979z00_3779 =
																						BGL_CLASS_NUM(BgL_classz00_3780);
																				}
																				BGL_OBJECT_CLASS_NUM_SET(
																					((BgL_objectz00_bglt)
																						BgL_new1185z00_3778),
																					BgL_arg1979z00_3779);
																			}
																			BgL_new1186z00_3777 = BgL_new1185z00_3778;
																		}
																		((((BgL_copz00_bglt) COBJECT(
																						((BgL_copz00_bglt)
																							BgL_new1186z00_3777)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_funcallz00_bglt)
																									BgL_nodez00_3671))))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_copz00_bglt)
																					COBJECT(((BgL_copz00_bglt)
																							BgL_new1186z00_3777)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_typez00_3733),
																			BUNSPEC);
																		((((BgL_csequencez00_bglt)
																					COBJECT(BgL_new1186z00_3777))->
																				BgL_czd2expzf3z21) =
																			((bool_t) ((bool_t) 0)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_5091;

																			{	/* Tools/trace.sch 53 */
																				BgL_localzd2varzd2_bglt
																					BgL_arg1969z00_3781;
																				BgL_csequencez00_bglt
																					BgL_arg1970z00_3782;
																				obj_t BgL_arg1971z00_3783;

																				{	/* Tools/trace.sch 53 */
																					BgL_localzd2varzd2_bglt
																						BgL_new1189z00_3784;
																					{	/* Tools/trace.sch 53 */
																						BgL_localzd2varzd2_bglt
																							BgL_new1187z00_3785;
																						BgL_new1187z00_3785 =
																							((BgL_localzd2varzd2_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_localzd2varzd2_bgl))));
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg1975z00_3786;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_classz00_3787;

																								BgL_classz00_3787 =
																									BGl_localzd2varzd2zzcgen_copz00;
																								BgL_arg1975z00_3786 =
																									BGL_CLASS_NUM
																									(BgL_classz00_3787);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1187z00_3785),
																								BgL_arg1975z00_3786);
																						}
																						BgL_new1189z00_3784 =
																							BgL_new1187z00_3785;
																					}
																					((((BgL_copz00_bglt) COBJECT(
																									((BgL_copz00_bglt)
																										BgL_new1189z00_3784)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt) (
																												(BgL_funcallz00_bglt)
																												BgL_nodez00_3671))))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_copz00_bglt)
																								COBJECT(((BgL_copz00_bglt)
																										BgL_new1189z00_3784)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2objza2z00zztype_cachez00)),
																						BUNSPEC);
																					((((BgL_localzd2varzd2_bglt)
																								COBJECT(BgL_new1189z00_3784))->
																							BgL_varsz00) =
																						((obj_t) BgL_auxsz00_3731),
																						BUNSPEC);
																					BgL_arg1969z00_3781 =
																						BgL_new1189z00_3784;
																				}
																				{	/* Tools/trace.sch 53 */
																					BgL_csequencez00_bglt
																						BgL_new1191z00_3788;
																					{	/* Tools/trace.sch 53 */
																						BgL_csequencez00_bglt
																							BgL_new1190z00_3789;
																						BgL_new1190z00_3789 =
																							((BgL_csequencez00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_csequencez00_bgl))));
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg1976z00_3790;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_classz00_3791;

																								BgL_classz00_3791 =
																									BGl_csequencez00zzcgen_copz00;
																								BgL_arg1976z00_3790 =
																									BGL_CLASS_NUM
																									(BgL_classz00_3791);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1190z00_3789),
																								BgL_arg1976z00_3790);
																						}
																						BgL_new1191z00_3788 =
																							BgL_new1190z00_3789;
																					}
																					((((BgL_copz00_bglt) COBJECT(
																									((BgL_copz00_bglt)
																										BgL_new1191z00_3788)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt) (
																												(BgL_funcallz00_bglt)
																												BgL_nodez00_3671))))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_copz00_bglt)
																								COBJECT(((BgL_copz00_bglt)
																										BgL_new1191z00_3788)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) (
																								(BgL_typez00_bglt)
																								BGl_za2objza2z00zztype_cachez00)),
																						BUNSPEC);
																					((((BgL_csequencez00_bglt)
																								COBJECT(BgL_new1191z00_3788))->
																							BgL_czd2expzf3z21) =
																						((bool_t) ((bool_t) 0)), BUNSPEC);
																					((((BgL_csequencez00_bglt)
																								COBJECT(BgL_new1191z00_3788))->
																							BgL_copsz00) =
																						((obj_t) BgL_expsz00_3732),
																						BUNSPEC);
																					BgL_arg1970z00_3782 =
																						BgL_new1191z00_3788;
																				}
																				{	/* Tools/trace.sch 53 */
																					BgL_cfuncallz00_bglt
																						BgL_arg1977z00_3792;
																					{	/* Tools/trace.sch 53 */
																						BgL_cfuncallz00_bglt
																							BgL_new1193z00_3793;
																						{	/* Tools/trace.sch 53 */
																							BgL_cfuncallz00_bglt
																								BgL_new1192z00_3794;
																							BgL_new1192z00_3794 =
																								((BgL_cfuncallz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_cfuncallz00_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg1978z00_3795;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_3796;

																									BgL_classz00_3796 =
																										BGl_cfuncallz00zzcgen_copz00;
																									BgL_arg1978z00_3795 =
																										BGL_CLASS_NUM
																										(BgL_classz00_3796);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1192z00_3794),
																									BgL_arg1978z00_3795);
																							}
																							BgL_new1193z00_3793 =
																								BgL_new1192z00_3794;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1193z00_3793)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_funcallz00_bglt)
																													BgL_nodez00_3671))))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1193z00_3793)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt)
																								BgL_typez00_3733), BUNSPEC);
																						((((BgL_cfuncallz00_bglt)
																									COBJECT
																									(BgL_new1193z00_3793))->
																								BgL_funz00) =
																							((BgL_copz00_bglt)
																								BgL_cfunz00_3769), BUNSPEC);
																						((((BgL_cfuncallz00_bglt)
																									COBJECT
																									(BgL_new1193z00_3793))->
																								BgL_argsz00) =
																							((obj_t)
																								bgl_reverse_bang
																								(BgL_newzd2actualszd2_3730)),
																							BUNSPEC);
																						((((BgL_cfuncallz00_bglt)
																									COBJECT
																									(BgL_new1193z00_3793))->
																								BgL_strengthz00) =
																							((obj_t) (((BgL_funcallz00_bglt)
																										COBJECT((
																												(BgL_funcallz00_bglt)
																												BgL_nodez00_3671)))->
																									BgL_strengthz00)), BUNSPEC);
																						BgL_arg1977z00_3792 =
																							BgL_new1193z00_3793;
																					}
																					BgL_arg1971z00_3783 =
																						BGL_PROCEDURE_CALL1
																						(BgL_kontz00_3672,
																						((obj_t) BgL_arg1977z00_3792));
																				}
																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_list1972z00_3797;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1973z00_3798;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1974z00_3799;

																							BgL_arg1974z00_3799 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1971z00_3783, BNIL);
																							BgL_arg1973z00_3798 =
																								MAKE_YOUNG_PAIR(((obj_t)
																									BgL_arg1970z00_3782),
																								BgL_arg1974z00_3799);
																						}
																						BgL_list1972z00_3797 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_arg1969z00_3781),
																							BgL_arg1973z00_3798);
																					}
																					BgL_auxz00_5091 =
																						BgL_list1972z00_3797;
																			}}
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1186z00_3777))->
																					BgL_copsz00) =
																				((obj_t) BgL_auxz00_5091), BUNSPEC);
																		}
																		BgL_auxz00_5078 =
																			((BgL_copz00_bglt) BgL_new1186z00_3777);
																	}
																	((((BgL_cblockz00_bglt)
																				COBJECT(BgL_new1184z00_3774))->
																			BgL_bodyz00) =
																		((BgL_copz00_bglt) BgL_auxz00_5078),
																		BUNSPEC);
																}
																BgL_auxz00_4968 = ((obj_t) BgL_new1184z00_3774);
													}}
												else
													{	/* Tools/trace.sch 53 */
														BgL_cblockz00_bglt BgL_new1195z00_3800;

														{	/* Tools/trace.sch 53 */
															BgL_cblockz00_bglt BgL_new1194z00_3801;

															BgL_new1194z00_3801 =
																((BgL_cblockz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_cblockz00_bgl))));
															{	/* Tools/trace.sch 53 */
																long BgL_arg1993z00_3802;

																BgL_arg1993z00_3802 =
																	BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1194z00_3801),
																	BgL_arg1993z00_3802);
															}
															BgL_new1195z00_3800 = BgL_new1194z00_3801;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1195z00_3800)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_funcallz00_bglt)
																					BgL_nodez00_3671))))->BgL_locz00)),
															BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1195z00_3800)))->BgL_typez00) =
															((BgL_typez00_bglt) BgL_typez00_3733), BUNSPEC);
														{
															BgL_copz00_bglt BgL_auxz00_5161;

															{	/* Tools/trace.sch 53 */
																BgL_csequencez00_bglt BgL_new1197z00_3803;

																{	/* Tools/trace.sch 53 */
																	BgL_csequencez00_bglt BgL_new1196z00_3804;

																	BgL_new1196z00_3804 =
																		((BgL_csequencez00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_csequencez00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg1992z00_3805;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_classz00_3806;

																			BgL_classz00_3806 =
																				BGl_csequencez00zzcgen_copz00;
																			BgL_arg1992z00_3805 =
																				BGL_CLASS_NUM(BgL_classz00_3806);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1196z00_3804),
																			BgL_arg1992z00_3805);
																	}
																	BgL_new1197z00_3803 = BgL_new1196z00_3804;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1197z00_3803)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_funcallz00_bglt)
																							BgL_nodez00_3671))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1197z00_3803)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_typez00_3733),
																	BUNSPEC);
																((((BgL_csequencez00_bglt)
																			COBJECT(BgL_new1197z00_3803))->
																		BgL_czd2expzf3z21) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
																{
																	obj_t BgL_auxz00_5174;

																	{	/* Tools/trace.sch 53 */
																		BgL_localzd2varzd2_bglt BgL_arg1981z00_3807;
																		BgL_csequencez00_bglt BgL_arg1982z00_3808;
																		obj_t BgL_arg1983z00_3809;

																		{	/* Tools/trace.sch 53 */
																			BgL_localzd2varzd2_bglt
																				BgL_new1200z00_3810;
																			{	/* Tools/trace.sch 53 */
																				BgL_localzd2varzd2_bglt
																					BgL_new1198z00_3811;
																				BgL_new1198z00_3811 =
																					((BgL_localzd2varzd2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_localzd2varzd2_bgl))));
																				{	/* Tools/trace.sch 53 */
																					long BgL_arg1987z00_3812;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_classz00_3813;

																						BgL_classz00_3813 =
																							BGl_localzd2varzd2zzcgen_copz00;
																						BgL_arg1987z00_3812 =
																							BGL_CLASS_NUM(BgL_classz00_3813);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1198z00_3811),
																						BgL_arg1987z00_3812);
																				}
																				BgL_new1200z00_3810 =
																					BgL_new1198z00_3811;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1200z00_3810)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt) (
																										(BgL_funcallz00_bglt)
																										BgL_nodez00_3671))))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1200z00_3810)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_localzd2varzd2_bglt)
																						COBJECT(BgL_new1200z00_3810))->
																					BgL_varsz00) =
																				((obj_t) MAKE_YOUNG_PAIR(((obj_t)
																							BgL_auxz00_3734),
																						BgL_auxsz00_3731)), BUNSPEC);
																			BgL_arg1981z00_3807 = BgL_new1200z00_3810;
																		}
																		{	/* Tools/trace.sch 53 */
																			BgL_csequencez00_bglt BgL_new1202z00_3814;

																			{	/* Tools/trace.sch 53 */
																				BgL_csequencez00_bglt
																					BgL_new1201z00_3815;
																				BgL_new1201z00_3815 =
																					((BgL_csequencez00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_csequencez00_bgl))));
																				{	/* Tools/trace.sch 53 */
																					long BgL_arg1988z00_3816;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_classz00_3817;

																						BgL_classz00_3817 =
																							BGl_csequencez00zzcgen_copz00;
																						BgL_arg1988z00_3816 =
																							BGL_CLASS_NUM(BgL_classz00_3817);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1201z00_3815),
																						BgL_arg1988z00_3816);
																				}
																				BgL_new1202z00_3814 =
																					BgL_new1201z00_3815;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1202z00_3814)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt) (
																										(BgL_funcallz00_bglt)
																										BgL_nodez00_3671))))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1202z00_3814)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((BgL_typez00_bglt)
																						BGl_za2objza2z00zztype_cachez00)),
																				BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1202z00_3814))->
																					BgL_czd2expzf3z21) =
																				((bool_t) ((bool_t) 0)), BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1202z00_3814))->
																					BgL_copsz00) =
																				((obj_t) MAKE_YOUNG_PAIR(((obj_t)
																							BgL_copz00_3735),
																						BgL_expsz00_3732)), BUNSPEC);
																			BgL_arg1982z00_3808 = BgL_new1202z00_3814;
																		}
																		{	/* Tools/trace.sch 53 */
																			BgL_cfuncallz00_bglt BgL_arg1989z00_3818;

																			{	/* Tools/trace.sch 53 */
																				BgL_cfuncallz00_bglt
																					BgL_new1205z00_3819;
																				{	/* Tools/trace.sch 53 */
																					BgL_cfuncallz00_bglt
																						BgL_new1204z00_3820;
																					BgL_new1204z00_3820 =
																						((BgL_cfuncallz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_cfuncallz00_bgl))));
																					{	/* Tools/trace.sch 53 */
																						long BgL_arg1991z00_3821;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_classz00_3822;

																							BgL_classz00_3822 =
																								BGl_cfuncallz00zzcgen_copz00;
																							BgL_arg1991z00_3821 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3822);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1204z00_3820),
																							BgL_arg1991z00_3821);
																					}
																					BgL_new1205z00_3819 =
																						BgL_new1204z00_3820;
																				}
																				((((BgL_copz00_bglt) COBJECT(
																								((BgL_copz00_bglt)
																									BgL_new1205z00_3819)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt) (
																											(BgL_funcallz00_bglt)
																											BgL_nodez00_3671))))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_copz00_bglt)
																							COBJECT(((BgL_copz00_bglt)
																									BgL_new1205z00_3819)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) BgL_typez00_3733),
																					BUNSPEC);
																				{
																					BgL_copz00_bglt BgL_auxz00_5217;

																					{	/* Tools/trace.sch 53 */
																						BgL_varcz00_bglt
																							BgL_new1207z00_3823;
																						{	/* Tools/trace.sch 53 */
																							BgL_varcz00_bglt
																								BgL_new1206z00_3824;
																							BgL_new1206z00_3824 =
																								((BgL_varcz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_varcz00_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg1990z00_3825;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_3826;

																									BgL_classz00_3826 =
																										BGl_varcz00zzcgen_copz00;
																									BgL_arg1990z00_3825 =
																										BGL_CLASS_NUM
																										(BgL_classz00_3826);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1206z00_3824),
																									BgL_arg1990z00_3825);
																							}
																							BgL_new1207z00_3823 =
																								BgL_new1206z00_3824;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1207z00_3823)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_funcallz00_bglt)
																													BgL_nodez00_3671))))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1207z00_3823)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) ((
																										(BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt)
																												BgL_auxz00_3734)))->
																									BgL_typez00)), BUNSPEC);
																						((((BgL_varcz00_bglt)
																									COBJECT
																									(BgL_new1207z00_3823))->
																								BgL_variablez00) =
																							((BgL_variablez00_bglt) (
																									(BgL_variablez00_bglt)
																									BgL_auxz00_3734)), BUNSPEC);
																						BgL_auxz00_5217 =
																							((BgL_copz00_bglt)
																							BgL_new1207z00_3823);
																					}
																					((((BgL_cfuncallz00_bglt)
																								COBJECT(BgL_new1205z00_3819))->
																							BgL_funz00) =
																						((BgL_copz00_bglt) BgL_auxz00_5217),
																						BUNSPEC);
																				}
																				((((BgL_cfuncallz00_bglt)
																							COBJECT(BgL_new1205z00_3819))->
																						BgL_argsz00) =
																					((obj_t)
																						bgl_reverse_bang
																						(BgL_newzd2actualszd2_3730)),
																					BUNSPEC);
																				((((BgL_cfuncallz00_bglt)
																							COBJECT(BgL_new1205z00_3819))->
																						BgL_strengthz00) =
																					((obj_t) (((BgL_funcallz00_bglt)
																								COBJECT(((BgL_funcallz00_bglt)
																										BgL_nodez00_3671)))->
																							BgL_strengthz00)), BUNSPEC);
																				BgL_arg1989z00_3818 =
																					BgL_new1205z00_3819;
																			}
																			BgL_arg1983z00_3809 =
																				BGL_PROCEDURE_CALL1(BgL_kontz00_3672,
																				((obj_t) BgL_arg1989z00_3818));
																		}
																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_list1984z00_3827;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1985z00_3828;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1986z00_3829;

																					BgL_arg1986z00_3829 =
																						MAKE_YOUNG_PAIR(BgL_arg1983z00_3809,
																						BNIL);
																					BgL_arg1985z00_3828 =
																						MAKE_YOUNG_PAIR(((obj_t)
																							BgL_arg1982z00_3808),
																						BgL_arg1986z00_3829);
																				}
																				BgL_list1984z00_3827 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg1981z00_3807),
																					BgL_arg1985z00_3828);
																			}
																			BgL_auxz00_5174 = BgL_list1984z00_3827;
																	}}
																	((((BgL_csequencez00_bglt)
																				COBJECT(BgL_new1197z00_3803))->
																			BgL_copsz00) =
																		((obj_t) BgL_auxz00_5174), BUNSPEC);
																}
																BgL_auxz00_5161 =
																	((BgL_copz00_bglt) BgL_new1197z00_3803);
															}
															((((BgL_cblockz00_bglt)
																		COBJECT(BgL_new1195z00_3800))->
																	BgL_bodyz00) =
																((BgL_copz00_bglt) BgL_auxz00_5161), BUNSPEC);
														}
														BgL_auxz00_4968 = ((obj_t) BgL_new1195z00_3800);
							}}}}}}
						else
							{	/* Tools/trace.sch 53 */
								obj_t BgL_az00_3830;

								BgL_az00_3830 = CAR(((obj_t) BgL_oldzd2actualszd2_3729));
								{	/* Tools/trace.sch 53 */
									BgL_localz00_bglt BgL_auxz00_3831;

									{	/* Tools/trace.sch 53 */
										BgL_typez00_bglt BgL_arg2017z00_3832;

										BgL_arg2017z00_3832 =
											BGl_getzd2typezd2zztype_typeofz00(
											((BgL_nodez00_bglt) BgL_az00_3830), ((bool_t) 0));
										BgL_auxz00_3831 =
											BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00
											(CNST_TABLE_REF(5), BgL_arg2017z00_3832);
									}
									{	/* Tools/trace.sch 53 */
										BgL_copz00_bglt BgL_copz00_3833;

										{	/* Tools/trace.sch 53 */
											BgL_setqz00_bglt BgL_arg2016z00_3834;

											BgL_arg2016z00_3834 =
												BGl_nodezd2setqzd2zzcgen_cgenz00(
												((BgL_variablez00_bglt) BgL_auxz00_3831),
												((BgL_nodez00_bglt) BgL_az00_3830));
											BgL_copz00_3833 =
												BGl_nodezd2ze3copz31zzcgen_cgenz00(
												((BgL_nodez00_bglt) BgL_arg2016z00_3834),
												BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
												CBOOL(BgL_inpushexitz00_3673));
										}
										{	/* Tools/trace.sch 53 */

											{	/* Tools/trace.sch 53 */
												bool_t BgL_test2242z00_5266;

												{	/* Tools/trace.sch 53 */
													bool_t BgL_test2243z00_5267;

													{	/* Tools/trace.sch 53 */
														obj_t BgL_classz00_3835;

														BgL_classz00_3835 = BGl_csetqz00zzcgen_copz00;
														{	/* Tools/trace.sch 53 */
															BgL_objectz00_bglt BgL_arg1807z00_3836;

															{	/* Tools/trace.sch 53 */
																obj_t BgL_tmpz00_5268;

																BgL_tmpz00_5268 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_copz00_3833));
																BgL_arg1807z00_3836 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_5268);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Tools/trace.sch 53 */
																	long BgL_idxz00_3837;

																	BgL_idxz00_3837 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3836);
																	BgL_test2243z00_5267 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3837 + 2L)) ==
																		BgL_classz00_3835);
																}
															else
																{	/* Tools/trace.sch 53 */
																	bool_t BgL_res2142z00_3840;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_oclassz00_3841;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_arg1815z00_3842;
																			long BgL_arg1816z00_3843;

																			BgL_arg1815z00_3842 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Tools/trace.sch 53 */
																				long BgL_arg1817z00_3844;

																				BgL_arg1817z00_3844 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3836);
																				BgL_arg1816z00_3843 =
																					(BgL_arg1817z00_3844 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3841 =
																				VECTOR_REF(BgL_arg1815z00_3842,
																				BgL_arg1816z00_3843);
																		}
																		{	/* Tools/trace.sch 53 */
																			bool_t BgL__ortest_1115z00_3845;

																			BgL__ortest_1115z00_3845 =
																				(BgL_classz00_3835 ==
																				BgL_oclassz00_3841);
																			if (BgL__ortest_1115z00_3845)
																				{	/* Tools/trace.sch 53 */
																					BgL_res2142z00_3840 =
																						BgL__ortest_1115z00_3845;
																				}
																			else
																				{	/* Tools/trace.sch 53 */
																					long BgL_odepthz00_3846;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1804z00_3847;

																						BgL_arg1804z00_3847 =
																							(BgL_oclassz00_3841);
																						BgL_odepthz00_3846 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3847);
																					}
																					if ((2L < BgL_odepthz00_3846))
																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1802z00_3848;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1803z00_3849;

																								BgL_arg1803z00_3849 =
																									(BgL_oclassz00_3841);
																								BgL_arg1802z00_3848 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3849, 2L);
																							}
																							BgL_res2142z00_3840 =
																								(BgL_arg1802z00_3848 ==
																								BgL_classz00_3835);
																						}
																					else
																						{	/* Tools/trace.sch 53 */
																							BgL_res2142z00_3840 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2243z00_5267 = BgL_res2142z00_3840;
																}
														}
													}
													if (BgL_test2243z00_5267)
														{	/* Tools/trace.sch 53 */
															BgL_test2242z00_5266 =
																(
																((obj_t)
																	(((BgL_varcz00_bglt) COBJECT(
																				(((BgL_csetqz00_bglt) COBJECT(
																							((BgL_csetqz00_bglt)
																								BgL_copz00_3833)))->
																					BgL_varz00)))->BgL_variablez00)) ==
																((obj_t) BgL_auxz00_3831));
														}
													else
														{	/* Tools/trace.sch 53 */
															BgL_test2242z00_5266 = ((bool_t) 0);
														}
												}
												if (BgL_test2242z00_5266)
													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg2003z00_3850;
														obj_t BgL_arg2004z00_3851;

														BgL_arg2003z00_3850 =
															CDR(((obj_t) BgL_oldzd2actualszd2_3729));
														{	/* Tools/trace.sch 53 */
															BgL_copz00_bglt BgL_arg2006z00_3852;

															BgL_arg2006z00_3852 =
																(((BgL_csetqz00_bglt) COBJECT(
																		((BgL_csetqz00_bglt) BgL_copz00_3833)))->
																BgL_valuez00);
															BgL_arg2004z00_3851 =
																MAKE_YOUNG_PAIR(((obj_t) BgL_arg2006z00_3852),
																BgL_newzd2actualszd2_3730);
														}
														{
															obj_t BgL_newzd2actualszd2_5304;
															obj_t BgL_oldzd2actualszd2_5303;

															BgL_oldzd2actualszd2_5303 = BgL_arg2003z00_3850;
															BgL_newzd2actualszd2_5304 = BgL_arg2004z00_3851;
															BgL_newzd2actualszd2_3730 =
																BgL_newzd2actualszd2_5304;
															BgL_oldzd2actualszd2_3729 =
																BgL_oldzd2actualszd2_5303;
															goto BgL_loopz00_3728;
														}
													}
												else
													{	/* Tools/trace.sch 53 */
														obj_t BgL_arg2007z00_3853;
														obj_t BgL_arg2008z00_3854;
														obj_t BgL_arg2009z00_3855;
														obj_t BgL_arg2010z00_3856;

														BgL_arg2007z00_3853 =
															CDR(((obj_t) BgL_oldzd2actualszd2_3729));
														{	/* Tools/trace.sch 53 */
															BgL_varcz00_bglt BgL_arg2011z00_3857;

															{	/* Tools/trace.sch 53 */
																BgL_varcz00_bglt BgL_new1209z00_3858;

																{	/* Tools/trace.sch 53 */
																	BgL_varcz00_bglt BgL_new1208z00_3859;

																	BgL_new1208z00_3859 =
																		((BgL_varcz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_varcz00_bgl))));
																	{	/* Tools/trace.sch 53 */
																		long BgL_arg2013z00_3860;

																		BgL_arg2013z00_3860 =
																			BGL_CLASS_NUM(BGl_varcz00zzcgen_copz00);
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1208z00_3859),
																			BgL_arg2013z00_3860);
																	}
																	BgL_new1209z00_3858 = BgL_new1208z00_3859;
																}
																((((BgL_copz00_bglt) COBJECT(
																				((BgL_copz00_bglt)
																					BgL_new1209z00_3858)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) CAR(((obj_t)
																								BgL_oldzd2actualszd2_3729)))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																					BgL_new1209z00_3858)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						BgL_auxz00_3831)))->BgL_typez00)),
																	BUNSPEC);
																((((BgL_varcz00_bglt)
																			COBJECT(BgL_new1209z00_3858))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt) BgL_auxz00_3831)),
																	BUNSPEC);
																BgL_arg2011z00_3857 = BgL_new1209z00_3858;
															}
															BgL_arg2008z00_3854 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg2011z00_3857),
																BgL_newzd2actualszd2_3730);
														}
														BgL_arg2009z00_3855 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_auxz00_3831), BgL_auxsz00_3731);
														BgL_arg2010z00_3856 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_copz00_3833), BgL_expsz00_3732);
														{
															obj_t BgL_expsz00_5332;
															obj_t BgL_auxsz00_5331;
															obj_t BgL_newzd2actualszd2_5330;
															obj_t BgL_oldzd2actualszd2_5329;

															BgL_oldzd2actualszd2_5329 = BgL_arg2007z00_3853;
															BgL_newzd2actualszd2_5330 = BgL_arg2008z00_3854;
															BgL_auxsz00_5331 = BgL_arg2009z00_3855;
															BgL_expsz00_5332 = BgL_arg2010z00_3856;
															BgL_expsz00_3732 = BgL_expsz00_5332;
															BgL_auxsz00_3731 = BgL_auxsz00_5331;
															BgL_newzd2actualszd2_3730 =
																BgL_newzd2actualszd2_5330;
															BgL_oldzd2actualszd2_3729 =
																BgL_oldzd2actualszd2_5329;
															goto BgL_loopz00_3728;
														}
													}
											}
										}
									}
								}
							}
						return ((BgL_copz00_bglt) BgL_auxz00_4968);
					}
				}
			}
		}

	}



/* &node->cop-app-ly1462 */
	BgL_copz00_bglt BGl_z62nodezd2ze3copzd2appzd2ly1462z53zzcgen_cappz00(obj_t
		BgL_envz00_3674, obj_t BgL_nodez00_3675, obj_t BgL_kontz00_3676,
		obj_t BgL_inpushexitz00_3677)
	{
		{	/* Cgen/capp.scm 34 */
			{	/* Tools/trace.sch 53 */
				BgL_nodez00_bglt BgL_valuez00_3863;

				BgL_valuez00_3863 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_3675)))->BgL_argz00);
				{	/* Tools/trace.sch 53 */
					BgL_localz00_bglt BgL_vauxz00_3864;

					BgL_vauxz00_3864 =
						BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(CNST_TABLE_REF(0),
						((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
					{	/* Tools/trace.sch 53 */
						BgL_copz00_bglt BgL_vcopz00_3865;

						{	/* Tools/trace.sch 53 */
							BgL_setqz00_bglt BgL_arg1955z00_3866;

							BgL_arg1955z00_3866 =
								BGl_nodezd2setqzd2zzcgen_cgenz00(
								((BgL_variablez00_bglt) BgL_vauxz00_3864), BgL_valuez00_3863);
							BgL_vcopz00_3865 =
								BGl_nodezd2ze3copz31zzcgen_cgenz00(
								((BgL_nodez00_bglt) BgL_arg1955z00_3866),
								BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
								CBOOL(BgL_inpushexitz00_3677));
						}
						{	/* Tools/trace.sch 53 */
							BgL_nodez00_bglt BgL_funz00_3867;

							BgL_funz00_3867 =
								(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_3675)))->BgL_funz00);
							{	/* Tools/trace.sch 53 */
								BgL_localz00_bglt BgL_fauxz00_3868;

								BgL_fauxz00_3868 =
									BGl_makezd2localzd2svarzf2namezf2zzcgen_cgenz00(CNST_TABLE_REF
									(6),
									((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00));
								{	/* Tools/trace.sch 53 */
									BgL_copz00_bglt BgL_fcopz00_3869;

									{	/* Tools/trace.sch 53 */
										BgL_setqz00_bglt BgL_arg1954z00_3870;

										BgL_arg1954z00_3870 =
											BGl_nodezd2setqzd2zzcgen_cgenz00(
											((BgL_variablez00_bglt) BgL_fauxz00_3868),
											BgL_funz00_3867);
										BgL_fcopz00_3869 =
											BGl_nodezd2ze3copz31zzcgen_cgenz00(((BgL_nodez00_bglt)
												BgL_arg1954z00_3870),
											BGl_za2idzd2kontza2zd2zzcgen_cgenz00,
											CBOOL(BgL_inpushexitz00_3677));
									}
									{	/* Tools/trace.sch 53 */

										{	/* Tools/trace.sch 53 */
											bool_t BgL_test2247z00_5354;

											{	/* Tools/trace.sch 53 */
												bool_t BgL_test2248z00_5355;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_classz00_3871;

													BgL_classz00_3871 = BGl_csetqz00zzcgen_copz00;
													{	/* Tools/trace.sch 53 */
														BgL_objectz00_bglt BgL_arg1807z00_3872;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_tmpz00_5356;

															BgL_tmpz00_5356 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_vcopz00_3865));
															BgL_arg1807z00_3872 =
																(BgL_objectz00_bglt) (BgL_tmpz00_5356);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Tools/trace.sch 53 */
																long BgL_idxz00_3873;

																BgL_idxz00_3873 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3872);
																BgL_test2248z00_5355 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3873 + 2L)) ==
																	BgL_classz00_3871);
															}
														else
															{	/* Tools/trace.sch 53 */
																bool_t BgL_res2125z00_3876;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_oclassz00_3877;

																	{	/* Tools/trace.sch 53 */
																		obj_t BgL_arg1815z00_3878;
																		long BgL_arg1816z00_3879;

																		BgL_arg1815z00_3878 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Tools/trace.sch 53 */
																			long BgL_arg1817z00_3880;

																			BgL_arg1817z00_3880 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3872);
																			BgL_arg1816z00_3879 =
																				(BgL_arg1817z00_3880 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3877 =
																			VECTOR_REF(BgL_arg1815z00_3878,
																			BgL_arg1816z00_3879);
																	}
																	{	/* Tools/trace.sch 53 */
																		bool_t BgL__ortest_1115z00_3881;

																		BgL__ortest_1115z00_3881 =
																			(BgL_classz00_3871 == BgL_oclassz00_3877);
																		if (BgL__ortest_1115z00_3881)
																			{	/* Tools/trace.sch 53 */
																				BgL_res2125z00_3876 =
																					BgL__ortest_1115z00_3881;
																			}
																		else
																			{	/* Tools/trace.sch 53 */
																				long BgL_odepthz00_3882;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1804z00_3883;

																					BgL_arg1804z00_3883 =
																						(BgL_oclassz00_3877);
																					BgL_odepthz00_3882 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3883);
																				}
																				if ((2L < BgL_odepthz00_3882))
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1802z00_3884;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1803z00_3885;

																							BgL_arg1803z00_3885 =
																								(BgL_oclassz00_3877);
																							BgL_arg1802z00_3884 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3885, 2L);
																						}
																						BgL_res2125z00_3876 =
																							(BgL_arg1802z00_3884 ==
																							BgL_classz00_3871);
																					}
																				else
																					{	/* Tools/trace.sch 53 */
																						BgL_res2125z00_3876 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2248z00_5355 = BgL_res2125z00_3876;
															}
													}
												}
												if (BgL_test2248z00_5355)
													{	/* Tools/trace.sch 53 */
														if (
															(((obj_t)
																	(((BgL_varcz00_bglt) COBJECT(
																				(((BgL_csetqz00_bglt) COBJECT(
																							((BgL_csetqz00_bglt)
																								BgL_vcopz00_3865)))->
																					BgL_varz00)))->BgL_variablez00)) ==
																((obj_t) BgL_vauxz00_3864)))
															{	/* Tools/trace.sch 53 */
																bool_t BgL_test2253z00_5386;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_classz00_3886;

																	BgL_classz00_3886 = BGl_csetqz00zzcgen_copz00;
																	{	/* Tools/trace.sch 53 */
																		BgL_objectz00_bglt BgL_arg1807z00_3887;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_tmpz00_5387;

																			BgL_tmpz00_5387 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_fcopz00_3869));
																			BgL_arg1807z00_3887 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_5387);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Tools/trace.sch 53 */
																				long BgL_idxz00_3888;

																				BgL_idxz00_3888 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_3887);
																				BgL_test2253z00_5386 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_3888 + 2L)) ==
																					BgL_classz00_3886);
																			}
																		else
																			{	/* Tools/trace.sch 53 */
																				bool_t BgL_res2126z00_3891;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_oclassz00_3892;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1815z00_3893;
																						long BgL_arg1816z00_3894;

																						BgL_arg1815z00_3893 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg1817z00_3895;

																							BgL_arg1817z00_3895 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_3887);
																							BgL_arg1816z00_3894 =
																								(BgL_arg1817z00_3895 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_3892 =
																							VECTOR_REF(BgL_arg1815z00_3893,
																							BgL_arg1816z00_3894);
																					}
																					{	/* Tools/trace.sch 53 */
																						bool_t BgL__ortest_1115z00_3896;

																						BgL__ortest_1115z00_3896 =
																							(BgL_classz00_3886 ==
																							BgL_oclassz00_3892);
																						if (BgL__ortest_1115z00_3896)
																							{	/* Tools/trace.sch 53 */
																								BgL_res2126z00_3891 =
																									BgL__ortest_1115z00_3896;
																							}
																						else
																							{	/* Tools/trace.sch 53 */
																								long BgL_odepthz00_3897;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1804z00_3898;

																									BgL_arg1804z00_3898 =
																										(BgL_oclassz00_3892);
																									BgL_odepthz00_3897 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_3898);
																								}
																								if ((2L < BgL_odepthz00_3897))
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1802z00_3899;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1803z00_3900;

																											BgL_arg1803z00_3900 =
																												(BgL_oclassz00_3892);
																											BgL_arg1802z00_3899 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_3900,
																												2L);
																										}
																										BgL_res2126z00_3891 =
																											(BgL_arg1802z00_3899 ==
																											BgL_classz00_3886);
																									}
																								else
																									{	/* Tools/trace.sch 53 */
																										BgL_res2126z00_3891 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2253z00_5386 =
																					BgL_res2126z00_3891;
																			}
																	}
																}
																if (BgL_test2253z00_5386)
																	{	/* Tools/trace.sch 53 */
																		BgL_test2247z00_5354 =
																			(
																			((obj_t)
																				(((BgL_varcz00_bglt) COBJECT(
																							(((BgL_csetqz00_bglt) COBJECT(
																										((BgL_csetqz00_bglt)
																											BgL_fcopz00_3869)))->
																								BgL_varz00)))->
																					BgL_variablez00)) ==
																			((obj_t) BgL_fauxz00_3868));
																	}
																else
																	{	/* Tools/trace.sch 53 */
																		BgL_test2247z00_5354 = ((bool_t) 0);
																	}
															}
														else
															{	/* Tools/trace.sch 53 */
																BgL_test2247z00_5354 = ((bool_t) 0);
															}
													}
												else
													{	/* Tools/trace.sch 53 */
														BgL_test2247z00_5354 = ((bool_t) 0);
													}
											}
											if (BgL_test2247z00_5354)
												{	/* Tools/trace.sch 53 */
													BgL_capplyz00_bglt BgL_arg1880z00_3901;

													{	/* Tools/trace.sch 53 */
														BgL_capplyz00_bglt BgL_new1136z00_3902;

														{	/* Tools/trace.sch 53 */
															BgL_capplyz00_bglt BgL_new1135z00_3903;

															BgL_new1135z00_3903 =
																((BgL_capplyz00_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_capplyz00_bgl))));
															{	/* Tools/trace.sch 53 */
																long BgL_arg1882z00_3904;

																BgL_arg1882z00_3904 =
																	BGL_CLASS_NUM(BGl_capplyz00zzcgen_copz00);
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_new1135z00_3903),
																	BgL_arg1882z00_3904);
															}
															BgL_new1136z00_3902 = BgL_new1135z00_3903;
														}
														((((BgL_copz00_bglt) COBJECT(
																		((BgL_copz00_bglt) BgL_new1136z00_3902)))->
																BgL_locz00) =
															((obj_t) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_appzd2lyzd2_bglt)
																					BgL_nodez00_3675))))->BgL_locz00)),
															BUNSPEC);
														((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																			BgL_new1136z00_3902)))->BgL_typez00) =
															((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																		COBJECT(((BgL_nodez00_bglt) (
																					(BgL_appzd2lyzd2_bglt)
																					BgL_nodez00_3675))))->BgL_typez00)),
															BUNSPEC);
														((((BgL_capplyz00_bglt)
																	COBJECT(BgL_new1136z00_3902))->BgL_funz00) =
															((BgL_copz00_bglt) (((BgL_csetqz00_bglt)
																		COBJECT(((BgL_csetqz00_bglt)
																				BgL_fcopz00_3869)))->BgL_valuez00)),
															BUNSPEC);
														((((BgL_capplyz00_bglt)
																	COBJECT(BgL_new1136z00_3902))->BgL_argz00) =
															((BgL_copz00_bglt) (((BgL_csetqz00_bglt)
																		COBJECT(((BgL_csetqz00_bglt)
																				BgL_vcopz00_3865)))->BgL_valuez00)),
															BUNSPEC);
														BgL_arg1880z00_3901 = BgL_new1136z00_3902;
													}
													return
														((BgL_copz00_bglt)
														BGL_PROCEDURE_CALL1(BgL_kontz00_3676,
															((obj_t) BgL_arg1880z00_3901)));
												}
											else
												{	/* Tools/trace.sch 53 */
													bool_t BgL_test2257z00_5442;

													{	/* Tools/trace.sch 53 */
														bool_t BgL_test2258z00_5443;

														{	/* Tools/trace.sch 53 */
															obj_t BgL_classz00_3905;

															BgL_classz00_3905 = BGl_csetqz00zzcgen_copz00;
															{	/* Tools/trace.sch 53 */
																BgL_objectz00_bglt BgL_arg1807z00_3906;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_tmpz00_5444;

																	BgL_tmpz00_5444 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_vcopz00_3865));
																	BgL_arg1807z00_3906 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_5444);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Tools/trace.sch 53 */
																		long BgL_idxz00_3907;

																		BgL_idxz00_3907 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3906);
																		BgL_test2258z00_5443 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3907 + 2L)) ==
																			BgL_classz00_3905);
																	}
																else
																	{	/* Tools/trace.sch 53 */
																		bool_t BgL_res2127z00_3910;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_oclassz00_3911;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_arg1815z00_3912;
																				long BgL_arg1816z00_3913;

																				BgL_arg1815z00_3912 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Tools/trace.sch 53 */
																					long BgL_arg1817z00_3914;

																					BgL_arg1817z00_3914 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3906);
																					BgL_arg1816z00_3913 =
																						(BgL_arg1817z00_3914 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3911 =
																					VECTOR_REF(BgL_arg1815z00_3912,
																					BgL_arg1816z00_3913);
																			}
																			{	/* Tools/trace.sch 53 */
																				bool_t BgL__ortest_1115z00_3915;

																				BgL__ortest_1115z00_3915 =
																					(BgL_classz00_3905 ==
																					BgL_oclassz00_3911);
																				if (BgL__ortest_1115z00_3915)
																					{	/* Tools/trace.sch 53 */
																						BgL_res2127z00_3910 =
																							BgL__ortest_1115z00_3915;
																					}
																				else
																					{	/* Tools/trace.sch 53 */
																						long BgL_odepthz00_3916;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1804z00_3917;

																							BgL_arg1804z00_3917 =
																								(BgL_oclassz00_3911);
																							BgL_odepthz00_3916 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3917);
																						}
																						if ((2L < BgL_odepthz00_3916))
																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1802z00_3918;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1803z00_3919;

																									BgL_arg1803z00_3919 =
																										(BgL_oclassz00_3911);
																									BgL_arg1802z00_3918 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3919, 2L);
																								}
																								BgL_res2127z00_3910 =
																									(BgL_arg1802z00_3918 ==
																									BgL_classz00_3905);
																							}
																						else
																							{	/* Tools/trace.sch 53 */
																								BgL_res2127z00_3910 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2258z00_5443 = BgL_res2127z00_3910;
																	}
															}
														}
														if (BgL_test2258z00_5443)
															{	/* Tools/trace.sch 53 */
																BgL_test2257z00_5442 =
																	(
																	((obj_t)
																		(((BgL_varcz00_bglt) COBJECT(
																					(((BgL_csetqz00_bglt) COBJECT(
																								((BgL_csetqz00_bglt)
																									BgL_vcopz00_3865)))->
																						BgL_varz00)))->BgL_variablez00)) ==
																	((obj_t) BgL_vauxz00_3864));
															}
														else
															{	/* Tools/trace.sch 53 */
																BgL_test2257z00_5442 = ((bool_t) 0);
															}
													}
													if (BgL_test2257z00_5442)
														{	/* Tools/trace.sch 53 */
															BgL_cblockz00_bglt BgL_new1138z00_3920;

															{	/* Tools/trace.sch 53 */
																BgL_cblockz00_bglt BgL_new1137z00_3921;

																BgL_new1137z00_3921 =
																	((BgL_cblockz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_cblockz00_bgl))));
																{	/* Tools/trace.sch 53 */
																	long BgL_arg1902z00_3922;

																	BgL_arg1902z00_3922 =
																		BGL_CLASS_NUM(BGl_cblockz00zzcgen_copz00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1137z00_3921),
																		BgL_arg1902z00_3922);
																}
																BgL_new1138z00_3920 = BgL_new1137z00_3921;
															}
															((((BgL_copz00_bglt) COBJECT(
																			((BgL_copz00_bglt)
																				BgL_new1138z00_3920)))->BgL_locz00) =
																((obj_t) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_appzd2lyzd2_bglt)
																						BgL_nodez00_3675))))->BgL_locz00)),
																BUNSPEC);
															((((BgL_copz00_bglt) COBJECT(((BgL_copz00_bglt)
																				BgL_new1138z00_3920)))->BgL_typez00) =
																((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_appzd2lyzd2_bglt)
																						BgL_nodez00_3675))))->BgL_typez00)),
																BUNSPEC);
															{
																BgL_copz00_bglt BgL_auxz00_5487;

																{	/* Tools/trace.sch 53 */
																	BgL_csequencez00_bglt BgL_new1140z00_3923;

																	{	/* Tools/trace.sch 53 */
																		BgL_csequencez00_bglt BgL_new1139z00_3924;

																		BgL_new1139z00_3924 =
																			((BgL_csequencez00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_csequencez00_bgl))));
																		{	/* Tools/trace.sch 53 */
																			long BgL_arg1901z00_3925;

																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_classz00_3926;

																				BgL_classz00_3926 =
																					BGl_csequencez00zzcgen_copz00;
																				BgL_arg1901z00_3925 =
																					BGL_CLASS_NUM(BgL_classz00_3926);
																			}
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1139z00_3924),
																				BgL_arg1901z00_3925);
																		}
																		BgL_new1140z00_3923 = BgL_new1139z00_3924;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1140z00_3923)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_appzd2lyzd2_bglt)
																								BgL_nodez00_3675))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1140z00_3923)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_appzd2lyzd2_bglt)
																								BgL_nodez00_3675))))->
																				BgL_typez00)), BUNSPEC);
																	((((BgL_csequencez00_bglt)
																				COBJECT(BgL_new1140z00_3923))->
																			BgL_czd2expzf3z21) =
																		((bool_t) ((bool_t) 0)), BUNSPEC);
																	{
																		obj_t BgL_auxz00_5503;

																		{	/* Tools/trace.sch 53 */
																			BgL_localzd2varzd2_bglt
																				BgL_arg1887z00_3927;
																			BgL_csequencez00_bglt BgL_arg1888z00_3928;
																			obj_t BgL_arg1889z00_3929;

																			{	/* Tools/trace.sch 53 */
																				BgL_localzd2varzd2_bglt
																					BgL_new1142z00_3930;
																				{	/* Tools/trace.sch 53 */
																					BgL_localzd2varzd2_bglt
																						BgL_new1141z00_3931;
																					BgL_new1141z00_3931 =
																						((BgL_localzd2varzd2_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_localzd2varzd2_bgl))));
																					{	/* Tools/trace.sch 53 */
																						long BgL_arg1894z00_3932;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_classz00_3933;

																							BgL_classz00_3933 =
																								BGl_localzd2varzd2zzcgen_copz00;
																							BgL_arg1894z00_3932 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3933);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1141z00_3931),
																							BgL_arg1894z00_3932);
																					}
																					BgL_new1142z00_3930 =
																						BgL_new1141z00_3931;
																				}
																				((((BgL_copz00_bglt) COBJECT(
																								((BgL_copz00_bglt)
																									BgL_new1142z00_3930)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt) (
																											(BgL_appzd2lyzd2_bglt)
																											BgL_nodez00_3675))))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_copz00_bglt)
																							COBJECT(((BgL_copz00_bglt)
																									BgL_new1142z00_3930)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) (
																							(BgL_typez00_bglt)
																							BGl_za2objza2z00zztype_cachez00)),
																					BUNSPEC);
																				{
																					obj_t BgL_auxz00_5516;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_list1893z00_3934;

																						BgL_list1893z00_3934 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_fauxz00_3868), BNIL);
																						BgL_auxz00_5516 =
																							BgL_list1893z00_3934;
																					}
																					((((BgL_localzd2varzd2_bglt)
																								COBJECT(BgL_new1142z00_3930))->
																							BgL_varsz00) =
																						((obj_t) BgL_auxz00_5516), BUNSPEC);
																				}
																				BgL_arg1887z00_3927 =
																					BgL_new1142z00_3930;
																			}
																			{	/* Tools/trace.sch 53 */
																				BgL_csequencez00_bglt
																					BgL_new1144z00_3935;
																				{	/* Tools/trace.sch 53 */
																					BgL_csequencez00_bglt
																						BgL_new1143z00_3936;
																					BgL_new1143z00_3936 =
																						((BgL_csequencez00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_csequencez00_bgl))));
																					{	/* Tools/trace.sch 53 */
																						long BgL_arg1896z00_3937;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_classz00_3938;

																							BgL_classz00_3938 =
																								BGl_csequencez00zzcgen_copz00;
																							BgL_arg1896z00_3937 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3938);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1143z00_3936),
																							BgL_arg1896z00_3937);
																					}
																					BgL_new1144z00_3935 =
																						BgL_new1143z00_3936;
																				}
																				((((BgL_copz00_bglt) COBJECT(
																								((BgL_copz00_bglt)
																									BgL_new1144z00_3935)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_nodez00_bglt)
																								COBJECT(((BgL_nodez00_bglt) (
																											(BgL_appzd2lyzd2_bglt)
																											BgL_nodez00_3675))))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_copz00_bglt)
																							COBJECT(((BgL_copz00_bglt)
																									BgL_new1144z00_3935)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) (
																							(BgL_typez00_bglt)
																							BGl_za2objza2z00zztype_cachez00)),
																					BUNSPEC);
																				((((BgL_csequencez00_bglt)
																							COBJECT(BgL_new1144z00_3935))->
																						BgL_czd2expzf3z21) =
																					((bool_t) ((bool_t) 0)), BUNSPEC);
																				{
																					obj_t BgL_auxz00_5533;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_list1895z00_3939;

																						BgL_list1895z00_3939 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_fcopz00_3869), BNIL);
																						BgL_auxz00_5533 =
																							BgL_list1895z00_3939;
																					}
																					((((BgL_csequencez00_bglt)
																								COBJECT(BgL_new1144z00_3935))->
																							BgL_copsz00) =
																						((obj_t) BgL_auxz00_5533), BUNSPEC);
																				}
																				BgL_arg1888z00_3928 =
																					BgL_new1144z00_3935;
																			}
																			{	/* Tools/trace.sch 53 */
																				BgL_capplyz00_bglt BgL_arg1897z00_3940;

																				{	/* Tools/trace.sch 53 */
																					BgL_capplyz00_bglt
																						BgL_new1146z00_3941;
																					{	/* Tools/trace.sch 53 */
																						BgL_capplyz00_bglt
																							BgL_new1145z00_3942;
																						BgL_new1145z00_3942 =
																							((BgL_capplyz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_capplyz00_bgl))));
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg1899z00_3943;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_classz00_3944;

																								BgL_classz00_3944 =
																									BGl_capplyz00zzcgen_copz00;
																								BgL_arg1899z00_3943 =
																									BGL_CLASS_NUM
																									(BgL_classz00_3944);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1145z00_3942),
																								BgL_arg1899z00_3943);
																						}
																						BgL_new1146z00_3941 =
																							BgL_new1145z00_3942;
																					}
																					((((BgL_copz00_bglt) COBJECT(
																									((BgL_copz00_bglt)
																										BgL_new1146z00_3941)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt) (
																												(BgL_appzd2lyzd2_bglt)
																												BgL_nodez00_3675))))->
																								BgL_locz00)), BUNSPEC);
																					((((BgL_copz00_bglt)
																								COBJECT(((BgL_copz00_bglt)
																										BgL_new1146z00_3941)))->
																							BgL_typez00) =
																						((BgL_typez00_bglt) ((
																									(BgL_nodez00_bglt)
																									COBJECT(((BgL_nodez00_bglt) (
																												(BgL_appzd2lyzd2_bglt)
																												BgL_nodez00_3675))))->
																								BgL_typez00)), BUNSPEC);
																					{
																						BgL_copz00_bglt BgL_auxz00_5551;

																						{	/* Tools/trace.sch 53 */
																							BgL_varcz00_bglt
																								BgL_new1148z00_3945;
																							{	/* Tools/trace.sch 53 */
																								BgL_varcz00_bglt
																									BgL_new1147z00_3946;
																								BgL_new1147z00_3946 =
																									((BgL_varcz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_varcz00_bgl))));
																								{	/* Tools/trace.sch 53 */
																									long BgL_arg1898z00_3947;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_classz00_3948;

																										BgL_classz00_3948 =
																											BGl_varcz00zzcgen_copz00;
																										BgL_arg1898z00_3947 =
																											BGL_CLASS_NUM
																											(BgL_classz00_3948);
																									}
																									BGL_OBJECT_CLASS_NUM_SET(
																										((BgL_objectz00_bglt)
																											BgL_new1147z00_3946),
																										BgL_arg1898z00_3947);
																								}
																								BgL_new1148z00_3945 =
																									BgL_new1147z00_3946;
																							}
																							((((BgL_copz00_bglt) COBJECT(
																											((BgL_copz00_bglt)
																												BgL_new1148z00_3945)))->
																									BgL_locz00) =
																								((obj_t) (((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt) (
																														(BgL_appzd2lyzd2_bglt)
																														BgL_nodez00_3675))))->
																										BgL_locz00)), BUNSPEC);
																							((((BgL_copz00_bglt)
																										COBJECT(((BgL_copz00_bglt)
																												BgL_new1148z00_3945)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt) ((
																											(BgL_variablez00_bglt)
																											COBJECT((
																													(BgL_variablez00_bglt)
																													BgL_fauxz00_3868)))->
																										BgL_typez00)), BUNSPEC);
																							((((BgL_varcz00_bglt)
																										COBJECT
																										(BgL_new1148z00_3945))->
																									BgL_variablez00) =
																								((BgL_variablez00_bglt) (
																										(BgL_variablez00_bglt)
																										BgL_fauxz00_3868)),
																								BUNSPEC);
																							BgL_auxz00_5551 =
																								((BgL_copz00_bglt)
																								BgL_new1148z00_3945);
																						}
																						((((BgL_capplyz00_bglt)
																									COBJECT
																									(BgL_new1146z00_3941))->
																								BgL_funz00) =
																							((BgL_copz00_bglt)
																								BgL_auxz00_5551), BUNSPEC);
																					}
																					((((BgL_capplyz00_bglt)
																								COBJECT(BgL_new1146z00_3941))->
																							BgL_argz00) =
																						((BgL_copz00_bglt) ((
																									(BgL_csetqz00_bglt)
																									COBJECT(((BgL_csetqz00_bglt)
																											BgL_vcopz00_3865)))->
																								BgL_valuez00)), BUNSPEC);
																					BgL_arg1897z00_3940 =
																						BgL_new1146z00_3941;
																				}
																				BgL_arg1889z00_3929 =
																					BGL_PROCEDURE_CALL1(BgL_kontz00_3676,
																					((obj_t) BgL_arg1897z00_3940));
																			}
																			{	/* Tools/trace.sch 53 */
																				obj_t BgL_list1890z00_3949;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_arg1891z00_3950;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1892z00_3951;

																						BgL_arg1892z00_3951 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1889z00_3929, BNIL);
																						BgL_arg1891z00_3950 =
																							MAKE_YOUNG_PAIR(((obj_t)
																								BgL_arg1888z00_3928),
																							BgL_arg1892z00_3951);
																					}
																					BgL_list1890z00_3949 =
																						MAKE_YOUNG_PAIR(
																						((obj_t) BgL_arg1887z00_3927),
																						BgL_arg1891z00_3950);
																				}
																				BgL_auxz00_5503 = BgL_list1890z00_3949;
																		}}
																		((((BgL_csequencez00_bglt)
																					COBJECT(BgL_new1140z00_3923))->
																				BgL_copsz00) =
																			((obj_t) BgL_auxz00_5503), BUNSPEC);
																	}
																	BgL_auxz00_5487 =
																		((BgL_copz00_bglt) BgL_new1140z00_3923);
																}
																((((BgL_cblockz00_bglt)
																			COBJECT(BgL_new1138z00_3920))->
																		BgL_bodyz00) =
																	((BgL_copz00_bglt) BgL_auxz00_5487), BUNSPEC);
															}
															return ((BgL_copz00_bglt) BgL_new1138z00_3920);
														}
													else
														{	/* Tools/trace.sch 53 */
															bool_t BgL_test2262z00_5586;

															{	/* Tools/trace.sch 53 */
																bool_t BgL_test2263z00_5587;

																{	/* Tools/trace.sch 53 */
																	obj_t BgL_classz00_3952;

																	BgL_classz00_3952 = BGl_csetqz00zzcgen_copz00;
																	{	/* Tools/trace.sch 53 */
																		BgL_objectz00_bglt BgL_arg1807z00_3953;

																		{	/* Tools/trace.sch 53 */
																			obj_t BgL_tmpz00_5588;

																			BgL_tmpz00_5588 =
																				((obj_t)
																				((BgL_objectz00_bglt)
																					BgL_fcopz00_3869));
																			BgL_arg1807z00_3953 =
																				(BgL_objectz00_bglt) (BgL_tmpz00_5588);
																		}
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Tools/trace.sch 53 */
																				long BgL_idxz00_3954;

																				BgL_idxz00_3954 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_3953);
																				BgL_test2263z00_5587 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_3954 + 2L)) ==
																					BgL_classz00_3952);
																			}
																		else
																			{	/* Tools/trace.sch 53 */
																				bool_t BgL_res2131z00_3957;

																				{	/* Tools/trace.sch 53 */
																					obj_t BgL_oclassz00_3958;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_arg1815z00_3959;
																						long BgL_arg1816z00_3960;

																						BgL_arg1815z00_3959 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Tools/trace.sch 53 */
																							long BgL_arg1817z00_3961;

																							BgL_arg1817z00_3961 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_3953);
																							BgL_arg1816z00_3960 =
																								(BgL_arg1817z00_3961 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_3958 =
																							VECTOR_REF(BgL_arg1815z00_3959,
																							BgL_arg1816z00_3960);
																					}
																					{	/* Tools/trace.sch 53 */
																						bool_t BgL__ortest_1115z00_3962;

																						BgL__ortest_1115z00_3962 =
																							(BgL_classz00_3952 ==
																							BgL_oclassz00_3958);
																						if (BgL__ortest_1115z00_3962)
																							{	/* Tools/trace.sch 53 */
																								BgL_res2131z00_3957 =
																									BgL__ortest_1115z00_3962;
																							}
																						else
																							{	/* Tools/trace.sch 53 */
																								long BgL_odepthz00_3963;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1804z00_3964;

																									BgL_arg1804z00_3964 =
																										(BgL_oclassz00_3958);
																									BgL_odepthz00_3963 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_3964);
																								}
																								if ((2L < BgL_odepthz00_3963))
																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_arg1802z00_3965;

																										{	/* Tools/trace.sch 53 */
																											obj_t BgL_arg1803z00_3966;

																											BgL_arg1803z00_3966 =
																												(BgL_oclassz00_3958);
																											BgL_arg1802z00_3965 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_3966,
																												2L);
																										}
																										BgL_res2131z00_3957 =
																											(BgL_arg1802z00_3965 ==
																											BgL_classz00_3952);
																									}
																								else
																									{	/* Tools/trace.sch 53 */
																										BgL_res2131z00_3957 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2263z00_5587 =
																					BgL_res2131z00_3957;
																			}
																	}
																}
																if (BgL_test2263z00_5587)
																	{	/* Tools/trace.sch 53 */
																		BgL_test2262z00_5586 =
																			(
																			((obj_t)
																				(((BgL_varcz00_bglt) COBJECT(
																							(((BgL_csetqz00_bglt) COBJECT(
																										((BgL_csetqz00_bglt)
																											BgL_fcopz00_3869)))->
																								BgL_varz00)))->
																					BgL_variablez00)) ==
																			((obj_t) BgL_fauxz00_3868));
																	}
																else
																	{	/* Tools/trace.sch 53 */
																		BgL_test2262z00_5586 = ((bool_t) 0);
																	}
															}
															if (BgL_test2262z00_5586)
																{	/* Tools/trace.sch 53 */
																	BgL_cblockz00_bglt BgL_new1150z00_3967;

																	{	/* Tools/trace.sch 53 */
																		BgL_cblockz00_bglt BgL_new1149z00_3968;

																		BgL_new1149z00_3968 =
																			((BgL_cblockz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_cblockz00_bgl))));
																		{	/* Tools/trace.sch 53 */
																			long BgL_arg1927z00_3969;

																			BgL_arg1927z00_3969 =
																				BGL_CLASS_NUM
																				(BGl_cblockz00zzcgen_copz00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1149z00_3968),
																				BgL_arg1927z00_3969);
																		}
																		BgL_new1150z00_3967 = BgL_new1149z00_3968;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1150z00_3967)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_appzd2lyzd2_bglt)
																								BgL_nodez00_3675))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1150z00_3967)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_appzd2lyzd2_bglt)
																								BgL_nodez00_3675))))->
																				BgL_typez00)), BUNSPEC);
																	{
																		BgL_copz00_bglt BgL_auxz00_5631;

																		{	/* Tools/trace.sch 53 */
																			BgL_csequencez00_bglt BgL_new1152z00_3970;

																			{	/* Tools/trace.sch 53 */
																				BgL_csequencez00_bglt
																					BgL_new1151z00_3971;
																				BgL_new1151z00_3971 =
																					((BgL_csequencez00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_csequencez00_bgl))));
																				{	/* Tools/trace.sch 53 */
																					long BgL_arg1926z00_3972;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_classz00_3973;

																						BgL_classz00_3973 =
																							BGl_csequencez00zzcgen_copz00;
																						BgL_arg1926z00_3972 =
																							BGL_CLASS_NUM(BgL_classz00_3973);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1151z00_3971),
																						BgL_arg1926z00_3972);
																				}
																				BgL_new1152z00_3970 =
																					BgL_new1151z00_3971;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1152z00_3970)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt) (
																										(BgL_appzd2lyzd2_bglt)
																										BgL_nodez00_3675))))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1152z00_3970)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt) (
																										(BgL_appzd2lyzd2_bglt)
																										BgL_nodez00_3675))))->
																						BgL_typez00)), BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1152z00_3970))->
																					BgL_czd2expzf3z21) =
																				((bool_t) ((bool_t) 0)), BUNSPEC);
																			{
																				obj_t BgL_auxz00_5647;

																				{	/* Tools/trace.sch 53 */
																					BgL_localzd2varzd2_bglt
																						BgL_arg1910z00_3974;
																					BgL_csequencez00_bglt
																						BgL_arg1911z00_3975;
																					obj_t BgL_arg1912z00_3976;

																					{	/* Tools/trace.sch 53 */
																						BgL_localzd2varzd2_bglt
																							BgL_new1154z00_3977;
																						{	/* Tools/trace.sch 53 */
																							BgL_localzd2varzd2_bglt
																								BgL_new1153z00_3978;
																							BgL_new1153z00_3978 =
																								((BgL_localzd2varzd2_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_localzd2varzd2_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg1918z00_3979;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_3980;

																									BgL_classz00_3980 =
																										BGl_localzd2varzd2zzcgen_copz00;
																									BgL_arg1918z00_3979 =
																										BGL_CLASS_NUM
																										(BgL_classz00_3980);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1153z00_3978),
																									BgL_arg1918z00_3979);
																							}
																							BgL_new1154z00_3977 =
																								BgL_new1153z00_3978;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1154z00_3977)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_appzd2lyzd2_bglt)
																													BgL_nodez00_3675))))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1154z00_3977)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BGl_za2objza2z00zztype_cachez00)),
																							BUNSPEC);
																						{
																							obj_t BgL_auxz00_5660;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_list1917z00_3981;

																								BgL_list1917z00_3981 =
																									MAKE_YOUNG_PAIR(
																									((obj_t) BgL_vauxz00_3864),
																									BNIL);
																								BgL_auxz00_5660 =
																									BgL_list1917z00_3981;
																							}
																							((((BgL_localzd2varzd2_bglt)
																										COBJECT
																										(BgL_new1154z00_3977))->
																									BgL_varsz00) =
																								((obj_t) BgL_auxz00_5660),
																								BUNSPEC);
																						}
																						BgL_arg1910z00_3974 =
																							BgL_new1154z00_3977;
																					}
																					{	/* Tools/trace.sch 53 */
																						BgL_csequencez00_bglt
																							BgL_new1157z00_3982;
																						{	/* Tools/trace.sch 53 */
																							BgL_csequencez00_bglt
																								BgL_new1156z00_3983;
																							BgL_new1156z00_3983 =
																								((BgL_csequencez00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_csequencez00_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg1920z00_3984;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_3985;

																									BgL_classz00_3985 =
																										BGl_csequencez00zzcgen_copz00;
																									BgL_arg1920z00_3984 =
																										BGL_CLASS_NUM
																										(BgL_classz00_3985);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1156z00_3983),
																									BgL_arg1920z00_3984);
																							}
																							BgL_new1157z00_3982 =
																								BgL_new1156z00_3983;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1157z00_3982)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_appzd2lyzd2_bglt)
																													BgL_nodez00_3675))))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1157z00_3982)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BGl_za2objza2z00zztype_cachez00)),
																							BUNSPEC);
																						((((BgL_csequencez00_bglt)
																									COBJECT
																									(BgL_new1157z00_3982))->
																								BgL_czd2expzf3z21) =
																							((bool_t) ((bool_t) 0)), BUNSPEC);
																						{
																							obj_t BgL_auxz00_5677;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_list1919z00_3986;

																								BgL_list1919z00_3986 =
																									MAKE_YOUNG_PAIR(
																									((obj_t) BgL_vcopz00_3865),
																									BNIL);
																								BgL_auxz00_5677 =
																									BgL_list1919z00_3986;
																							}
																							((((BgL_csequencez00_bglt)
																										COBJECT
																										(BgL_new1157z00_3982))->
																									BgL_copsz00) =
																								((obj_t) BgL_auxz00_5677),
																								BUNSPEC);
																						}
																						BgL_arg1911z00_3975 =
																							BgL_new1157z00_3982;
																					}
																					{	/* Tools/trace.sch 53 */
																						BgL_capplyz00_bglt
																							BgL_arg1923z00_3987;
																						{	/* Tools/trace.sch 53 */
																							BgL_capplyz00_bglt
																								BgL_new1159z00_3988;
																							{	/* Tools/trace.sch 53 */
																								BgL_capplyz00_bglt
																									BgL_new1158z00_3989;
																								BgL_new1158z00_3989 =
																									((BgL_capplyz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_capplyz00_bgl))));
																								{	/* Tools/trace.sch 53 */
																									long BgL_arg1925z00_3990;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_classz00_3991;

																										BgL_classz00_3991 =
																											BGl_capplyz00zzcgen_copz00;
																										BgL_arg1925z00_3990 =
																											BGL_CLASS_NUM
																											(BgL_classz00_3991);
																									}
																									BGL_OBJECT_CLASS_NUM_SET(
																										((BgL_objectz00_bglt)
																											BgL_new1158z00_3989),
																										BgL_arg1925z00_3990);
																								}
																								BgL_new1159z00_3988 =
																									BgL_new1158z00_3989;
																							}
																							((((BgL_copz00_bglt) COBJECT(
																											((BgL_copz00_bglt)
																												BgL_new1159z00_3988)))->
																									BgL_locz00) =
																								((obj_t) (((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt) (
																														(BgL_appzd2lyzd2_bglt)
																														BgL_nodez00_3675))))->
																										BgL_locz00)), BUNSPEC);
																							((((BgL_copz00_bglt)
																										COBJECT(((BgL_copz00_bglt)
																												BgL_new1159z00_3988)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt) ((
																											(BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt) (
																														(BgL_appzd2lyzd2_bglt)
																														BgL_nodez00_3675))))->
																										BgL_typez00)), BUNSPEC);
																							((((BgL_capplyz00_bglt)
																										COBJECT
																										(BgL_new1159z00_3988))->
																									BgL_funz00) =
																								((BgL_copz00_bglt) ((
																											(BgL_csetqz00_bglt)
																											COBJECT((
																													(BgL_csetqz00_bglt)
																													BgL_fcopz00_3869)))->
																										BgL_valuez00)), BUNSPEC);
																							{
																								BgL_copz00_bglt BgL_auxz00_5698;

																								{	/* Tools/trace.sch 53 */
																									BgL_varcz00_bglt
																										BgL_new1161z00_3992;
																									{	/* Tools/trace.sch 53 */
																										BgL_varcz00_bglt
																											BgL_new1160z00_3993;
																										BgL_new1160z00_3993 =
																											((BgL_varcz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_varcz00_bgl))));
																										{	/* Tools/trace.sch 53 */
																											long BgL_arg1924z00_3994;

																											{	/* Tools/trace.sch 53 */
																												obj_t BgL_classz00_3995;

																												BgL_classz00_3995 =
																													BGl_varcz00zzcgen_copz00;
																												BgL_arg1924z00_3994 =
																													BGL_CLASS_NUM
																													(BgL_classz00_3995);
																											}
																											BGL_OBJECT_CLASS_NUM_SET(
																												((BgL_objectz00_bglt)
																													BgL_new1160z00_3993),
																												BgL_arg1924z00_3994);
																										}
																										BgL_new1161z00_3992 =
																											BgL_new1160z00_3993;
																									}
																									((((BgL_copz00_bglt) COBJECT(
																													((BgL_copz00_bglt)
																														BgL_new1161z00_3992)))->
																											BgL_locz00) =
																										((obj_t) ((
																													(BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															((BgL_appzd2lyzd2_bglt) BgL_nodez00_3675))))->BgL_locz00)), BUNSPEC);
																									((((BgL_copz00_bglt)
																												COBJECT((
																														(BgL_copz00_bglt)
																														BgL_new1161z00_3992)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt) ((
																													(BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															BgL_vauxz00_3864)))->
																												BgL_typez00)), BUNSPEC);
																									((((BgL_varcz00_bglt)
																												COBJECT
																												(BgL_new1161z00_3992))->
																											BgL_variablez00) =
																										((BgL_variablez00_bglt) (
																												(BgL_variablez00_bglt)
																												BgL_vauxz00_3864)),
																										BUNSPEC);
																									BgL_auxz00_5698 =
																										((BgL_copz00_bglt)
																										BgL_new1161z00_3992);
																								}
																								((((BgL_capplyz00_bglt)
																											COBJECT
																											(BgL_new1159z00_3988))->
																										BgL_argz00) =
																									((BgL_copz00_bglt)
																										BgL_auxz00_5698), BUNSPEC);
																							}
																							BgL_arg1923z00_3987 =
																								BgL_new1159z00_3988;
																						}
																						BgL_arg1912z00_3976 =
																							BGL_PROCEDURE_CALL1
																							(BgL_kontz00_3676,
																							((obj_t) BgL_arg1923z00_3987));
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_list1913z00_3996;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1914z00_3997;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1916z00_3998;

																								BgL_arg1916z00_3998 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1912z00_3976, BNIL);
																								BgL_arg1914z00_3997 =
																									MAKE_YOUNG_PAIR(((obj_t)
																										BgL_arg1911z00_3975),
																									BgL_arg1916z00_3998);
																							}
																							BgL_list1913z00_3996 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_arg1910z00_3974),
																								BgL_arg1914z00_3997);
																						}
																						BgL_auxz00_5647 =
																							BgL_list1913z00_3996;
																				}}
																				((((BgL_csequencez00_bglt)
																							COBJECT(BgL_new1152z00_3970))->
																						BgL_copsz00) =
																					((obj_t) BgL_auxz00_5647), BUNSPEC);
																			}
																			BgL_auxz00_5631 =
																				((BgL_copz00_bglt) BgL_new1152z00_3970);
																		}
																		((((BgL_cblockz00_bglt)
																					COBJECT(BgL_new1150z00_3967))->
																				BgL_bodyz00) =
																			((BgL_copz00_bglt) BgL_auxz00_5631),
																			BUNSPEC);
																	}
																	return
																		((BgL_copz00_bglt) BgL_new1150z00_3967);
																}
															else
																{	/* Tools/trace.sch 53 */
																	BgL_cblockz00_bglt BgL_new1163z00_3999;

																	{	/* Tools/trace.sch 53 */
																		BgL_cblockz00_bglt BgL_new1162z00_4000;

																		BgL_new1162z00_4000 =
																			((BgL_cblockz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_cblockz00_bgl))));
																		{	/* Tools/trace.sch 53 */
																			long BgL_arg1945z00_4001;

																			BgL_arg1945z00_4001 =
																				BGL_CLASS_NUM
																				(BGl_cblockz00zzcgen_copz00);
																			BGL_OBJECT_CLASS_NUM_SET((
																					(BgL_objectz00_bglt)
																					BgL_new1162z00_4000),
																				BgL_arg1945z00_4001);
																		}
																		BgL_new1163z00_3999 = BgL_new1162z00_4000;
																	}
																	((((BgL_copz00_bglt) COBJECT(
																					((BgL_copz00_bglt)
																						BgL_new1163z00_3999)))->
																			BgL_locz00) =
																		((obj_t) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_appzd2lyzd2_bglt)
																								BgL_nodez00_3675))))->
																				BgL_locz00)), BUNSPEC);
																	((((BgL_copz00_bglt)
																				COBJECT(((BgL_copz00_bglt)
																						BgL_new1163z00_3999)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt) (
																								(BgL_appzd2lyzd2_bglt)
																								BgL_nodez00_3675))))->
																				BgL_typez00)), BUNSPEC);
																	{
																		BgL_copz00_bglt BgL_auxz00_5744;

																		{	/* Tools/trace.sch 53 */
																			BgL_csequencez00_bglt BgL_new1165z00_4002;

																			{	/* Tools/trace.sch 53 */
																				BgL_csequencez00_bglt
																					BgL_new1164z00_4003;
																				BgL_new1164z00_4003 =
																					((BgL_csequencez00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_csequencez00_bgl))));
																				{	/* Tools/trace.sch 53 */
																					long BgL_arg1944z00_4004;

																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_classz00_4005;

																						BgL_classz00_4005 =
																							BGl_csequencez00zzcgen_copz00;
																						BgL_arg1944z00_4004 =
																							BGL_CLASS_NUM(BgL_classz00_4005);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1164z00_4003),
																						BgL_arg1944z00_4004);
																				}
																				BgL_new1165z00_4002 =
																					BgL_new1164z00_4003;
																			}
																			((((BgL_copz00_bglt) COBJECT(
																							((BgL_copz00_bglt)
																								BgL_new1165z00_4002)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt) (
																										(BgL_appzd2lyzd2_bglt)
																										BgL_nodez00_3675))))->
																						BgL_locz00)), BUNSPEC);
																			((((BgL_copz00_bglt)
																						COBJECT(((BgL_copz00_bglt)
																								BgL_new1165z00_4002)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																							COBJECT(((BgL_nodez00_bglt) (
																										(BgL_appzd2lyzd2_bglt)
																										BgL_nodez00_3675))))->
																						BgL_typez00)), BUNSPEC);
																			((((BgL_csequencez00_bglt)
																						COBJECT(BgL_new1165z00_4002))->
																					BgL_czd2expzf3z21) =
																				((bool_t) ((bool_t) 0)), BUNSPEC);
																			{
																				obj_t BgL_auxz00_5760;

																				{	/* Tools/trace.sch 53 */
																					BgL_localzd2varzd2_bglt
																						BgL_arg1928z00_4006;
																					BgL_csequencez00_bglt
																						BgL_arg1929z00_4007;
																					obj_t BgL_arg1930z00_4008;

																					{	/* Tools/trace.sch 53 */
																						BgL_localzd2varzd2_bglt
																							BgL_new1167z00_4009;
																						{	/* Tools/trace.sch 53 */
																							BgL_localzd2varzd2_bglt
																								BgL_new1166z00_4010;
																							BgL_new1166z00_4010 =
																								((BgL_localzd2varzd2_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_localzd2varzd2_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg1936z00_4011;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_4012;

																									BgL_classz00_4012 =
																										BGl_localzd2varzd2zzcgen_copz00;
																									BgL_arg1936z00_4011 =
																										BGL_CLASS_NUM
																										(BgL_classz00_4012);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1166z00_4010),
																									BgL_arg1936z00_4011);
																							}
																							BgL_new1167z00_4009 =
																								BgL_new1166z00_4010;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1167z00_4009)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_appzd2lyzd2_bglt)
																													BgL_nodez00_3675))))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1167z00_4009)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BGl_za2objza2z00zztype_cachez00)),
																							BUNSPEC);
																						{
																							obj_t BgL_auxz00_5773;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_list1934z00_4013;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1935z00_4014;

																									BgL_arg1935z00_4014 =
																										MAKE_YOUNG_PAIR(
																										((obj_t) BgL_vauxz00_3864),
																										BNIL);
																									BgL_list1934z00_4013 =
																										MAKE_YOUNG_PAIR(((obj_t)
																											BgL_fauxz00_3868),
																										BgL_arg1935z00_4014);
																								}
																								BgL_auxz00_5773 =
																									BgL_list1934z00_4013;
																							}
																							((((BgL_localzd2varzd2_bglt)
																										COBJECT
																										(BgL_new1167z00_4009))->
																									BgL_varsz00) =
																								((obj_t) BgL_auxz00_5773),
																								BUNSPEC);
																						}
																						BgL_arg1928z00_4006 =
																							BgL_new1167z00_4009;
																					}
																					{	/* Tools/trace.sch 53 */
																						BgL_csequencez00_bglt
																							BgL_new1169z00_4015;
																						{	/* Tools/trace.sch 53 */
																							BgL_csequencez00_bglt
																								BgL_new1168z00_4016;
																							BgL_new1168z00_4016 =
																								((BgL_csequencez00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_csequencez00_bgl))));
																							{	/* Tools/trace.sch 53 */
																								long BgL_arg1939z00_4017;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_classz00_4018;

																									BgL_classz00_4018 =
																										BGl_csequencez00zzcgen_copz00;
																									BgL_arg1939z00_4017 =
																										BGL_CLASS_NUM
																										(BgL_classz00_4018);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1168z00_4016),
																									BgL_arg1939z00_4017);
																							}
																							BgL_new1169z00_4015 =
																								BgL_new1168z00_4016;
																						}
																						((((BgL_copz00_bglt) COBJECT(
																										((BgL_copz00_bglt)
																											BgL_new1169z00_4015)))->
																								BgL_locz00) =
																							((obj_t) (((BgL_nodez00_bglt)
																										COBJECT(((BgL_nodez00_bglt)
																												((BgL_appzd2lyzd2_bglt)
																													BgL_nodez00_3675))))->
																									BgL_locz00)), BUNSPEC);
																						((((BgL_copz00_bglt)
																									COBJECT(((BgL_copz00_bglt)
																											BgL_new1169z00_4015)))->
																								BgL_typez00) =
																							((BgL_typez00_bglt) (
																									(BgL_typez00_bglt)
																									BGl_za2objza2z00zztype_cachez00)),
																							BUNSPEC);
																						((((BgL_csequencez00_bglt)
																									COBJECT
																									(BgL_new1169z00_4015))->
																								BgL_czd2expzf3z21) =
																							((bool_t) ((bool_t) 0)), BUNSPEC);
																						{
																							obj_t BgL_auxz00_5792;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_list1937z00_4019;

																								{	/* Tools/trace.sch 53 */
																									obj_t BgL_arg1938z00_4020;

																									BgL_arg1938z00_4020 =
																										MAKE_YOUNG_PAIR(
																										((obj_t) BgL_vcopz00_3865),
																										BNIL);
																									BgL_list1937z00_4019 =
																										MAKE_YOUNG_PAIR(((obj_t)
																											BgL_fcopz00_3869),
																										BgL_arg1938z00_4020);
																								}
																								BgL_auxz00_5792 =
																									BgL_list1937z00_4019;
																							}
																							((((BgL_csequencez00_bglt)
																										COBJECT
																										(BgL_new1169z00_4015))->
																									BgL_copsz00) =
																								((obj_t) BgL_auxz00_5792),
																								BUNSPEC);
																						}
																						BgL_arg1929z00_4007 =
																							BgL_new1169z00_4015;
																					}
																					{	/* Tools/trace.sch 53 */
																						BgL_capplyz00_bglt
																							BgL_arg1940z00_4021;
																						{	/* Tools/trace.sch 53 */
																							BgL_capplyz00_bglt
																								BgL_new1171z00_4022;
																							{	/* Tools/trace.sch 53 */
																								BgL_capplyz00_bglt
																									BgL_new1170z00_4023;
																								BgL_new1170z00_4023 =
																									((BgL_capplyz00_bglt)
																									BOBJECT(GC_MALLOC(sizeof
																											(struct
																												BgL_capplyz00_bgl))));
																								{	/* Tools/trace.sch 53 */
																									long BgL_arg1943z00_4024;

																									{	/* Tools/trace.sch 53 */
																										obj_t BgL_classz00_4025;

																										BgL_classz00_4025 =
																											BGl_capplyz00zzcgen_copz00;
																										BgL_arg1943z00_4024 =
																											BGL_CLASS_NUM
																											(BgL_classz00_4025);
																									}
																									BGL_OBJECT_CLASS_NUM_SET(
																										((BgL_objectz00_bglt)
																											BgL_new1170z00_4023),
																										BgL_arg1943z00_4024);
																								}
																								BgL_new1171z00_4022 =
																									BgL_new1170z00_4023;
																							}
																							((((BgL_copz00_bglt) COBJECT(
																											((BgL_copz00_bglt)
																												BgL_new1171z00_4022)))->
																									BgL_locz00) =
																								((obj_t) (((BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt) (
																														(BgL_appzd2lyzd2_bglt)
																														BgL_nodez00_3675))))->
																										BgL_locz00)), BUNSPEC);
																							((((BgL_copz00_bglt)
																										COBJECT(((BgL_copz00_bglt)
																												BgL_new1171z00_4022)))->
																									BgL_typez00) =
																								((BgL_typez00_bglt) ((
																											(BgL_nodez00_bglt)
																											COBJECT((
																													(BgL_nodez00_bglt) (
																														(BgL_appzd2lyzd2_bglt)
																														BgL_nodez00_3675))))->
																										BgL_typez00)), BUNSPEC);
																							{
																								BgL_copz00_bglt BgL_auxz00_5812;

																								{	/* Tools/trace.sch 53 */
																									BgL_varcz00_bglt
																										BgL_new1173z00_4026;
																									{	/* Tools/trace.sch 53 */
																										BgL_varcz00_bglt
																											BgL_new1172z00_4027;
																										BgL_new1172z00_4027 =
																											((BgL_varcz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_varcz00_bgl))));
																										{	/* Tools/trace.sch 53 */
																											long BgL_arg1941z00_4028;

																											{	/* Tools/trace.sch 53 */
																												obj_t BgL_classz00_4029;

																												BgL_classz00_4029 =
																													BGl_varcz00zzcgen_copz00;
																												BgL_arg1941z00_4028 =
																													BGL_CLASS_NUM
																													(BgL_classz00_4029);
																											}
																											BGL_OBJECT_CLASS_NUM_SET(
																												((BgL_objectz00_bglt)
																													BgL_new1172z00_4027),
																												BgL_arg1941z00_4028);
																										}
																										BgL_new1173z00_4026 =
																											BgL_new1172z00_4027;
																									}
																									((((BgL_copz00_bglt) COBJECT(
																													((BgL_copz00_bglt)
																														BgL_new1173z00_4026)))->
																											BgL_locz00) =
																										((obj_t) ((
																													(BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															((BgL_appzd2lyzd2_bglt) BgL_nodez00_3675))))->BgL_locz00)), BUNSPEC);
																									((((BgL_copz00_bglt)
																												COBJECT((
																														(BgL_copz00_bglt)
																														BgL_new1173z00_4026)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt) ((
																													(BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															BgL_fauxz00_3868)))->
																												BgL_typez00)), BUNSPEC);
																									((((BgL_varcz00_bglt)
																												COBJECT
																												(BgL_new1173z00_4026))->
																											BgL_variablez00) =
																										((BgL_variablez00_bglt) (
																												(BgL_variablez00_bglt)
																												BgL_fauxz00_3868)),
																										BUNSPEC);
																									BgL_auxz00_5812 =
																										((BgL_copz00_bglt)
																										BgL_new1173z00_4026);
																								}
																								((((BgL_capplyz00_bglt)
																											COBJECT
																											(BgL_new1171z00_4022))->
																										BgL_funz00) =
																									((BgL_copz00_bglt)
																										BgL_auxz00_5812), BUNSPEC);
																							}
																							{
																								BgL_copz00_bglt BgL_auxz00_5830;

																								{	/* Tools/trace.sch 53 */
																									BgL_varcz00_bglt
																										BgL_new1175z00_4030;
																									{	/* Tools/trace.sch 53 */
																										BgL_varcz00_bglt
																											BgL_new1174z00_4031;
																										BgL_new1174z00_4031 =
																											((BgL_varcz00_bglt)
																											BOBJECT(GC_MALLOC(sizeof
																													(struct
																														BgL_varcz00_bgl))));
																										{	/* Tools/trace.sch 53 */
																											long BgL_arg1942z00_4032;

																											{	/* Tools/trace.sch 53 */
																												obj_t BgL_classz00_4033;

																												BgL_classz00_4033 =
																													BGl_varcz00zzcgen_copz00;
																												BgL_arg1942z00_4032 =
																													BGL_CLASS_NUM
																													(BgL_classz00_4033);
																											}
																											BGL_OBJECT_CLASS_NUM_SET(
																												((BgL_objectz00_bglt)
																													BgL_new1174z00_4031),
																												BgL_arg1942z00_4032);
																										}
																										BgL_new1175z00_4030 =
																											BgL_new1174z00_4031;
																									}
																									((((BgL_copz00_bglt) COBJECT(
																													((BgL_copz00_bglt)
																														BgL_new1175z00_4030)))->
																											BgL_locz00) =
																										((obj_t) ((
																													(BgL_nodez00_bglt)
																													COBJECT((
																															(BgL_nodez00_bglt)
																															((BgL_appzd2lyzd2_bglt) BgL_nodez00_3675))))->BgL_locz00)), BUNSPEC);
																									((((BgL_copz00_bglt)
																												COBJECT((
																														(BgL_copz00_bglt)
																														BgL_new1175z00_4030)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt) ((
																													(BgL_variablez00_bglt)
																													COBJECT((
																															(BgL_variablez00_bglt)
																															BgL_vauxz00_3864)))->
																												BgL_typez00)), BUNSPEC);
																									((((BgL_varcz00_bglt)
																												COBJECT
																												(BgL_new1175z00_4030))->
																											BgL_variablez00) =
																										((BgL_variablez00_bglt) (
																												(BgL_variablez00_bglt)
																												BgL_vauxz00_3864)),
																										BUNSPEC);
																									BgL_auxz00_5830 =
																										((BgL_copz00_bglt)
																										BgL_new1175z00_4030);
																								}
																								((((BgL_capplyz00_bglt)
																											COBJECT
																											(BgL_new1171z00_4022))->
																										BgL_argz00) =
																									((BgL_copz00_bglt)
																										BgL_auxz00_5830), BUNSPEC);
																							}
																							BgL_arg1940z00_4021 =
																								BgL_new1171z00_4022;
																						}
																						BgL_arg1930z00_4008 =
																							BGL_PROCEDURE_CALL1
																							(BgL_kontz00_3676,
																							((obj_t) BgL_arg1940z00_4021));
																					}
																					{	/* Tools/trace.sch 53 */
																						obj_t BgL_list1931z00_4034;

																						{	/* Tools/trace.sch 53 */
																							obj_t BgL_arg1932z00_4035;

																							{	/* Tools/trace.sch 53 */
																								obj_t BgL_arg1933z00_4036;

																								BgL_arg1933z00_4036 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1930z00_4008, BNIL);
																								BgL_arg1932z00_4035 =
																									MAKE_YOUNG_PAIR(((obj_t)
																										BgL_arg1929z00_4007),
																									BgL_arg1933z00_4036);
																							}
																							BgL_list1931z00_4034 =
																								MAKE_YOUNG_PAIR(
																								((obj_t) BgL_arg1928z00_4006),
																								BgL_arg1932z00_4035);
																						}
																						BgL_auxz00_5760 =
																							BgL_list1931z00_4034;
																				}}
																				((((BgL_csequencez00_bglt)
																							COBJECT(BgL_new1165z00_4002))->
																						BgL_copsz00) =
																					((obj_t) BgL_auxz00_5760), BUNSPEC);
																			}
																			BgL_auxz00_5744 =
																				((BgL_copz00_bglt) BgL_new1165z00_4002);
																		}
																		((((BgL_cblockz00_bglt)
																					COBJECT(BgL_new1163z00_3999))->
																				BgL_bodyz00) =
																			((BgL_copz00_bglt) BgL_auxz00_5744),
																			BUNSPEC);
																	}
																	return
																		((BgL_copz00_bglt) BgL_new1163z00_3999);
																}
														}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcgen_cappz00(void)
	{
		{	/* Cgen/capp.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zztype_toolsz00(453414928L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zzeffect_effectz00(460136018L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(474089076L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			BGl_modulezd2initializa7ationz75zzcgen_copz00(529595144L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
			return
				BGl_modulezd2initializa7ationz75zzcgen_cgenz00(54146247L,
				BSTRING_TO_STRING(BGl_string2160z00zzcgen_cappz00));
		}

	}

#ifdef __cplusplus
}
#endif
