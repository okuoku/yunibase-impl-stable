/*===========================================================================*/
/*   (Tvector/cnst.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tvector/cnst.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TVECTOR_CNST_TYPE_DEFINITIONS
#define BGL_TVECTOR_CNST_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tvecz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}              *BgL_tvecz00_bglt;


#endif													// BGL_TVECTOR_CNST_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t BGl_requirezd2initializa7ationz75zztvector_cnstz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_tvectorzd2Czd2staticzf3zf3zztvector_cnstz00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zztvector_cnstz00(void);
	static obj_t BGl_genericzd2initzd2zztvector_cnstz00(void);
	static obj_t BGl_objectzd2initzd2zztvector_cnstz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	static obj_t BGl_z62tvectorzd2ze3czd2vectorz81zztvector_cnstz00(obj_t, obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern obj_t BGl_za2stringza2z00zztype_cachez00;
	static obj_t BGl_methodzd2initzd2zztvector_cnstz00(void);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	static obj_t BGl_z62zc3z04anonymousza31075ze3ze5zztvector_cnstz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_z62tvectorzd2Czd2staticzf3z91zztvector_cnstz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31077ze3ze5zztvector_cnstz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_tvectorzd2czd2printerz00zztvector_cnstz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zztvector_cnstz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_libraryzd2moduleszd2initz00zztvector_cnstz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zztvector_cnstz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztvector_cnstz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_tvectorzd2ze3czd2vectorze3zztvector_cnstz00(obj_t);
	extern obj_t BGl_za2charza2z00zztype_cachez00;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1098z00zztvector_cnstz00,
		BgL_bgl_string1098za700za7za7t1105za7, "tvector-c-printer", 17);
	      DEFINE_STRING(BGl_string1099z00zztvector_cnstz00,
		BgL_bgl_string1099za700za7za7t1106za7,
		"This tvector can't not be compiled as a static C vector", 55);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1096z00zztvector_cnstz00,
		BgL_bgl_za762za7c3za704anonymo1107za7,
		BGl_z62zc3z04anonymousza31075ze3ze5zztvector_cnstz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1097z00zztvector_cnstz00,
		BgL_bgl_za762za7c3za704anonymo1108za7,
		BGl_z62zc3z04anonymousza31077ze3ze5zztvector_cnstz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tvectorzd2Czd2staticzf3zd2envz21zztvector_cnstz00,
		BgL_bgl_za762tvectorza7d2cza7d1109za7,
		BGl_z62tvectorzd2Czd2staticzf3z91zztvector_cnstz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tvectorzd2ze3czd2vectorzd2envz31zztvector_cnstz00,
		BgL_bgl_za762tvectorza7d2za7e31110za7,
		BGl_z62tvectorzd2ze3czd2vectorz81zztvector_cnstz00, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_displayzd2envzd2zz__r4_output_6_10_3z00;
	BGL_IMPORT obj_t BGl_writezd2envzd2zz__r4_output_6_10_3z00;
	   
		 
		DEFINE_STRING(BGl_string1100z00zztvector_cnstz00,
		BgL_bgl_string1100za700za7za7t1111za7, "(unsigned char)", 15);
	      DEFINE_STRING(BGl_string1101z00zztvector_cnstz00,
		BgL_bgl_string1101za700za7za7t1112za7, "1", 1);
	      DEFINE_STRING(BGl_string1102z00zztvector_cnstz00,
		BgL_bgl_string1102za700za7za7t1113za7, "0", 1);
	      DEFINE_STRING(BGl_string1103z00zztvector_cnstz00,
		BgL_bgl_string1103za700za7za7t1114za7, ", ", 2);
	      DEFINE_STRING(BGl_string1104z00zztvector_cnstz00,
		BgL_bgl_string1104za700za7za7t1115za7, "tvector_cnst", 12);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zztvector_cnstz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zztvector_cnstz00(long
		BgL_checksumz00_244, char *BgL_fromz00_245)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztvector_cnstz00))
				{
					BGl_requirezd2initializa7ationz75zztvector_cnstz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztvector_cnstz00();
					BGl_libraryzd2moduleszd2initz00zztvector_cnstz00();
					BGl_importedzd2moduleszd2initz00zztvector_cnstz00();
					return BGl_toplevelzd2initzd2zztvector_cnstz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztvector_cnstz00(void)
	{
		{	/* Tvector/cnst.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "tvector_cnst");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tvector_cnst");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztvector_cnstz00(void)
	{
		{	/* Tvector/cnst.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztvector_cnstz00(void)
	{
		{	/* Tvector/cnst.scm 15 */
			return BUNSPEC;
		}

	}



/* tvector-C-static? */
	BGL_EXPORTED_DEF bool_t BGl_tvectorzd2Czd2staticzf3zf3zztvector_cnstz00(obj_t
		BgL_tvectz00_13)
	{
		{	/* Tvector/cnst.scm 30 */
			{	/* Tvector/cnst.scm 31 */
				BgL_typez00_bglt BgL_itypez00_155;

				{	/* Tvector/cnst.scm 31 */
					BgL_typez00_bglt BgL_oz00_193;

					BgL_oz00_193 =
						((BgL_typez00_bglt)
						STRUCT_REF(((obj_t) BgL_tvectz00_13), (int) (0L)));
					{
						BgL_tvecz00_bglt BgL_auxz00_268;

						{
							obj_t BgL_auxz00_269;

							{	/* Tvector/cnst.scm 31 */
								BgL_objectz00_bglt BgL_tmpz00_270;

								BgL_tmpz00_270 = ((BgL_objectz00_bglt) BgL_oz00_193);
								BgL_auxz00_269 = BGL_OBJECT_WIDENING(BgL_tmpz00_270);
							}
							BgL_auxz00_268 = ((BgL_tvecz00_bglt) BgL_auxz00_269);
						}
						BgL_itypez00_155 =
							(((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_268))->
							BgL_itemzd2typezd2);
				}}
				if ((((obj_t) BgL_itypez00_155) == BGl_za2longza2z00zztype_cachez00))
					{	/* Tvector/cnst.scm 33 */
						return ((bool_t) 1);
					}
				else
					{	/* Tvector/cnst.scm 33 */
						if ((((obj_t) BgL_itypez00_155) == BGl_za2intza2z00zztype_cachez00))
							{	/* Tvector/cnst.scm 34 */
								return ((bool_t) 1);
							}
						else
							{	/* Tvector/cnst.scm 34 */
								if (
									(((obj_t) BgL_itypez00_155) ==
										BGl_za2charza2z00zztype_cachez00))
									{	/* Tvector/cnst.scm 35 */
										return ((bool_t) 1);
									}
								else
									{	/* Tvector/cnst.scm 35 */
										if (
											(((obj_t) BgL_itypez00_155) ==
												BGl_za2boolza2z00zztype_cachez00))
											{	/* Tvector/cnst.scm 36 */
												return ((bool_t) 1);
											}
										else
											{	/* Tvector/cnst.scm 36 */
												if (
													(((obj_t) BgL_itypez00_155) ==
														BGl_za2stringza2z00zztype_cachez00))
													{	/* Tvector/cnst.scm 37 */
														return ((bool_t) 1);
													}
												else
													{	/* Tvector/cnst.scm 37 */
														return
															(
															((obj_t) BgL_itypez00_155) ==
															BGl_za2realza2z00zztype_cachez00);
													}
											}
									}
							}
					}
			}
		}

	}



/* &tvector-C-static? */
	obj_t BGl_z62tvectorzd2Czd2staticzf3z91zztvector_cnstz00(obj_t BgL_envz00_224,
		obj_t BgL_tvectz00_225)
	{
		{	/* Tvector/cnst.scm 30 */
			return
				BBOOL(BGl_tvectorzd2Czd2staticzf3zf3zztvector_cnstz00
				(BgL_tvectz00_225));
		}

	}



/* tvector-c-printer */
	obj_t BGl_tvectorzd2czd2printerz00zztvector_cnstz00(obj_t BgL_tvectz00_14)
	{
		{	/* Tvector/cnst.scm 44 */
			{	/* Tvector/cnst.scm 45 */
				BgL_typez00_bglt BgL_itypez00_157;

				{	/* Tvector/cnst.scm 45 */
					BgL_typez00_bglt BgL_oz00_196;

					BgL_oz00_196 =
						((BgL_typez00_bglt)
						STRUCT_REF(((obj_t) BgL_tvectz00_14), (int) (0L)));
					{
						BgL_tvecz00_bglt BgL_auxz00_298;

						{
							obj_t BgL_auxz00_299;

							{	/* Tvector/cnst.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_300;

								BgL_tmpz00_300 = ((BgL_objectz00_bglt) BgL_oz00_196);
								BgL_auxz00_299 = BGL_OBJECT_WIDENING(BgL_tmpz00_300);
							}
							BgL_auxz00_298 = ((BgL_tvecz00_bglt) BgL_auxz00_299);
						}
						BgL_itypez00_157 =
							(((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_298))->
							BgL_itemzd2typezd2);
				}}
				if ((((obj_t) BgL_itypez00_157) == BGl_za2longza2z00zztype_cachez00))
					{	/* Tvector/cnst.scm 47 */
						return BGl_displayzd2envzd2zz__r4_output_6_10_3z00;
					}
				else
					{	/* Tvector/cnst.scm 47 */
						if ((((obj_t) BgL_itypez00_157) == BGl_za2intza2z00zztype_cachez00))
							{	/* Tvector/cnst.scm 48 */
								return BGl_displayzd2envzd2zz__r4_output_6_10_3z00;
							}
						else
							{	/* Tvector/cnst.scm 48 */
								if (
									(((obj_t) BgL_itypez00_157) ==
										BGl_za2charza2z00zztype_cachez00))
									{	/* Tvector/cnst.scm 49 */
										return BGl_proc1096z00zztvector_cnstz00;
									}
								else
									{	/* Tvector/cnst.scm 49 */
										if (
											(((obj_t) BgL_itypez00_157) ==
												BGl_za2boolza2z00zztype_cachez00))
											{	/* Tvector/cnst.scm 52 */
												return BGl_proc1097z00zztvector_cnstz00;
											}
										else
											{	/* Tvector/cnst.scm 52 */
												if (
													(((obj_t) BgL_itypez00_157) ==
														BGl_za2stringza2z00zztype_cachez00))
													{	/* Tvector/cnst.scm 56 */
														return BGl_writezd2envzd2zz__r4_output_6_10_3z00;
													}
												else
													{	/* Tvector/cnst.scm 56 */
														if (
															(((obj_t) BgL_itypez00_157) ==
																BGl_za2realza2z00zztype_cachez00))
															{	/* Tvector/cnst.scm 57 */
																return
																	BGl_displayzd2envzd2zz__r4_output_6_10_3z00;
															}
														else
															{	/* Tvector/cnst.scm 57 */
																return
																	BGl_errorz00zz__errorz00
																	(BGl_string1098z00zztvector_cnstz00,
																	BGl_string1099z00zztvector_cnstz00,
																	BgL_tvectz00_14);
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &<@anonymous:1075> */
	obj_t BGl_z62zc3z04anonymousza31075ze3ze5zztvector_cnstz00(obj_t
		BgL_envz00_228, obj_t BgL_xz00_229, obj_t BgL_portz00_230)
	{
		{	/* Tvector/cnst.scm 49 */
			{	/* Tvector/cnst.scm 50 */
				obj_t BgL_tmpz00_324;

				BgL_tmpz00_324 = ((obj_t) BgL_portz00_230);
				bgl_display_string(BGl_string1100z00zztvector_cnstz00, BgL_tmpz00_324);
			}
			{	/* Tvector/cnst.scm 51 */
				long BgL_arg1076z00_243;

				BgL_arg1076z00_243 = (CCHAR(BgL_xz00_229));
				return bgl_display_obj(BINT(BgL_arg1076z00_243), BgL_portz00_230);
			}
		}

	}



/* &<@anonymous:1077> */
	obj_t BGl_z62zc3z04anonymousza31077ze3ze5zztvector_cnstz00(obj_t
		BgL_envz00_231, obj_t BgL_xz00_232, obj_t BgL_portz00_233)
	{
		{	/* Tvector/cnst.scm 52 */
			if (CBOOL(BgL_xz00_232))
				{	/* Tvector/cnst.scm 54 */
					obj_t BgL_tmpz00_333;

					BgL_tmpz00_333 = ((obj_t) BgL_portz00_233);
					return
						bgl_display_string(BGl_string1101z00zztvector_cnstz00,
						BgL_tmpz00_333);
				}
			else
				{	/* Tvector/cnst.scm 55 */
					obj_t BgL_tmpz00_336;

					BgL_tmpz00_336 = ((obj_t) BgL_portz00_233);
					return
						bgl_display_string(BGl_string1102z00zztvector_cnstz00,
						BgL_tmpz00_336);
				}
		}

	}



/* tvector->c-vector */
	BGL_EXPORTED_DEF obj_t BGl_tvectorzd2ze3czd2vectorze3zztvector_cnstz00(obj_t
		BgL_tvectorz00_15)
	{
		{	/* Tvector/cnst.scm 65 */
			{	/* Tvector/cnst.scm 66 */
				obj_t BgL_vectz00_166;

				BgL_vectz00_166 = STRUCT_REF(((obj_t) BgL_tvectorz00_15), (int) (1L));
				{	/* Tvector/cnst.scm 66 */
					obj_t BgL_czd2printerzd2_167;

					BgL_czd2printerzd2_167 =
						BGl_tvectorzd2czd2printerz00zztvector_cnstz00(BgL_tvectorz00_15);
					{	/* Tvector/cnst.scm 67 */
						long BgL_lenzd21zd2_168;

						BgL_lenzd21zd2_168 =
							(VECTOR_LENGTH(((obj_t) BgL_vectz00_166)) - 1L);
						{	/* Tvector/cnst.scm 68 */
							obj_t BgL_portz00_169;

							{	/* Tvector/cnst.scm 69 */

								{	/* Tvector/cnst.scm 69 */

									BgL_portz00_169 =
										BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
							}}
							{	/* Tvector/cnst.scm 69 */

								bgl_display_char(((unsigned char) '{'), BgL_portz00_169);
								{
									long BgL_iz00_171;

									BgL_iz00_171 = 0L;
								BgL_zc3z04anonymousza31079ze3z87_172:
									if ((BgL_iz00_171 == BgL_lenzd21zd2_168))
										{	/* Tvector/cnst.scm 72 */
											{	/* Tvector/cnst.scm 74 */
												obj_t BgL_arg1082z00_174;

												BgL_arg1082z00_174 =
													VECTOR_REF(((obj_t) BgL_vectz00_166), BgL_iz00_171);
												BGL_PROCEDURE_CALL2(BgL_czd2printerzd2_167,
													BgL_arg1082z00_174, BgL_portz00_169);
											}
											bgl_display_char(((unsigned char) '}'), BgL_portz00_169);
											return bgl_close_output_port(BgL_portz00_169);
										}
									else
										{	/* Tvector/cnst.scm 72 */
											{	/* Tvector/cnst.scm 78 */
												obj_t BgL_arg1083z00_175;

												BgL_arg1083z00_175 =
													VECTOR_REF(((obj_t) BgL_vectz00_166), BgL_iz00_171);
												BGL_PROCEDURE_CALL2(BgL_czd2printerzd2_167,
													BgL_arg1083z00_175, BgL_portz00_169);
											}
											bgl_display_string(BGl_string1103z00zztvector_cnstz00,
												BgL_portz00_169);
											{
												long BgL_iz00_367;

												BgL_iz00_367 = (BgL_iz00_171 + 1L);
												BgL_iz00_171 = BgL_iz00_367;
												goto BgL_zc3z04anonymousza31079ze3z87_172;
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &tvector->c-vector */
	obj_t BGl_z62tvectorzd2ze3czd2vectorz81zztvector_cnstz00(obj_t BgL_envz00_240,
		obj_t BgL_tvectorz00_241)
	{
		{	/* Tvector/cnst.scm 65 */
			return
				BGl_tvectorzd2ze3czd2vectorze3zztvector_cnstz00(BgL_tvectorz00_241);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztvector_cnstz00(void)
	{
		{	/* Tvector/cnst.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztvector_cnstz00(void)
	{
		{	/* Tvector/cnst.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztvector_cnstz00(void)
	{
		{	/* Tvector/cnst.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztvector_cnstz00(void)
	{
		{	/* Tvector/cnst.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1104z00zztvector_cnstz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1104z00zztvector_cnstz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1104z00zztvector_cnstz00));
			return
				BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string1104z00zztvector_cnstz00));
		}

	}

#ifdef __cplusplus
}
#endif
