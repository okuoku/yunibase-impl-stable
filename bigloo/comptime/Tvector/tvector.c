/*===========================================================================*/
/*   (Tvector/tvector.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Tvector/tvector.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_TVECTOR_TVECTOR_TYPE_DEFINITIONS
#define BGL_TVECTOR_TVECTOR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tvecz00_bgl
	{
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}              *BgL_tvecz00_bglt;


#endif													// BGL_TVECTOR_TVECTOR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2pointedzd2tozd2byzd2setz12z12zztvector_tvectorz00
		(BgL_typez00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62tveczd2nilzb0zztvector_tvectorz00(obj_t);
	static obj_t BGl_z62emitzd2tvectorzd2typesz62zztvector_tvectorz00(obj_t,
		obj_t);
	static obj_t BGl_z62tveczd2namezb0zztvector_tvectorz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zztvector_tvectorz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2coercezd2tozd2setz12zc0zztvector_tvectorz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2tvectorzd2typez12z12zztvector_tvectorz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_z62tveczd2locationzd2setz12z70zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62tveczd2importzd2locationzd2setz12za2zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern BgL_typez00_bglt BGl_declarezd2subtypez12zc0zztype_envz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zztvector_tvectorz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2siza7ezd2setz12zb5zztvector_tvectorz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2namezd2zztvector_tvectorz00(BgL_typez00_bglt);
	static obj_t BGl_genericzd2initzd2zztvector_tvectorz00(void);
	static obj_t BGl_makezd2coercionzd2clausez00zztvector_tvectorz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2idzd2zztvector_tvectorz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_tveczd2initzf3z21zztvector_tvectorz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zztvector_tvectorz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2importzd2locationzd2setz12zc0zztvector_tvectorz00
		(BgL_typez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2locationzd2zztvector_tvectorz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_tveczd2magiczf3z21zztvector_tvectorz00(BgL_typez00_bglt);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62tveczd2initzf3z43zztvector_tvectorz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_tveczd2nilzd2zztvector_tvectorz00(void);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zztvector_tvectorz00(void);
	static obj_t BGl_z62tveczd2coercezd2toz62zztvector_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62tveczd2tvectorzb0zztvector_tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2parentszd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2tvectorzd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2aliaszd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_z62tveczd2aliaszd2setz12z70zztvector_tvectorz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00 = BUNSPEC;
	static obj_t BGl_z62tveczd2siza7ez17zztvector_tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_emitzd2tvectorzd2typesz00zztvector_tvectorz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2aliaszd2zztvector_tvectorz00(BgL_typez00_bglt);
	static obj_t BGl_z62addzd2tvectorzd2typez12z70zztvector_tvectorz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2parentszd2zztvector_tvectorz00(BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_z62tveczd2itemzd2typez62zztvector_tvectorz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda1200z62zztvector_tvectorz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1201z62zztvector_tvectorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62tveczd2pointedzd2tozd2byzb0zztvector_tvectorz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_tveczf3zf3zztvector_tvectorz00(obj_t);
	static obj_t BGl_z62tveczd2namezd2setz12z70zztvector_tvectorz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62tveczd2magiczf3z43zztvector_tvectorz00(obj_t, obj_t);
	static obj_t
		BGl_z62tveczd2pointedzd2tozd2byzd2setz12z70zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31191ze3ze5zztvector_tvectorz00(obj_t,
		obj_t);
	static obj_t BGl_z62tveczd2aliaszb0zztvector_tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2locationzd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2coercezd2toz00zztvector_tvectorz00(BgL_typez00_bglt);
	static obj_t BGl_z62tveczd2occurrencezd2setz12z70zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2initzf3zd2setz12ze1zztvector_tvectorz00(BgL_typez00_bglt,
		bool_t);
	static obj_t BGl_z62tveczd2initzf3zd2setz12z83zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2siza7ez75zztvector_tvectorz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2classzd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt, obj_t);
	extern obj_t BGl_producezd2modulezd2clausez12z12zzmodule_modulez00(obj_t);
	static obj_t BGl_z62tveczd2classzd2setz12z70zztvector_tvectorz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2occurrencezd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt,
		int);
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	static obj_t BGl_z62tveczf3z91zztvector_tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2magiczf3zd2setz12ze1zztvector_tvectorz00(BgL_typez00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2z42zd2setz12z50zztvector_tvectorz00(BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2z42z90zztvector_tvectorz00(BgL_typez00_bglt);
	static obj_t BGl_z62tveczd2importzd2locationz62zztvector_tvectorz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_declarezd2tvectorzd2typez12z12zztvector_tvectorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_tveczd2itemzd2typez00zztvector_tvectorz00(BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda1184z62zztvector_tvectorz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zztvector_tvectorz00(void);
	static BgL_typez00_bglt BGl_z62lambda1189z62zztvector_tvectorz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2namezd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zztvector_tvectorz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2importzd2locationz00zztvector_tvectorz00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zztvector_tvectorz00(void);
	static obj_t BGl_gczd2rootszd2initz00zztvector_tvectorz00(void);
	static obj_t BGl_z62tveczd2idzb0zztvector_tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_tvecz00zztvector_tvectorz00 = BUNSPEC;
	static BgL_typez00_bglt BGl_z62lambda1192z62zztvector_tvectorz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	static obj_t BGl_z62tveczd2parentszd2setz12z70zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62makezd2tveczb0zztvector_tvectorz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62tveczd2tvectorzd2setz12z70zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2classzd2zztvector_tvectorz00(BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_z62declarezd2tvectorzd2typez12z70zztvector_tvectorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tveczd2magiczf3zd2setz12z83zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62tveczd2coercezd2tozd2setz12za2zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_tveczd2occurrencezd2zztvector_tvectorz00(BgL_typez00_bglt);
	static obj_t BGl_z62tveczd2occurrencezb0zztvector_tvectorz00(obj_t, obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	static obj_t BGl_z62tveczd2classzb0zztvector_tvectorz00(obj_t, obj_t);
	extern BgL_typez00_bglt BGl_usezd2typez12zc0zztype_envz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_makezd2tveczd2zztvector_tvectorz00(obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, int,
		BgL_typez00_bglt);
	static obj_t BGl_z62tveczd2locationzb0zztvector_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62tveczd2z42zd2setz12z32zztvector_tvectorz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_userzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62tveczd2parentszb0zztvector_tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2tvectorzd2zztvector_tvectorz00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_tveczd2pointedzd2tozd2byzd2zztvector_tvectorz00(BgL_typez00_bglt);
	static obj_t BGl_z62tveczd2z42zf2zztvector_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62tveczd2siza7ezd2setz12zd7zztvector_tvectorz00(obj_t,
		obj_t, obj_t);
	static obj_t __cnst[9];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1221z00zztvector_tvectorz00,
		BgL_bgl_za762lambda1200za7621229z00,
		BGl_z62lambda1200z62zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1222z00zztvector_tvectorz00,
		BgL_bgl_za762lambda1192za7621230z00,
		BGl_z62lambda1192z62zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1223z00zztvector_tvectorz00,
		BgL_bgl_za762za7c3za704anonymo1231za7,
		BGl_z62zc3z04anonymousza31191ze3ze5zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1224z00zztvector_tvectorz00,
		BgL_bgl_za762lambda1189za7621232z00,
		BGl_z62lambda1189z62zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1225z00zztvector_tvectorz00,
		BgL_bgl_za762lambda1184za7621233z00,
		BGl_z62lambda1184z62zztvector_tvectorz00, 0L, BUNSPEC, 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2initzf3zd2setz12zd2envz33zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2initza7f1234za7,
		BGl_z62tveczd2initzf3zd2setz12z83zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emitzd2tvectorzd2typeszd2envzd2zztvector_tvectorz00,
		BgL_bgl_za762emitza7d2tvecto1235z00,
		BGl_z62emitzd2tvectorzd2typesz62zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2parentszd2setz12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2parent1236z00,
		BGl_z62tveczd2parentszd2setz12z70zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2magiczf3zd2envzf3zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2magicza71237za7,
		BGl_z62tveczd2magiczf3z43zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_declarezd2tvectorzd2typez12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762declareza7d2tve1238z00,
		BGl_z62declarezd2tvectorzd2typez12z70zztvector_tvectorz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2namezd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2nameza7b1239za7,
		BGl_z62tveczd2namezb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2pointedzd2tozd2byzd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2pointe1240z00,
		BGl_z62tveczd2pointedzd2tozd2byzb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2locationzd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2locati1241z00,
		BGl_z62tveczd2locationzb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2z42zd2setz12zd2envz82zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2za742za7d21242z00,
		BGl_z62tveczd2z42zd2setz12z32zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2itemzd2typezd2envzd2zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2itemza7d1243za7,
		BGl_z62tveczd2itemzd2typez62zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2coercezd2tozd2setz12zd2envz12zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2coerce1244z00,
		BGl_z62tveczd2coercezd2tozd2setz12za2zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2tvectorzd2setz12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2tvecto1245z00,
		BGl_z62tveczd2tvectorzd2setz12z70zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2classzd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2classza71246za7,
		BGl_z62tveczd2classzb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2classzd2setz12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2classza71247za7,
		BGl_z62tveczd2classzd2setz12z70zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2initzf3zd2envzf3zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2initza7f1248za7,
		BGl_z62tveczd2initzf3z43zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2importzd2locationzd2setz12zd2envz12zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2import1249z00,
		BGl_z62tveczd2importzd2locationzd2setz12za2zztvector_tvectorz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2tveczd2envz00zztvector_tvectorz00,
		BgL_bgl_za762makeza7d2tvecza7b1250za7,
		BGl_z62makezd2tveczb0zztvector_tvectorz00, 0L, BUNSPEC, 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2pointedzd2tozd2byzd2setz12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2pointe1251z00,
		BGl_z62tveczd2pointedzd2tozd2byzd2setz12z70zztvector_tvectorz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2coercezd2tozd2envzd2zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2coerce1252z00,
		BGl_z62tveczd2coercezd2toz62zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2nilzd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2nilza7b01253za7,
		BGl_z62tveczd2nilzb0zztvector_tvectorz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2aliaszd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2aliasza71254za7,
		BGl_z62tveczd2aliaszb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczf3zd2envz21zztvector_tvectorz00,
		BgL_bgl_za762tvecza7f3za791za7za7t1255za7,
		BGl_z62tveczf3z91zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2siza7ezd2setz12zd2envz67zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2siza7a7e1256za7,
		BGl_z62tveczd2siza7ezd2setz12zd7zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2tvectorzd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2tvecto1257z00,
		BGl_z62tveczd2tvectorzb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2importzd2locationzd2envzd2zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2import1258z00,
		BGl_z62tveczd2importzd2locationz62zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2idzd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2idza7b0za71259z00,
		BGl_z62tveczd2idzb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2tvectorzd2typez12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762addza7d2tvector1260z00,
		BGl_z62addzd2tvectorzd2typez12z70zztvector_tvectorz00, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_expzd2envzd2zz__r4_numbers_6_5z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2magiczf3zd2setz12zd2envz33zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2magicza71261za7,
		BGl_z62tveczd2magiczf3zd2setz12z83zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2aliaszd2setz12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2aliasza71262za7,
		BGl_z62tveczd2aliaszd2setz12z70zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2locationzd2setz12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2locati1263z00,
		BGl_z62tveczd2locationzd2setz12z70zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2siza7ezd2envza7zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2siza7a7e1264za7,
		BGl_z62tveczd2siza7ez17zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2occurrencezd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2occurr1265z00,
		BGl_z62tveczd2occurrencezb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2occurrencezd2setz12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2occurr1266z00,
		BGl_z62tveczd2occurrencezd2setz12z70zztvector_tvectorz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tveczd2namezd2setz12zd2envzc0zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2nameza7d1267za7,
		BGl_z62tveczd2namezd2setz12z70zztvector_tvectorz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1209z00zztvector_tvectorz00,
		BgL_bgl_string1209za700za7za7t1268za7, "declare-tvector-type!", 21);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2parentszd2envz00zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2parent1269z00,
		BGl_z62tveczd2parentszb0zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1210z00zztvector_tvectorz00,
		BgL_bgl_string1210za700za7za7t1270za7, "Unable to find `obj' type", 25);
	      DEFINE_STRING(BGl_string1211z00zztvector_tvectorz00,
		BgL_bgl_string1211za700za7za7t1271za7, "/* Tvector type definitions */",
		30);
	      DEFINE_STRING(BGl_string1212z00zztvector_tvectorz00,
		BgL_bgl_string1212za700za7za7t1272za7, "struct bgl_tvector_of_", 22);
	      DEFINE_STRING(BGl_string1213z00zztvector_tvectorz00,
		BgL_bgl_string1213za700za7za7t1273za7, " {", 2);
	      DEFINE_STRING(BGl_string1214z00zztvector_tvectorz00,
		BgL_bgl_string1214za700za7za7t1274za7, "   header_t header;", 19);
	      DEFINE_STRING(BGl_string1215z00zztvector_tvectorz00,
		BgL_bgl_string1215za700za7za7t1275za7, "   long     length;", 19);
	      DEFINE_STRING(BGl_string1216z00zztvector_tvectorz00,
		BgL_bgl_string1216za700za7za7t1276za7, "   obj_t    descr;", 18);
	      DEFINE_STRING(BGl_string1217z00zztvector_tvectorz00,
		BgL_bgl_string1217za700za7za7t1277za7, "   ", 3);
	      DEFINE_STRING(BGl_string1218z00zztvector_tvectorz00,
		BgL_bgl_string1218za700za7za7t1278za7, " el0;", 5);
	      DEFINE_STRING(BGl_string1219z00zztvector_tvectorz00,
		BgL_bgl_string1219za700za7za7t1279za7, "};\n", 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_tveczd2z42zd2envz42zztvector_tvectorz00,
		BgL_bgl_za762tvecza7d2za742za7f21280z00,
		BGl_z62tveczd2z42zf2zztvector_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1226z00zztvector_tvectorz00,
		BgL_bgl_string1226za700za7za7t1281za7, "tvector_tvector", 15);
	      DEFINE_STRING(BGl_string1227z00zztvector_tvectorz00,
		BgL_bgl_string1227za700za7za7t1282za7,
		"_ tvector_tvector tvec item-type type coerce tvector bigloo obj ", 64);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1220z00zztvector_tvectorz00,
		BgL_bgl_za762lambda1201za7621283z00,
		BGl_z62lambda1201z62zztvector_tvectorz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zztvector_tvectorz00));
		     ADD_ROOT((void
				*) (&BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00));
		     ADD_ROOT((void *) (&BGl_tvecz00zztvector_tvectorz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long
		BgL_checksumz00_660, char *BgL_fromz00_661)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zztvector_tvectorz00))
				{
					BGl_requirezd2initializa7ationz75zztvector_tvectorz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zztvector_tvectorz00();
					BGl_libraryzd2moduleszd2initz00zztvector_tvectorz00();
					BGl_cnstzd2initzd2zztvector_tvectorz00();
					BGl_importedzd2moduleszd2initz00zztvector_tvectorz00();
					BGl_objectzd2initzd2zztvector_tvectorz00();
					return BGl_toplevelzd2initzd2zztvector_tvectorz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvector.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"tvector_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"tvector_tvector");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvector.scm 15 */
			{	/* Tvector/tvector.scm 15 */
				obj_t BgL_cportz00_601;

				{	/* Tvector/tvector.scm 15 */
					obj_t BgL_stringz00_608;

					BgL_stringz00_608 = BGl_string1227z00zztvector_tvectorz00;
					{	/* Tvector/tvector.scm 15 */
						obj_t BgL_startz00_609;

						BgL_startz00_609 = BINT(0L);
						{	/* Tvector/tvector.scm 15 */
							obj_t BgL_endz00_610;

							BgL_endz00_610 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_608)));
							{	/* Tvector/tvector.scm 15 */

								BgL_cportz00_601 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_608, BgL_startz00_609, BgL_endz00_610);
				}}}}
				{
					long BgL_iz00_602;

					BgL_iz00_602 = 8L;
				BgL_loopz00_603:
					if ((BgL_iz00_602 == -1L))
						{	/* Tvector/tvector.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Tvector/tvector.scm 15 */
							{	/* Tvector/tvector.scm 15 */
								obj_t BgL_arg1228z00_604;

								{	/* Tvector/tvector.scm 15 */

									{	/* Tvector/tvector.scm 15 */
										obj_t BgL_locationz00_606;

										BgL_locationz00_606 = BBOOL(((bool_t) 0));
										{	/* Tvector/tvector.scm 15 */

											BgL_arg1228z00_604 =
												BGl_readz00zz__readerz00(BgL_cportz00_601,
												BgL_locationz00_606);
										}
									}
								}
								{	/* Tvector/tvector.scm 15 */
									int BgL_tmpz00_691;

									BgL_tmpz00_691 = (int) (BgL_iz00_602);
									CNST_TABLE_SET(BgL_tmpz00_691, BgL_arg1228z00_604);
							}}
							{	/* Tvector/tvector.scm 15 */
								int BgL_auxz00_607;

								BgL_auxz00_607 = (int) ((BgL_iz00_602 - 1L));
								{
									long BgL_iz00_696;

									BgL_iz00_696 = (long) (BgL_auxz00_607);
									BgL_iz00_602 = BgL_iz00_696;
									goto BgL_loopz00_603;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvector.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvector.scm 15 */
			return (BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00 =
				BNIL, BUNSPEC);
		}

	}



/* make-tvec */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_makezd2tveczd2zztvector_tvectorz00(obj_t
		BgL_id1071z00_3, obj_t BgL_name1072z00_4, obj_t BgL_siza7e1073za7_5,
		obj_t BgL_class1074z00_6, obj_t BgL_coercezd2to1075zd2_7,
		obj_t BgL_parents1076z00_8, bool_t BgL_initzf31077zf3_9,
		bool_t BgL_magiczf31078zf3_10, obj_t BgL_z421079z42_11,
		obj_t BgL_alias1080z00_12, obj_t BgL_pointedzd2tozd2by1081z00_13,
		obj_t BgL_tvector1082z00_14, obj_t BgL_location1083z00_15,
		obj_t BgL_importzd2location1084zd2_16, int BgL_occurrence1085z00_17,
		BgL_typez00_bglt BgL_itemzd2type1086zd2_18)
	{
		{	/* Tvector/tvectortype.sch 51 */
			{	/* Tvector/tvectortype.sch 51 */
				BgL_typez00_bglt BgL_new1075z00_612;

				{	/* Tvector/tvectortype.sch 51 */
					BgL_typez00_bglt BgL_tmp1073z00_613;
					BgL_tvecz00_bglt BgL_wide1074z00_614;

					{
						BgL_typez00_bglt BgL_auxz00_699;

						{	/* Tvector/tvectortype.sch 51 */
							BgL_typez00_bglt BgL_new1072z00_615;

							BgL_new1072z00_615 =
								((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_typez00_bgl))));
							{	/* Tvector/tvectortype.sch 51 */
								long BgL_arg1114z00_616;

								BgL_arg1114z00_616 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1072z00_615),
									BgL_arg1114z00_616);
							}
							{	/* Tvector/tvectortype.sch 51 */
								BgL_objectz00_bglt BgL_tmpz00_704;

								BgL_tmpz00_704 = ((BgL_objectz00_bglt) BgL_new1072z00_615);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_704, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1072z00_615);
							BgL_auxz00_699 = BgL_new1072z00_615;
						}
						BgL_tmp1073z00_613 = ((BgL_typez00_bglt) BgL_auxz00_699);
					}
					BgL_wide1074z00_614 =
						((BgL_tvecz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_tvecz00_bgl))));
					{	/* Tvector/tvectortype.sch 51 */
						obj_t BgL_auxz00_712;
						BgL_objectz00_bglt BgL_tmpz00_710;

						BgL_auxz00_712 = ((obj_t) BgL_wide1074z00_614);
						BgL_tmpz00_710 = ((BgL_objectz00_bglt) BgL_tmp1073z00_613);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_710, BgL_auxz00_712);
					}
					((BgL_objectz00_bglt) BgL_tmp1073z00_613);
					{	/* Tvector/tvectortype.sch 51 */
						long BgL_arg1104z00_617;

						BgL_arg1104z00_617 = BGL_CLASS_NUM(BGl_tvecz00zztvector_tvectorz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1073z00_613), BgL_arg1104z00_617);
					}
					BgL_new1075z00_612 = ((BgL_typez00_bglt) BgL_tmp1073z00_613);
				}
				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_new1075z00_612)))->BgL_idz00) =
					((obj_t) BgL_id1071z00_3), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_namez00) =
					((obj_t) BgL_name1072z00_4), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_siza7eza7) =
					((obj_t) BgL_siza7e1073za7_5), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_classz00) =
					((obj_t) BgL_class1074z00_6), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_coercezd2tozd2) =
					((obj_t) BgL_coercezd2to1075zd2_7), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_parentsz00) =
					((obj_t) BgL_parents1076z00_8), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_initzf3zf3) =
					((bool_t) BgL_initzf31077zf3_9), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_magiczf3zf3) =
					((bool_t) BgL_magiczf31078zf3_10), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_nullz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_z42z42) =
					((obj_t) BgL_z421079z42_11), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_aliasz00) =
					((obj_t) BgL_alias1080z00_12), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BgL_pointedzd2tozd2by1081z00_13), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_tvectorz00) =
					((obj_t) BgL_tvector1082z00_14), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_locationz00) =
					((obj_t) BgL_location1083z00_15), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_importzd2locationzd2) =
					((obj_t) BgL_importzd2location1084zd2_16), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
									BgL_new1075z00_612)))->BgL_occurrencez00) =
					((int) BgL_occurrence1085z00_17), BUNSPEC);
				{
					BgL_tvecz00_bglt BgL_auxz00_752;

					{
						obj_t BgL_auxz00_753;

						{	/* Tvector/tvectortype.sch 51 */
							BgL_objectz00_bglt BgL_tmpz00_754;

							BgL_tmpz00_754 = ((BgL_objectz00_bglt) BgL_new1075z00_612);
							BgL_auxz00_753 = BGL_OBJECT_WIDENING(BgL_tmpz00_754);
						}
						BgL_auxz00_752 = ((BgL_tvecz00_bglt) BgL_auxz00_753);
					}
					((((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_752))->BgL_itemzd2typezd2) =
						((BgL_typez00_bglt) BgL_itemzd2type1086zd2_18), BUNSPEC);
				}
				return BgL_new1075z00_612;
			}
		}

	}



/* &make-tvec */
	BgL_typez00_bglt BGl_z62makezd2tveczb0zztvector_tvectorz00(obj_t
		BgL_envz00_462, obj_t BgL_id1071z00_463, obj_t BgL_name1072z00_464,
		obj_t BgL_siza7e1073za7_465, obj_t BgL_class1074z00_466,
		obj_t BgL_coercezd2to1075zd2_467, obj_t BgL_parents1076z00_468,
		obj_t BgL_initzf31077zf3_469, obj_t BgL_magiczf31078zf3_470,
		obj_t BgL_z421079z42_471, obj_t BgL_alias1080z00_472,
		obj_t BgL_pointedzd2tozd2by1081z00_473, obj_t BgL_tvector1082z00_474,
		obj_t BgL_location1083z00_475, obj_t BgL_importzd2location1084zd2_476,
		obj_t BgL_occurrence1085z00_477, obj_t BgL_itemzd2type1086zd2_478)
	{
		{	/* Tvector/tvectortype.sch 51 */
			return
				BGl_makezd2tveczd2zztvector_tvectorz00(BgL_id1071z00_463,
				BgL_name1072z00_464, BgL_siza7e1073za7_465, BgL_class1074z00_466,
				BgL_coercezd2to1075zd2_467, BgL_parents1076z00_468,
				CBOOL(BgL_initzf31077zf3_469), CBOOL(BgL_magiczf31078zf3_470),
				BgL_z421079z42_471, BgL_alias1080z00_472,
				BgL_pointedzd2tozd2by1081z00_473, BgL_tvector1082z00_474,
				BgL_location1083z00_475, BgL_importzd2location1084zd2_476,
				CINT(BgL_occurrence1085z00_477),
				((BgL_typez00_bglt) BgL_itemzd2type1086zd2_478));
		}

	}



/* tvec? */
	BGL_EXPORTED_DEF bool_t BGl_tveczf3zf3zztvector_tvectorz00(obj_t
		BgL_objz00_19)
	{
		{	/* Tvector/tvectortype.sch 52 */
			{	/* Tvector/tvectortype.sch 52 */
				obj_t BgL_classz00_618;

				BgL_classz00_618 = BGl_tvecz00zztvector_tvectorz00;
				if (BGL_OBJECTP(BgL_objz00_19))
					{	/* Tvector/tvectortype.sch 52 */
						BgL_objectz00_bglt BgL_arg1807z00_619;

						BgL_arg1807z00_619 = (BgL_objectz00_bglt) (BgL_objz00_19);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Tvector/tvectortype.sch 52 */
								long BgL_idxz00_620;

								BgL_idxz00_620 = BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_619);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_620 + 2L)) == BgL_classz00_618);
							}
						else
							{	/* Tvector/tvectortype.sch 52 */
								bool_t BgL_res1204z00_623;

								{	/* Tvector/tvectortype.sch 52 */
									obj_t BgL_oclassz00_624;

									{	/* Tvector/tvectortype.sch 52 */
										obj_t BgL_arg1815z00_625;
										long BgL_arg1816z00_626;

										BgL_arg1815z00_625 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Tvector/tvectortype.sch 52 */
											long BgL_arg1817z00_627;

											BgL_arg1817z00_627 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_619);
											BgL_arg1816z00_626 = (BgL_arg1817z00_627 - OBJECT_TYPE);
										}
										BgL_oclassz00_624 =
											VECTOR_REF(BgL_arg1815z00_625, BgL_arg1816z00_626);
									}
									{	/* Tvector/tvectortype.sch 52 */
										bool_t BgL__ortest_1115z00_628;

										BgL__ortest_1115z00_628 =
											(BgL_classz00_618 == BgL_oclassz00_624);
										if (BgL__ortest_1115z00_628)
											{	/* Tvector/tvectortype.sch 52 */
												BgL_res1204z00_623 = BgL__ortest_1115z00_628;
											}
										else
											{	/* Tvector/tvectortype.sch 52 */
												long BgL_odepthz00_629;

												{	/* Tvector/tvectortype.sch 52 */
													obj_t BgL_arg1804z00_630;

													BgL_arg1804z00_630 = (BgL_oclassz00_624);
													BgL_odepthz00_629 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_630);
												}
												if ((2L < BgL_odepthz00_629))
													{	/* Tvector/tvectortype.sch 52 */
														obj_t BgL_arg1802z00_631;

														{	/* Tvector/tvectortype.sch 52 */
															obj_t BgL_arg1803z00_632;

															BgL_arg1803z00_632 = (BgL_oclassz00_624);
															BgL_arg1802z00_631 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_632, 2L);
														}
														BgL_res1204z00_623 =
															(BgL_arg1802z00_631 == BgL_classz00_618);
													}
												else
													{	/* Tvector/tvectortype.sch 52 */
														BgL_res1204z00_623 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1204z00_623;
							}
					}
				else
					{	/* Tvector/tvectortype.sch 52 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &tvec? */
	obj_t BGl_z62tveczf3z91zztvector_tvectorz00(obj_t BgL_envz00_479,
		obj_t BgL_objz00_480)
	{
		{	/* Tvector/tvectortype.sch 52 */
			return BBOOL(BGl_tveczf3zf3zztvector_tvectorz00(BgL_objz00_480));
		}

	}



/* tvec-nil */
	BGL_EXPORTED_DEF BgL_typez00_bglt BGl_tveczd2nilzd2zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvectortype.sch 53 */
			{	/* Tvector/tvectortype.sch 53 */
				obj_t BgL_classz00_316;

				BgL_classz00_316 = BGl_tvecz00zztvector_tvectorz00;
				{	/* Tvector/tvectortype.sch 53 */
					obj_t BgL__ortest_1117z00_317;

					BgL__ortest_1117z00_317 = BGL_CLASS_NIL(BgL_classz00_316);
					if (CBOOL(BgL__ortest_1117z00_317))
						{	/* Tvector/tvectortype.sch 53 */
							return ((BgL_typez00_bglt) BgL__ortest_1117z00_317);
						}
					else
						{	/* Tvector/tvectortype.sch 53 */
							return
								((BgL_typez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_316));
						}
				}
			}
		}

	}



/* &tvec-nil */
	BgL_typez00_bglt BGl_z62tveczd2nilzb0zztvector_tvectorz00(obj_t
		BgL_envz00_481)
	{
		{	/* Tvector/tvectortype.sch 53 */
			return BGl_tveczd2nilzd2zztvector_tvectorz00();
		}

	}



/* tvec-item-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_tveczd2itemzd2typez00zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_20)
	{
		{	/* Tvector/tvectortype.sch 54 */
			{
				BgL_tvecz00_bglt BgL_auxz00_795;

				{
					obj_t BgL_auxz00_796;

					{	/* Tvector/tvectortype.sch 54 */
						BgL_objectz00_bglt BgL_tmpz00_797;

						BgL_tmpz00_797 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_796 = BGL_OBJECT_WIDENING(BgL_tmpz00_797);
					}
					BgL_auxz00_795 = ((BgL_tvecz00_bglt) BgL_auxz00_796);
				}
				return
					(((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_795))->BgL_itemzd2typezd2);
			}
		}

	}



/* &tvec-item-type */
	BgL_typez00_bglt BGl_z62tveczd2itemzd2typez62zztvector_tvectorz00(obj_t
		BgL_envz00_482, obj_t BgL_oz00_483)
	{
		{	/* Tvector/tvectortype.sch 54 */
			return
				BGl_tveczd2itemzd2typez00zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_483));
		}

	}



/* tvec-occurrence */
	BGL_EXPORTED_DEF int
		BGl_tveczd2occurrencezd2zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_23)
	{
		{	/* Tvector/tvectortype.sch 56 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_23)))->BgL_occurrencez00);
		}

	}



/* &tvec-occurrence */
	obj_t BGl_z62tveczd2occurrencezb0zztvector_tvectorz00(obj_t BgL_envz00_484,
		obj_t BgL_oz00_485)
	{
		{	/* Tvector/tvectortype.sch 56 */
			return
				BINT(BGl_tveczd2occurrencezd2zztvector_tvectorz00(
					((BgL_typez00_bglt) BgL_oz00_485)));
		}

	}



/* tvec-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2occurrencezd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_24, int BgL_vz00_25)
	{
		{	/* Tvector/tvectortype.sch 57 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_24)))->BgL_occurrencez00) =
				((int) BgL_vz00_25), BUNSPEC);
		}

	}



/* &tvec-occurrence-set! */
	obj_t BGl_z62tveczd2occurrencezd2setz12z70zztvector_tvectorz00(obj_t
		BgL_envz00_486, obj_t BgL_oz00_487, obj_t BgL_vz00_488)
	{
		{	/* Tvector/tvectortype.sch 57 */
			return
				BGl_tveczd2occurrencezd2setz12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_487), CINT(BgL_vz00_488));
		}

	}



/* tvec-import-location */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2importzd2locationz00zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_26)
	{
		{	/* Tvector/tvectortype.sch 58 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_26)))->BgL_importzd2locationzd2);
		}

	}



/* &tvec-import-location */
	obj_t BGl_z62tveczd2importzd2locationz62zztvector_tvectorz00(obj_t
		BgL_envz00_489, obj_t BgL_oz00_490)
	{
		{	/* Tvector/tvectortype.sch 58 */
			return
				BGl_tveczd2importzd2locationz00zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_490));
		}

	}



/* tvec-import-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2importzd2locationzd2setz12zc0zztvector_tvectorz00
		(BgL_typez00_bglt BgL_oz00_27, obj_t BgL_vz00_28)
	{
		{	/* Tvector/tvectortype.sch 59 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_27)))->BgL_importzd2locationzd2) =
				((obj_t) BgL_vz00_28), BUNSPEC);
		}

	}



/* &tvec-import-location-set! */
	obj_t BGl_z62tveczd2importzd2locationzd2setz12za2zztvector_tvectorz00(obj_t
		BgL_envz00_491, obj_t BgL_oz00_492, obj_t BgL_vz00_493)
	{
		{	/* Tvector/tvectortype.sch 59 */
			return
				BGl_tveczd2importzd2locationzd2setz12zc0zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_492), BgL_vz00_493);
		}

	}



/* tvec-location */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2locationzd2zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_29)
	{
		{	/* Tvector/tvectortype.sch 60 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_29)))->BgL_locationz00);
		}

	}



/* &tvec-location */
	obj_t BGl_z62tveczd2locationzb0zztvector_tvectorz00(obj_t BgL_envz00_494,
		obj_t BgL_oz00_495)
	{
		{	/* Tvector/tvectortype.sch 60 */
			return
				BGl_tveczd2locationzd2zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_495));
		}

	}



/* tvec-location-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2locationzd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_30, obj_t BgL_vz00_31)
	{
		{	/* Tvector/tvectortype.sch 61 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_30)))->BgL_locationz00) =
				((obj_t) BgL_vz00_31), BUNSPEC);
		}

	}



/* &tvec-location-set! */
	obj_t BGl_z62tveczd2locationzd2setz12z70zztvector_tvectorz00(obj_t
		BgL_envz00_496, obj_t BgL_oz00_497, obj_t BgL_vz00_498)
	{
		{	/* Tvector/tvectortype.sch 61 */
			return
				BGl_tveczd2locationzd2setz12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_497), BgL_vz00_498);
		}

	}



/* tvec-tvector */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2tvectorzd2zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_32)
	{
		{	/* Tvector/tvectortype.sch 62 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_32)))->BgL_tvectorz00);
		}

	}



/* &tvec-tvector */
	obj_t BGl_z62tveczd2tvectorzb0zztvector_tvectorz00(obj_t BgL_envz00_499,
		obj_t BgL_oz00_500)
	{
		{	/* Tvector/tvectortype.sch 62 */
			return
				BGl_tveczd2tvectorzd2zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_500));
		}

	}



/* tvec-tvector-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2tvectorzd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_33, obj_t BgL_vz00_34)
	{
		{	/* Tvector/tvectortype.sch 63 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_33)))->BgL_tvectorz00) =
				((obj_t) BgL_vz00_34), BUNSPEC);
		}

	}



/* &tvec-tvector-set! */
	obj_t BGl_z62tveczd2tvectorzd2setz12z70zztvector_tvectorz00(obj_t
		BgL_envz00_501, obj_t BgL_oz00_502, obj_t BgL_vz00_503)
	{
		{	/* Tvector/tvectortype.sch 63 */
			return
				BGl_tveczd2tvectorzd2setz12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_502), BgL_vz00_503);
		}

	}



/* tvec-pointed-to-by */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2pointedzd2tozd2byzd2zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_35)
	{
		{	/* Tvector/tvectortype.sch 64 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_35)))->BgL_pointedzd2tozd2byz00);
		}

	}



/* &tvec-pointed-to-by */
	obj_t BGl_z62tveczd2pointedzd2tozd2byzb0zztvector_tvectorz00(obj_t
		BgL_envz00_504, obj_t BgL_oz00_505)
	{
		{	/* Tvector/tvectortype.sch 64 */
			return
				BGl_tveczd2pointedzd2tozd2byzd2zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_505));
		}

	}



/* tvec-pointed-to-by-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2pointedzd2tozd2byzd2setz12z12zztvector_tvectorz00
		(BgL_typez00_bglt BgL_oz00_36, obj_t BgL_vz00_37)
	{
		{	/* Tvector/tvectortype.sch 65 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_36)))->BgL_pointedzd2tozd2byz00) =
				((obj_t) BgL_vz00_37), BUNSPEC);
		}

	}



/* &tvec-pointed-to-by-set! */
	obj_t BGl_z62tveczd2pointedzd2tozd2byzd2setz12z70zztvector_tvectorz00(obj_t
		BgL_envz00_506, obj_t BgL_oz00_507, obj_t BgL_vz00_508)
	{
		{	/* Tvector/tvectortype.sch 65 */
			return
				BGl_tveczd2pointedzd2tozd2byzd2setz12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_507), BgL_vz00_508);
		}

	}



/* tvec-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2aliaszd2zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_38)
	{
		{	/* Tvector/tvectortype.sch 66 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_38)))->BgL_aliasz00);
		}

	}



/* &tvec-alias */
	obj_t BGl_z62tveczd2aliaszb0zztvector_tvectorz00(obj_t BgL_envz00_509,
		obj_t BgL_oz00_510)
	{
		{	/* Tvector/tvectortype.sch 66 */
			return
				BGl_tveczd2aliaszd2zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_510));
		}

	}



/* tvec-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2aliaszd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_39, obj_t BgL_vz00_40)
	{
		{	/* Tvector/tvectortype.sch 67 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_39)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_40), BUNSPEC);
		}

	}



/* &tvec-alias-set! */
	obj_t BGl_z62tveczd2aliaszd2setz12z70zztvector_tvectorz00(obj_t
		BgL_envz00_511, obj_t BgL_oz00_512, obj_t BgL_vz00_513)
	{
		{	/* Tvector/tvectortype.sch 67 */
			return
				BGl_tveczd2aliaszd2setz12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_512), BgL_vz00_513);
		}

	}



/* tvec-$ */
	BGL_EXPORTED_DEF obj_t BGl_tveczd2z42z90zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_41)
	{
		{	/* Tvector/tvectortype.sch 68 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_41)))->BgL_z42z42);
		}

	}



/* &tvec-$ */
	obj_t BGl_z62tveczd2z42zf2zztvector_tvectorz00(obj_t BgL_envz00_514,
		obj_t BgL_oz00_515)
	{
		{	/* Tvector/tvectortype.sch 68 */
			return
				BGl_tveczd2z42z90zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_515));
		}

	}



/* tvec-$-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2z42zd2setz12z50zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_42,
		obj_t BgL_vz00_43)
	{
		{	/* Tvector/tvectortype.sch 69 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_42)))->BgL_z42z42) =
				((obj_t) BgL_vz00_43), BUNSPEC);
		}

	}



/* &tvec-$-set! */
	obj_t BGl_z62tveczd2z42zd2setz12z32zztvector_tvectorz00(obj_t BgL_envz00_516,
		obj_t BgL_oz00_517, obj_t BgL_vz00_518)
	{
		{	/* Tvector/tvectortype.sch 69 */
			return
				BGl_tveczd2z42zd2setz12z50zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_517), BgL_vz00_518);
		}

	}



/* tvec-magic? */
	BGL_EXPORTED_DEF bool_t
		BGl_tveczd2magiczf3z21zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_44)
	{
		{	/* Tvector/tvectortype.sch 70 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_44)))->BgL_magiczf3zf3);
		}

	}



/* &tvec-magic? */
	obj_t BGl_z62tveczd2magiczf3z43zztvector_tvectorz00(obj_t BgL_envz00_519,
		obj_t BgL_oz00_520)
	{
		{	/* Tvector/tvectortype.sch 70 */
			return
				BBOOL(BGl_tveczd2magiczf3z21zztvector_tvectorz00(
					((BgL_typez00_bglt) BgL_oz00_520)));
		}

	}



/* tvec-magic?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2magiczf3zd2setz12ze1zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_45, bool_t BgL_vz00_46)
	{
		{	/* Tvector/tvectortype.sch 71 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_45)))->BgL_magiczf3zf3) =
				((bool_t) BgL_vz00_46), BUNSPEC);
		}

	}



/* &tvec-magic?-set! */
	obj_t BGl_z62tveczd2magiczf3zd2setz12z83zztvector_tvectorz00(obj_t
		BgL_envz00_521, obj_t BgL_oz00_522, obj_t BgL_vz00_523)
	{
		{	/* Tvector/tvectortype.sch 71 */
			return
				BGl_tveczd2magiczf3zd2setz12ze1zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_522), CBOOL(BgL_vz00_523));
		}

	}



/* tvec-init? */
	BGL_EXPORTED_DEF bool_t
		BGl_tveczd2initzf3z21zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_47)
	{
		{	/* Tvector/tvectortype.sch 72 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_47)))->BgL_initzf3zf3);
		}

	}



/* &tvec-init? */
	obj_t BGl_z62tveczd2initzf3z43zztvector_tvectorz00(obj_t BgL_envz00_524,
		obj_t BgL_oz00_525)
	{
		{	/* Tvector/tvectortype.sch 72 */
			return
				BBOOL(BGl_tveczd2initzf3z21zztvector_tvectorz00(
					((BgL_typez00_bglt) BgL_oz00_525)));
		}

	}



/* tvec-init?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2initzf3zd2setz12ze1zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_48, bool_t BgL_vz00_49)
	{
		{	/* Tvector/tvectortype.sch 73 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_48)))->BgL_initzf3zf3) =
				((bool_t) BgL_vz00_49), BUNSPEC);
		}

	}



/* &tvec-init?-set! */
	obj_t BGl_z62tveczd2initzf3zd2setz12z83zztvector_tvectorz00(obj_t
		BgL_envz00_526, obj_t BgL_oz00_527, obj_t BgL_vz00_528)
	{
		{	/* Tvector/tvectortype.sch 73 */
			return
				BGl_tveczd2initzf3zd2setz12ze1zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_527), CBOOL(BgL_vz00_528));
		}

	}



/* tvec-parents */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2parentszd2zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_50)
	{
		{	/* Tvector/tvectortype.sch 74 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_50)))->BgL_parentsz00);
		}

	}



/* &tvec-parents */
	obj_t BGl_z62tveczd2parentszb0zztvector_tvectorz00(obj_t BgL_envz00_529,
		obj_t BgL_oz00_530)
	{
		{	/* Tvector/tvectortype.sch 74 */
			return
				BGl_tveczd2parentszd2zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_530));
		}

	}



/* tvec-parents-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2parentszd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_51, obj_t BgL_vz00_52)
	{
		{	/* Tvector/tvectortype.sch 75 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_51)))->BgL_parentsz00) =
				((obj_t) BgL_vz00_52), BUNSPEC);
		}

	}



/* &tvec-parents-set! */
	obj_t BGl_z62tveczd2parentszd2setz12z70zztvector_tvectorz00(obj_t
		BgL_envz00_531, obj_t BgL_oz00_532, obj_t BgL_vz00_533)
	{
		{	/* Tvector/tvectortype.sch 75 */
			return
				BGl_tveczd2parentszd2setz12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_532), BgL_vz00_533);
		}

	}



/* tvec-coerce-to */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2coercezd2toz00zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_53)
	{
		{	/* Tvector/tvectortype.sch 76 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_53)))->BgL_coercezd2tozd2);
		}

	}



/* &tvec-coerce-to */
	obj_t BGl_z62tveczd2coercezd2toz62zztvector_tvectorz00(obj_t BgL_envz00_534,
		obj_t BgL_oz00_535)
	{
		{	/* Tvector/tvectortype.sch 76 */
			return
				BGl_tveczd2coercezd2toz00zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_535));
		}

	}



/* tvec-coerce-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2coercezd2tozd2setz12zc0zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_54, obj_t BgL_vz00_55)
	{
		{	/* Tvector/tvectortype.sch 77 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_54)))->BgL_coercezd2tozd2) =
				((obj_t) BgL_vz00_55), BUNSPEC);
		}

	}



/* &tvec-coerce-to-set! */
	obj_t BGl_z62tveczd2coercezd2tozd2setz12za2zztvector_tvectorz00(obj_t
		BgL_envz00_536, obj_t BgL_oz00_537, obj_t BgL_vz00_538)
	{
		{	/* Tvector/tvectortype.sch 77 */
			return
				BGl_tveczd2coercezd2tozd2setz12zc0zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_537), BgL_vz00_538);
		}

	}



/* tvec-class */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2classzd2zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_56)
	{
		{	/* Tvector/tvectortype.sch 78 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_56)))->BgL_classz00);
		}

	}



/* &tvec-class */
	obj_t BGl_z62tveczd2classzb0zztvector_tvectorz00(obj_t BgL_envz00_539,
		obj_t BgL_oz00_540)
	{
		{	/* Tvector/tvectortype.sch 78 */
			return
				BGl_tveczd2classzd2zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_540));
		}

	}



/* tvec-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2classzd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_57, obj_t BgL_vz00_58)
	{
		{	/* Tvector/tvectortype.sch 79 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_57)))->BgL_classz00) =
				((obj_t) BgL_vz00_58), BUNSPEC);
		}

	}



/* &tvec-class-set! */
	obj_t BGl_z62tveczd2classzd2setz12z70zztvector_tvectorz00(obj_t
		BgL_envz00_541, obj_t BgL_oz00_542, obj_t BgL_vz00_543)
	{
		{	/* Tvector/tvectortype.sch 79 */
			return
				BGl_tveczd2classzd2setz12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_542), BgL_vz00_543);
		}

	}



/* tvec-size */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2siza7ez75zztvector_tvectorz00(BgL_typez00_bglt BgL_oz00_59)
	{
		{	/* Tvector/tvectortype.sch 80 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_59)))->BgL_siza7eza7);
		}

	}



/* &tvec-size */
	obj_t BGl_z62tveczd2siza7ez17zztvector_tvectorz00(obj_t BgL_envz00_544,
		obj_t BgL_oz00_545)
	{
		{	/* Tvector/tvectortype.sch 80 */
			return
				BGl_tveczd2siza7ez75zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_545));
		}

	}



/* tvec-size-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2siza7ezd2setz12zb5zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_60, obj_t BgL_vz00_61)
	{
		{	/* Tvector/tvectortype.sch 81 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_60)))->BgL_siza7eza7) =
				((obj_t) BgL_vz00_61), BUNSPEC);
		}

	}



/* &tvec-size-set! */
	obj_t BGl_z62tveczd2siza7ezd2setz12zd7zztvector_tvectorz00(obj_t
		BgL_envz00_546, obj_t BgL_oz00_547, obj_t BgL_vz00_548)
	{
		{	/* Tvector/tvectortype.sch 81 */
			return
				BGl_tveczd2siza7ezd2setz12zb5zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_547), BgL_vz00_548);
		}

	}



/* tvec-name */
	BGL_EXPORTED_DEF obj_t BGl_tveczd2namezd2zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_62)
	{
		{	/* Tvector/tvectortype.sch 82 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_62)))->BgL_namez00);
		}

	}



/* &tvec-name */
	obj_t BGl_z62tveczd2namezb0zztvector_tvectorz00(obj_t BgL_envz00_549,
		obj_t BgL_oz00_550)
	{
		{	/* Tvector/tvectortype.sch 82 */
			return
				BGl_tveczd2namezd2zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_550));
		}

	}



/* tvec-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_tveczd2namezd2setz12z12zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_63, obj_t BgL_vz00_64)
	{
		{	/* Tvector/tvectortype.sch 83 */
			return
				((((BgL_typez00_bglt) COBJECT(
							((BgL_typez00_bglt) BgL_oz00_63)))->BgL_namez00) =
				((obj_t) BgL_vz00_64), BUNSPEC);
		}

	}



/* &tvec-name-set! */
	obj_t BGl_z62tveczd2namezd2setz12z70zztvector_tvectorz00(obj_t BgL_envz00_551,
		obj_t BgL_oz00_552, obj_t BgL_vz00_553)
	{
		{	/* Tvector/tvectortype.sch 83 */
			return
				BGl_tveczd2namezd2setz12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_oz00_552), BgL_vz00_553);
		}

	}



/* tvec-id */
	BGL_EXPORTED_DEF obj_t BGl_tveczd2idzd2zztvector_tvectorz00(BgL_typez00_bglt
		BgL_oz00_65)
	{
		{	/* Tvector/tvectortype.sch 84 */
			return
				(((BgL_typez00_bglt) COBJECT(
						((BgL_typez00_bglt) BgL_oz00_65)))->BgL_idz00);
		}

	}



/* &tvec-id */
	obj_t BGl_z62tveczd2idzb0zztvector_tvectorz00(obj_t BgL_envz00_554,
		obj_t BgL_oz00_555)
	{
		{	/* Tvector/tvectortype.sch 84 */
			return
				BGl_tveczd2idzd2zztvector_tvectorz00(((BgL_typez00_bglt) BgL_oz00_555));
		}

	}



/* add-tvector-type! */
	BGL_EXPORTED_DEF obj_t
		BGl_addzd2tvectorzd2typez12z12zztvector_tvectorz00(BgL_typez00_bglt
		BgL_typez00_68)
	{
		{	/* Tvector/tvector.scm 41 */
			return (BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00 =
				MAKE_YOUNG_PAIR(
					((obj_t) BgL_typez00_68),
					BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00), BUNSPEC);
		}

	}



/* &add-tvector-type! */
	obj_t BGl_z62addzd2tvectorzd2typez12z70zztvector_tvectorz00(obj_t
		BgL_envz00_556, obj_t BgL_typez00_557)
	{
		{	/* Tvector/tvector.scm 41 */
			return
				BGl_addzd2tvectorzd2typez12z12zztvector_tvectorz00(
				((BgL_typez00_bglt) BgL_typez00_557));
		}

	}



/* declare-tvector-type! */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_declarezd2tvectorzd2typez12z12zztvector_tvectorz00(obj_t
		BgL_tvectzd2idzd2_69, obj_t BgL_itemzd2idzd2_70, obj_t BgL_srcz00_71)
	{
		{	/* Tvector/tvector.scm 47 */
			{	/* Tvector/tvector.scm 48 */
				BgL_typez00_bglt BgL_objz00_156;

				BgL_objz00_156 = BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(0));
				{	/* Tvector/tvector.scm 49 */
					bool_t BgL_test1291z00_932;

					{	/* Tvector/tvector.scm 49 */
						obj_t BgL_classz00_319;

						BgL_classz00_319 = BGl_typez00zztype_typez00;
						{	/* Tvector/tvector.scm 49 */
							BgL_objectz00_bglt BgL_arg1807z00_321;

							{	/* Tvector/tvector.scm 49 */
								obj_t BgL_tmpz00_933;

								BgL_tmpz00_933 =
									((obj_t) ((BgL_objectz00_bglt) BgL_objz00_156));
								BgL_arg1807z00_321 = (BgL_objectz00_bglt) (BgL_tmpz00_933);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Tvector/tvector.scm 49 */
									long BgL_idxz00_327;

									BgL_idxz00_327 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_321);
									BgL_test1291z00_932 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_327 + 1L)) == BgL_classz00_319);
								}
							else
								{	/* Tvector/tvector.scm 49 */
									bool_t BgL_res1205z00_352;

									{	/* Tvector/tvector.scm 49 */
										obj_t BgL_oclassz00_335;

										{	/* Tvector/tvector.scm 49 */
											obj_t BgL_arg1815z00_343;
											long BgL_arg1816z00_344;

											BgL_arg1815z00_343 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Tvector/tvector.scm 49 */
												long BgL_arg1817z00_345;

												BgL_arg1817z00_345 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_321);
												BgL_arg1816z00_344 = (BgL_arg1817z00_345 - OBJECT_TYPE);
											}
											BgL_oclassz00_335 =
												VECTOR_REF(BgL_arg1815z00_343, BgL_arg1816z00_344);
										}
										{	/* Tvector/tvector.scm 49 */
											bool_t BgL__ortest_1115z00_336;

											BgL__ortest_1115z00_336 =
												(BgL_classz00_319 == BgL_oclassz00_335);
											if (BgL__ortest_1115z00_336)
												{	/* Tvector/tvector.scm 49 */
													BgL_res1205z00_352 = BgL__ortest_1115z00_336;
												}
											else
												{	/* Tvector/tvector.scm 49 */
													long BgL_odepthz00_337;

													{	/* Tvector/tvector.scm 49 */
														obj_t BgL_arg1804z00_338;

														BgL_arg1804z00_338 = (BgL_oclassz00_335);
														BgL_odepthz00_337 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_338);
													}
													if ((1L < BgL_odepthz00_337))
														{	/* Tvector/tvector.scm 49 */
															obj_t BgL_arg1802z00_340;

															{	/* Tvector/tvector.scm 49 */
																obj_t BgL_arg1803z00_341;

																BgL_arg1803z00_341 = (BgL_oclassz00_335);
																BgL_arg1802z00_340 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_341,
																	1L);
															}
															BgL_res1205z00_352 =
																(BgL_arg1802z00_340 == BgL_classz00_319);
														}
													else
														{	/* Tvector/tvector.scm 49 */
															BgL_res1205z00_352 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1291z00_932 = BgL_res1205z00_352;
								}
						}
					}
					if (BgL_test1291z00_932)
						{	/* Tvector/tvector.scm 51 */
							BgL_typez00_bglt BgL_typez00_158;
							BgL_typez00_bglt BgL_itemzd2typezd2_159;

							{	/* Tvector/tvector.scm 52 */
								obj_t BgL_arg1125z00_166;
								obj_t BgL_arg1126z00_167;

								BgL_arg1125z00_166 =
									(((BgL_typez00_bglt) COBJECT(BgL_objz00_156))->BgL_namez00);
								{	/* Tvector/tvector.scm 53 */
									obj_t BgL_list1127z00_168;

									BgL_list1127z00_168 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
									BgL_arg1126z00_167 = BgL_list1127z00_168;
								}
								BgL_typez00_158 =
									BGl_declarezd2subtypez12zc0zztype_envz00(BgL_tvectzd2idzd2_69,
									BgL_arg1125z00_166, BgL_arg1126z00_167, CNST_TABLE_REF(1));
							}
							BgL_itemzd2typezd2_159 =
								BGl_usezd2typez12zc0zztype_envz00(BgL_itemzd2idzd2_70,
								BGl_findzd2locationzd2zztools_locationz00(BgL_srcz00_71));
							{	/* Tvector/tvector.scm 57 */
								BgL_tvecz00_bglt BgL_wide1078z00_162;

								BgL_wide1078z00_162 =
									((BgL_tvecz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_tvecz00_bgl))));
								{	/* Tvector/tvector.scm 57 */
									obj_t BgL_auxz00_967;
									BgL_objectz00_bglt BgL_tmpz00_964;

									BgL_auxz00_967 = ((obj_t) BgL_wide1078z00_162);
									BgL_tmpz00_964 =
										((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_typez00_158));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_964, BgL_auxz00_967);
								}
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_typez00_158));
								{	/* Tvector/tvector.scm 57 */
									long BgL_arg1122z00_163;

									BgL_arg1122z00_163 =
										BGL_CLASS_NUM(BGl_tvecz00zztvector_tvectorz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_typez00_158)),
										BgL_arg1122z00_163);
								}
								((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_typez00_158));
							}
							{
								BgL_tvecz00_bglt BgL_auxz00_978;

								{
									obj_t BgL_auxz00_979;

									{	/* Tvector/tvector.scm 57 */
										BgL_objectz00_bglt BgL_tmpz00_980;

										BgL_tmpz00_980 =
											((BgL_objectz00_bglt)
											((BgL_typez00_bglt) BgL_typez00_158));
										BgL_auxz00_979 = BGL_OBJECT_WIDENING(BgL_tmpz00_980);
									}
									BgL_auxz00_978 = ((BgL_tvecz00_bglt) BgL_auxz00_979);
								}
								((((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_978))->
										BgL_itemzd2typezd2) =
									((BgL_typez00_bglt) BgL_itemzd2typezd2_159), BUNSPEC);
							}
							((BgL_typez00_bglt) BgL_typez00_158);
							BGl_producezd2modulezd2clausez12z12zzmodule_modulez00
								(BGl_makezd2coercionzd2clausez00zztvector_tvectorz00
								(BgL_tvectzd2idzd2_69));
							BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00 =
								MAKE_YOUNG_PAIR(((obj_t) ((BgL_typez00_bglt) BgL_typez00_158)),
								BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00);
							((((BgL_typez00_bglt) COBJECT(BgL_itemzd2typezd2_159))->
									BgL_tvectorz00) =
								((obj_t) ((obj_t) BgL_typez00_158)), BUNSPEC);
							return ((BgL_typez00_bglt) BgL_typez00_158);
						}
					else
						{	/* Tvector/tvector.scm 49 */
							return
								((BgL_typez00_bglt)
								BGl_userzd2errorzd2zztools_errorz00
								(BGl_string1209z00zztvector_tvectorz00,
									BGl_string1210z00zztvector_tvectorz00,
									BGl_expzd2envzd2zz__r4_numbers_6_5z00, BNIL));
						}
				}
			}
		}

	}



/* &declare-tvector-type! */
	BgL_typez00_bglt
		BGl_z62declarezd2tvectorzd2typez12z70zztvector_tvectorz00(obj_t
		BgL_envz00_558, obj_t BgL_tvectzd2idzd2_559, obj_t BgL_itemzd2idzd2_560,
		obj_t BgL_srcz00_561)
	{
		{	/* Tvector/tvector.scm 47 */
			return
				BGl_declarezd2tvectorzd2typez12z12zztvector_tvectorz00
				(BgL_tvectzd2idzd2_559, BgL_itemzd2idzd2_560, BgL_srcz00_561);
		}

	}



/* emit-tvector-types */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2tvectorzd2typesz00zztvector_tvectorz00(obj_t
		BgL_oportz00_72)
	{
		{	/* Tvector/tvector.scm 70 */
			if (NULLP(BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00))
				{	/* Tvector/tvector.scm 71 */
					BFALSE;
				}
			else
				{	/* Tvector/tvector.scm 71 */
					bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
					bgl_display_string(BGl_string1211z00zztvector_tvectorz00,
						BgL_oportz00_72);
					bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
				}
			{	/* Tvector/tvector.scm 73 */
				obj_t BgL_g1098z00_173;

				BgL_g1098z00_173 =
					bgl_reverse_bang
					(BGl_za2tvectorzd2typezd2listza2z00zztvector_tvectorz00);
				{
					obj_t BgL_l1096z00_175;

					BgL_l1096z00_175 = BgL_g1098z00_173;
				BgL_zc3z04anonymousza31132ze3z87_176:
					if (PAIRP(BgL_l1096z00_175))
						{	/* Tvector/tvector.scm 82 */
							{	/* Tvector/tvector.scm 74 */
								obj_t BgL_tvectorz00_178;

								BgL_tvectorz00_178 = CAR(BgL_l1096z00_175);
								{	/* Tvector/tvector.scm 74 */
									obj_t BgL_itemzd2typezd2namez00_179;

									{	/* Tvector/tvector.scm 74 */
										BgL_typez00_bglt BgL_arg1138z00_187;

										{
											BgL_tvecz00_bglt BgL_auxz00_1007;

											{
												obj_t BgL_auxz00_1008;

												{	/* Tvector/tvector.scm 74 */
													BgL_objectz00_bglt BgL_tmpz00_1009;

													BgL_tmpz00_1009 =
														((BgL_objectz00_bglt)
														((BgL_typez00_bglt) BgL_tvectorz00_178));
													BgL_auxz00_1008 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_1009);
												}
												BgL_auxz00_1007 = ((BgL_tvecz00_bglt) BgL_auxz00_1008);
											}
											BgL_arg1138z00_187 =
												(((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_1007))->
												BgL_itemzd2typezd2);
										}
										BgL_itemzd2typezd2namez00_179 =
											(((BgL_typez00_bglt) COBJECT(BgL_arg1138z00_187))->
											BgL_namez00);
									}
									bgl_display_string(BGl_string1212z00zztvector_tvectorz00,
										BgL_oportz00_72);
									bgl_display_obj(bigloo_mangle(BgL_itemzd2typezd2namez00_179),
										BgL_oportz00_72);
									bgl_display_string(BGl_string1213z00zztvector_tvectorz00,
										BgL_oportz00_72);
									bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
									bgl_display_string(BGl_string1214z00zztvector_tvectorz00,
										BgL_oportz00_72);
									bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
									bgl_display_string(BGl_string1215z00zztvector_tvectorz00,
										BgL_oportz00_72);
									bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
									bgl_display_string(BGl_string1216z00zztvector_tvectorz00,
										BgL_oportz00_72);
									bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
									bgl_display_string(BGl_string1217z00zztvector_tvectorz00,
										BgL_oportz00_72);
									bgl_display_obj(BgL_itemzd2typezd2namez00_179,
										BgL_oportz00_72);
									bgl_display_string(BGl_string1218z00zztvector_tvectorz00,
										BgL_oportz00_72);
									bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
									bgl_display_string(BGl_string1219z00zztvector_tvectorz00,
										BgL_oportz00_72);
									bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
							}}
							{
								obj_t BgL_l1096z00_1033;

								BgL_l1096z00_1033 = CDR(BgL_l1096z00_175);
								BgL_l1096z00_175 = BgL_l1096z00_1033;
								goto BgL_zc3z04anonymousza31132ze3z87_176;
							}
						}
					else
						{	/* Tvector/tvector.scm 82 */
							((bool_t) 1);
						}
				}
			}
			return bgl_display_char(((unsigned char) 10), BgL_oportz00_72);
		}

	}



/* &emit-tvector-types */
	obj_t BGl_z62emitzd2tvectorzd2typesz62zztvector_tvectorz00(obj_t
		BgL_envz00_564, obj_t BgL_oportz00_565)
	{
		{	/* Tvector/tvector.scm 70 */
			return
				BGl_emitzd2tvectorzd2typesz00zztvector_tvectorz00(BgL_oportz00_565);
		}

	}



/* make-coercion-clause */
	obj_t BGl_makezd2coercionzd2clausez00zztvector_tvectorz00(obj_t
		BgL_tvectzd2idzd2_73)
	{
		{	/* Tvector/tvector.scm 88 */
			{	/* Tvector/tvector.scm 89 */
				obj_t BgL_arg1141z00_190;

				{	/* Tvector/tvector.scm 89 */
					obj_t BgL_arg1142z00_191;
					obj_t BgL_arg1143z00_192;

					{	/* Tvector/tvector.scm 89 */
						obj_t BgL_arg1145z00_193;

						{	/* Tvector/tvector.scm 89 */
							obj_t BgL_arg1148z00_194;

							{	/* Tvector/tvector.scm 89 */
								obj_t BgL_arg1149z00_195;

								{	/* Tvector/tvector.scm 89 */
									obj_t BgL_arg1152z00_196;

									BgL_arg1152z00_196 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									BgL_arg1149z00_195 =
										MAKE_YOUNG_PAIR(BNIL, BgL_arg1152z00_196);
								}
								BgL_arg1148z00_194 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1149z00_195);
							}
							BgL_arg1145z00_193 =
								MAKE_YOUNG_PAIR(BgL_tvectzd2idzd2_73, BgL_arg1148z00_194);
						}
						BgL_arg1142z00_191 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1145z00_193);
					}
					{	/* Tvector/tvector.scm 90 */
						obj_t BgL_arg1153z00_197;

						{	/* Tvector/tvector.scm 90 */
							obj_t BgL_arg1154z00_198;

							{	/* Tvector/tvector.scm 90 */
								obj_t BgL_arg1157z00_199;

								{	/* Tvector/tvector.scm 90 */
									obj_t BgL_arg1158z00_200;

									{	/* Tvector/tvector.scm 90 */
										obj_t BgL_arg1162z00_201;

										BgL_arg1162z00_201 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										BgL_arg1158z00_200 =
											MAKE_YOUNG_PAIR(BNIL, BgL_arg1162z00_201);
									}
									BgL_arg1157z00_199 =
										MAKE_YOUNG_PAIR(BgL_tvectzd2idzd2_73, BgL_arg1158z00_200);
								}
								BgL_arg1154z00_198 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1157z00_199);
							}
							BgL_arg1153z00_197 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BgL_arg1154z00_198);
						}
						BgL_arg1143z00_192 = MAKE_YOUNG_PAIR(BgL_arg1153z00_197, BNIL);
					}
					BgL_arg1141z00_190 =
						MAKE_YOUNG_PAIR(BgL_arg1142z00_191, BgL_arg1143z00_192);
				}
				return MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BgL_arg1141z00_190);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvector.scm 15 */
			{	/* Tvector/tvector.scm 25 */
				obj_t BgL_arg1182z00_206;
				obj_t BgL_arg1183z00_207;

				{	/* Tvector/tvector.scm 25 */
					obj_t BgL_v1099z00_245;

					BgL_v1099z00_245 = create_vector(1L);
					{	/* Tvector/tvector.scm 25 */
						obj_t BgL_arg1197z00_246;

						BgL_arg1197z00_246 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc1221z00zztvector_tvectorz00,
							BGl_proc1220z00zztvector_tvectorz00, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1099z00_245, 0L, BgL_arg1197z00_246);
					}
					BgL_arg1182z00_206 = BgL_v1099z00_245;
				}
				{	/* Tvector/tvector.scm 25 */
					obj_t BgL_v1100z00_256;

					BgL_v1100z00_256 = create_vector(0L);
					BgL_arg1183z00_207 = BgL_v1100z00_256;
				}
				return (BGl_tvecz00zztvector_tvectorz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(6),
						CNST_TABLE_REF(7), BGl_typez00zztype_typez00, 21744L,
						BGl_proc1225z00zztvector_tvectorz00,
						BGl_proc1224z00zztvector_tvectorz00, BFALSE,
						BGl_proc1223z00zztvector_tvectorz00,
						BGl_proc1222z00zztvector_tvectorz00, BgL_arg1182z00_206,
						BgL_arg1183z00_207), BUNSPEC);
			}
		}

	}



/* &lambda1192 */
	BgL_typez00_bglt BGl_z62lambda1192z62zztvector_tvectorz00(obj_t
		BgL_envz00_572, obj_t BgL_o1071z00_573)
	{
		{	/* Tvector/tvector.scm 25 */
			{	/* Tvector/tvector.scm 25 */
				long BgL_arg1193z00_634;

				{	/* Tvector/tvector.scm 25 */
					obj_t BgL_arg1194z00_635;

					{	/* Tvector/tvector.scm 25 */
						obj_t BgL_arg1196z00_636;

						{	/* Tvector/tvector.scm 25 */
							obj_t BgL_arg1815z00_637;
							long BgL_arg1816z00_638;

							BgL_arg1815z00_637 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Tvector/tvector.scm 25 */
								long BgL_arg1817z00_639;

								BgL_arg1817z00_639 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1071z00_573)));
								BgL_arg1816z00_638 = (BgL_arg1817z00_639 - OBJECT_TYPE);
							}
							BgL_arg1196z00_636 =
								VECTOR_REF(BgL_arg1815z00_637, BgL_arg1816z00_638);
						}
						BgL_arg1194z00_635 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1196z00_636);
					}
					{	/* Tvector/tvector.scm 25 */
						obj_t BgL_tmpz00_1070;

						BgL_tmpz00_1070 = ((obj_t) BgL_arg1194z00_635);
						BgL_arg1193z00_634 = BGL_CLASS_NUM(BgL_tmpz00_1070);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_typez00_bglt) BgL_o1071z00_573)), BgL_arg1193z00_634);
			}
			{	/* Tvector/tvector.scm 25 */
				BgL_objectz00_bglt BgL_tmpz00_1076;

				BgL_tmpz00_1076 =
					((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1071z00_573));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1076, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_o1071z00_573));
			return ((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1071z00_573));
		}

	}



/* &<@anonymous:1191> */
	obj_t BGl_z62zc3z04anonymousza31191ze3ze5zztvector_tvectorz00(obj_t
		BgL_envz00_574, obj_t BgL_new1070z00_575)
	{
		{	/* Tvector/tvector.scm 25 */
			{
				BgL_typez00_bglt BgL_auxz00_1084;

				((((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt)
									((BgL_typez00_bglt) BgL_new1070z00_575))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(8)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_siza7eza7) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_coercezd2tozd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_parentsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_initzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_magiczf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_nullz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_z42z42) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_pointedzd2tozd2byz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_tvectorz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_locationz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_importzd2locationzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BgL_new1070z00_575))))->BgL_occurrencez00) =
					((int) (int) (0L)), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_1142;
					BgL_tvecz00_bglt BgL_auxz00_1135;

					{	/* Tvector/tvector.scm 25 */
						obj_t BgL_classz00_641;

						BgL_classz00_641 = BGl_typez00zztype_typez00;
						{	/* Tvector/tvector.scm 25 */
							obj_t BgL__ortest_1117z00_642;

							BgL__ortest_1117z00_642 = BGL_CLASS_NIL(BgL_classz00_641);
							if (CBOOL(BgL__ortest_1117z00_642))
								{	/* Tvector/tvector.scm 25 */
									BgL_auxz00_1142 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_642);
								}
							else
								{	/* Tvector/tvector.scm 25 */
									BgL_auxz00_1142 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_641));
								}
						}
					}
					{
						obj_t BgL_auxz00_1136;

						{	/* Tvector/tvector.scm 25 */
							BgL_objectz00_bglt BgL_tmpz00_1137;

							BgL_tmpz00_1137 =
								((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_new1070z00_575));
							BgL_auxz00_1136 = BGL_OBJECT_WIDENING(BgL_tmpz00_1137);
						}
						BgL_auxz00_1135 = ((BgL_tvecz00_bglt) BgL_auxz00_1136);
					}
					((((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_1135))->BgL_itemzd2typezd2) =
						((BgL_typez00_bglt) BgL_auxz00_1142), BUNSPEC);
				}
				BgL_auxz00_1084 = ((BgL_typez00_bglt) BgL_new1070z00_575);
				return ((obj_t) BgL_auxz00_1084);
			}
		}

	}



/* &lambda1189 */
	BgL_typez00_bglt BGl_z62lambda1189z62zztvector_tvectorz00(obj_t
		BgL_envz00_576, obj_t BgL_o1067z00_577)
	{
		{	/* Tvector/tvector.scm 25 */
			{	/* Tvector/tvector.scm 25 */
				BgL_tvecz00_bglt BgL_wide1069z00_644;

				BgL_wide1069z00_644 =
					((BgL_tvecz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_tvecz00_bgl))));
				{	/* Tvector/tvector.scm 25 */
					obj_t BgL_auxz00_1157;
					BgL_objectz00_bglt BgL_tmpz00_1153;

					BgL_auxz00_1157 = ((obj_t) BgL_wide1069z00_644);
					BgL_tmpz00_1153 =
						((BgL_objectz00_bglt)
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1067z00_577)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1153, BgL_auxz00_1157);
				}
				((BgL_objectz00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1067z00_577)));
				{	/* Tvector/tvector.scm 25 */
					long BgL_arg1190z00_645;

					BgL_arg1190z00_645 = BGL_CLASS_NUM(BGl_tvecz00zztvector_tvectorz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_typez00_bglt)
								((BgL_typez00_bglt) BgL_o1067z00_577))), BgL_arg1190z00_645);
				}
				return
					((BgL_typez00_bglt)
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_o1067z00_577)));
			}
		}

	}



/* &lambda1184 */
	BgL_typez00_bglt BGl_z62lambda1184z62zztvector_tvectorz00(obj_t
		BgL_envz00_578, obj_t BgL_id1050z00_579, obj_t BgL_name1051z00_580,
		obj_t BgL_siza7e1052za7_581, obj_t BgL_class1053z00_582,
		obj_t BgL_coercezd2to1054zd2_583, obj_t BgL_parents1055z00_584,
		obj_t BgL_initzf31056zf3_585, obj_t BgL_magiczf31057zf3_586,
		obj_t BgL_null1058z00_587, obj_t BgL_z421059z42_588,
		obj_t BgL_alias1060z00_589, obj_t BgL_pointedzd2tozd2by1061z00_590,
		obj_t BgL_tvector1062z00_591, obj_t BgL_location1063z00_592,
		obj_t BgL_importzd2location1064zd2_593, obj_t BgL_occurrence1065z00_594,
		obj_t BgL_itemzd2type1066zd2_595)
	{
		{	/* Tvector/tvector.scm 25 */
			{	/* Tvector/tvector.scm 25 */
				bool_t BgL_initzf31056zf3_647;
				bool_t BgL_magiczf31057zf3_648;
				int BgL_occurrence1065z00_649;

				BgL_initzf31056zf3_647 = CBOOL(BgL_initzf31056zf3_585);
				BgL_magiczf31057zf3_648 = CBOOL(BgL_magiczf31057zf3_586);
				BgL_occurrence1065z00_649 = CINT(BgL_occurrence1065z00_594);
				{	/* Tvector/tvector.scm 25 */
					BgL_typez00_bglt BgL_new1083z00_651;

					{	/* Tvector/tvector.scm 25 */
						BgL_typez00_bglt BgL_tmp1081z00_652;
						BgL_tvecz00_bglt BgL_wide1082z00_653;

						{
							BgL_typez00_bglt BgL_auxz00_1174;

							{	/* Tvector/tvector.scm 25 */
								BgL_typez00_bglt BgL_new1080z00_654;

								BgL_new1080z00_654 =
									((BgL_typez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_typez00_bgl))));
								{	/* Tvector/tvector.scm 25 */
									long BgL_arg1188z00_655;

									BgL_arg1188z00_655 = BGL_CLASS_NUM(BGl_typez00zztype_typez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1080z00_654),
										BgL_arg1188z00_655);
								}
								{	/* Tvector/tvector.scm 25 */
									BgL_objectz00_bglt BgL_tmpz00_1179;

									BgL_tmpz00_1179 = ((BgL_objectz00_bglt) BgL_new1080z00_654);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1179, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1080z00_654);
								BgL_auxz00_1174 = BgL_new1080z00_654;
							}
							BgL_tmp1081z00_652 = ((BgL_typez00_bglt) BgL_auxz00_1174);
						}
						BgL_wide1082z00_653 =
							((BgL_tvecz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_tvecz00_bgl))));
						{	/* Tvector/tvector.scm 25 */
							obj_t BgL_auxz00_1187;
							BgL_objectz00_bglt BgL_tmpz00_1185;

							BgL_auxz00_1187 = ((obj_t) BgL_wide1082z00_653);
							BgL_tmpz00_1185 = ((BgL_objectz00_bglt) BgL_tmp1081z00_652);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_1185, BgL_auxz00_1187);
						}
						((BgL_objectz00_bglt) BgL_tmp1081z00_652);
						{	/* Tvector/tvector.scm 25 */
							long BgL_arg1187z00_656;

							BgL_arg1187z00_656 =
								BGL_CLASS_NUM(BGl_tvecz00zztvector_tvectorz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1081z00_652), BgL_arg1187z00_656);
						}
						BgL_new1083z00_651 = ((BgL_typez00_bglt) BgL_tmp1081z00_652);
					}
					((((BgL_typez00_bglt) COBJECT(
									((BgL_typez00_bglt) BgL_new1083z00_651)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1050z00_579)), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_namez00) =
						((obj_t) BgL_name1051z00_580), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_siza7eza7) =
						((obj_t) BgL_siza7e1052za7_581), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_classz00) =
						((obj_t) BgL_class1053z00_582), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_coercezd2tozd2) =
						((obj_t) BgL_coercezd2to1054zd2_583), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_parentsz00) =
						((obj_t) BgL_parents1055z00_584), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_initzf3zf3) =
						((bool_t) BgL_initzf31056zf3_647), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_magiczf3zf3) =
						((bool_t) BgL_magiczf31057zf3_648), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_nullz00) =
						((obj_t) BgL_null1058z00_587), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_z42z42) =
						((obj_t) BgL_z421059z42_588), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_aliasz00) =
						((obj_t) BgL_alias1060z00_589), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_pointedzd2tozd2byz00) =
						((obj_t) BgL_pointedzd2tozd2by1061z00_590), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_tvectorz00) =
						((obj_t) BgL_tvector1062z00_591), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_locationz00) =
						((obj_t) BgL_location1063z00_592), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_importzd2locationzd2) =
						((obj_t) BgL_importzd2location1064zd2_593), BUNSPEC);
					((((BgL_typez00_bglt) COBJECT(((BgL_typez00_bglt)
										BgL_new1083z00_651)))->BgL_occurrencez00) =
						((int) BgL_occurrence1065z00_649), BUNSPEC);
					{
						BgL_tvecz00_bglt BgL_auxz00_1228;

						{
							obj_t BgL_auxz00_1229;

							{	/* Tvector/tvector.scm 25 */
								BgL_objectz00_bglt BgL_tmpz00_1230;

								BgL_tmpz00_1230 = ((BgL_objectz00_bglt) BgL_new1083z00_651);
								BgL_auxz00_1229 = BGL_OBJECT_WIDENING(BgL_tmpz00_1230);
							}
							BgL_auxz00_1228 = ((BgL_tvecz00_bglt) BgL_auxz00_1229);
						}
						((((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_1228))->
								BgL_itemzd2typezd2) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BgL_itemzd2type1066zd2_595)), BUNSPEC);
					}
					return BgL_new1083z00_651;
				}
			}
		}

	}



/* &lambda1201 */
	obj_t BGl_z62lambda1201z62zztvector_tvectorz00(obj_t BgL_envz00_596,
		obj_t BgL_oz00_597, obj_t BgL_vz00_598)
	{
		{	/* Tvector/tvector.scm 25 */
			{
				BgL_tvecz00_bglt BgL_auxz00_1236;

				{
					obj_t BgL_auxz00_1237;

					{	/* Tvector/tvector.scm 25 */
						BgL_objectz00_bglt BgL_tmpz00_1238;

						BgL_tmpz00_1238 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_597));
						BgL_auxz00_1237 = BGL_OBJECT_WIDENING(BgL_tmpz00_1238);
					}
					BgL_auxz00_1236 = ((BgL_tvecz00_bglt) BgL_auxz00_1237);
				}
				return
					((((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_1236))->BgL_itemzd2typezd2) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_598)), BUNSPEC);
			}
		}

	}



/* &lambda1200 */
	BgL_typez00_bglt BGl_z62lambda1200z62zztvector_tvectorz00(obj_t
		BgL_envz00_599, obj_t BgL_oz00_600)
	{
		{	/* Tvector/tvector.scm 25 */
			{
				BgL_tvecz00_bglt BgL_auxz00_1245;

				{
					obj_t BgL_auxz00_1246;

					{	/* Tvector/tvector.scm 25 */
						BgL_objectz00_bglt BgL_tmpz00_1247;

						BgL_tmpz00_1247 =
							((BgL_objectz00_bglt) ((BgL_typez00_bglt) BgL_oz00_600));
						BgL_auxz00_1246 = BGL_OBJECT_WIDENING(BgL_tmpz00_1247);
					}
					BgL_auxz00_1245 = ((BgL_tvecz00_bglt) BgL_auxz00_1246);
				}
				return
					(((BgL_tvecz00_bglt) COBJECT(BgL_auxz00_1245))->BgL_itemzd2typezd2);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvector.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvector.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zztvector_tvectorz00(void)
	{
		{	/* Tvector/tvector.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1226z00zztvector_tvectorz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1226z00zztvector_tvectorz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1226z00zztvector_tvectorz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1226z00zztvector_tvectorz00));
			return
				BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1226z00zztvector_tvectorz00));
		}

	}

#ifdef __cplusplus
}
#endif
