/*===========================================================================*/
/*   (Globalize/clocto.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/clocto.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_CLOCTO_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_CLOCTO_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_CLOCTO_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_cloctoza7 =
		BUNSPEC;
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_cloctoza7(void);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_cloctoza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_ctozd2transitivezd2closurez12z12zzglobaliza7e_cloctoza7
		(BgL_localz00_bglt);
	static obj_t BGl_appendzd221011zd2zzglobaliza7e_cloctoza7(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_cloctoza7(void);
	extern obj_t BGl_unionz00zzglobaliza7e_kaptureza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_cloctoza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_kaptureza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_cloctoza7(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_cloctoza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_cloctoza7(void);
	static obj_t
		BGl_z62ctozd2transitivezd2closurez12z70zzglobaliza7e_cloctoza7(obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_ctozd2transitivezd2closurez12zd2envzc0zzglobaliza7e_cloctoza7,
		BgL_bgl_za762ctoza7d2transit1608z00,
		BGl_z62ctozd2transitivezd2closurez12z70zzglobaliza7e_cloctoza7, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1607z00zzglobaliza7e_cloctoza7,
		BgL_bgl_string1607za700za7za7g1609za7, "globalize_clocto", 16);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_cloctoza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_cloctoza7(long
		BgL_checksumz00_1971, char *BgL_fromz00_1972)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_cloctoza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_cloctoza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_cloctoza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_cloctoza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_cloctoza7();
					return BGl_methodzd2initzd2zzglobaliza7e_cloctoza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_cloctoza7(void)
	{
		{	/* Globalize/clocto.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_clocto");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_clocto");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_clocto");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_cloctoza7(void)
	{
		{	/* Globalize/clocto.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzglobaliza7e_cloctoza7(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1688;

				BgL_headz00_1688 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1689;
					obj_t BgL_tailz00_1690;

					BgL_prevz00_1689 = BgL_headz00_1688;
					BgL_tailz00_1690 = BgL_l1z00_1;
				BgL_loopz00_1691:
					if (PAIRP(BgL_tailz00_1690))
						{
							obj_t BgL_newzd2prevzd2_1693;

							BgL_newzd2prevzd2_1693 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1690), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1689, BgL_newzd2prevzd2_1693);
							{
								obj_t BgL_tailz00_1991;
								obj_t BgL_prevz00_1990;

								BgL_prevz00_1990 = BgL_newzd2prevzd2_1693;
								BgL_tailz00_1991 = CDR(BgL_tailz00_1690);
								BgL_tailz00_1690 = BgL_tailz00_1991;
								BgL_prevz00_1689 = BgL_prevz00_1990;
								goto BgL_loopz00_1691;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1688);
				}
			}
		}

	}



/* cto-transitive-closure! */
	BGL_EXPORTED_DEF obj_t
		BGl_ctozd2transitivezd2closurez12z12zzglobaliza7e_cloctoza7
		(BgL_localz00_bglt BgL_hostz00_3)
	{
		{	/* Globalize/clocto.scm 36 */
			{	/* Globalize/clocto.scm 37 */
				BgL_valuez00_bglt BgL_infoz00_1696;

				BgL_infoz00_1696 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_hostz00_3)))->BgL_valuez00);
				{	/* Globalize/clocto.scm 37 */
					obj_t BgL_ctoz00_1697;

					{	/* Globalize/clocto.scm 38 */
						obj_t BgL_arg1331z00_1735;
						obj_t BgL_arg1332z00_1736;

						{
							BgL_sfunzf2ginfozf2_bglt BgL_auxz00_1996;

							{
								obj_t BgL_auxz00_1997;

								{	/* Globalize/clocto.scm 38 */
									BgL_objectz00_bglt BgL_tmpz00_1998;

									BgL_tmpz00_1998 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_infoz00_1696));
									BgL_auxz00_1997 = BGL_OBJECT_WIDENING(BgL_tmpz00_1998);
								}
								BgL_auxz00_1996 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_1997);
							}
							BgL_arg1331z00_1735 =
								(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_1996))->
								BgL_ctoz00);
						}
						{
							BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2004;

							{
								obj_t BgL_auxz00_2005;

								{	/* Globalize/clocto.scm 38 */
									BgL_objectz00_bglt BgL_tmpz00_2006;

									BgL_tmpz00_2006 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_infoz00_1696));
									BgL_auxz00_2005 = BGL_OBJECT_WIDENING(BgL_tmpz00_2006);
								}
								BgL_auxz00_2004 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2005);
							}
							BgL_arg1332z00_1736 =
								(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2004))->
								BgL_efunctionsz00);
						}
						BgL_ctoz00_1697 =
							BGl_appendzd221011zd2zzglobaliza7e_cloctoza7(BgL_arg1331z00_1735,
							BgL_arg1332z00_1736);
					}
					{	/* Globalize/clocto.scm 38 */
						obj_t BgL_ctoza2zd2origz70_1698;

						{	/* Globalize/clocto.scm 39 */
							obj_t BgL_list1329z00_1734;

							BgL_list1329z00_1734 = MAKE_YOUNG_PAIR(BgL_ctoz00_1697, BNIL);
							BgL_ctoza2zd2origz70_1698 = BgL_list1329z00_1734;
						}
						{	/* Globalize/clocto.scm 39 */

							{
								obj_t BgL_ctoz00_1700;
								obj_t BgL_ctoza2za2_1701;

								BgL_ctoz00_1700 = BgL_ctoz00_1697;
								BgL_ctoza2za2_1701 = BgL_ctoza2zd2origz70_1698;
							BgL_zc3z04anonymousza31250ze3z87_1702:
								if (NULLP(BgL_ctoz00_1700))
									{	/* Globalize/clocto.scm 45 */
										obj_t BgL_arg1252z00_1704;

										if ((BgL_ctoza2za2_1701 == BgL_ctoza2zd2origz70_1698))
											{	/* Globalize/clocto.scm 45 */
												BgL_arg1252z00_1704 =
													CAR(((obj_t) BgL_ctoza2zd2origz70_1698));
											}
										else
											{	/* Globalize/clocto.scm 45 */
												BgL_arg1252z00_1704 =
													BGl_unionz00zzglobaliza7e_kaptureza7
													(BgL_ctoza2za2_1701);
											}
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2021;

											{
												obj_t BgL_auxz00_2022;

												{	/* Globalize/clocto.scm 44 */
													BgL_objectz00_bglt BgL_tmpz00_2023;

													BgL_tmpz00_2023 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_infoz00_1696));
													BgL_auxz00_2022 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2023);
												}
												BgL_auxz00_2021 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2022);
											}
											return
												((((BgL_sfunzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_2021))->BgL_ctoza2za2) =
												((obj_t) BgL_arg1252z00_1704), BUNSPEC);
										}
									}
								else
									{	/* Globalize/clocto.scm 48 */
										bool_t BgL_test1615z00_2029;

										{	/* Globalize/clocto.scm 48 */
											obj_t BgL_arg1328z00_1732;

											BgL_arg1328z00_1732 = CAR(((obj_t) BgL_ctoz00_1700));
											BgL_test1615z00_2029 =
												(BgL_arg1328z00_1732 == BGl_localz00zzast_varz00);
										}
										if (BgL_test1615z00_2029)
											{	/* Globalize/clocto.scm 49 */
												obj_t BgL_arg1272z00_1707;

												BgL_arg1272z00_1707 = CDR(((obj_t) BgL_ctoz00_1700));
												{
													obj_t BgL_ctoz00_2035;

													BgL_ctoz00_2035 = BgL_arg1272z00_1707;
													BgL_ctoz00_1700 = BgL_ctoz00_2035;
													goto BgL_zc3z04anonymousza31250ze3z87_1702;
												}
											}
										else
											{	/* Globalize/clocto.scm 50 */
												bool_t BgL_test1617z00_2036;

												{	/* Globalize/clocto.scm 50 */
													BgL_sfunz00_bglt BgL_oz00_1943;

													BgL_oz00_1943 =
														((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_ctoz00_1700))))))->
															BgL_valuez00));
													{
														BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2043;

														{
															obj_t BgL_auxz00_2044;

															{	/* Globalize/clocto.scm 50 */
																BgL_objectz00_bglt BgL_tmpz00_2045;

																BgL_tmpz00_2045 =
																	((BgL_objectz00_bglt) BgL_oz00_1943);
																BgL_auxz00_2044 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2045);
															}
															BgL_auxz00_2043 =
																((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2044);
														}
														BgL_test1617z00_2036 =
															(((BgL_sfunzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_2043))->BgL_gzf3zf3);
													}
												}
												if (BgL_test1617z00_2036)
													{	/* Globalize/clocto.scm 51 */
														obj_t BgL_arg1306z00_1711;

														BgL_arg1306z00_1711 =
															CDR(((obj_t) BgL_ctoz00_1700));
														{
															obj_t BgL_ctoz00_2052;

															BgL_ctoz00_2052 = BgL_arg1306z00_1711;
															BgL_ctoz00_1700 = BgL_ctoz00_2052;
															goto BgL_zc3z04anonymousza31250ze3z87_1702;
														}
													}
												else
													{	/* Globalize/clocto.scm 52 */
														bool_t BgL_test1618z00_2053;

														{	/* Globalize/clocto.scm 52 */
															BgL_sfunz00_bglt BgL_oz00_1948;

															BgL_oz00_1948 =
																((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					CAR(
																						((obj_t) BgL_ctoz00_1700))))))->
																	BgL_valuez00));
															{	/* Globalize/clocto.scm 52 */
																obj_t BgL_tmpz00_2060;

																{
																	BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2061;

																	{
																		obj_t BgL_auxz00_2062;

																		{	/* Globalize/clocto.scm 52 */
																			BgL_objectz00_bglt BgL_tmpz00_2063;

																			BgL_tmpz00_2063 =
																				((BgL_objectz00_bglt) BgL_oz00_1948);
																			BgL_auxz00_2062 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2063);
																		}
																		BgL_auxz00_2061 =
																			((BgL_sfunzf2ginfozf2_bglt)
																			BgL_auxz00_2062);
																	}
																	BgL_tmpz00_2060 =
																		(((BgL_sfunzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_2061))->BgL_ctoza2za2);
																}
																BgL_test1618z00_2053 = CBOOL(BgL_tmpz00_2060);
															}
														}
														if (BgL_test1618z00_2053)
															{	/* Globalize/clocto.scm 54 */
																obj_t BgL_arg1310z00_1715;

																BgL_arg1310z00_1715 =
																	CDR(((obj_t) BgL_ctoz00_1700));
																{
																	obj_t BgL_ctoz00_2071;

																	BgL_ctoz00_2071 = BgL_arg1310z00_1715;
																	BgL_ctoz00_1700 = BgL_ctoz00_2071;
																	goto BgL_zc3z04anonymousza31250ze3z87_1702;
																}
															}
														else
															{	/* Globalize/clocto.scm 56 */
																obj_t BgL_calleez00_1716;

																{	/* Globalize/clocto.scm 57 */
																	obj_t BgL_arg1317z00_1722;
																	obj_t BgL_arg1318z00_1723;

																	{	/* Globalize/clocto.scm 57 */
																		BgL_sfunz00_bglt BgL_oz00_1953;

																		BgL_oz00_1953 =
																			((BgL_sfunz00_bglt)
																			(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_localz00_bglt)
																								CAR(
																									((obj_t)
																										BgL_ctoz00_1700))))))->
																				BgL_valuez00));
																		{
																			BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2078;

																			{
																				obj_t BgL_auxz00_2079;

																				{	/* Globalize/clocto.scm 57 */
																					BgL_objectz00_bglt BgL_tmpz00_2080;

																					BgL_tmpz00_2080 =
																						((BgL_objectz00_bglt)
																						BgL_oz00_1953);
																					BgL_auxz00_2079 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_2080);
																				}
																				BgL_auxz00_2078 =
																					((BgL_sfunzf2ginfozf2_bglt)
																					BgL_auxz00_2079);
																			}
																			BgL_arg1317z00_1722 =
																				(((BgL_sfunzf2ginfozf2_bglt)
																					COBJECT(BgL_auxz00_2078))->
																				BgL_ctoz00);
																		}
																	}
																	{	/* Globalize/clocto.scm 58 */
																		BgL_sfunz00_bglt BgL_oz00_1957;

																		BgL_oz00_1957 =
																			((BgL_sfunz00_bglt)
																			(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_localz00_bglt)
																								CAR(
																									((obj_t)
																										BgL_ctoz00_1700))))))->
																				BgL_valuez00));
																		{
																			BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2091;

																			{
																				obj_t BgL_auxz00_2092;

																				{	/* Globalize/clocto.scm 58 */
																					BgL_objectz00_bglt BgL_tmpz00_2093;

																					BgL_tmpz00_2093 =
																						((BgL_objectz00_bglt)
																						BgL_oz00_1957);
																					BgL_auxz00_2092 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_2093);
																				}
																				BgL_auxz00_2091 =
																					((BgL_sfunzf2ginfozf2_bglt)
																					BgL_auxz00_2092);
																			}
																			BgL_arg1318z00_1723 =
																				(((BgL_sfunzf2ginfozf2_bglt)
																					COBJECT(BgL_auxz00_2091))->
																				BgL_efunctionsz00);
																		}
																	}
																	BgL_calleez00_1716 =
																		BGl_appendzd221011zd2zzglobaliza7e_cloctoza7
																		(BgL_arg1317z00_1722, BgL_arg1318z00_1723);
																}
																{	/* Globalize/clocto.scm 60 */
																	BgL_valuez00_bglt BgL_arg1311z00_1717;

																	BgL_arg1311z00_1717 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						CAR(
																							((obj_t) BgL_ctoz00_1700))))))->
																		BgL_valuez00);
																	{
																		BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2104;

																		{
																			obj_t BgL_auxz00_2105;

																			{	/* Globalize/clocto.scm 60 */
																				BgL_objectz00_bglt BgL_tmpz00_2106;

																				BgL_tmpz00_2106 =
																					((BgL_objectz00_bglt)
																					((BgL_sfunz00_bglt)
																						BgL_arg1311z00_1717));
																				BgL_auxz00_2105 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_2106);
																			}
																			BgL_auxz00_2104 =
																				((BgL_sfunzf2ginfozf2_bglt)
																				BgL_auxz00_2105);
																		}
																		((((BgL_sfunzf2ginfozf2_bglt)
																					COBJECT(BgL_auxz00_2104))->
																				BgL_ctoza2za2) =
																			((obj_t) BTRUE), BUNSPEC);
																	}
																}
																{	/* Globalize/clocto.scm 62 */
																	obj_t BgL_arg1314z00_1719;
																	obj_t BgL_arg1315z00_1720;

																	{	/* Globalize/clocto.scm 62 */
																		obj_t BgL_arg1316z00_1721;

																		BgL_arg1316z00_1721 =
																			CDR(((obj_t) BgL_ctoz00_1700));
																		BgL_arg1314z00_1719 =
																			BGl_appendzd221011zd2zzglobaliza7e_cloctoza7
																			(BgL_arg1316z00_1721, BgL_calleez00_1716);
																	}
																	BgL_arg1315z00_1720 =
																		MAKE_YOUNG_PAIR(BgL_calleez00_1716,
																		BgL_ctoza2za2_1701);
																	{
																		obj_t BgL_ctoza2za2_2117;
																		obj_t BgL_ctoz00_2116;

																		BgL_ctoz00_2116 = BgL_arg1314z00_1719;
																		BgL_ctoza2za2_2117 = BgL_arg1315z00_1720;
																		BgL_ctoza2za2_1701 = BgL_ctoza2za2_2117;
																		BgL_ctoz00_1700 = BgL_ctoz00_2116;
																		goto BgL_zc3z04anonymousza31250ze3z87_1702;
																	}
																}
															}
													}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &cto-transitive-closure! */
	obj_t BGl_z62ctozd2transitivezd2closurez12z70zzglobaliza7e_cloctoza7(obj_t
		BgL_envz00_1969, obj_t BgL_hostz00_1970)
	{
		{	/* Globalize/clocto.scm 36 */
			return
				BGl_ctozd2transitivezd2closurez12z12zzglobaliza7e_cloctoza7(
				((BgL_localz00_bglt) BgL_hostz00_1970));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_cloctoza7(void)
	{
		{	/* Globalize/clocto.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_cloctoza7(void)
	{
		{	/* Globalize/clocto.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_cloctoza7(void)
	{
		{	/* Globalize/clocto.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_cloctoza7(void)
	{
		{	/* Globalize/clocto.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1607z00zzglobaliza7e_cloctoza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1607z00zzglobaliza7e_cloctoza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1607z00zzglobaliza7e_cloctoza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1607z00zzglobaliza7e_cloctoza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1607z00zzglobaliza7e_cloctoza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1607z00zzglobaliza7e_cloctoza7));
			return
				BGl_modulezd2initializa7ationz75zzglobaliza7e_kaptureza7(459831036L,
				BSTRING_TO_STRING(BGl_string1607z00zzglobaliza7e_cloctoza7));
		}

	}

#ifdef __cplusplus
}
#endif
