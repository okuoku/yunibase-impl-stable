/*===========================================================================*/
/*   (Globalize/integration.scm)                                             */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/integration.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_INTEGRATION_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_INTEGRATION_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_INTEGRATION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_integrationza7 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t
		BGl_traversezd2callzd2toz12z12zzglobaliza7e_integrationza7(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_integrationza7(void);
	static obj_t BGl_integratezd2inz12zc0zzglobaliza7e_integrationza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_integrationza7(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_appendzd221011zd2zzglobaliza7e_integrationza7(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_integrationza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2integrationz12zc0zzglobaliza7e_integrationza7(BgL_globalz00_bglt,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_integrationza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_kaptureza7(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globaliza7ez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t
		BGl_z62zc3z04anonymousza31348ze3ze5zzglobaliza7e_integrationza7(obj_t,
		obj_t);
	static obj_t
		BGl_libraryzd2moduleszd2initz00zzglobaliza7e_integrationza7(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zzglobaliza7e_integrationza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_integrationza7(void);
	static obj_t BGl_z62setzd2integrationz12za2zzglobaliza7e_integrationza7(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2integrationz12zd2envz12zzglobaliza7e_integrationza7,
		BgL_bgl_za762setza7d2integra1727z00,
		BGl_z62setzd2integrationz12za2zzglobaliza7e_integrationza7, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1724z00zzglobaliza7e_integrationza7,
		BgL_bgl_string1724za700za7za7g1728za7, "set-integration!", 16);
	      DEFINE_STRING(BGl_string1725z00zzglobaliza7e_integrationza7,
		BgL_bgl_string1725za700za7za7g1729za7, "No integration for ", 19);
	      DEFINE_STRING(BGl_string1726z00zzglobaliza7e_integrationza7,
		BgL_bgl_string1726za700za7za7g1730za7, "globalize_integration", 21);
	BGL_IMPORT obj_t BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_integrationza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_integrationza7(long
		BgL_checksumz00_2408, char *BgL_fromz00_2409)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_integrationza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_integrationza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_integrationza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_integrationza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_integrationza7();
					return BGl_methodzd2initzd2zzglobaliza7e_integrationza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_integrationza7(void)
	{
		{	/* Globalize/integration.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L,
				"globalize_integration");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"globalize_integration");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_integration");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_integration");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_integration");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_integration");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L,
				"globalize_integration");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_integrationza7(void)
	{
		{	/* Globalize/integration.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzglobaliza7e_integrationza7(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1696;

				BgL_headz00_1696 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1697;
					obj_t BgL_tailz00_1698;

					BgL_prevz00_1697 = BgL_headz00_1696;
					BgL_tailz00_1698 = BgL_l1z00_1;
				BgL_loopz00_1699:
					if (PAIRP(BgL_tailz00_1698))
						{
							obj_t BgL_newzd2prevzd2_1701;

							BgL_newzd2prevzd2_1701 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1698), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1697, BgL_newzd2prevzd2_1701);
							{
								obj_t BgL_tailz00_2432;
								obj_t BgL_prevz00_2431;

								BgL_prevz00_2431 = BgL_newzd2prevzd2_1701;
								BgL_tailz00_2432 = CDR(BgL_tailz00_1698);
								BgL_tailz00_1698 = BgL_tailz00_2432;
								BgL_prevz00_1697 = BgL_prevz00_2431;
								goto BgL_loopz00_1699;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1696);
				}
			}
		}

	}



/* set-integration! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2integrationz12zc0zzglobaliza7e_integrationza7(BgL_globalz00_bglt
		BgL_globalz00_3, obj_t BgL_ez00_4, obj_t BgL_g0z00_5, obj_t BgL_g1z00_6)
	{
		{	/* Globalize/integration.scm 31 */
			{
				obj_t BgL_l1257z00_1705;

				BgL_l1257z00_1705 = BgL_ez00_4;
			BgL_zc3z04anonymousza31305ze3z87_1706:
				if (PAIRP(BgL_l1257z00_1705))
					{	/* Globalize/integration.scm 34 */
						{	/* Globalize/integration.scm 35 */
							obj_t BgL_fz00_1708;

							BgL_fz00_1708 = CAR(BgL_l1257z00_1705);
							{
								BgL_localzf2ginfozf2_bglt BgL_auxz00_2438;

								{
									obj_t BgL_auxz00_2439;

									{	/* Globalize/integration.scm 36 */
										BgL_objectz00_bglt BgL_tmpz00_2440;

										BgL_tmpz00_2440 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt) BgL_fz00_1708));
										BgL_auxz00_2439 = BGL_OBJECT_WIDENING(BgL_tmpz00_2440);
									}
									BgL_auxz00_2438 =
										((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2439);
								}
								((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2438))->
										BgL_globaliza7edzf3z54) = ((bool_t) ((bool_t) 1)), BUNSPEC);
							}
						}
						{
							obj_t BgL_l1257z00_2446;

							BgL_l1257z00_2446 = CDR(BgL_l1257z00_1705);
							BgL_l1257z00_1705 = BgL_l1257z00_2446;
							goto BgL_zc3z04anonymousza31305ze3z87_1706;
						}
					}
				else
					{	/* Globalize/integration.scm 34 */
						((bool_t) 1);
					}
			}
			{	/* Globalize/integration.scm 40 */
				obj_t BgL_g1112z00_1712;

				BgL_g1112z00_1712 =
					MAKE_YOUNG_PAIR(((obj_t) BgL_globalz00_3), BgL_ez00_4);
				{
					obj_t BgL_fnsz00_1714;

					BgL_fnsz00_1714 = BgL_g1112z00_1712;
				BgL_zc3z04anonymousza31309ze3z87_1715:
					if (NULLP(BgL_fnsz00_1714))
						{	/* Globalize/integration.scm 41 */
							((bool_t) 0);
						}
					else
						{	/* Globalize/integration.scm 44 */
							obj_t BgL_arg1312z00_1717;

							{	/* Globalize/integration.scm 44 */
								obj_t BgL_arg1314z00_1718;

								{	/* Globalize/integration.scm 44 */
									obj_t BgL_runner1323z00_1737;

									{	/* Globalize/integration.scm 44 */
										obj_t BgL_head1261z00_1723;

										BgL_head1261z00_1723 =
											MAKE_YOUNG_PAIR
											(BGl_traversezd2callzd2toz12z12zzglobaliza7e_integrationza7
											(CAR(BgL_fnsz00_1714)), BNIL);
										{	/* Globalize/integration.scm 44 */
											obj_t BgL_g1264z00_1724;

											BgL_g1264z00_1724 = CDR(BgL_fnsz00_1714);
											{
												obj_t BgL_l1259z00_1726;
												obj_t BgL_tail1262z00_1727;

												BgL_l1259z00_1726 = BgL_g1264z00_1724;
												BgL_tail1262z00_1727 = BgL_head1261z00_1723;
											BgL_zc3z04anonymousza31316ze3z87_1728:
												if (NULLP(BgL_l1259z00_1726))
													{	/* Globalize/integration.scm 44 */
														BgL_runner1323z00_1737 = BgL_head1261z00_1723;
													}
												else
													{	/* Globalize/integration.scm 44 */
														obj_t BgL_newtail1263z00_1730;

														{	/* Globalize/integration.scm 44 */
															obj_t BgL_arg1319z00_1732;

															{	/* Globalize/integration.scm 44 */
																obj_t BgL_arg1320z00_1733;

																BgL_arg1320z00_1733 =
																	CAR(((obj_t) BgL_l1259z00_1726));
																BgL_arg1319z00_1732 =
																	BGl_traversezd2callzd2toz12z12zzglobaliza7e_integrationza7
																	(BgL_arg1320z00_1733);
															}
															BgL_newtail1263z00_1730 =
																MAKE_YOUNG_PAIR(BgL_arg1319z00_1732, BNIL);
														}
														SET_CDR(BgL_tail1262z00_1727,
															BgL_newtail1263z00_1730);
														{	/* Globalize/integration.scm 44 */
															obj_t BgL_arg1318z00_1731;

															BgL_arg1318z00_1731 =
																CDR(((obj_t) BgL_l1259z00_1726));
															{
																obj_t BgL_tail1262z00_2466;
																obj_t BgL_l1259z00_2465;

																BgL_l1259z00_2465 = BgL_arg1318z00_1731;
																BgL_tail1262z00_2466 = BgL_newtail1263z00_1730;
																BgL_tail1262z00_1727 = BgL_tail1262z00_2466;
																BgL_l1259z00_1726 = BgL_l1259z00_2465;
																goto BgL_zc3z04anonymousza31316ze3z87_1728;
															}
														}
													}
											}
										}
									}
									BgL_arg1314z00_1718 =
										BGl_appendz00zz__r4_pairs_and_lists_6_3z00
										(BgL_runner1323z00_1737);
								}
								{	/* Globalize/integration.scm 43 */

									BgL_arg1312z00_1717 =
										BGl_deletezd2duplicatesz12zc0zz__r4_pairs_and_lists_6_3z00
										(BgL_arg1314z00_1718,
										BGl_equalzf3zd2envz21zz__r4_equivalence_6_2z00);
								}
							}
							{
								obj_t BgL_fnsz00_2469;

								BgL_fnsz00_2469 = BgL_arg1312z00_1717;
								BgL_fnsz00_1714 = BgL_fnsz00_2469;
								goto BgL_zc3z04anonymousza31309ze3z87_1715;
							}
						}
				}
			}
			{
				obj_t BgL_l1265z00_1740;

				BgL_l1265z00_1740 = BgL_g1z00_6;
			BgL_zc3z04anonymousza31324ze3z87_1741:
				if (PAIRP(BgL_l1265z00_1740))
					{	/* Globalize/integration.scm 46 */
						{	/* Globalize/integration.scm 47 */
							obj_t BgL_fz00_1743;

							BgL_fz00_1743 = CAR(BgL_l1265z00_1740);
							{	/* Globalize/integration.scm 47 */
								bool_t BgL_test1739z00_2473;

								{	/* Globalize/integration.scm 108 */
									bool_t BgL__ortest_1118z00_2019;

									{	/* Globalize/integration.scm 108 */
										obj_t BgL_classz00_2020;

										BgL_classz00_2020 = BGl_globalz00zzast_varz00;
										if (BGL_OBJECTP(BgL_fz00_1743))
											{	/* Globalize/integration.scm 108 */
												BgL_objectz00_bglt BgL_arg1807z00_2022;

												BgL_arg1807z00_2022 =
													(BgL_objectz00_bglt) (BgL_fz00_1743);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Globalize/integration.scm 108 */
														long BgL_idxz00_2028;

														BgL_idxz00_2028 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2022);
														BgL__ortest_1118z00_2019 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2028 + 2L)) == BgL_classz00_2020);
													}
												else
													{	/* Globalize/integration.scm 108 */
														bool_t BgL_res1712z00_2053;

														{	/* Globalize/integration.scm 108 */
															obj_t BgL_oclassz00_2036;

															{	/* Globalize/integration.scm 108 */
																obj_t BgL_arg1815z00_2044;
																long BgL_arg1816z00_2045;

																BgL_arg1815z00_2044 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Globalize/integration.scm 108 */
																	long BgL_arg1817z00_2046;

																	BgL_arg1817z00_2046 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2022);
																	BgL_arg1816z00_2045 =
																		(BgL_arg1817z00_2046 - OBJECT_TYPE);
																}
																BgL_oclassz00_2036 =
																	VECTOR_REF(BgL_arg1815z00_2044,
																	BgL_arg1816z00_2045);
															}
															{	/* Globalize/integration.scm 108 */
																bool_t BgL__ortest_1115z00_2037;

																BgL__ortest_1115z00_2037 =
																	(BgL_classz00_2020 == BgL_oclassz00_2036);
																if (BgL__ortest_1115z00_2037)
																	{	/* Globalize/integration.scm 108 */
																		BgL_res1712z00_2053 =
																			BgL__ortest_1115z00_2037;
																	}
																else
																	{	/* Globalize/integration.scm 108 */
																		long BgL_odepthz00_2038;

																		{	/* Globalize/integration.scm 108 */
																			obj_t BgL_arg1804z00_2039;

																			BgL_arg1804z00_2039 =
																				(BgL_oclassz00_2036);
																			BgL_odepthz00_2038 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2039);
																		}
																		if ((2L < BgL_odepthz00_2038))
																			{	/* Globalize/integration.scm 108 */
																				obj_t BgL_arg1802z00_2041;

																				{	/* Globalize/integration.scm 108 */
																					obj_t BgL_arg1803z00_2042;

																					BgL_arg1803z00_2042 =
																						(BgL_oclassz00_2036);
																					BgL_arg1802z00_2041 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2042, 2L);
																				}
																				BgL_res1712z00_2053 =
																					(BgL_arg1802z00_2041 ==
																					BgL_classz00_2020);
																			}
																		else
																			{	/* Globalize/integration.scm 108 */
																				BgL_res1712z00_2053 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL__ortest_1118z00_2019 = BgL_res1712z00_2053;
													}
											}
										else
											{	/* Globalize/integration.scm 108 */
												BgL__ortest_1118z00_2019 = ((bool_t) 0);
											}
									}
									if (BgL__ortest_1118z00_2019)
										{	/* Globalize/integration.scm 108 */
											BgL_test1739z00_2473 = BgL__ortest_1118z00_2019;
										}
									else
										{
											BgL_localzf2ginfozf2_bglt BgL_auxz00_2497;

											{
												obj_t BgL_auxz00_2498;

												{	/* Globalize/integration.scm 108 */
													BgL_objectz00_bglt BgL_tmpz00_2499;

													BgL_tmpz00_2499 =
														((BgL_objectz00_bglt)
														((BgL_localz00_bglt) BgL_fz00_1743));
													BgL_auxz00_2498 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2499);
												}
												BgL_auxz00_2497 =
													((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2498);
											}
											BgL_test1739z00_2473 =
												(((BgL_localzf2ginfozf2_bglt)
													COBJECT(BgL_auxz00_2497))->BgL_globaliza7edzf3z54);
										}
								}
								if (BgL_test1739z00_2473)
									{	/* Globalize/integration.scm 47 */
										BFALSE;
									}
								else
									{	/* Globalize/integration.scm 51 */
										BgL_sfunz00_bglt BgL_i1114z00_1746;

										BgL_i1114z00_1746 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_fz00_1743))))->
												BgL_valuez00));
										{
											obj_t BgL_auxz00_2515;
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2509;

											{	/* Globalize/integration.scm 52 */
												obj_t BgL_arg1327z00_1747;

												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2516;

													{
														obj_t BgL_auxz00_2517;

														{	/* Globalize/integration.scm 52 */
															BgL_objectz00_bglt BgL_tmpz00_2518;

															BgL_tmpz00_2518 =
																((BgL_objectz00_bglt) BgL_i1114z00_1746);
															BgL_auxz00_2517 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2518);
														}
														BgL_auxz00_2516 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2517);
													}
													BgL_arg1327z00_1747 =
														(((BgL_sfunzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2516))->BgL_imarkz00);
												}
												BgL_auxz00_2515 = CAR(((obj_t) BgL_arg1327z00_1747));
											}
											{
												obj_t BgL_auxz00_2510;

												{	/* Globalize/integration.scm 52 */
													BgL_objectz00_bglt BgL_tmpz00_2511;

													BgL_tmpz00_2511 =
														((BgL_objectz00_bglt) BgL_i1114z00_1746);
													BgL_auxz00_2510 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2511);
												}
												BgL_auxz00_2509 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2510);
											}
											((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2509))->
													BgL_integratorz00) =
												((obj_t) BgL_auxz00_2515), BUNSPEC);
										}
									}
							}
						}
						{
							obj_t BgL_l1265z00_2526;

							BgL_l1265z00_2526 = CDR(BgL_l1265z00_1740);
							BgL_l1265z00_1740 = BgL_l1265z00_2526;
							goto BgL_zc3z04anonymousza31324ze3z87_1741;
						}
					}
				else
					{	/* Globalize/integration.scm 46 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1267z00_1751;

				{	/* Globalize/integration.scm 55 */
					bool_t BgL_tmpz00_2528;

					BgL_l1267z00_1751 = BgL_g1z00_6;
				BgL_zc3z04anonymousza31329ze3z87_1752:
					if (PAIRP(BgL_l1267z00_1751))
						{	/* Globalize/integration.scm 55 */
							{	/* Globalize/integration.scm 56 */
								obj_t BgL_fz00_1754;

								BgL_fz00_1754 = CAR(BgL_l1267z00_1751);
								{	/* Globalize/integration.scm 56 */
									BgL_sfunz00_bglt BgL_i1115z00_1755;

									BgL_i1115z00_1755 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_fz00_1754))))->
											BgL_valuez00));
									{	/* Globalize/integration.scm 59 */
										bool_t BgL_test1748z00_2536;

										{	/* Globalize/integration.scm 108 */
											bool_t BgL__ortest_1118z00_2063;

											{	/* Globalize/integration.scm 108 */
												obj_t BgL_classz00_2064;

												BgL_classz00_2064 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_fz00_1754))
													{	/* Globalize/integration.scm 108 */
														BgL_objectz00_bglt BgL_arg1807z00_2066;

														BgL_arg1807z00_2066 =
															(BgL_objectz00_bglt) (BgL_fz00_1754);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Globalize/integration.scm 108 */
																long BgL_idxz00_2072;

																BgL_idxz00_2072 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_2066);
																BgL__ortest_1118z00_2063 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_2072 + 2L)) ==
																	BgL_classz00_2064);
															}
														else
															{	/* Globalize/integration.scm 108 */
																bool_t BgL_res1713z00_2097;

																{	/* Globalize/integration.scm 108 */
																	obj_t BgL_oclassz00_2080;

																	{	/* Globalize/integration.scm 108 */
																		obj_t BgL_arg1815z00_2088;
																		long BgL_arg1816z00_2089;

																		BgL_arg1815z00_2088 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Globalize/integration.scm 108 */
																			long BgL_arg1817z00_2090;

																			BgL_arg1817z00_2090 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_2066);
																			BgL_arg1816z00_2089 =
																				(BgL_arg1817z00_2090 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2080 =
																			VECTOR_REF(BgL_arg1815z00_2088,
																			BgL_arg1816z00_2089);
																	}
																	{	/* Globalize/integration.scm 108 */
																		bool_t BgL__ortest_1115z00_2081;

																		BgL__ortest_1115z00_2081 =
																			(BgL_classz00_2064 == BgL_oclassz00_2080);
																		if (BgL__ortest_1115z00_2081)
																			{	/* Globalize/integration.scm 108 */
																				BgL_res1713z00_2097 =
																					BgL__ortest_1115z00_2081;
																			}
																		else
																			{	/* Globalize/integration.scm 108 */
																				long BgL_odepthz00_2082;

																				{	/* Globalize/integration.scm 108 */
																					obj_t BgL_arg1804z00_2083;

																					BgL_arg1804z00_2083 =
																						(BgL_oclassz00_2080);
																					BgL_odepthz00_2082 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_2083);
																				}
																				if ((2L < BgL_odepthz00_2082))
																					{	/* Globalize/integration.scm 108 */
																						obj_t BgL_arg1802z00_2085;

																						{	/* Globalize/integration.scm 108 */
																							obj_t BgL_arg1803z00_2086;

																							BgL_arg1803z00_2086 =
																								(BgL_oclassz00_2080);
																							BgL_arg1802z00_2085 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_2086, 2L);
																						}
																						BgL_res1713z00_2097 =
																							(BgL_arg1802z00_2085 ==
																							BgL_classz00_2064);
																					}
																				else
																					{	/* Globalize/integration.scm 108 */
																						BgL_res1713z00_2097 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL__ortest_1118z00_2063 = BgL_res1713z00_2097;
															}
													}
												else
													{	/* Globalize/integration.scm 108 */
														BgL__ortest_1118z00_2063 = ((bool_t) 0);
													}
											}
											if (BgL__ortest_1118z00_2063)
												{	/* Globalize/integration.scm 108 */
													BgL_test1748z00_2536 = BgL__ortest_1118z00_2063;
												}
											else
												{
													BgL_localzf2ginfozf2_bglt BgL_auxz00_2560;

													{
														obj_t BgL_auxz00_2561;

														{	/* Globalize/integration.scm 108 */
															BgL_objectz00_bglt BgL_tmpz00_2562;

															BgL_tmpz00_2562 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_fz00_1754));
															BgL_auxz00_2561 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2562);
														}
														BgL_auxz00_2560 =
															((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2561);
													}
													BgL_test1748z00_2536 =
														(((BgL_localzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2560))->
														BgL_globaliza7edzf3z54);
												}
										}
										if (BgL_test1748z00_2536)
											{	/* Globalize/integration.scm 59 */
												{	/* Globalize/integration.scm 114 */
													BgL_valuez00_bglt BgL_arg1472z00_2100;

													BgL_arg1472z00_2100 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_fz00_1754))))->
														BgL_valuez00);
													{
														BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2571;

														{
															obj_t BgL_auxz00_2572;

															{	/* Globalize/integration.scm 114 */
																BgL_objectz00_bglt BgL_tmpz00_2573;

																BgL_tmpz00_2573 =
																	((BgL_objectz00_bglt)
																	((BgL_sfunz00_bglt) BgL_arg1472z00_2100));
																BgL_auxz00_2572 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2573);
															}
															BgL_auxz00_2571 =
																((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2572);
														}
														((((BgL_sfunzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_2571))->
																BgL_integratorz00) = ((obj_t) BFALSE), BUNSPEC);
													}
												}
											}
										else
											{	/* Globalize/integration.scm 62 */
												bool_t BgL_test1756z00_2579;

												{	/* Globalize/integration.scm 62 */
													obj_t BgL_arg1340z00_1761;

													{
														BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2580;

														{
															obj_t BgL_auxz00_2581;

															{	/* Globalize/integration.scm 62 */
																BgL_objectz00_bglt BgL_tmpz00_2582;

																BgL_tmpz00_2582 =
																	((BgL_objectz00_bglt) BgL_i1115z00_1755);
																BgL_auxz00_2581 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2582);
															}
															BgL_auxz00_2580 =
																((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2581);
														}
														BgL_arg1340z00_1761 =
															(((BgL_sfunzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_2580))->BgL_integratorz00);
													}
													{	/* Globalize/integration.scm 62 */
														obj_t BgL_classz00_2105;

														BgL_classz00_2105 = BGl_variablez00zzast_varz00;
														if (BGL_OBJECTP(BgL_arg1340z00_1761))
															{	/* Globalize/integration.scm 62 */
																BgL_objectz00_bglt BgL_arg1807z00_2107;

																BgL_arg1807z00_2107 =
																	(BgL_objectz00_bglt) (BgL_arg1340z00_1761);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Globalize/integration.scm 62 */
																		long BgL_idxz00_2113;

																		BgL_idxz00_2113 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_2107);
																		BgL_test1756z00_2579 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_2113 + 1L)) ==
																			BgL_classz00_2105);
																	}
																else
																	{	/* Globalize/integration.scm 62 */
																		bool_t BgL_res1714z00_2138;

																		{	/* Globalize/integration.scm 62 */
																			obj_t BgL_oclassz00_2121;

																			{	/* Globalize/integration.scm 62 */
																				obj_t BgL_arg1815z00_2129;
																				long BgL_arg1816z00_2130;

																				BgL_arg1815z00_2129 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Globalize/integration.scm 62 */
																					long BgL_arg1817z00_2131;

																					BgL_arg1817z00_2131 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_2107);
																					BgL_arg1816z00_2130 =
																						(BgL_arg1817z00_2131 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_2121 =
																					VECTOR_REF(BgL_arg1815z00_2129,
																					BgL_arg1816z00_2130);
																			}
																			{	/* Globalize/integration.scm 62 */
																				bool_t BgL__ortest_1115z00_2122;

																				BgL__ortest_1115z00_2122 =
																					(BgL_classz00_2105 ==
																					BgL_oclassz00_2121);
																				if (BgL__ortest_1115z00_2122)
																					{	/* Globalize/integration.scm 62 */
																						BgL_res1714z00_2138 =
																							BgL__ortest_1115z00_2122;
																					}
																				else
																					{	/* Globalize/integration.scm 62 */
																						long BgL_odepthz00_2123;

																						{	/* Globalize/integration.scm 62 */
																							obj_t BgL_arg1804z00_2124;

																							BgL_arg1804z00_2124 =
																								(BgL_oclassz00_2121);
																							BgL_odepthz00_2123 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_2124);
																						}
																						if ((1L < BgL_odepthz00_2123))
																							{	/* Globalize/integration.scm 62 */
																								obj_t BgL_arg1802z00_2126;

																								{	/* Globalize/integration.scm 62 */
																									obj_t BgL_arg1803z00_2127;

																									BgL_arg1803z00_2127 =
																										(BgL_oclassz00_2121);
																									BgL_arg1802z00_2126 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_2127, 1L);
																								}
																								BgL_res1714z00_2138 =
																									(BgL_arg1802z00_2126 ==
																									BgL_classz00_2105);
																							}
																						else
																							{	/* Globalize/integration.scm 62 */
																								BgL_res1714z00_2138 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1756z00_2579 = BgL_res1714z00_2138;
																	}
															}
														else
															{	/* Globalize/integration.scm 62 */
																BgL_test1756z00_2579 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test1756z00_2579)
													{	/* Globalize/integration.scm 62 */
														{	/* Globalize/integration.scm 66 */
															obj_t BgL_arg1335z00_1759;

															{
																BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2609;

																{
																	obj_t BgL_auxz00_2610;

																	{	/* Globalize/integration.scm 66 */
																		BgL_objectz00_bglt BgL_tmpz00_2611;

																		BgL_tmpz00_2611 =
																			((BgL_objectz00_bglt) BgL_i1115z00_1755);
																		BgL_auxz00_2610 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2611);
																	}
																	BgL_auxz00_2609 =
																		((BgL_sfunzf2ginfozf2_bglt)
																		BgL_auxz00_2610);
																}
																BgL_arg1335z00_1759 =
																	(((BgL_sfunzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2609))->
																	BgL_integratorz00);
															}
															BGl_integratezd2inz12zc0zzglobaliza7e_integrationza7
																(BgL_fz00_1754, BgL_arg1335z00_1759);
														}
													}
												else
													{	/* Globalize/integration.scm 62 */
														BGl_internalzd2errorzd2zztools_errorz00
															(BGl_string1724z00zzglobaliza7e_integrationza7,
															BGl_string1725z00zzglobaliza7e_integrationza7,
															BGl_shapez00zztools_shapez00(BgL_fz00_1754));
													}
											}
									}
								}
							}
							{
								obj_t BgL_l1267z00_2619;

								BgL_l1267z00_2619 = CDR(BgL_l1267z00_1751);
								BgL_l1267z00_1751 = BgL_l1267z00_2619;
								goto BgL_zc3z04anonymousza31329ze3z87_1752;
							}
						}
					else
						{	/* Globalize/integration.scm 55 */
							BgL_tmpz00_2528 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_2528);
				}
			}
		}

	}



/* &set-integration! */
	obj_t BGl_z62setzd2integrationz12za2zzglobaliza7e_integrationza7(obj_t
		BgL_envz00_2364, obj_t BgL_globalz00_2365, obj_t BgL_ez00_2366,
		obj_t BgL_g0z00_2367, obj_t BgL_g1z00_2368)
	{
		{	/* Globalize/integration.scm 31 */
			return
				BGl_setzd2integrationz12zc0zzglobaliza7e_integrationza7(
				((BgL_globalz00_bglt) BgL_globalz00_2365), BgL_ez00_2366,
				BgL_g0z00_2367, BgL_g1z00_2368);
		}

	}



/* traverse-call-to! */
	obj_t BGl_traversezd2callzd2toz12z12zzglobaliza7e_integrationza7(obj_t
		BgL_fz00_7)
	{
		{	/* Globalize/integration.scm 77 */
			{	/* Globalize/integration.scm 78 */
				obj_t BgL_integratorsz00_1764;

				{	/* Globalize/integration.scm 78 */
					bool_t BgL_test1761z00_2624;

					{	/* Globalize/integration.scm 108 */
						bool_t BgL__ortest_1118z00_2141;

						{	/* Globalize/integration.scm 108 */
							obj_t BgL_classz00_2142;

							BgL_classz00_2142 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_fz00_7))
								{	/* Globalize/integration.scm 108 */
									BgL_objectz00_bglt BgL_arg1807z00_2144;

									BgL_arg1807z00_2144 = (BgL_objectz00_bglt) (BgL_fz00_7);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Globalize/integration.scm 108 */
											long BgL_idxz00_2150;

											BgL_idxz00_2150 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2144);
											BgL__ortest_1118z00_2141 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2150 + 2L)) == BgL_classz00_2142);
										}
									else
										{	/* Globalize/integration.scm 108 */
											bool_t BgL_res1715z00_2175;

											{	/* Globalize/integration.scm 108 */
												obj_t BgL_oclassz00_2158;

												{	/* Globalize/integration.scm 108 */
													obj_t BgL_arg1815z00_2166;
													long BgL_arg1816z00_2167;

													BgL_arg1815z00_2166 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Globalize/integration.scm 108 */
														long BgL_arg1817z00_2168;

														BgL_arg1817z00_2168 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2144);
														BgL_arg1816z00_2167 =
															(BgL_arg1817z00_2168 - OBJECT_TYPE);
													}
													BgL_oclassz00_2158 =
														VECTOR_REF(BgL_arg1815z00_2166,
														BgL_arg1816z00_2167);
												}
												{	/* Globalize/integration.scm 108 */
													bool_t BgL__ortest_1115z00_2159;

													BgL__ortest_1115z00_2159 =
														(BgL_classz00_2142 == BgL_oclassz00_2158);
													if (BgL__ortest_1115z00_2159)
														{	/* Globalize/integration.scm 108 */
															BgL_res1715z00_2175 = BgL__ortest_1115z00_2159;
														}
													else
														{	/* Globalize/integration.scm 108 */
															long BgL_odepthz00_2160;

															{	/* Globalize/integration.scm 108 */
																obj_t BgL_arg1804z00_2161;

																BgL_arg1804z00_2161 = (BgL_oclassz00_2158);
																BgL_odepthz00_2160 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2161);
															}
															if ((2L < BgL_odepthz00_2160))
																{	/* Globalize/integration.scm 108 */
																	obj_t BgL_arg1802z00_2163;

																	{	/* Globalize/integration.scm 108 */
																		obj_t BgL_arg1803z00_2164;

																		BgL_arg1803z00_2164 = (BgL_oclassz00_2158);
																		BgL_arg1802z00_2163 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2164, 2L);
																	}
																	BgL_res1715z00_2175 =
																		(BgL_arg1802z00_2163 == BgL_classz00_2142);
																}
															else
																{	/* Globalize/integration.scm 108 */
																	BgL_res1715z00_2175 = ((bool_t) 0);
																}
														}
												}
											}
											BgL__ortest_1118z00_2141 = BgL_res1715z00_2175;
										}
								}
							else
								{	/* Globalize/integration.scm 108 */
									BgL__ortest_1118z00_2141 = ((bool_t) 0);
								}
						}
						if (BgL__ortest_1118z00_2141)
							{	/* Globalize/integration.scm 108 */
								BgL_test1761z00_2624 = BgL__ortest_1118z00_2141;
							}
						else
							{
								BgL_localzf2ginfozf2_bglt BgL_auxz00_2648;

								{
									obj_t BgL_auxz00_2649;

									{	/* Globalize/integration.scm 108 */
										BgL_objectz00_bglt BgL_tmpz00_2650;

										BgL_tmpz00_2650 =
											((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_fz00_7));
										BgL_auxz00_2649 = BGL_OBJECT_WIDENING(BgL_tmpz00_2650);
									}
									BgL_auxz00_2648 =
										((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2649);
								}
								BgL_test1761z00_2624 =
									(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2648))->
									BgL_globaliza7edzf3z54);
							}
					}
					if (BgL_test1761z00_2624)
						{	/* Globalize/integration.scm 79 */
							obj_t BgL_list1436z00_1805;

							BgL_list1436z00_1805 = MAKE_YOUNG_PAIR(BgL_fz00_7, BNIL);
							BgL_integratorsz00_1764 = BgL_list1436z00_1805;
						}
					else
						{	/* Globalize/integration.scm 80 */
							obj_t BgL_arg1437z00_1806;
							obj_t BgL_arg1448z00_1807;

							{	/* Globalize/integration.scm 80 */
								BgL_sfunz00_bglt BgL_oz00_2180;

								BgL_oz00_2180 =
									((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_fz00_7))))->BgL_valuez00));
								{
									BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2661;

									{
										obj_t BgL_auxz00_2662;

										{	/* Globalize/integration.scm 80 */
											BgL_objectz00_bglt BgL_tmpz00_2663;

											BgL_tmpz00_2663 = ((BgL_objectz00_bglt) BgL_oz00_2180);
											BgL_auxz00_2662 = BGL_OBJECT_WIDENING(BgL_tmpz00_2663);
										}
										BgL_auxz00_2661 =
											((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2662);
									}
									BgL_arg1437z00_1806 =
										(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2661))->
										BgL_imarkz00);
								}
							}
							{	/* Globalize/integration.scm 80 */
								obj_t BgL_list1454z00_1809;

								BgL_list1454z00_1809 = MAKE_YOUNG_PAIR(BgL_fz00_7, BNIL);
								BgL_arg1448z00_1807 = BgL_list1454z00_1809;
							}
							BgL_integratorsz00_1764 =
								BGl_appendzd221011zd2zzglobaliza7e_integrationza7
								(BgL_arg1437z00_1806, BgL_arg1448z00_1807);
						}
				}
				{	/* Globalize/integration.scm 81 */
					obj_t BgL_arg1346z00_1766;

					{	/* Globalize/integration.scm 98 */
						BgL_sfunz00_bglt BgL_oz00_2184;

						BgL_oz00_2184 =
							((BgL_sfunz00_bglt)
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_fz00_7)))->BgL_valuez00));
						{
							BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2673;

							{
								obj_t BgL_auxz00_2674;

								{	/* Globalize/integration.scm 98 */
									BgL_objectz00_bglt BgL_tmpz00_2675;

									BgL_tmpz00_2675 = ((BgL_objectz00_bglt) BgL_oz00_2184);
									BgL_auxz00_2674 = BGL_OBJECT_WIDENING(BgL_tmpz00_2675);
								}
								BgL_auxz00_2673 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2674);
							}
							BgL_arg1346z00_1766 =
								(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2673))->
								BgL_ctoz00);
						}
					}
					{	/* Globalize/integration.scm 83 */
						obj_t BgL_zc3z04anonymousza31348ze3z87_2372;

						BgL_zc3z04anonymousza31348ze3z87_2372 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31348ze3ze5zzglobaliza7e_integrationza7,
							(int) (1L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31348ze3z87_2372, (int) (0L),
							BgL_integratorsz00_1764);
						PROCEDURE_SET(BgL_zc3z04anonymousza31348ze3z87_2372, (int) (1L),
							BgL_fz00_7);
						{	/* Globalize/integration.scm 81 */
							obj_t BgL_list1347z00_1767;

							BgL_list1347z00_1767 = MAKE_YOUNG_PAIR(BgL_arg1346z00_1766, BNIL);
							return
								BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
								(BgL_zc3z04anonymousza31348ze3z87_2372, BgL_list1347z00_1767);
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1348> */
	obj_t BGl_z62zc3z04anonymousza31348ze3ze5zzglobaliza7e_integrationza7(obj_t
		BgL_envz00_2373, obj_t BgL_gz00_2376)
	{
		{	/* Globalize/integration.scm 81 */
			{	/* Globalize/integration.scm 83 */
				obj_t BgL_integratorsz00_2374;
				obj_t BgL_fz00_2375;

				BgL_integratorsz00_2374 = PROCEDURE_REF(BgL_envz00_2373, (int) (0L));
				BgL_fz00_2375 = PROCEDURE_REF(BgL_envz00_2373, (int) (1L));
				{	/* Globalize/integration.scm 83 */
					bool_t BgL_test1768z00_2693;

					{	/* Globalize/integration.scm 83 */
						bool_t BgL_test1769z00_2694;

						{	/* Globalize/integration.scm 108 */
							bool_t BgL__ortest_1118z00_2377;

							{	/* Globalize/integration.scm 108 */
								obj_t BgL_classz00_2378;

								BgL_classz00_2378 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_2376))
									{	/* Globalize/integration.scm 108 */
										BgL_objectz00_bglt BgL_arg1807z00_2379;

										BgL_arg1807z00_2379 = (BgL_objectz00_bglt) (BgL_gz00_2376);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Globalize/integration.scm 108 */
												long BgL_idxz00_2380;

												BgL_idxz00_2380 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2379);
												BgL__ortest_1118z00_2377 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2380 + 2L)) == BgL_classz00_2378);
											}
										else
											{	/* Globalize/integration.scm 108 */
												bool_t BgL_res1718z00_2383;

												{	/* Globalize/integration.scm 108 */
													obj_t BgL_oclassz00_2384;

													{	/* Globalize/integration.scm 108 */
														obj_t BgL_arg1815z00_2385;
														long BgL_arg1816z00_2386;

														BgL_arg1815z00_2385 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Globalize/integration.scm 108 */
															long BgL_arg1817z00_2387;

															BgL_arg1817z00_2387 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2379);
															BgL_arg1816z00_2386 =
																(BgL_arg1817z00_2387 - OBJECT_TYPE);
														}
														BgL_oclassz00_2384 =
															VECTOR_REF(BgL_arg1815z00_2385,
															BgL_arg1816z00_2386);
													}
													{	/* Globalize/integration.scm 108 */
														bool_t BgL__ortest_1115z00_2388;

														BgL__ortest_1115z00_2388 =
															(BgL_classz00_2378 == BgL_oclassz00_2384);
														if (BgL__ortest_1115z00_2388)
															{	/* Globalize/integration.scm 108 */
																BgL_res1718z00_2383 = BgL__ortest_1115z00_2388;
															}
														else
															{	/* Globalize/integration.scm 108 */
																long BgL_odepthz00_2389;

																{	/* Globalize/integration.scm 108 */
																	obj_t BgL_arg1804z00_2390;

																	BgL_arg1804z00_2390 = (BgL_oclassz00_2384);
																	BgL_odepthz00_2389 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2390);
																}
																if ((2L < BgL_odepthz00_2389))
																	{	/* Globalize/integration.scm 108 */
																		obj_t BgL_arg1802z00_2391;

																		{	/* Globalize/integration.scm 108 */
																			obj_t BgL_arg1803z00_2392;

																			BgL_arg1803z00_2392 =
																				(BgL_oclassz00_2384);
																			BgL_arg1802z00_2391 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2392, 2L);
																		}
																		BgL_res1718z00_2383 =
																			(BgL_arg1802z00_2391 ==
																			BgL_classz00_2378);
																	}
																else
																	{	/* Globalize/integration.scm 108 */
																		BgL_res1718z00_2383 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL__ortest_1118z00_2377 = BgL_res1718z00_2383;
											}
									}
								else
									{	/* Globalize/integration.scm 108 */
										BgL__ortest_1118z00_2377 = ((bool_t) 0);
									}
							}
							if (BgL__ortest_1118z00_2377)
								{	/* Globalize/integration.scm 108 */
									BgL_test1769z00_2694 = BgL__ortest_1118z00_2377;
								}
							else
								{
									BgL_localzf2ginfozf2_bglt BgL_auxz00_2718;

									{
										obj_t BgL_auxz00_2719;

										{	/* Globalize/integration.scm 108 */
											BgL_objectz00_bglt BgL_tmpz00_2720;

											BgL_tmpz00_2720 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt) BgL_gz00_2376));
											BgL_auxz00_2719 = BGL_OBJECT_WIDENING(BgL_tmpz00_2720);
										}
										BgL_auxz00_2718 =
											((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2719);
									}
									BgL_test1769z00_2694 =
										(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2718))->
										BgL_globaliza7edzf3z54);
								}
						}
						if (BgL_test1769z00_2694)
							{	/* Globalize/integration.scm 83 */
								BgL_test1768z00_2693 = ((bool_t) 1);
							}
						else
							{	/* Globalize/integration.scm 83 */
								BgL_test1768z00_2693 = (BgL_fz00_2375 == BgL_gz00_2376);
							}
					}
					if (BgL_test1768z00_2693)
						{	/* Globalize/integration.scm 83 */
							return BFALSE;
						}
					else
						{	/* Globalize/integration.scm 84 */
							BgL_sfunz00_bglt BgL_i1117z00_2393;

							BgL_i1117z00_2393 =
								((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_gz00_2376))))->BgL_valuez00));
							{	/* Globalize/integration.scm 85 */
								bool_t BgL_test1775z00_2731;

								{	/* Globalize/integration.scm 85 */
									obj_t BgL_arg1422z00_2394;

									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2732;

										{
											obj_t BgL_auxz00_2733;

											{	/* Globalize/integration.scm 85 */
												BgL_objectz00_bglt BgL_tmpz00_2734;

												BgL_tmpz00_2734 =
													((BgL_objectz00_bglt) BgL_i1117z00_2393);
												BgL_auxz00_2733 = BGL_OBJECT_WIDENING(BgL_tmpz00_2734);
											}
											BgL_auxz00_2732 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2733);
										}
										BgL_arg1422z00_2394 =
											(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2732))->
											BgL_imarkz00);
									}
									BgL_test1775z00_2731 = (BgL_arg1422z00_2394 == BNIL);
								}
								if (BgL_test1775z00_2731)
									{	/* Globalize/integration.scm 85 */
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2740;

											{
												obj_t BgL_auxz00_2741;

												{	/* Globalize/integration.scm 87 */
													BgL_objectz00_bglt BgL_tmpz00_2742;

													BgL_tmpz00_2742 =
														((BgL_objectz00_bglt) BgL_i1117z00_2393);
													BgL_auxz00_2741 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2742);
												}
												BgL_auxz00_2740 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2741);
											}
											((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2740))->
													BgL_imarkz00) =
												((obj_t) BgL_integratorsz00_2374), BUNSPEC);
										}
										return BgL_gz00_2376;
									}
								else
									{	/* Globalize/integration.scm 89 */
										obj_t BgL_nimarkz00_2395;

										{	/* Globalize/integration.scm 89 */
											obj_t BgL_hook1273z00_2396;

											BgL_hook1273z00_2396 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											{	/* Globalize/integration.scm 89 */
												obj_t BgL_g1274z00_2397;

												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2748;

													{
														obj_t BgL_auxz00_2749;

														{	/* Globalize/integration.scm 89 */
															BgL_objectz00_bglt BgL_tmpz00_2750;

															BgL_tmpz00_2750 =
																((BgL_objectz00_bglt) BgL_i1117z00_2393);
															BgL_auxz00_2749 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2750);
														}
														BgL_auxz00_2748 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2749);
													}
													BgL_g1274z00_2397 =
														(((BgL_sfunzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2748))->BgL_imarkz00);
												}
												{
													obj_t BgL_l1270z00_2399;
													obj_t BgL_h1271z00_2400;

													BgL_l1270z00_2399 = BgL_g1274z00_2397;
													BgL_h1271z00_2400 = BgL_hook1273z00_2396;
												BgL_zc3z04anonymousza31378ze3z87_2398:
													if (NULLP(BgL_l1270z00_2399))
														{	/* Globalize/integration.scm 89 */
															BgL_nimarkz00_2395 = CDR(BgL_hook1273z00_2396);
														}
													else
														{	/* Globalize/integration.scm 89 */
															if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																	(CAR(((obj_t) BgL_l1270z00_2399)),
																		BgL_integratorsz00_2374)))
																{	/* Globalize/integration.scm 89 */
																	obj_t BgL_nh1272z00_2401;

																	{	/* Globalize/integration.scm 89 */
																		obj_t BgL_arg1410z00_2402;

																		BgL_arg1410z00_2402 =
																			CAR(((obj_t) BgL_l1270z00_2399));
																		BgL_nh1272z00_2401 =
																			MAKE_YOUNG_PAIR(BgL_arg1410z00_2402,
																			BNIL);
																	}
																	SET_CDR(BgL_h1271z00_2400,
																		BgL_nh1272z00_2401);
																	{	/* Globalize/integration.scm 89 */
																		obj_t BgL_arg1408z00_2403;

																		BgL_arg1408z00_2403 =
																			CDR(((obj_t) BgL_l1270z00_2399));
																		{
																			obj_t BgL_h1271z00_2770;
																			obj_t BgL_l1270z00_2769;

																			BgL_l1270z00_2769 = BgL_arg1408z00_2403;
																			BgL_h1271z00_2770 = BgL_nh1272z00_2401;
																			BgL_h1271z00_2400 = BgL_h1271z00_2770;
																			BgL_l1270z00_2399 = BgL_l1270z00_2769;
																			goto
																				BgL_zc3z04anonymousza31378ze3z87_2398;
																		}
																	}
																}
															else
																{	/* Globalize/integration.scm 89 */
																	obj_t BgL_arg1421z00_2404;

																	BgL_arg1421z00_2404 =
																		CDR(((obj_t) BgL_l1270z00_2399));
																	{
																		obj_t BgL_l1270z00_2773;

																		BgL_l1270z00_2773 = BgL_arg1421z00_2404;
																		BgL_l1270z00_2399 = BgL_l1270z00_2773;
																		goto BgL_zc3z04anonymousza31378ze3z87_2398;
																	}
																}
														}
												}
											}
										}
										if (NULLP(BgL_nimarkz00_2395))
											{	/* Globalize/integration.scm 91 */
												{
													BgL_localzf2ginfozf2_bglt BgL_auxz00_2776;

													{
														obj_t BgL_auxz00_2777;

														{	/* Globalize/integration.scm 92 */
															BgL_objectz00_bglt BgL_tmpz00_2778;

															BgL_tmpz00_2778 =
																((BgL_objectz00_bglt)
																((BgL_localz00_bglt) BgL_gz00_2376));
															BgL_auxz00_2777 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2778);
														}
														BgL_auxz00_2776 =
															((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2777);
													}
													((((BgL_localzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_2776))->
															BgL_globaliza7edzf3z54) =
														((bool_t) ((bool_t) 1)), BUNSPEC);
												}
												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2784;

													{
														obj_t BgL_auxz00_2785;

														{	/* Globalize/integration.scm 93 */
															BgL_objectz00_bglt BgL_tmpz00_2786;

															BgL_tmpz00_2786 =
																((BgL_objectz00_bglt) BgL_i1117z00_2393);
															BgL_auxz00_2785 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2786);
														}
														BgL_auxz00_2784 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2785);
													}
													((((BgL_sfunzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_2784))->BgL_imarkz00) =
														((obj_t) BNIL), BUNSPEC);
												}
												return BgL_gz00_2376;
											}
										else
											{	/* Globalize/integration.scm 95 */
												bool_t BgL_test1780z00_2791;

												{	/* Globalize/integration.scm 95 */
													long BgL_arg1375z00_2405;
													long BgL_arg1376z00_2406;

													BgL_arg1375z00_2405 =
														bgl_list_length(BgL_nimarkz00_2395);
													{	/* Globalize/integration.scm 95 */
														obj_t BgL_arg1377z00_2407;

														{
															BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2793;

															{
																obj_t BgL_auxz00_2794;

																{	/* Globalize/integration.scm 95 */
																	BgL_objectz00_bglt BgL_tmpz00_2795;

																	BgL_tmpz00_2795 =
																		((BgL_objectz00_bglt) BgL_i1117z00_2393);
																	BgL_auxz00_2794 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2795);
																}
																BgL_auxz00_2793 =
																	((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2794);
															}
															BgL_arg1377z00_2407 =
																(((BgL_sfunzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_2793))->BgL_imarkz00);
														}
														BgL_arg1376z00_2406 =
															bgl_list_length(BgL_arg1377z00_2407);
													}
													BgL_test1780z00_2791 =
														(BgL_arg1375z00_2405 == BgL_arg1376z00_2406);
												}
												if (BgL_test1780z00_2791)
													{	/* Globalize/integration.scm 95 */
														return BFALSE;
													}
												else
													{	/* Globalize/integration.scm 95 */
														{
															BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2802;

															{
																obj_t BgL_auxz00_2803;

																{	/* Globalize/integration.scm 96 */
																	BgL_objectz00_bglt BgL_tmpz00_2804;

																	BgL_tmpz00_2804 =
																		((BgL_objectz00_bglt) BgL_i1117z00_2393);
																	BgL_auxz00_2803 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2804);
																}
																BgL_auxz00_2802 =
																	((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2803);
															}
															((((BgL_sfunzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2802))->BgL_imarkz00) =
																((obj_t) BgL_nimarkz00_2395), BUNSPEC);
														}
														return BgL_gz00_2376;
													}
											}
									}
							}
						}
				}
			}
		}

	}



/* integrate-in! */
	obj_t BGl_integratezd2inz12zc0zzglobaliza7e_integrationza7(obj_t BgL_fz00_10,
		obj_t BgL_integratorz00_11)
	{
		{	/* Globalize/integration.scm 119 */
			{	/* Globalize/integration.scm 121 */
				BgL_valuez00_bglt BgL_arg1473z00_1813;

				BgL_arg1473z00_1813 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_localz00_bglt) BgL_fz00_10))))->BgL_valuez00);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2812;

					{
						obj_t BgL_auxz00_2813;

						{	/* Globalize/integration.scm 121 */
							BgL_objectz00_bglt BgL_tmpz00_2814;

							BgL_tmpz00_2814 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_arg1473z00_1813));
							BgL_auxz00_2813 = BGL_OBJECT_WIDENING(BgL_tmpz00_2814);
						}
						BgL_auxz00_2812 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2813);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2812))->
							BgL_integratorz00) = ((obj_t) BgL_integratorz00_11), BUNSPEC);
				}
			}
			{	/* Globalize/integration.scm 122 */
				BgL_valuez00_bglt BgL_arg1485z00_1814;

				BgL_arg1485z00_1814 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_localz00_bglt) BgL_fz00_10))))->BgL_valuez00);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2823;

					{
						obj_t BgL_auxz00_2824;

						{	/* Globalize/integration.scm 122 */
							BgL_objectz00_bglt BgL_tmpz00_2825;

							BgL_tmpz00_2825 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_arg1485z00_1814));
							BgL_auxz00_2824 = BGL_OBJECT_WIDENING(BgL_tmpz00_2825);
						}
						BgL_auxz00_2823 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2824);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2823))->
							BgL_gzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
			}
			{	/* Globalize/integration.scm 123 */
				BgL_sfunz00_bglt BgL_i1120z00_1815;

				BgL_i1120z00_1815 =
					((BgL_sfunz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_integratorz00_11)))->BgL_valuez00));
				{
					obj_t BgL_auxz00_2840;
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2834;

					{	/* Globalize/integration.scm 124 */
						obj_t BgL_arg1489z00_1816;

						{
							BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2841;

							{
								obj_t BgL_auxz00_2842;

								{	/* Globalize/integration.scm 124 */
									BgL_objectz00_bglt BgL_tmpz00_2843;

									BgL_tmpz00_2843 = ((BgL_objectz00_bglt) BgL_i1120z00_1815);
									BgL_auxz00_2842 = BGL_OBJECT_WIDENING(BgL_tmpz00_2843);
								}
								BgL_auxz00_2841 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2842);
							}
							BgL_arg1489z00_1816 =
								(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2841))->
								BgL_integratedz00);
						}
						BgL_auxz00_2840 = MAKE_YOUNG_PAIR(BgL_fz00_10, BgL_arg1489z00_1816);
					}
					{
						obj_t BgL_auxz00_2835;

						{	/* Globalize/integration.scm 124 */
							BgL_objectz00_bglt BgL_tmpz00_2836;

							BgL_tmpz00_2836 = ((BgL_objectz00_bglt) BgL_i1120z00_1815);
							BgL_auxz00_2835 = BGL_OBJECT_WIDENING(BgL_tmpz00_2836);
						}
						BgL_auxz00_2834 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2835);
					}
					return
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2834))->
							BgL_integratedz00) = ((obj_t) BgL_auxz00_2840), BUNSPEC);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_integrationza7(void)
	{
		{	/* Globalize/integration.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_integrationza7(void)
	{
		{	/* Globalize/integration.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_integrationza7(void)
	{
		{	/* Globalize/integration.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_integrationza7(void)
	{
		{	/* Globalize/integration.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_globaliza7ez00(34590581L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_kaptureza7(459831036L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
			return
				BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1726z00zzglobaliza7e_integrationza7));
		}

	}

#ifdef __cplusplus
}
#endif
