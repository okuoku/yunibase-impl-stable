/*===========================================================================*/
/*   (Globalize/walk.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_WALK_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_GLOBALIZE_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_walkza7 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_walkza7(void);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_walkza7(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_foreignzd2closureszd2zzglobaliza7e_globalzd2closurez75(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzglobaliza7e_walkza7(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_walkza7(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t
		BGl_escapezd2funz12zc0zzglobaliza7e_escapeza7(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_walkza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globaliza7ez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_escapeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_walkza7(void);
	extern obj_t
		BGl_globaliza7ez12zb5zzglobaliza7e_globaliza7ez00(BgL_globalz00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_walkza7(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_walkza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_walkza7(void);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	BGL_EXPORTED_DECL obj_t
		BGl_globaliza7ezd2walkz12z67zzglobaliza7e_walkza7(obj_t, obj_t);
	static obj_t BGl_z62globaliza7ezd2walkz12z05zzglobaliza7e_walkza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1574z00zzglobaliza7e_walkza7,
		BgL_bgl_string1574za700za7za7g1585za7, "Closure (globalize)", 19);
	      DEFINE_STRING(BGl_string1575z00zzglobaliza7e_walkza7,
		BgL_bgl_string1575za700za7za7g1586za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1576z00zzglobaliza7e_walkza7,
		BgL_bgl_string1576za700za7za7g1587za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1577z00zzglobaliza7e_walkza7,
		BgL_bgl_string1577za700za7za7g1588za7, " error", 6);
	      DEFINE_STRING(BGl_string1578z00zzglobaliza7e_walkza7,
		BgL_bgl_string1578za700za7za7g1589za7, "s", 1);
	      DEFINE_STRING(BGl_string1579z00zzglobaliza7e_walkza7,
		BgL_bgl_string1579za700za7za7g1590za7, "", 0);
	      DEFINE_STRING(BGl_string1580z00zzglobaliza7e_walkza7,
		BgL_bgl_string1580za700za7za7g1591za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1581z00zzglobaliza7e_walkza7,
		BgL_bgl_string1581za700za7za7g1592za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1582z00zzglobaliza7e_walkza7,
		BgL_bgl_string1582za700za7za7g1593za7, "globalize_walk", 14);
	      DEFINE_STRING(BGl_string1583z00zzglobaliza7e_walkza7,
		BgL_bgl_string1583za700za7za7g1594za7, "pass-started ", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globaliza7ezd2walkz12zd2envzb5zzglobaliza7e_walkza7,
		BgL_bgl_za762globaliza7a7eza7d1595za7,
		BGl_z62globaliza7ezd2walkz12z05zzglobaliza7e_walkza7, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_walkza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_walkza7(long
		BgL_checksumz00_1614, char *BgL_fromz00_1615)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_walkza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_walkza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_walkza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_walkza7();
					BGl_cnstzd2initzd2zzglobaliza7e_walkza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_walkza7();
					return BGl_methodzd2initzd2zzglobaliza7e_walkza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_walkza7(void)
	{
		{	/* Globalize/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"globalize_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"globalize_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "globalize_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"globalize_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_walkza7(void)
	{
		{	/* Globalize/walk.scm 15 */
			{	/* Globalize/walk.scm 15 */
				obj_t BgL_cportz00_1603;

				{	/* Globalize/walk.scm 15 */
					obj_t BgL_stringz00_1610;

					BgL_stringz00_1610 = BGl_string1583z00zzglobaliza7e_walkza7;
					{	/* Globalize/walk.scm 15 */
						obj_t BgL_startz00_1611;

						BgL_startz00_1611 = BINT(0L);
						{	/* Globalize/walk.scm 15 */
							obj_t BgL_endz00_1612;

							BgL_endz00_1612 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1610)));
							{	/* Globalize/walk.scm 15 */

								BgL_cportz00_1603 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1610, BgL_startz00_1611, BgL_endz00_1612);
				}}}}
				{
					long BgL_iz00_1604;

					BgL_iz00_1604 = 0L;
				BgL_loopz00_1605:
					if ((BgL_iz00_1604 == -1L))
						{	/* Globalize/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/walk.scm 15 */
							{	/* Globalize/walk.scm 15 */
								obj_t BgL_arg1584z00_1606;

								{	/* Globalize/walk.scm 15 */

									{	/* Globalize/walk.scm 15 */
										obj_t BgL_locationz00_1608;

										BgL_locationz00_1608 = BBOOL(((bool_t) 0));
										{	/* Globalize/walk.scm 15 */

											BgL_arg1584z00_1606 =
												BGl_readz00zz__readerz00(BgL_cportz00_1603,
												BgL_locationz00_1608);
										}
									}
								}
								{	/* Globalize/walk.scm 15 */
									int BgL_tmpz00_1641;

									BgL_tmpz00_1641 = (int) (BgL_iz00_1604);
									CNST_TABLE_SET(BgL_tmpz00_1641, BgL_arg1584z00_1606);
							}}
							{	/* Globalize/walk.scm 15 */
								int BgL_auxz00_1609;

								BgL_auxz00_1609 = (int) ((BgL_iz00_1604 - 1L));
								{
									long BgL_iz00_1646;

									BgL_iz00_1646 = (long) (BgL_auxz00_1609);
									BgL_iz00_1604 = BgL_iz00_1646;
									goto BgL_loopz00_1605;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_walkza7(void)
	{
		{	/* Globalize/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzglobaliza7e_walkza7(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1346;

				BgL_headz00_1346 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1347;
					obj_t BgL_tailz00_1348;

					BgL_prevz00_1347 = BgL_headz00_1346;
					BgL_tailz00_1348 = BgL_l1z00_1;
				BgL_loopz00_1349:
					if (PAIRP(BgL_tailz00_1348))
						{
							obj_t BgL_newzd2prevzd2_1351;

							BgL_newzd2prevzd2_1351 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1348), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1347, BgL_newzd2prevzd2_1351);
							{
								obj_t BgL_tailz00_1656;
								obj_t BgL_prevz00_1655;

								BgL_prevz00_1655 = BgL_newzd2prevzd2_1351;
								BgL_tailz00_1656 = CDR(BgL_tailz00_1348);
								BgL_tailz00_1348 = BgL_tailz00_1656;
								BgL_prevz00_1347 = BgL_prevz00_1655;
								goto BgL_loopz00_1349;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1346);
				}
			}
		}

	}



/* globalize-walk! */
	BGL_EXPORTED_DEF obj_t BGl_globaliza7ezd2walkz12z67zzglobaliza7e_walkza7(obj_t
		BgL_globalsz00_3, obj_t BgL_removez00_4)
	{
		{	/* Globalize/walk.scm 30 */
			{	/* Globalize/walk.scm 31 */
				obj_t BgL_list1239z00_1354;

				{	/* Globalize/walk.scm 31 */
					obj_t BgL_arg1242z00_1355;

					{	/* Globalize/walk.scm 31 */
						obj_t BgL_arg1244z00_1356;

						BgL_arg1244z00_1356 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1242z00_1355 =
							MAKE_YOUNG_PAIR(BGl_string1574z00zzglobaliza7e_walkza7,
							BgL_arg1244z00_1356);
					}
					BgL_list1239z00_1354 =
						MAKE_YOUNG_PAIR(BGl_string1575z00zzglobaliza7e_walkza7,
						BgL_arg1242z00_1355);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1239z00_1354);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1574z00zzglobaliza7e_walkza7;
			{	/* Globalize/walk.scm 31 */
				obj_t BgL_g1110z00_1357;

				BgL_g1110z00_1357 = BNIL;
				{
					obj_t BgL_hooksz00_1360;
					obj_t BgL_hnamesz00_1361;

					BgL_hooksz00_1360 = BgL_g1110z00_1357;
					BgL_hnamesz00_1361 = BNIL;
				BgL_zc3z04anonymousza31245ze3z87_1362:
					if (NULLP(BgL_hooksz00_1360))
						{	/* Globalize/walk.scm 31 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Globalize/walk.scm 31 */
							bool_t BgL_test1601z00_1669;

							{	/* Globalize/walk.scm 31 */
								obj_t BgL_fun1269z00_1369;

								BgL_fun1269z00_1369 = CAR(((obj_t) BgL_hooksz00_1360));
								BgL_test1601z00_1669 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1269z00_1369));
							}
							if (BgL_test1601z00_1669)
								{	/* Globalize/walk.scm 31 */
									obj_t BgL_arg1249z00_1366;
									obj_t BgL_arg1252z00_1367;

									BgL_arg1249z00_1366 = CDR(((obj_t) BgL_hooksz00_1360));
									BgL_arg1252z00_1367 = CDR(((obj_t) BgL_hnamesz00_1361));
									{
										obj_t BgL_hnamesz00_1681;
										obj_t BgL_hooksz00_1680;

										BgL_hooksz00_1680 = BgL_arg1249z00_1366;
										BgL_hnamesz00_1681 = BgL_arg1252z00_1367;
										BgL_hnamesz00_1361 = BgL_hnamesz00_1681;
										BgL_hooksz00_1360 = BgL_hooksz00_1680;
										goto BgL_zc3z04anonymousza31245ze3z87_1362;
									}
								}
							else
								{	/* Globalize/walk.scm 31 */
									obj_t BgL_arg1268z00_1368;

									BgL_arg1268z00_1368 = CAR(((obj_t) BgL_hnamesz00_1361));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1574z00zzglobaliza7e_walkza7,
										BGl_string1576z00zzglobaliza7e_walkza7,
										BgL_arg1268z00_1368);
								}
						}
				}
			}
			{
				obj_t BgL_l1232z00_1373;

				BgL_l1232z00_1373 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31271ze3z87_1374:
				if (PAIRP(BgL_l1232z00_1373))
					{	/* Globalize/walk.scm 33 */
						{	/* Globalize/walk.scm 33 */
							obj_t BgL_arg1284z00_1376;

							BgL_arg1284z00_1376 = CAR(BgL_l1232z00_1373);
							BGl_escapezd2funz12zc0zzglobaliza7e_escapeza7(
								((BgL_variablez00_bglt) BgL_arg1284z00_1376));
						}
						{
							obj_t BgL_l1232z00_1690;

							BgL_l1232z00_1690 = CDR(BgL_l1232z00_1373);
							BgL_l1232z00_1373 = BgL_l1232z00_1690;
							goto BgL_zc3z04anonymousza31271ze3z87_1374;
						}
					}
				else
					{	/* Globalize/walk.scm 33 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_globalsz00_1381;
				obj_t BgL_newzd2globalszd2_1382;

				BgL_globalsz00_1381 = BgL_globalsz00_3;
				BgL_newzd2globalszd2_1382 = BNIL;
			BgL_zc3z04anonymousza31305ze3z87_1383:
				if (NULLP(BgL_globalsz00_1381))
					{	/* Globalize/walk.scm 40 */
						obj_t BgL_valuez00_1385;

						BgL_valuez00_1385 =
							BGl_removezd2varzd2zzast_removez00(BgL_removez00_4,
							BGl_appendzd221011zd2zzglobaliza7e_walkza7
							(BgL_newzd2globalszd2_1382,
								BGl_foreignzd2closureszd2zzglobaliza7e_globalzd2closurez75()));
						if (((long)
								CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
							{	/* Globalize/walk.scm 40 */
								{	/* Globalize/walk.scm 40 */
									obj_t BgL_port1234z00_1387;

									{	/* Globalize/walk.scm 40 */
										obj_t BgL_tmpz00_1700;

										BgL_tmpz00_1700 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_port1234z00_1387 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1700);
									}
									bgl_display_obj
										(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
										BgL_port1234z00_1387);
									bgl_display_string(BGl_string1577z00zzglobaliza7e_walkza7,
										BgL_port1234z00_1387);
									{	/* Globalize/walk.scm 40 */
										obj_t BgL_arg1310z00_1388;

										{	/* Globalize/walk.scm 40 */
											bool_t BgL_test1605z00_1705;

											if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
												{	/* Globalize/walk.scm 40 */
													if (INTEGERP
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
														{	/* Globalize/walk.scm 40 */
															BgL_test1605z00_1705 =
																(
																(long)
																CINT
																(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																> 1L);
														}
													else
														{	/* Globalize/walk.scm 40 */
															BgL_test1605z00_1705 =
																BGl_2ze3ze3zz__r4_numbers_6_5z00
																(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																BINT(1L));
														}
												}
											else
												{	/* Globalize/walk.scm 40 */
													BgL_test1605z00_1705 = ((bool_t) 0);
												}
											if (BgL_test1605z00_1705)
												{	/* Globalize/walk.scm 40 */
													BgL_arg1310z00_1388 =
														BGl_string1578z00zzglobaliza7e_walkza7;
												}
											else
												{	/* Globalize/walk.scm 40 */
													BgL_arg1310z00_1388 =
														BGl_string1579z00zzglobaliza7e_walkza7;
												}
										}
										bgl_display_obj(BgL_arg1310z00_1388, BgL_port1234z00_1387);
									}
									bgl_display_string(BGl_string1580z00zzglobaliza7e_walkza7,
										BgL_port1234z00_1387);
									bgl_display_char(((unsigned char) 10), BgL_port1234z00_1387);
								}
								{	/* Globalize/walk.scm 40 */
									obj_t BgL_list1313z00_1392;

									BgL_list1313z00_1392 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
									return BGl_exitz00zz__errorz00(BgL_list1313z00_1392);
								}
							}
						else
							{	/* Globalize/walk.scm 40 */
								obj_t BgL_g1113z00_1393;

								BgL_g1113z00_1393 = BNIL;
								{
									obj_t BgL_hooksz00_1396;
									obj_t BgL_hnamesz00_1397;

									BgL_hooksz00_1396 = BgL_g1113z00_1393;
									BgL_hnamesz00_1397 = BNIL;
								BgL_zc3z04anonymousza31314ze3z87_1398:
									if (NULLP(BgL_hooksz00_1396))
										{	/* Globalize/walk.scm 40 */
											return BgL_valuez00_1385;
										}
									else
										{	/* Globalize/walk.scm 40 */
											bool_t BgL_test1609z00_1722;

											{	/* Globalize/walk.scm 40 */
												obj_t BgL_fun1321z00_1405;

												BgL_fun1321z00_1405 = CAR(((obj_t) BgL_hooksz00_1396));
												BgL_test1609z00_1722 =
													CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1321z00_1405));
											}
											if (BgL_test1609z00_1722)
												{	/* Globalize/walk.scm 40 */
													obj_t BgL_arg1318z00_1402;
													obj_t BgL_arg1319z00_1403;

													BgL_arg1318z00_1402 =
														CDR(((obj_t) BgL_hooksz00_1396));
													BgL_arg1319z00_1403 =
														CDR(((obj_t) BgL_hnamesz00_1397));
													{
														obj_t BgL_hnamesz00_1734;
														obj_t BgL_hooksz00_1733;

														BgL_hooksz00_1733 = BgL_arg1318z00_1402;
														BgL_hnamesz00_1734 = BgL_arg1319z00_1403;
														BgL_hnamesz00_1397 = BgL_hnamesz00_1734;
														BgL_hooksz00_1396 = BgL_hooksz00_1733;
														goto BgL_zc3z04anonymousza31314ze3z87_1398;
													}
												}
											else
												{	/* Globalize/walk.scm 40 */
													obj_t BgL_arg1320z00_1404;

													BgL_arg1320z00_1404 =
														CAR(((obj_t) BgL_hnamesz00_1397));
													return
														BGl_internalzd2errorzd2zztools_errorz00
														(BGl_za2currentzd2passza2zd2zzengine_passz00,
														BGl_string1581z00zzglobaliza7e_walkza7,
														BgL_arg1320z00_1404);
												}
										}
								}
							}
					}
				else
					{	/* Globalize/walk.scm 42 */
						obj_t BgL_arg1326z00_1410;
						obj_t BgL_arg1327z00_1411;

						BgL_arg1326z00_1410 = CDR(((obj_t) BgL_globalsz00_1381));
						{	/* Globalize/walk.scm 43 */
							obj_t BgL_arg1328z00_1412;

							{	/* Globalize/walk.scm 43 */
								obj_t BgL_arg1329z00_1413;

								BgL_arg1329z00_1413 = CAR(((obj_t) BgL_globalsz00_1381));
								BgL_arg1328z00_1412 =
									BGl_globaliza7ez12zb5zzglobaliza7e_globaliza7ez00(
									((BgL_globalz00_bglt) BgL_arg1329z00_1413));
							}
							BgL_arg1327z00_1411 =
								BGl_appendzd221011zd2zzglobaliza7e_walkza7(BgL_arg1328z00_1412,
								BgL_newzd2globalszd2_1382);
						}
						{
							obj_t BgL_newzd2globalszd2_1746;
							obj_t BgL_globalsz00_1745;

							BgL_globalsz00_1745 = BgL_arg1326z00_1410;
							BgL_newzd2globalszd2_1746 = BgL_arg1327z00_1411;
							BgL_newzd2globalszd2_1382 = BgL_newzd2globalszd2_1746;
							BgL_globalsz00_1381 = BgL_globalsz00_1745;
							goto BgL_zc3z04anonymousza31305ze3z87_1383;
						}
					}
			}
		}

	}



/* &globalize-walk! */
	obj_t BGl_z62globaliza7ezd2walkz12z05zzglobaliza7e_walkza7(obj_t
		BgL_envz00_1600, obj_t BgL_globalsz00_1601, obj_t BgL_removez00_1602)
	{
		{	/* Globalize/walk.scm 30 */
			return
				BGl_globaliza7ezd2walkz12z67zzglobaliza7e_walkza7(BgL_globalsz00_1601,
				BgL_removez00_1602);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_walkza7(void)
	{
		{	/* Globalize/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_walkza7(void)
	{
		{	/* Globalize/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_walkza7(void)
	{
		{	/* Globalize/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_walkza7(void)
	{
		{	/* Globalize/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_escapeza7(460086900L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_globaliza7ez00(34590581L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75
				(62221965L, BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
			return BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1582z00zzglobaliza7e_walkza7));
		}

	}

#ifdef __cplusplus
}
#endif
