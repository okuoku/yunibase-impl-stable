/*===========================================================================*/
/*   (Globalize/kapture.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/kapture.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_KAPTURE_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_KAPTURE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_svarzf2ginfozf2_bgl
	{
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
		bool_t BgL_celledzf3zf3;
		bool_t BgL_stackablez00;
	}                      *BgL_svarzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_KAPTURE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_kaptureza7 =
		BUNSPEC;
	extern obj_t BGl_funz00zzast_varz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2kapturedz12zc0zzglobaliza7e_kaptureza7(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_kaptureza7(void);
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_kaptureza7(void);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_kaptureza7(void);
	extern obj_t
		BGl_ctozd2transitivezd2closurez12z12zzglobaliza7e_cloctoza7
		(BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_getzd2freezd2varsz00zzglobaliza7e_freeza7(BgL_nodez00_bglt,
		BgL_localz00_bglt);
	static long BGl_za2unionzd2roundza2zd2zzglobaliza7e_kaptureza7 = 0L;
	static obj_t BGl_appendzd221011zd2zzglobaliza7e_kaptureza7(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_kaptureza7(void);
	extern obj_t BGl_freezd2fromzd2zzglobaliza7e_freeza7(obj_t,
		BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_unionz00zzglobaliza7e_kaptureza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_kaptureza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_cloctoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62unionz62zzglobaliza7e_kaptureza7(obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_kaptureza7(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_kaptureza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_kaptureza7(void);
	static obj_t BGl_z62setzd2kapturedz12za2zzglobaliza7e_kaptureza7(obj_t,
		obj_t);
	static obj_t BGl_getzd2onezd2kapturedz00zzglobaliza7e_kaptureza7(obj_t,
		obj_t);
	static obj_t
		BGl_setzd2onezd2kapturedz12z12zzglobaliza7e_kaptureza7(BgL_localz00_bglt);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7,
		BgL_bgl_string1712za700za7za7g1713za7, "globalize_kapture", 17);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2kapturedz12zd2envz12zzglobaliza7e_kaptureza7,
		BgL_bgl_za762setza7d2kapture1714z00,
		BGl_z62setzd2kapturedz12za2zzglobaliza7e_kaptureza7, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unionzd2envzd2zzglobaliza7e_kaptureza7,
		BgL_bgl_za762unionza762za7za7glo1715z00,
		BGl_z62unionz62zzglobaliza7e_kaptureza7, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_kaptureza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_kaptureza7(long
		BgL_checksumz00_2158, char *BgL_fromz00_2159)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_kaptureza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_kaptureza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_kaptureza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_kaptureza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_kaptureza7();
					return BGl_toplevelzd2initzd2zzglobaliza7e_kaptureza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_kaptureza7(void)
	{
		{	/* Globalize/kapture.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_kapture");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_kapture");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_kapture");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_kapture");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_kapture");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_kapture");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_kaptureza7(void)
	{
		{	/* Globalize/kapture.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_kaptureza7(void)
	{
		{	/* Globalize/kapture.scm 15 */
			return (BGl_za2unionzd2roundza2zd2zzglobaliza7e_kaptureza7 = 0L, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzglobaliza7e_kaptureza7(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1692;

				BgL_headz00_1692 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1693;
					obj_t BgL_tailz00_1694;

					BgL_prevz00_1693 = BgL_headz00_1692;
					BgL_tailz00_1694 = BgL_l1z00_1;
				BgL_loopz00_1695:
					if (PAIRP(BgL_tailz00_1694))
						{
							obj_t BgL_newzd2prevzd2_1697;

							BgL_newzd2prevzd2_1697 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1694), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1693, BgL_newzd2prevzd2_1697);
							{
								obj_t BgL_tailz00_2181;
								obj_t BgL_prevz00_2180;

								BgL_prevz00_2180 = BgL_newzd2prevzd2_1697;
								BgL_tailz00_2181 = CDR(BgL_tailz00_1694);
								BgL_tailz00_1694 = BgL_tailz00_2181;
								BgL_prevz00_1693 = BgL_prevz00_2180;
								goto BgL_loopz00_1695;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1692);
				}
			}
		}

	}



/* set-kaptured! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2kapturedz12zc0zzglobaliza7e_kaptureza7(obj_t
		BgL_localfunza2za2_3)
	{
		{	/* Globalize/kapture.scm 36 */
			{
				obj_t BgL_l1256z00_1701;

				BgL_l1256z00_1701 = BgL_localfunza2za2_3;
			BgL_zc3z04anonymousza31273ze3z87_1702:
				if (PAIRP(BgL_l1256z00_1701))
					{	/* Globalize/kapture.scm 39 */
						{	/* Globalize/kapture.scm 40 */
							obj_t BgL_localfunz00_1704;

							BgL_localfunz00_1704 = CAR(BgL_l1256z00_1701);
							BGl_ctozd2transitivezd2closurez12z12zzglobaliza7e_cloctoza7(
								((BgL_localz00_bglt) BgL_localfunz00_1704));
						}
						{
							obj_t BgL_l1256z00_2189;

							BgL_l1256z00_2189 = CDR(BgL_l1256z00_1701);
							BgL_l1256z00_1701 = BgL_l1256z00_2189;
							goto BgL_zc3z04anonymousza31273ze3z87_1702;
						}
					}
				else
					{	/* Globalize/kapture.scm 39 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1258z00_1708;

				{	/* Globalize/kapture.scm 43 */
					bool_t BgL_tmpz00_2191;

					BgL_l1258z00_1708 = BgL_localfunza2za2_3;
				BgL_zc3z04anonymousza31285ze3z87_1709:
					if (PAIRP(BgL_l1258z00_1708))
						{	/* Globalize/kapture.scm 43 */
							{	/* Globalize/kapture.scm 43 */
								obj_t BgL_arg1304z00_1711;

								BgL_arg1304z00_1711 = CAR(BgL_l1258z00_1708);
								BGl_setzd2onezd2kapturedz12z12zzglobaliza7e_kaptureza7(
									((BgL_localz00_bglt) BgL_arg1304z00_1711));
							}
							{
								obj_t BgL_l1258z00_2197;

								BgL_l1258z00_2197 = CDR(BgL_l1258z00_1708);
								BgL_l1258z00_1708 = BgL_l1258z00_2197;
								goto BgL_zc3z04anonymousza31285ze3z87_1709;
							}
						}
					else
						{	/* Globalize/kapture.scm 43 */
							BgL_tmpz00_2191 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_2191);
				}
			}
		}

	}



/* &set-kaptured! */
	obj_t BGl_z62setzd2kapturedz12za2zzglobaliza7e_kaptureza7(obj_t
		BgL_envz00_2150, obj_t BgL_localfunza2za2_2151)
	{
		{	/* Globalize/kapture.scm 36 */
			return
				BGl_setzd2kapturedz12zc0zzglobaliza7e_kaptureza7
				(BgL_localfunza2za2_2151);
		}

	}



/* set-one-kaptured! */
	obj_t BGl_setzd2onezd2kapturedz12z12zzglobaliza7e_kaptureza7(BgL_localz00_bglt
		BgL_localfunz00_4)
	{
		{	/* Globalize/kapture.scm 51 */
			{	/* Globalize/kapture.scm 55 */
				obj_t BgL_kapturedz00_1714;
				BgL_valuez00_bglt BgL_infoz00_1715;
				bool_t BgL_stackablez00_1716;

				BgL_kapturedz00_1714 =
					BGl_getzd2onezd2kapturedz00zzglobaliza7e_kaptureza7(
					((obj_t) BgL_localfunz00_4), BNIL);
				BgL_infoz00_1715 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localfunz00_4)))->BgL_valuez00);
				BgL_stackablez00_1716 =
					(
					(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_localfunz00_4)))->
										BgL_valuez00))))->BgL_stackablez00) == BTRUE);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2210;

					{
						obj_t BgL_auxz00_2211;

						{	/* Globalize/kapture.scm 58 */
							BgL_objectz00_bglt BgL_tmpz00_2212;

							BgL_tmpz00_2212 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_infoz00_1715));
							BgL_auxz00_2211 = BGL_OBJECT_WIDENING(BgL_tmpz00_2212);
						}
						BgL_auxz00_2210 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2211);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2210))->
							BgL_kapturedz00) = ((obj_t) BgL_kapturedz00_1714), BUNSPEC);
				}
				{
					obj_t BgL_l1260z00_1718;

					BgL_l1260z00_1718 = BgL_kapturedz00_1714;
				BgL_zc3z04anonymousza31306ze3z87_1719:
					if (PAIRP(BgL_l1260z00_1718))
						{	/* Globalize/kapture.scm 59 */
							{	/* Globalize/kapture.scm 60 */
								obj_t BgL_localz00_1721;

								BgL_localz00_1721 = CAR(BgL_l1260z00_1718);
								{	/* Globalize/kapture.scm 60 */
									BgL_valuez00_bglt BgL_lvz00_1722;

									BgL_lvz00_1722 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_1721))))->
										BgL_valuez00);
									{	/* Globalize/kapture.scm 64 */
										bool_t BgL_arg1308z00_1723;

										if (BgL_stackablez00_1716)
											{	/* Globalize/kapture.scm 64 */
												bool_t BgL_test1724z00_2225;

												{
													BgL_svarzf2ginfozf2_bglt BgL_auxz00_2226;

													{
														obj_t BgL_auxz00_2227;

														{	/* Globalize/kapture.scm 64 */
															BgL_objectz00_bglt BgL_tmpz00_2228;

															BgL_tmpz00_2228 =
																((BgL_objectz00_bglt)
																((BgL_svarz00_bglt) BgL_lvz00_1722));
															BgL_auxz00_2227 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2228);
														}
														BgL_auxz00_2226 =
															((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2227);
													}
													BgL_test1724z00_2225 =
														(((BgL_svarzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2226))->BgL_kapturedzf3zf3);
												}
												if (BgL_test1724z00_2225)
													{	/* Globalize/kapture.scm 64 */
														BgL_arg1308z00_1723 = ((bool_t) 0);
													}
												else
													{	/* Globalize/kapture.scm 64 */
														BgL_arg1308z00_1723 = ((bool_t) 1);
													}
											}
										else
											{	/* Globalize/kapture.scm 64 */
												BgL_arg1308z00_1723 = ((bool_t) 0);
											}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_2234;

											{
												obj_t BgL_auxz00_2235;

												{	/* Globalize/kapture.scm 63 */
													BgL_objectz00_bglt BgL_tmpz00_2236;

													BgL_tmpz00_2236 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_lvz00_1722));
													BgL_auxz00_2235 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2236);
												}
												BgL_auxz00_2234 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2235);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2234))->
													BgL_stackablez00) =
												((bool_t) BgL_arg1308z00_1723), BUNSPEC);
										}
									}
									{
										BgL_svarzf2ginfozf2_bglt BgL_auxz00_2242;

										{
											obj_t BgL_auxz00_2243;

											{	/* Globalize/kapture.scm 65 */
												BgL_objectz00_bglt BgL_tmpz00_2244;

												BgL_tmpz00_2244 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_lvz00_1722));
												BgL_auxz00_2243 = BGL_OBJECT_WIDENING(BgL_tmpz00_2244);
											}
											BgL_auxz00_2242 =
												((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2243);
										}
										((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2242))->
												BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
									}
								}
							}
							{
								obj_t BgL_l1260z00_2250;

								BgL_l1260z00_2250 = CDR(BgL_l1260z00_1718);
								BgL_l1260z00_1718 = BgL_l1260z00_2250;
								goto BgL_zc3z04anonymousza31306ze3z87_1719;
							}
						}
					else
						{	/* Globalize/kapture.scm 59 */
							((bool_t) 1);
						}
				}
				return BgL_kapturedz00_1714;
			}
		}

	}



/* get-one-kaptured */
	obj_t BGl_getzd2onezd2kapturedz00zzglobaliza7e_kaptureza7(obj_t
		BgL_localz00_5, obj_t BgL_stackz00_6)
	{
		{	/* Globalize/kapture.scm 72 */
			{	/* Globalize/kapture.scm 78 */
				BgL_valuez00_bglt BgL_infoz00_1730;

				BgL_infoz00_1730 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_localz00_bglt) BgL_localz00_5))))->BgL_valuez00);
				{	/* Globalize/kapture.scm 78 */
					obj_t BgL_kapturedz00_1731;

					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2255;

						{
							obj_t BgL_auxz00_2256;

							{	/* Globalize/kapture.scm 79 */
								BgL_objectz00_bglt BgL_tmpz00_2257;

								BgL_tmpz00_2257 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_infoz00_1730));
								BgL_auxz00_2256 = BGL_OBJECT_WIDENING(BgL_tmpz00_2257);
							}
							BgL_auxz00_2255 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2256);
						}
						BgL_kapturedz00_1731 =
							(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2255))->
							BgL_kapturedz00);
					}
					{	/* Globalize/kapture.scm 79 */

						{	/* Globalize/kapture.scm 81 */
							bool_t BgL_test1725z00_2263;

							if (PAIRP(BgL_kapturedz00_1731))
								{	/* Globalize/kapture.scm 81 */
									BgL_test1725z00_2263 = ((bool_t) 1);
								}
							else
								{	/* Globalize/kapture.scm 81 */
									BgL_test1725z00_2263 = NULLP(BgL_kapturedz00_1731);
								}
							if (BgL_test1725z00_2263)
								{	/* Globalize/kapture.scm 81 */
									return BgL_kapturedz00_1731;
								}
							else
								{	/* Globalize/kapture.scm 81 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
											(BgL_localz00_5, BgL_stackz00_6)))
										{	/* Globalize/kapture.scm 87 */
											return BNIL;
										}
									else
										{	/* Globalize/kapture.scm 94 */
											obj_t BgL_g1115z00_1736;
											obj_t BgL_g1116z00_1737;

											{	/* Globalize/kapture.scm 95 */
												obj_t BgL_arg1332z00_1761;
												obj_t BgL_arg1333z00_1762;

												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2270;

													{
														obj_t BgL_auxz00_2271;

														{	/* Globalize/kapture.scm 95 */
															BgL_objectz00_bglt BgL_tmpz00_2272;

															BgL_tmpz00_2272 =
																((BgL_objectz00_bglt)
																((BgL_sfunz00_bglt) BgL_infoz00_1730));
															BgL_auxz00_2271 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2272);
														}
														BgL_auxz00_2270 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2271);
													}
													BgL_arg1332z00_1761 =
														(((BgL_sfunzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2270))->BgL_ctoza2za2);
												}
												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2278;

													{
														obj_t BgL_auxz00_2279;

														{	/* Globalize/kapture.scm 96 */
															BgL_objectz00_bglt BgL_tmpz00_2280;

															BgL_tmpz00_2280 =
																((BgL_objectz00_bglt)
																((BgL_sfunz00_bglt) BgL_infoz00_1730));
															BgL_auxz00_2279 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2280);
														}
														BgL_auxz00_2278 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2279);
													}
													BgL_arg1333z00_1762 =
														(((BgL_sfunzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2278))->BgL_efunctionsz00);
												}
												BgL_g1115z00_1736 =
													BGl_appendzd221011zd2zzglobaliza7e_kaptureza7
													(BgL_arg1332z00_1761, BgL_arg1333z00_1762);
											}
											BgL_g1116z00_1737 =
												MAKE_YOUNG_PAIR(BgL_localz00_5, BgL_stackz00_6);
											{
												obj_t BgL_kapturedz00_1739;
												obj_t BgL_ctoz00_1740;
												obj_t BgL_nstackz00_1741;

												BgL_kapturedz00_1739 = BNIL;
												BgL_ctoz00_1740 = BgL_g1115z00_1736;
												BgL_nstackz00_1741 = BgL_g1116z00_1737;
											BgL_zc3z04anonymousza31316ze3z87_1742:
												if (NULLP(BgL_ctoz00_1740))
													{	/* Globalize/kapture.scm 100 */
														obj_t BgL_newzd2bodyzd2_1744;

														{
															BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2290;

															{
																obj_t BgL_auxz00_2291;

																{	/* Globalize/kapture.scm 100 */
																	BgL_objectz00_bglt BgL_tmpz00_2292;

																	BgL_tmpz00_2292 =
																		((BgL_objectz00_bglt)
																		((BgL_sfunz00_bglt) BgL_infoz00_1730));
																	BgL_auxz00_2291 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2292);
																}
																BgL_auxz00_2290 =
																	((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2291);
															}
															BgL_newzd2bodyzd2_1744 =
																(((BgL_sfunzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_2290))->BgL_newzd2bodyzd2);
														}
														{	/* Globalize/kapture.scm 100 */
															obj_t BgL_freez00_1745;

															BgL_freez00_1745 =
																BGl_getzd2freezd2varsz00zzglobaliza7e_freeza7(
																((BgL_nodez00_bglt) BgL_newzd2bodyzd2_1744),
																((BgL_localz00_bglt) BgL_localz00_5));
															{	/* Globalize/kapture.scm 101 */
																obj_t BgL_fkapturedz00_1746;

																BgL_fkapturedz00_1746 =
																	BGl_freezd2fromzd2zzglobaliza7e_freeza7
																	(BgL_kapturedz00_1739,
																	((BgL_localz00_bglt) BgL_localz00_5));
																{	/* Globalize/kapture.scm 102 */

																	{	/* Globalize/kapture.scm 111 */
																		obj_t BgL_arg1318z00_1747;

																		BgL_arg1318z00_1747 =
																			MAKE_YOUNG_PAIR(BgL_freez00_1745,
																			BgL_fkapturedz00_1746);
																		return
																			BGl_unionz00zzglobaliza7e_kaptureza7
																			(BgL_arg1318z00_1747);
																	}
																}
															}
														}
													}
												else
													{	/* Globalize/kapture.scm 112 */
														bool_t BgL_test1729z00_2305;

														{	/* Globalize/kapture.scm 112 */
															BgL_sfunz00_bglt BgL_oz00_2034;

															BgL_oz00_2034 =
																((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					CAR(
																						((obj_t) BgL_ctoz00_1740))))))->
																	BgL_valuez00));
															{
																BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2312;

																{
																	obj_t BgL_auxz00_2313;

																	{	/* Globalize/kapture.scm 112 */
																		BgL_objectz00_bglt BgL_tmpz00_2314;

																		BgL_tmpz00_2314 =
																			((BgL_objectz00_bglt) BgL_oz00_2034);
																		BgL_auxz00_2313 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2314);
																	}
																	BgL_auxz00_2312 =
																		((BgL_sfunzf2ginfozf2_bglt)
																		BgL_auxz00_2313);
																}
																BgL_test1729z00_2305 =
																	(((BgL_sfunzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2312))->BgL_gzf3zf3);
															}
														}
														if (BgL_test1729z00_2305)
															{	/* Globalize/kapture.scm 113 */
																obj_t BgL_otherzd2kapturedzd2_1751;

																{	/* Globalize/kapture.scm 113 */
																	obj_t BgL_arg1327z00_1756;

																	BgL_arg1327z00_1756 =
																		CAR(((obj_t) BgL_ctoz00_1740));
																	BgL_otherzd2kapturedzd2_1751 =
																		BGl_getzd2onezd2kapturedz00zzglobaliza7e_kaptureza7
																		(BgL_arg1327z00_1756, BgL_nstackz00_1741);
																}
																{	/* Globalize/kapture.scm 114 */
																	obj_t BgL_arg1322z00_1752;
																	obj_t BgL_arg1323z00_1753;
																	obj_t BgL_arg1325z00_1754;

																	BgL_arg1322z00_1752 =
																		MAKE_YOUNG_PAIR
																		(BgL_otherzd2kapturedzd2_1751,
																		BgL_kapturedz00_1739);
																	BgL_arg1323z00_1753 =
																		CDR(((obj_t) BgL_ctoz00_1740));
																	{	/* Globalize/kapture.scm 116 */
																		obj_t BgL_arg1326z00_1755;

																		BgL_arg1326z00_1755 =
																			CAR(((obj_t) BgL_ctoz00_1740));
																		BgL_arg1325z00_1754 =
																			MAKE_YOUNG_PAIR(BgL_arg1326z00_1755,
																			BgL_nstackz00_1741);
																	}
																	{
																		obj_t BgL_nstackz00_2330;
																		obj_t BgL_ctoz00_2329;
																		obj_t BgL_kapturedz00_2328;

																		BgL_kapturedz00_2328 = BgL_arg1322z00_1752;
																		BgL_ctoz00_2329 = BgL_arg1323z00_1753;
																		BgL_nstackz00_2330 = BgL_arg1325z00_1754;
																		BgL_nstackz00_1741 = BgL_nstackz00_2330;
																		BgL_ctoz00_1740 = BgL_ctoz00_2329;
																		BgL_kapturedz00_1739 = BgL_kapturedz00_2328;
																		goto BgL_zc3z04anonymousza31316ze3z87_1742;
																	}
																}
															}
														else
															{	/* Globalize/kapture.scm 118 */
																obj_t BgL_arg1328z00_1757;

																BgL_arg1328z00_1757 =
																	CDR(((obj_t) BgL_ctoz00_1740));
																{
																	obj_t BgL_ctoz00_2333;

																	BgL_ctoz00_2333 = BgL_arg1328z00_1757;
																	BgL_ctoz00_1740 = BgL_ctoz00_2333;
																	goto BgL_zc3z04anonymousza31316ze3z87_1742;
																}
															}
													}
											}
										}
								}
						}
					}
				}
			}
		}

	}



/* union */
	BGL_EXPORTED_DEF obj_t BGl_unionz00zzglobaliza7e_kaptureza7(obj_t
		BgL_setsz00_7)
	{
		{	/* Globalize/kapture.scm 128 */
			BGl_za2unionzd2roundza2zd2zzglobaliza7e_kaptureza7 =
				(1L + BGl_za2unionzd2roundza2zd2zzglobaliza7e_kaptureza7);
			{
				obj_t BgL_setsz00_1766;
				obj_t BgL_unionz00_1767;

				BgL_setsz00_1766 = BgL_setsz00_7;
				BgL_unionz00_1767 = BNIL;
			BgL_zc3z04anonymousza31334ze3z87_1768:
				if (NULLP(BgL_setsz00_1766))
					{	/* Globalize/kapture.scm 132 */
						return BgL_unionz00_1767;
					}
				else
					{	/* Globalize/kapture.scm 134 */
						obj_t BgL_g1118z00_1770;

						BgL_g1118z00_1770 = CAR(((obj_t) BgL_setsz00_1766));
						{
							obj_t BgL_setz00_1772;
							obj_t BgL_unionz00_1773;

							BgL_setz00_1772 = BgL_g1118z00_1770;
							BgL_unionz00_1773 = BgL_unionz00_1767;
						BgL_zc3z04anonymousza31336ze3z87_1774:
							if (NULLP(BgL_setz00_1772))
								{	/* Globalize/kapture.scm 138 */
									obj_t BgL_arg1339z00_1776;

									BgL_arg1339z00_1776 = CDR(((obj_t) BgL_setsz00_1766));
									{
										obj_t BgL_unionz00_2344;
										obj_t BgL_setsz00_2343;

										BgL_setsz00_2343 = BgL_arg1339z00_1776;
										BgL_unionz00_2344 = BgL_unionz00_1773;
										BgL_unionz00_1767 = BgL_unionz00_2344;
										BgL_setsz00_1766 = BgL_setsz00_2343;
										goto BgL_zc3z04anonymousza31334ze3z87_1768;
									}
								}
							else
								{	/* Globalize/kapture.scm 140 */
									bool_t BgL_test1733z00_2345;

									{	/* Globalize/kapture.scm 140 */
										BgL_valuez00_bglt BgL_arg1472z00_1806;

										BgL_arg1472z00_1806 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt)
															CAR(((obj_t) BgL_setz00_1772))))))->BgL_valuez00);
										{	/* Globalize/kapture.scm 140 */
											obj_t BgL_classz00_2045;

											BgL_classz00_2045 = BGl_funz00zzast_varz00;
											{	/* Globalize/kapture.scm 140 */
												BgL_objectz00_bglt BgL_arg1807z00_2047;

												{	/* Globalize/kapture.scm 140 */
													obj_t BgL_tmpz00_2351;

													BgL_tmpz00_2351 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1472z00_1806));
													BgL_arg1807z00_2047 =
														(BgL_objectz00_bglt) (BgL_tmpz00_2351);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Globalize/kapture.scm 140 */
														long BgL_idxz00_2053;

														BgL_idxz00_2053 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2047);
														BgL_test1733z00_2345 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2053 + 2L)) == BgL_classz00_2045);
													}
												else
													{	/* Globalize/kapture.scm 140 */
														bool_t BgL_res1710z00_2078;

														{	/* Globalize/kapture.scm 140 */
															obj_t BgL_oclassz00_2061;

															{	/* Globalize/kapture.scm 140 */
																obj_t BgL_arg1815z00_2069;
																long BgL_arg1816z00_2070;

																BgL_arg1815z00_2069 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Globalize/kapture.scm 140 */
																	long BgL_arg1817z00_2071;

																	BgL_arg1817z00_2071 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2047);
																	BgL_arg1816z00_2070 =
																		(BgL_arg1817z00_2071 - OBJECT_TYPE);
																}
																BgL_oclassz00_2061 =
																	VECTOR_REF(BgL_arg1815z00_2069,
																	BgL_arg1816z00_2070);
															}
															{	/* Globalize/kapture.scm 140 */
																bool_t BgL__ortest_1115z00_2062;

																BgL__ortest_1115z00_2062 =
																	(BgL_classz00_2045 == BgL_oclassz00_2061);
																if (BgL__ortest_1115z00_2062)
																	{	/* Globalize/kapture.scm 140 */
																		BgL_res1710z00_2078 =
																			BgL__ortest_1115z00_2062;
																	}
																else
																	{	/* Globalize/kapture.scm 140 */
																		long BgL_odepthz00_2063;

																		{	/* Globalize/kapture.scm 140 */
																			obj_t BgL_arg1804z00_2064;

																			BgL_arg1804z00_2064 =
																				(BgL_oclassz00_2061);
																			BgL_odepthz00_2063 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2064);
																		}
																		if ((2L < BgL_odepthz00_2063))
																			{	/* Globalize/kapture.scm 140 */
																				obj_t BgL_arg1802z00_2066;

																				{	/* Globalize/kapture.scm 140 */
																					obj_t BgL_arg1803z00_2067;

																					BgL_arg1803z00_2067 =
																						(BgL_oclassz00_2061);
																					BgL_arg1802z00_2066 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2067, 2L);
																				}
																				BgL_res1710z00_2078 =
																					(BgL_arg1802z00_2066 ==
																					BgL_classz00_2045);
																			}
																		else
																			{	/* Globalize/kapture.scm 140 */
																				BgL_res1710z00_2078 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1733z00_2345 = BgL_res1710z00_2078;
													}
											}
										}
									}
									if (BgL_test1733z00_2345)
										{	/* Globalize/kapture.scm 141 */
											bool_t BgL_test1738z00_2374;

											{	/* Globalize/kapture.scm 141 */
												long BgL_arg1371z00_1790;

												{	/* Globalize/kapture.scm 141 */
													BgL_sfunz00_bglt BgL_oz00_2081;

													BgL_oz00_2081 =
														((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_setz00_1772))))))->
															BgL_valuez00));
													{
														BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2381;

														{
															obj_t BgL_auxz00_2382;

															{	/* Globalize/kapture.scm 141 */
																BgL_objectz00_bglt BgL_tmpz00_2383;

																BgL_tmpz00_2383 =
																	((BgL_objectz00_bglt) BgL_oz00_2081);
																BgL_auxz00_2382 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2383);
															}
															BgL_auxz00_2381 =
																((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2382);
														}
														BgL_arg1371z00_1790 =
															(((BgL_sfunzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_2381))->BgL_umarkz00);
												}}
												BgL_test1738z00_2374 =
													(BgL_arg1371z00_1790 ==
													BGl_za2unionzd2roundza2zd2zzglobaliza7e_kaptureza7);
											}
											if (BgL_test1738z00_2374)
												{	/* Globalize/kapture.scm 143 */
													obj_t BgL_arg1351z00_1784;

													BgL_arg1351z00_1784 = CDR(((obj_t) BgL_setz00_1772));
													{
														obj_t BgL_setz00_2391;

														BgL_setz00_2391 = BgL_arg1351z00_1784;
														BgL_setz00_1772 = BgL_setz00_2391;
														goto BgL_zc3z04anonymousza31336ze3z87_1774;
													}
												}
											else
												{	/* Globalize/kapture.scm 141 */
													{	/* Globalize/kapture.scm 145 */
														BgL_valuez00_bglt BgL_arg1352z00_1785;

														BgL_arg1352z00_1785 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_setz00_1772))))))->
															BgL_valuez00);
														{	/* Globalize/kapture.scm 145 */
															long BgL_vz00_2087;

															BgL_vz00_2087 =
																BGl_za2unionzd2roundza2zd2zzglobaliza7e_kaptureza7;
															{
																BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2397;

																{
																	obj_t BgL_auxz00_2398;

																	{	/* Globalize/kapture.scm 145 */
																		BgL_objectz00_bglt BgL_tmpz00_2399;

																		BgL_tmpz00_2399 =
																			((BgL_objectz00_bglt)
																			((BgL_sfunz00_bglt) BgL_arg1352z00_1785));
																		BgL_auxz00_2398 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2399);
																	}
																	BgL_auxz00_2397 =
																		((BgL_sfunzf2ginfozf2_bglt)
																		BgL_auxz00_2398);
																}
																((((BgL_sfunzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_2397))->BgL_umarkz00) =
																	((long) BgL_vz00_2087), BUNSPEC);
													}}}
													{	/* Globalize/kapture.scm 147 */
														obj_t BgL_arg1364z00_1787;
														obj_t BgL_arg1367z00_1788;

														BgL_arg1364z00_1787 =
															CDR(((obj_t) BgL_setz00_1772));
														{	/* Globalize/kapture.scm 148 */
															obj_t BgL_arg1370z00_1789;

															BgL_arg1370z00_1789 =
																CAR(((obj_t) BgL_setz00_1772));
															BgL_arg1367z00_1788 =
																MAKE_YOUNG_PAIR(BgL_arg1370z00_1789,
																BgL_unionz00_1773);
														}
														{
															obj_t BgL_unionz00_2411;
															obj_t BgL_setz00_2410;

															BgL_setz00_2410 = BgL_arg1364z00_1787;
															BgL_unionz00_2411 = BgL_arg1367z00_1788;
															BgL_unionz00_1773 = BgL_unionz00_2411;
															BgL_setz00_1772 = BgL_setz00_2410;
															goto BgL_zc3z04anonymousza31336ze3z87_1774;
														}
													}
												}
										}
									else
										{	/* Globalize/kapture.scm 149 */
											bool_t BgL_test1739z00_2412;

											{	/* Globalize/kapture.scm 149 */
												long BgL_arg1448z00_1803;

												{	/* Globalize/kapture.scm 149 */
													BgL_svarz00_bglt BgL_oz00_2093;

													BgL_oz00_2093 =
														((BgL_svarz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_setz00_1772))))))->
															BgL_valuez00));
													{
														BgL_svarzf2ginfozf2_bglt BgL_auxz00_2419;

														{
															obj_t BgL_auxz00_2420;

															{	/* Globalize/kapture.scm 149 */
																BgL_objectz00_bglt BgL_tmpz00_2421;

																BgL_tmpz00_2421 =
																	((BgL_objectz00_bglt) BgL_oz00_2093);
																BgL_auxz00_2420 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2421);
															}
															BgL_auxz00_2419 =
																((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_2420);
														}
														BgL_arg1448z00_1803 =
															(((BgL_svarzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_2419))->BgL_markz00);
												}}
												BgL_test1739z00_2412 =
													(BgL_arg1448z00_1803 ==
													BGl_za2unionzd2roundza2zd2zzglobaliza7e_kaptureza7);
											}
											if (BgL_test1739z00_2412)
												{	/* Globalize/kapture.scm 150 */
													obj_t BgL_arg1408z00_1797;

													BgL_arg1408z00_1797 = CDR(((obj_t) BgL_setz00_1772));
													{
														obj_t BgL_setz00_2429;

														BgL_setz00_2429 = BgL_arg1408z00_1797;
														BgL_setz00_1772 = BgL_setz00_2429;
														goto BgL_zc3z04anonymousza31336ze3z87_1774;
													}
												}
											else
												{	/* Globalize/kapture.scm 149 */
													{	/* Globalize/kapture.scm 152 */
														BgL_valuez00_bglt BgL_arg1410z00_1798;

														BgL_arg1410z00_1798 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_setz00_1772))))))->
															BgL_valuez00);
														{	/* Globalize/kapture.scm 152 */
															long BgL_vz00_2099;

															BgL_vz00_2099 =
																BGl_za2unionzd2roundza2zd2zzglobaliza7e_kaptureza7;
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_2435;

																{
																	obj_t BgL_auxz00_2436;

																	{	/* Globalize/kapture.scm 152 */
																		BgL_objectz00_bglt BgL_tmpz00_2437;

																		BgL_tmpz00_2437 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_arg1410z00_1798));
																		BgL_auxz00_2436 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2437);
																	}
																	BgL_auxz00_2435 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_2436);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_2435))->BgL_markz00) =
																	((long) BgL_vz00_2099), BUNSPEC);
													}}}
													{	/* Globalize/kapture.scm 153 */
														obj_t BgL_arg1422z00_1800;
														obj_t BgL_arg1434z00_1801;

														BgL_arg1422z00_1800 =
															CDR(((obj_t) BgL_setz00_1772));
														{	/* Globalize/kapture.scm 154 */
															obj_t BgL_arg1437z00_1802;

															BgL_arg1437z00_1802 =
																CAR(((obj_t) BgL_setz00_1772));
															BgL_arg1434z00_1801 =
																MAKE_YOUNG_PAIR(BgL_arg1437z00_1802,
																BgL_unionz00_1773);
														}
														{
															obj_t BgL_unionz00_2449;
															obj_t BgL_setz00_2448;

															BgL_setz00_2448 = BgL_arg1422z00_1800;
															BgL_unionz00_2449 = BgL_arg1434z00_1801;
															BgL_unionz00_1773 = BgL_unionz00_2449;
															BgL_setz00_1772 = BgL_setz00_2448;
															goto BgL_zc3z04anonymousza31336ze3z87_1774;
														}
													}
												}
										}
								}
						}
					}
			}
		}

	}



/* &union */
	obj_t BGl_z62unionz62zzglobaliza7e_kaptureza7(obj_t BgL_envz00_2152,
		obj_t BgL_setsz00_2153)
	{
		{	/* Globalize/kapture.scm 128 */
			return BGl_unionz00zzglobaliza7e_kaptureza7(BgL_setsz00_2153);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_kaptureza7(void)
	{
		{	/* Globalize/kapture.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_kaptureza7(void)
	{
		{	/* Globalize/kapture.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_kaptureza7(void)
	{
		{	/* Globalize/kapture.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_kaptureza7(void)
	{
		{	/* Globalize/kapture.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(2706117L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(244215788L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
			return
				BGl_modulezd2initializa7ationz75zzglobaliza7e_cloctoza7(254520481L,
				BSTRING_TO_STRING(BGl_string1712z00zzglobaliza7e_kaptureza7));
		}

	}

#ifdef __cplusplus
}
#endif
