/*===========================================================================*/
/*   (Globalize/gn.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/gn.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_GN_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_GN_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_GN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_gnza7 = BUNSPEC;
	static obj_t BGl_z62Ezd2cast1289zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62Ezd2letzd2var1301z62zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62Ezd2extern1287zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62Ezd2kwote1271zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62Ezd2switch1297zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static obj_t BGl_z62Ezd2setzd2exzd2it1303zb0zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62Ezd2boxzd2ref1309z62zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62Ezd2conditional1293zb0zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62Ezd2setq1291zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62Ezd2sync1279zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_gnza7(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62Ezd2fail1295zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_gnza7(void);
	static obj_t BGl_z62Ezd2makezd2box1307z62zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_gnza7(void);
	static obj_t BGl_z62Ezd2boxzd2setz121311z70zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	static obj_t BGl_z62Ezd2var1273zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62Ezd2funcall1285zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_savezd2funz12zc0zzglobaliza7e_gnza7(BgL_variablez00_bglt,
		BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzglobaliza7e_gnza7(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_gnza7(void);
	static obj_t BGl_z62Ezd2closure1275zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62Ezd2app1281zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62Ezd2jumpzd2exzd2it1305zb0zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62E1266z62zzglobaliza7e_gnza7(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_Ez00zzglobaliza7e_gnza7(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62Gnz12z70zzglobaliza7e_gnza7(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_savezd2appz12zc0zzglobaliza7e_gnza7(obj_t,
		BgL_variablez00_bglt);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_gnza7(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globaliza7ez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62Ezd2atom1269zb0zzglobaliza7e_gnza7(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_Gzd2fromzd2ctoz00zzglobaliza7e_gnza7(obj_t);
	extern obj_t BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_gnza7(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_gnza7(void);
	static obj_t BGl_z62Ezd2sequence1277zb0zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_gnza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_gnza7(void);
	BGL_EXPORTED_DECL obj_t BGl_Gnz12z12zzglobaliza7e_gnza7(obj_t,
		BgL_nodez00_bglt, BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_Eza2za2zzglobaliza7e_gnza7(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62Ez62zzglobaliza7e_gnza7(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62Ezd2letzd2fun1299z62zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62Ezd2appzd2ly1283z62zzglobaliza7e_gnza7(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_za2G0za2z00zzglobaliza7e_globaliza7ez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_Ezd2envzd2zzglobaliza7e_gnza7,
		BgL_bgl_za762eza762za7za7globali1888z00, BGl_z62Ez62zzglobaliza7e_gnza7, 0L,
		BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1859z00zzglobaliza7e_gnza7,
		BgL_bgl_string1859za700za7za7g1889za7, "E1266", 5);
	      DEFINE_STRING(BGl_string1860z00zzglobaliza7e_gnza7,
		BgL_bgl_string1860za700za7za7g1890za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1862z00zzglobaliza7e_gnza7,
		BgL_bgl_string1862za700za7za7g1891za7, "E", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1858z00zzglobaliza7e_gnza7,
		BgL_bgl_za762e1266za762za7za7glo1892z00, BGl_z62E1266z62zzglobaliza7e_gnza7,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1861z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2atom1269za71893za7,
		BGl_z62Ezd2atom1269zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1863z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2kwote12711894z00,
		BGl_z62Ezd2kwote1271zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1864z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2var1273za7b1895za7,
		BGl_z62Ezd2var1273zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1865z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2closure121896z00,
		BGl_z62Ezd2closure1275zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1866z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2sequence11897z00,
		BGl_z62Ezd2sequence1277zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1867z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2sync1279za71898za7,
		BGl_z62Ezd2sync1279zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1868z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2app1281za7b1899za7,
		BGl_z62Ezd2app1281zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1869z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2appza7d2ly11900za7,
		BGl_z62Ezd2appzd2ly1283z62zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_Gnz12zd2envzc0zzglobaliza7e_gnza7,
		BgL_bgl_za762gnza712za770za7za7glo1901za7,
		BGl_z62Gnz12z70zzglobaliza7e_gnza7, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1870z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2funcall121902z00,
		BGl_z62Ezd2funcall1285zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2extern1281903z00,
		BGl_z62Ezd2extern1287zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1872z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2cast1289za71904za7,
		BGl_z62Ezd2cast1289zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2setq1291za71905za7,
		BGl_z62Ezd2setq1291zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2condition1906z00,
		BGl_z62Ezd2conditional1293zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2fail1295za71907za7,
		BGl_z62Ezd2fail1295zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2switch1291908z00,
		BGl_z62Ezd2switch1297zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2letza7d2fun1909za7,
		BGl_z62Ezd2letzd2fun1299z62zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1884z00zzglobaliza7e_gnza7,
		BgL_bgl_string1884za700za7za7g1910za7, "globalize_gn", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2letza7d2var1911za7,
		BGl_z62Ezd2letzd2var1301z62zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1885z00zzglobaliza7e_gnza7,
		BgL_bgl_string1885za700za7za7g1912za7, "E1266 done ", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2setza7d2exza71913z00,
		BGl_z62Ezd2setzd2exzd2it1303zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2jumpza7d2ex1914za7,
		BGl_z62Ezd2jumpzd2exzd2it1305zb0zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2makeza7d2bo1915za7,
		BGl_z62Ezd2makezd2box1307z62zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2boxza7d2ref1916za7,
		BGl_z62Ezd2boxzd2ref1309z62zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzglobaliza7e_gnza7,
		BgL_bgl_za762eza7d2boxza7d2set1917za7,
		BGl_z62Ezd2boxzd2setz121311z70zzglobaliza7e_gnza7, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_gnza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_gnza7(long
		BgL_checksumz00_2854, char *BgL_fromz00_2855)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_gnza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_gnza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_gnza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_gnza7();
					BGl_cnstzd2initzd2zzglobaliza7e_gnza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_gnza7();
					BGl_genericzd2initzd2zzglobaliza7e_gnza7();
					BGl_methodzd2initzd2zzglobaliza7e_gnza7();
					return BGl_toplevelzd2initzd2zzglobaliza7e_gnza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_gnza7(void)
	{
		{	/* Globalize/gn.scm 23 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_gn");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_gn");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_gn");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_gn");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "globalize_gn");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "globalize_gn");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "globalize_gn");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_gn");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "globalize_gn");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_gn");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_gnza7(void)
	{
		{	/* Globalize/gn.scm 23 */
			{	/* Globalize/gn.scm 23 */
				obj_t BgL_cportz00_2740;

				{	/* Globalize/gn.scm 23 */
					obj_t BgL_stringz00_2747;

					BgL_stringz00_2747 = BGl_string1885z00zzglobaliza7e_gnza7;
					{	/* Globalize/gn.scm 23 */
						obj_t BgL_startz00_2748;

						BgL_startz00_2748 = BINT(0L);
						{	/* Globalize/gn.scm 23 */
							obj_t BgL_endz00_2749;

							BgL_endz00_2749 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2747)));
							{	/* Globalize/gn.scm 23 */

								BgL_cportz00_2740 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2747, BgL_startz00_2748, BgL_endz00_2749);
				}}}}
				{
					long BgL_iz00_2741;

					BgL_iz00_2741 = 1L;
				BgL_loopz00_2742:
					if ((BgL_iz00_2741 == -1L))
						{	/* Globalize/gn.scm 23 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/gn.scm 23 */
							{	/* Globalize/gn.scm 23 */
								obj_t BgL_arg1887z00_2743;

								{	/* Globalize/gn.scm 23 */

									{	/* Globalize/gn.scm 23 */
										obj_t BgL_locationz00_2745;

										BgL_locationz00_2745 = BBOOL(((bool_t) 0));
										{	/* Globalize/gn.scm 23 */

											BgL_arg1887z00_2743 =
												BGl_readz00zz__readerz00(BgL_cportz00_2740,
												BgL_locationz00_2745);
										}
									}
								}
								{	/* Globalize/gn.scm 23 */
									int BgL_tmpz00_2885;

									BgL_tmpz00_2885 = (int) (BgL_iz00_2741);
									CNST_TABLE_SET(BgL_tmpz00_2885, BgL_arg1887z00_2743);
							}}
							{	/* Globalize/gn.scm 23 */
								int BgL_auxz00_2746;

								BgL_auxz00_2746 = (int) ((BgL_iz00_2741 - 1L));
								{
									long BgL_iz00_2890;

									BgL_iz00_2890 = (long) (BgL_auxz00_2746);
									BgL_iz00_2741 = BgL_iz00_2890;
									goto BgL_loopz00_2742;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_gnza7(void)
	{
		{	/* Globalize/gn.scm 23 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_gnza7(void)
	{
		{	/* Globalize/gn.scm 23 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzglobaliza7e_gnza7(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1768;

				BgL_headz00_1768 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1769;
					obj_t BgL_tailz00_1770;

					BgL_prevz00_1769 = BgL_headz00_1768;
					BgL_tailz00_1770 = BgL_l1z00_1;
				BgL_loopz00_1771:
					if (PAIRP(BgL_tailz00_1770))
						{
							obj_t BgL_newzd2prevzd2_1773;

							BgL_newzd2prevzd2_1773 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1770), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1769, BgL_newzd2prevzd2_1773);
							{
								obj_t BgL_tailz00_2900;
								obj_t BgL_prevz00_2899;

								BgL_prevz00_2899 = BgL_newzd2prevzd2_1773;
								BgL_tailz00_2900 = CDR(BgL_tailz00_1770);
								BgL_tailz00_1770 = BgL_tailz00_2900;
								BgL_prevz00_1769 = BgL_prevz00_2899;
								goto BgL_loopz00_1771;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1768);
				}
			}
		}

	}



/* Gn! */
	BGL_EXPORTED_DEF obj_t BGl_Gnz12z12zzglobaliza7e_gnza7(obj_t BgL_argsz00_3,
		BgL_nodez00_bglt BgL_nodez00_4, BgL_variablez00_bglt BgL_callerz00_5,
		obj_t BgL_gz00_6)
	{
		{	/* Globalize/gn.scm 41 */
			BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00 =
				BGl_Ez00zzglobaliza7e_gnza7(BgL_nodez00_4, BgL_callerz00_5, BgL_gz00_6);
			{
				obj_t BgL_gz00_1778;
				obj_t BgL_g1z00_1779;

				BgL_gz00_1778 = BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00;
				BgL_g1z00_1779 = BNIL;
			BgL_zc3z04anonymousza31316ze3z87_1780:
				if (NULLP(BgL_gz00_1778))
					{	/* Globalize/gn.scm 45 */
						BGl_za2G0za2z00zzglobaliza7e_globaliza7ez00 =
							BGl_appendzd221011zd2zzglobaliza7e_gnza7
							(BGl_za2Eza2z00zzglobaliza7e_globaliza7ez00, BgL_g1z00_1779);
						return (BGl_za2G1za2z00zzglobaliza7e_globaliza7ez00 =
							BgL_g1z00_1779, BUNSPEC);
					}
				else
					{	/* Globalize/gn.scm 49 */
						obj_t BgL_newzd2gzd2_1782;

						{	/* Globalize/gn.scm 49 */
							obj_t BgL_arg1321z00_1786;

							BgL_arg1321z00_1786 = CAR(((obj_t) BgL_gz00_1778));
							BgL_newzd2gzd2_1782 =
								BGl_Gzd2fromzd2ctoz00zzglobaliza7e_gnza7(BgL_arg1321z00_1786);
						}
						{	/* Globalize/gn.scm 50 */
							obj_t BgL_arg1318z00_1783;
							obj_t BgL_arg1319z00_1784;

							{	/* Globalize/gn.scm 50 */
								obj_t BgL_arg1320z00_1785;

								BgL_arg1320z00_1785 = CDR(((obj_t) BgL_gz00_1778));
								BgL_arg1318z00_1783 =
									BGl_appendzd221011zd2zzglobaliza7e_gnza7(BgL_newzd2gzd2_1782,
									BgL_arg1320z00_1785);
							}
							BgL_arg1319z00_1784 =
								BGl_appendzd221011zd2zzglobaliza7e_gnza7(BgL_newzd2gzd2_1782,
								BgL_g1z00_1779);
							{
								obj_t BgL_g1z00_2915;
								obj_t BgL_gz00_2914;

								BgL_gz00_2914 = BgL_arg1318z00_1783;
								BgL_g1z00_2915 = BgL_arg1319z00_1784;
								BgL_g1z00_1779 = BgL_g1z00_2915;
								BgL_gz00_1778 = BgL_gz00_2914;
								goto BgL_zc3z04anonymousza31316ze3z87_1780;
							}
						}
					}
			}
		}

	}



/* &Gn! */
	obj_t BGl_z62Gnz12z70zzglobaliza7e_gnza7(obj_t BgL_envz00_2616,
		obj_t BgL_argsz00_2617, obj_t BgL_nodez00_2618, obj_t BgL_callerz00_2619,
		obj_t BgL_gz00_2620)
	{
		{	/* Globalize/gn.scm 41 */
			return
				BGl_Gnz12z12zzglobaliza7e_gnza7(BgL_argsz00_2617,
				((BgL_nodez00_bglt) BgL_nodez00_2618),
				((BgL_variablez00_bglt) BgL_callerz00_2619), BgL_gz00_2620);
		}

	}



/* E* */
	obj_t BGl_Eza2za2zzglobaliza7e_gnza7(obj_t BgL_nodeza2za2_76,
		obj_t BgL_callerz00_77, obj_t BgL_gz00_78)
	{
		{	/* Globalize/gn.scm 237 */
			{
				obj_t BgL_nodeza2za2_1789;
				obj_t BgL_gz00_1790;

				BgL_nodeza2za2_1789 = BgL_nodeza2za2_76;
				BgL_gz00_1790 = BgL_gz00_78;
			BgL_zc3z04anonymousza31322ze3z87_1791:
				if (NULLP(BgL_nodeza2za2_1789))
					{	/* Globalize/gn.scm 240 */
						return BgL_gz00_1790;
					}
				else
					{	/* Globalize/gn.scm 242 */
						obj_t BgL_arg1325z00_1793;
						obj_t BgL_arg1326z00_1794;

						BgL_arg1325z00_1793 = CDR(((obj_t) BgL_nodeza2za2_1789));
						{	/* Globalize/gn.scm 243 */
							obj_t BgL_arg1327z00_1795;

							BgL_arg1327z00_1795 = CAR(((obj_t) BgL_nodeza2za2_1789));
							BgL_arg1326z00_1794 =
								BGl_Ez00zzglobaliza7e_gnza7(
								((BgL_nodez00_bglt) BgL_arg1327z00_1795),
								((BgL_variablez00_bglt) BgL_callerz00_77), BgL_gz00_1790);
						}
						{
							obj_t BgL_gz00_2929;
							obj_t BgL_nodeza2za2_2928;

							BgL_nodeza2za2_2928 = BgL_arg1325z00_1793;
							BgL_gz00_2929 = BgL_arg1326z00_1794;
							BgL_gz00_1790 = BgL_gz00_2929;
							BgL_nodeza2za2_1789 = BgL_nodeza2za2_2928;
							goto BgL_zc3z04anonymousza31322ze3z87_1791;
						}
					}
			}
		}

	}



/* save-app! */
	obj_t BGl_savezd2appz12zc0zzglobaliza7e_gnza7(obj_t BgL_callerz00_79,
		BgL_variablez00_bglt BgL_calleez00_80)
	{
		{	/* Globalize/gn.scm 248 */
			{	/* Globalize/gn.scm 249 */
				bool_t BgL_test1923z00_2930;

				{	/* Globalize/gn.scm 249 */
					obj_t BgL_classz00_2277;

					BgL_classz00_2277 = BGl_globalz00zzast_varz00;
					{	/* Globalize/gn.scm 249 */
						BgL_objectz00_bglt BgL_arg1807z00_2279;

						{	/* Globalize/gn.scm 249 */
							obj_t BgL_tmpz00_2931;

							BgL_tmpz00_2931 =
								((obj_t) ((BgL_objectz00_bglt) BgL_calleez00_80));
							BgL_arg1807z00_2279 = (BgL_objectz00_bglt) (BgL_tmpz00_2931);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Globalize/gn.scm 249 */
								long BgL_idxz00_2285;

								BgL_idxz00_2285 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2279);
								BgL_test1923z00_2930 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2285 + 2L)) == BgL_classz00_2277);
							}
						else
							{	/* Globalize/gn.scm 249 */
								bool_t BgL_res1847z00_2310;

								{	/* Globalize/gn.scm 249 */
									obj_t BgL_oclassz00_2293;

									{	/* Globalize/gn.scm 249 */
										obj_t BgL_arg1815z00_2301;
										long BgL_arg1816z00_2302;

										BgL_arg1815z00_2301 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Globalize/gn.scm 249 */
											long BgL_arg1817z00_2303;

											BgL_arg1817z00_2303 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2279);
											BgL_arg1816z00_2302 = (BgL_arg1817z00_2303 - OBJECT_TYPE);
										}
										BgL_oclassz00_2293 =
											VECTOR_REF(BgL_arg1815z00_2301, BgL_arg1816z00_2302);
									}
									{	/* Globalize/gn.scm 249 */
										bool_t BgL__ortest_1115z00_2294;

										BgL__ortest_1115z00_2294 =
											(BgL_classz00_2277 == BgL_oclassz00_2293);
										if (BgL__ortest_1115z00_2294)
											{	/* Globalize/gn.scm 249 */
												BgL_res1847z00_2310 = BgL__ortest_1115z00_2294;
											}
										else
											{	/* Globalize/gn.scm 249 */
												long BgL_odepthz00_2295;

												{	/* Globalize/gn.scm 249 */
													obj_t BgL_arg1804z00_2296;

													BgL_arg1804z00_2296 = (BgL_oclassz00_2293);
													BgL_odepthz00_2295 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2296);
												}
												if ((2L < BgL_odepthz00_2295))
													{	/* Globalize/gn.scm 249 */
														obj_t BgL_arg1802z00_2298;

														{	/* Globalize/gn.scm 249 */
															obj_t BgL_arg1803z00_2299;

															BgL_arg1803z00_2299 = (BgL_oclassz00_2293);
															BgL_arg1802z00_2298 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2299,
																2L);
														}
														BgL_res1847z00_2310 =
															(BgL_arg1802z00_2298 == BgL_classz00_2277);
													}
												else
													{	/* Globalize/gn.scm 249 */
														BgL_res1847z00_2310 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test1923z00_2930 = BgL_res1847z00_2310;
							}
					}
				}
				if (BgL_test1923z00_2930)
					{	/* Globalize/gn.scm 249 */
						return CNST_TABLE_REF(0);
					}
				else
					{	/* Globalize/gn.scm 251 */
						BgL_valuez00_bglt BgL_calleezd2infozd2_1798;

						BgL_calleezd2infozd2_1798 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_calleez00_80))))->BgL_valuez00);
						{	/* Globalize/gn.scm 252 */
							bool_t BgL_test1927z00_2958;

							{	/* Globalize/gn.scm 252 */
								obj_t BgL_arg1339z00_1806;

								{
									BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2959;

									{
										obj_t BgL_auxz00_2960;

										{	/* Globalize/gn.scm 252 */
											BgL_objectz00_bglt BgL_tmpz00_2961;

											BgL_tmpz00_2961 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_calleezd2infozd2_1798));
											BgL_auxz00_2960 = BGL_OBJECT_WIDENING(BgL_tmpz00_2961);
										}
										BgL_auxz00_2959 =
											((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2960);
									}
									BgL_arg1339z00_1806 =
										(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2959))->
										BgL_cfromz00);
								}
								BgL_test1927z00_2958 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
									(BgL_callerz00_79, BgL_arg1339z00_1806));
							}
							if (BgL_test1927z00_2958)
								{	/* Globalize/gn.scm 252 */
									BFALSE;
								}
							else
								{	/* Globalize/gn.scm 252 */
									{	/* Globalize/gn.scm 256 */
										obj_t BgL_arg1331z00_1801;

										{	/* Globalize/gn.scm 256 */
											obj_t BgL_arg1332z00_1802;

											{
												BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2969;

												{
													obj_t BgL_auxz00_2970;

													{	/* Globalize/gn.scm 256 */
														BgL_objectz00_bglt BgL_tmpz00_2971;

														BgL_tmpz00_2971 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_calleezd2infozd2_1798));
														BgL_auxz00_2970 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2971);
													}
													BgL_auxz00_2969 =
														((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2970);
												}
												BgL_arg1332z00_1802 =
													(((BgL_sfunzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_2969))->BgL_cfromz00);
											}
											BgL_arg1331z00_1801 =
												MAKE_YOUNG_PAIR(BgL_callerz00_79, BgL_arg1332z00_1802);
										}
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2978;

											{
												obj_t BgL_auxz00_2979;

												{	/* Globalize/gn.scm 254 */
													BgL_objectz00_bglt BgL_tmpz00_2980;

													BgL_tmpz00_2980 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_calleezd2infozd2_1798));
													BgL_auxz00_2979 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2980);
												}
												BgL_auxz00_2978 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2979);
											}
											((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2978))->
													BgL_cfromz00) =
												((obj_t) BgL_arg1331z00_1801), BUNSPEC);
										}
									}
									{	/* Globalize/gn.scm 257 */
										BgL_valuez00_bglt BgL_callerzd2infozd2_1803;

										BgL_callerzd2infozd2_1803 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_callerz00_79)))->
											BgL_valuez00);
										{	/* Globalize/gn.scm 260 */
											obj_t BgL_arg1333z00_1804;

											{	/* Globalize/gn.scm 260 */
												obj_t BgL_arg1335z00_1805;

												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2988;

													{
														obj_t BgL_auxz00_2989;

														{	/* Globalize/gn.scm 260 */
															BgL_objectz00_bglt BgL_tmpz00_2990;

															BgL_tmpz00_2990 =
																((BgL_objectz00_bglt)
																((BgL_sfunz00_bglt) BgL_callerzd2infozd2_1803));
															BgL_auxz00_2989 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2990);
														}
														BgL_auxz00_2988 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2989);
													}
													BgL_arg1335z00_1805 =
														(((BgL_sfunzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2988))->BgL_ctoz00);
												}
												BgL_arg1333z00_1804 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_calleez00_80), BgL_arg1335z00_1805);
											}
											{
												BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2998;

												{
													obj_t BgL_auxz00_2999;

													{	/* Globalize/gn.scm 258 */
														BgL_objectz00_bglt BgL_tmpz00_3000;

														BgL_tmpz00_3000 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_callerzd2infozd2_1803));
														BgL_auxz00_2999 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3000);
													}
													BgL_auxz00_2998 =
														((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2999);
												}
												((((BgL_sfunzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2998))->BgL_ctoz00) =
													((obj_t) BgL_arg1333z00_1804), BUNSPEC);
											}
										}
									}
								}
						}
						return CNST_TABLE_REF(0);
					}
			}
		}

	}



/* save-fun! */
	obj_t BGl_savezd2funz12zc0zzglobaliza7e_gnza7(BgL_variablez00_bglt
		BgL_callerz00_81, BgL_variablez00_bglt BgL_calleez00_82)
	{
		{	/* Globalize/gn.scm 267 */
			{	/* Globalize/gn.scm 268 */
				bool_t BgL_test1928z00_3007;

				{	/* Globalize/gn.scm 268 */
					bool_t BgL_test1929z00_3008;

					{	/* Globalize/gn.scm 268 */
						obj_t BgL_classz00_2323;

						BgL_classz00_2323 = BGl_globalz00zzast_varz00;
						{	/* Globalize/gn.scm 268 */
							BgL_objectz00_bglt BgL_arg1807z00_2325;

							{	/* Globalize/gn.scm 268 */
								obj_t BgL_tmpz00_3009;

								BgL_tmpz00_3009 =
									((obj_t) ((BgL_objectz00_bglt) BgL_callerz00_81));
								BgL_arg1807z00_2325 = (BgL_objectz00_bglt) (BgL_tmpz00_3009);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/gn.scm 268 */
									long BgL_idxz00_2331;

									BgL_idxz00_2331 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2325);
									BgL_test1929z00_3008 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2331 + 2L)) == BgL_classz00_2323);
								}
							else
								{	/* Globalize/gn.scm 268 */
									bool_t BgL_res1848z00_2356;

									{	/* Globalize/gn.scm 268 */
										obj_t BgL_oclassz00_2339;

										{	/* Globalize/gn.scm 268 */
											obj_t BgL_arg1815z00_2347;
											long BgL_arg1816z00_2348;

											BgL_arg1815z00_2347 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/gn.scm 268 */
												long BgL_arg1817z00_2349;

												BgL_arg1817z00_2349 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2325);
												BgL_arg1816z00_2348 =
													(BgL_arg1817z00_2349 - OBJECT_TYPE);
											}
											BgL_oclassz00_2339 =
												VECTOR_REF(BgL_arg1815z00_2347, BgL_arg1816z00_2348);
										}
										{	/* Globalize/gn.scm 268 */
											bool_t BgL__ortest_1115z00_2340;

											BgL__ortest_1115z00_2340 =
												(BgL_classz00_2323 == BgL_oclassz00_2339);
											if (BgL__ortest_1115z00_2340)
												{	/* Globalize/gn.scm 268 */
													BgL_res1848z00_2356 = BgL__ortest_1115z00_2340;
												}
											else
												{	/* Globalize/gn.scm 268 */
													long BgL_odepthz00_2341;

													{	/* Globalize/gn.scm 268 */
														obj_t BgL_arg1804z00_2342;

														BgL_arg1804z00_2342 = (BgL_oclassz00_2339);
														BgL_odepthz00_2341 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2342);
													}
													if ((2L < BgL_odepthz00_2341))
														{	/* Globalize/gn.scm 268 */
															obj_t BgL_arg1802z00_2344;

															{	/* Globalize/gn.scm 268 */
																obj_t BgL_arg1803z00_2345;

																BgL_arg1803z00_2345 = (BgL_oclassz00_2339);
																BgL_arg1802z00_2344 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2345,
																	2L);
															}
															BgL_res1848z00_2356 =
																(BgL_arg1802z00_2344 == BgL_classz00_2323);
														}
													else
														{	/* Globalize/gn.scm 268 */
															BgL_res1848z00_2356 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1929z00_3008 = BgL_res1848z00_2356;
								}
						}
					}
					if (BgL_test1929z00_3008)
						{	/* Globalize/gn.scm 268 */
							BgL_test1928z00_3007 = ((bool_t) 1);
						}
					else
						{	/* Globalize/gn.scm 268 */
							obj_t BgL_classz00_2357;

							BgL_classz00_2357 = BGl_globalz00zzast_varz00;
							{	/* Globalize/gn.scm 268 */
								BgL_objectz00_bglt BgL_arg1807z00_2359;

								{	/* Globalize/gn.scm 268 */
									obj_t BgL_tmpz00_3032;

									BgL_tmpz00_3032 =
										((obj_t) ((BgL_objectz00_bglt) BgL_calleez00_82));
									BgL_arg1807z00_2359 = (BgL_objectz00_bglt) (BgL_tmpz00_3032);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Globalize/gn.scm 268 */
										long BgL_idxz00_2365;

										BgL_idxz00_2365 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2359);
										BgL_test1928z00_3007 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2365 + 2L)) == BgL_classz00_2357);
									}
								else
									{	/* Globalize/gn.scm 268 */
										bool_t BgL_res1849z00_2390;

										{	/* Globalize/gn.scm 268 */
											obj_t BgL_oclassz00_2373;

											{	/* Globalize/gn.scm 268 */
												obj_t BgL_arg1815z00_2381;
												long BgL_arg1816z00_2382;

												BgL_arg1815z00_2381 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Globalize/gn.scm 268 */
													long BgL_arg1817z00_2383;

													BgL_arg1817z00_2383 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2359);
													BgL_arg1816z00_2382 =
														(BgL_arg1817z00_2383 - OBJECT_TYPE);
												}
												BgL_oclassz00_2373 =
													VECTOR_REF(BgL_arg1815z00_2381, BgL_arg1816z00_2382);
											}
											{	/* Globalize/gn.scm 268 */
												bool_t BgL__ortest_1115z00_2374;

												BgL__ortest_1115z00_2374 =
													(BgL_classz00_2357 == BgL_oclassz00_2373);
												if (BgL__ortest_1115z00_2374)
													{	/* Globalize/gn.scm 268 */
														BgL_res1849z00_2390 = BgL__ortest_1115z00_2374;
													}
												else
													{	/* Globalize/gn.scm 268 */
														long BgL_odepthz00_2375;

														{	/* Globalize/gn.scm 268 */
															obj_t BgL_arg1804z00_2376;

															BgL_arg1804z00_2376 = (BgL_oclassz00_2373);
															BgL_odepthz00_2375 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2376);
														}
														if ((2L < BgL_odepthz00_2375))
															{	/* Globalize/gn.scm 268 */
																obj_t BgL_arg1802z00_2378;

																{	/* Globalize/gn.scm 268 */
																	obj_t BgL_arg1803z00_2379;

																	BgL_arg1803z00_2379 = (BgL_oclassz00_2373);
																	BgL_arg1802z00_2378 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2379,
																		2L);
																}
																BgL_res1849z00_2390 =
																	(BgL_arg1802z00_2378 == BgL_classz00_2357);
															}
														else
															{	/* Globalize/gn.scm 268 */
																BgL_res1849z00_2390 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1928z00_3007 = BgL_res1849z00_2390;
									}
							}
						}
				}
				if (BgL_test1928z00_3007)
					{	/* Globalize/gn.scm 268 */
						return CNST_TABLE_REF(0);
					}
				else
					{	/* Globalize/gn.scm 270 */
						BgL_valuez00_bglt BgL_callerzd2infozd2_1809;

						BgL_callerzd2infozd2_1809 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_callerz00_81))))->BgL_valuez00);
						{	/* Globalize/gn.scm 274 */
							bool_t BgL_test1936z00_3059;

							{	/* Globalize/gn.scm 274 */
								obj_t BgL_arg1349z00_1814;

								{
									BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3060;

									{
										obj_t BgL_auxz00_3061;

										{	/* Globalize/gn.scm 274 */
											BgL_objectz00_bglt BgL_tmpz00_3062;

											BgL_tmpz00_3062 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_callerzd2infozd2_1809));
											BgL_auxz00_3061 = BGL_OBJECT_WIDENING(BgL_tmpz00_3062);
										}
										BgL_auxz00_3060 =
											((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3061);
									}
									BgL_arg1349z00_1814 =
										(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3060))->
										BgL_efunctionsz00);
								}
								BgL_test1936z00_3059 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
										((obj_t) BgL_calleez00_82), BgL_arg1349z00_1814));
							}
							if (BgL_test1936z00_3059)
								{	/* Globalize/gn.scm 274 */
									BFALSE;
								}
							else
								{	/* Globalize/gn.scm 276 */
									obj_t BgL_arg1346z00_1812;

									{	/* Globalize/gn.scm 276 */
										obj_t BgL_arg1348z00_1813;

										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3071;

											{
												obj_t BgL_auxz00_3072;

												{	/* Globalize/gn.scm 276 */
													BgL_objectz00_bglt BgL_tmpz00_3073;

													BgL_tmpz00_3073 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_callerzd2infozd2_1809));
													BgL_auxz00_3072 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3073);
												}
												BgL_auxz00_3071 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3072);
											}
											BgL_arg1348z00_1813 =
												(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3071))->
												BgL_efunctionsz00);
										}
										BgL_arg1346z00_1812 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_calleez00_82), BgL_arg1348z00_1813);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3081;

										{
											obj_t BgL_auxz00_3082;

											{	/* Globalize/gn.scm 275 */
												BgL_objectz00_bglt BgL_tmpz00_3083;

												BgL_tmpz00_3083 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_callerzd2infozd2_1809));
												BgL_auxz00_3082 = BGL_OBJECT_WIDENING(BgL_tmpz00_3083);
											}
											BgL_auxz00_3081 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3082);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3081))->
												BgL_efunctionsz00) =
											((obj_t) BgL_arg1346z00_1812), BUNSPEC);
									}
								}
						}
						return CNST_TABLE_REF(0);
					}
			}
		}

	}



/* G-from-cto */
	obj_t BGl_Gzd2fromzd2ctoz00zzglobaliza7e_gnza7(obj_t BgL_localz00_83)
	{
		{	/* Globalize/gn.scm 282 */
			{	/* Globalize/gn.scm 283 */
				obj_t BgL_g1129z00_1816;

				{	/* Globalize/gn.scm 283 */
					BgL_sfunz00_bglt BgL_oz00_2399;

					BgL_oz00_2399 =
						((BgL_sfunz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_localz00_83))))->BgL_valuez00));
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3094;

						{
							obj_t BgL_auxz00_3095;

							{	/* Globalize/gn.scm 283 */
								BgL_objectz00_bglt BgL_tmpz00_3096;

								BgL_tmpz00_3096 = ((BgL_objectz00_bglt) BgL_oz00_2399);
								BgL_auxz00_3095 = BGL_OBJECT_WIDENING(BgL_tmpz00_3096);
							}
							BgL_auxz00_3094 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3095);
						}
						BgL_g1129z00_1816 =
							(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3094))->
							BgL_ctoz00);
					}
				}
				{
					obj_t BgL_ctoz00_1819;
					obj_t BgL_gz00_1820;

					BgL_ctoz00_1819 = BgL_g1129z00_1816;
					BgL_gz00_1820 = BNIL;
				BgL_zc3z04anonymousza31350ze3z87_1821:
					if (NULLP(BgL_ctoz00_1819))
						{	/* Globalize/gn.scm 286 */
							return BgL_gz00_1820;
						}
					else
						{	/* Globalize/gn.scm 288 */
							bool_t BgL_test1938z00_3103;

							{	/* Globalize/gn.scm 288 */
								BgL_sfunz00_bglt BgL_oz00_2403;

								BgL_oz00_2403 =
									((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt)
														CAR(((obj_t) BgL_ctoz00_1819))))))->BgL_valuez00));
								{
									BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3110;

									{
										obj_t BgL_auxz00_3111;

										{	/* Globalize/gn.scm 288 */
											BgL_objectz00_bglt BgL_tmpz00_3112;

											BgL_tmpz00_3112 = ((BgL_objectz00_bglt) BgL_oz00_2403);
											BgL_auxz00_3111 = BGL_OBJECT_WIDENING(BgL_tmpz00_3112);
										}
										BgL_auxz00_3110 =
											((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3111);
									}
									BgL_test1938z00_3103 =
										(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3110))->
										BgL_gzf3zf3);
								}
							}
							if (BgL_test1938z00_3103)
								{	/* Globalize/gn.scm 289 */
									obj_t BgL_arg1367z00_1826;

									BgL_arg1367z00_1826 = CDR(((obj_t) BgL_ctoz00_1819));
									{
										obj_t BgL_ctoz00_3119;

										BgL_ctoz00_3119 = BgL_arg1367z00_1826;
										BgL_ctoz00_1819 = BgL_ctoz00_3119;
										goto BgL_zc3z04anonymousza31350ze3z87_1821;
									}
								}
							else
								{	/* Globalize/gn.scm 288 */
									{	/* Globalize/gn.scm 291 */
										BgL_valuez00_bglt BgL_arg1370z00_1827;

										BgL_arg1370z00_1827 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt)
															CAR(((obj_t) BgL_ctoz00_1819))))))->BgL_valuez00);
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3125;

											{
												obj_t BgL_auxz00_3126;

												{	/* Globalize/gn.scm 291 */
													BgL_objectz00_bglt BgL_tmpz00_3127;

													BgL_tmpz00_3127 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_arg1370z00_1827));
													BgL_auxz00_3126 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3127);
												}
												BgL_auxz00_3125 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3126);
											}
											((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3125))->
													BgL_gzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
										}
									}
									{	/* Globalize/gn.scm 292 */
										obj_t BgL_arg1375z00_1829;
										obj_t BgL_arg1376z00_1830;

										BgL_arg1375z00_1829 = CDR(((obj_t) BgL_ctoz00_1819));
										{	/* Globalize/gn.scm 292 */
											obj_t BgL_arg1377z00_1831;

											BgL_arg1377z00_1831 = CAR(((obj_t) BgL_ctoz00_1819));
											BgL_arg1376z00_1830 =
												MAKE_YOUNG_PAIR(BgL_arg1377z00_1831, BgL_gz00_1820);
										}
										{
											obj_t BgL_gz00_3139;
											obj_t BgL_ctoz00_3138;

											BgL_ctoz00_3138 = BgL_arg1375z00_1829;
											BgL_gz00_3139 = BgL_arg1376z00_1830;
											BgL_gz00_1820 = BgL_gz00_3139;
											BgL_ctoz00_1819 = BgL_ctoz00_3138;
											goto BgL_zc3z04anonymousza31350ze3z87_1821;
										}
									}
								}
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_gnza7(void)
	{
		{	/* Globalize/gn.scm 23 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_gnza7(void)
	{
		{	/* Globalize/gn.scm 23 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_proc1858z00zzglobaliza7e_gnza7,
				BGl_nodez00zzast_nodez00, BGl_string1859z00zzglobaliza7e_gnza7);
		}

	}



/* &E1266 */
	obj_t BGl_z62E1266z62zzglobaliza7e_gnza7(obj_t BgL_envz00_2622,
		obj_t BgL_nodez00_2623, obj_t BgL_callerz00_2624, obj_t BgL_gz00_2625)
	{
		{	/* Globalize/gn.scm 56 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string1860z00zzglobaliza7e_gnza7,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2623)));
		}

	}



/* E */
	obj_t BGl_Ez00zzglobaliza7e_gnza7(BgL_nodez00_bglt BgL_nodez00_7,
		BgL_variablez00_bglt BgL_callerz00_8, obj_t BgL_gz00_9)
	{
		{	/* Globalize/gn.scm 56 */
			{	/* Globalize/gn.scm 56 */
				obj_t BgL_method1267z00_1842;

				{	/* Globalize/gn.scm 56 */
					obj_t BgL_res1854z00_2443;

					{	/* Globalize/gn.scm 56 */
						long BgL_objzd2classzd2numz00_2414;

						BgL_objzd2classzd2numz00_2414 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_7));
						{	/* Globalize/gn.scm 56 */
							obj_t BgL_arg1811z00_2415;

							BgL_arg1811z00_2415 =
								PROCEDURE_REF(BGl_Ezd2envzd2zzglobaliza7e_gnza7, (int) (1L));
							{	/* Globalize/gn.scm 56 */
								int BgL_offsetz00_2418;

								BgL_offsetz00_2418 = (int) (BgL_objzd2classzd2numz00_2414);
								{	/* Globalize/gn.scm 56 */
									long BgL_offsetz00_2419;

									BgL_offsetz00_2419 =
										((long) (BgL_offsetz00_2418) - OBJECT_TYPE);
									{	/* Globalize/gn.scm 56 */
										long BgL_modz00_2420;

										BgL_modz00_2420 =
											(BgL_offsetz00_2419 >> (int) ((long) ((int) (4L))));
										{	/* Globalize/gn.scm 56 */
											long BgL_restz00_2422;

											BgL_restz00_2422 =
												(BgL_offsetz00_2419 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Globalize/gn.scm 56 */

												{	/* Globalize/gn.scm 56 */
													obj_t BgL_bucketz00_2424;

													BgL_bucketz00_2424 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2415), BgL_modz00_2420);
													BgL_res1854z00_2443 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2424), BgL_restz00_2422);
					}}}}}}}}
					BgL_method1267z00_1842 = BgL_res1854z00_2443;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1267z00_1842,
					((obj_t) BgL_nodez00_7), ((obj_t) BgL_callerz00_8), BgL_gz00_9);
			}
		}

	}



/* &E */
	obj_t BGl_z62Ez62zzglobaliza7e_gnza7(obj_t BgL_envz00_2626,
		obj_t BgL_nodez00_2627, obj_t BgL_callerz00_2628, obj_t BgL_gz00_2629)
	{
		{	/* Globalize/gn.scm 56 */
			return
				BGl_Ez00zzglobaliza7e_gnza7(
				((BgL_nodez00_bglt) BgL_nodez00_2627),
				((BgL_variablez00_bglt) BgL_callerz00_2628), BgL_gz00_2629);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_gnza7(void)
	{
		{	/* Globalize/gn.scm 23 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_atomz00zzast_nodez00,
				BGl_proc1861z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_kwotez00zzast_nodez00,
				BGl_proc1863z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_varz00zzast_nodez00,
				BGl_proc1864z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_closurez00zzast_nodez00,
				BGl_proc1865z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_sequencez00zzast_nodez00,
				BGl_proc1866z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_syncz00zzast_nodez00,
				BGl_proc1867z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_appz00zzast_nodez00,
				BGl_proc1868z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc1869z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_funcallz00zzast_nodez00,
				BGl_proc1870z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_externz00zzast_nodez00,
				BGl_proc1871z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_castz00zzast_nodez00,
				BGl_proc1872z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_setqz00zzast_nodez00,
				BGl_proc1873z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_conditionalz00zzast_nodez00,
				BGl_proc1874z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_failz00zzast_nodez00,
				BGl_proc1875z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_switchz00zzast_nodez00,
				BGl_proc1876z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1877z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1878z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1879z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1880z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc1881z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc1882z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_Ezd2envzd2zzglobaliza7e_gnza7, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1883z00zzglobaliza7e_gnza7,
				BGl_string1862z00zzglobaliza7e_gnza7);
		}

	}



/* &E-box-set!1311 */
	obj_t BGl_z62Ezd2boxzd2setz121311z70zzglobaliza7e_gnza7(obj_t BgL_envz00_2652,
		obj_t BgL_nodez00_2653, obj_t BgL_callerz00_2654, obj_t BgL_gz00_2655)
	{
		{	/* Globalize/gn.scm 230 */
			{	/* Globalize/gn.scm 232 */
				BgL_varz00_bglt BgL_arg1699z00_2754;
				obj_t BgL_arg1700z00_2755;

				BgL_arg1699z00_2754 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2653)))->BgL_varz00);
				{	/* Globalize/gn.scm 232 */
					BgL_nodez00_bglt BgL_arg1701z00_2756;

					BgL_arg1701z00_2756 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2653)))->BgL_valuez00);
					BgL_arg1700z00_2755 =
						BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1701z00_2756,
						((BgL_variablez00_bglt) BgL_callerz00_2654), BgL_gz00_2655);
				}
				return
					BGl_Ez00zzglobaliza7e_gnza7(
					((BgL_nodez00_bglt) BgL_arg1699z00_2754),
					((BgL_variablez00_bglt) BgL_callerz00_2654), BgL_arg1700z00_2755);
			}
		}

	}



/* &E-box-ref1309 */
	obj_t BGl_z62Ezd2boxzd2ref1309z62zzglobaliza7e_gnza7(obj_t BgL_envz00_2656,
		obj_t BgL_nodez00_2657, obj_t BgL_callerz00_2658, obj_t BgL_gz00_2659)
	{
		{	/* Globalize/gn.scm 223 */
			{	/* Globalize/gn.scm 225 */
				BgL_varz00_bglt BgL_arg1692z00_2758;

				BgL_arg1692z00_2758 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2657)))->BgL_varz00);
				return
					BGl_Ez00zzglobaliza7e_gnza7(
					((BgL_nodez00_bglt) BgL_arg1692z00_2758),
					((BgL_variablez00_bglt) BgL_callerz00_2658), BgL_gz00_2659);
			}
		}

	}



/* &E-make-box1307 */
	obj_t BGl_z62Ezd2makezd2box1307z62zzglobaliza7e_gnza7(obj_t BgL_envz00_2660,
		obj_t BgL_nodez00_2661, obj_t BgL_callerz00_2662, obj_t BgL_gz00_2663)
	{
		{	/* Globalize/gn.scm 216 */
			{	/* Globalize/gn.scm 218 */
				BgL_nodez00_bglt BgL_arg1691z00_2760;

				BgL_arg1691z00_2760 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2661)))->BgL_valuez00);
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1691z00_2760,
					((BgL_variablez00_bglt) BgL_callerz00_2662), BgL_gz00_2663);
			}
		}

	}



/* &E-jump-ex-it1305 */
	obj_t BGl_z62Ezd2jumpzd2exzd2it1305zb0zzglobaliza7e_gnza7(obj_t
		BgL_envz00_2664, obj_t BgL_nodez00_2665, obj_t BgL_callerz00_2666,
		obj_t BgL_gz00_2667)
	{
		{	/* Globalize/gn.scm 209 */
			{	/* Globalize/gn.scm 211 */
				BgL_nodez00_bglt BgL_arg1681z00_2762;
				obj_t BgL_arg1688z00_2763;

				BgL_arg1681z00_2762 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2665)))->BgL_exitz00);
				{	/* Globalize/gn.scm 211 */
					BgL_nodez00_bglt BgL_arg1689z00_2764;

					BgL_arg1689z00_2764 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2665)))->
						BgL_valuez00);
					BgL_arg1688z00_2763 =
						BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1689z00_2764,
						((BgL_variablez00_bglt) BgL_callerz00_2666), BgL_gz00_2667);
				}
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1681z00_2762,
					((BgL_variablez00_bglt) BgL_callerz00_2666), BgL_arg1688z00_2763);
			}
		}

	}



/* &E-set-ex-it1303 */
	obj_t BGl_z62Ezd2setzd2exzd2it1303zb0zzglobaliza7e_gnza7(obj_t
		BgL_envz00_2668, obj_t BgL_nodez00_2669, obj_t BgL_callerz00_2670,
		obj_t BgL_gz00_2671)
	{
		{	/* Globalize/gn.scm 202 */
			{	/* Globalize/gn.scm 204 */
				BgL_nodez00_bglt BgL_arg1663z00_2766;
				obj_t BgL_arg1675z00_2767;

				BgL_arg1663z00_2766 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2669)))->BgL_bodyz00);
				{	/* Globalize/gn.scm 204 */
					BgL_nodez00_bglt BgL_arg1678z00_2768;

					BgL_arg1678z00_2768 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2669)))->
						BgL_onexitz00);
					BgL_arg1675z00_2767 =
						BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1678z00_2768,
						((BgL_variablez00_bglt) BgL_callerz00_2670), BgL_gz00_2671);
				}
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1663z00_2766,
					((BgL_variablez00_bglt) BgL_callerz00_2670), BgL_arg1675z00_2767);
			}
		}

	}



/* &E-let-var1301 */
	obj_t BGl_z62Ezd2letzd2var1301z62zzglobaliza7e_gnza7(obj_t BgL_envz00_2672,
		obj_t BgL_nodez00_2673, obj_t BgL_callerz00_2674, obj_t BgL_gz00_2675)
	{
		{	/* Globalize/gn.scm 190 */
			{
				obj_t BgL_bindingsz00_2771;
				obj_t BgL_gz00_2772;

				BgL_bindingsz00_2771 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2673)))->BgL_bindingsz00);
				BgL_gz00_2772 = BgL_gz00_2675;
			BgL_loopz00_2770:
				if (NULLP(BgL_bindingsz00_2771))
					{	/* Globalize/gn.scm 195 */
						BgL_nodez00_bglt BgL_arg1646z00_2773;

						BgL_arg1646z00_2773 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_2673)))->BgL_bodyz00);
						return
							BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1646z00_2773,
							((BgL_variablez00_bglt) BgL_callerz00_2674), BgL_gz00_2772);
					}
				else
					{	/* Globalize/gn.scm 196 */
						obj_t BgL_arg1650z00_2774;
						obj_t BgL_arg1651z00_2775;

						BgL_arg1650z00_2774 = CDR(((obj_t) BgL_bindingsz00_2771));
						{	/* Globalize/gn.scm 197 */
							obj_t BgL_arg1654z00_2776;

							{	/* Globalize/gn.scm 197 */
								obj_t BgL_pairz00_2777;

								BgL_pairz00_2777 = CAR(((obj_t) BgL_bindingsz00_2771));
								BgL_arg1654z00_2776 = CDR(BgL_pairz00_2777);
							}
							BgL_arg1651z00_2775 =
								BGl_Ez00zzglobaliza7e_gnza7(
								((BgL_nodez00_bglt) BgL_arg1654z00_2776),
								((BgL_variablez00_bglt) BgL_callerz00_2674), BgL_gz00_2772);
						}
						{
							obj_t BgL_gz00_3252;
							obj_t BgL_bindingsz00_3251;

							BgL_bindingsz00_3251 = BgL_arg1650z00_2774;
							BgL_gz00_3252 = BgL_arg1651z00_2775;
							BgL_gz00_2772 = BgL_gz00_3252;
							BgL_bindingsz00_2771 = BgL_bindingsz00_3251;
							goto BgL_loopz00_2770;
						}
					}
			}
		}

	}



/* &E-let-fun1299 */
	obj_t BGl_z62Ezd2letzd2fun1299z62zzglobaliza7e_gnza7(obj_t BgL_envz00_2676,
		obj_t BgL_nodez00_2677, obj_t BgL_callerz00_2678, obj_t BgL_gz00_2679)
	{
		{	/* Globalize/gn.scm 176 */
			{
				obj_t BgL_localsz00_2780;
				obj_t BgL_gz00_2781;

				BgL_localsz00_2780 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2677)))->BgL_localsz00);
				BgL_gz00_2781 = BgL_gz00_2679;
			BgL_loopz00_2779:
				if (NULLP(BgL_localsz00_2780))
					{	/* Globalize/gn.scm 181 */
						BgL_nodez00_bglt BgL_arg1615z00_2782;

						BgL_arg1615z00_2782 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_2677)))->BgL_bodyz00);
						return
							BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1615z00_2782,
							((BgL_variablez00_bglt) BgL_callerz00_2678), BgL_gz00_2781);
					}
				else
					{	/* Globalize/gn.scm 182 */
						obj_t BgL_arg1616z00_2783;
						obj_t BgL_arg1625z00_2784;

						BgL_arg1616z00_2783 = CDR(((obj_t) BgL_localsz00_2780));
						{	/* Globalize/gn.scm 183 */
							obj_t BgL_arg1626z00_2785;
							obj_t BgL_arg1627z00_2786;

							BgL_arg1626z00_2785 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_localsz00_2780))))))->
												BgL_valuez00))))->BgL_bodyz00);
							BgL_arg1627z00_2786 = CAR(((obj_t) BgL_localsz00_2780));
							BgL_arg1625z00_2784 =
								BGl_Ez00zzglobaliza7e_gnza7(
								((BgL_nodez00_bglt) BgL_arg1626z00_2785),
								((BgL_variablez00_bglt) BgL_arg1627z00_2786), BgL_gz00_2781);
						}
						{
							obj_t BgL_gz00_3276;
							obj_t BgL_localsz00_3275;

							BgL_localsz00_3275 = BgL_arg1616z00_2783;
							BgL_gz00_3276 = BgL_arg1625z00_2784;
							BgL_gz00_2781 = BgL_gz00_3276;
							BgL_localsz00_2780 = BgL_localsz00_3275;
							goto BgL_loopz00_2779;
						}
					}
			}
		}

	}



/* &E-switch1297 */
	obj_t BGl_z62Ezd2switch1297zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2680,
		obj_t BgL_nodez00_2681, obj_t BgL_callerz00_2682, obj_t BgL_gz00_2683)
	{
		{	/* Globalize/gn.scm 165 */
			{
				obj_t BgL_clausesz00_2789;
				obj_t BgL_gz00_2790;

				BgL_clausesz00_2789 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2681)))->BgL_clausesz00);
				BgL_gz00_2790 = BgL_gz00_2683;
			BgL_loopz00_2788:
				if (NULLP(BgL_clausesz00_2789))
					{	/* Globalize/gn.scm 170 */
						BgL_nodez00_bglt BgL_arg1595z00_2791;

						BgL_arg1595z00_2791 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_2681)))->BgL_testz00);
						return
							BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1595z00_2791,
							((BgL_variablez00_bglt) BgL_callerz00_2682), BgL_gz00_2790);
					}
				else
					{	/* Globalize/gn.scm 171 */
						obj_t BgL_arg1602z00_2792;
						obj_t BgL_arg1605z00_2793;

						BgL_arg1602z00_2792 = CDR(((obj_t) BgL_clausesz00_2789));
						{	/* Globalize/gn.scm 171 */
							obj_t BgL_arg1606z00_2794;

							{	/* Globalize/gn.scm 171 */
								obj_t BgL_pairz00_2795;

								BgL_pairz00_2795 = CAR(((obj_t) BgL_clausesz00_2789));
								BgL_arg1606z00_2794 = CDR(BgL_pairz00_2795);
							}
							BgL_arg1605z00_2793 =
								BGl_Ez00zzglobaliza7e_gnza7(
								((BgL_nodez00_bglt) BgL_arg1606z00_2794),
								((BgL_variablez00_bglt) BgL_callerz00_2682), BgL_gz00_2790);
						}
						{
							obj_t BgL_gz00_3294;
							obj_t BgL_clausesz00_3293;

							BgL_clausesz00_3293 = BgL_arg1602z00_2792;
							BgL_gz00_3294 = BgL_arg1605z00_2793;
							BgL_gz00_2790 = BgL_gz00_3294;
							BgL_clausesz00_2789 = BgL_clausesz00_3293;
							goto BgL_loopz00_2788;
						}
					}
			}
		}

	}



/* &E-fail1295 */
	obj_t BGl_z62Ezd2fail1295zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2684,
		obj_t BgL_nodez00_2685, obj_t BgL_callerz00_2686, obj_t BgL_gz00_2687)
	{
		{	/* Globalize/gn.scm 158 */
			{	/* Globalize/gn.scm 160 */
				BgL_nodez00_bglt BgL_arg1575z00_2797;
				obj_t BgL_arg1576z00_2798;

				BgL_arg1575z00_2797 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2685)))->BgL_procz00);
				{	/* Globalize/gn.scm 160 */
					BgL_nodez00_bglt BgL_arg1584z00_2799;
					obj_t BgL_arg1585z00_2800;

					BgL_arg1584z00_2799 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2685)))->BgL_msgz00);
					{	/* Globalize/gn.scm 160 */
						BgL_nodez00_bglt BgL_arg1589z00_2801;

						BgL_arg1589z00_2801 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_2685)))->BgL_objz00);
						BgL_arg1585z00_2800 =
							BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1589z00_2801,
							((BgL_variablez00_bglt) BgL_callerz00_2686), BgL_gz00_2687);
					}
					BgL_arg1576z00_2798 =
						BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1584z00_2799,
						((BgL_variablez00_bglt) BgL_callerz00_2686), BgL_arg1585z00_2800);
				}
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1575z00_2797,
					((BgL_variablez00_bglt) BgL_callerz00_2686), BgL_arg1576z00_2798);
			}
		}

	}



/* &E-conditional1293 */
	obj_t BGl_z62Ezd2conditional1293zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2688,
		obj_t BgL_nodez00_2689, obj_t BgL_callerz00_2690, obj_t BgL_gz00_2691)
	{
		{	/* Globalize/gn.scm 151 */
			{	/* Globalize/gn.scm 153 */
				BgL_nodez00_bglt BgL_arg1561z00_2803;
				obj_t BgL_arg1564z00_2804;

				BgL_arg1561z00_2803 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2689)))->BgL_testz00);
				{	/* Globalize/gn.scm 153 */
					BgL_nodez00_bglt BgL_arg1565z00_2805;
					obj_t BgL_arg1571z00_2806;

					BgL_arg1565z00_2805 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2689)))->BgL_truez00);
					{	/* Globalize/gn.scm 153 */
						BgL_nodez00_bglt BgL_arg1573z00_2807;

						BgL_arg1573z00_2807 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2689)))->BgL_falsez00);
						BgL_arg1571z00_2806 =
							BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1573z00_2807,
							((BgL_variablez00_bglt) BgL_callerz00_2690), BgL_gz00_2691);
					}
					BgL_arg1564z00_2804 =
						BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1565z00_2805,
						((BgL_variablez00_bglt) BgL_callerz00_2690), BgL_arg1571z00_2806);
				}
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1561z00_2803,
					((BgL_variablez00_bglt) BgL_callerz00_2690), BgL_arg1564z00_2804);
			}
		}

	}



/* &E-setq1291 */
	obj_t BGl_z62Ezd2setq1291zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2692,
		obj_t BgL_nodez00_2693, obj_t BgL_callerz00_2694, obj_t BgL_gz00_2695)
	{
		{	/* Globalize/gn.scm 144 */
			{	/* Globalize/gn.scm 146 */
				BgL_nodez00_bglt BgL_arg1559z00_2809;

				BgL_arg1559z00_2809 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2693)))->BgL_valuez00);
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1559z00_2809,
					((BgL_variablez00_bglt) BgL_callerz00_2694), BgL_gz00_2695);
			}
		}

	}



/* &E-cast1289 */
	obj_t BGl_z62Ezd2cast1289zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2696,
		obj_t BgL_nodez00_2697, obj_t BgL_callerz00_2698, obj_t BgL_gz00_2699)
	{
		{	/* Globalize/gn.scm 137 */
			{	/* Globalize/gn.scm 139 */
				BgL_nodez00_bglt BgL_arg1553z00_2811;

				BgL_arg1553z00_2811 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2697)))->BgL_argz00);
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1553z00_2811,
					((BgL_variablez00_bglt) BgL_callerz00_2698), BgL_gz00_2699);
			}
		}

	}



/* &E-extern1287 */
	obj_t BGl_z62Ezd2extern1287zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2700,
		obj_t BgL_nodez00_2701, obj_t BgL_callerz00_2702, obj_t BgL_gz00_2703)
	{
		{	/* Globalize/gn.scm 130 */
			return
				BGl_Eza2za2zzglobaliza7e_gnza7(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2701)))->BgL_exprza2za2),
				BgL_callerz00_2702, BgL_gz00_2703);
		}

	}



/* &E-funcall1285 */
	obj_t BGl_z62Ezd2funcall1285zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2704,
		obj_t BgL_nodez00_2705, obj_t BgL_callerz00_2706, obj_t BgL_gz00_2707)
	{
		{	/* Globalize/gn.scm 123 */
			{	/* Globalize/gn.scm 125 */
				BgL_nodez00_bglt BgL_arg1540z00_2814;
				obj_t BgL_arg1544z00_2815;

				BgL_arg1540z00_2814 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2705)))->BgL_funz00);
				BgL_arg1544z00_2815 =
					BGl_Eza2za2zzglobaliza7e_gnza7(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2705)))->BgL_argsz00),
					BgL_callerz00_2706, BgL_gz00_2707);
				return BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1540z00_2814,
					((BgL_variablez00_bglt) BgL_callerz00_2706), BgL_arg1544z00_2815);
			}
		}

	}



/* &E-app-ly1283 */
	obj_t BGl_z62Ezd2appzd2ly1283z62zzglobaliza7e_gnza7(obj_t BgL_envz00_2708,
		obj_t BgL_nodez00_2709, obj_t BgL_callerz00_2710, obj_t BgL_gz00_2711)
	{
		{	/* Globalize/gn.scm 116 */
			{	/* Globalize/gn.scm 118 */
				BgL_nodez00_bglt BgL_arg1514z00_2817;
				obj_t BgL_arg1516z00_2818;

				BgL_arg1514z00_2817 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2709)))->BgL_funz00);
				{	/* Globalize/gn.scm 118 */
					BgL_nodez00_bglt BgL_arg1535z00_2819;

					BgL_arg1535z00_2819 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2709)))->BgL_argz00);
					BgL_arg1516z00_2818 =
						BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1535z00_2819,
						((BgL_variablez00_bglt) BgL_callerz00_2710), BgL_gz00_2711);
				}
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1514z00_2817,
					((BgL_variablez00_bglt) BgL_callerz00_2710), BgL_arg1516z00_2818);
			}
		}

	}



/* &E-app1281 */
	obj_t BGl_z62Ezd2app1281zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2712,
		obj_t BgL_nodez00_2713, obj_t BgL_callerz00_2714, obj_t BgL_gz00_2715)
	{
		{	/* Globalize/gn.scm 108 */
			BGl_savezd2appz12zc0zzglobaliza7e_gnza7(BgL_callerz00_2714,
				(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2713)))->BgL_funz00)))->
					BgL_variablez00));
			return BGl_Eza2za2zzglobaliza7e_gnza7((((BgL_appz00_bglt)
						COBJECT(((BgL_appz00_bglt) BgL_nodez00_2713)))->BgL_argsz00),
				BgL_callerz00_2714, BgL_gz00_2715);
		}

	}



/* &E-sync1279 */
	obj_t BGl_z62Ezd2sync1279zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2716,
		obj_t BgL_nodez00_2717, obj_t BgL_callerz00_2718, obj_t BgL_gz00_2719)
	{
		{	/* Globalize/gn.scm 100 */
			{	/* Globalize/gn.scm 101 */
				BgL_nodez00_bglt BgL_arg1454z00_2822;
				obj_t BgL_arg1472z00_2823;

				BgL_arg1454z00_2822 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2717)))->BgL_bodyz00);
				{	/* Globalize/gn.scm 102 */
					BgL_nodez00_bglt BgL_arg1473z00_2824;
					obj_t BgL_arg1485z00_2825;

					BgL_arg1473z00_2824 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2717)))->BgL_prelockz00);
					{	/* Globalize/gn.scm 103 */
						BgL_nodez00_bglt BgL_arg1489z00_2826;

						BgL_arg1489z00_2826 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_2717)))->BgL_mutexz00);
						BgL_arg1485z00_2825 =
							BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1489z00_2826,
							((BgL_variablez00_bglt) BgL_callerz00_2718), BgL_gz00_2719);
					}
					BgL_arg1472z00_2823 =
						BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1473z00_2824,
						((BgL_variablez00_bglt) BgL_callerz00_2718), BgL_arg1485z00_2825);
				}
				return
					BGl_Ez00zzglobaliza7e_gnza7(BgL_arg1454z00_2822,
					((BgL_variablez00_bglt) BgL_callerz00_2718), BgL_arg1472z00_2823);
			}
		}

	}



/* &E-sequence1277 */
	obj_t BGl_z62Ezd2sequence1277zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2720,
		obj_t BgL_nodez00_2721, obj_t BgL_callerz00_2722, obj_t BgL_gz00_2723)
	{
		{	/* Globalize/gn.scm 94 */
			return
				BGl_Eza2za2zzglobaliza7e_gnza7(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2721)))->BgL_nodesz00),
				BgL_callerz00_2722, BgL_gz00_2723);
		}

	}



/* &E-closure1275 */
	obj_t BGl_z62Ezd2closure1275zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2724,
		obj_t BgL_nodez00_2725, obj_t BgL_callerz00_2726, obj_t BgL_gz00_2727)
	{
		{	/* Globalize/gn.scm 79 */
			{	/* Globalize/gn.scm 80 */
				BgL_variablez00_bglt BgL_varz00_2830;

				BgL_varz00_2830 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt)
								((BgL_closurez00_bglt) BgL_nodez00_2725))))->BgL_variablez00);
				BGl_savezd2funz12zc0zzglobaliza7e_gnza7(
					((BgL_variablez00_bglt) BgL_callerz00_2726), BgL_varz00_2830);
				{	/* Globalize/gn.scm 82 */
					bool_t BgL_test1942z00_3374;

					{	/* Globalize/gn.scm 82 */
						bool_t BgL_test1943z00_3375;

						{	/* Globalize/gn.scm 82 */
							obj_t BgL_classz00_2831;

							BgL_classz00_2831 = BGl_localz00zzast_varz00;
							{	/* Globalize/gn.scm 82 */
								BgL_objectz00_bglt BgL_arg1807z00_2832;

								{	/* Globalize/gn.scm 82 */
									obj_t BgL_tmpz00_3376;

									BgL_tmpz00_3376 =
										((obj_t) ((BgL_objectz00_bglt) BgL_varz00_2830));
									BgL_arg1807z00_2832 = (BgL_objectz00_bglt) (BgL_tmpz00_3376);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Globalize/gn.scm 82 */
										long BgL_idxz00_2833;

										BgL_idxz00_2833 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2832);
										BgL_test1943z00_3375 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2833 + 2L)) == BgL_classz00_2831);
									}
								else
									{	/* Globalize/gn.scm 82 */
										bool_t BgL_res1855z00_2836;

										{	/* Globalize/gn.scm 82 */
											obj_t BgL_oclassz00_2837;

											{	/* Globalize/gn.scm 82 */
												obj_t BgL_arg1815z00_2838;
												long BgL_arg1816z00_2839;

												BgL_arg1815z00_2838 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Globalize/gn.scm 82 */
													long BgL_arg1817z00_2840;

													BgL_arg1817z00_2840 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2832);
													BgL_arg1816z00_2839 =
														(BgL_arg1817z00_2840 - OBJECT_TYPE);
												}
												BgL_oclassz00_2837 =
													VECTOR_REF(BgL_arg1815z00_2838, BgL_arg1816z00_2839);
											}
											{	/* Globalize/gn.scm 82 */
												bool_t BgL__ortest_1115z00_2841;

												BgL__ortest_1115z00_2841 =
													(BgL_classz00_2831 == BgL_oclassz00_2837);
												if (BgL__ortest_1115z00_2841)
													{	/* Globalize/gn.scm 82 */
														BgL_res1855z00_2836 = BgL__ortest_1115z00_2841;
													}
												else
													{	/* Globalize/gn.scm 82 */
														long BgL_odepthz00_2842;

														{	/* Globalize/gn.scm 82 */
															obj_t BgL_arg1804z00_2843;

															BgL_arg1804z00_2843 = (BgL_oclassz00_2837);
															BgL_odepthz00_2842 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2843);
														}
														if ((2L < BgL_odepthz00_2842))
															{	/* Globalize/gn.scm 82 */
																obj_t BgL_arg1802z00_2844;

																{	/* Globalize/gn.scm 82 */
																	obj_t BgL_arg1803z00_2845;

																	BgL_arg1803z00_2845 = (BgL_oclassz00_2837);
																	BgL_arg1802z00_2844 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2845,
																		2L);
																}
																BgL_res1855z00_2836 =
																	(BgL_arg1802z00_2844 == BgL_classz00_2831);
															}
														else
															{	/* Globalize/gn.scm 82 */
																BgL_res1855z00_2836 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1943z00_3375 = BgL_res1855z00_2836;
									}
							}
						}
						if (BgL_test1943z00_3375)
							{	/* Globalize/gn.scm 84 */
								bool_t BgL_test1947z00_3399;

								{
									BgL_localzf2ginfozf2_bglt BgL_auxz00_3400;

									{
										obj_t BgL_auxz00_3401;

										{	/* Globalize/gn.scm 84 */
											BgL_objectz00_bglt BgL_tmpz00_3402;

											BgL_tmpz00_3402 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt) BgL_varz00_2830));
											BgL_auxz00_3401 = BGL_OBJECT_WIDENING(BgL_tmpz00_3402);
										}
										BgL_auxz00_3400 =
											((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3401);
									}
									BgL_test1947z00_3399 =
										(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3400))->
										BgL_escapezf3zf3);
								}
								if (BgL_test1947z00_3399)
									{	/* Globalize/gn.scm 85 */
										bool_t BgL_test1948z00_3408;

										{	/* Globalize/gn.scm 85 */
											BgL_sfunz00_bglt BgL_oz00_2846;

											BgL_oz00_2846 =
												((BgL_sfunz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_varz00_2830))))->
													BgL_valuez00));
											{
												BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3413;

												{
													obj_t BgL_auxz00_3414;

													{	/* Globalize/gn.scm 85 */
														BgL_objectz00_bglt BgL_tmpz00_3415;

														BgL_tmpz00_3415 =
															((BgL_objectz00_bglt) BgL_oz00_2846);
														BgL_auxz00_3414 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3415);
													}
													BgL_auxz00_3413 =
														((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3414);
												}
												BgL_test1948z00_3408 =
													(((BgL_sfunzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_3413))->BgL_gzf3zf3);
											}
										}
										if (BgL_test1948z00_3408)
											{	/* Globalize/gn.scm 85 */
												BgL_test1942z00_3374 = ((bool_t) 0);
											}
										else
											{	/* Globalize/gn.scm 85 */
												BgL_test1942z00_3374 = ((bool_t) 1);
											}
									}
								else
									{	/* Globalize/gn.scm 84 */
										BgL_test1942z00_3374 = ((bool_t) 0);
									}
							}
						else
							{	/* Globalize/gn.scm 82 */
								BgL_test1942z00_3374 = ((bool_t) 0);
							}
					}
					if (BgL_test1942z00_3374)
						{	/* Globalize/gn.scm 82 */
							{	/* Globalize/gn.scm 87 */
								BgL_valuez00_bglt BgL_arg1437z00_2847;

								BgL_arg1437z00_2847 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_varz00_2830))))->BgL_valuez00);
								{
									BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3423;

									{
										obj_t BgL_auxz00_3424;

										{	/* Globalize/gn.scm 87 */
											BgL_objectz00_bglt BgL_tmpz00_3425;

											BgL_tmpz00_3425 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_arg1437z00_2847));
											BgL_auxz00_3424 = BGL_OBJECT_WIDENING(BgL_tmpz00_3425);
										}
										BgL_auxz00_3423 =
											((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3424);
									}
									((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3423))->
											BgL_gzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
								}
							}
							return MAKE_YOUNG_PAIR(((obj_t) BgL_varz00_2830), BgL_gz00_2727);
						}
					else
						{	/* Globalize/gn.scm 82 */
							return BgL_gz00_2727;
						}
				}
			}
		}

	}



/* &E-var1273 */
	obj_t BGl_z62Ezd2var1273zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2728,
		obj_t BgL_nodez00_2729, obj_t BgL_callerz00_2730, obj_t BgL_gz00_2731)
	{
		{	/* Globalize/gn.scm 73 */
			return BgL_gz00_2731;
		}

	}



/* &E-kwote1271 */
	obj_t BGl_z62Ezd2kwote1271zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2732,
		obj_t BgL_nodez00_2733, obj_t BgL_callerz00_2734, obj_t BgL_gz00_2735)
	{
		{	/* Globalize/gn.scm 67 */
			return BgL_gz00_2735;
		}

	}



/* &E-atom1269 */
	obj_t BGl_z62Ezd2atom1269zb0zzglobaliza7e_gnza7(obj_t BgL_envz00_2736,
		obj_t BgL_nodez00_2737, obj_t BgL_callerz00_2738, obj_t BgL_gz00_2739)
	{
		{	/* Globalize/gn.scm 61 */
			return BgL_gz00_2739;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_gnza7(void)
	{
		{	/* Globalize/gn.scm 23 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1884z00zzglobaliza7e_gnza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1884z00zzglobaliza7e_gnza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1884z00zzglobaliza7e_gnza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1884z00zzglobaliza7e_gnza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1884z00zzglobaliza7e_gnza7));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1884z00zzglobaliza7e_gnza7));
			return
				BGl_modulezd2initializa7ationz75zzglobaliza7e_globaliza7ez00(34590581L,
				BSTRING_TO_STRING(BGl_string1884z00zzglobaliza7e_gnza7));
		}

	}

#ifdef __cplusplus
}
#endif
