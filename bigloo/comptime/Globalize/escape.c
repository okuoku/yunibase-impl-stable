/*===========================================================================*/
/*   (Globalize/escape.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/escape.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_ESCAPE_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_ESCAPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_svarzf2ginfozf2_bgl
	{
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
		bool_t BgL_celledzf3zf3;
		bool_t BgL_stackablez00;
	}                      *BgL_svarzf2ginfozf2_bglt;

	typedef struct BgL_sexitzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
	}                       *BgL_sexitzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;

	typedef struct BgL_globalzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		obj_t BgL_globalzd2closurezd2;
	}                        *BgL_globalzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_ESCAPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62setzd2escapingzd2funz12z70zzglobaliza7e_escapeza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62escapezd2funz12zd2local1321z70zzglobaliza7e_escapeza7(obj_t, obj_t);
	static obj_t BGl_z62escapez12z70zzglobaliza7e_escapeza7(obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_escapeza7 =
		BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t
		BGl_z62setzd2escapingzd2funz12zd2lo1329za2zzglobaliza7e_escapeza7(obj_t,
		obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62escapez12zd2sequence1342za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_escapeza7(void);
	static obj_t
		BGl_z62escapez12zd2setzd2exzd2it1368za2zzglobaliza7e_escapeza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t
		BGl_z62setzd2escapingzd2funz121322z70zzglobaliza7e_escapeza7(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_escapeza7(void);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_escapeza7(void);
	static obj_t BGl_z62escapez121330z70zzglobaliza7e_escapeza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62escapez12zd2kwote1335za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2funcall1350za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2letzd2var1366z70zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_escapeza7(void);
	static obj_t BGl_z62escapez12zd2closure1340za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62escapez12zd2atom1333za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2boxzd2ref1380z70zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2appzd2ly1348z70zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_nodez00_bglt,
		BgL_variablez00_bglt);
	static bool_t BGl_escapeza2z12zb0zzglobaliza7e_escapeza7(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_escapezd2funz12zc0zzglobaliza7e_escapeza7(BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_escapeza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62escapez12zd2var1337za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t
		BGl_setzd2escapingzd2funz12z12zzglobaliza7e_escapeza7(BgL_variablez00_bglt);
	static obj_t BGl_z62escapez12zd2extern1352za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_escapeza7(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_escapeza7(void);
	static obj_t BGl_z62escapez12zd2switch1362za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_escapeza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_escapeza7(void);
	static obj_t BGl_z62escapezd2funz121316za2zzglobaliza7e_escapeza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62escapez12zd2conditional1358za2zzglobaliza7e_escapeza7(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t BGl_z62escapez12zd2app1346za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62escapezd2funz12za2zzglobaliza7e_escapeza7(obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2cast1354za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_variablez00zzast_varz00;
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t
		BGl_z62escapezd2funz12zd2global1319z70zzglobaliza7e_escapeza7(obj_t, obj_t);
	static obj_t
		BGl_z62escapez12zd2boxzd2setz121377z62zzglobaliza7e_escapeza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62escapez12zd2makezd2box1374z70zzglobaliza7e_escapeza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62escapez12zd2setq1356za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t
		BGl_z62escapez12zd2jumpzd2exzd2it1371za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62setzd2escapingzd2funz12zd2gl1325za2zzglobaliza7e_escapeza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62setzd2escapingzd2funz12zd2gl1327za2zzglobaliza7e_escapeza7(obj_t,
		obj_t);
	static obj_t BGl_z62escapez12zd2sync1344za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62escapez12zd2letzd2fun1364z70zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62escapez12zd2fail1360za2zzglobaliza7e_escapeza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string1900z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1900za700za7za7g1903za7, "globalize_escape", 16);
	      DEFINE_STRING(BGl_string1901z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1901za700za7za7g1904za7, "export done escape!1330 ", 24);
	      DEFINE_STRING(BGl_string1863z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1863za700za7za7g1905za7, "escape-fun!1316", 15);
	      DEFINE_STRING(BGl_string1865z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1865za700za7za7g1906za7, "set-escaping-fun!1322", 21);
	      DEFINE_STRING(BGl_string1867z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1867za700za7za7g1907za7, "escape!1330", 11);
	      DEFINE_STRING(BGl_string1868z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1868za700za7za7g1908za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1869z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1869za700za7za7g1909za7, "set-escaping-fun!", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1862z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza7d2funza71910za7,
		BGl_z62escapezd2funz121316za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1870z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1870za700za7za7g1911za7, "Illegal variable", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1864z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762setza7d2escapin1912z00,
		BGl_z62setzd2escapingzd2funz121322z70zzglobaliza7e_escapeza7, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string1872z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1872za700za7za7g1913za7, "escape-fun!", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1866z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza71213301914z00,
		BGl_z62escapez121330z70zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1878z00zzglobaliza7e_escapeza7,
		BgL_bgl_string1878za700za7za7g1915za7, "escape!", 7);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_escapezd2funz12zd2envz12zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza7d2funza71916za7,
		BGl_z62escapezd2funz12za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza7d2funza71917za7,
		BGl_z62escapezd2funz12zd2global1319z70zzglobaliza7e_escapeza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza7d2funza71918za7,
		BGl_z62escapezd2funz12zd2local1321z70zzglobaliza7e_escapeza7, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762setza7d2escapin1919z00,
		BGl_z62setzd2escapingzd2funz12zd2gl1325za2zzglobaliza7e_escapeza7, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762setza7d2escapin1920z00,
		BGl_z62setzd2escapingzd2funz12zd2gl1327za2zzglobaliza7e_escapeza7, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762setza7d2escapin1921z00,
		BGl_z62setzd2escapingzd2funz12zd2lo1329za2zzglobaliza7e_escapeza7, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2a1922za7,
		BGl_z62escapez12zd2atom1333za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2k1923za7,
		BGl_z62escapez12zd2kwote1335za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2v1924za7,
		BGl_z62escapez12zd2var1337za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2c1925za7,
		BGl_z62escapez12zd2closure1340za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2s1926za7,
		BGl_z62escapez12zd2sequence1342za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2s1927za7,
		BGl_z62escapez12zd2sync1344za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1884z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2a1928za7,
		BGl_z62escapez12zd2app1346za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1885z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2a1929za7,
		BGl_z62escapez12zd2appzd2ly1348z70zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2f1930za7,
		BGl_z62escapez12zd2funcall1350za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1887z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2e1931za7,
		BGl_z62escapez12zd2extern1352za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2c1932za7,
		BGl_z62escapez12zd2cast1354za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1889z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2s1933za7,
		BGl_z62escapez12zd2setq1356za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1890z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2c1934za7,
		BGl_z62escapez12zd2conditional1358za2zzglobaliza7e_escapeza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2f1935za7,
		BGl_z62escapez12zd2fail1360za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1892z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2s1936za7,
		BGl_z62escapez12zd2switch1362za2zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1893z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2l1937za7,
		BGl_z62escapez12zd2letzd2fun1364z70zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1894z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2l1938za7,
		BGl_z62escapez12zd2letzd2var1366z70zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1895z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2s1939za7,
		BGl_z62escapez12zd2setzd2exzd2it1368za2zzglobaliza7e_escapeza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1896z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2j1940za7,
		BGl_z62escapez12zd2jumpzd2exzd2it1371za2zzglobaliza7e_escapeza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1897z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2m1941za7,
		BGl_z62escapez12zd2makezd2box1374z70zzglobaliza7e_escapeza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1898z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2b1942za7,
		BGl_z62escapez12zd2boxzd2setz121377z62zzglobaliza7e_escapeza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1899z00zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za7d2b1943za7,
		BGl_z62escapez12zd2boxzd2ref1380z70zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
		BgL_bgl_za762escapeza712za770za71944z00,
		BGl_z62escapez12z70zzglobaliza7e_escapeza7, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_setzd2escapingzd2funz12zd2envzc0zzglobaliza7e_escapeza7,
		BgL_bgl_za762setza7d2escapin1945z00,
		BGl_z62setzd2escapingzd2funz12z70zzglobaliza7e_escapeza7, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_escapeza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_escapeza7(long
		BgL_checksumz00_2928, char *BgL_fromz00_2929)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_escapeza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_escapeza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_escapeza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_escapeza7();
					BGl_cnstzd2initzd2zzglobaliza7e_escapeza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_escapeza7();
					BGl_genericzd2initzd2zzglobaliza7e_escapeza7();
					BGl_methodzd2initzd2zzglobaliza7e_escapeza7();
					return BGl_toplevelzd2initzd2zzglobaliza7e_escapeza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_escapeza7(void)
	{
		{	/* Globalize/escape.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_escape");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_escape");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_escape");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "globalize_escape");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"globalize_escape");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_escape");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_escape");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "globalize_escape");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_escape");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_escape");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_escapeza7(void)
	{
		{	/* Globalize/escape.scm 15 */
			{	/* Globalize/escape.scm 15 */
				obj_t BgL_cportz00_2771;

				{	/* Globalize/escape.scm 15 */
					obj_t BgL_stringz00_2778;

					BgL_stringz00_2778 = BGl_string1901z00zzglobaliza7e_escapeza7;
					{	/* Globalize/escape.scm 15 */
						obj_t BgL_startz00_2779;

						BgL_startz00_2779 = BINT(0L);
						{	/* Globalize/escape.scm 15 */
							obj_t BgL_endz00_2780;

							BgL_endz00_2780 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2778)));
							{	/* Globalize/escape.scm 15 */

								BgL_cportz00_2771 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2778, BgL_startz00_2779, BgL_endz00_2780);
				}}}}
				{
					long BgL_iz00_2772;

					BgL_iz00_2772 = 2L;
				BgL_loopz00_2773:
					if ((BgL_iz00_2772 == -1L))
						{	/* Globalize/escape.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/escape.scm 15 */
							{	/* Globalize/escape.scm 15 */
								obj_t BgL_arg1902z00_2774;

								{	/* Globalize/escape.scm 15 */

									{	/* Globalize/escape.scm 15 */
										obj_t BgL_locationz00_2776;

										BgL_locationz00_2776 = BBOOL(((bool_t) 0));
										{	/* Globalize/escape.scm 15 */

											BgL_arg1902z00_2774 =
												BGl_readz00zz__readerz00(BgL_cportz00_2771,
												BgL_locationz00_2776);
										}
									}
								}
								{	/* Globalize/escape.scm 15 */
									int BgL_tmpz00_2959;

									BgL_tmpz00_2959 = (int) (BgL_iz00_2772);
									CNST_TABLE_SET(BgL_tmpz00_2959, BgL_arg1902z00_2774);
							}}
							{	/* Globalize/escape.scm 15 */
								int BgL_auxz00_2777;

								BgL_auxz00_2777 = (int) ((BgL_iz00_2772 - 1L));
								{
									long BgL_iz00_2964;

									BgL_iz00_2964 = (long) (BgL_auxz00_2777);
									BgL_iz00_2772 = BgL_iz00_2964;
									goto BgL_loopz00_2773;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_escapeza7(void)
	{
		{	/* Globalize/escape.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_escapeza7(void)
	{
		{	/* Globalize/escape.scm 15 */
			return BUNSPEC;
		}

	}



/* escape*! */
	bool_t BGl_escapeza2z12zb0zzglobaliza7e_escapeza7(obj_t BgL_nodeza2za2_56,
		obj_t BgL_oz00_57)
	{
		{	/* Globalize/escape.scm 267 */
			{
				obj_t BgL_l1314z00_1751;

				BgL_l1314z00_1751 = BgL_nodeza2za2_56;
			BgL_zc3z04anonymousza31411ze3z87_1752:
				if (PAIRP(BgL_l1314z00_1751))
					{	/* Globalize/escape.scm 268 */
						{	/* Globalize/escape.scm 268 */
							obj_t BgL_nz00_1754;

							BgL_nz00_1754 = CAR(BgL_l1314z00_1751);
							BGl_escapez12z12zzglobaliza7e_escapeza7(
								((BgL_nodez00_bglt) BgL_nz00_1754),
								((BgL_variablez00_bglt) BgL_oz00_57));
						}
						{
							obj_t BgL_l1314z00_2973;

							BgL_l1314z00_2973 = CDR(BgL_l1314z00_1751);
							BgL_l1314z00_1751 = BgL_l1314z00_2973;
							goto BgL_zc3z04anonymousza31411ze3z87_1752;
						}
					}
				else
					{	/* Globalize/escape.scm 268 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_escapeza7(void)
	{
		{	/* Globalize/escape.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_escapeza7(void)
	{
		{	/* Globalize/escape.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_escapezd2funz12zd2envz12zzglobaliza7e_escapeza7,
				BGl_proc1862z00zzglobaliza7e_escapeza7, BGl_variablez00zzast_varz00,
				BGl_string1863z00zzglobaliza7e_escapeza7);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_setzd2escapingzd2funz12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_proc1864z00zzglobaliza7e_escapeza7, BGl_variablez00zzast_varz00,
				BGl_string1865z00zzglobaliza7e_escapeza7);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_proc1866z00zzglobaliza7e_escapeza7, BGl_nodez00zzast_nodez00,
				BGl_string1867z00zzglobaliza7e_escapeza7);
		}

	}



/* &escape!1330 */
	obj_t BGl_z62escapez121330z70zzglobaliza7e_escapeza7(obj_t BgL_envz00_2654,
		obj_t BgL_nodez00_2655, obj_t BgL_oz00_2656)
	{
		{	/* Globalize/escape.scm 79 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string1868z00zzglobaliza7e_escapeza7,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2655)));
		}

	}



/* &set-escaping-fun!1322 */
	obj_t BGl_z62setzd2escapingzd2funz121322z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2657, obj_t BgL_variablez00_2658)
	{
		{	/* Globalize/escape.scm 55 */
			{	/* Globalize/escape.scm 56 */
				obj_t BgL_arg1472z00_2785;

				BgL_arg1472z00_2785 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_variablez00_bglt) BgL_variablez00_2658)));
				return
					BGl_errorz00zz__errorz00(BGl_string1869z00zzglobaliza7e_escapeza7,
					BGl_string1870z00zzglobaliza7e_escapeza7, BgL_arg1472z00_2785);
			}
		}

	}



/* &escape-fun!1316 */
	obj_t BGl_z62escapezd2funz121316za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2659, obj_t BgL_variablez00_2660)
	{
		{	/* Globalize/escape.scm 27 */
			{	/* Globalize/escape.scm 28 */
				BgL_valuez00_bglt BgL_funz00_2787;

				BgL_funz00_2787 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_variablez00_2660)))->BgL_valuez00);
				{	/* Globalize/escape.scm 29 */
					obj_t BgL_g1298z00_2788;

					BgL_g1298z00_2788 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_2787)))->BgL_argsz00);
					{
						obj_t BgL_l1296z00_2790;

						BgL_l1296z00_2790 = BgL_g1298z00_2788;
					BgL_zc3z04anonymousza31423ze3z87_2789:
						if (PAIRP(BgL_l1296z00_2790))
							{	/* Globalize/escape.scm 31 */
								{	/* Globalize/escape.scm 30 */
									obj_t BgL_localz00_2791;

									BgL_localz00_2791 = CAR(BgL_l1296z00_2790);
									{	/* Globalize/escape.scm 30 */
										BgL_svarz00_bglt BgL_tmp1107z00_2792;

										BgL_tmp1107z00_2792 =
											((BgL_svarz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_localz00_2791))))->
												BgL_valuez00));
										{	/* Globalize/escape.scm 30 */
											BgL_svarzf2ginfozf2_bglt BgL_wide1109z00_2793;

											BgL_wide1109z00_2793 =
												((BgL_svarzf2ginfozf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_svarzf2ginfozf2_bgl))));
											{	/* Globalize/escape.scm 30 */
												obj_t BgL_auxz00_3001;
												BgL_objectz00_bglt BgL_tmpz00_2998;

												BgL_auxz00_3001 = ((obj_t) BgL_wide1109z00_2793);
												BgL_tmpz00_2998 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1107z00_2792));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2998,
													BgL_auxz00_3001);
											}
											((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1107z00_2792));
											{	/* Globalize/escape.scm 30 */
												long BgL_arg1434z00_2794;

												BgL_arg1434z00_2794 =
													BGL_CLASS_NUM
													(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_svarz00_bglt) BgL_tmp1107z00_2792)),
													BgL_arg1434z00_2794);
											}
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1107z00_2792));
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3012;

											{
												obj_t BgL_auxz00_3013;

												{	/* Globalize/escape.scm 30 */
													BgL_objectz00_bglt BgL_tmpz00_3014;

													BgL_tmpz00_3014 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1107z00_2792));
													BgL_auxz00_3013 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3014);
												}
												BgL_auxz00_3012 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3013);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3012))->
													BgL_kapturedzf3zf3) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3020;

											{
												obj_t BgL_auxz00_3021;

												{	/* Globalize/escape.scm 30 */
													BgL_objectz00_bglt BgL_tmpz00_3022;

													BgL_tmpz00_3022 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1107z00_2792));
													BgL_auxz00_3021 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3022);
												}
												BgL_auxz00_3020 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3021);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3020))->
													BgL_freezd2markzd2) = ((long) -10L), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3028;

											{
												obj_t BgL_auxz00_3029;

												{	/* Globalize/escape.scm 30 */
													BgL_objectz00_bglt BgL_tmpz00_3030;

													BgL_tmpz00_3030 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1107z00_2792));
													BgL_auxz00_3029 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3030);
												}
												BgL_auxz00_3028 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3029);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3028))->
													BgL_markz00) = ((long) -10L), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3036;

											{
												obj_t BgL_auxz00_3037;

												{	/* Globalize/escape.scm 30 */
													BgL_objectz00_bglt BgL_tmpz00_3038;

													BgL_tmpz00_3038 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1107z00_2792));
													BgL_auxz00_3037 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3038);
												}
												BgL_auxz00_3036 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3037);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3036))->
													BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3044;

											{
												obj_t BgL_auxz00_3045;

												{	/* Globalize/escape.scm 30 */
													BgL_objectz00_bglt BgL_tmpz00_3046;

													BgL_tmpz00_3046 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1107z00_2792));
													BgL_auxz00_3045 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3046);
												}
												BgL_auxz00_3044 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3045);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3044))->
													BgL_stackablez00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
										}
										((BgL_svarz00_bglt) BgL_tmp1107z00_2792);
								}}
								{
									obj_t BgL_l1296z00_3053;

									BgL_l1296z00_3053 = CDR(BgL_l1296z00_2790);
									BgL_l1296z00_2790 = BgL_l1296z00_3053;
									goto BgL_zc3z04anonymousza31423ze3z87_2789;
								}
							}
						else
							{	/* Globalize/escape.scm 31 */
								((bool_t) 1);
							}
					}
				}
				{	/* Globalize/escape.scm 32 */
					BgL_sfunzf2ginfozf2_bglt BgL_wide1113z00_2795;

					BgL_wide1113z00_2795 =
						((BgL_sfunzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sfunzf2ginfozf2_bgl))));
					{	/* Globalize/escape.scm 32 */
						obj_t BgL_auxz00_3060;
						BgL_objectz00_bglt BgL_tmpz00_3056;

						BgL_auxz00_3060 = ((obj_t) BgL_wide1113z00_2795);
						BgL_tmpz00_3056 =
							((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3056, BgL_auxz00_3060);
					}
					((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
					{	/* Globalize/escape.scm 32 */
						long BgL_arg1448z00_2796;

						BgL_arg1448z00_2796 =
							BGL_CLASS_NUM(BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt)
									((BgL_sfunz00_bglt) BgL_funz00_2787))), BgL_arg1448z00_2796);
					}
					((BgL_sfunz00_bglt)
						((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3074;

					{
						obj_t BgL_auxz00_3075;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3076;

							BgL_tmpz00_3076 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3075 = BGL_OBJECT_WIDENING(BgL_tmpz00_3076);
						}
						BgL_auxz00_3074 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3075);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3074))->
							BgL_gzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3083;

					{
						obj_t BgL_auxz00_3084;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3085;

							BgL_tmpz00_3085 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3084 = BGL_OBJECT_WIDENING(BgL_tmpz00_3085);
						}
						BgL_auxz00_3083 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3084);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3083))->
							BgL_cfromz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3092;

					{
						obj_t BgL_auxz00_3093;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3094;

							BgL_tmpz00_3094 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3093 = BGL_OBJECT_WIDENING(BgL_tmpz00_3094);
						}
						BgL_auxz00_3092 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3093);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3092))->
							BgL_cfromza2za2) = ((obj_t) BFALSE), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3101;

					{
						obj_t BgL_auxz00_3102;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3103;

							BgL_tmpz00_3103 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3102 = BGL_OBJECT_WIDENING(BgL_tmpz00_3103);
						}
						BgL_auxz00_3101 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3102);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3101))->BgL_ctoz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3110;

					{
						obj_t BgL_auxz00_3111;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3112;

							BgL_tmpz00_3112 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3111 = BGL_OBJECT_WIDENING(BgL_tmpz00_3112);
						}
						BgL_auxz00_3110 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3111);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3110))->
							BgL_ctoza2za2) = ((obj_t) BFALSE), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3119;

					{
						obj_t BgL_auxz00_3120;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3121;

							BgL_tmpz00_3121 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3120 = BGL_OBJECT_WIDENING(BgL_tmpz00_3121);
						}
						BgL_auxz00_3119 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3120);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3119))->
							BgL_efunctionsz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3128;

					{
						obj_t BgL_auxz00_3129;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3130;

							BgL_tmpz00_3130 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3129 = BGL_OBJECT_WIDENING(BgL_tmpz00_3130);
						}
						BgL_auxz00_3128 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3129);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3128))->
							BgL_integratorz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3137;

					{
						obj_t BgL_auxz00_3138;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3139;

							BgL_tmpz00_3139 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3138 = BGL_OBJECT_WIDENING(BgL_tmpz00_3139);
						}
						BgL_auxz00_3137 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3138);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3137))->
							BgL_imarkz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3146;

					{
						obj_t BgL_auxz00_3147;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3148;

							BgL_tmpz00_3148 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3147 = BGL_OBJECT_WIDENING(BgL_tmpz00_3148);
						}
						BgL_auxz00_3146 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3147);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3146))->
							BgL_ownerz00) = ((obj_t) BFALSE), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3155;

					{
						obj_t BgL_auxz00_3156;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3157;

							BgL_tmpz00_3157 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3156 = BGL_OBJECT_WIDENING(BgL_tmpz00_3157);
						}
						BgL_auxz00_3155 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3156);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3155))->
							BgL_integratedz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3164;

					{
						obj_t BgL_auxz00_3165;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3166;

							BgL_tmpz00_3166 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3165 = BGL_OBJECT_WIDENING(BgL_tmpz00_3166);
						}
						BgL_auxz00_3164 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3165);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3164))->
							BgL_pluggedzd2inzd2) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3173;

					{
						obj_t BgL_auxz00_3174;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3175;

							BgL_tmpz00_3175 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3174 = BGL_OBJECT_WIDENING(BgL_tmpz00_3175);
						}
						BgL_auxz00_3173 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3174);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3173))->
							BgL_markz00) = ((long) -10L), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3182;

					{
						obj_t BgL_auxz00_3183;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3184;

							BgL_tmpz00_3184 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3183 = BGL_OBJECT_WIDENING(BgL_tmpz00_3184);
						}
						BgL_auxz00_3182 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3183);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3182))->
							BgL_freezd2markzd2) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3191;

					{
						obj_t BgL_auxz00_3192;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3193;

							BgL_tmpz00_3193 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3192 = BGL_OBJECT_WIDENING(BgL_tmpz00_3193);
						}
						BgL_auxz00_3191 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3192);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3191))->
							BgL_thezd2globalzd2) = ((obj_t) BFALSE), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3200;

					{
						obj_t BgL_auxz00_3201;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3202;

							BgL_tmpz00_3202 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3201 = BGL_OBJECT_WIDENING(BgL_tmpz00_3202);
						}
						BgL_auxz00_3200 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3201);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3200))->
							BgL_kapturedz00) = ((obj_t) BFALSE), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3209;

					{
						obj_t BgL_auxz00_3210;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3211;

							BgL_tmpz00_3211 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3210 = BGL_OBJECT_WIDENING(BgL_tmpz00_3211);
						}
						BgL_auxz00_3209 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3210);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3209))->
							BgL_newzd2bodyzd2) = ((obj_t) BFALSE), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3218;

					{
						obj_t BgL_auxz00_3219;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3220;

							BgL_tmpz00_3220 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3219 = BGL_OBJECT_WIDENING(BgL_tmpz00_3220);
						}
						BgL_auxz00_3218 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3219);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3218))->
							BgL_bmarkz00) = ((long) -10L), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3227;

					{
						obj_t BgL_auxz00_3228;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3229;

							BgL_tmpz00_3229 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3228 = BGL_OBJECT_WIDENING(BgL_tmpz00_3229);
						}
						BgL_auxz00_3227 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3228);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3227))->
							BgL_umarkz00) = ((long) -10L), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3236;

					{
						obj_t BgL_auxz00_3237;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3238;

							BgL_tmpz00_3238 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3237 = BGL_OBJECT_WIDENING(BgL_tmpz00_3238);
						}
						BgL_auxz00_3236 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3237);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3236))->
							BgL_freez00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3245;

					{
						obj_t BgL_auxz00_3246;

						{	/* Globalize/escape.scm 28 */
							BgL_objectz00_bglt BgL_tmpz00_3247;

							BgL_tmpz00_3247 =
								((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787)));
							BgL_auxz00_3246 = BGL_OBJECT_WIDENING(BgL_tmpz00_3247);
						}
						BgL_auxz00_3245 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3246);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3245))->
							BgL_boundz00) = ((obj_t) BNIL), BUNSPEC);
				}
				((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2787));
				{	/* Globalize/escape.scm 33 */
					obj_t BgL_arg1453z00_2797;

					BgL_arg1453z00_2797 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_2787)))->BgL_bodyz00);
					return
						BGl_escapez12z12zzglobaliza7e_escapeza7(
						((BgL_nodez00_bglt) BgL_arg1453z00_2797),
						((BgL_variablez00_bglt) BgL_variablez00_2660));
				}
			}
		}

	}



/* escape-fun! */
	BGL_EXPORTED_DEF obj_t
		BGl_escapezd2funz12zc0zzglobaliza7e_escapeza7(BgL_variablez00_bglt
		BgL_variablez00_3)
	{
		{	/* Globalize/escape.scm 27 */
			{	/* Globalize/escape.scm 27 */
				obj_t BgL_method1317z00_1791;

				{	/* Globalize/escape.scm 27 */
					obj_t BgL_res1847z00_2318;

					{	/* Globalize/escape.scm 27 */
						long BgL_objzd2classzd2numz00_2289;

						BgL_objzd2classzd2numz00_2289 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_variablez00_3));
						{	/* Globalize/escape.scm 27 */
							obj_t BgL_arg1811z00_2290;

							BgL_arg1811z00_2290 =
								PROCEDURE_REF
								(BGl_escapezd2funz12zd2envz12zzglobaliza7e_escapeza7,
								(int) (1L));
							{	/* Globalize/escape.scm 27 */
								int BgL_offsetz00_2293;

								BgL_offsetz00_2293 = (int) (BgL_objzd2classzd2numz00_2289);
								{	/* Globalize/escape.scm 27 */
									long BgL_offsetz00_2294;

									BgL_offsetz00_2294 =
										((long) (BgL_offsetz00_2293) - OBJECT_TYPE);
									{	/* Globalize/escape.scm 27 */
										long BgL_modz00_2295;

										BgL_modz00_2295 =
											(BgL_offsetz00_2294 >> (int) ((long) ((int) (4L))));
										{	/* Globalize/escape.scm 27 */
											long BgL_restz00_2297;

											BgL_restz00_2297 =
												(BgL_offsetz00_2294 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Globalize/escape.scm 27 */

												{	/* Globalize/escape.scm 27 */
													obj_t BgL_bucketz00_2299;

													BgL_bucketz00_2299 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2290), BgL_modz00_2295);
													BgL_res1847z00_2318 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2299), BgL_restz00_2297);
					}}}}}}}}
					BgL_method1317z00_1791 = BgL_res1847z00_2318;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1317z00_1791,
					((obj_t) BgL_variablez00_3));
			}
		}

	}



/* &escape-fun! */
	obj_t BGl_z62escapezd2funz12za2zzglobaliza7e_escapeza7(obj_t BgL_envz00_2661,
		obj_t BgL_variablez00_2662)
	{
		{	/* Globalize/escape.scm 27 */
			return
				BGl_escapezd2funz12zc0zzglobaliza7e_escapeza7(
				((BgL_variablez00_bglt) BgL_variablez00_2662));
		}

	}



/* set-escaping-fun! */
	obj_t
		BGl_setzd2escapingzd2funz12z12zzglobaliza7e_escapeza7(BgL_variablez00_bglt
		BgL_variablez00_6)
	{
		{	/* Globalize/escape.scm 55 */
			{	/* Globalize/escape.scm 55 */
				obj_t BgL_method1323z00_1792;

				{	/* Globalize/escape.scm 55 */
					obj_t BgL_res1852z00_2349;

					{	/* Globalize/escape.scm 55 */
						long BgL_objzd2classzd2numz00_2320;

						BgL_objzd2classzd2numz00_2320 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_variablez00_6));
						{	/* Globalize/escape.scm 55 */
							obj_t BgL_arg1811z00_2321;

							BgL_arg1811z00_2321 =
								PROCEDURE_REF
								(BGl_setzd2escapingzd2funz12zd2envzc0zzglobaliza7e_escapeza7,
								(int) (1L));
							{	/* Globalize/escape.scm 55 */
								int BgL_offsetz00_2324;

								BgL_offsetz00_2324 = (int) (BgL_objzd2classzd2numz00_2320);
								{	/* Globalize/escape.scm 55 */
									long BgL_offsetz00_2325;

									BgL_offsetz00_2325 =
										((long) (BgL_offsetz00_2324) - OBJECT_TYPE);
									{	/* Globalize/escape.scm 55 */
										long BgL_modz00_2326;

										BgL_modz00_2326 =
											(BgL_offsetz00_2325 >> (int) ((long) ((int) (4L))));
										{	/* Globalize/escape.scm 55 */
											long BgL_restz00_2328;

											BgL_restz00_2328 =
												(BgL_offsetz00_2325 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Globalize/escape.scm 55 */

												{	/* Globalize/escape.scm 55 */
													obj_t BgL_bucketz00_2330;

													BgL_bucketz00_2330 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2321), BgL_modz00_2326);
													BgL_res1852z00_2349 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2330), BgL_restz00_2328);
					}}}}}}}}
					BgL_method1323z00_1792 = BgL_res1852z00_2349;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1323z00_1792,
					((obj_t) BgL_variablez00_6));
			}
		}

	}



/* &set-escaping-fun! */
	obj_t BGl_z62setzd2escapingzd2funz12z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2663, obj_t BgL_variablez00_2664)
	{
		{	/* Globalize/escape.scm 55 */
			return
				BGl_setzd2escapingzd2funz12z12zzglobaliza7e_escapeza7(
				((BgL_variablez00_bglt) BgL_variablez00_2664));
		}

	}



/* escape! */
	obj_t BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_nodez00_bglt BgL_nodez00_10,
		BgL_variablez00_bglt BgL_oz00_11)
	{
		{	/* Globalize/escape.scm 79 */
			{	/* Globalize/escape.scm 79 */
				obj_t BgL_method1331z00_1793;

				{	/* Globalize/escape.scm 79 */
					obj_t BgL_res1857z00_2380;

					{	/* Globalize/escape.scm 79 */
						long BgL_objzd2classzd2numz00_2351;

						BgL_objzd2classzd2numz00_2351 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_10));
						{	/* Globalize/escape.scm 79 */
							obj_t BgL_arg1811z00_2352;

							BgL_arg1811z00_2352 =
								PROCEDURE_REF(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
								(int) (1L));
							{	/* Globalize/escape.scm 79 */
								int BgL_offsetz00_2355;

								BgL_offsetz00_2355 = (int) (BgL_objzd2classzd2numz00_2351);
								{	/* Globalize/escape.scm 79 */
									long BgL_offsetz00_2356;

									BgL_offsetz00_2356 =
										((long) (BgL_offsetz00_2355) - OBJECT_TYPE);
									{	/* Globalize/escape.scm 79 */
										long BgL_modz00_2357;

										BgL_modz00_2357 =
											(BgL_offsetz00_2356 >> (int) ((long) ((int) (4L))));
										{	/* Globalize/escape.scm 79 */
											long BgL_restz00_2359;

											BgL_restz00_2359 =
												(BgL_offsetz00_2356 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Globalize/escape.scm 79 */

												{	/* Globalize/escape.scm 79 */
													obj_t BgL_bucketz00_2361;

													BgL_bucketz00_2361 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2352), BgL_modz00_2357);
													BgL_res1857z00_2380 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2361), BgL_restz00_2359);
					}}}}}}}}
					BgL_method1331z00_1793 = BgL_res1857z00_2380;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1331z00_1793,
					((obj_t) BgL_nodez00_10), ((obj_t) BgL_oz00_11));
			}
		}

	}



/* &escape! */
	obj_t BGl_z62escapez12z70zzglobaliza7e_escapeza7(obj_t BgL_envz00_2665,
		obj_t BgL_nodez00_2666, obj_t BgL_oz00_2667)
	{
		{	/* Globalize/escape.scm 79 */
			return
				BGl_escapez12z12zzglobaliza7e_escapeza7(
				((BgL_nodez00_bglt) BgL_nodez00_2666),
				((BgL_variablez00_bglt) BgL_oz00_2667));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_escapeza7(void)
	{
		{	/* Globalize/escape.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapezd2funz12zd2envz12zzglobaliza7e_escapeza7,
				BGl_globalz00zzast_varz00, BGl_proc1871z00zzglobaliza7e_escapeza7,
				BGl_string1872z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapezd2funz12zd2envz12zzglobaliza7e_escapeza7,
				BGl_localz00zzast_varz00, BGl_proc1873z00zzglobaliza7e_escapeza7,
				BGl_string1872z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2escapingzd2funz12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_globalz00zzast_varz00, BGl_proc1874z00zzglobaliza7e_escapeza7,
				BGl_string1869z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2escapingzd2funz12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7,
				BGl_proc1875z00zzglobaliza7e_escapeza7,
				BGl_string1869z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2escapingzd2funz12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7,
				BGl_proc1876z00zzglobaliza7e_escapeza7,
				BGl_string1869z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_atomz00zzast_nodez00, BGl_proc1877z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_kwotez00zzast_nodez00, BGl_proc1879z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7, BGl_varz00zzast_nodez00,
				BGl_proc1880z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_closurez00zzast_nodez00, BGl_proc1881z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_sequencez00zzast_nodez00, BGl_proc1882z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_syncz00zzast_nodez00, BGl_proc1883z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7, BGl_appz00zzast_nodez00,
				BGl_proc1884z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1885z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_funcallz00zzast_nodez00, BGl_proc1886z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_externz00zzast_nodez00, BGl_proc1887z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_castz00zzast_nodez00, BGl_proc1888z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_setqz00zzast_nodez00, BGl_proc1889z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_conditionalz00zzast_nodez00, BGl_proc1890z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_failz00zzast_nodez00, BGl_proc1891z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_switchz00zzast_nodez00, BGl_proc1892z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1893z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1894z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1895z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1896z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1897z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1898z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_escapez12zd2envzc0zzglobaliza7e_escapeza7,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1899z00zzglobaliza7e_escapeza7,
				BGl_string1878z00zzglobaliza7e_escapeza7);
		}

	}



/* &escape!-box-ref1380 */
	obj_t BGl_z62escapez12zd2boxzd2ref1380z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2695, obj_t BgL_nodez00_2696, obj_t BgL_oz00_2697)
	{
		{	/* Globalize/escape.scm 260 */
			{	/* Globalize/escape.scm 262 */
				BgL_varz00_bglt BgL_arg1691z00_2799;

				BgL_arg1691z00_2799 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2696)))->BgL_varz00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(
					((BgL_nodez00_bglt) BgL_arg1691z00_2799),
					((BgL_variablez00_bglt) BgL_oz00_2697));
			}
		}

	}



/* &escape!-box-set!1377 */
	obj_t BGl_z62escapez12zd2boxzd2setz121377z62zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2698, obj_t BgL_nodez00_2699, obj_t BgL_oz00_2700)
	{
		{	/* Globalize/escape.scm 253 */
			{	/* Globalize/escape.scm 255 */
				BgL_nodez00_bglt BgL_arg1689z00_2801;

				BgL_arg1689z00_2801 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2699)))->BgL_valuez00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1689z00_2801,
					((BgL_variablez00_bglt) BgL_oz00_2700));
			}
		}

	}



/* &escape!-make-box1374 */
	obj_t BGl_z62escapez12zd2makezd2box1374z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2701, obj_t BgL_nodez00_2702, obj_t BgL_oz00_2703)
	{
		{	/* Globalize/escape.scm 247 */
			{	/* Globalize/escape.scm 248 */
				BgL_nodez00_bglt BgL_arg1688z00_2803;

				BgL_arg1688z00_2803 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2702)))->BgL_valuez00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1688z00_2803,
					((BgL_variablez00_bglt) BgL_oz00_2703));
			}
		}

	}



/* &escape!-jump-ex-it1371 */
	obj_t BGl_z62escapez12zd2jumpzd2exzd2it1371za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2704, obj_t BgL_nodez00_2705, obj_t BgL_oz00_2706)
	{
		{	/* Globalize/escape.scm 239 */
			{	/* Globalize/escape.scm 241 */
				BgL_nodez00_bglt BgL_arg1678z00_2805;

				BgL_arg1678z00_2805 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2705)))->BgL_exitz00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1678z00_2805,
					((BgL_variablez00_bglt) BgL_oz00_2706));
			}
			{	/* Globalize/escape.scm 242 */
				BgL_nodez00_bglt BgL_arg1681z00_2806;

				BgL_arg1681z00_2806 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2705)))->BgL_valuez00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1681z00_2806,
					((BgL_variablez00_bglt) BgL_oz00_2706));
			}
		}

	}



/* &escape!-set-ex-it1368 */
	obj_t BGl_z62escapez12zd2setzd2exzd2it1368za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2707, obj_t BgL_nodez00_2708, obj_t BgL_oz00_2709)
	{
		{	/* Globalize/escape.scm 230 */
			{	/* Globalize/escape.scm 232 */
				BgL_sexitz00_bglt BgL_tmp1154z00_2808;

				BgL_tmp1154z00_2808 =
					((BgL_sexitz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt)
										(((BgL_varz00_bglt) COBJECT(
													(((BgL_setzd2exzd2itz00_bglt) COBJECT(
																((BgL_setzd2exzd2itz00_bglt)
																	BgL_nodez00_2708)))->BgL_varz00)))->
											BgL_variablez00)))))->BgL_valuez00));
				{	/* Globalize/escape.scm 232 */
					BgL_sexitzf2ginfozf2_bglt BgL_wide1156z00_2809;

					BgL_wide1156z00_2809 =
						((BgL_sexitzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sexitzf2ginfozf2_bgl))));
					{	/* Globalize/escape.scm 232 */
						obj_t BgL_auxz00_3419;
						BgL_objectz00_bglt BgL_tmpz00_3416;

						BgL_auxz00_3419 = ((obj_t) BgL_wide1156z00_2809);
						BgL_tmpz00_3416 =
							((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_tmp1154z00_2808));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3416, BgL_auxz00_3419);
					}
					((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_tmp1154z00_2808));
					{	/* Globalize/escape.scm 232 */
						long BgL_arg1651z00_2810;

						BgL_arg1651z00_2810 =
							BGL_CLASS_NUM(BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_tmp1154z00_2808)),
							BgL_arg1651z00_2810);
					}
					((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_tmp1154z00_2808));
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_3430;

					{
						obj_t BgL_auxz00_3431;

						{	/* Globalize/escape.scm 232 */
							BgL_objectz00_bglt BgL_tmpz00_3432;

							BgL_tmpz00_3432 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_tmp1154z00_2808));
							BgL_auxz00_3431 = BGL_OBJECT_WIDENING(BgL_tmpz00_3432);
						}
						BgL_auxz00_3430 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_3431);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3430))->
							BgL_gzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_3438;

					{
						obj_t BgL_auxz00_3439;

						{	/* Globalize/escape.scm 232 */
							BgL_objectz00_bglt BgL_tmpz00_3440;

							BgL_tmpz00_3440 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_tmp1154z00_2808));
							BgL_auxz00_3439 = BGL_OBJECT_WIDENING(BgL_tmpz00_3440);
						}
						BgL_auxz00_3438 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_3439);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3438))->
							BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_3446;

					{
						obj_t BgL_auxz00_3447;

						{	/* Globalize/escape.scm 232 */
							BgL_objectz00_bglt BgL_tmpz00_3448;

							BgL_tmpz00_3448 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_tmp1154z00_2808));
							BgL_auxz00_3447 = BGL_OBJECT_WIDENING(BgL_tmpz00_3448);
						}
						BgL_auxz00_3446 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_3447);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3446))->
							BgL_freezd2markzd2) = ((long) -10L), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_3454;

					{
						obj_t BgL_auxz00_3455;

						{	/* Globalize/escape.scm 232 */
							BgL_objectz00_bglt BgL_tmpz00_3456;

							BgL_tmpz00_3456 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_tmp1154z00_2808));
							BgL_auxz00_3455 = BGL_OBJECT_WIDENING(BgL_tmpz00_3456);
						}
						BgL_auxz00_3454 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_3455);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3454))->
							BgL_markz00) = ((long) -10L), BUNSPEC);
				}
				((BgL_sexitz00_bglt) BgL_tmp1154z00_2808);
			}
			{	/* Globalize/escape.scm 233 */
				BgL_nodez00_bglt BgL_arg1663z00_2811;

				BgL_arg1663z00_2811 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2708)))->BgL_onexitz00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1663z00_2811,
					((BgL_variablez00_bglt) BgL_oz00_2709));
			}
			{	/* Globalize/escape.scm 234 */
				BgL_nodez00_bglt BgL_arg1675z00_2812;

				BgL_arg1675z00_2812 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2708)))->BgL_bodyz00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1675z00_2812,
					((BgL_variablez00_bglt) BgL_oz00_2709));
			}
		}

	}



/* &escape!-let-var1366 */
	obj_t BGl_z62escapez12zd2letzd2var1366z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2710, obj_t BgL_nodez00_2711, obj_t BgL_oz00_2712)
	{
		{	/* Globalize/escape.scm 219 */
			{	/* Globalize/escape.scm 220 */
				bool_t BgL_tmpz00_3471;

				{	/* Globalize/escape.scm 221 */
					BgL_nodez00_bglt BgL_arg1627z00_2814;

					BgL_arg1627z00_2814 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2711)))->BgL_bodyz00);
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1627z00_2814,
						((BgL_variablez00_bglt) BgL_oz00_2712));
				}
				{	/* Globalize/escape.scm 222 */
					obj_t BgL_g1313z00_2815;

					BgL_g1313z00_2815 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2711)))->BgL_bindingsz00);
					{
						obj_t BgL_l1311z00_2817;

						BgL_l1311z00_2817 = BgL_g1313z00_2815;
					BgL_zc3z04anonymousza31628ze3z87_2816:
						if (PAIRP(BgL_l1311z00_2817))
							{	/* Globalize/escape.scm 222 */
								{	/* Globalize/escape.scm 223 */
									obj_t BgL_bindingz00_2818;

									BgL_bindingz00_2818 = CAR(BgL_l1311z00_2817);
									{	/* Globalize/escape.scm 223 */
										BgL_svarz00_bglt BgL_tmp1149z00_2819;

										BgL_tmp1149z00_2819 =
											((BgL_svarz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_bindingz00_2818))))))->
												BgL_valuez00));
										{	/* Globalize/escape.scm 223 */
											BgL_svarzf2ginfozf2_bglt BgL_wide1151z00_2820;

											BgL_wide1151z00_2820 =
												((BgL_svarzf2ginfozf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_svarzf2ginfozf2_bgl))));
											{	/* Globalize/escape.scm 223 */
												obj_t BgL_auxz00_3491;
												BgL_objectz00_bglt BgL_tmpz00_3488;

												BgL_auxz00_3491 = ((obj_t) BgL_wide1151z00_2820);
												BgL_tmpz00_3488 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1149z00_2819));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3488,
													BgL_auxz00_3491);
											}
											((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1149z00_2819));
											{	/* Globalize/escape.scm 223 */
												long BgL_arg1630z00_2821;

												BgL_arg1630z00_2821 =
													BGL_CLASS_NUM
													(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_svarz00_bglt) BgL_tmp1149z00_2819)),
													BgL_arg1630z00_2821);
											}
											((BgL_svarz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1149z00_2819));
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3502;

											{
												obj_t BgL_auxz00_3503;

												{	/* Globalize/escape.scm 223 */
													BgL_objectz00_bglt BgL_tmpz00_3504;

													BgL_tmpz00_3504 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_2819));
													BgL_auxz00_3503 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3504);
												}
												BgL_auxz00_3502 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3503);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3502))->
													BgL_kapturedzf3zf3) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3510;

											{
												obj_t BgL_auxz00_3511;

												{	/* Globalize/escape.scm 223 */
													BgL_objectz00_bglt BgL_tmpz00_3512;

													BgL_tmpz00_3512 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_2819));
													BgL_auxz00_3511 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3512);
												}
												BgL_auxz00_3510 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3511);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3510))->
													BgL_freezd2markzd2) = ((long) -10L), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3518;

											{
												obj_t BgL_auxz00_3519;

												{	/* Globalize/escape.scm 223 */
													BgL_objectz00_bglt BgL_tmpz00_3520;

													BgL_tmpz00_3520 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_2819));
													BgL_auxz00_3519 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3520);
												}
												BgL_auxz00_3518 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3519);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3518))->
													BgL_markz00) = ((long) -10L), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3526;

											{
												obj_t BgL_auxz00_3527;

												{	/* Globalize/escape.scm 223 */
													BgL_objectz00_bglt BgL_tmpz00_3528;

													BgL_tmpz00_3528 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_2819));
													BgL_auxz00_3527 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3528);
												}
												BgL_auxz00_3526 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3527);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3526))->
													BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										{
											BgL_svarzf2ginfozf2_bglt BgL_auxz00_3534;

											{
												obj_t BgL_auxz00_3535;

												{	/* Globalize/escape.scm 223 */
													BgL_objectz00_bglt BgL_tmpz00_3536;

													BgL_tmpz00_3536 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1149z00_2819));
													BgL_auxz00_3535 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3536);
												}
												BgL_auxz00_3534 =
													((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3535);
											}
											((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3534))->
													BgL_stackablez00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
										}
										((BgL_svarz00_bglt) BgL_tmp1149z00_2819);
									}
									{	/* Globalize/escape.scm 224 */
										obj_t BgL_arg1646z00_2822;

										BgL_arg1646z00_2822 = CDR(((obj_t) BgL_bindingz00_2818));
										BGl_escapez12z12zzglobaliza7e_escapeza7(
											((BgL_nodez00_bglt) BgL_arg1646z00_2822),
											((BgL_variablez00_bglt) BgL_oz00_2712));
								}}
								{
									obj_t BgL_l1311z00_3548;

									BgL_l1311z00_3548 = CDR(BgL_l1311z00_2817);
									BgL_l1311z00_2817 = BgL_l1311z00_3548;
									goto BgL_zc3z04anonymousza31628ze3z87_2816;
								}
							}
						else
							{	/* Globalize/escape.scm 222 */
								BgL_tmpz00_3471 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_3471);
			}
		}

	}



/* &escape!-let-fun1364 */
	obj_t BGl_z62escapez12zd2letzd2fun1364z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2713, obj_t BgL_nodez00_2714, obj_t BgL_oz00_2715)
	{
		{	/* Globalize/escape.scm 196 */
			{	/* Globalize/escape.scm 199 */
				obj_t BgL_g1307z00_2824;

				BgL_g1307z00_2824 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2714)))->BgL_localsz00);
				{
					obj_t BgL_l1305z00_2826;

					BgL_l1305z00_2826 = BgL_g1307z00_2824;
				BgL_zc3z04anonymousza31595ze3z87_2825:
					if (PAIRP(BgL_l1305z00_2826))
						{	/* Globalize/escape.scm 199 */
							{	/* Globalize/escape.scm 200 */
								obj_t BgL_localz00_2827;

								BgL_localz00_2827 = CAR(BgL_l1305z00_2826);
								{	/* Globalize/escape.scm 200 */
									BgL_localzf2ginfozf2_bglt BgL_wide1138z00_2828;

									BgL_wide1138z00_2828 =
										((BgL_localzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_localzf2ginfozf2_bgl))));
									{	/* Globalize/escape.scm 200 */
										obj_t BgL_auxz00_3561;
										BgL_objectz00_bglt BgL_tmpz00_3557;

										BgL_auxz00_3561 = ((obj_t) BgL_wide1138z00_2828);
										BgL_tmpz00_3557 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_localz00_2827)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3557, BgL_auxz00_3561);
									}
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt)
											((BgL_localz00_bglt) BgL_localz00_2827)));
									{	/* Globalize/escape.scm 200 */
										long BgL_arg1602z00_2829;

										BgL_arg1602z00_2829 =
											BGL_CLASS_NUM(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2827))),
											BgL_arg1602z00_2829);
									}
									((BgL_localz00_bglt)
										((BgL_localz00_bglt)
											((BgL_localz00_bglt) BgL_localz00_2827)));
								}
								{
									BgL_localzf2ginfozf2_bglt BgL_auxz00_3575;

									{
										obj_t BgL_auxz00_3576;

										{	/* Globalize/escape.scm 200 */
											BgL_objectz00_bglt BgL_tmpz00_3577;

											BgL_tmpz00_3577 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2827)));
											BgL_auxz00_3576 = BGL_OBJECT_WIDENING(BgL_tmpz00_3577);
										}
										BgL_auxz00_3575 =
											((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3576);
									}
									((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3575))->
											BgL_escapezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
								}
								{
									BgL_localzf2ginfozf2_bglt BgL_auxz00_3584;

									{
										obj_t BgL_auxz00_3585;

										{	/* Globalize/escape.scm 200 */
											BgL_objectz00_bglt BgL_tmpz00_3586;

											BgL_tmpz00_3586 =
												((BgL_objectz00_bglt)
												((BgL_localz00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2827)));
											BgL_auxz00_3585 = BGL_OBJECT_WIDENING(BgL_tmpz00_3586);
										}
										BgL_auxz00_3584 =
											((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3585);
									}
									((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3584))->
											BgL_globaliza7edzf3z54) =
										((bool_t) ((bool_t) 0)), BUNSPEC);
								}
								((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_localz00_2827));
								{	/* Globalize/escape.scm 201 */
									BgL_valuez00_bglt BgL_funz00_2830;

									BgL_funz00_2830 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2827))))->
										BgL_valuez00);
									{	/* Globalize/escape.scm 202 */
										BgL_sfunzf2ginfozf2_bglt BgL_wide1142z00_2831;

										BgL_wide1142z00_2831 =
											((BgL_sfunzf2ginfozf2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_sfunzf2ginfozf2_bgl))));
										{	/* Globalize/escape.scm 202 */
											obj_t BgL_auxz00_3603;
											BgL_objectz00_bglt BgL_tmpz00_3599;

											BgL_auxz00_3603 = ((obj_t) BgL_wide1142z00_2831);
											BgL_tmpz00_3599 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt)
													((BgL_sfunz00_bglt) BgL_funz00_2830)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3599, BgL_auxz00_3603);
										}
										((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_2830)));
										{	/* Globalize/escape.scm 202 */
											long BgL_arg1605z00_2832;

											BgL_arg1605z00_2832 =
												BGL_CLASS_NUM
												(BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7);
											BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
														(BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
															BgL_funz00_2830))), BgL_arg1605z00_2832);
										}
										((BgL_sfunz00_bglt)
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_2830)));
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3617;

										{
											obj_t BgL_auxz00_3618;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3619;

												BgL_tmpz00_3619 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3618 = BGL_OBJECT_WIDENING(BgL_tmpz00_3619);
											}
											BgL_auxz00_3617 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3618);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3617))->
												BgL_gzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3626;

										{
											obj_t BgL_auxz00_3627;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3628;

												BgL_tmpz00_3628 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3627 = BGL_OBJECT_WIDENING(BgL_tmpz00_3628);
											}
											BgL_auxz00_3626 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3627);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3626))->
												BgL_cfromz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3635;

										{
											obj_t BgL_auxz00_3636;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3637;

												BgL_tmpz00_3637 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3636 = BGL_OBJECT_WIDENING(BgL_tmpz00_3637);
											}
											BgL_auxz00_3635 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3636);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3635))->
												BgL_cfromza2za2) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3644;

										{
											obj_t BgL_auxz00_3645;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3646;

												BgL_tmpz00_3646 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3645 = BGL_OBJECT_WIDENING(BgL_tmpz00_3646);
											}
											BgL_auxz00_3644 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3645);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3644))->
												BgL_ctoz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3653;

										{
											obj_t BgL_auxz00_3654;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3655;

												BgL_tmpz00_3655 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3654 = BGL_OBJECT_WIDENING(BgL_tmpz00_3655);
											}
											BgL_auxz00_3653 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3654);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3653))->
												BgL_ctoza2za2) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3662;

										{
											obj_t BgL_auxz00_3663;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3664;

												BgL_tmpz00_3664 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3663 = BGL_OBJECT_WIDENING(BgL_tmpz00_3664);
											}
											BgL_auxz00_3662 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3663);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3662))->
												BgL_efunctionsz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3671;

										{
											obj_t BgL_auxz00_3672;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3673;

												BgL_tmpz00_3673 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3672 = BGL_OBJECT_WIDENING(BgL_tmpz00_3673);
											}
											BgL_auxz00_3671 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3672);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3671))->
												BgL_integratorz00) = ((obj_t) BUNSPEC), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3680;

										{
											obj_t BgL_auxz00_3681;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3682;

												BgL_tmpz00_3682 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3681 = BGL_OBJECT_WIDENING(BgL_tmpz00_3682);
											}
											BgL_auxz00_3680 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3681);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3680))->
												BgL_imarkz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3689;

										{
											obj_t BgL_auxz00_3690;

											{	/* Globalize/escape.scm 203 */
												BgL_objectz00_bglt BgL_tmpz00_3691;

												BgL_tmpz00_3691 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3690 = BGL_OBJECT_WIDENING(BgL_tmpz00_3691);
											}
											BgL_auxz00_3689 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3690);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3689))->
												BgL_ownerz00) = ((obj_t) BgL_oz00_2715), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3698;

										{
											obj_t BgL_auxz00_3699;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3700;

												BgL_tmpz00_3700 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3699 = BGL_OBJECT_WIDENING(BgL_tmpz00_3700);
											}
											BgL_auxz00_3698 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3699);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3698))->
												BgL_integratedz00) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3707;

										{
											obj_t BgL_auxz00_3708;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3709;

												BgL_tmpz00_3709 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3708 = BGL_OBJECT_WIDENING(BgL_tmpz00_3709);
											}
											BgL_auxz00_3707 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3708);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3707))->
												BgL_pluggedzd2inzd2) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3716;

										{
											obj_t BgL_auxz00_3717;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3718;

												BgL_tmpz00_3718 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3717 = BGL_OBJECT_WIDENING(BgL_tmpz00_3718);
											}
											BgL_auxz00_3716 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3717);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3716))->
												BgL_markz00) = ((long) -10L), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3725;

										{
											obj_t BgL_auxz00_3726;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3727;

												BgL_tmpz00_3727 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3726 = BGL_OBJECT_WIDENING(BgL_tmpz00_3727);
											}
											BgL_auxz00_3725 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3726);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3725))->
												BgL_freezd2markzd2) = ((obj_t) BNIL), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3734;

										{
											obj_t BgL_auxz00_3735;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3736;

												BgL_tmpz00_3736 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3735 = BGL_OBJECT_WIDENING(BgL_tmpz00_3736);
											}
											BgL_auxz00_3734 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3735);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3734))->
												BgL_thezd2globalzd2) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3743;

										{
											obj_t BgL_auxz00_3744;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3745;

												BgL_tmpz00_3745 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3744 = BGL_OBJECT_WIDENING(BgL_tmpz00_3745);
											}
											BgL_auxz00_3743 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3744);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3743))->
												BgL_kapturedz00) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3752;

										{
											obj_t BgL_auxz00_3753;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3754;

												BgL_tmpz00_3754 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3753 = BGL_OBJECT_WIDENING(BgL_tmpz00_3754);
											}
											BgL_auxz00_3752 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3753);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3752))->
												BgL_newzd2bodyzd2) = ((obj_t) BFALSE), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3761;

										{
											obj_t BgL_auxz00_3762;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3763;

												BgL_tmpz00_3763 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3762 = BGL_OBJECT_WIDENING(BgL_tmpz00_3763);
											}
											BgL_auxz00_3761 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3762);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3761))->
												BgL_bmarkz00) = ((long) -10L), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3770;

										{
											obj_t BgL_auxz00_3771;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3772;

												BgL_tmpz00_3772 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3771 = BGL_OBJECT_WIDENING(BgL_tmpz00_3772);
											}
											BgL_auxz00_3770 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3771);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3770))->
												BgL_umarkz00) = ((long) -10L), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3779;

										{
											obj_t BgL_auxz00_3780;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3781;

												BgL_tmpz00_3781 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3780 = BGL_OBJECT_WIDENING(BgL_tmpz00_3781);
											}
											BgL_auxz00_3779 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3780);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3779))->
												BgL_freez00) = ((obj_t) BUNSPEC), BUNSPEC);
									}
									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3788;

										{
											obj_t BgL_auxz00_3789;

											{	/* Globalize/escape.scm 201 */
												BgL_objectz00_bglt BgL_tmpz00_3790;

												BgL_tmpz00_3790 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_2830)));
												BgL_auxz00_3789 = BGL_OBJECT_WIDENING(BgL_tmpz00_3790);
											}
											BgL_auxz00_3788 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3789);
										}
										((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3788))->
												BgL_boundz00) = ((obj_t) BNIL), BUNSPEC);
									}
									((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_2830));
									{	/* Globalize/escape.scm 204 */
										obj_t BgL_g1304z00_2833;

										BgL_g1304z00_2833 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_2830)))->BgL_argsz00);
										{
											obj_t BgL_l1302z00_2835;

											BgL_l1302z00_2835 = BgL_g1304z00_2833;
										BgL_zc3z04anonymousza31606ze3z87_2834:
											if (PAIRP(BgL_l1302z00_2835))
												{	/* Globalize/escape.scm 206 */
													{	/* Globalize/escape.scm 205 */
														obj_t BgL_localz00_2836;

														BgL_localz00_2836 = CAR(BgL_l1302z00_2835);
														{	/* Globalize/escape.scm 205 */
															BgL_svarz00_bglt BgL_tmp1144z00_2837;

															BgL_tmp1144z00_2837 =
																((BgL_svarz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_localz00_2836))))->BgL_valuez00));
															{	/* Globalize/escape.scm 205 */
																BgL_svarzf2ginfozf2_bglt BgL_wide1146z00_2838;

																BgL_wide1146z00_2838 =
																	((BgL_svarzf2ginfozf2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_svarzf2ginfozf2_bgl))));
																{	/* Globalize/escape.scm 205 */
																	obj_t BgL_auxz00_3812;
																	BgL_objectz00_bglt BgL_tmpz00_3809;

																	BgL_auxz00_3812 =
																		((obj_t) BgL_wide1146z00_2838);
																	BgL_tmpz00_3809 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1144z00_2837));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3809,
																		BgL_auxz00_3812);
																}
																((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1144z00_2837));
																{	/* Globalize/escape.scm 205 */
																	long BgL_arg1609z00_2839;

																	BgL_arg1609z00_2839 =
																		BGL_CLASS_NUM
																		(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_2837)),
																		BgL_arg1609z00_2839);
																}
																((BgL_svarz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1144z00_2837));
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3823;

																{
																	obj_t BgL_auxz00_3824;

																	{	/* Globalize/escape.scm 205 */
																		BgL_objectz00_bglt BgL_tmpz00_3825;

																		BgL_tmpz00_3825 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_2837));
																		BgL_auxz00_3824 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3825);
																	}
																	BgL_auxz00_3823 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3824);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3823))->
																		BgL_kapturedzf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3831;

																{
																	obj_t BgL_auxz00_3832;

																	{	/* Globalize/escape.scm 205 */
																		BgL_objectz00_bglt BgL_tmpz00_3833;

																		BgL_tmpz00_3833 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_2837));
																		BgL_auxz00_3832 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3833);
																	}
																	BgL_auxz00_3831 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3832);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3831))->
																		BgL_freezd2markzd2) =
																	((long) -10L), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3839;

																{
																	obj_t BgL_auxz00_3840;

																	{	/* Globalize/escape.scm 205 */
																		BgL_objectz00_bglt BgL_tmpz00_3841;

																		BgL_tmpz00_3841 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_2837));
																		BgL_auxz00_3840 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3841);
																	}
																	BgL_auxz00_3839 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3840);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3839))->BgL_markz00) =
																	((long) -10L), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3847;

																{
																	obj_t BgL_auxz00_3848;

																	{	/* Globalize/escape.scm 205 */
																		BgL_objectz00_bglt BgL_tmpz00_3849;

																		BgL_tmpz00_3849 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_2837));
																		BgL_auxz00_3848 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3849);
																	}
																	BgL_auxz00_3847 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3848);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3847))->
																		BgL_celledzf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															{
																BgL_svarzf2ginfozf2_bglt BgL_auxz00_3855;

																{
																	obj_t BgL_auxz00_3856;

																	{	/* Globalize/escape.scm 205 */
																		BgL_objectz00_bglt BgL_tmpz00_3857;

																		BgL_tmpz00_3857 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_2837));
																		BgL_auxz00_3856 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3857);
																	}
																	BgL_auxz00_3855 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BgL_auxz00_3856);
																}
																((((BgL_svarzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3855))->
																		BgL_stackablez00) =
																	((bool_t) ((bool_t) 1)), BUNSPEC);
															}
															((BgL_svarz00_bglt) BgL_tmp1144z00_2837);
													}}
													{
														obj_t BgL_l1302z00_3864;

														BgL_l1302z00_3864 = CDR(BgL_l1302z00_2835);
														BgL_l1302z00_2835 = BgL_l1302z00_3864;
														goto BgL_zc3z04anonymousza31606ze3z87_2834;
													}
												}
											else
												{	/* Globalize/escape.scm 206 */
													((bool_t) 1);
												}
										}
									}
								}
							}
							{
								obj_t BgL_l1305z00_3866;

								BgL_l1305z00_3866 = CDR(BgL_l1305z00_2826);
								BgL_l1305z00_2826 = BgL_l1305z00_3866;
								goto BgL_zc3z04anonymousza31595ze3z87_2825;
							}
						}
					else
						{	/* Globalize/escape.scm 199 */
							((bool_t) 1);
						}
				}
			}
			{	/* Globalize/escape.scm 209 */
				obj_t BgL_g1310z00_2840;

				BgL_g1310z00_2840 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2714)))->BgL_localsz00);
				{
					obj_t BgL_l1308z00_2842;

					BgL_l1308z00_2842 = BgL_g1310z00_2840;
				BgL_zc3z04anonymousza31614ze3z87_2841:
					if (PAIRP(BgL_l1308z00_2842))
						{	/* Globalize/escape.scm 209 */
							{	/* Globalize/escape.scm 210 */
								obj_t BgL_localz00_2843;

								BgL_localz00_2843 = CAR(BgL_l1308z00_2842);
								{	/* Globalize/escape.scm 210 */
									BgL_valuez00_bglt BgL_funz00_2844;

									BgL_funz00_2844 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2843))))->
										BgL_valuez00);
									{	/* Globalize/escape.scm 211 */
										obj_t BgL_arg1616z00_2845;

										BgL_arg1616z00_2845 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_2844)))->BgL_bodyz00);
										BGl_escapez12z12zzglobaliza7e_escapeza7(
											((BgL_nodez00_bglt) BgL_arg1616z00_2845),
											((BgL_variablez00_bglt) BgL_localz00_2843));
									}
								}
							}
							{
								obj_t BgL_l1308z00_3881;

								BgL_l1308z00_3881 = CDR(BgL_l1308z00_2842);
								BgL_l1308z00_2842 = BgL_l1308z00_3881;
								goto BgL_zc3z04anonymousza31614ze3z87_2841;
							}
						}
					else
						{	/* Globalize/escape.scm 209 */
							((bool_t) 1);
						}
				}
			}
			{	/* Globalize/escape.scm 214 */
				BgL_nodez00_bglt BgL_arg1626z00_2846;

				BgL_arg1626z00_2846 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2714)))->BgL_bodyz00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1626z00_2846,
					((BgL_variablez00_bglt) BgL_oz00_2715));
			}
		}

	}



/* &escape!-switch1362 */
	obj_t BGl_z62escapez12zd2switch1362za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2716, obj_t BgL_nodez00_2717, obj_t BgL_oz00_2718)
	{
		{	/* Globalize/escape.scm 182 */
			{	/* Globalize/escape.scm 183 */
				bool_t BgL_tmpz00_3887;

				{	/* Globalize/escape.scm 184 */
					BgL_nodez00_bglt BgL_arg1589z00_2848;

					BgL_arg1589z00_2848 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2717)))->BgL_testz00);
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1589z00_2848,
						((BgL_variablez00_bglt) BgL_oz00_2718));
				}
				{	/* Globalize/escape.scm 185 */
					obj_t BgL_g1301z00_2849;

					BgL_g1301z00_2849 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2717)))->BgL_clausesz00);
					{
						obj_t BgL_l1299z00_2851;

						BgL_l1299z00_2851 = BgL_g1301z00_2849;
					BgL_zc3z04anonymousza31590ze3z87_2850:
						if (PAIRP(BgL_l1299z00_2851))
							{	/* Globalize/escape.scm 185 */
								{	/* Globalize/escape.scm 186 */
									obj_t BgL_clausez00_2852;

									BgL_clausez00_2852 = CAR(BgL_l1299z00_2851);
									{	/* Globalize/escape.scm 186 */
										obj_t BgL_arg1593z00_2853;

										BgL_arg1593z00_2853 = CDR(((obj_t) BgL_clausez00_2852));
										BGl_escapez12z12zzglobaliza7e_escapeza7(
											((BgL_nodez00_bglt) BgL_arg1593z00_2853),
											((BgL_variablez00_bglt) BgL_oz00_2718));
									}
								}
								{
									obj_t BgL_l1299z00_3902;

									BgL_l1299z00_3902 = CDR(BgL_l1299z00_2851);
									BgL_l1299z00_2851 = BgL_l1299z00_3902;
									goto BgL_zc3z04anonymousza31590ze3z87_2850;
								}
							}
						else
							{	/* Globalize/escape.scm 185 */
								BgL_tmpz00_3887 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_3887);
			}
		}

	}



/* &escape!-fail1360 */
	obj_t BGl_z62escapez12zd2fail1360za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2719, obj_t BgL_nodez00_2720, obj_t BgL_oz00_2721)
	{
		{	/* Globalize/escape.scm 173 */
			{	/* Globalize/escape.scm 175 */
				BgL_nodez00_bglt BgL_arg1576z00_2855;

				BgL_arg1576z00_2855 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2720)))->BgL_procz00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1576z00_2855,
					((BgL_variablez00_bglt) BgL_oz00_2721));
			}
			{	/* Globalize/escape.scm 176 */
				BgL_nodez00_bglt BgL_arg1584z00_2856;

				BgL_arg1584z00_2856 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2720)))->BgL_msgz00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1584z00_2856,
					((BgL_variablez00_bglt) BgL_oz00_2721));
			}
			{	/* Globalize/escape.scm 177 */
				BgL_nodez00_bglt BgL_arg1585z00_2857;

				BgL_arg1585z00_2857 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2720)))->BgL_objz00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1585z00_2857,
					((BgL_variablez00_bglt) BgL_oz00_2721));
			}
		}

	}



/* &escape!-conditional1358 */
	obj_t BGl_z62escapez12zd2conditional1358za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2722, obj_t BgL_nodez00_2723, obj_t BgL_oz00_2724)
	{
		{	/* Globalize/escape.scm 164 */
			{	/* Globalize/escape.scm 166 */
				BgL_nodez00_bglt BgL_arg1571z00_2859;

				BgL_arg1571z00_2859 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2723)))->BgL_testz00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1571z00_2859,
					((BgL_variablez00_bglt) BgL_oz00_2724));
			}
			{	/* Globalize/escape.scm 167 */
				BgL_nodez00_bglt BgL_arg1573z00_2860;

				BgL_arg1573z00_2860 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2723)))->BgL_truez00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1573z00_2860,
					((BgL_variablez00_bglt) BgL_oz00_2724));
			}
			{	/* Globalize/escape.scm 168 */
				BgL_nodez00_bglt BgL_arg1575z00_2861;

				BgL_arg1575z00_2861 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2723)))->BgL_falsez00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1575z00_2861,
					((BgL_variablez00_bglt) BgL_oz00_2724));
			}
		}

	}



/* &escape!-setq1356 */
	obj_t BGl_z62escapez12zd2setq1356za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2725, obj_t BgL_nodez00_2726, obj_t BgL_oz00_2727)
	{
		{	/* Globalize/escape.scm 157 */
			{	/* Globalize/escape.scm 159 */
				BgL_nodez00_bglt BgL_arg1565z00_2863;

				BgL_arg1565z00_2863 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2726)))->BgL_valuez00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1565z00_2863,
					((BgL_variablez00_bglt) BgL_oz00_2727));
			}
		}

	}



/* &escape!-cast1354 */
	obj_t BGl_z62escapez12zd2cast1354za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2728, obj_t BgL_nodez00_2729, obj_t BgL_oz00_2730)
	{
		{	/* Globalize/escape.scm 151 */
			{	/* Globalize/escape.scm 152 */
				BgL_nodez00_bglt BgL_arg1564z00_2865;

				BgL_arg1564z00_2865 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2729)))->BgL_argz00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1564z00_2865,
					((BgL_variablez00_bglt) BgL_oz00_2730));
			}
		}

	}



/* &escape!-extern1352 */
	obj_t BGl_z62escapez12zd2extern1352za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2731, obj_t BgL_nodez00_2732, obj_t BgL_oz00_2733)
	{
		{	/* Globalize/escape.scm 145 */
			return
				BBOOL(BGl_escapeza2z12zb0zzglobaliza7e_escapeza7(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2732)))->BgL_exprza2za2),
					BgL_oz00_2733));
		}

	}



/* &escape!-funcall1350 */
	obj_t BGl_z62escapez12zd2funcall1350za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2734, obj_t BgL_nodez00_2735, obj_t BgL_oz00_2736)
	{
		{	/* Globalize/escape.scm 137 */
			{	/* Globalize/escape.scm 138 */
				bool_t BgL_tmpz00_3941;

				{	/* Globalize/escape.scm 139 */
					BgL_nodez00_bglt BgL_arg1553z00_2868;

					BgL_arg1553z00_2868 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2735)))->BgL_funz00);
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1553z00_2868,
						((BgL_variablez00_bglt) BgL_oz00_2736));
				}
				BgL_tmpz00_3941 =
					BGl_escapeza2z12zb0zzglobaliza7e_escapeza7(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2735)))->BgL_argsz00),
					BgL_oz00_2736);
				return BBOOL(BgL_tmpz00_3941);
			}
		}

	}



/* &escape!-app-ly1348 */
	obj_t BGl_z62escapez12zd2appzd2ly1348z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2737, obj_t BgL_nodez00_2738, obj_t BgL_oz00_2739)
	{
		{	/* Globalize/escape.scm 129 */
			{	/* Globalize/escape.scm 131 */
				BgL_nodez00_bglt BgL_arg1546z00_2870;

				BgL_arg1546z00_2870 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2738)))->BgL_funz00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1546z00_2870,
					((BgL_variablez00_bglt) BgL_oz00_2739));
			}
			{	/* Globalize/escape.scm 132 */
				BgL_nodez00_bglt BgL_arg1552z00_2871;

				BgL_arg1552z00_2871 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2738)))->BgL_argz00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1552z00_2871,
					((BgL_variablez00_bglt) BgL_oz00_2739));
			}
		}

	}



/* &escape!-app1346 */
	obj_t BGl_z62escapez12zd2app1346za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2740, obj_t BgL_nodez00_2741, obj_t BgL_oz00_2742)
	{
		{	/* Globalize/escape.scm 122 */
			return
				BBOOL(BGl_escapeza2z12zb0zzglobaliza7e_escapeza7(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2741)))->BgL_argsz00),
					BgL_oz00_2742));
		}

	}



/* &escape!-sync1344 */
	obj_t BGl_z62escapez12zd2sync1344za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2743, obj_t BgL_nodez00_2744, obj_t BgL_oz00_2745)
	{
		{	/* Globalize/escape.scm 114 */
			{	/* Globalize/escape.scm 115 */
				BgL_nodez00_bglt BgL_arg1516z00_2874;

				BgL_arg1516z00_2874 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2744)))->BgL_mutexz00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1516z00_2874,
					((BgL_variablez00_bglt) BgL_oz00_2745));
			}
			{	/* Globalize/escape.scm 116 */
				BgL_nodez00_bglt BgL_arg1535z00_2875;

				BgL_arg1535z00_2875 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2744)))->BgL_prelockz00);
				BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1535z00_2875,
					((BgL_variablez00_bglt) BgL_oz00_2745));
			}
			{	/* Globalize/escape.scm 117 */
				BgL_nodez00_bglt BgL_arg1540z00_2876;

				BgL_arg1540z00_2876 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2744)))->BgL_bodyz00);
				return
					BGl_escapez12z12zzglobaliza7e_escapeza7(BgL_arg1540z00_2876,
					((BgL_variablez00_bglt) BgL_oz00_2745));
			}
		}

	}



/* &escape!-sequence1342 */
	obj_t BGl_z62escapez12zd2sequence1342za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2746, obj_t BgL_nodez00_2747, obj_t BgL_oz00_2748)
	{
		{	/* Globalize/escape.scm 108 */
			return
				BBOOL(BGl_escapeza2z12zb0zzglobaliza7e_escapeza7(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2747)))->BgL_nodesz00),
					BgL_oz00_2748));
		}

	}



/* &escape!-closure1340 */
	obj_t BGl_z62escapez12zd2closure1340za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2749, obj_t BgL_nodez00_2750, obj_t BgL_oz00_2751)
	{
		{	/* Globalize/escape.scm 102 */
			{	/* Globalize/escape.scm 103 */
				BgL_variablez00_bglt BgL_arg1513z00_2879;

				BgL_arg1513z00_2879 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt)
								((BgL_closurez00_bglt) BgL_nodez00_2750))))->BgL_variablez00);
				return
					BGl_setzd2escapingzd2funz12z12zzglobaliza7e_escapeza7
					(BgL_arg1513z00_2879);
			}
		}

	}



/* &escape!-var1337 */
	obj_t BGl_z62escapez12zd2var1337za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2752, obj_t BgL_nodez00_2753, obj_t BgL_oz00_2754)
	{
		{	/* Globalize/escape.scm 96 */
			return CNST_TABLE_REF(1);
		}

	}



/* &escape!-kwote1335 */
	obj_t BGl_z62escapez12zd2kwote1335za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2755, obj_t BgL_nodez00_2756, obj_t BgL_oz00_2757)
	{
		{	/* Globalize/escape.scm 90 */
			return CNST_TABLE_REF(1);
		}

	}



/* &escape!-atom1333 */
	obj_t BGl_z62escapez12zd2atom1333za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2758, obj_t BgL_nodez00_2759, obj_t BgL_oz00_2760)
	{
		{	/* Globalize/escape.scm 84 */
			return CNST_TABLE_REF(1);
		}

	}



/* &set-escaping-fun!-lo1329 */
	obj_t BGl_z62setzd2escapingzd2funz12zd2lo1329za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2761, obj_t BgL_variablez00_2762)
	{
		{	/* Globalize/escape.scm 73 */
			{
				BgL_localzf2ginfozf2_bglt BgL_auxz00_3985;

				{
					obj_t BgL_auxz00_3986;

					{	/* Globalize/escape.scm 74 */
						BgL_objectz00_bglt BgL_tmpz00_3987;

						BgL_tmpz00_3987 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_variablez00_2762));
						BgL_auxz00_3986 = BGL_OBJECT_WIDENING(BgL_tmpz00_3987);
					}
					BgL_auxz00_3985 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3986);
				}
				return
					((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3985))->
						BgL_escapezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
			}
		}

	}



/* &set-escaping-fun!-gl1327 */
	obj_t BGl_z62setzd2escapingzd2funz12zd2gl1327za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2763, obj_t BgL_variablez00_2764)
	{
		{	/* Globalize/escape.scm 67 */
			{
				BgL_globalzf2ginfozf2_bglt BgL_auxz00_3993;

				{
					obj_t BgL_auxz00_3994;

					{	/* Globalize/escape.scm 68 */
						BgL_objectz00_bglt BgL_tmpz00_3995;

						BgL_tmpz00_3995 =
							((BgL_objectz00_bglt)
							((BgL_globalz00_bglt) BgL_variablez00_2764));
						BgL_auxz00_3994 = BGL_OBJECT_WIDENING(BgL_tmpz00_3995);
					}
					BgL_auxz00_3993 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_3994);
				}
				return
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3993))->
						BgL_escapezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
			}
		}

	}



/* &set-escaping-fun!-gl1325 */
	obj_t BGl_z62setzd2escapingzd2funz12zd2gl1325za2zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2765, obj_t BgL_variablez00_2766)
	{
		{	/* Globalize/escape.scm 61 */
			{
				BgL_globalz00_bglt BgL_auxz00_4001;

				{	/* Globalize/escape.scm 62 */
					BgL_globalzf2ginfozf2_bglt BgL_wide1125z00_2886;

					BgL_wide1125z00_2886 =
						((BgL_globalzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_globalzf2ginfozf2_bgl))));
					{	/* Globalize/escape.scm 62 */
						obj_t BgL_auxz00_4007;
						BgL_objectz00_bglt BgL_tmpz00_4003;

						BgL_auxz00_4007 = ((obj_t) BgL_wide1125z00_2886);
						BgL_tmpz00_4003 =
							((BgL_objectz00_bglt)
							((BgL_globalz00_bglt)
								((BgL_globalz00_bglt) BgL_variablez00_2766)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4003, BgL_auxz00_4007);
					}
					((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_variablez00_2766)));
					{	/* Globalize/escape.scm 62 */
						long BgL_arg1509z00_2887;

						BgL_arg1509z00_2887 =
							BGL_CLASS_NUM(BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_globalz00_bglt)
									((BgL_globalz00_bglt) BgL_variablez00_2766))),
							BgL_arg1509z00_2887);
					}
					((BgL_globalz00_bglt)
						((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_variablez00_2766)));
				}
				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_4021;

					{
						obj_t BgL_auxz00_4022;

						{	/* Globalize/escape.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_4023;

							BgL_tmpz00_4023 =
								((BgL_objectz00_bglt)
								((BgL_globalz00_bglt)
									((BgL_globalz00_bglt) BgL_variablez00_2766)));
							BgL_auxz00_4022 = BGL_OBJECT_WIDENING(BgL_tmpz00_4023);
						}
						BgL_auxz00_4021 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_4022);
					}
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4021))->
							BgL_escapezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
				}
				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_4030;

					{
						obj_t BgL_auxz00_4031;

						{	/* Globalize/escape.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_4032;

							BgL_tmpz00_4032 =
								((BgL_objectz00_bglt)
								((BgL_globalz00_bglt)
									((BgL_globalz00_bglt) BgL_variablez00_2766)));
							BgL_auxz00_4031 = BGL_OBJECT_WIDENING(BgL_tmpz00_4032);
						}
						BgL_auxz00_4030 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_4031);
					}
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4030))->
							BgL_globalzd2closurezd2) = ((obj_t) BFALSE), BUNSPEC);
				}
				BgL_auxz00_4001 =
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_variablez00_2766));
				return ((obj_t) BgL_auxz00_4001);
			}
		}

	}



/* &escape-fun!-local1321 */
	obj_t BGl_z62escapezd2funz12zd2local1321z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2767, obj_t BgL_variablez00_2768)
	{
		{	/* Globalize/escape.scm 47 */
			{

				{	/* Globalize/escape.scm 48 */
					bool_t BgL_test1955z00_4042;

					{	/* Globalize/escape.scm 48 */
						obj_t BgL_classz00_2891;

						BgL_classz00_2891 = BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7;
						{	/* Globalize/escape.scm 48 */
							BgL_objectz00_bglt BgL_arg1807z00_2892;

							{	/* Globalize/escape.scm 48 */
								obj_t BgL_tmpz00_4043;

								BgL_tmpz00_4043 =
									((obj_t) ((BgL_localz00_bglt) BgL_variablez00_2768));
								BgL_arg1807z00_2892 = (BgL_objectz00_bglt) (BgL_tmpz00_4043);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/escape.scm 48 */
									long BgL_idxz00_2893;

									BgL_idxz00_2893 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2892);
									BgL_test1955z00_4042 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2893 + 3L)) == BgL_classz00_2891);
								}
							else
								{	/* Globalize/escape.scm 48 */
									bool_t BgL_res1859z00_2896;

									{	/* Globalize/escape.scm 48 */
										obj_t BgL_oclassz00_2897;

										{	/* Globalize/escape.scm 48 */
											obj_t BgL_arg1815z00_2898;
											long BgL_arg1816z00_2899;

											BgL_arg1815z00_2898 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/escape.scm 48 */
												long BgL_arg1817z00_2900;

												BgL_arg1817z00_2900 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2892);
												BgL_arg1816z00_2899 =
													(BgL_arg1817z00_2900 - OBJECT_TYPE);
											}
											BgL_oclassz00_2897 =
												VECTOR_REF(BgL_arg1815z00_2898, BgL_arg1816z00_2899);
										}
										{	/* Globalize/escape.scm 48 */
											bool_t BgL__ortest_1115z00_2901;

											BgL__ortest_1115z00_2901 =
												(BgL_classz00_2891 == BgL_oclassz00_2897);
											if (BgL__ortest_1115z00_2901)
												{	/* Globalize/escape.scm 48 */
													BgL_res1859z00_2896 = BgL__ortest_1115z00_2901;
												}
											else
												{	/* Globalize/escape.scm 48 */
													long BgL_odepthz00_2902;

													{	/* Globalize/escape.scm 48 */
														obj_t BgL_arg1804z00_2903;

														BgL_arg1804z00_2903 = (BgL_oclassz00_2897);
														BgL_odepthz00_2902 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2903);
													}
													if ((3L < BgL_odepthz00_2902))
														{	/* Globalize/escape.scm 48 */
															obj_t BgL_arg1802z00_2904;

															{	/* Globalize/escape.scm 48 */
																obj_t BgL_arg1803z00_2905;

																BgL_arg1803z00_2905 = (BgL_oclassz00_2897);
																BgL_arg1802z00_2904 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2905,
																	3L);
															}
															BgL_res1859z00_2896 =
																(BgL_arg1802z00_2904 == BgL_classz00_2891);
														}
													else
														{	/* Globalize/escape.scm 48 */
															BgL_res1859z00_2896 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1955z00_4042 = BgL_res1859z00_2896;
								}
						}
					}
					if (BgL_test1955z00_4042)
						{	/* Globalize/escape.scm 48 */
							BFALSE;
						}
					else
						{	/* Globalize/escape.scm 48 */
							{	/* Globalize/escape.scm 49 */
								BgL_localzf2ginfozf2_bglt BgL_wide1121z00_2906;

								BgL_wide1121z00_2906 =
									((BgL_localzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localzf2ginfozf2_bgl))));
								{	/* Globalize/escape.scm 49 */
									obj_t BgL_auxz00_4071;
									BgL_objectz00_bglt BgL_tmpz00_4067;

									BgL_auxz00_4071 = ((obj_t) BgL_wide1121z00_2906);
									BgL_tmpz00_4067 =
										((BgL_objectz00_bglt)
										((BgL_localz00_bglt)
											((BgL_localz00_bglt) BgL_variablez00_2768)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4067, BgL_auxz00_4071);
								}
								((BgL_objectz00_bglt)
									((BgL_localz00_bglt)
										((BgL_localz00_bglt) BgL_variablez00_2768)));
								{	/* Globalize/escape.scm 49 */
									long BgL_arg1502z00_2907;

									BgL_arg1502z00_2907 =
										BGL_CLASS_NUM(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_variablez00_2768))),
										BgL_arg1502z00_2907);
								}
								((BgL_localz00_bglt)
									((BgL_localz00_bglt)
										((BgL_localz00_bglt) BgL_variablez00_2768)));
							}
							{
								BgL_localzf2ginfozf2_bglt BgL_auxz00_4085;

								{
									obj_t BgL_auxz00_4086;

									{	/* Globalize/escape.scm 49 */
										BgL_objectz00_bglt BgL_tmpz00_4087;

										BgL_tmpz00_4087 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_variablez00_2768)));
										BgL_auxz00_4086 = BGL_OBJECT_WIDENING(BgL_tmpz00_4087);
									}
									BgL_auxz00_4085 =
										((BgL_localzf2ginfozf2_bglt) BgL_auxz00_4086);
								}
								((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4085))->
										BgL_escapezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_localzf2ginfozf2_bglt BgL_auxz00_4094;

								{
									obj_t BgL_auxz00_4095;

									{	/* Globalize/escape.scm 49 */
										BgL_objectz00_bglt BgL_tmpz00_4096;

										BgL_tmpz00_4096 =
											((BgL_objectz00_bglt)
											((BgL_localz00_bglt)
												((BgL_localz00_bglt) BgL_variablez00_2768)));
										BgL_auxz00_4095 = BGL_OBJECT_WIDENING(BgL_tmpz00_4096);
									}
									BgL_auxz00_4094 =
										((BgL_localzf2ginfozf2_bglt) BgL_auxz00_4095);
								}
								((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4094))->
										BgL_globaliza7edzf3z54) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							((obj_t)
								((BgL_localz00_bglt)
									((BgL_localz00_bglt) BgL_variablez00_2768)));
				}}
				{	/* Globalize/escape.scm 47 */
					obj_t BgL_nextzd2method1320zd2_2890;

					BgL_nextzd2method1320zd2_2890 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt) BgL_variablez00_2768)),
						BGl_escapezd2funz12zd2envz12zzglobaliza7e_escapeza7,
						BGl_localz00zzast_varz00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1320zd2_2890,
						((obj_t) ((BgL_localz00_bglt) BgL_variablez00_2768)));
				}
			}
		}

	}



/* &escape-fun!-global1319 */
	obj_t BGl_z62escapezd2funz12zd2global1319z70zzglobaliza7e_escapeza7(obj_t
		BgL_envz00_2769, obj_t BgL_variablez00_2770)
	{
		{	/* Globalize/escape.scm 38 */
			{

				{	/* Globalize/escape.scm 39 */
					bool_t BgL_test1959z00_4115;

					{	/* Globalize/escape.scm 39 */
						obj_t BgL_classz00_2911;

						BgL_classz00_2911 = BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7;
						{	/* Globalize/escape.scm 39 */
							BgL_objectz00_bglt BgL_arg1807z00_2912;

							{	/* Globalize/escape.scm 39 */
								obj_t BgL_tmpz00_4116;

								BgL_tmpz00_4116 =
									((obj_t) ((BgL_globalz00_bglt) BgL_variablez00_2770));
								BgL_arg1807z00_2912 = (BgL_objectz00_bglt) (BgL_tmpz00_4116);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/escape.scm 39 */
									long BgL_idxz00_2913;

									BgL_idxz00_2913 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2912);
									BgL_test1959z00_4115 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2913 + 3L)) == BgL_classz00_2911);
								}
							else
								{	/* Globalize/escape.scm 39 */
									bool_t BgL_res1858z00_2916;

									{	/* Globalize/escape.scm 39 */
										obj_t BgL_oclassz00_2917;

										{	/* Globalize/escape.scm 39 */
											obj_t BgL_arg1815z00_2918;
											long BgL_arg1816z00_2919;

											BgL_arg1815z00_2918 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/escape.scm 39 */
												long BgL_arg1817z00_2920;

												BgL_arg1817z00_2920 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2912);
												BgL_arg1816z00_2919 =
													(BgL_arg1817z00_2920 - OBJECT_TYPE);
											}
											BgL_oclassz00_2917 =
												VECTOR_REF(BgL_arg1815z00_2918, BgL_arg1816z00_2919);
										}
										{	/* Globalize/escape.scm 39 */
											bool_t BgL__ortest_1115z00_2921;

											BgL__ortest_1115z00_2921 =
												(BgL_classz00_2911 == BgL_oclassz00_2917);
											if (BgL__ortest_1115z00_2921)
												{	/* Globalize/escape.scm 39 */
													BgL_res1858z00_2916 = BgL__ortest_1115z00_2921;
												}
											else
												{	/* Globalize/escape.scm 39 */
													long BgL_odepthz00_2922;

													{	/* Globalize/escape.scm 39 */
														obj_t BgL_arg1804z00_2923;

														BgL_arg1804z00_2923 = (BgL_oclassz00_2917);
														BgL_odepthz00_2922 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2923);
													}
													if ((3L < BgL_odepthz00_2922))
														{	/* Globalize/escape.scm 39 */
															obj_t BgL_arg1802z00_2924;

															{	/* Globalize/escape.scm 39 */
																obj_t BgL_arg1803z00_2925;

																BgL_arg1803z00_2925 = (BgL_oclassz00_2917);
																BgL_arg1802z00_2924 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2925,
																	3L);
															}
															BgL_res1858z00_2916 =
																(BgL_arg1802z00_2924 == BgL_classz00_2911);
														}
													else
														{	/* Globalize/escape.scm 39 */
															BgL_res1858z00_2916 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1959z00_4115 = BgL_res1858z00_2916;
								}
						}
					}
					if (BgL_test1959z00_4115)
						{	/* Globalize/escape.scm 39 */
							BFALSE;
						}
					else
						{	/* Globalize/escape.scm 39 */
							{	/* Globalize/escape.scm 40 */
								BgL_globalzf2ginfozf2_bglt BgL_wide1117z00_2926;

								BgL_wide1117z00_2926 =
									((BgL_globalzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_globalzf2ginfozf2_bgl))));
								{	/* Globalize/escape.scm 40 */
									obj_t BgL_auxz00_4144;
									BgL_objectz00_bglt BgL_tmpz00_4140;

									BgL_auxz00_4144 = ((obj_t) BgL_wide1117z00_2926);
									BgL_tmpz00_4140 =
										((BgL_objectz00_bglt)
										((BgL_globalz00_bglt)
											((BgL_globalz00_bglt) BgL_variablez00_2770)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4140, BgL_auxz00_4144);
								}
								((BgL_objectz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_variablez00_2770)));
								{	/* Globalize/escape.scm 40 */
									long BgL_arg1485z00_2927;

									BgL_arg1485z00_2927 =
										BGL_CLASS_NUM(BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_variablez00_2770))),
										BgL_arg1485z00_2927);
								}
								((BgL_globalz00_bglt)
									((BgL_globalz00_bglt)
										((BgL_globalz00_bglt) BgL_variablez00_2770)));
							}
							{
								BgL_globalzf2ginfozf2_bglt BgL_auxz00_4158;

								{
									obj_t BgL_auxz00_4159;

									{	/* Globalize/escape.scm 41 */
										BgL_objectz00_bglt BgL_tmpz00_4160;

										BgL_tmpz00_4160 =
											((BgL_objectz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_variablez00_2770)));
										BgL_auxz00_4159 = BGL_OBJECT_WIDENING(BgL_tmpz00_4160);
									}
									BgL_auxz00_4158 =
										((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_4159);
								}
								((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4158))->
										BgL_escapezf3zf3) =
									((bool_t) ((((BgL_globalz00_bglt)
													COBJECT(((BgL_globalz00_bglt)
															BgL_variablez00_2770)))->BgL_importz00) ==
											CNST_TABLE_REF(2))), BUNSPEC);
							}
							{
								BgL_globalzf2ginfozf2_bglt BgL_auxz00_4171;

								{
									obj_t BgL_auxz00_4172;

									{	/* Globalize/escape.scm 41 */
										BgL_objectz00_bglt BgL_tmpz00_4173;

										BgL_tmpz00_4173 =
											((BgL_objectz00_bglt)
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt) BgL_variablez00_2770)));
										BgL_auxz00_4172 = BGL_OBJECT_WIDENING(BgL_tmpz00_4173);
									}
									BgL_auxz00_4171 =
										((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_4172);
								}
								((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4171))->
										BgL_globalzd2closurezd2) = ((obj_t) BFALSE), BUNSPEC);
							}
							((obj_t)
								((BgL_globalz00_bglt)
									((BgL_globalz00_bglt) BgL_variablez00_2770)));
				}}
				{	/* Globalize/escape.scm 38 */
					obj_t BgL_nextzd2method1318zd2_2910;

					BgL_nextzd2method1318zd2_2910 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_globalz00_bglt) BgL_variablez00_2770)),
						BGl_escapezd2funz12zd2envz12zzglobaliza7e_escapeza7,
						BGl_globalz00zzast_varz00);
					return BGL_PROCEDURE_CALL1(BgL_nextzd2method1318zd2_2910,
						((obj_t) ((BgL_globalz00_bglt) BgL_variablez00_2770)));
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_escapeza7(void)
	{
		{	/* Globalize/escape.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1900z00zzglobaliza7e_escapeza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1900z00zzglobaliza7e_escapeza7));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1900z00zzglobaliza7e_escapeza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1900z00zzglobaliza7e_escapeza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1900z00zzglobaliza7e_escapeza7));
			return
				BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1900z00zzglobaliza7e_escapeza7));
		}

	}

#ifdef __cplusplus
}
#endif
