/*===========================================================================*/
/*   (Globalize/new_body.scm)                                                */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/new_body.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_GLOBALIZE_NEWzd2BODYzd2_TYPE_DEFINITIONS
#define BGL_BgL_GLOBALIZE_NEWzd2BODYzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;


#endif													// BGL_BgL_GLOBALIZE_NEWzd2BODYzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62remz12zd2boxzd2setz121330z62zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2var1294za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_newzd2bodyz75 =
		BUNSPEC;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_newzd2bodyz75(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_newzd2bodyz75(void);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2jumpzd2exzd2it1324za2zzglobaliza7e_newzd2bodyz75(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_newzd2bodyz75(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62remz121287z70zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_remza2z12zb0zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2globaliza7edzd2newzd2bodiesz12z67zzglobaliza7e_newzd2bodyz75
		(BgL_globalz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62setzd2globaliza7edzd2newzd2bodiesz12z05zzglobaliza7e_newzd2bodyz75
		(obj_t, obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_newzd2bodyz75(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62remz12zd2atom1290za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2letzd2fun1318z70zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62remz12zd2appzd2ly1302z70zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62remz12zd2sequence1296za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static long BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75 = 0L;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt BGl_z62remz12z70zzglobaliza7e_newzd2bodyz75(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2cast1308za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2extern1306za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_newzd2bodyz75(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62remz12zd2switch1316za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_nodez00_bglt, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2conditional1312za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2setq1310za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_newzd2bodyz75(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_newzd2bodyz75(void);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2fail1314za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zzglobaliza7e_newzd2bodyz75(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_newzd2bodyz75(void);
	static bool_t BGl_iszd2inzf3z21zzglobaliza7e_newzd2bodyz75(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2funcall1304za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62remz12zd2letzd2var1320z70zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_setzd2globaliza7edzd2newzd2bodyz12z67zzglobaliza7e_newzd2bodyz75(obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2kwote1292za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62remz12zd2setzd2exzd2it1322za2zzglobaliza7e_newzd2bodyz75(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62remz12zd2boxzd2ref1328z70zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2sync1298za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_nodez00_bglt
		BGl_z62remz12zd2app1300za2zzglobaliza7e_newzd2bodyz75(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62remz12zd2makezd2box1326z70zzglobaliza7e_newzd2bodyz75(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za770za7za7gl1932za7,
		BGl_z62remz12z70zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1905z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_string1905za700za7za7g1933za7, "rem!1287", 8);
	      DEFINE_STRING(BGl_string1906z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_string1906za700za7za7g1934za7, "No method for this object", 25);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2globaliza7edzd2newzd2bodiesz12zd2envzb5zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762setza7d2globali1935z00,
		BGl_z62setzd2globaliza7edzd2newzd2bodiesz12z05zzglobaliza7e_newzd2bodyz75,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1908z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_string1908za700za7za7g1936za7, "rem!", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1904z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza7121287za7701937za7,
		BGl_z62remz121287z70zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1907z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2atom1938za7,
		BGl_z62remz12zd2atom1290za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1909z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2kwot1939za7,
		BGl_z62remz12zd2kwote1292za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1910z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2var11940za7,
		BGl_z62remz12zd2var1294za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1911z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2sequ1941za7,
		BGl_z62remz12zd2sequence1296za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1912z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2sync1942za7,
		BGl_z62remz12zd2sync1298za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1913z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2app11943za7,
		BGl_z62remz12zd2app1300za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1914z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2appza71944z00,
		BGl_z62remz12zd2appzd2ly1302z70zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1915z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2func1945za7,
		BGl_z62remz12zd2funcall1304za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1916z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2exte1946za7,
		BGl_z62remz12zd2extern1306za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1917z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2cast1947za7,
		BGl_z62remz12zd2cast1308za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1918z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2setq1948za7,
		BGl_z62remz12zd2setq1310za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1919z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2cond1949za7,
		BGl_z62remz12zd2conditional1312za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_string1929za700za7za7g1950za7, "globalize_new-body", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1920z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2fail1951za7,
		BGl_z62remz12zd2fail1314za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1921z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2swit1952za7,
		BGl_z62remz12zd2switch1316za2zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1922z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2letza71953z00,
		BGl_z62remz12zd2letzd2fun1318z70zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1923z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2letza71954z00,
		BGl_z62remz12zd2letzd2var1320z70zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string1930z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_string1930za700za7za7g1955za7, "rem!1287 done ", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1924z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2setza71956z00,
		BGl_z62remz12zd2setzd2exzd2it1322za2zzglobaliza7e_newzd2bodyz75, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1925z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2jump1957za7,
		BGl_z62remz12zd2jumpzd2exzd2it1324za2zzglobaliza7e_newzd2bodyz75, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1926z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2make1958za7,
		BGl_z62remz12zd2makezd2box1326z70zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1927z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2boxza71959z00,
		BGl_z62remz12zd2boxzd2ref1328z70zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC,
		3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1928z00zzglobaliza7e_newzd2bodyz75,
		BgL_bgl_za762remza712za7d2boxza71960z00,
		BGl_z62remz12zd2boxzd2setz121330z62zzglobaliza7e_newzd2bodyz75, 0L, BUNSPEC,
		3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_newzd2bodyz75));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_newzd2bodyz75(long
		BgL_checksumz00_2783, char *BgL_fromz00_2784)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_newzd2bodyz75))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_newzd2bodyz75 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_newzd2bodyz75();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_newzd2bodyz75();
					BGl_cnstzd2initzd2zzglobaliza7e_newzd2bodyz75();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_newzd2bodyz75();
					BGl_genericzd2initzd2zzglobaliza7e_newzd2bodyz75();
					BGl_methodzd2initzd2zzglobaliza7e_newzd2bodyz75();
					return BGl_toplevelzd2initzd2zzglobaliza7e_newzd2bodyz75();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_newzd2bodyz75(void)
	{
		{	/* Globalize/new_body.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_new-body");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_new-body");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_newzd2bodyz75(void)
	{
		{	/* Globalize/new_body.scm 15 */
			{	/* Globalize/new_body.scm 15 */
				obj_t BgL_cportz00_2686;

				{	/* Globalize/new_body.scm 15 */
					obj_t BgL_stringz00_2693;

					BgL_stringz00_2693 = BGl_string1930z00zzglobaliza7e_newzd2bodyz75;
					{	/* Globalize/new_body.scm 15 */
						obj_t BgL_startz00_2694;

						BgL_startz00_2694 = BINT(0L);
						{	/* Globalize/new_body.scm 15 */
							obj_t BgL_endz00_2695;

							BgL_endz00_2695 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2693)));
							{	/* Globalize/new_body.scm 15 */

								BgL_cportz00_2686 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2693, BgL_startz00_2694, BgL_endz00_2695);
				}}}}
				{
					long BgL_iz00_2687;

					BgL_iz00_2687 = 1L;
				BgL_loopz00_2688:
					if ((BgL_iz00_2687 == -1L))
						{	/* Globalize/new_body.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/new_body.scm 15 */
							{	/* Globalize/new_body.scm 15 */
								obj_t BgL_arg1931z00_2689;

								{	/* Globalize/new_body.scm 15 */

									{	/* Globalize/new_body.scm 15 */
										obj_t BgL_locationz00_2691;

										BgL_locationz00_2691 = BBOOL(((bool_t) 0));
										{	/* Globalize/new_body.scm 15 */

											BgL_arg1931z00_2689 =
												BGl_readz00zz__readerz00(BgL_cportz00_2686,
												BgL_locationz00_2691);
										}
									}
								}
								{	/* Globalize/new_body.scm 15 */
									int BgL_tmpz00_2814;

									BgL_tmpz00_2814 = (int) (BgL_iz00_2687);
									CNST_TABLE_SET(BgL_tmpz00_2814, BgL_arg1931z00_2689);
							}}
							{	/* Globalize/new_body.scm 15 */
								int BgL_auxz00_2692;

								BgL_auxz00_2692 = (int) ((BgL_iz00_2687 - 1L));
								{
									long BgL_iz00_2819;

									BgL_iz00_2819 = (long) (BgL_auxz00_2692);
									BgL_iz00_2687 = BgL_iz00_2819;
									goto BgL_loopz00_2688;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_newzd2bodyz75(void)
	{
		{	/* Globalize/new_body.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_newzd2bodyz75(void)
	{
		{	/* Globalize/new_body.scm 15 */
			BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75 = 0L;
			return BUNSPEC;
		}

	}



/* set-globalized-new-bodies! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2globaliza7edzd2newzd2bodiesz12z67zzglobaliza7e_newzd2bodyz75
		(BgL_globalz00_bglt BgL_globalz00_3, obj_t BgL_localsz00_4)
	{
		{	/* Globalize/new_body.scm 32 */
			BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75 =
				(BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75 + 1L);
			{	/* Globalize/new_body.scm 37 */
				BgL_valuez00_bglt BgL_funz00_1769;

				BgL_funz00_1769 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_globalz00_3)))->BgL_valuez00);
				{	/* Globalize/new_body.scm 38 */
					BgL_nodez00_bglt BgL_arg1335z00_1770;

					{	/* Globalize/new_body.scm 38 */
						obj_t BgL_arg1339z00_1771;

						BgL_arg1339z00_1771 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1769)))->BgL_bodyz00);
						BgL_arg1335z00_1770 =
							BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
							((BgL_nodez00_bglt) BgL_arg1339z00_1771),
							((obj_t) BgL_globalz00_3), ((obj_t) BgL_globalz00_3));
					}
					((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1769)))->BgL_bodyz00) =
						((obj_t) ((obj_t) BgL_arg1335z00_1770)), BUNSPEC);
				}
			}
			{
				obj_t BgL_l1271z00_1773;

				{	/* Globalize/new_body.scm 40 */
					bool_t BgL_tmpz00_2834;

					BgL_l1271z00_1773 = BgL_localsz00_4;
				BgL_zc3z04anonymousza31340ze3z87_1774:
					if (PAIRP(BgL_l1271z00_1773))
						{	/* Globalize/new_body.scm 40 */
							BGl_setzd2globaliza7edzd2newzd2bodyz12z67zzglobaliza7e_newzd2bodyz75
								(CAR(BgL_l1271z00_1773));
							{
								obj_t BgL_l1271z00_2839;

								BgL_l1271z00_2839 = CDR(BgL_l1271z00_1773);
								BgL_l1271z00_1773 = BgL_l1271z00_2839;
								goto BgL_zc3z04anonymousza31340ze3z87_1774;
							}
						}
					else
						{	/* Globalize/new_body.scm 40 */
							BgL_tmpz00_2834 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_2834);
				}
			}
		}

	}



/* &set-globalized-new-bodies! */
	obj_t
		BGl_z62setzd2globaliza7edzd2newzd2bodiesz12z05zzglobaliza7e_newzd2bodyz75
		(obj_t BgL_envz00_2569, obj_t BgL_globalz00_2570, obj_t BgL_localsz00_2571)
	{
		{	/* Globalize/new_body.scm 32 */
			return
				BGl_setzd2globaliza7edzd2newzd2bodiesz12z67zzglobaliza7e_newzd2bodyz75(
				((BgL_globalz00_bglt) BgL_globalz00_2570), BgL_localsz00_2571);
		}

	}



/* set-globalized-new-body! */
	obj_t
		BGl_setzd2globaliza7edzd2newzd2bodyz12z67zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_localz00_5)
	{
		{	/* Globalize/new_body.scm 45 */
			BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75 =
				(BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75 + 1L);
			{	/* Globalize/new_body.scm 53 */
				BgL_valuez00_bglt BgL_funz00_1779;

				BgL_funz00_1779 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_localz00_bglt) BgL_localz00_5))))->BgL_valuez00);
				{	/* Globalize/new_body.scm 53 */
					BgL_valuez00_bglt BgL_infoz00_1780;

					BgL_infoz00_1780 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_localz00_5))))->BgL_valuez00);
					{	/* Globalize/new_body.scm 54 */
						obj_t BgL_oldzd2bodyzd2_1781;

						BgL_oldzd2bodyzd2_1781 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1779)))->BgL_bodyz00);
						{	/* Globalize/new_body.scm 55 */
							obj_t BgL_obindingsz00_1782;

							{	/* Globalize/new_body.scm 56 */
								obj_t BgL_hook1277z00_1848;

								BgL_hook1277z00_1848 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
								{	/* Globalize/new_body.scm 59 */
									obj_t BgL_g1278z00_1849;

									{
										BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2854;

										{
											obj_t BgL_auxz00_2855;

											{	/* Globalize/new_body.scm 59 */
												BgL_objectz00_bglt BgL_tmpz00_2856;

												BgL_tmpz00_2856 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_infoz00_1780));
												BgL_auxz00_2855 = BGL_OBJECT_WIDENING(BgL_tmpz00_2856);
											}
											BgL_auxz00_2854 =
												((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2855);
										}
										BgL_g1278z00_1849 =
											(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2854))->
											BgL_integratedz00);
									}
									{
										obj_t BgL_l1274z00_1851;
										obj_t BgL_h1275z00_1852;

										BgL_l1274z00_1851 = BgL_g1278z00_1849;
										BgL_h1275z00_1852 = BgL_hook1277z00_1848;
									BgL_zc3z04anonymousza31503ze3z87_1853:
										if (NULLP(BgL_l1274z00_1851))
											{	/* Globalize/new_body.scm 59 */
												BgL_obindingsz00_1782 = CDR(BgL_hook1277z00_1848);
											}
										else
											{	/* Globalize/new_body.scm 59 */
												bool_t BgL_test1965z00_2865;

												{	/* Globalize/new_body.scm 57 */
													bool_t BgL_test1966z00_2866;

													{	/* Globalize/new_body.scm 57 */
														obj_t BgL_arg1552z00_1868;

														{	/* Globalize/new_body.scm 57 */
															BgL_sfunz00_bglt BgL_oz00_2342;

															BgL_oz00_2342 =
																((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					CAR(
																						((obj_t) BgL_l1274z00_1851))))))->
																	BgL_valuez00));
															{
																BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2873;

																{
																	obj_t BgL_auxz00_2874;

																	{	/* Globalize/new_body.scm 57 */
																		BgL_objectz00_bglt BgL_tmpz00_2875;

																		BgL_tmpz00_2875 =
																			((BgL_objectz00_bglt) BgL_oz00_2342);
																		BgL_auxz00_2874 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2875);
																	}
																	BgL_auxz00_2873 =
																		((BgL_sfunzf2ginfozf2_bglt)
																		BgL_auxz00_2874);
																}
																BgL_arg1552z00_1868 =
																	(((BgL_sfunzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2873))->BgL_ownerz00);
															}
														}
														BgL_test1966z00_2866 =
															(BgL_arg1552z00_1868 == BgL_localz00_5);
													}
													if (BgL_test1966z00_2866)
														{	/* Globalize/new_body.scm 57 */
															BgL_test1965z00_2865 = ((bool_t) 0);
														}
													else
														{	/* Globalize/new_body.scm 57 */
															BgL_test1965z00_2865 = ((bool_t) 1);
														}
												}
												if (BgL_test1965z00_2865)
													{	/* Globalize/new_body.scm 59 */
														obj_t BgL_nh1276z00_1862;

														{	/* Globalize/new_body.scm 59 */
															obj_t BgL_arg1544z00_1864;

															BgL_arg1544z00_1864 =
																CAR(((obj_t) BgL_l1274z00_1851));
															BgL_nh1276z00_1862 =
																MAKE_YOUNG_PAIR(BgL_arg1544z00_1864, BNIL);
														}
														SET_CDR(BgL_h1275z00_1852, BgL_nh1276z00_1862);
														{	/* Globalize/new_body.scm 59 */
															obj_t BgL_arg1540z00_1863;

															BgL_arg1540z00_1863 =
																CDR(((obj_t) BgL_l1274z00_1851));
															{
																obj_t BgL_h1275z00_2888;
																obj_t BgL_l1274z00_2887;

																BgL_l1274z00_2887 = BgL_arg1540z00_1863;
																BgL_h1275z00_2888 = BgL_nh1276z00_1862;
																BgL_h1275z00_1852 = BgL_h1275z00_2888;
																BgL_l1274z00_1851 = BgL_l1274z00_2887;
																goto BgL_zc3z04anonymousza31503ze3z87_1853;
															}
														}
													}
												else
													{	/* Globalize/new_body.scm 59 */
														obj_t BgL_arg1546z00_1865;

														BgL_arg1546z00_1865 =
															CDR(((obj_t) BgL_l1274z00_1851));
														{
															obj_t BgL_l1274z00_2891;

															BgL_l1274z00_2891 = BgL_arg1546z00_1865;
															BgL_l1274z00_1851 = BgL_l1274z00_2891;
															goto BgL_zc3z04anonymousza31503ze3z87_1853;
														}
													}
											}
									}
								}
							}
							{	/* Globalize/new_body.scm 56 */
								BgL_nodez00_bglt BgL_newzd2bodyzd2_1783;

								BgL_newzd2bodyzd2_1783 =
									BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
									((BgL_nodez00_bglt) BgL_oldzd2bodyzd2_1781), BgL_localz00_5,
									BgL_localz00_5);
								{	/* Globalize/new_body.scm 60 */

									{
										obj_t BgL_l1279z00_1785;

										BgL_l1279z00_1785 = BgL_obindingsz00_1782;
									BgL_zc3z04anonymousza31344ze3z87_1786:
										if (PAIRP(BgL_l1279z00_1785))
											{	/* Globalize/new_body.scm 64 */
												{	/* Globalize/new_body.scm 65 */
													obj_t BgL_fz00_1788;

													BgL_fz00_1788 = CAR(BgL_l1279z00_1785);
													{	/* Globalize/new_body.scm 66 */
														bool_t BgL_test1968z00_2897;

														{	/* Globalize/new_body.scm 66 */
															long BgL_arg1352z00_1795;

															{	/* Globalize/new_body.scm 66 */
																BgL_sfunz00_bglt BgL_oz00_2350;

																BgL_oz00_2350 =
																	((BgL_sfunz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						BgL_fz00_1788))))->BgL_valuez00));
																{
																	BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2902;

																	{
																		obj_t BgL_auxz00_2903;

																		{	/* Globalize/new_body.scm 66 */
																			BgL_objectz00_bglt BgL_tmpz00_2904;

																			BgL_tmpz00_2904 =
																				((BgL_objectz00_bglt) BgL_oz00_2350);
																			BgL_auxz00_2903 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2904);
																		}
																		BgL_auxz00_2902 =
																			((BgL_sfunzf2ginfozf2_bglt)
																			BgL_auxz00_2903);
																	}
																	BgL_arg1352z00_1795 =
																		(((BgL_sfunzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_2902))->BgL_bmarkz00);
															}}
															BgL_test1968z00_2897 =
																(BgL_arg1352z00_1795 ==
																BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75);
														}
														if (BgL_test1968z00_2897)
															{	/* Globalize/new_body.scm 66 */
																BFALSE;
															}
														else
															{	/* Globalize/new_body.scm 67 */
																BgL_valuez00_bglt BgL_funz00_1792;

																BgL_funz00_1792 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt) BgL_fz00_1788))))->
																	BgL_valuez00);
																{	/* Globalize/new_body.scm 67 */
																	obj_t BgL_bodyz00_1793;

																	BgL_bodyz00_1793 =
																		(((BgL_sfunz00_bglt) COBJECT(
																				((BgL_sfunz00_bglt) BgL_funz00_1792)))->
																		BgL_bodyz00);
																	{	/* Globalize/new_body.scm 68 */

																		{	/* Globalize/new_body.scm 69 */
																			BgL_nodez00_bglt BgL_arg1351z00_1794;

																			BgL_arg1351z00_1794 =
																				BGl_remz12z12zzglobaliza7e_newzd2bodyz75
																				(((BgL_nodez00_bglt) BgL_bodyz00_1793),
																				BgL_localz00_5, BgL_fz00_1788);
																			((((BgL_sfunz00_bglt)
																						COBJECT(((BgL_sfunz00_bglt)
																								BgL_funz00_1792)))->
																					BgL_bodyz00) =
																				((obj_t) ((obj_t) BgL_arg1351z00_1794)),
																				BUNSPEC);
																		}
																	}
																}
															}
													}
												}
												{
													obj_t BgL_l1279z00_2920;

													BgL_l1279z00_2920 = CDR(BgL_l1279z00_1785);
													BgL_l1279z00_1785 = BgL_l1279z00_2920;
													goto BgL_zc3z04anonymousza31344ze3z87_1786;
												}
											}
										else
											{	/* Globalize/new_body.scm 64 */
												((bool_t) 1);
											}
									}
									{	/* Globalize/new_body.scm 72 */
										obj_t BgL_nbindingsz00_1799;

										{
											obj_t BgL_nbdingsz00_1832;
											obj_t BgL_obdingsz00_1833;

											BgL_nbdingsz00_1832 = BNIL;
											BgL_obdingsz00_1833 = BgL_obindingsz00_1782;
										BgL_zc3z04anonymousza31422ze3z87_1834:
											if (NULLP(BgL_obdingsz00_1833))
												{	/* Globalize/new_body.scm 75 */
													BgL_nbindingsz00_1799 = BgL_nbdingsz00_1832;
												}
											else
												{	/* Globalize/new_body.scm 77 */
													bool_t BgL_test1970z00_2924;

													{	/* Globalize/new_body.scm 78 */
														long BgL_arg1485z00_1844;

														{	/* Globalize/new_body.scm 77 */
															BgL_sfunz00_bglt BgL_oz00_2360;

															BgL_oz00_2360 =
																((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					CAR(
																						((obj_t) BgL_obdingsz00_1833))))))->
																	BgL_valuez00));
															{
																BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2931;

																{
																	obj_t BgL_auxz00_2932;

																	{	/* Globalize/new_body.scm 77 */
																		BgL_objectz00_bglt BgL_tmpz00_2933;

																		BgL_tmpz00_2933 =
																			((BgL_objectz00_bglt) BgL_oz00_2360);
																		BgL_auxz00_2932 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2933);
																	}
																	BgL_auxz00_2931 =
																		((BgL_sfunzf2ginfozf2_bglt)
																		BgL_auxz00_2932);
																}
																BgL_arg1485z00_1844 =
																	(((BgL_sfunzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2931))->BgL_bmarkz00);
														}}
														BgL_test1970z00_2924 =
															(BgL_arg1485z00_1844 ==
															BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75);
													}
													if (BgL_test1970z00_2924)
														{	/* Globalize/new_body.scm 81 */
															obj_t BgL_arg1453z00_1840;

															BgL_arg1453z00_1840 =
																CDR(((obj_t) BgL_obdingsz00_1833));
															{
																obj_t BgL_obdingsz00_2941;

																BgL_obdingsz00_2941 = BgL_arg1453z00_1840;
																BgL_obdingsz00_1833 = BgL_obdingsz00_2941;
																goto BgL_zc3z04anonymousza31422ze3z87_1834;
															}
														}
													else
														{	/* Globalize/new_body.scm 83 */
															obj_t BgL_arg1454z00_1841;
															obj_t BgL_arg1472z00_1842;

															{	/* Globalize/new_body.scm 83 */
																obj_t BgL_arg1473z00_1843;

																BgL_arg1473z00_1843 =
																	CAR(((obj_t) BgL_obdingsz00_1833));
																BgL_arg1454z00_1841 =
																	MAKE_YOUNG_PAIR(BgL_arg1473z00_1843,
																	BgL_nbdingsz00_1832);
															}
															BgL_arg1472z00_1842 =
																CDR(((obj_t) BgL_obdingsz00_1833));
															{
																obj_t BgL_obdingsz00_2948;
																obj_t BgL_nbdingsz00_2947;

																BgL_nbdingsz00_2947 = BgL_arg1454z00_1841;
																BgL_obdingsz00_2948 = BgL_arg1472z00_1842;
																BgL_obdingsz00_1833 = BgL_obdingsz00_2948;
																BgL_nbdingsz00_1832 = BgL_nbdingsz00_2947;
																goto BgL_zc3z04anonymousza31422ze3z87_1834;
															}
														}
												}
										}
										{	/* Globalize/new_body.scm 85 */
											BgL_nodez00_bglt BgL_new2zd2bodyzd2_1800;

											if (NULLP(BgL_nbindingsz00_1799))
												{	/* Globalize/new_body.scm 85 */
													BgL_new2zd2bodyzd2_1800 = BgL_newzd2bodyzd2_1783;
												}
											else
												{	/* Globalize/new_body.scm 87 */
													BgL_letzd2funzd2_bglt BgL_new1114z00_1827;

													{	/* Globalize/new_body.scm 88 */
														BgL_letzd2funzd2_bglt BgL_new1113z00_1828;

														BgL_new1113z00_1828 =
															((BgL_letzd2funzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_letzd2funzd2_bgl))));
														{	/* Globalize/new_body.scm 88 */
															long BgL_arg1421z00_1829;

															BgL_arg1421z00_1829 =
																BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1113z00_1828),
																BgL_arg1421z00_1829);
														}
														{	/* Globalize/new_body.scm 88 */
															BgL_objectz00_bglt BgL_tmpz00_2955;

															BgL_tmpz00_2955 =
																((BgL_objectz00_bglt) BgL_new1113z00_1828);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2955, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1113z00_1828);
														BgL_new1114z00_1827 = BgL_new1113z00_1828;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1114z00_1827)))->
															BgL_locz00) =
														((obj_t) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt)
																			BgL_oldzd2bodyzd2_1781)))->BgL_locz00)),
														BUNSPEC);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_new1114z00_1827)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_nodez00_bglt)
																	COBJECT(((BgL_nodez00_bglt)
																			BgL_oldzd2bodyzd2_1781)))->BgL_typez00)),
														BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1114z00_1827)))->
															BgL_sidezd2effectzd2) =
														((obj_t) BUNSPEC), BUNSPEC);
													((((BgL_nodezf2effectzf2_bglt)
																COBJECT(((BgL_nodezf2effectzf2_bglt)
																		BgL_new1114z00_1827)))->BgL_keyz00) =
														((obj_t) BINT(-1L)), BUNSPEC);
													((((BgL_letzd2funzd2_bglt)
																COBJECT(BgL_new1114z00_1827))->BgL_localsz00) =
														((obj_t) BgL_nbindingsz00_1799), BUNSPEC);
													((((BgL_letzd2funzd2_bglt)
																COBJECT(BgL_new1114z00_1827))->BgL_bodyz00) =
														((BgL_nodez00_bglt) BgL_newzd2bodyzd2_1783),
														BUNSPEC);
													BgL_new2zd2bodyzd2_1800 =
														((BgL_nodez00_bglt) BgL_new1114z00_1827);
												}
											{	/* Globalize/new_body.scm 95 */
												obj_t BgL_g1115z00_1801;

												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2975;

													{
														obj_t BgL_auxz00_2976;

														{	/* Globalize/new_body.scm 95 */
															BgL_objectz00_bglt BgL_tmpz00_2977;

															BgL_tmpz00_2977 =
																((BgL_objectz00_bglt)
																((BgL_sfunz00_bglt) BgL_infoz00_1780));
															BgL_auxz00_2976 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2977);
														}
														BgL_auxz00_2975 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2976);
													}
													BgL_g1115z00_1801 =
														(((BgL_sfunzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_2975))->BgL_ctoz00);
												}
												{
													obj_t BgL_nctoz00_1803;
													obj_t BgL_nbindingsz00_1804;

													BgL_nctoz00_1803 = BgL_g1115z00_1801;
													BgL_nbindingsz00_1804 = BgL_nbindingsz00_1799;
												BgL_zc3z04anonymousza31365ze3z87_1805:
													if (NULLP(BgL_nbindingsz00_1804))
														{
															BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2985;

															{
																obj_t BgL_auxz00_2986;

																{	/* Globalize/new_body.scm 98 */
																	BgL_objectz00_bglt BgL_tmpz00_2987;

																	BgL_tmpz00_2987 =
																		((BgL_objectz00_bglt)
																		((BgL_sfunz00_bglt) BgL_infoz00_1780));
																	BgL_auxz00_2986 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2987);
																}
																BgL_auxz00_2985 =
																	((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_2986);
															}
															((((BgL_sfunzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_2985))->BgL_ctoz00) =
																((obj_t) BgL_nctoz00_1803), BUNSPEC);
														}
													else
														{	/* Globalize/new_body.scm 99 */
															obj_t BgL_g1116z00_1807;

															{	/* Globalize/new_body.scm 100 */
																BgL_sfunz00_bglt BgL_oz00_2379;

																BgL_oz00_2379 =
																	((BgL_sfunz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					((BgL_localz00_bglt)
																						CAR(
																							((obj_t)
																								BgL_nbindingsz00_1804))))))->
																		BgL_valuez00));
																{
																	BgL_sfunzf2ginfozf2_bglt BgL_auxz00_2999;

																	{
																		obj_t BgL_auxz00_3000;

																		{	/* Globalize/new_body.scm 100 */
																			BgL_objectz00_bglt BgL_tmpz00_3001;

																			BgL_tmpz00_3001 =
																				((BgL_objectz00_bglt) BgL_oz00_2379);
																			BgL_auxz00_3000 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_3001);
																		}
																		BgL_auxz00_2999 =
																			((BgL_sfunzf2ginfozf2_bglt)
																			BgL_auxz00_3000);
																	}
																	BgL_g1116z00_1807 =
																		(((BgL_sfunzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_2999))->BgL_ctoz00);
																}
															}
															{
																obj_t BgL_nctoz00_1809;
																obj_t BgL_lctoz00_1810;

																BgL_nctoz00_1809 = BgL_nctoz00_1803;
																BgL_lctoz00_1810 = BgL_g1116z00_1807;
															BgL_zc3z04anonymousza31367ze3z87_1811:
																if (NULLP(BgL_lctoz00_1810))
																	{	/* Globalize/new_body.scm 104 */
																		obj_t BgL_arg1370z00_1813;

																		BgL_arg1370z00_1813 =
																			CDR(((obj_t) BgL_nbindingsz00_1804));
																		{
																			obj_t BgL_nbindingsz00_3011;
																			obj_t BgL_nctoz00_3010;

																			BgL_nctoz00_3010 = BgL_nctoz00_1809;
																			BgL_nbindingsz00_3011 =
																				BgL_arg1370z00_1813;
																			BgL_nbindingsz00_1804 =
																				BgL_nbindingsz00_3011;
																			BgL_nctoz00_1803 = BgL_nctoz00_3010;
																			goto
																				BgL_zc3z04anonymousza31365ze3z87_1805;
																		}
																	}
																else
																	{	/* Globalize/new_body.scm 103 */
																		if (CBOOL
																			(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(CAR(((obj_t) BgL_lctoz00_1810)),
																					BgL_nctoz00_1809)))
																			{	/* Globalize/new_body.scm 106 */
																				obj_t BgL_arg1375z00_1816;

																				BgL_arg1375z00_1816 =
																					CDR(((obj_t) BgL_lctoz00_1810));
																				{
																					obj_t BgL_lctoz00_3019;

																					BgL_lctoz00_3019 =
																						BgL_arg1375z00_1816;
																					BgL_lctoz00_1810 = BgL_lctoz00_3019;
																					goto
																						BgL_zc3z04anonymousza31367ze3z87_1811;
																				}
																			}
																		else
																			{	/* Globalize/new_body.scm 108 */
																				obj_t BgL_arg1376z00_1817;
																				obj_t BgL_arg1377z00_1818;

																				{	/* Globalize/new_body.scm 108 */
																					obj_t BgL_arg1378z00_1819;

																					BgL_arg1378z00_1819 =
																						CAR(((obj_t) BgL_lctoz00_1810));
																					BgL_arg1376z00_1817 =
																						MAKE_YOUNG_PAIR(BgL_arg1378z00_1819,
																						BgL_nctoz00_1809);
																				}
																				BgL_arg1377z00_1818 =
																					CDR(((obj_t) BgL_lctoz00_1810));
																				{
																					obj_t BgL_lctoz00_3026;
																					obj_t BgL_nctoz00_3025;

																					BgL_nctoz00_3025 =
																						BgL_arg1376z00_1817;
																					BgL_lctoz00_3026 =
																						BgL_arg1377z00_1818;
																					BgL_lctoz00_1810 = BgL_lctoz00_3026;
																					BgL_nctoz00_1809 = BgL_nctoz00_3025;
																					goto
																						BgL_zc3z04anonymousza31367ze3z87_1811;
																				}
																			}
																	}
															}
														}
												}
											}
											{	/* Globalize/new_body.scm 119 */
												BgL_nodez00_bglt BgL_arg1410z00_1825;

												BgL_arg1410z00_1825 =
													BGl_remz12z12zzglobaliza7e_newzd2bodyz75
													(BgL_new2zd2bodyzd2_1800, BgL_localz00_5,
													BgL_localz00_5);
												{
													BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3028;

													{
														obj_t BgL_auxz00_3029;

														{	/* Globalize/new_body.scm 119 */
															BgL_objectz00_bglt BgL_tmpz00_3030;

															BgL_tmpz00_3030 =
																((BgL_objectz00_bglt)
																((BgL_sfunz00_bglt) BgL_infoz00_1780));
															BgL_auxz00_3029 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3030);
														}
														BgL_auxz00_3028 =
															((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3029);
													}
													return
														((((BgL_sfunzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_3028))->BgL_newzd2bodyzd2) =
														((obj_t) ((obj_t) BgL_arg1410z00_1825)), BUNSPEC);
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* is-in? */
	bool_t BGl_iszd2inzf3z21zzglobaliza7e_newzd2bodyz75(obj_t BgL_f1z00_6,
		obj_t BgL_f2z00_7)
	{
		{	/* Globalize/new_body.scm 126 */
		BGl_iszd2inzf3z21zzglobaliza7e_newzd2bodyz75:
			{	/* Globalize/new_body.scm 127 */
				BgL_valuez00_bglt BgL_infoz00_1871;

				BgL_infoz00_1871 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_localz00_bglt) BgL_f1z00_6))))->BgL_valuez00);
				{	/* Globalize/new_body.scm 129 */
					bool_t BgL_test1975z00_3040;

					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3041;

						{
							obj_t BgL_auxz00_3042;

							{	/* Globalize/new_body.scm 129 */
								BgL_objectz00_bglt BgL_tmpz00_3043;

								BgL_tmpz00_3043 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_infoz00_1871));
								BgL_auxz00_3042 = BGL_OBJECT_WIDENING(BgL_tmpz00_3043);
							}
							BgL_auxz00_3041 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3042);
						}
						BgL_test1975z00_3040 =
							(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3041))->
							BgL_gzf3zf3);
					}
					if (BgL_test1975z00_3040)
						{	/* Globalize/new_body.scm 129 */
							return ((bool_t) 0);
						}
					else
						{	/* Globalize/new_body.scm 131 */
							bool_t BgL_test1976z00_3049;

							{	/* Globalize/new_body.scm 131 */
								obj_t BgL_arg1584z00_1881;

								{
									BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3050;

									{
										obj_t BgL_auxz00_3051;

										{	/* Globalize/new_body.scm 131 */
											BgL_objectz00_bglt BgL_tmpz00_3052;

											BgL_tmpz00_3052 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_infoz00_1871));
											BgL_auxz00_3051 = BGL_OBJECT_WIDENING(BgL_tmpz00_3052);
										}
										BgL_auxz00_3050 =
											((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3051);
									}
									BgL_arg1584z00_1881 =
										(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3050))->
										BgL_integratorz00);
								}
								{	/* Globalize/new_body.scm 131 */
									obj_t BgL_classz00_2393;

									BgL_classz00_2393 = BGl_localz00zzast_varz00;
									if (BGL_OBJECTP(BgL_arg1584z00_1881))
										{	/* Globalize/new_body.scm 131 */
											BgL_objectz00_bglt BgL_arg1807z00_2395;

											BgL_arg1807z00_2395 =
												(BgL_objectz00_bglt) (BgL_arg1584z00_1881);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Globalize/new_body.scm 131 */
													long BgL_idxz00_2401;

													BgL_idxz00_2401 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2395);
													BgL_test1976z00_3049 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2401 + 2L)) == BgL_classz00_2393);
												}
											else
												{	/* Globalize/new_body.scm 131 */
													bool_t BgL_res1897z00_2426;

													{	/* Globalize/new_body.scm 131 */
														obj_t BgL_oclassz00_2409;

														{	/* Globalize/new_body.scm 131 */
															obj_t BgL_arg1815z00_2417;
															long BgL_arg1816z00_2418;

															BgL_arg1815z00_2417 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Globalize/new_body.scm 131 */
																long BgL_arg1817z00_2419;

																BgL_arg1817z00_2419 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2395);
																BgL_arg1816z00_2418 =
																	(BgL_arg1817z00_2419 - OBJECT_TYPE);
															}
															BgL_oclassz00_2409 =
																VECTOR_REF(BgL_arg1815z00_2417,
																BgL_arg1816z00_2418);
														}
														{	/* Globalize/new_body.scm 131 */
															bool_t BgL__ortest_1115z00_2410;

															BgL__ortest_1115z00_2410 =
																(BgL_classz00_2393 == BgL_oclassz00_2409);
															if (BgL__ortest_1115z00_2410)
																{	/* Globalize/new_body.scm 131 */
																	BgL_res1897z00_2426 =
																		BgL__ortest_1115z00_2410;
																}
															else
																{	/* Globalize/new_body.scm 131 */
																	long BgL_odepthz00_2411;

																	{	/* Globalize/new_body.scm 131 */
																		obj_t BgL_arg1804z00_2412;

																		BgL_arg1804z00_2412 = (BgL_oclassz00_2409);
																		BgL_odepthz00_2411 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2412);
																	}
																	if ((2L < BgL_odepthz00_2411))
																		{	/* Globalize/new_body.scm 131 */
																			obj_t BgL_arg1802z00_2414;

																			{	/* Globalize/new_body.scm 131 */
																				obj_t BgL_arg1803z00_2415;

																				BgL_arg1803z00_2415 =
																					(BgL_oclassz00_2409);
																				BgL_arg1802z00_2414 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2415, 2L);
																			}
																			BgL_res1897z00_2426 =
																				(BgL_arg1802z00_2414 ==
																				BgL_classz00_2393);
																		}
																	else
																		{	/* Globalize/new_body.scm 131 */
																			BgL_res1897z00_2426 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1976z00_3049 = BgL_res1897z00_2426;
												}
										}
									else
										{	/* Globalize/new_body.scm 131 */
											BgL_test1976z00_3049 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test1976z00_3049)
								{	/* Globalize/new_body.scm 133 */
									bool_t BgL_test1981z00_3080;

									{	/* Globalize/new_body.scm 133 */
										obj_t BgL_arg1576z00_1880;

										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3081;

											{
												obj_t BgL_auxz00_3082;

												{	/* Globalize/new_body.scm 133 */
													BgL_objectz00_bglt BgL_tmpz00_3083;

													BgL_tmpz00_3083 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_infoz00_1871));
													BgL_auxz00_3082 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3083);
												}
												BgL_auxz00_3081 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3082);
											}
											BgL_arg1576z00_1880 =
												(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3081))->
												BgL_integratorz00);
										}
										BgL_test1981z00_3080 = (BgL_arg1576z00_1880 == BgL_f2z00_7);
									}
									if (BgL_test1981z00_3080)
										{	/* Globalize/new_body.scm 133 */
											return ((bool_t) 1);
										}
									else
										{	/* Globalize/new_body.scm 135 */
											obj_t BgL_arg1575z00_1879;

											{
												BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3090;

												{
													obj_t BgL_auxz00_3091;

													{	/* Globalize/new_body.scm 135 */
														BgL_objectz00_bglt BgL_tmpz00_3092;

														BgL_tmpz00_3092 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_infoz00_1871));
														BgL_auxz00_3091 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3092);
													}
													BgL_auxz00_3090 =
														((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3091);
												}
												BgL_arg1575z00_1879 =
													(((BgL_sfunzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_3090))->BgL_integratorz00);
											}
											{
												obj_t BgL_f1z00_3098;

												BgL_f1z00_3098 = BgL_arg1575z00_1879;
												BgL_f1z00_6 = BgL_f1z00_3098;
												goto BGl_iszd2inzf3z21zzglobaliza7e_newzd2bodyz75;
											}
										}
								}
							else
								{	/* Globalize/new_body.scm 131 */
									return ((bool_t) 1);
								}
						}
				}
			}
		}

	}



/* rem*! */
	obj_t BGl_remza2z12zb0zzglobaliza7e_newzd2bodyz75(obj_t BgL_nodeza2za2_74,
		obj_t BgL_ownerz00_75, obj_t BgL_currentz00_76)
	{
		{	/* Globalize/new_body.scm 377 */
			{
				obj_t BgL_nodeza2za2_1883;

				BgL_nodeza2za2_1883 = BgL_nodeza2za2_74;
			BgL_zc3z04anonymousza31585ze3z87_1884:
				if (NULLP(BgL_nodeza2za2_1883))
					{	/* Globalize/new_body.scm 379 */
						return CNST_TABLE_REF(0);
					}
				else
					{	/* Globalize/new_body.scm 379 */
						{	/* Globalize/new_body.scm 382 */
							BgL_nodez00_bglt BgL_arg1589z00_1886;

							{	/* Globalize/new_body.scm 382 */
								obj_t BgL_arg1591z00_1887;

								BgL_arg1591z00_1887 = CAR(((obj_t) BgL_nodeza2za2_1883));
								BgL_arg1589z00_1886 =
									BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
									((BgL_nodez00_bglt) BgL_arg1591z00_1887), BgL_ownerz00_75,
									BgL_currentz00_76);
							}
							{	/* Globalize/new_body.scm 382 */
								obj_t BgL_auxz00_3108;
								obj_t BgL_tmpz00_3106;

								BgL_auxz00_3108 = ((obj_t) BgL_arg1589z00_1886);
								BgL_tmpz00_3106 = ((obj_t) BgL_nodeza2za2_1883);
								SET_CAR(BgL_tmpz00_3106, BgL_auxz00_3108);
							}
						}
						{	/* Globalize/new_body.scm 383 */
							obj_t BgL_arg1593z00_1888;

							BgL_arg1593z00_1888 = CDR(((obj_t) BgL_nodeza2za2_1883));
							{
								obj_t BgL_nodeza2za2_3113;

								BgL_nodeza2za2_3113 = BgL_arg1593z00_1888;
								BgL_nodeza2za2_1883 = BgL_nodeza2za2_3113;
								goto BgL_zc3z04anonymousza31585ze3z87_1884;
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_newzd2bodyz75(void)
	{
		{	/* Globalize/new_body.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_newzd2bodyz75(void)
	{
		{	/* Globalize/new_body.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_proc1904z00zzglobaliza7e_newzd2bodyz75, BGl_nodez00zzast_nodez00,
				BGl_string1905z00zzglobaliza7e_newzd2bodyz75);
		}

	}



/* &rem!1287 */
	obj_t BGl_z62remz121287z70zzglobaliza7e_newzd2bodyz75(obj_t BgL_envz00_2573,
		obj_t BgL_nodez00_2574, obj_t BgL_ownerz00_2575, obj_t BgL_currentz00_2576)
	{
		{	/* Globalize/new_body.scm 148 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string1906z00zzglobaliza7e_newzd2bodyz75,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2574)));
		}

	}



/* rem! */
	BgL_nodez00_bglt BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_nodez00_bglt
		BgL_nodez00_8, obj_t BgL_ownerz00_9, obj_t BgL_currentz00_10)
	{
		{	/* Globalize/new_body.scm 148 */
			{	/* Globalize/new_body.scm 148 */
				obj_t BgL_method1288z00_1896;

				{	/* Globalize/new_body.scm 148 */
					obj_t BgL_res1902z00_2464;

					{	/* Globalize/new_body.scm 148 */
						long BgL_objzd2classzd2numz00_2435;

						BgL_objzd2classzd2numz00_2435 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_8));
						{	/* Globalize/new_body.scm 148 */
							obj_t BgL_arg1811z00_2436;

							BgL_arg1811z00_2436 =
								PROCEDURE_REF(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
								(int) (1L));
							{	/* Globalize/new_body.scm 148 */
								int BgL_offsetz00_2439;

								BgL_offsetz00_2439 = (int) (BgL_objzd2classzd2numz00_2435);
								{	/* Globalize/new_body.scm 148 */
									long BgL_offsetz00_2440;

									BgL_offsetz00_2440 =
										((long) (BgL_offsetz00_2439) - OBJECT_TYPE);
									{	/* Globalize/new_body.scm 148 */
										long BgL_modz00_2441;

										BgL_modz00_2441 =
											(BgL_offsetz00_2440 >> (int) ((long) ((int) (4L))));
										{	/* Globalize/new_body.scm 148 */
											long BgL_restz00_2443;

											BgL_restz00_2443 =
												(BgL_offsetz00_2440 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Globalize/new_body.scm 148 */

												{	/* Globalize/new_body.scm 148 */
													obj_t BgL_bucketz00_2445;

													BgL_bucketz00_2445 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2436), BgL_modz00_2441);
													BgL_res1902z00_2464 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2445), BgL_restz00_2443);
					}}}}}}}}
					BgL_method1288z00_1896 = BgL_res1902z00_2464;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1288z00_1896,
						((obj_t) BgL_nodez00_8), BgL_ownerz00_9, BgL_currentz00_10));
			}
		}

	}



/* &rem! */
	BgL_nodez00_bglt BGl_z62remz12z70zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2577, obj_t BgL_nodez00_2578, obj_t BgL_ownerz00_2579,
		obj_t BgL_currentz00_2580)
	{
		{	/* Globalize/new_body.scm 148 */
			return
				BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
				((BgL_nodez00_bglt) BgL_nodez00_2578), BgL_ownerz00_2579,
				BgL_currentz00_2580);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_newzd2bodyz75(void)
	{
		{	/* Globalize/new_body.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_atomz00zzast_nodez00, BGl_proc1907z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_kwotez00zzast_nodez00, BGl_proc1909z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_varz00zzast_nodez00, BGl_proc1910z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_sequencez00zzast_nodez00,
				BGl_proc1911z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_syncz00zzast_nodez00, BGl_proc1912z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_appz00zzast_nodez00, BGl_proc1913z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc1914z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_funcallz00zzast_nodez00, BGl_proc1915z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_externz00zzast_nodez00, BGl_proc1916z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_castz00zzast_nodez00, BGl_proc1917z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_setqz00zzast_nodez00, BGl_proc1918z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_conditionalz00zzast_nodez00,
				BGl_proc1919z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_failz00zzast_nodez00, BGl_proc1920z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_switchz00zzast_nodez00, BGl_proc1921z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1922z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1923z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1924z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1925z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc1926z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc1927z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_remz12zd2envzc0zzglobaliza7e_newzd2bodyz75,
				BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1928z00zzglobaliza7e_newzd2bodyz75,
				BGl_string1908z00zzglobaliza7e_newzd2bodyz75);
		}

	}



/* &rem!-box-set!1330 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2boxzd2setz121330z62zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2602, obj_t BgL_nodez00_2603, obj_t BgL_ownerz00_2604,
		obj_t BgL_currentz00_2605)
	{
		{	/* Globalize/new_body.scm 368 */
			{
				BgL_varz00_bglt BgL_auxz00_3175;

				{	/* Globalize/new_body.scm 370 */
					BgL_varz00_bglt BgL_arg1799z00_2699;

					BgL_arg1799z00_2699 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2603)))->BgL_varz00);
					BgL_auxz00_3175 =
						((BgL_varz00_bglt)
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
							((BgL_nodez00_bglt) BgL_arg1799z00_2699), BgL_ownerz00_2604,
							BgL_currentz00_2605));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2603)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3175), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3183;

				{	/* Globalize/new_body.scm 371 */
					BgL_nodez00_bglt BgL_arg1805z00_2700;

					BgL_arg1805z00_2700 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2603)))->BgL_valuez00);
					BgL_auxz00_3183 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1805z00_2700,
						BgL_ownerz00_2604, BgL_currentz00_2605);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2603)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3183), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2603));
		}

	}



/* &rem!-box-ref1328 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2boxzd2ref1328z70zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2606, obj_t BgL_nodez00_2607, obj_t BgL_ownerz00_2608,
		obj_t BgL_currentz00_2609)
	{
		{	/* Globalize/new_body.scm 360 */
			{
				BgL_varz00_bglt BgL_auxz00_3191;

				{	/* Globalize/new_body.scm 362 */
					BgL_varz00_bglt BgL_arg1798z00_2702;

					BgL_arg1798z00_2702 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2607)))->BgL_varz00);
					BgL_auxz00_3191 =
						((BgL_varz00_bglt)
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
							((BgL_nodez00_bglt) BgL_arg1798z00_2702), BgL_ownerz00_2608,
							BgL_currentz00_2609));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2607)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3191), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2607));
		}

	}



/* &rem!-make-box1326 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2makezd2box1326z70zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2610, obj_t BgL_nodez00_2611, obj_t BgL_ownerz00_2612,
		obj_t BgL_currentz00_2613)
	{
		{	/* Globalize/new_body.scm 352 */
			{
				BgL_nodez00_bglt BgL_auxz00_3201;

				{	/* Globalize/new_body.scm 354 */
					BgL_nodez00_bglt BgL_arg1775z00_2704;

					BgL_arg1775z00_2704 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2611)))->BgL_valuez00);
					BgL_auxz00_3201 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1775z00_2704,
						BgL_ownerz00_2612, BgL_currentz00_2613);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2611)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3201), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2611));
		}

	}



/* &rem!-jump-ex-it1324 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2jumpzd2exzd2it1324za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2614, obj_t BgL_nodez00_2615, obj_t BgL_ownerz00_2616,
		obj_t BgL_currentz00_2617)
	{
		{	/* Globalize/new_body.scm 343 */
			{
				BgL_nodez00_bglt BgL_auxz00_3209;

				{	/* Globalize/new_body.scm 345 */
					BgL_nodez00_bglt BgL_arg1771z00_2706;

					BgL_arg1771z00_2706 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2615)))->BgL_exitz00);
					BgL_auxz00_3209 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1771z00_2706,
						BgL_ownerz00_2616, BgL_currentz00_2617);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2615)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3209), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3215;

				{	/* Globalize/new_body.scm 346 */
					BgL_nodez00_bglt BgL_arg1773z00_2707;

					BgL_arg1773z00_2707 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2615)))->
						BgL_valuez00);
					BgL_auxz00_3215 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1773z00_2707,
						BgL_ownerz00_2616, BgL_currentz00_2617);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2615)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_3215), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2615));
		}

	}



/* &rem!-set-ex-it1322 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2setzd2exzd2it1322za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2618, obj_t BgL_nodez00_2619, obj_t BgL_ownerz00_2620,
		obj_t BgL_currentz00_2621)
	{
		{	/* Globalize/new_body.scm 334 */
			{
				BgL_nodez00_bglt BgL_auxz00_3223;

				{	/* Globalize/new_body.scm 336 */
					BgL_nodez00_bglt BgL_arg1767z00_2709;

					BgL_arg1767z00_2709 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2619)))->BgL_bodyz00);
					BgL_auxz00_3223 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1767z00_2709,
						BgL_ownerz00_2620, BgL_currentz00_2621);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2619)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3223), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3229;

				{	/* Globalize/new_body.scm 337 */
					BgL_nodez00_bglt BgL_arg1770z00_2710;

					BgL_arg1770z00_2710 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2619)))->
						BgL_onexitz00);
					BgL_auxz00_3229 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1770z00_2710,
						BgL_ownerz00_2620, BgL_currentz00_2621);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2619)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3229), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2619));
		}

	}



/* &rem!-let-var1320 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2letzd2var1320z70zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2622, obj_t BgL_nodez00_2623, obj_t BgL_ownerz00_2624,
		obj_t BgL_currentz00_2625)
	{
		{	/* Globalize/new_body.scm 323 */
			{
				BgL_nodez00_bglt BgL_auxz00_3237;

				{	/* Globalize/new_body.scm 325 */
					BgL_nodez00_bglt BgL_arg1755z00_2712;

					BgL_arg1755z00_2712 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2623)))->BgL_bodyz00);
					BgL_auxz00_3237 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1755z00_2712,
						BgL_ownerz00_2624, BgL_currentz00_2625);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2623)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3237), BUNSPEC);
			}
			{	/* Globalize/new_body.scm 326 */
				obj_t BgL_g1286z00_2713;

				BgL_g1286z00_2713 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2623)))->BgL_bindingsz00);
				{
					obj_t BgL_l1284z00_2715;

					BgL_l1284z00_2715 = BgL_g1286z00_2713;
				BgL_zc3z04anonymousza31756ze3z87_2714:
					if (PAIRP(BgL_l1284z00_2715))
						{	/* Globalize/new_body.scm 326 */
							{	/* Globalize/new_body.scm 327 */
								obj_t BgL_bindingz00_2716;

								BgL_bindingz00_2716 = CAR(BgL_l1284z00_2715);
								{	/* Globalize/new_body.scm 327 */
									BgL_nodez00_bglt BgL_arg1761z00_2717;

									{	/* Globalize/new_body.scm 327 */
										obj_t BgL_arg1762z00_2718;

										BgL_arg1762z00_2718 = CDR(((obj_t) BgL_bindingz00_2716));
										BgL_arg1761z00_2717 =
											BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
											((BgL_nodez00_bglt) BgL_arg1762z00_2718),
											BgL_ownerz00_2624, BgL_currentz00_2625);
									}
									{	/* Globalize/new_body.scm 327 */
										obj_t BgL_auxz00_3254;
										obj_t BgL_tmpz00_3252;

										BgL_auxz00_3254 = ((obj_t) BgL_arg1761z00_2717);
										BgL_tmpz00_3252 = ((obj_t) BgL_bindingz00_2716);
										SET_CDR(BgL_tmpz00_3252, BgL_auxz00_3254);
									}
								}
							}
							{
								obj_t BgL_l1284z00_3257;

								BgL_l1284z00_3257 = CDR(BgL_l1284z00_2715);
								BgL_l1284z00_2715 = BgL_l1284z00_3257;
								goto BgL_zc3z04anonymousza31756ze3z87_2714;
							}
						}
					else
						{	/* Globalize/new_body.scm 326 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2623));
		}

	}



/* &rem!-let-fun1318 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2letzd2fun1318z70zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2626, obj_t BgL_nodez00_2627, obj_t BgL_ownerz00_2628,
		obj_t BgL_currentz00_2629)
	{
		{	/* Globalize/new_body.scm 270 */
			{
				BgL_nodez00_bglt BgL_auxz00_3261;

				{	/* Globalize/new_body.scm 272 */
					BgL_nodez00_bglt BgL_arg1678z00_2720;

					BgL_arg1678z00_2720 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2627)))->BgL_bodyz00);
					BgL_auxz00_3261 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1678z00_2720,
						BgL_ownerz00_2628, BgL_currentz00_2629);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2627)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3261), BUNSPEC);
			}
			{
				obj_t BgL_obindingsz00_2722;
				obj_t BgL_nbindingsz00_2723;

				{	/* Globalize/new_body.scm 273 */
					obj_t BgL_arg1681z00_2748;

					BgL_arg1681z00_2748 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2627)))->BgL_localsz00);
					{
						BgL_letzd2funzd2_bglt BgL_auxz00_3269;

						BgL_obindingsz00_2722 = BgL_arg1681z00_2748;
						BgL_nbindingsz00_2723 = BNIL;
					BgL_liipz00_2721:
						if (NULLP(BgL_obindingsz00_2722))
							{	/* Globalize/new_body.scm 276 */
								((((BgL_letzd2funzd2_bglt) COBJECT(
												((BgL_letzd2funzd2_bglt) BgL_nodez00_2627)))->
										BgL_localsz00) = ((obj_t) BgL_nbindingsz00_2723), BUNSPEC);
								BgL_auxz00_3269 = ((BgL_letzd2funzd2_bglt) BgL_nodez00_2627);
							}
						else
							{	/* Globalize/new_body.scm 279 */
								bool_t BgL_test1985z00_3275;

								{	/* Globalize/new_body.scm 279 */
									long BgL_arg1752z00_2724;

									{	/* Globalize/new_body.scm 279 */
										BgL_sfunz00_bglt BgL_oz00_2725;

										BgL_oz00_2725 =
											((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_obindingsz00_2722))))))->
												BgL_valuez00));
										{
											BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3282;

											{
												obj_t BgL_auxz00_3283;

												{	/* Globalize/new_body.scm 279 */
													BgL_objectz00_bglt BgL_tmpz00_3284;

													BgL_tmpz00_3284 =
														((BgL_objectz00_bglt) BgL_oz00_2725);
													BgL_auxz00_3283 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3284);
												}
												BgL_auxz00_3282 =
													((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3283);
											}
											BgL_arg1752z00_2724 =
												(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3282))->
												BgL_bmarkz00);
									}}
									BgL_test1985z00_3275 =
										(BgL_arg1752z00_2724 ==
										BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75);
								}
								if (BgL_test1985z00_3275)
									{	/* Globalize/new_body.scm 279 */
										{	/* Globalize/new_body.scm 286 */
											bool_t BgL_test1986z00_3290;

											{	/* Globalize/new_body.scm 287 */
												obj_t BgL_arg1710z00_2726;

												{	/* Globalize/new_body.scm 287 */
													BgL_sfunz00_bglt BgL_oz00_2727;

													BgL_oz00_2727 =
														((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_obindingsz00_2722))))))->
															BgL_valuez00));
													{
														BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3297;

														{
															obj_t BgL_auxz00_3298;

															{	/* Globalize/new_body.scm 287 */
																BgL_objectz00_bglt BgL_tmpz00_3299;

																BgL_tmpz00_3299 =
																	((BgL_objectz00_bglt) BgL_oz00_2727);
																BgL_auxz00_3298 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3299);
															}
															BgL_auxz00_3297 =
																((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3298);
														}
														BgL_arg1710z00_2726 =
															(((BgL_sfunzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_3297))->BgL_pluggedzd2inzd2);
													}
												}
												BgL_test1986z00_3290 =
													(BgL_currentz00_2629 == BgL_arg1710z00_2726);
											}
											if (BgL_test1986z00_3290)
												{	/* Globalize/new_body.scm 289 */
													obj_t BgL_arg1703z00_2728;
													obj_t BgL_arg1705z00_2729;

													BgL_arg1703z00_2728 =
														CDR(((obj_t) BgL_obindingsz00_2722));
													{	/* Globalize/new_body.scm 289 */
														obj_t BgL_arg1708z00_2730;

														BgL_arg1708z00_2730 =
															CAR(((obj_t) BgL_obindingsz00_2722));
														BgL_arg1705z00_2729 =
															MAKE_YOUNG_PAIR(BgL_arg1708z00_2730,
															BgL_nbindingsz00_2723);
													}
													{
														obj_t BgL_nbindingsz00_3311;
														obj_t BgL_obindingsz00_3310;

														BgL_obindingsz00_3310 = BgL_arg1703z00_2728;
														BgL_nbindingsz00_3311 = BgL_arg1705z00_2729;
														BgL_nbindingsz00_2723 = BgL_nbindingsz00_3311;
														BgL_obindingsz00_2722 = BgL_obindingsz00_3310;
														goto BgL_liipz00_2721;
													}
												}
											else
												{	/* Globalize/new_body.scm 291 */
													obj_t BgL_arg1709z00_2731;

													BgL_arg1709z00_2731 =
														CDR(((obj_t) BgL_obindingsz00_2722));
													{
														obj_t BgL_obindingsz00_3314;

														BgL_obindingsz00_3314 = BgL_arg1709z00_2731;
														BgL_obindingsz00_2722 = BgL_obindingsz00_3314;
														goto BgL_liipz00_2721;
													}
												}
										}
									}
								else
									{	/* Globalize/new_body.scm 293 */
										bool_t BgL_test1987z00_3315;

										{	/* Globalize/new_body.scm 293 */
											obj_t BgL_arg1751z00_2732;

											BgL_arg1751z00_2732 =
												CAR(((obj_t) BgL_obindingsz00_2722));
											BgL_test1987z00_3315 =
												BGl_iszd2inzf3z21zzglobaliza7e_newzd2bodyz75
												(BgL_arg1751z00_2732, BgL_ownerz00_2628);
										}
										if (BgL_test1987z00_3315)
											{	/* Globalize/new_body.scm 293 */
												{	/* Globalize/new_body.scm 299 */
													BgL_valuez00_bglt BgL_arg1720z00_2733;

													BgL_arg1720z00_2733 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt)
																		CAR(
																			((obj_t) BgL_obindingsz00_2722))))))->
														BgL_valuez00);
													{	/* Globalize/new_body.scm 299 */
														long BgL_vz00_2734;

														BgL_vz00_2734 =
															BGl_za2roundza2z00zzglobaliza7e_newzd2bodyz75;
														{
															BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3324;

															{
																obj_t BgL_auxz00_3325;

																{	/* Globalize/new_body.scm 299 */
																	BgL_objectz00_bglt BgL_tmpz00_3326;

																	BgL_tmpz00_3326 =
																		((BgL_objectz00_bglt)
																		((BgL_sfunz00_bglt) BgL_arg1720z00_2733));
																	BgL_auxz00_3325 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_3326);
																}
																BgL_auxz00_3324 =
																	((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3325);
															}
															((((BgL_sfunzf2ginfozf2_bglt)
																		COBJECT(BgL_auxz00_3324))->BgL_bmarkz00) =
																((long) BgL_vz00_2734), BUNSPEC);
												}}}
												{	/* Globalize/new_body.scm 301 */
													BgL_valuez00_bglt BgL_arg1724z00_2735;

													BgL_arg1724z00_2735 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt)
																		CAR(
																			((obj_t) BgL_obindingsz00_2722))))))->
														BgL_valuez00);
													{
														BgL_sfunzf2ginfozf2_bglt BgL_auxz00_3337;

														{
															obj_t BgL_auxz00_3338;

															{	/* Globalize/new_body.scm 301 */
																BgL_objectz00_bglt BgL_tmpz00_3339;

																BgL_tmpz00_3339 =
																	((BgL_objectz00_bglt)
																	((BgL_sfunz00_bglt) BgL_arg1724z00_2735));
																BgL_auxz00_3338 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3339);
															}
															BgL_auxz00_3337 =
																((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_3338);
														}
														((((BgL_sfunzf2ginfozf2_bglt)
																	COBJECT(BgL_auxz00_3337))->
																BgL_pluggedzd2inzd2) =
															((obj_t) BgL_currentz00_2629), BUNSPEC);
												}}
												{	/* Globalize/new_body.scm 302 */
													BgL_valuez00_bglt BgL_funz00_2736;

													BgL_funz00_2736 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt)
																		CAR(
																			((obj_t) BgL_obindingsz00_2722))))))->
														BgL_valuez00);
													{	/* Globalize/new_body.scm 302 */
														obj_t BgL_bodz00_2737;

														BgL_bodz00_2737 =
															(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_funz00_2736)))->
															BgL_bodyz00);
														{	/* Globalize/new_body.scm 303 */

															{	/* Globalize/new_body.scm 304 */
																BgL_nodez00_bglt BgL_arg1734z00_2738;

																{	/* Globalize/new_body.scm 304 */
																	obj_t BgL_arg1735z00_2739;

																	BgL_arg1735z00_2739 =
																		CAR(((obj_t) BgL_obindingsz00_2722));
																	BgL_arg1734z00_2738 =
																		BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
																		((BgL_nodez00_bglt) BgL_bodz00_2737),
																		BgL_ownerz00_2628, BgL_arg1735z00_2739);
																}
																((((BgL_sfunz00_bglt) COBJECT(
																				((BgL_sfunz00_bglt) BgL_funz00_2736)))->
																		BgL_bodyz00) =
																	((obj_t) ((obj_t) BgL_arg1734z00_2738)),
																	BUNSPEC);
															}
															{	/* Globalize/new_body.scm 305 */
																obj_t BgL_arg1736z00_2740;
																obj_t BgL_arg1737z00_2741;

																BgL_arg1736z00_2740 =
																	CDR(((obj_t) BgL_obindingsz00_2722));
																{	/* Globalize/new_body.scm 306 */
																	obj_t BgL_arg1738z00_2742;

																	BgL_arg1738z00_2742 =
																		CAR(((obj_t) BgL_obindingsz00_2722));
																	BgL_arg1737z00_2741 =
																		MAKE_YOUNG_PAIR(BgL_arg1738z00_2742,
																		BgL_nbindingsz00_2723);
																}
																{
																	obj_t BgL_nbindingsz00_3365;
																	obj_t BgL_obindingsz00_3364;

																	BgL_obindingsz00_3364 = BgL_arg1736z00_2740;
																	BgL_nbindingsz00_3365 = BgL_arg1737z00_2741;
																	BgL_nbindingsz00_2723 = BgL_nbindingsz00_3365;
																	BgL_obindingsz00_2722 = BgL_obindingsz00_3364;
																	goto BgL_liipz00_2721;
																}
															}
														}
													}
												}
											}
										else
											{	/* Globalize/new_body.scm 307 */
												bool_t BgL_test1988z00_3366;

												{	/* Globalize/new_body.scm 307 */
													BgL_localz00_bglt BgL_oz00_2743;

													BgL_oz00_2743 =
														((BgL_localz00_bglt)
														CAR(((obj_t) BgL_obindingsz00_2722)));
													{
														BgL_localzf2ginfozf2_bglt BgL_auxz00_3370;

														{
															obj_t BgL_auxz00_3371;

															{	/* Globalize/new_body.scm 307 */
																BgL_objectz00_bglt BgL_tmpz00_3372;

																BgL_tmpz00_3372 =
																	((BgL_objectz00_bglt) BgL_oz00_2743);
																BgL_auxz00_3371 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_3372);
															}
															BgL_auxz00_3370 =
																((BgL_localzf2ginfozf2_bglt) BgL_auxz00_3371);
														}
														BgL_test1988z00_3366 =
															(((BgL_localzf2ginfozf2_bglt)
																COBJECT(BgL_auxz00_3370))->BgL_escapezf3zf3);
													}
												}
												if (BgL_test1988z00_3366)
													{	/* Globalize/new_body.scm 307 */
														{	/* Globalize/new_body.scm 311 */
															obj_t BgL_arg1746z00_2744;
															obj_t BgL_arg1747z00_2745;

															BgL_arg1746z00_2744 =
																CDR(((obj_t) BgL_obindingsz00_2722));
															{	/* Globalize/new_body.scm 312 */
																obj_t BgL_arg1748z00_2746;

																BgL_arg1748z00_2746 =
																	CAR(((obj_t) BgL_obindingsz00_2722));
																BgL_arg1747z00_2745 =
																	MAKE_YOUNG_PAIR(BgL_arg1748z00_2746,
																	BgL_nbindingsz00_2723);
															}
															{
																obj_t BgL_nbindingsz00_3383;
																obj_t BgL_obindingsz00_3382;

																BgL_obindingsz00_3382 = BgL_arg1746z00_2744;
																BgL_nbindingsz00_3383 = BgL_arg1747z00_2745;
																BgL_nbindingsz00_2723 = BgL_nbindingsz00_3383;
																BgL_obindingsz00_2722 = BgL_obindingsz00_3382;
																goto BgL_liipz00_2721;
															}
														}
													}
												else
													{	/* Globalize/new_body.scm 307 */
														{	/* Globalize/new_body.scm 317 */
															obj_t BgL_arg1749z00_2747;

															BgL_arg1749z00_2747 =
																CDR(((obj_t) BgL_obindingsz00_2722));
															{
																obj_t BgL_obindingsz00_3386;

																BgL_obindingsz00_3386 = BgL_arg1749z00_2747;
																BgL_obindingsz00_2722 = BgL_obindingsz00_3386;
																goto BgL_liipz00_2721;
															}
														}
													}
											}
									}
							}
						return ((BgL_nodez00_bglt) BgL_auxz00_3269);
					}
				}
			}
		}

	}



/* &rem!-switch1316 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2switch1316za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2630, obj_t BgL_nodez00_2631, obj_t BgL_ownerz00_2632,
		obj_t BgL_currentz00_2633)
	{
		{	/* Globalize/new_body.scm 259 */
			{
				BgL_nodez00_bglt BgL_auxz00_3388;

				{	/* Globalize/new_body.scm 261 */
					BgL_nodez00_bglt BgL_arg1654z00_2750;

					BgL_arg1654z00_2750 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2631)))->BgL_testz00);
					BgL_auxz00_3388 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1654z00_2750,
						BgL_ownerz00_2632, BgL_currentz00_2633);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2631)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3388), BUNSPEC);
			}
			{	/* Globalize/new_body.scm 262 */
				obj_t BgL_g1283z00_2751;

				BgL_g1283z00_2751 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2631)))->BgL_clausesz00);
				{
					obj_t BgL_l1281z00_2753;

					BgL_l1281z00_2753 = BgL_g1283z00_2751;
				BgL_zc3z04anonymousza31655ze3z87_2752:
					if (PAIRP(BgL_l1281z00_2753))
						{	/* Globalize/new_body.scm 262 */
							{	/* Globalize/new_body.scm 263 */
								obj_t BgL_clausez00_2754;

								BgL_clausez00_2754 = CAR(BgL_l1281z00_2753);
								{	/* Globalize/new_body.scm 263 */
									BgL_nodez00_bglt BgL_arg1661z00_2755;

									{	/* Globalize/new_body.scm 263 */
										obj_t BgL_arg1663z00_2756;

										BgL_arg1663z00_2756 = CDR(((obj_t) BgL_clausez00_2754));
										BgL_arg1661z00_2755 =
											BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
											((BgL_nodez00_bglt) BgL_arg1663z00_2756),
											BgL_ownerz00_2632, BgL_currentz00_2633);
									}
									{	/* Globalize/new_body.scm 263 */
										obj_t BgL_auxz00_3405;
										obj_t BgL_tmpz00_3403;

										BgL_auxz00_3405 = ((obj_t) BgL_arg1661z00_2755);
										BgL_tmpz00_3403 = ((obj_t) BgL_clausez00_2754);
										SET_CDR(BgL_tmpz00_3403, BgL_auxz00_3405);
									}
								}
							}
							{
								obj_t BgL_l1281z00_3408;

								BgL_l1281z00_3408 = CDR(BgL_l1281z00_2753);
								BgL_l1281z00_2753 = BgL_l1281z00_3408;
								goto BgL_zc3z04anonymousza31655ze3z87_2752;
							}
						}
					else
						{	/* Globalize/new_body.scm 262 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2631));
		}

	}



/* &rem!-fail1314 */
	BgL_nodez00_bglt BGl_z62remz12zd2fail1314za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2634, obj_t BgL_nodez00_2635, obj_t BgL_ownerz00_2636,
		obj_t BgL_currentz00_2637)
	{
		{	/* Globalize/new_body.scm 249 */
			{
				BgL_nodez00_bglt BgL_auxz00_3412;

				{	/* Globalize/new_body.scm 251 */
					BgL_nodez00_bglt BgL_arg1646z00_2758;

					BgL_arg1646z00_2758 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2635)))->BgL_procz00);
					BgL_auxz00_3412 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1646z00_2758,
						BgL_ownerz00_2636, BgL_currentz00_2637);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2635)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3412), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3418;

				{	/* Globalize/new_body.scm 252 */
					BgL_nodez00_bglt BgL_arg1650z00_2759;

					BgL_arg1650z00_2759 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2635)))->BgL_msgz00);
					BgL_auxz00_3418 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1650z00_2759,
						BgL_ownerz00_2636, BgL_currentz00_2637);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2635)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3418), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3424;

				{	/* Globalize/new_body.scm 253 */
					BgL_nodez00_bglt BgL_arg1651z00_2760;

					BgL_arg1651z00_2760 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2635)))->BgL_objz00);
					BgL_auxz00_3424 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1651z00_2760,
						BgL_ownerz00_2636, BgL_currentz00_2637);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2635)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3424), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2635));
		}

	}



/* &rem!-conditional1312 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2conditional1312za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2638, obj_t BgL_nodez00_2639, obj_t BgL_ownerz00_2640,
		obj_t BgL_currentz00_2641)
	{
		{	/* Globalize/new_body.scm 239 */
			{
				BgL_nodez00_bglt BgL_auxz00_3432;

				{	/* Globalize/new_body.scm 241 */
					BgL_nodez00_bglt BgL_arg1629z00_2762;

					BgL_arg1629z00_2762 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2639)))->BgL_testz00);
					BgL_auxz00_3432 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1629z00_2762,
						BgL_ownerz00_2640, BgL_currentz00_2641);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2639)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3432), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3438;

				{	/* Globalize/new_body.scm 242 */
					BgL_nodez00_bglt BgL_arg1630z00_2763;

					BgL_arg1630z00_2763 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2639)))->BgL_truez00);
					BgL_auxz00_3438 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1630z00_2763,
						BgL_ownerz00_2640, BgL_currentz00_2641);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2639)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3438), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3444;

				{	/* Globalize/new_body.scm 243 */
					BgL_nodez00_bglt BgL_arg1642z00_2764;

					BgL_arg1642z00_2764 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2639)))->BgL_falsez00);
					BgL_auxz00_3444 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1642z00_2764,
						BgL_ownerz00_2640, BgL_currentz00_2641);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2639)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3444), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2639));
		}

	}



/* &rem!-setq1310 */
	BgL_nodez00_bglt BGl_z62remz12zd2setq1310za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2642, obj_t BgL_nodez00_2643, obj_t BgL_ownerz00_2644,
		obj_t BgL_currentz00_2645)
	{
		{	/* Globalize/new_body.scm 231 */
			{
				BgL_nodez00_bglt BgL_auxz00_3452;

				{	/* Globalize/new_body.scm 233 */
					BgL_nodez00_bglt BgL_arg1627z00_2766;

					BgL_arg1627z00_2766 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2643)))->BgL_valuez00);
					BgL_auxz00_3452 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1627z00_2766,
						BgL_ownerz00_2644, BgL_currentz00_2645);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2643)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3452), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2643));
		}

	}



/* &rem!-cast1308 */
	BgL_nodez00_bglt BGl_z62remz12zd2cast1308za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2646, obj_t BgL_nodez00_2647, obj_t BgL_ownerz00_2648,
		obj_t BgL_currentz00_2649)
	{
		{	/* Globalize/new_body.scm 223 */
			BGl_remz12z12zzglobaliza7e_newzd2bodyz75(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2647)))->BgL_argz00),
				BgL_ownerz00_2648, BgL_currentz00_2649);
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2647));
		}

	}



/* &rem!-extern1306 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2extern1306za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2650, obj_t BgL_nodez00_2651, obj_t BgL_ownerz00_2652,
		obj_t BgL_currentz00_2653)
	{
		{	/* Globalize/new_body.scm 215 */
			BGl_remza2z12zb0zzglobaliza7e_newzd2bodyz75(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2651)))->BgL_exprza2za2),
				BgL_ownerz00_2652, BgL_currentz00_2653);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2651));
		}

	}



/* &rem!-funcall1304 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2funcall1304za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2654, obj_t BgL_nodez00_2655, obj_t BgL_ownerz00_2656,
		obj_t BgL_currentz00_2657)
	{
		{	/* Globalize/new_body.scm 206 */
			{
				BgL_nodez00_bglt BgL_auxz00_3470;

				{	/* Globalize/new_body.scm 208 */
					BgL_nodez00_bglt BgL_arg1615z00_2770;

					BgL_arg1615z00_2770 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2655)))->BgL_funz00);
					BgL_auxz00_3470 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1615z00_2770,
						BgL_ownerz00_2656, BgL_currentz00_2657);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2655)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3470), BUNSPEC);
			}
			BGl_remza2z12zb0zzglobaliza7e_newzd2bodyz75(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2655)))->BgL_argsz00),
				BgL_ownerz00_2656, BgL_currentz00_2657);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2655));
		}

	}



/* &rem!-app-ly1302 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2appzd2ly1302z70zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2658, obj_t BgL_nodez00_2659, obj_t BgL_ownerz00_2660,
		obj_t BgL_currentz00_2661)
	{
		{	/* Globalize/new_body.scm 197 */
			{
				BgL_nodez00_bglt BgL_auxz00_3481;

				{	/* Globalize/new_body.scm 199 */
					BgL_nodez00_bglt BgL_arg1611z00_2772;

					BgL_arg1611z00_2772 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2659)))->BgL_funz00);
					BgL_auxz00_3481 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1611z00_2772,
						BgL_ownerz00_2660, BgL_currentz00_2661);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2659)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3481), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3487;

				{	/* Globalize/new_body.scm 200 */
					BgL_nodez00_bglt BgL_arg1613z00_2773;

					BgL_arg1613z00_2773 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2659)))->BgL_argz00);
					BgL_auxz00_3487 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1613z00_2773,
						BgL_ownerz00_2660, BgL_currentz00_2661);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2659)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3487), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2659));
		}

	}



/* &rem!-app1300 */
	BgL_nodez00_bglt BGl_z62remz12zd2app1300za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2662, obj_t BgL_nodez00_2663, obj_t BgL_ownerz00_2664,
		obj_t BgL_currentz00_2665)
	{
		{	/* Globalize/new_body.scm 189 */
			BGl_remza2z12zb0zzglobaliza7e_newzd2bodyz75(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2663)))->BgL_argsz00),
				BgL_ownerz00_2664, BgL_currentz00_2665);
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_2663));
		}

	}



/* &rem!-sync1298 */
	BgL_nodez00_bglt BGl_z62remz12zd2sync1298za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2666, obj_t BgL_nodez00_2667, obj_t BgL_ownerz00_2668,
		obj_t BgL_currentz00_2669)
	{
		{	/* Globalize/new_body.scm 179 */
			{
				BgL_nodez00_bglt BgL_auxz00_3500;

				{	/* Globalize/new_body.scm 181 */
					BgL_nodez00_bglt BgL_arg1602z00_2776;

					BgL_arg1602z00_2776 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2667)))->BgL_mutexz00);
					BgL_auxz00_3500 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1602z00_2776,
						BgL_ownerz00_2668, BgL_currentz00_2669);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2667)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3500), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3506;

				{	/* Globalize/new_body.scm 182 */
					BgL_nodez00_bglt BgL_arg1605z00_2777;

					BgL_arg1605z00_2777 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2667)))->BgL_prelockz00);
					BgL_auxz00_3506 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1605z00_2777,
						BgL_ownerz00_2668, BgL_currentz00_2669);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2667)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3506), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3512;

				{	/* Globalize/new_body.scm 183 */
					BgL_nodez00_bglt BgL_arg1606z00_2778;

					BgL_arg1606z00_2778 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2667)))->BgL_bodyz00);
					BgL_auxz00_3512 =
						BGl_remz12z12zzglobaliza7e_newzd2bodyz75(BgL_arg1606z00_2778,
						BgL_ownerz00_2668, BgL_currentz00_2669);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2667)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3512), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2667));
		}

	}



/* &rem!-sequence1296 */
	BgL_nodez00_bglt
		BGl_z62remz12zd2sequence1296za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2670, obj_t BgL_nodez00_2671, obj_t BgL_ownerz00_2672,
		obj_t BgL_currentz00_2673)
	{
		{	/* Globalize/new_body.scm 171 */
			BGl_remza2z12zb0zzglobaliza7e_newzd2bodyz75(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2671)))->BgL_nodesz00),
				BgL_ownerz00_2672, BgL_currentz00_2673);
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2671));
		}

	}



/* &rem!-var1294 */
	BgL_nodez00_bglt BGl_z62remz12zd2var1294za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2674, obj_t BgL_nodez00_2675, obj_t BgL_ownerz00_2676,
		obj_t BgL_currentz00_2677)
	{
		{	/* Globalize/new_body.scm 165 */
			return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_2675));
		}

	}



/* &rem!-kwote1292 */
	BgL_nodez00_bglt BGl_z62remz12zd2kwote1292za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2678, obj_t BgL_nodez00_2679, obj_t BgL_ownerz00_2680,
		obj_t BgL_currentz00_2681)
	{
		{	/* Globalize/new_body.scm 159 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_2679));
		}

	}



/* &rem!-atom1290 */
	BgL_nodez00_bglt BGl_z62remz12zd2atom1290za2zzglobaliza7e_newzd2bodyz75(obj_t
		BgL_envz00_2682, obj_t BgL_nodez00_2683, obj_t BgL_ownerz00_2684,
		obj_t BgL_currentz00_2685)
	{
		{	/* Globalize/new_body.scm 153 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_2683));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_newzd2bodyz75(void)
	{
		{	/* Globalize/new_body.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
			return
				BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(2706117L,
				BSTRING_TO_STRING(BGl_string1929z00zzglobaliza7e_newzd2bodyz75));
		}

	}

#ifdef __cplusplus
}
#endif
