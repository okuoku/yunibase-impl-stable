/*===========================================================================*/
/*   (Globalize/gloclo.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/gloclo.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_GLOBALIZE_GLOBALzd2CLOSUREzd2_TYPE_DEFINITIONS
#define BGL_BgL_GLOBALIZE_GLOBALzd2CLOSUREzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_svarzf2ginfozf2_bgl
	{
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
		bool_t BgL_celledzf3zf3;
		bool_t BgL_stackablez00;
	}                      *BgL_svarzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;

	typedef struct BgL_globalzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		obj_t BgL_globalzd2closurezd2;
	}                        *BgL_globalzf2ginfozf2_bglt;


#endif													// BGL_BgL_GLOBALIZE_GLOBALzd2CLOSUREzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_appz00_bglt
		BGl_makezd2nooptzd2bodyz00zzglobaliza7e_globalzd2closurez75(obj_t,
		BgL_globalz00_bglt, obj_t);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t
		BGl_makezd2nooptzd2globalzd2closurezd2zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt);
	static obj_t
		BGl_globaliza7edzd2typezd2idza7zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt);
	static obj_t
		BGl_requirezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75 =
		BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_globalzd2closurezd2zzglobaliza7e_globalzd2closurez75(BgL_globalz00_bglt,
		obj_t);
	extern obj_t BGl_zb2zd2arityz60zztools_argsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_globalzd2closurez75(void);
	extern obj_t BGl_makezd2nzd2protoz00zztools_argsz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_globalzd2closurez75(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_globalzd2closurez75(void);
	static obj_t BGl_za2foreignzd2closuresza2zd2zzglobaliza7e_globalzd2closurez75
		= BUNSPEC;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_foreignzd2closureszd2zzglobaliza7e_globalzd2closurez75(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	static obj_t
		BGl_makezd2optzf2keyzd2globalzd2closurez20zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_globalzd2closurez75(void);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	static obj_t
		BGl_glocloz00zzglobaliza7e_globalzd2closurez75(BgL_globalz00_bglt,
		BgL_localz00_bglt, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_z62makezd2globalzd2closurez62zzglobaliza7e_globalzd2closurez75(obj_t,
		obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t
		BGl_z62foreignzd2closureszb0zzglobaliza7e_globalzd2closurez75(obj_t);
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_IMPORT bool_t symbol_exists_p(char *);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_cfunz00zzast_varz00;
	extern obj_t BGl_nodez00zzast_nodez00;
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_makezd2globalzd2closurez00zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_globalzd2closurez75(void);
	static obj_t
		BGl_libraryzd2moduleszd2initz00zzglobaliza7e_globalzd2closurez75(void);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	extern obj_t
		BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(BgL_globalz00_bglt,
		obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zzglobaliza7e_globalzd2closurez75(void);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_globalzd2closurez75(void);
	extern obj_t BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7;
	static obj_t BGl_defaultzd2typezd2zzglobaliza7e_globalzd2closurez75(void);
	extern int BGl_funzd2optionalzd2arityz00zzast_varz00(BgL_funz00_bglt);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static BgL_globalz00_bglt
		BGl_z62globalzd2closurezb0zzglobaliza7e_globalzd2closurez75(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t
		BGl_fillzd2glocloz12zc0zzglobaliza7e_globalzd2closurez75(BgL_globalz00_bglt,
		BgL_globalz00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[9];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_foreignzd2closureszd2envz00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_za762foreignza7d2clo1832z00,
		BGl_z62foreignzd2closureszb0zzglobaliza7e_globalzd2closurez75, 0L, BUNSPEC,
		0);
	      DEFINE_STRING(BGl_string1821z00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_string1821za700za7za7g1833za7, "make-global-closure", 19);
	      DEFINE_STRING(BGl_string1822z00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_string1822za700za7za7g1834za7, "Unexpected value", 16);
	      DEFINE_STRING(BGl_string1823z00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_string1823za700za7za7g1835za7, "&", 1);
	      DEFINE_STRING(BGl_string1824z00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_string1824za700za7za7g1836za7, "global-closure", 14);
	      DEFINE_STRING(BGl_string1825z00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_string1825za700za7za7g1837za7, "Can't allocate global closure", 29);
	      DEFINE_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_string1826za700za7za7g1838za7, "globalize_global-closure", 24);
	      DEFINE_STRING(BGl_string1827z00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_string1827za700za7za7g1839za7,
		"never now sfun foreign & obj static env _ ", 42);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2globalzd2closurezd2envzd2zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_za762makeza7d2global1840z00,
		BGl_z62makezd2globalzd2closurez62zzglobaliza7e_globalzd2closurez75, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzd2closurezd2envz00zzglobaliza7e_globalzd2closurez75,
		BgL_bgl_za762globalza7d2clos1841z00,
		BGl_z62globalzd2closurezb0zzglobaliza7e_globalzd2closurez75, 0L, BUNSPEC,
		2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*)
			(&BGl_requirezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75));
		     ADD_ROOT((void
				*) (&BGl_za2foreignzd2closuresza2zd2zzglobaliza7e_globalzd2closurez75));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75(long
		BgL_checksumz00_2749, char *BgL_fromz00_2750)
	{
		{
			if (CBOOL
				(BGl_requirezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_globalzd2closurez75 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_globalzd2closurez75();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_globalzd2closurez75();
					BGl_cnstzd2initzd2zzglobaliza7e_globalzd2closurez75();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_globalzd2closurez75();
					return BGl_toplevelzd2initzd2zzglobaliza7e_globalzd2closurez75();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L,
				"globalize_global-closure");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"globalize_global-closure");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L,
				"globalize_global-closure");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_global-closure");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"globalize_global-closure");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"globalize_global-closure");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_global-closure");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_global-closure");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_global-closure");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 15 */
			{	/* Globalize/gloclo.scm 15 */
				obj_t BgL_cportz00_2738;

				{	/* Globalize/gloclo.scm 15 */
					obj_t BgL_stringz00_2745;

					BgL_stringz00_2745 =
						BGl_string1827z00zzglobaliza7e_globalzd2closurez75;
					{	/* Globalize/gloclo.scm 15 */
						obj_t BgL_startz00_2746;

						BgL_startz00_2746 = BINT(0L);
						{	/* Globalize/gloclo.scm 15 */
							obj_t BgL_endz00_2747;

							BgL_endz00_2747 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2745)));
							{	/* Globalize/gloclo.scm 15 */

								BgL_cportz00_2738 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2745, BgL_startz00_2746, BgL_endz00_2747);
				}}}}
				{
					long BgL_iz00_2739;

					BgL_iz00_2739 = 8L;
				BgL_loopz00_2740:
					if ((BgL_iz00_2739 == -1L))
						{	/* Globalize/gloclo.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/gloclo.scm 15 */
							{	/* Globalize/gloclo.scm 15 */
								obj_t BgL_arg1831z00_2741;

								{	/* Globalize/gloclo.scm 15 */

									{	/* Globalize/gloclo.scm 15 */
										obj_t BgL_locationz00_2743;

										BgL_locationz00_2743 = BBOOL(((bool_t) 0));
										{	/* Globalize/gloclo.scm 15 */

											BgL_arg1831z00_2741 =
												BGl_readz00zz__readerz00(BgL_cportz00_2738,
												BgL_locationz00_2743);
										}
									}
								}
								{	/* Globalize/gloclo.scm 15 */
									int BgL_tmpz00_2777;

									BgL_tmpz00_2777 = (int) (BgL_iz00_2739);
									CNST_TABLE_SET(BgL_tmpz00_2777, BgL_arg1831z00_2741);
							}}
							{	/* Globalize/gloclo.scm 15 */
								int BgL_auxz00_2744;

								BgL_auxz00_2744 = (int) ((BgL_iz00_2739 - 1L));
								{
									long BgL_iz00_2782;

									BgL_iz00_2782 = (long) (BgL_auxz00_2744);
									BgL_iz00_2739 = BgL_iz00_2782;
									goto BgL_loopz00_2740;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 15 */
			return (BGl_za2foreignzd2closuresza2zd2zzglobaliza7e_globalzd2closurez75 =
				BNIL, BUNSPEC);
		}

	}



/* global-closure */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_globalzd2closurezd2zzglobaliza7e_globalzd2closurez75(BgL_globalz00_bglt
		BgL_globalz00_3, obj_t BgL_locz00_4)
	{
		{	/* Globalize/gloclo.scm 41 */
			BGl_thezd2globalzd2closurez00zzglobaliza7e_freeza7(BgL_globalz00_3,
				BgL_locz00_4);
			return
				BGl_makezd2globalzd2closurez00zzglobaliza7e_globalzd2closurez75
				(BgL_globalz00_3);
		}

	}



/* &global-closure */
	BgL_globalz00_bglt
		BGl_z62globalzd2closurezb0zzglobaliza7e_globalzd2closurez75(obj_t
		BgL_envz00_2732, obj_t BgL_globalz00_2733, obj_t BgL_locz00_2734)
	{
		{	/* Globalize/gloclo.scm 41 */
			return
				BGl_globalzd2closurezd2zzglobaliza7e_globalzd2closurez75(
				((BgL_globalz00_bglt) BgL_globalz00_2733), BgL_locz00_2734);
		}

	}



/* make-global-closure */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_makezd2globalzd2closurez00zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt BgL_globalz00_5)
	{
		{	/* Globalize/gloclo.scm 48 */
			{	/* Globalize/gloclo.scm 49 */
				obj_t BgL_gloz00_1718;

				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_2789;

					{
						obj_t BgL_auxz00_2790;

						{	/* Globalize/gloclo.scm 49 */
							BgL_objectz00_bglt BgL_tmpz00_2791;

							BgL_tmpz00_2791 =
								((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_globalz00_5));
							BgL_auxz00_2790 = BGL_OBJECT_WIDENING(BgL_tmpz00_2791);
						}
						BgL_auxz00_2789 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_2790);
					}
					BgL_gloz00_1718 =
						(((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2789))->
						BgL_globalzd2closurezd2);
				}
				{	/* Globalize/gloclo.scm 50 */
					bool_t BgL_test1844z00_2797;

					{	/* Globalize/gloclo.scm 50 */
						obj_t BgL_classz00_2083;

						BgL_classz00_2083 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_gloz00_1718))
							{	/* Globalize/gloclo.scm 50 */
								BgL_objectz00_bglt BgL_arg1807z00_2085;

								BgL_arg1807z00_2085 = (BgL_objectz00_bglt) (BgL_gloz00_1718);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Globalize/gloclo.scm 50 */
										long BgL_idxz00_2091;

										BgL_idxz00_2091 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2085);
										BgL_test1844z00_2797 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2091 + 2L)) == BgL_classz00_2083);
									}
								else
									{	/* Globalize/gloclo.scm 50 */
										bool_t BgL_res1806z00_2116;

										{	/* Globalize/gloclo.scm 50 */
											obj_t BgL_oclassz00_2099;

											{	/* Globalize/gloclo.scm 50 */
												obj_t BgL_arg1815z00_2107;
												long BgL_arg1816z00_2108;

												BgL_arg1815z00_2107 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Globalize/gloclo.scm 50 */
													long BgL_arg1817z00_2109;

													BgL_arg1817z00_2109 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2085);
													BgL_arg1816z00_2108 =
														(BgL_arg1817z00_2109 - OBJECT_TYPE);
												}
												BgL_oclassz00_2099 =
													VECTOR_REF(BgL_arg1815z00_2107, BgL_arg1816z00_2108);
											}
											{	/* Globalize/gloclo.scm 50 */
												bool_t BgL__ortest_1115z00_2100;

												BgL__ortest_1115z00_2100 =
													(BgL_classz00_2083 == BgL_oclassz00_2099);
												if (BgL__ortest_1115z00_2100)
													{	/* Globalize/gloclo.scm 50 */
														BgL_res1806z00_2116 = BgL__ortest_1115z00_2100;
													}
												else
													{	/* Globalize/gloclo.scm 50 */
														long BgL_odepthz00_2101;

														{	/* Globalize/gloclo.scm 50 */
															obj_t BgL_arg1804z00_2102;

															BgL_arg1804z00_2102 = (BgL_oclassz00_2099);
															BgL_odepthz00_2101 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2102);
														}
														if ((2L < BgL_odepthz00_2101))
															{	/* Globalize/gloclo.scm 50 */
																obj_t BgL_arg1802z00_2104;

																{	/* Globalize/gloclo.scm 50 */
																	obj_t BgL_arg1803z00_2105;

																	BgL_arg1803z00_2105 = (BgL_oclassz00_2099);
																	BgL_arg1802z00_2104 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2105,
																		2L);
																}
																BgL_res1806z00_2116 =
																	(BgL_arg1802z00_2104 == BgL_classz00_2083);
															}
														else
															{	/* Globalize/gloclo.scm 50 */
																BgL_res1806z00_2116 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1844z00_2797 = BgL_res1806z00_2116;
									}
							}
						else
							{	/* Globalize/gloclo.scm 50 */
								BgL_test1844z00_2797 = ((bool_t) 0);
							}
					}
					if (BgL_test1844z00_2797)
						{	/* Globalize/gloclo.scm 50 */
							return ((BgL_globalz00_bglt) BgL_gloz00_1718);
						}
					else
						{	/* Globalize/gloclo.scm 52 */
							BgL_valuez00_bglt BgL_oldzd2funzd2_1720;

							BgL_oldzd2funzd2_1720 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_5)))->BgL_valuez00);
							{	/* Globalize/gloclo.scm 53 */
								bool_t BgL_test1849z00_2823;

								{	/* Globalize/gloclo.scm 53 */
									bool_t BgL_test1850z00_2824;

									{	/* Globalize/gloclo.scm 53 */
										obj_t BgL_classz00_2118;

										BgL_classz00_2118 = BGl_sfunz00zzast_varz00;
										{	/* Globalize/gloclo.scm 53 */
											BgL_objectz00_bglt BgL_arg1807z00_2120;

											{	/* Globalize/gloclo.scm 53 */
												obj_t BgL_tmpz00_2825;

												BgL_tmpz00_2825 =
													((obj_t)
													((BgL_objectz00_bglt) BgL_oldzd2funzd2_1720));
												BgL_arg1807z00_2120 =
													(BgL_objectz00_bglt) (BgL_tmpz00_2825);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Globalize/gloclo.scm 53 */
													long BgL_idxz00_2126;

													BgL_idxz00_2126 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2120);
													BgL_test1850z00_2824 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2126 + 3L)) == BgL_classz00_2118);
												}
											else
												{	/* Globalize/gloclo.scm 53 */
													bool_t BgL_res1807z00_2151;

													{	/* Globalize/gloclo.scm 53 */
														obj_t BgL_oclassz00_2134;

														{	/* Globalize/gloclo.scm 53 */
															obj_t BgL_arg1815z00_2142;
															long BgL_arg1816z00_2143;

															BgL_arg1815z00_2142 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Globalize/gloclo.scm 53 */
																long BgL_arg1817z00_2144;

																BgL_arg1817z00_2144 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2120);
																BgL_arg1816z00_2143 =
																	(BgL_arg1817z00_2144 - OBJECT_TYPE);
															}
															BgL_oclassz00_2134 =
																VECTOR_REF(BgL_arg1815z00_2142,
																BgL_arg1816z00_2143);
														}
														{	/* Globalize/gloclo.scm 53 */
															bool_t BgL__ortest_1115z00_2135;

															BgL__ortest_1115z00_2135 =
																(BgL_classz00_2118 == BgL_oclassz00_2134);
															if (BgL__ortest_1115z00_2135)
																{	/* Globalize/gloclo.scm 53 */
																	BgL_res1807z00_2151 =
																		BgL__ortest_1115z00_2135;
																}
															else
																{	/* Globalize/gloclo.scm 53 */
																	long BgL_odepthz00_2136;

																	{	/* Globalize/gloclo.scm 53 */
																		obj_t BgL_arg1804z00_2137;

																		BgL_arg1804z00_2137 = (BgL_oclassz00_2134);
																		BgL_odepthz00_2136 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2137);
																	}
																	if ((3L < BgL_odepthz00_2136))
																		{	/* Globalize/gloclo.scm 53 */
																			obj_t BgL_arg1802z00_2139;

																			{	/* Globalize/gloclo.scm 53 */
																				obj_t BgL_arg1803z00_2140;

																				BgL_arg1803z00_2140 =
																					(BgL_oclassz00_2134);
																				BgL_arg1802z00_2139 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2140, 3L);
																			}
																			BgL_res1807z00_2151 =
																				(BgL_arg1802z00_2139 ==
																				BgL_classz00_2118);
																		}
																	else
																		{	/* Globalize/gloclo.scm 53 */
																			BgL_res1807z00_2151 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1850z00_2824 = BgL_res1807z00_2151;
												}
										}
									}
									if (BgL_test1850z00_2824)
										{	/* Globalize/gloclo.scm 54 */
											bool_t BgL_test1854z00_2848;

											{	/* Globalize/gloclo.scm 54 */
												bool_t BgL_test1855z00_2849;

												{	/* Globalize/gloclo.scm 54 */
													obj_t BgL_tmpz00_2850;

													BgL_tmpz00_2850 =
														(((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1720)))->
														BgL_optionalsz00);
													BgL_test1855z00_2849 = PAIRP(BgL_tmpz00_2850);
												}
												if (BgL_test1855z00_2849)
													{	/* Globalize/gloclo.scm 54 */
														BgL_test1854z00_2848 = ((bool_t) 1);
													}
												else
													{	/* Globalize/gloclo.scm 55 */
														obj_t BgL_tmpz00_2854;

														BgL_tmpz00_2854 =
															(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1720)))->
															BgL_keysz00);
														BgL_test1854z00_2848 = PAIRP(BgL_tmpz00_2854);
													}
											}
											if (BgL_test1854z00_2848)
												{	/* Globalize/gloclo.scm 56 */
													obj_t BgL_arg1320z00_1734;

													BgL_arg1320z00_1734 =
														(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_5))->
														BgL_modulez00);
													BgL_test1849z00_2823 =
														(BgL_arg1320z00_1734 ==
														BGl_za2moduleza2z00zzmodule_modulez00);
												}
											else
												{	/* Globalize/gloclo.scm 54 */
													BgL_test1849z00_2823 = ((bool_t) 0);
												}
										}
									else
										{	/* Globalize/gloclo.scm 53 */
											BgL_test1849z00_2823 = ((bool_t) 0);
										}
								}
								if (BgL_test1849z00_2823)
									{	/* Globalize/gloclo.scm 53 */
										return
											((BgL_globalz00_bglt)
											BGl_makezd2optzf2keyzd2globalzd2closurez20zzglobaliza7e_globalzd2closurez75
											(BgL_globalz00_5));
									}
								else
									{	/* Globalize/gloclo.scm 53 */
										return
											((BgL_globalz00_bglt)
											BGl_makezd2nooptzd2globalzd2closurezd2zzglobaliza7e_globalzd2closurez75
											(BgL_globalz00_5));
									}
							}
						}
				}
			}
		}

	}



/* &make-global-closure */
	BgL_globalz00_bglt
		BGl_z62makezd2globalzd2closurez62zzglobaliza7e_globalzd2closurez75(obj_t
		BgL_envz00_2735, obj_t BgL_globalz00_2736)
	{
		{	/* Globalize/gloclo.scm 48 */
			return
				BGl_makezd2globalzd2closurez00zzglobaliza7e_globalzd2closurez75(
				((BgL_globalz00_bglt) BgL_globalz00_2736));
		}

	}



/* make-opt/key-global-closure */
	obj_t
		BGl_makezd2optzf2keyzd2globalzd2closurez20zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt BgL_globalz00_6)
	{
		{	/* Globalize/gloclo.scm 63 */
			{	/* Globalize/gloclo.scm 64 */
				obj_t BgL_glocloz00_1738;

				{	/* Globalize/gloclo.scm 64 */
					obj_t BgL_arg1325z00_1740;
					obj_t BgL_arg1326z00_1741;

					{	/* Globalize/gloclo.scm 64 */
						obj_t BgL_arg1328z00_1743;

						{	/* Globalize/gloclo.scm 64 */
							obj_t BgL_arg1329z00_1744;
							obj_t BgL_arg1331z00_1745;

							{	/* Globalize/gloclo.scm 64 */
								obj_t BgL_symbolz00_2155;

								BgL_symbolz00_2155 = CNST_TABLE_REF(0);
								{	/* Globalize/gloclo.scm 64 */
									obj_t BgL_arg1455z00_2156;

									BgL_arg1455z00_2156 = SYMBOL_TO_STRING(BgL_symbolz00_2155);
									BgL_arg1329z00_1744 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2156);
								}
							}
							{	/* Globalize/gloclo.scm 64 */
								obj_t BgL_arg1332z00_1746;

								BgL_arg1332z00_1746 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_6)))->BgL_idz00);
								{	/* Globalize/gloclo.scm 64 */
									obj_t BgL_arg1455z00_2159;

									BgL_arg1455z00_2159 = SYMBOL_TO_STRING(BgL_arg1332z00_1746);
									BgL_arg1331z00_1745 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2159);
								}
							}
							BgL_arg1328z00_1743 =
								string_append(BgL_arg1329z00_1744, BgL_arg1331z00_1745);
						}
						BgL_arg1325z00_1740 = bstring_to_symbol(BgL_arg1328z00_1743);
					}
					BgL_arg1326z00_1741 =
						(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_6))->BgL_modulez00);
					{	/* Globalize/gloclo.scm 64 */
						obj_t BgL_list1327z00_1742;

						BgL_list1327z00_1742 = MAKE_YOUNG_PAIR(BgL_arg1326z00_1741, BNIL);
						BgL_glocloz00_1738 =
							BGl_findzd2globalzd2zzast_envz00(BgL_arg1325z00_1740,
							BgL_list1327z00_1742);
					}
				}
				BGl_fillzd2glocloz12zc0zzglobaliza7e_globalzd2closurez75
					(BgL_globalz00_6, ((BgL_globalz00_bglt) BgL_glocloz00_1738));
				{	/* Globalize/gloclo.scm 67 */
					BgL_valuez00_bglt BgL_arg1323z00_1739;

					BgL_arg1323z00_1739 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_glocloz00_1738))))->BgL_valuez00);
					((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_arg1323z00_1739)))->
							BgL_thezd2closurezd2globalz00) =
						((obj_t) ((obj_t) BgL_globalz00_6)), BUNSPEC);
				}
				return BgL_glocloz00_1738;
			}
		}

	}



/* default-type */
	obj_t BGl_defaultzd2typezd2zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 73 */
			{	/* Globalize/gloclo.scm 74 */
				bool_t BgL_test1856z00_2886;

				if (CBOOL
					(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
					{	/* Globalize/gloclo.scm 74 */
						BgL_test1856z00_2886 =
							((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L);
					}
				else
					{	/* Globalize/gloclo.scm 74 */
						BgL_test1856z00_2886 = ((bool_t) 0);
					}
				if (BgL_test1856z00_2886)
					{	/* Globalize/gloclo.scm 74 */
						return BGl_za2_za2z00zztype_cachez00;
					}
				else
					{	/* Globalize/gloclo.scm 74 */
						return BGl_za2objza2z00zztype_cachez00;
					}
			}
		}

	}



/* make-noopt-global-closure */
	obj_t
		BGl_makezd2nooptzd2globalzd2closurezd2zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt BgL_globalz00_7)
	{
		{	/* Globalize/gloclo.scm 79 */
			{	/* Globalize/gloclo.scm 80 */
				BgL_valuez00_bglt BgL_oldzd2funzd2_1748;

				BgL_oldzd2funzd2_1748 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_globalz00_7)))->BgL_valuez00);
				{	/* Globalize/gloclo.scm 80 */
					BgL_localz00_bglt BgL_envz00_1749;

					{	/* Globalize/gloclo.scm 81 */
						BgL_localz00_bglt BgL_varz00_1808;

						BgL_varz00_1808 =
							BGl_makezd2localzd2svarz00zzast_localz00(CNST_TABLE_REF(1),
							((BgL_typez00_bglt) BGl_za2procedureza2z00zztype_cachez00));
						{	/* Globalize/gloclo.scm 82 */
							BgL_localzf2ginfozf2_bglt BgL_wide1114z00_1811;

							BgL_wide1114z00_1811 =
								((BgL_localzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_localzf2ginfozf2_bgl))));
							{	/* Globalize/gloclo.scm 82 */
								obj_t BgL_auxz00_2900;
								BgL_objectz00_bglt BgL_tmpz00_2897;

								BgL_auxz00_2900 = ((obj_t) BgL_wide1114z00_1811);
								BgL_tmpz00_2897 =
									((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_varz00_1808));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2897, BgL_auxz00_2900);
							}
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_varz00_1808));
							{	/* Globalize/gloclo.scm 82 */
								long BgL_arg1408z00_1812;

								BgL_arg1408z00_1812 =
									BGL_CLASS_NUM(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_varz00_1808)),
									BgL_arg1408z00_1812);
							}
							((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_varz00_1808));
						}
						{
							BgL_localzf2ginfozf2_bglt BgL_auxz00_2911;

							{
								obj_t BgL_auxz00_2912;

								{	/* Globalize/gloclo.scm 82 */
									BgL_objectz00_bglt BgL_tmpz00_2913;

									BgL_tmpz00_2913 =
										((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_varz00_1808));
									BgL_auxz00_2912 = BGL_OBJECT_WIDENING(BgL_tmpz00_2913);
								}
								BgL_auxz00_2911 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2912);
							}
							((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2911))->
									BgL_escapezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
						}
						{
							BgL_localzf2ginfozf2_bglt BgL_auxz00_2919;

							{
								obj_t BgL_auxz00_2920;

								{	/* Globalize/gloclo.scm 82 */
									BgL_objectz00_bglt BgL_tmpz00_2921;

									BgL_tmpz00_2921 =
										((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_varz00_1808));
									BgL_auxz00_2920 = BGL_OBJECT_WIDENING(BgL_tmpz00_2921);
								}
								BgL_auxz00_2919 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_2920);
							}
							((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_2919))->
									BgL_globaliza7edzf3z54) = ((bool_t) ((bool_t) 0)), BUNSPEC);
						}
						((BgL_localz00_bglt) BgL_varz00_1808);
						BgL_envz00_1749 = BgL_varz00_1808;
					}
					{	/* Globalize/gloclo.scm 81 */
						obj_t BgL_newzd2argszd2_1750;

						{	/* Globalize/gloclo.scm 84 */
							obj_t BgL_l1276z00_1776;

							{	/* Globalize/gloclo.scm 95 */
								bool_t BgL_test1858z00_2928;

								{	/* Globalize/gloclo.scm 95 */
									obj_t BgL_classz00_2172;

									BgL_classz00_2172 = BGl_sfunz00zzast_varz00;
									{	/* Globalize/gloclo.scm 95 */
										BgL_objectz00_bglt BgL_arg1807z00_2174;

										{	/* Globalize/gloclo.scm 95 */
											obj_t BgL_tmpz00_2929;

											BgL_tmpz00_2929 =
												((obj_t) ((BgL_objectz00_bglt) BgL_oldzd2funzd2_1748));
											BgL_arg1807z00_2174 =
												(BgL_objectz00_bglt) (BgL_tmpz00_2929);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Globalize/gloclo.scm 95 */
												long BgL_idxz00_2180;

												BgL_idxz00_2180 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2174);
												BgL_test1858z00_2928 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2180 + 3L)) == BgL_classz00_2172);
											}
										else
											{	/* Globalize/gloclo.scm 95 */
												bool_t BgL_res1808z00_2205;

												{	/* Globalize/gloclo.scm 95 */
													obj_t BgL_oclassz00_2188;

													{	/* Globalize/gloclo.scm 95 */
														obj_t BgL_arg1815z00_2196;
														long BgL_arg1816z00_2197;

														BgL_arg1815z00_2196 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Globalize/gloclo.scm 95 */
															long BgL_arg1817z00_2198;

															BgL_arg1817z00_2198 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2174);
															BgL_arg1816z00_2197 =
																(BgL_arg1817z00_2198 - OBJECT_TYPE);
														}
														BgL_oclassz00_2188 =
															VECTOR_REF(BgL_arg1815z00_2196,
															BgL_arg1816z00_2197);
													}
													{	/* Globalize/gloclo.scm 95 */
														bool_t BgL__ortest_1115z00_2189;

														BgL__ortest_1115z00_2189 =
															(BgL_classz00_2172 == BgL_oclassz00_2188);
														if (BgL__ortest_1115z00_2189)
															{	/* Globalize/gloclo.scm 95 */
																BgL_res1808z00_2205 = BgL__ortest_1115z00_2189;
															}
														else
															{	/* Globalize/gloclo.scm 95 */
																long BgL_odepthz00_2190;

																{	/* Globalize/gloclo.scm 95 */
																	obj_t BgL_arg1804z00_2191;

																	BgL_arg1804z00_2191 = (BgL_oclassz00_2188);
																	BgL_odepthz00_2190 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2191);
																}
																if ((3L < BgL_odepthz00_2190))
																	{	/* Globalize/gloclo.scm 95 */
																		obj_t BgL_arg1802z00_2193;

																		{	/* Globalize/gloclo.scm 95 */
																			obj_t BgL_arg1803z00_2194;

																			BgL_arg1803z00_2194 =
																				(BgL_oclassz00_2188);
																			BgL_arg1802z00_2193 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2194, 3L);
																		}
																		BgL_res1808z00_2205 =
																			(BgL_arg1802z00_2193 ==
																			BgL_classz00_2172);
																	}
																else
																	{	/* Globalize/gloclo.scm 95 */
																		BgL_res1808z00_2205 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1858z00_2928 = BgL_res1808z00_2205;
											}
									}
								}
								if (BgL_test1858z00_2928)
									{	/* Globalize/gloclo.scm 95 */
										BgL_l1276z00_1776 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1748)))->
											BgL_argsz00);
									}
								else
									{	/* Globalize/gloclo.scm 97 */
										bool_t BgL_test1862z00_2954;

										{	/* Globalize/gloclo.scm 97 */
											obj_t BgL_classz00_2207;

											BgL_classz00_2207 = BGl_cfunz00zzast_varz00;
											{	/* Globalize/gloclo.scm 97 */
												BgL_objectz00_bglt BgL_arg1807z00_2209;

												{	/* Globalize/gloclo.scm 97 */
													obj_t BgL_tmpz00_2955;

													BgL_tmpz00_2955 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_oldzd2funzd2_1748));
													BgL_arg1807z00_2209 =
														(BgL_objectz00_bglt) (BgL_tmpz00_2955);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Globalize/gloclo.scm 97 */
														long BgL_idxz00_2215;

														BgL_idxz00_2215 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2209);
														BgL_test1862z00_2954 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2215 + 3L)) == BgL_classz00_2207);
													}
												else
													{	/* Globalize/gloclo.scm 97 */
														bool_t BgL_res1809z00_2240;

														{	/* Globalize/gloclo.scm 97 */
															obj_t BgL_oclassz00_2223;

															{	/* Globalize/gloclo.scm 97 */
																obj_t BgL_arg1815z00_2231;
																long BgL_arg1816z00_2232;

																BgL_arg1815z00_2231 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Globalize/gloclo.scm 97 */
																	long BgL_arg1817z00_2233;

																	BgL_arg1817z00_2233 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2209);
																	BgL_arg1816z00_2232 =
																		(BgL_arg1817z00_2233 - OBJECT_TYPE);
																}
																BgL_oclassz00_2223 =
																	VECTOR_REF(BgL_arg1815z00_2231,
																	BgL_arg1816z00_2232);
															}
															{	/* Globalize/gloclo.scm 97 */
																bool_t BgL__ortest_1115z00_2224;

																BgL__ortest_1115z00_2224 =
																	(BgL_classz00_2207 == BgL_oclassz00_2223);
																if (BgL__ortest_1115z00_2224)
																	{	/* Globalize/gloclo.scm 97 */
																		BgL_res1809z00_2240 =
																			BgL__ortest_1115z00_2224;
																	}
																else
																	{	/* Globalize/gloclo.scm 97 */
																		long BgL_odepthz00_2225;

																		{	/* Globalize/gloclo.scm 97 */
																			obj_t BgL_arg1804z00_2226;

																			BgL_arg1804z00_2226 =
																				(BgL_oclassz00_2223);
																			BgL_odepthz00_2225 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2226);
																		}
																		if ((3L < BgL_odepthz00_2225))
																			{	/* Globalize/gloclo.scm 97 */
																				obj_t BgL_arg1802z00_2228;

																				{	/* Globalize/gloclo.scm 97 */
																					obj_t BgL_arg1803z00_2229;

																					BgL_arg1803z00_2229 =
																						(BgL_oclassz00_2223);
																					BgL_arg1802z00_2228 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2229, 3L);
																				}
																				BgL_res1809z00_2240 =
																					(BgL_arg1802z00_2228 ==
																					BgL_classz00_2207);
																			}
																		else
																			{	/* Globalize/gloclo.scm 97 */
																				BgL_res1809z00_2240 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1862z00_2954 = BgL_res1809z00_2240;
													}
											}
										}
										if (BgL_test1862z00_2954)
											{	/* Globalize/gloclo.scm 97 */
												BgL_l1276z00_1776 =
													(((BgL_cfunz00_bglt) COBJECT(
															((BgL_cfunz00_bglt) BgL_oldzd2funzd2_1748)))->
													BgL_argszd2typezd2);
											}
										else
											{	/* Globalize/gloclo.scm 97 */
												BgL_l1276z00_1776 =
													BGl_internalzd2errorzd2zztools_errorz00
													(BGl_string1821z00zzglobaliza7e_globalzd2closurez75,
													BGl_string1822z00zzglobaliza7e_globalzd2closurez75,
													((obj_t) BgL_oldzd2funzd2_1748));
											}
									}
							}
							if (NULLP(BgL_l1276z00_1776))
								{	/* Globalize/gloclo.scm 84 */
									BgL_newzd2argszd2_1750 = BNIL;
								}
							else
								{	/* Globalize/gloclo.scm 84 */
									obj_t BgL_head1278z00_1778;

									BgL_head1278z00_1778 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1276z00_1780;
										obj_t BgL_tail1279z00_1781;

										BgL_l1276z00_1780 = BgL_l1276z00_1776;
										BgL_tail1279z00_1781 = BgL_head1278z00_1778;
									BgL_zc3z04anonymousza31363ze3z87_1782:
										if (NULLP(BgL_l1276z00_1780))
											{	/* Globalize/gloclo.scm 84 */
												BgL_newzd2argszd2_1750 = CDR(BgL_head1278z00_1778);
											}
										else
											{	/* Globalize/gloclo.scm 84 */
												obj_t BgL_newtail1280z00_1784;

												{	/* Globalize/gloclo.scm 84 */
													BgL_localz00_bglt BgL_arg1370z00_1786;

													{	/* Globalize/gloclo.scm 84 */
														obj_t BgL_oldz00_1787;

														BgL_oldz00_1787 = CAR(((obj_t) BgL_l1276z00_1780));
														{	/* Globalize/gloclo.scm 85 */
															BgL_localz00_bglt BgL_newz00_1788;

															{	/* Globalize/gloclo.scm 86 */
																obj_t BgL_arg1378z00_1801;
																obj_t BgL_arg1379z00_1802;

																{	/* Globalize/gloclo.scm 86 */
																	bool_t BgL_test1868z00_2990;

																	{	/* Globalize/gloclo.scm 86 */
																		obj_t BgL_classz00_2244;

																		BgL_classz00_2244 =
																			BGl_localz00zzast_varz00;
																		if (BGL_OBJECTP(BgL_oldz00_1787))
																			{	/* Globalize/gloclo.scm 86 */
																				BgL_objectz00_bglt BgL_arg1807z00_2246;

																				BgL_arg1807z00_2246 =
																					(BgL_objectz00_bglt)
																					(BgL_oldz00_1787);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Globalize/gloclo.scm 86 */
																						long BgL_idxz00_2252;

																						BgL_idxz00_2252 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_2246);
																						BgL_test1868z00_2990 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_2252 + 2L)) ==
																							BgL_classz00_2244);
																					}
																				else
																					{	/* Globalize/gloclo.scm 86 */
																						bool_t BgL_res1810z00_2277;

																						{	/* Globalize/gloclo.scm 86 */
																							obj_t BgL_oclassz00_2260;

																							{	/* Globalize/gloclo.scm 86 */
																								obj_t BgL_arg1815z00_2268;
																								long BgL_arg1816z00_2269;

																								BgL_arg1815z00_2268 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Globalize/gloclo.scm 86 */
																									long BgL_arg1817z00_2270;

																									BgL_arg1817z00_2270 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_2246);
																									BgL_arg1816z00_2269 =
																										(BgL_arg1817z00_2270 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_2260 =
																									VECTOR_REF
																									(BgL_arg1815z00_2268,
																									BgL_arg1816z00_2269);
																							}
																							{	/* Globalize/gloclo.scm 86 */
																								bool_t BgL__ortest_1115z00_2261;

																								BgL__ortest_1115z00_2261 =
																									(BgL_classz00_2244 ==
																									BgL_oclassz00_2260);
																								if (BgL__ortest_1115z00_2261)
																									{	/* Globalize/gloclo.scm 86 */
																										BgL_res1810z00_2277 =
																											BgL__ortest_1115z00_2261;
																									}
																								else
																									{	/* Globalize/gloclo.scm 86 */
																										long BgL_odepthz00_2262;

																										{	/* Globalize/gloclo.scm 86 */
																											obj_t BgL_arg1804z00_2263;

																											BgL_arg1804z00_2263 =
																												(BgL_oclassz00_2260);
																											BgL_odepthz00_2262 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_2263);
																										}
																										if (
																											(2L < BgL_odepthz00_2262))
																											{	/* Globalize/gloclo.scm 86 */
																												obj_t
																													BgL_arg1802z00_2265;
																												{	/* Globalize/gloclo.scm 86 */
																													obj_t
																														BgL_arg1803z00_2266;
																													BgL_arg1803z00_2266 =
																														(BgL_oclassz00_2260);
																													BgL_arg1802z00_2265 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_2266,
																														2L);
																												}
																												BgL_res1810z00_2277 =
																													(BgL_arg1802z00_2265
																													== BgL_classz00_2244);
																											}
																										else
																											{	/* Globalize/gloclo.scm 86 */
																												BgL_res1810z00_2277 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test1868z00_2990 =
																							BgL_res1810z00_2277;
																					}
																			}
																		else
																			{	/* Globalize/gloclo.scm 86 */
																				BgL_test1868z00_2990 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test1868z00_2990)
																		{	/* Globalize/gloclo.scm 86 */
																			BgL_arg1378z00_1801 =
																				(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_localz00_bglt)
																								BgL_oldz00_1787))))->BgL_idz00);
																		}
																	else
																		{	/* Globalize/gloclo.scm 88 */

																			{	/* Globalize/gloclo.scm 88 */

																				BgL_arg1378z00_1801 =
																					BGl_gensymz00zz__r4_symbols_6_4z00
																					(BFALSE);
																			}
																		}
																}
																BgL_arg1379z00_1802 =
																	BGl_defaultzd2typezd2zzglobaliza7e_globalzd2closurez75
																	();
																BgL_newz00_1788 =
																	BGl_makezd2localzd2svarz00zzast_localz00
																	(BgL_arg1378z00_1801,
																	((BgL_typez00_bglt) BgL_arg1379z00_1802));
															}
															{	/* Globalize/gloclo.scm 90 */
																bool_t BgL_test1873z00_3020;

																{	/* Globalize/gloclo.scm 90 */
																	obj_t BgL_classz00_2279;

																	BgL_classz00_2279 = BGl_localz00zzast_varz00;
																	if (BGL_OBJECTP(BgL_oldz00_1787))
																		{	/* Globalize/gloclo.scm 90 */
																			BgL_objectz00_bglt BgL_arg1807z00_2281;

																			BgL_arg1807z00_2281 =
																				(BgL_objectz00_bglt) (BgL_oldz00_1787);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Globalize/gloclo.scm 90 */
																					long BgL_idxz00_2287;

																					BgL_idxz00_2287 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2281);
																					BgL_test1873z00_3020 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2287 + 2L)) ==
																						BgL_classz00_2279);
																				}
																			else
																				{	/* Globalize/gloclo.scm 90 */
																					bool_t BgL_res1811z00_2312;

																					{	/* Globalize/gloclo.scm 90 */
																						obj_t BgL_oclassz00_2295;

																						{	/* Globalize/gloclo.scm 90 */
																							obj_t BgL_arg1815z00_2303;
																							long BgL_arg1816z00_2304;

																							BgL_arg1815z00_2303 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Globalize/gloclo.scm 90 */
																								long BgL_arg1817z00_2305;

																								BgL_arg1817z00_2305 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2281);
																								BgL_arg1816z00_2304 =
																									(BgL_arg1817z00_2305 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2295 =
																								VECTOR_REF(BgL_arg1815z00_2303,
																								BgL_arg1816z00_2304);
																						}
																						{	/* Globalize/gloclo.scm 90 */
																							bool_t BgL__ortest_1115z00_2296;

																							BgL__ortest_1115z00_2296 =
																								(BgL_classz00_2279 ==
																								BgL_oclassz00_2295);
																							if (BgL__ortest_1115z00_2296)
																								{	/* Globalize/gloclo.scm 90 */
																									BgL_res1811z00_2312 =
																										BgL__ortest_1115z00_2296;
																								}
																							else
																								{	/* Globalize/gloclo.scm 90 */
																									long BgL_odepthz00_2297;

																									{	/* Globalize/gloclo.scm 90 */
																										obj_t BgL_arg1804z00_2298;

																										BgL_arg1804z00_2298 =
																											(BgL_oclassz00_2295);
																										BgL_odepthz00_2297 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2298);
																									}
																									if ((2L < BgL_odepthz00_2297))
																										{	/* Globalize/gloclo.scm 90 */
																											obj_t BgL_arg1802z00_2300;

																											{	/* Globalize/gloclo.scm 90 */
																												obj_t
																													BgL_arg1803z00_2301;
																												BgL_arg1803z00_2301 =
																													(BgL_oclassz00_2295);
																												BgL_arg1802z00_2300 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2301,
																													2L);
																											}
																											BgL_res1811z00_2312 =
																												(BgL_arg1802z00_2300 ==
																												BgL_classz00_2279);
																										}
																									else
																										{	/* Globalize/gloclo.scm 90 */
																											BgL_res1811z00_2312 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1873z00_3020 =
																						BgL_res1811z00_2312;
																				}
																		}
																	else
																		{	/* Globalize/gloclo.scm 90 */
																			BgL_test1873z00_3020 = ((bool_t) 0);
																		}
																}
																if (BgL_test1873z00_3020)
																	{	/* Globalize/gloclo.scm 91 */
																		bool_t BgL_arg1375z00_1790;

																		BgL_arg1375z00_1790 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							BgL_oldz00_1787))))->
																			BgL_userzf3zf3);
																		((((BgL_variablez00_bglt)
																					COBJECT(((BgL_variablez00_bglt)
																							BgL_newz00_1788)))->
																				BgL_userzf3zf3) =
																			((bool_t) BgL_arg1375z00_1790), BUNSPEC);
																	}
																else
																	{	/* Globalize/gloclo.scm 90 */
																		BFALSE;
																	}
															}
															{	/* Globalize/gloclo.scm 92 */
																BgL_svarz00_bglt BgL_tmp1116z00_1791;

																BgL_tmp1116z00_1791 =
																	((BgL_svarz00_bglt)
																	(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_newz00_1788)))->BgL_valuez00));
																{	/* Globalize/gloclo.scm 92 */
																	BgL_svarzf2ginfozf2_bglt BgL_wide1118z00_1793;

																	BgL_wide1118z00_1793 =
																		((BgL_svarzf2ginfozf2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_svarzf2ginfozf2_bgl))));
																	{	/* Globalize/gloclo.scm 92 */
																		obj_t BgL_auxz00_3055;
																		BgL_objectz00_bglt BgL_tmpz00_3052;

																		BgL_auxz00_3055 =
																			((obj_t) BgL_wide1118z00_1793);
																		BgL_tmpz00_3052 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1116z00_1791));
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3052,
																			BgL_auxz00_3055);
																	}
																	((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1116z00_1791));
																	{	/* Globalize/gloclo.scm 92 */
																		long BgL_arg1376z00_1794;

																		BgL_arg1376z00_1794 =
																			BGL_CLASS_NUM
																			(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt) ((BgL_svarz00_bglt)
																					BgL_tmp1116z00_1791)),
																			BgL_arg1376z00_1794);
																	}
																	((BgL_svarz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1116z00_1791));
																}
																{
																	BgL_svarzf2ginfozf2_bglt BgL_auxz00_3066;

																	{
																		obj_t BgL_auxz00_3067;

																		{	/* Globalize/gloclo.scm 92 */
																			BgL_objectz00_bglt BgL_tmpz00_3068;

																			BgL_tmpz00_3068 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_tmp1116z00_1791));
																			BgL_auxz00_3067 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_3068);
																		}
																		BgL_auxz00_3066 =
																			((BgL_svarzf2ginfozf2_bglt)
																			BgL_auxz00_3067);
																	}
																	((((BgL_svarzf2ginfozf2_bglt)
																				COBJECT(BgL_auxz00_3066))->
																			BgL_kapturedzf3zf3) =
																		((bool_t) ((bool_t) 0)), BUNSPEC);
																}
																{
																	BgL_svarzf2ginfozf2_bglt BgL_auxz00_3074;

																	{
																		obj_t BgL_auxz00_3075;

																		{	/* Globalize/gloclo.scm 92 */
																			BgL_objectz00_bglt BgL_tmpz00_3076;

																			BgL_tmpz00_3076 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_tmp1116z00_1791));
																			BgL_auxz00_3075 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_3076);
																		}
																		BgL_auxz00_3074 =
																			((BgL_svarzf2ginfozf2_bglt)
																			BgL_auxz00_3075);
																	}
																	((((BgL_svarzf2ginfozf2_bglt)
																				COBJECT(BgL_auxz00_3074))->
																			BgL_freezd2markzd2) =
																		((long) -10L), BUNSPEC);
																}
																{
																	BgL_svarzf2ginfozf2_bglt BgL_auxz00_3082;

																	{
																		obj_t BgL_auxz00_3083;

																		{	/* Globalize/gloclo.scm 92 */
																			BgL_objectz00_bglt BgL_tmpz00_3084;

																			BgL_tmpz00_3084 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_tmp1116z00_1791));
																			BgL_auxz00_3083 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_3084);
																		}
																		BgL_auxz00_3082 =
																			((BgL_svarzf2ginfozf2_bglt)
																			BgL_auxz00_3083);
																	}
																	((((BgL_svarzf2ginfozf2_bglt)
																				COBJECT(BgL_auxz00_3082))->
																			BgL_markz00) = ((long) -10L), BUNSPEC);
																}
																{
																	BgL_svarzf2ginfozf2_bglt BgL_auxz00_3090;

																	{
																		obj_t BgL_auxz00_3091;

																		{	/* Globalize/gloclo.scm 92 */
																			BgL_objectz00_bglt BgL_tmpz00_3092;

																			BgL_tmpz00_3092 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_tmp1116z00_1791));
																			BgL_auxz00_3091 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_3092);
																		}
																		BgL_auxz00_3090 =
																			((BgL_svarzf2ginfozf2_bglt)
																			BgL_auxz00_3091);
																	}
																	((((BgL_svarzf2ginfozf2_bglt)
																				COBJECT(BgL_auxz00_3090))->
																			BgL_celledzf3zf3) =
																		((bool_t) ((bool_t) 0)), BUNSPEC);
																}
																{
																	BgL_svarzf2ginfozf2_bglt BgL_auxz00_3098;

																	{
																		obj_t BgL_auxz00_3099;

																		{	/* Globalize/gloclo.scm 92 */
																			BgL_objectz00_bglt BgL_tmpz00_3100;

																			BgL_tmpz00_3100 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_tmp1116z00_1791));
																			BgL_auxz00_3099 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_3100);
																		}
																		BgL_auxz00_3098 =
																			((BgL_svarzf2ginfozf2_bglt)
																			BgL_auxz00_3099);
																	}
																	((((BgL_svarzf2ginfozf2_bglt)
																				COBJECT(BgL_auxz00_3098))->
																			BgL_stackablez00) =
																		((bool_t) ((bool_t) 1)), BUNSPEC);
																}
																((BgL_svarz00_bglt) BgL_tmp1116z00_1791);
															}
															{	/* Globalize/gloclo.scm 93 */
																BgL_localzf2ginfozf2_bglt BgL_wide1122z00_1798;

																BgL_wide1122z00_1798 =
																	((BgL_localzf2ginfozf2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_localzf2ginfozf2_bgl))));
																{	/* Globalize/gloclo.scm 93 */
																	obj_t BgL_auxz00_3111;
																	BgL_objectz00_bglt BgL_tmpz00_3108;

																	BgL_auxz00_3111 =
																		((obj_t) BgL_wide1122z00_1798);
																	BgL_tmpz00_3108 =
																		((BgL_objectz00_bglt)
																		((BgL_localz00_bglt) BgL_newz00_1788));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3108,
																		BgL_auxz00_3111);
																}
																((BgL_objectz00_bglt)
																	((BgL_localz00_bglt) BgL_newz00_1788));
																{	/* Globalize/gloclo.scm 93 */
																	long BgL_arg1377z00_1799;

																	BgL_arg1377z00_1799 =
																		BGL_CLASS_NUM
																		(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_localz00_bglt) BgL_newz00_1788)),
																		BgL_arg1377z00_1799);
																}
																((BgL_localz00_bglt)
																	((BgL_localz00_bglt) BgL_newz00_1788));
															}
															{
																BgL_localzf2ginfozf2_bglt BgL_auxz00_3122;

																{
																	obj_t BgL_auxz00_3123;

																	{	/* Globalize/gloclo.scm 93 */
																		BgL_objectz00_bglt BgL_tmpz00_3124;

																		BgL_tmpz00_3124 =
																			((BgL_objectz00_bglt)
																			((BgL_localz00_bglt) BgL_newz00_1788));
																		BgL_auxz00_3123 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3124);
																	}
																	BgL_auxz00_3122 =
																		((BgL_localzf2ginfozf2_bglt)
																		BgL_auxz00_3123);
																}
																((((BgL_localzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3122))->
																		BgL_escapezf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															{
																BgL_localzf2ginfozf2_bglt BgL_auxz00_3130;

																{
																	obj_t BgL_auxz00_3131;

																	{	/* Globalize/gloclo.scm 93 */
																		BgL_objectz00_bglt BgL_tmpz00_3132;

																		BgL_tmpz00_3132 =
																			((BgL_objectz00_bglt)
																			((BgL_localz00_bglt) BgL_newz00_1788));
																		BgL_auxz00_3131 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3132);
																	}
																	BgL_auxz00_3130 =
																		((BgL_localzf2ginfozf2_bglt)
																		BgL_auxz00_3131);
																}
																((((BgL_localzf2ginfozf2_bglt)
																			COBJECT(BgL_auxz00_3130))->
																		BgL_globaliza7edzf3z54) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															((BgL_localz00_bglt) BgL_newz00_1788);
															BgL_arg1370z00_1786 = BgL_newz00_1788;
													}}
													BgL_newtail1280z00_1784 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1370z00_1786), BNIL);
												}
												SET_CDR(BgL_tail1279z00_1781, BgL_newtail1280z00_1784);
												{	/* Globalize/gloclo.scm 84 */
													obj_t BgL_arg1367z00_1785;

													BgL_arg1367z00_1785 =
														CDR(((obj_t) BgL_l1276z00_1780));
													{
														obj_t BgL_tail1279z00_3145;
														obj_t BgL_l1276z00_3144;

														BgL_l1276z00_3144 = BgL_arg1367z00_1785;
														BgL_tail1279z00_3145 = BgL_newtail1280z00_1784;
														BgL_tail1279z00_1781 = BgL_tail1279z00_3145;
														BgL_l1276z00_1780 = BgL_l1276z00_3144;
														goto BgL_zc3z04anonymousza31363ze3z87_1782;
													}
												}
											}
									}
								}
						}
						{	/* Globalize/gloclo.scm 84 */
							obj_t BgL_locz00_1751;

							{	/* Globalize/gloclo.scm 106 */
								bool_t BgL_test1878z00_3146;

								{	/* Globalize/gloclo.scm 106 */
									obj_t BgL_classz00_2334;

									BgL_classz00_2334 = BGl_sfunz00zzast_varz00;
									{	/* Globalize/gloclo.scm 106 */
										BgL_objectz00_bglt BgL_arg1807z00_2336;

										{	/* Globalize/gloclo.scm 106 */
											obj_t BgL_tmpz00_3147;

											BgL_tmpz00_3147 =
												((obj_t) ((BgL_objectz00_bglt) BgL_oldzd2funzd2_1748));
											BgL_arg1807z00_2336 =
												(BgL_objectz00_bglt) (BgL_tmpz00_3147);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Globalize/gloclo.scm 106 */
												long BgL_idxz00_2342;

												BgL_idxz00_2342 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2336);
												BgL_test1878z00_3146 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2342 + 3L)) == BgL_classz00_2334);
											}
										else
											{	/* Globalize/gloclo.scm 106 */
												bool_t BgL_res1812z00_2367;

												{	/* Globalize/gloclo.scm 106 */
													obj_t BgL_oclassz00_2350;

													{	/* Globalize/gloclo.scm 106 */
														obj_t BgL_arg1815z00_2358;
														long BgL_arg1816z00_2359;

														BgL_arg1815z00_2358 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Globalize/gloclo.scm 106 */
															long BgL_arg1817z00_2360;

															BgL_arg1817z00_2360 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2336);
															BgL_arg1816z00_2359 =
																(BgL_arg1817z00_2360 - OBJECT_TYPE);
														}
														BgL_oclassz00_2350 =
															VECTOR_REF(BgL_arg1815z00_2358,
															BgL_arg1816z00_2359);
													}
													{	/* Globalize/gloclo.scm 106 */
														bool_t BgL__ortest_1115z00_2351;

														BgL__ortest_1115z00_2351 =
															(BgL_classz00_2334 == BgL_oclassz00_2350);
														if (BgL__ortest_1115z00_2351)
															{	/* Globalize/gloclo.scm 106 */
																BgL_res1812z00_2367 = BgL__ortest_1115z00_2351;
															}
														else
															{	/* Globalize/gloclo.scm 106 */
																long BgL_odepthz00_2352;

																{	/* Globalize/gloclo.scm 106 */
																	obj_t BgL_arg1804z00_2353;

																	BgL_arg1804z00_2353 = (BgL_oclassz00_2350);
																	BgL_odepthz00_2352 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2353);
																}
																if ((3L < BgL_odepthz00_2352))
																	{	/* Globalize/gloclo.scm 106 */
																		obj_t BgL_arg1802z00_2355;

																		{	/* Globalize/gloclo.scm 106 */
																			obj_t BgL_arg1803z00_2356;

																			BgL_arg1803z00_2356 =
																				(BgL_oclassz00_2350);
																			BgL_arg1802z00_2355 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2356, 3L);
																		}
																		BgL_res1812z00_2367 =
																			(BgL_arg1802z00_2355 ==
																			BgL_classz00_2334);
																	}
																else
																	{	/* Globalize/gloclo.scm 106 */
																		BgL_res1812z00_2367 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1878z00_3146 = BgL_res1812z00_2367;
											}
									}
								}
								if (BgL_test1878z00_3146)
									{	/* Globalize/gloclo.scm 107 */
										bool_t BgL_test1882z00_3170;

										{	/* Globalize/gloclo.scm 107 */
											bool_t BgL_test1883z00_3171;

											{	/* Globalize/gloclo.scm 107 */
												obj_t BgL_arg1361z00_1775;

												BgL_arg1361z00_1775 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1748)))->
													BgL_bodyz00);
												{	/* Globalize/gloclo.scm 107 */
													obj_t BgL_classz00_2369;

													BgL_classz00_2369 = BGl_nodez00zzast_nodez00;
													if (BGL_OBJECTP(BgL_arg1361z00_1775))
														{	/* Globalize/gloclo.scm 107 */
															BgL_objectz00_bglt BgL_arg1807z00_2371;

															BgL_arg1807z00_2371 =
																(BgL_objectz00_bglt) (BgL_arg1361z00_1775);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Globalize/gloclo.scm 107 */
																	long BgL_idxz00_2377;

																	BgL_idxz00_2377 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2371);
																	BgL_test1883z00_3171 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2377 + 1L)) ==
																		BgL_classz00_2369);
																}
															else
																{	/* Globalize/gloclo.scm 107 */
																	bool_t BgL_res1813z00_2402;

																	{	/* Globalize/gloclo.scm 107 */
																		obj_t BgL_oclassz00_2385;

																		{	/* Globalize/gloclo.scm 107 */
																			obj_t BgL_arg1815z00_2393;
																			long BgL_arg1816z00_2394;

																			BgL_arg1815z00_2393 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Globalize/gloclo.scm 107 */
																				long BgL_arg1817z00_2395;

																				BgL_arg1817z00_2395 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2371);
																				BgL_arg1816z00_2394 =
																					(BgL_arg1817z00_2395 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2385 =
																				VECTOR_REF(BgL_arg1815z00_2393,
																				BgL_arg1816z00_2394);
																		}
																		{	/* Globalize/gloclo.scm 107 */
																			bool_t BgL__ortest_1115z00_2386;

																			BgL__ortest_1115z00_2386 =
																				(BgL_classz00_2369 ==
																				BgL_oclassz00_2385);
																			if (BgL__ortest_1115z00_2386)
																				{	/* Globalize/gloclo.scm 107 */
																					BgL_res1813z00_2402 =
																						BgL__ortest_1115z00_2386;
																				}
																			else
																				{	/* Globalize/gloclo.scm 107 */
																					long BgL_odepthz00_2387;

																					{	/* Globalize/gloclo.scm 107 */
																						obj_t BgL_arg1804z00_2388;

																						BgL_arg1804z00_2388 =
																							(BgL_oclassz00_2385);
																						BgL_odepthz00_2387 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2388);
																					}
																					if ((1L < BgL_odepthz00_2387))
																						{	/* Globalize/gloclo.scm 107 */
																							obj_t BgL_arg1802z00_2390;

																							{	/* Globalize/gloclo.scm 107 */
																								obj_t BgL_arg1803z00_2391;

																								BgL_arg1803z00_2391 =
																									(BgL_oclassz00_2385);
																								BgL_arg1802z00_2390 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2391, 1L);
																							}
																							BgL_res1813z00_2402 =
																								(BgL_arg1802z00_2390 ==
																								BgL_classz00_2369);
																						}
																					else
																						{	/* Globalize/gloclo.scm 107 */
																							BgL_res1813z00_2402 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1883z00_3171 = BgL_res1813z00_2402;
																}
														}
													else
														{	/* Globalize/gloclo.scm 107 */
															BgL_test1883z00_3171 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test1883z00_3171)
												{	/* Globalize/gloclo.scm 107 */
													BgL_test1882z00_3170 =
														CBOOL(
														(((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt)
																		(((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						BgL_oldzd2funzd2_1748)))->
																			BgL_bodyz00))))->BgL_locz00));
												}
											else
												{	/* Globalize/gloclo.scm 107 */
													BgL_test1882z00_3170 = ((bool_t) 0);
												}
										}
										if (BgL_test1882z00_3170)
											{	/* Globalize/gloclo.scm 107 */
												BgL_locz00_1751 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt)
																				BgL_oldzd2funzd2_1748)))->
																	BgL_bodyz00))))->BgL_locz00);
											}
										else
											{	/* Globalize/gloclo.scm 107 */
												BgL_locz00_1751 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1748)))->
													BgL_locz00);
											}
									}
								else
									{	/* Globalize/gloclo.scm 106 */
										BgL_locz00_1751 = BFALSE;
									}
							}
							{	/* Globalize/gloclo.scm 106 */
								obj_t BgL_glocloz00_1752;

								BgL_glocloz00_1752 =
									BGl_glocloz00zzglobaliza7e_globalzd2closurez75
									(BgL_globalz00_7, BgL_envz00_1749, BgL_newzd2argszd2_1750);
								{	/* Globalize/gloclo.scm 111 */
									BgL_valuez00_bglt BgL_newzd2funzd2_1753;

									BgL_newzd2funzd2_1753 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_glocloz00_1752))))->
										BgL_valuez00);
									{	/* Globalize/gloclo.scm 112 */

										{	/* Globalize/gloclo.scm 114 */
											BgL_valuez00_bglt BgL_arg1335z00_1754;

											BgL_arg1335z00_1754 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_glocloz00_1752))))->
												BgL_valuez00);
											((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
																BgL_arg1335z00_1754)))->
													BgL_thezd2closurezd2globalz00) =
												((obj_t) ((obj_t) BgL_globalz00_7)), BUNSPEC);
										}
										{	/* Globalize/gloclo.scm 116 */
											BgL_svarz00_bglt BgL_tmp1124z00_1755;

											BgL_tmp1124z00_1755 =
												((BgL_svarz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_envz00_1749)))->
													BgL_valuez00));
											{	/* Globalize/gloclo.scm 116 */
												BgL_svarzf2ginfozf2_bglt BgL_wide1126z00_1757;

												BgL_wide1126z00_1757 =
													((BgL_svarzf2ginfozf2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_svarzf2ginfozf2_bgl))));
												{	/* Globalize/gloclo.scm 116 */
													obj_t BgL_auxz00_3224;
													BgL_objectz00_bglt BgL_tmpz00_3221;

													BgL_auxz00_3224 = ((obj_t) BgL_wide1126z00_1757);
													BgL_tmpz00_3221 =
														((BgL_objectz00_bglt)
														((BgL_svarz00_bglt) BgL_tmp1124z00_1755));
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3221,
														BgL_auxz00_3224);
												}
												((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1124z00_1755));
												{	/* Globalize/gloclo.scm 116 */
													long BgL_arg1339z00_1758;

													BgL_arg1339z00_1758 =
														BGL_CLASS_NUM
														(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
													BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																(BgL_svarz00_bglt) BgL_tmp1124z00_1755)),
														BgL_arg1339z00_1758);
												}
												((BgL_svarz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1124z00_1755));
											}
											{
												BgL_svarzf2ginfozf2_bglt BgL_auxz00_3235;

												{
													obj_t BgL_auxz00_3236;

													{	/* Globalize/gloclo.scm 116 */
														BgL_objectz00_bglt BgL_tmpz00_3237;

														BgL_tmpz00_3237 =
															((BgL_objectz00_bglt)
															((BgL_svarz00_bglt) BgL_tmp1124z00_1755));
														BgL_auxz00_3236 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3237);
													}
													BgL_auxz00_3235 =
														((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3236);
												}
												((((BgL_svarzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_3235))->BgL_kapturedzf3zf3) =
													((bool_t) ((bool_t) 0)), BUNSPEC);
											}
											{
												BgL_svarzf2ginfozf2_bglt BgL_auxz00_3243;

												{
													obj_t BgL_auxz00_3244;

													{	/* Globalize/gloclo.scm 116 */
														BgL_objectz00_bglt BgL_tmpz00_3245;

														BgL_tmpz00_3245 =
															((BgL_objectz00_bglt)
															((BgL_svarz00_bglt) BgL_tmp1124z00_1755));
														BgL_auxz00_3244 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3245);
													}
													BgL_auxz00_3243 =
														((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3244);
												}
												((((BgL_svarzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_3243))->BgL_freezd2markzd2) =
													((long) -10L), BUNSPEC);
											}
											{
												BgL_svarzf2ginfozf2_bglt BgL_auxz00_3251;

												{
													obj_t BgL_auxz00_3252;

													{	/* Globalize/gloclo.scm 116 */
														BgL_objectz00_bglt BgL_tmpz00_3253;

														BgL_tmpz00_3253 =
															((BgL_objectz00_bglt)
															((BgL_svarz00_bglt) BgL_tmp1124z00_1755));
														BgL_auxz00_3252 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3253);
													}
													BgL_auxz00_3251 =
														((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3252);
												}
												((((BgL_svarzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_3251))->BgL_markz00) =
													((long) -10L), BUNSPEC);
											}
											{
												BgL_svarzf2ginfozf2_bglt BgL_auxz00_3259;

												{
													obj_t BgL_auxz00_3260;

													{	/* Globalize/gloclo.scm 116 */
														BgL_objectz00_bglt BgL_tmpz00_3261;

														BgL_tmpz00_3261 =
															((BgL_objectz00_bglt)
															((BgL_svarz00_bglt) BgL_tmp1124z00_1755));
														BgL_auxz00_3260 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3261);
													}
													BgL_auxz00_3259 =
														((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3260);
												}
												((((BgL_svarzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_3259))->BgL_celledzf3zf3) =
													((bool_t) ((bool_t) 0)), BUNSPEC);
											}
											{
												BgL_svarzf2ginfozf2_bglt BgL_auxz00_3267;

												{
													obj_t BgL_auxz00_3268;

													{	/* Globalize/gloclo.scm 116 */
														BgL_objectz00_bglt BgL_tmpz00_3269;

														BgL_tmpz00_3269 =
															((BgL_objectz00_bglt)
															((BgL_svarz00_bglt) BgL_tmp1124z00_1755));
														BgL_auxz00_3268 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3269);
													}
													BgL_auxz00_3267 =
														((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_3268);
												}
												((((BgL_svarzf2ginfozf2_bglt)
															COBJECT(BgL_auxz00_3267))->BgL_stackablez00) =
													((bool_t) ((bool_t) 1)), BUNSPEC);
											}
											((BgL_svarz00_bglt) BgL_tmp1124z00_1755);
										}
										{	/* Globalize/gloclo.scm 118 */
											BgL_globalzf2ginfozf2_bglt BgL_wide1130z00_1762;

											BgL_wide1130z00_1762 =
												((BgL_globalzf2ginfozf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_globalzf2ginfozf2_bgl))));
											{	/* Globalize/gloclo.scm 118 */
												obj_t BgL_auxz00_3281;
												BgL_objectz00_bglt BgL_tmpz00_3277;

												BgL_auxz00_3281 = ((obj_t) BgL_wide1130z00_1762);
												BgL_tmpz00_3277 =
													((BgL_objectz00_bglt)
													((BgL_globalz00_bglt)
														((BgL_globalz00_bglt) BgL_glocloz00_1752)));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3277,
													BgL_auxz00_3281);
											}
											((BgL_objectz00_bglt)
												((BgL_globalz00_bglt)
													((BgL_globalz00_bglt) BgL_glocloz00_1752)));
											{	/* Globalize/gloclo.scm 118 */
												long BgL_arg1340z00_1763;

												BgL_arg1340z00_1763 =
													BGL_CLASS_NUM
													(BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
															(BgL_globalz00_bglt) ((BgL_globalz00_bglt)
																BgL_glocloz00_1752))), BgL_arg1340z00_1763);
											}
											((BgL_globalz00_bglt)
												((BgL_globalz00_bglt)
													((BgL_globalz00_bglt) BgL_glocloz00_1752)));
										}
										{
											BgL_globalzf2ginfozf2_bglt BgL_auxz00_3295;

											{
												obj_t BgL_auxz00_3296;

												{	/* Globalize/gloclo.scm 118 */
													BgL_objectz00_bglt BgL_tmpz00_3297;

													BgL_tmpz00_3297 =
														((BgL_objectz00_bglt)
														((BgL_globalz00_bglt)
															((BgL_globalz00_bglt) BgL_glocloz00_1752)));
													BgL_auxz00_3296 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3297);
												}
												BgL_auxz00_3295 =
													((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_3296);
											}
											((((BgL_globalzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_3295))->BgL_escapezf3zf3) =
												((bool_t) ((bool_t) 1)), BUNSPEC);
										}
										{
											BgL_globalzf2ginfozf2_bglt BgL_auxz00_3304;

											{
												obj_t BgL_auxz00_3305;

												{	/* Globalize/gloclo.scm 118 */
													BgL_objectz00_bglt BgL_tmpz00_3306;

													BgL_tmpz00_3306 =
														((BgL_objectz00_bglt)
														((BgL_globalz00_bglt)
															((BgL_globalz00_bglt) BgL_glocloz00_1752)));
													BgL_auxz00_3305 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3306);
												}
												BgL_auxz00_3304 =
													((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_3305);
											}
											((((BgL_globalzf2ginfozf2_bglt)
														COBJECT(BgL_auxz00_3304))->
													BgL_globalzd2closurezd2) = ((obj_t) BFALSE), BUNSPEC);
										}
										((BgL_globalz00_bglt)
											((BgL_globalz00_bglt) BgL_glocloz00_1752));
										((((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_newzd2funzd2_1753)))->
												BgL_bodyz00) =
											((obj_t) ((obj_t)
													BGl_makezd2nooptzd2bodyz00zzglobaliza7e_globalzd2closurez75
													(BgL_locz00_1751, BgL_globalz00_7,
														BgL_newzd2argszd2_1750))), BUNSPEC);
										return BgL_glocloz00_1752;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* make-noopt-body */
	BgL_appz00_bglt
		BGl_makezd2nooptzd2bodyz00zzglobaliza7e_globalzd2closurez75(obj_t
		BgL_locz00_8, BgL_globalz00_bglt BgL_globalz00_9,
		obj_t BgL_newzd2argszd2_10)
	{
		{	/* Globalize/gloclo.scm 130 */
			{	/* Globalize/gloclo.scm 131 */
				BgL_appz00_bglt BgL_new1134z00_1814;

				{	/* Globalize/gloclo.scm 132 */
					BgL_appz00_bglt BgL_new1133z00_1837;

					BgL_new1133z00_1837 =
						((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_appz00_bgl))));
					{	/* Globalize/gloclo.scm 132 */
						long BgL_arg1454z00_1838;

						BgL_arg1454z00_1838 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1133z00_1837), BgL_arg1454z00_1838);
					}
					{	/* Globalize/gloclo.scm 132 */
						BgL_objectz00_bglt BgL_tmpz00_3323;

						BgL_tmpz00_3323 = ((BgL_objectz00_bglt) BgL_new1133z00_1837);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3323, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1133z00_1837);
					BgL_new1134z00_1814 = BgL_new1133z00_1837;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1134z00_1814)))->BgL_locz00) =
					((obj_t) BgL_locz00_8), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3329;

					{	/* Globalize/gloclo.scm 133 */
						BgL_typez00_bglt BgL_arg1410z00_1815;

						BgL_arg1410z00_1815 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_globalz00_9)))->BgL_typez00);
						BgL_auxz00_3329 =
							BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1410z00_1815,
							((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt) BgL_new1134z00_1814)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_3329), BUNSPEC);
				}
				((((BgL_nodezf2effectzf2_bglt) COBJECT(
								((BgL_nodezf2effectzf2_bglt) BgL_new1134z00_1814)))->
						BgL_sidezd2effectzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1134z00_1814)))->BgL_keyz00) =
					((obj_t) BINT(-1L)), BUNSPEC);
				{
					BgL_varz00_bglt BgL_auxz00_3341;

					{	/* Globalize/gloclo.scm 134 */
						BgL_refz00_bglt BgL_new1136z00_1816;

						{	/* Globalize/gloclo.scm 135 */
							BgL_refz00_bglt BgL_new1135z00_1818;

							BgL_new1135z00_1818 =
								((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_refz00_bgl))));
							{	/* Globalize/gloclo.scm 135 */
								long BgL_arg1422z00_1819;

								{	/* Globalize/gloclo.scm 135 */
									obj_t BgL_classz00_2432;

									BgL_classz00_2432 = BGl_refz00zzast_nodez00;
									BgL_arg1422z00_1819 = BGL_CLASS_NUM(BgL_classz00_2432);
								}
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1135z00_1818),
									BgL_arg1422z00_1819);
							}
							{	/* Globalize/gloclo.scm 135 */
								BgL_objectz00_bglt BgL_tmpz00_3346;

								BgL_tmpz00_3346 = ((BgL_objectz00_bglt) BgL_new1135z00_1818);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3346, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1135z00_1818);
							BgL_new1136z00_1816 = BgL_new1135z00_1818;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1136z00_1816)))->BgL_locz00) =
							((obj_t) BgL_locz00_8), BUNSPEC);
						{
							BgL_typez00_bglt BgL_auxz00_3352;

							{	/* Globalize/gloclo.scm 136 */
								BgL_typez00_bglt BgL_arg1421z00_1817;

								BgL_arg1421z00_1817 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_9)))->BgL_typez00);
								BgL_auxz00_3352 =
									BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1421z00_1817,
									((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00));
							}
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_new1136z00_1816)))->BgL_typez00) =
								((BgL_typez00_bglt) BgL_auxz00_3352), BUNSPEC);
						}
						((((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_new1136z00_1816)))->
								BgL_variablez00) =
							((BgL_variablez00_bglt) ((BgL_variablez00_bglt) BgL_globalz00_9)),
							BUNSPEC);
						BgL_auxz00_3341 = ((BgL_varz00_bglt) BgL_new1136z00_1816);
					}
					((((BgL_appz00_bglt) COBJECT(BgL_new1134z00_1814))->BgL_funz00) =
						((BgL_varz00_bglt) BgL_auxz00_3341), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_3364;

					if (NULLP(BgL_newzd2argszd2_10))
						{	/* Globalize/gloclo.scm 141 */
							BgL_auxz00_3364 = BNIL;
						}
					else
						{	/* Globalize/gloclo.scm 141 */
							obj_t BgL_head1283z00_1822;

							BgL_head1283z00_1822 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1281z00_1824;
								obj_t BgL_tail1284z00_1825;

								BgL_l1281z00_1824 = BgL_newzd2argszd2_10;
								BgL_tail1284z00_1825 = BgL_head1283z00_1822;
							BgL_zc3z04anonymousza31424ze3z87_1826:
								if (NULLP(BgL_l1281z00_1824))
									{	/* Globalize/gloclo.scm 141 */
										BgL_auxz00_3364 = CDR(BgL_head1283z00_1822);
									}
								else
									{	/* Globalize/gloclo.scm 141 */
										obj_t BgL_newtail1285z00_1828;

										{	/* Globalize/gloclo.scm 141 */
											BgL_refz00_bglt BgL_arg1437z00_1830;

											{	/* Globalize/gloclo.scm 141 */
												obj_t BgL_vz00_1831;

												BgL_vz00_1831 = CAR(((obj_t) BgL_l1281z00_1824));
												{	/* Globalize/gloclo.scm 142 */
													BgL_refz00_bglt BgL_new1138z00_1832;

													{	/* Globalize/gloclo.scm 145 */
														BgL_refz00_bglt BgL_new1137z00_1834;

														BgL_new1137z00_1834 =
															((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_refz00_bgl))));
														{	/* Globalize/gloclo.scm 145 */
															long BgL_arg1453z00_1835;

															{	/* Globalize/gloclo.scm 145 */
																obj_t BgL_classz00_2439;

																BgL_classz00_2439 = BGl_refz00zzast_nodez00;
																BgL_arg1453z00_1835 =
																	BGL_CLASS_NUM(BgL_classz00_2439);
															}
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1137z00_1834),
																BgL_arg1453z00_1835);
														}
														{	/* Globalize/gloclo.scm 145 */
															BgL_objectz00_bglt BgL_tmpz00_3377;

															BgL_tmpz00_3377 =
																((BgL_objectz00_bglt) BgL_new1137z00_1834);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3377, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1137z00_1834);
														BgL_new1138z00_1832 = BgL_new1137z00_1834;
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1138z00_1832)))->
															BgL_locz00) = ((obj_t) BgL_locz00_8), BUNSPEC);
													{
														BgL_typez00_bglt BgL_auxz00_3383;

														{	/* Globalize/gloclo.scm 144 */
															BgL_typez00_bglt BgL_arg1448z00_1833;

															BgL_arg1448z00_1833 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_vz00_1831)))->
																BgL_typez00);
															BgL_auxz00_3383 =
																BGl_strictzd2nodezd2typez00zzast_nodez00
																(BgL_arg1448z00_1833,
																((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00));
														}
														((((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_new1138z00_1832)))->
																BgL_typez00) =
															((BgL_typez00_bglt) BgL_auxz00_3383), BUNSPEC);
													}
													((((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_new1138z00_1832)))->
															BgL_variablez00) =
														((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																BgL_vz00_1831)), BUNSPEC);
													BgL_arg1437z00_1830 = BgL_new1138z00_1832;
											}}
											BgL_newtail1285z00_1828 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1437z00_1830), BNIL);
										}
										SET_CDR(BgL_tail1284z00_1825, BgL_newtail1285z00_1828);
										{	/* Globalize/gloclo.scm 141 */
											obj_t BgL_arg1434z00_1829;

											BgL_arg1434z00_1829 = CDR(((obj_t) BgL_l1281z00_1824));
											{
												obj_t BgL_tail1284z00_3399;
												obj_t BgL_l1281z00_3398;

												BgL_l1281z00_3398 = BgL_arg1434z00_1829;
												BgL_tail1284z00_3399 = BgL_newtail1285z00_1828;
												BgL_tail1284z00_1825 = BgL_tail1284z00_3399;
												BgL_l1281z00_1824 = BgL_l1281z00_3398;
												goto BgL_zc3z04anonymousza31424ze3z87_1826;
											}
										}
									}
							}
						}
					((((BgL_appz00_bglt) COBJECT(BgL_new1134z00_1814))->BgL_argsz00) =
						((obj_t) BgL_auxz00_3364), BUNSPEC);
				}
				((((BgL_appz00_bglt) COBJECT(BgL_new1134z00_1814))->BgL_stackablez00) =
					((obj_t) BFALSE), BUNSPEC);
				return BgL_new1134z00_1814;
			}
		}

	}



/* foreign-closures */
	BGL_EXPORTED_DEF obj_t
		BGl_foreignzd2closureszd2zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 156 */
			{	/* Globalize/gloclo.scm 157 */
				obj_t BgL_resz00_1839;

				BgL_resz00_1839 =
					BGl_za2foreignzd2closuresza2zd2zzglobaliza7e_globalzd2closurez75;
				BGl_za2foreignzd2closuresza2zd2zzglobaliza7e_globalzd2closurez75 = BNIL;
				return BgL_resz00_1839;
			}
		}

	}



/* &foreign-closures */
	obj_t BGl_z62foreignzd2closureszb0zzglobaliza7e_globalzd2closurez75(obj_t
		BgL_envz00_2737)
	{
		{	/* Globalize/gloclo.scm 156 */
			return BGl_foreignzd2closureszd2zzglobaliza7e_globalzd2closurez75();
		}

	}



/* globalized-type-id */
	obj_t
		BGl_globaliza7edzd2typezd2idza7zzglobaliza7e_globalzd2closurez75
		(BgL_globalz00_bglt BgL_globalz00_11)
	{
		{	/* Globalize/gloclo.scm 169 */
			{	/* Globalize/gloclo.scm 170 */
				BgL_typez00_bglt BgL_typez00_1840;

				BgL_typez00_1840 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_globalz00_11)))->BgL_typez00);
				if (CBOOL
					(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
					{	/* Globalize/gloclo.scm 172 */
						if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_1840))
							{	/* Globalize/gloclo.scm 174 */
								return
									(((BgL_typez00_bglt) COBJECT(BgL_typez00_1840))->BgL_idz00);
							}
						else
							{	/* Globalize/gloclo.scm 174 */
								if (
									((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_11))->
											BgL_importz00) == CNST_TABLE_REF(2)))
									{	/* Globalize/gloclo.scm 176 */
										return CNST_TABLE_REF(0);
									}
								else
									{	/* Globalize/gloclo.scm 176 */
										return
											(((BgL_typez00_bglt)
												COBJECT(BGl_getzd2bigloozd2typez00zztype_cachez00
													(BgL_typez00_1840)))->BgL_idz00);
									}
							}
					}
				else
					{	/* Globalize/gloclo.scm 172 */
						return CNST_TABLE_REF(3);
					}
			}
		}

	}



/* gloclo */
	obj_t BGl_glocloz00zzglobaliza7e_globalzd2closurez75(BgL_globalz00_bglt
		BgL_globalz00_12, BgL_localz00_bglt BgL_envz00_13, obj_t BgL_argsz00_14)
	{
		{	/* Globalize/gloclo.scm 186 */
			{	/* Globalize/gloclo.scm 187 */
				int BgL_arityz00_1846;

				{	/* Globalize/gloclo.scm 187 */
					BgL_valuez00_bglt BgL_arg1594z00_1875;

					BgL_arg1594z00_1875 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_12)))->BgL_valuez00);
					BgL_arityz00_1846 =
						BGl_funzd2optionalzd2arityz00zzast_varz00(
						((BgL_funz00_bglt) BgL_arg1594z00_1875));
				}
				{	/* Globalize/gloclo.scm 187 */
					obj_t BgL_idz00_1847;

					{	/* Globalize/gloclo.scm 188 */
						obj_t BgL_strz00_1862;

						{	/* Globalize/gloclo.scm 190 */
							obj_t BgL_arg1591z00_1873;

							{	/* Globalize/gloclo.scm 190 */
								obj_t BgL_arg1593z00_1874;

								BgL_arg1593z00_1874 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_12)))->BgL_idz00);
								{	/* Globalize/gloclo.scm 190 */
									obj_t BgL_arg1455z00_2453;

									BgL_arg1455z00_2453 = SYMBOL_TO_STRING(BgL_arg1593z00_1874);
									BgL_arg1591z00_1873 =
										BGl_stringzd2copyzd2zz__r4_strings_6_7z00
										(BgL_arg1455z00_2453);
							}}
							BgL_strz00_1862 =
								string_append
								(BGl_string1823z00zzglobaliza7e_globalzd2closurez75,
								BgL_arg1591z00_1873);
						}
						if (symbol_exists_p(BSTRING_TO_STRING(BgL_strz00_1862)))
							{	/* Globalize/gloclo.scm 192 */
								obj_t BgL_arg1564z00_1864;

								{	/* Globalize/gloclo.scm 192 */
									obj_t BgL_arg1565z00_1865;

									{	/* Globalize/gloclo.scm 192 */
										obj_t BgL_arg1571z00_1866;
										obj_t BgL_arg1573z00_1867;

										{	/* Globalize/gloclo.scm 192 */
											obj_t BgL_symbolz00_2454;

											BgL_symbolz00_2454 = CNST_TABLE_REF(4);
											{	/* Globalize/gloclo.scm 192 */
												obj_t BgL_arg1455z00_2455;

												BgL_arg1455z00_2455 =
													SYMBOL_TO_STRING(BgL_symbolz00_2454);
												BgL_arg1571z00_1866 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2455);
											}
										}
										{	/* Globalize/gloclo.scm 192 */
											obj_t BgL_arg1575z00_1868;

											BgL_arg1575z00_1868 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_globalz00_12)))->
												BgL_idz00);
											{	/* Globalize/gloclo.scm 192 */
												obj_t BgL_arg1455z00_2458;

												BgL_arg1455z00_2458 =
													SYMBOL_TO_STRING(BgL_arg1575z00_1868);
												BgL_arg1573z00_1867 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_2458);
											}
										}
										BgL_arg1565z00_1865 =
											string_append(BgL_arg1571z00_1866, BgL_arg1573z00_1867);
									}
									BgL_arg1564z00_1864 = bstring_to_symbol(BgL_arg1565z00_1865);
								}
								BgL_idz00_1847 =
									BGl_gensymz00zz__r4_symbols_6_4z00(BgL_arg1564z00_1864);
							}
						else
							{	/* Globalize/gloclo.scm 193 */
								obj_t BgL_arg1576z00_1869;

								{	/* Globalize/gloclo.scm 193 */
									obj_t BgL_arg1584z00_1870;
									obj_t BgL_arg1585z00_1871;

									{	/* Globalize/gloclo.scm 193 */
										obj_t BgL_symbolz00_2460;

										BgL_symbolz00_2460 = CNST_TABLE_REF(4);
										{	/* Globalize/gloclo.scm 193 */
											obj_t BgL_arg1455z00_2461;

											BgL_arg1455z00_2461 =
												SYMBOL_TO_STRING(BgL_symbolz00_2460);
											BgL_arg1584z00_1870 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2461);
										}
									}
									{	/* Globalize/gloclo.scm 193 */
										obj_t BgL_arg1589z00_1872;

										BgL_arg1589z00_1872 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_12)))->
											BgL_idz00);
										{	/* Globalize/gloclo.scm 193 */
											obj_t BgL_arg1455z00_2464;

											BgL_arg1455z00_2464 =
												SYMBOL_TO_STRING(BgL_arg1589z00_1872);
											BgL_arg1585z00_1871 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_2464);
										}
									}
									BgL_arg1576z00_1869 =
										string_append(BgL_arg1584z00_1870, BgL_arg1585z00_1871);
								}
								BgL_idz00_1847 = bstring_to_symbol(BgL_arg1576z00_1869);
							}
					}
					{	/* Globalize/gloclo.scm 188 */
						obj_t BgL_tyidz00_1848;

						BgL_tyidz00_1848 =
							BGl_globaliza7edzd2typezd2idza7zzglobaliza7e_globalzd2closurez75
							(BgL_globalz00_12);
						{	/* Globalize/gloclo.scm 194 */
							BgL_globalz00_bglt BgL_glocloz00_1849;

							{	/* Globalize/gloclo.scm 195 */
								obj_t BgL_arg1514z00_1853;
								obj_t BgL_arg1516z00_1854;
								obj_t BgL_arg1535z00_1855;
								obj_t BgL_arg1540z00_1856;
								obj_t BgL_arg1544z00_1857;

								BgL_arg1514z00_1853 =
									BGl_makezd2typedzd2identz00zzast_identz00(BgL_idz00_1847,
									BgL_tyidz00_1848);
								BgL_arg1516z00_1854 =
									BGl_makezd2nzd2protoz00zztools_argsz00
									(BGl_zb2zd2arityz60zztools_argsz00(BINT(BgL_arityz00_1846),
										BINT(1L)));
								BgL_arg1535z00_1855 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_envz00_13), BgL_argsz00_14);
								if (((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_12))->
											BgL_importz00) == CNST_TABLE_REF(5)))
									{	/* Globalize/gloclo.scm 198 */
										BgL_arg1540z00_1856 = BGl_za2moduleza2z00zzmodule_modulez00;
									}
								else
									{	/* Globalize/gloclo.scm 198 */
										BgL_arg1540z00_1856 =
											(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_12))->
											BgL_modulez00);
									}
								BgL_arg1544z00_1857 =
									(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_12))->
									BgL_importz00);
								BgL_glocloz00_1849 =
									BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2
									(BgL_arg1514z00_1853, BgL_arg1516z00_1854,
									BgL_arg1535z00_1855, BgL_arg1540z00_1856, BgL_arg1544z00_1857,
									CNST_TABLE_REF(6), CNST_TABLE_REF(7), BUNSPEC);
							}
							{	/* Globalize/gloclo.scm 195 */

								{	/* Globalize/gloclo.scm 205 */
									BgL_valuez00_bglt BgL_arg1502z00_1850;
									obj_t BgL_arg1509z00_1851;

									BgL_arg1502z00_1850 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_glocloz00_1849)))->
										BgL_valuez00);
									BgL_arg1509z00_1851 =
										(((BgL_funz00_bglt)
											COBJECT(((BgL_funz00_bglt) (((BgL_variablez00_bglt)
															COBJECT(((BgL_variablez00_bglt)
																	BgL_globalz00_12)))->BgL_valuez00))))->
										BgL_argszd2noescapezd2);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_arg1502z00_1850)))->BgL_argszd2noescapezd2) =
										((obj_t) BgL_arg1509z00_1851), BUNSPEC);
								}
								return
									BGl_fillzd2glocloz12zc0zzglobaliza7e_globalzd2closurez75
									(BgL_globalz00_12, BgL_glocloz00_1849);
							}
						}
					}
				}
			}
		}

	}



/* fill-gloclo! */
	obj_t
		BGl_fillzd2glocloz12zc0zzglobaliza7e_globalzd2closurez75(BgL_globalz00_bglt
		BgL_globalz00_15, BgL_globalz00_bglt BgL_gcloz00_16)
	{
		{	/* Globalize/gloclo.scm 212 */
			{
				BgL_globalzf2ginfozf2_bglt BgL_auxz00_3475;

				{
					obj_t BgL_auxz00_3476;

					{	/* Globalize/gloclo.scm 213 */
						BgL_objectz00_bglt BgL_tmpz00_3477;

						BgL_tmpz00_3477 =
							((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_globalz00_15));
						BgL_auxz00_3476 = BGL_OBJECT_WIDENING(BgL_tmpz00_3477);
					}
					BgL_auxz00_3475 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_3476);
				}
				((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_3475))->
						BgL_globalzd2closurezd2) =
					((obj_t) ((obj_t) BgL_gcloz00_16)), BUNSPEC);
			}
			{	/* Globalize/gloclo.scm 216 */
				BgL_valuez00_bglt BgL_sfunz00_1876;

				BgL_sfunz00_1876 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_globalz00_15)))->BgL_valuez00);
				{	/* Globalize/gloclo.scm 217 */
					bool_t BgL_test1895z00_3486;

					{	/* Globalize/gloclo.scm 217 */
						obj_t BgL_classz00_2476;

						BgL_classz00_2476 = BGl_sfunz00zzast_varz00;
						{	/* Globalize/gloclo.scm 217 */
							BgL_objectz00_bglt BgL_arg1807z00_2478;

							{	/* Globalize/gloclo.scm 217 */
								obj_t BgL_tmpz00_3487;

								BgL_tmpz00_3487 =
									((obj_t) ((BgL_objectz00_bglt) BgL_sfunz00_1876));
								BgL_arg1807z00_2478 = (BgL_objectz00_bglt) (BgL_tmpz00_3487);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/gloclo.scm 217 */
									long BgL_idxz00_2484;

									BgL_idxz00_2484 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2478);
									BgL_test1895z00_3486 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2484 + 3L)) == BgL_classz00_2476);
								}
							else
								{	/* Globalize/gloclo.scm 217 */
									bool_t BgL_res1814z00_2509;

									{	/* Globalize/gloclo.scm 217 */
										obj_t BgL_oclassz00_2492;

										{	/* Globalize/gloclo.scm 217 */
											obj_t BgL_arg1815z00_2500;
											long BgL_arg1816z00_2501;

											BgL_arg1815z00_2500 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/gloclo.scm 217 */
												long BgL_arg1817z00_2502;

												BgL_arg1817z00_2502 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2478);
												BgL_arg1816z00_2501 =
													(BgL_arg1817z00_2502 - OBJECT_TYPE);
											}
											BgL_oclassz00_2492 =
												VECTOR_REF(BgL_arg1815z00_2500, BgL_arg1816z00_2501);
										}
										{	/* Globalize/gloclo.scm 217 */
											bool_t BgL__ortest_1115z00_2493;

											BgL__ortest_1115z00_2493 =
												(BgL_classz00_2476 == BgL_oclassz00_2492);
											if (BgL__ortest_1115z00_2493)
												{	/* Globalize/gloclo.scm 217 */
													BgL_res1814z00_2509 = BgL__ortest_1115z00_2493;
												}
											else
												{	/* Globalize/gloclo.scm 217 */
													long BgL_odepthz00_2494;

													{	/* Globalize/gloclo.scm 217 */
														obj_t BgL_arg1804z00_2495;

														BgL_arg1804z00_2495 = (BgL_oclassz00_2492);
														BgL_odepthz00_2494 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2495);
													}
													if ((3L < BgL_odepthz00_2494))
														{	/* Globalize/gloclo.scm 217 */
															obj_t BgL_arg1802z00_2497;

															{	/* Globalize/gloclo.scm 217 */
																obj_t BgL_arg1803z00_2498;

																BgL_arg1803z00_2498 = (BgL_oclassz00_2492);
																BgL_arg1802z00_2497 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2498,
																	3L);
															}
															BgL_res1814z00_2509 =
																(BgL_arg1802z00_2497 == BgL_classz00_2476);
														}
													else
														{	/* Globalize/gloclo.scm 217 */
															BgL_res1814z00_2509 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1895z00_3486 = BgL_res1814z00_2509;
								}
						}
					}
					if (BgL_test1895z00_3486)
						{	/* Globalize/gloclo.scm 218 */
							BgL_valuez00_bglt BgL_arg1602z00_1878;
							obj_t BgL_arg1605z00_1879;

							BgL_arg1602z00_1878 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_gcloz00_16)))->BgL_valuez00);
							BgL_arg1605z00_1879 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_sfunz00_1876)))->BgL_locz00);
							((((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_arg1602z00_1878)))->BgL_locz00) =
								((obj_t) BgL_arg1605z00_1879), BUNSPEC);
						}
					else
						{	/* Globalize/gloclo.scm 217 */
							BFALSE;
						}
				}
				if (
					((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_15))->BgL_importz00) ==
						CNST_TABLE_REF(5)))
					{	/* Globalize/gloclo.scm 227 */
						BGl_za2foreignzd2closuresza2zd2zzglobaliza7e_globalzd2closurez75 =
							MAKE_YOUNG_PAIR(
							((obj_t) BgL_gcloz00_16),
							BGl_za2foreignzd2closuresza2zd2zzglobaliza7e_globalzd2closurez75);
						{	/* Globalize/gloclo.scm 230 */
							obj_t BgL_vz00_2515;

							BgL_vz00_2515 = CNST_TABLE_REF(8);
							((((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_gcloz00_16)))->
									BgL_removablez00) = ((obj_t) BgL_vz00_2515), BUNSPEC);
						}
						{	/* Globalize/gloclo.scm 231 */
							obj_t BgL_vz00_2517;

							BgL_vz00_2517 = CNST_TABLE_REF(2);
							((((BgL_globalz00_bglt) COBJECT(BgL_gcloz00_16))->BgL_importz00) =
								((obj_t) BgL_vz00_2517), BUNSPEC);
						}
					}
				else
					{	/* Globalize/gloclo.scm 227 */
						if (
							((((BgL_globalz00_bglt) COBJECT(BgL_globalz00_15))->
									BgL_importz00) == CNST_TABLE_REF(2)))
							{	/* Globalize/gloclo.scm 233 */
								BFALSE;
							}
						else
							{	/* Globalize/gloclo.scm 234 */
								obj_t BgL_vz00_2520;

								BgL_vz00_2520 = CNST_TABLE_REF(8);
								((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_gcloz00_16)))->
										BgL_removablez00) = ((obj_t) BgL_vz00_2520), BUNSPEC);
							}
						{	/* Globalize/gloclo.scm 235 */
							obj_t BgL_vz00_2522;

							BgL_vz00_2522 = CNST_TABLE_REF(2);
							((((BgL_globalz00_bglt) COBJECT(BgL_gcloz00_16))->BgL_importz00) =
								((obj_t) BgL_vz00_2522), BUNSPEC);
						}
					}
				{	/* Globalize/gloclo.scm 236 */
					BgL_valuez00_bglt BgL_arg1615z00_1886;
					obj_t BgL_arg1616z00_1887;

					BgL_arg1615z00_1886 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_gcloz00_16)))->BgL_valuez00);
					BgL_arg1616z00_1887 =
						(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_sfunz00_1876)))->BgL_sidezd2effectzd2);
					((((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_arg1615z00_1886)))->
							BgL_sidezd2effectzd2) = ((obj_t) BgL_arg1616z00_1887), BUNSPEC);
				}
				{	/* Globalize/gloclo.scm 237 */
					bool_t BgL_test1901z00_3542;

					{	/* Globalize/gloclo.scm 237 */
						obj_t BgL_classz00_2526;

						BgL_classz00_2526 = BGl_globalz00zzast_varz00;
						{	/* Globalize/gloclo.scm 237 */
							BgL_objectz00_bglt BgL_arg1807z00_2528;

							{	/* Globalize/gloclo.scm 237 */
								obj_t BgL_tmpz00_3543;

								BgL_tmpz00_3543 = ((obj_t) BgL_gcloz00_16);
								BgL_arg1807z00_2528 = (BgL_objectz00_bglt) (BgL_tmpz00_3543);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Globalize/gloclo.scm 237 */
									long BgL_idxz00_2534;

									BgL_idxz00_2534 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2528);
									BgL_test1901z00_3542 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2534 + 2L)) == BgL_classz00_2526);
								}
							else
								{	/* Globalize/gloclo.scm 237 */
									bool_t BgL_res1815z00_2559;

									{	/* Globalize/gloclo.scm 237 */
										obj_t BgL_oclassz00_2542;

										{	/* Globalize/gloclo.scm 237 */
											obj_t BgL_arg1815z00_2550;
											long BgL_arg1816z00_2551;

											BgL_arg1815z00_2550 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Globalize/gloclo.scm 237 */
												long BgL_arg1817z00_2552;

												BgL_arg1817z00_2552 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2528);
												BgL_arg1816z00_2551 =
													(BgL_arg1817z00_2552 - OBJECT_TYPE);
											}
											BgL_oclassz00_2542 =
												VECTOR_REF(BgL_arg1815z00_2550, BgL_arg1816z00_2551);
										}
										{	/* Globalize/gloclo.scm 237 */
											bool_t BgL__ortest_1115z00_2543;

											BgL__ortest_1115z00_2543 =
												(BgL_classz00_2526 == BgL_oclassz00_2542);
											if (BgL__ortest_1115z00_2543)
												{	/* Globalize/gloclo.scm 237 */
													BgL_res1815z00_2559 = BgL__ortest_1115z00_2543;
												}
											else
												{	/* Globalize/gloclo.scm 237 */
													long BgL_odepthz00_2544;

													{	/* Globalize/gloclo.scm 237 */
														obj_t BgL_arg1804z00_2545;

														BgL_arg1804z00_2545 = (BgL_oclassz00_2542);
														BgL_odepthz00_2544 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2545);
													}
													if ((2L < BgL_odepthz00_2544))
														{	/* Globalize/gloclo.scm 237 */
															obj_t BgL_arg1802z00_2547;

															{	/* Globalize/gloclo.scm 237 */
																obj_t BgL_arg1803z00_2548;

																BgL_arg1803z00_2548 = (BgL_oclassz00_2542);
																BgL_arg1802z00_2547 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2548,
																	2L);
															}
															BgL_res1815z00_2559 =
																(BgL_arg1802z00_2547 == BgL_classz00_2526);
														}
													else
														{	/* Globalize/gloclo.scm 237 */
															BgL_res1815z00_2559 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1901z00_3542 = BgL_res1815z00_2559;
								}
						}
					}
					if (BgL_test1901z00_3542)
						{	/* Globalize/gloclo.scm 237 */
							return ((obj_t) BgL_gcloz00_16);
						}
					else
						{	/* Globalize/gloclo.scm 237 */
							return
								BGl_internalzd2errorzd2zztools_errorz00
								(BGl_string1824z00zzglobaliza7e_globalzd2closurez75,
								BGl_string1825z00zzglobaliza7e_globalzd2closurez75,
								((obj_t) BgL_gcloz00_16));
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_globalzd2closurez75(void)
	{
		{	/* Globalize/gloclo.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(0L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_nodeza7(2706117L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_freeza7(244215788L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1826z00zzglobaliza7e_globalzd2closurez75));
		}

	}

#ifdef __cplusplus
}
#endif
