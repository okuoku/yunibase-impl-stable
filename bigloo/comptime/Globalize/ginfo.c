/*===========================================================================*/
/*   (Globalize/ginfo.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Globalize/ginfo.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_GLOBALIZE_GINFO_TYPE_DEFINITIONS
#define BGL_GLOBALIZE_GINFO_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_sfunzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		obj_t BgL_cfromz00;
		obj_t BgL_cfromza2za2;
		obj_t BgL_ctoz00;
		obj_t BgL_ctoza2za2;
		obj_t BgL_efunctionsz00;
		obj_t BgL_integratorz00;
		obj_t BgL_imarkz00;
		obj_t BgL_ownerz00;
		obj_t BgL_integratedz00;
		obj_t BgL_pluggedzd2inzd2;
		long BgL_markz00;
		obj_t BgL_freezd2markzd2;
		obj_t BgL_thezd2globalzd2;
		obj_t BgL_kapturedz00;
		obj_t BgL_newzd2bodyzd2;
		long BgL_bmarkz00;
		long BgL_umarkz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
	}                      *BgL_sfunzf2ginfozf2_bglt;

	typedef struct BgL_svarzf2ginfozf2_bgl
	{
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
		bool_t BgL_celledzf3zf3;
		bool_t BgL_stackablez00;
	}                      *BgL_svarzf2ginfozf2_bglt;

	typedef struct BgL_sexitzf2ginfozf2_bgl
	{
		bool_t BgL_gzf3zf3;
		bool_t BgL_kapturedzf3zf3;
		long BgL_freezd2markzd2;
		long BgL_markz00;
	}                       *BgL_sexitzf2ginfozf2_bglt;

	typedef struct BgL_localzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		bool_t BgL_globaliza7edzf3z54;
	}                       *BgL_localzf2ginfozf2_bglt;

	typedef struct BgL_globalzf2ginfozf2_bgl
	{
		bool_t BgL_escapezf3zf3;
		obj_t BgL_globalzd2closurezd2;
	}                        *BgL_globalzf2ginfozf2_bglt;


#endif													// BGL_GLOBALIZE_GINFO_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda1846z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1847z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2namez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	static obj_t
		BGl_z62localzf2Ginfozd2globaliza7edzf3z16zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1768z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1769z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1689z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static BgL_sexitz00_bglt
		BGl_z62makezd2sexitzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_localzf2Ginfozd2keyz20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2escapezf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, bool_t);
	static obj_t BGl_z62localzf2Ginfozd2keyz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1930z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1931z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1690z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31757ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31919ze3ze5zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static BgL_svarz00_bglt BGl_z62lambda1855z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1937z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2namezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62lambda1938z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static BgL_svarz00_bglt BGl_z62lambda1858z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static BgL_globalz00_bglt
		BGl_z62makezd2globalzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2kapturedzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2evalzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62sexitzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62localzf2Ginfozd2occurrencez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzglobaliza7e_ginfoza7 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2pragmazd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	static obj_t
		BGl_z62svarzf2Ginfozd2celledzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_svarzf2Ginfozd2celledzf3zd3zzglobaliza7e_ginfoza7(BgL_svarz00_bglt);
	static BgL_svarz00_bglt BGl_z62lambda1861z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31588ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1944z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1945z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2globalz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2dssslzd2keywordsz90zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sexitzf2Ginfozd2kapturedzf3zb1zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2loczd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2modulez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2srcz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2accessz20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2thezd2closurezd2globalzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt
		BGl_sfunzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2stackablezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2newzd2bodyzd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31880ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31953ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2globalzd2closurezd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2typezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, BgL_typez00_bglt);
	static obj_t BGl_z62lambda1951z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2Gzf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31848ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1871z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1952z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31597ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1872z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_localzf2Ginfozd2occurrencewz20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_globalzf2Ginfozd2valuez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static BgL_localz00_bglt
		BGl_z62makezd2localzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzf2Ginfozd2userzf3zd3zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static obj_t BGl_z62lambda1878z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2imarkz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1879z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1799z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2classzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32013ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2failsafez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2argszd2noescapezf2zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Ginfozd2handlerzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31873ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2newzd2bodyz90zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31946ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static BgL_localz00_bglt BGl_z62lambda1960z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda1963z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2occurrencewzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2fastzd2alphazd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static BgL_localz00_bglt BGl_z62lambda1966z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_sfunzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1886z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1887z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2occurrencezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, long);
	static obj_t BGl_z62sexitzf2Ginfozd2Gzf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2locz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32006ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2accesszd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzglobaliza7e_ginfoza7(void);
	static obj_t
		BGl_z62sfunzf2Ginfozd2propertyzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_sfunzf2Ginfozd2arityz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2valuezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, BgL_valuez00_bglt);
	static obj_t
		BGl_z62localzf2Ginfozd2userzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31939ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2bodyzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2ctoza2zd2setz12z20zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2importz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static obj_t BGl_z62lambda1893z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1974z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Ginfozd2Gzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt, bool_t);
	static obj_t BGl_z62lambda1894z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1975z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Ginfozd2detachedzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2sidezd2effectzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62localzf2Ginfozd2idz42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2ownerzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2propertyz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62globalzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2argszd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2dssslzd2keywordszf2zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1981z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1982z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_localzf2Ginfozd2typez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	static obj_t BGl_genericzd2initzd2zzglobaliza7e_ginfoza7(void);
	static obj_t BGl_z62localzf2Ginfozd2namez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62svarzf2Ginfozd2loczd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_globalzf2Ginfozd2occurrencez20zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt
		BGl_makezd2sfunzf2Ginfoz20zzglobaliza7e_ginfoza7(long, obj_t, obj_t, obj_t,
		bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, long, obj_t, obj_t,
		obj_t, obj_t, long, long, obj_t, obj_t);
	extern obj_t BGl_sexitz00zzast_varz00;
	static BgL_sfunz00_bglt
		BGl_z62makezd2sfunzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2Gzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2bodyzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2ctoza2zd2setz12z42zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2ownerz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31965ze3ze5zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2effectz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL long
		BGl_sfunzf2Ginfozd2bmarkz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_objectzd2initzd2zzglobaliza7e_ginfoza7(void);
	static BgL_globalz00_bglt BGl_z62lambda1990z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2stackablez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2typezd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static BgL_globalz00_bglt BGl_z62lambda1993z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static BgL_globalz00_bglt BGl_z62lambda1996z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2modulezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62sexitzf2Ginfozd2detachedzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2initzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7 = BUNSPEC;
	static obj_t BGl_z62globalzf2Ginfozd2aliasz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2thezd2closurezd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2argszd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2argszd2noescapezd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t
		BGl_z62sfunzf2Ginfozd2predicatezd2ofz90zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2sidezd2effectzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2aliaszd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2srcz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static BgL_svarz00_bglt
		BGl_z62svarzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2strengthzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2libraryzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2stackablez20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62sfunzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2thezd2closurezf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31983ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2occurrencewzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, long);
	static obj_t
		BGl_z62globalzf2Ginfozd2globalzd2closurezd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sexitzf2Ginfozd2handlerz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2fastzd2alphaz90zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2classz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Ginfozd2imarkz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_sexitzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt);
	static obj_t BGl_z62sfunzf2Ginfozd2argszd2namez90zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2cfromza2ze0zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2ownerzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2removablez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2userzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, bool_t);
	static obj_t BGl_z62zc3z04anonymousza31895ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31976ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_localzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void);
	BGL_EXPORTED_DECL bool_t
		BGl_sexitzf2Ginfozd2Gzf3zd3zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt);
	static BgL_localz00_bglt
		BGl_z62localzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2cfromza2zd2setz12z42zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2idz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static obj_t BGl_z62svarzf2Ginfozd2locz42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2integratedz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62globalzf2Ginfozd2idz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62svarzf2Ginfozd2stackablezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2arityz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2globalzd2closurezf2zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31888ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7 = BUNSPEC;
	static obj_t
		BGl_z62globalzf2Ginfozd2namezd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2namez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static obj_t BGl_methodzd2initzd2zzglobaliza7e_ginfoza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2failsafez20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2argszd2namezf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2idz20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2freez20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2newzd2bodyzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Ginfozd2freez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62sexitzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_sfunzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Ginfozd2markz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2typezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, BgL_typez00_bglt);
	static BgL_typez00_bglt
		BGl_z62localzf2Ginfozd2typez42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2thezd2globalzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2newzd2bodyzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2libraryzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62localzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31995ze3ze5zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2ownerz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2predicatezd2ofzd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2bmarkz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2efunctionszd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62svarzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2evaluablezf3zd2setz12z71zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2propertyz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2removablezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2thezd2closurezd2globalz20zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62sfunzf2Ginfozd2dssslzd2keywordszd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62localzf2Ginfozd2removablez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2strengthz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2fastzd2alphazf2zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Ginfozd2loczd2setz12ze0zzglobaliza7e_ginfoza7(BgL_svarz00_bglt,
		obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2occurrencewz42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2propertyzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62lambda2004z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda2005z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_svarz00_bglt
		BGl_svarzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Ginfozd2stackablezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_svarz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2globaliza7edzf3zd2setz12zb4zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sfunzf2Ginfozd2Gzf3zd3zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62globalzf2Ginfozd2evaluablezf3zb1zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2integratedz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2imarkzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2ctoza2z82zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_sexitz00_bglt
		BGl_sexitzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void);
	static obj_t BGl_z62sfunzf2Ginfozd2ctoza2ze0zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_globalzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2thezd2globalzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static BgL_sexitz00_bglt
		BGl_z62sexitzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL long
		BGl_svarzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(BgL_svarz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2accesszd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2classz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2efunctionszd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2efunctionsz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda2011z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda2012z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2evalzf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62sexitzf2Ginfozd2markz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2sidezd2effectz90zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2effectzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2bmarkzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		long);
	static obj_t
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_localzf2Ginfozd2globaliza7edzf3z74zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt);
	static obj_t
		BGl_z62localzf2Ginfozd2removablezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_svarz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2cfromzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_svarzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2failsafezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Ginfozd2locz20zzglobaliza7e_ginfoza7(BgL_svarz00_bglt);
	static obj_t
		BGl_z62sfunzf2Ginfozd2sidezd2effectzd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2jvmzd2typezd2namez20zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31612ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2thezd2globalz90zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Ginfozd2handlerz20zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt);
	static obj_t BGl_z62globalzf2Ginfozd2libraryz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2boundzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2pragmaz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2modulezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2evalzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, bool_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_globalzf2Ginfozd2typez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2boundz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Ginfozd2optionalsz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2thezd2globalzd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Ginfozd2celledzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_svarz00_bglt, bool_t);
	static obj_t BGl_z62sfunzf2Ginfozd2kapturedz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2initzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2ctoz42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_valuez00_bglt
		BGl_localzf2Ginfozd2valuez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	static obj_t BGl_z62globalzf2Ginfozd2accessz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_localzf2Ginfozd2userzf3zd3zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	static obj_t
		BGl_z62globalzf2Ginfozd2fastzd2alphazd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2occurrencezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_svarz00_bglt
		BGl_makezd2svarzf2Ginfoz20zzglobaliza7e_ginfoza7(obj_t, bool_t, long, long,
		bool_t, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2stackzd2allocatorzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static BgL_svarz00_bglt
		BGl_z62makezd2svarzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2stackzd2allocatorzd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2jvmzd2typezd2namez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2initz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2libraryz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static obj_t
		BGl_z62globalzf2Ginfozd2importzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62svarzf2Ginfozd2stackablez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t
		BGl_z62sfunzf2Ginfozd2thezd2closurez90zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2removablez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static obj_t BGl_z62sfunzf2Ginfozd2effectz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2imarkzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2occurrencewz42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2globalzd2closurez90zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2jvmzd2typezd2namezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2optionalsz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62globalzf2Ginfozd2jvmzd2typezd2namezd2setz12z82zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_sfunzf2Ginfozd2umarkz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31801ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sexitzf2Ginfozd2detachedzf3zd3zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2argszd2noescapezd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31704ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2efunctionsz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2dssslzd2keywordszd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2bmarkzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62svarzf2Ginfozd2celledzf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2cfromzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2Gzf3zd2setz12z13zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		bool_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2cfromza2zd2setz12z20zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31713ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt, long);
	static obj_t BGl_z62localzf2Ginfozd2accessz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2globalzd2setz12z82zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_svarzf2Ginfozd2stackablez20zzglobaliza7e_ginfoza7(BgL_svarz00_bglt);
	static obj_t
		BGl_z62svarzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2removablezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2valuezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_makezd2globalzf2Ginfoz20zzglobaliza7e_ginfoza7(obj_t, obj_t,
		BgL_typez00_bglt, BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long,
		bool_t, obj_t, obj_t, bool_t, bool_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, bool_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2cfromz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static BgL_valuez00_bglt
		BGl_z62globalzf2Ginfozd2valuez42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2boundzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2userzf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62svarzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2keysz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static BgL_globalz00_bglt
		BGl_z62globalzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2aliasz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2integratorz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	extern obj_t BGl_localz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2integratedzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2keysz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62localzf2Ginfozd2escapezf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62svarzf2Ginfozd2kapturedzf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2ctozd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2valuezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, BgL_valuez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2occurrencewzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Ginfozd2kapturedzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt, bool_t);
	BGL_EXPORTED_DECL long
		BGl_sexitzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2pluggedzd2inzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_cnstzd2initzd2zzglobaliza7e_ginfoza7(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2strengthz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_ginfoza7(void);
	static obj_t
		BGl_z62globalzf2Ginfozd2typezd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2escapezf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62svarzf2Ginfozd2kapturedzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_ginfoza7(void);
	static obj_t
		BGl_z62globalzf2Ginfozd2occurrencez42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2stackzd2allocatorzf2zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt);
	static obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_ginfoza7(void);
	static obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_svarz00_bglt,
		long);
	static obj_t BGl_z62zc3z04anonymousza31545ze3ze5zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1517z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2fastzd2alphazd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, obj_t);
	static obj_t
		BGl_z62sexitzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2occurrencezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, long);
	BGL_EXPORTED_DECL long
		BGl_localzf2Ginfozd2occurrencez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	static obj_t BGl_z62globalzf2Ginfozd2namez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2boundz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_svarzf2Ginfozd2kapturedzf3zd3zzglobaliza7e_ginfoza7(BgL_svarz00_bglt);
	static obj_t
		BGl_z62sfunzf2Ginfozd2kapturedzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31902ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2escapezf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2ctoz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62sfunzf2Ginfozd2integratedzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62sexitzf2Ginfozd2detachedzf3zb1zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2namezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, obj_t);
	BGL_EXPORTED_DECL long
		BGl_svarzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(BgL_svarz00_bglt);
	static obj_t
		BGl_z62globalzf2Ginfozd2pragmazd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62sexitzf2Ginfozd2kapturedzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62svarzf2Ginfozd2markz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2predicatezd2ofzf2zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL long
		BGl_globalzf2Ginfozd2occurrencewz20zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt);
	static obj_t BGl_z62svarzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2aliaszd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2integratorzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62lambda1610z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1611z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31628ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2stackzd2allocatorz90zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2escapezf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2pragmaz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2bodyz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Ginfozd2bodyz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_svarz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2kapturedz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31750ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2integratorz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1702z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2umarkz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2pluggedzd2inzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62lambda1703z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31815ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1541z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2accessz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static obj_t BGl_z62lambda1626z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1627z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1546z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2effectzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2argszd2noescapez90zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sexitzf2Ginfozd2handlerzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sexitz00_bglt
		BGl_makezd2sexitzf2Ginfoz20zzglobaliza7e_ginfoza7(obj_t, bool_t, bool_t,
		bool_t, long, long);
	static obj_t
		BGl_z62sexitzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2topzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2pluggedzd2inz90zzglobaliza7e_ginfoza7(obj_t, obj_t);
	extern obj_t BGl_valuez00zzast_varz00;
	BGL_EXPORTED_DECL bool_t
		BGl_sfunzf2Ginfozd2topzf3zd3zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62globalzf2Ginfozd2userzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2failsafezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2topzf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Ginfozd2kapturedzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_svarz00_bglt, bool_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2loczd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2integratorzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2modulez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzf2Ginfozd2escapezf3zd3zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	static obj_t BGl_z62lambda1711z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1712z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2accesszd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2userzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2srczd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2umarkzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		long);
	static obj_t BGl_z62sfunzf2Ginfozd2cfromz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2escapezf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, bool_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2freezd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1800z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1721z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1722z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31574ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2pluggedzd2inzd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2topzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, bool_t);
	static obj_t BGl_z62lambda1647z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1648z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2namezd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31834ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31664ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1813z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static BgL_sfunz00_bglt
		BGl_z62sfunzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1814z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2importzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, obj_t);
	static obj_t BGl_z62lambda1572z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2globaliza7edzf3zd2setz12zd6zzglobaliza7e_ginfoza7
		(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1573z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1736z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_localz00_bglt
		BGl_makezd2localzf2Ginfoz20zzglobaliza7e_ginfoza7(obj_t, obj_t,
		BgL_typez00_bglt, BgL_valuez00_bglt, obj_t, obj_t, obj_t, long, long,
		bool_t, long, bool_t, bool_t);
	static obj_t BGl_z62lambda1737z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2removablezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2occurrencewzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2importz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt, long);
	static obj_t
		BGl_z62sexitzf2Ginfozd2Gzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2removablez42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzf2Ginfozd2evalzf3zd3zzglobaliza7e_ginfoza7(BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2freezd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62globalzf2Ginfozd2typez42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_localzf2Ginfozd2escapezf3zd3zzglobaliza7e_ginfoza7(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2ctozd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2cfromza2z82zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_sexitzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1900z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31770ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1901z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31932ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1662z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31738ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31649ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1663z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1586z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62lambda1748z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sexitzf2Ginfozd2kapturedzf3zd3zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt);
	static obj_t BGl_z62lambda1587z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1749z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static BgL_valuez00_bglt
		BGl_z62localzf2Ginfozd2valuez42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62localzf2Ginfozd2userzf3zb1zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2fastzd2alphaz90zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_localzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2locz42zzglobaliza7e_ginfoza7(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_globalzf2Ginfozd2evaluablezf3zd3zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza31860ze3ze5zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1832z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62lambda1913z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2stackablezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1833z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2predicatezd2ofzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2argsz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1755z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62lambda1917z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1756z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62globalzf2Ginfozd2initz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2strengthzd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1595z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2fastzd2alphazd2setz12z50zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzf2Ginfozd2fastzd2alphazf2zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Ginfozd2classzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda1596z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1839z62zzglobaliza7e_ginfoza7(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Ginfozd2argsz42zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7 = BUNSPEC;
	static obj_t
		BGl_z62localzf2Ginfozd2occurrencezd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62globalzf2Ginfozd2srczd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_globalzf2Ginfozd2evaluablezf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt, bool_t);
	static obj_t
		BGl_z62sfunzf2Ginfozd2umarkzd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2accesszd2setz12z82zzglobaliza7e_ginfoza7(obj_t,
		obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62lambda1920z62zzglobaliza7e_ginfoza7(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31691ze3ze5zzglobaliza7e_ginfoza7(obj_t);
	static obj_t BGl_z62lambda1840z62zzglobaliza7e_ginfoza7(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62localzf2Ginfozd2valuezd2setz12z82zzglobaliza7e_ginfoza7(obj_t, obj_t,
		obj_t);
	static obj_t __cnst[36];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2112z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2205za7,
		BGl_z62zc3z04anonymousza31723ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2113z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1722za7622206z00,
		BGl_z62lambda1722z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2201z00zzglobaliza7e_ginfoza7,
		BgL_bgl_string2201za700za7za7g2207za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2114z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1721za7622208z00,
		BGl_z62lambda1721z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2202z00zzglobaliza7e_ginfoza7,
		BgL_bgl_string2202za700za7za7g2209za7, "globalize_ginfo", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2115z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2210za7,
		BGl_z62zc3z04anonymousza31738ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2detachedzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2211z00,
		BGl_z62sexitzf2Ginfozd2detachedzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2203z00zzglobaliza7e_ginfoza7,
		BgL_bgl_string2203za700za7za7g2212za7,
		"_ global/Ginfo global-closure local/Ginfo globalized? escape? sexit/Ginfo svar/Ginfo stackable celled? kaptured? globalize_ginfo sfun/Ginfo bound free umark bmark new-body kaptured the-global free-mark long mark plugged-in integrated owner imark integrator efunctions cto* cto cfrom* obj cfrom bool G? ",
		302);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2116z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1737za7622213z00,
		BGl_z62lambda1737z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2117z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1736za7622214z00,
		BGl_z62lambda1736z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2118z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2215za7,
		BGl_z62zc3z04anonymousza31750ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2119z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1749za7622216z00,
		BGl_z62lambda1749z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2accesszd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2217z00,
		BGl_z62globalzf2Ginfozd2accessz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2sexitzf2Ginfozd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762makeza7d2sexitza72218za7,
		BGl_z62makezd2sexitzf2Ginfoz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2200z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1990za7622219z00,
		BGl_z62lambda1990z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2120z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1748za7622220z00,
		BGl_z62lambda1748z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2121z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2221za7,
		BGl_z62zc3z04anonymousza31757ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2122z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1756za7622222z00,
		BGl_z62lambda1756z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1755za7622223z00,
		BGl_z62lambda1755z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2cfromza2zd2setz12zd2envz90zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72224za7,
		BGl_z62sfunzf2Ginfozd2cfromza2zd2setz12z20zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2225za7,
		BGl_z62zc3z04anonymousza31770ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1769za7622226z00,
		BGl_z62lambda1769z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2srczd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2227z00,
		BGl_z62globalzf2Ginfozd2srcz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1768za7622228z00,
		BGl_z62lambda1768z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2229za7,
		BGl_z62zc3z04anonymousza31801ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1800za7622230z00,
		BGl_z62lambda1800z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1799za7622231z00,
		BGl_z62lambda1799z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2userzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2232z00,
		BGl_z62localzf2Ginfozd2userzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2initzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2233z00,
		BGl_z62globalzf2Ginfozd2initz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2srczd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2234z00,
		BGl_z62globalzf2Ginfozd2srczd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2occurrencezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2235z00,
		BGl_z62localzf2Ginfozd2occurrencez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2236za7,
		BGl_z62zc3z04anonymousza31815ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2jvmzd2typezd2namezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2237z00,
		BGl_z62globalzf2Ginfozd2jvmzd2typezd2namezd2setz12z82zzglobaliza7e_ginfoza7,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2optionalszd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72238za7,
		BGl_z62sfunzf2Ginfozd2optionalsz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1814za7622239z00,
		BGl_z62lambda1814z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2freezd2markzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72240za7,
		BGl_z62svarzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1813za7622241z00,
		BGl_z62lambda1813z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2failsafezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72242za7,
		BGl_z62sfunzf2Ginfozd2failsafez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2argszd2namezd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72243za7,
		BGl_z62sfunzf2Ginfozd2argszd2namez90zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2244za7,
		BGl_z62zc3z04anonymousza31834ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2134z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1833za7622245z00,
		BGl_z62lambda1833z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1832za7622246z00,
		BGl_z62lambda1832z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2136z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2247za7,
		BGl_z62zc3z04anonymousza31841ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2escapezf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2248z00,
		BGl_z62globalzf2Ginfozd2escapezf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2137z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1840za7622249z00,
		BGl_z62lambda1840z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2138z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1839za7622250z00,
		BGl_z62lambda1839z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2139z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2251za7,
		BGl_z62zc3z04anonymousza31848ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2markzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72252za7,
		BGl_z62sfunzf2Ginfozd2markz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozf3zd2envzd3zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72253za7,
		BGl_z62svarzf2Ginfozf3z63zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2140z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1847za7622254z00,
		BGl_z62lambda1847z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2141z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1846za7622255z00,
		BGl_z62lambda1846z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2142z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1546za7622256z00,
		BGl_z62lambda1546z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2143z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2257za7,
		BGl_z62zc3z04anonymousza31545ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1541za7622258z00,
		BGl_z62lambda1541z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2145z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1517za7622259z00,
		BGl_z62lambda1517z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 42);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2260za7,
		BGl_z62zc3z04anonymousza31873ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1872za7622261z00,
		BGl_z62lambda1872z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1871za7622262z00,
		BGl_z62lambda1871z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2263za7,
		BGl_z62zc3z04anonymousza31880ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2boundzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72264za7,
		BGl_z62sfunzf2Ginfozd2boundzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1879za7622265z00,
		BGl_z62lambda1879z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1878za7622266z00,
		BGl_z62lambda1878z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2markzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2267z00,
		BGl_z62sexitzf2Ginfozd2markz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2268za7,
		BGl_z62zc3z04anonymousza31888ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2153z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1887za7622269z00,
		BGl_z62lambda1887z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1886za7622270z00,
		BGl_z62lambda1886z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2escapezf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2271z00,
		BGl_z62localzf2Ginfozd2escapezf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2272za7,
		BGl_z62zc3z04anonymousza31895ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2kapturedzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72273za7,
		BGl_z62sfunzf2Ginfozd2kapturedz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1894za7622274z00,
		BGl_z62lambda1894z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2integratedzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72275za7,
		BGl_z62sfunzf2Ginfozd2integratedzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1893za7622276z00,
		BGl_z62lambda1893z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2celledzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72277za7,
		BGl_z62svarzf2Ginfozd2celledzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2278za7,
		BGl_z62zc3z04anonymousza31902ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1901za7622279z00,
		BGl_z62lambda1901z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2ownerzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72280za7,
		BGl_z62sfunzf2Ginfozd2ownerzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2occurrencewzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2281z00,
		BGl_z62localzf2Ginfozd2occurrencewzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2removablezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2282z00,
		BGl_z62localzf2Ginfozd2removablez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1900za7622283z00,
		BGl_z62lambda1900z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1861za7622284z00,
		BGl_z62lambda1861z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2162z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2285za7,
		BGl_z62zc3z04anonymousza31860ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1858za7622286z00,
		BGl_z62lambda1858z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2082z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2287za7,
		BGl_z62zc3z04anonymousza31574ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1855za7622288z00,
		BGl_z62lambda1855z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2083z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1573za7622289z00,
		BGl_z62lambda1573z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2165z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2290za7,
		BGl_z62zc3z04anonymousza31932ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2084z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1572za7622291z00,
		BGl_z62lambda1572z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1931za7622292z00,
		BGl_z62lambda1931z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2085z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2293za7,
		BGl_z62zc3z04anonymousza31588ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1930za7622294z00,
		BGl_z62lambda1930z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2086z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1587za7622295z00,
		BGl_z62lambda1587z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2nilzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72296za7,
		BGl_z62svarzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2297za7,
		BGl_z62zc3z04anonymousza31939ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2087z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1586za7622298z00,
		BGl_z62lambda1586z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1938za7622299z00,
		BGl_z62lambda1938z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2088z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2300za7,
		BGl_z62zc3z04anonymousza31597ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2089z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1596za7622301z00,
		BGl_z62lambda1596z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2libraryzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2302z00,
		BGl_z62globalzf2Ginfozd2libraryz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2effectzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72303za7,
		BGl_z62sfunzf2Ginfozd2effectz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2bmarkzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72304za7,
		BGl_z62sfunzf2Ginfozd2bmarkz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2removablezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2305z00,
		BGl_z62globalzf2Ginfozd2removablez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2accesszd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2306z00,
		BGl_z62localzf2Ginfozd2accesszd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2idzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2307z00,
		BGl_z62localzf2Ginfozd2idz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1937za7622308z00,
		BGl_z62lambda1937z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2309za7,
		BGl_z62zc3z04anonymousza31946ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2090z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1595za7622310z00,
		BGl_z62lambda1595z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2172z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1945za7622311z00,
		BGl_z62lambda1945z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2091z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2312za7,
		BGl_z62zc3z04anonymousza31612ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2evaluablezf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2313z00,
		BGl_z62globalzf2Ginfozd2evaluablezf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2173z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1944za7622314z00,
		BGl_z62lambda1944z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2092z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1611za7622315z00,
		BGl_z62lambda1611z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2174z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2316za7,
		BGl_z62zc3z04anonymousza31953ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2093z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1610za7622317z00,
		BGl_z62lambda1610z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2175z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1952za7622318z00,
		BGl_z62lambda1952z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2094z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2319za7,
		BGl_z62zc3z04anonymousza31628ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2detachedzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2320z00,
		BGl_z62sexitzf2Ginfozd2detachedzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2argszd2noescapezd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72321za7,
		BGl_z62sfunzf2Ginfozd2argszd2noescapezd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2176z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1951za7622322z00,
		BGl_z62lambda1951z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2095z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1627za7622323z00,
		BGl_z62lambda1627z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2177z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1920za7622324z00,
		BGl_z62lambda1920z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2096z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1626za7622325z00,
		BGl_z62lambda1626z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2178z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2326za7,
		BGl_z62zc3z04anonymousza31919ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2097z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2327za7,
		BGl_z62zc3z04anonymousza31649ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2thezd2closurezd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72328za7,
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2179z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1917za7622329z00,
		BGl_z62lambda1917z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2098z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1648za7622330z00,
		BGl_z62lambda1648z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2099z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1647za7622331z00,
		BGl_z62lambda1647z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2globaliza7edzf3zd2envza6zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2332z00,
		BGl_z62localzf2Ginfozd2globaliza7edzf3z16zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2aliaszd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2333z00,
		BGl_z62globalzf2Ginfozd2aliaszd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2integratorzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72334za7,
		BGl_z62sfunzf2Ginfozd2integratorzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2180z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1913za7622335z00,
		BGl_z62lambda1913z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2loczd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72336za7,
		BGl_z62sfunzf2Ginfozd2locz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2181z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2337za7,
		BGl_z62zc3z04anonymousza31976ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2182z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1975za7622338z00,
		BGl_z62lambda1975z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2183z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1974za7622339z00,
		BGl_z62lambda1974z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2184z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2340za7,
		BGl_z62zc3z04anonymousza31983ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2185z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1982za7622341z00,
		BGl_z62lambda1982z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2186z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1981za7622342z00,
		BGl_z62lambda1981z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2libraryzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2343z00,
		BGl_z62globalzf2Ginfozd2libraryzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2187z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1966za7622344z00,
		BGl_z62lambda1966z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2188z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2345za7,
		BGl_z62zc3z04anonymousza31965ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2189z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1963za7622346z00,
		BGl_z62lambda1963z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2stackablezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72347za7,
		BGl_z62svarzf2Ginfozd2stackablez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2freezd2markzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72348za7,
		BGl_z62sfunzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2190z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1960za7622349z00,
		BGl_z62lambda1960z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2191z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2350za7,
		BGl_z62zc3z04anonymousza32006ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2ctozd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72351za7,
		BGl_z62sfunzf2Ginfozd2ctozd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2192z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda2005za7622352z00,
		BGl_z62lambda2005z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2193z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda2004za7622353z00,
		BGl_z62lambda2004z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2194z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2354za7,
		BGl_z62zc3z04anonymousza32013ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2195z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda2012za7622355z00,
		BGl_z62lambda2012z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2196z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda2011za7622356z00,
		BGl_z62lambda2011z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2197z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1996za7622357z00,
		BGl_z62lambda1996z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2198z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2358za7,
		BGl_z62zc3z04anonymousza31995ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2pragmazd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2359z00,
		BGl_z62globalzf2Ginfozd2pragmazd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2199z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1993za7622360z00,
		BGl_z62lambda1993z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2kapturedzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2361z00,
		BGl_z62sexitzf2Ginfozd2kapturedzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2userzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2362z00,
		BGl_z62globalzf2Ginfozd2userzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2dssslzd2keywordszd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72363za7,
		BGl_z62sfunzf2Ginfozd2dssslzd2keywordsz90zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2kapturedzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2364z00,
		BGl_z62sexitzf2Ginfozd2kapturedzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2kapturedzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72365za7,
		BGl_z62svarzf2Ginfozd2kapturedzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2occurrencezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2366z00,
		BGl_z62globalzf2Ginfozd2occurrencez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2sfunzf2Ginfozd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762makeza7d2sfunza7f2367za7,
		BGl_z62makezd2sfunzf2Ginfoz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 41);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2stackablezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72368za7,
		BGl_z62svarzf2Ginfozd2stackablezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2markzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72369za7,
		BGl_z62sfunzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2fastzd2alphazd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2370z00,
		BGl_z62localzf2Ginfozd2fastzd2alphazd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2occurrencewzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2371z00,
		BGl_z62localzf2Ginfozd2occurrencewz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2fastzd2alphazd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2372z00,
		BGl_z62localzf2Ginfozd2fastzd2alphaz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2classzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72373za7,
		BGl_z62sfunzf2Ginfozd2classzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2occurrencezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2374z00,
		BGl_z62localzf2Ginfozd2occurrencezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2nilzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2375z00,
		BGl_z62sexitzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2newzd2bodyzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72376za7,
		BGl_z62sfunzf2Ginfozd2newzd2bodyz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2namezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2377z00,
		BGl_z62localzf2Ginfozd2namezd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2namezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2378z00,
		BGl_z62localzf2Ginfozd2namez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2keyzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2379z00,
		BGl_z62localzf2Ginfozd2keyz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2namezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2380z00,
		BGl_z62globalzf2Ginfozd2namezd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2globalzf2Ginfozd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762makeza7d2global2381z00,
		BGl_z62makezd2globalzf2Ginfoz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 22);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2classzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72382za7,
		BGl_z62sfunzf2Ginfozd2classz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2sidezd2effectzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72383za7,
		BGl_z62sfunzf2Ginfozd2sidezd2effectz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2cfromza2zd2envz50zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72384za7,
		BGl_z62sfunzf2Ginfozd2cfromza2ze0zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2propertyzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72385za7,
		BGl_z62sfunzf2Ginfozd2propertyz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2celledzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72386za7,
		BGl_z62svarzf2Ginfozd2celledzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2freezd2markzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2387z00,
		BGl_z62sexitzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2valuezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2388z00,
		BGl_z62localzf2Ginfozd2valuez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2topzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72389za7,
		BGl_z62sfunzf2Ginfozd2topzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2loczd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72390za7,
		BGl_z62sfunzf2Ginfozd2loczd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozf3zd2envzd3zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2391z00,
		BGl_z62localzf2Ginfozf3z63zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2globaliza7edzf3zd2setz12zd2envz66zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2392z00,
		BGl_z62localzf2Ginfozd2globaliza7edzf3zd2setz12zd6zzglobaliza7e_ginfoza7,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2namezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2393z00,
		BGl_z62globalzf2Ginfozd2namez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2umarkzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72394za7,
		BGl_z62sfunzf2Ginfozd2umarkzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2argszd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72395za7,
		BGl_z62sfunzf2Ginfozd2argszd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2argszd2noescapezd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72396za7,
		BGl_z62sfunzf2Ginfozd2argszd2noescapez90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2markzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72397za7,
		BGl_z62svarzf2Ginfozd2markz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2fastzd2alphazd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2398z00,
		BGl_z62globalzf2Ginfozd2fastzd2alphazd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2predicatezd2ofzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72399za7,
		BGl_z62sfunzf2Ginfozd2predicatezd2ofz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2occurrencezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2400z00,
		BGl_z62globalzf2Ginfozd2occurrencezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2escapezf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2401z00,
		BGl_z62localzf2Ginfozd2escapezf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2importzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2402z00,
		BGl_z62globalzf2Ginfozd2importzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2removablezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2403z00,
		BGl_z62globalzf2Ginfozd2removablezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2occurrencewzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2404z00,
		BGl_z62globalzf2Ginfozd2occurrencewzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2importzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2405z00,
		BGl_z62globalzf2Ginfozd2importz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2modulezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2406z00,
		BGl_z62globalzf2Ginfozd2modulezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2keyszd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72407za7,
		BGl_z62sfunzf2Ginfozd2keysz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2removablezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2408z00,
		BGl_z62localzf2Ginfozd2removablezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2pluggedzd2inzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72409za7,
		BGl_z62sfunzf2Ginfozd2pluggedzd2inz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2userzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2410z00,
		BGl_z62localzf2Ginfozd2userzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2fastzd2alphazd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2411z00,
		BGl_z62globalzf2Ginfozd2fastzd2alphaz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2Gzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2412z00,
		BGl_z62sexitzf2Ginfozd2Gzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2accesszd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2413z00,
		BGl_z62localzf2Ginfozd2accessz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2stackzd2allocatorzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72414za7,
		BGl_z62sfunzf2Ginfozd2stackzd2allocatorz90zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2Gzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72415za7,
		BGl_z62sfunzf2Ginfozd2Gzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2integratedzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72416za7,
		BGl_z62sfunzf2Ginfozd2integratedz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2bodyzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72417za7,
		BGl_z62sfunzf2Ginfozd2bodyzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2imarkzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72418za7,
		BGl_z62sfunzf2Ginfozd2imarkzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2efunctionszd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72419za7,
		BGl_z62sfunzf2Ginfozd2efunctionsz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2handlerzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2420z00,
		BGl_z62sexitzf2Ginfozd2handlerzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2loczd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72421za7,
		BGl_z62svarzf2Ginfozd2locz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2thezd2globalzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72422za7,
		BGl_z62sfunzf2Ginfozd2thezd2globalz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2newzd2bodyzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72423za7,
		BGl_z62sfunzf2Ginfozd2newzd2bodyzd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2accesszd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2424z00,
		BGl_z62globalzf2Ginfozd2accesszd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2freezd2markzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2425z00,
		BGl_z62sexitzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2arityzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72426za7,
		BGl_z62sfunzf2Ginfozd2arityz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2Gzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2427z00,
		BGl_z62sexitzf2Ginfozd2Gzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2freezd2markzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72428za7,
		BGl_z62svarzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2evalzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2429z00,
		BGl_z62globalzf2Ginfozd2evalzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2kapturedzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72430za7,
		BGl_z62sfunzf2Ginfozd2kapturedzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozf3zd2envzd3zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2431z00,
		BGl_z62sexitzf2Ginfozf3z63zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2typezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2432z00,
		BGl_z62localzf2Ginfozd2typez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2svarzf2Ginfozd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762makeza7d2svarza7f2433za7,
		BGl_z62makezd2svarzf2Ginfoz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2stackzd2allocatorzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72434za7,
		BGl_z62sfunzf2Ginfozd2stackzd2allocatorzd2setz12z50zzglobaliza7e_ginfoza7,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2propertyzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72435za7,
		BGl_z62sfunzf2Ginfozd2propertyzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2markzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72436za7,
		BGl_z62svarzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2globalzd2closurezd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2437z00,
		BGl_z62globalzf2Ginfozd2globalzd2closurezd2setz12z50zzglobaliza7e_ginfoza7,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2globalzd2closurezd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2438z00,
		BGl_z62globalzf2Ginfozd2globalzd2closurez90zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2valuezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2439z00,
		BGl_z62globalzf2Ginfozd2valuez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2freezd2markzd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72440za7,
		BGl_z62sfunzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2effectzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72441za7,
		BGl_z62sfunzf2Ginfozd2effectzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2bmarkzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72442za7,
		BGl_z62sfunzf2Ginfozd2bmarkzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2topzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72443za7,
		BGl_z62sfunzf2Ginfozd2topzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozf3zd2envzd3zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72444za7,
		BGl_z62sfunzf2Ginfozf3z63zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2occurrencewzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2445z00,
		BGl_z62globalzf2Ginfozd2occurrencewz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2sidezd2effectzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72446za7,
		BGl_z62sfunzf2Ginfozd2sidezd2effectzd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2jvmzd2typezd2namezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2447z00,
		BGl_z62globalzf2Ginfozd2jvmzd2typezd2namez42zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2strengthzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72448za7,
		BGl_z62sfunzf2Ginfozd2strengthzd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2pragmazd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2449z00,
		BGl_z62globalzf2Ginfozd2pragmaz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2handlerzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2450z00,
		BGl_z62sexitzf2Ginfozd2handlerz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2typezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2451z00,
		BGl_z62globalzf2Ginfozd2typez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2efunctionszd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72452za7,
		BGl_z62sfunzf2Ginfozd2efunctionszd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2evalzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2453z00,
		BGl_z62globalzf2Ginfozd2evalzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2freezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72454za7,
		BGl_z62sfunzf2Ginfozd2freezd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2valuezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2455z00,
		BGl_z62localzf2Ginfozd2valuezd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozf3zd2envzd3zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2456z00,
		BGl_z62globalzf2Ginfozf3z63zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2failsafezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72457za7,
		BGl_z62sfunzf2Ginfozd2failsafezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2loczd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72458za7,
		BGl_z62svarzf2Ginfozd2loczd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2modulezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2459z00,
		BGl_z62globalzf2Ginfozd2modulez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2thezd2closurezd2globalzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72460za7,
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2globalzd2setz12z82zzglobaliza7e_ginfoza7,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2nilzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72461za7,
		BGl_z62sfunzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2escapezf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2462z00,
		BGl_z62globalzf2Ginfozd2escapezf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2ctoza2zd2setz12zd2envz90zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72463za7,
		BGl_z62sfunzf2Ginfozd2ctoza2zd2setz12z20zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2typezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2464z00,
		BGl_z62localzf2Ginfozd2typezd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2ctoza2zd2envz50zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72465za7,
		BGl_z62sfunzf2Ginfozd2ctoza2ze0zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2userzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2466z00,
		BGl_z62globalzf2Ginfozd2userzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2nilzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2467z00,
		BGl_z62globalzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2ctozd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72468za7,
		BGl_z62sfunzf2Ginfozd2ctoz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2typezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2469z00,
		BGl_z62globalzf2Ginfozd2typezd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2stackablezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72470za7,
		BGl_z62sfunzf2Ginfozd2stackablez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2localzf2Ginfozd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762makeza7d2localza72471za7,
		BGl_z62makezd2localzf2Ginfoz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2bodyzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72472za7,
		BGl_z62sfunzf2Ginfozd2bodyz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2cfromzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72473za7,
		BGl_z62sfunzf2Ginfozd2cfromz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2boundzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72474za7,
		BGl_z62sfunzf2Ginfozd2boundz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2aliaszd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2475z00,
		BGl_z62globalzf2Ginfozd2aliasz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2integratorzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72476za7,
		BGl_z62sfunzf2Ginfozd2integratorz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2pluggedzd2inzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72477za7,
		BGl_z62sfunzf2Ginfozd2pluggedzd2inzd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2thezd2closurezd2globalzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72478za7,
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2globalz42zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2strengthzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72479za7,
		BGl_z62sfunzf2Ginfozd2strengthz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzf2Ginfozd2nilzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762localza7f2ginfo2480z00,
		BGl_z62localzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2idzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2481z00,
		BGl_z62globalzf2Ginfozd2idz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2stackablezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72482za7,
		BGl_z62sfunzf2Ginfozd2stackablezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2evaluablezf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2483z00,
		BGl_z62globalzf2Ginfozd2evaluablezf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2ownerzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72484za7,
		BGl_z62sfunzf2Ginfozd2ownerz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2predicatezd2ofzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72485za7,
		BGl_z62sfunzf2Ginfozd2predicatezd2ofzd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2argszd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72486za7,
		BGl_z62sfunzf2Ginfozd2argsz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2imarkzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72487za7,
		BGl_z62sfunzf2Ginfozd2imarkz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2thezd2globalzd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72488za7,
		BGl_z62sfunzf2Ginfozd2thezd2globalzd2setz12z50zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Ginfozd2kapturedzf3zd2envz01zzglobaliza7e_ginfoza7,
		BgL_bgl_za762svarza7f2ginfoza72489za7,
		BGl_z62svarzf2Ginfozd2kapturedzf3zb1zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2umarkzd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72490za7,
		BGl_z62sfunzf2Ginfozd2umarkz42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2freezd2envzf2zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72491za7,
		BGl_z62sfunzf2Ginfozd2freez42zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2initzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2492z00,
		BGl_z62globalzf2Ginfozd2initzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2dssslzd2keywordszd2setz12zd2envze0zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72493za7,
		BGl_z62sfunzf2Ginfozd2dssslzd2keywordszd2setz12z50zzglobaliza7e_ginfoza7,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2cfromzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72494za7,
		BGl_z62sfunzf2Ginfozd2cfromzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2100z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2495za7,
		BGl_z62zc3z04anonymousza31664ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2101z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1663za7622496z00,
		BGl_z62lambda1663z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2102z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1662za7622497z00,
		BGl_z62lambda1662z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2103z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2498za7,
		BGl_z62zc3z04anonymousza31691ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2104z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1690za7622499z00,
		BGl_z62lambda1690z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2105z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1689za7622500z00,
		BGl_z62lambda1689z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2106z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2501za7,
		BGl_z62zc3z04anonymousza31704ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2107z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1703za7622502z00,
		BGl_z62lambda1703z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2108z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1702za7622503z00,
		BGl_z62lambda1702z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2109z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762za7c3za704anonymo2504za7,
		BGl_z62zc3z04anonymousza31713ze3ze5zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2Gzf3zd2setz12zd2envzc1zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72505za7,
		BGl_z62sfunzf2Ginfozd2Gzf3zd2setz12z71zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Ginfozd2thezd2closurezd2envz20zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sfunza7f2ginfoza72506za7,
		BGl_z62sfunzf2Ginfozd2thezd2closurez90zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_globalzf2Ginfozd2valuezd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762globalza7f2ginf2507z00,
		BGl_z62globalzf2Ginfozd2valuezd2setz12z82zzglobaliza7e_ginfoza7, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Ginfozd2markzd2setz12zd2envz32zzglobaliza7e_ginfoza7,
		BgL_bgl_za762sexitza7f2ginfo2508z00,
		BGl_z62sexitzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2110z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1712za7622509z00,
		BGl_z62lambda1712z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2111z00zzglobaliza7e_ginfoza7,
		BgL_bgl_za762lambda1711za7622510z00,
		BGl_z62lambda1711z62zzglobaliza7e_ginfoza7, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzglobaliza7e_ginfoza7));
		     ADD_ROOT((void *) (&BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7));
		     ADD_ROOT((void *) (&BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7));
		     ADD_ROOT((void *) (&BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7));
		     ADD_ROOT((void *) (&BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7));
		     ADD_ROOT((void *) (&BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzglobaliza7e_ginfoza7(long
		BgL_checksumz00_4536, char *BgL_fromz00_4537)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzglobaliza7e_ginfoza7))
				{
					BGl_requirezd2initializa7ationz75zzglobaliza7e_ginfoza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzglobaliza7e_ginfoza7();
					BGl_libraryzd2moduleszd2initz00zzglobaliza7e_ginfoza7();
					BGl_cnstzd2initzd2zzglobaliza7e_ginfoza7();
					BGl_importedzd2moduleszd2initz00zzglobaliza7e_ginfoza7();
					BGl_objectzd2initzd2zzglobaliza7e_ginfoza7();
					return BGl_toplevelzd2initzd2zzglobaliza7e_ginfoza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "globalize_ginfo");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "globalize_ginfo");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"globalize_ginfo");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"globalize_ginfo");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"globalize_ginfo");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"globalize_ginfo");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"globalize_ginfo");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.scm 15 */
			{	/* Globalize/ginfo.scm 15 */
				obj_t BgL_cportz00_4207;

				{	/* Globalize/ginfo.scm 15 */
					obj_t BgL_stringz00_4214;

					BgL_stringz00_4214 = BGl_string2203z00zzglobaliza7e_ginfoza7;
					{	/* Globalize/ginfo.scm 15 */
						obj_t BgL_startz00_4215;

						BgL_startz00_4215 = BINT(0L);
						{	/* Globalize/ginfo.scm 15 */
							obj_t BgL_endz00_4216;

							BgL_endz00_4216 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4214)));
							{	/* Globalize/ginfo.scm 15 */

								BgL_cportz00_4207 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4214, BgL_startz00_4215, BgL_endz00_4216);
				}}}}
				{
					long BgL_iz00_4208;

					BgL_iz00_4208 = 35L;
				BgL_loopz00_4209:
					if ((BgL_iz00_4208 == -1L))
						{	/* Globalize/ginfo.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Globalize/ginfo.scm 15 */
							{	/* Globalize/ginfo.scm 15 */
								obj_t BgL_arg2204z00_4210;

								{	/* Globalize/ginfo.scm 15 */

									{	/* Globalize/ginfo.scm 15 */
										obj_t BgL_locationz00_4212;

										BgL_locationz00_4212 = BBOOL(((bool_t) 0));
										{	/* Globalize/ginfo.scm 15 */

											BgL_arg2204z00_4210 =
												BGl_readz00zz__readerz00(BgL_cportz00_4207,
												BgL_locationz00_4212);
										}
									}
								}
								{	/* Globalize/ginfo.scm 15 */
									int BgL_tmpz00_4563;

									BgL_tmpz00_4563 = (int) (BgL_iz00_4208);
									CNST_TABLE_SET(BgL_tmpz00_4563, BgL_arg2204z00_4210);
							}}
							{	/* Globalize/ginfo.scm 15 */
								int BgL_auxz00_4213;

								BgL_auxz00_4213 = (int) ((BgL_iz00_4208 - 1L));
								{
									long BgL_iz00_4568;

									BgL_iz00_4568 = (long) (BgL_auxz00_4213);
									BgL_iz00_4208 = BgL_iz00_4568;
									goto BgL_loopz00_4209;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.scm 15 */
			return BUNSPEC;
		}

	}



/* make-sfun/Ginfo */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt
		BGl_makezd2sfunzf2Ginfoz20zzglobaliza7e_ginfoza7(long BgL_arity1271z00_3,
		obj_t BgL_sidezd2effect1272zd2_4, obj_t BgL_predicatezd2of1273zd2_5,
		obj_t BgL_stackzd2allocator1274zd2_6, bool_t BgL_topzf31275zf3_7,
		obj_t BgL_thezd2closure1276zd2_8, obj_t BgL_effect1277z00_9,
		obj_t BgL_failsafe1278z00_10, obj_t BgL_argszd2noescape1279zd2_11,
		obj_t BgL_property1280z00_12, obj_t BgL_args1281z00_13,
		obj_t BgL_argszd2name1282zd2_14, obj_t BgL_body1283z00_15,
		obj_t BgL_class1284z00_16, obj_t BgL_dssslzd2keywords1285zd2_17,
		obj_t BgL_loc1286z00_18, obj_t BgL_optionals1287z00_19,
		obj_t BgL_keys1288z00_20, obj_t BgL_thezd2closurezd2global1289z00_21,
		obj_t BgL_strength1290z00_22, obj_t BgL_stackable1291z00_23,
		bool_t BgL_gzf31292zf3_24, obj_t BgL_cfrom1293z00_25,
		obj_t BgL_cfromza21294za2_26, obj_t BgL_cto1295z00_27,
		obj_t BgL_ctoza21296za2_28, obj_t BgL_efunctions1297z00_29,
		obj_t BgL_integrator1298z00_30, obj_t BgL_imark1299z00_31,
		obj_t BgL_owner1300z00_32, obj_t BgL_integrated1301z00_33,
		obj_t BgL_pluggedzd2in1302zd2_34, long BgL_mark1303z00_35,
		obj_t BgL_freezd2mark1304zd2_36, obj_t BgL_thezd2global1305zd2_37,
		obj_t BgL_kaptured1306z00_38, obj_t BgL_newzd2body1307zd2_39,
		long BgL_bmark1308z00_40, long BgL_umark1309z00_41,
		obj_t BgL_free1310z00_42, obj_t BgL_bound1311z00_43)
	{
		{	/* Globalize/ginfo.sch 218 */
			{	/* Globalize/ginfo.sch 218 */
				BgL_sfunz00_bglt BgL_new1226z00_4218;

				{	/* Globalize/ginfo.sch 218 */
					BgL_sfunz00_bglt BgL_tmp1224z00_4219;
					BgL_sfunzf2ginfozf2_bglt BgL_wide1225z00_4220;

					{
						BgL_sfunz00_bglt BgL_auxz00_4571;

						{	/* Globalize/ginfo.sch 218 */
							BgL_sfunz00_bglt BgL_new1223z00_4221;

							BgL_new1223z00_4221 =
								((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sfunz00_bgl))));
							{	/* Globalize/ginfo.sch 218 */
								long BgL_arg1422z00_4222;

								BgL_arg1422z00_4222 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1223z00_4221),
									BgL_arg1422z00_4222);
							}
							{	/* Globalize/ginfo.sch 218 */
								BgL_objectz00_bglt BgL_tmpz00_4576;

								BgL_tmpz00_4576 = ((BgL_objectz00_bglt) BgL_new1223z00_4221);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4576, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1223z00_4221);
							BgL_auxz00_4571 = BgL_new1223z00_4221;
						}
						BgL_tmp1224z00_4219 = ((BgL_sfunz00_bglt) BgL_auxz00_4571);
					}
					BgL_wide1225z00_4220 =
						((BgL_sfunzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sfunzf2ginfozf2_bgl))));
					{	/* Globalize/ginfo.sch 218 */
						obj_t BgL_auxz00_4584;
						BgL_objectz00_bglt BgL_tmpz00_4582;

						BgL_auxz00_4584 = ((obj_t) BgL_wide1225z00_4220);
						BgL_tmpz00_4582 = ((BgL_objectz00_bglt) BgL_tmp1224z00_4219);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4582, BgL_auxz00_4584);
					}
					((BgL_objectz00_bglt) BgL_tmp1224z00_4219);
					{	/* Globalize/ginfo.sch 218 */
						long BgL_arg1421z00_4223;

						BgL_arg1421z00_4223 =
							BGL_CLASS_NUM(BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1224z00_4219), BgL_arg1421z00_4223);
					}
					BgL_new1226z00_4218 = ((BgL_sfunz00_bglt) BgL_tmp1224z00_4219);
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1226z00_4218)))->BgL_arityz00) =
					((long) BgL_arity1271z00_3), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1272zd2_4), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1273zd2_5), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1274zd2_6), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31275zf3_7), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1276zd2_8), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_effectz00) = ((obj_t) BgL_effect1277z00_9), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1278z00_10), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1279zd2_11), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1226z00_4218)))->
						BgL_argszd2retescapezd2) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_propertyz00) =
					((obj_t) BgL_property1280z00_12), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_argsz00) =
					((obj_t) BgL_args1281z00_13), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_argszd2namezd2) =
					((obj_t) BgL_argszd2name1282zd2_14), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_bodyz00) =
					((obj_t) BgL_body1283z00_15), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_classz00) =
					((obj_t) BgL_class1284z00_16), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_dssslzd2keywordszd2) =
					((obj_t) BgL_dssslzd2keywords1285zd2_17), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_locz00) =
					((obj_t) BgL_loc1286z00_18), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_optionalsz00) =
					((obj_t) BgL_optionals1287z00_19), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_keysz00) =
					((obj_t) BgL_keys1288z00_20), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BgL_thezd2closurezd2global1289z00_21), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_strengthz00) =
					((obj_t) BgL_strength1290z00_22), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1226z00_4218)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1291z00_23), BUNSPEC);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4636;

					{
						obj_t BgL_auxz00_4637;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4638;

							BgL_tmpz00_4638 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4637 = BGL_OBJECT_WIDENING(BgL_tmpz00_4638);
						}
						BgL_auxz00_4636 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4637);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4636))->
							BgL_gzf3zf3) = ((bool_t) BgL_gzf31292zf3_24), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4643;

					{
						obj_t BgL_auxz00_4644;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4645;

							BgL_tmpz00_4645 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4644 = BGL_OBJECT_WIDENING(BgL_tmpz00_4645);
						}
						BgL_auxz00_4643 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4644);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4643))->
							BgL_cfromz00) = ((obj_t) BgL_cfrom1293z00_25), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4650;

					{
						obj_t BgL_auxz00_4651;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4652;

							BgL_tmpz00_4652 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4651 = BGL_OBJECT_WIDENING(BgL_tmpz00_4652);
						}
						BgL_auxz00_4650 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4651);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4650))->
							BgL_cfromza2za2) = ((obj_t) BgL_cfromza21294za2_26), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4657;

					{
						obj_t BgL_auxz00_4658;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4659;

							BgL_tmpz00_4659 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4658 = BGL_OBJECT_WIDENING(BgL_tmpz00_4659);
						}
						BgL_auxz00_4657 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4658);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4657))->BgL_ctoz00) =
						((obj_t) BgL_cto1295z00_27), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4664;

					{
						obj_t BgL_auxz00_4665;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4666;

							BgL_tmpz00_4666 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4665 = BGL_OBJECT_WIDENING(BgL_tmpz00_4666);
						}
						BgL_auxz00_4664 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4665);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4664))->
							BgL_ctoza2za2) = ((obj_t) BgL_ctoza21296za2_28), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4671;

					{
						obj_t BgL_auxz00_4672;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4673;

							BgL_tmpz00_4673 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4672 = BGL_OBJECT_WIDENING(BgL_tmpz00_4673);
						}
						BgL_auxz00_4671 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4672);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4671))->
							BgL_efunctionsz00) = ((obj_t) BgL_efunctions1297z00_29), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4678;

					{
						obj_t BgL_auxz00_4679;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4680;

							BgL_tmpz00_4680 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4679 = BGL_OBJECT_WIDENING(BgL_tmpz00_4680);
						}
						BgL_auxz00_4678 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4679);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4678))->
							BgL_integratorz00) = ((obj_t) BgL_integrator1298z00_30), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4685;

					{
						obj_t BgL_auxz00_4686;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4687;

							BgL_tmpz00_4687 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4686 = BGL_OBJECT_WIDENING(BgL_tmpz00_4687);
						}
						BgL_auxz00_4685 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4686);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4685))->
							BgL_imarkz00) = ((obj_t) BgL_imark1299z00_31), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4692;

					{
						obj_t BgL_auxz00_4693;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4694;

							BgL_tmpz00_4694 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4693 = BGL_OBJECT_WIDENING(BgL_tmpz00_4694);
						}
						BgL_auxz00_4692 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4693);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4692))->
							BgL_ownerz00) = ((obj_t) BgL_owner1300z00_32), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4699;

					{
						obj_t BgL_auxz00_4700;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4701;

							BgL_tmpz00_4701 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4700 = BGL_OBJECT_WIDENING(BgL_tmpz00_4701);
						}
						BgL_auxz00_4699 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4700);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4699))->
							BgL_integratedz00) = ((obj_t) BgL_integrated1301z00_33), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4706;

					{
						obj_t BgL_auxz00_4707;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4708;

							BgL_tmpz00_4708 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4707 = BGL_OBJECT_WIDENING(BgL_tmpz00_4708);
						}
						BgL_auxz00_4706 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4707);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4706))->
							BgL_pluggedzd2inzd2) =
						((obj_t) BgL_pluggedzd2in1302zd2_34), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4713;

					{
						obj_t BgL_auxz00_4714;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4715;

							BgL_tmpz00_4715 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4714 = BGL_OBJECT_WIDENING(BgL_tmpz00_4715);
						}
						BgL_auxz00_4713 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4714);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4713))->
							BgL_markz00) = ((long) BgL_mark1303z00_35), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4720;

					{
						obj_t BgL_auxz00_4721;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4722;

							BgL_tmpz00_4722 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4721 = BGL_OBJECT_WIDENING(BgL_tmpz00_4722);
						}
						BgL_auxz00_4720 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4721);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4720))->
							BgL_freezd2markzd2) =
						((obj_t) BgL_freezd2mark1304zd2_36), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4727;

					{
						obj_t BgL_auxz00_4728;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4729;

							BgL_tmpz00_4729 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4728 = BGL_OBJECT_WIDENING(BgL_tmpz00_4729);
						}
						BgL_auxz00_4727 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4728);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4727))->
							BgL_thezd2globalzd2) =
						((obj_t) BgL_thezd2global1305zd2_37), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4734;

					{
						obj_t BgL_auxz00_4735;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4736;

							BgL_tmpz00_4736 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4735 = BGL_OBJECT_WIDENING(BgL_tmpz00_4736);
						}
						BgL_auxz00_4734 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4735);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4734))->
							BgL_kapturedz00) = ((obj_t) BgL_kaptured1306z00_38), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4741;

					{
						obj_t BgL_auxz00_4742;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4743;

							BgL_tmpz00_4743 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4742 = BGL_OBJECT_WIDENING(BgL_tmpz00_4743);
						}
						BgL_auxz00_4741 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4742);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4741))->
							BgL_newzd2bodyzd2) = ((obj_t) BgL_newzd2body1307zd2_39), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4748;

					{
						obj_t BgL_auxz00_4749;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4750;

							BgL_tmpz00_4750 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4749 = BGL_OBJECT_WIDENING(BgL_tmpz00_4750);
						}
						BgL_auxz00_4748 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4749);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4748))->
							BgL_bmarkz00) = ((long) BgL_bmark1308z00_40), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4755;

					{
						obj_t BgL_auxz00_4756;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4757;

							BgL_tmpz00_4757 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4756 = BGL_OBJECT_WIDENING(BgL_tmpz00_4757);
						}
						BgL_auxz00_4755 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4756);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4755))->
							BgL_umarkz00) = ((long) BgL_umark1309z00_41), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4762;

					{
						obj_t BgL_auxz00_4763;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4764;

							BgL_tmpz00_4764 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4763 = BGL_OBJECT_WIDENING(BgL_tmpz00_4764);
						}
						BgL_auxz00_4762 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4763);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4762))->
							BgL_freez00) = ((obj_t) BgL_free1310z00_42), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4769;

					{
						obj_t BgL_auxz00_4770;

						{	/* Globalize/ginfo.sch 218 */
							BgL_objectz00_bglt BgL_tmpz00_4771;

							BgL_tmpz00_4771 = ((BgL_objectz00_bglt) BgL_new1226z00_4218);
							BgL_auxz00_4770 = BGL_OBJECT_WIDENING(BgL_tmpz00_4771);
						}
						BgL_auxz00_4769 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4770);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4769))->
							BgL_boundz00) = ((obj_t) BgL_bound1311z00_43), BUNSPEC);
				}
				return BgL_new1226z00_4218;
			}
		}

	}



/* &make-sfun/Ginfo */
	BgL_sfunz00_bglt BGl_z62makezd2sfunzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3237, obj_t BgL_arity1271z00_3238,
		obj_t BgL_sidezd2effect1272zd2_3239, obj_t BgL_predicatezd2of1273zd2_3240,
		obj_t BgL_stackzd2allocator1274zd2_3241, obj_t BgL_topzf31275zf3_3242,
		obj_t BgL_thezd2closure1276zd2_3243, obj_t BgL_effect1277z00_3244,
		obj_t BgL_failsafe1278z00_3245, obj_t BgL_argszd2noescape1279zd2_3246,
		obj_t BgL_property1280z00_3247, obj_t BgL_args1281z00_3248,
		obj_t BgL_argszd2name1282zd2_3249, obj_t BgL_body1283z00_3250,
		obj_t BgL_class1284z00_3251, obj_t BgL_dssslzd2keywords1285zd2_3252,
		obj_t BgL_loc1286z00_3253, obj_t BgL_optionals1287z00_3254,
		obj_t BgL_keys1288z00_3255, obj_t BgL_thezd2closurezd2global1289z00_3256,
		obj_t BgL_strength1290z00_3257, obj_t BgL_stackable1291z00_3258,
		obj_t BgL_gzf31292zf3_3259, obj_t BgL_cfrom1293z00_3260,
		obj_t BgL_cfromza21294za2_3261, obj_t BgL_cto1295z00_3262,
		obj_t BgL_ctoza21296za2_3263, obj_t BgL_efunctions1297z00_3264,
		obj_t BgL_integrator1298z00_3265, obj_t BgL_imark1299z00_3266,
		obj_t BgL_owner1300z00_3267, obj_t BgL_integrated1301z00_3268,
		obj_t BgL_pluggedzd2in1302zd2_3269, obj_t BgL_mark1303z00_3270,
		obj_t BgL_freezd2mark1304zd2_3271, obj_t BgL_thezd2global1305zd2_3272,
		obj_t BgL_kaptured1306z00_3273, obj_t BgL_newzd2body1307zd2_3274,
		obj_t BgL_bmark1308z00_3275, obj_t BgL_umark1309z00_3276,
		obj_t BgL_free1310z00_3277, obj_t BgL_bound1311z00_3278)
	{
		{	/* Globalize/ginfo.sch 218 */
			return
				BGl_makezd2sfunzf2Ginfoz20zzglobaliza7e_ginfoza7(
				(long) CINT(BgL_arity1271z00_3238), BgL_sidezd2effect1272zd2_3239,
				BgL_predicatezd2of1273zd2_3240, BgL_stackzd2allocator1274zd2_3241,
				CBOOL(BgL_topzf31275zf3_3242), BgL_thezd2closure1276zd2_3243,
				BgL_effect1277z00_3244, BgL_failsafe1278z00_3245,
				BgL_argszd2noescape1279zd2_3246, BgL_property1280z00_3247,
				BgL_args1281z00_3248, BgL_argszd2name1282zd2_3249, BgL_body1283z00_3250,
				BgL_class1284z00_3251, BgL_dssslzd2keywords1285zd2_3252,
				BgL_loc1286z00_3253, BgL_optionals1287z00_3254, BgL_keys1288z00_3255,
				BgL_thezd2closurezd2global1289z00_3256, BgL_strength1290z00_3257,
				BgL_stackable1291z00_3258, CBOOL(BgL_gzf31292zf3_3259),
				BgL_cfrom1293z00_3260, BgL_cfromza21294za2_3261, BgL_cto1295z00_3262,
				BgL_ctoza21296za2_3263, BgL_efunctions1297z00_3264,
				BgL_integrator1298z00_3265, BgL_imark1299z00_3266,
				BgL_owner1300z00_3267, BgL_integrated1301z00_3268,
				BgL_pluggedzd2in1302zd2_3269, (long) CINT(BgL_mark1303z00_3270),
				BgL_freezd2mark1304zd2_3271, BgL_thezd2global1305zd2_3272,
				BgL_kaptured1306z00_3273, BgL_newzd2body1307zd2_3274,
				(long) CINT(BgL_bmark1308z00_3275), (long) CINT(BgL_umark1309z00_3276),
				BgL_free1310z00_3277, BgL_bound1311z00_3278);
		}

	}



/* sfun/Ginfo? */
	BGL_EXPORTED_DEF bool_t BGl_sfunzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t
		BgL_objz00_44)
	{
		{	/* Globalize/ginfo.sch 219 */
			{	/* Globalize/ginfo.sch 219 */
				obj_t BgL_classz00_4224;

				BgL_classz00_4224 = BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7;
				if (BGL_OBJECTP(BgL_objz00_44))
					{	/* Globalize/ginfo.sch 219 */
						BgL_objectz00_bglt BgL_arg1807z00_4225;

						BgL_arg1807z00_4225 = (BgL_objectz00_bglt) (BgL_objz00_44);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Globalize/ginfo.sch 219 */
								long BgL_idxz00_4226;

								BgL_idxz00_4226 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4225);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4226 + 4L)) == BgL_classz00_4224);
							}
						else
							{	/* Globalize/ginfo.sch 219 */
								bool_t BgL_res2077z00_4229;

								{	/* Globalize/ginfo.sch 219 */
									obj_t BgL_oclassz00_4230;

									{	/* Globalize/ginfo.sch 219 */
										obj_t BgL_arg1815z00_4231;
										long BgL_arg1816z00_4232;

										BgL_arg1815z00_4231 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Globalize/ginfo.sch 219 */
											long BgL_arg1817z00_4233;

											BgL_arg1817z00_4233 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4225);
											BgL_arg1816z00_4232 = (BgL_arg1817z00_4233 - OBJECT_TYPE);
										}
										BgL_oclassz00_4230 =
											VECTOR_REF(BgL_arg1815z00_4231, BgL_arg1816z00_4232);
									}
									{	/* Globalize/ginfo.sch 219 */
										bool_t BgL__ortest_1115z00_4234;

										BgL__ortest_1115z00_4234 =
											(BgL_classz00_4224 == BgL_oclassz00_4230);
										if (BgL__ortest_1115z00_4234)
											{	/* Globalize/ginfo.sch 219 */
												BgL_res2077z00_4229 = BgL__ortest_1115z00_4234;
											}
										else
											{	/* Globalize/ginfo.sch 219 */
												long BgL_odepthz00_4235;

												{	/* Globalize/ginfo.sch 219 */
													obj_t BgL_arg1804z00_4236;

													BgL_arg1804z00_4236 = (BgL_oclassz00_4230);
													BgL_odepthz00_4235 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4236);
												}
												if ((4L < BgL_odepthz00_4235))
													{	/* Globalize/ginfo.sch 219 */
														obj_t BgL_arg1802z00_4237;

														{	/* Globalize/ginfo.sch 219 */
															obj_t BgL_arg1803z00_4238;

															BgL_arg1803z00_4238 = (BgL_oclassz00_4230);
															BgL_arg1802z00_4237 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4238,
																4L);
														}
														BgL_res2077z00_4229 =
															(BgL_arg1802z00_4237 == BgL_classz00_4224);
													}
												else
													{	/* Globalize/ginfo.sch 219 */
														BgL_res2077z00_4229 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2077z00_4229;
							}
					}
				else
					{	/* Globalize/ginfo.sch 219 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sfun/Ginfo? */
	obj_t BGl_z62sfunzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3279,
		obj_t BgL_objz00_3280)
	{
		{	/* Globalize/ginfo.sch 219 */
			return
				BBOOL(BGl_sfunzf2Ginfozf3z01zzglobaliza7e_ginfoza7(BgL_objz00_3280));
		}

	}



/* sfun/Ginfo-nil */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt
		BGl_sfunzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.sch 220 */
			{	/* Globalize/ginfo.sch 220 */
				obj_t BgL_classz00_2646;

				BgL_classz00_2646 = BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7;
				{	/* Globalize/ginfo.sch 220 */
					obj_t BgL__ortest_1117z00_2647;

					BgL__ortest_1117z00_2647 = BGL_CLASS_NIL(BgL_classz00_2646);
					if (CBOOL(BgL__ortest_1117z00_2647))
						{	/* Globalize/ginfo.sch 220 */
							return ((BgL_sfunz00_bglt) BgL__ortest_1117z00_2647);
						}
					else
						{	/* Globalize/ginfo.sch 220 */
							return
								((BgL_sfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2646));
						}
				}
			}
		}

	}



/* &sfun/Ginfo-nil */
	BgL_sfunz00_bglt BGl_z62sfunzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3281)
	{
		{	/* Globalize/ginfo.sch 220 */
			return BGl_sfunzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7();
		}

	}



/* sfun/Ginfo-bound */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2boundz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_45)
	{
		{	/* Globalize/ginfo.sch 221 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4814;

				{
					obj_t BgL_auxz00_4815;

					{	/* Globalize/ginfo.sch 221 */
						BgL_objectz00_bglt BgL_tmpz00_4816;

						BgL_tmpz00_4816 = ((BgL_objectz00_bglt) BgL_oz00_45);
						BgL_auxz00_4815 = BGL_OBJECT_WIDENING(BgL_tmpz00_4816);
					}
					BgL_auxz00_4814 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4815);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4814))->BgL_boundz00);
			}
		}

	}



/* &sfun/Ginfo-bound */
	obj_t BGl_z62sfunzf2Ginfozd2boundz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3282, obj_t BgL_oz00_3283)
	{
		{	/* Globalize/ginfo.sch 221 */
			return
				BGl_sfunzf2Ginfozd2boundz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3283));
		}

	}



/* sfun/Ginfo-bound-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2boundzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_46, obj_t BgL_vz00_47)
	{
		{	/* Globalize/ginfo.sch 222 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4823;

				{
					obj_t BgL_auxz00_4824;

					{	/* Globalize/ginfo.sch 222 */
						BgL_objectz00_bglt BgL_tmpz00_4825;

						BgL_tmpz00_4825 = ((BgL_objectz00_bglt) BgL_oz00_46);
						BgL_auxz00_4824 = BGL_OBJECT_WIDENING(BgL_tmpz00_4825);
					}
					BgL_auxz00_4823 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4824);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4823))->
						BgL_boundz00) = ((obj_t) BgL_vz00_47), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-bound-set! */
	obj_t BGl_z62sfunzf2Ginfozd2boundzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3284, obj_t BgL_oz00_3285, obj_t BgL_vz00_3286)
	{
		{	/* Globalize/ginfo.sch 222 */
			return
				BGl_sfunzf2Ginfozd2boundzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3285), BgL_vz00_3286);
		}

	}



/* sfun/Ginfo-free */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2freez20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_48)
	{
		{	/* Globalize/ginfo.sch 223 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4832;

				{
					obj_t BgL_auxz00_4833;

					{	/* Globalize/ginfo.sch 223 */
						BgL_objectz00_bglt BgL_tmpz00_4834;

						BgL_tmpz00_4834 = ((BgL_objectz00_bglt) BgL_oz00_48);
						BgL_auxz00_4833 = BGL_OBJECT_WIDENING(BgL_tmpz00_4834);
					}
					BgL_auxz00_4832 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4833);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4832))->BgL_freez00);
			}
		}

	}



/* &sfun/Ginfo-free */
	obj_t BGl_z62sfunzf2Ginfozd2freez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3287, obj_t BgL_oz00_3288)
	{
		{	/* Globalize/ginfo.sch 223 */
			return
				BGl_sfunzf2Ginfozd2freez20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3288));
		}

	}



/* sfun/Ginfo-free-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2freezd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_49, obj_t BgL_vz00_50)
	{
		{	/* Globalize/ginfo.sch 224 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4841;

				{
					obj_t BgL_auxz00_4842;

					{	/* Globalize/ginfo.sch 224 */
						BgL_objectz00_bglt BgL_tmpz00_4843;

						BgL_tmpz00_4843 = ((BgL_objectz00_bglt) BgL_oz00_49);
						BgL_auxz00_4842 = BGL_OBJECT_WIDENING(BgL_tmpz00_4843);
					}
					BgL_auxz00_4841 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4842);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4841))->
						BgL_freez00) = ((obj_t) BgL_vz00_50), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-free-set! */
	obj_t BGl_z62sfunzf2Ginfozd2freezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3289, obj_t BgL_oz00_3290, obj_t BgL_vz00_3291)
	{
		{	/* Globalize/ginfo.sch 224 */
			return
				BGl_sfunzf2Ginfozd2freezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3290), BgL_vz00_3291);
		}

	}



/* sfun/Ginfo-umark */
	BGL_EXPORTED_DEF long
		BGl_sfunzf2Ginfozd2umarkz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_51)
	{
		{	/* Globalize/ginfo.sch 225 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4850;

				{
					obj_t BgL_auxz00_4851;

					{	/* Globalize/ginfo.sch 225 */
						BgL_objectz00_bglt BgL_tmpz00_4852;

						BgL_tmpz00_4852 = ((BgL_objectz00_bglt) BgL_oz00_51);
						BgL_auxz00_4851 = BGL_OBJECT_WIDENING(BgL_tmpz00_4852);
					}
					BgL_auxz00_4850 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4851);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4850))->BgL_umarkz00);
			}
		}

	}



/* &sfun/Ginfo-umark */
	obj_t BGl_z62sfunzf2Ginfozd2umarkz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3292, obj_t BgL_oz00_3293)
	{
		{	/* Globalize/ginfo.sch 225 */
			return
				BINT(BGl_sfunzf2Ginfozd2umarkz20zzglobaliza7e_ginfoza7(
					((BgL_sfunz00_bglt) BgL_oz00_3293)));
		}

	}



/* sfun/Ginfo-umark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2umarkzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_52, long BgL_vz00_53)
	{
		{	/* Globalize/ginfo.sch 226 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4860;

				{
					obj_t BgL_auxz00_4861;

					{	/* Globalize/ginfo.sch 226 */
						BgL_objectz00_bglt BgL_tmpz00_4862;

						BgL_tmpz00_4862 = ((BgL_objectz00_bglt) BgL_oz00_52);
						BgL_auxz00_4861 = BGL_OBJECT_WIDENING(BgL_tmpz00_4862);
					}
					BgL_auxz00_4860 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4861);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4860))->
						BgL_umarkz00) = ((long) BgL_vz00_53), BUNSPEC);
		}}

	}



/* &sfun/Ginfo-umark-set! */
	obj_t BGl_z62sfunzf2Ginfozd2umarkzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3294, obj_t BgL_oz00_3295, obj_t BgL_vz00_3296)
	{
		{	/* Globalize/ginfo.sch 226 */
			return
				BGl_sfunzf2Ginfozd2umarkzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3295), (long) CINT(BgL_vz00_3296));
		}

	}



/* sfun/Ginfo-bmark */
	BGL_EXPORTED_DEF long
		BGl_sfunzf2Ginfozd2bmarkz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_54)
	{
		{	/* Globalize/ginfo.sch 227 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4870;

				{
					obj_t BgL_auxz00_4871;

					{	/* Globalize/ginfo.sch 227 */
						BgL_objectz00_bglt BgL_tmpz00_4872;

						BgL_tmpz00_4872 = ((BgL_objectz00_bglt) BgL_oz00_54);
						BgL_auxz00_4871 = BGL_OBJECT_WIDENING(BgL_tmpz00_4872);
					}
					BgL_auxz00_4870 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4871);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4870))->BgL_bmarkz00);
			}
		}

	}



/* &sfun/Ginfo-bmark */
	obj_t BGl_z62sfunzf2Ginfozd2bmarkz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3297, obj_t BgL_oz00_3298)
	{
		{	/* Globalize/ginfo.sch 227 */
			return
				BINT(BGl_sfunzf2Ginfozd2bmarkz20zzglobaliza7e_ginfoza7(
					((BgL_sfunz00_bglt) BgL_oz00_3298)));
		}

	}



/* sfun/Ginfo-bmark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2bmarkzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_55, long BgL_vz00_56)
	{
		{	/* Globalize/ginfo.sch 228 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4880;

				{
					obj_t BgL_auxz00_4881;

					{	/* Globalize/ginfo.sch 228 */
						BgL_objectz00_bglt BgL_tmpz00_4882;

						BgL_tmpz00_4882 = ((BgL_objectz00_bglt) BgL_oz00_55);
						BgL_auxz00_4881 = BGL_OBJECT_WIDENING(BgL_tmpz00_4882);
					}
					BgL_auxz00_4880 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4881);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4880))->
						BgL_bmarkz00) = ((long) BgL_vz00_56), BUNSPEC);
		}}

	}



/* &sfun/Ginfo-bmark-set! */
	obj_t BGl_z62sfunzf2Ginfozd2bmarkzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3299, obj_t BgL_oz00_3300, obj_t BgL_vz00_3301)
	{
		{	/* Globalize/ginfo.sch 228 */
			return
				BGl_sfunzf2Ginfozd2bmarkzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3300), (long) CINT(BgL_vz00_3301));
		}

	}



/* sfun/Ginfo-new-body */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2newzd2bodyzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_57)
	{
		{	/* Globalize/ginfo.sch 229 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4890;

				{
					obj_t BgL_auxz00_4891;

					{	/* Globalize/ginfo.sch 229 */
						BgL_objectz00_bglt BgL_tmpz00_4892;

						BgL_tmpz00_4892 = ((BgL_objectz00_bglt) BgL_oz00_57);
						BgL_auxz00_4891 = BGL_OBJECT_WIDENING(BgL_tmpz00_4892);
					}
					BgL_auxz00_4890 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4891);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4890))->
					BgL_newzd2bodyzd2);
			}
		}

	}



/* &sfun/Ginfo-new-body */
	obj_t BGl_z62sfunzf2Ginfozd2newzd2bodyz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3302, obj_t BgL_oz00_3303)
	{
		{	/* Globalize/ginfo.sch 229 */
			return
				BGl_sfunzf2Ginfozd2newzd2bodyzf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3303));
		}

	}



/* sfun/Ginfo-new-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2newzd2bodyzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_58, obj_t BgL_vz00_59)
	{
		{	/* Globalize/ginfo.sch 230 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4899;

				{
					obj_t BgL_auxz00_4900;

					{	/* Globalize/ginfo.sch 230 */
						BgL_objectz00_bglt BgL_tmpz00_4901;

						BgL_tmpz00_4901 = ((BgL_objectz00_bglt) BgL_oz00_58);
						BgL_auxz00_4900 = BGL_OBJECT_WIDENING(BgL_tmpz00_4901);
					}
					BgL_auxz00_4899 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4900);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4899))->
						BgL_newzd2bodyzd2) = ((obj_t) BgL_vz00_59), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-new-body-set! */
	obj_t BGl_z62sfunzf2Ginfozd2newzd2bodyzd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3304, obj_t BgL_oz00_3305, obj_t BgL_vz00_3306)
	{
		{	/* Globalize/ginfo.sch 230 */
			return
				BGl_sfunzf2Ginfozd2newzd2bodyzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3305), BgL_vz00_3306);
		}

	}



/* sfun/Ginfo-kaptured */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2kapturedz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_60)
	{
		{	/* Globalize/ginfo.sch 231 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4908;

				{
					obj_t BgL_auxz00_4909;

					{	/* Globalize/ginfo.sch 231 */
						BgL_objectz00_bglt BgL_tmpz00_4910;

						BgL_tmpz00_4910 = ((BgL_objectz00_bglt) BgL_oz00_60);
						BgL_auxz00_4909 = BGL_OBJECT_WIDENING(BgL_tmpz00_4910);
					}
					BgL_auxz00_4908 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4909);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4908))->
					BgL_kapturedz00);
			}
		}

	}



/* &sfun/Ginfo-kaptured */
	obj_t BGl_z62sfunzf2Ginfozd2kapturedz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3307, obj_t BgL_oz00_3308)
	{
		{	/* Globalize/ginfo.sch 231 */
			return
				BGl_sfunzf2Ginfozd2kapturedz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3308));
		}

	}



/* sfun/Ginfo-kaptured-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2kapturedzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_61, obj_t BgL_vz00_62)
	{
		{	/* Globalize/ginfo.sch 232 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4917;

				{
					obj_t BgL_auxz00_4918;

					{	/* Globalize/ginfo.sch 232 */
						BgL_objectz00_bglt BgL_tmpz00_4919;

						BgL_tmpz00_4919 = ((BgL_objectz00_bglt) BgL_oz00_61);
						BgL_auxz00_4918 = BGL_OBJECT_WIDENING(BgL_tmpz00_4919);
					}
					BgL_auxz00_4917 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4918);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4917))->
						BgL_kapturedz00) = ((obj_t) BgL_vz00_62), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-kaptured-set! */
	obj_t BGl_z62sfunzf2Ginfozd2kapturedzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3309, obj_t BgL_oz00_3310, obj_t BgL_vz00_3311)
	{
		{	/* Globalize/ginfo.sch 232 */
			return
				BGl_sfunzf2Ginfozd2kapturedzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3310), BgL_vz00_3311);
		}

	}



/* sfun/Ginfo-the-global */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2thezd2globalzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_63)
	{
		{	/* Globalize/ginfo.sch 233 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4926;

				{
					obj_t BgL_auxz00_4927;

					{	/* Globalize/ginfo.sch 233 */
						BgL_objectz00_bglt BgL_tmpz00_4928;

						BgL_tmpz00_4928 = ((BgL_objectz00_bglt) BgL_oz00_63);
						BgL_auxz00_4927 = BGL_OBJECT_WIDENING(BgL_tmpz00_4928);
					}
					BgL_auxz00_4926 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4927);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4926))->
					BgL_thezd2globalzd2);
			}
		}

	}



/* &sfun/Ginfo-the-global */
	obj_t BGl_z62sfunzf2Ginfozd2thezd2globalz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3312, obj_t BgL_oz00_3313)
	{
		{	/* Globalize/ginfo.sch 233 */
			return
				BGl_sfunzf2Ginfozd2thezd2globalzf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3313));
		}

	}



/* sfun/Ginfo-the-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2thezd2globalzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_64, obj_t BgL_vz00_65)
	{
		{	/* Globalize/ginfo.sch 234 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4935;

				{
					obj_t BgL_auxz00_4936;

					{	/* Globalize/ginfo.sch 234 */
						BgL_objectz00_bglt BgL_tmpz00_4937;

						BgL_tmpz00_4937 = ((BgL_objectz00_bglt) BgL_oz00_64);
						BgL_auxz00_4936 = BGL_OBJECT_WIDENING(BgL_tmpz00_4937);
					}
					BgL_auxz00_4935 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4936);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4935))->
						BgL_thezd2globalzd2) = ((obj_t) BgL_vz00_65), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-the-global-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2thezd2globalzd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3314, obj_t BgL_oz00_3315, obj_t BgL_vz00_3316)
	{
		{	/* Globalize/ginfo.sch 234 */
			return
				BGl_sfunzf2Ginfozd2thezd2globalzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3315), BgL_vz00_3316);
		}

	}



/* sfun/Ginfo-free-mark */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_66)
	{
		{	/* Globalize/ginfo.sch 235 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4944;

				{
					obj_t BgL_auxz00_4945;

					{	/* Globalize/ginfo.sch 235 */
						BgL_objectz00_bglt BgL_tmpz00_4946;

						BgL_tmpz00_4946 = ((BgL_objectz00_bglt) BgL_oz00_66);
						BgL_auxz00_4945 = BGL_OBJECT_WIDENING(BgL_tmpz00_4946);
					}
					BgL_auxz00_4944 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4945);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4944))->
					BgL_freezd2markzd2);
			}
		}

	}



/* &sfun/Ginfo-free-mark */
	obj_t BGl_z62sfunzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3317, obj_t BgL_oz00_3318)
	{
		{	/* Globalize/ginfo.sch 235 */
			return
				BGl_sfunzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3318));
		}

	}



/* sfun/Ginfo-free-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_67, obj_t BgL_vz00_68)
	{
		{	/* Globalize/ginfo.sch 236 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4953;

				{
					obj_t BgL_auxz00_4954;

					{	/* Globalize/ginfo.sch 236 */
						BgL_objectz00_bglt BgL_tmpz00_4955;

						BgL_tmpz00_4955 = ((BgL_objectz00_bglt) BgL_oz00_67);
						BgL_auxz00_4954 = BGL_OBJECT_WIDENING(BgL_tmpz00_4955);
					}
					BgL_auxz00_4953 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4954);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4953))->
						BgL_freezd2markzd2) = ((obj_t) BgL_vz00_68), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-free-mark-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3319, obj_t BgL_oz00_3320, obj_t BgL_vz00_3321)
	{
		{	/* Globalize/ginfo.sch 236 */
			return
				BGl_sfunzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3320), BgL_vz00_3321);
		}

	}



/* sfun/Ginfo-mark */
	BGL_EXPORTED_DEF long
		BGl_sfunzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_69)
	{
		{	/* Globalize/ginfo.sch 237 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4962;

				{
					obj_t BgL_auxz00_4963;

					{	/* Globalize/ginfo.sch 237 */
						BgL_objectz00_bglt BgL_tmpz00_4964;

						BgL_tmpz00_4964 = ((BgL_objectz00_bglt) BgL_oz00_69);
						BgL_auxz00_4963 = BGL_OBJECT_WIDENING(BgL_tmpz00_4964);
					}
					BgL_auxz00_4962 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4963);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4962))->BgL_markz00);
			}
		}

	}



/* &sfun/Ginfo-mark */
	obj_t BGl_z62sfunzf2Ginfozd2markz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3322, obj_t BgL_oz00_3323)
	{
		{	/* Globalize/ginfo.sch 237 */
			return
				BINT(BGl_sfunzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(
					((BgL_sfunz00_bglt) BgL_oz00_3323)));
		}

	}



/* sfun/Ginfo-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_70, long BgL_vz00_71)
	{
		{	/* Globalize/ginfo.sch 238 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4972;

				{
					obj_t BgL_auxz00_4973;

					{	/* Globalize/ginfo.sch 238 */
						BgL_objectz00_bglt BgL_tmpz00_4974;

						BgL_tmpz00_4974 = ((BgL_objectz00_bglt) BgL_oz00_70);
						BgL_auxz00_4973 = BGL_OBJECT_WIDENING(BgL_tmpz00_4974);
					}
					BgL_auxz00_4972 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4973);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4972))->
						BgL_markz00) = ((long) BgL_vz00_71), BUNSPEC);
		}}

	}



/* &sfun/Ginfo-mark-set! */
	obj_t BGl_z62sfunzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3324, obj_t BgL_oz00_3325, obj_t BgL_vz00_3326)
	{
		{	/* Globalize/ginfo.sch 238 */
			return
				BGl_sfunzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3325), (long) CINT(BgL_vz00_3326));
		}

	}



/* sfun/Ginfo-plugged-in */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2pluggedzd2inzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_72)
	{
		{	/* Globalize/ginfo.sch 239 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4982;

				{
					obj_t BgL_auxz00_4983;

					{	/* Globalize/ginfo.sch 239 */
						BgL_objectz00_bglt BgL_tmpz00_4984;

						BgL_tmpz00_4984 = ((BgL_objectz00_bglt) BgL_oz00_72);
						BgL_auxz00_4983 = BGL_OBJECT_WIDENING(BgL_tmpz00_4984);
					}
					BgL_auxz00_4982 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4983);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4982))->
					BgL_pluggedzd2inzd2);
			}
		}

	}



/* &sfun/Ginfo-plugged-in */
	obj_t BGl_z62sfunzf2Ginfozd2pluggedzd2inz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3327, obj_t BgL_oz00_3328)
	{
		{	/* Globalize/ginfo.sch 239 */
			return
				BGl_sfunzf2Ginfozd2pluggedzd2inzf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3328));
		}

	}



/* sfun/Ginfo-plugged-in-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2pluggedzd2inzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_73, obj_t BgL_vz00_74)
	{
		{	/* Globalize/ginfo.sch 240 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_4991;

				{
					obj_t BgL_auxz00_4992;

					{	/* Globalize/ginfo.sch 240 */
						BgL_objectz00_bglt BgL_tmpz00_4993;

						BgL_tmpz00_4993 = ((BgL_objectz00_bglt) BgL_oz00_73);
						BgL_auxz00_4992 = BGL_OBJECT_WIDENING(BgL_tmpz00_4993);
					}
					BgL_auxz00_4991 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_4992);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_4991))->
						BgL_pluggedzd2inzd2) = ((obj_t) BgL_vz00_74), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-plugged-in-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2pluggedzd2inzd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3329, obj_t BgL_oz00_3330, obj_t BgL_vz00_3331)
	{
		{	/* Globalize/ginfo.sch 240 */
			return
				BGl_sfunzf2Ginfozd2pluggedzd2inzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3330), BgL_vz00_3331);
		}

	}



/* sfun/Ginfo-integrated */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2integratedz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_75)
	{
		{	/* Globalize/ginfo.sch 241 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5000;

				{
					obj_t BgL_auxz00_5001;

					{	/* Globalize/ginfo.sch 241 */
						BgL_objectz00_bglt BgL_tmpz00_5002;

						BgL_tmpz00_5002 = ((BgL_objectz00_bglt) BgL_oz00_75);
						BgL_auxz00_5001 = BGL_OBJECT_WIDENING(BgL_tmpz00_5002);
					}
					BgL_auxz00_5000 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5001);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5000))->
					BgL_integratedz00);
			}
		}

	}



/* &sfun/Ginfo-integrated */
	obj_t BGl_z62sfunzf2Ginfozd2integratedz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3332, obj_t BgL_oz00_3333)
	{
		{	/* Globalize/ginfo.sch 241 */
			return
				BGl_sfunzf2Ginfozd2integratedz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3333));
		}

	}



/* sfun/Ginfo-integrated-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2integratedzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_76, obj_t BgL_vz00_77)
	{
		{	/* Globalize/ginfo.sch 242 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5009;

				{
					obj_t BgL_auxz00_5010;

					{	/* Globalize/ginfo.sch 242 */
						BgL_objectz00_bglt BgL_tmpz00_5011;

						BgL_tmpz00_5011 = ((BgL_objectz00_bglt) BgL_oz00_76);
						BgL_auxz00_5010 = BGL_OBJECT_WIDENING(BgL_tmpz00_5011);
					}
					BgL_auxz00_5009 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5010);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5009))->
						BgL_integratedz00) = ((obj_t) BgL_vz00_77), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-integrated-set! */
	obj_t BGl_z62sfunzf2Ginfozd2integratedzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3334, obj_t BgL_oz00_3335, obj_t BgL_vz00_3336)
	{
		{	/* Globalize/ginfo.sch 242 */
			return
				BGl_sfunzf2Ginfozd2integratedzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3335), BgL_vz00_3336);
		}

	}



/* sfun/Ginfo-owner */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2ownerz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_78)
	{
		{	/* Globalize/ginfo.sch 243 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5018;

				{
					obj_t BgL_auxz00_5019;

					{	/* Globalize/ginfo.sch 243 */
						BgL_objectz00_bglt BgL_tmpz00_5020;

						BgL_tmpz00_5020 = ((BgL_objectz00_bglt) BgL_oz00_78);
						BgL_auxz00_5019 = BGL_OBJECT_WIDENING(BgL_tmpz00_5020);
					}
					BgL_auxz00_5018 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5019);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5018))->BgL_ownerz00);
			}
		}

	}



/* &sfun/Ginfo-owner */
	obj_t BGl_z62sfunzf2Ginfozd2ownerz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3337, obj_t BgL_oz00_3338)
	{
		{	/* Globalize/ginfo.sch 243 */
			return
				BGl_sfunzf2Ginfozd2ownerz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3338));
		}

	}



/* sfun/Ginfo-owner-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2ownerzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_79, obj_t BgL_vz00_80)
	{
		{	/* Globalize/ginfo.sch 244 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5027;

				{
					obj_t BgL_auxz00_5028;

					{	/* Globalize/ginfo.sch 244 */
						BgL_objectz00_bglt BgL_tmpz00_5029;

						BgL_tmpz00_5029 = ((BgL_objectz00_bglt) BgL_oz00_79);
						BgL_auxz00_5028 = BGL_OBJECT_WIDENING(BgL_tmpz00_5029);
					}
					BgL_auxz00_5027 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5028);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5027))->
						BgL_ownerz00) = ((obj_t) BgL_vz00_80), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-owner-set! */
	obj_t BGl_z62sfunzf2Ginfozd2ownerzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3339, obj_t BgL_oz00_3340, obj_t BgL_vz00_3341)
	{
		{	/* Globalize/ginfo.sch 244 */
			return
				BGl_sfunzf2Ginfozd2ownerzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3340), BgL_vz00_3341);
		}

	}



/* sfun/Ginfo-imark */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2imarkz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_81)
	{
		{	/* Globalize/ginfo.sch 245 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5036;

				{
					obj_t BgL_auxz00_5037;

					{	/* Globalize/ginfo.sch 245 */
						BgL_objectz00_bglt BgL_tmpz00_5038;

						BgL_tmpz00_5038 = ((BgL_objectz00_bglt) BgL_oz00_81);
						BgL_auxz00_5037 = BGL_OBJECT_WIDENING(BgL_tmpz00_5038);
					}
					BgL_auxz00_5036 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5037);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5036))->BgL_imarkz00);
			}
		}

	}



/* &sfun/Ginfo-imark */
	obj_t BGl_z62sfunzf2Ginfozd2imarkz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3342, obj_t BgL_oz00_3343)
	{
		{	/* Globalize/ginfo.sch 245 */
			return
				BGl_sfunzf2Ginfozd2imarkz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3343));
		}

	}



/* sfun/Ginfo-imark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2imarkzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_82, obj_t BgL_vz00_83)
	{
		{	/* Globalize/ginfo.sch 246 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5045;

				{
					obj_t BgL_auxz00_5046;

					{	/* Globalize/ginfo.sch 246 */
						BgL_objectz00_bglt BgL_tmpz00_5047;

						BgL_tmpz00_5047 = ((BgL_objectz00_bglt) BgL_oz00_82);
						BgL_auxz00_5046 = BGL_OBJECT_WIDENING(BgL_tmpz00_5047);
					}
					BgL_auxz00_5045 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5046);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5045))->
						BgL_imarkz00) = ((obj_t) BgL_vz00_83), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-imark-set! */
	obj_t BGl_z62sfunzf2Ginfozd2imarkzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3344, obj_t BgL_oz00_3345, obj_t BgL_vz00_3346)
	{
		{	/* Globalize/ginfo.sch 246 */
			return
				BGl_sfunzf2Ginfozd2imarkzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3345), BgL_vz00_3346);
		}

	}



/* sfun/Ginfo-integrator */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2integratorz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_84)
	{
		{	/* Globalize/ginfo.sch 247 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5054;

				{
					obj_t BgL_auxz00_5055;

					{	/* Globalize/ginfo.sch 247 */
						BgL_objectz00_bglt BgL_tmpz00_5056;

						BgL_tmpz00_5056 = ((BgL_objectz00_bglt) BgL_oz00_84);
						BgL_auxz00_5055 = BGL_OBJECT_WIDENING(BgL_tmpz00_5056);
					}
					BgL_auxz00_5054 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5055);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5054))->
					BgL_integratorz00);
			}
		}

	}



/* &sfun/Ginfo-integrator */
	obj_t BGl_z62sfunzf2Ginfozd2integratorz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3347, obj_t BgL_oz00_3348)
	{
		{	/* Globalize/ginfo.sch 247 */
			return
				BGl_sfunzf2Ginfozd2integratorz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3348));
		}

	}



/* sfun/Ginfo-integrator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2integratorzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_85, obj_t BgL_vz00_86)
	{
		{	/* Globalize/ginfo.sch 248 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5063;

				{
					obj_t BgL_auxz00_5064;

					{	/* Globalize/ginfo.sch 248 */
						BgL_objectz00_bglt BgL_tmpz00_5065;

						BgL_tmpz00_5065 = ((BgL_objectz00_bglt) BgL_oz00_85);
						BgL_auxz00_5064 = BGL_OBJECT_WIDENING(BgL_tmpz00_5065);
					}
					BgL_auxz00_5063 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5064);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5063))->
						BgL_integratorz00) = ((obj_t) BgL_vz00_86), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-integrator-set! */
	obj_t BGl_z62sfunzf2Ginfozd2integratorzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3349, obj_t BgL_oz00_3350, obj_t BgL_vz00_3351)
	{
		{	/* Globalize/ginfo.sch 248 */
			return
				BGl_sfunzf2Ginfozd2integratorzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3350), BgL_vz00_3351);
		}

	}



/* sfun/Ginfo-efunctions */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2efunctionsz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_87)
	{
		{	/* Globalize/ginfo.sch 249 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5072;

				{
					obj_t BgL_auxz00_5073;

					{	/* Globalize/ginfo.sch 249 */
						BgL_objectz00_bglt BgL_tmpz00_5074;

						BgL_tmpz00_5074 = ((BgL_objectz00_bglt) BgL_oz00_87);
						BgL_auxz00_5073 = BGL_OBJECT_WIDENING(BgL_tmpz00_5074);
					}
					BgL_auxz00_5072 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5073);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5072))->
					BgL_efunctionsz00);
			}
		}

	}



/* &sfun/Ginfo-efunctions */
	obj_t BGl_z62sfunzf2Ginfozd2efunctionsz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3352, obj_t BgL_oz00_3353)
	{
		{	/* Globalize/ginfo.sch 249 */
			return
				BGl_sfunzf2Ginfozd2efunctionsz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3353));
		}

	}



/* sfun/Ginfo-efunctions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2efunctionszd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_88, obj_t BgL_vz00_89)
	{
		{	/* Globalize/ginfo.sch 250 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5081;

				{
					obj_t BgL_auxz00_5082;

					{	/* Globalize/ginfo.sch 250 */
						BgL_objectz00_bglt BgL_tmpz00_5083;

						BgL_tmpz00_5083 = ((BgL_objectz00_bglt) BgL_oz00_88);
						BgL_auxz00_5082 = BGL_OBJECT_WIDENING(BgL_tmpz00_5083);
					}
					BgL_auxz00_5081 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5082);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5081))->
						BgL_efunctionsz00) = ((obj_t) BgL_vz00_89), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-efunctions-set! */
	obj_t BGl_z62sfunzf2Ginfozd2efunctionszd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3354, obj_t BgL_oz00_3355, obj_t BgL_vz00_3356)
	{
		{	/* Globalize/ginfo.sch 250 */
			return
				BGl_sfunzf2Ginfozd2efunctionszd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3355), BgL_vz00_3356);
		}

	}



/* sfun/Ginfo-cto* */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2ctoza2z82zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_90)
	{
		{	/* Globalize/ginfo.sch 251 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5090;

				{
					obj_t BgL_auxz00_5091;

					{	/* Globalize/ginfo.sch 251 */
						BgL_objectz00_bglt BgL_tmpz00_5092;

						BgL_tmpz00_5092 = ((BgL_objectz00_bglt) BgL_oz00_90);
						BgL_auxz00_5091 = BGL_OBJECT_WIDENING(BgL_tmpz00_5092);
					}
					BgL_auxz00_5090 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5091);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5090))->
					BgL_ctoza2za2);
			}
		}

	}



/* &sfun/Ginfo-cto* */
	obj_t BGl_z62sfunzf2Ginfozd2ctoza2ze0zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3357, obj_t BgL_oz00_3358)
	{
		{	/* Globalize/ginfo.sch 251 */
			return
				BGl_sfunzf2Ginfozd2ctoza2z82zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3358));
		}

	}



/* sfun/Ginfo-cto*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2ctoza2zd2setz12z42zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_91, obj_t BgL_vz00_92)
	{
		{	/* Globalize/ginfo.sch 252 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5099;

				{
					obj_t BgL_auxz00_5100;

					{	/* Globalize/ginfo.sch 252 */
						BgL_objectz00_bglt BgL_tmpz00_5101;

						BgL_tmpz00_5101 = ((BgL_objectz00_bglt) BgL_oz00_91);
						BgL_auxz00_5100 = BGL_OBJECT_WIDENING(BgL_tmpz00_5101);
					}
					BgL_auxz00_5099 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5100);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5099))->
						BgL_ctoza2za2) = ((obj_t) BgL_vz00_92), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-cto*-set! */
	obj_t BGl_z62sfunzf2Ginfozd2ctoza2zd2setz12z20zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3359, obj_t BgL_oz00_3360, obj_t BgL_vz00_3361)
	{
		{	/* Globalize/ginfo.sch 252 */
			return
				BGl_sfunzf2Ginfozd2ctoza2zd2setz12z42zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3360), BgL_vz00_3361);
		}

	}



/* sfun/Ginfo-cto */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2ctoz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_93)
	{
		{	/* Globalize/ginfo.sch 253 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5108;

				{
					obj_t BgL_auxz00_5109;

					{	/* Globalize/ginfo.sch 253 */
						BgL_objectz00_bglt BgL_tmpz00_5110;

						BgL_tmpz00_5110 = ((BgL_objectz00_bglt) BgL_oz00_93);
						BgL_auxz00_5109 = BGL_OBJECT_WIDENING(BgL_tmpz00_5110);
					}
					BgL_auxz00_5108 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5109);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5108))->BgL_ctoz00);
			}
		}

	}



/* &sfun/Ginfo-cto */
	obj_t BGl_z62sfunzf2Ginfozd2ctoz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3362, obj_t BgL_oz00_3363)
	{
		{	/* Globalize/ginfo.sch 253 */
			return
				BGl_sfunzf2Ginfozd2ctoz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3363));
		}

	}



/* sfun/Ginfo-cto-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2ctozd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_94, obj_t BgL_vz00_95)
	{
		{	/* Globalize/ginfo.sch 254 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5117;

				{
					obj_t BgL_auxz00_5118;

					{	/* Globalize/ginfo.sch 254 */
						BgL_objectz00_bglt BgL_tmpz00_5119;

						BgL_tmpz00_5119 = ((BgL_objectz00_bglt) BgL_oz00_94);
						BgL_auxz00_5118 = BGL_OBJECT_WIDENING(BgL_tmpz00_5119);
					}
					BgL_auxz00_5117 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5118);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5117))->BgL_ctoz00) =
					((obj_t) BgL_vz00_95), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-cto-set! */
	obj_t BGl_z62sfunzf2Ginfozd2ctozd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3364, obj_t BgL_oz00_3365, obj_t BgL_vz00_3366)
	{
		{	/* Globalize/ginfo.sch 254 */
			return
				BGl_sfunzf2Ginfozd2ctozd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3365), BgL_vz00_3366);
		}

	}



/* sfun/Ginfo-cfrom* */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2cfromza2z82zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_96)
	{
		{	/* Globalize/ginfo.sch 255 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5126;

				{
					obj_t BgL_auxz00_5127;

					{	/* Globalize/ginfo.sch 255 */
						BgL_objectz00_bglt BgL_tmpz00_5128;

						BgL_tmpz00_5128 = ((BgL_objectz00_bglt) BgL_oz00_96);
						BgL_auxz00_5127 = BGL_OBJECT_WIDENING(BgL_tmpz00_5128);
					}
					BgL_auxz00_5126 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5127);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5126))->
					BgL_cfromza2za2);
			}
		}

	}



/* &sfun/Ginfo-cfrom* */
	obj_t BGl_z62sfunzf2Ginfozd2cfromza2ze0zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3367, obj_t BgL_oz00_3368)
	{
		{	/* Globalize/ginfo.sch 255 */
			return
				BGl_sfunzf2Ginfozd2cfromza2z82zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3368));
		}

	}



/* sfun/Ginfo-cfrom*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2cfromza2zd2setz12z42zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_97, obj_t BgL_vz00_98)
	{
		{	/* Globalize/ginfo.sch 256 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5135;

				{
					obj_t BgL_auxz00_5136;

					{	/* Globalize/ginfo.sch 256 */
						BgL_objectz00_bglt BgL_tmpz00_5137;

						BgL_tmpz00_5137 = ((BgL_objectz00_bglt) BgL_oz00_97);
						BgL_auxz00_5136 = BGL_OBJECT_WIDENING(BgL_tmpz00_5137);
					}
					BgL_auxz00_5135 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5136);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5135))->
						BgL_cfromza2za2) = ((obj_t) BgL_vz00_98), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-cfrom*-set! */
	obj_t BGl_z62sfunzf2Ginfozd2cfromza2zd2setz12z20zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3369, obj_t BgL_oz00_3370, obj_t BgL_vz00_3371)
	{
		{	/* Globalize/ginfo.sch 256 */
			return
				BGl_sfunzf2Ginfozd2cfromza2zd2setz12z42zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3370), BgL_vz00_3371);
		}

	}



/* sfun/Ginfo-cfrom */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2cfromz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_99)
	{
		{	/* Globalize/ginfo.sch 257 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5144;

				{
					obj_t BgL_auxz00_5145;

					{	/* Globalize/ginfo.sch 257 */
						BgL_objectz00_bglt BgL_tmpz00_5146;

						BgL_tmpz00_5146 = ((BgL_objectz00_bglt) BgL_oz00_99);
						BgL_auxz00_5145 = BGL_OBJECT_WIDENING(BgL_tmpz00_5146);
					}
					BgL_auxz00_5144 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5145);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5144))->BgL_cfromz00);
			}
		}

	}



/* &sfun/Ginfo-cfrom */
	obj_t BGl_z62sfunzf2Ginfozd2cfromz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3372, obj_t BgL_oz00_3373)
	{
		{	/* Globalize/ginfo.sch 257 */
			return
				BGl_sfunzf2Ginfozd2cfromz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3373));
		}

	}



/* sfun/Ginfo-cfrom-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2cfromzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_100, obj_t BgL_vz00_101)
	{
		{	/* Globalize/ginfo.sch 258 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5153;

				{
					obj_t BgL_auxz00_5154;

					{	/* Globalize/ginfo.sch 258 */
						BgL_objectz00_bglt BgL_tmpz00_5155;

						BgL_tmpz00_5155 = ((BgL_objectz00_bglt) BgL_oz00_100);
						BgL_auxz00_5154 = BGL_OBJECT_WIDENING(BgL_tmpz00_5155);
					}
					BgL_auxz00_5153 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5154);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5153))->
						BgL_cfromz00) = ((obj_t) BgL_vz00_101), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-cfrom-set! */
	obj_t BGl_z62sfunzf2Ginfozd2cfromzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3374, obj_t BgL_oz00_3375, obj_t BgL_vz00_3376)
	{
		{	/* Globalize/ginfo.sch 258 */
			return
				BGl_sfunzf2Ginfozd2cfromzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3375), BgL_vz00_3376);
		}

	}



/* sfun/Ginfo-G? */
	BGL_EXPORTED_DEF bool_t
		BGl_sfunzf2Ginfozd2Gzf3zd3zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_102)
	{
		{	/* Globalize/ginfo.sch 259 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5162;

				{
					obj_t BgL_auxz00_5163;

					{	/* Globalize/ginfo.sch 259 */
						BgL_objectz00_bglt BgL_tmpz00_5164;

						BgL_tmpz00_5164 = ((BgL_objectz00_bglt) BgL_oz00_102);
						BgL_auxz00_5163 = BGL_OBJECT_WIDENING(BgL_tmpz00_5164);
					}
					BgL_auxz00_5162 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5163);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5162))->BgL_gzf3zf3);
			}
		}

	}



/* &sfun/Ginfo-G? */
	obj_t BGl_z62sfunzf2Ginfozd2Gzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3377, obj_t BgL_oz00_3378)
	{
		{	/* Globalize/ginfo.sch 259 */
			return
				BBOOL(BGl_sfunzf2Ginfozd2Gzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_sfunz00_bglt) BgL_oz00_3378)));
		}

	}



/* sfun/Ginfo-G?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2Gzf3zd2setz12z13zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_103, bool_t BgL_vz00_104)
	{
		{	/* Globalize/ginfo.sch 260 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_5172;

				{
					obj_t BgL_auxz00_5173;

					{	/* Globalize/ginfo.sch 260 */
						BgL_objectz00_bglt BgL_tmpz00_5174;

						BgL_tmpz00_5174 = ((BgL_objectz00_bglt) BgL_oz00_103);
						BgL_auxz00_5173 = BGL_OBJECT_WIDENING(BgL_tmpz00_5174);
					}
					BgL_auxz00_5172 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_5173);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5172))->
						BgL_gzf3zf3) = ((bool_t) BgL_vz00_104), BUNSPEC);
			}
		}

	}



/* &sfun/Ginfo-G?-set! */
	obj_t BGl_z62sfunzf2Ginfozd2Gzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3379, obj_t BgL_oz00_3380, obj_t BgL_vz00_3381)
	{
		{	/* Globalize/ginfo.sch 260 */
			return
				BGl_sfunzf2Ginfozd2Gzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3380), CBOOL(BgL_vz00_3381));
		}

	}



/* sfun/Ginfo-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2stackablez20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_105)
	{
		{	/* Globalize/ginfo.sch 261 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_105)))->BgL_stackablez00);
		}

	}



/* &sfun/Ginfo-stackable */
	obj_t BGl_z62sfunzf2Ginfozd2stackablez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3382, obj_t BgL_oz00_3383)
	{
		{	/* Globalize/ginfo.sch 261 */
			return
				BGl_sfunzf2Ginfozd2stackablez20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3383));
		}

	}



/* sfun/Ginfo-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2stackablezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_106, obj_t BgL_vz00_107)
	{
		{	/* Globalize/ginfo.sch 262 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_106)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_107), BUNSPEC);
		}

	}



/* &sfun/Ginfo-stackable-set! */
	obj_t BGl_z62sfunzf2Ginfozd2stackablezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3384, obj_t BgL_oz00_3385, obj_t BgL_vz00_3386)
	{
		{	/* Globalize/ginfo.sch 262 */
			return
				BGl_sfunzf2Ginfozd2stackablezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3385), BgL_vz00_3386);
		}

	}



/* sfun/Ginfo-strength */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2strengthz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_108)
	{
		{	/* Globalize/ginfo.sch 263 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_108)))->BgL_strengthz00);
		}

	}



/* &sfun/Ginfo-strength */
	obj_t BGl_z62sfunzf2Ginfozd2strengthz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3387, obj_t BgL_oz00_3388)
	{
		{	/* Globalize/ginfo.sch 263 */
			return
				BGl_sfunzf2Ginfozd2strengthz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3388));
		}

	}



/* sfun/Ginfo-strength-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2strengthzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_109, obj_t BgL_vz00_110)
	{
		{	/* Globalize/ginfo.sch 264 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_109)))->BgL_strengthz00) =
				((obj_t) BgL_vz00_110), BUNSPEC);
		}

	}



/* &sfun/Ginfo-strength-set! */
	obj_t BGl_z62sfunzf2Ginfozd2strengthzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3389, obj_t BgL_oz00_3390, obj_t BgL_vz00_3391)
	{
		{	/* Globalize/ginfo.sch 264 */
			return
				BGl_sfunzf2Ginfozd2strengthzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3390), BgL_vz00_3391);
		}

	}



/* sfun/Ginfo-the-closure-global */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2thezd2closurezd2globalz20zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_111)
	{
		{	/* Globalize/ginfo.sch 265 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_111)))->BgL_thezd2closurezd2globalz00);
		}

	}



/* &sfun/Ginfo-the-closure-global */
	obj_t
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2globalz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3392, obj_t BgL_oz00_3393)
	{
		{	/* Globalize/ginfo.sch 265 */
			return
				BGl_sfunzf2Ginfozd2thezd2closurezd2globalz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3393));
		}

	}



/* sfun/Ginfo-the-closure-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2thezd2closurezd2globalzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_112, obj_t BgL_vz00_113)
	{
		{	/* Globalize/ginfo.sch 266 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_112)))->
					BgL_thezd2closurezd2globalz00) = ((obj_t) BgL_vz00_113), BUNSPEC);
		}

	}



/* &sfun/Ginfo-the-closure-global-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2globalzd2setz12z82zzglobaliza7e_ginfoza7
		(obj_t BgL_envz00_3394, obj_t BgL_oz00_3395, obj_t BgL_vz00_3396)
	{
		{	/* Globalize/ginfo.sch 266 */
			return
				BGl_sfunzf2Ginfozd2thezd2closurezd2globalzd2setz12ze0zzglobaliza7e_ginfoza7
				(((BgL_sfunz00_bglt) BgL_oz00_3395), BgL_vz00_3396);
		}

	}



/* sfun/Ginfo-keys */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2keysz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_114)
	{
		{	/* Globalize/ginfo.sch 267 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_114)))->BgL_keysz00);
		}

	}



/* &sfun/Ginfo-keys */
	obj_t BGl_z62sfunzf2Ginfozd2keysz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3397, obj_t BgL_oz00_3398)
	{
		{	/* Globalize/ginfo.sch 267 */
			return
				BGl_sfunzf2Ginfozd2keysz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3398));
		}

	}



/* sfun/Ginfo-optionals */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2optionalsz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_117)
	{
		{	/* Globalize/ginfo.sch 269 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_117)))->BgL_optionalsz00);
		}

	}



/* &sfun/Ginfo-optionals */
	obj_t BGl_z62sfunzf2Ginfozd2optionalsz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3399, obj_t BgL_oz00_3400)
	{
		{	/* Globalize/ginfo.sch 269 */
			return
				BGl_sfunzf2Ginfozd2optionalsz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3400));
		}

	}



/* sfun/Ginfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2locz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_120)
	{
		{	/* Globalize/ginfo.sch 271 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_120)))->BgL_locz00);
		}

	}



/* &sfun/Ginfo-loc */
	obj_t BGl_z62sfunzf2Ginfozd2locz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3401, obj_t BgL_oz00_3402)
	{
		{	/* Globalize/ginfo.sch 271 */
			return
				BGl_sfunzf2Ginfozd2locz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3402));
		}

	}



/* sfun/Ginfo-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2loczd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_121, obj_t BgL_vz00_122)
	{
		{	/* Globalize/ginfo.sch 272 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_121)))->BgL_locz00) =
				((obj_t) BgL_vz00_122), BUNSPEC);
		}

	}



/* &sfun/Ginfo-loc-set! */
	obj_t BGl_z62sfunzf2Ginfozd2loczd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3403, obj_t BgL_oz00_3404, obj_t BgL_vz00_3405)
	{
		{	/* Globalize/ginfo.sch 272 */
			return
				BGl_sfunzf2Ginfozd2loczd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3404), BgL_vz00_3405);
		}

	}



/* sfun/Ginfo-dsssl-keywords */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2dssslzd2keywordszf2zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_123)
	{
		{	/* Globalize/ginfo.sch 273 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_123)))->BgL_dssslzd2keywordszd2);
		}

	}



/* &sfun/Ginfo-dsssl-keywords */
	obj_t BGl_z62sfunzf2Ginfozd2dssslzd2keywordsz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3406, obj_t BgL_oz00_3407)
	{
		{	/* Globalize/ginfo.sch 273 */
			return
				BGl_sfunzf2Ginfozd2dssslzd2keywordszf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3407));
		}

	}



/* sfun/Ginfo-dsssl-keywords-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2dssslzd2keywordszd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_124, obj_t BgL_vz00_125)
	{
		{	/* Globalize/ginfo.sch 274 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_124)))->BgL_dssslzd2keywordszd2) =
				((obj_t) BgL_vz00_125), BUNSPEC);
		}

	}



/* &sfun/Ginfo-dsssl-keywords-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2dssslzd2keywordszd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t BgL_envz00_3408, obj_t BgL_oz00_3409, obj_t BgL_vz00_3410)
	{
		{	/* Globalize/ginfo.sch 274 */
			return
				BGl_sfunzf2Ginfozd2dssslzd2keywordszd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3409), BgL_vz00_3410);
		}

	}



/* sfun/Ginfo-class */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2classz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_126)
	{
		{	/* Globalize/ginfo.sch 275 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_126)))->BgL_classz00);
		}

	}



/* &sfun/Ginfo-class */
	obj_t BGl_z62sfunzf2Ginfozd2classz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3411, obj_t BgL_oz00_3412)
	{
		{	/* Globalize/ginfo.sch 275 */
			return
				BGl_sfunzf2Ginfozd2classz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3412));
		}

	}



/* sfun/Ginfo-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2classzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_127, obj_t BgL_vz00_128)
	{
		{	/* Globalize/ginfo.sch 276 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_127)))->BgL_classz00) =
				((obj_t) BgL_vz00_128), BUNSPEC);
		}

	}



/* &sfun/Ginfo-class-set! */
	obj_t BGl_z62sfunzf2Ginfozd2classzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3413, obj_t BgL_oz00_3414, obj_t BgL_vz00_3415)
	{
		{	/* Globalize/ginfo.sch 276 */
			return
				BGl_sfunzf2Ginfozd2classzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3414), BgL_vz00_3415);
		}

	}



/* sfun/Ginfo-body */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2bodyz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_129)
	{
		{	/* Globalize/ginfo.sch 277 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_129)))->BgL_bodyz00);
		}

	}



/* &sfun/Ginfo-body */
	obj_t BGl_z62sfunzf2Ginfozd2bodyz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3416, obj_t BgL_oz00_3417)
	{
		{	/* Globalize/ginfo.sch 277 */
			return
				BGl_sfunzf2Ginfozd2bodyz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3417));
		}

	}



/* sfun/Ginfo-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2bodyzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_130, obj_t BgL_vz00_131)
	{
		{	/* Globalize/ginfo.sch 278 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_130)))->BgL_bodyz00) =
				((obj_t) BgL_vz00_131), BUNSPEC);
		}

	}



/* &sfun/Ginfo-body-set! */
	obj_t BGl_z62sfunzf2Ginfozd2bodyzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3418, obj_t BgL_oz00_3419, obj_t BgL_vz00_3420)
	{
		{	/* Globalize/ginfo.sch 278 */
			return
				BGl_sfunzf2Ginfozd2bodyzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3419), BgL_vz00_3420);
		}

	}



/* sfun/Ginfo-args-name */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2argszd2namezf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_132)
	{
		{	/* Globalize/ginfo.sch 279 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_132)))->BgL_argszd2namezd2);
		}

	}



/* &sfun/Ginfo-args-name */
	obj_t BGl_z62sfunzf2Ginfozd2argszd2namez90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3421, obj_t BgL_oz00_3422)
	{
		{	/* Globalize/ginfo.sch 279 */
			return
				BGl_sfunzf2Ginfozd2argszd2namezf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3422));
		}

	}



/* sfun/Ginfo-args */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2argsz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_135)
	{
		{	/* Globalize/ginfo.sch 281 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_135)))->BgL_argsz00);
		}

	}



/* &sfun/Ginfo-args */
	obj_t BGl_z62sfunzf2Ginfozd2argsz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3423, obj_t BgL_oz00_3424)
	{
		{	/* Globalize/ginfo.sch 281 */
			return
				BGl_sfunzf2Ginfozd2argsz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3424));
		}

	}



/* sfun/Ginfo-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2argszd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_136, obj_t BgL_vz00_137)
	{
		{	/* Globalize/ginfo.sch 282 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_136)))->BgL_argsz00) =
				((obj_t) BgL_vz00_137), BUNSPEC);
		}

	}



/* &sfun/Ginfo-args-set! */
	obj_t BGl_z62sfunzf2Ginfozd2argszd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3425, obj_t BgL_oz00_3426, obj_t BgL_vz00_3427)
	{
		{	/* Globalize/ginfo.sch 282 */
			return
				BGl_sfunzf2Ginfozd2argszd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3426), BgL_vz00_3427);
		}

	}



/* sfun/Ginfo-property */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2propertyz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_138)
	{
		{	/* Globalize/ginfo.sch 283 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_138)))->BgL_propertyz00);
		}

	}



/* &sfun/Ginfo-property */
	obj_t BGl_z62sfunzf2Ginfozd2propertyz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3428, obj_t BgL_oz00_3429)
	{
		{	/* Globalize/ginfo.sch 283 */
			return
				BGl_sfunzf2Ginfozd2propertyz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3429));
		}

	}



/* sfun/Ginfo-property-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2propertyzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_139, obj_t BgL_vz00_140)
	{
		{	/* Globalize/ginfo.sch 284 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_139)))->BgL_propertyz00) =
				((obj_t) BgL_vz00_140), BUNSPEC);
		}

	}



/* &sfun/Ginfo-property-set! */
	obj_t BGl_z62sfunzf2Ginfozd2propertyzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3430, obj_t BgL_oz00_3431, obj_t BgL_vz00_3432)
	{
		{	/* Globalize/ginfo.sch 284 */
			return
				BGl_sfunzf2Ginfozd2propertyzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3431), BgL_vz00_3432);
		}

	}



/* sfun/Ginfo-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2argszd2noescapezf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_141)
	{
		{	/* Globalize/ginfo.sch 285 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_141)))->BgL_argszd2noescapezd2);
		}

	}



/* &sfun/Ginfo-args-noescape */
	obj_t BGl_z62sfunzf2Ginfozd2argszd2noescapez90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3433, obj_t BgL_oz00_3434)
	{
		{	/* Globalize/ginfo.sch 285 */
			return
				BGl_sfunzf2Ginfozd2argszd2noescapezf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3434));
		}

	}



/* sfun/Ginfo-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2argszd2noescapezd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_142, obj_t BgL_vz00_143)
	{
		{	/* Globalize/ginfo.sch 286 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_142)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_143), BUNSPEC);
		}

	}



/* &sfun/Ginfo-args-noescape-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2argszd2noescapezd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t BgL_envz00_3435, obj_t BgL_oz00_3436, obj_t BgL_vz00_3437)
	{
		{	/* Globalize/ginfo.sch 286 */
			return
				BGl_sfunzf2Ginfozd2argszd2noescapezd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3436), BgL_vz00_3437);
		}

	}



/* sfun/Ginfo-failsafe */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2failsafez20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_144)
	{
		{	/* Globalize/ginfo.sch 287 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_144)))->BgL_failsafez00);
		}

	}



/* &sfun/Ginfo-failsafe */
	obj_t BGl_z62sfunzf2Ginfozd2failsafez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3438, obj_t BgL_oz00_3439)
	{
		{	/* Globalize/ginfo.sch 287 */
			return
				BGl_sfunzf2Ginfozd2failsafez20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3439));
		}

	}



/* sfun/Ginfo-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2failsafezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_145, obj_t BgL_vz00_146)
	{
		{	/* Globalize/ginfo.sch 288 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_145)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_146), BUNSPEC);
		}

	}



/* &sfun/Ginfo-failsafe-set! */
	obj_t BGl_z62sfunzf2Ginfozd2failsafezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3440, obj_t BgL_oz00_3441, obj_t BgL_vz00_3442)
	{
		{	/* Globalize/ginfo.sch 288 */
			return
				BGl_sfunzf2Ginfozd2failsafezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3441), BgL_vz00_3442);
		}

	}



/* sfun/Ginfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2effectz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_147)
	{
		{	/* Globalize/ginfo.sch 289 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_147)))->BgL_effectz00);
		}

	}



/* &sfun/Ginfo-effect */
	obj_t BGl_z62sfunzf2Ginfozd2effectz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3443, obj_t BgL_oz00_3444)
	{
		{	/* Globalize/ginfo.sch 289 */
			return
				BGl_sfunzf2Ginfozd2effectz20zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3444));
		}

	}



/* sfun/Ginfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2effectzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_148, obj_t BgL_vz00_149)
	{
		{	/* Globalize/ginfo.sch 290 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_148)))->BgL_effectz00) =
				((obj_t) BgL_vz00_149), BUNSPEC);
		}

	}



/* &sfun/Ginfo-effect-set! */
	obj_t BGl_z62sfunzf2Ginfozd2effectzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3445, obj_t BgL_oz00_3446, obj_t BgL_vz00_3447)
	{
		{	/* Globalize/ginfo.sch 290 */
			return
				BGl_sfunzf2Ginfozd2effectzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3446), BgL_vz00_3447);
		}

	}



/* sfun/Ginfo-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2thezd2closurezf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_150)
	{
		{	/* Globalize/ginfo.sch 291 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_150)))->BgL_thezd2closurezd2);
		}

	}



/* &sfun/Ginfo-the-closure */
	obj_t BGl_z62sfunzf2Ginfozd2thezd2closurez90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3448, obj_t BgL_oz00_3449)
	{
		{	/* Globalize/ginfo.sch 291 */
			return
				BGl_sfunzf2Ginfozd2thezd2closurezf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3449));
		}

	}



/* sfun/Ginfo-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2thezd2closurezd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_151, obj_t BgL_vz00_152)
	{
		{	/* Globalize/ginfo.sch 292 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_151)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_152), BUNSPEC);
		}

	}



/* &sfun/Ginfo-the-closure-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2thezd2closurezd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3450, obj_t BgL_oz00_3451, obj_t BgL_vz00_3452)
	{
		{	/* Globalize/ginfo.sch 292 */
			return
				BGl_sfunzf2Ginfozd2thezd2closurezd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3451), BgL_vz00_3452);
		}

	}



/* sfun/Ginfo-top? */
	BGL_EXPORTED_DEF bool_t
		BGl_sfunzf2Ginfozd2topzf3zd3zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_153)
	{
		{	/* Globalize/ginfo.sch 293 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_153)))->BgL_topzf3zf3);
		}

	}



/* &sfun/Ginfo-top? */
	obj_t BGl_z62sfunzf2Ginfozd2topzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3453, obj_t BgL_oz00_3454)
	{
		{	/* Globalize/ginfo.sch 293 */
			return
				BBOOL(BGl_sfunzf2Ginfozd2topzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_sfunz00_bglt) BgL_oz00_3454)));
		}

	}



/* sfun/Ginfo-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2topzf3zd2setz12z13zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_154, bool_t BgL_vz00_155)
	{
		{	/* Globalize/ginfo.sch 294 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_154)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_155), BUNSPEC);
		}

	}



/* &sfun/Ginfo-top?-set! */
	obj_t BGl_z62sfunzf2Ginfozd2topzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3455, obj_t BgL_oz00_3456, obj_t BgL_vz00_3457)
	{
		{	/* Globalize/ginfo.sch 294 */
			return
				BGl_sfunzf2Ginfozd2topzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3456), CBOOL(BgL_vz00_3457));
		}

	}



/* sfun/Ginfo-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2stackzd2allocatorzf2zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_156)
	{
		{	/* Globalize/ginfo.sch 295 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_156)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &sfun/Ginfo-stack-allocator */
	obj_t BGl_z62sfunzf2Ginfozd2stackzd2allocatorz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3458, obj_t BgL_oz00_3459)
	{
		{	/* Globalize/ginfo.sch 295 */
			return
				BGl_sfunzf2Ginfozd2stackzd2allocatorzf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3459));
		}

	}



/* sfun/Ginfo-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2stackzd2allocatorzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_157, obj_t BgL_vz00_158)
	{
		{	/* Globalize/ginfo.sch 296 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_157)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_158), BUNSPEC);
		}

	}



/* &sfun/Ginfo-stack-allocator-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2stackzd2allocatorzd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t BgL_envz00_3460, obj_t BgL_oz00_3461, obj_t BgL_vz00_3462)
	{
		{	/* Globalize/ginfo.sch 296 */
			return
				BGl_sfunzf2Ginfozd2stackzd2allocatorzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3461), BgL_vz00_3462);
		}

	}



/* sfun/Ginfo-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2predicatezd2ofzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_159)
	{
		{	/* Globalize/ginfo.sch 297 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_159)))->BgL_predicatezd2ofzd2);
		}

	}



/* &sfun/Ginfo-predicate-of */
	obj_t BGl_z62sfunzf2Ginfozd2predicatezd2ofz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3463, obj_t BgL_oz00_3464)
	{
		{	/* Globalize/ginfo.sch 297 */
			return
				BGl_sfunzf2Ginfozd2predicatezd2ofzf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3464));
		}

	}



/* sfun/Ginfo-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2predicatezd2ofzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_160, obj_t BgL_vz00_161)
	{
		{	/* Globalize/ginfo.sch 298 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_160)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_161), BUNSPEC);
		}

	}



/* &sfun/Ginfo-predicate-of-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2predicatezd2ofzd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3465, obj_t BgL_oz00_3466, obj_t BgL_vz00_3467)
	{
		{	/* Globalize/ginfo.sch 298 */
			return
				BGl_sfunzf2Ginfozd2predicatezd2ofzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3466), BgL_vz00_3467);
		}

	}



/* sfun/Ginfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2sidezd2effectzf2zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_162)
	{
		{	/* Globalize/ginfo.sch 299 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_162)))->BgL_sidezd2effectzd2);
		}

	}



/* &sfun/Ginfo-side-effect */
	obj_t BGl_z62sfunzf2Ginfozd2sidezd2effectz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3468, obj_t BgL_oz00_3469)
	{
		{	/* Globalize/ginfo.sch 299 */
			return
				BGl_sfunzf2Ginfozd2sidezd2effectzf2zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3469));
		}

	}



/* sfun/Ginfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Ginfozd2sidezd2effectzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sfunz00_bglt BgL_oz00_163, obj_t BgL_vz00_164)
	{
		{	/* Globalize/ginfo.sch 300 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_163)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_164), BUNSPEC);
		}

	}



/* &sfun/Ginfo-side-effect-set! */
	obj_t
		BGl_z62sfunzf2Ginfozd2sidezd2effectzd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3470, obj_t BgL_oz00_3471, obj_t BgL_vz00_3472)
	{
		{	/* Globalize/ginfo.sch 300 */
			return
				BGl_sfunzf2Ginfozd2sidezd2effectzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sfunz00_bglt) BgL_oz00_3471), BgL_vz00_3472);
		}

	}



/* sfun/Ginfo-arity */
	BGL_EXPORTED_DEF long
		BGl_sfunzf2Ginfozd2arityz20zzglobaliza7e_ginfoza7(BgL_sfunz00_bglt
		BgL_oz00_165)
	{
		{	/* Globalize/ginfo.sch 301 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_165)))->BgL_arityz00);
		}

	}



/* &sfun/Ginfo-arity */
	obj_t BGl_z62sfunzf2Ginfozd2arityz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3473, obj_t BgL_oz00_3474)
	{
		{	/* Globalize/ginfo.sch 301 */
			return
				BINT(BGl_sfunzf2Ginfozd2arityz20zzglobaliza7e_ginfoza7(
					((BgL_sfunz00_bglt) BgL_oz00_3474)));
		}

	}



/* make-svar/Ginfo */
	BGL_EXPORTED_DEF BgL_svarz00_bglt
		BGl_makezd2svarzf2Ginfoz20zzglobaliza7e_ginfoza7(obj_t BgL_loc1264z00_168,
		bool_t BgL_kapturedzf31265zf3_169, long BgL_freezd2mark1266zd2_170,
		long BgL_mark1267z00_171, bool_t BgL_celledzf31268zf3_172,
		bool_t BgL_stackable1269z00_173)
	{
		{	/* Globalize/ginfo.sch 305 */
			{	/* Globalize/ginfo.sch 305 */
				BgL_svarz00_bglt BgL_new1231z00_4239;

				{	/* Globalize/ginfo.sch 305 */
					BgL_svarz00_bglt BgL_tmp1229z00_4240;
					BgL_svarzf2ginfozf2_bglt BgL_wide1230z00_4241;

					{
						BgL_svarz00_bglt BgL_auxz00_5337;

						{	/* Globalize/ginfo.sch 305 */
							BgL_svarz00_bglt BgL_new1228z00_4242;

							BgL_new1228z00_4242 =
								((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_svarz00_bgl))));
							{	/* Globalize/ginfo.sch 305 */
								long BgL_arg1437z00_4243;

								BgL_arg1437z00_4243 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1228z00_4242),
									BgL_arg1437z00_4243);
							}
							{	/* Globalize/ginfo.sch 305 */
								BgL_objectz00_bglt BgL_tmpz00_5342;

								BgL_tmpz00_5342 = ((BgL_objectz00_bglt) BgL_new1228z00_4242);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5342, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1228z00_4242);
							BgL_auxz00_5337 = BgL_new1228z00_4242;
						}
						BgL_tmp1229z00_4240 = ((BgL_svarz00_bglt) BgL_auxz00_5337);
					}
					BgL_wide1230z00_4241 =
						((BgL_svarzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_svarzf2ginfozf2_bgl))));
					{	/* Globalize/ginfo.sch 305 */
						obj_t BgL_auxz00_5350;
						BgL_objectz00_bglt BgL_tmpz00_5348;

						BgL_auxz00_5350 = ((obj_t) BgL_wide1230z00_4241);
						BgL_tmpz00_5348 = ((BgL_objectz00_bglt) BgL_tmp1229z00_4240);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5348, BgL_auxz00_5350);
					}
					((BgL_objectz00_bglt) BgL_tmp1229z00_4240);
					{	/* Globalize/ginfo.sch 305 */
						long BgL_arg1434z00_4244;

						BgL_arg1434z00_4244 =
							BGL_CLASS_NUM(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1229z00_4240), BgL_arg1434z00_4244);
					}
					BgL_new1231z00_4239 = ((BgL_svarz00_bglt) BgL_tmp1229z00_4240);
				}
				((((BgL_svarz00_bglt) COBJECT(
								((BgL_svarz00_bglt) BgL_new1231z00_4239)))->BgL_locz00) =
					((obj_t) BgL_loc1264z00_168), BUNSPEC);
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_5360;

					{
						obj_t BgL_auxz00_5361;

						{	/* Globalize/ginfo.sch 305 */
							BgL_objectz00_bglt BgL_tmpz00_5362;

							BgL_tmpz00_5362 = ((BgL_objectz00_bglt) BgL_new1231z00_4239);
							BgL_auxz00_5361 = BGL_OBJECT_WIDENING(BgL_tmpz00_5362);
						}
						BgL_auxz00_5360 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5361);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5360))->
							BgL_kapturedzf3zf3) =
						((bool_t) BgL_kapturedzf31265zf3_169), BUNSPEC);
				}
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_5367;

					{
						obj_t BgL_auxz00_5368;

						{	/* Globalize/ginfo.sch 305 */
							BgL_objectz00_bglt BgL_tmpz00_5369;

							BgL_tmpz00_5369 = ((BgL_objectz00_bglt) BgL_new1231z00_4239);
							BgL_auxz00_5368 = BGL_OBJECT_WIDENING(BgL_tmpz00_5369);
						}
						BgL_auxz00_5367 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5368);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5367))->
							BgL_freezd2markzd2) =
						((long) BgL_freezd2mark1266zd2_170), BUNSPEC);
				}
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_5374;

					{
						obj_t BgL_auxz00_5375;

						{	/* Globalize/ginfo.sch 305 */
							BgL_objectz00_bglt BgL_tmpz00_5376;

							BgL_tmpz00_5376 = ((BgL_objectz00_bglt) BgL_new1231z00_4239);
							BgL_auxz00_5375 = BGL_OBJECT_WIDENING(BgL_tmpz00_5376);
						}
						BgL_auxz00_5374 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5375);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5374))->
							BgL_markz00) = ((long) BgL_mark1267z00_171), BUNSPEC);
				}
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_5381;

					{
						obj_t BgL_auxz00_5382;

						{	/* Globalize/ginfo.sch 305 */
							BgL_objectz00_bglt BgL_tmpz00_5383;

							BgL_tmpz00_5383 = ((BgL_objectz00_bglt) BgL_new1231z00_4239);
							BgL_auxz00_5382 = BGL_OBJECT_WIDENING(BgL_tmpz00_5383);
						}
						BgL_auxz00_5381 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5382);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5381))->
							BgL_celledzf3zf3) = ((bool_t) BgL_celledzf31268zf3_172), BUNSPEC);
				}
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_5388;

					{
						obj_t BgL_auxz00_5389;

						{	/* Globalize/ginfo.sch 305 */
							BgL_objectz00_bglt BgL_tmpz00_5390;

							BgL_tmpz00_5390 = ((BgL_objectz00_bglt) BgL_new1231z00_4239);
							BgL_auxz00_5389 = BGL_OBJECT_WIDENING(BgL_tmpz00_5390);
						}
						BgL_auxz00_5388 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5389);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5388))->
							BgL_stackablez00) = ((bool_t) BgL_stackable1269z00_173), BUNSPEC);
				}
				return BgL_new1231z00_4239;
			}
		}

	}



/* &make-svar/Ginfo */
	BgL_svarz00_bglt BGl_z62makezd2svarzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3475, obj_t BgL_loc1264z00_3476,
		obj_t BgL_kapturedzf31265zf3_3477, obj_t BgL_freezd2mark1266zd2_3478,
		obj_t BgL_mark1267z00_3479, obj_t BgL_celledzf31268zf3_3480,
		obj_t BgL_stackable1269z00_3481)
	{
		{	/* Globalize/ginfo.sch 305 */
			return
				BGl_makezd2svarzf2Ginfoz20zzglobaliza7e_ginfoza7(BgL_loc1264z00_3476,
				CBOOL(BgL_kapturedzf31265zf3_3477),
				(long) CINT(BgL_freezd2mark1266zd2_3478),
				(long) CINT(BgL_mark1267z00_3479),
				CBOOL(BgL_celledzf31268zf3_3480), CBOOL(BgL_stackable1269z00_3481));
		}

	}



/* svar/Ginfo? */
	BGL_EXPORTED_DEF bool_t BGl_svarzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t
		BgL_objz00_174)
	{
		{	/* Globalize/ginfo.sch 306 */
			{	/* Globalize/ginfo.sch 306 */
				obj_t BgL_classz00_4245;

				BgL_classz00_4245 = BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
				if (BGL_OBJECTP(BgL_objz00_174))
					{	/* Globalize/ginfo.sch 306 */
						BgL_objectz00_bglt BgL_arg1807z00_4246;

						BgL_arg1807z00_4246 = (BgL_objectz00_bglt) (BgL_objz00_174);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Globalize/ginfo.sch 306 */
								long BgL_idxz00_4247;

								BgL_idxz00_4247 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4246);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4247 + 3L)) == BgL_classz00_4245);
							}
						else
							{	/* Globalize/ginfo.sch 306 */
								bool_t BgL_res2078z00_4250;

								{	/* Globalize/ginfo.sch 306 */
									obj_t BgL_oclassz00_4251;

									{	/* Globalize/ginfo.sch 306 */
										obj_t BgL_arg1815z00_4252;
										long BgL_arg1816z00_4253;

										BgL_arg1815z00_4252 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Globalize/ginfo.sch 306 */
											long BgL_arg1817z00_4254;

											BgL_arg1817z00_4254 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4246);
											BgL_arg1816z00_4253 = (BgL_arg1817z00_4254 - OBJECT_TYPE);
										}
										BgL_oclassz00_4251 =
											VECTOR_REF(BgL_arg1815z00_4252, BgL_arg1816z00_4253);
									}
									{	/* Globalize/ginfo.sch 306 */
										bool_t BgL__ortest_1115z00_4255;

										BgL__ortest_1115z00_4255 =
											(BgL_classz00_4245 == BgL_oclassz00_4251);
										if (BgL__ortest_1115z00_4255)
											{	/* Globalize/ginfo.sch 306 */
												BgL_res2078z00_4250 = BgL__ortest_1115z00_4255;
											}
										else
											{	/* Globalize/ginfo.sch 306 */
												long BgL_odepthz00_4256;

												{	/* Globalize/ginfo.sch 306 */
													obj_t BgL_arg1804z00_4257;

													BgL_arg1804z00_4257 = (BgL_oclassz00_4251);
													BgL_odepthz00_4256 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4257);
												}
												if ((3L < BgL_odepthz00_4256))
													{	/* Globalize/ginfo.sch 306 */
														obj_t BgL_arg1802z00_4258;

														{	/* Globalize/ginfo.sch 306 */
															obj_t BgL_arg1803z00_4259;

															BgL_arg1803z00_4259 = (BgL_oclassz00_4251);
															BgL_arg1802z00_4258 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4259,
																3L);
														}
														BgL_res2078z00_4250 =
															(BgL_arg1802z00_4258 == BgL_classz00_4245);
													}
												else
													{	/* Globalize/ginfo.sch 306 */
														BgL_res2078z00_4250 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2078z00_4250;
							}
					}
				else
					{	/* Globalize/ginfo.sch 306 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &svar/Ginfo? */
	obj_t BGl_z62svarzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3482,
		obj_t BgL_objz00_3483)
	{
		{	/* Globalize/ginfo.sch 306 */
			return
				BBOOL(BGl_svarzf2Ginfozf3z01zzglobaliza7e_ginfoza7(BgL_objz00_3483));
		}

	}



/* svar/Ginfo-nil */
	BGL_EXPORTED_DEF BgL_svarz00_bglt
		BGl_svarzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.sch 307 */
			{	/* Globalize/ginfo.sch 307 */
				obj_t BgL_classz00_2741;

				BgL_classz00_2741 = BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7;
				{	/* Globalize/ginfo.sch 307 */
					obj_t BgL__ortest_1117z00_2742;

					BgL__ortest_1117z00_2742 = BGL_CLASS_NIL(BgL_classz00_2741);
					if (CBOOL(BgL__ortest_1117z00_2742))
						{	/* Globalize/ginfo.sch 307 */
							return ((BgL_svarz00_bglt) BgL__ortest_1117z00_2742);
						}
					else
						{	/* Globalize/ginfo.sch 307 */
							return
								((BgL_svarz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2741));
						}
				}
			}
		}

	}



/* &svar/Ginfo-nil */
	BgL_svarz00_bglt BGl_z62svarzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3484)
	{
		{	/* Globalize/ginfo.sch 307 */
			return BGl_svarzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7();
		}

	}



/* svar/Ginfo-stackable */
	BGL_EXPORTED_DEF bool_t
		BGl_svarzf2Ginfozd2stackablez20zzglobaliza7e_ginfoza7(BgL_svarz00_bglt
		BgL_oz00_175)
	{
		{	/* Globalize/ginfo.sch 308 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5432;

				{
					obj_t BgL_auxz00_5433;

					{	/* Globalize/ginfo.sch 308 */
						BgL_objectz00_bglt BgL_tmpz00_5434;

						BgL_tmpz00_5434 = ((BgL_objectz00_bglt) BgL_oz00_175);
						BgL_auxz00_5433 = BGL_OBJECT_WIDENING(BgL_tmpz00_5434);
					}
					BgL_auxz00_5432 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5433);
				}
				return
					(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5432))->
					BgL_stackablez00);
			}
		}

	}



/* &svar/Ginfo-stackable */
	obj_t BGl_z62svarzf2Ginfozd2stackablez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3485, obj_t BgL_oz00_3486)
	{
		{	/* Globalize/ginfo.sch 308 */
			return
				BBOOL(BGl_svarzf2Ginfozd2stackablez20zzglobaliza7e_ginfoza7(
					((BgL_svarz00_bglt) BgL_oz00_3486)));
		}

	}



/* svar/Ginfo-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Ginfozd2stackablezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_svarz00_bglt BgL_oz00_176, bool_t BgL_vz00_177)
	{
		{	/* Globalize/ginfo.sch 309 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5442;

				{
					obj_t BgL_auxz00_5443;

					{	/* Globalize/ginfo.sch 309 */
						BgL_objectz00_bglt BgL_tmpz00_5444;

						BgL_tmpz00_5444 = ((BgL_objectz00_bglt) BgL_oz00_176);
						BgL_auxz00_5443 = BGL_OBJECT_WIDENING(BgL_tmpz00_5444);
					}
					BgL_auxz00_5442 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5443);
				}
				return
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5442))->
						BgL_stackablez00) = ((bool_t) BgL_vz00_177), BUNSPEC);
			}
		}

	}



/* &svar/Ginfo-stackable-set! */
	obj_t BGl_z62svarzf2Ginfozd2stackablezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3487, obj_t BgL_oz00_3488, obj_t BgL_vz00_3489)
	{
		{	/* Globalize/ginfo.sch 309 */
			return
				BGl_svarzf2Ginfozd2stackablezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_svarz00_bglt) BgL_oz00_3488), CBOOL(BgL_vz00_3489));
		}

	}



/* svar/Ginfo-celled? */
	BGL_EXPORTED_DEF bool_t
		BGl_svarzf2Ginfozd2celledzf3zd3zzglobaliza7e_ginfoza7(BgL_svarz00_bglt
		BgL_oz00_178)
	{
		{	/* Globalize/ginfo.sch 310 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5452;

				{
					obj_t BgL_auxz00_5453;

					{	/* Globalize/ginfo.sch 310 */
						BgL_objectz00_bglt BgL_tmpz00_5454;

						BgL_tmpz00_5454 = ((BgL_objectz00_bglt) BgL_oz00_178);
						BgL_auxz00_5453 = BGL_OBJECT_WIDENING(BgL_tmpz00_5454);
					}
					BgL_auxz00_5452 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5453);
				}
				return
					(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5452))->
					BgL_celledzf3zf3);
			}
		}

	}



/* &svar/Ginfo-celled? */
	obj_t BGl_z62svarzf2Ginfozd2celledzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3490, obj_t BgL_oz00_3491)
	{
		{	/* Globalize/ginfo.sch 310 */
			return
				BBOOL(BGl_svarzf2Ginfozd2celledzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_svarz00_bglt) BgL_oz00_3491)));
		}

	}



/* svar/Ginfo-celled?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Ginfozd2celledzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_svarz00_bglt BgL_oz00_179, bool_t BgL_vz00_180)
	{
		{	/* Globalize/ginfo.sch 311 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5462;

				{
					obj_t BgL_auxz00_5463;

					{	/* Globalize/ginfo.sch 311 */
						BgL_objectz00_bglt BgL_tmpz00_5464;

						BgL_tmpz00_5464 = ((BgL_objectz00_bglt) BgL_oz00_179);
						BgL_auxz00_5463 = BGL_OBJECT_WIDENING(BgL_tmpz00_5464);
					}
					BgL_auxz00_5462 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5463);
				}
				return
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5462))->
						BgL_celledzf3zf3) = ((bool_t) BgL_vz00_180), BUNSPEC);
			}
		}

	}



/* &svar/Ginfo-celled?-set! */
	obj_t BGl_z62svarzf2Ginfozd2celledzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3492, obj_t BgL_oz00_3493, obj_t BgL_vz00_3494)
	{
		{	/* Globalize/ginfo.sch 311 */
			return
				BGl_svarzf2Ginfozd2celledzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_svarz00_bglt) BgL_oz00_3493), CBOOL(BgL_vz00_3494));
		}

	}



/* svar/Ginfo-mark */
	BGL_EXPORTED_DEF long
		BGl_svarzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(BgL_svarz00_bglt
		BgL_oz00_181)
	{
		{	/* Globalize/ginfo.sch 312 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5472;

				{
					obj_t BgL_auxz00_5473;

					{	/* Globalize/ginfo.sch 312 */
						BgL_objectz00_bglt BgL_tmpz00_5474;

						BgL_tmpz00_5474 = ((BgL_objectz00_bglt) BgL_oz00_181);
						BgL_auxz00_5473 = BGL_OBJECT_WIDENING(BgL_tmpz00_5474);
					}
					BgL_auxz00_5472 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5473);
				}
				return
					(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5472))->BgL_markz00);
			}
		}

	}



/* &svar/Ginfo-mark */
	obj_t BGl_z62svarzf2Ginfozd2markz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3495, obj_t BgL_oz00_3496)
	{
		{	/* Globalize/ginfo.sch 312 */
			return
				BINT(BGl_svarzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(
					((BgL_svarz00_bglt) BgL_oz00_3496)));
		}

	}



/* svar/Ginfo-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_svarz00_bglt
		BgL_oz00_182, long BgL_vz00_183)
	{
		{	/* Globalize/ginfo.sch 313 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5482;

				{
					obj_t BgL_auxz00_5483;

					{	/* Globalize/ginfo.sch 313 */
						BgL_objectz00_bglt BgL_tmpz00_5484;

						BgL_tmpz00_5484 = ((BgL_objectz00_bglt) BgL_oz00_182);
						BgL_auxz00_5483 = BGL_OBJECT_WIDENING(BgL_tmpz00_5484);
					}
					BgL_auxz00_5482 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5483);
				}
				return
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5482))->
						BgL_markz00) = ((long) BgL_vz00_183), BUNSPEC);
		}}

	}



/* &svar/Ginfo-mark-set! */
	obj_t BGl_z62svarzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3497, obj_t BgL_oz00_3498, obj_t BgL_vz00_3499)
	{
		{	/* Globalize/ginfo.sch 313 */
			return
				BGl_svarzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_svarz00_bglt) BgL_oz00_3498), (long) CINT(BgL_vz00_3499));
		}

	}



/* svar/Ginfo-free-mark */
	BGL_EXPORTED_DEF long
		BGl_svarzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(BgL_svarz00_bglt
		BgL_oz00_184)
	{
		{	/* Globalize/ginfo.sch 314 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5492;

				{
					obj_t BgL_auxz00_5493;

					{	/* Globalize/ginfo.sch 314 */
						BgL_objectz00_bglt BgL_tmpz00_5494;

						BgL_tmpz00_5494 = ((BgL_objectz00_bglt) BgL_oz00_184);
						BgL_auxz00_5493 = BGL_OBJECT_WIDENING(BgL_tmpz00_5494);
					}
					BgL_auxz00_5492 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5493);
				}
				return
					(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5492))->
					BgL_freezd2markzd2);
			}
		}

	}



/* &svar/Ginfo-free-mark */
	obj_t BGl_z62svarzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3500, obj_t BgL_oz00_3501)
	{
		{	/* Globalize/ginfo.sch 314 */
			return
				BINT(BGl_svarzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(
					((BgL_svarz00_bglt) BgL_oz00_3501)));
		}

	}



/* svar/Ginfo-free-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_svarz00_bglt BgL_oz00_185, long BgL_vz00_186)
	{
		{	/* Globalize/ginfo.sch 315 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5502;

				{
					obj_t BgL_auxz00_5503;

					{	/* Globalize/ginfo.sch 315 */
						BgL_objectz00_bglt BgL_tmpz00_5504;

						BgL_tmpz00_5504 = ((BgL_objectz00_bglt) BgL_oz00_185);
						BgL_auxz00_5503 = BGL_OBJECT_WIDENING(BgL_tmpz00_5504);
					}
					BgL_auxz00_5502 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5503);
				}
				return
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5502))->
						BgL_freezd2markzd2) = ((long) BgL_vz00_186), BUNSPEC);
		}}

	}



/* &svar/Ginfo-free-mark-set! */
	obj_t
		BGl_z62svarzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3502, obj_t BgL_oz00_3503, obj_t BgL_vz00_3504)
	{
		{	/* Globalize/ginfo.sch 315 */
			return
				BGl_svarzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_svarz00_bglt) BgL_oz00_3503), (long) CINT(BgL_vz00_3504));
		}

	}



/* svar/Ginfo-kaptured? */
	BGL_EXPORTED_DEF bool_t
		BGl_svarzf2Ginfozd2kapturedzf3zd3zzglobaliza7e_ginfoza7(BgL_svarz00_bglt
		BgL_oz00_187)
	{
		{	/* Globalize/ginfo.sch 316 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5512;

				{
					obj_t BgL_auxz00_5513;

					{	/* Globalize/ginfo.sch 316 */
						BgL_objectz00_bglt BgL_tmpz00_5514;

						BgL_tmpz00_5514 = ((BgL_objectz00_bglt) BgL_oz00_187);
						BgL_auxz00_5513 = BGL_OBJECT_WIDENING(BgL_tmpz00_5514);
					}
					BgL_auxz00_5512 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5513);
				}
				return
					(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5512))->
					BgL_kapturedzf3zf3);
			}
		}

	}



/* &svar/Ginfo-kaptured? */
	obj_t BGl_z62svarzf2Ginfozd2kapturedzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3505, obj_t BgL_oz00_3506)
	{
		{	/* Globalize/ginfo.sch 316 */
			return
				BBOOL(BGl_svarzf2Ginfozd2kapturedzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_svarz00_bglt) BgL_oz00_3506)));
		}

	}



/* svar/Ginfo-kaptured?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Ginfozd2kapturedzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_svarz00_bglt BgL_oz00_188, bool_t BgL_vz00_189)
	{
		{	/* Globalize/ginfo.sch 317 */
			{
				BgL_svarzf2ginfozf2_bglt BgL_auxz00_5522;

				{
					obj_t BgL_auxz00_5523;

					{	/* Globalize/ginfo.sch 317 */
						BgL_objectz00_bglt BgL_tmpz00_5524;

						BgL_tmpz00_5524 = ((BgL_objectz00_bglt) BgL_oz00_188);
						BgL_auxz00_5523 = BGL_OBJECT_WIDENING(BgL_tmpz00_5524);
					}
					BgL_auxz00_5522 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_5523);
				}
				return
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5522))->
						BgL_kapturedzf3zf3) = ((bool_t) BgL_vz00_189), BUNSPEC);
			}
		}

	}



/* &svar/Ginfo-kaptured?-set! */
	obj_t
		BGl_z62svarzf2Ginfozd2kapturedzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3507, obj_t BgL_oz00_3508, obj_t BgL_vz00_3509)
	{
		{	/* Globalize/ginfo.sch 317 */
			return
				BGl_svarzf2Ginfozd2kapturedzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_svarz00_bglt) BgL_oz00_3508), CBOOL(BgL_vz00_3509));
		}

	}



/* svar/Ginfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Ginfozd2locz20zzglobaliza7e_ginfoza7(BgL_svarz00_bglt
		BgL_oz00_190)
	{
		{	/* Globalize/ginfo.sch 318 */
			return
				(((BgL_svarz00_bglt) COBJECT(
						((BgL_svarz00_bglt) BgL_oz00_190)))->BgL_locz00);
		}

	}



/* &svar/Ginfo-loc */
	obj_t BGl_z62svarzf2Ginfozd2locz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3510, obj_t BgL_oz00_3511)
	{
		{	/* Globalize/ginfo.sch 318 */
			return
				BGl_svarzf2Ginfozd2locz20zzglobaliza7e_ginfoza7(
				((BgL_svarz00_bglt) BgL_oz00_3511));
		}

	}



/* svar/Ginfo-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Ginfozd2loczd2setz12ze0zzglobaliza7e_ginfoza7(BgL_svarz00_bglt
		BgL_oz00_191, obj_t BgL_vz00_192)
	{
		{	/* Globalize/ginfo.sch 319 */
			return
				((((BgL_svarz00_bglt) COBJECT(
							((BgL_svarz00_bglt) BgL_oz00_191)))->BgL_locz00) =
				((obj_t) BgL_vz00_192), BUNSPEC);
		}

	}



/* &svar/Ginfo-loc-set! */
	obj_t BGl_z62svarzf2Ginfozd2loczd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3512, obj_t BgL_oz00_3513, obj_t BgL_vz00_3514)
	{
		{	/* Globalize/ginfo.sch 319 */
			return
				BGl_svarzf2Ginfozd2loczd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_svarz00_bglt) BgL_oz00_3513), BgL_vz00_3514);
		}

	}



/* make-sexit/Ginfo */
	BGL_EXPORTED_DEF BgL_sexitz00_bglt
		BGl_makezd2sexitzf2Ginfoz20zzglobaliza7e_ginfoza7(obj_t
		BgL_handler1257z00_193, bool_t BgL_detachedzf31258zf3_194,
		bool_t BgL_gzf31259zf3_195, bool_t BgL_kapturedzf31260zf3_196,
		long BgL_freezd2mark1261zd2_197, long BgL_mark1262z00_198)
	{
		{	/* Globalize/ginfo.sch 322 */
			{	/* Globalize/ginfo.sch 322 */
				BgL_sexitz00_bglt BgL_new1235z00_4260;

				{	/* Globalize/ginfo.sch 322 */
					BgL_sexitz00_bglt BgL_tmp1233z00_4261;
					BgL_sexitzf2ginfozf2_bglt BgL_wide1234z00_4262;

					{
						BgL_sexitz00_bglt BgL_auxz00_5540;

						{	/* Globalize/ginfo.sch 322 */
							BgL_sexitz00_bglt BgL_new1232z00_4263;

							BgL_new1232z00_4263 =
								((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sexitz00_bgl))));
							{	/* Globalize/ginfo.sch 322 */
								long BgL_arg1453z00_4264;

								BgL_arg1453z00_4264 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1232z00_4263),
									BgL_arg1453z00_4264);
							}
							{	/* Globalize/ginfo.sch 322 */
								BgL_objectz00_bglt BgL_tmpz00_5545;

								BgL_tmpz00_5545 = ((BgL_objectz00_bglt) BgL_new1232z00_4263);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5545, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1232z00_4263);
							BgL_auxz00_5540 = BgL_new1232z00_4263;
						}
						BgL_tmp1233z00_4261 = ((BgL_sexitz00_bglt) BgL_auxz00_5540);
					}
					BgL_wide1234z00_4262 =
						((BgL_sexitzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sexitzf2ginfozf2_bgl))));
					{	/* Globalize/ginfo.sch 322 */
						obj_t BgL_auxz00_5553;
						BgL_objectz00_bglt BgL_tmpz00_5551;

						BgL_auxz00_5553 = ((obj_t) BgL_wide1234z00_4262);
						BgL_tmpz00_5551 = ((BgL_objectz00_bglt) BgL_tmp1233z00_4261);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5551, BgL_auxz00_5553);
					}
					((BgL_objectz00_bglt) BgL_tmp1233z00_4261);
					{	/* Globalize/ginfo.sch 322 */
						long BgL_arg1448z00_4265;

						BgL_arg1448z00_4265 =
							BGL_CLASS_NUM(BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1233z00_4261), BgL_arg1448z00_4265);
					}
					BgL_new1235z00_4260 = ((BgL_sexitz00_bglt) BgL_tmp1233z00_4261);
				}
				((((BgL_sexitz00_bglt) COBJECT(
								((BgL_sexitz00_bglt) BgL_new1235z00_4260)))->BgL_handlerz00) =
					((obj_t) BgL_handler1257z00_193), BUNSPEC);
				((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt)
									BgL_new1235z00_4260)))->BgL_detachedzf3zf3) =
					((bool_t) BgL_detachedzf31258zf3_194), BUNSPEC);
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5565;

					{
						obj_t BgL_auxz00_5566;

						{	/* Globalize/ginfo.sch 322 */
							BgL_objectz00_bglt BgL_tmpz00_5567;

							BgL_tmpz00_5567 = ((BgL_objectz00_bglt) BgL_new1235z00_4260);
							BgL_auxz00_5566 = BGL_OBJECT_WIDENING(BgL_tmpz00_5567);
						}
						BgL_auxz00_5565 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5566);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5565))->
							BgL_gzf3zf3) = ((bool_t) BgL_gzf31259zf3_195), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5572;

					{
						obj_t BgL_auxz00_5573;

						{	/* Globalize/ginfo.sch 322 */
							BgL_objectz00_bglt BgL_tmpz00_5574;

							BgL_tmpz00_5574 = ((BgL_objectz00_bglt) BgL_new1235z00_4260);
							BgL_auxz00_5573 = BGL_OBJECT_WIDENING(BgL_tmpz00_5574);
						}
						BgL_auxz00_5572 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5573);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5572))->
							BgL_kapturedzf3zf3) =
						((bool_t) BgL_kapturedzf31260zf3_196), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5579;

					{
						obj_t BgL_auxz00_5580;

						{	/* Globalize/ginfo.sch 322 */
							BgL_objectz00_bglt BgL_tmpz00_5581;

							BgL_tmpz00_5581 = ((BgL_objectz00_bglt) BgL_new1235z00_4260);
							BgL_auxz00_5580 = BGL_OBJECT_WIDENING(BgL_tmpz00_5581);
						}
						BgL_auxz00_5579 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5580);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5579))->
							BgL_freezd2markzd2) =
						((long) BgL_freezd2mark1261zd2_197), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5586;

					{
						obj_t BgL_auxz00_5587;

						{	/* Globalize/ginfo.sch 322 */
							BgL_objectz00_bglt BgL_tmpz00_5588;

							BgL_tmpz00_5588 = ((BgL_objectz00_bglt) BgL_new1235z00_4260);
							BgL_auxz00_5587 = BGL_OBJECT_WIDENING(BgL_tmpz00_5588);
						}
						BgL_auxz00_5586 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5587);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5586))->
							BgL_markz00) = ((long) BgL_mark1262z00_198), BUNSPEC);
				}
				return BgL_new1235z00_4260;
			}
		}

	}



/* &make-sexit/Ginfo */
	BgL_sexitz00_bglt BGl_z62makezd2sexitzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3515, obj_t BgL_handler1257z00_3516,
		obj_t BgL_detachedzf31258zf3_3517, obj_t BgL_gzf31259zf3_3518,
		obj_t BgL_kapturedzf31260zf3_3519, obj_t BgL_freezd2mark1261zd2_3520,
		obj_t BgL_mark1262z00_3521)
	{
		{	/* Globalize/ginfo.sch 322 */
			return
				BGl_makezd2sexitzf2Ginfoz20zzglobaliza7e_ginfoza7
				(BgL_handler1257z00_3516, CBOOL(BgL_detachedzf31258zf3_3517),
				CBOOL(BgL_gzf31259zf3_3518), CBOOL(BgL_kapturedzf31260zf3_3519),
				(long) CINT(BgL_freezd2mark1261zd2_3520),
				(long) CINT(BgL_mark1262z00_3521));
		}

	}



/* sexit/Ginfo? */
	BGL_EXPORTED_DEF bool_t BGl_sexitzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t
		BgL_objz00_199)
	{
		{	/* Globalize/ginfo.sch 323 */
			{	/* Globalize/ginfo.sch 323 */
				obj_t BgL_classz00_4266;

				BgL_classz00_4266 = BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7;
				if (BGL_OBJECTP(BgL_objz00_199))
					{	/* Globalize/ginfo.sch 323 */
						BgL_objectz00_bglt BgL_arg1807z00_4267;

						BgL_arg1807z00_4267 = (BgL_objectz00_bglt) (BgL_objz00_199);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Globalize/ginfo.sch 323 */
								long BgL_idxz00_4268;

								BgL_idxz00_4268 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4267);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4268 + 3L)) == BgL_classz00_4266);
							}
						else
							{	/* Globalize/ginfo.sch 323 */
								bool_t BgL_res2079z00_4271;

								{	/* Globalize/ginfo.sch 323 */
									obj_t BgL_oclassz00_4272;

									{	/* Globalize/ginfo.sch 323 */
										obj_t BgL_arg1815z00_4273;
										long BgL_arg1816z00_4274;

										BgL_arg1815z00_4273 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Globalize/ginfo.sch 323 */
											long BgL_arg1817z00_4275;

											BgL_arg1817z00_4275 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4267);
											BgL_arg1816z00_4274 = (BgL_arg1817z00_4275 - OBJECT_TYPE);
										}
										BgL_oclassz00_4272 =
											VECTOR_REF(BgL_arg1815z00_4273, BgL_arg1816z00_4274);
									}
									{	/* Globalize/ginfo.sch 323 */
										bool_t BgL__ortest_1115z00_4276;

										BgL__ortest_1115z00_4276 =
											(BgL_classz00_4266 == BgL_oclassz00_4272);
										if (BgL__ortest_1115z00_4276)
											{	/* Globalize/ginfo.sch 323 */
												BgL_res2079z00_4271 = BgL__ortest_1115z00_4276;
											}
										else
											{	/* Globalize/ginfo.sch 323 */
												long BgL_odepthz00_4277;

												{	/* Globalize/ginfo.sch 323 */
													obj_t BgL_arg1804z00_4278;

													BgL_arg1804z00_4278 = (BgL_oclassz00_4272);
													BgL_odepthz00_4277 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4278);
												}
												if ((3L < BgL_odepthz00_4277))
													{	/* Globalize/ginfo.sch 323 */
														obj_t BgL_arg1802z00_4279;

														{	/* Globalize/ginfo.sch 323 */
															obj_t BgL_arg1803z00_4280;

															BgL_arg1803z00_4280 = (BgL_oclassz00_4272);
															BgL_arg1802z00_4279 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4280,
																3L);
														}
														BgL_res2079z00_4271 =
															(BgL_arg1802z00_4279 == BgL_classz00_4266);
													}
												else
													{	/* Globalize/ginfo.sch 323 */
														BgL_res2079z00_4271 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2079z00_4271;
							}
					}
				else
					{	/* Globalize/ginfo.sch 323 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sexit/Ginfo? */
	obj_t BGl_z62sexitzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3522,
		obj_t BgL_objz00_3523)
	{
		{	/* Globalize/ginfo.sch 323 */
			return
				BBOOL(BGl_sexitzf2Ginfozf3z01zzglobaliza7e_ginfoza7(BgL_objz00_3523));
		}

	}



/* sexit/Ginfo-nil */
	BGL_EXPORTED_DEF BgL_sexitz00_bglt
		BGl_sexitzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.sch 324 */
			{	/* Globalize/ginfo.sch 324 */
				obj_t BgL_classz00_2805;

				BgL_classz00_2805 = BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7;
				{	/* Globalize/ginfo.sch 324 */
					obj_t BgL__ortest_1117z00_2806;

					BgL__ortest_1117z00_2806 = BGL_CLASS_NIL(BgL_classz00_2805);
					if (CBOOL(BgL__ortest_1117z00_2806))
						{	/* Globalize/ginfo.sch 324 */
							return ((BgL_sexitz00_bglt) BgL__ortest_1117z00_2806);
						}
					else
						{	/* Globalize/ginfo.sch 324 */
							return
								((BgL_sexitz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2805));
						}
				}
			}
		}

	}



/* &sexit/Ginfo-nil */
	BgL_sexitz00_bglt BGl_z62sexitzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3524)
	{
		{	/* Globalize/ginfo.sch 324 */
			return BGl_sexitzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7();
		}

	}



/* sexit/Ginfo-mark */
	BGL_EXPORTED_DEF long
		BGl_sexitzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt
		BgL_oz00_200)
	{
		{	/* Globalize/ginfo.sch 325 */
			{
				BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5630;

				{
					obj_t BgL_auxz00_5631;

					{	/* Globalize/ginfo.sch 325 */
						BgL_objectz00_bglt BgL_tmpz00_5632;

						BgL_tmpz00_5632 = ((BgL_objectz00_bglt) BgL_oz00_200);
						BgL_auxz00_5631 = BGL_OBJECT_WIDENING(BgL_tmpz00_5632);
					}
					BgL_auxz00_5630 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5631);
				}
				return
					(((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5630))->BgL_markz00);
			}
		}

	}



/* &sexit/Ginfo-mark */
	obj_t BGl_z62sexitzf2Ginfozd2markz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3525, obj_t BgL_oz00_3526)
	{
		{	/* Globalize/ginfo.sch 325 */
			return
				BINT(BGl_sexitzf2Ginfozd2markz20zzglobaliza7e_ginfoza7(
					((BgL_sexitz00_bglt) BgL_oz00_3526)));
		}

	}



/* sexit/Ginfo-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt
		BgL_oz00_201, long BgL_vz00_202)
	{
		{	/* Globalize/ginfo.sch 326 */
			{
				BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5640;

				{
					obj_t BgL_auxz00_5641;

					{	/* Globalize/ginfo.sch 326 */
						BgL_objectz00_bglt BgL_tmpz00_5642;

						BgL_tmpz00_5642 = ((BgL_objectz00_bglt) BgL_oz00_201);
						BgL_auxz00_5641 = BGL_OBJECT_WIDENING(BgL_tmpz00_5642);
					}
					BgL_auxz00_5640 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5641);
				}
				return
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5640))->
						BgL_markz00) = ((long) BgL_vz00_202), BUNSPEC);
		}}

	}



/* &sexit/Ginfo-mark-set! */
	obj_t BGl_z62sexitzf2Ginfozd2markzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3527, obj_t BgL_oz00_3528, obj_t BgL_vz00_3529)
	{
		{	/* Globalize/ginfo.sch 326 */
			return
				BGl_sexitzf2Ginfozd2markzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sexitz00_bglt) BgL_oz00_3528), (long) CINT(BgL_vz00_3529));
		}

	}



/* sexit/Ginfo-free-mark */
	BGL_EXPORTED_DEF long
		BGl_sexitzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt
		BgL_oz00_203)
	{
		{	/* Globalize/ginfo.sch 327 */
			{
				BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5650;

				{
					obj_t BgL_auxz00_5651;

					{	/* Globalize/ginfo.sch 327 */
						BgL_objectz00_bglt BgL_tmpz00_5652;

						BgL_tmpz00_5652 = ((BgL_objectz00_bglt) BgL_oz00_203);
						BgL_auxz00_5651 = BGL_OBJECT_WIDENING(BgL_tmpz00_5652);
					}
					BgL_auxz00_5650 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5651);
				}
				return
					(((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5650))->
					BgL_freezd2markzd2);
			}
		}

	}



/* &sexit/Ginfo-free-mark */
	obj_t BGl_z62sexitzf2Ginfozd2freezd2markz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3530, obj_t BgL_oz00_3531)
	{
		{	/* Globalize/ginfo.sch 327 */
			return
				BINT(BGl_sexitzf2Ginfozd2freezd2markzf2zzglobaliza7e_ginfoza7(
					((BgL_sexitz00_bglt) BgL_oz00_3531)));
		}

	}



/* sexit/Ginfo-free-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt BgL_oz00_204, long BgL_vz00_205)
	{
		{	/* Globalize/ginfo.sch 328 */
			{
				BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5660;

				{
					obj_t BgL_auxz00_5661;

					{	/* Globalize/ginfo.sch 328 */
						BgL_objectz00_bglt BgL_tmpz00_5662;

						BgL_tmpz00_5662 = ((BgL_objectz00_bglt) BgL_oz00_204);
						BgL_auxz00_5661 = BGL_OBJECT_WIDENING(BgL_tmpz00_5662);
					}
					BgL_auxz00_5660 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5661);
				}
				return
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5660))->
						BgL_freezd2markzd2) = ((long) BgL_vz00_205), BUNSPEC);
		}}

	}



/* &sexit/Ginfo-free-mark-set! */
	obj_t
		BGl_z62sexitzf2Ginfozd2freezd2markzd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3532, obj_t BgL_oz00_3533, obj_t BgL_vz00_3534)
	{
		{	/* Globalize/ginfo.sch 328 */
			return
				BGl_sexitzf2Ginfozd2freezd2markzd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_sexitz00_bglt) BgL_oz00_3533), (long) CINT(BgL_vz00_3534));
		}

	}



/* sexit/Ginfo-kaptured? */
	BGL_EXPORTED_DEF bool_t
		BGl_sexitzf2Ginfozd2kapturedzf3zd3zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt
		BgL_oz00_206)
	{
		{	/* Globalize/ginfo.sch 329 */
			{
				BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5670;

				{
					obj_t BgL_auxz00_5671;

					{	/* Globalize/ginfo.sch 329 */
						BgL_objectz00_bglt BgL_tmpz00_5672;

						BgL_tmpz00_5672 = ((BgL_objectz00_bglt) BgL_oz00_206);
						BgL_auxz00_5671 = BGL_OBJECT_WIDENING(BgL_tmpz00_5672);
					}
					BgL_auxz00_5670 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5671);
				}
				return
					(((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5670))->
					BgL_kapturedzf3zf3);
			}
		}

	}



/* &sexit/Ginfo-kaptured? */
	obj_t BGl_z62sexitzf2Ginfozd2kapturedzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3535, obj_t BgL_oz00_3536)
	{
		{	/* Globalize/ginfo.sch 329 */
			return
				BBOOL(BGl_sexitzf2Ginfozd2kapturedzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_sexitz00_bglt) BgL_oz00_3536)));
		}

	}



/* sexit/Ginfo-kaptured?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Ginfozd2kapturedzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt BgL_oz00_207, bool_t BgL_vz00_208)
	{
		{	/* Globalize/ginfo.sch 330 */
			{
				BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5680;

				{
					obj_t BgL_auxz00_5681;

					{	/* Globalize/ginfo.sch 330 */
						BgL_objectz00_bglt BgL_tmpz00_5682;

						BgL_tmpz00_5682 = ((BgL_objectz00_bglt) BgL_oz00_207);
						BgL_auxz00_5681 = BGL_OBJECT_WIDENING(BgL_tmpz00_5682);
					}
					BgL_auxz00_5680 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5681);
				}
				return
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5680))->
						BgL_kapturedzf3zf3) = ((bool_t) BgL_vz00_208), BUNSPEC);
			}
		}

	}



/* &sexit/Ginfo-kaptured?-set! */
	obj_t
		BGl_z62sexitzf2Ginfozd2kapturedzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3537, obj_t BgL_oz00_3538, obj_t BgL_vz00_3539)
	{
		{	/* Globalize/ginfo.sch 330 */
			return
				BGl_sexitzf2Ginfozd2kapturedzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_sexitz00_bglt) BgL_oz00_3538), CBOOL(BgL_vz00_3539));
		}

	}



/* sexit/Ginfo-G? */
	BGL_EXPORTED_DEF bool_t
		BGl_sexitzf2Ginfozd2Gzf3zd3zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt
		BgL_oz00_209)
	{
		{	/* Globalize/ginfo.sch 331 */
			{
				BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5690;

				{
					obj_t BgL_auxz00_5691;

					{	/* Globalize/ginfo.sch 331 */
						BgL_objectz00_bglt BgL_tmpz00_5692;

						BgL_tmpz00_5692 = ((BgL_objectz00_bglt) BgL_oz00_209);
						BgL_auxz00_5691 = BGL_OBJECT_WIDENING(BgL_tmpz00_5692);
					}
					BgL_auxz00_5690 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5691);
				}
				return
					(((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5690))->BgL_gzf3zf3);
			}
		}

	}



/* &sexit/Ginfo-G? */
	obj_t BGl_z62sexitzf2Ginfozd2Gzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3540, obj_t BgL_oz00_3541)
	{
		{	/* Globalize/ginfo.sch 331 */
			return
				BBOOL(BGl_sexitzf2Ginfozd2Gzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_sexitz00_bglt) BgL_oz00_3541)));
		}

	}



/* sexit/Ginfo-G?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Ginfozd2Gzf3zd2setz12z13zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt
		BgL_oz00_210, bool_t BgL_vz00_211)
	{
		{	/* Globalize/ginfo.sch 332 */
			{
				BgL_sexitzf2ginfozf2_bglt BgL_auxz00_5700;

				{
					obj_t BgL_auxz00_5701;

					{	/* Globalize/ginfo.sch 332 */
						BgL_objectz00_bglt BgL_tmpz00_5702;

						BgL_tmpz00_5702 = ((BgL_objectz00_bglt) BgL_oz00_210);
						BgL_auxz00_5701 = BGL_OBJECT_WIDENING(BgL_tmpz00_5702);
					}
					BgL_auxz00_5700 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_5701);
				}
				return
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5700))->
						BgL_gzf3zf3) = ((bool_t) BgL_vz00_211), BUNSPEC);
			}
		}

	}



/* &sexit/Ginfo-G?-set! */
	obj_t BGl_z62sexitzf2Ginfozd2Gzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3542, obj_t BgL_oz00_3543, obj_t BgL_vz00_3544)
	{
		{	/* Globalize/ginfo.sch 332 */
			return
				BGl_sexitzf2Ginfozd2Gzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_sexitz00_bglt) BgL_oz00_3543), CBOOL(BgL_vz00_3544));
		}

	}



/* sexit/Ginfo-detached? */
	BGL_EXPORTED_DEF bool_t
		BGl_sexitzf2Ginfozd2detachedzf3zd3zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt
		BgL_oz00_212)
	{
		{	/* Globalize/ginfo.sch 333 */
			return
				(((BgL_sexitz00_bglt) COBJECT(
						((BgL_sexitz00_bglt) BgL_oz00_212)))->BgL_detachedzf3zf3);
		}

	}



/* &sexit/Ginfo-detached? */
	obj_t BGl_z62sexitzf2Ginfozd2detachedzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3545, obj_t BgL_oz00_3546)
	{
		{	/* Globalize/ginfo.sch 333 */
			return
				BBOOL(BGl_sexitzf2Ginfozd2detachedzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_sexitz00_bglt) BgL_oz00_3546)));
		}

	}



/* sexit/Ginfo-detached?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Ginfozd2detachedzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt BgL_oz00_213, bool_t BgL_vz00_214)
	{
		{	/* Globalize/ginfo.sch 334 */
			return
				((((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt) BgL_oz00_213)))->BgL_detachedzf3zf3) =
				((bool_t) BgL_vz00_214), BUNSPEC);
		}

	}



/* &sexit/Ginfo-detached?-set! */
	obj_t
		BGl_z62sexitzf2Ginfozd2detachedzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3547, obj_t BgL_oz00_3548, obj_t BgL_vz00_3549)
	{
		{	/* Globalize/ginfo.sch 334 */
			return
				BGl_sexitzf2Ginfozd2detachedzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_sexitz00_bglt) BgL_oz00_3548), CBOOL(BgL_vz00_3549));
		}

	}



/* sexit/Ginfo-handler */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Ginfozd2handlerz20zzglobaliza7e_ginfoza7(BgL_sexitz00_bglt
		BgL_oz00_215)
	{
		{	/* Globalize/ginfo.sch 335 */
			return
				(((BgL_sexitz00_bglt) COBJECT(
						((BgL_sexitz00_bglt) BgL_oz00_215)))->BgL_handlerz00);
		}

	}



/* &sexit/Ginfo-handler */
	obj_t BGl_z62sexitzf2Ginfozd2handlerz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3550, obj_t BgL_oz00_3551)
	{
		{	/* Globalize/ginfo.sch 335 */
			return
				BGl_sexitzf2Ginfozd2handlerz20zzglobaliza7e_ginfoza7(
				((BgL_sexitz00_bglt) BgL_oz00_3551));
		}

	}



/* sexit/Ginfo-handler-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Ginfozd2handlerzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_sexitz00_bglt BgL_oz00_216, obj_t BgL_vz00_217)
	{
		{	/* Globalize/ginfo.sch 336 */
			return
				((((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt) BgL_oz00_216)))->BgL_handlerz00) =
				((obj_t) BgL_vz00_217), BUNSPEC);
		}

	}



/* &sexit/Ginfo-handler-set! */
	obj_t BGl_z62sexitzf2Ginfozd2handlerzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3552, obj_t BgL_oz00_3553, obj_t BgL_vz00_3554)
	{
		{	/* Globalize/ginfo.sch 336 */
			return
				BGl_sexitzf2Ginfozd2handlerzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_sexitz00_bglt) BgL_oz00_3553), BgL_vz00_3554);
		}

	}



/* make-local/Ginfo */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_makezd2localzf2Ginfoz20zzglobaliza7e_ginfoza7(obj_t BgL_id1243z00_218,
		obj_t BgL_name1244z00_219, BgL_typez00_bglt BgL_type1245z00_220,
		BgL_valuez00_bglt BgL_value1246z00_221, obj_t BgL_access1247z00_222,
		obj_t BgL_fastzd2alpha1248zd2_223, obj_t BgL_removable1249z00_224,
		long BgL_occurrence1250z00_225, long BgL_occurrencew1251z00_226,
		bool_t BgL_userzf31252zf3_227, long BgL_key1253z00_228,
		bool_t BgL_escapezf31254zf3_229, bool_t BgL_globaliza7edzf31255z54_230)
	{
		{	/* Globalize/ginfo.sch 339 */
			{	/* Globalize/ginfo.sch 339 */
				BgL_localz00_bglt BgL_new1239z00_4281;

				{	/* Globalize/ginfo.sch 339 */
					BgL_localz00_bglt BgL_tmp1237z00_4282;
					BgL_localzf2ginfozf2_bglt BgL_wide1238z00_4283;

					{
						BgL_localz00_bglt BgL_auxz00_5728;

						{	/* Globalize/ginfo.sch 339 */
							BgL_localz00_bglt BgL_new1236z00_4284;

							BgL_new1236z00_4284 =
								((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_localz00_bgl))));
							{	/* Globalize/ginfo.sch 339 */
								long BgL_arg1472z00_4285;

								BgL_arg1472z00_4285 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1236z00_4284),
									BgL_arg1472z00_4285);
							}
							{	/* Globalize/ginfo.sch 339 */
								BgL_objectz00_bglt BgL_tmpz00_5733;

								BgL_tmpz00_5733 = ((BgL_objectz00_bglt) BgL_new1236z00_4284);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5733, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1236z00_4284);
							BgL_auxz00_5728 = BgL_new1236z00_4284;
						}
						BgL_tmp1237z00_4282 = ((BgL_localz00_bglt) BgL_auxz00_5728);
					}
					BgL_wide1238z00_4283 =
						((BgL_localzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_localzf2ginfozf2_bgl))));
					{	/* Globalize/ginfo.sch 339 */
						obj_t BgL_auxz00_5741;
						BgL_objectz00_bglt BgL_tmpz00_5739;

						BgL_auxz00_5741 = ((obj_t) BgL_wide1238z00_4283);
						BgL_tmpz00_5739 = ((BgL_objectz00_bglt) BgL_tmp1237z00_4282);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5739, BgL_auxz00_5741);
					}
					((BgL_objectz00_bglt) BgL_tmp1237z00_4282);
					{	/* Globalize/ginfo.sch 339 */
						long BgL_arg1454z00_4286;

						BgL_arg1454z00_4286 =
							BGL_CLASS_NUM(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1237z00_4282), BgL_arg1454z00_4286);
					}
					BgL_new1239z00_4281 = ((BgL_localz00_bglt) BgL_tmp1237z00_4282);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1239z00_4281)))->BgL_idz00) =
					((obj_t) BgL_id1243z00_218), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_namez00) =
					((obj_t) BgL_name1244z00_219), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1245z00_220), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1246z00_221), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_accessz00) =
					((obj_t) BgL_access1247z00_222), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1248zd2_223), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_removablez00) =
					((obj_t) BgL_removable1249z00_224), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_occurrencez00) =
					((long) BgL_occurrence1250z00_225), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1251z00_226), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1239z00_4281)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31252zf3_227), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1239z00_4281)))->BgL_keyz00) =
					((long) BgL_key1253z00_228), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1239z00_4281)))->BgL_valzd2noescapezd2) =
					((obj_t) BTRUE), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
									BgL_new1239z00_4281)))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2ginfozf2_bglt BgL_auxz00_5775;

					{
						obj_t BgL_auxz00_5776;

						{	/* Globalize/ginfo.sch 339 */
							BgL_objectz00_bglt BgL_tmpz00_5777;

							BgL_tmpz00_5777 = ((BgL_objectz00_bglt) BgL_new1239z00_4281);
							BgL_auxz00_5776 = BGL_OBJECT_WIDENING(BgL_tmpz00_5777);
						}
						BgL_auxz00_5775 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5776);
					}
					((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5775))->
							BgL_escapezf3zf3) = ((bool_t) BgL_escapezf31254zf3_229), BUNSPEC);
				}
				{
					BgL_localzf2ginfozf2_bglt BgL_auxz00_5782;

					{
						obj_t BgL_auxz00_5783;

						{	/* Globalize/ginfo.sch 339 */
							BgL_objectz00_bglt BgL_tmpz00_5784;

							BgL_tmpz00_5784 = ((BgL_objectz00_bglt) BgL_new1239z00_4281);
							BgL_auxz00_5783 = BGL_OBJECT_WIDENING(BgL_tmpz00_5784);
						}
						BgL_auxz00_5782 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5783);
					}
					((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5782))->
							BgL_globaliza7edzf3z54) =
						((bool_t) BgL_globaliza7edzf31255z54_230), BUNSPEC);
				}
				return BgL_new1239z00_4281;
			}
		}

	}



/* &make-local/Ginfo */
	BgL_localz00_bglt BGl_z62makezd2localzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3555, obj_t BgL_id1243z00_3556, obj_t BgL_name1244z00_3557,
		obj_t BgL_type1245z00_3558, obj_t BgL_value1246z00_3559,
		obj_t BgL_access1247z00_3560, obj_t BgL_fastzd2alpha1248zd2_3561,
		obj_t BgL_removable1249z00_3562, obj_t BgL_occurrence1250z00_3563,
		obj_t BgL_occurrencew1251z00_3564, obj_t BgL_userzf31252zf3_3565,
		obj_t BgL_key1253z00_3566, obj_t BgL_escapezf31254zf3_3567,
		obj_t BgL_globaliza7edzf31255z54_3568)
	{
		{	/* Globalize/ginfo.sch 339 */
			return
				BGl_makezd2localzf2Ginfoz20zzglobaliza7e_ginfoza7(BgL_id1243z00_3556,
				BgL_name1244z00_3557, ((BgL_typez00_bglt) BgL_type1245z00_3558),
				((BgL_valuez00_bglt) BgL_value1246z00_3559), BgL_access1247z00_3560,
				BgL_fastzd2alpha1248zd2_3561, BgL_removable1249z00_3562,
				(long) CINT(BgL_occurrence1250z00_3563),
				(long) CINT(BgL_occurrencew1251z00_3564),
				CBOOL(BgL_userzf31252zf3_3565), (long) CINT(BgL_key1253z00_3566),
				CBOOL(BgL_escapezf31254zf3_3567),
				CBOOL(BgL_globaliza7edzf31255z54_3568));
		}

	}



/* local/Ginfo? */
	BGL_EXPORTED_DEF bool_t BGl_localzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t
		BgL_objz00_231)
	{
		{	/* Globalize/ginfo.sch 340 */
			{	/* Globalize/ginfo.sch 340 */
				obj_t BgL_classz00_4287;

				BgL_classz00_4287 = BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7;
				if (BGL_OBJECTP(BgL_objz00_231))
					{	/* Globalize/ginfo.sch 340 */
						BgL_objectz00_bglt BgL_arg1807z00_4288;

						BgL_arg1807z00_4288 = (BgL_objectz00_bglt) (BgL_objz00_231);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Globalize/ginfo.sch 340 */
								long BgL_idxz00_4289;

								BgL_idxz00_4289 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4288);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4289 + 3L)) == BgL_classz00_4287);
							}
						else
							{	/* Globalize/ginfo.sch 340 */
								bool_t BgL_res2080z00_4292;

								{	/* Globalize/ginfo.sch 340 */
									obj_t BgL_oclassz00_4293;

									{	/* Globalize/ginfo.sch 340 */
										obj_t BgL_arg1815z00_4294;
										long BgL_arg1816z00_4295;

										BgL_arg1815z00_4294 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Globalize/ginfo.sch 340 */
											long BgL_arg1817z00_4296;

											BgL_arg1817z00_4296 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4288);
											BgL_arg1816z00_4295 = (BgL_arg1817z00_4296 - OBJECT_TYPE);
										}
										BgL_oclassz00_4293 =
											VECTOR_REF(BgL_arg1815z00_4294, BgL_arg1816z00_4295);
									}
									{	/* Globalize/ginfo.sch 340 */
										bool_t BgL__ortest_1115z00_4297;

										BgL__ortest_1115z00_4297 =
											(BgL_classz00_4287 == BgL_oclassz00_4293);
										if (BgL__ortest_1115z00_4297)
											{	/* Globalize/ginfo.sch 340 */
												BgL_res2080z00_4292 = BgL__ortest_1115z00_4297;
											}
										else
											{	/* Globalize/ginfo.sch 340 */
												long BgL_odepthz00_4298;

												{	/* Globalize/ginfo.sch 340 */
													obj_t BgL_arg1804z00_4299;

													BgL_arg1804z00_4299 = (BgL_oclassz00_4293);
													BgL_odepthz00_4298 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4299);
												}
												if ((3L < BgL_odepthz00_4298))
													{	/* Globalize/ginfo.sch 340 */
														obj_t BgL_arg1802z00_4300;

														{	/* Globalize/ginfo.sch 340 */
															obj_t BgL_arg1803z00_4301;

															BgL_arg1803z00_4301 = (BgL_oclassz00_4293);
															BgL_arg1802z00_4300 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4301,
																3L);
														}
														BgL_res2080z00_4292 =
															(BgL_arg1802z00_4300 == BgL_classz00_4287);
													}
												else
													{	/* Globalize/ginfo.sch 340 */
														BgL_res2080z00_4292 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2080z00_4292;
							}
					}
				else
					{	/* Globalize/ginfo.sch 340 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &local/Ginfo? */
	obj_t BGl_z62localzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3569,
		obj_t BgL_objz00_3570)
	{
		{	/* Globalize/ginfo.sch 340 */
			return
				BBOOL(BGl_localzf2Ginfozf3z01zzglobaliza7e_ginfoza7(BgL_objz00_3570));
		}

	}



/* local/Ginfo-nil */
	BGL_EXPORTED_DEF BgL_localz00_bglt
		BGl_localzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.sch 341 */
			{	/* Globalize/ginfo.sch 341 */
				obj_t BgL_classz00_2865;

				BgL_classz00_2865 = BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7;
				{	/* Globalize/ginfo.sch 341 */
					obj_t BgL__ortest_1117z00_2866;

					BgL__ortest_1117z00_2866 = BGL_CLASS_NIL(BgL_classz00_2865);
					if (CBOOL(BgL__ortest_1117z00_2866))
						{	/* Globalize/ginfo.sch 341 */
							return ((BgL_localz00_bglt) BgL__ortest_1117z00_2866);
						}
					else
						{	/* Globalize/ginfo.sch 341 */
							return
								((BgL_localz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2865));
						}
				}
			}
		}

	}



/* &local/Ginfo-nil */
	BgL_localz00_bglt BGl_z62localzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3571)
	{
		{	/* Globalize/ginfo.sch 341 */
			return BGl_localzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7();
		}

	}



/* local/Ginfo-globalized? */
	BGL_EXPORTED_DEF bool_t
		BGl_localzf2Ginfozd2globaliza7edzf3z74zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_232)
	{
		{	/* Globalize/ginfo.sch 342 */
			{
				BgL_localzf2ginfozf2_bglt BgL_auxz00_5829;

				{
					obj_t BgL_auxz00_5830;

					{	/* Globalize/ginfo.sch 342 */
						BgL_objectz00_bglt BgL_tmpz00_5831;

						BgL_tmpz00_5831 = ((BgL_objectz00_bglt) BgL_oz00_232);
						BgL_auxz00_5830 = BGL_OBJECT_WIDENING(BgL_tmpz00_5831);
					}
					BgL_auxz00_5829 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5830);
				}
				return
					(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5829))->
					BgL_globaliza7edzf3z54);
			}
		}

	}



/* &local/Ginfo-globalized? */
	obj_t BGl_z62localzf2Ginfozd2globaliza7edzf3z16zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3572, obj_t BgL_oz00_3573)
	{
		{	/* Globalize/ginfo.sch 342 */
			return
				BBOOL(BGl_localzf2Ginfozd2globaliza7edzf3z74zzglobaliza7e_ginfoza7(
					((BgL_localz00_bglt) BgL_oz00_3573)));
		}

	}



/* local/Ginfo-globalized?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2globaliza7edzf3zd2setz12zb4zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_233, bool_t BgL_vz00_234)
	{
		{	/* Globalize/ginfo.sch 343 */
			{
				BgL_localzf2ginfozf2_bglt BgL_auxz00_5839;

				{
					obj_t BgL_auxz00_5840;

					{	/* Globalize/ginfo.sch 343 */
						BgL_objectz00_bglt BgL_tmpz00_5841;

						BgL_tmpz00_5841 = ((BgL_objectz00_bglt) BgL_oz00_233);
						BgL_auxz00_5840 = BGL_OBJECT_WIDENING(BgL_tmpz00_5841);
					}
					BgL_auxz00_5839 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5840);
				}
				return
					((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5839))->
						BgL_globaliza7edzf3z54) = ((bool_t) BgL_vz00_234), BUNSPEC);
			}
		}

	}



/* &local/Ginfo-globalized?-set! */
	obj_t
		BGl_z62localzf2Ginfozd2globaliza7edzf3zd2setz12zd6zzglobaliza7e_ginfoza7
		(obj_t BgL_envz00_3574, obj_t BgL_oz00_3575, obj_t BgL_vz00_3576)
	{
		{	/* Globalize/ginfo.sch 343 */
			return
				BGl_localzf2Ginfozd2globaliza7edzf3zd2setz12zb4zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3575), CBOOL(BgL_vz00_3576));
		}

	}



/* local/Ginfo-escape? */
	BGL_EXPORTED_DEF bool_t
		BGl_localzf2Ginfozd2escapezf3zd3zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_235)
	{
		{	/* Globalize/ginfo.sch 344 */
			{
				BgL_localzf2ginfozf2_bglt BgL_auxz00_5849;

				{
					obj_t BgL_auxz00_5850;

					{	/* Globalize/ginfo.sch 344 */
						BgL_objectz00_bglt BgL_tmpz00_5851;

						BgL_tmpz00_5851 = ((BgL_objectz00_bglt) BgL_oz00_235);
						BgL_auxz00_5850 = BGL_OBJECT_WIDENING(BgL_tmpz00_5851);
					}
					BgL_auxz00_5849 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5850);
				}
				return
					(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5849))->
					BgL_escapezf3zf3);
			}
		}

	}



/* &local/Ginfo-escape? */
	obj_t BGl_z62localzf2Ginfozd2escapezf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3577, obj_t BgL_oz00_3578)
	{
		{	/* Globalize/ginfo.sch 344 */
			return
				BBOOL(BGl_localzf2Ginfozd2escapezf3zd3zzglobaliza7e_ginfoza7(
					((BgL_localz00_bglt) BgL_oz00_3578)));
		}

	}



/* local/Ginfo-escape?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2escapezf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_236, bool_t BgL_vz00_237)
	{
		{	/* Globalize/ginfo.sch 345 */
			{
				BgL_localzf2ginfozf2_bglt BgL_auxz00_5859;

				{
					obj_t BgL_auxz00_5860;

					{	/* Globalize/ginfo.sch 345 */
						BgL_objectz00_bglt BgL_tmpz00_5861;

						BgL_tmpz00_5861 = ((BgL_objectz00_bglt) BgL_oz00_236);
						BgL_auxz00_5860 = BGL_OBJECT_WIDENING(BgL_tmpz00_5861);
					}
					BgL_auxz00_5859 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_5860);
				}
				return
					((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_5859))->
						BgL_escapezf3zf3) = ((bool_t) BgL_vz00_237), BUNSPEC);
			}
		}

	}



/* &local/Ginfo-escape?-set! */
	obj_t BGl_z62localzf2Ginfozd2escapezf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3579, obj_t BgL_oz00_3580, obj_t BgL_vz00_3581)
	{
		{	/* Globalize/ginfo.sch 345 */
			return
				BGl_localzf2Ginfozd2escapezf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3580), CBOOL(BgL_vz00_3581));
		}

	}



/* local/Ginfo-key */
	BGL_EXPORTED_DEF long
		BGl_localzf2Ginfozd2keyz20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_238)
	{
		{	/* Globalize/ginfo.sch 346 */
			return
				(((BgL_localz00_bglt) COBJECT(
						((BgL_localz00_bglt) BgL_oz00_238)))->BgL_keyz00);
		}

	}



/* &local/Ginfo-key */
	obj_t BGl_z62localzf2Ginfozd2keyz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3582, obj_t BgL_oz00_3583)
	{
		{	/* Globalize/ginfo.sch 346 */
			return
				BINT(BGl_localzf2Ginfozd2keyz20zzglobaliza7e_ginfoza7(
					((BgL_localz00_bglt) BgL_oz00_3583)));
		}

	}



/* local/Ginfo-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_localzf2Ginfozd2userzf3zd3zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_241)
	{
		{	/* Globalize/ginfo.sch 348 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_241)))->BgL_userzf3zf3);
		}

	}



/* &local/Ginfo-user? */
	obj_t BGl_z62localzf2Ginfozd2userzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3584, obj_t BgL_oz00_3585)
	{
		{	/* Globalize/ginfo.sch 348 */
			return
				BBOOL(BGl_localzf2Ginfozd2userzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_localz00_bglt) BgL_oz00_3585)));
		}

	}



/* local/Ginfo-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2userzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_242, bool_t BgL_vz00_243)
	{
		{	/* Globalize/ginfo.sch 349 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_242)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_243), BUNSPEC);
		}

	}



/* &local/Ginfo-user?-set! */
	obj_t BGl_z62localzf2Ginfozd2userzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3586, obj_t BgL_oz00_3587, obj_t BgL_vz00_3588)
	{
		{	/* Globalize/ginfo.sch 349 */
			return
				BGl_localzf2Ginfozd2userzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3587), CBOOL(BgL_vz00_3588));
		}

	}



/* local/Ginfo-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_localzf2Ginfozd2occurrencewz20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_244)
	{
		{	/* Globalize/ginfo.sch 350 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_244)))->BgL_occurrencewz00);
		}

	}



/* &local/Ginfo-occurrencew */
	obj_t BGl_z62localzf2Ginfozd2occurrencewz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3589, obj_t BgL_oz00_3590)
	{
		{	/* Globalize/ginfo.sch 350 */
			return
				BINT(BGl_localzf2Ginfozd2occurrencewz20zzglobaliza7e_ginfoza7(
					((BgL_localz00_bglt) BgL_oz00_3590)));
		}

	}



/* local/Ginfo-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2occurrencewzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_245, long BgL_vz00_246)
	{
		{	/* Globalize/ginfo.sch 351 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_245)))->BgL_occurrencewz00) =
				((long) BgL_vz00_246), BUNSPEC);
		}

	}



/* &local/Ginfo-occurrencew-set! */
	obj_t
		BGl_z62localzf2Ginfozd2occurrencewzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3591, obj_t BgL_oz00_3592, obj_t BgL_vz00_3593)
	{
		{	/* Globalize/ginfo.sch 351 */
			return
				BGl_localzf2Ginfozd2occurrencewzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3592), (long) CINT(BgL_vz00_3593));
		}

	}



/* local/Ginfo-occurrence */
	BGL_EXPORTED_DEF long
		BGl_localzf2Ginfozd2occurrencez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_247)
	{
		{	/* Globalize/ginfo.sch 352 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_247)))->BgL_occurrencez00);
		}

	}



/* &local/Ginfo-occurrence */
	obj_t BGl_z62localzf2Ginfozd2occurrencez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3594, obj_t BgL_oz00_3595)
	{
		{	/* Globalize/ginfo.sch 352 */
			return
				BINT(BGl_localzf2Ginfozd2occurrencez20zzglobaliza7e_ginfoza7(
					((BgL_localz00_bglt) BgL_oz00_3595)));
		}

	}



/* local/Ginfo-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2occurrencezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_248, long BgL_vz00_249)
	{
		{	/* Globalize/ginfo.sch 353 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_248)))->BgL_occurrencez00) =
				((long) BgL_vz00_249), BUNSPEC);
		}

	}



/* &local/Ginfo-occurrence-set! */
	obj_t
		BGl_z62localzf2Ginfozd2occurrencezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3596, obj_t BgL_oz00_3597, obj_t BgL_vz00_3598)
	{
		{	/* Globalize/ginfo.sch 353 */
			return
				BGl_localzf2Ginfozd2occurrencezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3597), (long) CINT(BgL_vz00_3598));
		}

	}



/* local/Ginfo-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2removablez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_250)
	{
		{	/* Globalize/ginfo.sch 354 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_250)))->BgL_removablez00);
		}

	}



/* &local/Ginfo-removable */
	obj_t BGl_z62localzf2Ginfozd2removablez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3599, obj_t BgL_oz00_3600)
	{
		{	/* Globalize/ginfo.sch 354 */
			return
				BGl_localzf2Ginfozd2removablez20zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3600));
		}

	}



/* local/Ginfo-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2removablezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_251, obj_t BgL_vz00_252)
	{
		{	/* Globalize/ginfo.sch 355 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_251)))->BgL_removablez00) =
				((obj_t) BgL_vz00_252), BUNSPEC);
		}

	}



/* &local/Ginfo-removable-set! */
	obj_t BGl_z62localzf2Ginfozd2removablezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3601, obj_t BgL_oz00_3602, obj_t BgL_vz00_3603)
	{
		{	/* Globalize/ginfo.sch 355 */
			return
				BGl_localzf2Ginfozd2removablezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3602), BgL_vz00_3603);
		}

	}



/* local/Ginfo-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2fastzd2alphazf2zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_253)
	{
		{	/* Globalize/ginfo.sch 356 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_253)))->BgL_fastzd2alphazd2);
		}

	}



/* &local/Ginfo-fast-alpha */
	obj_t BGl_z62localzf2Ginfozd2fastzd2alphaz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3604, obj_t BgL_oz00_3605)
	{
		{	/* Globalize/ginfo.sch 356 */
			return
				BGl_localzf2Ginfozd2fastzd2alphazf2zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3605));
		}

	}



/* local/Ginfo-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2fastzd2alphazd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_254, obj_t BgL_vz00_255)
	{
		{	/* Globalize/ginfo.sch 357 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_254)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_255), BUNSPEC);
		}

	}



/* &local/Ginfo-fast-alpha-set! */
	obj_t
		BGl_z62localzf2Ginfozd2fastzd2alphazd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3606, obj_t BgL_oz00_3607, obj_t BgL_vz00_3608)
	{
		{	/* Globalize/ginfo.sch 357 */
			return
				BGl_localzf2Ginfozd2fastzd2alphazd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3607), BgL_vz00_3608);
		}

	}



/* local/Ginfo-access */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2accessz20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_256)
	{
		{	/* Globalize/ginfo.sch 358 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_256)))->BgL_accessz00);
		}

	}



/* &local/Ginfo-access */
	obj_t BGl_z62localzf2Ginfozd2accessz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3609, obj_t BgL_oz00_3610)
	{
		{	/* Globalize/ginfo.sch 358 */
			return
				BGl_localzf2Ginfozd2accessz20zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3610));
		}

	}



/* local/Ginfo-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2accesszd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_257, obj_t BgL_vz00_258)
	{
		{	/* Globalize/ginfo.sch 359 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_257)))->BgL_accessz00) =
				((obj_t) BgL_vz00_258), BUNSPEC);
		}

	}



/* &local/Ginfo-access-set! */
	obj_t BGl_z62localzf2Ginfozd2accesszd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3611, obj_t BgL_oz00_3612, obj_t BgL_vz00_3613)
	{
		{	/* Globalize/ginfo.sch 359 */
			return
				BGl_localzf2Ginfozd2accesszd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3612), BgL_vz00_3613);
		}

	}



/* local/Ginfo-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_localzf2Ginfozd2valuez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_259)
	{
		{	/* Globalize/ginfo.sch 360 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_259)))->BgL_valuez00);
		}

	}



/* &local/Ginfo-value */
	BgL_valuez00_bglt BGl_z62localzf2Ginfozd2valuez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3614, obj_t BgL_oz00_3615)
	{
		{	/* Globalize/ginfo.sch 360 */
			return
				BGl_localzf2Ginfozd2valuez20zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3615));
		}

	}



/* local/Ginfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2valuezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_localz00_bglt BgL_oz00_260, BgL_valuez00_bglt BgL_vz00_261)
	{
		{	/* Globalize/ginfo.sch 361 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_260)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_261), BUNSPEC);
		}

	}



/* &local/Ginfo-value-set! */
	obj_t BGl_z62localzf2Ginfozd2valuezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3616, obj_t BgL_oz00_3617, obj_t BgL_vz00_3618)
	{
		{	/* Globalize/ginfo.sch 361 */
			return
				BGl_localzf2Ginfozd2valuezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3617),
				((BgL_valuez00_bglt) BgL_vz00_3618));
		}

	}



/* local/Ginfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_localzf2Ginfozd2typez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_262)
	{
		{	/* Globalize/ginfo.sch 362 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_262)))->BgL_typez00);
		}

	}



/* &local/Ginfo-type */
	BgL_typez00_bglt BGl_z62localzf2Ginfozd2typez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3619, obj_t BgL_oz00_3620)
	{
		{	/* Globalize/ginfo.sch 362 */
			return
				BGl_localzf2Ginfozd2typez20zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3620));
		}

	}



/* local/Ginfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2typezd2setz12ze0zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_263, BgL_typez00_bglt BgL_vz00_264)
	{
		{	/* Globalize/ginfo.sch 363 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_263)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_264), BUNSPEC);
		}

	}



/* &local/Ginfo-type-set! */
	obj_t BGl_z62localzf2Ginfozd2typezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3621, obj_t BgL_oz00_3622, obj_t BgL_vz00_3623)
	{
		{	/* Globalize/ginfo.sch 363 */
			return
				BGl_localzf2Ginfozd2typezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3622),
				((BgL_typez00_bglt) BgL_vz00_3623));
		}

	}



/* local/Ginfo-name */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2namez20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_265)
	{
		{	/* Globalize/ginfo.sch 364 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_265)))->BgL_namez00);
		}

	}



/* &local/Ginfo-name */
	obj_t BGl_z62localzf2Ginfozd2namez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3624, obj_t BgL_oz00_3625)
	{
		{	/* Globalize/ginfo.sch 364 */
			return
				BGl_localzf2Ginfozd2namez20zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3625));
		}

	}



/* local/Ginfo-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2namezd2setz12ze0zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_266, obj_t BgL_vz00_267)
	{
		{	/* Globalize/ginfo.sch 365 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_266)))->BgL_namez00) =
				((obj_t) BgL_vz00_267), BUNSPEC);
		}

	}



/* &local/Ginfo-name-set! */
	obj_t BGl_z62localzf2Ginfozd2namezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3626, obj_t BgL_oz00_3627, obj_t BgL_vz00_3628)
	{
		{	/* Globalize/ginfo.sch 365 */
			return
				BGl_localzf2Ginfozd2namezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3627), BgL_vz00_3628);
		}

	}



/* local/Ginfo-id */
	BGL_EXPORTED_DEF obj_t
		BGl_localzf2Ginfozd2idz20zzglobaliza7e_ginfoza7(BgL_localz00_bglt
		BgL_oz00_268)
	{
		{	/* Globalize/ginfo.sch 366 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_268)))->BgL_idz00);
		}

	}



/* &local/Ginfo-id */
	obj_t BGl_z62localzf2Ginfozd2idz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3629, obj_t BgL_oz00_3630)
	{
		{	/* Globalize/ginfo.sch 366 */
			return
				BGl_localzf2Ginfozd2idz20zzglobaliza7e_ginfoza7(
				((BgL_localz00_bglt) BgL_oz00_3630));
		}

	}



/* make-global/Ginfo */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_makezd2globalzf2Ginfoz20zzglobaliza7e_ginfoza7(obj_t BgL_id1220z00_271,
		obj_t BgL_name1221z00_272, BgL_typez00_bglt BgL_type1222z00_273,
		BgL_valuez00_bglt BgL_value1223z00_274, obj_t BgL_access1224z00_275,
		obj_t BgL_fastzd2alpha1225zd2_276, obj_t BgL_removable1226z00_277,
		long BgL_occurrence1227z00_278, long BgL_occurrencew1228z00_279,
		bool_t BgL_userzf31229zf3_280, obj_t BgL_module1230z00_281,
		obj_t BgL_import1231z00_282, bool_t BgL_evaluablezf31232zf3_283,
		bool_t BgL_evalzf31233zf3_284, obj_t BgL_library1234z00_285,
		obj_t BgL_pragma1235z00_286, obj_t BgL_src1236z00_287,
		obj_t BgL_jvmzd2typezd2name1237z00_288, obj_t BgL_init1238z00_289,
		obj_t BgL_alias1239z00_290, bool_t BgL_escapezf31240zf3_291,
		obj_t BgL_globalzd2closure1241zd2_292)
	{
		{	/* Globalize/ginfo.sch 370 */
			{	/* Globalize/ginfo.sch 370 */
				BgL_globalz00_bglt BgL_new1243z00_4302;

				{	/* Globalize/ginfo.sch 370 */
					BgL_globalz00_bglt BgL_tmp1241z00_4303;
					BgL_globalzf2ginfozf2_bglt BgL_wide1242z00_4304;

					{
						BgL_globalz00_bglt BgL_auxz00_5958;

						{	/* Globalize/ginfo.sch 370 */
							BgL_globalz00_bglt BgL_new1240z00_4305;

							BgL_new1240z00_4305 =
								((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_globalz00_bgl))));
							{	/* Globalize/ginfo.sch 370 */
								long BgL_arg1485z00_4306;

								BgL_arg1485z00_4306 = BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1240z00_4305),
									BgL_arg1485z00_4306);
							}
							{	/* Globalize/ginfo.sch 370 */
								BgL_objectz00_bglt BgL_tmpz00_5963;

								BgL_tmpz00_5963 = ((BgL_objectz00_bglt) BgL_new1240z00_4305);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5963, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1240z00_4305);
							BgL_auxz00_5958 = BgL_new1240z00_4305;
						}
						BgL_tmp1241z00_4303 = ((BgL_globalz00_bglt) BgL_auxz00_5958);
					}
					BgL_wide1242z00_4304 =
						((BgL_globalzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_globalzf2ginfozf2_bgl))));
					{	/* Globalize/ginfo.sch 370 */
						obj_t BgL_auxz00_5971;
						BgL_objectz00_bglt BgL_tmpz00_5969;

						BgL_auxz00_5971 = ((obj_t) BgL_wide1242z00_4304);
						BgL_tmpz00_5969 = ((BgL_objectz00_bglt) BgL_tmp1241z00_4303);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5969, BgL_auxz00_5971);
					}
					((BgL_objectz00_bglt) BgL_tmp1241z00_4303);
					{	/* Globalize/ginfo.sch 370 */
						long BgL_arg1473z00_4307;

						BgL_arg1473z00_4307 =
							BGL_CLASS_NUM(BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1241z00_4303), BgL_arg1473z00_4307);
					}
					BgL_new1243z00_4302 = ((BgL_globalz00_bglt) BgL_tmp1241z00_4303);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_new1243z00_4302)))->BgL_idz00) =
					((obj_t) BgL_id1220z00_271), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_namez00) =
					((obj_t) BgL_name1221z00_272), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1222z00_273), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_valuez00) =
					((BgL_valuez00_bglt) BgL_value1223z00_274), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_accessz00) =
					((obj_t) BgL_access1224z00_275), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_fastzd2alphazd2) =
					((obj_t) BgL_fastzd2alpha1225zd2_276), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_removablez00) =
					((obj_t) BgL_removable1226z00_277), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_occurrencez00) =
					((long) BgL_occurrence1227z00_278), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_occurrencewz00) =
					((long) BgL_occurrencew1228z00_279), BUNSPEC);
				((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
									BgL_new1243z00_4302)))->BgL_userzf3zf3) =
					((bool_t) BgL_userzf31229zf3_280), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_modulez00) =
					((obj_t) BgL_module1230z00_281), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_importz00) =
					((obj_t) BgL_import1231z00_282), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_evaluablezf3zf3) =
					((bool_t) BgL_evaluablezf31232zf3_283), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_evalzf3zf3) =
					((bool_t) BgL_evalzf31233zf3_284), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_libraryz00) =
					((obj_t) BgL_library1234z00_285), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_pragmaz00) =
					((obj_t) BgL_pragma1235z00_286), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_srcz00) =
					((obj_t) BgL_src1236z00_287), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BgL_jvmzd2typezd2name1237z00_288), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_initz00) =
					((obj_t) BgL_init1238z00_289), BUNSPEC);
				((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
									BgL_new1243z00_4302)))->BgL_aliasz00) =
					((obj_t) BgL_alias1239z00_290), BUNSPEC);
				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_6019;

					{
						obj_t BgL_auxz00_6020;

						{	/* Globalize/ginfo.sch 370 */
							BgL_objectz00_bglt BgL_tmpz00_6021;

							BgL_tmpz00_6021 = ((BgL_objectz00_bglt) BgL_new1243z00_4302);
							BgL_auxz00_6020 = BGL_OBJECT_WIDENING(BgL_tmpz00_6021);
						}
						BgL_auxz00_6019 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6020);
					}
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6019))->
							BgL_escapezf3zf3) = ((bool_t) BgL_escapezf31240zf3_291), BUNSPEC);
				}
				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_6026;

					{
						obj_t BgL_auxz00_6027;

						{	/* Globalize/ginfo.sch 370 */
							BgL_objectz00_bglt BgL_tmpz00_6028;

							BgL_tmpz00_6028 = ((BgL_objectz00_bglt) BgL_new1243z00_4302);
							BgL_auxz00_6027 = BGL_OBJECT_WIDENING(BgL_tmpz00_6028);
						}
						BgL_auxz00_6026 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6027);
					}
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6026))->
							BgL_globalzd2closurezd2) =
						((obj_t) BgL_globalzd2closure1241zd2_292), BUNSPEC);
				}
				return BgL_new1243z00_4302;
			}
		}

	}



/* &make-global/Ginfo */
	BgL_globalz00_bglt BGl_z62makezd2globalzf2Ginfoz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3631, obj_t BgL_id1220z00_3632, obj_t BgL_name1221z00_3633,
		obj_t BgL_type1222z00_3634, obj_t BgL_value1223z00_3635,
		obj_t BgL_access1224z00_3636, obj_t BgL_fastzd2alpha1225zd2_3637,
		obj_t BgL_removable1226z00_3638, obj_t BgL_occurrence1227z00_3639,
		obj_t BgL_occurrencew1228z00_3640, obj_t BgL_userzf31229zf3_3641,
		obj_t BgL_module1230z00_3642, obj_t BgL_import1231z00_3643,
		obj_t BgL_evaluablezf31232zf3_3644, obj_t BgL_evalzf31233zf3_3645,
		obj_t BgL_library1234z00_3646, obj_t BgL_pragma1235z00_3647,
		obj_t BgL_src1236z00_3648, obj_t BgL_jvmzd2typezd2name1237z00_3649,
		obj_t BgL_init1238z00_3650, obj_t BgL_alias1239z00_3651,
		obj_t BgL_escapezf31240zf3_3652, obj_t BgL_globalzd2closure1241zd2_3653)
	{
		{	/* Globalize/ginfo.sch 370 */
			return
				BGl_makezd2globalzf2Ginfoz20zzglobaliza7e_ginfoza7(BgL_id1220z00_3632,
				BgL_name1221z00_3633, ((BgL_typez00_bglt) BgL_type1222z00_3634),
				((BgL_valuez00_bglt) BgL_value1223z00_3635), BgL_access1224z00_3636,
				BgL_fastzd2alpha1225zd2_3637, BgL_removable1226z00_3638,
				(long) CINT(BgL_occurrence1227z00_3639),
				(long) CINT(BgL_occurrencew1228z00_3640),
				CBOOL(BgL_userzf31229zf3_3641), BgL_module1230z00_3642,
				BgL_import1231z00_3643, CBOOL(BgL_evaluablezf31232zf3_3644),
				CBOOL(BgL_evalzf31233zf3_3645), BgL_library1234z00_3646,
				BgL_pragma1235z00_3647, BgL_src1236z00_3648,
				BgL_jvmzd2typezd2name1237z00_3649, BgL_init1238z00_3650,
				BgL_alias1239z00_3651, CBOOL(BgL_escapezf31240zf3_3652),
				BgL_globalzd2closure1241zd2_3653);
		}

	}



/* global/Ginfo? */
	BGL_EXPORTED_DEF bool_t BGl_globalzf2Ginfozf3z01zzglobaliza7e_ginfoza7(obj_t
		BgL_objz00_293)
	{
		{	/* Globalize/ginfo.sch 371 */
			{	/* Globalize/ginfo.sch 371 */
				obj_t BgL_classz00_4308;

				BgL_classz00_4308 = BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7;
				if (BGL_OBJECTP(BgL_objz00_293))
					{	/* Globalize/ginfo.sch 371 */
						BgL_objectz00_bglt BgL_arg1807z00_4309;

						BgL_arg1807z00_4309 = (BgL_objectz00_bglt) (BgL_objz00_293);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Globalize/ginfo.sch 371 */
								long BgL_idxz00_4310;

								BgL_idxz00_4310 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4309);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4310 + 3L)) == BgL_classz00_4308);
							}
						else
							{	/* Globalize/ginfo.sch 371 */
								bool_t BgL_res2081z00_4313;

								{	/* Globalize/ginfo.sch 371 */
									obj_t BgL_oclassz00_4314;

									{	/* Globalize/ginfo.sch 371 */
										obj_t BgL_arg1815z00_4315;
										long BgL_arg1816z00_4316;

										BgL_arg1815z00_4315 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Globalize/ginfo.sch 371 */
											long BgL_arg1817z00_4317;

											BgL_arg1817z00_4317 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4309);
											BgL_arg1816z00_4316 = (BgL_arg1817z00_4317 - OBJECT_TYPE);
										}
										BgL_oclassz00_4314 =
											VECTOR_REF(BgL_arg1815z00_4315, BgL_arg1816z00_4316);
									}
									{	/* Globalize/ginfo.sch 371 */
										bool_t BgL__ortest_1115z00_4318;

										BgL__ortest_1115z00_4318 =
											(BgL_classz00_4308 == BgL_oclassz00_4314);
										if (BgL__ortest_1115z00_4318)
											{	/* Globalize/ginfo.sch 371 */
												BgL_res2081z00_4313 = BgL__ortest_1115z00_4318;
											}
										else
											{	/* Globalize/ginfo.sch 371 */
												long BgL_odepthz00_4319;

												{	/* Globalize/ginfo.sch 371 */
													obj_t BgL_arg1804z00_4320;

													BgL_arg1804z00_4320 = (BgL_oclassz00_4314);
													BgL_odepthz00_4319 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4320);
												}
												if ((3L < BgL_odepthz00_4319))
													{	/* Globalize/ginfo.sch 371 */
														obj_t BgL_arg1802z00_4321;

														{	/* Globalize/ginfo.sch 371 */
															obj_t BgL_arg1803z00_4322;

															BgL_arg1803z00_4322 = (BgL_oclassz00_4314);
															BgL_arg1802z00_4321 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4322,
																3L);
														}
														BgL_res2081z00_4313 =
															(BgL_arg1802z00_4321 == BgL_classz00_4308);
													}
												else
													{	/* Globalize/ginfo.sch 371 */
														BgL_res2081z00_4313 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2081z00_4313;
							}
					}
				else
					{	/* Globalize/ginfo.sch 371 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &global/Ginfo? */
	obj_t BGl_z62globalzf2Ginfozf3z63zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3654,
		obj_t BgL_objz00_3655)
	{
		{	/* Globalize/ginfo.sch 371 */
			return
				BBOOL(BGl_globalzf2Ginfozf3z01zzglobaliza7e_ginfoza7(BgL_objz00_3655));
		}

	}



/* global/Ginfo-nil */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_globalzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.sch 372 */
			{	/* Globalize/ginfo.sch 372 */
				obj_t BgL_classz00_2921;

				BgL_classz00_2921 = BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7;
				{	/* Globalize/ginfo.sch 372 */
					obj_t BgL__ortest_1117z00_2922;

					BgL__ortest_1117z00_2922 = BGL_CLASS_NIL(BgL_classz00_2921);
					if (CBOOL(BgL__ortest_1117z00_2922))
						{	/* Globalize/ginfo.sch 372 */
							return ((BgL_globalz00_bglt) BgL__ortest_1117z00_2922);
						}
					else
						{	/* Globalize/ginfo.sch 372 */
							return
								((BgL_globalz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2921));
						}
				}
			}
		}

	}



/* &global/Ginfo-nil */
	BgL_globalz00_bglt BGl_z62globalzf2Ginfozd2nilz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3656)
	{
		{	/* Globalize/ginfo.sch 372 */
			return BGl_globalzf2Ginfozd2nilz20zzglobaliza7e_ginfoza7();
		}

	}



/* global/Ginfo-global-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2globalzd2closurezf2zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_294)
	{
		{	/* Globalize/ginfo.sch 373 */
			{
				BgL_globalzf2ginfozf2_bglt BgL_auxz00_6073;

				{
					obj_t BgL_auxz00_6074;

					{	/* Globalize/ginfo.sch 373 */
						BgL_objectz00_bglt BgL_tmpz00_6075;

						BgL_tmpz00_6075 = ((BgL_objectz00_bglt) BgL_oz00_294);
						BgL_auxz00_6074 = BGL_OBJECT_WIDENING(BgL_tmpz00_6075);
					}
					BgL_auxz00_6073 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6074);
				}
				return
					(((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6073))->
					BgL_globalzd2closurezd2);
			}
		}

	}



/* &global/Ginfo-global-closure */
	obj_t BGl_z62globalzf2Ginfozd2globalzd2closurez90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3657, obj_t BgL_oz00_3658)
	{
		{	/* Globalize/ginfo.sch 373 */
			return
				BGl_globalzf2Ginfozd2globalzd2closurezf2zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3658));
		}

	}



/* global/Ginfo-global-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2globalzd2closurezd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_295, obj_t BgL_vz00_296)
	{
		{	/* Globalize/ginfo.sch 374 */
			{
				BgL_globalzf2ginfozf2_bglt BgL_auxz00_6082;

				{
					obj_t BgL_auxz00_6083;

					{	/* Globalize/ginfo.sch 374 */
						BgL_objectz00_bglt BgL_tmpz00_6084;

						BgL_tmpz00_6084 = ((BgL_objectz00_bglt) BgL_oz00_295);
						BgL_auxz00_6083 = BGL_OBJECT_WIDENING(BgL_tmpz00_6084);
					}
					BgL_auxz00_6082 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6083);
				}
				return
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6082))->
						BgL_globalzd2closurezd2) = ((obj_t) BgL_vz00_296), BUNSPEC);
			}
		}

	}



/* &global/Ginfo-global-closure-set! */
	obj_t
		BGl_z62globalzf2Ginfozd2globalzd2closurezd2setz12z50zzglobaliza7e_ginfoza7
		(obj_t BgL_envz00_3659, obj_t BgL_oz00_3660, obj_t BgL_vz00_3661)
	{
		{	/* Globalize/ginfo.sch 374 */
			return
				BGl_globalzf2Ginfozd2globalzd2closurezd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3660), BgL_vz00_3661);
		}

	}



/* global/Ginfo-escape? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzf2Ginfozd2escapezf3zd3zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_297)
	{
		{	/* Globalize/ginfo.sch 375 */
			{
				BgL_globalzf2ginfozf2_bglt BgL_auxz00_6091;

				{
					obj_t BgL_auxz00_6092;

					{	/* Globalize/ginfo.sch 375 */
						BgL_objectz00_bglt BgL_tmpz00_6093;

						BgL_tmpz00_6093 = ((BgL_objectz00_bglt) BgL_oz00_297);
						BgL_auxz00_6092 = BGL_OBJECT_WIDENING(BgL_tmpz00_6093);
					}
					BgL_auxz00_6091 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6092);
				}
				return
					(((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6091))->
					BgL_escapezf3zf3);
			}
		}

	}



/* &global/Ginfo-escape? */
	obj_t BGl_z62globalzf2Ginfozd2escapezf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3662, obj_t BgL_oz00_3663)
	{
		{	/* Globalize/ginfo.sch 375 */
			return
				BBOOL(BGl_globalzf2Ginfozd2escapezf3zd3zzglobaliza7e_ginfoza7(
					((BgL_globalz00_bglt) BgL_oz00_3663)));
		}

	}



/* global/Ginfo-escape?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2escapezf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_298, bool_t BgL_vz00_299)
	{
		{	/* Globalize/ginfo.sch 376 */
			{
				BgL_globalzf2ginfozf2_bglt BgL_auxz00_6101;

				{
					obj_t BgL_auxz00_6102;

					{	/* Globalize/ginfo.sch 376 */
						BgL_objectz00_bglt BgL_tmpz00_6103;

						BgL_tmpz00_6103 = ((BgL_objectz00_bglt) BgL_oz00_298);
						BgL_auxz00_6102 = BGL_OBJECT_WIDENING(BgL_tmpz00_6103);
					}
					BgL_auxz00_6101 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6102);
				}
				return
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6101))->
						BgL_escapezf3zf3) = ((bool_t) BgL_vz00_299), BUNSPEC);
			}
		}

	}



/* &global/Ginfo-escape?-set! */
	obj_t
		BGl_z62globalzf2Ginfozd2escapezf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3664, obj_t BgL_oz00_3665, obj_t BgL_vz00_3666)
	{
		{	/* Globalize/ginfo.sch 376 */
			return
				BGl_globalzf2Ginfozd2escapezf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3665), CBOOL(BgL_vz00_3666));
		}

	}



/* global/Ginfo-alias */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2aliasz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_300)
	{
		{	/* Globalize/ginfo.sch 377 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_300)))->BgL_aliasz00);
		}

	}



/* &global/Ginfo-alias */
	obj_t BGl_z62globalzf2Ginfozd2aliasz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3667, obj_t BgL_oz00_3668)
	{
		{	/* Globalize/ginfo.sch 377 */
			return
				BGl_globalzf2Ginfozd2aliasz20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3668));
		}

	}



/* global/Ginfo-alias-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2aliaszd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_301, obj_t BgL_vz00_302)
	{
		{	/* Globalize/ginfo.sch 378 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_301)))->BgL_aliasz00) =
				((obj_t) BgL_vz00_302), BUNSPEC);
		}

	}



/* &global/Ginfo-alias-set! */
	obj_t BGl_z62globalzf2Ginfozd2aliaszd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3669, obj_t BgL_oz00_3670, obj_t BgL_vz00_3671)
	{
		{	/* Globalize/ginfo.sch 378 */
			return
				BGl_globalzf2Ginfozd2aliaszd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3670), BgL_vz00_3671);
		}

	}



/* global/Ginfo-init */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2initz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_303)
	{
		{	/* Globalize/ginfo.sch 379 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_303)))->BgL_initz00);
		}

	}



/* &global/Ginfo-init */
	obj_t BGl_z62globalzf2Ginfozd2initz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3672, obj_t BgL_oz00_3673)
	{
		{	/* Globalize/ginfo.sch 379 */
			return
				BGl_globalzf2Ginfozd2initz20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3673));
		}

	}



/* global/Ginfo-init-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2initzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_304, obj_t BgL_vz00_305)
	{
		{	/* Globalize/ginfo.sch 380 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_304)))->BgL_initz00) =
				((obj_t) BgL_vz00_305), BUNSPEC);
		}

	}



/* &global/Ginfo-init-set! */
	obj_t BGl_z62globalzf2Ginfozd2initzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3674, obj_t BgL_oz00_3675, obj_t BgL_vz00_3676)
	{
		{	/* Globalize/ginfo.sch 380 */
			return
				BGl_globalzf2Ginfozd2initzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3675), BgL_vz00_3676);
		}

	}



/* global/Ginfo-jvm-type-name */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2jvmzd2typezd2namez20zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_306)
	{
		{	/* Globalize/ginfo.sch 381 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_306)))->BgL_jvmzd2typezd2namez00);
		}

	}



/* &global/Ginfo-jvm-type-name */
	obj_t BGl_z62globalzf2Ginfozd2jvmzd2typezd2namez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3677, obj_t BgL_oz00_3678)
	{
		{	/* Globalize/ginfo.sch 381 */
			return
				BGl_globalzf2Ginfozd2jvmzd2typezd2namez20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3678));
		}

	}



/* global/Ginfo-jvm-type-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2jvmzd2typezd2namezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_307, obj_t BgL_vz00_308)
	{
		{	/* Globalize/ginfo.sch 382 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_307)))->BgL_jvmzd2typezd2namez00) =
				((obj_t) BgL_vz00_308), BUNSPEC);
		}

	}



/* &global/Ginfo-jvm-type-name-set! */
	obj_t
		BGl_z62globalzf2Ginfozd2jvmzd2typezd2namezd2setz12z82zzglobaliza7e_ginfoza7
		(obj_t BgL_envz00_3679, obj_t BgL_oz00_3680, obj_t BgL_vz00_3681)
	{
		{	/* Globalize/ginfo.sch 382 */
			return
				BGl_globalzf2Ginfozd2jvmzd2typezd2namezd2setz12ze0zzglobaliza7e_ginfoza7
				(((BgL_globalz00_bglt) BgL_oz00_3680), BgL_vz00_3681);
		}

	}



/* global/Ginfo-src */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2srcz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_309)
	{
		{	/* Globalize/ginfo.sch 383 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_309)))->BgL_srcz00);
		}

	}



/* &global/Ginfo-src */
	obj_t BGl_z62globalzf2Ginfozd2srcz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3682, obj_t BgL_oz00_3683)
	{
		{	/* Globalize/ginfo.sch 383 */
			return
				BGl_globalzf2Ginfozd2srcz20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3683));
		}

	}



/* global/Ginfo-src-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2srczd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_310, obj_t BgL_vz00_311)
	{
		{	/* Globalize/ginfo.sch 384 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_310)))->BgL_srcz00) =
				((obj_t) BgL_vz00_311), BUNSPEC);
		}

	}



/* &global/Ginfo-src-set! */
	obj_t BGl_z62globalzf2Ginfozd2srczd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3684, obj_t BgL_oz00_3685, obj_t BgL_vz00_3686)
	{
		{	/* Globalize/ginfo.sch 384 */
			return
				BGl_globalzf2Ginfozd2srczd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3685), BgL_vz00_3686);
		}

	}



/* global/Ginfo-pragma */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2pragmaz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_312)
	{
		{	/* Globalize/ginfo.sch 385 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_312)))->BgL_pragmaz00);
		}

	}



/* &global/Ginfo-pragma */
	obj_t BGl_z62globalzf2Ginfozd2pragmaz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3687, obj_t BgL_oz00_3688)
	{
		{	/* Globalize/ginfo.sch 385 */
			return
				BGl_globalzf2Ginfozd2pragmaz20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3688));
		}

	}



/* global/Ginfo-pragma-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2pragmazd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_313, obj_t BgL_vz00_314)
	{
		{	/* Globalize/ginfo.sch 386 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_313)))->BgL_pragmaz00) =
				((obj_t) BgL_vz00_314), BUNSPEC);
		}

	}



/* &global/Ginfo-pragma-set! */
	obj_t BGl_z62globalzf2Ginfozd2pragmazd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3689, obj_t BgL_oz00_3690, obj_t BgL_vz00_3691)
	{
		{	/* Globalize/ginfo.sch 386 */
			return
				BGl_globalzf2Ginfozd2pragmazd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3690), BgL_vz00_3691);
		}

	}



/* global/Ginfo-library */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2libraryz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_315)
	{
		{	/* Globalize/ginfo.sch 387 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_315)))->BgL_libraryz00);
		}

	}



/* &global/Ginfo-library */
	obj_t BGl_z62globalzf2Ginfozd2libraryz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3692, obj_t BgL_oz00_3693)
	{
		{	/* Globalize/ginfo.sch 387 */
			return
				BGl_globalzf2Ginfozd2libraryz20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3693));
		}

	}



/* global/Ginfo-library-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2libraryzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_316, obj_t BgL_vz00_317)
	{
		{	/* Globalize/ginfo.sch 388 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_316)))->BgL_libraryz00) =
				((obj_t) BgL_vz00_317), BUNSPEC);
		}

	}



/* &global/Ginfo-library-set! */
	obj_t BGl_z62globalzf2Ginfozd2libraryzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3694, obj_t BgL_oz00_3695, obj_t BgL_vz00_3696)
	{
		{	/* Globalize/ginfo.sch 388 */
			return
				BGl_globalzf2Ginfozd2libraryzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3695), BgL_vz00_3696);
		}

	}



/* global/Ginfo-eval? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzf2Ginfozd2evalzf3zd3zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_318)
	{
		{	/* Globalize/ginfo.sch 389 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_318)))->BgL_evalzf3zf3);
		}

	}



/* &global/Ginfo-eval? */
	obj_t BGl_z62globalzf2Ginfozd2evalzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3697, obj_t BgL_oz00_3698)
	{
		{	/* Globalize/ginfo.sch 389 */
			return
				BBOOL(BGl_globalzf2Ginfozd2evalzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_globalz00_bglt) BgL_oz00_3698)));
		}

	}



/* global/Ginfo-eval?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2evalzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_319, bool_t BgL_vz00_320)
	{
		{	/* Globalize/ginfo.sch 390 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_319)))->BgL_evalzf3zf3) =
				((bool_t) BgL_vz00_320), BUNSPEC);
		}

	}



/* &global/Ginfo-eval?-set! */
	obj_t BGl_z62globalzf2Ginfozd2evalzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3699, obj_t BgL_oz00_3700, obj_t BgL_vz00_3701)
	{
		{	/* Globalize/ginfo.sch 390 */
			return
				BGl_globalzf2Ginfozd2evalzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3700), CBOOL(BgL_vz00_3701));
		}

	}



/* global/Ginfo-evaluable? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzf2Ginfozd2evaluablezf3zd3zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_321)
	{
		{	/* Globalize/ginfo.sch 391 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_321)))->BgL_evaluablezf3zf3);
		}

	}



/* &global/Ginfo-evaluable? */
	obj_t BGl_z62globalzf2Ginfozd2evaluablezf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3702, obj_t BgL_oz00_3703)
	{
		{	/* Globalize/ginfo.sch 391 */
			return
				BBOOL(BGl_globalzf2Ginfozd2evaluablezf3zd3zzglobaliza7e_ginfoza7(
					((BgL_globalz00_bglt) BgL_oz00_3703)));
		}

	}



/* global/Ginfo-evaluable?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2evaluablezf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_322, bool_t BgL_vz00_323)
	{
		{	/* Globalize/ginfo.sch 392 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_322)))->BgL_evaluablezf3zf3) =
				((bool_t) BgL_vz00_323), BUNSPEC);
		}

	}



/* &global/Ginfo-evaluable?-set! */
	obj_t
		BGl_z62globalzf2Ginfozd2evaluablezf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3704, obj_t BgL_oz00_3705, obj_t BgL_vz00_3706)
	{
		{	/* Globalize/ginfo.sch 392 */
			return
				BGl_globalzf2Ginfozd2evaluablezf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3705), CBOOL(BgL_vz00_3706));
		}

	}



/* global/Ginfo-import */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2importz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_324)
	{
		{	/* Globalize/ginfo.sch 393 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_324)))->BgL_importz00);
		}

	}



/* &global/Ginfo-import */
	obj_t BGl_z62globalzf2Ginfozd2importz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3707, obj_t BgL_oz00_3708)
	{
		{	/* Globalize/ginfo.sch 393 */
			return
				BGl_globalzf2Ginfozd2importz20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3708));
		}

	}



/* global/Ginfo-import-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2importzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_325, obj_t BgL_vz00_326)
	{
		{	/* Globalize/ginfo.sch 394 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_325)))->BgL_importz00) =
				((obj_t) BgL_vz00_326), BUNSPEC);
		}

	}



/* &global/Ginfo-import-set! */
	obj_t BGl_z62globalzf2Ginfozd2importzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3709, obj_t BgL_oz00_3710, obj_t BgL_vz00_3711)
	{
		{	/* Globalize/ginfo.sch 394 */
			return
				BGl_globalzf2Ginfozd2importzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3710), BgL_vz00_3711);
		}

	}



/* global/Ginfo-module */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2modulez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_327)
	{
		{	/* Globalize/ginfo.sch 395 */
			return
				(((BgL_globalz00_bglt) COBJECT(
						((BgL_globalz00_bglt) BgL_oz00_327)))->BgL_modulez00);
		}

	}



/* &global/Ginfo-module */
	obj_t BGl_z62globalzf2Ginfozd2modulez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3712, obj_t BgL_oz00_3713)
	{
		{	/* Globalize/ginfo.sch 395 */
			return
				BGl_globalzf2Ginfozd2modulez20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3713));
		}

	}



/* global/Ginfo-module-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2modulezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_328, obj_t BgL_vz00_329)
	{
		{	/* Globalize/ginfo.sch 396 */
			return
				((((BgL_globalz00_bglt) COBJECT(
							((BgL_globalz00_bglt) BgL_oz00_328)))->BgL_modulez00) =
				((obj_t) BgL_vz00_329), BUNSPEC);
		}

	}



/* &global/Ginfo-module-set! */
	obj_t BGl_z62globalzf2Ginfozd2modulezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3714, obj_t BgL_oz00_3715, obj_t BgL_vz00_3716)
	{
		{	/* Globalize/ginfo.sch 396 */
			return
				BGl_globalzf2Ginfozd2modulezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3715), BgL_vz00_3716);
		}

	}



/* global/Ginfo-user? */
	BGL_EXPORTED_DEF bool_t
		BGl_globalzf2Ginfozd2userzf3zd3zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_330)
	{
		{	/* Globalize/ginfo.sch 397 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_330)))->BgL_userzf3zf3);
		}

	}



/* &global/Ginfo-user? */
	obj_t BGl_z62globalzf2Ginfozd2userzf3zb1zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3717, obj_t BgL_oz00_3718)
	{
		{	/* Globalize/ginfo.sch 397 */
			return
				BBOOL(BGl_globalzf2Ginfozd2userzf3zd3zzglobaliza7e_ginfoza7(
					((BgL_globalz00_bglt) BgL_oz00_3718)));
		}

	}



/* global/Ginfo-user?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2userzf3zd2setz12z13zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_331, bool_t BgL_vz00_332)
	{
		{	/* Globalize/ginfo.sch 398 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_331)))->BgL_userzf3zf3) =
				((bool_t) BgL_vz00_332), BUNSPEC);
		}

	}



/* &global/Ginfo-user?-set! */
	obj_t BGl_z62globalzf2Ginfozd2userzf3zd2setz12z71zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3719, obj_t BgL_oz00_3720, obj_t BgL_vz00_3721)
	{
		{	/* Globalize/ginfo.sch 398 */
			return
				BGl_globalzf2Ginfozd2userzf3zd2setz12z13zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3720), CBOOL(BgL_vz00_3721));
		}

	}



/* global/Ginfo-occurrencew */
	BGL_EXPORTED_DEF long
		BGl_globalzf2Ginfozd2occurrencewz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_333)
	{
		{	/* Globalize/ginfo.sch 399 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_333)))->BgL_occurrencewz00);
		}

	}



/* &global/Ginfo-occurrencew */
	obj_t BGl_z62globalzf2Ginfozd2occurrencewz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3722, obj_t BgL_oz00_3723)
	{
		{	/* Globalize/ginfo.sch 399 */
			return
				BINT(BGl_globalzf2Ginfozd2occurrencewz20zzglobaliza7e_ginfoza7(
					((BgL_globalz00_bglt) BgL_oz00_3723)));
		}

	}



/* global/Ginfo-occurrencew-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2occurrencewzd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_334, long BgL_vz00_335)
	{
		{	/* Globalize/ginfo.sch 400 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_334)))->BgL_occurrencewz00) =
				((long) BgL_vz00_335), BUNSPEC);
		}

	}



/* &global/Ginfo-occurrencew-set! */
	obj_t
		BGl_z62globalzf2Ginfozd2occurrencewzd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3724, obj_t BgL_oz00_3725, obj_t BgL_vz00_3726)
	{
		{	/* Globalize/ginfo.sch 400 */
			return
				BGl_globalzf2Ginfozd2occurrencewzd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3725), (long) CINT(BgL_vz00_3726));
		}

	}



/* global/Ginfo-occurrence */
	BGL_EXPORTED_DEF long
		BGl_globalzf2Ginfozd2occurrencez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_336)
	{
		{	/* Globalize/ginfo.sch 401 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_336)))->BgL_occurrencez00);
		}

	}



/* &global/Ginfo-occurrence */
	obj_t BGl_z62globalzf2Ginfozd2occurrencez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3727, obj_t BgL_oz00_3728)
	{
		{	/* Globalize/ginfo.sch 401 */
			return
				BINT(BGl_globalzf2Ginfozd2occurrencez20zzglobaliza7e_ginfoza7(
					((BgL_globalz00_bglt) BgL_oz00_3728)));
		}

	}



/* global/Ginfo-occurrence-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2occurrencezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_337, long BgL_vz00_338)
	{
		{	/* Globalize/ginfo.sch 402 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_337)))->BgL_occurrencez00) =
				((long) BgL_vz00_338), BUNSPEC);
		}

	}



/* &global/Ginfo-occurrence-set! */
	obj_t
		BGl_z62globalzf2Ginfozd2occurrencezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3729, obj_t BgL_oz00_3730, obj_t BgL_vz00_3731)
	{
		{	/* Globalize/ginfo.sch 402 */
			return
				BGl_globalzf2Ginfozd2occurrencezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3730), (long) CINT(BgL_vz00_3731));
		}

	}



/* global/Ginfo-removable */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2removablez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_339)
	{
		{	/* Globalize/ginfo.sch 403 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_339)))->BgL_removablez00);
		}

	}



/* &global/Ginfo-removable */
	obj_t BGl_z62globalzf2Ginfozd2removablez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3732, obj_t BgL_oz00_3733)
	{
		{	/* Globalize/ginfo.sch 403 */
			return
				BGl_globalzf2Ginfozd2removablez20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3733));
		}

	}



/* global/Ginfo-removable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2removablezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_340, obj_t BgL_vz00_341)
	{
		{	/* Globalize/ginfo.sch 404 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_340)))->BgL_removablez00) =
				((obj_t) BgL_vz00_341), BUNSPEC);
		}

	}



/* &global/Ginfo-removable-set! */
	obj_t
		BGl_z62globalzf2Ginfozd2removablezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3734, obj_t BgL_oz00_3735, obj_t BgL_vz00_3736)
	{
		{	/* Globalize/ginfo.sch 404 */
			return
				BGl_globalzf2Ginfozd2removablezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3735), BgL_vz00_3736);
		}

	}



/* global/Ginfo-fast-alpha */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2fastzd2alphazf2zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_342)
	{
		{	/* Globalize/ginfo.sch 405 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_342)))->BgL_fastzd2alphazd2);
		}

	}



/* &global/Ginfo-fast-alpha */
	obj_t BGl_z62globalzf2Ginfozd2fastzd2alphaz90zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3737, obj_t BgL_oz00_3738)
	{
		{	/* Globalize/ginfo.sch 405 */
			return
				BGl_globalzf2Ginfozd2fastzd2alphazf2zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3738));
		}

	}



/* global/Ginfo-fast-alpha-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2fastzd2alphazd2setz12z32zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_343, obj_t BgL_vz00_344)
	{
		{	/* Globalize/ginfo.sch 406 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_343)))->BgL_fastzd2alphazd2) =
				((obj_t) BgL_vz00_344), BUNSPEC);
		}

	}



/* &global/Ginfo-fast-alpha-set! */
	obj_t
		BGl_z62globalzf2Ginfozd2fastzd2alphazd2setz12z50zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3739, obj_t BgL_oz00_3740, obj_t BgL_vz00_3741)
	{
		{	/* Globalize/ginfo.sch 406 */
			return
				BGl_globalzf2Ginfozd2fastzd2alphazd2setz12z32zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3740), BgL_vz00_3741);
		}

	}



/* global/Ginfo-access */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2accessz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_345)
	{
		{	/* Globalize/ginfo.sch 407 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_345)))->BgL_accessz00);
		}

	}



/* &global/Ginfo-access */
	obj_t BGl_z62globalzf2Ginfozd2accessz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3742, obj_t BgL_oz00_3743)
	{
		{	/* Globalize/ginfo.sch 407 */
			return
				BGl_globalzf2Ginfozd2accessz20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3743));
		}

	}



/* global/Ginfo-access-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2accesszd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_346, obj_t BgL_vz00_347)
	{
		{	/* Globalize/ginfo.sch 408 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_346)))->BgL_accessz00) =
				((obj_t) BgL_vz00_347), BUNSPEC);
		}

	}



/* &global/Ginfo-access-set! */
	obj_t BGl_z62globalzf2Ginfozd2accesszd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3744, obj_t BgL_oz00_3745, obj_t BgL_vz00_3746)
	{
		{	/* Globalize/ginfo.sch 408 */
			return
				BGl_globalzf2Ginfozd2accesszd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3745), BgL_vz00_3746);
		}

	}



/* global/Ginfo-value */
	BGL_EXPORTED_DEF BgL_valuez00_bglt
		BGl_globalzf2Ginfozd2valuez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_348)
	{
		{	/* Globalize/ginfo.sch 409 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_348)))->BgL_valuez00);
		}

	}



/* &global/Ginfo-value */
	BgL_valuez00_bglt BGl_z62globalzf2Ginfozd2valuez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3747, obj_t BgL_oz00_3748)
	{
		{	/* Globalize/ginfo.sch 409 */
			return
				BGl_globalzf2Ginfozd2valuez20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3748));
		}

	}



/* global/Ginfo-value-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2valuezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_349, BgL_valuez00_bglt BgL_vz00_350)
	{
		{	/* Globalize/ginfo.sch 410 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_349)))->BgL_valuez00) =
				((BgL_valuez00_bglt) BgL_vz00_350), BUNSPEC);
		}

	}



/* &global/Ginfo-value-set! */
	obj_t BGl_z62globalzf2Ginfozd2valuezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3749, obj_t BgL_oz00_3750, obj_t BgL_vz00_3751)
	{
		{	/* Globalize/ginfo.sch 410 */
			return
				BGl_globalzf2Ginfozd2valuezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3750),
				((BgL_valuez00_bglt) BgL_vz00_3751));
		}

	}



/* global/Ginfo-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_globalzf2Ginfozd2typez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_351)
	{
		{	/* Globalize/ginfo.sch 411 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_351)))->BgL_typez00);
		}

	}



/* &global/Ginfo-type */
	BgL_typez00_bglt BGl_z62globalzf2Ginfozd2typez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3752, obj_t BgL_oz00_3753)
	{
		{	/* Globalize/ginfo.sch 411 */
			return
				BGl_globalzf2Ginfozd2typez20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3753));
		}

	}



/* global/Ginfo-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2typezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_352, BgL_typez00_bglt BgL_vz00_353)
	{
		{	/* Globalize/ginfo.sch 412 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_352)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_353), BUNSPEC);
		}

	}



/* &global/Ginfo-type-set! */
	obj_t BGl_z62globalzf2Ginfozd2typezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3754, obj_t BgL_oz00_3755, obj_t BgL_vz00_3756)
	{
		{	/* Globalize/ginfo.sch 412 */
			return
				BGl_globalzf2Ginfozd2typezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3755),
				((BgL_typez00_bglt) BgL_vz00_3756));
		}

	}



/* global/Ginfo-name */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2namez20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_354)
	{
		{	/* Globalize/ginfo.sch 413 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_354)))->BgL_namez00);
		}

	}



/* &global/Ginfo-name */
	obj_t BGl_z62globalzf2Ginfozd2namez42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3757, obj_t BgL_oz00_3758)
	{
		{	/* Globalize/ginfo.sch 413 */
			return
				BGl_globalzf2Ginfozd2namez20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3758));
		}

	}



/* global/Ginfo-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2namezd2setz12ze0zzglobaliza7e_ginfoza7
		(BgL_globalz00_bglt BgL_oz00_355, obj_t BgL_vz00_356)
	{
		{	/* Globalize/ginfo.sch 414 */
			return
				((((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_oz00_355)))->BgL_namez00) =
				((obj_t) BgL_vz00_356), BUNSPEC);
		}

	}



/* &global/Ginfo-name-set! */
	obj_t BGl_z62globalzf2Ginfozd2namezd2setz12z82zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3759, obj_t BgL_oz00_3760, obj_t BgL_vz00_3761)
	{
		{	/* Globalize/ginfo.sch 414 */
			return
				BGl_globalzf2Ginfozd2namezd2setz12ze0zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3760), BgL_vz00_3761);
		}

	}



/* global/Ginfo-id */
	BGL_EXPORTED_DEF obj_t
		BGl_globalzf2Ginfozd2idz20zzglobaliza7e_ginfoza7(BgL_globalz00_bglt
		BgL_oz00_357)
	{
		{	/* Globalize/ginfo.sch 415 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt) BgL_oz00_357)))->BgL_idz00);
		}

	}



/* &global/Ginfo-id */
	obj_t BGl_z62globalzf2Ginfozd2idz42zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3762, obj_t BgL_oz00_3763)
	{
		{	/* Globalize/ginfo.sch 415 */
			return
				BGl_globalzf2Ginfozd2idz20zzglobaliza7e_ginfoza7(
				((BgL_globalz00_bglt) BgL_oz00_3763));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.scm 15 */
			{	/* Globalize/ginfo.scm 20 */
				obj_t BgL_arg1514z00_1743;
				obj_t BgL_arg1516z00_1744;

				{	/* Globalize/ginfo.scm 20 */
					obj_t BgL_v1382z00_1807;

					BgL_v1382z00_1807 = create_vector(20L);
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1561z00_1808;

						BgL_arg1561z00_1808 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2084z00zzglobaliza7e_ginfoza7,
							BGl_proc2083z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2082z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1382z00_1807, 0L, BgL_arg1561z00_1808);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1575z00_1821;

						BgL_arg1575z00_1821 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc2087z00zzglobaliza7e_ginfoza7,
							BGl_proc2086z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2085z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 1L, BgL_arg1575z00_1821);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1589z00_1834;

						BgL_arg1589z00_1834 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc2090z00zzglobaliza7e_ginfoza7,
							BGl_proc2089z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2088z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 2L, BgL_arg1589z00_1834);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1602z00_1847;

						BgL_arg1602z00_1847 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc2093z00zzglobaliza7e_ginfoza7,
							BGl_proc2092z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2091z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 3L, BgL_arg1602z00_1847);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1613z00_1860;

						BgL_arg1613z00_1860 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc2096z00zzglobaliza7e_ginfoza7,
							BGl_proc2095z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2094z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 4L, BgL_arg1613z00_1860);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1629z00_1873;

						BgL_arg1629z00_1873 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc2099z00zzglobaliza7e_ginfoza7,
							BGl_proc2098z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2097z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 5L, BgL_arg1629z00_1873);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1650z00_1886;

						BgL_arg1650z00_1886 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc2102z00zzglobaliza7e_ginfoza7,
							BGl_proc2101z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2100z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 6L, BgL_arg1650z00_1886);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1675z00_1899;

						BgL_arg1675z00_1899 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(9),
							BGl_proc2105z00zzglobaliza7e_ginfoza7,
							BGl_proc2104z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2103z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 7L, BgL_arg1675z00_1899);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1692z00_1912;

						BgL_arg1692z00_1912 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc2108z00zzglobaliza7e_ginfoza7,
							BGl_proc2107z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2106z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 8L, BgL_arg1692z00_1912);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1705z00_1925;

						BgL_arg1705z00_1925 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc2111z00zzglobaliza7e_ginfoza7,
							BGl_proc2110z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2109z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 9L, BgL_arg1705z00_1925);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1714z00_1938;

						BgL_arg1714z00_1938 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc2114z00zzglobaliza7e_ginfoza7,
							BGl_proc2113z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2112z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 10L, BgL_arg1714z00_1938);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1724z00_1951;

						BgL_arg1724z00_1951 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(13),
							BGl_proc2117z00zzglobaliza7e_ginfoza7,
							BGl_proc2116z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2115z00zzglobaliza7e_ginfoza7,
							CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1382z00_1807, 11L, BgL_arg1724z00_1951);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1739z00_1964;

						BgL_arg1739z00_1964 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2120z00zzglobaliza7e_ginfoza7,
							BGl_proc2119z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2118z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 12L, BgL_arg1739z00_1964);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1751z00_1977;

						BgL_arg1751z00_1977 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(16),
							BGl_proc2123z00zzglobaliza7e_ginfoza7,
							BGl_proc2122z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2121z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 13L, BgL_arg1751z00_1977);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1761z00_1990;

						BgL_arg1761z00_1990 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc2126z00zzglobaliza7e_ginfoza7,
							BGl_proc2125z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2124z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 14L, BgL_arg1761z00_1990);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1771z00_2003;

						BgL_arg1771z00_2003 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc2129z00zzglobaliza7e_ginfoza7,
							BGl_proc2128z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2127z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 15L, BgL_arg1771z00_2003);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1805z00_2016;

						BgL_arg1805z00_2016 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(19),
							BGl_proc2132z00zzglobaliza7e_ginfoza7,
							BGl_proc2131z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2130z00zzglobaliza7e_ginfoza7,
							CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1382z00_1807, 16L, BgL_arg1805z00_2016);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1820z00_2029;

						BgL_arg1820z00_2029 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc2135z00zzglobaliza7e_ginfoza7,
							BGl_proc2134z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2133z00zzglobaliza7e_ginfoza7,
							CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1382z00_1807, 17L, BgL_arg1820z00_2029);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1835z00_2042;

						BgL_arg1835z00_2042 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2138z00zzglobaliza7e_ginfoza7,
							BGl_proc2137z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2136z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 18L, BgL_arg1835z00_2042);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1842z00_2055;

						BgL_arg1842z00_2055 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2141z00zzglobaliza7e_ginfoza7,
							BGl_proc2140z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2139z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1382z00_1807, 19L, BgL_arg1842z00_2055);
					}
					BgL_arg1514z00_1743 = BgL_v1382z00_1807;
				}
				{	/* Globalize/ginfo.scm 20 */
					obj_t BgL_v1383z00_2068;

					BgL_v1383z00_2068 = create_vector(0L);
					BgL_arg1516z00_1744 = BgL_v1383z00_2068;
				}
				BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(23),
					CNST_TABLE_REF(24), BGl_sfunz00zzast_varz00, 7595L,
					BGl_proc2145z00zzglobaliza7e_ginfoza7,
					BGl_proc2144z00zzglobaliza7e_ginfoza7, BFALSE,
					BGl_proc2143z00zzglobaliza7e_ginfoza7,
					BGl_proc2142z00zzglobaliza7e_ginfoza7, BgL_arg1514z00_1743,
					BgL_arg1516z00_1744);
			}
			{	/* Globalize/ginfo.scm 62 */
				obj_t BgL_arg1853z00_2077;
				obj_t BgL_arg1854z00_2078;

				{	/* Globalize/ginfo.scm 62 */
					obj_t BgL_v1384z00_2105;

					BgL_v1384z00_2105 = create_vector(5L);
					{	/* Globalize/ginfo.scm 62 */
						obj_t BgL_arg1866z00_2106;

						BgL_arg1866z00_2106 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2148z00zzglobaliza7e_ginfoza7,
							BGl_proc2147z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2146z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1384z00_2105, 0L, BgL_arg1866z00_2106);
					}
					{	/* Globalize/ginfo.scm 62 */
						obj_t BgL_arg1874z00_2119;

						BgL_arg1874z00_2119 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2151z00zzglobaliza7e_ginfoza7,
							BGl_proc2150z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2149z00zzglobaliza7e_ginfoza7,
							CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1384z00_2105, 1L, BgL_arg1874z00_2119);
					}
					{	/* Globalize/ginfo.scm 62 */
						obj_t BgL_arg1882z00_2132;

						BgL_arg1882z00_2132 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(13),
							BGl_proc2154z00zzglobaliza7e_ginfoza7,
							BGl_proc2153z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2152z00zzglobaliza7e_ginfoza7,
							CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1384z00_2105, 2L, BgL_arg1882z00_2132);
					}
					{	/* Globalize/ginfo.scm 62 */
						obj_t BgL_arg1889z00_2145;

						BgL_arg1889z00_2145 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc2157z00zzglobaliza7e_ginfoza7,
							BGl_proc2156z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2155z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1384z00_2105, 3L, BgL_arg1889z00_2145);
					}
					{	/* Globalize/ginfo.scm 62 */
						obj_t BgL_arg1896z00_2158;

						BgL_arg1896z00_2158 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2160z00zzglobaliza7e_ginfoza7,
							BGl_proc2159z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2158z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1384z00_2105, 4L, BgL_arg1896z00_2158);
					}
					BgL_arg1853z00_2077 = BgL_v1384z00_2105;
				}
				{	/* Globalize/ginfo.scm 62 */
					obj_t BgL_v1385z00_2171;

					BgL_v1385z00_2171 = create_vector(0L);
					BgL_arg1854z00_2078 = BgL_v1385z00_2171;
				}
				BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(28),
					CNST_TABLE_REF(24), BGl_svarz00zzast_varz00, 23020L,
					BGl_proc2164z00zzglobaliza7e_ginfoza7,
					BGl_proc2163z00zzglobaliza7e_ginfoza7, BFALSE,
					BGl_proc2162z00zzglobaliza7e_ginfoza7,
					BGl_proc2161z00zzglobaliza7e_ginfoza7, BgL_arg1853z00_2077,
					BgL_arg1854z00_2078);
			}
			{	/* Globalize/ginfo.scm 74 */
				obj_t BgL_arg1911z00_2180;
				obj_t BgL_arg1912z00_2181;

				{	/* Globalize/ginfo.scm 74 */
					obj_t BgL_v1386z00_2208;

					BgL_v1386z00_2208 = create_vector(4L);
					{	/* Globalize/ginfo.scm 74 */
						obj_t BgL_arg1926z00_2209;

						BgL_arg1926z00_2209 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2167z00zzglobaliza7e_ginfoza7,
							BGl_proc2166z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2165z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1386z00_2208, 0L, BgL_arg1926z00_2209);
					}
					{	/* Globalize/ginfo.scm 74 */
						obj_t BgL_arg1933z00_2222;

						BgL_arg1933z00_2222 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2170z00zzglobaliza7e_ginfoza7,
							BGl_proc2169z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2168z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1386z00_2208, 1L, BgL_arg1933z00_2222);
					}
					{	/* Globalize/ginfo.scm 74 */
						obj_t BgL_arg1940z00_2235;

						BgL_arg1940z00_2235 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2173z00zzglobaliza7e_ginfoza7,
							BGl_proc2172z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2171z00zzglobaliza7e_ginfoza7,
							CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1386z00_2208, 2L, BgL_arg1940z00_2235);
					}
					{	/* Globalize/ginfo.scm 74 */
						obj_t BgL_arg1947z00_2248;

						BgL_arg1947z00_2248 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(13),
							BGl_proc2176z00zzglobaliza7e_ginfoza7,
							BGl_proc2175z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2174z00zzglobaliza7e_ginfoza7,
							CNST_TABLE_REF(14));
						VECTOR_SET(BgL_v1386z00_2208, 3L, BgL_arg1947z00_2248);
					}
					BgL_arg1911z00_2180 = BgL_v1386z00_2208;
				}
				{	/* Globalize/ginfo.scm 74 */
					obj_t BgL_v1387z00_2261;

					BgL_v1387z00_2261 = create_vector(0L);
					BgL_arg1912z00_2181 = BgL_v1387z00_2261;
				}
				BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(29),
					CNST_TABLE_REF(24), BGl_sexitz00zzast_varz00, 59541L,
					BGl_proc2180z00zzglobaliza7e_ginfoza7,
					BGl_proc2179z00zzglobaliza7e_ginfoza7, BFALSE,
					BGl_proc2178z00zzglobaliza7e_ginfoza7,
					BGl_proc2177z00zzglobaliza7e_ginfoza7, BgL_arg1911z00_2180,
					BgL_arg1912z00_2181);
			}
			{	/* Globalize/ginfo.scm 84 */
				obj_t BgL_arg1958z00_2270;
				obj_t BgL_arg1959z00_2271;

				{	/* Globalize/ginfo.scm 84 */
					obj_t BgL_v1388z00_2307;

					BgL_v1388z00_2307 = create_vector(2L);
					{	/* Globalize/ginfo.scm 84 */
						obj_t BgL_arg1970z00_2308;

						BgL_arg1970z00_2308 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(30),
							BGl_proc2183z00zzglobaliza7e_ginfoza7,
							BGl_proc2182z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2181z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1388z00_2307, 0L, BgL_arg1970z00_2308);
					}
					{	/* Globalize/ginfo.scm 84 */
						obj_t BgL_arg1977z00_2321;

						BgL_arg1977z00_2321 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(31),
							BGl_proc2186z00zzglobaliza7e_ginfoza7,
							BGl_proc2185z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2184z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1388z00_2307, 1L, BgL_arg1977z00_2321);
					}
					BgL_arg1958z00_2270 = BgL_v1388z00_2307;
				}
				{	/* Globalize/ginfo.scm 84 */
					obj_t BgL_v1389z00_2334;

					BgL_v1389z00_2334 = create_vector(0L);
					BgL_arg1959z00_2271 = BgL_v1389z00_2334;
				}
				BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(32),
					CNST_TABLE_REF(24), BGl_localz00zzast_varz00, 65245L,
					BGl_proc2190z00zzglobaliza7e_ginfoza7,
					BGl_proc2189z00zzglobaliza7e_ginfoza7, BFALSE,
					BGl_proc2188z00zzglobaliza7e_ginfoza7,
					BGl_proc2187z00zzglobaliza7e_ginfoza7, BgL_arg1958z00_2270,
					BgL_arg1959z00_2271);
			}
			{	/* Globalize/ginfo.scm 90 */
				obj_t BgL_arg1988z00_2343;
				obj_t BgL_arg1989z00_2344;

				{	/* Globalize/ginfo.scm 90 */
					obj_t BgL_v1390z00_2387;

					BgL_v1390z00_2387 = create_vector(2L);
					{	/* Globalize/ginfo.scm 90 */
						obj_t BgL_arg2000z00_2388;

						BgL_arg2000z00_2388 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(30),
							BGl_proc2193z00zzglobaliza7e_ginfoza7,
							BGl_proc2192z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2191z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1390z00_2387, 0L, BgL_arg2000z00_2388);
					}
					{	/* Globalize/ginfo.scm 90 */
						obj_t BgL_arg2007z00_2401;

						BgL_arg2007z00_2401 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc2196z00zzglobaliza7e_ginfoza7,
							BGl_proc2195z00zzglobaliza7e_ginfoza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2194z00zzglobaliza7e_ginfoza7, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1390z00_2387, 1L, BgL_arg2007z00_2401);
					}
					BgL_arg1988z00_2343 = BgL_v1390z00_2387;
				}
				{	/* Globalize/ginfo.scm 90 */
					obj_t BgL_v1391z00_2414;

					BgL_v1391z00_2414 = create_vector(0L);
					BgL_arg1989z00_2344 = BgL_v1391z00_2414;
				}
				return (BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(34),
						CNST_TABLE_REF(24), BGl_globalz00zzast_varz00, 29682L,
						BGl_proc2200z00zzglobaliza7e_ginfoza7,
						BGl_proc2199z00zzglobaliza7e_ginfoza7, BFALSE,
						BGl_proc2198z00zzglobaliza7e_ginfoza7,
						BGl_proc2197z00zzglobaliza7e_ginfoza7, BgL_arg1988z00_2343,
						BgL_arg1989z00_2344), BUNSPEC);
			}
		}

	}



/* &lambda1996 */
	BgL_globalz00_bglt BGl_z62lambda1996z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3883, obj_t BgL_o1222z00_3884)
	{
		{	/* Globalize/ginfo.scm 90 */
			{	/* Globalize/ginfo.scm 90 */
				long BgL_arg1997z00_4324;

				{	/* Globalize/ginfo.scm 90 */
					obj_t BgL_arg1998z00_4325;

					{	/* Globalize/ginfo.scm 90 */
						obj_t BgL_arg1999z00_4326;

						{	/* Globalize/ginfo.scm 90 */
							obj_t BgL_arg1815z00_4327;
							long BgL_arg1816z00_4328;

							BgL_arg1815z00_4327 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Globalize/ginfo.scm 90 */
								long BgL_arg1817z00_4329;

								BgL_arg1817z00_4329 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_globalz00_bglt) BgL_o1222z00_3884)));
								BgL_arg1816z00_4328 = (BgL_arg1817z00_4329 - OBJECT_TYPE);
							}
							BgL_arg1999z00_4326 =
								VECTOR_REF(BgL_arg1815z00_4327, BgL_arg1816z00_4328);
						}
						BgL_arg1998z00_4325 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1999z00_4326);
					}
					{	/* Globalize/ginfo.scm 90 */
						obj_t BgL_tmpz00_6443;

						BgL_tmpz00_6443 = ((obj_t) BgL_arg1998z00_4325);
						BgL_arg1997z00_4324 = BGL_CLASS_NUM(BgL_tmpz00_6443);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) BgL_o1222z00_3884)), BgL_arg1997z00_4324);
			}
			{	/* Globalize/ginfo.scm 90 */
				BgL_objectz00_bglt BgL_tmpz00_6449;

				BgL_tmpz00_6449 =
					((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_o1222z00_3884));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6449, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_o1222z00_3884));
			return ((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1222z00_3884));
		}

	}



/* &<@anonymous:1995> */
	obj_t BGl_z62zc3z04anonymousza31995ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3885, obj_t BgL_new1221z00_3886)
	{
		{	/* Globalize/ginfo.scm 90 */
			{
				BgL_globalz00_bglt BgL_auxz00_6457;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1221z00_3886))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(35)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_6465;

					{	/* Globalize/ginfo.scm 90 */
						obj_t BgL_classz00_4331;

						BgL_classz00_4331 = BGl_typez00zztype_typez00;
						{	/* Globalize/ginfo.scm 90 */
							obj_t BgL__ortest_1117z00_4332;

							BgL__ortest_1117z00_4332 = BGL_CLASS_NIL(BgL_classz00_4331);
							if (CBOOL(BgL__ortest_1117z00_4332))
								{	/* Globalize/ginfo.scm 90 */
									BgL_auxz00_6465 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4332);
								}
							else
								{	/* Globalize/ginfo.scm 90 */
									BgL_auxz00_6465 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4331));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1221z00_3886))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_6465), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_6475;

					{	/* Globalize/ginfo.scm 90 */
						obj_t BgL_classz00_4333;

						BgL_classz00_4333 = BGl_valuez00zzast_varz00;
						{	/* Globalize/ginfo.scm 90 */
							obj_t BgL__ortest_1117z00_4334;

							BgL__ortest_1117z00_4334 = BGL_CLASS_NIL(BgL_classz00_4333);
							if (CBOOL(BgL__ortest_1117z00_4334))
								{	/* Globalize/ginfo.scm 90 */
									BgL_auxz00_6475 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_4334);
								}
							else
								{	/* Globalize/ginfo.scm 90 */
									BgL_auxz00_6475 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4333));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1221z00_3886))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_6475), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1221z00_3886))))->
						BgL_accessz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_modulez00) =
					((obj_t) CNST_TABLE_REF(35)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_importz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_evaluablezf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_evalzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_libraryz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_pragmaz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_srcz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BGl_string2201z00zzglobaliza7e_ginfoza7), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_initz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1221z00_3886))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_6534;

					{
						obj_t BgL_auxz00_6535;

						{	/* Globalize/ginfo.scm 90 */
							BgL_objectz00_bglt BgL_tmpz00_6536;

							BgL_tmpz00_6536 =
								((BgL_objectz00_bglt)
								((BgL_globalz00_bglt) BgL_new1221z00_3886));
							BgL_auxz00_6535 = BGL_OBJECT_WIDENING(BgL_tmpz00_6536);
						}
						BgL_auxz00_6534 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6535);
					}
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6534))->
							BgL_escapezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_6542;

					{
						obj_t BgL_auxz00_6543;

						{	/* Globalize/ginfo.scm 90 */
							BgL_objectz00_bglt BgL_tmpz00_6544;

							BgL_tmpz00_6544 =
								((BgL_objectz00_bglt)
								((BgL_globalz00_bglt) BgL_new1221z00_3886));
							BgL_auxz00_6543 = BGL_OBJECT_WIDENING(BgL_tmpz00_6544);
						}
						BgL_auxz00_6542 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6543);
					}
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6542))->
							BgL_globalzd2closurezd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_6457 = ((BgL_globalz00_bglt) BgL_new1221z00_3886);
				return ((obj_t) BgL_auxz00_6457);
			}
		}

	}



/* &lambda1993 */
	BgL_globalz00_bglt BGl_z62lambda1993z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3887, obj_t BgL_o1218z00_3888)
	{
		{	/* Globalize/ginfo.scm 90 */
			{	/* Globalize/ginfo.scm 90 */
				BgL_globalzf2ginfozf2_bglt BgL_wide1220z00_4336;

				BgL_wide1220z00_4336 =
					((BgL_globalzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_globalzf2ginfozf2_bgl))));
				{	/* Globalize/ginfo.scm 90 */
					obj_t BgL_auxz00_6557;
					BgL_objectz00_bglt BgL_tmpz00_6553;

					BgL_auxz00_6557 = ((obj_t) BgL_wide1220z00_4336);
					BgL_tmpz00_6553 =
						((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1218z00_3888)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6553, BgL_auxz00_6557);
				}
				((BgL_objectz00_bglt)
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1218z00_3888)));
				{	/* Globalize/ginfo.scm 90 */
					long BgL_arg1994z00_4337;

					BgL_arg1994z00_4337 =
						BGL_CLASS_NUM(BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_globalz00_bglt)
								((BgL_globalz00_bglt) BgL_o1218z00_3888))),
						BgL_arg1994z00_4337);
				}
				return
					((BgL_globalz00_bglt)
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1218z00_3888)));
			}
		}

	}



/* &lambda1990 */
	BgL_globalz00_bglt BGl_z62lambda1990z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3889, obj_t BgL_id1196z00_3890, obj_t BgL_name1197z00_3891,
		obj_t BgL_type1198z00_3892, obj_t BgL_value1199z00_3893,
		obj_t BgL_access1200z00_3894, obj_t BgL_fastzd2alpha1201zd2_3895,
		obj_t BgL_removable1202z00_3896, obj_t BgL_occurrence1203z00_3897,
		obj_t BgL_occurrencew1204z00_3898, obj_t BgL_userzf31205zf3_3899,
		obj_t BgL_module1206z00_3900, obj_t BgL_import1207z00_3901,
		obj_t BgL_evaluablezf31208zf3_3902, obj_t BgL_evalzf31209zf3_3903,
		obj_t BgL_library1210z00_3904, obj_t BgL_pragma1211z00_3905,
		obj_t BgL_src1212z00_3906, obj_t BgL_jvmzd2typezd2name1213z00_3907,
		obj_t BgL_init1214z00_3908, obj_t BgL_alias1215z00_3909,
		obj_t BgL_escapezf31216zf3_3910, obj_t BgL_globalzd2closure1217zd2_3911)
	{
		{	/* Globalize/ginfo.scm 90 */
			{	/* Globalize/ginfo.scm 90 */
				long BgL_occurrence1203z00_4341;
				long BgL_occurrencew1204z00_4342;
				bool_t BgL_userzf31205zf3_4343;
				bool_t BgL_evaluablezf31208zf3_4345;
				bool_t BgL_evalzf31209zf3_4346;
				bool_t BgL_escapezf31216zf3_4348;

				BgL_occurrence1203z00_4341 = (long) CINT(BgL_occurrence1203z00_3897);
				BgL_occurrencew1204z00_4342 = (long) CINT(BgL_occurrencew1204z00_3898);
				BgL_userzf31205zf3_4343 = CBOOL(BgL_userzf31205zf3_3899);
				BgL_evaluablezf31208zf3_4345 = CBOOL(BgL_evaluablezf31208zf3_3902);
				BgL_evalzf31209zf3_4346 = CBOOL(BgL_evalzf31209zf3_3903);
				BgL_escapezf31216zf3_4348 = CBOOL(BgL_escapezf31216zf3_3910);
				{	/* Globalize/ginfo.scm 90 */
					BgL_globalz00_bglt BgL_new1268z00_4349;

					{	/* Globalize/ginfo.scm 90 */
						BgL_globalz00_bglt BgL_tmp1266z00_4350;
						BgL_globalzf2ginfozf2_bglt BgL_wide1267z00_4351;

						{
							BgL_globalz00_bglt BgL_auxz00_6577;

							{	/* Globalize/ginfo.scm 90 */
								BgL_globalz00_bglt BgL_new1265z00_4352;

								BgL_new1265z00_4352 =
									((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_globalz00_bgl))));
								{	/* Globalize/ginfo.scm 90 */
									long BgL_arg1992z00_4353;

									BgL_arg1992z00_4353 =
										BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1265z00_4352),
										BgL_arg1992z00_4353);
								}
								{	/* Globalize/ginfo.scm 90 */
									BgL_objectz00_bglt BgL_tmpz00_6582;

									BgL_tmpz00_6582 = ((BgL_objectz00_bglt) BgL_new1265z00_4352);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6582, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1265z00_4352);
								BgL_auxz00_6577 = BgL_new1265z00_4352;
							}
							BgL_tmp1266z00_4350 = ((BgL_globalz00_bglt) BgL_auxz00_6577);
						}
						BgL_wide1267z00_4351 =
							((BgL_globalzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_globalzf2ginfozf2_bgl))));
						{	/* Globalize/ginfo.scm 90 */
							obj_t BgL_auxz00_6590;
							BgL_objectz00_bglt BgL_tmpz00_6588;

							BgL_auxz00_6590 = ((obj_t) BgL_wide1267z00_4351);
							BgL_tmpz00_6588 = ((BgL_objectz00_bglt) BgL_tmp1266z00_4350);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6588, BgL_auxz00_6590);
						}
						((BgL_objectz00_bglt) BgL_tmp1266z00_4350);
						{	/* Globalize/ginfo.scm 90 */
							long BgL_arg1991z00_4354;

							BgL_arg1991z00_4354 =
								BGL_CLASS_NUM(BGl_globalzf2Ginfozf2zzglobaliza7e_ginfoza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1266z00_4350),
								BgL_arg1991z00_4354);
						}
						BgL_new1268z00_4349 = ((BgL_globalz00_bglt) BgL_tmp1266z00_4350);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1268z00_4349)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1196z00_3890)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_namez00) =
						((obj_t) BgL_name1197z00_3891), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1198z00_3892)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1199z00_3893)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_accessz00) =
						((obj_t) BgL_access1200z00_3894), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1201zd2_3895), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_removablez00) =
						((obj_t) BgL_removable1202z00_3896), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_occurrencez00) =
						((long) BgL_occurrence1203z00_4341), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1204z00_4342), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1268z00_4349)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31205zf3_4343), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_modulez00) =
						((obj_t) ((obj_t) BgL_module1206z00_3900)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_importz00) =
						((obj_t) BgL_import1207z00_3901), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_evaluablezf3zf3) =
						((bool_t) BgL_evaluablezf31208zf3_4345), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_evalzf3zf3) =
						((bool_t) BgL_evalzf31209zf3_4346), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_libraryz00) =
						((obj_t) BgL_library1210z00_3904), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_pragmaz00) =
						((obj_t) BgL_pragma1211z00_3905), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_srcz00) =
						((obj_t) BgL_src1212z00_3906), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_jvmzd2typezd2namez00) =
						((obj_t) ((obj_t) BgL_jvmzd2typezd2name1213z00_3907)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_initz00) =
						((obj_t) BgL_init1214z00_3908), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1268z00_4349)))->BgL_aliasz00) =
						((obj_t) BgL_alias1215z00_3909), BUNSPEC);
					{
						BgL_globalzf2ginfozf2_bglt BgL_auxz00_6643;

						{
							obj_t BgL_auxz00_6644;

							{	/* Globalize/ginfo.scm 90 */
								BgL_objectz00_bglt BgL_tmpz00_6645;

								BgL_tmpz00_6645 = ((BgL_objectz00_bglt) BgL_new1268z00_4349);
								BgL_auxz00_6644 = BGL_OBJECT_WIDENING(BgL_tmpz00_6645);
							}
							BgL_auxz00_6643 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6644);
						}
						((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6643))->
								BgL_escapezf3zf3) =
							((bool_t) BgL_escapezf31216zf3_4348), BUNSPEC);
					}
					{
						BgL_globalzf2ginfozf2_bglt BgL_auxz00_6650;

						{
							obj_t BgL_auxz00_6651;

							{	/* Globalize/ginfo.scm 90 */
								BgL_objectz00_bglt BgL_tmpz00_6652;

								BgL_tmpz00_6652 = ((BgL_objectz00_bglt) BgL_new1268z00_4349);
								BgL_auxz00_6651 = BGL_OBJECT_WIDENING(BgL_tmpz00_6652);
							}
							BgL_auxz00_6650 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6651);
						}
						((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6650))->
								BgL_globalzd2closurezd2) =
							((obj_t) BgL_globalzd2closure1217zd2_3911), BUNSPEC);
					}
					return BgL_new1268z00_4349;
				}
			}
		}

	}



/* &<@anonymous:2013> */
	obj_t BGl_z62zc3z04anonymousza32013ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3912)
	{
		{	/* Globalize/ginfo.scm 90 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2012 */
	obj_t BGl_z62lambda2012z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3913,
		obj_t BgL_oz00_3914, obj_t BgL_vz00_3915)
	{
		{	/* Globalize/ginfo.scm 90 */
			{
				BgL_globalzf2ginfozf2_bglt BgL_auxz00_6658;

				{
					obj_t BgL_auxz00_6659;

					{	/* Globalize/ginfo.scm 90 */
						BgL_objectz00_bglt BgL_tmpz00_6660;

						BgL_tmpz00_6660 =
							((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_3914));
						BgL_auxz00_6659 = BGL_OBJECT_WIDENING(BgL_tmpz00_6660);
					}
					BgL_auxz00_6658 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6659);
				}
				return
					((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6658))->
						BgL_globalzd2closurezd2) = ((obj_t) BgL_vz00_3915), BUNSPEC);
			}
		}

	}



/* &lambda2011 */
	obj_t BGl_z62lambda2011z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3916,
		obj_t BgL_oz00_3917)
	{
		{	/* Globalize/ginfo.scm 90 */
			{
				BgL_globalzf2ginfozf2_bglt BgL_auxz00_6666;

				{
					obj_t BgL_auxz00_6667;

					{	/* Globalize/ginfo.scm 90 */
						BgL_objectz00_bglt BgL_tmpz00_6668;

						BgL_tmpz00_6668 =
							((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_3917));
						BgL_auxz00_6667 = BGL_OBJECT_WIDENING(BgL_tmpz00_6668);
					}
					BgL_auxz00_6666 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6667);
				}
				return
					(((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6666))->
					BgL_globalzd2closurezd2);
			}
		}

	}



/* &<@anonymous:2006> */
	obj_t BGl_z62zc3z04anonymousza32006ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3918)
	{
		{	/* Globalize/ginfo.scm 90 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2005 */
	obj_t BGl_z62lambda2005z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3919,
		obj_t BgL_oz00_3920, obj_t BgL_vz00_3921)
	{
		{	/* Globalize/ginfo.scm 90 */
			{	/* Globalize/ginfo.scm 90 */
				bool_t BgL_vz00_4358;

				BgL_vz00_4358 = CBOOL(BgL_vz00_3921);
				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_6676;

					{
						obj_t BgL_auxz00_6677;

						{	/* Globalize/ginfo.scm 90 */
							BgL_objectz00_bglt BgL_tmpz00_6678;

							BgL_tmpz00_6678 =
								((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_3920));
							BgL_auxz00_6677 = BGL_OBJECT_WIDENING(BgL_tmpz00_6678);
						}
						BgL_auxz00_6676 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6677);
					}
					return
						((((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6676))->
							BgL_escapezf3zf3) = ((bool_t) BgL_vz00_4358), BUNSPEC);
				}
			}
		}

	}



/* &lambda2004 */
	obj_t BGl_z62lambda2004z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3922,
		obj_t BgL_oz00_3923)
	{
		{	/* Globalize/ginfo.scm 90 */
			{	/* Globalize/ginfo.scm 90 */
				bool_t BgL_tmpz00_6684;

				{
					BgL_globalzf2ginfozf2_bglt BgL_auxz00_6685;

					{
						obj_t BgL_auxz00_6686;

						{	/* Globalize/ginfo.scm 90 */
							BgL_objectz00_bglt BgL_tmpz00_6687;

							BgL_tmpz00_6687 =
								((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_3923));
							BgL_auxz00_6686 = BGL_OBJECT_WIDENING(BgL_tmpz00_6687);
						}
						BgL_auxz00_6685 = ((BgL_globalzf2ginfozf2_bglt) BgL_auxz00_6686);
					}
					BgL_tmpz00_6684 =
						(((BgL_globalzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6685))->
						BgL_escapezf3zf3);
				}
				return BBOOL(BgL_tmpz00_6684);
			}
		}

	}



/* &lambda1966 */
	BgL_localz00_bglt BGl_z62lambda1966z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3924, obj_t BgL_o1194z00_3925)
	{
		{	/* Globalize/ginfo.scm 84 */
			{	/* Globalize/ginfo.scm 84 */
				long BgL_arg1967z00_4361;

				{	/* Globalize/ginfo.scm 84 */
					obj_t BgL_arg1968z00_4362;

					{	/* Globalize/ginfo.scm 84 */
						obj_t BgL_arg1969z00_4363;

						{	/* Globalize/ginfo.scm 84 */
							obj_t BgL_arg1815z00_4364;
							long BgL_arg1816z00_4365;

							BgL_arg1815z00_4364 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Globalize/ginfo.scm 84 */
								long BgL_arg1817z00_4366;

								BgL_arg1817z00_4366 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1194z00_3925)));
								BgL_arg1816z00_4365 = (BgL_arg1817z00_4366 - OBJECT_TYPE);
							}
							BgL_arg1969z00_4363 =
								VECTOR_REF(BgL_arg1815z00_4364, BgL_arg1816z00_4365);
						}
						BgL_arg1968z00_4362 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1969z00_4363);
					}
					{	/* Globalize/ginfo.scm 84 */
						obj_t BgL_tmpz00_6701;

						BgL_tmpz00_6701 = ((obj_t) BgL_arg1968z00_4362);
						BgL_arg1967z00_4361 = BGL_CLASS_NUM(BgL_tmpz00_6701);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1194z00_3925)), BgL_arg1967z00_4361);
			}
			{	/* Globalize/ginfo.scm 84 */
				BgL_objectz00_bglt BgL_tmpz00_6707;

				BgL_tmpz00_6707 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1194z00_3925));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6707, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1194z00_3925));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1194z00_3925));
		}

	}



/* &<@anonymous:1965> */
	obj_t BGl_z62zc3z04anonymousza31965ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3926, obj_t BgL_new1193z00_3927)
	{
		{	/* Globalize/ginfo.scm 84 */
			{
				BgL_localz00_bglt BgL_auxz00_6715;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1193z00_3927))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(35)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_6723;

					{	/* Globalize/ginfo.scm 84 */
						obj_t BgL_classz00_4368;

						BgL_classz00_4368 = BGl_typez00zztype_typez00;
						{	/* Globalize/ginfo.scm 84 */
							obj_t BgL__ortest_1117z00_4369;

							BgL__ortest_1117z00_4369 = BGL_CLASS_NIL(BgL_classz00_4368);
							if (CBOOL(BgL__ortest_1117z00_4369))
								{	/* Globalize/ginfo.scm 84 */
									BgL_auxz00_6723 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4369);
								}
							else
								{	/* Globalize/ginfo.scm 84 */
									BgL_auxz00_6723 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4368));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1193z00_3927))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_6723), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_6733;

					{	/* Globalize/ginfo.scm 84 */
						obj_t BgL_classz00_4370;

						BgL_classz00_4370 = BGl_valuez00zzast_varz00;
						{	/* Globalize/ginfo.scm 84 */
							obj_t BgL__ortest_1117z00_4371;

							BgL__ortest_1117z00_4371 = BGL_CLASS_NIL(BgL_classz00_4370);
							if (CBOOL(BgL__ortest_1117z00_4371))
								{	/* Globalize/ginfo.scm 84 */
									BgL_auxz00_6733 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_4371);
								}
							else
								{	/* Globalize/ginfo.scm 84 */
									BgL_auxz00_6733 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4370));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1193z00_3927))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_6733), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1193z00_3927))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1193z00_3927))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_localzf2ginfozf2_bglt BgL_auxz00_6770;

					{
						obj_t BgL_auxz00_6771;

						{	/* Globalize/ginfo.scm 84 */
							BgL_objectz00_bglt BgL_tmpz00_6772;

							BgL_tmpz00_6772 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1193z00_3927));
							BgL_auxz00_6771 = BGL_OBJECT_WIDENING(BgL_tmpz00_6772);
						}
						BgL_auxz00_6770 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_6771);
					}
					((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6770))->
							BgL_escapezf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_localzf2ginfozf2_bglt BgL_auxz00_6778;

					{
						obj_t BgL_auxz00_6779;

						{	/* Globalize/ginfo.scm 84 */
							BgL_objectz00_bglt BgL_tmpz00_6780;

							BgL_tmpz00_6780 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1193z00_3927));
							BgL_auxz00_6779 = BGL_OBJECT_WIDENING(BgL_tmpz00_6780);
						}
						BgL_auxz00_6778 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_6779);
					}
					((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6778))->
							BgL_globaliza7edzf3z54) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_6715 = ((BgL_localz00_bglt) BgL_new1193z00_3927);
				return ((obj_t) BgL_auxz00_6715);
			}
		}

	}



/* &lambda1963 */
	BgL_localz00_bglt BGl_z62lambda1963z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3928, obj_t BgL_o1190z00_3929)
	{
		{	/* Globalize/ginfo.scm 84 */
			{	/* Globalize/ginfo.scm 84 */
				BgL_localzf2ginfozf2_bglt BgL_wide1192z00_4373;

				BgL_wide1192z00_4373 =
					((BgL_localzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_localzf2ginfozf2_bgl))));
				{	/* Globalize/ginfo.scm 84 */
					obj_t BgL_auxz00_6793;
					BgL_objectz00_bglt BgL_tmpz00_6789;

					BgL_auxz00_6793 = ((obj_t) BgL_wide1192z00_4373);
					BgL_tmpz00_6789 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1190z00_3929)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6789, BgL_auxz00_6793);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1190z00_3929)));
				{	/* Globalize/ginfo.scm 84 */
					long BgL_arg1964z00_4374;

					BgL_arg1964z00_4374 =
						BGL_CLASS_NUM(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1190z00_3929))), BgL_arg1964z00_4374);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1190z00_3929)));
			}
		}

	}



/* &lambda1960 */
	BgL_localz00_bglt BGl_z62lambda1960z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3930, obj_t BgL_id1175z00_3931, obj_t BgL_name1176z00_3932,
		obj_t BgL_type1177z00_3933, obj_t BgL_value1178z00_3934,
		obj_t BgL_access1179z00_3935, obj_t BgL_fastzd2alpha1180zd2_3936,
		obj_t BgL_removable1181z00_3937, obj_t BgL_occurrence1182z00_3938,
		obj_t BgL_occurrencew1183z00_3939, obj_t BgL_userzf31184zf3_3940,
		obj_t BgL_key1185z00_3941, obj_t BgL_valzd2noescape1186zd2_3942,
		obj_t BgL_volatile1187z00_3943, obj_t BgL_escapezf31188zf3_3944,
		obj_t BgL_globaliza7edzf31189z54_3945)
	{
		{	/* Globalize/ginfo.scm 84 */
			{	/* Globalize/ginfo.scm 84 */
				long BgL_occurrence1182z00_4378;
				long BgL_occurrencew1183z00_4379;
				bool_t BgL_userzf31184zf3_4380;
				long BgL_key1185z00_4381;
				bool_t BgL_volatile1187z00_4382;
				bool_t BgL_escapezf31188zf3_4383;
				bool_t BgL_globaliza7edzf31189z54_4384;

				BgL_occurrence1182z00_4378 = (long) CINT(BgL_occurrence1182z00_3938);
				BgL_occurrencew1183z00_4379 = (long) CINT(BgL_occurrencew1183z00_3939);
				BgL_userzf31184zf3_4380 = CBOOL(BgL_userzf31184zf3_3940);
				BgL_key1185z00_4381 = (long) CINT(BgL_key1185z00_3941);
				BgL_volatile1187z00_4382 = CBOOL(BgL_volatile1187z00_3943);
				BgL_escapezf31188zf3_4383 = CBOOL(BgL_escapezf31188zf3_3944);
				BgL_globaliza7edzf31189z54_4384 =
					CBOOL(BgL_globaliza7edzf31189z54_3945);
				{	/* Globalize/ginfo.scm 84 */
					BgL_localz00_bglt BgL_new1263z00_4385;

					{	/* Globalize/ginfo.scm 84 */
						BgL_localz00_bglt BgL_tmp1261z00_4386;
						BgL_localzf2ginfozf2_bglt BgL_wide1262z00_4387;

						{
							BgL_localz00_bglt BgL_auxz00_6814;

							{	/* Globalize/ginfo.scm 84 */
								BgL_localz00_bglt BgL_new1260z00_4388;

								BgL_new1260z00_4388 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* Globalize/ginfo.scm 84 */
									long BgL_arg1962z00_4389;

									BgL_arg1962z00_4389 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1260z00_4388),
										BgL_arg1962z00_4389);
								}
								{	/* Globalize/ginfo.scm 84 */
									BgL_objectz00_bglt BgL_tmpz00_6819;

									BgL_tmpz00_6819 = ((BgL_objectz00_bglt) BgL_new1260z00_4388);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6819, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1260z00_4388);
								BgL_auxz00_6814 = BgL_new1260z00_4388;
							}
							BgL_tmp1261z00_4386 = ((BgL_localz00_bglt) BgL_auxz00_6814);
						}
						BgL_wide1262z00_4387 =
							((BgL_localzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_localzf2ginfozf2_bgl))));
						{	/* Globalize/ginfo.scm 84 */
							obj_t BgL_auxz00_6827;
							BgL_objectz00_bglt BgL_tmpz00_6825;

							BgL_auxz00_6827 = ((obj_t) BgL_wide1262z00_4387);
							BgL_tmpz00_6825 = ((BgL_objectz00_bglt) BgL_tmp1261z00_4386);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6825, BgL_auxz00_6827);
						}
						((BgL_objectz00_bglt) BgL_tmp1261z00_4386);
						{	/* Globalize/ginfo.scm 84 */
							long BgL_arg1961z00_4390;

							BgL_arg1961z00_4390 =
								BGL_CLASS_NUM(BGl_localzf2Ginfozf2zzglobaliza7e_ginfoza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1261z00_4386),
								BgL_arg1961z00_4390);
						}
						BgL_new1263z00_4385 = ((BgL_localz00_bglt) BgL_tmp1261z00_4386);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1263z00_4385)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1175z00_3931)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_namez00) =
						((obj_t) BgL_name1176z00_3932), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1177z00_3933)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1178z00_3934)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_accessz00) =
						((obj_t) BgL_access1179z00_3935), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1180zd2_3936), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_removablez00) =
						((obj_t) BgL_removable1181z00_3937), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_occurrencez00) =
						((long) BgL_occurrence1182z00_4378), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1183z00_4379), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1263z00_4385)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31184zf3_4380), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1263z00_4385)))->BgL_keyz00) =
						((long) BgL_key1185z00_4381), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1263z00_4385)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1186zd2_3942), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1263z00_4385)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1187z00_4382), BUNSPEC);
					{
						BgL_localzf2ginfozf2_bglt BgL_auxz00_6864;

						{
							obj_t BgL_auxz00_6865;

							{	/* Globalize/ginfo.scm 84 */
								BgL_objectz00_bglt BgL_tmpz00_6866;

								BgL_tmpz00_6866 = ((BgL_objectz00_bglt) BgL_new1263z00_4385);
								BgL_auxz00_6865 = BGL_OBJECT_WIDENING(BgL_tmpz00_6866);
							}
							BgL_auxz00_6864 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_6865);
						}
						((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6864))->
								BgL_escapezf3zf3) =
							((bool_t) BgL_escapezf31188zf3_4383), BUNSPEC);
					}
					{
						BgL_localzf2ginfozf2_bglt BgL_auxz00_6871;

						{
							obj_t BgL_auxz00_6872;

							{	/* Globalize/ginfo.scm 84 */
								BgL_objectz00_bglt BgL_tmpz00_6873;

								BgL_tmpz00_6873 = ((BgL_objectz00_bglt) BgL_new1263z00_4385);
								BgL_auxz00_6872 = BGL_OBJECT_WIDENING(BgL_tmpz00_6873);
							}
							BgL_auxz00_6871 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_6872);
						}
						((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6871))->
								BgL_globaliza7edzf3z54) =
							((bool_t) BgL_globaliza7edzf31189z54_4384), BUNSPEC);
					}
					return BgL_new1263z00_4385;
				}
			}
		}

	}



/* &<@anonymous:1983> */
	obj_t BGl_z62zc3z04anonymousza31983ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3946)
	{
		{	/* Globalize/ginfo.scm 84 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1982 */
	obj_t BGl_z62lambda1982z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3947,
		obj_t BgL_oz00_3948, obj_t BgL_vz00_3949)
	{
		{	/* Globalize/ginfo.scm 84 */
			{	/* Globalize/ginfo.scm 84 */
				bool_t BgL_vz00_4392;

				BgL_vz00_4392 = CBOOL(BgL_vz00_3949);
				{
					BgL_localzf2ginfozf2_bglt BgL_auxz00_6880;

					{
						obj_t BgL_auxz00_6881;

						{	/* Globalize/ginfo.scm 84 */
							BgL_objectz00_bglt BgL_tmpz00_6882;

							BgL_tmpz00_6882 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_3948));
							BgL_auxz00_6881 = BGL_OBJECT_WIDENING(BgL_tmpz00_6882);
						}
						BgL_auxz00_6880 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_6881);
					}
					return
						((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6880))->
							BgL_globaliza7edzf3z54) = ((bool_t) BgL_vz00_4392), BUNSPEC);
				}
			}
		}

	}



/* &lambda1981 */
	obj_t BGl_z62lambda1981z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3950,
		obj_t BgL_oz00_3951)
	{
		{	/* Globalize/ginfo.scm 84 */
			{	/* Globalize/ginfo.scm 84 */
				bool_t BgL_tmpz00_6888;

				{
					BgL_localzf2ginfozf2_bglt BgL_auxz00_6889;

					{
						obj_t BgL_auxz00_6890;

						{	/* Globalize/ginfo.scm 84 */
							BgL_objectz00_bglt BgL_tmpz00_6891;

							BgL_tmpz00_6891 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_3951));
							BgL_auxz00_6890 = BGL_OBJECT_WIDENING(BgL_tmpz00_6891);
						}
						BgL_auxz00_6889 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_6890);
					}
					BgL_tmpz00_6888 =
						(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6889))->
						BgL_globaliza7edzf3z54);
				}
				return BBOOL(BgL_tmpz00_6888);
			}
		}

	}



/* &<@anonymous:1976> */
	obj_t BGl_z62zc3z04anonymousza31976ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3952)
	{
		{	/* Globalize/ginfo.scm 84 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1975 */
	obj_t BGl_z62lambda1975z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3953,
		obj_t BgL_oz00_3954, obj_t BgL_vz00_3955)
	{
		{	/* Globalize/ginfo.scm 84 */
			{	/* Globalize/ginfo.scm 84 */
				bool_t BgL_vz00_4395;

				BgL_vz00_4395 = CBOOL(BgL_vz00_3955);
				{
					BgL_localzf2ginfozf2_bglt BgL_auxz00_6900;

					{
						obj_t BgL_auxz00_6901;

						{	/* Globalize/ginfo.scm 84 */
							BgL_objectz00_bglt BgL_tmpz00_6902;

							BgL_tmpz00_6902 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_3954));
							BgL_auxz00_6901 = BGL_OBJECT_WIDENING(BgL_tmpz00_6902);
						}
						BgL_auxz00_6900 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_6901);
					}
					return
						((((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6900))->
							BgL_escapezf3zf3) = ((bool_t) BgL_vz00_4395), BUNSPEC);
				}
			}
		}

	}



/* &lambda1974 */
	obj_t BGl_z62lambda1974z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3956,
		obj_t BgL_oz00_3957)
	{
		{	/* Globalize/ginfo.scm 84 */
			{	/* Globalize/ginfo.scm 84 */
				bool_t BgL_tmpz00_6908;

				{
					BgL_localzf2ginfozf2_bglt BgL_auxz00_6909;

					{
						obj_t BgL_auxz00_6910;

						{	/* Globalize/ginfo.scm 84 */
							BgL_objectz00_bglt BgL_tmpz00_6911;

							BgL_tmpz00_6911 =
								((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_3957));
							BgL_auxz00_6910 = BGL_OBJECT_WIDENING(BgL_tmpz00_6911);
						}
						BgL_auxz00_6909 = ((BgL_localzf2ginfozf2_bglt) BgL_auxz00_6910);
					}
					BgL_tmpz00_6908 =
						(((BgL_localzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6909))->
						BgL_escapezf3zf3);
				}
				return BBOOL(BgL_tmpz00_6908);
			}
		}

	}



/* &lambda1920 */
	BgL_sexitz00_bglt BGl_z62lambda1920z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3958, obj_t BgL_o1173z00_3959)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				long BgL_arg1923z00_4398;

				{	/* Globalize/ginfo.scm 74 */
					obj_t BgL_arg1924z00_4399;

					{	/* Globalize/ginfo.scm 74 */
						obj_t BgL_arg1925z00_4400;

						{	/* Globalize/ginfo.scm 74 */
							obj_t BgL_arg1815z00_4401;
							long BgL_arg1816z00_4402;

							BgL_arg1815z00_4401 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Globalize/ginfo.scm 74 */
								long BgL_arg1817z00_4403;

								BgL_arg1817z00_4403 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sexitz00_bglt) BgL_o1173z00_3959)));
								BgL_arg1816z00_4402 = (BgL_arg1817z00_4403 - OBJECT_TYPE);
							}
							BgL_arg1925z00_4400 =
								VECTOR_REF(BgL_arg1815z00_4401, BgL_arg1816z00_4402);
						}
						BgL_arg1924z00_4399 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1925z00_4400);
					}
					{	/* Globalize/ginfo.scm 74 */
						obj_t BgL_tmpz00_6925;

						BgL_tmpz00_6925 = ((obj_t) BgL_arg1924z00_4399);
						BgL_arg1923z00_4398 = BGL_CLASS_NUM(BgL_tmpz00_6925);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sexitz00_bglt) BgL_o1173z00_3959)), BgL_arg1923z00_4398);
			}
			{	/* Globalize/ginfo.scm 74 */
				BgL_objectz00_bglt BgL_tmpz00_6931;

				BgL_tmpz00_6931 =
					((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_o1173z00_3959));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6931, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_o1173z00_3959));
			return ((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_o1173z00_3959));
		}

	}



/* &<@anonymous:1919> */
	obj_t BGl_z62zc3z04anonymousza31919ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3960, obj_t BgL_new1172z00_3961)
	{
		{	/* Globalize/ginfo.scm 74 */
			{
				BgL_sexitz00_bglt BgL_auxz00_6939;

				((((BgL_sexitz00_bglt) COBJECT(
								((BgL_sexitz00_bglt)
									((BgL_sexitz00_bglt) BgL_new1172z00_3961))))->
						BgL_handlerz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt)
										BgL_new1172z00_3961))))->BgL_detachedzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_6946;

					{
						obj_t BgL_auxz00_6947;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_6948;

							BgL_tmpz00_6948 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_new1172z00_3961));
							BgL_auxz00_6947 = BGL_OBJECT_WIDENING(BgL_tmpz00_6948);
						}
						BgL_auxz00_6946 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_6947);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6946))->
							BgL_gzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_6954;

					{
						obj_t BgL_auxz00_6955;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_6956;

							BgL_tmpz00_6956 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_new1172z00_3961));
							BgL_auxz00_6955 = BGL_OBJECT_WIDENING(BgL_tmpz00_6956);
						}
						BgL_auxz00_6954 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_6955);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6954))->
							BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_6962;

					{
						obj_t BgL_auxz00_6963;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_6964;

							BgL_tmpz00_6964 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_new1172z00_3961));
							BgL_auxz00_6963 = BGL_OBJECT_WIDENING(BgL_tmpz00_6964);
						}
						BgL_auxz00_6962 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_6963);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6962))->
							BgL_freezd2markzd2) = ((long) 0L), BUNSPEC);
				}
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_6970;

					{
						obj_t BgL_auxz00_6971;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_6972;

							BgL_tmpz00_6972 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_new1172z00_3961));
							BgL_auxz00_6971 = BGL_OBJECT_WIDENING(BgL_tmpz00_6972);
						}
						BgL_auxz00_6970 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_6971);
					}
					((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_6970))->
							BgL_markz00) = ((long) 0L), BUNSPEC);
				}
				BgL_auxz00_6939 = ((BgL_sexitz00_bglt) BgL_new1172z00_3961);
				return ((obj_t) BgL_auxz00_6939);
			}
		}

	}



/* &lambda1917 */
	BgL_sexitz00_bglt BGl_z62lambda1917z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3962, obj_t BgL_o1169z00_3963)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				BgL_sexitzf2ginfozf2_bglt BgL_wide1171z00_4406;

				BgL_wide1171z00_4406 =
					((BgL_sexitzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sexitzf2ginfozf2_bgl))));
				{	/* Globalize/ginfo.scm 74 */
					obj_t BgL_auxz00_6985;
					BgL_objectz00_bglt BgL_tmpz00_6981;

					BgL_auxz00_6985 = ((obj_t) BgL_wide1171z00_4406);
					BgL_tmpz00_6981 =
						((BgL_objectz00_bglt)
						((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_o1169z00_3963)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6981, BgL_auxz00_6985);
				}
				((BgL_objectz00_bglt)
					((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_o1169z00_3963)));
				{	/* Globalize/ginfo.scm 74 */
					long BgL_arg1918z00_4407;

					BgL_arg1918z00_4407 =
						BGL_CLASS_NUM(BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sexitz00_bglt)
								((BgL_sexitz00_bglt) BgL_o1169z00_3963))), BgL_arg1918z00_4407);
				}
				return
					((BgL_sexitz00_bglt)
					((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_o1169z00_3963)));
			}
		}

	}



/* &lambda1913 */
	BgL_sexitz00_bglt BGl_z62lambda1913z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3964, obj_t BgL_handler1163z00_3965,
		obj_t BgL_detachedzf31164zf3_3966, obj_t BgL_gzf31165zf3_3967,
		obj_t BgL_kapturedzf31166zf3_3968, obj_t BgL_freezd2mark1167zd2_3969,
		obj_t BgL_mark1168z00_3970)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				bool_t BgL_detachedzf31164zf3_4408;
				bool_t BgL_gzf31165zf3_4409;
				bool_t BgL_kapturedzf31166zf3_4410;
				long BgL_freezd2mark1167zd2_4411;
				long BgL_mark1168z00_4412;

				BgL_detachedzf31164zf3_4408 = CBOOL(BgL_detachedzf31164zf3_3966);
				BgL_gzf31165zf3_4409 = CBOOL(BgL_gzf31165zf3_3967);
				BgL_kapturedzf31166zf3_4410 = CBOOL(BgL_kapturedzf31166zf3_3968);
				BgL_freezd2mark1167zd2_4411 = (long) CINT(BgL_freezd2mark1167zd2_3969);
				BgL_mark1168z00_4412 = (long) CINT(BgL_mark1168z00_3970);
				{	/* Globalize/ginfo.scm 74 */
					BgL_sexitz00_bglt BgL_new1257z00_4413;

					{	/* Globalize/ginfo.scm 74 */
						BgL_sexitz00_bglt BgL_tmp1255z00_4414;
						BgL_sexitzf2ginfozf2_bglt BgL_wide1256z00_4415;

						{
							BgL_sexitz00_bglt BgL_auxz00_7004;

							{	/* Globalize/ginfo.scm 74 */
								BgL_sexitz00_bglt BgL_new1254z00_4416;

								BgL_new1254z00_4416 =
									((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sexitz00_bgl))));
								{	/* Globalize/ginfo.scm 74 */
									long BgL_arg1916z00_4417;

									BgL_arg1916z00_4417 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1254z00_4416),
										BgL_arg1916z00_4417);
								}
								{	/* Globalize/ginfo.scm 74 */
									BgL_objectz00_bglt BgL_tmpz00_7009;

									BgL_tmpz00_7009 = ((BgL_objectz00_bglt) BgL_new1254z00_4416);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7009, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1254z00_4416);
								BgL_auxz00_7004 = BgL_new1254z00_4416;
							}
							BgL_tmp1255z00_4414 = ((BgL_sexitz00_bglt) BgL_auxz00_7004);
						}
						BgL_wide1256z00_4415 =
							((BgL_sexitzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sexitzf2ginfozf2_bgl))));
						{	/* Globalize/ginfo.scm 74 */
							obj_t BgL_auxz00_7017;
							BgL_objectz00_bglt BgL_tmpz00_7015;

							BgL_auxz00_7017 = ((obj_t) BgL_wide1256z00_4415);
							BgL_tmpz00_7015 = ((BgL_objectz00_bglt) BgL_tmp1255z00_4414);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7015, BgL_auxz00_7017);
						}
						((BgL_objectz00_bglt) BgL_tmp1255z00_4414);
						{	/* Globalize/ginfo.scm 74 */
							long BgL_arg1914z00_4418;

							BgL_arg1914z00_4418 =
								BGL_CLASS_NUM(BGl_sexitzf2Ginfozf2zzglobaliza7e_ginfoza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1255z00_4414),
								BgL_arg1914z00_4418);
						}
						BgL_new1257z00_4413 = ((BgL_sexitz00_bglt) BgL_tmp1255z00_4414);
					}
					((((BgL_sexitz00_bglt) COBJECT(
									((BgL_sexitz00_bglt) BgL_new1257z00_4413)))->BgL_handlerz00) =
						((obj_t) BgL_handler1163z00_3965), BUNSPEC);
					((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt)
										BgL_new1257z00_4413)))->BgL_detachedzf3zf3) =
						((bool_t) BgL_detachedzf31164zf3_4408), BUNSPEC);
					{
						BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7029;

						{
							obj_t BgL_auxz00_7030;

							{	/* Globalize/ginfo.scm 74 */
								BgL_objectz00_bglt BgL_tmpz00_7031;

								BgL_tmpz00_7031 = ((BgL_objectz00_bglt) BgL_new1257z00_4413);
								BgL_auxz00_7030 = BGL_OBJECT_WIDENING(BgL_tmpz00_7031);
							}
							BgL_auxz00_7029 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7030);
						}
						((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7029))->
								BgL_gzf3zf3) = ((bool_t) BgL_gzf31165zf3_4409), BUNSPEC);
					}
					{
						BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7036;

						{
							obj_t BgL_auxz00_7037;

							{	/* Globalize/ginfo.scm 74 */
								BgL_objectz00_bglt BgL_tmpz00_7038;

								BgL_tmpz00_7038 = ((BgL_objectz00_bglt) BgL_new1257z00_4413);
								BgL_auxz00_7037 = BGL_OBJECT_WIDENING(BgL_tmpz00_7038);
							}
							BgL_auxz00_7036 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7037);
						}
						((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7036))->
								BgL_kapturedzf3zf3) =
							((bool_t) BgL_kapturedzf31166zf3_4410), BUNSPEC);
					}
					{
						BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7043;

						{
							obj_t BgL_auxz00_7044;

							{	/* Globalize/ginfo.scm 74 */
								BgL_objectz00_bglt BgL_tmpz00_7045;

								BgL_tmpz00_7045 = ((BgL_objectz00_bglt) BgL_new1257z00_4413);
								BgL_auxz00_7044 = BGL_OBJECT_WIDENING(BgL_tmpz00_7045);
							}
							BgL_auxz00_7043 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7044);
						}
						((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7043))->
								BgL_freezd2markzd2) =
							((long) BgL_freezd2mark1167zd2_4411), BUNSPEC);
					}
					{
						BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7050;

						{
							obj_t BgL_auxz00_7051;

							{	/* Globalize/ginfo.scm 74 */
								BgL_objectz00_bglt BgL_tmpz00_7052;

								BgL_tmpz00_7052 = ((BgL_objectz00_bglt) BgL_new1257z00_4413);
								BgL_auxz00_7051 = BGL_OBJECT_WIDENING(BgL_tmpz00_7052);
							}
							BgL_auxz00_7050 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7051);
						}
						((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7050))->
								BgL_markz00) = ((long) BgL_mark1168z00_4412), BUNSPEC);
					}
					return BgL_new1257z00_4413;
				}
			}
		}

	}



/* &<@anonymous:1953> */
	obj_t BGl_z62zc3z04anonymousza31953ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3971)
	{
		{	/* Globalize/ginfo.scm 74 */
			return BINT(-10L);
		}

	}



/* &lambda1952 */
	obj_t BGl_z62lambda1952z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3972,
		obj_t BgL_oz00_3973, obj_t BgL_vz00_3974)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				long BgL_vz00_4420;

				BgL_vz00_4420 = (long) CINT(BgL_vz00_3974);
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7059;

					{
						obj_t BgL_auxz00_7060;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_7061;

							BgL_tmpz00_7061 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3973));
							BgL_auxz00_7060 = BGL_OBJECT_WIDENING(BgL_tmpz00_7061);
						}
						BgL_auxz00_7059 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7060);
					}
					return
						((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7059))->
							BgL_markz00) = ((long) BgL_vz00_4420), BUNSPEC);
		}}}

	}



/* &lambda1951 */
	obj_t BGl_z62lambda1951z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3975,
		obj_t BgL_oz00_3976)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				long BgL_tmpz00_7067;

				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7068;

					{
						obj_t BgL_auxz00_7069;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_7070;

							BgL_tmpz00_7070 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3976));
							BgL_auxz00_7069 = BGL_OBJECT_WIDENING(BgL_tmpz00_7070);
						}
						BgL_auxz00_7068 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7069);
					}
					BgL_tmpz00_7067 =
						(((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7068))->
						BgL_markz00);
				}
				return BINT(BgL_tmpz00_7067);
			}
		}

	}



/* &<@anonymous:1946> */
	obj_t BGl_z62zc3z04anonymousza31946ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3977)
	{
		{	/* Globalize/ginfo.scm 74 */
			return BINT(-10L);
		}

	}



/* &lambda1945 */
	obj_t BGl_z62lambda1945z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3978,
		obj_t BgL_oz00_3979, obj_t BgL_vz00_3980)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				long BgL_vz00_4423;

				BgL_vz00_4423 = (long) CINT(BgL_vz00_3980);
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7079;

					{
						obj_t BgL_auxz00_7080;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_7081;

							BgL_tmpz00_7081 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3979));
							BgL_auxz00_7080 = BGL_OBJECT_WIDENING(BgL_tmpz00_7081);
						}
						BgL_auxz00_7079 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7080);
					}
					return
						((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7079))->
							BgL_freezd2markzd2) = ((long) BgL_vz00_4423), BUNSPEC);
		}}}

	}



/* &lambda1944 */
	obj_t BGl_z62lambda1944z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3981,
		obj_t BgL_oz00_3982)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				long BgL_tmpz00_7087;

				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7088;

					{
						obj_t BgL_auxz00_7089;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_7090;

							BgL_tmpz00_7090 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3982));
							BgL_auxz00_7089 = BGL_OBJECT_WIDENING(BgL_tmpz00_7090);
						}
						BgL_auxz00_7088 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7089);
					}
					BgL_tmpz00_7087 =
						(((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7088))->
						BgL_freezd2markzd2);
				}
				return BINT(BgL_tmpz00_7087);
			}
		}

	}



/* &<@anonymous:1939> */
	obj_t BGl_z62zc3z04anonymousza31939ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3983)
	{
		{	/* Globalize/ginfo.scm 74 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1938 */
	obj_t BGl_z62lambda1938z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3984,
		obj_t BgL_oz00_3985, obj_t BgL_vz00_3986)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				bool_t BgL_vz00_4426;

				BgL_vz00_4426 = CBOOL(BgL_vz00_3986);
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7099;

					{
						obj_t BgL_auxz00_7100;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_7101;

							BgL_tmpz00_7101 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3985));
							BgL_auxz00_7100 = BGL_OBJECT_WIDENING(BgL_tmpz00_7101);
						}
						BgL_auxz00_7099 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7100);
					}
					return
						((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7099))->
							BgL_kapturedzf3zf3) = ((bool_t) BgL_vz00_4426), BUNSPEC);
				}
			}
		}

	}



/* &lambda1937 */
	obj_t BGl_z62lambda1937z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3987,
		obj_t BgL_oz00_3988)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				bool_t BgL_tmpz00_7107;

				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7108;

					{
						obj_t BgL_auxz00_7109;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_7110;

							BgL_tmpz00_7110 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3988));
							BgL_auxz00_7109 = BGL_OBJECT_WIDENING(BgL_tmpz00_7110);
						}
						BgL_auxz00_7108 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7109);
					}
					BgL_tmpz00_7107 =
						(((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7108))->
						BgL_kapturedzf3zf3);
				}
				return BBOOL(BgL_tmpz00_7107);
			}
		}

	}



/* &<@anonymous:1932> */
	obj_t BGl_z62zc3z04anonymousza31932ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3989)
	{
		{	/* Globalize/ginfo.scm 74 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1931 */
	obj_t BGl_z62lambda1931z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3990,
		obj_t BgL_oz00_3991, obj_t BgL_vz00_3992)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				bool_t BgL_vz00_4429;

				BgL_vz00_4429 = CBOOL(BgL_vz00_3992);
				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7119;

					{
						obj_t BgL_auxz00_7120;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_7121;

							BgL_tmpz00_7121 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3991));
							BgL_auxz00_7120 = BGL_OBJECT_WIDENING(BgL_tmpz00_7121);
						}
						BgL_auxz00_7119 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7120);
					}
					return
						((((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7119))->
							BgL_gzf3zf3) = ((bool_t) BgL_vz00_4429), BUNSPEC);
				}
			}
		}

	}



/* &lambda1930 */
	obj_t BGl_z62lambda1930z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_3993,
		obj_t BgL_oz00_3994)
	{
		{	/* Globalize/ginfo.scm 74 */
			{	/* Globalize/ginfo.scm 74 */
				bool_t BgL_tmpz00_7127;

				{
					BgL_sexitzf2ginfozf2_bglt BgL_auxz00_7128;

					{
						obj_t BgL_auxz00_7129;

						{	/* Globalize/ginfo.scm 74 */
							BgL_objectz00_bglt BgL_tmpz00_7130;

							BgL_tmpz00_7130 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3994));
							BgL_auxz00_7129 = BGL_OBJECT_WIDENING(BgL_tmpz00_7130);
						}
						BgL_auxz00_7128 = ((BgL_sexitzf2ginfozf2_bglt) BgL_auxz00_7129);
					}
					BgL_tmpz00_7127 =
						(((BgL_sexitzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7128))->
						BgL_gzf3zf3);
				}
				return BBOOL(BgL_tmpz00_7127);
			}
		}

	}



/* &lambda1861 */
	BgL_svarz00_bglt BGl_z62lambda1861z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3995, obj_t BgL_o1161z00_3996)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				long BgL_arg1862z00_4432;

				{	/* Globalize/ginfo.scm 62 */
					obj_t BgL_arg1863z00_4433;

					{	/* Globalize/ginfo.scm 62 */
						obj_t BgL_arg1864z00_4434;

						{	/* Globalize/ginfo.scm 62 */
							obj_t BgL_arg1815z00_4435;
							long BgL_arg1816z00_4436;

							BgL_arg1815z00_4435 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Globalize/ginfo.scm 62 */
								long BgL_arg1817z00_4437;

								BgL_arg1817z00_4437 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_svarz00_bglt) BgL_o1161z00_3996)));
								BgL_arg1816z00_4436 = (BgL_arg1817z00_4437 - OBJECT_TYPE);
							}
							BgL_arg1864z00_4434 =
								VECTOR_REF(BgL_arg1815z00_4435, BgL_arg1816z00_4436);
						}
						BgL_arg1863z00_4433 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1864z00_4434);
					}
					{	/* Globalize/ginfo.scm 62 */
						obj_t BgL_tmpz00_7144;

						BgL_tmpz00_7144 = ((obj_t) BgL_arg1863z00_4433);
						BgL_arg1862z00_4432 = BGL_CLASS_NUM(BgL_tmpz00_7144);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_svarz00_bglt) BgL_o1161z00_3996)), BgL_arg1862z00_4432);
			}
			{	/* Globalize/ginfo.scm 62 */
				BgL_objectz00_bglt BgL_tmpz00_7150;

				BgL_tmpz00_7150 =
					((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_o1161z00_3996));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7150, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_o1161z00_3996));
			return ((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_o1161z00_3996));
		}

	}



/* &<@anonymous:1860> */
	obj_t BGl_z62zc3z04anonymousza31860ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3997, obj_t BgL_new1160z00_3998)
	{
		{	/* Globalize/ginfo.scm 62 */
			{
				BgL_svarz00_bglt BgL_auxz00_7158;

				((((BgL_svarz00_bglt) COBJECT(
								((BgL_svarz00_bglt)
									((BgL_svarz00_bglt) BgL_new1160z00_3998))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7162;

					{
						obj_t BgL_auxz00_7163;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7164;

							BgL_tmpz00_7164 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1160z00_3998));
							BgL_auxz00_7163 = BGL_OBJECT_WIDENING(BgL_tmpz00_7164);
						}
						BgL_auxz00_7162 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7163);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7162))->
							BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7170;

					{
						obj_t BgL_auxz00_7171;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7172;

							BgL_tmpz00_7172 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1160z00_3998));
							BgL_auxz00_7171 = BGL_OBJECT_WIDENING(BgL_tmpz00_7172);
						}
						BgL_auxz00_7170 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7171);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7170))->
							BgL_freezd2markzd2) = ((long) 0L), BUNSPEC);
				}
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7178;

					{
						obj_t BgL_auxz00_7179;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7180;

							BgL_tmpz00_7180 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1160z00_3998));
							BgL_auxz00_7179 = BGL_OBJECT_WIDENING(BgL_tmpz00_7180);
						}
						BgL_auxz00_7178 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7179);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7178))->
							BgL_markz00) = ((long) 0L), BUNSPEC);
				}
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7186;

					{
						obj_t BgL_auxz00_7187;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7188;

							BgL_tmpz00_7188 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1160z00_3998));
							BgL_auxz00_7187 = BGL_OBJECT_WIDENING(BgL_tmpz00_7188);
						}
						BgL_auxz00_7186 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7187);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7186))->
							BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7194;

					{
						obj_t BgL_auxz00_7195;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7196;

							BgL_tmpz00_7196 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1160z00_3998));
							BgL_auxz00_7195 = BGL_OBJECT_WIDENING(BgL_tmpz00_7196);
						}
						BgL_auxz00_7194 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7195);
					}
					((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7194))->
							BgL_stackablez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_7158 = ((BgL_svarz00_bglt) BgL_new1160z00_3998);
				return ((obj_t) BgL_auxz00_7158);
			}
		}

	}



/* &lambda1858 */
	BgL_svarz00_bglt BGl_z62lambda1858z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_3999, obj_t BgL_o1157z00_4000)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				BgL_svarzf2ginfozf2_bglt BgL_wide1159z00_4440;

				BgL_wide1159z00_4440 =
					((BgL_svarzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_svarzf2ginfozf2_bgl))));
				{	/* Globalize/ginfo.scm 62 */
					obj_t BgL_auxz00_7209;
					BgL_objectz00_bglt BgL_tmpz00_7205;

					BgL_auxz00_7209 = ((obj_t) BgL_wide1159z00_4440);
					BgL_tmpz00_7205 =
						((BgL_objectz00_bglt)
						((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_o1157z00_4000)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7205, BgL_auxz00_7209);
				}
				((BgL_objectz00_bglt)
					((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_o1157z00_4000)));
				{	/* Globalize/ginfo.scm 62 */
					long BgL_arg1859z00_4441;

					BgL_arg1859z00_4441 =
						BGL_CLASS_NUM(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_svarz00_bglt)
								((BgL_svarz00_bglt) BgL_o1157z00_4000))), BgL_arg1859z00_4441);
				}
				return
					((BgL_svarz00_bglt)
					((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_o1157z00_4000)));
			}
		}

	}



/* &lambda1855 */
	BgL_svarz00_bglt BGl_z62lambda1855z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4001, obj_t BgL_loc1151z00_4002,
		obj_t BgL_kapturedzf31152zf3_4003, obj_t BgL_freezd2mark1153zd2_4004,
		obj_t BgL_mark1154z00_4005, obj_t BgL_celledzf31155zf3_4006,
		obj_t BgL_stackable1156z00_4007)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				bool_t BgL_kapturedzf31152zf3_4442;
				long BgL_freezd2mark1153zd2_4443;
				long BgL_mark1154z00_4444;
				bool_t BgL_celledzf31155zf3_4445;
				bool_t BgL_stackable1156z00_4446;

				BgL_kapturedzf31152zf3_4442 = CBOOL(BgL_kapturedzf31152zf3_4003);
				BgL_freezd2mark1153zd2_4443 = (long) CINT(BgL_freezd2mark1153zd2_4004);
				BgL_mark1154z00_4444 = (long) CINT(BgL_mark1154z00_4005);
				BgL_celledzf31155zf3_4445 = CBOOL(BgL_celledzf31155zf3_4006);
				BgL_stackable1156z00_4446 = CBOOL(BgL_stackable1156z00_4007);
				{	/* Globalize/ginfo.scm 62 */
					BgL_svarz00_bglt BgL_new1252z00_4447;

					{	/* Globalize/ginfo.scm 62 */
						BgL_svarz00_bglt BgL_tmp1250z00_4448;
						BgL_svarzf2ginfozf2_bglt BgL_wide1251z00_4449;

						{
							BgL_svarz00_bglt BgL_auxz00_7228;

							{	/* Globalize/ginfo.scm 62 */
								BgL_svarz00_bglt BgL_new1249z00_4450;

								BgL_new1249z00_4450 =
									((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_svarz00_bgl))));
								{	/* Globalize/ginfo.scm 62 */
									long BgL_arg1857z00_4451;

									BgL_arg1857z00_4451 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1249z00_4450),
										BgL_arg1857z00_4451);
								}
								{	/* Globalize/ginfo.scm 62 */
									BgL_objectz00_bglt BgL_tmpz00_7233;

									BgL_tmpz00_7233 = ((BgL_objectz00_bglt) BgL_new1249z00_4450);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7233, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1249z00_4450);
								BgL_auxz00_7228 = BgL_new1249z00_4450;
							}
							BgL_tmp1250z00_4448 = ((BgL_svarz00_bglt) BgL_auxz00_7228);
						}
						BgL_wide1251z00_4449 =
							((BgL_svarzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_svarzf2ginfozf2_bgl))));
						{	/* Globalize/ginfo.scm 62 */
							obj_t BgL_auxz00_7241;
							BgL_objectz00_bglt BgL_tmpz00_7239;

							BgL_auxz00_7241 = ((obj_t) BgL_wide1251z00_4449);
							BgL_tmpz00_7239 = ((BgL_objectz00_bglt) BgL_tmp1250z00_4448);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7239, BgL_auxz00_7241);
						}
						((BgL_objectz00_bglt) BgL_tmp1250z00_4448);
						{	/* Globalize/ginfo.scm 62 */
							long BgL_arg1856z00_4452;

							BgL_arg1856z00_4452 =
								BGL_CLASS_NUM(BGl_svarzf2Ginfozf2zzglobaliza7e_ginfoza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1250z00_4448),
								BgL_arg1856z00_4452);
						}
						BgL_new1252z00_4447 = ((BgL_svarz00_bglt) BgL_tmp1250z00_4448);
					}
					((((BgL_svarz00_bglt) COBJECT(
									((BgL_svarz00_bglt) BgL_new1252z00_4447)))->BgL_locz00) =
						((obj_t) BgL_loc1151z00_4002), BUNSPEC);
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_7251;

						{
							obj_t BgL_auxz00_7252;

							{	/* Globalize/ginfo.scm 62 */
								BgL_objectz00_bglt BgL_tmpz00_7253;

								BgL_tmpz00_7253 = ((BgL_objectz00_bglt) BgL_new1252z00_4447);
								BgL_auxz00_7252 = BGL_OBJECT_WIDENING(BgL_tmpz00_7253);
							}
							BgL_auxz00_7251 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7252);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7251))->
								BgL_kapturedzf3zf3) =
							((bool_t) BgL_kapturedzf31152zf3_4442), BUNSPEC);
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_7258;

						{
							obj_t BgL_auxz00_7259;

							{	/* Globalize/ginfo.scm 62 */
								BgL_objectz00_bglt BgL_tmpz00_7260;

								BgL_tmpz00_7260 = ((BgL_objectz00_bglt) BgL_new1252z00_4447);
								BgL_auxz00_7259 = BGL_OBJECT_WIDENING(BgL_tmpz00_7260);
							}
							BgL_auxz00_7258 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7259);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7258))->
								BgL_freezd2markzd2) =
							((long) BgL_freezd2mark1153zd2_4443), BUNSPEC);
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_7265;

						{
							obj_t BgL_auxz00_7266;

							{	/* Globalize/ginfo.scm 62 */
								BgL_objectz00_bglt BgL_tmpz00_7267;

								BgL_tmpz00_7267 = ((BgL_objectz00_bglt) BgL_new1252z00_4447);
								BgL_auxz00_7266 = BGL_OBJECT_WIDENING(BgL_tmpz00_7267);
							}
							BgL_auxz00_7265 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7266);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7265))->
								BgL_markz00) = ((long) BgL_mark1154z00_4444), BUNSPEC);
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_7272;

						{
							obj_t BgL_auxz00_7273;

							{	/* Globalize/ginfo.scm 62 */
								BgL_objectz00_bglt BgL_tmpz00_7274;

								BgL_tmpz00_7274 = ((BgL_objectz00_bglt) BgL_new1252z00_4447);
								BgL_auxz00_7273 = BGL_OBJECT_WIDENING(BgL_tmpz00_7274);
							}
							BgL_auxz00_7272 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7273);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7272))->
								BgL_celledzf3zf3) =
							((bool_t) BgL_celledzf31155zf3_4445), BUNSPEC);
					}
					{
						BgL_svarzf2ginfozf2_bglt BgL_auxz00_7279;

						{
							obj_t BgL_auxz00_7280;

							{	/* Globalize/ginfo.scm 62 */
								BgL_objectz00_bglt BgL_tmpz00_7281;

								BgL_tmpz00_7281 = ((BgL_objectz00_bglt) BgL_new1252z00_4447);
								BgL_auxz00_7280 = BGL_OBJECT_WIDENING(BgL_tmpz00_7281);
							}
							BgL_auxz00_7279 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7280);
						}
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7279))->
								BgL_stackablez00) =
							((bool_t) BgL_stackable1156z00_4446), BUNSPEC);
					}
					return BgL_new1252z00_4447;
				}
			}
		}

	}



/* &<@anonymous:1902> */
	obj_t BGl_z62zc3z04anonymousza31902ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4008)
	{
		{	/* Globalize/ginfo.scm 62 */
			return BBOOL(((bool_t) 1));
		}

	}



/* &lambda1901 */
	obj_t BGl_z62lambda1901z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4009,
		obj_t BgL_oz00_4010, obj_t BgL_vz00_4011)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				bool_t BgL_vz00_4454;

				BgL_vz00_4454 = CBOOL(BgL_vz00_4011);
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7288;

					{
						obj_t BgL_auxz00_7289;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7290;

							BgL_tmpz00_7290 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4010));
							BgL_auxz00_7289 = BGL_OBJECT_WIDENING(BgL_tmpz00_7290);
						}
						BgL_auxz00_7288 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7289);
					}
					return
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7288))->
							BgL_stackablez00) = ((bool_t) BgL_vz00_4454), BUNSPEC);
				}
			}
		}

	}



/* &lambda1900 */
	obj_t BGl_z62lambda1900z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4012,
		obj_t BgL_oz00_4013)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				bool_t BgL_tmpz00_7296;

				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7297;

					{
						obj_t BgL_auxz00_7298;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7299;

							BgL_tmpz00_7299 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4013));
							BgL_auxz00_7298 = BGL_OBJECT_WIDENING(BgL_tmpz00_7299);
						}
						BgL_auxz00_7297 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7298);
					}
					BgL_tmpz00_7296 =
						(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7297))->
						BgL_stackablez00);
				}
				return BBOOL(BgL_tmpz00_7296);
			}
		}

	}



/* &<@anonymous:1895> */
	obj_t BGl_z62zc3z04anonymousza31895ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4014)
	{
		{	/* Globalize/ginfo.scm 62 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1894 */
	obj_t BGl_z62lambda1894z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4015,
		obj_t BgL_oz00_4016, obj_t BgL_vz00_4017)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				bool_t BgL_vz00_4457;

				BgL_vz00_4457 = CBOOL(BgL_vz00_4017);
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7308;

					{
						obj_t BgL_auxz00_7309;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7310;

							BgL_tmpz00_7310 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4016));
							BgL_auxz00_7309 = BGL_OBJECT_WIDENING(BgL_tmpz00_7310);
						}
						BgL_auxz00_7308 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7309);
					}
					return
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7308))->
							BgL_celledzf3zf3) = ((bool_t) BgL_vz00_4457), BUNSPEC);
				}
			}
		}

	}



/* &lambda1893 */
	obj_t BGl_z62lambda1893z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4018,
		obj_t BgL_oz00_4019)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				bool_t BgL_tmpz00_7316;

				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7317;

					{
						obj_t BgL_auxz00_7318;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7319;

							BgL_tmpz00_7319 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4019));
							BgL_auxz00_7318 = BGL_OBJECT_WIDENING(BgL_tmpz00_7319);
						}
						BgL_auxz00_7317 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7318);
					}
					BgL_tmpz00_7316 =
						(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7317))->
						BgL_celledzf3zf3);
				}
				return BBOOL(BgL_tmpz00_7316);
			}
		}

	}



/* &<@anonymous:1888> */
	obj_t BGl_z62zc3z04anonymousza31888ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4020)
	{
		{	/* Globalize/ginfo.scm 62 */
			return BINT(-10L);
		}

	}



/* &lambda1887 */
	obj_t BGl_z62lambda1887z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4021,
		obj_t BgL_oz00_4022, obj_t BgL_vz00_4023)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				long BgL_vz00_4460;

				BgL_vz00_4460 = (long) CINT(BgL_vz00_4023);
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7328;

					{
						obj_t BgL_auxz00_7329;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7330;

							BgL_tmpz00_7330 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4022));
							BgL_auxz00_7329 = BGL_OBJECT_WIDENING(BgL_tmpz00_7330);
						}
						BgL_auxz00_7328 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7329);
					}
					return
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7328))->
							BgL_markz00) = ((long) BgL_vz00_4460), BUNSPEC);
		}}}

	}



/* &lambda1886 */
	obj_t BGl_z62lambda1886z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4024,
		obj_t BgL_oz00_4025)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				long BgL_tmpz00_7336;

				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7337;

					{
						obj_t BgL_auxz00_7338;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7339;

							BgL_tmpz00_7339 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4025));
							BgL_auxz00_7338 = BGL_OBJECT_WIDENING(BgL_tmpz00_7339);
						}
						BgL_auxz00_7337 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7338);
					}
					BgL_tmpz00_7336 =
						(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7337))->
						BgL_markz00);
				}
				return BINT(BgL_tmpz00_7336);
			}
		}

	}



/* &<@anonymous:1880> */
	obj_t BGl_z62zc3z04anonymousza31880ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4026)
	{
		{	/* Globalize/ginfo.scm 62 */
			return BINT(-10L);
		}

	}



/* &lambda1879 */
	obj_t BGl_z62lambda1879z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4027,
		obj_t BgL_oz00_4028, obj_t BgL_vz00_4029)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				long BgL_vz00_4463;

				BgL_vz00_4463 = (long) CINT(BgL_vz00_4029);
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7348;

					{
						obj_t BgL_auxz00_7349;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7350;

							BgL_tmpz00_7350 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4028));
							BgL_auxz00_7349 = BGL_OBJECT_WIDENING(BgL_tmpz00_7350);
						}
						BgL_auxz00_7348 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7349);
					}
					return
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7348))->
							BgL_freezd2markzd2) = ((long) BgL_vz00_4463), BUNSPEC);
		}}}

	}



/* &lambda1878 */
	obj_t BGl_z62lambda1878z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4030,
		obj_t BgL_oz00_4031)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				long BgL_tmpz00_7356;

				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7357;

					{
						obj_t BgL_auxz00_7358;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7359;

							BgL_tmpz00_7359 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4031));
							BgL_auxz00_7358 = BGL_OBJECT_WIDENING(BgL_tmpz00_7359);
						}
						BgL_auxz00_7357 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7358);
					}
					BgL_tmpz00_7356 =
						(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7357))->
						BgL_freezd2markzd2);
				}
				return BINT(BgL_tmpz00_7356);
			}
		}

	}



/* &<@anonymous:1873> */
	obj_t BGl_z62zc3z04anonymousza31873ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4032)
	{
		{	/* Globalize/ginfo.scm 62 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1872 */
	obj_t BGl_z62lambda1872z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4033,
		obj_t BgL_oz00_4034, obj_t BgL_vz00_4035)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				bool_t BgL_vz00_4466;

				BgL_vz00_4466 = CBOOL(BgL_vz00_4035);
				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7368;

					{
						obj_t BgL_auxz00_7369;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7370;

							BgL_tmpz00_7370 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4034));
							BgL_auxz00_7369 = BGL_OBJECT_WIDENING(BgL_tmpz00_7370);
						}
						BgL_auxz00_7368 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7369);
					}
					return
						((((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7368))->
							BgL_kapturedzf3zf3) = ((bool_t) BgL_vz00_4466), BUNSPEC);
				}
			}
		}

	}



/* &lambda1871 */
	obj_t BGl_z62lambda1871z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4036,
		obj_t BgL_oz00_4037)
	{
		{	/* Globalize/ginfo.scm 62 */
			{	/* Globalize/ginfo.scm 62 */
				bool_t BgL_tmpz00_7376;

				{
					BgL_svarzf2ginfozf2_bglt BgL_auxz00_7377;

					{
						obj_t BgL_auxz00_7378;

						{	/* Globalize/ginfo.scm 62 */
							BgL_objectz00_bglt BgL_tmpz00_7379;

							BgL_tmpz00_7379 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_4037));
							BgL_auxz00_7378 = BGL_OBJECT_WIDENING(BgL_tmpz00_7379);
						}
						BgL_auxz00_7377 = ((BgL_svarzf2ginfozf2_bglt) BgL_auxz00_7378);
					}
					BgL_tmpz00_7376 =
						(((BgL_svarzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7377))->
						BgL_kapturedzf3zf3);
				}
				return BBOOL(BgL_tmpz00_7376);
			}
		}

	}



/* &lambda1546 */
	BgL_sfunz00_bglt BGl_z62lambda1546z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4038, obj_t BgL_o1149z00_4039)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				long BgL_arg1552z00_4469;

				{	/* Globalize/ginfo.scm 20 */
					obj_t BgL_arg1553z00_4470;

					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_arg1559z00_4471;

						{	/* Globalize/ginfo.scm 20 */
							obj_t BgL_arg1815z00_4472;
							long BgL_arg1816z00_4473;

							BgL_arg1815z00_4472 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Globalize/ginfo.scm 20 */
								long BgL_arg1817z00_4474;

								BgL_arg1817z00_4474 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_o1149z00_4039)));
								BgL_arg1816z00_4473 = (BgL_arg1817z00_4474 - OBJECT_TYPE);
							}
							BgL_arg1559z00_4471 =
								VECTOR_REF(BgL_arg1815z00_4472, BgL_arg1816z00_4473);
						}
						BgL_arg1553z00_4470 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1559z00_4471);
					}
					{	/* Globalize/ginfo.scm 20 */
						obj_t BgL_tmpz00_7393;

						BgL_tmpz00_7393 = ((obj_t) BgL_arg1553z00_4470);
						BgL_arg1552z00_4469 = BGL_CLASS_NUM(BgL_tmpz00_7393);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) BgL_o1149z00_4039)), BgL_arg1552z00_4469);
			}
			{	/* Globalize/ginfo.scm 20 */
				BgL_objectz00_bglt BgL_tmpz00_7399;

				BgL_tmpz00_7399 =
					((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1149z00_4039));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7399, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1149z00_4039));
			return ((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1149z00_4039));
		}

	}



/* &<@anonymous:1545> */
	obj_t BGl_z62zc3z04anonymousza31545ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4040, obj_t BgL_new1148z00_4041)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunz00_bglt BgL_auxz00_7407;

				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_sfunz00_bglt) BgL_new1148z00_4041))))->BgL_arityz00) =
					((long) 0L), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_predicatezd2ofzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_stackzd2allocatorzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_topzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_thezd2closurezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_failsafez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_argszd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_argszd2retescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_propertyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_argszd2namezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_bodyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_dssslzd2keywordszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_optionalsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_keysz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_strengthz00) =
					((obj_t) CNST_TABLE_REF(35)), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1148z00_4041))))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7475;

					{
						obj_t BgL_auxz00_7476;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7477;

							BgL_tmpz00_7477 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7476 = BGL_OBJECT_WIDENING(BgL_tmpz00_7477);
						}
						BgL_auxz00_7475 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7476);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7475))->
							BgL_gzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7483;

					{
						obj_t BgL_auxz00_7484;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7485;

							BgL_tmpz00_7485 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7484 = BGL_OBJECT_WIDENING(BgL_tmpz00_7485);
						}
						BgL_auxz00_7483 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7484);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7483))->
							BgL_cfromz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7491;

					{
						obj_t BgL_auxz00_7492;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7493;

							BgL_tmpz00_7493 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7492 = BGL_OBJECT_WIDENING(BgL_tmpz00_7493);
						}
						BgL_auxz00_7491 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7492);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7491))->
							BgL_cfromza2za2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7499;

					{
						obj_t BgL_auxz00_7500;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7501;

							BgL_tmpz00_7501 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7500 = BGL_OBJECT_WIDENING(BgL_tmpz00_7501);
						}
						BgL_auxz00_7499 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7500);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7499))->BgL_ctoz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7507;

					{
						obj_t BgL_auxz00_7508;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7509;

							BgL_tmpz00_7509 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7508 = BGL_OBJECT_WIDENING(BgL_tmpz00_7509);
						}
						BgL_auxz00_7507 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7508);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7507))->
							BgL_ctoza2za2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7515;

					{
						obj_t BgL_auxz00_7516;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7517;

							BgL_tmpz00_7517 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7516 = BGL_OBJECT_WIDENING(BgL_tmpz00_7517);
						}
						BgL_auxz00_7515 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7516);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7515))->
							BgL_efunctionsz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7523;

					{
						obj_t BgL_auxz00_7524;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7525;

							BgL_tmpz00_7525 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7524 = BGL_OBJECT_WIDENING(BgL_tmpz00_7525);
						}
						BgL_auxz00_7523 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7524);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7523))->
							BgL_integratorz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7531;

					{
						obj_t BgL_auxz00_7532;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7533;

							BgL_tmpz00_7533 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7532 = BGL_OBJECT_WIDENING(BgL_tmpz00_7533);
						}
						BgL_auxz00_7531 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7532);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7531))->
							BgL_imarkz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7539;

					{
						obj_t BgL_auxz00_7540;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7541;

							BgL_tmpz00_7541 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7540 = BGL_OBJECT_WIDENING(BgL_tmpz00_7541);
						}
						BgL_auxz00_7539 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7540);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7539))->
							BgL_ownerz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7547;

					{
						obj_t BgL_auxz00_7548;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7549;

							BgL_tmpz00_7549 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7548 = BGL_OBJECT_WIDENING(BgL_tmpz00_7549);
						}
						BgL_auxz00_7547 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7548);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7547))->
							BgL_integratedz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7555;

					{
						obj_t BgL_auxz00_7556;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7557;

							BgL_tmpz00_7557 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7556 = BGL_OBJECT_WIDENING(BgL_tmpz00_7557);
						}
						BgL_auxz00_7555 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7556);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7555))->
							BgL_pluggedzd2inzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7563;

					{
						obj_t BgL_auxz00_7564;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7565;

							BgL_tmpz00_7565 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7564 = BGL_OBJECT_WIDENING(BgL_tmpz00_7565);
						}
						BgL_auxz00_7563 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7564);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7563))->
							BgL_markz00) = ((long) 0L), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7571;

					{
						obj_t BgL_auxz00_7572;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7573;

							BgL_tmpz00_7573 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7572 = BGL_OBJECT_WIDENING(BgL_tmpz00_7573);
						}
						BgL_auxz00_7571 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7572);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7571))->
							BgL_freezd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7579;

					{
						obj_t BgL_auxz00_7580;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7581;

							BgL_tmpz00_7581 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7580 = BGL_OBJECT_WIDENING(BgL_tmpz00_7581);
						}
						BgL_auxz00_7579 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7580);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7579))->
							BgL_thezd2globalzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7587;

					{
						obj_t BgL_auxz00_7588;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7589;

							BgL_tmpz00_7589 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7588 = BGL_OBJECT_WIDENING(BgL_tmpz00_7589);
						}
						BgL_auxz00_7587 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7588);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7587))->
							BgL_kapturedz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7595;

					{
						obj_t BgL_auxz00_7596;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7597;

							BgL_tmpz00_7597 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7596 = BGL_OBJECT_WIDENING(BgL_tmpz00_7597);
						}
						BgL_auxz00_7595 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7596);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7595))->
							BgL_newzd2bodyzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7603;

					{
						obj_t BgL_auxz00_7604;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7605;

							BgL_tmpz00_7605 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7604 = BGL_OBJECT_WIDENING(BgL_tmpz00_7605);
						}
						BgL_auxz00_7603 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7604);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7603))->
							BgL_bmarkz00) = ((long) 0L), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7611;

					{
						obj_t BgL_auxz00_7612;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7613;

							BgL_tmpz00_7613 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7612 = BGL_OBJECT_WIDENING(BgL_tmpz00_7613);
						}
						BgL_auxz00_7611 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7612);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7611))->
							BgL_umarkz00) = ((long) 0L), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7619;

					{
						obj_t BgL_auxz00_7620;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7621;

							BgL_tmpz00_7621 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7620 = BGL_OBJECT_WIDENING(BgL_tmpz00_7621);
						}
						BgL_auxz00_7619 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7620);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7619))->
							BgL_freez00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7627;

					{
						obj_t BgL_auxz00_7628;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7629;

							BgL_tmpz00_7629 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1148z00_4041));
							BgL_auxz00_7628 = BGL_OBJECT_WIDENING(BgL_tmpz00_7629);
						}
						BgL_auxz00_7627 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7628);
					}
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7627))->
							BgL_boundz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_7407 = ((BgL_sfunz00_bglt) BgL_new1148z00_4041);
				return ((obj_t) BgL_auxz00_7407);
			}
		}

	}



/* &lambda1541 */
	BgL_sfunz00_bglt BGl_z62lambda1541z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4042, obj_t BgL_o1145z00_4043)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				BgL_sfunzf2ginfozf2_bglt BgL_wide1147z00_4477;

				BgL_wide1147z00_4477 =
					((BgL_sfunzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sfunzf2ginfozf2_bgl))));
				{	/* Globalize/ginfo.scm 20 */
					obj_t BgL_auxz00_7642;
					BgL_objectz00_bglt BgL_tmpz00_7638;

					BgL_auxz00_7642 = ((obj_t) BgL_wide1147z00_4477);
					BgL_tmpz00_7638 =
						((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1145z00_4043)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7638, BgL_auxz00_7642);
				}
				((BgL_objectz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1145z00_4043)));
				{	/* Globalize/ginfo.scm 20 */
					long BgL_arg1544z00_4478;

					BgL_arg1544z00_4478 =
						BGL_CLASS_NUM(BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt)
								((BgL_sfunz00_bglt) BgL_o1145z00_4043))), BgL_arg1544z00_4478);
				}
				return
					((BgL_sfunz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1145z00_4043)));
			}
		}

	}



/* &lambda1517 */
	BgL_sfunz00_bglt BGl_z62lambda1517z62zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4044, obj_t BgL_arity1103z00_4045,
		obj_t BgL_sidezd2effect1104zd2_4046, obj_t BgL_predicatezd2of1105zd2_4047,
		obj_t BgL_stackzd2allocator1106zd2_4048, obj_t BgL_topzf31107zf3_4049,
		obj_t BgL_thezd2closure1108zd2_4050, obj_t BgL_effect1109z00_4051,
		obj_t BgL_failsafe1110z00_4052, obj_t BgL_argszd2noescape1111zd2_4053,
		obj_t BgL_argszd2retescape1112zd2_4054, obj_t BgL_property1113z00_4055,
		obj_t BgL_args1114z00_4056, obj_t BgL_argszd2name1115zd2_4057,
		obj_t BgL_body1116z00_4058, obj_t BgL_class1117z00_4059,
		obj_t BgL_dssslzd2keywords1118zd2_4060, obj_t BgL_loc1119z00_4061,
		obj_t BgL_optionals1120z00_4062, obj_t BgL_keys1121z00_4063,
		obj_t BgL_thezd2closurezd2global1122z00_4064,
		obj_t BgL_strength1123z00_4065, obj_t BgL_stackable1124z00_4066,
		obj_t BgL_gzf31125zf3_4067, obj_t BgL_cfrom1126z00_4068,
		obj_t BgL_cfromza21127za2_4069, obj_t BgL_cto1128z00_4070,
		obj_t BgL_ctoza21129za2_4071, obj_t BgL_efunctions1130z00_4072,
		obj_t BgL_integrator1131z00_4073, obj_t BgL_imark1132z00_4074,
		obj_t BgL_owner1133z00_4075, obj_t BgL_integrated1134z00_4076,
		obj_t BgL_pluggedzd2in1135zd2_4077, obj_t BgL_mark1136z00_4078,
		obj_t BgL_freezd2mark1137zd2_4079, obj_t BgL_thezd2global1138zd2_4080,
		obj_t BgL_kaptured1139z00_4081, obj_t BgL_newzd2body1140zd2_4082,
		obj_t BgL_bmark1141z00_4083, obj_t BgL_umark1142z00_4084,
		obj_t BgL_free1143z00_4085, obj_t BgL_bound1144z00_4086)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				long BgL_arity1103z00_4479;
				bool_t BgL_topzf31107zf3_4480;
				bool_t BgL_gzf31125zf3_4482;
				long BgL_mark1136z00_4483;
				long BgL_bmark1141z00_4484;
				long BgL_umark1142z00_4485;

				BgL_arity1103z00_4479 = (long) CINT(BgL_arity1103z00_4045);
				BgL_topzf31107zf3_4480 = CBOOL(BgL_topzf31107zf3_4049);
				BgL_gzf31125zf3_4482 = CBOOL(BgL_gzf31125zf3_4067);
				BgL_mark1136z00_4483 = (long) CINT(BgL_mark1136z00_4078);
				BgL_bmark1141z00_4484 = (long) CINT(BgL_bmark1141z00_4083);
				BgL_umark1142z00_4485 = (long) CINT(BgL_umark1142z00_4084);
				{	/* Globalize/ginfo.scm 20 */
					BgL_sfunz00_bglt BgL_new1247z00_4486;

					{	/* Globalize/ginfo.scm 20 */
						BgL_sfunz00_bglt BgL_tmp1245z00_4487;
						BgL_sfunzf2ginfozf2_bglt BgL_wide1246z00_4488;

						{
							BgL_sfunz00_bglt BgL_auxz00_7662;

							{	/* Globalize/ginfo.scm 20 */
								BgL_sfunz00_bglt BgL_new1244z00_4489;

								BgL_new1244z00_4489 =
									((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sfunz00_bgl))));
								{	/* Globalize/ginfo.scm 20 */
									long BgL_arg1540z00_4490;

									BgL_arg1540z00_4490 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1244z00_4489),
										BgL_arg1540z00_4490);
								}
								{	/* Globalize/ginfo.scm 20 */
									BgL_objectz00_bglt BgL_tmpz00_7667;

									BgL_tmpz00_7667 = ((BgL_objectz00_bglt) BgL_new1244z00_4489);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7667, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1244z00_4489);
								BgL_auxz00_7662 = BgL_new1244z00_4489;
							}
							BgL_tmp1245z00_4487 = ((BgL_sfunz00_bglt) BgL_auxz00_7662);
						}
						BgL_wide1246z00_4488 =
							((BgL_sfunzf2ginfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sfunzf2ginfozf2_bgl))));
						{	/* Globalize/ginfo.scm 20 */
							obj_t BgL_auxz00_7675;
							BgL_objectz00_bglt BgL_tmpz00_7673;

							BgL_auxz00_7675 = ((obj_t) BgL_wide1246z00_4488);
							BgL_tmpz00_7673 = ((BgL_objectz00_bglt) BgL_tmp1245z00_4487);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7673, BgL_auxz00_7675);
						}
						((BgL_objectz00_bglt) BgL_tmp1245z00_4487);
						{	/* Globalize/ginfo.scm 20 */
							long BgL_arg1535z00_4491;

							BgL_arg1535z00_4491 =
								BGL_CLASS_NUM(BGl_sfunzf2Ginfozf2zzglobaliza7e_ginfoza7);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1245z00_4487),
								BgL_arg1535z00_4491);
						}
						BgL_new1247z00_4486 = ((BgL_sfunz00_bglt) BgL_tmp1245z00_4487);
					}
					((((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_new1247z00_4486)))->BgL_arityz00) =
						((long) BgL_arity1103z00_4479), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1104zd2_4046), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_predicatezd2ofzd2) =
						((obj_t) BgL_predicatezd2of1105zd2_4047), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_stackzd2allocatorzd2) =
						((obj_t) BgL_stackzd2allocator1106zd2_4048), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_topzf3zf3) =
						((bool_t) BgL_topzf31107zf3_4480), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_thezd2closurezd2) =
						((obj_t) BgL_thezd2closure1108zd2_4050), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_effectz00) =
						((obj_t) BgL_effect1109z00_4051), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_failsafez00) =
						((obj_t) BgL_failsafe1110z00_4052), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_argszd2noescapezd2) =
						((obj_t) BgL_argszd2noescape1111zd2_4053), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1247z00_4486)))->BgL_argszd2retescapezd2) =
						((obj_t) BgL_argszd2retescape1112zd2_4054), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_propertyz00) =
						((obj_t) BgL_property1113z00_4055), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_argsz00) =
						((obj_t) BgL_args1114z00_4056), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_argszd2namezd2) =
						((obj_t) BgL_argszd2name1115zd2_4057), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_bodyz00) =
						((obj_t) BgL_body1116z00_4058), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_classz00) =
						((obj_t) BgL_class1117z00_4059), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_dssslzd2keywordszd2) =
						((obj_t) BgL_dssslzd2keywords1118zd2_4060), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_locz00) =
						((obj_t) BgL_loc1119z00_4061), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_optionalsz00) =
						((obj_t) BgL_optionals1120z00_4062), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_keysz00) =
						((obj_t) BgL_keys1121z00_4063), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_thezd2closurezd2globalz00) =
						((obj_t) BgL_thezd2closurezd2global1122z00_4064), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_strengthz00) =
						((obj_t) ((obj_t) BgL_strength1123z00_4065)), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1247z00_4486)))->BgL_stackablez00) =
						((obj_t) BgL_stackable1124z00_4066), BUNSPEC);
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7728;

						{
							obj_t BgL_auxz00_7729;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7730;

								BgL_tmpz00_7730 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7729 = BGL_OBJECT_WIDENING(BgL_tmpz00_7730);
							}
							BgL_auxz00_7728 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7729);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7728))->
								BgL_gzf3zf3) = ((bool_t) BgL_gzf31125zf3_4482), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7735;

						{
							obj_t BgL_auxz00_7736;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7737;

								BgL_tmpz00_7737 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7736 = BGL_OBJECT_WIDENING(BgL_tmpz00_7737);
							}
							BgL_auxz00_7735 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7736);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7735))->
								BgL_cfromz00) = ((obj_t) BgL_cfrom1126z00_4068), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7742;

						{
							obj_t BgL_auxz00_7743;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7744;

								BgL_tmpz00_7744 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7743 = BGL_OBJECT_WIDENING(BgL_tmpz00_7744);
							}
							BgL_auxz00_7742 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7743);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7742))->
								BgL_cfromza2za2) = ((obj_t) BgL_cfromza21127za2_4069), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7749;

						{
							obj_t BgL_auxz00_7750;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7751;

								BgL_tmpz00_7751 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7750 = BGL_OBJECT_WIDENING(BgL_tmpz00_7751);
							}
							BgL_auxz00_7749 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7750);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7749))->
								BgL_ctoz00) = ((obj_t) BgL_cto1128z00_4070), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7756;

						{
							obj_t BgL_auxz00_7757;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7758;

								BgL_tmpz00_7758 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7757 = BGL_OBJECT_WIDENING(BgL_tmpz00_7758);
							}
							BgL_auxz00_7756 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7757);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7756))->
								BgL_ctoza2za2) = ((obj_t) BgL_ctoza21129za2_4071), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7763;

						{
							obj_t BgL_auxz00_7764;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7765;

								BgL_tmpz00_7765 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7764 = BGL_OBJECT_WIDENING(BgL_tmpz00_7765);
							}
							BgL_auxz00_7763 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7764);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7763))->
								BgL_efunctionsz00) =
							((obj_t) BgL_efunctions1130z00_4072), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7770;

						{
							obj_t BgL_auxz00_7771;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7772;

								BgL_tmpz00_7772 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7771 = BGL_OBJECT_WIDENING(BgL_tmpz00_7772);
							}
							BgL_auxz00_7770 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7771);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7770))->
								BgL_integratorz00) =
							((obj_t) BgL_integrator1131z00_4073), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7777;

						{
							obj_t BgL_auxz00_7778;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7779;

								BgL_tmpz00_7779 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7778 = BGL_OBJECT_WIDENING(BgL_tmpz00_7779);
							}
							BgL_auxz00_7777 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7778);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7777))->
								BgL_imarkz00) = ((obj_t) BgL_imark1132z00_4074), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7784;

						{
							obj_t BgL_auxz00_7785;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7786;

								BgL_tmpz00_7786 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7785 = BGL_OBJECT_WIDENING(BgL_tmpz00_7786);
							}
							BgL_auxz00_7784 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7785);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7784))->
								BgL_ownerz00) = ((obj_t) BgL_owner1133z00_4075), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7791;

						{
							obj_t BgL_auxz00_7792;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7793;

								BgL_tmpz00_7793 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7792 = BGL_OBJECT_WIDENING(BgL_tmpz00_7793);
							}
							BgL_auxz00_7791 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7792);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7791))->
								BgL_integratedz00) =
							((obj_t) BgL_integrated1134z00_4076), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7798;

						{
							obj_t BgL_auxz00_7799;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7800;

								BgL_tmpz00_7800 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7799 = BGL_OBJECT_WIDENING(BgL_tmpz00_7800);
							}
							BgL_auxz00_7798 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7799);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7798))->
								BgL_pluggedzd2inzd2) =
							((obj_t) BgL_pluggedzd2in1135zd2_4077), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7805;

						{
							obj_t BgL_auxz00_7806;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7807;

								BgL_tmpz00_7807 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7806 = BGL_OBJECT_WIDENING(BgL_tmpz00_7807);
							}
							BgL_auxz00_7805 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7806);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7805))->
								BgL_markz00) = ((long) BgL_mark1136z00_4483), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7812;

						{
							obj_t BgL_auxz00_7813;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7814;

								BgL_tmpz00_7814 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7813 = BGL_OBJECT_WIDENING(BgL_tmpz00_7814);
							}
							BgL_auxz00_7812 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7813);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7812))->
								BgL_freezd2markzd2) =
							((obj_t) BgL_freezd2mark1137zd2_4079), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7819;

						{
							obj_t BgL_auxz00_7820;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7821;

								BgL_tmpz00_7821 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7820 = BGL_OBJECT_WIDENING(BgL_tmpz00_7821);
							}
							BgL_auxz00_7819 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7820);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7819))->
								BgL_thezd2globalzd2) =
							((obj_t) BgL_thezd2global1138zd2_4080), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7826;

						{
							obj_t BgL_auxz00_7827;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7828;

								BgL_tmpz00_7828 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7827 = BGL_OBJECT_WIDENING(BgL_tmpz00_7828);
							}
							BgL_auxz00_7826 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7827);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7826))->
								BgL_kapturedz00) = ((obj_t) BgL_kaptured1139z00_4081), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7833;

						{
							obj_t BgL_auxz00_7834;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7835;

								BgL_tmpz00_7835 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7834 = BGL_OBJECT_WIDENING(BgL_tmpz00_7835);
							}
							BgL_auxz00_7833 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7834);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7833))->
								BgL_newzd2bodyzd2) =
							((obj_t) BgL_newzd2body1140zd2_4082), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7840;

						{
							obj_t BgL_auxz00_7841;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7842;

								BgL_tmpz00_7842 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7841 = BGL_OBJECT_WIDENING(BgL_tmpz00_7842);
							}
							BgL_auxz00_7840 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7841);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7840))->
								BgL_bmarkz00) = ((long) BgL_bmark1141z00_4484), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7847;

						{
							obj_t BgL_auxz00_7848;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7849;

								BgL_tmpz00_7849 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7848 = BGL_OBJECT_WIDENING(BgL_tmpz00_7849);
							}
							BgL_auxz00_7847 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7848);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7847))->
								BgL_umarkz00) = ((long) BgL_umark1142z00_4485), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7854;

						{
							obj_t BgL_auxz00_7855;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7856;

								BgL_tmpz00_7856 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7855 = BGL_OBJECT_WIDENING(BgL_tmpz00_7856);
							}
							BgL_auxz00_7854 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7855);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7854))->
								BgL_freez00) = ((obj_t) BgL_free1143z00_4085), BUNSPEC);
					}
					{
						BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7861;

						{
							obj_t BgL_auxz00_7862;

							{	/* Globalize/ginfo.scm 20 */
								BgL_objectz00_bglt BgL_tmpz00_7863;

								BgL_tmpz00_7863 = ((BgL_objectz00_bglt) BgL_new1247z00_4486);
								BgL_auxz00_7862 = BGL_OBJECT_WIDENING(BgL_tmpz00_7863);
							}
							BgL_auxz00_7861 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7862);
						}
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7861))->
								BgL_boundz00) = ((obj_t) BgL_bound1144z00_4086), BUNSPEC);
					}
					return BgL_new1247z00_4486;
				}
			}
		}

	}



/* &<@anonymous:1848> */
	obj_t BGl_z62zc3z04anonymousza31848ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4087)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BNIL;
		}

	}



/* &lambda1847 */
	obj_t BGl_z62lambda1847z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4088,
		obj_t BgL_oz00_4089, obj_t BgL_vz00_4090)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7868;

				{
					obj_t BgL_auxz00_7869;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7870;

						BgL_tmpz00_7870 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4089));
						BgL_auxz00_7869 = BGL_OBJECT_WIDENING(BgL_tmpz00_7870);
					}
					BgL_auxz00_7868 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7869);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7868))->
						BgL_boundz00) = ((obj_t) BgL_vz00_4090), BUNSPEC);
			}
		}

	}



/* &lambda1846 */
	obj_t BGl_z62lambda1846z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4091,
		obj_t BgL_oz00_4092)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7876;

				{
					obj_t BgL_auxz00_7877;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7878;

						BgL_tmpz00_7878 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4092));
						BgL_auxz00_7877 = BGL_OBJECT_WIDENING(BgL_tmpz00_7878);
					}
					BgL_auxz00_7876 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7877);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7876))->BgL_boundz00);
			}
		}

	}



/* &<@anonymous:1841> */
	obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4093)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BUNSPEC;
		}

	}



/* &lambda1840 */
	obj_t BGl_z62lambda1840z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4094,
		obj_t BgL_oz00_4095, obj_t BgL_vz00_4096)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7884;

				{
					obj_t BgL_auxz00_7885;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7886;

						BgL_tmpz00_7886 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4095));
						BgL_auxz00_7885 = BGL_OBJECT_WIDENING(BgL_tmpz00_7886);
					}
					BgL_auxz00_7884 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7885);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7884))->
						BgL_freez00) = ((obj_t) BgL_vz00_4096), BUNSPEC);
			}
		}

	}



/* &lambda1839 */
	obj_t BGl_z62lambda1839z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4097,
		obj_t BgL_oz00_4098)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7892;

				{
					obj_t BgL_auxz00_7893;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7894;

						BgL_tmpz00_7894 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4098));
						BgL_auxz00_7893 = BGL_OBJECT_WIDENING(BgL_tmpz00_7894);
					}
					BgL_auxz00_7892 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7893);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7892))->BgL_freez00);
			}
		}

	}



/* &<@anonymous:1834> */
	obj_t BGl_z62zc3z04anonymousza31834ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4099)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BINT(-10L);
		}

	}



/* &lambda1833 */
	obj_t BGl_z62lambda1833z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4100,
		obj_t BgL_oz00_4101, obj_t BgL_vz00_4102)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				long BgL_vz00_4497;

				BgL_vz00_4497 = (long) CINT(BgL_vz00_4102);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7902;

					{
						obj_t BgL_auxz00_7903;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7904;

							BgL_tmpz00_7904 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4101));
							BgL_auxz00_7903 = BGL_OBJECT_WIDENING(BgL_tmpz00_7904);
						}
						BgL_auxz00_7902 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7903);
					}
					return
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7902))->
							BgL_umarkz00) = ((long) BgL_vz00_4497), BUNSPEC);
		}}}

	}



/* &lambda1832 */
	obj_t BGl_z62lambda1832z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4103,
		obj_t BgL_oz00_4104)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				long BgL_tmpz00_7910;

				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7911;

					{
						obj_t BgL_auxz00_7912;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7913;

							BgL_tmpz00_7913 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4104));
							BgL_auxz00_7912 = BGL_OBJECT_WIDENING(BgL_tmpz00_7913);
						}
						BgL_auxz00_7911 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7912);
					}
					BgL_tmpz00_7910 =
						(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7911))->
						BgL_umarkz00);
				}
				return BINT(BgL_tmpz00_7910);
			}
		}

	}



/* &<@anonymous:1815> */
	obj_t BGl_z62zc3z04anonymousza31815ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4105)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BINT(-10L);
		}

	}



/* &lambda1814 */
	obj_t BGl_z62lambda1814z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4106,
		obj_t BgL_oz00_4107, obj_t BgL_vz00_4108)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				long BgL_vz00_4500;

				BgL_vz00_4500 = (long) CINT(BgL_vz00_4108);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7922;

					{
						obj_t BgL_auxz00_7923;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7924;

							BgL_tmpz00_7924 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4107));
							BgL_auxz00_7923 = BGL_OBJECT_WIDENING(BgL_tmpz00_7924);
						}
						BgL_auxz00_7922 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7923);
					}
					return
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7922))->
							BgL_bmarkz00) = ((long) BgL_vz00_4500), BUNSPEC);
		}}}

	}



/* &lambda1813 */
	obj_t BGl_z62lambda1813z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4109,
		obj_t BgL_oz00_4110)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				long BgL_tmpz00_7930;

				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7931;

					{
						obj_t BgL_auxz00_7932;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_7933;

							BgL_tmpz00_7933 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4110));
							BgL_auxz00_7932 = BGL_OBJECT_WIDENING(BgL_tmpz00_7933);
						}
						BgL_auxz00_7931 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7932);
					}
					BgL_tmpz00_7930 =
						(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7931))->
						BgL_bmarkz00);
				}
				return BINT(BgL_tmpz00_7930);
			}
		}

	}



/* &<@anonymous:1801> */
	obj_t BGl_z62zc3z04anonymousza31801ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4111)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1800 */
	obj_t BGl_z62lambda1800z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4112,
		obj_t BgL_oz00_4113, obj_t BgL_vz00_4114)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7941;

				{
					obj_t BgL_auxz00_7942;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7943;

						BgL_tmpz00_7943 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4113));
						BgL_auxz00_7942 = BGL_OBJECT_WIDENING(BgL_tmpz00_7943);
					}
					BgL_auxz00_7941 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7942);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7941))->
						BgL_newzd2bodyzd2) = ((obj_t) BgL_vz00_4114), BUNSPEC);
			}
		}

	}



/* &lambda1799 */
	obj_t BGl_z62lambda1799z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4115,
		obj_t BgL_oz00_4116)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7949;

				{
					obj_t BgL_auxz00_7950;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7951;

						BgL_tmpz00_7951 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4116));
						BgL_auxz00_7950 = BGL_OBJECT_WIDENING(BgL_tmpz00_7951);
					}
					BgL_auxz00_7949 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7950);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7949))->
					BgL_newzd2bodyzd2);
			}
		}

	}



/* &<@anonymous:1770> */
	obj_t BGl_z62zc3z04anonymousza31770ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4117)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1769 */
	obj_t BGl_z62lambda1769z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4118,
		obj_t BgL_oz00_4119, obj_t BgL_vz00_4120)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7958;

				{
					obj_t BgL_auxz00_7959;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7960;

						BgL_tmpz00_7960 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4119));
						BgL_auxz00_7959 = BGL_OBJECT_WIDENING(BgL_tmpz00_7960);
					}
					BgL_auxz00_7958 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7959);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7958))->
						BgL_kapturedz00) = ((obj_t) BgL_vz00_4120), BUNSPEC);
			}
		}

	}



/* &lambda1768 */
	obj_t BGl_z62lambda1768z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4121,
		obj_t BgL_oz00_4122)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7966;

				{
					obj_t BgL_auxz00_7967;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7968;

						BgL_tmpz00_7968 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4122));
						BgL_auxz00_7967 = BGL_OBJECT_WIDENING(BgL_tmpz00_7968);
					}
					BgL_auxz00_7966 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7967);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7966))->
					BgL_kapturedz00);
			}
		}

	}



/* &<@anonymous:1757> */
	obj_t BGl_z62zc3z04anonymousza31757ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4123)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1756 */
	obj_t BGl_z62lambda1756z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4124,
		obj_t BgL_oz00_4125, obj_t BgL_vz00_4126)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7975;

				{
					obj_t BgL_auxz00_7976;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7977;

						BgL_tmpz00_7977 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4125));
						BgL_auxz00_7976 = BGL_OBJECT_WIDENING(BgL_tmpz00_7977);
					}
					BgL_auxz00_7975 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7976);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7975))->
						BgL_thezd2globalzd2) = ((obj_t) BgL_vz00_4126), BUNSPEC);
			}
		}

	}



/* &lambda1755 */
	obj_t BGl_z62lambda1755z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4127,
		obj_t BgL_oz00_4128)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7983;

				{
					obj_t BgL_auxz00_7984;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7985;

						BgL_tmpz00_7985 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4128));
						BgL_auxz00_7984 = BGL_OBJECT_WIDENING(BgL_tmpz00_7985);
					}
					BgL_auxz00_7983 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7984);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7983))->
					BgL_thezd2globalzd2);
			}
		}

	}



/* &<@anonymous:1750> */
	obj_t BGl_z62zc3z04anonymousza31750ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4129)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BNIL;
		}

	}



/* &lambda1749 */
	obj_t BGl_z62lambda1749z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4130,
		obj_t BgL_oz00_4131, obj_t BgL_vz00_4132)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7991;

				{
					obj_t BgL_auxz00_7992;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_7993;

						BgL_tmpz00_7993 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4131));
						BgL_auxz00_7992 = BGL_OBJECT_WIDENING(BgL_tmpz00_7993);
					}
					BgL_auxz00_7991 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_7992);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7991))->
						BgL_freezd2markzd2) = ((obj_t) BgL_vz00_4132), BUNSPEC);
			}
		}

	}



/* &lambda1748 */
	obj_t BGl_z62lambda1748z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4133,
		obj_t BgL_oz00_4134)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_7999;

				{
					obj_t BgL_auxz00_8000;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8001;

						BgL_tmpz00_8001 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4134));
						BgL_auxz00_8000 = BGL_OBJECT_WIDENING(BgL_tmpz00_8001);
					}
					BgL_auxz00_7999 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8000);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_7999))->
					BgL_freezd2markzd2);
			}
		}

	}



/* &<@anonymous:1738> */
	obj_t BGl_z62zc3z04anonymousza31738ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4135)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BINT(-10L);
		}

	}



/* &lambda1737 */
	obj_t BGl_z62lambda1737z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4136,
		obj_t BgL_oz00_4137, obj_t BgL_vz00_4138)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				long BgL_vz00_4511;

				BgL_vz00_4511 = (long) CINT(BgL_vz00_4138);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8009;

					{
						obj_t BgL_auxz00_8010;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_8011;

							BgL_tmpz00_8011 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4137));
							BgL_auxz00_8010 = BGL_OBJECT_WIDENING(BgL_tmpz00_8011);
						}
						BgL_auxz00_8009 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8010);
					}
					return
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8009))->
							BgL_markz00) = ((long) BgL_vz00_4511), BUNSPEC);
		}}}

	}



/* &lambda1736 */
	obj_t BGl_z62lambda1736z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4139,
		obj_t BgL_oz00_4140)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				long BgL_tmpz00_8017;

				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8018;

					{
						obj_t BgL_auxz00_8019;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_8020;

							BgL_tmpz00_8020 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4140));
							BgL_auxz00_8019 = BGL_OBJECT_WIDENING(BgL_tmpz00_8020);
						}
						BgL_auxz00_8018 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8019);
					}
					BgL_tmpz00_8017 =
						(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8018))->
						BgL_markz00);
				}
				return BINT(BgL_tmpz00_8017);
			}
		}

	}



/* &<@anonymous:1723> */
	obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4141)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BNIL;
		}

	}



/* &lambda1722 */
	obj_t BGl_z62lambda1722z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4142,
		obj_t BgL_oz00_4143, obj_t BgL_vz00_4144)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8027;

				{
					obj_t BgL_auxz00_8028;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8029;

						BgL_tmpz00_8029 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4143));
						BgL_auxz00_8028 = BGL_OBJECT_WIDENING(BgL_tmpz00_8029);
					}
					BgL_auxz00_8027 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8028);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8027))->
						BgL_pluggedzd2inzd2) = ((obj_t) BgL_vz00_4144), BUNSPEC);
			}
		}

	}



/* &lambda1721 */
	obj_t BGl_z62lambda1721z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4145,
		obj_t BgL_oz00_4146)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8035;

				{
					obj_t BgL_auxz00_8036;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8037;

						BgL_tmpz00_8037 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4146));
						BgL_auxz00_8036 = BGL_OBJECT_WIDENING(BgL_tmpz00_8037);
					}
					BgL_auxz00_8035 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8036);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8035))->
					BgL_pluggedzd2inzd2);
			}
		}

	}



/* &<@anonymous:1713> */
	obj_t BGl_z62zc3z04anonymousza31713ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4147)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BNIL;
		}

	}



/* &lambda1712 */
	obj_t BGl_z62lambda1712z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4148,
		obj_t BgL_oz00_4149, obj_t BgL_vz00_4150)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8043;

				{
					obj_t BgL_auxz00_8044;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8045;

						BgL_tmpz00_8045 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4149));
						BgL_auxz00_8044 = BGL_OBJECT_WIDENING(BgL_tmpz00_8045);
					}
					BgL_auxz00_8043 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8044);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8043))->
						BgL_integratedz00) = ((obj_t) BgL_vz00_4150), BUNSPEC);
			}
		}

	}



/* &lambda1711 */
	obj_t BGl_z62lambda1711z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4151,
		obj_t BgL_oz00_4152)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8051;

				{
					obj_t BgL_auxz00_8052;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8053;

						BgL_tmpz00_8053 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4152));
						BgL_auxz00_8052 = BGL_OBJECT_WIDENING(BgL_tmpz00_8053);
					}
					BgL_auxz00_8051 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8052);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8051))->
					BgL_integratedz00);
			}
		}

	}



/* &<@anonymous:1704> */
	obj_t BGl_z62zc3z04anonymousza31704ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4153)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1703 */
	obj_t BGl_z62lambda1703z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4154,
		obj_t BgL_oz00_4155, obj_t BgL_vz00_4156)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8060;

				{
					obj_t BgL_auxz00_8061;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8062;

						BgL_tmpz00_8062 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4155));
						BgL_auxz00_8061 = BGL_OBJECT_WIDENING(BgL_tmpz00_8062);
					}
					BgL_auxz00_8060 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8061);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8060))->
						BgL_ownerz00) = ((obj_t) BgL_vz00_4156), BUNSPEC);
			}
		}

	}



/* &lambda1702 */
	obj_t BGl_z62lambda1702z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4157,
		obj_t BgL_oz00_4158)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8068;

				{
					obj_t BgL_auxz00_8069;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8070;

						BgL_tmpz00_8070 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4158));
						BgL_auxz00_8069 = BGL_OBJECT_WIDENING(BgL_tmpz00_8070);
					}
					BgL_auxz00_8068 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8069);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8068))->BgL_ownerz00);
			}
		}

	}



/* &<@anonymous:1691> */
	obj_t BGl_z62zc3z04anonymousza31691ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4159)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BNIL;
		}

	}



/* &lambda1690 */
	obj_t BGl_z62lambda1690z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4160,
		obj_t BgL_oz00_4161, obj_t BgL_vz00_4162)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8076;

				{
					obj_t BgL_auxz00_8077;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8078;

						BgL_tmpz00_8078 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4161));
						BgL_auxz00_8077 = BGL_OBJECT_WIDENING(BgL_tmpz00_8078);
					}
					BgL_auxz00_8076 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8077);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8076))->
						BgL_imarkz00) = ((obj_t) BgL_vz00_4162), BUNSPEC);
			}
		}

	}



/* &lambda1689 */
	obj_t BGl_z62lambda1689z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4163,
		obj_t BgL_oz00_4164)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8084;

				{
					obj_t BgL_auxz00_8085;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8086;

						BgL_tmpz00_8086 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4164));
						BgL_auxz00_8085 = BGL_OBJECT_WIDENING(BgL_tmpz00_8086);
					}
					BgL_auxz00_8084 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8085);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8084))->BgL_imarkz00);
			}
		}

	}



/* &<@anonymous:1664> */
	obj_t BGl_z62zc3z04anonymousza31664ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4165)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BUNSPEC;
		}

	}



/* &lambda1663 */
	obj_t BGl_z62lambda1663z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4166,
		obj_t BgL_oz00_4167, obj_t BgL_vz00_4168)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8092;

				{
					obj_t BgL_auxz00_8093;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8094;

						BgL_tmpz00_8094 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4167));
						BgL_auxz00_8093 = BGL_OBJECT_WIDENING(BgL_tmpz00_8094);
					}
					BgL_auxz00_8092 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8093);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8092))->
						BgL_integratorz00) = ((obj_t) BgL_vz00_4168), BUNSPEC);
			}
		}

	}



/* &lambda1662 */
	obj_t BGl_z62lambda1662z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4169,
		obj_t BgL_oz00_4170)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8100;

				{
					obj_t BgL_auxz00_8101;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8102;

						BgL_tmpz00_8102 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4170));
						BgL_auxz00_8101 = BGL_OBJECT_WIDENING(BgL_tmpz00_8102);
					}
					BgL_auxz00_8100 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8101);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8100))->
					BgL_integratorz00);
			}
		}

	}



/* &<@anonymous:1649> */
	obj_t BGl_z62zc3z04anonymousza31649ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4171)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BNIL;
		}

	}



/* &lambda1648 */
	obj_t BGl_z62lambda1648z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4172,
		obj_t BgL_oz00_4173, obj_t BgL_vz00_4174)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8108;

				{
					obj_t BgL_auxz00_8109;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8110;

						BgL_tmpz00_8110 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4173));
						BgL_auxz00_8109 = BGL_OBJECT_WIDENING(BgL_tmpz00_8110);
					}
					BgL_auxz00_8108 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8109);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8108))->
						BgL_efunctionsz00) = ((obj_t) BgL_vz00_4174), BUNSPEC);
			}
		}

	}



/* &lambda1647 */
	obj_t BGl_z62lambda1647z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4175,
		obj_t BgL_oz00_4176)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8116;

				{
					obj_t BgL_auxz00_8117;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8118;

						BgL_tmpz00_8118 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4176));
						BgL_auxz00_8117 = BGL_OBJECT_WIDENING(BgL_tmpz00_8118);
					}
					BgL_auxz00_8116 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8117);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8116))->
					BgL_efunctionsz00);
			}
		}

	}



/* &<@anonymous:1628> */
	obj_t BGl_z62zc3z04anonymousza31628ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4177)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1627 */
	obj_t BGl_z62lambda1627z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4178,
		obj_t BgL_oz00_4179, obj_t BgL_vz00_4180)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8125;

				{
					obj_t BgL_auxz00_8126;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8127;

						BgL_tmpz00_8127 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4179));
						BgL_auxz00_8126 = BGL_OBJECT_WIDENING(BgL_tmpz00_8127);
					}
					BgL_auxz00_8125 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8126);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8125))->
						BgL_ctoza2za2) = ((obj_t) BgL_vz00_4180), BUNSPEC);
			}
		}

	}



/* &lambda1626 */
	obj_t BGl_z62lambda1626z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4181,
		obj_t BgL_oz00_4182)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8133;

				{
					obj_t BgL_auxz00_8134;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8135;

						BgL_tmpz00_8135 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4182));
						BgL_auxz00_8134 = BGL_OBJECT_WIDENING(BgL_tmpz00_8135);
					}
					BgL_auxz00_8133 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8134);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8133))->
					BgL_ctoza2za2);
			}
		}

	}



/* &<@anonymous:1612> */
	obj_t BGl_z62zc3z04anonymousza31612ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4183)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BNIL;
		}

	}



/* &lambda1611 */
	obj_t BGl_z62lambda1611z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4184,
		obj_t BgL_oz00_4185, obj_t BgL_vz00_4186)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8141;

				{
					obj_t BgL_auxz00_8142;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8143;

						BgL_tmpz00_8143 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4185));
						BgL_auxz00_8142 = BGL_OBJECT_WIDENING(BgL_tmpz00_8143);
					}
					BgL_auxz00_8141 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8142);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8141))->BgL_ctoz00) =
					((obj_t) BgL_vz00_4186), BUNSPEC);
			}
		}

	}



/* &lambda1610 */
	obj_t BGl_z62lambda1610z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4187,
		obj_t BgL_oz00_4188)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8149;

				{
					obj_t BgL_auxz00_8150;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8151;

						BgL_tmpz00_8151 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4188));
						BgL_auxz00_8150 = BGL_OBJECT_WIDENING(BgL_tmpz00_8151);
					}
					BgL_auxz00_8149 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8150);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8149))->BgL_ctoz00);
			}
		}

	}



/* &<@anonymous:1597> */
	obj_t BGl_z62zc3z04anonymousza31597ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4189)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1596 */
	obj_t BGl_z62lambda1596z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4190,
		obj_t BgL_oz00_4191, obj_t BgL_vz00_4192)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8158;

				{
					obj_t BgL_auxz00_8159;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8160;

						BgL_tmpz00_8160 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4191));
						BgL_auxz00_8159 = BGL_OBJECT_WIDENING(BgL_tmpz00_8160);
					}
					BgL_auxz00_8158 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8159);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8158))->
						BgL_cfromza2za2) = ((obj_t) BgL_vz00_4192), BUNSPEC);
			}
		}

	}



/* &lambda1595 */
	obj_t BGl_z62lambda1595z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4193,
		obj_t BgL_oz00_4194)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8166;

				{
					obj_t BgL_auxz00_8167;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8168;

						BgL_tmpz00_8168 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4194));
						BgL_auxz00_8167 = BGL_OBJECT_WIDENING(BgL_tmpz00_8168);
					}
					BgL_auxz00_8166 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8167);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8166))->
					BgL_cfromza2za2);
			}
		}

	}



/* &<@anonymous:1588> */
	obj_t BGl_z62zc3z04anonymousza31588ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4195)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BNIL;
		}

	}



/* &lambda1587 */
	obj_t BGl_z62lambda1587z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4196,
		obj_t BgL_oz00_4197, obj_t BgL_vz00_4198)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8174;

				{
					obj_t BgL_auxz00_8175;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8176;

						BgL_tmpz00_8176 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4197));
						BgL_auxz00_8175 = BGL_OBJECT_WIDENING(BgL_tmpz00_8176);
					}
					BgL_auxz00_8174 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8175);
				}
				return
					((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8174))->
						BgL_cfromz00) = ((obj_t) BgL_vz00_4198), BUNSPEC);
			}
		}

	}



/* &lambda1586 */
	obj_t BGl_z62lambda1586z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4199,
		obj_t BgL_oz00_4200)
	{
		{	/* Globalize/ginfo.scm 20 */
			{
				BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8182;

				{
					obj_t BgL_auxz00_8183;

					{	/* Globalize/ginfo.scm 20 */
						BgL_objectz00_bglt BgL_tmpz00_8184;

						BgL_tmpz00_8184 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4200));
						BgL_auxz00_8183 = BGL_OBJECT_WIDENING(BgL_tmpz00_8184);
					}
					BgL_auxz00_8182 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8183);
				}
				return
					(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8182))->BgL_cfromz00);
			}
		}

	}



/* &<@anonymous:1574> */
	obj_t BGl_z62zc3z04anonymousza31574ze3ze5zzglobaliza7e_ginfoza7(obj_t
		BgL_envz00_4201)
	{
		{	/* Globalize/ginfo.scm 20 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1573 */
	obj_t BGl_z62lambda1573z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4202,
		obj_t BgL_oz00_4203, obj_t BgL_vz00_4204)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				bool_t BgL_vz00_4534;

				BgL_vz00_4534 = CBOOL(BgL_vz00_4204);
				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8192;

					{
						obj_t BgL_auxz00_8193;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_8194;

							BgL_tmpz00_8194 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4203));
							BgL_auxz00_8193 = BGL_OBJECT_WIDENING(BgL_tmpz00_8194);
						}
						BgL_auxz00_8192 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8193);
					}
					return
						((((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8192))->
							BgL_gzf3zf3) = ((bool_t) BgL_vz00_4534), BUNSPEC);
				}
			}
		}

	}



/* &lambda1572 */
	obj_t BGl_z62lambda1572z62zzglobaliza7e_ginfoza7(obj_t BgL_envz00_4205,
		obj_t BgL_oz00_4206)
	{
		{	/* Globalize/ginfo.scm 20 */
			{	/* Globalize/ginfo.scm 20 */
				bool_t BgL_tmpz00_8200;

				{
					BgL_sfunzf2ginfozf2_bglt BgL_auxz00_8201;

					{
						obj_t BgL_auxz00_8202;

						{	/* Globalize/ginfo.scm 20 */
							BgL_objectz00_bglt BgL_tmpz00_8203;

							BgL_tmpz00_8203 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_4206));
							BgL_auxz00_8202 = BGL_OBJECT_WIDENING(BgL_tmpz00_8203);
						}
						BgL_auxz00_8201 = ((BgL_sfunzf2ginfozf2_bglt) BgL_auxz00_8202);
					}
					BgL_tmpz00_8200 =
						(((BgL_sfunzf2ginfozf2_bglt) COBJECT(BgL_auxz00_8201))->
						BgL_gzf3zf3);
				}
				return BBOOL(BgL_tmpz00_8200);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzglobaliza7e_ginfoza7(void)
	{
		{	/* Globalize/ginfo.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2202z00zzglobaliza7e_ginfoza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2202z00zzglobaliza7e_ginfoza7));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2202z00zzglobaliza7e_ginfoza7));
		}

	}

#ifdef __cplusplus
}
#endif
